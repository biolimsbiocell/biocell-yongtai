package com.biolims.restful;

import javax.annotation.Resource;

import com.biolims.common.code.service.CodingRuleService;

public class Test {
	@Resource
	private static CodingRuleService  codingRuleService;
	public static void main(String[] args) throws Exception {
	
		String modelName = "CrmPatient";
		String markCode = "P";
		String autoID = codingRuleService.genTransID(modelName, markCode);
	    System.out.println(autoID);
	}
	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}
	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}
}
