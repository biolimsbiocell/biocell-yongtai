package com.biolims.restful;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.biolims.PropertyAttributionHolder;
import com.biolims.SpringContextHolder;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.model.user.User;
import com.biolims.core.user.service.UserService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.customer.service.CrmCustomerService;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.doctor.service.CrmDoctorService;
import com.biolims.dic.model.DicType;
import com.biolims.dic.service.DicTypeService;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.service.SampleInfoMainService;
import com.biolims.report.service.ReportService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleProductItem;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.system.family.model.Family;
import com.biolims.system.family.service.FamilyService;

@Path("RestfulService")
public class Restful {
	@POST	
	@Path("/getOrder")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	/**同步订单**/
	public String getMassarrayInfo(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws Exception {	
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setContentType("application/json;charset=utf-8");
        System.out.println("success");
		StringBuffer str = new StringBuffer();
		JSONObject json=new JSONObject(); 
		
		try {
			FileItemFactory fif = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(fif);
			List<FileItem> list = upload.parseRequest(request);
			File fileDir = null ;
			if(null != list){
				CrmCustomerService crmCustomerService = SpringContextHolder.getBean("crmCustomerService");
				CrmPatientService crmPatientService = SpringContextHolder.getBean("crmPatientService");
				CodingRuleService codingRuleService = SpringContextHolder.getBean("codingRuleService");
				CrmDoctorService crmDoctorService = SpringContextHolder.getBean("crmDoctorService");
				FamilyService familyService =  SpringContextHolder.getBean("familyService");
				UserService userService =  SpringContextHolder.getBean("userService");
				SampleOrderService sampleOrderService = (SampleOrderService)  SpringContextHolder.getBean("sampleOrderService");
				SampleInfoMainService sampleInfoMainService = SpringContextHolder.getBean("sampleInfoMainService");
				DicTypeService dicTypeService = SpringContextHolder.getBean("dicTypeService");
				for (FileItem fileItem : list) {
					String fKey = "";
					Object fValue = null;
					if(fileItem.isFormField()){
						fKey = fileItem.getFieldName();
						fValue = fileItem.getString("utf-8");
						System.out.println("接受的json"+fValue);
						if(null!=fValue && !"".equals(fValue)){
						fValue = URLDecoder.decode(fValue.toString(), "utf-8");
						JSONObject dataJson = JSONObject.fromObject(fValue);
						JSONArray jsorder=dataJson.getJSONArray("sampleOrder");
						
						List<SampleProductItem> items = new ArrayList<SampleProductItem>();
						for(int i1=0;i1<jsorder.size();i1++){
							JSONObject json1=(JSONObject) jsorder.get(i1);
							SampleOrder sampleOrder=new SampleOrder();
							/** 申请单编号 **/	
							if(null!=json1.getString("id")&&!"".equals(json1.getString("id"))&&!"null".equals(json1.getString("id"))){
								sampleOrder.setId(json1.getString("id"));
							}
							/**产品名称*/
							if(null!=json1.getString("productName")&&!"".equals(json1.getString("productName"))&&!"null".equals(json1.getString("productName"))){
								sampleOrder.setProductName(json1.getString("productName"));
							}
							/**产品Id*/
							if(null!=json1.getString("productId")&&!"".equals(json1.getString("productId"))&&!"null".equals(json1.getString("productId"))){
								sampleOrder.setProductId(json1.getString("productId"));
							}
							/**家系编号**/
							String serialCode = codingRuleService.getSerialCode("Family", "J", 6, "075000","id");
							if(null!=json1.getString("familyCode")&&!"".equals(json1.getString("familyCode"))&&!"null".equals(json1.getString("familyCode"))){		
								
								String faId = json1.getString("familyCode").toString();
								Family   family = familyService.get(faId);
								if(null==family){
									
									family = new Family();
									family.setId(serialCode);
									family.setState("1");
									familyService.save(family);
								}
								sampleOrder.setFamilyId(family);
							}else{
								Family   family = new Family();
									
								family = new Family();
								family.setId(serialCode);
								familyService.save(family);
								sampleOrder.setFamilyId(family);
							}
//							/** 受检人证件号 **/ /** 身份证号 **/ 
//							if(null!=json1.getString("idCard1")&&!"".equals(json1.getString("idCard1"))&&!"null".equals(json1.getString("idCard1"))){
//								CrmPatient crmPatient = crmPatientService.getPatient(json1.getString("idCard1"));
//								if(null==crmPatient){
//									/** 受检者编号  **/ /** 受检者编号   **/
//									if(null!=json1.getString("emrCode1")&&!"".equals(json1.getString("emrCode1"))&&!"null".equals(json1.getString("emrCode1"))){
//										crmPatient = crmPatientService.get(json1.getString("emrCode1"));
//										if(null==crmPatient){
//											String modelName = "CrmPatient";
//											String markCode = "P";
//											String autoID = codingRuleService.genTransID(modelName, markCode);
//											crmPatient = new CrmPatient();
//											crmPatient.setId(autoID);
//											crmPatient.setAge(json1.getString("patientAge1"));
//											crmPatient.setGender(json1.getString("gender1"));
//											crmPatient.setName(json1.getString("patientName1"));
////											DicType dicType = dicTypeService.slectDic("sfhb", json1.getString("phenotype1"));
////											crmPatient.setPhenotype(dicType);
//											crmPatient.setRace(json1.getString("patientNation1"));
//											crmPatient.setSfz(json1.getString("idCard1"));
//											crmPatient.setFamilyCode(sampleOrder.getFamilyId().getId());
//											crmPatient.setFamilyId(sampleOrder.getFamilyId());
////											FamilyRelation familyRelation = familyRelationService.get("X");
//											crmPatient.setCreateDate(sampleOrder.getCreateDate());
////											crmPatient.setModifyDate(new Date());
////											crmPatient.setRelation(familyRelation);
//											crmPatient.setPlaceOfBirth(json1.getString("nativePlace1"));
//											crmPatientService.save(crmPatient);
//							
//											SampleProductItem sampleProductItem = new SampleProductItem();
//											sampleProductItem.setName(json1.getString("patientName1"));
//											sampleProductItem.setPatientId(autoID);
////											sampleProductItem.setPhenotype(dicType);
//											sampleProductItem.setGender(json1.getString("gender1"));
//											sampleProductItem.setAge(json1.getString("patientAge1"));
//											sampleProductItem.setPlaceOfBirth(json1.getString("nativePlace1"));
//											sampleProductItem.setAddress(json1.getString("address1"));
//											sampleProductItem.setCreateDate(sampleOrder.getCreateDate());
////											sampleProductItem.setFamilyRelation(familyRelation);
//											sampleProductItem.setFamily(sampleOrder.getFamilyId());
//											sampleProductItem.setCreateUser(sampleOrder.getCreateUser());
//											sampleProductItem.setModifyDate(new Date());
//											sampleProductItem.setSampleOrder(sampleOrder);
//											items.add(sampleProductItem);
//											
//										}
//									}else{
//										String modelName = "CrmPatient";
//										String markCode = "P";
//										String autoID = codingRuleService.genTransID(modelName, markCode);
//										crmPatient = new CrmPatient();
//										crmPatient.setId(autoID);
//										crmPatient.setAge(json1.getString("patientAge1"));
//										crmPatient.setPlaceOfBirth(json1.getString("nativePlace1"));
//										crmPatient.setGender(json1.getString("gender1"));
//										crmPatient.setName(json1.getString("patientName1"));
////										DicType dicType = dicTypeService.slectDic("sfhb", json1.getString("phenotype1"));
////										crmPatient.setPhenotype(dicType);
//										crmPatient.setRace(json1.getString("patientNation1"));
//										crmPatient.setSfz(json1.getString("idCard1"));
//										crmPatient.setFamilyCode(sampleOrder.getFamilyId().getId());
//										crmPatient.setFamilyId(sampleOrder.getFamilyId());
////										FamilyRelation familyRelation = familyRelationService.get("X");
//										crmPatient.setCreateDate(sampleOrder.getCreateDate());
////										crmPatient.setModifyDate(new Date());
////										crmPatient.setRelation(familyRelation);
//										crmPatientService.save(crmPatient);
//						
//										SampleProductItem sampleProductItem = new SampleProductItem();
//										sampleProductItem.setName(json1.getString("patientName1"));
//										sampleProductItem.setPatientId(autoID);
////										sampleProductItem.setPhenotype(dicType);
//										sampleProductItem.setGender(json1.getString("gender1"));
//										sampleProductItem.setAge(json1.getString("patientAge1"));
//										sampleProductItem.setPlaceOfBirth(json1.getString("nativePlace1"));
//										sampleProductItem.setAddress(json1.getString("address1"));
//										sampleProductItem.setCreateDate(sampleOrder.getCreateDate());
////										sampleProductItem.setFamilyRelation(familyRelation);
//										sampleProductItem.setFamily(sampleOrder.getFamilyId());
//										sampleProductItem.setCreateUser(sampleOrder.getCreateUser());
//										sampleProductItem.setModifyDate(new Date());
//										sampleProductItem.setSampleOrder(sampleOrder);
//										items.add(sampleProductItem);
//										
//									}
//									
//								}
//							}else{
//								CrmPatient crmPatient = new CrmPatient();
//								String modelName = "CrmPatient";
//								String markCode = "P";
//								String autoID = codingRuleService.genTransID(modelName, markCode);
//								crmPatient = new CrmPatient();
//								crmPatient.setId(autoID);
//								crmPatient.setAge(json1.getString("patientAge1"));
//								crmPatient.setGender(json1.getString("gender1"));
//								crmPatient.setName(json1.getString("patientName1"));
////								DicType dicType = dicTypeService.slectDic("sfhb", json1.getString("phenotype1"));
////								crmPatient.setPhenotype(dicType);
//								crmPatient.setSfz(json1.getString("idCard1"));
//								crmPatient.setRace(json1.getString("patientNation1"));
//								crmPatient.setFamilyCode(sampleOrder.getFamilyId().getId());
//								crmPatient.setFamilyId(sampleOrder.getFamilyId());
////								FamilyRelation familyRelation = familyRelationService.get("X");
//								crmPatient.setCreateDate(sampleOrder.getCreateDate());
////								crmPatient.setModifyDate(new Date());
//								crmPatient.setPlaceOfBirth(json1.getString("nativePlace1"));
////								crmPatient.setRelation(familyRelation);
//								crmPatientService.save(crmPatient);
//				
//								SampleProductItem sampleProductItem = new SampleProductItem();
//								sampleProductItem.setName(json1.getString("patientName1"));
//								sampleProductItem.setPatientId(autoID);
////								sampleProductItem.setPhenotype(dicType);
//								sampleProductItem.setGender(json1.getString("gender1"));
//								sampleProductItem.setAge(json1.getString("patientAge1"));
//								sampleProductItem.setPlaceOfBirth(json1.getString("nativePlace1"));
//								sampleProductItem.setAddress(json1.getString("address1"));
//								sampleProductItem.setCreateDate(sampleOrder.getCreateDate());
////								sampleProductItem.setFamilyRelation(familyRelation);
//								sampleProductItem.setFamily(sampleOrder.getFamilyId());
//								sampleProductItem.setCreateUser(sampleOrder.getCreateUser());
//								sampleProductItem.setModifyDate(new Date());
//								sampleProductItem.setSampleOrder(sampleOrder);
//								items.add(sampleProductItem);
//								
//							}
						
							
							/**备注*/
							if(null!=json1.getString("note")&&!"".equals(json1.getString("note"))&&!"null".equals(json1.getString("note"))){
								sampleOrder.setNote(json1.getString("note"));
							}					
							
							/** 受检人证件号 **/ /** 身份证号 **/ 
							if(null!=json1.getString("idCard")&&!"".equals(json1.getString("idCard"))&&!"null".equals(json1.getString("idCard"))){
								sampleOrder.setIdCard(json1.getString("idCard"));
							}			
							/** 受检者编号  **/ /** 受检者编号   **/
							if(null!=json1.getString("emrCode")&&!"".equals(json1.getString("emrCode"))&&!"null".equals(json1.getString("emrCode"))){
								CrmPatient crmPatient = crmPatientService.get(json1.getString("emrCode"));
								if(null==crmPatient){
									if(null!=json1.getString("idCard")&&!"".equals(json1.getString("idCard"))&&!"null".equals(json1.getString("idCard"))){
										
										crmPatient = crmPatientService.getPatient(json1.getString("idCard"));
										if(null==crmPatient){
											String modelName = "CrmPatient";
											String markCode = "P";
											String autoID = codingRuleService.genTransID(modelName, markCode);
											crmPatient = new CrmPatient();
											crmPatient.setId(autoID);
											crmPatient.setAge(json1.getString("patientAge"));
											crmPatient.setPlaceOfBirth(json1.getString("nativePlace"));
											crmPatient.setGender(json1.getString("gender"));
//											DicType dicType = dicTypeService.slectDic("sfhb", json1.getString("phenotype"));
//											crmPatient.setPhenotype(dicType);
											crmPatient.setName(json1.getString("patientName"));
											crmPatient.setSfz(json1.getString("idCard"));
											crmPatient.setFamilyId(sampleOrder.getFamilyId());
											crmPatient.setRace(json1.getString("patientNation"));
											crmPatient.setFamilyCode(sampleOrder.getFamilyId().getId());
//											FamilyRelation familyRelation = familyRelationService.get("X");
//											crmPatient.setRelation(familyRelation);
											crmPatient.setCreateDate(sampleOrder.getCreateDate());
//											crmPatient.setModifyDate(new Date());
											crmPatientService.save(crmPatient);
											
											SampleProductItem sampleProductItem = new SampleProductItem();
											sampleProductItem.setName(json1.getString("patientName"));
											sampleProductItem.setPatientId(autoID);
//											sampleProductItem.setPhenotype(dicType);
											sampleProductItem.setGender(json1.getString("gender"));
											sampleProductItem.setAge(json1.getString("patientAge"));
											sampleProductItem.setPlaceOfBirth(json1.getString("nativePlace"));
											sampleProductItem.setAddress(json1.getString("address"));
//											sampleProductItem.setFamilyRelation(familyRelation);
											sampleProductItem.setFamily(sampleOrder.getFamilyId());
											sampleProductItem.setCreateDate(sampleOrder.getCreateDate());
											sampleProductItem.setCreateUser(sampleOrder.getCreateUser());
											sampleProductItem.setModifyDate(new Date());
											sampleProductItem.setSampleOrder(sampleOrder);
											items.add(sampleProductItem);
											
											sampleOrder.setMedicalNumber(autoID);					
										}else{
											sampleOrder.setMedicalNumber(crmPatient.getId());
										}
									}
								
									
								}else{
									sampleOrder.setMedicalNumber(crmPatient.getId());
								}
							}else{
								if(null!=json1.getString("idCard")&&!"".equals(json1.getString("idCard"))&&!"null".equals(json1.getString("idCard"))){
									
									CrmPatient crmPatient = crmPatientService.getPatient(json1.getString("idCard"));
									if(null==crmPatient){				
										String modelName = "CrmPatient";
										String markCode = "P";
										String autoID = codingRuleService.genTransID(modelName, markCode);
										crmPatient = new CrmPatient();
										crmPatient.setId(autoID);
										crmPatient.setAge(json1.getString("patientAge"));
										crmPatient.setGender(json1.getString("gender"));
										crmPatient.setPlaceOfBirth(json1.getString("nativePlace"));
//										DicType dicType = dicTypeService.slectDic("sfhb",json1.getString("phenotype"));
//										crmPatient.setPhenotype(dicType);
										crmPatient.setName(json1.getString("patientName"));
										crmPatient.setSfz(json1.getString("idCard"));
										crmPatient.setFamilyId(sampleOrder.getFamilyId());
										crmPatient.setRace(json1.getString("patientNation"));
										crmPatient.setFamilyCode(sampleOrder.getFamilyId().getId());
//										FamilyRelation familyRelation = familyRelationService.get("X");
										crmPatient.setCreateDate(sampleOrder.getCreateDate());
//										crmPatient.setModifyDate(new Date());
//										crmPatient.setRelation(familyRelation);
										crmPatientService.save(crmPatient);
										
										SampleProductItem sampleProductItem = new SampleProductItem();
										sampleProductItem.setName(json1.getString("patientName"));
										sampleProductItem.setPatientId(autoID);
//										sampleProductItem.setPhenotype(dicType);
										sampleProductItem.setGender(json1.getString("gender"));
										sampleProductItem.setAge(json1.getString("patientAge"));
										sampleProductItem.setPlaceOfBirth(json1.getString("nativePlace"));
										sampleProductItem.setAddress(json1.getString("address"));
										sampleProductItem.setCreateDate(sampleOrder.getCreateDate());
										sampleProductItem.setCreateUser(sampleOrder.getCreateUser());
										sampleProductItem.setFamily(sampleOrder.getFamilyId());
//										sampleProductItem.setFamilyRelation(familyRelation);
										sampleProductItem.setModifyDate(new Date());
										sampleProductItem.setSampleOrder(sampleOrder);
										items.add(sampleProductItem);
										
										sampleOrder.setMedicalNumber(autoID);					
									}else{
										sampleOrder.setMedicalNumber(crmPatient.getId());
									}
								}
									
									
							}
							/** 受检人姓名/先证  **/ /** 姓名  **/
							if(null!=json1.getString("patientName")&&!"".equals(json1.getString("patientName"))&&!"null".equals(json1.getString("patientName"))){
								sampleOrder.setName(json1.getString("patientName"));
							}
							/** 受检人电话  **/ /** 受检人电话  **/
							if(null!=json1.getString("patientMobile")&&!"".equals(json1.getString("patientMobile"))&&!"null".equals(json1.getString("patientMobile"))){
								sampleOrder.setPhone(json1.getString("patientMobile"));
							}
							/** 受检人性别 **/ /** 姓名  **/
							if(null!=json1.getString("gender")&&!"".equals(json1.getString("gender"))&&!"null".equals(json1.getString("gender"))){
								sampleOrder.setGender(json1.getString("gender"));
							}
							/** 受检人年龄**/  /** 年龄**/
							if(null!=json1.getString("patientAge")&&!"".equals(json1.getString("patientAge"))&&!"null".equals(json1.getString("patientAge"))){
								sampleOrder.setAge(json1.getString("patientAge"));
							}
							/** 民族**/ /** 民族**/
							if(null!=json1.getString("patientNation")&&!"".equals(json1.getString("patientNation"))&&!"null".equals(json1.getString("patientNation"))){
								sampleOrder.setNation(json1.getString("patientNation"));
							}
							/** 送检单位 **/ /** 医疗机构 */
							
							if(null!=json1.getJSONObject("sendHospital").getString("id")&&!"".equals(json1.getJSONObject("sendHospital").getString("id"))&&!"null".equals(json1.getJSONObject("sendHospital").getString("id"))){
								String id =json1.getJSONObject("sendHospital").getString("id").toString();
								CrmCustomer cr=crmCustomerService.get(id);
								sampleOrder.setCrmCustomer(cr);
							}			
							/** 送检单位名称 **/ /** 医疗机构 名称*/
							if(null!=json1.getJSONObject("sendHospital").getString("name")&&!"".equals(json1.getJSONObject("sendHospital").getString("name"))&&!"null".equals(json1.getJSONObject("sendHospital").getString("name"))){
								sampleOrder.setMedicalInstitutions(json1.getJSONObject("sendHospital").getString("name"));
							}
							/** 送检单位地址 **/ /** 医疗机构联系地址 */
							if(null!=json1.getString("sendAddress")&&!"".equals(json1.getString("sendAddress"))&&!"null".equals(json1.getString("sendAddress"))){
								sampleOrder.setMedicalInstitutionsSite(json1.getString("sendAddress"));
							}
							/** 医生联系电话 **/ /** 医生联系电话 */
							if(null!=json1.getString("doctorMobile")&&!"".equals(json1.getString("doctorMobile"))&&!"null".equals(json1.getString("doctorMobile"))){
								sampleOrder.setMedicalInstitutionsPhone(json1.getString("doctorMobile"));
							}
							
							/** 申请医师 **/ /** 送检医师*/
							if(null!=json1.getJSONObject("applyDoctor").getString("id")&&!"".equals(json1.getJSONObject("applyDoctor").getString("id"))&&!"null".equals(json1.getJSONObject("applyDoctor").getString("id"))){
								
								String crmdId = json1.getJSONObject("applyDoctor").getString("id").toString();
								CrmDoctor  crmDoctor = crmDoctorService.get(crmdId);
								sampleOrder.setCrmDoctor(crmDoctor);
							}
							/** 申请医师 姓名**/ /** 送检医师姓名*/
							if(null!=json1.getJSONObject("applyDoctor").getString("name")&&!"".equals(json1.getJSONObject("applyDoctor").getString("name"))&&!"null".equals(json1.getJSONObject("applyDoctor").getString("name"))){
								sampleOrder.setPhysicianName(json1.getJSONObject("applyDoctor").getString("name"));
							}
							/** 申请医师电话 **/ /** 送检医师电话*/
							if(null!=json1.getJSONObject("applyDoctor").getString("mobile")&&!"".equals(json1.getJSONObject("applyDoctor").getString("mobile"))&&!"null".equals(json1.getJSONObject("applyDoctor").getString("mobile"))){
								sampleOrder.setPhysicianPhone(json1.getJSONObject("applyDoctor").getString("mobile"));
							}
							/** 报告接收邮箱 **/ /** 送检医师邮箱*/
							if(null!=json1.getString("reportReceiveEmail")&&!"".equals(json1.getString("reportReceiveEmail"))&&!"null".equals(json1.getString("reportReceiveEmail"))){
								sampleOrder.setPhysicianEmail(json1.getString("reportReceiveEmail"));
							}
							/** 临床诊断**/ /** 临床诊断**/
							if(null!=json1.getString("diagnosis")&&!"".equals(json1.getString("diagnosis"))&&!"null".equals(json1.getString("diagnosis"))){
								sampleOrder.setDiagnosis(json1.getString("diagnosis"));
							}
							/** 病历关键词**/ /** 病历关键词 **/
//							if(null!=json1.getString("emrKeyWord")&&!"".equals(json1.getString("emrKeyWord"))&&!"null".equals(json1.getString("emrKeyWord"))){
//								sampleOrder.setEmrKeyWord(json1.getString("emrKeyWord"));
//							}
							/** 通信地址**/
							if(null!=json1.getString("address")&&!"".equals(json1.getString("address"))&&!"null".equals(json1.getString("address"))){
								sampleOrder.setAddress(json1.getString("address"));
							}
							/** 应收金额**/
//							if(null!=json1.getString("chargeAmount")&&!"".equals(json1.getString("chargeAmount"))&&!"null".equals(json1.getString("chargeAmount"))){
//								sampleOrder.setChargeAmount(Double.parseDouble(json1.getString("chargeAmount")));
//							}
							/** 详细地址**/ 
//							if(null!=json1.getString("detailAddr")&&!"".equals(json1.getString("detailAddr"))){
//								sampleOrder.setDetailAddr(json1.getString("detailAddr"));
//							}
							/** 报告邮箱**/
//							if(null!=json1.getString("reportReceiveEmail")&&!"".equals(json1.getString("reportReceiveEmail"))&&!"null".equals(json1.getString("reportReceiveEmail"))){
//								sampleOrder.setReportReceiveEmail(json1.getString("reportReceiveEmail"));
//							}
							/** 折扣**/
//							if(null!=json1.getString("discount")&&!"".equals(json1.getString("discount"))&&!"null".equals(json1.getString("discount"))){
//								sampleOrder.setDiscount(json1.getString("discount"));
//							}
							/** 收费说明**/ /** 收费说明**/
							if(null!=json1.getString("chargeDec")&&!"".equals(json1.getString("chargeDec"))&&!"null".equals(json1.getString("chargeDec"))){
								sampleOrder.setChargeNote(json1.getString("chargeDec"));
							}
							/**送检科室**/
							if(null!=json1.getJSONObject("sendDepartment").getString("id")&&!"".equals(json1.getJSONObject("sendDepartment").getString("id"))&&!"null".equals(json1.getJSONObject("sendDepartment").getString("id"))){
								
								String crmdeId = json1.getJSONObject("sendDepartment").getString("id").toString();
								DicType  crmDepartment = dicTypeService.findDicTypeById(crmdeId).get(0);
								sampleOrder.setInspectionDepartment(crmDepartment);
							}
							
							/**录入人信息**/
							if(null!=json1.getString("createUser")&&!"".equals(json1.getString("createUser"))&&!"null".equals(json1.getString("createUser"))){
							if(null!=json1.getJSONObject("createUser").getString("id")&&!"".equals(json1.getJSONObject("createUser").getString("id"))&&!"null".equals(json1.getJSONObject("createUser").getString("id"))){
							
								String userId = json1.getJSONObject("createUser").getString("id").toString();
								User  commissioner = userService.findUser(userId);
								sampleOrder.setCommissioner(commissioner);
							}
							}
							/** 籍贯**/ /** 籍贯*/
							if(null!=json1.getString("nativePlace")&&!"".equals(json1.getString("nativePlace"))&&!"null".equals(json1.getString("nativePlace"))){
								sampleOrder.setNativePlace(json1.getString("nativePlace"));
							}
							/** 是否患病**/ /** 是否患病*/
//							if(null!=json1.getString("phenotype")&&!"".equals(json1.getString("phenotype"))&&!"null".equals(json1.getString("phenotype"))){
//								DicType phenotype =	dicTypeService.slectDic("sfhb",json1.getString("phenotype").toString() );
//								if(null!=phenotype){
//								sampleOrder.setPhenotype(phenotype);
//								}
//							}
							/** 健康状况**/ /**健康状况*/
							if(null!=json1.getString("healthDetail")&&!"".equals(json1.getString("healthDetail"))&&!"null".equals(json1.getString("healthDetail"))){
								sampleOrder.setMedicalHistory(json1.getString("healthDetail"));
							}
							/** 费用类型**/ /** 费用表型*/
//							if(null!=json1.getString("feeType")&&!"".equals(json1.getString("feeType"))&&!"null".equals(json1.getString("feeType"))){
//								DicType feeType =	dicTypeService.slectDic("fylx",json1.getString("feeType").toString() );
//								if(null!=feeType){
//								sampleOrder.setFeeType(feeType);
//								}
//							}
							/** 是否需要发票**/ /** 是否需要发票*/
//							if(null!=json1.getString("needBill")&&!"".equals(json1.getString("needBill"))&&!"null".equals(json1.getString("needBill"))){
//								sampleOrder.setNeedBill(json1.getString("needBill"));
//							}
							/** 录入时间**/ /** 创建时间 */
							if(null!=json1.getString("createDate")&&!"".equals(json1.getString("createDate"))&&!"null".equals(json1.getString("createDate"))){
							String createDate =	json1.getString("createDate");			
							SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");//小写的mm表示的是分钟  		
							java.util.Date date=sdf.parse(createDate);
								sampleOrder.setCreateDate(date);
							}
							/** 修改时间**/ /** 修改时间 */
							if(null!=json1.getString("modifyDate")&&!"".equals(json1.getString("modifyDate"))&&!"null".equals(json1.getString("modifyDate"))){
								String modifyDate =	json1.getString("modifyDate");			
								SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//小写的mm表示的是分钟  		
								java.util.Date date=sdf.parse(modifyDate);
								sampleOrder.setCreateDate(date);
							}
							/** 送检日期**/ /** 送检日期 */
							if(null!=json1.getString("sendDate")&&!"".equals(json1.getString("sendDate"))&&!"null".equals(json1.getString("sendDate"))){
							String sendDate =	json1.getString("sendDate");			
							SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");//小写的mm表示的是分钟  		
							java.util.Date date=sdf.parse(sendDate);
								sampleOrder.setSendDate(date);
							}
							sampleOrderService.save(sampleOrder);
						}		
						/** 费用信息 **/
//						JSONArray jsfee=dataJson.getJSONArray("fee");			
//						for(int n=0;n<jsfee.size();n++){
//							FeeInfo fee=new FeeInfo();
//							JSONObject jsonFee=(JSONObject) jsfee.get(n);
//							SampleOrder  sam = null;
//							if(null!=jsonFee.getJSONObject("appId").getString("id")&&!"".equals(jsonFee.getJSONObject("appId").getString("id"))){
//								String appId = jsonFee.getJSONObject("appId").getString("id").toString();
//								  sam = sampleOrderService.get(appId);
//								fee.setSampleOrder(sam);
//							}
//							if(null!=jsonFee.getString("chargeAmount")&&!"".equals(jsonFee.getString("chargeAmount"))&&!"null".equals(jsonFee.getString("chargeAmount"))){
//								fee.setChargeAmount(Double.parseDouble(jsonFee.getString("chargeAmount")));
//							}
//							if(null!=jsonFee.getString("chargeMethod")&&!"".equals(jsonFee.getString("chargeMethod"))&&!"null".equals(jsonFee.getString("chargeMethod"))){
//								fee.setChargeMethod(jsonFee.getString("chargeMethod"));
//							}
//							if(null!=jsonFee.getString("dealBank")&&!"".equals(jsonFee.getString("dealBank"))&&!"null".equals(jsonFee.getString("dealBank"))){
//								fee.setDealBank(jsonFee.getString("dealBank"));
//							}
//							if(null!=jsonFee.getString("feeAccount")&&!"".equals(jsonFee.getString("feeAccount"))&&!"null".equals(jsonFee.getString("feeAccount"))){
//								fee.setFeeAccount(jsonFee.getString("feeAccount"));
//							}
//							if(null!=jsonFee.getString("refNo")&&!"".equals(jsonFee.getString("refNo"))&&!"null".equals(jsonFee.getString("refNo"))){
//								fee.setRefNo(jsonFee.getString("refNo"));
//							}
//							if(null!=jsonFee.getString("backup1")&&!"".equals(jsonFee.getString("backup1"))&&!"null".equals(jsonFee.getString("backup1"))){
//								fee.setFeeAccount(jsonFee.getString("backup1"));
//							}
//							if(null!=jsonFee.getString("backup2")&&!"".equals(jsonFee.getString("backup2"))&&!"null".equals(jsonFee.getString("backup2"))){
//								fee.setFeeAccount(jsonFee.getString("backup2"));
//							}
//							if(null!=jsonFee.getString("backup3")&&!"".equals(jsonFee.getString("backup3"))&&!"null".equals(jsonFee.getString("backup3"))){
//								fee.setFeeAccount(jsonFee.getString("backup3"));
//							}
//							fee.setCreateDate(sam.getCreateDate());
//							fee.setModifyDate(new Date());
//							feeInfoService.save(fee);
//						}
						
//						JSONArray sample=dataJson.getJSONArray("sample");			
//						for(int n=0;n<sample.size();n++){
//							SampleOrderItem orderItem=new SampleOrderItem();
//							JSONObject sampleJson=(JSONObject) sample.get(n);
//							if(null!=sampleJson.getString("feeType")&&!"".equals(sampleJson.getString("feeType"))&&!"null".equals(sampleJson.getString("feeType"))){
//								orderItem.setFeeType(sampleJson.getString("feeType"));
//							}
//							if(null!=sampleJson.getString("name")&&!"".equals(sampleJson.getString("name"))){
//								orderItem.setName(sampleJson.getString("name"));
//							}
//							if(null!=sampleJson.getString("party")&&!"".equals(sampleJson.getString("party"))&&!"null".equals(sampleJson.getString("party"))){
//								orderItem.setParty(sampleJson.getString("party"));
//							}
//							if(null!=sampleJson.getString("processState")&&!"".equals(sampleJson.getString("processState"))&&!"null".equals(sampleJson.getString("processState"))){
//								orderItem.setProcessState(sampleJson.getString("processState"));
//							}
//							if(!sampleJson.getJSONObject("relation").isNullObject()){
//								FamilyRelation relation = new FamilyRelation();
//								relation.setId(sampleJson.getJSONObject("relation").getString("id"));
//								
//								orderItem.setRelation(relation);
//							}
//							
//							if(null!=sampleJson.getString("replenishBatch")&&!"".equals(sampleJson.getString("replenishBatch"))&&!"null".equals(sampleJson.getString("replenishBatch"))){
//								orderItem.setReplenishBatch(Integer.parseInt(sampleJson.getString("replenishBatch")));
//							}
//							/**采样日期*/
//							if(null!=sampleJson.getString("sampleDate")&&!"".equals(sampleJson.getString("sampleDate"))&&!"null".equals(sampleJson.getString("sampleDate"))){
//								String samplingDate = sampleJson.getString("sampleDate");	
//								System.out.println(sampleJson.getString("sampleDate"));
//								SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");//小写的mm表示的是分钟  		
//								java.util.Date date=sdf.parse(samplingDate);
//								orderItem.setSamplingDate(date);
//							}
//							/**样本状态*/
//							if(null!=sampleJson.getString("sampleState")&&!"".equals(sampleJson.getString("sampleState"))&&!"null".equals(sampleJson.getString("sampleState"))){
//								orderItem.setState(sampleJson.getString("sampleState"));
//							}
//							/**样本条码*/
//							if(null!=sampleJson.getString("barcode")&&!"".equals(sampleJson.getString("barcode"))&&!"null".equals(sampleJson.getString("barcode"))){
//								orderItem.setSlideCode(sampleJson.getString("barcode"));
//							}
//
//							/**样本录入时间*//**样本接收时间*/
//							if(null!=sampleJson.getString("createDate")&&!"".equals(sampleJson.getString("createDate"))&&!"null".equals(sampleJson.getString("createDate"))){
//								String receiveDate =	sampleJson.getString("createDate");			
//								SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");//小写的mm表示的是分钟  		
//								java.util.Date date=sdf.parse(receiveDate);
//								orderItem.setReceiveDate(date);
//							}
//							if(!sampleJson.getJSONObject("sampleType").isNullObject()){
//								DicSampleTypeService dicSampleTypeService = SpringContextHolder.getBean("dicSampleTypeService");
//								String dicId = sampleJson.getJSONObject("sampleType").getString("id").toString();
//								DicSampleType  sam = dicSampleTypeService.get(dicId);
//								orderItem.setSampleType(sam);
//								
//							}
//							SampleOrder  sam = null;
//							if(!sampleJson.getJSONObject("applyId").isNullObject()){
//								String appId = sampleJson.getJSONObject("applyId").getString("id").toString();
//								sam = sampleOrderService.get(appId);
//								orderItem.setSampleOrder(sam);
//							}
//							if(null!=sam.getFamilyId()){
//								orderItem.setFamily(sam.getFamilyId());
//							}
//							orderItem.setCreateDate(sam.getCreateDate());
//							orderItem.setModifyDate(new Date());
//							sampleOrderService.save1(orderItem);
//							/**
//							 * 物流信息
//							 */
//							JSONArray procession=dataJson.getJSONArray("procession");
//							
//							for (Object object : procession) {
//								JSONObject object2 = JSONObject.fromObject(object);
//								String sampleOrderId= object2.getString("id");
//								String processionId = object2.getString("expressNumber");
//								SampleInfoMain sampleInfoMain = sampleInfoMainService.get(processionId);
//								if(null != sampleInfoMain){
//									SampleOrder sampleOrder2 = sampleOrderService.get(sampleOrderId);
//									if(null != sampleOrder2){
//										sampleOrder2.setSampleInfoMain(processionId);
//										sampleOrderService.save(sampleOrder2);
//									}
//								}else{
//									sampleInfoMain = new SampleInfoMain();
//									sampleInfoMain.setId(processionId);
//									sampleInfoMainService.save(sampleInfoMain);
//									SampleOrder sampleOrder2 = sampleOrderService.get(sampleOrderId);
//									if(null != sampleOrder2){
//										sampleOrder2.setSampleInfoMain(processionId);
//										sampleOrderService.save(sampleOrder2);
//									}
//								}
//								
//							}
//							}
							
							/**
							 * 如果有家系信息则保存家系
							 */
//							if(!"".equals(dataJson.getString("family"))){
//								JSONArray familyArry=dataJson.getJSONArray("family");
//								if(familyArry.size()>0){					
//									for(int n=0;n<familyArry.size();n++){
//										JSONObject familyJson = (JSONObject)familyArry.get(n);
//										if(null==familyJson.getString("emrCode") || "".equals(familyJson.getString("emrCode")) || "null".equals(familyJson.getString("emrCode"))){
//											String modelName = "CrmPatient";
//											String markCode = "P";
//											String autoID = codingRuleService.genTransID(modelName, markCode);
//											String serialCode = codingRuleService.getSerialCode("CrmPatient", "M", 6, "075000","familyMemberCode");
//											CrmPatient  crmPatient = new CrmPatient();
//											crmPatient.setId(autoID);
//											String sampleOrderId = familyJson.getJSONObject("applyId").getString("id");
//											SampleOrder sampleOrder = sampleOrderService.get(sampleOrderId);
//											if(null!=sampleOrder.getFamilyId()){
//												crmPatient.setFamilyId(sampleOrder.getFamilyId());	
//												crmPatient.setFamilyCode(sampleOrder.getFamilyId().getId());
//											}
////											crmPatient.setFamilyMemberCode(serialCode);
//											crmPatient.setName(familyJson.getString("name"));
//											crmPatient.setGender(familyJson.getString("gender"));
//											//crmPatient.setRelation((FamilyRelation) (familyJson.get("relation")));
//											String relationId = familyJson.getJSONObject("relation").getString("id");
////											FamilyRelation  relation = familyRelationService.get(relationId);
////											crmPatient.setRelation(relation);
////											DicType dicType = dicTypeService.slectDic("sfhb", familyJson.getString("phenotype"));
////											crmPatient.setPhenotype(dicType);				
//											crmPatient.setAge((familyJson.getString("age")));
//											crmPatient.setCreateDate(sampleOrder.getCreateDate());
////											crmPatient.setModifyDate(new Date());
//											crmPatientService.save(crmPatient);
//											
//											
//											SampleProductItem sampleProductItem = new SampleProductItem();
//											sampleProductItem.setName(crmPatient.getName());
//											sampleProductItem.setPatientId(autoID);
//											if(null!=sampleOrder.getFamilyId()){
//												sampleProductItem.setFamily(sampleOrder.getFamilyId());
//											}
////											FamilyRelation familyRelation = sampleOrderService.getSampleItemByName(crmPatient.getName()).getRelation();
////											if(null!=familyRelation){
////												sampleProductItem.setFamilyRelation(familyRelation);
////											}
////											sampleProductItem.setPhenotype(crmPatient.getPhenotype());
//											sampleProductItem.setGender(crmPatient.getGender());
//											sampleProductItem.setAge(crmPatient.getAge());
//											sampleProductItem.setPlaceOfBirth(crmPatient.getPlaceOfBirth());
//											sampleProductItem.setAddress(crmPatient.getAddress());
//											sampleProductItem.setCreateDate(crmPatient.getCreateDate());
//											sampleProductItem.setModifyDate(new Date());
//											sampleProductItem.setCreateUser(crmPatient.getCreateUser());
//											sampleProductItem.setSampleOrder(sampleOrder);
//											items.add(sampleProductItem);
//										}
//									}
//								}
//							}
//							sampleOrderService.save(items);
						}
					}else{
						fKey=fileItem.getFieldName();
						fValue=fileItem.getInputStream();
						String name = fileItem.getName();
						if(null != name && !"".equals(name)){
							name = name.substring(name.lastIndexOf(File.separator)+1);
							String dirPath = PropertyAttributionHolder.getAttribute("dir_path");
							fileDir = new File(dirPath);
							if(!fileDir.exists())fileDir.mkdir();
							String filePath = dirPath+File.separator+name;
							InputStream in = (InputStream) fValue;
							FileOutputStream fos = new FileOutputStream(filePath);
							byte[] buffer = new byte[1024];
							while(in.read(buffer)!=-1){
								fos.write(buffer, 0, buffer.length);
							}
							fos.flush();  
		                    fos.close();  
		                    in.close();
						}
					}
				}
			}
						
			json.put("success",true);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			json.put("success", false);
			json.put("errmsg", ex.getMessage());
		}
		System.out.println("要发送的的Json"+json.toString());	
		return json.toString();
	}
	
	@POST
	@Path("/confirmPatient")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String confirmPatient(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws Exception {	
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setContentType("application/json;charset=utf-8");
		System.out.println("success");
		StringBuffer str = new StringBuffer();
		JSONObject json=new JSONObject(); 
		
		try {
			BufferedInputStream in = new BufferedInputStream(request.getInputStream());
			int i;
			char c;
			while ((i = in.read()) != -1) {
				c = (char) i;
				str.append(c);
			}
			String strJson = java.net.URLDecoder.decode(str.toString(),"utf-8");
			System.out.println("接受的json"+strJson);
			if(strJson!=null && !"".equals(strJson)){
				JSONObject dataJson = JSONObject.fromObject(strJson);
				CrmPatient crmPatient = null;
				
				//注入service
				CrmPatientService crmPatientService = (CrmPatientService)  SpringContextHolder.getBean("crmPatientService");
				
				//如果传来的是身份证号，则按身份证查询，如果是受检者编号，则按电子病历号查询
				if(dataJson.has("idCard")){
					String idCard = dataJson.getString("idCard");
					crmPatient = crmPatientService.getPatient(idCard);
				}else if(dataJson.has("emrCode")){
					String emrCode = dataJson.getString("emrCode");
					crmPatient = crmPatientService.get(emrCode);
					
				}
				//如果查到信息则返回，否则返回空
				if(null != crmPatient)json.put("patient", JSONObject.fromObject(crmPatient).toString());
				else json.put("patient", "");
			}	
			json.put("success",true);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			json.put("success", false);
			json.put("errmsg", ex.getMessage());
		}
		System.out.println("要发送的的Json"+json.toString());	
		return json.toString();
	}
//	@POST
//	@Path("/getFamily")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	public String getFamily(@Context HttpServletRequest request,
//			@Context HttpServletResponse response) throws Exception {	
//		response.setHeader("Access-Control-Allow-Origin", "*");
//		response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE");
//		response.setHeader("Access-Control-Max-Age", "3600");
//		response.setContentType("application/json;charset=utf-8");
//		System.out.println("success");
//		StringBuffer str = new StringBuffer();
//		JSONObject json=new JSONObject(); 
//		
//		try {
//			BufferedInputStream in = new BufferedInputStream(request.getInputStream());
//			int i;
//			char c;
//			while ((i = in.read()) != -1) {
//				c = (char) i;
//				str.append(c);
//			}
//			String strJson = java.net.URLDecoder.decode(str.toString(),"utf-8");
//			System.out.println("接受的json"+strJson);
//			if(strJson!=null && !"".equals(strJson)){
//				JSONObject dataJson = JSONObject.fromObject(strJson);
//				Family family = null;
//				JSONArray jsar=new JSONArray();
//				//注入service
//				FamilyService familyService = (FamilyService)  SpringContextHolder.getBean("familyService");
//				
//				if(dataJson.has("familyCode")){
//					String familyCode = dataJson.getString("familyCode");
//					family = familyService.get(familyCode);
//					if(family!=null){
//					CrmPatientService crmPatientService = (CrmPatientService)  SpringContextHolder.getBean("crmPatientService");	
//					List<CrmPatient> list=crmPatientService.selectByFamilyID(familyCode);
//					for(int x = 0;x<list.size();x++){
//						JSONObject crmPatient = JSONObject.fromObject(list.get(x));
//						jsar.add(crmPatient);
//					}
//					}					
//					
//				}
//				//如果查到信息则返回，否则返回空
//				if(jsar.size()>0)json.put("family", JSONArray.fromObject(jsar).toString());
//				else json.put("family", "");
//			}
//
//			json.put("success",true);
//			
//		} catch (Exception ex){
//			ex.printStackTrace();
//			json.put("success", false);
//			json.put("errmsg", ex.getMessage());
//		}
//		System.out.println("要发送的的Json"+json.toString());	
//		return json.toString();
//	}
//	@POST
//	@Path("/getFile")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.MULTIPART_FORM_DATA)
//	public String getFile(@Context HttpServletRequest request,
//			@Context HttpServletResponse response) throws Exception {	
//		response.setHeader("Access-Control-Allow-Origin", "*");
//		response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE");
//		response.setHeader("Access-Control-Max-Age", "3600");
//		response.setContentType("text/html;charset=utf-8");
//		System.out.println("success");
//		StringBuffer str = new StringBuffer();
//		JSONObject json=new JSONObject(); 
//		
//		try {
//			FileItemFactory fif = new DiskFileItemFactory();
//			ServletFileUpload upload = new ServletFileUpload(fif);
//			List<FileItem> list = upload.parseRequest(request);
//			File fileDir = null ;
//			Map<String, Object> map = new HashMap<String, Object>();
//			if(null != list){
//				for (FileItem fileItem : list) {
//					String fKey = "";
//					Object fValue = null;
//					if(fileItem.isFormField()){
//						fKey = fileItem.getFieldName();
//						fValue = fileItem.getString("utf-8");
//						map.put(fKey, fValue);
//					}else{
//						fKey=fileItem.getFieldName();
//						fValue=fileItem.getInputStream();
//						String name = fileItem.getName();
//						if(null != name && !"".equals(name)){
//							name = name.substring(name.lastIndexOf(File.separator)+1);
//							String dirPath = PropertyAttributionHolder.getAttribute("dir_path");
//							fileDir = new File(dirPath);
//							if(!fileDir.exists())fileDir.mkdir();
//							String filePath = dirPath+File.separator+name;
//							InputStream in = (InputStream) fValue;
//							FileOutputStream fos = new FileOutputStream(filePath);
//							byte[] buffer = new byte[1024];
//							while(in.read(buffer)!=-1){
//								fos.write(buffer, 0, buffer.length);
//						}
//							fos.flush();  
//		                    fos.close();  
//		                    in.close();
//
//						}
//					}
//					
//					
//				}
//			}
//			json.put("success",true);
//			
//		} catch (Exception ex){
//			ex.printStackTrace();
//			json.put("success", false);
//			json.put("errmsg", ex.getMessage());
//		}
//		System.out.println("要发送的的Json"+json.toString());	
//		return json.toString();
//	}
	@POST
	@Path("/getReportFile")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getReportFile(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws Exception {	
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setContentType("application/json;charset=utf-8");
		System.out.println("success");
		StringBuffer str = new StringBuffer();
		JSONObject json=new JSONObject(); 
		
		try {
			BufferedInputStream in = new BufferedInputStream(request.getInputStream());
			int i;
			char c;
			while ((i = in.read()) != -1) {
				c = (char) i;
				str.append(c);
			}
			String strJson = java.net.URLDecoder.decode(str.toString(),"utf-8");
//			strJson="{\""+strJson.replace("=", "\":\"")+"\"}";
			System.out.println("接受的json"+strJson);
			if(strJson!=null && !"".equals(strJson)){
				strJson = strJson.substring(strJson.indexOf("=")+1);
				JSONObject dataJson = JSONObject.fromObject(strJson);
//				String sampleCode = dataJson.getString("sampleCode");
				String url = dataJson.getString("url");
				String fileName = dataJson.getString("fileName");
				String arr[] = fileName.split("-");
				String orderNo = arr[0].split("_")[0];
				String productId = arr[0].split("_")[1];
//				UserService userService =  SpringContextHolder.getBean("userService");	
//				String id = "";
//				User user = userService.get(User.class,id);
				
				
				URL fileUrl = new URL(url);
				HttpURLConnection connection = (HttpURLConnection) fileUrl.openConnection();
				connection.setConnectTimeout(3*1000);  
			    //防止屏蔽程序抓取而返回403错误  
				connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");  
				InputStream inputStream = connection.getInputStream();
				byte[] buffer = new byte[1024];    
		        int len = 0;    
		        ByteArrayOutputStream bos = new ByteArrayOutputStream();
		        while((len = inputStream.read(buffer)) != -1) {    
		            bos.write(buffer, 0, len);    
		        }    
		        bos.close(); 
		        String saveDir = PropertyAttributionHolder.getAttribute("report_dir_path");
//		        File dir = new File(saveDir);
		        File dir = new File(saveDir+File.separator+orderNo);
		        if(!dir.exists()){
		        	dir.mkdirs();
		        }
//		        File file = new File(saveDir+File.separator+fileName);      
		        File file = new File(saveDir+File.separator+orderNo+File.separator+fileName);      
		        FileOutputStream fos = new FileOutputStream(file);       
		        fos.write(bos.toByteArray());   
		        if(fos!=null){  
		            fos.close();    
		        }  
		        if(inputStream!=null){  
		            inputStream.close();  
		        }  
		  
		        
		        System.out.println("info:"+url+" download success");   
		        
		        
		      	ReportService reportService =  SpringContextHolder.getBean("reportService");	
		        FileInfo fileInfo = new FileInfo();
		        fileInfo.setFileName(fileName);
		        fileInfo.setFilePath(saveDir+File.separator+orderNo+File.separator+fileName);
//		        fileInfo.setUploadUser(user);
		        fileInfo.setUploadTime(new Date());
		        fileInfo.setState("1");
		        reportService.save(fileInfo);
		        
		        json.put("success",true);
			}else{
				json.put("success",false);
				
			}
			
			
			
		} catch (Exception ex){
			ex.printStackTrace();
			json.put("success", false);
			json.put("errmsg", ex.getMessage());
		}
		System.out.println("要发送的的Json"+json.toString());	
		return json.toString();
	}
}
