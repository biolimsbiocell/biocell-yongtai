package com.biolims;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.dao.UserGroupDao;
import com.biolims.remind.model.SysRemind;
import com.biolims.system.code.SystemConstants;

public class EventParent {
	/*
	 * 向QA发送提醒
	 */
	public void sendQA(String title, String content, String contentId, String appTypeTableId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		UserGroupDao userGroupDao = (UserGroupDao) ctx.getBean("userGroupDao");
		List<UserGroupUser> list = userGroupDao.selectUserGroupUserByGroupId(SystemConstants.USER_GROUP_ID_QC);
		if (list.size() > 0) {
			List<SysRemind> sysList = new ArrayList<SysRemind>();
			Date currentDate = new Date();
			for (UserGroupUser user : list) {
				SysRemind sr = new SysRemind();
				sr.setTitle(title);
				sr.setContent(content);
				sr.setType(SystemConstants.DIC_TYPE_REMIND_CHECK);//发起
				sr.setStartDate(currentDate);
				sr.setRemindUser("SYSTEM");
				sr.setHandleUser(user.getUser());
				sr.setContentId(contentId);
				sr.setState("0");
				ApplicationTypeTable att = new ApplicationTypeTable();
				att.setId(appTypeTableId);
				//sr.setApplicationTypeTable(att);
				sr.setTableId(appTypeTableId);
				sysList.add(sr);
			}
			commonService.saveOrUpdate(list);
			commonService.saveOrUpdate(sysList);
		}
	}
}
