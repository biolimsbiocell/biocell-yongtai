package com.biolims.sample.dic.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.sample.dic.model.DicPriLib;
import com.biolims.sample.dic.service.DicPriLibService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/dic")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DicPriLibAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2415";
	@Autowired
	private DicPriLibService dicPriLibService;
	private DicPriLib dicPriLib = new DicPriLib();

	@Action(value = "showDicPriLibList")
	public String showDicPriLibList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dic/dicPriLib.jsp");
	}

	@Action(value = "showDicPriLibListJson")
	public void showDicPriLibListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dicPriLibService.findDicPriLibList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DicPriLib> list = (List<DicPriLib>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("twfPosition", "");
		map.put("solPosition", "");
		map.put("dryPosition", "");
		map.put("geneName", "");
		map.put("chromosomeNumber", "");
		map.put("site", "");
		map.put("genePosPriName", "");
		map.put("chrPosPriName", "");
		map.put("ref5", "");
		map.put("ref3", "");
		map.put("state", "");
		map.put("orderId", "");
		map.put("item-name", "");
		map.put("item-id", "");
		map.put("orderCompany", "");
		map.put("orderDate", "");
		map.put("name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "dicPriLibSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDicPriLibList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dic/dicPriLibDialog.jsp");
	}

	@Action(value = "showDialogDicPriLibListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDicPriLibListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dicPriLibService.findDicPriLibList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DicPriLib> list = (List<DicPriLib>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("twfPosition", "");
		map.put("solPosition", "");
		map.put("dryPosition", "");
		map.put("geneName", "");
		map.put("chromosomeNumber", "");
		map.put("site", "");
		map.put("genePosPriName", "");
		map.put("chrPosPriName", "");
		map.put("ref5", "");
		map.put("ref3", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editDicPriLib")
	public String editDicPriLib() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			dicPriLib = dicPriLibService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/sample/dic/dicPriLibEdit.jsp");
	}

	@Action(value = "copyDicPriLib")
	public String copyDicPriLib() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dicPriLib = dicPriLibService.get(id);
		dicPriLib.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/dic/dicPriLibEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = dicPriLib.getId();
		if (id != null && id.equals("")) {
			dicPriLib.setId(null);
		}
		Map aMap = new HashMap();
		dicPriLibService.save(dicPriLib, aMap);
		return redirect("/sample/dic/editDicPriLib.action?id="
				+ dicPriLib.getId());

	}

	/**
	 * 保存详细信息列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveItem")
	public void saveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			dicPriLibService.saveItem(itemDataJson);
			map.put("success", true);

		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除样本接收详细明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delItem")
	public void delItem() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] id = getRequest().getParameterValues("ids[]");
			// String[] dnaid = getRequest().getParameterValues("dnaid[]");
			dicPriLibService.delItem(id);

			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewDicPriLib")
	public String toViewDicPriLib() throws Exception {
		String id = getParameterFromRequest("id");
		dicPriLib = dicPriLibService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/dic/dicPriLibEdit.jsp");
	}

	@Action(value = "showDicMainTypeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDicMainTypeListJson() throws Exception {
		String item = super.getRequest().getParameter("item");
		String outStr = "{results:"
				+ dicPriLibService.getDicSampleTypeJson(item) + "}";
		return renderText(outStr);
	}

	@Action(value = "showDicMainTypeList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDicMainTypeList() throws Exception {
		String item = super.getRequest().getParameter("item");
		String outStr = "{results:" + dicPriLibService.getDicSampleType(item)
				+ "}";
		return renderText(outStr);
	}
	
	@Action(value = "dicPriLibTypeCobJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String  dicPriLibTypeCobJson() throws Exception {

		String outStr = "{'results':" + dicPriLibService.getDicPriLibTypeJson() + "}";
		return renderText(outStr);
		
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DicPriLibService getDicPriLibService() {
		return dicPriLibService;
	}

	public void setDicPriLibService(DicPriLibService dicPriLibService) {
		this.dicPriLibService = dicPriLibService;
	}

	public DicPriLib getDicPriLib() {
		return dicPriLib;
	}

	public void setDicPriLib(DicPriLib dicPriLib) {
		this.dicPriLib = dicPriLib;
	}

}
