package com.biolims.sample.dic.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dic.dao.DicPriLibDao;
import com.biolims.sample.dic.model.DicPriLib;
import com.biolims.sample.model.SampleInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DicPriLibService {
	@Resource
	private DicPriLibDao dicPriLibDao;
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> findDicPriLibList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dicPriLibDao.selectDicPriLibList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DicPriLib i) throws Exception {

		dicPriLibDao.saveOrUpdate(i);

	}

	public DicPriLib get(String id) {
		DicPriLib dicPriLib = commonDAO.get(DicPriLib.class, id);
		return dicPriLib;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DicPriLib sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dicPriLibDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	public String getDicSampleTypeJson(String type) throws Exception {
		List<DicType> list = dicPriLibDao.findDicSamplType(type);
		return JsonUtils.toJsonString(list);
	}

	public String getDicSampleType(String type) throws Exception {
		List<DicPriLib> list = dicPriLibDao.findDicSamplTypelist(type);
		return JsonUtils.toJsonString(list);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(String itemDataJson) throws Exception {
		List<DicPriLib> saveItems = new ArrayList<DicPriLib>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);

		for (Map map : list) {
			DicPriLib sbi = new DicPriLib();
			sbi = (DicPriLib) dicPriLibDao.Map2Bean(map, sbi);
			sbi.setId(null);
			sbi.setState("1");
			saveItems.add(sbi);

		}
		dicPriLibDao.saveOrUpdateAll(saveItems);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delItem(String[] ids) throws Exception {
		for (String id : ids) {
			DicPriLib sbrimte = dicPriLibDao.get(DicPriLib.class, id);
			dicPriLibDao.delete(sbrimte);
		}
	}
	
	public String getDicPriLibTypeJson() throws Exception {
		 List<DicPriLib> list= dicPriLibDao.find("from DicPriLib");
		return JsonUtils.toJsonString(list);
	}

}
