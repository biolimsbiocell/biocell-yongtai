package com.biolims.sample.dic.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.dic.model.DicPriLib;

@Repository
@SuppressWarnings("unchecked")
public class DicPriLibDao extends BaseHibernateDao {
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> selectDicPriLibList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from DicPriLib where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DicPriLib> list = new ArrayList<DicPriLib>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public List<DicType> findDicSamplType(String type) {
		List<DicType> list = commonDAO.find("from DicType where sysCode='" + type + "'  order by id asc");
		return list;

	}

	public List<DicPriLib> findDicSamplTypelist(String type) {
		List<DicPriLib> list = commonDAO.find("from DicPriLib where item.id='" + type + "' order by id asc");
		return list;

	}
}