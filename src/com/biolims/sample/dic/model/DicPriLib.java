package com.biolims.sample.dic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.product.model.Product;

/**
 * @Title: Model
 * @Description: 引物字典
 * @author lims-platform
 * @date 2014-10-04 17:02:35
 * @version V1.0
 * 
 */
@Entity
@Table(name = "T_DIC_PRIMER_LIBRARY")
@SuppressWarnings("serial")
public class DicPriLib extends EntityDao<DicPriLib> implements
		java.io.Serializable {

	private java.lang.String id;
	/** 工作液位置（5μm） */
	private java.lang.String twfPosition;
	/** 储存液位置（100μm-r） */
	private java.lang.String solPosition;
	/** 干粉位置 */
	private java.lang.String dryPosition;
	/** 基因名 */
	private java.lang.String geneName;
	/** 染色体编号 */
	private java.lang.String chromosomeNumber;
	/** 位点 */
	private java.lang.String site;
	/** gene-pos引物名称 */
	private java.lang.String genePosPriName;
	/** chr-pos 引物名称 */
	private java.lang.String chrPosPriName;
	/** ref-5' */
	private java.lang.String ref5;
	/** ref-3' */
	private java.lang.String ref3;
	/** 引物订单编号 */
	private java.lang.String orderId;

	/** 引物订单日期 */
	private java.lang.String orderCompany;

	/** 引物订单日期 */
	private java.lang.String orderDate;

	/** 引物名称 */
	private java.lang.String name;

	/** 状态 */
	private java.lang.String state;

	private Product item;

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 引物订单编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_BLOOD_OUT_ID")
	private Product crmProduct;// 检测项目

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 引物订单编号
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 工作液位置（5μm）
	 */
	@Column(name = "TWF_POSITION", length = 200)
	public java.lang.String getTwfPosition() {
		return this.twfPosition;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 工作液位置（5μm）
	 */
	public void setTwfPosition(java.lang.String twfPosition) {
		this.twfPosition = twfPosition;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 储存液位置（100μm-r）
	 */
	@Column(name = "SOL_POSITION", length = 200)
	public java.lang.String getSolPosition() {
		return this.solPosition;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 储存液位置（100μm-r）
	 */
	public void setSolPosition(java.lang.String solPosition) {
		this.solPosition = solPosition;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 干粉位置
	 */
	@Column(name = "DRY_POSITION", length = 200)
	public java.lang.String getDryPosition() {
		return this.dryPosition;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 干粉位置
	 */
	public void setDryPosition(java.lang.String dryPosition) {
		this.dryPosition = dryPosition;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 基因名
	 */
	@Column(name = "GENE_NAME", length = 200)
	public java.lang.String getGeneName() {
		return this.geneName;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 基因名
	 */
	public void setGeneName(java.lang.String geneName) {
		this.geneName = geneName;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 染色体编号
	 */
	@Column(name = "CHROMOSOME_NUMBER", length = 200)
	public java.lang.String getChromosomeNumber() {
		return this.chromosomeNumber;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 染色体编号
	 */
	public void setChromosomeNumber(java.lang.String chromosomeNumber) {
		this.chromosomeNumber = chromosomeNumber;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 位点
	 */
	@Column(name = "SITE", length = 200)
	public java.lang.String getSite() {
		return this.site;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 位点
	 */
	public void setSite(java.lang.String site) {
		this.site = site;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String gene-pos引物名称
	 */
	@Column(name = "GENE_POS_PRI_NAME", length = 200)
	public java.lang.String getGenePosPriName() {
		return this.genePosPriName;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String gene-pos引物名称
	 */
	public void setGenePosPriName(java.lang.String genePosPriName) {
		this.genePosPriName = genePosPriName;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String chr-pos 引物名称
	 */
	@Column(name = "CHR_POS_PRI_NAME", length = 200)
	public java.lang.String getChrPosPriName() {
		return this.chrPosPriName;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String chr-pos 引物名称
	 */
	public void setChrPosPriName(java.lang.String chrPosPriName) {
		this.chrPosPriName = chrPosPriName;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String ref-5'
	 */
	@Column(name = "REF_5", length = 200)
	public java.lang.String getRef5() {
		return this.ref5;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String ref-5'
	 */
	public void setRef5(java.lang.String ref5) {
		this.ref5 = ref5;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String ref-3'
	 */
	@Column(name = "REF_3", length = 200)
	public java.lang.String getRef3() {
		return this.ref3;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String ref-3'
	 */
	public void setRef3(java.lang.String ref3) {
		this.ref3 = ref3;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 状态
	 */
	@Column(name = "STATE", length = 200)
	public java.lang.String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 状态
	 */
	public void setState(java.lang.String state) {
		this.state = state;
	}

	public java.lang.String getOrderId() {
		return orderId;
	}

	public void setOrderId(java.lang.String orderId) {
		this.orderId = orderId;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getOrderCompany() {
		return orderCompany;
	}

	public void setOrderCompany(java.lang.String orderCompany) {
		this.orderCompany = orderCompany;
	}

	public java.lang.String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(java.lang.String orderDate) {
		this.orderDate = orderDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ITEM")
	public Product getItem() {
		return item;
	}

	public void setItem(Product item) {
		this.item = item;
	}
}