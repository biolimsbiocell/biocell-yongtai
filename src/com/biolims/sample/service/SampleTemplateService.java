package com.biolims.sample.service;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleTemplateDao;
import com.biolims.sample.model.SampleInputTemp;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleTemplateService {
	@Resource
	private SampleTemplateDao sampleTemplateDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSampleTemplateList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleTemplateDao.selectSampleTemplateList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInputTemp i) throws Exception {

		sampleTemplateDao.saveOrUpdate(i);

	}
	public SampleInputTemp get(String id) {
		SampleInputTemp sampleTemplate = commonDAO.get(SampleInputTemp.class, id);
		return sampleTemplate;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInputTemp sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleTemplateDao.saveOrUpdate(sc);
		
			String jsonStr = "";
		}
   }
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		SampleInputTemp sr = sampleTemplateDao.get(SampleInputTemp.class, id);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		//sct.setConfirmUser(user);
		//sct.setConfirmDate(new Date());

		sampleTemplateDao.update(sr);
	}
}
