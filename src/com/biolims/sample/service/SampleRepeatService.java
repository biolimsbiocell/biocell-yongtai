package com.biolims.sample.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.SampleFeedbackDao;
import com.biolims.project.feedback.model.FeedbackSample;
import com.biolims.sample.dao.SampleRepeatDao;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleBackByCompany;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleRepeatService {
	@Resource
	private SampleFeedbackDao sampleFeedbackDao;
	@Resource
	private SampleRepeatDao sampleRepeatDao;
	@Resource
	private CommonDAO commonDAO;

	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	private SampleInfo sampleInfo = new SampleInfo();
	private SampleAbnormal sampleAbnormal = new SampleAbnormal();
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findRepeatPlasmaList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleRepeatDao.selectRepeatPlasmaList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public SampleInfo findSampleInfo(String id) {
		SampleInfo findSampleInfoById = sampleFeedbackDao
				.findSampleInfoById(id);
		return findSampleInfoById;
	}

	public SampleInputTemp findSampleInputTemp(String id) {
		SampleInputTemp findSampleInputTempById = sampleFeedbackDao
				.findSampleInputTempById(id).get(0);
		return findSampleInputTempById;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackSample i) throws Exception {

		sampleFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackSample get(String id) {
		FeedbackSample sampleFeedback = commonDAO.get(FeedbackSample.class, id);
		return sampleFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInputTemp sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleInfo = sampleFeedbackDao.findSampleInfoById(sc.getCode());
			sc.setSampleInfo(sampleInfo);
			// 保存完以后new出一个smpleAbnormal对象
			sampleAbnormal.setSampleCode(sc.getCode());
			sampleAbnormal.setPatientName(sc.getPatientName());
			// sampleAbnormal.setSampleType(sc.getSampleType());
			sampleFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	// 保存血浆异常样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePlasmaRepeat(String itemDataJson) throws Exception {
		List<PlasmaAbnormal> saveItems = new ArrayList<PlasmaAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PlasmaAbnormal sbi = new PlasmaAbnormal();
			sbi = (PlasmaAbnormal) sampleRepeatDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
			if (sbi != null ) {
				if (sbi.getMethod().equals("") && !sbi.getNextFlow().equals("")) {
					if (sbi.getNextFlow().equals("1")) {
						// 重抽血,去公司反馈
						SampleBackByCompany sc = new SampleBackByCompany();
						// 根据样本号获取sampleInfo
						SampleInfo si = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						// 把sampleInfo的信息传到公司反馈中
						sc.setSampleInfo(si);
						sc.setSampleCode(si.getCode());
						// sc.setAcceptDate(si.getReceiveDate());
						sc.setIdCard(si.getIdCard());
						sc.setInspectDate(si.getInspectDate());

						if (si.getOrderId() != null) {
							sc.setOrderId(si.getOrderId().getId());
						} else {
							sc.setOrderId("");
						}
						sc.setPatientName(si.getPatientName());
						sc.setPhone(si.getPhone());
						sc.setProductId(si.getProductId());
						sc.setProductName(si.getProductName());
						sc.setReportDate(si.getReportDate());
						sc.setReturnDate(new Date());
						sc.setSequenceFun(si.getSequenceFun());
						sc.setMethod("0");
						sc.setState("1");
						sc.setClassify(si.getClassify());
						commonDAO.saveOrUpdate(sc);

						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
						}
					} else {
						// 终止
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					}
				} else if (!sbi.getMethod().equals("")) {
					if (sbi.getMethod().equals("3")) {
						// 重抽血,去公司反馈
						// SampleOutTemp sot=new SampleOutTemp();
						// sot.setCode(sbi.getCode());
						// sot.setSampleCode(sbi.getSampleCode());
						// sot.setMethod("重抽血");
						// sot.setState("1");
						// commonDAO.saveOrUpdate(sot);
						SampleBackByCompany sc = new SampleBackByCompany();
						// 根据样本号获取sampleInfo
						SampleInfo si = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						// 把sampleInfo的信息传到公司反馈中
						sc.setSampleInfo(si);
						sc.setSampleCode(si.getCode());
						// sc.setAcceptDate(si.getReceiveDate());
						sc.setIdCard(si.getIdCard());
						sc.setInspectDate(si.getInspectDate());
						sc.setClassify(si.getClassify());
						if (si.getOrderId() != null) {
							sc.setOrderId(si.getOrderId().getId());
						} else {
							sc.setOrderId("");
						}
						sc.setPatientName(si.getPatientName());
						sc.setPhone(si.getPhone());
						sc.setProductId(si.getProductId());
						sc.setProductName(si.getProductName());
						sc.setReportDate(si.getReportDate());
						sc.setReturnDate(new Date());
						sc.setSequenceFun(si.getSequenceFun());
						sc.setMethod("0");// 重抽血
						sc.setState("1");

						commonDAO.saveOrUpdate(sc);
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
						}
					} else {
						// 终止
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					}
				}
			}
			this.sampleRepeatDao.saveOrUpdate(sbi);
		}
	}

	// //根据条件检索数据
	// public List<Map<String, String>> selectBloodRepeat(String
	// sampleCode,String bloodCode) throws Exception {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result =
	// sampleRepeatDao.selectBloodRepeat(sampleCode,bloodCode);
	// List<SamplePlasmaInfo> list = (List<SamplePlasmaInfo>)
	// result.get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (SamplePlasmaInfo srai : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", srai.getId());
	// map.put("bloodCode", srai.getCode());
	// map.put("sampleCode", srai.getSampleCode());
	// map.put("isExecute", srai.getIsExecute());
	// map.put("nextFlow", srai.getNextFlow());
	// map.put("note", srai.getNote());
	// map.put("method", srai.getMethod());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }
}
