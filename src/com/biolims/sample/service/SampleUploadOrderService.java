package com.biolims.sample.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleUploadOrderDao;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleUploadOrder;
import com.biolims.system.product.model.Product;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.weichat.GetImage;
import com.biolims.weichat.service.CancerTypeService;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleUploadOrderService {
	@Resource
	private SampleUploadOrderDao sampleUploadOrderDao;
	@Resource
	private CommonService commonService;
	@Resource
	private CancerTypeService cancerTypeService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleOrderList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleUploadOrderDao.selectSampleOrderList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

		
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrder i) throws Exception {

		sampleUploadOrderDao.saveOrUpdate(i);

	}

	public SampleOrder get(String id) {
		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, id);
		return sampleOrder;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(SampleOrder sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleUploadOrderDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}

	// 添加子表的service
	public Map<String, Object> findSampleOrderPersonnelList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleUploadOrderDao
				.selectSampleOrderPersonnelList(scId, startNum, limitNum, dir,
						sort);
		List<SampleOrderPersonnel> list = (List<SampleOrderPersonnel>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSampleOrderItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleUploadOrderDao.selectSampleOrderItemList(
				scId, startNum, limitNum, dir, sort);
		List<SampleOrderItem> list = (List<SampleOrderItem>) result.get("list");
		return result;
	}

	// 查询订单信息样本
	public Map<String, Object> findSampleOrderInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleUploadOrderDao.selectSampleOrderInfoList(
				scId, startNum, limitNum, dir, sort);
		return result;
	}

	// 保存一条空记录，收费确认
	public void savemaket(CrmConsumerMarket ck) throws Exception {
		this.sampleUploadOrderDao.save(ck);
	}

	// 查询子表记录
	public List<SampleOrderItem> querySampleItem(String id) {
		return this.sampleUploadOrderDao.querySampleItem(id);
	}

	public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
		return this.sampleUploadOrderDao.querySamplePersonnel(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderPersonnel(SampleOrder sc, String itemDataJson)
			throws Exception {
		List<SampleOrderPersonnel> saveItems = new ArrayList<SampleOrderPersonnel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderPersonnel scp = new SampleOrderPersonnel();
			// 将map信息读入实体类
			scp = (SampleOrderPersonnel) sampleUploadOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null) {
				if (scp.getId().equals(""))
					scp.setId(null);
			}
			scp.setSampleOrder(sc);
			saveItems.add(scp);
		}
		sampleUploadOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOrderPersonnel(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOrderPersonnel scp = sampleUploadOrderDao.get(
					SampleOrderPersonnel.class, id);
			sampleUploadOrderDao.delete(scp);
		}
	}

	/**
	 * 解析json，保存订单
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderByJson(String jsons) throws Exception {
		// 解析json
		JSONArray array = JSONArray.fromObject("[" + jsons + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		SampleOrder order = new SampleOrder();
		// 根据微信号,查询user,并且把user存到订单的世和专员

		// commonService.saveOrUpdate(u);
		List<User> l = commonService.get(User.class, "weichatId",
				(String) object.get("userId"));
		order.setId(object.getString("medicalNumber"));
		// order.setMedicalNumber(object.getString("medicalNumber"));
		order.setName(object.getString("testName"));
		// order.setSampleInfoMain(sampleInfoMain)
		order.setSampleTypeId(object.getString("sample"));

		order.setCancerType(cancerTypeService
				.getCancerTypeByName((String) object.get("cancerType")));
		order.setCancerTypeSeedOne(cancerTypeService
				.getCancerTypeSeedOneByName((String) object
						.getString("cancerTypeSeedOne")));
		order.setCancerTypeSeedTwo(cancerTypeService
				.getCancerTypeSeedTwoByName((String) object
						.getString("cancerTypeSeedTwo")));
		if (l.size() > 0) {
			order.setCommissioner(l.get(0));
		}

		// 得到图片下载路径
		GetImage getImage = new GetImage();
		Map<String, String> map = getImage.getImage(object
				.getString("downloadUrl"));
		FileInfo fi = new FileInfo();
		fi.setFilePath(map.get("path"));
		fi.setFileName(map.get("fileName"));
		fi.setFileType("jpg");
		fi.setState("1");
		/*
		 * User user = (User)
		 * this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		 * fi.setUploadUser(user);
		 */
		fi.setUseType("1");
		this.sampleUploadOrderDao.saveOrUpdate(fi);
		// 把该图片关联到订单
		order.setState("3");
		order.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);

		this.sampleUploadOrderDao.saveOrUpdate(order);

		// 保存数据
	}

	/**
	 * 查询数据，返回字符串
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String outputJsonByString(String userId) throws Exception {
		// 查询数据
		// 根据微信号,查询user,并且把user存到订单的世和专员
		String sampleJson = this.sampleUploadOrderDao.selectSampleOrderByList(userId);
		return sampleJson;
	}

	public String selectSampleOrderDetail(String id) {
		return sampleUploadOrderDao.selectSampleOrderDetail(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderItem(SampleOrder sc, String itemDataJson)
			throws Exception {
		List<SampleOrderItem> saveItems = new ArrayList<SampleOrderItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderItem scp = new SampleOrderItem();
			// 将map信息读入实体类
			scp = (SampleOrderItem) sampleUploadOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleOrder(sc);

			saveItems.add(scp);
		}
		sampleUploadOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOrderItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOrderItem scp = sampleUploadOrderDao.get(SampleOrderItem.class, id);
			sampleUploadOrderDao.delete(scp);
		}
	}

	// 根据模板ID加载子表明细
	public List<Map<String, String>> setTemplateItem(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.sampleUploadOrderDao.setTemplateItem(code);
		List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SampleCancerTempPersonnel ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("checkOutTheAge", ti.getCheckOutTheAge());
				if (ti.getSampleCancerTemp().getOrderNumber() != null)
					map.put("sampleorder", ti.getSampleCancerTemp()
							.getOrderNumber());
				else
					map.put("sampleorder", "");
				if (ti.getTumorCategory() != null)
					map.put("tumorCategory", ti.getTumorCategory().getId());
				else
					map.put("tumorCategory", "");
				map.put("familyRelation", ti.getFamilyRelation());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据模板ID加载子表明细2
	public List<Map<String, String>> setSampleOrderItem2(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.sampleUploadOrderDao
				.setSampleOrderItem(code);
		List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SampleCancerTempItem ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("drugDate", ti.getDrugDate().toString());
				map.put("useDrugName", ti.getUseDrugName());
				map.put("effectOfProgress", ti.getEffectOfProgress());
				map.put("effectOfProgressSpeed", ti.getEffectOfProgressSpeed());
				map.put("geneticTestHistory", ti.getGeneticTestHistory());
				map.put("sampleDetectionName", ti.getSampleDetectionName());
				map.put("sampleExonRegion", ti.getSampleExonRegion());
				map.put("sampleDetectionResult", ti.getSampleDetectionResult());

				map.put("sampleOrder", ti.getSampleCancerTemp()
						.getOrderNumber());

				mapList.add(map);
			}

		}
		return mapList;
	}

	// 查询订单编号是否存在 并查询 是否使用
	public int selSampleOrderId(String id) {
		int flg = 0;// 不存在
		Long total = this.sampleUploadOrderDao.selSampleOrderId(id);
		if (total > 0) {

			Long total2 = this.sampleUploadOrderDao.selSampleOrderId2(id);
			if (total2 > 0) {
				flg = 1;// 存在并被使用
			} else {
				flg = 2;// 存在未被使用
			}
		}

		return flg;
	}
	public Map<String, Object> selSampleOrder(String itemDataJson) throws Exception{
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Map<String, Object> maps = new HashMap<String, Object>();
		String orderids = "";
		String message = "保存成功！";
		String flag = "0";
		for(Map<String, Object> map : list){
			SampleUploadOrder so = new SampleUploadOrder();
			so = (SampleUploadOrder) sampleUploadOrderDao.Map2Bean(map, so);
			if(!"".equals(so.getId()) && so.getId()!=null){
				List<SampleUploadOrder> upsample = sampleUploadOrderDao.selSampleOrder();
				if(upsample.size() != 0){
					for(SampleUploadOrder order : upsample){
						if(so.getId().equals(order.getId())){
							flag = "1";
							orderids += so.getId()+ ",";
							message = "以下订单编号已存在！请修改后再上传！";
							break;
						}
					}
				
				}
			}
		}
		//当表数据中的非空订单编号都不相等时，为订单编号为空的订单赋值
		if("0".equals(flag)){
			for(Map<String, Object> map : list){
				SampleUploadOrder so = new SampleUploadOrder();
				so = (SampleUploadOrder) sampleUploadOrderDao.Map2Bean(map, so);
				if(so.getId() ==null || "".equals(so.getId())){
					// 生成订单号
					 String id = this.sampleUploadOrderDao.selmaxOrderIdByorderType(so
					 .getOrderType());
					 if (id == null || "".equals(id)) {
						 String modelName = "SampleOrder";
							String markCode = "DD";
							Date date = new Date();
							DateFormat format = new SimpleDateFormat("yy");
							String stime = format.format(date);
							String autoID = codingRuleService.getCodeByPrefix(modelName,
									markCode, stime, 000000, 6, null);
							so.setId(autoID);
						continue;
					 }
				}
			}
		}
		maps.put("sampleOrderid", orderids);
		maps.put("message", message);
		maps.put("flag", flag);
		return maps;
	}
	// 保存订单
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrderUpload(String itemDataJson) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleUploadOrder so = new SampleUploadOrder();
			so = (SampleUploadOrder) sampleUploadOrderDao.Map2Bean(map, so);
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			so.setCreateUser(user);
			so.setCreateDate(new Date());

			
			// 查询检测项目是否在
			Product p = this.sampleUploadOrderDao.selProductBynName(so
					.getProductName());
			if (p == null) {
				so.setProductName(null);
			} else {
				so.setProductId(p.getId());
			}

			// 查询销售代表是否存在
			User u = this.sampleUploadOrderDao.selUserByName(so.getInfoUserStr());
			if (u == null) {
				so.setCommissioner(null);
			} else {
				so.setCommissioner(u);
			}

			// 查询医院是否存在
			CrmCustomer c = this.sampleUploadOrderDao.selCrmCustomerByName(so
					.getMedicalInstitutions());
			if (c == null) {
				so.setMedicalInstitutions(null);
			}
			sampleUploadOrderDao.saveOrUpdate(so);
		}

	}
	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleInItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleUploadOrder scp = commonDAO.get(SampleUploadOrder.class, id);
			if (scp != null) {
				commonDAO.delete(scp);
			}
		}
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrder sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleUploadOrderDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleOrderPersonnel");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleOrderPersonnel(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sampleOrderItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleOrderItem(sc, jsonStr);
			}
		}
	}

	public SampleCancerTemp fuzZhi(SampleOrder so) throws Exception {
		SampleCancerTemp a = new SampleCancerTemp();
		a.setOrderNumber(so.getId());
		a.setId(so.getId());
		a.setName(so.getName());
		a.setGender(so.getGender());
		a.setBirthDate(so.getBirthDate());
		a.setDiagnosisDate(DateUtil.parse(so.getDiagnosisDate()));
		a.setDicType(so.getDicType());
		a.setSampleStage(so.getSampleStage());
		a.setInspectionDepartment(so.getInspectionDepartment());
		a.setCrmProduct(so.getCrmProduct());
		a.setProductId(so.getProductId());
		a.setProductName(so.getProductName());
		a.setSamplingDate(so.getSamplingDate());
		a.setSamplingLocation(so.getSamplingLocation());
		a.setSamplingNumber(so.getSamplingNumber());
		a.setPathologyConfirmed(so.getPathologyConfirmed());
		// a.setBloodSampleDate(so.getBloodSampleDate());
		// a.setPlasmapheresisDate(so.getPlasmapheresisDate());
		a.setCommissioner(so.getCommissioner());
		a.setReceivedDate(so.getReceivedDate());
		a.setSampleTypeId(so.getSampleTypeId());
		a.setSampleTypeName(so.getSampleTypeName());
		a.setMedicalNumber(so.getMedicalNumber());
		a.setSampleCode(so.getSampleCode());
		a.setFamily(so.getFamily());
		a.setFamilyPhone(so.getFamilyPhone());
		a.setFamilySite(so.getFamilySite());
		a.setCrmCustomer(so.getCrmCustomer());
		a.setMedicalInstitutions(so.getMedicalInstitutions());
		a.setMedicalInstitutionsPhone(so.getMedicalInstitutionsPhone());
		a.setMedicalInstitutionsSite(so.getMedicalInstitutionsSite());
		a.setCrmDoctor(so.getCrmDoctor());
		a.setAttendingDoctor(so.getAttendingDoctor());
		a.setAttendingDoctorPhone(so.getAttendingDoctorPhone());
		a.setAttendingDoctorSite(so.getAttendingDoctorSite());
		a.setNote(so.getNote());
		a.setCreateUser(so.getCreateUser());
		a.setCreateDate(so.getCreateDate());
		a.setConfirmUser(so.getConfirmUser());
		a.setConfirmDate(so.getConfirmDate());
		a.setState(so.getState());
		a.setStateName(so.getStateName());
		

		return a;
	}

	// 微信端读取报告
	public List<FileInfo> findSampleReport(String id) {
		return sampleUploadOrderDao.findSampleReport(id);
	}
	
	/**
	 * 用检测项目名字查找id
	 * @param itemDataJson
	 * @return
	 * @throws Exception
	 */
	public String GetProductId(String pruductname) throws Exception {
		String product = "";
		if("".equals(pruductname)){
			product="";
		}else{
			product=sampleUploadOrderDao.GetProductId(pruductname);
		}
		
		return product;
	}
	
	/**
	 * 通过字典名称及类型得到id
	 * 
	 * @param pruductname
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public String GetDicTypeId(String pruductname,String type) throws Exception {
		String dicid = "";
		if("".equals(pruductname)){
			dicid="";
		}else{
			dicid=sampleUploadOrderDao.GetDicTypeId(pruductname,type);
		}
		
		return dicid;
	}
	
	/**
	 * 通过销售代表名字得到id
	 * 
	 * @param pruductname
	 * @return
	 * @throws Exception
	 */
	public String GetUserId(String pruductname) throws Exception {
		String id = "";
		if("".equals(pruductname)){
			id="";
		}else{
			id=sampleUploadOrderDao.GetUserId(pruductname);
		}
		
		return id;
	}
	
	/**
	 * 通过样本类型名字得到id
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String GetsampleTypeId(String name) throws Exception {
		String id = "";
		if("".equals(name)){
			id="";
		}else{
			id=sampleUploadOrderDao.GetsampleTypeId(name);
		}
		
		return id;
	}
}
