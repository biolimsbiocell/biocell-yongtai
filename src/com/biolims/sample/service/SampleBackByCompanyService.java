package com.biolims.sample.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.dao.SampleBackByCompanyDao;
import com.biolims.sample.model.SampleBackByCompany;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleBackByCompanyService {
	@Resource
	private SampleBackByCompanyDao sampleBackByCompanyDao;
	@Resource
	private CommonDAO commonDAO;
	
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSampleBackByCompany(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleBackByCompanyDao.selectSampleBackByCompany(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleBackByCompany(String itemDataJson) throws Exception {
		List<SampleBackByCompany> saveItems = new ArrayList<SampleBackByCompany>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleBackByCompany sbi = new SampleBackByCompany();
			sbi = (SampleBackByCompany) sampleBackByCompanyDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
		}
		sampleBackByCompanyDao.saveOrUpdateAll(saveItems);
	}
}
