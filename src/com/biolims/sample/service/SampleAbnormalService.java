package com.biolims.sample.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.abnormal.model.AbnormalItem;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.agent.primary.dao.PrimaryTaskDao;
import com.biolims.crm.customer.patient.dao.CrmPatientDao;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleAbnormalDao;
import com.biolims.sample.dao.SampleInputDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class SampleAbnormalService {
	@Resource
	private SampleAbnormalDao sampleAbnormalDao;
	@Resource
	private SampleInputDao sampleInputDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private PrimaryTaskDao primaryTaskDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private CommonService commonService;

	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	@Resource
	private CrmPatientDao crmPatientDao;
	StringBuffer json = new StringBuffer();

	public SampleAbnormal get(String id) {
		SampleAbnormal sampleAbnormal = commonDAO.get(SampleAbnormal.class, id);
		return sampleAbnormal;
	}

	public void saveSampleItem(String itemDataJson, String changeLog)
			throws Exception {
		List<SampleAbnormal> saveItems1 = new ArrayList<SampleAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		SampleAbnormal scp = new SampleAbnormal();
		for(Map<String, Object> map:list){
			scp = (SampleAbnormal) sampleAbnormalDao.Map2Bean(map, scp);
			SampleAbnormal sbi=commonDAO.get(SampleAbnormal.class, scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setMethod(scp.getMethod());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(scp.getSampleCode());
			li.setClassName("SampleAbnormal");
			li.setModifyContent(changeLog);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
		sampleAbnormalDao.saveOrUpdateAll(saveItems1);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void next(SampleReceiveItem sri,
			SampleAbnormal sbi) throws Exception {
		if (sbi.getNextFlowId().equals("0009")) {// 样本入库
			SampleInItemTemp st = new SampleInItemTemp();
			st.setCode(sri.getSampleCode());
			st.setSampleCode(sri.getSampleCode());
			st.setConcentration(sri.getConcentration());
			st.setVolume(sri.getVolume());
			if (sri.getSampleNum() != null) {
				st.setSumTotal(Double.parseDouble(sri.getSampleNum()));
			}
			// DicSampleType t = commonDAO.get(DicSampleType.class, sri
			// .getDicSampleType().getId());
			st.setSampleType(sri.getDicSampleType().getName());
			st.setState("1");
			st.setPatientName(sri.getPatientName());
			st.setSampleTypeId(sri.getDicSampleType().getId());
			st.setInfoFrom("SampleInfo");
			st.setGroupNo(sri.getName());// 分组号
			sampleReceiveDao.saveOrUpdate(st);
		} else if (sbi.getNextFlowId().equals("0017")) {// 核酸提取
			DnaTaskTemp d = new DnaTaskTemp();
			sri.setState("1");
			sampleInputService.copy(d, sri);
			if (sri.getSampleNum() != null) {
			}
			d.setSampleCode(sri.getSampleCode());
			d.setCode(sri.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sri
					.getDicSampleType().getId());
			d.setSampleType(t.getName());
			if (sri.getProductId() != null) {
				Product p = commonDAO.get(Product.class, sri.getProductId());
			}
			sampleReceiveDao.saveOrUpdate(d);
		} else if (sbi.getNextFlowId().equals("0018")) {// 文库构建
			WkTaskTemp d = new WkTaskTemp();
			sri.setState("1");
			sampleInputService.copy(d, sri);
			d.setSampleCode(sri.getSampleCode());
			d.setCode(sri.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sri
					.getDicSampleType().getId());
			if (sri.getProductId() != null) {
				Product p = commonDAO.get(Product.class, sri.getProductId());
			}
			d.setSampleType(t.getName());
			sampleReceiveDao.saveOrUpdate(d);
		} else {
			// 得到下一步流向的相关表单
			List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(sri
					.getNextFlowId());
			for (NextFlow n : list_nextFlow) {
				Object o = Class.forName(
						n.getApplicationTypeTable().getClassPath())
						.newInstance();
				sri.setState("1");
				DicSampleType t = commonDAO.get(DicSampleType.class, sri
						.getDicSampleType().getId());
				sri.setSampleType(t.getName());
				try {
					BeanUtils.getDeclaredField(o, "code");
					BeanUtils.setFieldValue(o, "code", sri.getSampleCode());

				} catch (NoSuchFieldException e) {
				}
				sampleInputService.copy(o, sri);
			}
		}

	}

	public Map<String, Object> selectAbnormalTypeAll() {
		return sampleAbnormalDao.selectAbnormalTypeAll();
	}

	public Map<String, Object> selectDicUnusualMethodAll() {
		return sampleAbnormalDao.selectDicUnusualMethodAll();
	}

	public Map<String, Object> showSampleAbnormalTableJson(Integer start,
			Integer length, String query, String col, String sort) {
		return sampleAbnormalDao.showSampleAbnormalTableJson(start, length, query, col, sort);
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson, String changeLog) throws Exception {

		List<SampleAbnormal> saveItems1 = new ArrayList<SampleAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				dataJson, List.class);
		SampleAbnormal scp = new SampleAbnormal();
		for(Map<String, Object> map:list){
			scp = (SampleAbnormal) sampleAbnormalDao.Map2Bean(map, scp);
			SampleAbnormal sbi=commonDAO.get(SampleAbnormal.class, scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setMethod(scp.getMethod());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(changeLog);
			li.setFileId(scp.getSampleCode());
			li.setClassName("SampleAbnormal");
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
		sampleAbnormalDao.saveOrUpdateAll(saveItems1);
		List<SampleAbnormal> saveItems = new ArrayList<SampleAbnormal>();
		for(String id:ids){
			SampleAbnormal sbi=commonDAO.get(SampleAbnormal.class, id);
			sbi.setIsExecute("1");
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {
					String itemId = sbi.getItemId();
					SampleReceiveItem sri = sampleAbnormalDao.get(
							SampleReceiveItem.class, itemId);
					if(sbi.getMethod().equals("1")){
						next(sri,sbi);
					}else{
					}
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					sampleStateService
							.saveSampleState(
									sri.getSampleCode(),
									sri.getSampleCode(),
									sri.getProductId(),
									sri.getProductName(),
									"",
									format.format(sri.getAcceptDate()),
									format.format(new Date()),
									"SampleAbnormal",
									"开箱检验异常",
									(User) ServletActionContext
											.getRequest()
											.getSession()
											.getAttribute(
													SystemConstants.USER_SESSION_KEY),
									sri.getId(), sbi.getNextFlow(),
									sri.getMethod(), null, null, null,
									null, null, null, null, null);
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		sampleAbnormalDao.saveOrUpdateAll(saveItems);
	
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void feedbackAbnormal(String[] ids) {
		if(ids!=null&&ids.length>0){
			for(String id:ids){
				SampleAbnormal sa=commonDAO.get(SampleAbnormal.class, id);
				sa.setState("2");
				AbnormalItem ai=new AbnormalItem();
				ai.setAcceptDate(sa.getAcceptDate());
				ai.setDicSampleType(sa.getDicSampleType());
				ai.setDicType(sa.getDicType());
				ai.setMethod(sa.getMethod());
				ai.setName(sa.getName());
				ai.setNextFlow(sa.getNextFlow());
				ai.setNextFlowId(sa.getNextFlowId());
				ai.setNote(sa.getNote());
				ai.setOrderId(sa.getOrderId());
				ai.setPatientName(sa.getPatientName());
				ai.setProductId(sa.getProductId());
				ai.setProductName(sa.getProductName());
				ai.setProject(sa.getProject());
				ai.setSampleCode(sa.getSampleCode());
				ai.setSampleNum(sa.getSampleNum());
				ai.setState("1");
				commonDAO.saveOrUpdate(sa);
				commonDAO.saveOrUpdate(ai);
			}
		}
		
	}
}
