package com.biolims.sample.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.SampleOrderChangeDao;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderChange;
import com.biolims.sample.model.SampleOrderChangeItem;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.util.JsonUtils;
import com.biolims.weichat.service.CancerTypeService;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleOrderChangeService {
	@Resource
	private SampleOrderDao sampleOrderDao;
	@Resource
	private SampleOrderChangeDao sampleOrderChangeDao;
	@Resource
	private CommonService commonService;
	@Resource
	private CancerTypeService cancerTypeService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CodingRuleService codingRuleService;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrderChangeItem i) throws Exception {

		sampleOrderChangeDao.saveOrUpdate(i);

	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String sampleOrderChangeId) throws Exception {

		SampleOrderChange sam = commonDAO.get(SampleOrderChange.class,
				sampleOrderChangeId);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sam.setAcceptDate(new Date());
		sam.setAcceptUser(user);
		sam.setState("1");
		sam.setStateName("完成");
		String change = sam.getId();
		if(change!=null) {
			List<SampleOrderChangeItem>list=sampleOrderChangeDao.findSampleOrderChangeItemByChange(change);
			if("修改申请单".equals(sam.getChangeType())) {
				SampleOrderChangeItem item = list.get(0);
				String oldId = item.getOldId();
				SampleOrder sampleOrder = commonDAO.get(SampleOrder.class,oldId);
				sampleOrder.setState("0");
				sampleOrder.setStateName("作废");
				SampleOrder so = commonDAO.get(SampleOrder.class, item.getUpdateId());
				so.setFlagValue("");
			}
			//修改原来订单的状态
			for (SampleOrderChangeItem item : list) {
				String itemId = item.getUpdateId();
				//为修改和追加
				if(itemId!=null) {
					SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, item.getOldId());
					sampleOrder.setState("1");
					sampleOrder.setStateName("Complete");
					SampleOrder so = commonDAO.get(SampleOrder.class, item.getUpdateId());
					so.setFlagValue("");
				}else {//取消
					String oldId = item.getOldId();
					SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, oldId);
					sampleOrder.setState("0");
					sampleOrder.setStateName("Cancel");
					List<SampleInfo> infoList = sampleInfoMainDao.findSampleInfoByOrderNum(oldId);
					if(infoList.size()>0) {
					for (SampleInfo sampleInfo : infoList) {
						sampleInfo.setChangeType("0");
						commonDAO.saveOrUpdate(sampleInfo);
					}
				}
				}
			}
		}
		

	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderItem(SampleOrderChangeItem sc, String itemDataJson,String changeLogItem)
			throws Exception {
		List<SampleOrderItem> saveItems = new ArrayList<SampleOrderItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderItem scp = new SampleOrderItem();
			// 将map信息读入实体类
			scp = (SampleOrderItem) sampleOrderChangeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
//			scp.setSampleOrderChange(sc);

			saveItems.add(scp);
		}
		sampleOrderChangeDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setModifyContent(changeLogItem);
			commonDAO.saveOrUpdate(li);
		}
	}


	public SampleOrderChangeItem get(String id) {
		SampleOrderChangeItem sampleOrderChange = commonDAO.get(SampleOrderChangeItem.class, id);
		return sampleOrderChange;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(SampleOrderChangeItem sc, Map jsonMap) throws Exception {
		if (sc != null) {
			
			sampleOrderDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}
	public Map<String, Object> findSampleOrderChangeTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return sampleOrderChangeDao.findSampleOrderChangeTable(start, length, query, col,
				sort);
	}

	// 保存一条空记录，收费确认
	public void savemaket(CrmConsumerMarket ck) throws Exception {
		this.sampleOrderDao.save(ck);
	}

	public Map<String, Object> findSampleOrderChangeItemList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return sampleOrderChangeDao.findSampleOrderChangeItemList(start, length, query, col,
				sort, id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrderChange so, String dataJson, String changeLog) throws Exception {
		/*ArrayList<SampleOrderChangeItem> saveItems = new ArrayList<SampleOrderChangeItem>();
		String mainJson = "[" + dataJson + "]";
		List<Map<String,Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
		SampleOrderChange sa =new SampleOrderChange();
		sa=(SampleOrderChange)commonDAO.Map2Bean(list.get(0), sa);
		so.setNote(sa.getNote());
		so.setName(sa.getName());
		so.setAcceptUser(sa.getAcceptUser());
		commonDAO.saveOrUpdate(so);*/
		commonDAO.saveOrUpdate(so);
	}
}
	/**
	 * 解析json，保存订单
	 * 
	 * @param ids
	 * @throws Exception
	 */
	/*@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderChangeByJson(String jsons) throws Exception {
		// 解析json
		JSONArray array = JSONArray.fromObject("[" + jsons + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		SampleOrderChangeItem order = new SampleOrderChangeItem();
		// 根据微信号,查询user,并且把user存到订单的世和专员

		// commonService.saveOrUpdate(u);
		List<User> l = commonService.get(User.class, "weichatId",
				(String) object.get("userId"));
		order.setId(object.getString("medicalNumber"));
		// order.setMedicalNumber(object.getString("medicalNumber"));
		order.setName(object.getString("testName"));
		// order.setSampleInfoMain(sampleInfoMain)
		order.setSampleTypeId(object.getString("sample"));

		order.setCancerType(cancerTypeService
				.getCancerTypeByName((String) object.get("cancerType")));
		order.setCancerTypeSeedOne(cancerTypeService
				.getCancerTypeSeedOneByName((String) object
						.getString("cancerTypeSeedOne")));
		order.setCancerTypeSeedTwo(cancerTypeService
				.getCancerTypeSeedTwoByName((String) object
						.getString("cancerTypeSeedTwo")));
		if (l.size() > 0) {
			order.setCommissioner(l.get(0));
		}

		// 得到图片下载路径
		GetImage getImage = new GetImage();
		Map<String, String> map = getImage.getImage(object
				.getString("downloadUrl"));
		FileInfo fi = new FileInfo();
		fi.setFilePath(map.get("path"));
		fi.setFileName(map.get("fileName"));
		fi.setFileType("jpg");
		fi.setState("1");
		fi.setUseType("1");
		this.sampleOrderDao.saveOrUpdate(fi);
		// 把该图片关联到订单
		order.setUpLoadAccessory(fi);
		order.setState("3");
		order.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);

		this.sampleOrderDao.saveOrUpdate(order);

		// 保存数据
	}
}*/