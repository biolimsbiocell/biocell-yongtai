package com.biolims.sample.service;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleBloodDiseaseTempDao;
import com.biolims.sample.dao.SampleChromosomeTempDao;
import com.biolims.sample.dao.SampleFolicAcidTempDao;
import com.biolims.sample.dao.SampleGeneTempDao;
import com.biolims.sample.dao.SampleInputDao;
import com.biolims.sample.dao.SampleInputTempDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.dao.SampleTumorTempDao;
import com.biolims.sample.dao.SampleVisitTempDao;
import com.biolims.sample.model.SampleBloodDiseaseTemp;
import com.biolims.sample.model.SampleChromosome;
import com.biolims.sample.model.SampleChromosomeTemp;
import com.biolims.sample.model.SampleFolicAcid;
import com.biolims.sample.model.SampleFolicAcidTemp;
import com.biolims.sample.model.SampleGene;
import com.biolims.sample.model.SampleGeneTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleTumor;
import com.biolims.sample.model.SampleTumorTemp;
import com.biolims.sample.model.SampleVisit;
import com.biolims.sample.model.SampleVisitTemp;
import com.biolims.system.product.dao.ProductDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleInputService {
	@Resource
	private SampleInputDao sampleInputDao;
	@Resource
	private ProductDao productDao;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private SampleInputTempDao sampleInputTempDao;
	@Resource
	private SampleTumorTempDao sampleTumorTempDao;
	@Resource
	private SampleBloodDiseaseTempDao sampleBloodDiseaseTempDao;
	@Resource
	private SampleChromosomeTempDao sampleChromosomeTempDao;
	@Resource
	private SampleVisitTempDao sampleVisitTempDao;
	@Resource
	private SampleFolicAcidTempDao sampleFolicAcidTempDao;
	@Resource
	private SampleGeneTempDao sampleGeneTempDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleInputList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String createUser1) {
		return sampleInputDao.selectSampleInputList(mapForQuery, startNum,
				limitNum, dir, sort, createUser1);
	}

	public Map<String, Object> findSampleInputList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, User user, String state) {

		return sampleInputDao.selectSampleInputList1(mapForQuery, startNum,
				limitNum, dir, sort, user, state);
	}

	public SampleInput get(String id) {
		SampleInput sampleInput = commonDAO.get(SampleInput.class, id);
		return sampleInput;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInputTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			if (img.equals("") || img == null || !img.equals(imgId)) {
				List<FileInfo> findPathById = sampleInputTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				// 把SampleInfo里的值赋到新保存的SampleInfo中
				String code = sc.getSampleInfo().getCode();
				SampleInfo sampleInfo = sampleInputTempDao
						.findSampleInfoByCode(code);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleInputTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleInputTempDao.get(SampleInfo.class, sc
						.getSampleInfo().getId());
				sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sampleInputTempDao.saveOrUpdate(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);
			}
		}
	}

	// 两个集合进行对比 如果有两条数据一样就把SampleInputTemp里的值赋给SmpleInput里
	public void compareAndTransmit(SampleInfo si) throws Exception {
		boolean flag = true;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");
		String code = si.getCode();
		String co = code.substring(0, 1);
		if (co.equals("A")
				|| (!co.equals("B") && !co.equals("E") && !co.equals("G")
						&& !co.equals("V") && !co.equals("T")
						&& !co.equals("U") && !co.equals("W")
						&& !co.equals("J") && !co.equals("K"))) {// 产前

			List<SampleInputTemp> list = sampleInputDao
					.findListBySampleInfo(code);
			SampleInputTemp s0 = list.get(0);
			// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
			if (list.size() == 2) {
				SampleInputTemp s1 = list.get(1);
				Class c = s0.getClass();
				Field[] m = c.getDeclaredFields();
				try {
					for (int i = 0; i < m.length; i++) {
						if (!m[i].getName().equals("id")
								&& !m[i].getName().equals("createUser")
								&& !m[i].getName().equals("sampleInfo")
								&& !m[i].getName().equals("state")
								&& !m[i].getName().equals("stateName")
								&& !m[i].getName().equals("sampleType")
								&& !m[i].getName().equals("voucherType")
								&& !m[i].getName().equals("receiptType")
								&& !m[i].getName().equals("birthday")
								&& !m[i].getName().equals("upLoadAccessory")) {
							m[i].setAccessible(true);
							Object o0 = BeanUtils.getFieldValue(s0,
									m[i].getName());
							Object o1 = BeanUtils.getFieldValue(s1,
									m[i].getName());

							if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
								flag = false;
							}
						}
					}
				} catch (Throwable e) {
					System.err.println(e);
				}

				s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				// 当前用户给到SamplInfo中
				si.setCreateUser2(s1.getCreateUser1());
				// 两次录入后的送检医院和SampleInfo中的送检医院是否一样,如果一样就flag就是true,不一样flag就返回false
				String hospital2 = s0.getHospital();// 第一次录入的送检医院
				String hospital3 = s1.getHospital();// 第二次录入的送检医院
				String hospital4 = si.getHospital();// SampleInfo的送检医院
				String serviceNum = code.substring(0, 1);
				String addressNum = code.substring(4, 6);
				// 模板之间的不同，
				if (serviceNum.equals("A")) {
					if (addressNum.equals("EK")) {// 用送检医院这个字段
						if (hospital4 != null && !hospital2.equals(hospital3)
								&& !hospital3.equals(hospital4)) {// 判断SampleInfo中的送检医院是否是空如果是空就不进行对比
							flag = false;
						}
					} else if (addressNum.equals("EB")) {// 没有送检医院字段
						if (hospital4 != null && !hospital2.equals(hospital3)
								&& !hospital3.equals(hospital4)) {
							flag = false;
						}
					} else {
						if (hospital4 != null && !hospital2.equals(hospital3)
								&& !hospital3.equals(hospital4)) {
							flag = false;
						}
					}
				} else {
					if (hospital4 != null && !hospital2.equals(hospital3)
							&& !hospital3.equals(hospital4)) {
						flag = false;
					}
				}
				// 录入的时候 两次完全一样
				if (flag == true) {
					// 两条数据都改为信息已录入 其中第一条数据在这里给状态
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);

					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
					SampleInput sampleInput = new SampleInput();
					// 取SmpleInputTemp其中一条数据给SampleInput
					copy(sampleInput, s0);
					// 两条数据相同的时候把SampleInput里的数据给SampleInfo;
					String patientName = sampleInput.getPatientName();
					if (patientName != null) {
						si.setPatientName(patientName); // 病人姓名
					}
					String voucherCode = sampleInput.getVoucherCode();
					if (voucherCode != null) {
						si.setIdCard(voucherCode); // 身份证号
					}
					String phoneNum = sampleInput.getPhoneNum();
					if (phoneNum != null) {
						si.setPhone(phoneNum); // 手机号
					}
					String productId = sampleInput.getProductId();
					if (productId != null) {
						si.setProductId(productId); // 检测项目
					}
					String productName = sampleInput.getProductName();
					if (productName != null) {
						si.setProductName(productName); // 检测项目
					}
					String hospital = sampleInput.getHospital();
					if (hospital != null) {
						si.setHospital(sampleInput.getHospital());// 医院
					}
					Date date1 = sampleInput.getSendDate(); // 取样时间
					if (date1 != null) {
						String scDate = formatter.format(date1);
						si.setSampleTime(scDate);
					}
					String address = sampleInput.getAddress(); // 家庭住址
					if (address != null) {
						si.setFamilyAddress(address);
					}
					User createUser = sampleInput.getCreateUser();// 录入人
					if (createUser != null) {
						si.setCreateUser(createUser);
					}
					User auditMan = sampleInput.getAuditMan(); // 审核者
					if (auditMan != null) {
						si.setConfirmUser1(auditMan.toString());
					}
					Date date3 = sampleInput.getReportDate(); // 应出报告日期
					if (date3 != null) {
						String scDate = formatter.format(date3);
						si.setReportDate(scDate);
					}
					Date acceptDate = sampleInput.getAcceptDate();// 接收日期
					if (acceptDate != null) {
						// String scDate = formatter.format(acceptDate);
						si.setReceiveDate(acceptDate);
					}
					transmit(si);
				}
			}
			if (list.size() < 2 || s0.getState().equals("3")) {
				s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				si.setCreateUser1(s0.getCreateUser1());
			}
		} else if (co.equals("B")) {// 血液病

			List<SampleBloodDiseaseTemp> list = sampleBloodDiseaseTempDao
					.findListBySampleInfoCode1(code);
			if (list.size() > 0) {
				SampleBloodDiseaseTemp s0 = list.get(0);
				// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
				if (list.size() == 2) {
					SampleBloodDiseaseTemp s1 = list.get(1);
					Class c = s0.getClass();
					Field[] m = c.getDeclaredFields();
					try {
						for (int i = 0; i < m.length; i++) {
							if (!m[i].getName().equals("id")
									&& !m[i].getName().equals("createUser")
									&& !m[i].getName().equals("sampleInfo")
									&& !m[i].getName().equals("state")
									&& !m[i].getName().equals("stateName")
									&& !m[i].getName().equals("sampleType")
									&& !m[i].getName().equals("voucherType")
									&& !m[i].getName().equals("receiptType")
									&& !m[i].getName().equals("birthday")
									&& !m[i].getName()
											.equals("upLoadAccessory")) {
								m[i].setAccessible(true);
								Object o0 = BeanUtils.getFieldValue(s0,
										m[i].getName());
								Object o1 = BeanUtils.getFieldValue(s1,
										m[i].getName());

								if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
									flag = false;
								}
							}
						}
					} catch (Throwable e) {
						System.err.println(e);
					}
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					// 当前用户给到SamplInfo中
					si.setCreateUser2(s1.getCreateUser());
					// 录入的时候 两次完全一样
					if (flag == true) {
						// 两条数据都改为信息已录入 其中两条数据在这里给状态和SampleInfo
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						SampleTumor sampleTumor = new SampleTumor();
						// 取SmpleInputTemp其中一条数据给SampleInput
						copy(sampleTumor, s0);

						// 两条数据相同的时候把SampleInput里的数据给SampleInfo;
						String patientName = sampleTumor.getPatientName();
						if (patientName != null) {
							si.setPatientName(patientName); // 病人姓名
						}
						String voucherCode = sampleTumor.getVoucherCode();
						if (voucherCode != null) {
							si.setIdCard(voucherCode); // 身份证号
						}
						String phoneNum = sampleTumor.getPhone();
						if (phoneNum != null) {
							si.setPhone(phoneNum); // 手机号
						}
						String productId = sampleTumor.getProductId();
						if (productId != null) {
							si.setProductId(productId); // 检测项目
						}
						String productName = sampleTumor.getProductName();
						if (productName != null) {
							si.setProductName(productName); // 检测项目
						}
						String hospital = sampleTumor.getHospital();
						if (hospital != null) {
							si.setHospital(sampleTumor.getHospital());// 医院
						}
						Date date1 = sampleTumor.getSendDate(); // 取样时间
						if (date1 != null) {
							String scDate = formatter.format(date1);
							si.setSampleTime(scDate);
						}
						String address = sampleTumor.getAddress(); // 家庭住址
						if (address != null) {
							si.setFamilyAddress(address);
						}
						// User createUser = sampleTumor.getCreateUser();// 录入人
						// if (createUser != null) {
						// si.setCreateUser(createUser);
						// }
						User auditMan = sampleTumor.getAuditMan(); // 审核者
						if (auditMan != null) {
							si.setConfirmUser1(auditMan.toString());
						}
						Date date3 = sampleTumor.getReportDate(); // 应出报告日期
						if (date3 != null) {
							String scDate = formatter.format(date3);
							si.setReportDate(scDate);
						}
						Date acceptDate = sampleTumor.getAcceptDate();// 接收日期
						if (acceptDate != null) {
							String scDate = formatter.format(acceptDate);
							si.setReceiveDate(acceptDate);
						}
						transmit(si);
					}
				}
				if (list.size() < 2 || s0.getState().equals("3")) {
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setCreateUser1(s0.getCreateUser());
				}
			}
		} else if (co.equals("E")) {// 染色体
			List<SampleChromosomeTemp> list = sampleChromosomeTempDao
					.findListBySampleInfoCode1(code);
			if (list.size() > 0) {
				SampleChromosomeTemp s0 = list.get(0);
				// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
				if (list.size() == 2) {
					SampleChromosomeTemp s1 = list.get(1);
					Class c = s0.getClass();
					Field[] m = c.getDeclaredFields();
					try {
						for (int i = 0; i < m.length; i++) {
							if (!m[i].getName().equals("id")
									&& !m[i].getName().equals("createUser")
									&& !m[i].getName().equals("sampleInfo")
									&& !m[i].getName().equals("state")
									&& !m[i].getName().equals("stateName")
									&& !m[i].getName().equals("sampleType")
									&& !m[i].getName().equals("voucherType")
									&& !m[i].getName().equals("receiptType")
									&& !m[i].getName().equals("birthday")
									&& !m[i].getName()
											.equals("upLoadAccessory")) {
								m[i].setAccessible(true);
								Object o0 = BeanUtils.getFieldValue(s0,
										m[i].getName());
								Object o1 = BeanUtils.getFieldValue(s1,
										m[i].getName());

								if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
									flag = false;
								}
							}
						}
					} catch (Throwable e) {
						System.err.println(e);
					}
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					// 当前用户给到SamplInfo中
					si.setCreateUser2(s1.getCreateUser());
					// 录入的时候 两次完全一样
					if (flag == true) {
						// 两条数据都改为信息已录入 其中两条数据在这里给状态和SampleInfo
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						SampleChromosome sampleChromosome = new SampleChromosome();
						// 取SmpleInputTemp其中一条数据给SampleInput
						copy(sampleChromosome, s0);

						// 两条数据相同的时候把SampleInput里的数据给SampleInfo;
						String patientName = sampleChromosome.getPatientName();
						if (patientName != null) {
							si.setPatientName(patientName); // 病人姓名
						}
						String voucherCode = sampleChromosome.getVoucherCode();
						if (voucherCode != null) {
							si.setIdCard(voucherCode); // 身份证号
						}
						String phoneNum = sampleChromosome.getPhone();
						if (phoneNum != null) {
							si.setPhone(phoneNum); // 手机号
						}
						String productId = sampleChromosome.getProductId();
						if (productId != null) {
							si.setProductId(productId); // 检测项目
						}
						String productName = sampleChromosome.getProductName();
						if (productName != null) {
							si.setProductName(productName); // 检测项目
						}
						String hospital = sampleChromosome.getHospital();
						if (hospital != null) {
							si.setHospital(sampleChromosome.getHospital());// 医院
						}
						Date date1 = sampleChromosome.getSendDate(); // 取样时间
						if (date1 != null) {
							String scDate = formatter.format(date1);
							si.setSampleTime(scDate);
						}
						String address = sampleChromosome.getAddress(); // 家庭住址
						if (address != null) {
							si.setFamilyAddress(address);
						}
						// User createUser = sampleChromosome.getCreateUser();//
						// 录入人
						// if (createUser != null) {
						// si.setCreateUser(createUser);
						// }
						User auditMan = sampleChromosome.getAuditMan(); // 审核者
						if (auditMan != null) {
							si.setConfirmUser1(auditMan.toString());
						}
						Date date3 = sampleChromosome.getReportDate(); // 应出报告日期
						if (date3 != null) {
							String scDate = formatter.format(date3);
							si.setReportDate(scDate);
						}
						Date acceptDate = sampleChromosome.getAcceptDate();// 接收日期
						if (acceptDate != null) {
							String scDate = formatter.format(acceptDate);
							si.setReceiveDate(acceptDate);
						}
						transmit(si);
					}
				}
				if (list.size() < 2 || s0.getState().equals("3")) {
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setCreateUser1(s0.getCreateUser());
				}
			}
		} else if (co.equals("G") || co.equals("V")) {
		} else if (co.equals("T")) {// 肿瘤

			List<SampleTumorTemp> list = sampleTumorTempDao
					.findListBySampleInfoCode1(code);
			if (list.size() > 0) {
				SampleTumorTemp s0 = list.get(0);

				// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
				if (list.size() == 2) {

					SampleTumorTemp s1 = list.get(1);
					Class c = s0.getClass();
					Field[] m = c.getDeclaredFields();
					try {
						for (int i = 0; i < m.length; i++) {
							if (!m[i].getName().equals("id")
									&& !m[i].getName().equals("createUser")
									&& !m[i].getName().equals("sampleInfo")
									&& !m[i].getName().equals("state")
									&& !m[i].getName().equals("stateName")
									&& !m[i].getName().equals("sampleType")
									&& !m[i].getName().equals("voucherType")
									&& !m[i].getName().equals("receiptType")
									&& !m[i].getName().equals("birthday")
									&& !m[i].getName()
											.equals("upLoadAccessory")) {
								m[i].setAccessible(true);
								Object o0 = BeanUtils.getFieldValue(s0,
										m[i].getName());
								Object o1 = BeanUtils.getFieldValue(s1,
										m[i].getName());

								if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
									flag = false;
								}
							}
						}
					} catch (Throwable e) {
						System.err.println(e);
					}
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					// 当前用户给到SamplInfo中
					si.setCreateUser2(s1.getCreateUser());
					// 录入的时候 两次完全一样
					if (flag == true) {
						// 两条数据都改为信息已录入 其中两条数据在这里给状态和SampleInfo
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);

						SampleTumor sampleTumor = new SampleTumor();
						// 取SmpleInputTemp其中一条数据给SampleInput
						copy(sampleTumor, s0);

						// 两条数据相同的时候把SampleInput里的数据给SampleInfo;
						String patientName = sampleTumor.getPatientName();
						if (patientName != null) {
							si.setPatientName(patientName); // 病人姓名
						}

						si.setSampleRelevanceMain(sampleTumor.getSampleNum());// 主样本号
						String voucherCode = sampleTumor.getVoucherCode();
						if (voucherCode != null) {
							si.setIdCard(voucherCode); // 身份证号
						}
						String phoneNum = sampleTumor.getPhone();
						if (phoneNum != null) {
							si.setPhone(phoneNum); // 手机号
						}
						String productId = sampleTumor.getProductId();
						if (productId != null) {
							si.setProductId(productId); // 检测项目
						}
						String productName = sampleTumor.getProductName();
						if (productName != null) {
							si.setProductName(productName); // 检测项目
						}
						String hospital = sampleTumor.getHospital();
						if (hospital != null) {
							si.setHospital(sampleTumor.getHospital());// 医院
						}
						Date date1 = sampleTumor.getSendDate(); // 取样时间
						if (date1 != null) {
							String scDate = formatter.format(date1);
							si.setSampleTime(scDate);
						}
						String address = sampleTumor.getAddress(); // 家庭住址
						if (address != null) {
							si.setFamilyAddress(address);
						}
						// User createUser =
						// sampleBloodDisease.getCreateUser();// 录入人
						// if (createUser != null) {
						// si.setCreateUser(createUser);
						// }
						User auditMan = sampleTumor.getAuditMan(); // 审核者
						if (auditMan != null) {
							si.setConfirmUser1(auditMan.toString());
						}
						Date date3 = sampleTumor.getReportDate(); // 应出报告日期
						if (date3 != null) {
							String scDate = formatter.format(date3);
							si.setReportDate(scDate);
						}
						Date acceptDate = sampleTumor.getAcceptDate();// 接收日期
						if (acceptDate != null) {
							String scDate = formatter.format(acceptDate);
							si.setReceiveDate(acceptDate);
						}
						transmit(si);
					}
				}
				if (list.size() < 2 || s0.getState().equals("3")) {
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setCreateUser1(s0.getCreateUser());
				}
			}
		} else if (co.equals("U")) {// 单基因
			List<SampleGeneTemp> list = sampleGeneTempDao
					.findListBySampleInfoCode1(code);
			if (list.size() > 0) {
				SampleGeneTemp s0 = list.get(0);
				// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
				if (list.size() == 2) {
					SampleGeneTemp s1 = list.get(1);
					Class c = s0.getClass();
					Field[] m = c.getDeclaredFields();
					try {
						for (int i = 0; i < m.length; i++) {
							if (!m[i].getName().equals("id")
									&& !m[i].getName().equals("createUser")
									&& !m[i].getName().equals("sampleInfo")
									&& !m[i].getName().equals("state")
									&& !m[i].getName().equals("stateName")
									&& !m[i].getName().equals("sampleType")
									&& !m[i].getName().equals("voucherType")
									&& !m[i].getName().equals("receiptType")
									&& !m[i].getName().equals("birthday")
									&& !m[i].getName()
											.equals("upLoadAccessory")) {
								m[i].setAccessible(true);
								Object o0 = BeanUtils.getFieldValue(s0,
										m[i].getName());
								Object o1 = BeanUtils.getFieldValue(s1,
										m[i].getName());

								if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
									flag = false;
								}
							}
						}
					} catch (Throwable e) {
						System.err.println(e);
					}
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					// 当前用户给到SamplInfo中
					si.setCreateUser2(s1.getCreateUser());
					// 录入的时候 两次完全一样
					if (flag == true) {
						// 两条数据都改为信息已录入 其中两条数据在这里给状态和SampleInfo
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						SampleGene sampleGene = new SampleGene();
						// 取SmpleInputTemp其中一条数据给SampleInput
						copy(sampleGene, s0);
						// 两条数据相同的时候把SampleInput里的数据给SampleInfo;
						String patientName = sampleGene.getPatientName();
						if (patientName != null) {
							si.setPatientName(patientName); // 病人姓名
						}
						String voucherCode = sampleGene.getVoucherCode();
						if (voucherCode != null) {
							si.setIdCard(voucherCode); // 身份证号
						}
						String phoneNum = sampleGene.getPhone();
						if (phoneNum != null) {
							si.setPhone(phoneNum); // 手机号
						}
						String productId = sampleGene.getProductId();
						if (productId != null) {
							si.setProductId(productId); // 检测项目
						}
						String productName = sampleGene.getProductName();
						if (productName != null) {
							si.setProductName(productName); // 检测项目
						}
						String hospital = sampleGene.getHospital();
						if (hospital != null) {
							si.setHospital(sampleGene.getHospital());// 医院
						}
						Date date1 = sampleGene.getSendDate(); // 取样时间
						if (date1 != null) {
							String scDate = formatter.format(date1);
							si.setSampleTime(scDate);
						}
						String address = sampleGene.getAddress(); // 家庭住址
						if (address != null) {
							si.setFamilyAddress(address);
						}
						// User createUser = sampleGene.getCreateUser();// 录入人
						// if (createUser != null) {
						// si.setCreateUser(createUser);
						// }
						User auditMan = sampleGene.getAuditMan(); // 审核者
						if (auditMan != null) {
							si.setConfirmUser1(auditMan.toString());
						}
						Date date3 = sampleGene.getReportDate(); // 应出报告日期
						if (date3 != null) {
							String scDate = formatter.format(date3);
							si.setReportDate(scDate);
						}
						Date acceptDate = sampleGene.getAcceptDate();// 接收日期
						if (acceptDate != null) {
							String scDate = formatter.format(acceptDate);
							si.setReceiveDate(acceptDate);
						}
						transmit(si);
					}
				}
				if (list.size() < 2 || s0.getState().equals("3")) {
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setCreateUser1(s0.getCreateUser());
				}
			}
		} else if (co.equals("W")) {// 叶酸
			List<SampleFolicAcidTemp> list = sampleFolicAcidTempDao
					.findListBySampleInfoCode1(code);
			if (list.size() > 0) {
				SampleFolicAcidTemp s0 = list.get(0);
				// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
				if (list.size() == 2) {
					SampleFolicAcidTemp s1 = list.get(1);
					Class c = s0.getClass();
					Field[] m = c.getDeclaredFields();
					try {
						for (int i = 0; i < m.length; i++) {
							if (!m[i].getName().equals("id")
									&& !m[i].getName().equals("createUser")
									&& !m[i].getName().equals("sampleInfo")
									&& !m[i].getName().equals("state")
									&& !m[i].getName().equals("stateName")
									&& !m[i].getName().equals("sampleType")
									&& !m[i].getName().equals("voucherType")
									&& !m[i].getName().equals("receiptType")
									&& !m[i].getName().equals("birthday")
									&& !m[i].getName()
											.equals("upLoadAccessory")) {
								m[i].setAccessible(true);
								Object o0 = BeanUtils.getFieldValue(s0,
										m[i].getName());
								Object o1 = BeanUtils.getFieldValue(s1,
										m[i].getName());

								if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
									flag = false;
								}
							}
						}
					} catch (Throwable e) {
						System.err.println(e);
					}
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					// 当前用户给到SamplInfo中
					si.setCreateUser2(s1.getCreateUser());
					// 录入的时候 两次完全一样
					if (flag == true) {
						// 两条数据都改为信息已录入 其中两条数据在这里给状态和SampleInfo
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						SampleFolicAcid sampleFolicAcid = new SampleFolicAcid();
						// 取SmpleInputTemp其中一条数据给SampleInput
						copy(sampleFolicAcid, s0);
						// 两条数据相同的时候把SampleInput里的数据给SampleInfo;
						String patientName = sampleFolicAcid.getPatientName();
						if (patientName != null) {
							si.setPatientName(patientName); // 病人姓名
						}
						// String voucherCode =
						// sampleFolicAcid.getVoucherCode();
						// if (voucherCode != null) {
						// si.setIdCard(voucherCode); // 身份证号
						// }
						String phoneNum = sampleFolicAcid.getPhoneNum();
						if (phoneNum != null) {
							si.setPhone(phoneNum); // 手机号
						}
						String productId = sampleFolicAcid.getProductId();
						if (productId != null) {
							si.setProductId(productId); // 检测项目
						}
						String productName = sampleFolicAcid.getProductName();
						if (productName != null) {
							si.setProductName(productName); // 检测项目
						}
						String hospital = sampleFolicAcid.getHospital();
						if (hospital != null) {
							si.setHospital(sampleFolicAcid.getHospital());// 医院
						}
						Date date1 = sampleFolicAcid.getSendDate(); // 取样时间
						if (date1 != null) {
							String scDate = formatter.format(date1);
							si.setSampleTime(scDate);
						}
						String address = sampleFolicAcid.getAddress(); // 家庭住址
						if (address != null) {
							si.setFamilyAddress(address);
						}
						// User createUser = sampleFolicAcid.getCreateUser();//
						// 录入人
						// if (createUser != null) {
						// si.setCreateUser(createUser);
						// }
						User auditMan = sampleFolicAcid.getAuditMan(); // 审核者
						if (auditMan != null) {
							si.setConfirmUser1(auditMan.toString());
						}
						Date date3 = sampleFolicAcid.getReportDate(); // 应出报告日期
						if (date3 != null) {
							String scDate = formatter.format(date3);
							si.setReportDate(scDate);
						}
						Date acceptDate = sampleFolicAcid.getAcceptDate();// 接收日期
						if (acceptDate != null) {
							String scDate = formatter.format(acceptDate);
							si.setReceiveDate(acceptDate);
						}
						transmit(si);
					}
				}
				if (list.size() < 2 || s0.getState().equals("3")) {
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setCreateUser1(s0.getCreateUser());
				}
			}
		} else if (co.equals("J") || co.equals("K")) {// 基因突变
			List<SampleVisitTemp> list = sampleVisitTempDao
					.findListBySampleInfoCode1(code);
			if (list.size() > 0) {
				SampleVisitTemp s0 = list.get(0);
				// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
				if (list.size() == 2) {
					SampleVisitTemp s1 = list.get(1);
					Class c = s0.getClass();
					Field[] m = c.getDeclaredFields();
					try {
						for (int i = 0; i < m.length; i++) {
							if (!m[i].getName().equals("id")
									&& !m[i].getName().equals("createUser")
									&& !m[i].getName().equals("sampleInfo")
									&& !m[i].getName().equals("state")
									&& !m[i].getName().equals("stateName")
									&& !m[i].getName().equals("sampleType")
									&& !m[i].getName().equals("voucherType")
									&& !m[i].getName().equals("receiptType")
									&& !m[i].getName().equals("birthday")
									&& !m[i].getName()
											.equals("upLoadAccessory")) {
								m[i].setAccessible(true);
								Object o0 = BeanUtils.getFieldValue(s0,
										m[i].getName());
								Object o1 = BeanUtils.getFieldValue(s1,
										m[i].getName());

								if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
									flag = false;
								}
							}
						}
					} catch (Throwable e) {
						System.err.println(e);
					}
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					// 当前用户给到SamplInfo中
					si.setCreateUser2(s1.getCreateUser());
					// 录入的时候 两次完全一样
					if (flag == true) {
						// 两条数据都改为信息已录入 其中两条数据在这里给状态和SampleInfo
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						SampleVisit sampleVisit = new SampleVisit();
						// 取SmpleInputTemp其中一条数据给SampleInput
						copy(sampleVisit, s0);
						String patientName = sampleVisit.getPatientName();
						if (patientName != null) {
							si.setPatientName(patientName); // 病人姓名
						}
						String voucherCode = sampleVisit.getVoucherCode();
						if (voucherCode != null) {
							si.setIdCard(voucherCode); // 身份证号
						}
						String phoneNum = sampleVisit.getPhoneNum();
						if (phoneNum != null) {
							si.setPhone(phoneNum); // 手机号
						}
						String productId = sampleVisit.getProductId();
						if (productId != null) {
							si.setProductId(productId); // 检测项目
						}
						String productName = sampleVisit.getProductName();
						if (productName != null) {
							si.setProductName(productName); // 检测项目
						}
						String hospital = sampleVisit.getHospital();
						if (hospital != null) {
							si.setHospital(sampleVisit.getHospital());// 医院
						}
						Date date1 = sampleVisit.getSendDate(); // 取样时间
						if (date1 != null) {
							String scDate = formatter.format(date1);
							si.setSampleTime(scDate);
						}
						String address = sampleVisit.getAddress(); // 家庭住址
						if (address != null) {
							si.setFamilyAddress(address);
						}
						// User createUser = sampleVisit.getCreateUser();// 录入人
						// if (createUser != null) {
						// si.setCreateUser(createUser);
						// }
						// User auditMan = sampleVisit.getAuditMan(); // 审核者
						// if (auditMan != null) {
						// si.setConfirmUser1(auditMan.toString());
						// }
						Date date3 = sampleVisit.getReportDate(); // 应出报告日期
						if (date3 != null) {
							String scDate = formatter.format(date3);
							si.setReportDate(scDate);
						}
						Date acceptDate = sampleVisit.getAcceptDate();// 接收日期
						if (acceptDate != null) {
							String scDate = formatter.format(acceptDate);
							si.setReceiveDate(acceptDate);
						}
						transmit(si);
					}
				}
				if (list.size() < 2 || s0.getState().equals("3")) {
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					si.setCreateUser1(s0.getCreateUser());
				}
			}
		}
	}

	// 两个对象之间的赋值
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void copy(Object input, Object inputTemp) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		Field[] inputFields = input.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			try {
				inputMap.put(fd.getName(),
						BeanUtils.getFieldValue(inputTemp, fd.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Field[] inputTempFields = input.getClass().getDeclaredFields();
		for (Field fd : inputTempFields) {
			if (inputMap.get(fd.getName()) == null) {
				continue;
			}
			try {
				// 给属性赋值
				BeanUtils.setFieldValue(input, fd.getName(),
						inputMap.get(fd.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		sampleInputTempDao.save(input);
	}

	/**
	 * 保存肿瘤模板
	 * 
	 * @param sc
	 * @param jsonMap
	 * @param imgId
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveZl(SampleTumorTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			// 把SampleInfo里的值赋到新保存的SampleInfo中
			if (img.equals("") || img == null || !img.equals(imgId)) {
				String code = sc.getCode();
				SampleInfo sampleInfo = sampleTumorTempDao
						.findSampleInfoByCode(code);
				List<FileInfo> findPathById = sampleTumorTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleTumorTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleTumorTempDao.get(SampleInfo.class, sc
						.getSampleInfo().getId());
				sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(si);
				sampleTumorTempDao.save(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);

			}
		}
	}

	/**
	 * 保存血液病
	 * 
	 * @param sc
	 * @param jsonMap
	 * @param imgId
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveXyb(SampleBloodDiseaseTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			// 把SampleInfo里的值赋到新保存的SampleInfo中
			if (img.equals("") || img == null || !img.equals(imgId)) {
				String code = sc.getCode();
				SampleInfo sampleInfo = sampleBloodDiseaseTempDao
						.findSampleInfoByCode(code);
				List<FileInfo> findPathById = sampleBloodDiseaseTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleBloodDiseaseTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleBloodDiseaseTempDao.get(SampleInfo.class,
						sc.getSampleInfo().getId());
				sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(si);
				sampleBloodDiseaseTempDao.save(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);

			}
		}
	}

	/**
	 * 保存染色体
	 * 
	 * @param sc
	 * @param jsonMap
	 * @param imgId
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRst(SampleChromosomeTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			// 把SampleInfo里的值赋到新保存的SampleInfo中
			if (img.equals("") || img == null || !img.equals(imgId)) {
				String code = sc.getCode();
				SampleInfo sampleInfo = sampleChromosomeTempDao
						.findSampleInfoByCode(code);
				List<FileInfo> findPathById = sampleChromosomeTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleChromosomeTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleChromosomeTempDao.get(SampleInfo.class,
						sc.getSampleInfo().getId());
				sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(si);
				sampleChromosomeTempDao.save(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);

			}
		}
	}

	/**
	 * 保存叶酸
	 * 
	 * @param sc
	 * @param jsonMap
	 * @param imgId
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveYs(SampleFolicAcidTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			// 把SampleInfo里的值赋到新保存的SampleInfo中
			if (img.equals("") || img == null || !img.equals(imgId)) {
				String code = sc.getCode();
				SampleInfo sampleInfo = sampleFolicAcidTempDao
						.findSampleInfoByCode(code);
				List<FileInfo> findPathById = sampleFolicAcidTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleFolicAcidTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleFolicAcidTempDao.get(SampleInfo.class, sc
						.getSampleInfo().getId());
				sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(si);
				sampleFolicAcidTempDao.save(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);

			}
		}
	}

	/**
	 * 保存单基因
	 * 
	 * @param sc
	 * @param jsonMap
	 * @param imgId
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDjy(SampleGeneTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			// 把SampleInfo里的值赋到新保存的SampleInfo中
			if (img.equals("") || img == null || !img.equals(imgId)) {
				String code = sc.getCode();
				SampleInfo sampleInfo = sampleGeneTempDao
						.findSampleInfoByCode(code);
				List<FileInfo> findPathById = sampleGeneTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleGeneTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleGeneTempDao.get(SampleInfo.class, sc
						.getSampleInfo().getId());
				sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(si);
				sampleGeneTempDao.save(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);

			}
		}
	}

	/**
	 * 保存基因突变
	 * 
	 * @param sc
	 * @param jsonMap
	 * @param imgId
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveJytb(SampleVisitTemp sc, Map jsonMap, String imgId)
			throws Exception {
		if (sc != null) {
			String img = sc.getSampleInfo().getUpLoadAccessory().getId();
			// 把SampleInfo里的值赋到新保存的SampleInfo中
			if (img.equals("") || img == null || !img.equals(imgId)) {
				String code = sc.getCode();
				SampleInfo sampleInfo = sampleVisitTempDao
						.findSampleInfoByCode(code);
				List<FileInfo> findPathById = sampleVisitTempDao
						.findPathTempById(imgId);
				// 保存图片的时候sampleInfo是没状态的所以 把"完成打包"的状态给sampleInfo
				sc.getSampleInfo()
						.setState(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.getSampleInfo()
						.setStateName(
								com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(sampleInfo);
				sc.getSampleInfo().setUpLoadAccessory(findPathById.get(0));
				sampleVisitTempDao.saveOrUpdate(sc.getSampleInfo());
			} else {
				SampleInfo si = sampleVisitTempDao.get(SampleInfo.class, sc
						.getSampleInfo().getId());
				// sc.setCode(si.getCode());
				// 保存时把SampleInputTemp里的录入人保存到当前SampleInfo 里
				sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				sc.setSampleInfo(si);
				sampleVisitTempDao.save(sc);
				si.setUpLoadAccessory(sc.getUpLoadAccessory());
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
				compareAndTransmit(si);

			}
		}
	}

	// 审批完成
	public void changeState(String applicationTypeActionId, String id) {
		SampleInput sr = sampleInputDao.get(SampleInput.class, id);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		sampleInputDao.update(sr);
	}

	// 根据code查
	public Long searchByCode(String code) throws Exception {
		Long num = sampleInputDao.findByCode(code);
		if (num == 0) {
			return num;
		}
		return (long) 0;
	}

	// 保存SampleInfo
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToSampleInfo(String itemDataJson) throws Exception {
		List<SampleOrder> saveItems = new ArrayList<SampleOrder>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrder sbi = new SampleOrder();
			sbi = (SampleOrder) sampleInputDao.Map2Bean(map, sbi);

			saveItems.add(sbi);
		}
		sampleInputDao.saveOrUpdateAll(saveItems);
	}

	// 批量上传图片生成一条SampleInfo
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInput(String code) throws Exception {

		String sCode = code.substring(0, code.length() - 4);
		SampleOrder si = sampleInputDao.findSampleOrderByCode(sCode);
		if (si == null) {
			SampleOrder sinfo = new SampleOrder();

			sinfo.setId(sCode);
			sinfo.setUpImgTime(new Date());
			sinfo.setState("15");
			sinfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
			FileInfo fi = sampleInputDao.findFileName(code);

			sampleInputDao.saveOrUpdate(sinfo);
		}
	}

	/**
	 * 公共的传递值
	 * 
	 * @param si
	 * @throws Exception
	 */
	public void transmit(SampleInfo si) throws Exception {
		Map<String, Object> result = productDao.findProductByMark(si.getCode()
				.substring(0, 1));
		List<Product> prolist = (List<Product>) result.get("list");

		if (prolist.size() > 0) {
			WorkOrder wo = new WorkOrder();
			wo = workOrderDao.findWorkOrderByProduct(prolist.get(0).getId());
			si.setOrderId(wo);
		}

		String sampleCode = si.getCode();// 样本编号
		SampleReceiveItem sr = sampleReceiveDao
				.getSampleReceiveItem(sampleCode);
		String mark1 = sampleCode.substring(1, 2);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");
		if (sr != null) {
			if (sr.getState() != null && !sr.getState().equals("")
					&& sr.getState().equals("59")) {
				Product product = null;
				String co = sampleCode.substring(0, 1);
				if (co.equals("A")
						|| (!co.equals("B") && !co.equals("E")
								&& !co.equals("G") && !co.equals("V")
								&& !co.equals("T") && !co.equals("U")
								&& !co.equals("W") && !co.equals("J") && !co
									.equals("K"))) {// 产前
					product = sampleInputDao.get(Product.class,
							si.getProductId());
				} else if (co.equals("B")) {// 血液病
					product = sampleBloodDiseaseTempDao.get(Product.class,
							si.getProductId());
				} else if (co.equals("E")) {// 染色体
					product = sampleBloodDiseaseTempDao.get(Product.class,
							si.getProductId());
				} else if (co.equals("G") || co.equals("V")) {// 乳腺癌

				} else if (co.equals("T")) {// 肿瘤
					product = sampleTumorTempDao.get(Product.class,
							si.getProductId());
				} else if (co.equals("U")) {// 单基因
					product = sampleGeneTempDao.get(Product.class,
							si.getProductId());
				} else if (co.equals("W")) {// 叶酸
					product = sampleFolicAcidTempDao.get(Product.class,
							si.getProductId());
				} else if (co.equals("J") || co.equals("K")) {// 基因突变
					product = sampleVisitTempDao.get(Product.class,
							si.getProductId());
				}

			}
		}
	}
}
