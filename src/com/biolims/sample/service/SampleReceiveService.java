package com.biolims.sample.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.WorkDay;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.crm.agent.primary.dao.PrimaryTaskDao;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.dao.CrmPatientDao;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleInputDao;
import com.biolims.sample.dao.SampleOrderChangeDao;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.encod.model.SampleOrderInfoItem;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.DicSampleTypeItem;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderChange;
import com.biolims.sample.model.SampleOrderChangeItem;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleSystemItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.dao.ProductDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleReceiveService {
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private PrimaryTaskDao primaryTaskDao;
	@Resource
	private SampleInputDao sampleInputDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SampleOrderDao sampleOrderDao;
	@Resource
	private CommonService commonService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private ProductDao productDao;
	@Resource
	private CrmPatientDao crmPatientDao;
	@Resource
	private SampleOrderChangeDao sampleOrderChangeDao;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleReceiveList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		return sampleReceiveDao.selectSampleReceiveList(mapForQuery, startNum, limitNum, dir, sort, type);
	}

	/** 样本明细条件查询 */
	public Map<String, Object> findSampleItemList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		return sampleReceiveDao.selectSampleItemList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/** 异常样本 */
	public Map<String, Object> findSampleAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleReceiveDao.selectSampleAbnormalList(mapForQuery, startNum, limitNum, dir, sort);
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(SampleReceive i) throws Exception {
	//
	// sampleReceiveDao.saveOrUpdate(i);
	//
	// }

	public SampleReceive get(String id) {
		SampleReceive sampleReceive = commonDAO.get(SampleReceive.class, id);
		return sampleReceive;
	}

	public Map<String, Object> findSampleReceiveItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleReceiveDao.selectSampleReceiveItemList(scId, startNum, limitNum, dir, sort);
		List<SampleReceiveItem> list = (List<SampleReceiveItem>) result.get("list");
		for (SampleReceiveItem sii : list) {
			long num = fileInfoService.findFileInfoCount(sii.getId(), "sampleReceiveItem");
			sii.setFileNum(String.valueOf(num));
		}
		result.put("list", list);
		return result;
	}

	public Map<String, Object> findSampleReceiveItemListByIsgood(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		// Map<String, Object> result =
		// sampleReceiveDao.selectSampleReceiveItemListByIsGood(mapForQuery,
		// startNum, limitNum, dir, sort);
		// List<SampleReceiveItem> list = (List<SampleReceiveItem>)
		// result.get("list");
		// for (SampleReceiveItem sii : list) {
		// long num = fileInfoService.findFileInfoCount(sii.getId(),
		// "sampleReceiveItem");
		// sii.setFileNum(String.valueOf(num));
		// }
		// result.put("list", list);
		// return result;
		return sampleReceiveDao.selectSampleReceiveItemListByIsGood(mapForQuery, startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findSampleSystemItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleReceiveDao.selectSampleSystemItemList(scId, startNum, limitNum, dir, sort);
		List<SampleSystemItem> list = (List<SampleSystemItem>) result.get("list");
		result.put("list", list);
		return result;
	}

	// //到文库的样本
	// public Map<String, Object> selectSampleReceiveItemListToWK(Integer
	// startNum, Integer limitNum, String dir,
	// String sort,String state) throws Exception {
	// Map<String, Object> result =
	// sampleReceiveDao.selectSampleReceiveItemListToWK(startNum, limitNum, dir,
	// sort, state);
	// List<SampleReceiveItem> list = (List<SampleReceiveItem>)
	// result.get("list");
	// for (SampleReceiveItem sii : list) {
	// long num = fileInfoService.findFileInfoCount(sii.getId(),
	// "sampleReceiveItem");
	// sii.setFileNum(String.valueOf(num));
	// }
	// result.put("list", list);
	// return result;
	// }

	// // 到血浆分离的样本
	// public Map<String, Object> selectPlasmaReceiveItemList(Integer startNum,
	// Integer limitNum, String dir, String sort) throws Exception {
	// Map<String, Object> result = sampleReceiveDao
	// .selectPlasmaReceiveTempList(startNum, limitNum, dir, sort);
	// List<PlasmaReceiveTemp> list = (List<PlasmaReceiveTemp>) result
	// .get("list");
	// result.put("list", list);
	// return result;
	// }

	// 到核酸提取的样本
	// public Map<String, Object> selectDnaReceiveTempList(Integer startNum,
	// Integer limitNum, String dir, String sort) throws Exception {
	// Map<String, Object> result = sampleReceiveDao.selectDnaReceiveTempList(
	// startNum, limitNum, dir, sort);
	// List<DnaReceiveTemp> list = (List<DnaReceiveTemp>) result.get("list");
	// result.put("list", list);
	// return result;
	// }

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleReceiveItem(SampleReceive sc, String itemDataJson) throws Exception {
		List<SampleReceiveItem> saveItems = new ArrayList<SampleReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleReceiveItem scp = new SampleReceiveItem();
			// 将map信息读入实体类
			scp = (SampleReceiveItem) sampleReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleReceive(sc);
			scp.setAcceptDate(new Date());
			if (scp.getOrderCode() != null && !"".equals(scp.getOrderCode())) {
				SampleOrder so = sampleReceiveDao.get(SampleOrder.class, scp.getOrderCode());
				if (so != null) {
					if (scp.getProductId() == null || (scp.getProductId() != null && scp.getProductId().equals(""))) {
						scp.setProductId(so.getProductId());
						scp.setProductName(so.getProductName());
					}
					if (scp.getPatientName() == null
							|| (scp.getPatientName() != null && scp.getPatientName().equals(""))) {
						scp.setPatientName(so.getName());
					}
					if (scp.getGender() == null || (scp.getGender() != null && scp.getGender().equals(""))) {
						scp.setGender(so.getGender());
					}
					if (scp.getInspectDate() == null && so.getReceivedDate() != null) {
						scp.setInspectDate(so.getInspectDate());
					}
					if (scp.getSellPerson() == null && so.getCommissioner() != null) {
						scp.setSellPerson(so.getCommissioner());
					}
					if (scp.getCrmCustomer() == null && so.getCrmCustomer() != null) {
						CrmCustomer cc = commonDAO.get(CrmCustomer.class, so.getCrmCustomer().getId());
						scp.setCrmCustomer(cc);
					}

				} else {

					so = new SampleOrder();
					if (scp.getOrderCode() != null && !scp.getOrderCode().equals("")) {
						so.setId(scp.getOrderCode());
					}
					if (scp.getProductId() != null && !scp.getProductId().equals("")) {
						so.setProductId(scp.getProductId());
						so.setProductName(scp.getProductName());
					}

					if (scp.getPatientName() != null && !scp.getPatientName().equals("")) {
						so.setName(scp.getPatientName());
					}

					if (scp.getInspectDate() != null && !scp.getInspectDate().equals("")) {
						so.setInspectDate(scp.getInspectDate());
					}

					if (scp.getSellPerson() != null) {
						so.setCommissioner(scp.getSellPerson());
					}
					so.setState("3");
					so.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
					so.setCreateDate(new Date());
					so.setCreateUser((User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY));
					sampleReceiveDao.saveOrUpdate(so);
				}
			}
			// 样本标志设置
			scp.setClassify(sc.getClassify());
			if (scp.getSampleCode() == null || (scp.getSampleCode() != null && scp.getSampleCode().equals(""))) {

				String markCode = "";
				if (scp.getDicSampleType() == null || scp.getDicSampleType().equals("")) {
					markCode = DateUtil.dateFormatterByPattern(sc.getAcceptDate(), "yyMMdd");

				} else {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
					markCode = d.getCode() + DateUtil.dateFormatterByPattern(sc.getAcceptDate(), "yyMMdd");
				}

				String code = codingRuleService.getSampleCode("SampleReceiveItem", markCode, 0000, 4, null);
				scp.setSampleCode(code);

			}
			if (scp.getDicSampleType() != null) {
				if (scp.getProductId() != null && !scp.getProductId().equals("")) {
					String[] productId = scp.getProductId().split(",");
					String pid = "";
					for (int i = 0; i < productId.length; i++) {
						pid += "and productId like '%" + productId[i] + "%' ";
					}
					List<DicSampleTypeItem> list_typeItem = this.dicSampleTypeDao
							.selectDicSampleTypeItemByid(scp.getDicSampleType().getId(), pid);
					for (DicSampleTypeItem d : list_typeItem) {
						if (scp.getNextFlowId() == null || "".equals(scp.getNextFlowId())) {

							scp.setNextFlowId(d.getDnextId());
							scp.setNextFlow(d.getDnextName());
						}
					}
				}
			}
			sampleReceiveDao.saveOrUpdate(scp);
		}
		// 查询外部样本号
		List<String> wb_list = this.sampleReceiveDao.getIdCard(sc.getId());
		for (String str : wb_list) {
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			String stime = format.format(date);
			String fzCode = codingRuleService.get("SampleReceiveItem", stime, 000, 3, null, "name");
			// 根据外部样本号查询
			List<SampleReceiveItem> list2 = this.sampleReceiveDao.setSampleReceiveItemBywbh(str, sc.getId());
			for (SampleReceiveItem s : list2) {
				if (s.getName() == null || "".equals(s.getName())) {
					s.setName(fzCode);
				}
			}
		}

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleReceiveItem scp = sampleReceiveDao.get(SampleReceiveItem.class, id);

			String sc = "样本接收：" + scp.getProductName() + "：条形码：" + scp.getBarCode() + "的数据被删除";
			if (ids != null && !"".equals(ids)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getSampleReceive().getId());
				li.setClassName("SampleReceive");
				li.setState("2");
				li.setStateName("数据删除");
				li.setModifyContent(sc);
				commonDAO.saveOrUpdate(li);
			}
			sampleReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleReceiveDao.saveOrUpdate(sc);

			String jsonSysStr = "";
			jsonSysStr = (String) jsonMap.get("sampleSystemItem");
			if (jsonSysStr != null && !jsonSysStr.equals("{}") && !jsonSysStr.equals("")) {
				saveSampleSystem(sc, jsonSysStr);
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 打包体系
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleSystem(SampleReceive sc, String itemDataJson) throws Exception {
		List<SampleSystemItem> saveItems = new ArrayList<SampleSystemItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleSystemItem scp = new SampleSystemItem();
			// 将map信息读入实体类
			scp = (SampleSystemItem) sampleReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleReceive(sc);

			saveItems.add(scp);
		}
		sampleReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除体系
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleSystemItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleSystemItem scp = sampleReceiveDao.get(SampleSystemItem.class, id);
			sampleReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public synchronized void changeState(String applicationTypeActionId, String id) throws Exception {

		Thread.sleep(2000);

		SampleReceive sr = sampleReceiveDao.get(SampleReceive.class, id);

		if (sr.getState().equals("1")) {

		} else {
			sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			sr.setConfirmUser(sr.getProductionUser());
			sr.setConfirmDate(new Date());

			SampleOrder soo = null;
			if (sr.getSampleOrder() != null && !"".equals(sr.getSampleOrder())) {
				soo = commonDAO.get(SampleOrder.class, sr.getSampleOrder());
				if (soo != null) {
					soo.setFeedBackTime(sr.getFeedBackTime());
					soo.setBatchState("1");
					soo.setBatchStateName("样本已接收");

					soo.setState("1");
					soo.setStateName("实际订单");
					commonDAO.merge(soo);

				}
//				if (!"".equals(soo.getRound())) {
//					int i = Integer.parseInt(soo.getRound());
//					System.out.println(i);
//					List<SampleFutureFind> sfff = sampleOrderDao.findId();
//					for (SampleFutureFind sfFind : sfff) {
//						int ii = Integer.parseInt(sfFind.getRound());
//						if (soo.getFiltrateCode().equals(sfFind.getFiltrateCode()) && i == ii) {
//							
//						}
//					}
//				}

			}

			if (sr.getState() != null && !sr.getState().equals("") && sr.getState().equals("1")) {
				List<SampleReceiveItem> sriList = sampleReceiveDao.selectItemList(sr.getId());
				if (!sriList.isEmpty()) {
					if (soo != null) {
						int i = Integer.parseInt(soo.getRound());
						SampleFutureFind sff = sampleReceiveDao
								.selectSampleOrderByFiltrateCodeAndRound(soo.getFiltrateCode(), i);// 筛选号和轮次
						if (sff != null) {
							Date sd = sriList.get(0).getSamplingDate();
							SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
							if (sd != null) {
								if(sDateFormat.format(sd).equals(sff.getSampleTime())) {
									sff.setSampleTime(sDateFormat.format(sd));
									sff.setState("2");
									sff.setStateName("样本已接收");
									sff.setTimeState("2");
									sff.setTimeShowState("1");
									if(soo!=null) {
										sff.setDingDanCode(soo.getId());
									}
									commonDAO.merge(sff);
								}else {
									SampleFutureFind sff2 = new SampleFutureFind();
									sff2 = (SampleFutureFind) sff.clone();
									sff2.setId(null);
									sff2.setSampleTime(sDateFormat.format(sd));
									sff2.setState("2");
									sff2.setStateName("样本已接收");
									sff2.setDingDanCode(soo.getId());
									sff2.setTimeState("1");
									sff2.setTimeShowState("1");
									commonDAO.merge(sff2);
									
									
									
									sff.setTimeShowState("0");
									sff.setState("2");
									sff.setStateName("样本已接收");
									commonDAO.merge(sff);
									
								}
							}
							//创建20轮预排信息
						} else if (i == 1 && soo.getStateName().equals("实际订单")) {
							List<SampleReceiveItem> sriItem = commonService.get(SampleReceiveItem.class,
									"sampleReceive.id", sr.getId());
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String cxDate = sdf.format(sriItem.get(0).getSamplingDate());
							checkU1(soo.getFiltrateCode(), cxDate,soo.getId(),soo.getRound());
						}

					}

					for (SampleReceiveItem sri : sriList) {
						SampleOrderItem si = sampleReceiveDao.selectSampleOrderItemByCode(sri.getBarCode(),
								sri.getOrderCode());
						if (si != null) {
							si.setReceiveDate(new Date());
							si.setReceiveUser(user);
							si.setState("1");
							si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RECEIVE_COMPLETE_NAME);
						}
						// 得到追加订单的类型给主数据changeType赋值
						String orderCode = sri.getOrderCode();// 订单
						String changeType = null;
						SampleOrderChangeItem changeItem = null;
						if (orderCode != null) {
							changeItem = sampleOrderChangeDao.findChangeItemByOrderCode(orderCode);
							if (changeItem != null) {
								SampleOrderChange change = changeItem.getSampleOrderChange();
								changeType = change.getChangeType();
							}
						}

						// // 样本主数据
						// SampleInfo sampleInfo = new SampleInfo();
						//
						// if ("取消申请单".equals(changeType)) {
						// sampleInfo.setChangeType("0");
						// }
						// if ("修改申请单".equals(changeType)) {
						// sampleInfo.setChangeType("1");
						// sampleInfo.setOrderNum(changeItem.getOldId());
						// }
						// if ("追加申请单".equals(changeType)) {
						// sampleInfo.setChangeType("2");
						// sampleInfo.setOrderNum(changeItem.getOldId() + "," +
						// changeItem.getUpdateId());
						// }
						if (sri.getMethod() != null && !sri.getMethod().equals("")) {
							if (sri.getMethod().equals("1") || sri.getMethod().equals("2")) {// 合格或让步放行
								if (sri.getOrderCode() != null && !sri.getOrderCode().equals("")) {
									SampleOrder so = sampleReceiveDao.get(SampleOrder.class, sri.getOrderCode());
									if (so == null) {
										so = new SampleOrder();
										if (sri.getOrderCode() != null && !sri.getOrderCode().equals("")) {
											so.setId(sri.getOrderCode());
										}
										if (sri.getProductId() != null && !sri.getProductId().equals("")) {

											so.setProductId(sri.getProductId());
											so.setProductName(sri.getProductName());
										}

										if (sri.getPatientName() != null && !sri.getPatientName().equals("")) {

											so.setName(sri.getPatientName());
										}

										if (sri.getInspectDate() != null && !sri.getInspectDate().equals("")) {

											so.setInspectDate(sri.getInspectDate());
										}

										if (sri.getSellPerson() != null) {

											so.setCommissioner(sri.getSellPerson());
										}
										so.setState("3");
										so.setStateName(
												com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
										so.setCreateDate(new Date());
										so.setCreateUser((User) ServletActionContext.getRequest().getSession()
												.getAttribute(SystemConstants.USER_SESSION_KEY));
										sampleReceiveDao.saveOrUpdate(so);

									}
									int maxTestDate = 0;
									if (sri.getProductId() != null && !"".equals(sri.getProductId())) {
										String[] sequIds = sri.getProductId().split(",");
										List<Integer> testLife = new ArrayList<Integer>();
										for (int i = 0; i < sequIds.length; i++) {
											Product cp = commonService.get(Product.class, sequIds[i]);
											if (cp.getTestTime() != null && !cp.getTestTime().equals("")) {
												testLife.add(Integer.valueOf(cp.getTestTime()));
											}
										}
										if (testLife.size() > 0) {
											maxTestDate = testLife.get(0);
											for (int j = 0; j < testLife.size(); j++) {
												int preTestDate = testLife.get(j);
												if (preTestDate > maxTestDate) {
													maxTestDate = preTestDate;
												}
											}
											Calendar calendar = Calendar.getInstance();
											calendar.setTime(sr.getAcceptDate());
											Calendar ca = Calendar.getInstance();
											ca = WorkDay.addDateByWorkDay(calendar, maxTestDate);
											Date a = (Date) ca.getTime();
											so.setReportDate(a);
										}
										commonDAO.merge(so);
									}

								}
								// 合格数据填加到样本主数据中
								// sampleInfo.setScopeId(sr.getScopeId());
								// sampleInfo.setScopeName(sr.getScopeName());
								// sampleInfo.setCode(sri.getSampleCode());
								// sampleInfo.setName(sri.getName());
								// sampleInfo.setIdCard(sri.getIdCard());// 外部样本编号
								// sampleInfo.setCardNumber(sri.getLabCode());// 实际外部号
								// sampleInfo.setUnit(sri.getUnit());
								// sampleInfo.setPatientName(sri.getPatientName());
								// sampleInfo.setProductId(sri.getProductId());
								// sampleInfo.setProductName(sri.getProductName());
								// sampleInfo.setSpecies(sri.getSpecies());
								// sampleInfo.setSellPerson(sri.getSellPerson());
								// sampleInfo.setState("1");
								// // sampleInfo.setOrderNum(sri.getOrderCode());
								// sampleInfo.setCrmCustomer(sr.getCrmCustomer());
								// sampleInfo.setCrmDoctor(sr.getCrmDoctor());
								// sampleInfo.setReceiveDate(sri.getAcceptDate());
								// if ("1".equals(sr.getType())) {
								// sampleInfo.setSampleStyle("1");// 科研
								// } else if ("3".equals(sr.getType())) {
								// sampleInfo.setSampleStyle("2");// 临床
								// }
								// SampleOrder so = null;
								// if (sri.getOrderCode() != null) {
								// so = sampleReceiveDao.get(SampleOrder.class, sri.getOrderCode());
								// if (so != null) {
								// sampleInfo.setSampleOrder(so);
								// sampleInfo.setReportDate(DateUtil.format(so.getReportDate()));
								// sampleInfo.setCrmCustomer(so.getCrmCustomer());
								// sampleInfo.setCrmDoctor(so.getCrmDoctor());
								// }
								//
								// }
								// sampleInfo.setReceiveDate(sri.getAcceptDate());
								// sampleInfo.setSampleType(sri.getDicSampleType());
								// DicSampleType t = commonDAO.get(DicSampleType.class,
								// sri.getDicSampleType().getId());
								// sampleInfo.setSampleType2(t.getName());
								// sampleInfo.setSampleNum(sri.getSampleNum());
								// sampleInfo.setProject(sr.getProject());
								// sampleInfo.setUnit(sri.getUnit());
								// sampleInfo.setSampleReceive(sr);
								// sampleInfo.setSamplingDate(sri.getSamplingDate());
								// // sampleInfo.setInspectDate(sri.getInspectDate());
								// sampleInfo.setDicTypeName(sri.getDicTypeName());
								// sampleInfo.setNote(sri.getNote());
								// int maxTestDate = 0;
								// if (sri.getProductId() != null && !"".equals(sri.getProductId())) {
								// String[] sequIds = sri.getProductId().split(",");
								// List<Integer> testLife = new ArrayList<Integer>();
								// for (int i = 0; i < sequIds.length; i++) {
								// Product cp = commonService.get(Product.class, sequIds[i]);
								// if (cp.getTestTime() != null && !cp.getTestTime().equals("")) {
								// testLife.add(Integer.valueOf(cp.getTestTime()));
								// }
								// }
								// if (testLife.size() > 0) {
								// maxTestDate = testLife.get(0);
								// for (int j = 0; j < testLife.size(); j++) {
								// int preTestDate = testLife.get(j);
								// if (preTestDate > maxTestDate) {
								// maxTestDate = preTestDate;
								// }
								// }
								// Calendar calendar = Calendar.getInstance();
								// calendar.setTime(sr.getAcceptDate());
								// Calendar ca = Calendar.getInstance();
								// ca = WorkDay.addDateByWorkDay(calendar, maxTestDate);
								// Date a = (Date) ca.getTime();
								// sampleInfo.setReportDate(DateUtil.format(a));
								// }
								// }
								// sampleReceiveDao.saveOrUpdate(sampleInfo);
								// 样本订单表
								SampleOrderInfoItem soii = new SampleOrderInfoItem();
								soii.setCode(sri.getOrderCode());
								soii.setSampleCode(sri.getSampleCode());
								sampleReceiveDao.saveOrUpdate(soii);

								// 样本订单表
								List<SampleOrderItem> sois = commonService.get(SampleOrderItem.class, "slideCode",
										sri.getBarCode());
								if (sois.size() > 0) {
									for (SampleOrderItem soi : sois) {
										soi.setReceiveDate(sr.getAcceptDate());
										soi.setReceiveUser(sr.getAcceptUser());
										soi.setReleaseResults(sri.getMethod());
										soi.setSamplingDate(sri.getSamplingDate());
										sampleReceiveDao.saveOrUpdate(soi);
									}
								}

								next(sr, sri);// , so, sampleInfo);

							} else {// 不合格或备用空管
								SampleAbnormal sa = new SampleAbnormal();
								sampleInputService.copy(sa, sri);
								sa.setState("1");
								sa.setItemId(sri.getId());
								sa.setOrderId(sr.getId());
								sampleReceiveDao.saveOrUpdate(sa);

								// 样本订单表
								List<SampleOrderItem> sois = commonService.get(SampleOrderItem.class, "slideCode",
										sri.getBarCode());
								if (sois.size() > 0) {
									for (SampleOrderItem soi : sois) {
										soi.setReceiveDate(sr.getAcceptDate());
										soi.setReceiveUser(sr.getAcceptUser());
										soi.setReleaseResults(sri.getMethod());
										soi.setSamplingDate(sri.getSamplingDate());
										sampleReceiveDao.saveOrUpdate(soi);
									}
								}
							}
						}

						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						sampleStateService.saveSampleState1(commonDAO.get(SampleOrder.class, sri.getOrderCode()),
								sri.getSampleCode(), sri.getSampleCode(), sri.getProductId(), sri.getProductName(), "",
								format.format(sr.getAcceptDate()), format.format(new Date()), "SampleReceive", "开箱检验",
								(User) ServletActionContext.getRequest().getSession()
										.getAttribute(SystemConstants.USER_SESSION_KEY),
								sr.getId(), sri.getNextFlow(), sri.getMethod(), null, null, null, null, null, null,
								null, null);
					}
					User u = (User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "YS001");
					for (UserGroupUser QAugu : QAugus) {
						SysRemind srz = new SysRemind();
						srz.setId(null);
						// 提醒类型
						srz.setType(null);
						// 事件标题
						srz.setTitle("样本接收提醒");
						// 发起人（系统为SYSTEM）
						srz.setRemindUser(u.getName());
						// 发起时间
						srz.setStartDate(new Date());
						// 查阅提醒时间
						srz.setReadDate(new Date());
//						String zt = "";
//						if("1".equals(sri.getMethod())){
//							zt = "合格";
//						}else if("0".equals(sri.getMethod())){
//							zt = "不合格";
//						}else if("2".equals(sri.getMethod())){
//							zt = "让步放行";
//						}else if("3".equals(sri.getMethod())){
//							zt = "备用空管";
//						}else{
//							
//						}
						// 提醒内容
						srz.setContent("病人：" + sr.getCcoi() + "的样本，审核完成。");
						// 状态 0.草稿 1.已阅
						srz.setState("0");
						// 提醒的用户
						srz.setHandleUser(QAugu.getUser());
						// 表单id
						srz.setContentId(sr.getId());
						srz.setTableId("SampleReceive");
						commonDAO.saveOrUpdate(srz);
					}

					if (sr.getApprovalUser() != null) {
						User uu1 = commonDAO.get(User.class, sr.getApprovalUser().getId());
						if (uu1 != null) {
							// 通知QA
							SysRemind srz1 = new SysRemind();
							srz1.setId(null);
							// 提醒类型
							srz1.setType(null);
							// 事件标题
							srz1.setTitle("样本接收提醒");
							// 发起人（系统为SYSTEM）
							srz1.setRemindUser(u.getName());
							// 发起时间
							srz1.setStartDate(new Date());
							// 查阅提醒时间
							srz1.setReadDate(new Date());
							// 提醒内容
							srz1.setContent("病人：" + sr.getCcoi() + "的样本，审核完成。");
							// 状态 0.草稿 1.已阅
							srz1.setState("0");
							// 提醒的用户
							srz1.setHandleUser(uu1);
							// 表单id
							srz1.setContentId(sr.getId());
							srz1.setTableId("SampleReceive");
							commonDAO.saveOrUpdate(srz1);
						}
					}

					if (sr.getProductionUser() != null) {
						User uu2 = commonDAO.get(User.class, sr.getProductionUser().getId());
						if (uu2 != null) {
							// 通知生产
							SysRemind srz2 = new SysRemind();
							srz2.setId(null);
							// 提醒类型
							srz2.setType(null);
							// 事件标题
							srz2.setTitle("样本接收提醒");
							// 发起人（系统为SYSTEM）
							srz2.setRemindUser(u.getName());
							// 发起时间
							srz2.setStartDate(new Date());
							// 查阅提醒时间
							srz2.setReadDate(new Date());
							// 提醒内容
							srz2.setContent("病人：" + sr.getCcoi() + "的样本，审核完成。");
							// 状态 0.草稿 1.已阅
							srz2.setState("0");
							// 提醒的用户
							srz2.setHandleUser(uu2);
							// 表单id
							srz2.setContentId(sr.getId());
							srz2.setTableId("SampleReceive");
							commonDAO.saveOrUpdate(srz2);
						}
					}
				}
			}
			sampleReceiveDao.saveOrUpdate(sr);
			if (sr.getSampleInfoMain() != null)
				sr.getSampleInfoMain().setState("2");
		}
	}

	// 下一步流向方法
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void next(SampleReceive sr, SampleReceiveItem sri) throws Exception {// , SampleOrder so, SampleInfo
																				// sampleInfo) throws Exception {
		List<SampleInfo> sis = commonService.get(SampleInfo.class, "code", sri.getOrderCode());
		SampleInfo sampleInfo = new SampleInfo();
		if (sis.size() > 0) {
			sampleInfo = sis.get(0);
		} else {
			sampleInfo.setId(null);
			SampleOrder so = commonDAO.get(SampleOrder.class, sri.getOrderCode());
			if (so != null) {
				sampleInfo.setSampleOrder(so);
			}
			sampleInfo.setCode(sri.getOrderCode());
			commonDAO.saveOrUpdate(sampleInfo);
		}
		if (sri.getNextFlowId().equals("0009")) {// 样本入库
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SampleInItemTemp st = new SampleInItemTemp();
			st.setSampleState("1");
			st.setScopeId(sr.getScopeId());
			st.setProductId(sri.getProductId());
			st.setProductName(sri.getProductName());
			st.setBarCode(sri.getBarCode());
			if (sri.getSamplingDate() != null) {
				st.setSamplingDate(sdf.format(sri.getSamplingDate()));
			}
			st.setScopeName(sr.getScopeName());
			st.setCode(sri.getSampleCode());
			st.setSampleCode(sri.getSampleCode());
			st.setConcentration(sri.getConcentration());
			st.setVolume(sri.getVolume());
			st.setSamplingDate(sri.getInspectDate());
			if (sri.getSampleNum() != null && !"".equals(sri.getSampleNum())) {
				st.setSumTotal(Double.parseDouble(sri.getSampleNum()));
			}
			DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
			if (t != null) {
				st.setSampleType(t.getName());
			}
			st.setDicSampleType(sri.getDicSampleType());
			st.setState("1");
			st.setPatientName(sri.getPatientName());
			st.setDataTraffic(sri.getDataTraffic());
			st.setSampleTypeId(sri.getDicSampleType().getId());
			st.setInfoFrom("SampleInfo");
			st.setSampleInfo(sampleInfo);
			st.setGroupNo(sri.getName());// 分组号
			if ("1".equals(sr.getType())) {
				st.setSampleStyle("1");// 科研
			} else if ("3".equals(sr.getType())) {
				st.setSampleStyle("2");// 临床
			}
			if (sri.getOrderCode() != null && !"".equals(sri.getOrderCode())) {
				SampleOrder soo = commonDAO.get(SampleOrder.class, sri.getOrderCode());
				st.setSampleOrder(soo);
			}
			sampleReceiveDao.saveOrUpdate(st);
		} else if (sri.getNextFlowId().equals("0016")) {// 样本处理
			PlasmaTaskTemp d = new PlasmaTaskTemp();
			sri.setState("1");
			d.setState("1");
			d.setScopeId(sr.getScopeId());
			d.setScopeName(sr.getScopeName());
			d.setConcentration(sri.getConcentration());
			d.setVolume(sri.getVolume());
			d.setOrderId(sr.getId());
			d.setProductId(sri.getProductId());
			d.setProductName(sri.getProductName());
			d.setSampleCode(sri.getSampleCode());
			d.setCode(sri.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
			d.setSampleType(t.getName());
			d.setSampleInfo(sampleInfo);
			sampleReceiveDao.saveOrUpdate(d);
		} else if (sri.getNextFlowId().equals("0017")) {// 核酸提取
			DnaTaskTemp d = new DnaTaskTemp();
			sri.setState("1");
			d.setState("1");
			d.setConcentration(sri.getConcentration());
			d.setVolume(sri.getVolume());
			d.setOrderId(sr.getId());
			d.setScopeId(sr.getScopeId());
			d.setScopeName(sr.getScopeName());
			d.setProductId(sri.getProductId());
			d.setProductName(sri.getProductName());
			d.setSampleCode(sri.getSampleCode());
			d.setCode(sri.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
			d.setSampleType(t.getName());
			d.setSampleInfo(sampleInfo);
			sampleReceiveDao.saveOrUpdate(d);
		} else if (sri.getNextFlowId().equals("0018")) {// 文库构建
			WkTaskTemp d = new WkTaskTemp();
			sri.setState("1");
			d.setState("1");
			d.setScopeId(sr.getScopeId());
			d.setScopeName(sr.getScopeName());
			d.setProductId(sri.getProductId());
			d.setProductName(sri.getProductName());
			d.setConcentration(sri.getConcentration());
			d.setVolume(sri.getVolume());
			d.setOrderId(sr.getId());
			d.setSampleCode(sri.getSampleCode());
			d.setCode(sri.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
			d.setSampleType(t.getName());
			d.setSampleInfo(sampleInfo);
			sampleReceiveDao.saveOrUpdate(d);
		} else if (sri.getNextFlowId().equals("0073")) {// 细胞生产
			CellPassageTemp d = new CellPassageTemp();
			sri.setState("1");

			d.setId(null);
			d.setSampleCode(sri.getSampleCode());
			d.setCode(sri.getSampleCode());
			d.setPatientName(sri.getPatientName());
			d.setNote(sri.getNote());
			d.setState("1");
			d.setProductId(sri.getProductId());
			d.setProductName(sri.getProductName());
			d.setOrderId(sr.getId());
			d.setOrderCode(sri.getOrderCode());
			d.setSampleNum(sri.getSampleNum());
			DicSampleType t = new DicSampleType();
			if (!"".equals(sri.getDicSampleType()) && sri.getDicSampleType() != null) {
				t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
			}
			if (t != null) {
				d.setSampleType(t.getName());
			}
			d.setSampleInfo(sampleInfo);
			d.setScopeId(sr.getScopeId());
			d.setScopeName(sr.getScopeName());
			SampleOrder sor = commonDAO.get(SampleOrder.class, sri.getOrderCode());
			if (sor != null) {
				d.setSampleOrder(sor);
				d.setBatch(sor.getBarcode());
			}
			if (sri.getBatch() != null && !"".equals(sri.getBatch())
					&& (d.getBatch() == null || "".equals(d.getBatch()))) {
				d.setBatch(sri.getBatch());
			}
			sampleReceiveDao.saveOrUpdate(d);
		} else {
			// 得到下一步流向的相关表单
			List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(sri.getNextFlowId());
			for (NextFlow n : list_nextFlow) {
				Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
				sri.setState("1");
				DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
				sri.setSampleType(t.getName());
				sri.setSampleInfo(sampleInfo);
				sri.setScopeId(sr.getScopeId());
				sri.setScopeName(sr.getScopeName());
				// if (so != null)
				// sri.setReportDate(so.getReportDate());
				try {
					BeanUtils.getDeclaredField(o, "code");
					BeanUtils.setFieldValue(o, "code", sri.getSampleCode());

				} catch (NoSuchFieldException e) {
				}
				if ("1".equals(sr.getType())) {
					sri.setSampleStyle("1");// 科研
				} else if ("3".equals(sr.getType())) {
					sri.setSampleStyle("2");// 临床
				}
				sampleInputService.copy(o, sri);
			}
		}

	}

	// 作废
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void stateNo(String id) throws Exception {

		SampleReceive sr = sampleReceiveDao.get(SampleReceive.class, id);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sr.setConfirmUser(user);
		sr.setConfirmDate(new Date());

		List<SampleReceiveItem> sriList = sampleReceiveDao.selectItemList(sr.getId());
		for (SampleReceiveItem sri : sriList) {
			NextFlow list_nextFlow = nextFlowDao.seletNextFlowById(sri.getNextFlowId()).get(0);
			Object o = list_nextFlow.getApplicationTypeTable().getId();
			Object obj = sampleReceiveDao.selModelByCode(o, sri.getSampleCode());
			if (obj != null) {
				sampleReceiveDao.delete(obj);
			}
		}

	}

	// 根据工作流状态查询样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleReceive(SampleReceive sc, String itemDataJson) throws Exception {
		List<SampleReceiveItem> saveItems = new ArrayList<SampleReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleReceiveItem scp = new SampleReceiveItem();
			// 将map信息读入实体类
			scp = (SampleReceiveItem) sampleReceiveDao.selectReceiveList(sc.getStateName());
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleReceive(sc);
			saveItems.add(scp);
		}
		sampleReceiveDao.saveOrUpdateAll(saveItems);
	}

	// 根据样本号查询
	public Map<String, Object> selBySampleCode(String sampleCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] scode = sampleCode.split(",");
		// for (int i = 0; i < scode.length; i++) {
		SampleReceiveItem s = sampleReceiveDao.selectItemByCode(scode[0]);
		if (s.getSampleReceive().getCrmCustomer() != null) {// 科研
			// map.put("sellPerson", s.getSellPerson());// 销售
			map.put("customer", s.getSampleReceive().getCrmCustomer());// 客户(客户单位)
		} else {// 临床
			SampleOrder so = sampleReceiveDao.get(SampleOrder.class, s.getOrderCode());
			// map.put("sellPerson", s.getSellPerson());// 销售
			map.put("customer", so.getCrmCustomer());// 客户(客户单位)
		}
		// }
		return map;
	}

	public void fenzu(String[] ids) throws Exception {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		String stime = format.format(date);
		String fzCode = codingRuleService.get("SampleReceiveItem", stime, 000, 3, null, "name");
		for (int i = 0; i < ids.length; i++) {

			SampleReceiveItem s = this.commonDAO.get(SampleReceiveItem.class, ids[i]);
			s.setName(fzCode);
			this.commonDAO.saveOrUpdate(s);
		}
	}

	/**
	 * 根据条码号查询订单明细信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> getSampleOrderItemMapList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sampleReceiveDao.getSampleOrderItemMapList(code);
		List<SampleOrderItem> list = (List<SampleOrderItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SampleOrderItem si : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", si.getId());
				map.put("name", si.getName());
				map.put("gender", si.getGender());
				map.put("sampleCode", si.getSampleCode());
				map.put("slideCode", si.getSlideCode());
				if (si.getSampleType() != null) {
					map.put("sampleTypeId", si.getSampleType().getId());
					map.put("sampleTypeName", si.getSampleType().getName());
				} else {
					map.put("sampleTypeId", "");
					map.put("sampleTypeName", "");
				}

				map.put("orderCode", si.getSampleOrder().getId());
				Format format = new SimpleDateFormat("yyyy-MM-dd");
				if (si.getSamplingDate() == null) {
					map.put("samplingDate", "");
				} else {
					String samplingDate = format.format(si.getSamplingDate());
					map.put("samplingDate", samplingDate);
				}

				if (si.getSampleOrder() != null) {
					map.put("productId", si.getSampleOrder().getProductId());
					map.put("productName", si.getSampleOrder().getProductName());
					if (si.getSampleOrder().getCrmCustomer() != null) {
						map.put("hosId", si.getSampleOrder().getCrmCustomer().getId());
						map.put("hosName", si.getSampleOrder().getCrmCustomer().getName());
					} else {
						map.put("hosId", "");
						map.put("hosName", "");
					}
				} else {
					map.put("productId", "");
					map.put("productName", "");
				}

				map.put("note", si.getNote());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据条码号查询订单明细信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<SampleOrderItem> getSampleOrderItemList(String code) {
		return sampleReceiveDao.getSampleOrderItemList(code);
	}

	public String selDoctor(String gid) {
		return sampleReceiveDao.findDoctor(gid);
	}

	public Map<String, Object> showSampleReceiveTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleReceiveDao.showSampleReceiveTableJson(start, length, query, col, sort);
	}

	public Map<String, Object> showSampleFutureFindTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleReceiveDao.showSampleFutureFindTableJson(start, length, query, col, sort);
	}

	public Map<String, Object> showSampleReceiveItemListJson(String scId, Integer start, Integer length, String query,
			String col, String sort) {
		return sampleReceiveDao.showSampleReceiveItemListJson(scId, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, String> scanCodeInsert(String id, String type, String slideCode, String sj) throws Exception {
		DateFormat formatsf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat formatsfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, String> map = new HashMap<String, String>();
		String prompt = "";
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);

		List<SampleOrderItem> itemList1 = null;
		itemList1 = sampleReceiveDao.scanCodeInsert(id, type, slideCode);
		if (!itemList1.isEmpty() && itemList1.size() > 0) {
			String slideCode1 = "样本接收：条形码" + slideCode + " 已扫描";
			if (slideCode1 != null && !"".equals(slideCode1)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(id);
				li.setClassName("SampleReceive");
				li.setUserId(u.getId());
				if ("NEW".equals(id) || "".equals(id)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				li.setModifyContent(slideCode1);
				commonDAO.saveOrUpdate(li);
			}
		} else {
			itemList1 = sampleReceiveDao.scanCodeInsertBybarcode(id, type, slideCode);
			String slideCode1 = "样本接收：生产批号" + slideCode + " 已扫描";
			if (slideCode1 != null && !"".equals(slideCode1)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(id);
				li.setClassName("SampleReceive");
				li.setUserId(u.getId());
				li.setModifyContent(slideCode1);
				if ("NEW".equals(id) || "".equals(id)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}

				commonDAO.saveOrUpdate(li);
			}
		}

		SampleOrderItem soi = null;

//		if (itemList.size() > 0) {
//			soi = itemList.get(0);
//		}
		List<SampleOrderItem> itemList = new ArrayList<SampleOrderItem>();
		if (itemList1.size() > 0) {
			soi = itemList1.get(0);
			itemList = commonService.get(SampleOrderItem.class, "sampleOrder.id", soi.getSampleOrder().getId());
		}
		SampleReceive sr = null;
		if (itemList.size() > 0) {
//			String code = soi.getSlideCode();
			// String sampleOrderId = soi.getSampleOrder().getBarcode();
			// String maxSampleCode = findMaxSampleCodeByOrderCode(sampleOrderId);
			// if (maxSampleCode != null) {
			// int index = maxSampleCode.lastIndexOf("-");
			// String substring = maxSampleCode.substring(index + 1,
			// maxSampleCode.length());
			// int vv = Integer.parseInt(substring) + 1;
			// if (vv <= 9) {
			// code = sampleOrderId + "-" + "0" + vv;
			// } else {
			// code = sampleOrderId + "-" + vv;
			// }
			// } else {
			// code = sampleOrderId + "-" + "01";
			// }
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				sr = new SampleReceive();
				String modelName = "SampleReceive";
				String markCode = "KXJY";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				sr.setId(autoID);
				sr.setType(type);
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sr.setState("3");
				sr.setStateName("新建");
				sr.setSampleOrder(soi.getSampleOrder().getId());
				sr.setStartBackTime(soi.getSampleOrder().getStartBackTime());
				sr.setFeedBackTime(soi.getSampleOrder().getFeedBackTime());
				sr.setCcoi(soi.getSampleOrder().getCcoi());
				sr.setAcceptUser(u);
				// 订单批次号
				String barco = soi.getSampleOrder().getBarcode();
				sr.setBarcode(barco);

				if (sj != null && !"".equals(sj)) {
					sr.setAcceptDate(formatsf.parse(sj));
					sr.setCreateDate(format.parse(sj));
				}
				sampleReceiveDao.saveOrUpdate(sr);
				// 获取条形码对应的采血管数
//				for (int j = 0; j < itemList.size(); j++) {
				for (SampleOrderItem soii : itemList) {
					SampleReceiveItem sri = new SampleReceiveItem();
					/*
					 * String mark = ""; if (itemList.get(j).getSampleType() == null) { mark =
					 * DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd");
					 * 
					 * } else { DicSampleType d = commonDAO.get(DicSampleType.class,
					 * itemList.get(j).getSampleType().getId()); mark = d.getCode() +
					 * DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd"); } String code
					 * = codingRuleService.getSampleCode("SampleReceiveItem", mark, 0000, 4, null);
					 */

//					sri.setSampleCode(code);
//					sri.setDicSampleType(itemList.get(j).getSampleType());
//					sri.setBarCode(itemList.get(j).getSlideCode());
//					sri.setGender(itemList.get(j).getGender());
//					sri.setAcceptDate(sr.getAcceptDate());
//					sri.setPatientName(itemList.get(j).getName());
//					sri.setSamplingDate(itemList.get(j).getSamplingDate());
//					if (itemList.get(j).getSampleOrder() != null && !"".equals(itemList.get(j).getSampleOrder())) {
//						sri.setOrderCode(itemList.get(j).getSampleOrder().getId());
//						sri.setProductId(itemList.get(j).getSampleOrder().getProductId());
//						sri.setProductName(itemList.get(j).getSampleOrder().getProductName());
//						sri.setEMRId(itemList.get(j).getSampleOrder().getMedicalNumber());
//						sri.setFamilyId(itemList.get(j).getSampleOrder().getFamilyId());
//						sri.setBatch(itemList.get(j).getSampleOrder().getBarcode());
//					}
//					sri.setSampleReceive(sr);
//					sri.setSampleNum("1");
//					sri.setLabCode(itemList.get(j).getSampleCode());
					sri.setSampleCode(soii.getSlideCode());
					sri.setDicSampleType(soii.getSampleType());
					sri.setBarCode(soii.getSlideCode());
					sri.setGender(soii.getGender());
					sri.setAcceptDate(sr.getAcceptDate());
					sri.setPatientName(soii.getName());
					sri.setAbbreviation(soii.getSampleOrder().getAbbreviation()); // 血样颜色检验:0:正常
					sri.setBloodColourTest("0");
					sri.setCoagulationTest("否");
					sri.setSealingBloodVessels("否");
					sri.setBloodVesselTime("是");
					sri.setStainedWith("是");
					// 1:是(是否正常)
					sri.setIsGood("1");
					// 1:合格(结果)
					sri.setBloodVolume("9");
					if (soii.getSampleOrder() != null && !"".equals(soii.getSampleOrder())) {
						sri.setOrderCode(soii.getSampleOrder().getId());
						sri.setProductId(soii.getSampleOrder().getProductId());
						sri.setProductName(soii.getSampleOrder().getProductName());
						sri.setEMRId(soii.getSampleOrder().getMedicalNumber());
						sri.setFamilyId(soii.getSampleOrder().getFamilyId());
						sri.setBatch(soii.getSampleOrder().getBarcode());
					}
					sri.setSampleReceive(sr);
					sri.setSampleNum("1");
					sri.setLabCode(soii.getSlideCode());
					if (slideCode.equals(soii.getSlideCode())) {
						sri.setReceiveState("1");
						if (sri.getReceiveState().equals("1")) {
							sri.setMethod("1");
						} else {
							sri.setMethod("3");
						}
					} else {
						sri.setReceiveState("0");
						if (sri.getReceiveState().equals("0")) {
							sri.setMethod("3");
						} else {
							sri.setMethod("1");
						}
					}
					sri.setSamplingDate(soii.getSamplingDate());
					if (sri.getSamplingDate() != null) {

					} else {
						if (sr.getAcceptDate() != null) {
							sri.setSamplingDate(formatsf.parse(formatsf.format(sr.getAcceptDate())));
						} else {
							sri.setSamplingDate(formatsf.parse(formatsf.format(new Date())));
						}
					}

					sri.setBatch(soii.getSampleOrder().getBarcode());

					List<WorkOrderItem> list = workOrderDao.selectNextFlow1("SampleReceive", sri.getProductId());
					for (WorkOrderItem w : list) {
						sri.setNextFlow(w.getDnextName());
						sri.setNextFlowId(w.getDnextId());
					}

//					if (sri.getProductId() != null && !scp.getProductId().equals("")) {
//						String[] productId = scp.getProductId().split(",");
//						String pid = "";
//						for (int i = 0; i < productId.length; i++) {
//							pid += "and productId like '%" + productId[i] + "%' ";
//						}
//						List<DicSampleTypeItem> list_typeItem = this.dicSampleTypeDao
//								.selectDicSampleTypeItemByid(scp.getDicSampleType().getId(), pid);
//						for (DicSampleTypeItem d : list_typeItem) {
//							if (scp.getNextFlowId() == null || "".equals(scp.getNextFlowId())) {
//
//								scp.setNextFlowId(d.getDnextId());
//								scp.setNextFlow(d.getDnextName());
//							}
//						}
//					}

					sampleReceiveDao.saveOrUpdate(sri);
				}
			} else {
				sr = commonDAO.get(SampleReceive.class, id);
				sr.setCcoi(soi.getSampleOrder().getCcoi());
				sr.setSampleOrder(soi.getSampleOrder().getId());
				// 订单批次号
				String barco = soi.getSampleOrder().getBarcode();
				sr.setBarcode(barco);
				for (SampleOrderItem soii : itemList) {
					SampleReceiveItem sri = new SampleReceiveItem();
					/*
					 * String mark = ""; if (soi.getSampleType() == null) { mark =
					 * DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd");
					 * 
					 * } else { DicSampleType d = commonDAO.get(DicSampleType.class,
					 * soi.getSampleType().getId()); mark = d.getCode() +
					 * DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd"); }
					 * 
					 * String code = codingRuleService.getSampleCode("SampleReceiveItem", mark,
					 * 0000, 4, null);
					 */
					sri.setSampleCode(soii.getSlideCode());
					sri.setDicSampleType(soii.getSampleType());
					sri.setBarCode(soii.getSlideCode());
					sri.setGender(soii.getGender());
					sri.setAcceptDate(sr.getAcceptDate());
					sri.setPatientName(soii.getName());
					sri.setAbbreviation(soii.getSampleOrder().getAbbreviation());
					// 血样颜色检验:0:正常
					if (soii.getSampleOrder() != null) {
						sri.setOrderCode(soii.getSampleOrder().getId());
						sri.setProductId(soii.getSampleOrder().getProductId());
						sri.setProductName(soii.getSampleOrder().getProductName());
						sri.setEMRId(soii.getSampleOrder().getMedicalNumber());
						sri.setFamilyId(soii.getSampleOrder().getFamilyId());
						sri.setBatch(soii.getSampleOrder().getBarcode());
//						sri.setBarCode(soii.getSampleOrder().getBarcode());
					}
					sri.setSampleReceive(sr);
					sri.setSampleNum("1");
					sri.setLabCode(soii.getSlideCode());
					if (slideCode.equals(soii.getSlideCode())) {
						sri.setReceiveState("1");
					} else {
						sri.setReceiveState("0");
					}
					sri.setSamplingDate(soii.getSamplingDate());
					if (sri.getSamplingDate() != null) {

					} else {
						if (sr.getAcceptDate() != null) {
							sri.setSamplingDate(format.parse(format.format(sr.getAcceptDate())));
						} else {
							sri.setSamplingDate(format.parse(format.format(new Date())));
						}
					}
					List<WorkOrderItem> list = workOrderDao.selectNextFlow1("SampleReceive", sri.getProductId());
					for (WorkOrderItem w : list) {
						sri.setNextFlow(w.getDnextName());
						sri.setNextFlowId(w.getDnextId());
					}
					sampleReceiveDao.saveOrUpdate(sri);
				}
				commonDAO.saveOrUpdate(sr);
			}
			map.put("id", sr.getId());
			map.put("sampleOrder", sr.getSampleOrder());

			if (sr.getStartBackTime() != null && !"".equals(sr.getStartBackTime())) {
				map.put("startBackTime", new SimpleDateFormat("yyyy-MM-dd").format(sr.getStartBackTime()));

			} else {
				map.put("startBackTime", null);
			}
			if (sr.getFeedBackTime() != null && !"".equals(sr.getFeedBackTime())) {
				map.put("feedBackTime", new SimpleDateFormat("yyyy-MM-dd").format(sr.getFeedBackTime()));

			} else {
				map.put("feedBackTime", null);
			}
			if (sr.getCcoi() != null && !"".equals(sr.getCcoi())) {
				map.put("ccoi", sr.getCcoi());

			} else {
				map.put("ccoi", null);
			}

			if (sr.getBarcode() != null && !"".equals(sr.getBarcode())) {
				map.put("barcode", sr.getBarcode());

			} else {
				map.put("barcode", null);
			}

			prompt = "1";
		} else {
			prompt = "0";
		}
		map.put("prompt", prompt);

		String state = "1";
		if (itemList1.size() == 0) {
			state = "2";
		}
		map.put("sorry", state);
		return map;
	}

	private String findMaxSampleCodeByOrderCode(String sampleOrderId) {
		return sampleReceiveDao.findMaxSampleCodeByOrderCode(sampleOrderId);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> getCsvContent(String id, String type, String fileId) throws Exception {
		FileInfo fileInfo = sampleReceiveDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		SampleReceive sr = null;
		Map<String, Object> map = new HashMap<String, Object>();
		String prompt = "";
		if (a.isFile()) {
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				sr = new SampleReceive();
				String modelName = "SampleReceive";
				String markCode = "KXJY";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				sr.setId(autoID);
				sr.setType(type);
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sr.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sr.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sr.setAcceptDate(new Date());
				User user = (User) ActionContext.getContext().getSession().get(SystemConstants.USER_SESSION_KEY);
				sr.setAcceptUser(user);
				sampleReceiveDao.saveOrUpdate(sr);
				InputStream is = new FileInputStream(filepath);
				if (is != null) {
					CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
					reader.readHeaders();// 去除表头
					SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
					int i = 2;
					while (reader.readRecord()) {
						String error = "";
						SampleReceiveItem sri = null;
						Boolean boo = true;
						if (reader.get("Actual External ID") != null && !"".equals(reader.get("Actual External ID"))) {
							sri = sampleReceiveDao.getSampleReceiveItemByLabCode(reader.get("Actual External ID"));
							if (sri != null) {
								error += "Actual External ID  already exists,";
								boo = false;
							}
						}
						if (boo) {
							sri = new SampleReceiveItem();
							sri.setBarCode(reader.get("Barcode"));
							if (reader.get("Test") != null && !"".equals(reader.get("Test"))) {
								Product p = productDao.getProductByName(reader.get("Test"));
								if (p != null) {
									sri.setProductId(p.getId());
									sri.setProductName(reader.get("Test"));
								} else {
									error += "Test does not exist in the database,";
								}
							}

							sri.setPatientName(reader.get("Patient Name"));
							if (reader.get("Gender") != null && !"".equals(reader.get("Gender"))) {
								if ("男".equals(reader.get("Gender")) || reader.get("Gender").equals("Male")) {
									sri.setGender("1");
								} else if ("女".equals(reader.get("Gender")) || reader.get("Gender").equals("Female")) {
									sri.setGender("0");
								} else if ("未知".equals(reader.get("Gender")) || reader.get("Gender").equals("Unknow")) {
									sri.setGender("3");
								} else {
									error += " Gender must be entered Male or Female or Unknow,";
								}
							}
							String mark = "";
							if (reader.get("Sample Type") != null && !"".equals(reader.get("Sample Type"))) {
								DicSampleType dst = dicSampleTypeDao
										.selectDicSampleTypeByName(reader.get("Sample Type"));
								if (dst != null) {
									sri.setDicSampleType(dst);
									DicSampleType d = commonDAO.get(DicSampleType.class, dst.getId());
									mark = d.getCode() + DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd");
								} else {
									mark = DateUtil.dateFormatterByPattern(new Date(), "yyMMdd");
									error += " Sample Type does not exist in the database,";
								}
							} else {
								mark = DateUtil.dateFormatterByPattern(new Date(), "yyMMdd");
							}
							String code = codingRuleService.getSampleCode("SampleReceiveItem", mark, 0000, 4, null);
							sri.setSampleCode(code);
							sri.setAcceptDate(sr.getAcceptDate());
							if (reader.get("Sampling Date") != null && !"".equals(reader.get("Sampling Date"))) {
								String pattern = "^((0?[1-9]|1[0-2])/(0?[1-9]|1[0-9]|2[0-8])|(0?[1-9]|1[0-2])/(29|30)|(0?[13578]|1[02])/31)/(?!0000)[0-9]{4}$";
								boolean isMatch = Pattern.matches(pattern, reader.get("Sampling Date"));
								if (isMatch) {
									sri.setSamplingDate(sdf.parse(reader.get("Sampling Date")));
								} else {
									error += "Sampling Date is incorrect,Date format:MM/dd/yyyy,";
								}
							}
							sri.setSampleNum(reader.get("Samples Quantity"));
							sri.setUnit(reader.get("Unit"));
							sri.setLabCode(reader.get("Actual External ID"));
							if (reader.get("Normal/Aberrant") != null && !"".equals(reader.get("Normal/Aberrant"))) {
								if (reader.get("Normal/Aberrant").equals("Y")
										|| reader.get("Normal/Aberrant").equals("是")) {
									sri.setIsGood("1");
								} else if (reader.get("Normal/Aberrant").equals("N")
										|| reader.get("Normal/Aberrant").equals("否")) {
									sri.setIsGood("0");
								} else {
									error += " Normal/Aberrant must be entered Y ro N,";
								}
							}
							sri.setEMRId(reader.get("EMR ID"));
							sri.setNote(reader.get("Note"));
							sri.setSampleReceive(sr);
							if (error.equals("")) {
								sampleReceiveDao.saveOrUpdate(sri);
							}
						}
						if (!error.equals("")) {
							prompt += "The " + i + " Line:" + error + ";";
						}
						i++;
					}
				}
			}
		}
		map.put("prompt", prompt);
		map.put("id", sr.getId());
		return map;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public synchronized String saveSampleReceive(String main, String itemJson, User user, String ids, String changeLog,
			String changeLogItem) throws Exception {
		String id = null;
		if (main != null) {
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(ids);
				li.setClassName("SampleReceive");
				if (ids.equals("NEW") || "".equals(ids)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				li.setModifyContent(changeLog);
				commonDAO.saveOrUpdate(li);
			}
			if (changeLogItem != null && !"".equals(changeLogItem)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(ids);
				if (ids.equals("NEW") || "".equals(ids)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				li.setClassName("SampleReceive");
				li.setModifyContent(changeLogItem);
				commonDAO.saveOrUpdate(li);
			}

			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			SampleReceive sr = new SampleReceive();
			sr = (SampleReceive) commonDAO.Map2Bean(list.get(0), sr);
			if ((sr.getId() != null && "".equals(sr.getId())) || sr.getId().equals("NEW")) {
				String modelName = "SampleReceive";
				String markCode = "KXJY";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				id = autoID;
				sr.setId(autoID);
				sr.setAcceptUser(user);
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sampleReceiveDao.saveOrUpdate(sr);
			} else {
				if ("".equals(sr.getAcceptUser()) || sr.getAcceptUser() == null) {
					sr.setAcceptUser(user);
				}
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				SampleOrder sampleOrder = new SampleOrder();
				sampleOrder = commonDAO.get(SampleOrder.class, sr.getSampleOrder());
				sampleOrder.setStartBackTime(sr.getStartBackTime());
				sampleOrder.setFeedBackTime(sr.getFeedBackTime());
				sampleReceiveDao.save(sampleOrder);

				sampleReceiveDao.saveOrUpdate(sr);
				id = sr.getId();
			}
			List<Map<String, Object>> soilist = JsonUtils.toListByJsonArray(itemJson, List.class);
			int count = 0;
			for (Map<String, Object> map : soilist) {
				count += 1;
				SampleReceiveItem scp = new SampleReceiveItem();
				scp = (SampleReceiveItem) commonDAO.Map2Bean(map, scp);

				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				if (scp.getOrderCode() != null && !"".equals(scp.getOrderCode())) {
					SampleOrder so = sampleReceiveDao.get(SampleOrder.class, scp.getOrderCode());
					if (so != null) {
						if (scp.getProductId() == null
								|| (scp.getProductId() != null && scp.getProductId().equals(""))) {
							scp.setProductId(so.getProductId());
							scp.setProductName(so.getProductName());
						}
						if (scp.getPatientName() == null
								|| (scp.getPatientName() != null && scp.getPatientName().equals(""))) {
							scp.setPatientName(so.getName());
						}
						if (scp.getGender() == null || (scp.getGender() != null && scp.getGender().equals(""))) {
							scp.setGender(so.getGender());
						}
						if (scp.getInspectDate() == null && so.getReceivedDate() != null) {
							scp.setInspectDate(so.getInspectDate());
						}
						if (scp.getSellPerson() == null && so.getCommissioner() != null) {
							scp.setSellPerson(so.getCommissioner());
						}
						if (scp.getCrmCustomer() == null && so.getCrmCustomer() != null) {
							CrmCustomer cc = commonDAO.get(CrmCustomer.class, so.getCrmCustomer().getId());
							scp.setCrmCustomer(cc);
						}

					} else {

						so = new SampleOrder();
						if (scp.getOrderCode() != null && !scp.getOrderCode().equals("")) {
							so.setId(scp.getOrderCode());
						}
						if (scp.getProductId() != null && !scp.getProductId().equals("")) {
							so.setProductId(scp.getProductId());
							so.setProductName(scp.getProductName());
						}

						if (scp.getPatientName() != null && !scp.getPatientName().equals("")) {
							so.setName(scp.getPatientName());
						}

						if (scp.getInspectDate() != null && !scp.getInspectDate().equals("")) {
							so.setInspectDate(scp.getInspectDate());
						}

						if (scp.getSellPerson() != null) {
							so.setCommissioner(scp.getSellPerson());
						}
						so.setState("3");
						so.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
						so.setCreateDate(new Date());
						so.setCreateUser((User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY));
						sampleReceiveDao.saveOrUpdate(so);
					}
				}
				scp.setSampleReceive(sr);

				if (scp.getSampleCode() == null || (scp.getSampleCode() != null && scp.getSampleCode().equals(""))) {

					/*
					 * String markCode = "HT"; if (scp.getDicSampleType() == null ||
					 * scp.getDicSampleType().equals("")) { markCode +=
					 * DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd");
					 * 
					 * } else { DicSampleType d = commonDAO.get(DicSampleType.class,
					 * scp.getDicSampleType().getId()); markCode +=
					 * DateUtil.dateFormatterByPattern(sr.getAcceptDate(), "yyMMdd") + d.getCode() +
					 * "S1"; }
					 * 
					 * String code = codingRuleService.getSampleCode("SampleReceiveItem", markCode,
					 * 00000, 5, null);
					 */
					String code = scp.getOrderCode() + "-" + count;
					scp.setSampleCode(code);

				}
				if (scp.getDicSampleType() != null) {
					if (scp.getProductId() != null && !scp.getProductId().equals("")) {
						String[] productId = scp.getProductId().split(",");
						String pid = "";
						for (int i = 0; i < productId.length; i++) {
							pid += "and productId like '%" + productId[i] + "%' ";
						}
						List<DicSampleTypeItem> list_typeItem = this.dicSampleTypeDao
								.selectDicSampleTypeItemByid(scp.getDicSampleType().getId(), pid);
						for (DicSampleTypeItem d : list_typeItem) {
							if (scp.getNextFlowId() == null || "".equals(scp.getNextFlowId())) {

								scp.setNextFlowId(d.getDnextId());
								scp.setNextFlow(d.getDnextName());
							}
						}
					}
				}
				scp.setAcceptDate(sr.getAcceptDate());
				sampleReceiveDao.saveOrUpdate(scp);
			}

		}
		return id;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleFutureFind(String itemJson) throws Exception {
		List<Map<String, Object>> soilist = JsonUtils.toListByJsonArray(itemJson, List.class);
		for (Map<String, Object> map : soilist) {
			SampleFutureFind scp = new SampleFutureFind();
			scp = (SampleFutureFind) commonDAO.Map2Bean(map, scp);

			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			sampleReceiveDao.saveOrUpdate(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeSampleOrderReceive(String id) {
		SampleReceive se = new SampleReceive();
		se = commonDAO.get(SampleReceive.class, id);
		if ("完成".equals(se.getStateName())) {
			List<SampleReceiveItem> sriList = new ArrayList<SampleReceiveItem>();
			sriList = sampleReceiveDao.getSampleReceiveItemList(id);
			if (!sriList.isEmpty()) {
				for (SampleReceiveItem sri : sriList) {
					SampleOrder so = new SampleOrder();
					so = commonDAO.get(SampleOrder.class, sri.getOrderCode());
					if (so != null) {
						Integer countItem = sampleReceiveDao.countSampleOrderItem(sri.getOrderCode());
						Integer countItemAll = sampleReceiveDao.countSampleOrderItemAll(sri.getOrderCode());
						if (countItemAll > 0) {
							if (countItem == countItemAll) {
								so.setReceiveState("0");
							} else if (((countItemAll - countItem) > 0) && countItem > 0) {
								so.setReceiveState("1");
							} else if (countItem == 0) {
								so.setReceiveState("2");
							}
						}
					}
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createSampleOrderBatch(SampleReceive sct) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);

		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		list = sampleReceiveDao.getSampleReceiveItemOrderByOrderCode(sct.getId());
		if (!list.isEmpty()) {
			for (SampleReceiveItem sri : list) {
				SampleOrder so = new SampleOrder();
				so = sampleReceiveDao.get(SampleOrder.class, sri.getOrderCode());
//				List<SampleOrder> soList = new ArrayList<SampleOrder>();
//				soList = sampleReceiveDao.selSampleOrderCCOIProduct(so);
//				Date drawBloodTime = so.getDrawBloodTime();
//				SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
//				String drawBloodTimeStr = sdf.format(drawBloodTime);
//				String lc = "";
//				if (so.getRound().length() == 1) {
//					lc = "0" + so.getRound();
//				} else {
//					lc = so.getRound();
//				}
//				String barcode = "";
//				if (soList.size() == 0) {
//					barcode = codingRuleService.getSampleOrderBarcode("SampleOrder", so, 0000, 4, null);
//					// Integer countBatch = sampleReceiveDao.selBarcodeLikeProduct(so);
//				} else {
//					if(soList.get(0).getBarcode()!=null
//							&&!"".equals(soList.get(0).getBarcode())) {
//						barcode = soList.get(0).getBarcode().substring(0, soList.get(0).getBarcode().indexOf("-"));//.getBarCode().substring(so.getBarcode().indexOf("-"));
//					}else {
//						barcode = codingRuleService.getSampleOrderBarcode("SampleOrder", so, 0000, 4, null);
//					}
//				}
//				so.setBarcode(barcode + "-" + lc + "-" + drawBloodTimeStr);
//				List<SampleReceiveItem> stiList = new ArrayList<SampleReceiveItem>();
//				stiList = sampleReceiveDao.getSampleReceiveItemOrder(sri.getOrderCode(), sct.getId());
//				if (!stiList.isEmpty()) {
//					for (SampleReceiveItem item : stiList) {
//						item.setBatch(barcode + "-" + lc + "-" + drawBloodTimeStr);
//						commonDAO.saveOrUpdate(item);
//					}
//				}
//				
//				sct.setBarcode(so.getBarcode());

				so.setState("1");
				so.setStateName("实际订单");

				commonDAO.saveOrUpdate(so);

//				sri.setBarCode(so.getBarcode());
//				commonDAO.saveOrUpdate(sri);

			}

			List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "YS001");
			for (UserGroupUser QAugu : QAugus) {
				SysRemind sr = new SysRemind();
				sr.setId(null);
				// 提醒类型
				sr.setType(null);
				// 事件标题
				sr.setTitle("样本接收提醒");
				// 发起人（系统为SYSTEM）
				sr.setRemindUser(u.getName());
				// 发起时间
				sr.setStartDate(new Date());
				// 查阅提醒时间
				sr.setReadDate(new Date());
//				String zt = "";
//				if("1".equals(sri.getMethod())){
//					zt = "合格";
//				}else if("0".equals(sri.getMethod())){
//					zt = "不合格";
//				}else if("2".equals(sri.getMethod())){
//					zt = "让步放行";
//				}else if("3".equals(sri.getMethod())){
//					zt = "备用空管";
//				}else{
//					
//				}
				// 提醒内容
				sr.setContent("病人：" + sct.getCcoi() + "的样本，生产审核中。");
				// 状态 0.草稿 1.已阅
				sr.setState("0");
				// 提醒的用户
				sr.setHandleUser(QAugu.getUser());
				// 表单id
				sr.setContentId(sct.getId());
				sr.setTableId("SampleReceive");
				commonDAO.saveOrUpdate(sr);
			}
		}
		commonDAO.merge(sct);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProductionUser(String id, String userId) {
		SampleReceive sr = new SampleReceive();
		sr = commonDAO.get(SampleReceive.class, id);
		User user = new User();
		user = commonDAO.get(User.class, userId);
		sr.setProductionUser(user);
		commonDAO.saveOrUpdate(sr);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeSampleOrderState(String id) {
//		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
//		list = sampleReceiveDao.getSampleReceiveItemOrderByOrderCode(id);
//		if (!list.isEmpty()) {
//			for (SampleReceiveItem sri : list) {
//				SampleOrder so = new SampleOrder();
//				so = commonDAO.get(SampleOrder.class, sri.getOrderCode());
//				if (!"完成".equals(so.getStateName()) && !"作废".equals(so.getStateName())) {
//					so.setState("1");
//					so.setStateName("实际订单");
//					commonDAO.saveOrUpdate(so);
//				}
//			}
//		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void tongzhi(String id) {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		SampleReceive sri = commonDAO.get(SampleReceive.class, id);
		if (id != null && !"".equals(id) && sri != null) {
			List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "YS001");
			for (UserGroupUser QAugu : QAugus) {
				SysRemind sr = new SysRemind();
				sr.setId(null);
				// 提醒类型
				sr.setType(null);
				// 事件标题
				sr.setTitle("样本接收提醒");
				// 发起人（系统为SYSTEM）
				sr.setRemindUser(u.getName());
				// 发起时间
				sr.setStartDate(new Date());
				// 查阅提醒时间
				sr.setReadDate(new Date());
				// 提醒内容
				sr.setContent("病人：" + sri.getCcoi() + "的样本，QA审核中。");
				// 状态 0.草稿 1.已阅
				sr.setState("0");
				// 提醒的用户
				sr.setHandleUser(QAugu.getUser());
				// 表单id
				sr.setContentId(id);
				sr.setTableId("SampleReceive");
				commonDAO.saveOrUpdate(sr);
			}

		}

//		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
//		list = sampleReceiveDao.getSampleReceiveItemOrderByOrderCode(id);
//		if (!list.isEmpty()) {
//			for (SampleReceiveItem sri : list) {
//				//通知医事服务部人员
//				SampleOrder so = new SampleOrder();
//				so = commonDAO.get(SampleOrder.class, sri.getOrderCode());
//				List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "YS001");
//				for(UserGroupUser QAugu:QAugus){
//					SysRemind sr = new SysRemind();
//					sr.setId(null);
//					// 提醒类型
//					sr.setType(null);
//					// 事件标题
//					sr.setTitle("样本接收提醒");
//					// 发起人（系统为SYSTEM）
//					sr.setRemindUser(u.getName());
//					// 发起时间
//					sr.setStartDate(new Date());
//					// 查阅提醒时间
//					sr.setReadDate(new Date());
//					String zt = "";
//					if("1".equals(sri.getMethod())){
//						zt = "合格";
//					}else if("0".equals(sri.getMethod())){
//						zt = "不合格";
//					}else if("2".equals(sri.getMethod())){
//						zt = "让步放行";
//					}else if("3".equals(sri.getMethod())){
//						zt = "备用空管";
//					}else{
//						
//					}
//					// 提醒内容
//					sr.setContent("病人："+so.getName()+";样本编号："+sri.getSampleCode()+"的样本，QA审核中，当前检测状态为"+zt);
//					// 状态 0.草稿  1.已阅 
//					sr.setState("0");
//					//提醒的用户
//					sr.setHandleUser(QAugu.getUser());
//					// 表单id
//					sr.setContentId(sri.getSampleReceive().getId());
//					sr.setTableId("SampleReceive");
//					commonDAO.saveOrUpdate(sr);
//				}
//			}
//		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplereceiveItemBycan(SampleReceiveItem sri) {
		sri.setReceiveState("1");
		String barcode = "样本接收：条形码为" + sri.getBarCode() + " 已扫描";
		if (sri.getReceiveState().equals("1")) {
			sri.setMethod("1");
		} else {
			sri.setMethod("3");
		}
		if (barcode != null && !"".equals(barcode)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(barcode);
			li.setFileId(sri.getSampleReceive().getId());
			li.setClassName("SampleReceive");
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}
		commonDAO.saveOrUpdate(sri);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void checkU(String sxh, String cxsj) throws ParseException {
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar calendar = Calendar.getInstance();
		int[] a = { 6, 12, 12, 18, 18, 24, 30, 30, 42, 42, 54, 54, 66, 66, 78, 87, 99, 108, 120, 129, 141, 150, 162,
				178, 190, 206, 218, 234, 246, 262, 274, 290, 302, 318, 330, 346, 358, 374, 386 };
		int[] b = { 0, 6, 12, 18, 30, 42, 54, 66, 87, 108, 129, 150, 178, 206, 234, 262, 290, 318, 346, 374 };
		int[] c = { 12, 18, 24, 30, 42, 54, 66, 78, 99, 120, 141, 162, 190, 218, 246, 274, 302, 330, 358, 386 };
		for (int i = 0; i < b.length; i++) {
			calendar.setTime(sDateFormat.parse(cxsj));
			calendar.add(Calendar.DAY_OF_YEAR, b[i]);
			Date ne = calendar.getTime();
			System.out.println(ne);
			SampleFutureFind sff = new SampleFutureFind();
			sff.setId(null);
			sff.setFiltrateCode(sxh);
			sff.setHappenEvent("1");
			sff.setRound((i + 1) + "");
			sff.setSampleState("0");
			sff.setSampleTime(sDateFormat.format(ne));
			sff.setState("1");
			commonDAO.saveOrUpdate(sff);
		}
		for (int j = 0; j < c.length; j++) {
			calendar.setTime(sDateFormat.parse(cxsj));
			calendar.add(Calendar.DAY_OF_YEAR, c[j]);
			Date ne = calendar.getTime();
			SampleFutureFind sff = new SampleFutureFind();
			sff.setId(null);
			sff.setFiltrateCode(sxh);
			sff.setHappenEvent("2");
			sff.setSampleState("0");
			sff.setRound((j + 1) + "");
			sff.setSampleTime(sDateFormat.format(ne));
			sff.setState("1");
			commonDAO.saveOrUpdate(sff);
		}
	}
	
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void checkU1(String sxh, String cxsj,String id,String round1) throws ParseException {
		SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Calendar calendar = Calendar.getInstance();
		int[] a = { 6, 12, 12, 18, 18, 24, 30, 30, 42, 42, 54, 54, 66, 66, 78, 87, 99, 108, 120, 129, 141, 150, 162,
				178, 190, 206, 218, 234, 246, 262, 274, 290, 302, 318, 330, 346, 358, 374, 386 };
		int[] b = { 0, 6, 12, 18, 30, 42, 54, 66, 87, 108, 129, 150, 178, 206, 234, 262, 290, 318, 346, 374 };
		int[] c = { 12, 18, 24, 30, 42, 54, 66, 78, 99, 120, 141, 162, 190, 218, 246, 274, 302, 330, 358, 386 };
		for (int i = 0; i < b.length; i++) {
			calendar.setTime(sDateFormat.parse(cxsj));
			calendar.add(Calendar.DAY_OF_YEAR, b[i]);
			Date ne = calendar.getTime();
			System.out.println(ne);
			SampleFutureFind sff = new SampleFutureFind();
			sff.setId(null);
			sff.setFiltrateCode(sxh);
			sff.setHappenEvent("1");
			sff.setRound((i + 1) + "");
			sff.setSampleState("0");
			if(sff.getRound().equals("1")) {
				sff.setDingDanCode(id);
				sff.setTimeState("2");
				sff.setStateName("样本已接收");
				sff.setState("2");
			}else {
				sff.setDingDanCode(null);
				sff.setTimeState("0");
				sff.setState("1");
			}
			sff.setTimeShowState("1");
			sff.setSampleTime(sDateFormat.format(ne));
			
			commonDAO.saveOrUpdate(sff);
		}
		for (int j = 0; j < c.length; j++) {
			calendar.setTime(sDateFormat.parse(cxsj));
			calendar.add(Calendar.DAY_OF_YEAR, c[j]);
			Date ne = calendar.getTime();
			SampleFutureFind sff = new SampleFutureFind();
			sff.setId(null);
			sff.setFiltrateCode(sxh);
			sff.setHappenEvent("2");
			sff.setSampleState("0");
			sff.setRound((j + 1) + "");
			sff.setSampleTime(sDateFormat.format(ne));
			sff.setState("1");
			commonDAO.saveOrUpdate(sff);
		}
	}

	public List<SampleFutureFind> chaxunsj(String kssj, String jssj) {
		return sampleReceiveDao.chaxunsj(kssj, jssj);
	}

	public List<Object[]> chaxunsjAndid(String[] sjbb, String filtrateCode) {
		return sampleReceiveDao.chaxunsjAndid(sjbb, filtrateCode);
	}

	public List<Object[]> chaxunsjid(String zzz, String filtrateCode) {
		return sampleReceiveDao.chaxunsjid(zzz, filtrateCode);
	}

	public List<String> chaxunsjd(String[] sjbb) {
		return sampleReceiveDao.chaxunsjd(sjbb);
	}

	public List<Object[]> findIDAndStates() {
		return sampleReceiveDao.findIDAndStates();
	}

	public String selectSampleNumByCount(String id) {
		SampleFutureFind sff = commonDAO.get(SampleFutureFind.class, id);
		return sampleReceiveDao.selectSampleNumByCount(sff.getFiltrateCode(), sff.getRound());
	}

	public String selectSampleNumByState(String id) {
		SampleFutureFind sff = commonDAO.get(SampleFutureFind.class, id);
		return sampleReceiveDao.selectSampleNumByState(sff.getFiltrateCode(), sff.getRound());
	}

	public SampleOrder getSampleOrder(String id) {
		SampleOrder so = commonDAO.get(SampleOrder.class, id);
		return so;
	}

	public SampleFutureFind findFiltrateCodeOfRound(String id, String round) {
//		SampleFutureFind sff = commonDAO.get(SampleFutureFind.class, id);
		return sampleReceiveDao.findFiltrateCodeOfRound(id, round);
	}

	public List<SampleOrder> findSampleOrderId(String id) {
		List<SampleOrder> so = sampleReceiveDao.findSampleOrderId(id);
		return so;
	}

	public SampleOrder findRound(String round, String filtrateCode) {
		return sampleReceiveDao.findRound(round, filtrateCode);
	}

}
