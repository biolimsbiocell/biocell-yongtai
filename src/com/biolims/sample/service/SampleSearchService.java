package com.biolims.sample.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.dao.SampleSearchDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.weichat.service.CancerTypeService;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleSearchService {
	@Resource
	private SampleOrderDao sampleOrderDao;
	@Resource
	private CommonService commonService;
	@Resource
	private CancerTypeService cancerTypeService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleSearchDao sampleSearchDao;
	
	
	StringBuffer json = new StringBuffer();

	/** 根据批次号查询步骤信息 */
	public List<CellPassageTemplate> searchCellPassageTemplates(String batch) {
		return sampleSearchDao.searchCellPassageTemplates(batch);
	}

	/** 根据批次号查询没作废的运输明细 */
	public List<TransportOrderCell> getTransportOrderCells(String batch) {
		return sampleSearchDao.getTransportOrderCells(batch);
	}

	/** 根据实验单号，查询 第一步的实体*/
	public List<CellProductionRecord> searchCellPassageRecord(String id, String batch, String stepNums) {
		return sampleSearchDao.searchCellPassageRecord( id, batch, stepNums);
	}

	/** 根据批次号查询偏差子表，不是作废的单子*/
	public List<InfluenceProduct> getPlanInfluenceProducts(String batch) {
		return sampleSearchDao.getPlanInfluenceProducts(batch);
	}

	public Map<String, Object> showCellPassageQualityItemJson(Integer start, Integer length, String query, String col,
			String sort, String batch, String ordernum) {
		return sampleSearchDao.showCellPassageQualityItemJson(start, length, query, col, sort, batch, ordernum);
	}
	//添加日志
	public void insertLog(String searchnum,String productName) {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		if (searchnum != null && !"".equals(searchnum)) {
			String slideCode1="";
			if(!"".equals(productName)&&productName!=null) {
				slideCode1 = "样本信息查询：产品批号为"+""+ searchnum +""+"的样本被 "+""+u.getName()+""+" 查看,查询项目为："+productName;
			}else {
				slideCode1 = "样本信息查询：产品批号为"+""+ searchnum +""+"的样本被 "+""+u.getName()+""+" 查看";
			}
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setModifyContent(slideCode1);
			li.setState("5");
			li.setStateName("查询");
			li.setClassName("样本信息");
			li.setFileId(searchnum);
			
			commonDAO.saveOrUpdate(li);

	}
		
	}

	public List<CellPassageQualityItem> getCellPassageQualityItemUnquality(String id, String orderNum) {
		return sampleSearchDao.getCellPassageQualityItemUnquality(id, orderNum);
	}

	public Map<String, Object> showSampleOrdersJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleSearchDao.showSampleOrdersJson(start, length, query, col, sort);
	}

	public List<SampleOrderItem> findBatchNumByCellCode(String batch) {
		return sampleSearchDao.findBatchNumByCellCode(batch);
	}

}
