package com.biolims.sample.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleState;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleStateService  {
	



	@Resource
	private CommonDAO commonDAO;

	@Resource
	private CommonService commonService;

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleState1(SampleOrder sampleOrder, String code, String sampleCode,
			String productId, String productName, String stageTime,
			String startDate, String endDate, String tableTypeId,
			String stageName, User acceptUser, String taskId,
			String taskMethod, String taskResult, String techTaskId,
			String note, String note2, String note3, String note4,
			String note5, String note6, String note7) throws Exception {
		SampleState sampleState = new SampleState();
		if(sampleOrder!=null) {
			sampleState.setSampleOrder(sampleOrder);
		}
		sampleState.setCode(code);
		sampleState.setSampleCode(sampleCode);
		sampleState.setProductId(productId);
		sampleState.setProductName(productName);
		sampleState.setStageName(stageName);
		sampleState.setStageTime(stageTime);
		sampleState.setStartDate(startDate);
		sampleState.setEndDate(endDate);
		sampleState.setTableTypeId(tableTypeId);
		ApplicationTypeTable o = commonService.get(com.biolims.applicationType.model.ApplicationTypeTable.class, tableTypeId);
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("systemLan");
		/*if(lan.equals("en"))
			sampleState.setStageName(o.getEnName());
		else
			sampleState.setStageName(o.getName());*/
		sampleState.setAcceptUser(acceptUser);
		sampleState.setTaskId(taskId);
		sampleState.setTaskMethod(taskMethod);
		sampleState.setTaskResult(taskResult);
		sampleState.setTechTaskId(techTaskId);
		sampleState.setNote(note);
		sampleState.setNote2(note2);
		sampleState.setNote3(note3);
		sampleState.setNote4(note4);
		sampleState.setNote5(note5);
		sampleState.setNote6(note6);
		sampleState.setNote7(note7);
		commonDAO.saveOrUpdate(sampleState);
		
		if (sampleCode != null && !sampleCode.equals("")) {
			String[] scode = sampleCode.split(",");
			for (int i = 0; i < scode.length; i++) {
				List<SampleInfo> sil = commonService.get(SampleInfo.class,
						"code", scode[i]);
				if (sil.size() > 0) {
					SampleInfo si = sil.get(0);
					si.setState(tableTypeId);
					si.setStateName(stageName);
					commonDAO.saveOrUpdate(si);
				}
			}
		}
		
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleState(String code, String sampleCode,
			String productId, String productName, String stageTime,
			String startDate, String endDate, String tableTypeId,
			String stageName, User acceptUser, String taskId,
			String taskMethod, String taskResult, String techTaskId,
			String note, String note2, String note3, String note4,
			String note5, String note6, String note7) throws Exception {
		SampleState sampleState = new SampleState();
		sampleState.setCode(code);
		sampleState.setSampleCode(sampleCode);
		sampleState.setProductId(productId);
		sampleState.setProductName(productName);
		sampleState.setStageName(stageName);
		sampleState.setStageTime(stageTime);
		sampleState.setStartDate(startDate);
		sampleState.setEndDate(endDate);
		sampleState.setTableTypeId(tableTypeId);
		ApplicationTypeTable o = commonService.get(com.biolims.applicationType.model.ApplicationTypeTable.class, tableTypeId);
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("systemLan");
		if(lan.equals("en"))
			sampleState.setStageName(o.getEnName());
		else
			sampleState.setStageName(o.getName());
		sampleState.setAcceptUser(acceptUser);
		sampleState.setTaskId(taskId);
		sampleState.setTaskMethod(taskMethod);
		sampleState.setTaskResult(taskResult);
		sampleState.setTechTaskId(techTaskId);
		sampleState.setNote(note);
		sampleState.setNote2(note2);
		sampleState.setNote3(note3);
		sampleState.setNote4(note4);
		sampleState.setNote5(note5);
		sampleState.setNote6(note6);
		sampleState.setNote7(note7);
		commonDAO.saveOrUpdate(sampleState);

		if (sampleCode != null && !sampleCode.equals("")) {
			String[] scode = sampleCode.split(",");
			for (int i = 0; i < scode.length; i++) {
				List<SampleInfo> sil = commonService.get(SampleInfo.class,
						"code", scode[i]);
				if (sil.size() > 0) {
					SampleInfo si = sil.get(0);
					si.setState(tableTypeId);
					si.setStateName(stageName);
					commonDAO.saveOrUpdate(si);
				}
			}
		}

	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleState1(String code, String sampleCode,
			String productId, String productName, String stageTime,
			String startDate, String endDate, String tableTypeId,
			String stageName, User acceptUser, String taskId,
			String taskMethod, String taskResult, String techTaskId,
			String note, String note2, String note3, String note4,
			String note5, String note6, String note7,String result,String resultDesc,String lcAdvice,String abnormal) throws Exception {
		SampleState sampleState = new SampleState();
		sampleState.setCode(code);
		sampleState.setSampleCode(sampleCode);
		sampleState.setProductId(productId);
		sampleState.setProductName(productName);
		ApplicationTypeTable o = commonService.get(com.biolims.applicationType.model.ApplicationTypeTable.class, tableTypeId);
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("systemLan");
		if(lan.equals("en"))
			sampleState.setStageName(o.getEnName());
		else
			sampleState.setStageName(o.getName());
		sampleState.setStartDate(startDate);
		sampleState.setEndDate(endDate);
		sampleState.setTableTypeId(tableTypeId);
		sampleState.setStageName(stageName);
		sampleState.setAcceptUser(acceptUser);
		sampleState.setTaskId(taskId);
		sampleState.setTaskMethod(taskMethod);
		sampleState.setTaskResult(taskResult);
		sampleState.setTechTaskId(techTaskId);
		sampleState.setNote(note);
		sampleState.setNote2(note2);
		sampleState.setNote3(note3);
		sampleState.setNote4(note4);
		sampleState.setNote5(note5);
		sampleState.setNote6(note6);
		sampleState.setNote7(note7);
//		sampleState.setResult(result);
//		sampleState.setResultDesc(resultDesc);
//		sampleState.setLcAdvice(lcAdvice);
//		sampleState.setAbnormal(abnormal);
		commonDAO.saveOrUpdate(sampleState);
	}
}
