package com.biolims.sample.service;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleCancerTempDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.system.product.model.Product;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleCancerTempService {
	@Resource
	private SampleCancerTempDao sampleCancerTempDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	
	
	//根据ID查询一录和二录
	public List<SampleCancerTemp> querySampleCancerList(String id){
		List<SampleCancerTemp> list = sampleCancerTempDao.QuerySampleCancerTemp(id);
		return list;
	}
	//查询子表纪录1
	public List<SampleCancerTempPersonnel>  getPersonnel(String id){
		return this.sampleCancerTempDao.getPersonnel(id);
	}
	//查询子表纪录2
	public List<SampleCancerTempItem>  getCancerTempItem(String id){
			return this.sampleCancerTempDao.getCancerTempItem(id);
		}
				
				
	public Map<String, Object> findSampleCancerTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleCancerTempDao.selectSampleCancerTempList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleCancerTemp i) throws Exception {

		sampleCancerTempDao.saveOrUpdate(i);

	}
	//保存一条空记录，收费确认
	public void savemaket(CrmConsumerMarket ck) throws Exception {
		sampleCancerTempDao.save(ck);
	}
	public List<DicSampleType> findDicSampleType(String type) throws Exception {//jiaru
		 
		List<DicSampleType> list = new ArrayList<DicSampleType>();
		list =sampleCancerTempDao.selectDicSampleType(type);
		return list;
	}
	public List<DicType> findDicSampleType1(String type) throws Exception {//jiaru
		
		List<DicType> list = new ArrayList<DicType>();
		list =sampleCancerTempDao.selectDicSampleType1(type);
		return list;
	}
	public List<DicType> findDicSampleType2(String type) throws Exception {//jiaru
		
		List<DicType> list = new ArrayList<DicType>();
		list =sampleCancerTempDao.selectDicSampleType2(type);
		return list;
	}
	public List<DicType> findDicSampleType3(String type) throws Exception {//jiaru
		
		List<DicType> list = new ArrayList<DicType>();
		list =sampleCancerTempDao.selectDicSampleType3(type);
		return list;
	}
	public SampleCancerTemp get(String id) {
		SampleCancerTemp sampleCancerTemp = commonDAO.get(SampleCancerTemp.class, id);
		return sampleCancerTemp;
	}
	public SampleOrder getSampleOrder(String id) {
		SampleOrder order = commonDAO.get(SampleOrder.class, id);
		return order;
	}
	/*
	 *明细查询 
	 * */
	
	public Map<String, Object> findSampleCancerTempPersonnelList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleCancerTempDao.selectSampleCancerTempPersonnelList(scId, startNum, limitNum, dir, sort);
		List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>) result.get("list");
		return result;
	}
	public Map<String, Object> findSampleCancerTempItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleCancerTempDao.selectSampleCancerTempItemList(scId, startNum, limitNum, dir, sort);
		List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleCancerTempPersonnel(SampleCancerTemp sc, String itemDataJson) throws Exception {
		List<SampleCancerTempPersonnel> saveItems = new ArrayList<SampleCancerTempPersonnel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleCancerTempPersonnel scp = new SampleCancerTempPersonnel();
			// 将map信息读入实体类
			scp = (SampleCancerTempPersonnel) sampleCancerTempDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleCancerTemp(sc);

			saveItems.add(scp);
		}
		sampleCancerTempDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleCancerTempPersonnel(String[] ids) throws Exception {
		for (String id : ids) {
			SampleCancerTempPersonnel scp =  sampleCancerTempDao.get(SampleCancerTempPersonnel.class, id);
			 sampleCancerTempDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleCancerTempItem(SampleCancerTemp sc, String itemDataJson) throws Exception {
		List<SampleCancerTempItem> saveItems = new ArrayList<SampleCancerTempItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleCancerTempItem scp = new SampleCancerTempItem();
			// 将map信息读入实体类
			scp = (SampleCancerTempItem) sampleCancerTempDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleCancerTemp(sc);

			saveItems.add(scp);
		}
		sampleCancerTempDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleCancerTempItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleCancerTempItem scp =  sampleCancerTempDao.get(SampleCancerTempItem.class, id);
			 sampleCancerTempDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleCancerTemp sc, Map jsonMap) throws Exception {
		System.out.println(sc.getSampleTypeId()+"=================================================");
		if (sc != null) {
			sampleCancerTempDao.saveOrUpdate(sc);
		
//			String jsonStr = "";
//			jsonStr = (String)jsonMap.get("sampleCancerTempPersonnel");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveSampleCancerTempPersonnel(sc, jsonStr);
//			}
//			jsonStr = (String)jsonMap.get("sampleCancerTempItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveSampleCancerTempItem(sc, jsonStr);
//			}
		}
   }
	public boolean compareAndTransmit(SampleCancerTemp si) throws Exception {
		boolean flag = true;
		List<SampleCancerTemp> list = querySampleCancerList(si.getOrderNumber());
		
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd ");

		SampleCancerTemp s0 = list.get(0);
			// 如果查出的list结果大于两条 集合中每个字段就进行逐个对比
			if (list.size() == 2) {
				SampleCancerTemp s1 = list.get(1);
				Class c = s0.getClass();
				Field[] m = c.getDeclaredFields();
				try {
					for (int i = 0; i < m.length; i++) {
						if (!m[i].getName().equals("id")
								&& !m[i].getName().equals("note")
								&& !m[i].getName().equals("createUser")
								&& !m[i].getName().equals("createDate")
								&& !m[i].getName().equals("confirmUser")
								&& !m[i].getName().equals("confirmDate")
								&& !m[i].getName().equals("state")
								&& !m[i].getName().equals("stateName")
								&& !m[i].getName().equals("crmProduct")
								&& !m[i].getName().equals("productId")
								&& !m[i].getName().equals("productName")) {
							m[i].setAccessible(true);
							Object o0 = BeanUtils.getFieldValue(s0,
									m[i].getName());
							Object o1 = BeanUtils.getFieldValue(s1,
									m[i].getName());

							if (o0 != null && o1 != null && !o0.equals(o1)) {// 这里可以查看那个字段不一样
								flag = false;
							}
						}
					}
				} catch (Throwable e) {
					System.err.println(e);
				}

				s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
				si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
				// 当前用户给到SamplInfo中
					if (flag == true) {
						// 两条数据都改为信息已录入 其中第一条数据在这里给状态
						s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
	
						si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
						si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
						SampleOrder order = new SampleOrder();
						// 取SmpleInputTemp其中一条数据给SampleInput
							copy(order, s0);
						
					}
				}
				return flag;
			}
//copy方法
				public void copy(Object order, Object cancer) throws Exception {
					Map<String, Object> inputMap = new HashMap<String, Object>();
					Field[] inputFields = order.getClass().getDeclaredFields();
					for (Field fd : inputFields) {
						if(fd.getName().equals("id")){
							continue;
						}
						try {
							inputMap.put(fd.getName(),
									BeanUtils.getFieldValue(cancer, fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					Field[] inputTempFields = order.getClass().getDeclaredFields();
					for (Field fd : inputTempFields) {
						if (inputMap.get(fd.getName()) == null) {
							continue;
						}
						try {
							// 给属性赋值
							BeanUtils.setFieldValue(order, fd.getName(),
									inputMap.get(fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//两条记录基本一致，直接修改订单纪录
					SampleOrder sampleOrderOne = (SampleOrder) order;
					SampleCancerTemp s = (SampleCancerTemp)cancer;
					sampleOrderOne.setId(s.getOrderNumber());
					this.sampleCancerTempDao.merge(sampleOrderOne);
					
				}
				//保存订单子表纪录personnel1
				public void copyPersonnel(Object order, Object person,SampleOrder s) throws Exception {
					Map<String, Object> inputMap = new HashMap<String, Object>();
					Field[] inputFields = order.getClass().getDeclaredFields();
					for (Field fd : inputFields) {
						if(fd.getName().equals("id")){
							continue;
						}
						try {
							inputMap.put(fd.getName(),
									BeanUtils.getFieldValue(person, fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					Field[] inputTempFields = order.getClass().getDeclaredFields();
					for (Field fd : inputTempFields) {
						if (inputMap.get(fd.getName()) == null) {
							continue;
						}
						try {
							// 给属性赋值
							BeanUtils.setFieldValue(order, fd.getName(),
									inputMap.get(fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//两条记录基本一致，直接修改订单纪录
					
					SampleOrderPersonnel p1 = (SampleOrderPersonnel) order;
					p1.setSampleOrder(s);
					this.sampleCancerTempDao.save(p1);
					
				}
				//保存订单子表纪录item1
				public void copyItem(Object order, Object item,SampleOrder s) throws Exception {
					Map<String, Object> inputMap = new HashMap<String, Object>();
					Field[] inputFields = order.getClass().getDeclaredFields();
					for (Field fd : inputFields) {
						if(fd.getName().equals("id")){
							continue;
						}
						try {
							inputMap.put(fd.getName(),
									BeanUtils.getFieldValue(item, fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					Field[] inputTempFields = order.getClass().getDeclaredFields();
					for (Field fd : inputTempFields) {
						if (inputMap.get(fd.getName()) == null) {
							continue;
						}
						try {
							// 给属性赋值
							BeanUtils.setFieldValue(order, fd.getName(),
									inputMap.get(fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//两条记录基本一致，直接修改订单纪录
					SampleOrderItem item1 = (SampleOrderItem) order;
					item1.setSampleOrder(s);
					this.sampleCancerTempDao.save(item1);
					
				}
				//保存电子病历子表纪录person
				public void copycmrPerson(Object order, Object item,CrmPatient crm) throws Exception {
					Map<String, Object> inputMap = new HashMap<String, Object>();
					Field[] inputFields = order.getClass().getDeclaredFields();
					for (Field fd : inputFields) {
						if(fd.getName().equals("id")){
							continue;
						}
						try {
							inputMap.put(fd.getName(),
									BeanUtils.getFieldValue(item, fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					Field[] inputTempFields = order.getClass().getDeclaredFields();
					for (Field fd : inputTempFields) {
						if (inputMap.get(fd.getName()) == null) {
							continue;
						}
						try {
							// 给属性赋值
							BeanUtils.setFieldValue(order, fd.getName(),
									inputMap.get(fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//两条记录基本一致，直接修改订单纪录
					CrmPatientPersonnel p1 = (CrmPatientPersonnel) order;
					p1.setCrmPatient(crm);
					this.sampleCancerTempDao.save(p1);
					
				}
				
				
				//保存电子病历子表纪录item
				public void copycmrItem(Object order, Object item,CrmPatient crm) throws Exception {
					Map<String, Object> inputMap = new HashMap<String, Object>();
					Field[] inputFields = order.getClass().getDeclaredFields();
					for (Field fd : inputFields) {
						if(fd.getName().equals("id")){
							continue;
						}
						try {
							inputMap.put(fd.getName(),
									BeanUtils.getFieldValue(item, fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					Field[] inputTempFields = order.getClass().getDeclaredFields();
					for (Field fd : inputTempFields) {
						if (inputMap.get(fd.getName()) == null) {
							continue;
						}
						try {
							// 给属性赋值
							BeanUtils.setFieldValue(order, fd.getName(),
									inputMap.get(fd.getName()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					//两条记录基本一致，直接修改订单纪录
					CrmPatientItem item1 = (CrmPatientItem) order;
					item1.setCrmPatient(crm);
					this.sampleCancerTempDao.save(item1);
					
				}
				
				/**
				 * 利用treePanel复选框
				 * 
				 * @throws Exception
				 * 
				 */
				//健康状况
				public String getTreeJson(List<DicType> list) throws Exception {
					if (list != null && list.size() > 0) {
					}
					json = new StringBuffer();
					List<DicType> nodeList0 = new ArrayList<DicType>();
					Iterator<DicType> it1 = list.iterator();
					while (it1.hasNext()) {
						DicType node = (DicType) it1.next();
						nodeList0.add(node);
					}
					Iterator<DicType> it = nodeList0.iterator();
					while (it.hasNext()) {
						DicType node = (DicType) it.next();
						constrctorTreeJson(list, node);
					}
					String jsonDate = json.toString();
					return ("[" + jsonDate + "]").replaceAll(",]", "]");

				}
				
				/**
				 * 
				 * 构建treeJson
				 * 
				 * @throws Exception
				 * 
				 */
				public void constrctorTreeJson(List<DicType> list, DicType treeNode)
						throws Exception {

						json.append("{id:\"");
						json.append(treeNode.getId() + "\"");
						json.append(",text:\"");
						json.append(treeNode.getName() + "\"");
						json.append(",leaf:true");
						json.append(",checked:false");
						json.append("},");

				}
				
	
}


