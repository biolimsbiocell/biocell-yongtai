package com.biolims.sample.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.dao.CodingRuleDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackSample;
import com.biolims.sample.dao.SampleInputTechnologyDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTechnology;
import com.biolims.sample.storage.model.SampleInItemTemp;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleInputTechnologyService {
	@Resource
	private SampleInputTechnologyDao sampleInputTechnologyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleDao codingRuleDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSampleInputTechnologyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleInputTechnologyDao.selectSampleInputTechnologyList(mapForQuery, startNum, limitNum, dir, sort);
	}
	public SampleInfo findSampleInfoByCode(String code)throws Exception{		
		SampleInfo list = sampleInputTechnologyDao.findByCode(code);
		return list;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInputTechnology i) throws Exception {

		sampleInputTechnologyDao.saveOrUpdate(i);

	}
	public SampleInputTechnology get(String id) {
		SampleInputTechnology sampleInputTechnology = commonDAO.get(SampleInputTechnology.class, id);
		return sampleInputTechnology;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInputTechnology sc, Map jsonMap) throws Exception {
		if (sc != null) {
			if(sc.getSampleInfo()==null||(sc.getSampleInfo()!=null&&sc.getSampleInfo().getId().equals(""))){
				SampleInfo sampleInfo = new SampleInfo();
				sampleInfo.setClassify("1");//标示是科技服务的信息录入
				if(sc.getIsQualified()!=null && sc.getIsQualified().equals("0")){//不合格
					sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW);
					sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_NEW_NAME);
					sampleInfo.setCode(sc.getSampleCode());
					sampleInputTechnologyDao.saveOrUpdate(sampleInfo);
					sc.setSampleInfo(sampleInfo);
					
					//如果不合格去项目管理
					FeedbackSample feedbackSample = new FeedbackSample();
					feedbackSample.setId(sc.getSampleCode());
					feedbackSample.setNextflow("0");//标示下一步流向，显示到项目管理里
					sampleInputTechnologyDao.saveOrUpdate(feedbackSample);
				}else if(sc.getIsQualified()!=null && sc.getIsQualified().equals("1")){//合格
					sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
					sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
					sampleInfo.setCode(sc.getSampleCode());
					sampleInfo.setCreateUser(sc.getCreateUser());
					sampleInfo.setPatientName(sc.getClientName());
					sampleInputTechnologyDao.saveOrUpdate(sampleInfo);
					sc.setSampleInfo(sampleInfo);
					
					//合格以后样本去到入库
//					SampleInItemTemp sampleInItemTemp = new SampleInItemTemp();
//					sampleInItemTemp.setSampleInputTechnology(sc);
//					sampleInItemTemp.setState("1");//标示改为1
//					sampleInItemTemp.setSampleCode(sc.getSampleCode());
//					
//					sampleInputTechnologyDao.saveOrUpdate(sampleInItemTemp);
				}
			}
			String sampleCode = sc.getSampleCode();
			SampleInfo s = sampleInputTechnologyDao.findByCode(sampleCode);
			s.setCreateUser1(sc.getCreateUser().getId());
			s.setPatientName(sc.getClientName());
			sampleInputTechnologyDao.saveOrUpdate(sc);
			
		}
	}
	

	//生成编码日期+5位
	public String genTransID(String modelName,String markCode) throws Exception {
		Date date=new Date();
		DateFormat format=new SimpleDateFormat("yyyyMMdd");
		String s=format.format(date);
		System.out.println(s);
		String stime =s.substring(2,6);
		String autoID = getCodeByPrefix(modelName,markCode,stime , 00000, 5,null);
		return autoID;
	}
	public String getCodeByPrefix(String modelName,String markCode,String prefix, long code, Integer flowLength,Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = markCode+prefix;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("sampleCode", "like##@@##'" + val + "%'");

		String maxId = sampleInputTechnologyDao.selectModelTotalByType(modelName,mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}
}
