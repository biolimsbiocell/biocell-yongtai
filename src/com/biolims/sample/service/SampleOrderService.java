package com.biolims.sample.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.experiment.quality.model.QualityTestResultManage;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderChange;
import com.biolims.sample.model.SampleOrderChangeItem;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.system.product.model.Product;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.weichat.GetImage;
import com.biolims.weichat.service.CancerTypeService;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oracle.net.aso.i;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleOrderService {
	@Resource
	private SampleOrderDao sampleOrderDao;
	@Resource
	private CommonService commonService;
	@Resource
	private CancerTypeService cancerTypeService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrder i) throws Exception {

		sampleOrderDao.saveOrUpdate(i);

	}

	public SampleOrder get(String id) {
		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, id);
		return sampleOrder;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(SampleOrder sc, Map jsonMap) throws Exception {
		if (sc != null) {

			sampleOrderDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}

	// 保存一条空记录，收费确认
	public void savemaket(CrmConsumerMarket ck) throws Exception {
		this.sampleOrderDao.save(ck);
	}

	// 查询子表记录
	public List<SampleOrderItem> querySampleItem(String id) {
		return this.sampleOrderDao.querySampleItem(id);
	}

	public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
		return this.sampleOrderDao.querySamplePersonnel(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderPersonnel(SampleOrder sc, String itemDataJson) throws Exception {
		List<SampleOrderPersonnel> saveItems = new ArrayList<SampleOrderPersonnel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderPersonnel scp = new SampleOrderPersonnel();
			// 将map信息读入实体类
			scp = (SampleOrderPersonnel) sampleOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null) {
				if (scp.getId().equals(""))
					scp.setId(null);
			}
			scp.setSampleOrder(sc);
			saveItems.add(scp);
		}
		sampleOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOrderPersonnel(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOrderPersonnel scp = sampleOrderDao.get(SampleOrderPersonnel.class, id);
			sampleOrderDao.delete(scp);
		}
		
		/*
		 * if (changeLog != null && !"".equals(changeLog)) { LogInfo li = new LogInfo();
		 * li.setLogDate(new Date()); User u = (User)
		 * ServletActionContext.getRequest().getSession()
		 * .getAttribute(SystemConstants.USER_SESSION_KEY); li.setUserId(u.getId());
		 * li.setFileId(sc.getId()); li.setModifyContent(changeLog);
		 * commonDAO.saveOrUpdate(li); }
		 */
	}

	/**
	 * 解析json，保存订单
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderByJson(String jsons) throws Exception {
		// 解析json
		JSONArray array = JSONArray.fromObject("[" + jsons + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		SampleOrder order = new SampleOrder();
		// 根据微信号,查询user,并且把user存到订单的世和专员

		// commonService.saveOrUpdate(u);
		List<User> l = commonService.get(User.class, "weichatId", (String) object.get("userId"));
		order.setId(object.getString("medicalNumber"));
		// order.setMedicalNumber(object.getString("medicalNumber"));
		order.setName(object.getString("testName"));
		// order.setSampleInfoMain(sampleInfoMain)
		order.setSampleTypeId(object.getString("sample"));

		order.setCancerType(cancerTypeService.getCancerTypeByName((String) object.get("cancerType")));
		order.setCancerTypeSeedOne(
				cancerTypeService.getCancerTypeSeedOneByName((String) object.getString("cancerTypeSeedOne")));
		order.setCancerTypeSeedTwo(
				cancerTypeService.getCancerTypeSeedTwoByName((String) object.getString("cancerTypeSeedTwo")));
		if (l.size() > 0) {
			order.setCommissioner(l.get(0));
		}

		// 得到图片下载路径
		GetImage getImage = new GetImage();
		Map<String, String> map = getImage.getImage(object.getString("downloadUrl"));
		FileInfo fi = new FileInfo();
		fi.setFilePath(map.get("path"));
		fi.setFileName(map.get("fileName"));
		fi.setFileType("jpg");
		fi.setState("1");
		/*
		 * User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		 * fi.setUploadUser(user);
		 */
		fi.setUseType("1");
		this.sampleOrderDao.saveOrUpdate(fi);
		// 把该图片关联到订单
		order.setState("3");
		order.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);

		this.sampleOrderDao.saveOrUpdate(order);

		// 保存数据
	}

	/**
	 * 查询数据，返回字符串
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String outputJsonByString(String userId) throws Exception {
		// 查询数据
		// 根据微信号,查询user,并且把user存到订单的世和专员
		String sampleJson = this.sampleOrderDao.selectSampleOrderByList(userId);
		return sampleJson;
	}

	public String selectSampleOrderDetail(String id) {
		return sampleOrderDao.selectSampleOrderDetail(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderItem(SampleOrder sc, String itemDataJson, String changeLogItem) throws Exception {
		List<SampleOrderItem> saveItems = new ArrayList<SampleOrderItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderItem scp = new SampleOrderItem();
			// 将map信息读入实体类
			scp = (SampleOrderItem) sampleOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleOrder(sc);
			// scp.setReceiveDate(new Date());

			saveItems.add(scp);
		}
		sampleOrderDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("SampleOrder");
			li.setModifyContent(changeLogItem);
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}
	}
	
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public boolean findTypeName(String id) throws Exception {
		
		
		return sampleOrderDao.findTypeName(id);
		
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOrderItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOrderItem scp = sampleOrderDao.get(SampleOrderItem.class, id);
//			scp
			String sc = "订单明细："+"删除了ID为"+id+"且条形码为"+scp.getSlideCode()+"的数据";
			sampleOrderDao.delete(scp);
			
			if (ids != null && !"".equals(ids)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getSampleOrder().getId());
				li.setClassName("SampleOrder");
				li.setModifyContent(sc);
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
			
		}
		
	}

	// 根据模板ID加载子表明细
	public List<Map<String, String>> setTemplateItem(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.sampleOrderDao.setTemplateItem(code);
		List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SampleCancerTempPersonnel ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("checkOutTheAge", ti.getCheckOutTheAge());
				if (ti.getSampleCancerTemp().getOrderNumber() != null)
					map.put("sampleorder", ti.getSampleCancerTemp().getOrderNumber());
				else
					map.put("sampleorder", "");
				if (ti.getTumorCategory() != null)
					map.put("tumorCategory", ti.getTumorCategory().getId());
				else
					map.put("tumorCategory", "");
				map.put("familyRelation", ti.getFamilyRelation());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据模板ID加载子表明细2
	public List<Map<String, String>> setSampleOrderItem2(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.sampleOrderDao.setSampleOrderItem(code);
		List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SampleCancerTempItem ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("drugDate", ti.getDrugDate().toString());
				map.put("useDrugName", ti.getUseDrugName());
				map.put("effectOfProgress", ti.getEffectOfProgress());
				map.put("effectOfProgressSpeed", ti.getEffectOfProgressSpeed());
				map.put("geneticTestHistory", ti.getGeneticTestHistory());
				map.put("sampleDetectionName", ti.getSampleDetectionName());
				map.put("sampleExonRegion", ti.getSampleExonRegion());
				map.put("sampleDetectionResult", ti.getSampleDetectionResult());

				map.put("sampleOrder", ti.getSampleCancerTemp().getOrderNumber());

				mapList.add(map);
			}

		}
		return mapList;
	}

	// 查询订单编号是否存在 并查询 是否使用
	public int selSampleOrderId(String id) {
		int flg = 0;// 不存在
		Long total = this.sampleOrderDao.selSampleOrderId(id);
		if (total > 0) {

			Long total2 = this.sampleOrderDao.selSampleOrderId2(id);
			if (total2 > 0) {
				flg = 1;// 存在并被使用
			} else {
				flg = 2;// 存在未被使用
			}
		}

		return flg;
	}

	// 保存订单
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveUpload(String itemDataJson) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrder so = new SampleOrder();
			so = (SampleOrder) sampleOrderDao.Map2Bean(map, so);
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			so.setCreateUser(user);
			so.setCreateDate(new Date());

			// 生成订单号
			// String id = this.sampleOrderDao.selmaxOrderIdByorderType(so
			// .getOrderType());
			// if (id == null || "".equals(id)) {
			// if (so.getOrderType().equals("非眼癌")) {
			// so.setId("01000001");
			// } else if (so.getOrderType().equals("眼癌")) {
			// so.setId("02000001");
			// } else if (so.getOrderType().equals("科研")) {
			//
			// }
			// } else {
			// String id2 = codingRuleService.getorderId("SampleOrder",
			// 00000000, 8, null, so.getOrderType());
			// so.setId(id2);
			// }
			// 查询检测项目是否在
			Product p = this.sampleOrderDao.selProductBynName(so.getProductName());
			if (p == null) {
				so.setProductName(null);
			} else {
				so.setProductId(p.getId());
			}

			// 查询销售代表是否存在
			User u = this.sampleOrderDao.selUserByName(so.getInfoUserStr());
			if (u == null) {
				so.setCommissioner(null);
			} else {
				so.setCommissioner(u);
			}

			// 查询医院是否存在
			CrmCustomer c = this.sampleOrderDao.selCrmCustomerByName(so.getMedicalInstitutions());
			if (c == null) {
				so.setMedicalInstitutions(null);
			}
			sampleOrderDao.saveOrUpdate(so);
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(SampleOrder sc, Map jsonMap, String changeLog, String changeLogItem, String flag,String log)
			throws Exception {
		String jsonStr = "";
		String samId = "";
		String saId = "";
		String type = "";
		SampleOrderChange sa = null;
		DecimalFormat df = new DecimalFormat("0.00");
		if (sc != null) {
			if (sc.getWhiteBloodCellNum() != null 
					&& !"".equals(sc.getWhiteBloodCellNum())
					&& sc.getPercentageOfLymphocytes() != null
					&& !"".equals(sc.getWhiteBloodCellNum())) {
				String fm = df.format(sc.getWhiteBloodCellNum() * sc.getPercentageOfLymphocytes());
				sc.setLymphoidCellSeries(fm);
				double bf = 100 / (sc.getWhiteBloodCellNum() * sc.getPercentageOfLymphocytes());
				String sj = df.format(bf);
				sc.setCounterDrawBlood(sj);

			}
			if(sc.getLymphoidCellSeries()!=null
					&&!"".equals(sc.getLymphoidCellSeries())) {
				double bf = 100 / Double.valueOf(sc.getLymphoidCellSeries());
				String sj = df.format(bf);
				sc.setCounterDrawBlood(sj);
			}
			
			
			List<SampleOrder> soList = new ArrayList<SampleOrder>();
			soList = sampleReceiveDao.selSampleOrderCCOIProduct(sc);
			Date drawBloodTime = sc.getDrawBloodTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
			String drawBloodTimeStr = sdf.format(drawBloodTime);
			String lc = "";
			if (sc.getRound().length() == 1) {
				lc = "0" + sc.getRound();
			} else {
				lc = sc.getRound();
			}
			String barcode = "";
			if(sc.getBarcodeNum()!=null
					&&!"".equals(sc.getBarcodeNum())) {
				sc.setBarcode(sc.getProductId()+ sc.getBarcodeNum() + "-" + lc + "-" + drawBloodTimeStr);
			}else {
				if (soList.size() == 0) {
					barcode = codingRuleService.getSampleOrderBarcode("SampleOrder", sc, 0000, 4, null);
					// Integer countBatch = sampleReceiveDao.selBarcodeLikeProduct(so);
				} else {
					if(soList.get(0).getBarcode()!=null
							&&!"".equals(soList.get(0).getBarcode())) {
						barcode = soList.get(0).getBarcode().substring(0, soList.get(0).getBarcode().indexOf("-"));//.getBarCode().substring(so.getBarcode().indexOf("-"));
					}else {
						barcode = codingRuleService.getSampleOrderBarcode("SampleOrder", sc, 0000, 4, null);
					}
				}
				String barcodeNum = barcode.substring(barcode.length()-4,barcode.length());
				sc.setBarcodeNum(barcodeNum);
				sc.setBarcode(barcode + "-" + lc + "-" + drawBloodTimeStr);
			}
			
			
			
			
			
//			计算采血管数
			if (sc.getCounterDrawBlood() != null
					&&!"".equals(sc.getCounterDrawBlood())) {
				
				double d  = (Double.parseDouble(sc.getCounterDrawBlood()))/9;
				int i = ((int)Math.ceil(d));
				sc.setHeparinTube(String.valueOf(i));
				
			} else {
				if (sc.getWhiteBloodCellNum() != null 
						&& !"".equals(sc.getWhiteBloodCellNum())
						&& sc.getPercentageOfLymphocytes() != null
						&& !"".equals(sc.getWhiteBloodCellNum())) {
					String fm = df.format(sc.getWhiteBloodCellNum() * sc.getPercentageOfLymphocytes());
					sc.setLymphoidCellSeries(fm);
					double bf = 100 / (sc.getWhiteBloodCellNum() * sc.getPercentageOfLymphocytes());
					String sj = df.format(bf);
					sc.setCounterDrawBlood(sj);
					double d  = Double.parseDouble(sc.getCounterDrawBlood())/9;
					int i = (int)Math.ceil(d);
					sc.setHeparinTube(String.valueOf(i));
				}
			}

			if ("EDIT".equals(flag)) {
				// 生成新的SampleOrder
				sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				String modelName = "SampleOrder";
				String markCode = "DD";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
				SampleOrder clone = (SampleOrder) sc.clone();
				String id = sc.getId();
				clone.setId(autoID);
				clone.setOldId(id);
				clone.setState("1");
				clone.setStateName("Complete");
				clone.setFlagValue("EDIT");
				SampleOrderItem scpp = new SampleOrderItem();
				List<SampleOrderItem> querySampleItem = sampleOrderDao.querySampleItem(id);
				for (SampleOrderItem scp : querySampleItem) {
					if (scp.getId() != null && !"".equals(scp.getId())) {
						if (scp.getSampleOrder() != null) {
							String scpId = scp.getSampleOrder().getId();
							// 老订单
							SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, scpId);
							scpp.setName(scp.getName());
							scpp.setGender(scp.getGender());
							scpp.setSampleCode(scp.getSampleCode());
							scpp.setSlideCode(scp.getSlideCode());
							scpp.setSampleType(scp.getSampleType());
							scpp.setSamplingDate(scp.getSamplingDate());
							scpp.setReceiveUser(scp.getReceiveUser());
							scpp.setReceiveDate(scp.getReceiveDate());
							scpp.setStateName(scp.getStateName());
							scpp.setSampleOrder(sampleOrder);
							commonDAO.saveOrUpdate(scpp);
						}
					}
					scp.setSampleOrder(clone);
				}
				sampleOrderDao.saveOrUpdate(clone);
				SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, id);
				sampleOrder.setState("0");
				sampleOrder.setStateName("无效");
				commonDAO.saveOrUpdate(sampleOrder);
				// 生成新的SampleOrderChange
				sa = new SampleOrderChange();
				String modelName1 = "SampleOrderChange";
				String markCode1 = "SOC";
				Date date1 = new Date();
				DateFormat format1 = new SimpleDateFormat("yy");
				String stime1 = format1.format(date1);
				String autoID1 = codingRuleService.getCodeByPrefix(modelName1, markCode1, stime1, 000000, 6, null);
				sa.setId(autoID1);
				sa.setChangeType("修改申请单");
				sa.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sa.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				User user = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				sa.setCreateUser(user);
				sa.setCreateDate(new Date());
				commonDAO.saveOrUpdate(sa);
				saId = sa.getId();
				// 生成新的SampleOrderChangeItem
				SampleOrderChangeItem sam = new SampleOrderChangeItem();
				String modelName2 = "SampleOrderChangeItem";
				String markCode2 = "SOCI";
				Date date2 = new Date();
				DateFormat format2 = new SimpleDateFormat("yy");
				String stime2 = format2.format(date2);
				String autoID2 = codingRuleService.getCodeByPrefix(modelName2, markCode2, stime2, 000000, 6, null);
				sam.setId(autoID2);
				sam.setSampleOrder(clone);
				sam.setSampleOrderChange(sa);
				sam.setOldId(id);
				sam.setUpdateId(clone.getId());
				commonDAO.saveOrUpdate(sam);
				jsonStr = (String) jsonMap.get("sampleOrderItem");
				if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
					saveSampleOrderItem(clone, jsonStr, changeLogItem);
				}
				samId = clone.getId();
			} else if ("ADD".equals(flag)) {
				// 生成新的SampleOrder
				sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				String modelName = "SampleOrder";
				String markCode = "DD";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
				String id = sc.getId();
				sc.setId(autoID);
				sc.setOldId(id);
				sc.setFlagValue("ADD");
				SampleOrderItem scpp = new SampleOrderItem();
				List<SampleOrderItem> querySampleItem = sampleOrderDao.querySampleItem(id);
				for (SampleOrderItem scp : querySampleItem) {
					if (scp.getId() != null && !"".equals(scp.getId())) {
						if (scp.getSampleOrder() != null) {
							String scpId = scp.getSampleOrder().getId();
							// 老订单
							SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, scpId);
							scpp.setName(scp.getName());
							scpp.setGender(scp.getGender());
							scpp.setSampleCode(scp.getSampleCode());
							scpp.setSlideCode(scp.getSlideCode());
							scpp.setSampleType(scp.getSampleType());
							scpp.setSamplingDate(scp.getSamplingDate());
							scpp.setReceiveUser(scp.getReceiveUser());
							scpp.setReceiveDate(scp.getReceiveDate());
							scpp.setStateName(scp.getStateName());
							scpp.setSampleOrder(sampleOrder);
							commonDAO.saveOrUpdate(scpp);
						}
					}
					scp.setSampleOrder(sc);
				}
				sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sampleOrderDao.saveOrUpdate(sc);
				// 生成新的SampleOrderChange
				sa = new SampleOrderChange();
				String modelName1 = "SampleOrderChange";
				String markCode1 = "SOC";
				Date date1 = new Date();
				DateFormat format1 = new SimpleDateFormat("yy");
				String stime1 = format1.format(date1);
				String autoID1 = codingRuleService.getCodeByPrefix(modelName1, markCode1, stime1, 000000, 6, null);
				sa.setId(autoID1);
				sa.setChangeType("追加申请单");
				sa.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sa.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				User user = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				sa.setCreateUser(user);
				sa.setCreateDate(new Date());
				commonDAO.saveOrUpdate(sa);
				saId = sa.getId();
				// 生成新的SampleOrderChangeItem
				SampleOrderChangeItem sam = new SampleOrderChangeItem();
				String modelName2 = "SampleOrderChangeItem";
				String markCode2 = "SOCI";
				Date date2 = new Date();
				DateFormat format2 = new SimpleDateFormat("yy");
				String stime2 = format2.format(date2);
				String autoID2 = codingRuleService.getCodeByPrefix(modelName2, markCode2, stime2, 000000, 6, null);
				sam.setId(autoID2);
				sam.setSampleOrder(sc);
				sam.setSampleOrderChange(sa);
				sam.setOldId(sc.getOldId());
				sam.setUpdateId(sc.getId());
				commonDAO.saveOrUpdate(sam);
				jsonStr = (String) jsonMap.get("sampleOrderItem");
				if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
					saveSampleOrderItem(sc, jsonStr, changeLogItem);
				}
				samId = sc.getId();
				
//				List<SampleOrder> sf = commonDAO.findByProperty(SampleOrder.class, "id", samId);
//				if(sf.size()>0) {
//					for (SampleOrder so : sf) {
//						
//						if(!"".equals(so.getRound())) {
//							int i = Integer.parseInt(so.getRound());
//							System.out.println(i);
//							List<SampleFutureFind> sfff = sampleOrderDao.findId();
//							for (SampleFutureFind sfFind : sfff) {
//								int ii = Integer.parseInt(sfFind.getRound());
//								if(so.getFiltrateCode().equals(sfFind.getFiltrateCode())&&i==ii) {
//									sfFind.setDingDanCode(so.getId());
//									sfFind.setStateName(so.getStateName());
//								}
//							}
//						}
//						
//						
//					}
//				}
			} else {
				sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				commonDAO.merge(sc);
				jsonStr = (String) jsonMap.get("sampleOrderItem");
				if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
					saveSampleOrderItem(sc, jsonStr, changeLogItem);
				}
				samId = sc.getId();
				saId = "123";
				type = "0";
//				List<SampleOrder> sf = commonDAO.findByProperty(SampleOrder.class, "id", samId);
//				if(sf.size()>0) {
//					for (SampleOrder so : sf) {
//						
//						if(!"".equals(so.getRound())) {
//							int i = Integer.parseInt(so.getRound());
//							System.out.println(i);
//							List<SampleFutureFind> sfff = sampleOrderDao.findId();
//							for (SampleFutureFind sfFind : sfff) {
//								int ii = Integer.parseInt(sfFind.getRound());
//								if(so.getFiltrateCode().equals(sfFind.getFiltrateCode())&&i==ii) {
//									sfFind.setDingDanCode(so.getId());
//									sfFind.setStateName(so.getStateName());
//								}
//							}
//						}
//						
//						
//					}
//				}
				
			}
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("SampleOrder");
				li.setModifyContent(changeLog);
				if(!log.equals("")||log.equals("123")) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				
				commonDAO.saveOrUpdate(li);
			}

			/*
			 * String jsonStr = ""; jsonStr = (String) jsonMap.get("sampleOrderItem"); if
			 * (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			 * saveSampleOrderItem(sc, jsonStr, changeLogItem); }
			 */
			//新建订单时生成质检结果管理；先判断该订单是否有该表，有不动，没有新建
			List<QualityTestResultManage> qtrms = commonService.get(QualityTestResultManage.class, "sampleOrder.id", sc.getId());
			if(qtrms.size()>0) {
				
			}else {
				QualityTestResultManage qtrm = new QualityTestResultManage();
				String modelName = "QualityTestResultManage";
				String markCode = "QTRM";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);

				qtrm.setId(autoID);
				qtrm.setSampleOrder(sc);
				qtrm.setState("3");
				qtrm.setStateName("新建");
				commonDAO.saveOrUpdate(qtrm);
			}
		}
		return samId + "," + saId+ "," +type;
	}

	public SampleCancerTemp fuzZhi(SampleOrder so) throws Exception {
		SampleCancerTemp a = new SampleCancerTemp();
		a.setOrderNumber(so.getId());
		a.setId(so.getId());
		a.setName(so.getName());
		a.setGender(so.getGender());
		a.setBirthDate(so.getBirthDate());
		a.setDiagnosisDate(DateUtil.parse(so.getDiagnosisDate()));
		a.setDicType(so.getDicType());
		a.setSampleStage(so.getSampleStage());
		a.setInspectionDepartment(so.getInspectionDepartment());
		a.setCrmProduct(so.getCrmProduct());
		a.setProductId(so.getProductId());
		a.setProductName(so.getProductName());
		a.setSamplingDate(so.getSamplingDate());
		a.setSamplingLocation(so.getSamplingLocation());
		a.setSamplingNumber(so.getSamplingNumber());
		a.setPathologyConfirmed(so.getPathologyConfirmed());
		// a.setBloodSampleDate(so.getBloodSampleDate());
		// a.setPlasmapheresisDate(so.getPlasmapheresisDate());
		a.setCommissioner(so.getCommissioner());
		a.setReceivedDate(so.getReceivedDate());
		a.setSampleTypeId(so.getSampleTypeId());
		a.setSampleTypeName(so.getSampleTypeName());
		a.setMedicalNumber(so.getMedicalNumber());
		a.setSampleCode(so.getSampleCode());
		a.setFamily(so.getFamily());
		a.setFamilyPhone(so.getFamilyPhone());
		a.setFamilySite(so.getFamilySite());
		a.setCrmCustomer(so.getCrmCustomer());
		a.setMedicalInstitutions(so.getMedicalInstitutions());
		a.setMedicalInstitutionsPhone(so.getMedicalInstitutionsPhone());
		a.setMedicalInstitutionsSite(so.getMedicalInstitutionsSite());
		a.setCrmDoctor(so.getCrmDoctor());
		a.setAttendingDoctor(so.getAttendingDoctor());
		a.setAttendingDoctorPhone(so.getAttendingDoctorPhone());
		a.setAttendingDoctorSite(so.getAttendingDoctorSite());
		a.setNote(so.getNote());
		a.setCreateUser(so.getCreateUser());
		a.setCreateDate(so.getCreateDate());
		a.setConfirmUser(so.getConfirmUser());
		a.setConfirmDate(so.getConfirmDate());
		a.setState(so.getState());
		a.setStateName(so.getStateName());

		return a;
	}

	// 微信端读取报告
	public List<FileInfo> findSampleReport(String id) {
		return sampleOrderDao.findSampleReport(id);
	}

	/**
	 * 用检测项目名字查找id
	 * 
	 * @param itemDataJson
	 * @return
	 * @throws Exception
	 */
	public String GetProductId(String pruductname) throws Exception {
		String product = "";
		if ("".equals(pruductname)) {
			product = "";
		} else {
			product = sampleOrderDao.GetProductId(pruductname);
		}

		return product;
	}

	/**
	 * 通过字典名称及类型得到id
	 * 
	 * @param pruductname
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public String GetDicTypeId(String pruductname, String type) throws Exception {
		String dicid = "";
		if ("".equals(pruductname)) {
			dicid = "";
		} else {
			dicid = sampleOrderDao.GetDicTypeId(pruductname, type);
		}

		return dicid;
	}

	/**
	 * 通过销售代表名字得到id
	 * 
	 * @param pruductname
	 * @return
	 * @throws Exception
	 */
	public String GetUserId(String pruductname) throws Exception {
		String id = "";
		if ("".equals(pruductname)) {
			id = "";
		} else {
			id = sampleOrderDao.GetUserId(pruductname);
		}

		return id;
	}

	/**
	 * 通过样本类型名字得到id
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String GetsampleTypeId(String name) throws Exception {
		String id = "";
		if ("".equals(name)) {
			id = "";
		} else {
			id = sampleOrderDao.GetsampleTypeId(name);
		}

		return id;
	}

	/**
	 * 通过样本单位名字得到id
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public String GetsampleUnitId(String name, String type) throws Exception {
		String id = "";
		if ("".equals(name)) {
			id = "";
		} else {
			id = sampleOrderDao.GetsampleUnitId(name, type);
		}
		return id;
	}

	public Map<String, Object> findSampleOrderTable(String type, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return sampleOrderDao.findSampleOrderTable(type, start, length, query, col, sort);
	}

	public Map<String, Object> findSampleOrderTables(String type, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return sampleOrderDao.findSampleOrderTables(type, start, length, query, col, sort);
	}

	public Map<String, Object> findSampleOrderDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOrderDao.findSampleOrderDialogList(start, length, query, col, sort);
	}

	public Map<String, Object> findSampleOrderCrmDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOrderDao.findSampleOrderCrmDialogList(start, length, query, col, sort);
	}

	public Map<String, Object> showSampleOrderTaskKanban(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOrderDao.showSampleOrderTaskKanban(start, length, query, col, sort);
	}

	public Map<String, Object> findSampleOrderItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		return sampleOrderDao.findSampleOrderItemTable(start, length, query, col, sort, id);
	}

	public Map<String, Object> searchOptions(String type) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> product = sampleOrderDao.productOpts(type);
		List<Object> sale = sampleOrderDao.saleOpts(type);
		List<Object> customer = sampleOrderDao.customerOpts(type);
		List<Object> doctor = sampleOrderDao.doctorOpts(type);
		map.put("product", product);
		map.put("sale", sale);
		map.put("customer", customer);
		map.put("doctor", doctor);
		return map;
	}

	public Map<String, Object> sampleOrderChart(String type, String query, String year, String month, String[] product,
			String[] customer, String[] sale, String[] doctor) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> hlist = sampleOrderDao.histogram(type, query, year, product, customer, sale, doctor);
		List<Object> plist = sampleOrderDao.pieChart(type, query, year, month, product, customer, sale, doctor);
		map.put("histogram", hlist);
		map.put("pieChart", plist);
		return map;
	}

	public Map<String, Object> sampleOrderSearchTable(String type, String year, String month, String[] product,
			String[] customer, String[] sale, String[] doctor, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOrderDao.sampleOrderSearchTable(type, year, month, product, customer, sale, doctor, start, length,
				query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addPurchaseApplyMakeUp(String id, SampleOrderChange sa) throws Exception {
		SampleOrderChangeItem soc = null;
		if (id != null && !"".equals(id)) {
			SampleOrder so = commonDAO.get(SampleOrder.class, id);
			so.setState("0");
			so.setStateName("无效");
			soc = new SampleOrderChangeItem();
			String modelName = "SampleOrderChangeItem";
			String markCode = "SOCI";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);

			soc.setId(autoID);
			soc.setSampleOrder(so);
			soc.setSampleOrderChange(sa);
			soc.setOldId(so.getId());
			commonDAO.saveOrUpdate(soc);
		}
		return soc.getId();
	}

	public Map<String, Object> findSampleOrderSelTable(Integer start, Integer length, String query, String col,
			String sort, String orderNum) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		String[] orderNums = orderNum.split(",");
		for (String ord : orderNums) {
			SampleOrder so = commonDAO.get(SampleOrder.class, ord);
			list.add(so);
		}
		map.put("recordsTotal", orderNums.length);
		map.put("recordsFiltered", orderNums.length);
		map.put("list", list);
		return map;

	}

	public List<FileInfo> showPicture(String id) {
		return sampleOrderDao.showPicture(id);
	}

	public Map<String, Object> showStateCompleteSampleOrderList() {
		return sampleOrderDao.showStateCompleteSampleOrderList();
	}

	public Map<String, Object> findSampleStateNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return sampleOrderDao.selectSampleStateNewList(start, length, query, col, sort, id);
	}

	// 保存样本明细
	public void saveUploadOrdItem(SampleOrderItem orderItem) {
		sampleOrderDao.saveOrUpdate(orderItem);
		;
	}

	// 通过检测单 id 获取样本明细list
	public List<SampleOrderItem> getSampleOrderItemListPage(String orderItem) {
		return sampleOrderDao.getSampleOrderItemListPage(orderItem);
	}

	public List<SampleOrderItem> selectSampleOrder() {
		return sampleOrderDao.selectSampleOrder();
	}

	public Integer countSampleOrderItemNum(String id) {
		return sampleOrderDao.countSampleOrderItemNum(id);
	}

	public String selectMaxSlideCode(String id) {
		return sampleOrderDao.selectMaxSlideCode(id);
	}
}
