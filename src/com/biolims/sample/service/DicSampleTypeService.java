package com.biolims.sample.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.DicSampleTypeItem;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DicSampleTypeService {
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDicSampleTypeList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dicSampleTypeDao.selectDicSampleTypeList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DicSampleType i) throws Exception {

		dicSampleTypeDao.saveOrUpdate(i);

	}

	public DicSampleType get(String id) {
		DicSampleType dicSampleType = commonDAO.get(DicSampleType.class, id);
		return dicSampleType;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DicSampleType sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dicSampleTypeDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("dicSampleTypeItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDicSampleTypeItem(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicSampleTypeItem(DicSampleType sc, String itemDataJson)
			throws Exception {
		List<DicSampleTypeItem> saveItems = new ArrayList<DicSampleTypeItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DicSampleTypeItem scp = new DicSampleTypeItem();
			// 将map信息读入实体类
			scp = (DicSampleTypeItem) dicSampleTypeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDicSampleType(sc);

			saveItems.add(scp);

		}
		dicSampleTypeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicSampleTypeItem(String[] ids) throws Exception {
		for (String id : ids) {
			DicSampleTypeItem scp = dicSampleTypeDao.get(
					DicSampleTypeItem.class, id);
			dicSampleTypeDao.delete(scp);
		}
	}

	public Map<String, Object> findDicSampleTypeItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dicSampleTypeDao
				.selectDicSampleTypeItemList(scId, startNum, limitNum, dir,
						sort);
		List<DicSampleTypeItem> list = (List<DicSampleTypeItem>) result
				.get("list");
		return result;
	}

	public List<DicSampleType> findDicCountTableByType(String type)
			throws Exception {// jiaru

		List<DicSampleType> list = new ArrayList<DicSampleType>();
		list = dicSampleTypeDao.selectDicSampleTypeListByType(type);
		return list;
	}

	/**
	 * 
	 * 检索通用字典
	 * 
	 * @return list
	 */
	public List<DicSampleType> findDicType() {

		List<DicSampleType> list = commonDAO
				.find("from DicSampleType where  state = '1' order by orderNumber asc");
		return list;

	}

	public String getDicTypeJson() throws Exception {
		List<DicSampleType> list = findDicType();
		return JsonUtils.toJsonString(list);
	}

	// 通过样本名称查询
	public DicSampleType selectDicSampleTypeByName(String name)
			throws Exception {
		DicSampleType type = dicSampleTypeDao.selectDicSampleTypeByName(name);
		return type;
	}

	public Map<String, Object> showDialogDicSampleTypeTableJson(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return dicSampleTypeDao.showDialogDicSampleTypeTableJson(start,length,query,col,sort);
	}

	public Map<String, Object> findDicSampleTypeTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return dicSampleTypeDao.findDicSampleTypeTable(start, length, query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DicSampleType sc, Map jsonMap,String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			dicSampleTypeDao.saveOrUpdate(sc);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("DicSampleType");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			String logStr = "";
	}
   }
}
