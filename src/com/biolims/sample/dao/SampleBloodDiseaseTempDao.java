package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.SampleBloodDiseaseTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleTumorTemp;

@Repository
@SuppressWarnings("unchecked")
public class SampleBloodDiseaseTempDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInputTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleBloodDiseaseTemp where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleBloodDiseaseTemp> list = new ArrayList<SampleBloodDiseaseTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	
	
	public Map<String, Object> selectSampleInputCheckList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort ) {
		String key = " ";
		String hql = " from SampleInfo info where 1=1 and info.state = '18' ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql+key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	
	//根据code 查出SampleInfo里的两条数据
	public SampleInfo findSampleInfoByCode(String code){
		String hql ="from SampleInfo t where 1=1 and t.code = '"+code+"'";
		SampleInfo sampleInfo = (SampleInfo) this.getSession().createQuery(hql).uniqueResult();
		return sampleInfo;
	}
	
	
	//查询两次记录  
	public List<SampleBloodDiseaseTemp> findListBySampleInfo(String id){
		String hql = "from SampleBloodDiseaseTemp t where 1=1 and t.sampleInfo.id ='"+id+"'";
		List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询两次记录  
		public List<SampleBloodDiseaseTemp> findListByCode(String code){
			String hql = "from SampleBloodDiseaseTemp t where 1=1 and t.code ='"+code+"'";
			List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql).list();
			return list;
		}
	
	
//	//查询两次记录  
//	public List<SampleInputTemp> findListBySampleInfoCode(String id){
//		String hql = "from SampleInputTemp t where 1=1 and t.sampleInfo.id ='"+id+"'";
//		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
//		return list;
//	}
	//查询两次记录  
	public List<SampleBloodDiseaseTemp> findListBySampleInfoCode(String code){
		String hql = "from SampleBloodDiseaseTemp t where 1=1 and t.code ='"+code+"'";
		List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询两次记录  
	public List<SampleBloodDiseaseTemp> findListBySampleInfoCode1(String code){
		String hql = "from SampleBloodDiseaseTemp t where 1=1 and t.sampleInfo.code ='"+code+"'";
		List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//根据样本编号查询样本
	public List<SampleInfo> findIdByCode(String code){
		String hql = "from SampleInfo t where 1=1 and t.code='"+code+"'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据样本Id查询图片路径
	public List<FileInfo> findPathById(String id){
		String hql = "from FileInfo t where 1=1 and t.modelContentId='"+id+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//根据样本Id查询图片路径
	public List<FileInfo> findPathTempById(String id){
		String hql = "from FileInfo t where 1=1 and t='"+id+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询数据库中所有的录入信息
	public List<SampleInputTemp> findIdentity(){
		String hql = "from SampleInputTemp t where 1=1";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
}