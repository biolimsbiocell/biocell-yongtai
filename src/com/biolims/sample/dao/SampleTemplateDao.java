package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.system.sys.model.SamplePackSystem;
import com.biolims.sample.model.SampleInputTemp;
@Repository
@SuppressWarnings("unchecked")
public class SampleTemplateDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleTemplateList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleTemplate where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInputTemp> list = new ArrayList<SampleInputTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	//根据样本编号查询样本
	public List<SampleInfo> findIdByCode(String code){
		String hql = "from SampleInfo t where 1=1 and t.code='"+code+"'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据样本Id查询图片路径
	public List<FileInfo> findPathById(String id){
		String hql = "from FileInfo t where 1=1 and t.modelContentId='"+id+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//查询数据库中所有的录入信息
	public List<SampleInputTemp> findIdentity(){
		String hql = "from SampleInputTemp t where 1=1";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
//	public List<SampleInputTemp> findIdentity(){
//		String hql = "from SampleTemplate t where 1=1";
//		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
}