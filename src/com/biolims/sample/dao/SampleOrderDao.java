package com.biolims.sample.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.file.model.FileInfo;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleState;
import com.biolims.system.product.model.Product;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;

@Repository
@SuppressWarnings("unchecked")
public class SampleOrderDao extends BaseHibernateDao {

	// 微信条件查询列表
	public String selectSampleOrderByList(String userId) {
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		String key = " ";
		String hql = " from SampleOrder t where 1=1 ";
		if (userId != null) {
			String userhql = "from User u where u.weichatId='" + userId + "'";
			Long userCount = (Long) this.getSession().createQuery("select count(*) " + userhql).uniqueResult();
			if (userCount > 0) {
				hql = hql + "and t.commissioner.weichatId ='" + userId + "'";
			} else {
				String doctorhql = "from CrmDoctor d where d.weichatId='" + userId + "'";
				Long doctorCount = (Long) this.getSession().createQuery("select count(*) " + doctorhql).uniqueResult();
				if (doctorCount > 0) {
					hql = hql + "and t.crmDoctor.weichatId ='" + userId + "'";
				} else {
					String patienthql = "from CrmPatient p where p.weichatId='" + userId + "'";
					List<CrmPatient> crmPatient = this.getSession().createQuery(patienthql).list();
					if (crmPatient.size() > 0) {
						hql = hql + "and t.medicalNumber ='" + crmPatient.get(0).getId() + "'";
					} else {
						hql = hql + "and 1=0";
					}
				}
			}
		}
		list = this.getSession().createQuery(hql + key).list();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "";
		/**
		 * 拼接字符串
		 */
		StringBuffer strOpc = new StringBuffer();
		strOpc.append("[");
		for (int i = 0; i < list.size(); i++) {
			SampleOrder so = list.get(i);
			/**
			 * 收费信息
			 */
			String hql1 = " from CrmConsumerMarket where 1=1 and sampleOrder.id = '" + so.getId() + "'";
			List<CrmConsumerMarket> crmConsumerMarkets = new ArrayList<CrmConsumerMarket>();
			CrmConsumerMarket crmConsumerMarket = new CrmConsumerMarket();
			crmConsumerMarkets = this.getSession().createQuery(hql1).list();
			if (!crmConsumerMarkets.isEmpty()) {
				crmConsumerMarket = crmConsumerMarkets.get(0);
			}
			/**
			 * 时间转换
			 */
			if (so.getSamplingDate() != null)
				time = formatter.format(so.getSamplingDate());
			else
				time = "";

			if ((list.size() - 1) > i) {
				strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId() + "\",\"testName" + "\"" + ":" + "\""
						+ so.getName() + "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId() + "\",\"date" + "\""
						+ ":" + "\"" + time + "\",\"isFee" + "\"" + ":" + "\"" + crmConsumerMarket.getIsFee()
						+ "\",\"typeId" + "\"" + ":" + "\"" + so.getSampleTypeId() + "\"},");
			} else {
				strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId() + "\",\"testName" + "\"" + ":" + "\""
						+ so.getName() + "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId() + "\",\"date" + "\""
						+ ":" + "\"" + time + "\",\"isFee" + "\"" + ":" + "\"" + crmConsumerMarket.getIsFee()
						+ "\",\"typeId" + "\"" + ":" + "\"" + so.getSampleTypeId() + "\"}");
			}
		}
		strOpc.append("]");
		System.out.println(strOpc.toString());
		return strOpc.toString();
	}

	// 微信查询单条订单详情
	public String selectSampleOrderDetail(String id) {
		String key = " ";
		String hql = " from SampleOrder t where 1=1 and t.sampleInfoMain is null ";
		if (id != null && !id.equals("")) {
			hql = hql + "and t.id ='" + id + "'";
		}
		List<SampleOrder> list = new ArrayList<SampleOrder>();

		list = this.getSession().createQuery(hql + key).list();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "";
		/**
		 * 拼接字符串
		 */
		StringBuffer strOpc = new StringBuffer();
		strOpc.append("[");
		SampleOrder so = list.get(0);
		/**
		 * 收费信息
		 */
		String hql1 = " from CrmConsumerMarket where 1=1 and sampleOrder.id = '" + so.getId() + "'";
		List<CrmConsumerMarket> crmConsumerMarkets = new ArrayList<CrmConsumerMarket>();
		CrmConsumerMarket crmConsumerMarket = new CrmConsumerMarket();
		crmConsumerMarkets = this.getSession().createQuery(hql1).list();
		if (!crmConsumerMarkets.isEmpty()) {
			crmConsumerMarket = crmConsumerMarkets.get(0);
		}
		/**
		 * 样本状态详情
		 */
		String hql2 = " from SampleInfo where 1=1 and orderNum = '" + so.getId() + "'";
		StringBuffer sb = new StringBuffer();
		List<SampleInfo> sampleInfoList = new ArrayList<SampleInfo>();
		sampleInfoList = this.getSession().createQuery(hql2).list();
		if (!sampleInfoList.isEmpty()) {
			sb.append("[");
			for (int i = 0; i < sampleInfoList.size(); i++) {
				String hql3 = "from SampleState where 1=1 and sampleCode='" + sampleInfoList.get(i).getCode()
						+ "' order by endDate desc";
				List<SampleState> sampleState = this.getSession().createQuery(hql3).list();
				if (!sampleState.isEmpty()) {
					String sampleCode = sampleState.get(0).getSampleCode();
					String productName = sampleState.get(0).getProductName();
					String endDate = sampleState.get(0).getEndDate();
					String stageName = sampleState.get(0).getStageName();
					sb.append("{\"sampleCode\":" + "\"" + sampleCode + "\",\"productName\":" + "\"" + productName
							+ "\",\"endDate\":" + "\"" + endDate + "\"," + "\"stageName\":" + "\"" + stageName + "\"}");
				}
				if (i + 1 < sampleInfoList.size()) {
					sb.append(",");
				}
			}
			sb.append("]");
		} else {
			sb.append("\"null\"");
		}

		/* String[] states = sb.toString().split(","); */
		/**
		 * 时间转换
		 */
		if (so.getSamplingDate() != null)
			time = formatter.format(so.getSamplingDate());
		else
			time = "";
		strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId() + "\",\"testName" + "\"" + ":" + "\""
				+ so.getName() + "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId() + "\",\"date" + "\"" + ":" + "\""
				+ time + "\",\"isFee" + "\"" + ":" + "\"" + crmConsumerMarket.getIsFee() + "\",\"typeId" + "\"" + ":"
				+ "\"" + so.getSampleTypeId() + "\",\"states" + "\"" + ":" + sb.toString() + "}");
		strOpc.append("]");
		System.out.println(strOpc.toString());
		return strOpc.toString();
	}

	// 根据主表ID加载子表明细数据
	public Map<String, Object> setTemplateItem(String code) {
		String hql = "from SampleCancerTempPersonnel t where 1=1 and t.sampleCancerTemp.orderNumber='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<SampleCancerTempPersonnel> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据2
	public Map<String, Object> setSampleOrderItem(String code) {
		String hql = "from SampleCancerTempItem t where 1=1 and t.sampleCancerTemp.orderNumber='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<SampleCancerTempItem> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据3
	public List<SampleOrderItem> querySampleItem(String id) {
		String hql = "from SampleOrderItem t where 1=1 and t.sampleOrder.id='" + id + "'";
		List<SampleOrderItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表ID加载子表明细数据4
	public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
		String hql = "from SampleOrderPersonnel t where 1=1 and t.sampleOrder.id='" + id + "'";
		List<SampleOrderPersonnel> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询订单编号是否存在
	public Long selSampleOrderId(String id) {
		String hql = "from OrderEncodItem t where 1=1 and t.code='" + id + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	// 查询订单编号是否被使用
	public Long selSampleOrderId2(String id) {
		String hql = "from SampleOrder t where 1=1 and t.id='" + id + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	// 根据订单类型加载最大订单号
	public String selmaxOrderIdByorderType(String orderType) {
		String hql = "from SampleOrder t where 1=1 and t.orderType='" + orderType + "'";
		String total = (String) this.getSession().createQuery("select max(t.id) " + hql).uniqueResult();

		return total;
	}

	// 查询检测项目是否存在
	public Product selProductBynName(String productName) {
		String hql = "from Product t where 1=1 and t.name='" + productName + "'";
		Product p = (Product) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}

	// 查询销售代表是否存在
	public User selUserByName(String name) {
		String hql = "from User t where 1=1 and t.name='" + name + "'";
		User u = (User) this.getSession().createQuery(hql).uniqueResult();
		return u;
	}

	// 查询医院是否存在
	public CrmCustomer selCrmCustomerByName(String name) {
		String hql = "from CrmCustomer t where 1=1 and t.name='" + name + "'";
		CrmCustomer c = (CrmCustomer) this.getSession().createQuery(hql).uniqueResult();
		return c;
	}

	// 查看报告
	public List<FileInfo> findSampleReport(String id) {
		List<FileInfo> fileInfo = new ArrayList<FileInfo>();
		FileInfo info = new FileInfo();
		String hql = " from SampleReportItem where 1=1 and orderNum = '" + id + "'";
		List<SampleReportItem> sampleReportItem = new ArrayList<SampleReportItem>();
		sampleReportItem = this.getSession().createQuery(hql).list();
		for (int i = 0; i < sampleReportItem.size(); i++) {
			info = sampleReportItem.get(i).getReportFile();
			info.setFileNote(sampleReportItem.get(i).getId());
			fileInfo.add(info);
		}
		return fileInfo;
	}

	/**
	 * 通过项目名称得到id
	 * 
	 * @param pruductname
	 * @return
	 */
	public String GetProductId(String pruductname) {
		// List<Product> Productname = new ArrayList<Product>();
		// String name[] = pruductname.split(",");
		// for(int i=0;i<name.length; i++){
		//
		// }
		String hql = "select id from Product where 1=1 and name = '" + pruductname + "'";
		String productid = this.getSession().createQuery(hql).uniqueResult().toString();
		return productid;
	}

	/**
	 * 通过字典名称及类型得到id
	 * 
	 * @param name
	 * @param type 类型
	 * @return
	 */
	public String GetDicTypeId(String name, String type) {
		String hql = "select id from DicType where 1=1 and name = '" + name + "' and type = '" + type + "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String productid = ((obj == null) ? "" : obj).toString();
		return productid;
	}

	/**
	 * 通过销售代表名字得到id
	 * 
	 * @param name
	 * @return
	 */
	public String GetUserId(String name) {
		String hql = "select id from User where 1=1 and name = '" + name + "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String id = ((obj == null) ? "" : obj).toString();
		return id;
	}

	/**
	 * 通过样本类型名字得到id
	 * 
	 * @param name
	 * @return
	 */
	public String GetsampleTypeId(String name) {
		String hql = "select id from DicSampleType where 1=1 and name = '" + name + "'";
		String id = this.getSession().createQuery(hql).uniqueResult().toString();
		return id;
	}

	/**
	 * 通过样本单位名字得到id
	 * 
	 * @param name
	 * @return
	 */
	public String GetsampleUnitId(String name, String type) {
		String hql = "select id from DicUnit where 1=1 and type='" + type + "' and name = '" + name + "'";
		String id = this.getSession().createQuery(hql).uniqueResult().toString();

		return id;
	}

	// 按年查询订单柱状图
	public List findOrderNumberByYear(String dateYear) {

		// Calendar now = Calendar.getInstance();
		// String dateYear = String.valueOf(now.get(Calendar.YEAR));
		String sqlh = "select date_format(t.create_date,'%m') mon,count(t.id) num from sample_order t where date_format(t.create_date,'%Y')='"
				+ dateYear + "' group by date_format(t.create_date,'%m') order by mon asc";
		List list = this.getSession().createSQLQuery(sqlh).list();
		return list;
	}

	// 按年查询订单中检测项目病状图
	public List findProductByOrderByYear(String dateYear) {

		// Calendar now = Calendar.getInstance();
		// String dateYear = String.valueOf(now.get(Calendar.YEAR));
		String sqlp = "select t.product_name productName ,count(t.id) num  from sample_order t where date_format(t.create_date,'%Y')='"
				+ dateYear + "' group by t.product_name order by num desc";
		List<Object[]> listp = this.getSession().createSQLQuery(sqlp).list();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < listp.size(); i++) {
			if ((listp.get(i)[0]) != null && !"".equals(listp.get(i)[0])) {
				String b = listp.get(i)[0].toString();// NAME
				String c = listp.get(i)[1].toString();// Num
				String[] a = b.split(",");
				for (int j = 0; j < a.length; j++) {
					Set keys1 = map.keySet();
					if (keys1.contains(a[j])) {
						map.put(a[j], map.get(a[j]) + Integer.parseInt(c));
					} else {
						map.put(a[j], Integer.parseInt(c));
					}
				}
			}

		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 按年查询订单状态
	public List findStateByOrderByYear(String dateYear) {

//		String sqlp = "select t.batch_state_name batchStateName ,count(t.id) num  from sample_order t where date_format(t.create_date,'%Y')='"
//				+ dateYear + "' group by t.batch_state_name order by num desc";
//		List<Object[]> listp = this.getSession().createSQLQuery(sqlp).list();
		Map<String, Integer> map = new HashMap<String, Integer>();
//		map.put("订单录入完成", 0);
//		map.put("样本已接收", 0);
//		map.put("正在生产", 0);
//		map.put("已放行", 0);
//		map.put("运输完成", 0);
//		map.put("不合格", 0);

		String sqlzong = "from SampleOrder t where date_format(t.createDate,'%Y')='" + dateYear
				+ "' and t.state='1' and t.stateName='实际订单'  ";
		List<SampleOrder> listzong = this.getSession().createQuery(sqlzong).list();
		if (listzong.size() > 0) {
			map.put("细胞总数量", listzong.size());
		} else {
			map.put("细胞总数量", 0);
		}

		String sqlpeiyang = "from SampleOrder t where date_format(t.createDate,'%Y')='" + dateYear
				+ "' and t.state='1' and t.stateName='实际订单'  and (t.batchState='0' or t.batchState='1' or t.batchState='2' or t.batchState='3') ";
		List<SampleOrder> listpeiyang = this.getSession().createQuery(sqlpeiyang).list();
		if (listpeiyang.size() > 0) {
			map.put("在培养数量", listpeiyang.size());
		} else {
			map.put("在培养数量", 0);
		}

		String sqlwancheng = "from SampleOrder t where date_format(t.createDate,'%Y')='" + dateYear
				+ "' and t.state='1' and t.stateName='实际订单'  and t.batchState='4' ";
		List<SampleOrder> listwancheng = this.getSession().createQuery(sqlwancheng).list();
		if (listwancheng.size() > 0) {
			map.put("已完成细胞数量", listwancheng.size());
		} else {
			map.put("已完成细胞数量", 0);
		}

		String sqlwenti = "from SampleOrder t where date_format(t.createDate,'%Y')='" + dateYear
				+ "' and t.state='1' and t.stateName='实际订单'  and t.batchState='5' ";
		List<SampleOrder> listwenti = this.getSession().createQuery(sqlwenti).list();
		if (listwenti.size() > 0) {
			map.put("问题细胞数量", listwenti.size());
		} else {
			map.put("问题细胞数量", 0);
		}

//		for (int i = 0; i < listp.size(); i++) {
//			if ((listp.get(i)[0]) != null && !"".equals(listp.get(i)[0])) {
//				String b = listp.get(i)[0].toString();// NAME
//				String c = listp.get(i)[1].toString();// Num
//				String[] a = b.split(",");
//				for (int j = 0; j < a.length; j++) {
//					Set keys1 = map.keySet();
//					if(a[j]!=null
//							&&!"".equals(a[j])) {
//						if (keys1.contains(a[j])) {
//							map.put(a[j], map.get(a[j]) + Integer.parseInt(c));
//						} else {
//							map.put(a[j], Integer.parseInt(c));
//						}
//					}
//				}
//			}
//
//		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 按月查询订单中检测项目病状图
	public List findProductByOrderByMonth(String dateYearMonth) {

		String sqlp = "select t.product_name productName ,count(t.id) num  from sample_order t where date_format(t.create_date,'%Y-%m')='"
				+ dateYearMonth + "' group by t.product_name order by num desc";
		List<Object[]> listp = this.getSession().createSQLQuery(sqlp).list();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < listp.size(); i++) {
			if ((listp.get(i)[0]) != null && !"".equals(listp.get(i)[0])) {
				String b = listp.get(i)[0].toString();// NAME
				String c = listp.get(i)[1].toString();// Num
				String[] a = b.split(",");
				for (int j = 0; j < a.length; j++) {
					Set keys1 = map.keySet();
					if (keys1.contains(a[j])) {
						map.put(a[j], map.get(a[j]) + Integer.parseInt(c));
					} else {
						map.put(a[j], Integer.parseInt(c));
					}
				}
			}
		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 订单图片路径
	public List<String> findPic(String id) {
		String sql = "select t.file_path  from t_file_info t where t.model_content_id='" + id + "'";
		List<String> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<FileInfo> selectFileInfoList(String sId) throws Exception {
		String key = " and modelContentId = '" + sId + "' order by uploadTime DESC";
		String hql = "from FileInfo where 1=1 ";
		List<FileInfo> result = this.getSession().createQuery(hql + key).list();
		return result;
	}

	public Map<String, Object> findSampleOrderTable(String type, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder where 1=1 and (flagValue = null or flagValue = '')";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (type != null) {
			key += " and (orderType = '" + type + "' or orderType = '')";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder where 1=1 and (flagValue = null or flagValue = '')";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				if (col.indexOf("-") != -1) {
//					col = col.replace("-", ".");
//				}
//				if (col.equals("id")) {
//					key += " order by createDate desc ";
//				} else {
//					key += " order by " + col + " " + sort;
//				}
//			}
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 出库明细(选择实验)
	public Map<String, Object> findSampleOrderTables(String type, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassage where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
//		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
//		if (!"all".equals(scopeId)) {
//			key += " and scopeId='" + scopeId + "'";
//		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassage where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOrderDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
			}
			key += " order by " + col + " " + sort;
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOrderCrmDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
			}
			key += " order by " + col + " " + sort;
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showSampleOrderTaskKanban(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder where 1=1 and state = '10'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder where 1=1 and state = '10'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOrderItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrderItem where 1=1 and sampleOrder.id='" + id + "'";
		String key = "";
		// if(query!=null&&!"".equals(query)){
		// key=map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrderItem where 1=1 and sampleOrder.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<Object> productOpts(String type) {
		String sql = "select id,name,(select count(*) from sample_order o where find_in_set(s.id,o.product_id) and o.order_type = '"
				+ type + "') from sys_product s";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> saleOpts(String type) {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join t_user u on u.id=t.commissioner where 1=1 and t.order_type = '"
				+ type + "'  group by id  order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> customerOpts(String type) {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join CRM_CUSTOMER u on u.id=t.CRM_CUSTOMER where 1=1 and t.order_type ='"
				+ type + "' group by id order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> doctorOpts(String type) {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join CRM_DOCTOR u on u.id=t.CRM_DOCTOR where 1=1 and t.order_type ='"
				+ type + "' group by id order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> histogram(String type, String query, String year, String[] product, String[] customer,
			String[] sale, String[] doctor) throws Exception {
		String hql = "select DATE_FORMAT(s.create_date,'%m') mon,count(*) from sample_order s where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (type != null) {
			key += " and s.order_type = '" + type + "'";
		}
		if (year != null && !"".equals(year)) {
			key += " and DATE_FORMAT(s.create_date,'%Y')='" + year + "'";
		}
		if (product != null && product.length > 0) {
			key += " and (";
			for (int a = 0; a < product.length; a++) {
				if (a != product.length - 1) {
					key += " find_in_set('" + product[a] + "',s.product_id)>0 or";
				} else {
					key += " find_in_set('" + product[a] + "',s.product_id)>0 )";
				}
			}
		}
		if (customer != null && customer.length > 0) {
			key += " and (";
			for (int b = 0; b < customer.length; b++) {
				if (b != customer.length - 1) {
					key += " s.crm_customer='" + customer[b] + "' or";
				} else {
					key += " s.crm_customer='" + customer[b] + "' )";
				}
			}
		}
		if (sale != null && sale.length > 0) {
			key += " and (";
			for (int c = 0; c < sale.length; c++) {
				if (c != sale.length - 1) {
					key += " s.commissioner='" + sale[c] + "' or";
				} else {
					key += " s.commissioner='" + sale[c] + "' )";
				}
			}
		}
		if (doctor != null && doctor.length > 0) {
			key += " and (";
			for (int d = 0; d < doctor.length; d++) {
				if (d != doctor.length - 1) {
					key += " s.crm_doctor='" + doctor[d] + "' or";
				} else {
					key += " s.crm_doctor='" + doctor[d] + "' )";
				}
			}
		}
		key += " group by mon";
		List<Object> list = this.getSession().createSQLQuery(hql + key).list();
		return list;
	}

	public List<Object> pieChart(String type, String query, String year, String month, String[] product,
			String[] customer, String[] sale, String[] doctor) throws Exception {
		String hql = "select s.product_name,count(*) from sample_order s where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (type != null) {
			key += " and s.order_type = '" + type + "'";
		}
		if (month != null && !"".equals(month)) {
			key += " and DATE_FORMAT(s.create_date,'%Y-%m')='" + year + "-" + month + "'";
		} else {
			if (year != null && !"".equals(year)) {
				key += " and DATE_FORMAT(s.create_date,'%Y')=" + year;
			}
		}
		if (product != null && product.length > 0) {
			key += " and (";
			for (int a = 0; a < product.length; a++) {
				if (a != product.length - 1) {
					key += " find_in_set('" + product[a] + "',s.product_id)>0 or";
				} else {
					key += " find_in_set('" + product[a] + "',s.product_id)>0 )";
				}
			}
		}
		if (customer != null && customer.length > 0) {
			key += " and (";
			for (int b = 0; b < customer.length; b++) {
				if (b != customer.length - 1) {
					key += " s.crm_customer='" + customer[b] + "' or";
				} else {
					key += " s.crm_customer='" + customer[b] + "' )";
				}
			}
		}
		if (sale != null && sale.length > 0) {
			key += " and (";
			for (int c = 0; c < sale.length; c++) {
				if (c != sale.length - 1) {
					key += " s.commissioner='" + sale[c] + "' or";
				} else {
					key += " s.commissioner='" + sale[c] + "' )";
				}
			}
		}
		if (doctor != null && doctor.length > 0) {
			key += " and (";
			for (int d = 0; d < doctor.length; d++) {
				if (d != doctor.length - 1) {
					key += " s.crm_doctor='" + doctor[d] + "' or";
				} else {
					key += " s.crm_doctor='" + doctor[d] + "' )";
				}
			}
		}
		key += " group by s.product_name";
		List<Object> list = this.getSession().createSQLQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> sampleOrderSearchTable(String type, String year, String month, String[] product,
			String[] customer, String[] sale, String[] doctor, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder s where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (type != null) {
			key += " and orderType = '" + type + "'";
		}
		if (month != null && !"".equals(month)) {
			key += " and DATE_FORMAT(s.createDate,'%Y-%m')='" + year + "-" + month + "'";
		} else {
			if (year != null && !"".equals(year)) {
				key += " and DATE_FORMAT(s.createDate,'%Y')=" + year;
			}
		}
		if (product != null && product.length > 0) {
			key += " and (";
			for (int a = 0; a < product.length; a++) {
				if (a != product.length - 1) {
					key += " find_in_set('" + product[a] + "',s.productId)>0 or";
				} else {
					key += " find_in_set('" + product[a] + "',s.productId)>0 )";
				}
			}
		}
		if (customer != null && customer.length > 0) {
			key += " and (";
			for (int b = 0; b < customer.length; b++) {
				if (b != customer.length - 1) {
					key += " s.crmCustomer.id='" + customer[b] + "' or";
				} else {
					key += " s.crmCustomer.id='" + customer[b] + "' )";
				}
			}
		}
		if (sale != null && sale.length > 0) {
			key += " and (";
			for (int c = 0; c < sale.length; c++) {
				if (c != sale.length - 1) {
					key += " s.commissioner.id='" + sale[c] + "' or";
				} else {
					key += " s.commissioner.id='" + sale[c] + "' )";
				}
			}
		}
		if (doctor != null && doctor.length > 0) {
			key += " and (";
			for (int d = 0; d < doctor.length; d++) {
				if (d != doctor.length - 1) {
					key += " s.crmDoctor.id='" + doctor[d] + "' or";
				} else {
					key += " s.crmDoctor.id='" + doctor[d] + "' )";
				}
			}
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder s where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOrderSelTable(Integer start, Integer length, String query, String col,
			String sort, String orderNum) {
		String hql = "";
		return null;
	}

	public List<FileInfo> showPicture(String id) {
		String hql = " from FileInfo where 1=1 and modelContentId='" + id + "'";
		List<FileInfo> list = new ArrayList<FileInfo>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showStateCompleteSampleOrderList() {
		Map<String, Object> map = new HashMap<String, Object>();
		String hql = " from SampleOrder where 1=1 and state = '1'";
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		list = this.getSession().createQuery(hql).list();
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectSampleStateNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleState where 1=1 ";
		String key = "";
		if (!id.equals(""))
			key = key + "and sampleOrder.id ='" + id + "' ";
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleState where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			} else {
				key += " order by endDate desc ";
			}
			List<SampleState> list = new ArrayList<SampleState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 通过检测单id获取样本明细
	public List<SampleOrderItem> getSampleOrderItemListPage(String sampleOrderId) {
		String hql = "from SampleOrderItem where 1=1 and sampleOrder.id='" + sampleOrderId + "'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrderItem> selectSampleOrder() {
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		String hql = "from SampleOrderItem where 1=1 and (receiveUser ='' or receiveUser is null)";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Integer countPoducFromId(SampleOrder so) {
		String hql = "select count(*) from CrmPatient where 1=1 and id like '" + so.getProductId() + "%'";
		Long sum = (Long) getSession().createQuery(hql).uniqueResult();
		Integer result = sum.intValue();
		return result;
	}

	public Integer countSampleOrderItemNum(String id) {
		String countHql = "select count(*) from SampleOrderItem where sampleOrder='" + id + "'";
		Long sum = (Long) getSession().createQuery(countHql).uniqueResult();
		Integer result = sum.intValue();
		return result;
	}

	public String selectMaxSlideCode(String id) {
		String hql = "select MAX(slideCode) from SampleOrderItem where sampleOrder='" + id + "'";
		String result = (String) getSession().createQuery(hql).uniqueResult();
		return result;
	}

	public Map<String, Object> selectAgreementTaskTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PrimaryTask where 1=1 and state.id='1s'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from PrimaryTask where 1=1  and state.id='1s'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
				key += " order by " + col + " " + sort;
			}
			List<PrimaryTask> list = new ArrayList<PrimaryTask>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	/** 查询条形码 **/
	public List<SampleOrderItem> findSlideCode(String id) {
		String hql = "from SampleOrderItem where slideCode='" + id + "'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查找状态
	public boolean findTypeName(String id) {
		String hql = "from SampleOrderItem where sampleOrder.id='" + id + "'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = this.getSession().createQuery(hql).list();
		Boolean state = true;
		for (SampleOrderItem sampleOrderItem : list) {
			if (sampleOrderItem.getSampleType() != null) {

			} else {
				state = false;
				break;
			}

		}
		return state;
	}

	/**
	 * 判断筛选号是否重复
	 * 
	 * @param jianxie
	 * @return
	 */
	public List<SampleOrder> findFiltrateCode(String id, String sid) {
		String hql = "from SampleOrder where filtrateCode ='" + id + "'and id !='" + sid + "'";
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 判断随机号是否重复
	 * 
	 * @param id
	 * @return
	 */
	public List<SampleOrder> findRandomCode(String id, String sid) {
		String hql = "from SampleOrder where randomCode ='" + id + "'and id !='" + sid + "'";
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 判断姓名简写
	 * 
	 * @param id
	 * @return
	 */
	public List<SampleOrder> findAbbreviation(String id, String sid) {
		String hql = "from SampleOrder where abbreviation ='" + id + "'and id !='" + sid + "'";
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询产品+个数是否存在 不存在可以使用，存在要判断CCOI是否一致
	 * 
	 * @param string
	 * @return
	 */
	public List<SampleOrder> panduanBarcodeNum(String produc) {
		String hql = "from SampleOrder where barcode like'" + produc + "%' and stateName!='作废' ";
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrder> panduanBarcodeAndid(String barcode, String id) {
		String hql = "from SampleOrder where barcode ='" + barcode + "' and id!='" + id + "' and  stateName!='作废' ";
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Object findRound(String round,String state,String id) {
		String hql = "select Max(round) from SampleOrder where 1=1 and stateName !='作废'and filtrateCode='"+id+"'";
		Object a = this.getSession().createQuery(hql).uniqueResult();
		return a;
	}

	public List<SampleOrder> roundAndFiltrateCode(String sid,String round,String filtrateCode) {
		String sql = "from SampleOrder where 1=1 and id='"+sid+"'";
		List<SampleOrder> list = getSession().createQuery(sql).list();
		return list;
	}

	public List<SampleFutureFind> findId() {
		String hql = "from SampleFutureFind where 1=1";
		List<SampleFutureFind> soi = this.getSession().createQuery(hql).list();
		return soi;
	}

	public List<SampleOrder> findSamleDingDan(String filtrateCode) {
		String sql = "from SampleOrder where 1=1 and filtrateCode='"+filtrateCode+"'";
		List<SampleOrder> list = getSession().createQuery(sql).list();
		return list;
	}

	public SampleOrder findDingDanId(String round, String filtrateCode) {
		String sql = "from SampleOrder where 1=1 and filtrateCode='"+filtrateCode+"'and round='"+round+"'";
		SampleOrder list = (SampleOrder) getSession().createQuery(sql).uniqueResult();
		return list;
	}

}