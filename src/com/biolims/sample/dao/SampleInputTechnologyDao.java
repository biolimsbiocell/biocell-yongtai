package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTechnology;

@Repository
@SuppressWarnings("unchecked")
public class SampleInputTechnologyDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInputTechnologyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleInfo where 1=1 and state = '15' and classify ='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	public String codeParse(String val, Integer codeLength) {
		String result = null;
		if (codeLength != null && val.length() < codeLength) {
			int endVal = codeLength - val.length();
			String tem = "";
			for (int i = 0; i < endVal; i++) {
				tem = tem + 0;
			}
			result = tem + val;
		} else {
			result = val;
		}
		return result;
	}
	//查询编码的最大值
	public String selectModelTotalByType(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(sampleCode) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}
	
	
	
	//根据samoleCode查询
	public SampleInfo findByCode(String code) throws Exception{
		String hql ="from SampleInfo t where 1=1 and t.code='"+code+"'";
		SampleInfo list = (SampleInfo) this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
}