package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.DicSampleTypeItem;
import com.biolims.sample.model.SampleOrder;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DicSampleTypeDao extends BaseHibernateDao {
	public Map<String, Object> selectDicSampleTypeList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from DicSampleType where 1=1 ";
		// if (mapForQuery != null)
		// key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DicSampleType> list = new ArrayList<DicSampleType>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = " order by " + sort + " " + dir;
			} else {
				key = " order by orderNumber ASC";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public List<DicSampleType> selectDicSampleTypeListByType(String type)
			throws Exception {// jiaru
		String hql = " from DicSampleType where 1=1";//
		String key = "";
		if (type != null) {
			key = key + " and  func like '%" + type + "%'";
		}
		List<DicSampleType> list = new ArrayList<DicSampleType>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过样本名称查询
	public DicSampleType selectDicSampleTypeByName(String name)
			throws Exception {
		String hql = " from DicSampleType where 1=1 and name='" + name + "'";
		DicSampleType list = new DicSampleType();
		list = (DicSampleType) this.getSession().createQuery(hql)
				.uniqueResult();
		return list;
	}

	public Map<String, Object> selectDicSampleTypeItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DicSampleTypeItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dicSampleType.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DicSampleTypeItem> list = new ArrayList<DicSampleTypeItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<DicSampleTypeItem> selectDicSampleTypeItemByid(String id,
			String pid) {
		String hql = "from DicSampleTypeItem where dicSampleType.id='" + id
				+ "' " + pid + "";
		List<DicSampleTypeItem> list = new ArrayList<DicSampleTypeItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showDialogDicSampleTypeTableJson(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from DicSampleType where 1=1";
			String key = "";
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from DicSampleType where 1=1";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					key+=" order by "+col+" "+sort;
				}
				List<SampleOrder> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
	}

	public Map<String, Object> findDicSampleTypeTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  DicSampleType where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		//String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		//if(!"all".equals(scopeId)){
		//	key+=" and scopeId='"+scopeId+"'";
		//}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from DicSampleType where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<DicSampleType> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}