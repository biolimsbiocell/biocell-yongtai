package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleSystemItem;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SampleReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleReceiveList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		String key = "";
		String hql = " from SampleReceive where 1=1 ";
		if (!"".equals(type) && type != null) {
			key = " and type='" + type + "' ";
		}
		if (mapForQuery != null && mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			// key = key + " and state not in('0','1')";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleReceive> list = new ArrayList<SampleReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by state desc,id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/** 条件查询样本明细 */
	public Map<String, Object> selectSampleItemList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from SampleReceiveItem where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSampleReceiveItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleReceive.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by sampleCode asc";

			}

			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSampleSystemItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleSystemItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleReceive.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleSystemItem> list = new ArrayList<SampleSystemItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/** 异常样本 */
	public Map<String, Object> selectSampleAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleReceiveItem where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 查询开箱检验合格的样本
	public Map<String, Object> selectSampleReceiveItemListByIsGood(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from SampleReceiveItem where 1=1";
		String key = "";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// // 查询样本接收临时表通过 去到样本处理
	// public Map<String, Object> selectPlasmaReceiveTempList(Integer startNum,
	// Integer limitNum, String dir, String sort) throws Exception {
	// String hql = "from PlasmaReceiveTemp where 1=1 and state='1'";
	// String key = "";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<PlasmaReceiveTemp> list = new ArrayList<PlasmaReceiveTemp>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// } else {
	//
	// key = key + " order by sampleCode DESC";
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	// 查询核酸接收临时表 通过的样本去到核酸提取
	// public Map<String, Object> selectDnaReceiveTempList(Integer startNum,
	// Integer limitNum, String dir, String sort) throws Exception {
	// String hql = "from DnaReceiveTemp where 1=1 and state='1'";
	// String key = "";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<DnaReceiveTemp> list = new ArrayList<DnaReceiveTemp>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// } else {
	//
	// key = key + " order by code DESC";
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/*
	 * 根据明细id查询明细信息
	 */
	public List<SampleReceiveItem> selectItemList(String tid) {
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive='" + tid + "'";
		List<SampleReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据明细样本号查询明细信息
	 */
	public SampleReceiveItem selectItemByCode(String tid) {
		String hql = "from SampleReceiveItem where 1=1 and sampleCode='" + tid + "'";
		SampleReceiveItem list = (SampleReceiveItem) this.getSession().createQuery(hql).uniqueResult();
		return list;
	}

	/*
	 * 根据样本编号查询开箱检验明细信息
	 */
	public SampleReceiveItem getSampleReceiveItem(String code) {
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive in (select id from SampleReceive where state='1') and sampleCode='"
				+ code + "' and method='1'";
		SampleReceiveItem list = (SampleReceiveItem) this.getSession().createQuery(hql).uniqueResult();
		return list;
	}

	/**
	 * 根据工作流状态查询样本
	 */
	public List<SampleReceiveItem> selectReceiveList(String stateName) {
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive.stateName=1";
		List<SampleReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据表名和样本号删除样本
	public Object selModelByCode(Object o, String code) {
		String hql = "";
		if (o.equals("PlasmaReceiveTemp")) {
			hql = "from PlasmaReceiveTemp where 1=1 and sampleCode='" + code + "'";
		} else {
			hql = "from " + o + " where 1=1 and code='" + code + "'";
		}
		Object obj = (Object) this.getSession().createQuery(hql).uniqueResult();
		return obj;
	}

	// 查询外部样本编号
	public List<String> getIdCard(String id) {
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive.id='" + id + "'";
		List<String> list = new ArrayList<String>();
		list = this.getSession().createQuery("select distinct idCard " + hql).list();
		return list;
	}

	// 根据外部样本号查询
	public List<SampleReceiveItem> setSampleReceiveItemBywbh(String idCard, String id) {
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive.id='" + id + "' and idCard='" + idCard + "'";
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据条码号查询订单明细信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getSampleOrderItemMapList(String code) throws Exception {
		String hql = "from SampleOrderItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.slideCode='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrderItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据条码号查询订单明细信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<SampleOrderItem> getSampleOrderItemList(String code) {
		String hql = "from SampleOrderItem t where 1=1 and  t.slideCode='" + code + "'";
		List<SampleOrderItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据条码号查询订单明细
	 * 
	 * @param tid
	 * @return
	 */
	public SampleOrderItem selectSampleOrderItemByCode(String tid, String id) {
		String hql = "from SampleOrderItem where 1=1 and slideCode='" + tid + "' and sampleOrder='" + id + "'";
		List<SampleOrderItem> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public String findDoctor(String gid) {
		String sql = "select t.crm_customer_id from crm_doctor t where t.id='" + gid + "'";
		String doctorId = (String) this.getSession().createSQLQuery(sql).uniqueResult();
		return doctorId;
	}

	public Map<String, Object> showSampleReceiveTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReceive where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleReceive where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showSampleFutureFindTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleFutureFind where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleFutureFind where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
				if (col.equals("id")) {
					key += "order by sampleState  asc ";
				} else {
					key += " order by " + col + " " + sort;
				}
			}
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showSampleReceiveItemListJson(String scId, Integer start, Integer length, String query,
			String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReceiveItem where 1=1 and sampleReceive.id='" + scId + "'";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleReceiveItem  where 1=1 and sampleReceive.id='" + scId + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleReceiveItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleOrderItem> scanCodeInsert(String id, String type, String slideCode) {
		String hql = "from SampleOrderItem where 1=1 and slideCode='" + slideCode + "' and sampleOrder.stateName='预订单'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = (List<SampleOrderItem>) getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SampleOrderItem> scanCodeInsertBybarcode(String id, String type, String slideCode) {
		String hql = "from SampleOrderItem where 1=1 and sampleOrder.barcode='" + slideCode + "' and sampleOrder.stateName='预订单'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = (List<SampleOrderItem>) getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrderItem> scanCodeInsertBySampleOrder(String id, String type, String slideCode) {
		String hql = "from SampleOrderItem where 1=1 and slideCode='" + slideCode + "' and sampleOrder.stateName!='作废'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = (List<SampleOrderItem>) getSession().createQuery(hql).list();
		return list;
	}

	public SampleReceiveItem getSampleReceiveItemByLabCode(String labCode) {
		String hql = "from SampleReceiveItem where 1=1 and labCode='" + labCode + "'";
		SampleReceiveItem soi = new SampleReceiveItem();
		soi = (SampleReceiveItem) getSession().createQuery(hql).uniqueResult();
		return soi;
	}

	public String findMaxSampleCodeByOrderCode(String sampleOrderId) {
		String hql = "select MAX(sampleCode) from SampleReceiveItem where sampleCode like '%" + sampleOrderId + "%'";
		String str = (String) this.getSession().createQuery(hql).uniqueResult();
		return str;
	}

	public Integer countSampleOrderItem(String orderCode) {
		String countHql = "select count(*) from SampleOrderItem where 1=1 and (receiveUser is null or receiveUser='') and sampleOrder.id='"
				+ orderCode + "'";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		Integer result = sumCount.intValue();
		return result;
	}

	public List<SampleReceiveItem> getSampleReceiveItemList(String id) {
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Integer countSampleOrderItemAll(String orderCode) {
		String countHql = "select count(*) from SampleOrderItem where 1=1 and sampleOrder.id='" + orderCode + "'";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		Integer result = sumCount.intValue();
		return result;
	}

	public List<SampleReceiveItem> getSampleReceiveItemOrderByOrderCode(String id) {
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		String hql = "from SampleReceiveItem where 1=1 and sampleReceive.id='" + id + "' group by orderCode";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrder> selSampleOrderCCOIProduct(SampleOrder so) {
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		String hql = "from SampleOrder where 1=1 and ccoi='" + so.getCcoi() + "' and productId='" + so.getProductId()
				+ "' and id!='" + so.getId() + "' and stateName !='作废' and (barcode is not null and barcode!='') ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Integer selBarcodeLikeProduct(SampleOrder so) {
		String hql = "select count(*) from SampleOrder where 1=1 and barcode like'" + so.getProductId() + "%' and id!='"
				+ so.getId() + "' and stateName!='作废'";
		Long sum = (Long) getSession().createQuery(hql).uniqueResult();
		Integer result = sum.intValue();
		return result;
	}

	public List<SampleReceiveItem> getSampleReceiveItemOrder(String orderCode, String id) {
		List<SampleReceiveItem> list = new ArrayList<SampleReceiveItem>();
		String hql = "from SampleReceiveItem where 1=1 and orderCode='" + orderCode + "' and sampleReceive.id='" + id
				+ "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrder> selSampleOrderCCOIProduct(String ccoi, String productid, String id) {
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		String hql = "from SampleOrder where 1=1 and ccoi='" + ccoi + "' and productId='" + productid + "' and id!='"
				+ id + "' and stateName !='作废' and (barcode is not null and barcode!='') ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleFutureFind> chaxunsj(String kssj, String jssj) {
		List<SampleFutureFind> list = new ArrayList<SampleFutureFind>();
		String hql = "from SampleFutureFind where 1=1 and sampleTime>='" + kssj + "' and sampleTime<='" + jssj
				+ "' group by sampleTime ORDER BY sampleTime ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<Object[]> chaxunsjid(String zzz, String filtrateCode) {
		String hql = "select happen_event,round from sample_future_find where 1=1 and sample_time='" + zzz
				+ "' and filtrate_code='" + filtrateCode + "' ORDER BY sample_time,happen_event ";
		List<Object[]> list = getSession().createSQLQuery(hql).list();
		return list;
	}

	public List<Object[]> chaxunsjAndid(String[] sjbb, String filtrateCode) {
		String sj = "";
		for (String sjb : sjbb) {
			if ("".equals(sj)) {
				sj = sj + "'" + sjb + "'";
			} else {
				sj = sj + ",'" + sjb + "'";
			}
		}
		String hql = "select happen_event,round,sample_time,ding_dan_code,sample_state,time_state,time_show_state from sample_future_find where 1=1 and sample_time in (" + sj
				+ ") and filtrate_code='" + filtrateCode + "' ORDER BY sample_time,happen_event ";
		List<Object[]> list = getSession().createSQLQuery(hql).list();
		return list;
	}

	public List<String> chaxunsjd(String[] sjbb) {
		String sj = "";
		for (String sjb : sjbb) {
			if ("".equals(sj)) {
				sj = sj + "'" + sjb + "'";
			} else {
				sj = sj + ",'" + sjb + "'";
			}
		}
//		List<SampleFutureFind> list = new ArrayList<SampleFutureFind>();
		String hql = "select filtrate_code from sample_future_find where 1=1 and sample_time in (" + sj
				+ ") group by filtrate_code ORDER BY sample_time,happen_event ";
//		list = getSession().createQuery(hql).list();
//		return list;
		List<String> list = getSession().createSQLQuery(hql).list();
		return list;
	}

	public SampleFutureFind selectSampleOrderByFiltrateCodeAndRound(String filtrateCode, int round) {
		String hql = "from SampleFutureFind where 1=1 and filtrateCode='" + filtrateCode + "' and round='" + round
				+ "' and happenEvent='1' ";
		SampleFutureFind soi = new SampleFutureFind();
		soi = (SampleFutureFind) getSession().createQuery(hql).uniqueResult();
		return soi;
	}

	public List<Object[]> findIDAndStates() {
		String sql = "select sff.round,sff.filtrate_code,so.round,so.filtrate_code from sample_future_find sff inner join sample_order so  on sff.round = so.round and sff.filtrate_code=so.filtrate_code where 1=1";
		List<Object[]> list = getSession().createSQLQuery(sql).list();
		return list;
	}

	public String selectSampleNumByCount(String filtrateCode, String string) {
		String aString = "";
		String sql = "select id from sample_order where 1=1 and filtrate_code='"+filtrateCode+"'and round='"+string+"'";
		List<Object> list = getSession().createSQLQuery(sql).list();
		if (!list.isEmpty()) {
			aString = list.get(0).toString();
		}
		return aString;
	}

	public String selectSampleNumByState(String filtrateCode, String round) {
		String aString = "";
		String sql = "select state_name from sample_order where 1=1 and filtrate_code='"+filtrateCode+"'and round='"+round+"'";
		List<Object> list = getSession().createSQLQuery(sql).list();
		if (!list.isEmpty()) {
			aString = list.get(0).toString();
		}
		return aString;
	}

	public SampleFutureFind findFiltrateCodeOfRound(String filtrateCode,String round) {
		String hql = "from SampleFutureFind where round='" + round +"'and filtrateCode='"+filtrateCode+"'and happenEvent='1' and timeShowState='1' ";
		SampleFutureFind soi = new SampleFutureFind();
		soi = (SampleFutureFind) getSession().createQuery(hql).uniqueResult();
		return soi;
	}
	
	public List<SampleOrder> findSampleOrderId(String filtrateCode) {
		String hql = "from SampleOrder where filtrateCode='"+filtrateCode+"'and stateName !='作废'";
		List<SampleOrder> soi = this.getSession().createQuery(hql).list();
		return soi;
	}

	public SampleOrder findRound(String round,String filtrateCode) {
		String hql = "from SampleOrder where round='" + round+"'and filtrateCode='"+filtrateCode+"'";
		SampleOrder so = new SampleOrder();
		so = (SampleOrder) getSession().createQuery(hql).uniqueResult();
		return so;
	}



}