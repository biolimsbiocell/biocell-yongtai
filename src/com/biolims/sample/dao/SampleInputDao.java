package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class SampleInputDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInputList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort,String createUser1) {
		String key = " ";
		String hql = " from SampleOrder t where 1=1 and (state ='3'  or state ='15' or (state = '2' and sampleFlag <>'2' and infoUserStr not like '%"+createUser1+"%') )";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			//key=" and (t.createUser1 !='"+createUser1+"' or t.createUser1 is null) and t.state = '15' and t.classify ='0'";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	public Map<String, Object> selectSampleInputList1(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort ,User user ,String state) {
		String userIds = user.getId();
		String key = " ";
		String hql = " from SampleOrder info where 1=1 and (state ='3'  or (state = '2' and sampleFlag <>'2' and infoUserStr not like '%"+user.getId()+"%') )";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	
	//查询两次记录  
		public List<SampleInputTemp> findListBySampleInfo(String id){
			String hql = "from SampleInputTemp t where 1=1 and t.sampleInfo.code ='"+id+"'";
			List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
			return list;
		}
	//根据样本编号查询个数
	public Long findByCode(String code){
		String hql="select count(*) from SampleInfo t where 1=1 and t.state=15 and t.code='"+code+"'";
		Long num = (Long) this.getSession().createQuery(hql).uniqueResult();
		return num;
	}
	
	//根据样本编号查询样本
	public SampleInfo findSampleInfoByCode(String code){
		String hql = "from SampleInfo t where 1=1 and t.code='"+code+"'";
		SampleInfo sf= (SampleInfo) this.getSession().createQuery(hql).uniqueResult();
		return sf;
	}	
	
	
	public SampleOrder findSampleOrderByCode(String code){
		String hql = "from SampleOrder t where 1=1 and t.id='"+code+"'";
		SampleOrder sf= (SampleOrder) this.getSession().createQuery(hql).uniqueResult();
		return sf;
	}	
	//根据样本编号查询样本
	public List<SampleInfo> findIdByCode(String code){
		String hql = "from SampleInfo t where 1=1 and t.code='"+code+"'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据样本Id查询图片路径
	public List<FileInfo> findPathById(String id){
		String hql = "from FileInfo t where 1=1 and t.id='"+id+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据文件名查询图片实体
	public FileInfo findFileName(String name){
		String hql = "from FileInfo t where 1=1 and t.fileName='"+name+"'";
		FileInfo list = (FileInfo)this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
	//查询数据库中所有的录入信息
	public List<SampleInput> findIdentity(){
		String hql = "from SampleInput t where 1=1";
		List<SampleInput> list = this.getSession().createQuery(hql).list();
		return list;
	}
}