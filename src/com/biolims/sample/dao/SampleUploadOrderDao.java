package com.biolims.sample.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleState;
import com.biolims.sample.model.SampleUploadOrder;
import com.biolims.system.product.model.Product;

@Repository
@SuppressWarnings("unchecked")
public class SampleUploadOrderDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleOrderList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleUploadOrder where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by receivedDate desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public List<SampleUploadOrder> selSampleOrder() {
		String key = " ";
		String hql = " from SampleUploadOrder where 1=1 ";
		List<SampleUploadOrder> list = this.getSession().createQuery(hql).list();
		return list;

	}
	// 微信条件查询列表
	public String selectSampleOrderByList(String userId) {
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		String key = " ";
		String hql = " from SampleOrder t where 1=1 ";
		if (userId != null) {
			String userhql = "from User u where u.weichatId='" + userId + "'";
			Long userCount = (Long) this.getSession()
					.createQuery("select count(*) " + userhql).uniqueResult();
			if (userCount > 0) {
				hql = hql + "and t.commissioner.weichatId ='" + userId + "'";
			} else {
				String doctorhql = "from CrmDoctor d where d.weichatId='"
						+ userId + "'";
				Long doctorCount = (Long) this.getSession()
						.createQuery("select count(*) " + doctorhql)
						.uniqueResult();
				if (doctorCount > 0) {
					hql = hql + "and t.crmDoctor.weichatId ='" + userId + "'";
				} else {
					String patienthql = "from CrmPatient p where p.weichatId='"
							+ userId + "'";
					List<CrmPatient> crmPatient = this.getSession()
							.createQuery(patienthql).list();
					if (crmPatient.size() > 0) {
						hql = hql + "and t.medicalNumber ='"
								+ crmPatient.get(0).getId() + "'";
					} else {
						hql = hql + "and 1=0";
					}
				}
			}
		}
		list = this.getSession().createQuery(hql + key).list();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "";
		/**
		 * 拼接字符串
		 */
		StringBuffer strOpc = new StringBuffer();
		strOpc.append("[");
		for (int i = 0; i < list.size(); i++) {
			SampleOrder so = list.get(i);
			/**
			 * 收费信息
			 * */
			String hql1 = " from CrmConsumerMarket where 1=1 and sampleOrder.id = '"
					+ so.getId() + "'";
			List<CrmConsumerMarket> crmConsumerMarkets = new ArrayList<CrmConsumerMarket>();
			CrmConsumerMarket crmConsumerMarket = new CrmConsumerMarket();
			crmConsumerMarkets = this.getSession().createQuery(hql1).list();
			if (!crmConsumerMarkets.isEmpty()) {
				crmConsumerMarket = crmConsumerMarkets.get(0);
			}
			/**
			 * 时间转换
			 */
			if (so.getSamplingDate() != null)
				time = formatter.format(so.getSamplingDate());
			else
				time = "";

			if ((list.size() - 1) > i) {
				strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"testName" + "\"" + ":" + "\"" + so.getName()
						+ "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"date" + "\"" + ":" + "\"" + time + "\",\"isFee"
						+ "\"" + ":" + "\"" + crmConsumerMarket.getIsFee()
						+ "\",\"typeId" + "\"" + ":" + "\""
						+ so.getSampleTypeId() + "\"},");
			} else {
				strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"testName" + "\"" + ":" + "\"" + so.getName()
						+ "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"date" + "\"" + ":" + "\"" + time + "\",\"isFee"
						+ "\"" + ":" + "\"" + crmConsumerMarket.getIsFee()
						+ "\",\"typeId" + "\"" + ":" + "\""
						+ so.getSampleTypeId() + "\"}");
			}
		}
		strOpc.append("]");
		System.out.println(strOpc.toString());
		return strOpc.toString();
	}

	// 微信查询单条订单详情
	public String selectSampleOrderDetail(String id) {
		String key = " ";
		String hql = " from SampleOrder t where 1=1 and t.sampleInfoMain is null ";
		if (id != null && !id.equals("")) {
			hql = hql + "and t.id ='" + id + "'";
		}
		List<SampleOrder> list = new ArrayList<SampleOrder>();

		list = this.getSession().createQuery(hql + key).list();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "";
		/**
		 * 拼接字符串
		 */
		StringBuffer strOpc = new StringBuffer();
		strOpc.append("[");
		SampleOrder so = list.get(0);
		/**
		 * 收费信息
		 * */
		String hql1 = " from CrmConsumerMarket where 1=1 and sampleOrder.id = '"
				+ so.getId() + "'";
		List<CrmConsumerMarket> crmConsumerMarkets = new ArrayList<CrmConsumerMarket>();
		CrmConsumerMarket crmConsumerMarket = new CrmConsumerMarket();
		crmConsumerMarkets = this.getSession().createQuery(hql1).list();
		if (!crmConsumerMarkets.isEmpty()) {
			crmConsumerMarket = crmConsumerMarkets.get(0);
		}
		/**
		 * 样本状态详情
		 * */
		String hql2 = " from SampleInfo where 1=1 and orderNum = '"
				+ so.getId() + "'";
		StringBuffer sb = new StringBuffer();
		List<SampleInfo> sampleInfoList = new ArrayList<SampleInfo>();
		sampleInfoList = this.getSession().createQuery(hql2).list();
		if (!sampleInfoList.isEmpty()) {
			sb.append("[");
			for (int i = 0; i < sampleInfoList.size(); i++) {
				String hql3 = "from SampleState where 1=1 and sampleCode='"
						+ sampleInfoList.get(i).getCode()
						+ "' order by endDate desc";
				List<SampleState> sampleState = this.getSession()
						.createQuery(hql3).list();
				if (!sampleState.isEmpty()) {
					String sampleCode = sampleState.get(0).getSampleCode();
					String productName = sampleState.get(0).getProductName();
					String endDate = sampleState.get(0).getEndDate();
					String stageName = sampleState.get(0).getStageName();
					sb.append("{\"sampleCode\":" + "\"" + sampleCode
							+ "\",\"productName\":" + "\"" + productName
							+ "\",\"endDate\":" + "\"" + endDate + "\","
							+ "\"stageName\":" + "\"" + stageName + "\"}");
				}
				if (i + 1 < sampleInfoList.size()) {
					sb.append(",");
				}
			}
			sb.append("]");
		} else {
			sb.append("\"null\"");
		}

		/* String[] states = sb.toString().split(","); */
		/**
		 * 时间转换
		 */
		if (so.getSamplingDate() != null)
			time = formatter.format(so.getSamplingDate());
		else
			time = "";
		strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId()
				+ "\",\"testName" + "\"" + ":" + "\"" + so.getName()
				+ "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId()
				+ "\",\"date" + "\"" + ":" + "\"" + time + "\",\"isFee" + "\""
				+ ":" + "\"" + crmConsumerMarket.getIsFee() + "\",\"typeId"
				+ "\"" + ":" + "\"" + so.getSampleTypeId() + "\",\"states"
				+ "\"" + ":" + sb.toString() + "}");
		strOpc.append("]");
		System.out.println(strOpc.toString());
		return strOpc.toString();
	}

	// 添加子表的dao
	public Map<String, Object> selectSampleOrderPersonnelList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleOrderPersonnel where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleOrder.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrderPersonnel> list = new ArrayList<SampleOrderPersonnel>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSampleOrderItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleOrderItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleOrder.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询订单的样本
	public Map<String, Object> selectSampleOrderInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleOrder.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据
	public Map<String, Object> setTemplateItem(String code) {
		String hql = "from SampleCancerTempPersonnel t where 1=1 and t.sampleCancerTemp.orderNumber='"
				+ code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<SampleCancerTempPersonnel> list = this.getSession()
				.createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据2
	public Map<String, Object> setSampleOrderItem(String code) {
		String hql = "from SampleCancerTempItem t where 1=1 and t.sampleCancerTemp.orderNumber='"
				+ code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<SampleCancerTempItem> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据3
	public List<SampleOrderItem> querySampleItem(String id) {
		String hql = "from SampleOrderItem t where 1=1 and t.sampleOrder.id='"
				+ id + "'";
		List<SampleOrderItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表ID加载子表明细数据4
	public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
		String hql = "from SampleOrderPersonnel t where 1=1 and t.sampleOrder.id='"
				+ id + "'";
		List<SampleOrderPersonnel> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 查询订单编号是否存在
	public Long selSampleOrderId(String id) {
		String hql = "from OrderEncodItem t where 1=1 and t.code='" + id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	// 查询订单编号是否被使用
	public Long selSampleOrderId2(String id) {
		String hql = "from SampleOrder t where 1=1 and t.id='" + id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	// 根据订单类型加载最大订单号
	public String selmaxOrderIdByorderType(String orderType) {
		String hql = "from SampleOrder t where 1=1 and t.orderType='"
				+ orderType + "'";
		String total = (String) this.getSession()
				.createQuery("select max(t.id) " + hql).uniqueResult();

		return total;
	}

	// 查询检测项目是否存在
	public Product selProductBynName(String productName) {
		String hql = "from Product t where 1=1 and t.name='" + productName
				+ "'";
		Product p = (Product) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}

	// 查询销售代表是否存在
	public User selUserByName(String name) {
		String hql = "from User t where 1=1 and t.name='" + name + "'";
		User u = (User) this.getSession().createQuery(hql).uniqueResult();
		return u;
	}

	// 查询医院是否存在
	public CrmCustomer selCrmCustomerByName(String name) {
		String hql = "from CrmCustomer t where 1=1 and t.name='" + name + "'";
		CrmCustomer c = (CrmCustomer) this.getSession().createQuery(hql)
				.uniqueResult();
		return c;
	}

	// 查看报告
	public List<FileInfo> findSampleReport(String id) {
		List<FileInfo> fileInfo = new ArrayList<FileInfo>();
		FileInfo info = new FileInfo();
		String hql = " from SampleReportItem where 1=1 and orderNum = '" + id
				+ "'";
		List<SampleReportItem> sampleReportItem = new ArrayList<SampleReportItem>();
		sampleReportItem = this.getSession().createQuery(hql).list();
		for (int i = 0; i < sampleReportItem.size(); i++) {
			info = sampleReportItem.get(i).getReportFile();
			info.setFileNote(sampleReportItem.get(i).getId());
			fileInfo.add(info);
		}
		return fileInfo;
	}
	
	/**
	 * 通过项目名称得到id
	 * @param pruductname
	 * @return
	 */
	public String GetProductId(String pruductname) {
//		List<Product> Productname = new ArrayList<Product>();
//		String name[] = pruductname.split(",");
//		for(int i=0;i<name.length; i++){
//			
//		}
		String hql = "select id from Product where 1=1 and name = '" + pruductname
				+ "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String productid = ((obj==null)?"":obj).toString();
		return productid;
	}
	
	/**
	 * 通过字典名称及类型得到id
	 * 
	 * @param name 
	 * @param type 类型
	 * @return
	 */
	public String GetDicTypeId(String name,String type) {
		String hql = "select id from DicType where 1=1 and name = '" + name
				+ "' and type = '" + type + "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String productid = ((obj==null)?"":obj).toString();
		return productid;
	}
	
	/**
	 * 通过销售代表名字得到id
	 * 
	 * @param name 
	 * @return
	 */
	public String GetUserId(String name) {
		String hql = "select id from User where 1=1 and name = '" + name
				+ "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String id = ((obj==null)?"":obj).toString();
		return id;
	}
	
	/**
	 * 通过样本类型名字得到id
	 * 
	 * @param name 
	 * @return
	 */
	public String GetsampleTypeId(String name) {
		String hql = "select id from DicSampleType where 1=1 and name = '" + name
				+ "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String id = ((obj==null)?"":obj).toString();
		return id;
	}
}