package com.biolims.sample.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderChangeItem;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleState;
import com.biolims.system.product.model.Product;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SampleOrderChangeDao extends BaseHibernateDao {

	// 微信条件查询列表
	public String selectSampleOrderByList(String userId) {
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		String key = " ";
		String hql = " from SampleOrderChange t where 1=1 ";
		if (userId != null) {
			String userhql = "from User u where u.weichatId='" + userId + "'";
			Long userCount = (Long) this.getSession()
					.createQuery("select count(*) " + userhql).uniqueResult();
			if (userCount > 0) {
				hql = hql + "and t.commissioner.weichatId ='" + userId + "'";
			} else {
				String doctorhql = "from CrmDoctor d where d.weichatId='"
						+ userId + "'";
				Long doctorCount = (Long) this.getSession()
						.createQuery("select count(*) " + doctorhql)
						.uniqueResult();
				if (doctorCount > 0) {
					hql = hql + "and t.crmDoctor.weichatId ='" + userId + "'";
				} else {
					String patienthql = "from CrmPatient p where p.weichatId='"
							+ userId + "'";
					List<CrmPatient> crmPatient = this.getSession()
							.createQuery(patienthql).list();
					if (crmPatient.size() > 0) {
						hql = hql + "and t.medicalNumber ='"
								+ crmPatient.get(0).getId() + "'";
					} else {
						hql = hql + "and 1=0";
					}
				}
			}
		}
		list = this.getSession().createQuery(hql + key).list();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "";
		/**
		 * 拼接字符串
		 */
		StringBuffer strOpc = new StringBuffer();
		strOpc.append("[");
		for (int i = 0; i < list.size(); i++) {
			SampleOrder so = list.get(i);
			/**
			 * 收费信息
			 * */
			String hql1 = " from CrmConsumerMarket where 1=1 and sampleOrder.id = '"
					+ so.getId() + "'";
			List<CrmConsumerMarket> crmConsumerMarkets = new ArrayList<CrmConsumerMarket>();
			CrmConsumerMarket crmConsumerMarket = new CrmConsumerMarket();
			crmConsumerMarkets = this.getSession().createQuery(hql1).list();
			if (!crmConsumerMarkets.isEmpty()) {
				crmConsumerMarket = crmConsumerMarkets.get(0);
			}
			/**
			 * 时间转换
			 */
			if (so.getSamplingDate() != null)
				time = formatter.format(so.getSamplingDate());
			else
				time = "";

			if ((list.size() - 1) > i) {
				strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"testName" + "\"" + ":" + "\"" + so.getName()
						+ "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"date" + "\"" + ":" + "\"" + time + "\",\"isFee"
						+ "\"" + ":" + "\"" + crmConsumerMarket.getIsFee()
						+ "\",\"typeId" + "\"" + ":" + "\""
						+ so.getSampleTypeId() + "\"},");
			} else {
				strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"testName" + "\"" + ":" + "\"" + so.getName()
						+ "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId()
						+ "\",\"date" + "\"" + ":" + "\"" + time + "\",\"isFee"
						+ "\"" + ":" + "\"" + crmConsumerMarket.getIsFee()
						+ "\",\"typeId" + "\"" + ":" + "\""
						+ so.getSampleTypeId() + "\"}");
			}
		}
		strOpc.append("]");
		System.out.println(strOpc.toString());
		return strOpc.toString();
	}

	// 微信查询单条订单详情
	public String selectSampleOrderDetail(String id) {
		String key = " ";
		String hql = " from SampleOrderChange t where 1=1 and t.sampleInfoMain is null ";
		if (id != null && !id.equals("")) {
			hql = hql + "and t.id ='" + id + "'";
		}
		List<SampleOrderChangeItem> list = new ArrayList<SampleOrderChangeItem>();

		list = this.getSession().createQuery(hql + key).list();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = "";
		/**
		 * 拼接字符串
		 */
		StringBuffer strOpc = new StringBuffer();
		strOpc.append("[");
		SampleOrderChangeItem so = list.get(0);
		/**
		 * 收费信息
		 * */
		String hql1 = " from CrmConsumerMarket where 1=1 and sampleOrder.id = '"
				+ so.getId() + "'";
		List<CrmConsumerMarket> crmConsumerMarkets = new ArrayList<CrmConsumerMarket>();
		CrmConsumerMarket crmConsumerMarket = new CrmConsumerMarket();
		crmConsumerMarkets = this.getSession().createQuery(hql1).list();
		if (!crmConsumerMarkets.isEmpty()) {
			crmConsumerMarket = crmConsumerMarkets.get(0);
		}
		/**
		 * 样本状态详情
		 * */
		String hql2 = " from SampleInfo where 1=1 and orderNum = '"
				+ so.getId() + "'";
		StringBuffer sb = new StringBuffer();
		List<SampleInfo> sampleInfoList = new ArrayList<SampleInfo>();
		sampleInfoList = this.getSession().createQuery(hql2).list();
		if (!sampleInfoList.isEmpty()) {
			sb.append("[");
			for (int i = 0; i < sampleInfoList.size(); i++) {
				String hql3 = "from SampleState where 1=1 and sampleCode='"
						+ sampleInfoList.get(i).getCode()
						+ "' order by endDate desc";
				List<SampleState> sampleState = this.getSession()
						.createQuery(hql3).list();
				if (!sampleState.isEmpty()) {
					String sampleCode = sampleState.get(0).getSampleCode();
					String productName = sampleState.get(0).getProductName();
					String endDate = sampleState.get(0).getEndDate();
					String stageName = sampleState.get(0).getStageName();
					sb.append("{\"sampleCode\":" + "\"" + sampleCode
							+ "\",\"productName\":" + "\"" + productName
							+ "\",\"endDate\":" + "\"" + endDate + "\","
							+ "\"stageName\":" + "\"" + stageName + "\"}");
				}
				if (i + 1 < sampleInfoList.size()) {
					sb.append(",");
				}
			}
			sb.append("]");
		} else {
			sb.append("\"null\"");
		}

		/* String[] states = sb.toString().split(","); */
		/**
		 * 时间转换
		 */
	/*	if (so.getSamplingDate() != null)
			time = formatter.format(so.getSamplingDate());
		else
			time = "";
		strOpc.append("{\"orderNum" + "\"" + ":" + "\"" + so.getId()
				+ "\",\"testName" + "\"" + ":" + "\"" + so.getName()
				+ "\",\"medicalNumber" + "\"" + ":" + "\"" + so.getId()
				+ "\",\"date" + "\"" + ":" + "\"" + time + "\",\"isFee" + "\""
				+ ":" + "\"" + crmConsumerMarket.getIsFee() + "\",\"typeId"
				+ "\"" + ":" + "\"" + so.getSampleTypeId() + "\",\"states"
				+ "\"" + ":" + sb.toString() + "}");*/
		strOpc.append("]");
		System.out.println(strOpc.toString());
		return strOpc.toString();
	}

	// 根据主表ID加载子表明细数据
	public Map<String, Object> setTemplateItem(String code) {
		String hql = "from SampleCancerTempPersonnel t where 1=1 and t.sampleCancerTemp.orderNumber='"
				+ code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<SampleCancerTempPersonnel> list = this.getSession()
				.createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据2
	public Map<String, Object> setSampleOrderItem(String code) {
		String hql = "from SampleCancerTempItem t where 1=1 and t.sampleCancerTemp.orderNumber='"
				+ code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<SampleCancerTempItem> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主表ID加载子表明细数据3
	public List<SampleOrderItem> querySampleItem(String id) {
		String hql = "from SampleOrderItem t where 1=1 and t.sampleOrder.id='"
				+ id + "'";
		List<SampleOrderItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表ID加载子表明细数据4
	public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
		String hql = "from SampleOrderPersonnel t where 1=1 and t.sampleOrder.id='"
				+ id + "'";
		List<SampleOrderPersonnel> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 查询订单编号是否存在
	public Long selSampleOrderId(String id) {
		String hql = "from OrderEncodItem t where 1=1 and t.code='" + id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	// 查询订单编号是否被使用
	public Long selSampleOrderId2(String id) {
		String hql = "from SampleOrderChange t where 1=1 and t.id='" + id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	// 根据订单类型加载最大订单号
	public String selmaxOrderIdByorderType(String orderType) {
		String hql = "from SampleOrderChange t where 1=1 and t.orderType='"
				+ orderType + "'";
		String total = (String) this.getSession()
				.createQuery("select max(t.id) " + hql).uniqueResult();

		return total;
	}

	// 查询检测项目是否存在
	public Product selProductBynName(String productName) {
		String hql = "from Product t where 1=1 and t.name='" + productName
				+ "'";
		Product p = (Product) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}

	// 查询销售代表是否存在
	public User selUserByName(String name) {
		String hql = "from User t where 1=1 and t.name='" + name + "'";
		User u = (User) this.getSession().createQuery(hql).uniqueResult();
		return u;
	}

	// 查询医院是否存在
	public CrmCustomer selCrmCustomerByName(String name) {
		String hql = "from CrmCustomer t where 1=1 and t.name='" + name + "'";
		CrmCustomer c = (CrmCustomer) this.getSession().createQuery(hql)
				.uniqueResult();
		return c;
	}

	// 查看报告
	public List<FileInfo> findSampleReport(String id) {
		List<FileInfo> fileInfo = new ArrayList<FileInfo>();
		FileInfo info = new FileInfo();
		String hql = " from SampleReportItem where 1=1 and orderNum = '" + id
				+ "'";
		List<SampleReportItem> sampleReportItem = new ArrayList<SampleReportItem>();
		sampleReportItem = this.getSession().createQuery(hql).list();
		for (int i = 0; i < sampleReportItem.size(); i++) {
			info = sampleReportItem.get(i).getReportFile();
			info.setFileNote(sampleReportItem.get(i).getId());
			fileInfo.add(info);
		}
		return fileInfo;
	}

	/**
	 * 通过项目名称得到id
	 * 
	 * @param pruductname
	 * @return
	 */
	public String GetProductId(String pruductname) {
		// List<Product> Productname = new ArrayList<Product>();
		// String name[] = pruductname.split(",");
		// for(int i=0;i<name.length; i++){
		//
		// }
		String hql = "select id from Product where 1=1 and name = '"
				+ pruductname + "'";
		String productid = this.getSession().createQuery(hql).uniqueResult()
				.toString();
		return productid;
	}

	/**
	 * 通过字典名称及类型得到id
	 * 
	 * @param name
	 * @param type
	 *            类型
	 * @return
	 */
	public String GetDicTypeId(String name, String type) {
		String hql = "select id from DicType where 1=1 and name = '" + name
				+ "' and type = '" + type + "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String productid = ((obj == null) ? "" : obj).toString();
		return productid;
	}

	/**
	 * 通过销售代表名字得到id
	 * 
	 * @param name
	 * @return
	 */
	public String GetUserId(String name) {
		String hql = "select id from User where 1=1 and name = '" + name + "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		String id = ((obj == null) ? "" : obj).toString();
		return id;
	}

	/**
	 * 通过样本类型名字得到id
	 * 
	 * @param name
	 * @return
	 */
	public String GetsampleTypeId(String name) {
		String hql = "select id from DicSampleType where 1=1 and name = '"
				+ name + "'";
		String id = this.getSession().createQuery(hql).uniqueResult()
				.toString();
		return id;
	}

	/**
	 * 通过样本单位名字得到id
	 * 
	 * @param name
	 * @return
	 */
	public String GetsampleUnitId(String name, String type) {
		String hql = "select id from DicUnit where 1=1 and type='" + type
				+ "' and name = '" + name + "'";
		String id = this.getSession().createQuery(hql).uniqueResult()
				.toString();

		return id;
	}

	// 按年查询订单柱状图
	public List findOrderNumberByYear(String dateYear) {

		// Calendar now = Calendar.getInstance();
		// String dateYear = String.valueOf(now.get(Calendar.YEAR));
		String sqlh = "select date_format(t.create_date,'%m') mon,count(t.id) num from sample_order t where date_format(t.create_date,'%Y')='"
				+ dateYear
				+ "' group by date_format(t.create_date,'%m') order by mon asc";
		List list = this.getSession().createSQLQuery(sqlh).list();
		return list;
	}

	// 按年查询订单中检测项目病状图
	public List findProductByOrderByYear(String dateYear) {

		// Calendar now = Calendar.getInstance();
		// String dateYear = String.valueOf(now.get(Calendar.YEAR));
		String sqlp = "select t.product_name productName ,count(t.id) num  from sample_order t where date_format(t.create_date,'%Y')='"
				+ dateYear + "' group by t.product_name order by num desc";
		List<Object[]> listp = this.getSession().createSQLQuery(sqlp).list();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < listp.size(); i++) {
			if ((listp.get(i)[0]) != null && !"".equals(listp.get(i)[0])) {
				String b = listp.get(i)[0].toString();// NAME
				String c = listp.get(i)[1].toString();// Num
				String[] a = b.split(",");
				for (int j = 0; j < a.length; j++) {
					Set keys1 = map.keySet();
					if (keys1.contains(a[j])) {
						map.put(a[j], map.get(a[j]) + Integer.parseInt(c));
					} else {
						map.put(a[j], Integer.parseInt(c));
					}
				}
			}

		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 按月查询订单中检测项目病状图
	public List findProductByOrderByMonth(String dateYearMonth) {

		String sqlp = "select t.product_name productName ,count(t.id) num  from sample_order t where date_format(t.create_date,'%Y-%m')='"
				+ dateYearMonth + "' group by t.product_name order by num desc";
		List<Object[]> listp = this.getSession().createSQLQuery(sqlp).list();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < listp.size(); i++) {
			if ((listp.get(i)[0]) != null && !"".equals(listp.get(i)[0])) {
				String b = listp.get(i)[0].toString();// NAME
				String c = listp.get(i)[1].toString();// Num
				String[] a = b.split(",");
				for (int j = 0; j < a.length; j++) {
					Set keys1 = map.keySet();
					if (keys1.contains(a[j])) {
						map.put(a[j], map.get(a[j]) + Integer.parseInt(c));
					} else {
						map.put(a[j], Integer.parseInt(c));
					}
				}
			}
		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 订单图片路径
	public List<String> findPic(String id) {
		String sql = "select t.file_path  from t_file_info t where t.model_content_id='"
				+ id + "'";
		List<String> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<FileInfo> selectFileInfoList(String sId) throws Exception {
		String key = " and modelContentId = '" + sId
				+ "' order by uploadTime DESC";
		String hql = "from FileInfo where 1=1 ";
		List<FileInfo> result = this.getSession().createQuery(hql + key).list();
		return result;
	}

	public Map<String, Object> findSampleOrderChangeTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrderChange where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleOrderChange where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrderChangeItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public Map<String, Object> findSampleOrderItemTable(Integer start,
			Integer length, String query, String col, String sort, String id)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrderItem where 1=1 and sampleOrder.id='"
				+ id + "'";
		String key = "";
		// if(query!=null&&!"".equals(query)){
		// key=map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleOrderItem where 1=1 and sampleOrder.id='"
					+ id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<Object> productOpts() {
		String sql = "select id,name,(select count(*) from sample_order o where find_in_set(s.id,o.product_id)) from sys_product s";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> saleOpts() {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join t_user u on u.id=t.commissioner where 1=1  group by id  order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> customerOpts() {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join CRM_CUSTOMER u on u.id=t.CRM_CUSTOMER where 1=1 group by id order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> doctorOpts() {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join CRM_DOCTOR u on u.id=t.CRM_DOCTOR where 1=1 group by id order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> histogram(String query,String year, String[] product,
			String[] customer, String[] sale, String[] doctor) throws Exception {
		String hql = "select DATE_FORMAT(s.create_date,'%m') mon,count(*) from sample_order s where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (year != null && !"".equals(year)) {
			key += " and DATE_FORMAT(s.create_date,'%Y')='" + year + "'";
		}
		if (product != null && product.length > 0) {
			key += " and (";
			for (int a = 0; a < product.length; a++) {
				if (a != product.length - 1) {
					key += " find_in_set('" + product[a]
							+ "',s.product_id)>0 or";
				} else {
					key += " find_in_set('" + product[a]
							+ "',s.product_id)>0 )";
				}
			}
		}
		if (customer != null && customer.length > 0) {
			key += " and (";
			for (int b = 0; b < customer.length; b++) {
				if (b != customer.length - 1) {
					key += " s.crm_customer='" + customer[b] + "' or";
				} else {
					key += " s.crm_customer='" + customer[b] + "' )";
				}
			}
		}
		if (sale != null && sale.length > 0) {
			key += " and (";
			for (int c = 0; c < sale.length; c++) {
				if (c != sale.length - 1) {
					key += " s.commissioner='" + sale[c] + "' or";
				} else {
					key += " s.commissioner='" + sale[c] + "' )";
				}
			}
		}
		if (doctor != null && doctor.length > 0) {
			key += " and (";
			for (int d = 0; d < doctor.length; d++) {
				if (d != doctor.length - 1) {
					key += " s.crm_doctor='" + doctor[d] + "' or";
				} else {
					key += " s.crm_doctor='" + doctor[d] + "' )";
				}
			}
		}
		key += " group by mon";
		List<Object> list = this.getSession().createSQLQuery(hql + key).list();
		return list;
	}

	public List<Object> pieChart(String query,String year, String month, String[] product,
			String[] customer, String[] sale, String[] doctor) throws Exception {
		String hql = "select s.product_name,count(*) from sample_order s where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (month != null && !"".equals(month)) {
			key += " and DATE_FORMAT(s.create_date,'%Y-%m')='" + year + "-"
					+ month + "'";
		} else {
			if (year != null && !"".equals(year)) {
				key += " and DATE_FORMAT(s.create_date,'%Y')=" + year;
			}
		}
		if (product != null && product.length > 0) {
			key += " and (";
			for (int a = 0; a < product.length; a++) {
				if (a != product.length - 1) {
					key += " find_in_set('" + product[a]
							+ "',s.product_id)>0 or";
				} else {
					key += " find_in_set('" + product[a]
							+ "',s.product_id)>0 )";
				}
			}
		}
		if (customer != null && customer.length > 0) {
			key += " and (";
			for (int b = 0; b < customer.length; b++) {
				if (b != customer.length - 1) {
					key += " s.crm_customer='" + customer[b] + "' or";
				} else {
					key += " s.crm_customer='" + customer[b] + "' )";
				}
			}
		}
		if (sale != null && sale.length > 0) {
			key += " and (";
			for (int c = 0; c < sale.length; c++) {
				if (c != sale.length - 1) {
					key += " s.commissioner='" + sale[c] + "' or";
				} else {
					key += " s.commissioner='" + sale[c] + "' )";
				}
			}
		}
		if (doctor != null && doctor.length > 0) {
			key += " and (";
			for (int d = 0; d < doctor.length; d++) {
				if (d != doctor.length - 1) {
					key += " s.crm_doctor='" + doctor[d] + "' or";
				} else {
					key += " s.crm_doctor='" + doctor[d] + "' )";
				}
			}
		}
		key += " group by s.product_name";
		List<Object> list = this.getSession().createSQLQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> sampleOrderSearchTable(String year,
			String month, String[] product, String[] customer, String[] sale,
			String[] doctor, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder s where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (month != null && !"".equals(month)) {
			key += " and DATE_FORMAT(s.createDate,'%Y-%m')='" + year + "-"
					+ month + "'";
		} else {
			if (year != null && !"".equals(year)) {
				key += " and DATE_FORMAT(s.createDate,'%Y')=" + year;
			}
		}
		if (product != null && product.length > 0) {
			key += " and (";
			for (int a = 0; a < product.length; a++) {
				if (a != product.length - 1) {
					key += " find_in_set('" + product[a]
							+ "',s.productId)>0 or";
				} else {
					key += " find_in_set('" + product[a] + "',s.productId)>0 )";
				}
			}
		}
		if (customer != null && customer.length > 0) {
			key += " and (";
			for (int b = 0; b < customer.length; b++) {
				if (b != customer.length - 1) {
					key += " s.crmCustomer.id='" + customer[b] + "' or";
				} else {
					key += " s.crmCustomer.id='" + customer[b] + "' )";
				}
			}
		}
		if (sale != null && sale.length > 0) {
			key += " and (";
			for (int c = 0; c < sale.length; c++) {
				if (c != sale.length - 1) {
					key += " s.commissioner.id='" + sale[c] + "' or";
				} else {
					key += " s.commissioner.id='" + sale[c] + "' )";
				}
			}
		}
		if (doctor != null && doctor.length > 0) {
			key += " and (";
			for (int d = 0; d < doctor.length; d++) {
				if (d != doctor.length - 1) {
					key += " s.crmDoctor.id='" + doctor[d] + "' or";
				} else {
					key += " s.crmDoctor.id='" + doctor[d] + "' )";
				}
			}
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleOrderChange s where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOrderChangeItemList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrderChangeItem where 1=1 and sampleOrderChange.id='"+id+"'";
		String key = "";
		
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SampleOrderChangeItem where 1=1 and sampleOrderChange.id='"+id+"' ";
			List<SampleOrderChangeItem> list = new ArrayList<SampleOrderChangeItem>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleOrderChangeItem> findSampleOrderChangeItemByChange(String change) {
		String key = " and sampleOrderChange = '" + change+"'";
		String hql = "from SampleOrderChangeItem where 1=1 ";
		List<SampleOrderChangeItem> result = this.getSession().createQuery(hql + key).list();
		return result;
	}

	public SampleOrderChangeItem findChangeItemByOrderCode(String orderCode) {
		String hql = "from SampleOrderChangeItem where 1=1 and updateId = '"+orderCode+"'";
		SampleOrderChangeItem changeItem= (SampleOrderChangeItem)this.getSession().createQuery(hql).uniqueResult();
		return changeItem;
	}

}