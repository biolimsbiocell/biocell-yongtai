package com.biolims.sample.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.file.model.FileInfo;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleState;
import com.biolims.system.product.model.Product;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;

@Repository
@SuppressWarnings("unchecked")
public class SampleSearchDao extends BaseHibernateDao {
	
	/** 根据批次号查询步骤信息 */
	public List<CellPassageTemplate> searchCellPassageTemplates(String batch) {
		String hql = "from CellPassageTemplate where 1=1 and cellPassage.batch='" + batch + "' and (state is null or state='' or state='1') ";
		List<CellPassageTemplate> list = (List<CellPassageTemplate>) this.getSession().createQuery(hql).list();
		return list;
	}
	
	/** 根据批次号查询没作废的运输明细 */
	public List<TransportOrderCell> getTransportOrderCells(String batch) {
		String hql = "from TransportOrderCell where 1=1 and code='" + batch + "' and (efficacious is null or efficacious='' or efficacious='有效') ";
		List<TransportOrderCell> list = (List<TransportOrderCell>) this.getSession().createQuery(hql).list();
		return list;
	}
	
	/** 根据任务单号和步骤数取第一步工前准备的值 */
	public List<CellProductionRecord> searchCellPassageRecord(String id, String batch, String stepNums) {
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + id + "' and orderNum='"+stepNums+"' ";
		List<CellProductionRecord> list = (List<CellProductionRecord>) this.getSession().createQuery(hql).list();
		return list;
	}
	
	/** 根据批次号查询偏差子表，不是作废的单子*/
	public List<InfluenceProduct> getPlanInfluenceProducts(String batch) {
		String hql = "from InfluenceProduct where 1=1 and orderCode='" + batch + "' and deviationHandlingReport.stateName!='作废' ";
		List<InfluenceProduct> list = (List<InfluenceProduct>) this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findSampleOrderItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrderItem where 1=1 and sampleOrder.id='" + id + "'";
		String key = "";
		// if(query!=null&&!"".equals(query)){
		// key=map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrderItem where 1=1 and sampleOrder.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<Object> productOpts(String type) {
		String sql = "select id,name,(select count(*) from sample_order o where find_in_set(s.id,o.product_id) and o.order_type = '"
				+ type + "') from sys_product s";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public List<Object> saleOpts(String type) {
		String sql = "select u.id id, u.name name ,count(t.id) num  from sample_order t left join t_user u on u.id=t.commissioner where 1=1 and t.order_type = '"
				+ type + "'  group by id  order by num desc";
		List<Object> list = this.getSession().createSQLQuery(sql).list();
		return list;
	}

	public Map<String, Object> showCellPassageQualityItemJson(Integer start, Integer length, String query, String col,
			String sort, String batch, String ordernum) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageQualityItem where 1=1 and cellPassage.id='" + batch + "'";
		String key = " and stepNum='"+ordernum+"' ";
//		if (query != null) {
//			key = map2Where(query);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageQualityItem  where 1=1 and cellPassage.id='" + batch + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageQualityItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CellPassageQualityItem> getCellPassageQualityItemUnquality(String id, String orderNum) {
		String hql = "from CellPassageQualityItem where 1=1 and cellPassage.id='" + id + "' and stepNum='"+orderNum+"' and qualified='1' ";
		List<CellPassageQualityItem> list = (List<CellPassageQualityItem>) this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showSampleOrdersJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder where 1=1 ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}else {
				key += " order by id ";
			}
			List<SampleOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}


	public String map2Where(String query) throws Exception {
		Map<String, String> mapForQuery = (Map<String, String>)JsonUtils.toObjectByJson(query, Map.class);  
		String whereStr = "";
		if (mapForQuery != null) {
			Set<String> key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {

				String sOld = (String) it.next();
				String s = sOld;
				if (mapForQuery.get(sOld) != null && !mapForQuery.get(sOld).equals("")) {

					String[] sN;
					if (sOld.contains("##@@##")) {

						sN = sOld.split("##@@##");
						s = sN[0];
					}

					String ss = mapForQuery.get(sOld);
					
					if("style".equals(sOld)) {
						if("1".equals(ss)) {
							whereStr += " and state='1' and stateName='实际订单' ";
						}else if("2".equals(ss)) {
							whereStr += " and state='1' and stateName='实际订单' and (batchState='0' or batchState='1' or batchState='2' or batchState='3') ";
						}else if("3".equals(ss)) {
							whereStr += " and state='1' and stateName='实际订单'   and batchState='4' ";
						}else if("4".equals(ss)) {
							whereStr += " and state='1' and stateName='实际订单' and batchState='5' ";
						}
					}else if("time".equals(sOld)) {
						whereStr += " and " + "date_format(createDate,'%Y')='"
								+ ss.replaceAll("%", "") + "' ";
					}else {
						if (ss.contains("##@@##")) {

							String[] s1 = ss.split("##@@##");
							String q="";
							if(s1[0].equals("s")){
								q=">=";
							}else if(s1[0].equals("e")){
								q="<=";
							}else{
								q="=";
							}

							if (s.endsWith("Date") || s.endsWith("Time")||s.endsWith("Birth")) {

								String[] a = ss.split(":");
								if(s1.length>1){
								if (a.length == 0) {
									whereStr += " and " + s + " " + q + " " + s1[1];
								}
								if (a.length == 1) {
									whereStr += " and " + s + " " + q + " str_to_date('" + s1[1] + "','%Y-%m-%d')";
									
									
								}
								if (a.length == 2) {
									whereStr += " and " + s + " " + q + " str_to_date('" + s1[1] + "','%Y-%m-%d HH24:mi')";

								}
								if (a.length == 3) {
									whereStr += " and " + s + " " + q + " str_to_date('" + s1[1] + "','%Y-%m-%d H:i:s')";
								}
								}
							} else{
								if(s1.length>1){
									whereStr += " and " + s + " " + q + " " + s1[1];
								}
							}
								

						} else {
							String whereS = s.replace("-", ".");

							if (!mapForQuery.get(s).startsWith("%") && !mapForQuery.get(s).endsWith("%"))
								whereS = whereS + " = '" + mapForQuery.get(s) + "'";
							else {
								whereS = whereS + " like '" + mapForQuery.get(s) + "'";

							}

							whereStr += " and " + whereS;
						}
					}

				}

			}
		}
		return whereStr;
	}

	public List<SampleOrderItem> findBatchNumByCellCode(String batch) {
		
		String batchnum = "";
		String[] splitstr=batch.split("-");
	    for(int i=0;i<5;i++){
	    	if("".equals(batchnum)) {
	    		batchnum = batchnum + splitstr[i];
	    	}else {
	    		batchnum = batchnum + "-" + splitstr[i];
	    	}
	    }
		
		String hql = "from SampleOrderItem where 1=1 and slideCode like '" + batchnum + "%' and sampleOrder.stateName!='作废' ";
		List<SampleOrderItem> list = (List<SampleOrderItem>) this.getSession().createQuery(hql).list();
		return list;
	}


	


}