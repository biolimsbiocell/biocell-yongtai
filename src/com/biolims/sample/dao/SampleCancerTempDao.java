package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class SampleCancerTempDao extends BaseHibernateDao {
	
	public List<SampleCancerTemp> QuerySampleCancerTemp(String id){
		String hql = " from SampleCancerTemp where ORDER_NUMBER='"+id+"'";
		 List<SampleCancerTemp>  List= this.getSession().createQuery(hql).list();
		return List;
	}
	
	public Map<String, Object> selectSampleCancerTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleCancerTemp where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleCancerTemp> list = new ArrayList<SampleCancerTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	//明细查询（根据samplePersonnel）
		public Map<String, Object> selectSampleCancerTempPersonnelList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleCancerTempPersonnel where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCancerTemp.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleCancerTempPersonnel> list = new ArrayList<SampleCancerTempPersonnel>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}


		public Map<String, Object> selectSampleCancerTempItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleCancerTempItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCancerTemp.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleCancerTempItem> list = new ArrayList<SampleCancerTempItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<DicSampleType> selectDicSampleType(String type) throws Exception {//jiaru
			String hql = " from DicSampleType where 1=1";//
			String key = "";
			if (type != null) {
				key = key + " and  func like '%" + type + "%'";
			}
			
			key = key + " order by orderNumber ASC";
			List<DicSampleType> list = new ArrayList<DicSampleType>();
			list = this.getSession().createQuery(hql + key).list();
			return list;
			}
		public List<DicType> selectDicSampleType1(String type) throws Exception {//jiaru
			String hql = " from DicType where 1=1 and type='jkzk'";//
			String key = "";
//			if (type != null) {
//				key = key + " and  func like '%" + type + "%'";
//			}
//			
			key = key + " order by orderNumber ASC";
			List<DicType> list = new ArrayList<DicType>();
			list = this.getSession().createQuery(hql + key).list();
			return list;
		}
		
		public List<DicType> selectDicSampleType2(String type) throws Exception {//jiaru
			String hql = " from DicType where 1=1 and type='yyqk'";//
			String key = "";
		
			key = key + " order by orderNumber ASC";
			List<DicType> list = new ArrayList<DicType>();
			list = this.getSession().createQuery(hql + key).list();
			return list;
		}
		public List<DicType> selectDicSampleType3(String type) throws Exception {//jiaru
			String hql = " from DicType where 1=1 and type='sfcx'";//
			String key = "";
//			if (type != null) {
//				key = key + " and  func like '%" + type + "%'";
//			}
//			
			key = key + " order by orderNumber ASC";
			List<DicType> list = new ArrayList<DicType>();
			list = this.getSession().createQuery(hql + key).list();
			return list;
		}
		//根据主表ID加载子表明细数据1
		public List<SampleCancerTempPersonnel>  getPersonnel(String code){
			String hql="from SampleCancerTempPersonnel t where 1=1 and t.sampleCancerTemp.id='"+code+"'";
			List<SampleCancerTempPersonnel> list = this.getSession().createQuery(hql).list();
			return list;
		}
		//根据主表ID加载子表明细数据2
		public List<SampleCancerTempItem>  getCancerTempItem(String code){
			String hql="from SampleCancerTempItem t where 1=1 and t.sampleCancerTemp.id='"+code+"'";
			List<SampleCancerTempItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
	
	
}