package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.system.nextFlow.model.NextFlow;

@Repository
@SuppressWarnings("unchecked")
public class SampleAbnormalDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleAbnormal where 1=1 and state='1' ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleAbnormal> list = new ArrayList<SampleAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	public Map<String, Object> selectSampleAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String sampleStyle) {
		String key = " ";
		String hql = " from SampleAbnormal where 1=1 and state='1' ";
//		if (!"".equals(sampleStyle)) {
//			key += " and sampleStyle='" + sampleStyle + "'";
//		} else {
//			key += " and (sampleStyle='2' or sampleStyle is null)";
//		}
		if (mapForQuery != null)
			key = key+map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleAbnormal> list = new ArrayList<SampleAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectAbnormalTypeAll() {
		String key = " ";
		String hql = " from AbnomalType where 1=1 order by id asc";

		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<NextFlow> list = new ArrayList<NextFlow>();
		if (total > 0) {

			list = this.getSession().createQuery(hql + key).list();

		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectDicUnusualMethodAll() {
		String key = " ";
		String hql = " from DicType where 1=1 and type='yccl' order by orderNumber asc";

		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<NextFlow> list = new ArrayList<NextFlow>();
		if (total > 0) {

			list = this.getSession().createQuery(hql + key).list();

		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> showSampleAbnormalTableJson(Integer start,
			Integer length, String query, String col, String sort) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleAbnormal where 1=1 and state='1'";
		String key = "";
//		if(query!=null&&!"".equals(query)){
//			key=map2Where(query);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SampleAbnormal where 1=1 and state='1' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SampleAbnormal> list = new ArrayList<SampleAbnormal>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}
}