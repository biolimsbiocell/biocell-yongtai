package com.biolims.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleReceiveItem;

@Repository
@SuppressWarnings("unchecked")
public class SampleInputTempDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInputTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleInputTemp where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInputTemp> list = new ArrayList<SampleInputTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	
	
	public Map<String, Object> selectSampleInputCheckList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort ) {
		String key = " ";
		String hql = " from SampleInfo info where 1=1 and info.state = '18' and info.next is null and info.classify = '0'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql+key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}

	
	
	
	public Map<String, Object> selectSampleInputTempItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleInputTemp where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and sampleInfo.code='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql+key).uniqueResult();
		List<SampleInputTemp> list = new ArrayList<SampleInputTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectSampleInputList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleInput where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInput> list = new ArrayList<SampleInput>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
		
	}
	
	
	public SampleInfo findSampleInfoByCode(String code){
		String hql ="from SampleInfo t where 1=1 and t.code = '"+code+"'";
		SampleInfo sampleInfo = (SampleInfo) this.getSession().createQuery(hql).uniqueResult();
		return sampleInfo;
	}
	
	//查询两次记录  
	public List<SampleInputTemp> findListBySampleInfo(String id){
		String hql = "from SampleInputTemp t where 1=1 and t.sampleInfo.id ='"+id+"'";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询两次记录  
	public List<SampleInputTemp> findListBySampleInfoCode(String code){
		String hql = "from SampleInputTemp t where 1=1 and t.code ='"+code+"'";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//查询两次记录  
	public List<SampleInputTemp> findListBySampleInfoCode1(String code){
		String hql = "from SampleInputTemp t where 1=1 and t.sampleInfo.code ='"+code+"'";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据样本编号查询样本
	public List<SampleInfo> findIdByCode(String code){
		String hql = "from SampleInfo t where 1=1 and t.code='"+code+"'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据样本Id查询图片路径
	public List<FileInfo> findPathById(String id){
		String hql = "from FileInfo t where 1=1 and t.modelContentId='"+id+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//根据样本Id查询图片路径
	public List<FileInfo> findPathTempById(String id){
		String hql = "from FileInfo t where 1=1 and t='"+id+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询数据库中所有的录入信息
	public List<SampleInputTemp> findIdentity(){
		String hql = "from SampleInputTemp t where 1=1";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//根据Code查开箱检验明细是否有数据
	public List<SampleReceiveItem> findSampleReceiveItemByCode(String code){
		String hql="from SampleReceiveItem t where 1=1 and t.sampleCode='"+code+"'";
		List<SampleReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查出DicType (样本类型、证件类型、发票类型)
	public DicType findDicTypeByName(String name){
		String hql = "from DicType t where 1=1 and t.name='"+name+"'";
		DicType dic = (DicType) this.getSession().createQuery(hql).uniqueResult();
		return dic;
	}
}