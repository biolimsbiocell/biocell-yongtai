package com.biolims.sample.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.sample.service.SampleReceiveService;

public class SampleReceiveStateNoEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SampleReceiveService srService = (SampleReceiveService) ctx
				.getBean("sampleReceiveService");
		srService.stateNo(contentId);

		return "";
	}
}
