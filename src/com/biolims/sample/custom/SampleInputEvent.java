package com.biolims.sample.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.goods.mate.service.GoodsMaterialsReadyService;
import com.biolims.sample.service.SampleInputService;

public class SampleInputEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleInputService siService = (SampleInputService) ctx.getBean("sampleInputService");
		siService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
