package com.biolims.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.work.model.WorkOrder;

/**
 * @Title: Model
 * @Description: 样本类型明细
 * @author lims-platform
 * @date 2016-03-08 09:59:34
 * @version V1.0
 * 
 */
@Entity
@Table(name = "DIC_SAMPLE_TYPE_ITEM")
@SuppressWarnings("serial")
public class DicSampleTypeItem extends EntityDao<DicSampleTypeItem> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 描述 */
	private String name;
	/** 相关主表 */
	private DicSampleType dicSampleType;
	/** 检测项目Id */
	private String productId;
	// 检测项目
	private String productName;
	// 默认下一步ID
	private String dnextId;
	// 默认下一步name
	private String dnextName;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDnextId() {
		return dnextId;
	}

	public void setDnextId(String dnextId) {
		this.dnextId = dnextId;
	}

	public String getDnextName() {
		return dnextName;
	}

	public void setDnextName(String dnextName) {
		this.dnextName = dnextName;
	}

}