package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.product.model.Product;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;
/**   
 * @Title: Model
 * @Description: 检测订单
 * @author lims-platform
 * @date 2016-03-07 11:11:26
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_CANCER_TEMP")
@SuppressWarnings("serial")
public class SampleCancerTemp extends EntityDao<SampleCancerTemp> implements java.io.Serializable {
	/**订单号*/
	private String orderNumber;
	/**编码*/
	private String id;
	/**姓名*/
	private String name;
	/**性别*/
	private String gender;
	/**出生日期*/
	private Date birthDate;
	/**确诊日期*/
	private Date diagnosisDate;
	/**肿瘤类别*/
	private DicType dicType;
	/**分期*/
	private String sampleStage;
	
	/**送检科室*/
	private DicType inspectionDepartment;
	/**检测项目*/
	private Product crmProduct;
	
	/**检测项目*/
	private String productId;
	
	/**检测项目*/
	private String productName;
	
	
	/**采样日期*/
	private Date samplingDate;
	/**采样部位*/
	private DicType samplingLocation;
	/**样本编号*/
	private String samplingNumber;
	/**是否病理确认*/
	private String pathologyConfirmed;
	/**血液采样时间*/
	private Date bloodSampleDate;
	/**血浆分离时间*/
	private Date plasmapheresisDate;
	/**世和专员*/
	private User commissioner;
	/**收样日期*/
	private Date receivedDate;
	/**样本类型id*/
	private String sampleTypeId;
	/**样本类型*/
	private String sampleTypeName;
	/**关联订单*/
	private SampleOrder sampleOrder;
	/**电子病历编号*/
	private String  medicalNumber;
	
	
	/**收费方式*/
	private DicType collectionManner;
	/**收费说明*/
	private String  chargeNote;
	/**实验室内部样本编号*/
	private String sampleCode;
	/**家属联系人*/
	private String family;
	/**家属联系人电话*/
	private String familyPhone;
	/**家属联系人地址*/
	private String familySite;

	private CrmCustomer crmCustomer;
	/**医疗机构*/
	private String medicalInstitutions;
	/**医疗机构联系电话*/
	private String medicalInstitutionsPhone;
	/**医疗机构联系地址*/
	private String medicalInstitutionsSite;
	
	
	private CrmDoctor crmDoctor;
	/**主治医生*/
	private String attendingDoctor;
	/**主治医生联系电话*/
	private String attendingDoctorPhone;
	/**主治医生联系地址*/
	private String attendingDoctorSite;
	/**备注*/
	private String note;
	/**创建人*/
	private User createUser;
	/**创建时间*/
	private Date createDate;
	/**审批人*/
	private User confirmUser;
	/**审批时间*/
	private Date confirmDate;
	/**状态*/
	private String state;
	/**状态名称*/
	private String stateName;
	/**取样主表*/
	private String sampleInfoMain;
	/**肿瘤类型*/
	private WeiChatCancerType  cancerType;
	/**肿瘤类型一*/
	private WeiChatCancerTypeSeedOne  cancerTypeSeedOne;
	/**肿瘤类型二*/
	private WeiChatCancerTypeSeedTwo  cancerTypeSeedTwo;
	/**
	 *方法: 取得String
	 *@return: String  订单号
	 */
	@Column(name ="ORDER_NUMBER", length = 50)
	public String getOrderNumber(){
		return this.orderNumber;
	}
	/**
	 *方法: 设置String
	 *@param: String  订单号
	 */
	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  姓名
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  姓名
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="GENDER", length = 10)
	public String getGender(){
		return this.gender;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender){
		this.gender = gender;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  出生日期
	 */
	@Column(name ="BIRTH_DATE", length = 50)
	public Date getBirthDate(){
		return this.birthDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  出生日期
	 */
	public void setBirthDate(Date birthDate){
		this.birthDate = birthDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  确诊日期
	 */
	@Column(name ="DIAGNOSIS_DATE", length = 50)
	public Date getDiagnosisDate(){
		return this.diagnosisDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  确诊日期
	 */
	public void setDiagnosisDate(Date diagnosisDate){
		this.diagnosisDate = diagnosisDate;
	}
	
	
	
	/**
	 *方法: 取得String
	 *@return: String  分期
	 */
	@Column(name ="SAMPLE_STAGE", length = 50)
	public String getSampleStage() {
		return sampleStage;
	}
	/**
	 *方法: 取得
	 *@return:   肿瘤类别
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicType getDicType() {
		return dicType;
	}
	public void setDicType(DicType dicType) {
		this.dicType = dicType;
	}
	/**
	 *方法: 设置String
	 *@param: String  分期
	 */
	public void setSampleStage(String sampleStage) {
		this.sampleStage = sampleStage;
	}
	
	/**
	 *方法: 取得DicType
	 *@return: DicType  送检科室
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSPECTION_DEPARTMENT")
	public DicType getInspectionDepartment(){
		return this.inspectionDepartment;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  送检科室
	 */
	public void setInspectionDepartment(DicType inspectionDepartment){
		this.inspectionDepartment = inspectionDepartment;
	}
	/**
	 *方法: 取得Product
	 *@return: Product  检测项目
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PRODUCT")
	public Product getCrmProduct(){
		return this.crmProduct;
	}
	/**
	 *方法: 设置Product
	 *@param: Product  检测项目
	 */
	public void setCrmProduct(Product crmProduct){
		this.crmProduct = crmProduct;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  采样日期
	 */
	@Column(name ="SAMPLING_DATE", length = 50)
	public Date getSamplingDate(){
		return this.samplingDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  采样日期
	 */
	public void setSamplingDate(Date samplingDate){
		this.samplingDate = samplingDate;
	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  采样部位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLING_LOCATION")
	public DicType getSamplingLocation(){
		return this.samplingLocation;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  采样部位
	 */
	public void setSamplingLocation(DicType samplingLocation){
		this.samplingLocation = samplingLocation;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLING_NUMBER", length = 50)
	public String getSamplingNumber(){
		return this.samplingNumber;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSamplingNumber(String samplingNumber){
		this.samplingNumber = samplingNumber;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否病理确认
	 */
	@Column(name ="PATHOLOGY_CONFIRMED", length = 50)
	public String getPathologyConfirmed(){
		return this.pathologyConfirmed;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否病理确认
	 */
	public void setPathologyConfirmed(String pathologyConfirmed){
		this.pathologyConfirmed = pathologyConfirmed;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  血液采样时间
	 */
	@Column(name ="BLOOD_SAMPLE_DATE", length = 50)
	public Date getBloodSampleDate(){
		return this.bloodSampleDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  血液采样时间
	 */
	public void setBloodSampleDate(Date bloodSampleDate){
		this.bloodSampleDate = bloodSampleDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  血浆分离时间
	 */
	@Column(name ="PLASMAPHERESIS_DATE", length = 50)
	public Date getPlasmapheresisDate(){
		return this.plasmapheresisDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  血浆分离时间
	 */
	public void setPlasmapheresisDate(Date plasmapheresisDate){
		this.plasmapheresisDate = plasmapheresisDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  世和专员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COMMISSIONER")
	public User getCommissioner(){
		return this.commissioner;
	}
	/**
	 *方法: 设置User
	 *@param: User  世和专员
	 */
	public void setCommissioner(User commissioner){
		this.commissioner = commissioner;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  收样日期
	 */
	@Column(name ="RECEIVED_DATE", length = 50)
	public Date getReceivedDate(){
		return this.receivedDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  收样日期
	 */
	public void setReceivedDate(Date receivedDate){
		this.receivedDate = receivedDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本类型id
	 */
	@Column(name ="SAMPLE_TYPE_ID", length = 100)
	public String getSampleTypeId(){
		return this.sampleTypeId;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本类型id
	 */
	public void setSampleTypeId(String sampleTypeId){
		this.sampleTypeId = sampleTypeId;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本类型
	 */
	@Column(name ="SAMPLE_TYPE_NAME", length = 100)
	public String getSampleTypeName(){
		return this.sampleTypeName;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本类型
	 */
	public void setSampleTypeName(String sampleTypeName){
		this.sampleTypeName = sampleTypeName;
	}
	/**
	 *方法: 取得SampleOrder
	 *@return: SampleOrder  关联订单
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder(){
		return this.sampleOrder;
	}
	/**
	 *方法: 设置SampleOrder
	 *@param: SampleOrder  关联订单
	 */
	public void setSampleOrder(SampleOrder sampleOrder){
		this.sampleOrder = sampleOrder;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  电子病历编号
	 */
	@Column(name ="MEDICAL_NUMBER", length = 50)
	public String getMedicalNumber() {
		return medicalNumber;
	}
	public void setMedicalNumber(String medicalNumber) {
		this.medicalNumber = medicalNumber;
	}
	/**
	 *方法: 取得String
	 *@return: String  实验室内部样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 100)
	public String getSampleCode(){
		return this.sampleCode;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  实验室内部样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  家属联系人
	 */
	@Column(name ="FAMILY", length = 50)
	public String getFamily(){
		return this.family;
	}
	/**
	 *方法: 设置String
	 *@param: String  家属联系人
	 */
	public void setFamily(String family){
		this.family = family;
	}
	/**
	 *方法: 取得String
	 *@return: String  家属联系人电话
	 */
	@Column(name ="FAMILY_PHONE", length = 50)
	public String getFamilyPhone(){
		return this.familyPhone;
	}
	/**
	 *方法: 设置String
	 *@param: String  家属联系人电话
	 */
	public void setFamilyPhone(String familyPhone){
		this.familyPhone = familyPhone;
	}
	/**
	 *方法: 取得String
	 *@return: String  家属联系人地址
	 */
	@Column(name ="FAMILY_SITE", length = 100)
	public String getFamilySite(){
		return this.familySite;
	}
	/**
	 *方法: 设置String
	 *@param: String  家属联系人地址
	 */
	public void setFamilySite(String familySite){
		this.familySite = familySite;
	}
	/**
	 *方法: 取得String
	 *@return: String  医疗机构
	 */
	@Column(name ="MEDICAL_INSTITUTIONS", length = 50)
	public String getMedicalInstitutions(){
		return this.medicalInstitutions;
	}
	/**
	 *方法: 设置String
	 *@param: String  医疗机构
	 */
	public void setMedicalInstitutions(String medicalInstitutions){
		this.medicalInstitutions = medicalInstitutions;
	}
	/**
	 *方法: 取得String
	 *@return: String  医疗机构联系电话
	 */
	@Column(name ="MEDICAL_INSTITUTIONS_PHONE", length = 50)
	public String getMedicalInstitutionsPhone(){
		return this.medicalInstitutionsPhone;
	}
	/**
	 *方法: 设置String
	 *@param: String  医疗机构联系电话
	 */
	public void setMedicalInstitutionsPhone(String medicalInstitutionsPhone){
		this.medicalInstitutionsPhone = medicalInstitutionsPhone;
	}
	/**
	 *方法: 取得String
	 *@return: String  医疗机构联系地址
	 */
	@Column(name ="MEDICAL_INSTITUTIONS_SITE", length = 100)
	public String getMedicalInstitutionsSite(){
		return this.medicalInstitutionsSite;
	}
	/**
	 *方法: 设置String
	 *@param: String  医疗机构联系地址
	 */
	public void setMedicalInstitutionsSite(String medicalInstitutionsSite){
		this.medicalInstitutionsSite = medicalInstitutionsSite;
	}
	/**
	 *方法: 取得String
	 *@return: String  主治医生
	 */
	@Column(name ="ATTENDING_DOCTOR", length = 50)
	public String getAttendingDoctor(){
		return this.attendingDoctor;
	}
	/**
	 *方法: 设置String
	 *@param: String  主治医生
	 */
	public void setAttendingDoctor(String attendingDoctor){
		this.attendingDoctor = attendingDoctor;
	}
	/**
	 *方法: 取得String
	 *@return: String  主治医生联系电话
	 */
	@Column(name ="ATTENDING_DOCTOR_PHONE", length = 50)
	public String getAttendingDoctorPhone(){
		return this.attendingDoctorPhone;
	}
	/**
	 *方法: 设置String
	 *@param: String  主治医生联系电话
	 */
	public void setAttendingDoctorPhone(String attendingDoctorPhone){
		this.attendingDoctorPhone = attendingDoctorPhone;
	}
	/**
	 *方法: 取得String
	 *@return: String  主治医生联系地址
	 */
	@Column(name ="ATTENDING_DOCTOR_SITE", length = 100)
	public String getAttendingDoctorSite(){
		return this.attendingDoctorSite;
	}
	/**
	 *方法: 设置String
	 *@param: String  主治医生联系地址
	 */
	public void setAttendingDoctorSite(String attendingDoctorSite){
		this.attendingDoctorSite = attendingDoctorSite;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 500)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  审批人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser(){
		return this.confirmUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  审批人
	 */
	public void setConfirmUser(User confirmUser){
		this.confirmUser = confirmUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审批时间
	 */
	@Column(name ="CONFIRM_DATE", length = 50)
	public Date getConfirmDate(){
		return this.confirmDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审批时间
	 */
	public void setConfirmDate(Date confirmDate){
		this.confirmDate = confirmDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态名称
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态名称
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 * 检测项目
	 * @return
	 */
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}
	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}
	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}
	//取样主表
	public String getSampleInfoMain() {
		return sampleInfoMain;
	}
	public void setSampleInfoMain(String sampleInfoMain) {
		this.sampleInfoMain = sampleInfoMain;
	}
	/**
	 *方法: 取得String
	 *@return: String  癌症种类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE")
	public WeiChatCancerType getCancerType() {
		return cancerType;
	}
	public void setCancerType(WeiChatCancerType cancerType) {
		this.cancerType = cancerType;
	}
	
	
	/**
	 *方法: 取得String
	 *@return: String  子类一
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_ONE")
	public WeiChatCancerTypeSeedOne getCancerTypeSeedOne() {
		return cancerTypeSeedOne;
	}
	public void setCancerTypeSeedOne(WeiChatCancerTypeSeedOne cancerTypeSeedOne) {
		this.cancerTypeSeedOne = cancerTypeSeedOne;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  子类二
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_TWO")
	public WeiChatCancerTypeSeedTwo getCancerTypeSeedTwo() {
		return cancerTypeSeedTwo;
	}
	public void setCancerTypeSeedTwo(WeiChatCancerTypeSeedTwo cancerTypeSeedTwo) {
		this.cancerTypeSeedTwo = cancerTypeSeedTwo;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COLLECTION_MANNER")
	public DicType getCollectionManner() {
		return collectionManner;
	}
	public void setCollectionManner(DicType collectionManner) {
		this.collectionManner = collectionManner;
	}
	@Column(name ="CHARGE_NOTE", length = 50)
	public String getChargeNote() {
		return chargeNote;
	}
	public void setChargeNote(String chargeNote) {
		this.chargeNote = chargeNote;
	}
	
	
}