package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.format.annotation.DateTimeFormat;

import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.family.model.Family;
import com.biolims.system.product.model.Product;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;

/**
 * @Title: Model
 * @Description: 订单表
 * @author lims-platform
 * @date 2016-03-07 11:01:47
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_ORDER")
@SuppressWarnings("serial")
public class SampleOrder extends EntityDao<SampleOrder> implements java.io.Serializable, Cloneable {
	/** 编码 */
	private String id;
	private String oldId;
	/** 接收状态 */
	private String receiveState;
	/** 姓名 */
	private String name;
	/** 性别 */
	private String gender;
	/** 身份证号 */
	private String idCard;
	/** 民族 */
	private String nation;
	/** email */
	private String email;
	/** 邮编 */
	private String zipCode;
	/** 地址 */
	private String address;
	/** 出生日期 */
	private Date birthDate;
	/** 确诊日期 */
	private String diagnosisDate;
	/** 肿瘤类别 */
	private DicType dicType;
	/** 分期 */
	private String sampleStage;
	private DicType inspectionDepartment;
	/** 检测项目 */
	private Product crmProduct;
	/** 检测项目 */
	private String productId;
	/** 检测项目名称 */
	private String productName;

	/** 采样日期 */
	private Date samplingDate;
	/** 采样部位 */
	private DicType samplingLocation;
	/** 样本编号 */
	private String samplingNumber;
	/** 是否病理确认 */
	private String pathologyConfirmed;
	/** 血液采样时间 */
	private String bloodSampleDate;
	/** 血浆分离时间 */
	private String plasmapheresisDate;
	/** 销售代表 */
	private User commissioner;
	/** 收样日期 */
	private Date receivedDate;
	/** 样本类型id */
	private String sampleTypeId;
	/** 样本类型 */
	private String sampleTypeName;
	/** 实验室内部样本编号 */
	private String sampleCode;
	/** 电子病历编号 */
	private String medicalNumber;
	/** 采样日期 */
	private String inspectDate;
	/** 收费方式 */
	private DicType collectionManner;
	/** 收费说明 */
	private String chargeNote;
	/** 家族编号 */
	private String familyCode;
	/** 家族编号 */
	private Family familyId;
	/** 家属联系人 */
	private String family;
	/** 家属联系人电话 */
	private String familyPhone;
	/** 家属联系人地址 */
	private String familySite;
	private CrmCustomer crmCustomer;
	/** 医疗机构 */
	private String medicalInstitutions;
	/** 医疗机构联系电话 */
	private String medicalInstitutionsPhone;
	
	/**科室*/
	private CrmCustomerIteam crmCustomerIteam;
	/** 科室电话 */
	private String crmPhone;
	/** 医疗机构联系地址 */
	private String medicalInstitutionsSite;
	private CrmDoctor crmDoctor;
	/** 主治医生 */
	private String attendingDoctor;
	/** 主治医生联系电话 */
	private String attendingDoctorPhone;
	/** 主治医生联系地址 */
	private String attendingDoctorSite;
	/** 备注 */
	private String note;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 审批人 */
	private User confirmUser;
	/** 审批时间 */
	private Date confirmDate;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 录入进度 */
	private String sampleFlag;

	/** 报告时间 */
	private Date reportDate;
	/** 录入人 */
	private String infoUserStr;
	/** 取样主表 */
	private String sampleInfoMain;
	/** 审核是否通过 */
	private String successFlag;
	// 选择代理商
	private PrimaryTask primary;

	// 条形码
	private String barcode;
	// 受试者编号
	private String subjectID;

	// 家族史描述
	private String familyHistorysummary;
	// 是否处于孕期
	private String prenatal;
	// 送样单位
	private String Institute;
	// 门诊号/住院号
	private String hospitalPatientID;
	// 送检单位地址
	private String customerAdress;
	// 送检医师姓名
	private String physicianName;
	// 送检医师电话
	private String physicianPhone;
	// 送检医师传真
	private String physicianFax;
	// 送检医师邮箱
	private String physicianEmail;
	// 选择类型
	private String type;

	// 体重
	private String weights;
	// 孕周
	private String gestationalWeeks;
	// 姓名拼音
	private String nameEn;
	// 手机号
	private String phone;

	// 用药情况
	private String yongyao;

	private String yongyaoId;
	private DicType yaopin;

	// 是否出现
	private String zhengzhuang;

	private String zhengzhuangId;
	// 床号
	private String bedNo;

	// 发生频率
	private String fspl;
	// 运输方式
	private DicType yushufs;

	// 临床诊断
	private String diagnosis;

	// 过敏史
	private String guomin;

	// 临床病例小结
	private String summary;
	// 访问
	private String visit;
	// 籍贯
	private String nativePlace;
	/** 送检日期 */
	private Date sendDate;
	// 范围ID
	private String scopeId;

	// 范围
	private String scopeName;

	/** 送检原因 */
	private String submitReasonName;

	/** 自定义字段值 */
	private String fieldContent;

	private String flagValue;
	/** 最新的执行单号 */
	private String newTask;

	/* 新增字段 */
	/** 乙肝HBV */
	@Column(name = "hepatitis_hbv", length = 50)
	private String hepatitisHbv;
	/** 丙肝HCV */
	@Column(name = "hepatitis_hcv", length = 50)
	private String hepatitisHcv;
	/** 艾滋HIV */
	@Column(name = "Hiv_virus", length = 50)
	private String HivVirus;
	/** 梅毒TPHA */
	@Column(name = "syphilis", length = 50)
	private String syphilis;
	/** 淋巴细胞绝对值 */
	@Column(name = "lymphoid_cell_series", length = 50)
	private String lymphoidCellSeries;
	/** 采血计量 */
	@Column(name = "counter_draw_blood", length = 50)
	private String counterDrawBlood;
	/** 采血管 */
	@Column(name = "heparin_tube", length = 50)
	private String heparinTube;
	/** 采血时间 */
	@Column(name = "draw_blood_time", length = 50)
	private Date drawBloodTime;
	/**预计回输结束时间*/
	@Column(name = "feed_back_time", length = 50)
	private Date feedBackTime;
	/**预计回输开始时间*/
	@Column(name = "start_back_time", length = 50)
	private Date startBackTime;
	
//	作废原因
	private String cancel;
	
	/** 批次状态·0订单录入完成(订单审核通过)
1样本已接收（接收状态完成）
2正在生产（创建任务单）
3已放行（放行审核通过）
4运输完成（运输已扫码）
5不合格（生产不合格） */
	private String batchState;
	/** 批次状态名称 */
	private String batchStateName;

	
	/** 产品个数 */
	private String barcodeNum;
	
	
	
	public String getBarcodeNum() {
		return barcodeNum;
	}

	public void setBarcodeNum(String barcodeNum) {
		this.barcodeNum = barcodeNum;
	}

	public String getBatchState() {
		return batchState;
	}

	public void setBatchState(String batchState) {
		this.batchState = batchState;
	}

	public String getBatchStateName() {
		return batchStateName;
	}

	public void setBatchStateName(String batchStateName) {
		this.batchStateName = batchStateName;
	}

	public Date getStartBackTime() {
		return startBackTime;
	}

	public void setStartBackTime(Date startBackTime) {
		this.startBackTime = startBackTime;
	}

	public Date getFeedBackTime() {
		return feedBackTime;
	}

	public void setFeedBackTime(Date feedBackTime) {
		this.feedBackTime = feedBackTime;
	}

	/** 随机码 */
	@Column(name = "random_code", length = 50)
	private String randomCode;
	/** 筛选码 */
	@Column(name = "filtrate_code", length = 50)
	private String filtrateCode;
	/** ccoi */
	@Column(name = "ccoi", length = 50)
	private String ccoi;
	// 肿瘤分期
	private DicType tumorStaging;
	// 肿瘤类型
	private DicType zhongliu;

	/** 肿瘤分期备注 */
	private String zhongliuNote;
	/** 肿瘤分期备注 */
	private String tumorStagingNote;

	/** 采血轮次 */
	private String round;
	/** 白细胞计数 */
	private Double whiteBloodCellNum;
	/** 淋巴细胞百分比 */
	private Double percentageOfLymphocytes;
	/** 临床用药方案 1放疗、2化疗、3靶向药、4手术、5其他 */
	private String medicationPlan;
	/** 代理商 */
	private PrimaryTask agreementTask;
	/** 姓名缩写 */
	private String abbreviation;
	/** 参考回输日期 */
	private String backtolosetime;
	/** 时间段 */
	private String timebucket;
	

	
	
	
	

	public String getTimebucket() {
		return timebucket;
	}

	public void setTimebucket(String timebucket) {
		this.timebucket = timebucket;
	}

	public String getBacktolosetime() {
		return backtolosetime;
	}

	public void setBacktolosetime(String backtolosetime) {
		this.backtolosetime = backtolosetime;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	public PrimaryTask getAgreementTask() {
		return agreementTask;
	}

	public void setAgreementTask(PrimaryTask agreementTask) {
		this.agreementTask = agreementTask;
	}

	public String getMedicationPlan() {
		return medicationPlan;
	}

	public void setMedicationPlan(String medicationPlan) {
		this.medicationPlan = medicationPlan;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public String getReceiveState() {
		return receiveState;
	}

	public void setReceiveState(String receiveState) {
		this.receiveState = receiveState;
	}

	public Double getWhiteBloodCellNum() {
		return whiteBloodCellNum;
	}

	public void setWhiteBloodCellNum(Double whiteBloodCellNum) {
		this.whiteBloodCellNum = whiteBloodCellNum;
	}

	public Double getPercentageOfLymphocytes() {
		return percentageOfLymphocytes;
	}

	public void setPercentageOfLymphocytes(Double percentageOfLymphocytes) {
		this.percentageOfLymphocytes = percentageOfLymphocytes;
	}

	public String getRound() {
		return round;
	}

	public void setRound(String round) {
		this.round = round;
	}

	public String getFiltrateCode() {
		return filtrateCode;
	}

	public void setFiltrateCode(String filtrateCode) {
		this.filtrateCode = filtrateCode;
	}

	public String getCcoi() {
		return ccoi;
	}

	public void setCcoi(String ccoi) {
		this.ccoi = ccoi;
	}

	public String getZhongliuNote() {
		return zhongliuNote;
	}

	public void setZhongliuNote(String zhongliuNote) {
		this.zhongliuNote = zhongliuNote;
	}

	public String getTumorStagingNote() {
		return tumorStagingNote;
	}

	public void setTumorStagingNote(String tumorStagingNote) {
		this.tumorStagingNote = tumorStagingNote;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "TUMOR_STAGING")
	public DicType getTumorStaging() {
		return tumorStaging;
	}

	public void setTumorStaging(DicType tumorStaging) {
		this.tumorStaging = tumorStaging;
	}

	public String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}

	public String getCounterDrawBlood() {
		return counterDrawBlood;
	}

	public void setCounterDrawBlood(String counterDrawBlood) {
		this.counterDrawBlood = counterDrawBlood;
	}

	public String getHepatitisHbv() {
		return hepatitisHbv;
	}

	public void setHepatitisHbv(String hepatitisHbv) {
		this.hepatitisHbv = hepatitisHbv;
	}

	public String getHepatitisHcv() {
		return hepatitisHcv;
	}

	public void setHepatitisHcv(String hepatitisHcv) {
		this.hepatitisHcv = hepatitisHcv;
	}

	public String getHivVirus() {
		return HivVirus;
	}

	public void setHivVirus(String hivVirus) {
		HivVirus = hivVirus;
	}

	public String getSyphilis() {
		return syphilis;
	}

	public void setSyphilis(String syphilis) {
		this.syphilis = syphilis;
	}

	public String getLymphoidCellSeries() {
		return lymphoidCellSeries;
	}

	public void setLymphoidCellSeries(String lymphoidCellSeries) {
		this.lymphoidCellSeries = lymphoidCellSeries;
	}

	public String getHeparinTube() {
		return heparinTube;
	}

	public void setHeparinTube(String heparinTube) {
		this.heparinTube = heparinTube;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
	public Date getDrawBloodTime() {
		return drawBloodTime;
	}

	public void setDrawBloodTime(Date drawBloodTime) {
		this.drawBloodTime = drawBloodTime;
	}

	public String getNewTask() {
		return newTask;
	}

	public void setNewTask(String newTask) {
		this.newTask = newTask;
	}

	public String getFlagValue() {
		return flagValue;
	}

	public void setFlagValue(String flagValue) {
		this.flagValue = flagValue;
	}

	public String getFieldContent() {
		return fieldContent;
	}

	public void setFieldContent(String fieldContent) {
		this.fieldContent = fieldContent;
	}

	public String getSubmitReasonName() {
		return submitReasonName;
	}

	public void setSubmitReasonName(String submitReasonName) {
		this.submitReasonName = submitReasonName;
	}

	public String getGuomin() {
		return guomin;
	}

	public void setGuomin(String guomin) {
		this.guomin = guomin;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getBedNo() {
		return bedNo;
	}

	public void setBedNo(String bedNo) {
		this.bedNo = bedNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "ZHONGLIU")
	public DicType getZhongliu() {
		return zhongliu;
	}

	public void setZhongliu(DicType zhongliu) {
		this.zhongliu = zhongliu;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "YUSHUFS")
	public DicType getYushufs() {
		return yushufs;
	}

	public void setYushufs(DicType yushufs) {
		this.yushufs = yushufs;
	}

	public String getZhengzhuangId() {
		return zhengzhuangId;
	}

	public void setZhengzhuangId(String zhengzhuangId) {
		this.zhengzhuangId = zhengzhuangId;
	}

	public String getZhengzhuang() {
		return zhengzhuang;
	}

	public void setZhengzhuang(String zhengzhuang) {
		this.zhengzhuang = zhengzhuang;
	}

	public String getFspl() {
		return fspl;
	}

	public void setFspl(String fspl) {
		this.fspl = fspl;
	}

	/**
	 * 方法: 取得
	 * 
	 * @return: yaopin
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "YAOPIN")
	public DicType getYaopin() {
		return yaopin;
	}

	public void setYaopin(DicType yaopin) {
		this.yaopin = yaopin;
	}

	public String getYongyao() {
		return yongyao;
	}

	public void setYongyao(String yongyao) {
		this.yongyao = yongyao;
	}

	public String getYongyaoId() {
		return yongyaoId;
	}

	public void setYongyaoId(String yongyaoId) {
		this.yongyaoId = yongyaoId;
	}

	public String getWeights() {
		return weights;
	}

	public void setWeights(String weights) {
		this.weights = weights;
	}

	public String getGestationalWeeks() {
		return gestationalWeeks;
	}

	public void setGestationalWeeks(String gestationalWeeks) {
		this.gestationalWeeks = gestationalWeeks;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getSubjectID() {
		return subjectID;
	}

	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}

	public String getFamilyHistorysummary() {
		return familyHistorysummary;
	}

	public void setFamilyHistorysummary(String familyHistorysummary) {
		this.familyHistorysummary = familyHistorysummary;
	}

	public String getPrenatal() {
		return prenatal;
	}

	public void setPrenatal(String prenatal) {
		this.prenatal = prenatal;
	}

	public String getInstitute() {
		return Institute;
	}

	public void setInstitute(String institute) {
		Institute = institute;
	}

	public String getHospitalPatientID() {
		return hospitalPatientID;
	}

	public void setHospitalPatientID(String hospitalPatientID) {
		this.hospitalPatientID = hospitalPatientID;
	}

	public String getCustomerAdress() {
		return customerAdress;
	}

	public void setCustomerAdress(String customerAdress) {
		this.customerAdress = customerAdress;
	}

	public String getPhysicianName() {
		return physicianName;
	}

	public void setPhysicianName(String physicianName) {
		this.physicianName = physicianName;
	}

	public String getPhysicianPhone() {
		return physicianPhone;
	}

	public void setPhysicianPhone(String physicianPhone) {
		this.physicianPhone = physicianPhone;
	}

	public String getPhysicianFax() {
		return physicianFax;
	}

	public void setPhysicianFax(String physicianFax) {
		this.physicianFax = physicianFax;
	}

	public String getPhysicianEmail() {
		return physicianEmail;
	}

	public void setPhysicianEmail(String physicianEmail) {
		this.physicianEmail = physicianEmail;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ADVANCE")
	public PrimaryTask getPrimary() {
		return primary;
	}

	public void setPrimary(PrimaryTask primary) {
		this.primary = primary;
	}

	// 图片上传时间
	private Date upImgTime;

	/** 癌症种类 */
	private WeiChatCancerType cancerType;
	/** 子类一 */
	private WeiChatCancerTypeSeedOne cancerTypeSeedOne;
	/** 子类二 */
	private WeiChatCancerTypeSeedTwo cancerTypeSeedTwo;

	private String isFee;

	private String fee;

	/**
	 * 订单状态 1:未完成,2:已出报告,3:已完成
	 */
	private String bgState;

	// 年龄
	private String age;
	// 订单类型 0：眼癌 1：非眼癌 2：科研
	private String orderType;
	/**
	 * 用药情况 by wudi 1220
	 */
	private String medicalHistoryId;
	private String medicalHistory;

	private TechJkServiceTask techJkService;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE")
	public TechJkServiceTask getTechJkService() {
		return techJkService;
	}

	public void setTechJkService(TechJkServiceTask techJkService) {
		this.techJkService = techJkService;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBgState() {
		return bgState;
	}

	public void setBgState(String bgStste) {
		this.bgState = bgStste;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 姓名
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             姓名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 性别
	 */
	@Column(name = "GENDER", length = 10)
	public String getGender() {
		return this.gender;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             性别
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             身份证号
	 */
	public String getIdCard() {
		return idCard;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             性身份证号
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 出生日期
	 */
	@Column(name = "BIRTH_DATE", length = 50)
	public Date getBirthDate() {
		return this.birthDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             出生日期
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	// /**
	// * 方法: 取得Date
	// *
	// * @return: Date 确诊日期
	// */
	// @Column(name = "DIAGNOSIS_DATE", length = 50)
	// public Date getDiagnosisDate() {
	// return this.diagnosisDate;
	// }
	//
	// /**
	// * 方法: 设置Date
	// *
	// * @param: Date 确诊日期
	// */
	// public void setDiagnosisDate(Date diagnosisDate) {
	// this.diagnosisDate = diagnosisDate;
	// }

	/**
	 * 方法: 取得
	 * 
	 * @return: 肿瘤类别
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicType getDicType() {
		return dicType;
	}

	public String getDiagnosisDate() {
		return diagnosisDate;
	}

	public void setDiagnosisDate(String diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	public void setDicType(DicType dicType) {
		this.dicType = dicType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 分期
	 */
	@Column(name = "SAMPLE_STAGE", length = 500)
	public String getSampleStage() {
		return sampleStage;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 分期
	 */
	public void setSampleStage(String sampleStage) {
		this.sampleStage = sampleStage;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 送检科室
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSPECTION_DEPARTMENT")
	public DicType getInspectionDepartment() {
		return this.inspectionDepartment;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             送检科室
	 */
	public void setInspectionDepartment(DicType inspectionDepartment) {
		this.inspectionDepartment = inspectionDepartment;
	}

	/**
	 * 方法: 取得Product
	 * 
	 * @return: Product 检测项目
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PRODUCT")
	public Product getCrmProduct() {
		return this.crmProduct;
	}

	/**
	 * 方法: 设置Product
	 * 
	 * @param: Product
	 *             检测项目
	 */
	public void setCrmProduct(Product crmProduct) {
		this.crmProduct = crmProduct;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 采样日期
	 */
	@Column(name = "SAMPLING_DATE", length = 50)
	public Date getSamplingDate() {
		return this.samplingDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             采样日期
	 */
	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 采样部位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLING_LOCATION")
	public DicType getSamplingLocation() {
		return this.samplingLocation;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             采样部位
	 */
	public void setSamplingLocation(DicType samplingLocation) {
		this.samplingLocation = samplingLocation;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "SAMPLING_NUMBER", length = 50)
	public String getSamplingNumber() {
		return this.samplingNumber;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             样本编号
	 */
	public void setSamplingNumber(String samplingNumber) {
		this.samplingNumber = samplingNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否病理确认
	 */
	@Column(name = "PATHOLOGY_CONFIRMED", length = 50)
	public String getPathologyConfirmed() {
		return this.pathologyConfirmed;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             是否病理确认
	 */
	public void setPathologyConfirmed(String pathologyConfirmed) {
		this.pathologyConfirmed = pathologyConfirmed;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 血液采样时间
	 */

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 世和专员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COMMISSIONER")
	public User getCommissioner() {
		return this.commissioner;
	}

	public String getBloodSampleDate() {
		return bloodSampleDate;
	}

	public void setBloodSampleDate(String bloodSampleDate) {
		this.bloodSampleDate = bloodSampleDate;
	}

	public String getPlasmapheresisDate() {
		return plasmapheresisDate;
	}

	public void setPlasmapheresisDate(String plasmapheresisDate) {
		this.plasmapheresisDate = plasmapheresisDate;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             世和专员
	 */
	public void setCommissioner(User commissioner) {
		this.commissioner = commissioner;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 收样日期
	 */
	@Column(name = "RECEIVED_DATE", length = 50)
	public Date getReceivedDate() {
		return this.receivedDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             收样日期
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型id
	 */
	@Column(name = "SAMPLE_TYPE_ID", length = 200)
	public String getSampleTypeId() {
		return this.sampleTypeId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             样本类型id
	 */
	public void setSampleTypeId(String sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE_NAME", length = 100)
	public String getSampleTypeName() {
		return this.sampleTypeName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             样本类型
	 */
	public void setSampleTypeName(String sampleTypeName) {
		this.sampleTypeName = sampleTypeName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验室内部样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             实验室内部样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 电子病历编号
	 */
	@Column(name = "MEDICAL_NUMBER", length = 50)
	public String getMedicalNumber() {
		return medicalNumber;
	}

	public void setMedicalNumber(String medicalNumber) {
		this.medicalNumber = medicalNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 家族编号
	 */
	@Column(name = "FAMILY_CODE", length = 50)
	public String getFamilyCode() {
		return familyCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             家族编号
	 */
	public void setFamilyCode(String familyCode) {
		this.familyCode = familyCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 家属联系人
	 */
	@Column(name = "FAMILY", length = 50)
	public String getFamily() {
		return this.family;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             家属联系人
	 */
	public void setFamily(String family) {
		this.family = family;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 家属联系人电话
	 */
	@Column(name = "FAMILY_PHONE", length = 50)
	public String getFamilyPhone() {
		return this.familyPhone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             家属联系人电话
	 */
	public void setFamilyPhone(String familyPhone) {
		this.familyPhone = familyPhone;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 家属联系人地址
	 */
	@Column(name = "FAMILY_SITE", length = 100)
	public String getFamilySite() {
		return this.familySite;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             家属联系人地址
	 */
	public void setFamilySite(String familySite) {
		this.familySite = familySite;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 医疗机构
	 */
	@Column(name = "MEDICAL_INSTITUTIONS", length = 50)
	public String getMedicalInstitutions() {
		return this.medicalInstitutions;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             医疗机构
	 */
	public void setMedicalInstitutions(String medicalInstitutions) {
		this.medicalInstitutions = medicalInstitutions;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 医疗机构联系电话
	 */
	@Column(name = "MEDICAL_INSTITUTIONS_PHONE", length = 50)
	public String getMedicalInstitutionsPhone() {
		return this.medicalInstitutionsPhone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             医疗机构联系电话
	 */
	public void setMedicalInstitutionsPhone(String medicalInstitutionsPhone) {
		this.medicalInstitutionsPhone = medicalInstitutionsPhone;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 医疗机构联系地址
	 */
	@Column(name = "MEDICAL_INSTITUTIONS_SITE", length = 100)
	public String getMedicalInstitutionsSite() {
		return this.medicalInstitutionsSite;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             医疗机构联系地址
	 */
	public void setMedicalInstitutionsSite(String medicalInstitutionsSite) {
		this.medicalInstitutionsSite = medicalInstitutionsSite;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 主治医生
	 */
	@Column(name = "ATTENDING_DOCTOR", length = 50)
	public String getAttendingDoctor() {
		return this.attendingDoctor;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             主治医生
	 */
	public void setAttendingDoctor(String attendingDoctor) {
		this.attendingDoctor = attendingDoctor;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 主治医生联系电话
	 */
	@Column(name = "ATTENDING_DOCTOR_PHONE", length = 50)
	public String getAttendingDoctorPhone() {
		return this.attendingDoctorPhone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             主治医生联系电话
	 */
	public void setAttendingDoctorPhone(String attendingDoctorPhone) {
		this.attendingDoctorPhone = attendingDoctorPhone;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 主治医生联系地址
	 */
	@Column(name = "ATTENDING_DOCTOR_SITE", length = 100)
	public String getAttendingDoctorSite() {
		return this.attendingDoctorSite;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             主治医生联系地址
	 */
	public void setAttendingDoctorSite(String attendingDoctorSite) {
		this.attendingDoctorSite = attendingDoctorSite;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 500)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审批人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             审批人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审批时间
	 */
	@Column(name = "CONFIRM_DATE", length = 50)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             审批时间
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态名称
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 录入进度
	 */
	@Column(name = "SAMPLE_FLAG", length = 50)
	public String getSampleFlag() {
		return this.sampleFlag;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             录入进度
	 */
	public void setSampleFlag(String sampleFlag) {
		this.sampleFlag = sampleFlag;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	/**
	 * 检测项目
	 * 
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSampleInfoMain() {
		return sampleInfoMain;
	}

	public void setSampleInfoMain(String sampleInfoMain) {
		this.sampleInfoMain = sampleInfoMain;
	}

	@Column(name = "UP_IMG_TIME", length = 50)
	public Date getUpImgTime() {
		return upImgTime;
	}

	public void setUpImgTime(Date upImgTime) {
		this.upImgTime = upImgTime;
	}

	public String getInfoUserStr() {
		return infoUserStr;
	}

	public void setInfoUserStr(String infoUserStr) {
		this.infoUserStr = infoUserStr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 姓名
	 */
	@Column(name = "SUCCESSFLAG", length = 100)
	public String getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(String successFlag) {
		this.successFlag = successFlag;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 癌症种类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE")
	public WeiChatCancerType getCancerType() {
		return cancerType;
	}

	public void setCancerType(WeiChatCancerType cancerType) {
		this.cancerType = cancerType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子类一
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_ONE")
	public WeiChatCancerTypeSeedOne getCancerTypeSeedOne() {
		return cancerTypeSeedOne;
	}

	public void setCancerTypeSeedOne(WeiChatCancerTypeSeedOne cancerTypeSeedOne) {
		this.cancerTypeSeedOne = cancerTypeSeedOne;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子类二
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_TWO")
	public WeiChatCancerTypeSeedTwo getCancerTypeSeedTwo() {
		return cancerTypeSeedTwo;
	}

	public void setCancerTypeSeedTwo(WeiChatCancerTypeSeedTwo cancerTypeSeedTwo) {
		this.cancerTypeSeedTwo = cancerTypeSeedTwo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COLLECTION_MANNER")
	public DicType getCollectionManner() {
		return collectionManner;
	}

	public void setCollectionManner(DicType collectionManner) {
		this.collectionManner = collectionManner;
	}

	@Column(name = "CHARGE_NOTE", length = 50)
	public String getChargeNote() {
		return chargeNote;
	}

	public void setChargeNote(String chargeNote) {
		this.chargeNote = chargeNote;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "IS_FEE", length = 50)
	public String getIsFee() {
		return isFee;
	}

	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}

	@Column(name = "FEE", length = 50)
	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getVisit() {
		return visit;
	}

	public void setVisit(String visit) {
		this.visit = visit;
	}

	public String getNativePlace() {
		return nativePlace;
	}

	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}

	public String getMedicalHistoryId() {
		return medicalHistoryId;
	}

	public void setMedicalHistoryId(String medicalHistoryId) {
		this.medicalHistoryId = medicalHistoryId;
	}

	public String getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * @return the familyId
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public Family getFamilyId() {
		return familyId;
	}

	/**
	 * @param familyId
	 *            the familyId to set
	 */
	public void setFamilyId(Family familyId) {
		this.familyId = familyId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the sendDate
	 */
	public Date getSendDate() {
		return sendDate;
	}

	/**
	 * @param sendDate
	 *            the sendDate to set
	 */
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getCrmPhone() {
		return crmPhone;
	}

	public void setCrmPhone(String crmPhone) {
		this.crmPhone = crmPhone;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	public CrmCustomerIteam getCrmCustomerIteam() {
		return crmCustomerIteam;
	}

	public void setCrmCustomerIteam(CrmCustomerIteam crmCustomerIteam) {
		this.crmCustomerIteam = crmCustomerIteam;
	}

}
