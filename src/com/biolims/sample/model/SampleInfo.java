package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmFamilyPatientShip;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 样本主数据明细
 * @author lims-platform
 * @date 2015-11-03 16:21:36
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_INFO")
@SuppressWarnings("serial")
public class SampleInfo extends EntityDao<SampleInfo> implements java.io.Serializable {
	/** 编码 */
	private String id;
	/** 订单号 */
	private String orderNum;
	/** 病历号 */
	private String patientId;
	/** 订单实体 */
	private SampleOrder sampleOrder;
	/** 样本名称 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 病人姓名 */
	private String patientName;
	/** 项目编号 */
	private String productId;
	/** 项目名称 */
	private String productName;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 备注 */
	private String note;
	/** 分期 */
	private String sampleStage;
	/** 项目主数据 */
	private Project project;
	/** 样本关系 */
	private CrmFamilyPatientShip personShip;

	private SampleReceive sampleReceive;
	// 样本数量
	private String sampleNum;
	// 样本类型
	private String sampleType2;
	/** 采样日期 */
	private Date samplingDate;
	/** 采样部位 */
	private DicType dicType;
	// 单位
	private String unit;
	// 样本说明
	private String sampleNote;
	/** 采样部位 */
	private String dicTypeName;
	// 客户(客户单位)
	private CrmCustomer crmCustomer;
	// 医生（客户）
	private CrmDoctor crmDoctor;
	// 物种
	private String species;
	// 销售
	private User sellPerson;
	/** 是否是质控品 */
	private String isZkp;
	/** 是否使用质控品 */
	private String isuseZkp;
	/** 产品类型 1科研2临床 */
	private String sampleStyle;

	// 追加订单的类型
	private String changeType;

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getSampleStyle() {
		return sampleStyle;
	}

	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	public String getIsuseZkp() {
		return isuseZkp;
	}

	public void setIsuseZkp(String isuseZkp) {
		this.isuseZkp = isuseZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	public User getSellPerson() {
		return sellPerson;
	}

	public void setSellPerson(User sellPerson) {
		this.sellPerson = sellPerson;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	public String getDicTypeName() {
		return dicTypeName;
	}

	public void setDicTypeName(String dicTypeName) {
		this.dicTypeName = dicTypeName;
	}

	public String getSampleNote() {
		return sampleNote;
	}

	public void setSampleNote(String sampleNote) {
		this.sampleNote = sampleNote;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Date getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	public String getSampleType2() {
		return sampleType2;
	}

	public void setSampleType2(String sampleType2) {
		this.sampleType2 = sampleType2;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	/** 外部样本号 */
	private String idCard;
	// 证件号码（实际外部号）
	private String cardNumber;
	/** 手机号 */
	private String phone;
	/** 样本类型 */
	private DicSampleType sampleType;
	/** 检测方法 */
	private WorkType businessType;
	/** 检测方法 */
	private String sequenceFun;
	/** 送检医院 */
	private String hospital;
	/** 送检医院 */
	private String sampleUnit;
	/** 送检医生 */
	private String doctor;
	// 推荐人
	private String referUser;
	/** 价格 */
	private Double price;
	// 金额
	private Double fee;
	/** 价格 */
	private Double projectFee;
	/** 付款方式 */
	private DicType type;
	/** 付款方式 */
	private DicType feeWay;
	// /**项目*/
	// private Product product;

	/** 附件数量 */
	private String fileNum;

	// 取样时间
	private String sampleTime;
	// 送检日期
	private String inspectDate;
	// 接收日期
	private Date receiveDate;
	// 应出报告日期
	private String reportDate;
	// 姓名拼音
	private String spellName;
	// 性别
	private String gender;
	// 年龄
	private String age;
	// 籍贯
	private String birthAddress;
	// 民族
	private String nation;
	// 身高
	private Double hight;
	// 体重
	private Double weight;
	// 婚姻情况
	private String marriage;
	// 证件类型
	private String cardType;

	// 联系方式
	private String tele;
	// 邮箱
	private String email;
	// 家庭住址
	private String familyAddress;
	// 是否收费
	private String isFee;
	// 优惠类型
	private String privilege;
	// 发票号
	private String billNumber;
	// 发票号
	private String invoiceCode;
	// 发票抬头
	private String billTitle;
	// 发票抬头
	private String invoiceNum;
	// 录入人
	private User createUser;
	// 录入人1
	private String createUser1;
	// 录入人2
	private String createUser2;
	// 录入人
	private String inputUser;
	// 审核人1
	private String confirmUser1;
	// 审核人2
	private String confirmUser2;
	// 业务员
	private User salesman;
	// 上传图片
	private FileInfo upLoadAccessory;
	// 地区
	private String area;
	// 病历号
	private String medicalCard;
	// 任务单
	private WorkOrder orderId;
	// 图片上传时间
	private Date upImgTime;
	// 标示查询SampleInfo
	private String next;
	// 产前
	private SampleInput si;
	// 染色体
	private SampleChromosome sc;

	// 叶酸
	private SampleFolicAcid sfa;
	// 基因
	private SampleGene sg;
	// 肿瘤
	private SampleTumor st;
	// 临检所
	private SampleVisit sv;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 关联主样本
	private String sampleRelevanceMain;
	/** 发送电子报告日期 */
	private Date sendReportDate;

	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public Date getSendReportDate() {
		return sendReportDate;
	}

	public void setSendReportDate(Date sendReportDate) {
		this.sendReportDate = sendReportDate;
	}

	public String getSampleRelevanceMain() {
		return sampleRelevanceMain;
	}

	public void setSampleRelevanceMain(String sampleRelevanceMain) {
		this.sampleRelevanceMain = sampleRelevanceMain;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	// 储位
	private String location;

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	// * 选择模板
	//
	// /**
	// * @return
	// */
	// @Column(name ="TEMP", length = 255)
	// public String getTemp() {
	// return temp;
	// }
	// public void setTemp(String temp) {
	// this.temp = temp;
	// }
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 姓名
	 */
	@Column(name = "NAME", length = 60)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             姓名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 价格
	 */
	@Column(name = "PRICE", length = 60)
	public Double getPrice() {
		return this.price;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 取样时间
	 */
	public String getSampleTime() {
		return sampleTime;
	}

	/**
	 * 设置 : 取得String
	 * 
	 * @param sampleTime
	 */
	public void setSampleTime(String sampleTime) {
		this.sampleTime = sampleTime;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             价格
	 */
	public void setPrice(Double price) {
		this.price = price;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 付款方式
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	// /**
	// *方法: 取得String
	// *@return: String 电话
	// */
	// @Column(name ="PHONE_DATE", length = 255)
	// public String getPhoneDate(){
	// return this.phoneDate;
	// }
	// /**
	// *方法: 设置String
	// *@param: String 电话
	// */
	// public void setPhoneDate(String phoneDate){
	// this.phoneDate = phoneDate;
	// }

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return this.note;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             附件数量
	 */
	public String getFileNum() {
		return fileNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             附件数量
	 */
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	/**
	 * 方法：获取Hospital
	 * 
	 * @return String Hospital 送检医院
	 */
	@Column(name = "HOSPITAL", length = 120)
	public String getHospital() {
		return hospital;
	}

	/**
	 * 方法：设置Hospital
	 * 
	 * @param hospital
	 *            String 送检医院
	 */
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	@Column(name = "PATIENT_NAME", length = 120)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "STATE", length = 120)
	public String getState() {
		return state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_KIND")
	public DicSampleType getSampleType() {
		return sampleType;
	}

	public void setSampleType(DicSampleType sampleType) {
		this.sampleType = sampleType;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "STATE_NAME", length = 120)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法：获取BusinessType
	 * 
	 * @return String BusinessType 检测方法
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSINESS_TYPE")
	public WorkType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(WorkType businessType) {
		this.businessType = businessType;
	}

	public String getSpellName() {
		return spellName;
	}

	public void setSpellName(String spellName) {
		this.spellName = spellName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getBirthAddress() {
		return birthAddress;
	}

	public void setBirthAddress(String birthAddress) {
		this.birthAddress = birthAddress;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public Double getHight() {
		return hight;
	}

	public void setHight(Double hight) {
		this.hight = hight;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getMarriage() {
		return marriage;
	}

	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getTele() {
		return tele;
	}

	public void setTele(String tele) {
		this.tele = tele;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFamilyAddress() {
		return familyAddress;
	}

	public void setFamilyAddress(String familyAddress) {
		this.familyAddress = familyAddress;
	}

	public String getIsFee() {
		return isFee;
	}

	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}

	public String getPrivilege() {
		return privilege;
	}

	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}

	public String getReferUser() {
		return referUser;
	}

	public void setReferUser(String referUser) {
		this.referUser = referUser;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillTitle() {
		return billTitle;
	}

	public void setBillTitle(String billTitle) {
		this.billTitle = billTitle;
	}

	public String getInputUser() {
		return inputUser;
	}

	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}

	public String getConfirmUser1() {
		return confirmUser1;
	}

	public void setConfirmUser1(String confirmUser1) {
		this.confirmUser1 = confirmUser1;
	}

	public String getConfirmUser2() {
		return confirmUser2;
	}

	public void setConfirmUser2(String confirmUser2) {
		this.confirmUser2 = confirmUser2;
	}

	// public Date getReceiveDate() {
	// return receiveDate;
	// }
	// public void setReceiveDate(Date receiveDate) {
	// this.receiveDate = receiveDate;
	// }
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public String getMedicalCard() {
		return medicalCard;
	}

	public void setMedicalCard(String medicalCard) {
		this.medicalCard = medicalCard;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "PRODUCT_ID", length = 200)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 录入人
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ORDER_ID")
	public WorkOrder getOrderId() {
		return orderId;
	}

	public void setOrderId(WorkOrder orderId) {
		this.orderId = orderId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}

	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}

	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	public String getSampleUnit() {
		return sampleUnit;
	}

	public void setSampleUnit(String sampleUnit) {
		this.sampleUnit = sampleUnit;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public Double getProjectFee() {
		return projectFee;
	}

	public void setProjectFee(Double projectFee) {
		this.projectFee = projectFee;
	}

	public DicType getFeeWay() {
		return feeWay;
	}

	public void setFeeWay(DicType feeWay) {
		this.feeWay = feeWay;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getCreateUser1() {
		return createUser1;
	}

	public void setCreateUser1(String createUser1) {
		this.createUser1 = createUser1;
	}

	public String getCreateUser2() {
		return createUser2;
	}

	public void setCreateUser2(String createUser2) {
		this.createUser2 = createUser2;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SALES_MAN")
	public User getSalesman() {
		return salesman;
	}

	public void setSalesman(User salesman) {
		this.salesman = salesman;
	}

	public Date getUpImgTime() {
		return upImgTime;
	}

	public void setUpImgTime(Date upImgTime) {
		this.upImgTime = upImgTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INPUT")
	public SampleInput getSi() {
		return si;
	}

	public void setSi(SampleInput si) {
		this.si = si;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_CHROMOSOME")
	public SampleChromosome getSc() {
		return sc;
	}

	public void setSc(SampleChromosome sc) {
		this.sc = sc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_FOLIC_ACID")
	public SampleFolicAcid getSfa() {
		return sfa;
	}

	public void setSfa(SampleFolicAcid sfa) {
		this.sfa = sfa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_GENE")
	public SampleGene getSg() {
		return sg;
	}

	public void setSg(SampleGene sg) {
		this.sg = sg;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TUMOR")
	public SampleTumor getSt() {
		return st;
	}

	public void setSt(SampleTumor st) {
		this.st = st;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_VISIT")
	public SampleVisit getSv() {
		return sv;
	}

	public void setSv(SampleVisit sv) {
		this.sv = sv;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/** 订单实体 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/** 订单号 */
	@Column(name = "orderNum", length = 50)
	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	/** 病历号 */
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	/**
	 * 方法: 取得
	 * 
	 * @return: 肿瘤类别
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE")
	public DicType getDicType() {
		return dicType;
	}

	public void setDicType(DicType dicType) {
		this.dicType = dicType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 分期
	 */
	@Column(name = "SAMPLE_STAGE", length = 500)
	public String getSampleStage() {
		return sampleStage;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 分期
	 */
	public void setSampleStage(String sampleStage) {
		this.sampleStage = sampleStage;
	}

	/**
	 * 科研项目
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * 方法: 取得
	 * 
	 * @return: 样本关系
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PERSON_TYPE")
	public CrmFamilyPatientShip getPersonShip() {
		return personShip;
	}

	public void setPersonShip(CrmFamilyPatientShip personShip) {
		this.personShip = personShip;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_RECEIVE")
	public SampleReceive getSampleReceive() {
		return sampleReceive;
	}

	public void setSampleReceive(SampleReceive sampleReceive) {
		this.sampleReceive = sampleReceive;
	}

}