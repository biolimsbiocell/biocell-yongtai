package com.biolims.sample.model;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 血液病审核
 * @author lims-platform
 * @date 2015-11-03 16:18:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_BLOOD_DISEASE_TEMP")
@SuppressWarnings("serial")
public class SampleBloodDiseaseTemp extends EntityDao<SampleBloodDiseaseTemp> implements java.io.Serializable {
	/**信息录入id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**孕妇姓名*/
	private String patientName;
	/**姓名拼音*/
	private String patientNameSpell;
	/**年龄*/
	private String age;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**地区*/
	private String area;
	/**送检医院*/
	private String hospital;
	/**送检日期*/
	private Date sendDate;
	/**样本类型*/
	private DicType sampleType;
	/**检测时期*/
	private String gestationalAge;
	/**证件类型*/
	private DicType voucherType;
	/**证件号码*/
	private String voucherCode;
	/**化疗*/
	private String otherTumorHistory;
	//状态
	private String state;
	//状态名称
	private String stateName;
	//审核人
	private String createUser;
	/**检测项目id*/
	private String productId;
	/**检测项目name*/
	private String productName;
	/**具体的免疫抑制剂*/
	private String medicine;
	/**免疫抑制剂*/
	private String medicineType;
	/**骨骼生长基数因子*/
	private String cadmium;
	/**手机号码2*/
	private String phoneNum2;
	/**样本编号*/
	private String sampleNum;
	/**相关病史*/
	private String reason2;
	/**临床诊断*/
	private String diagnosis;
	/**是否收费*/
	private String isInsure;
	/**优惠类型*/
	private String privilegeType;
	/**推荐人*/
	private String linkman;
	/**是否需要发票*/
	private String isInvoice;
	/**开票单位*/
	private String paymentUnit;
	/**备注*/
	private String note;
	/**金额*/
	private String money;
	/**性别*/
	private String gender;
	/**联系方式*/
	private String phone;
	//放疗
	private String radioactiveRays;
	//化疗
	private String tumorHistory;
	//医生
	private String doctor;
	//是否收费
	private  String isFee;
	
	
	public String getRadioactiveRays() {
		return radioactiveRays;
	}
	public void setRadioactiveRays(String radioactiveRays) {
		this.radioactiveRays = radioactiveRays;
	}
	public String getTumorHistory() {
		return tumorHistory;
	}
	public void setTumorHistory(String tumorHistory) {
		this.tumorHistory = tumorHistory;
	}
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}
	public String getIsFee() {
		return isFee;
	}
	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}
	private String testItem;
	/**电子邮箱*/
	private String emailAddress;
	/**核对人1*/
	private User reportMan;
	/**核对人2*/
	private User auditMan;
	/**SP*/
	private String bloodHistory;
	/**数量*/
	private String confirmAgeOne;
	/**确诊年龄（2）*/
	private String confirmAgeTwo;
	/**原始细胞*/
	private String confirmAgeThree;
	/**幼稚细胞*/
	private String confirmAge;
	/**家庭住址*/
	private String address;
	/**骨髓检查结果*/
	private String badMotherhood;
	/**最后一次化疗时间*/
	private Date firstTransfusionDate;
	/**其他细胞*/
	private String stemCellsCure;
	/**重要体征*/
	private String reason;
	/**21-三体比值*/
	private String trisome21Value;
	/**18-三体比值*/
	private String trisome18Value;
	/**13-三体比值*/
	private String trisome13Value;
	/**下一步流向*/
	private String nextStepFlow;
	
	public String getNextStepFlow() {
		return nextStepFlow;
	}
	public void setNextStepFlow(String nextStepFlow) {
		this.nextStepFlow = nextStepFlow;
	}
	//相关主表
	private SampleInfo sampleInfo;

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getOtherTumorHistory() {
		return otherTumorHistory;
	}
	public void setOtherTumorHistory(String otherTumorHistory) {
		this.otherTumorHistory = otherTumorHistory;
	}
	public String getMedicine() {
		return medicine;
	}
	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}
	public String getMedicineType() {
		return medicineType;
	}
	public void setMedicineType(String medicineType) {
		this.medicineType = medicineType;
	}

	public String getCadmium() {
		return cadmium;
	}
	public void setCadmium(String cadmium) {
		this.cadmium = cadmium;
	}
	public String getBloodHistory() {
		return bloodHistory;
	}
	public void setBloodHistory(String bloodHistory) {
		this.bloodHistory = bloodHistory;
	}
	
	public String getConfirmAgeOne() {
		return confirmAgeOne;
	}
	public void setConfirmAgeOne(String confirmAgeOne) {
		this.confirmAgeOne = confirmAgeOne;
	}
	public String getConfirmAgeTwo() {
		return confirmAgeTwo;
	}
	public void setConfirmAgeTwo(String confirmAgeTwo) {
		this.confirmAgeTwo = confirmAgeTwo;
	}
	public String getConfirmAgeThree() {
		return confirmAgeThree;
	}
	public void setConfirmAgeThree(String confirmAgeThree) {
		this.confirmAgeThree = confirmAgeThree;
	}
	public String getConfirmAge() {
		return confirmAge;
	}
	public void setConfirmAge(String confirmAge) {
		this.confirmAge = confirmAge;
	}
	public String getTrisome13Value() {
		return trisome13Value;
	}
	public void setTrisome13Value(String trisome13Value) {
		this.trisome13Value = trisome13Value;
	}
	
	public String getPhoneNum2() {
		return phoneNum2;
	}
	public void setPhoneNum2(String phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getTestItem() {
		return testItem;
	}
	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTMAN")
	public User getReportMan() {
		return reportMan;
	}
	public void setReportMan(User reportMan) {
		this.reportMan = reportMan;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITMAN")
	public User getAuditMan() {
		return auditMan;
	}
	public void setAuditMan(User auditMan) {
		this.auditMan = auditMan;
	}
	/**

	 *方法: 取得String
	 *@return: String  信息录入id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  信息录入id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME", length = 100)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME_SPELL", length = 100)
	public String getPatientNameSpell() {
		return patientNameSpell;
	}
	public void setPatientNameSpell(String patientNameSpell) {
		this.patientNameSpell = patientNameSpell;
	}
	/**
	 *方法: 取得String
	 *@return: String  年龄
	 */
	@Column(name ="AGE", length = 100)
	public String getAge(){
		return this.age;
	}
	/**
	 *方法: 设置String
	 *@param: String  年龄
	 */
	public void setAge(String age){
		this.age = age;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 100)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 100)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  地区
	 */
	@Column(name ="AREA", length = 100)
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
	@Column(name ="HOSPITAL", length = 100)
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  送检日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	@Column(name ="GESTATIONAL_AGE", length = 100)
	public String getGestationalAge() {
		return gestationalAge;
	}
	public void setGestationalAge(String gestationalAge) {
		this.gestationalAge = gestationalAge;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "VOUCHER_TYPE")
	public DicType getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(DicType voucherType) {
		this.voucherType = voucherType;
	}
	@Column(name ="VOUCHER_CODE", length = 100)
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	@Column(name ="ADDRESS", length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name ="BAD_MOTHER_HOOD", length = 100)
	public String getBadMotherhood() {
		return badMotherhood;
	}
	public void setBadMotherhood(String badMotherhood) {
		this.badMotherhood = badMotherhood;
	}
	
	
	@Column(name ="FIRST_TRANSFUSION_DATE", length = 100)
	public Date getFirstTransfusionDate() {
		return firstTransfusionDate;
	}
	public void setFirstTransfusionDate(Date firstTransfusionDate) {
		this.firstTransfusionDate = firstTransfusionDate;
	}
	@Column(name ="STEMCELLS_CURE", length = 100)
	public String getStemCellsCure() {
		return stemCellsCure;
	}
	public void setStemCellsCure(String stemCellsCure) {
		this.stemCellsCure = stemCellsCure;
	}
	
	@Column(name ="REASON", length = 100)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name ="TRISOME_21VALUE", length = 100)
	public String getTrisome21Value() {
		return trisome21Value;
	}
	public void setTrisome21Value(String trisome21Value) {
		this.trisome21Value = trisome21Value;
	}
	@Column(name ="TRISOME_18VALUE", length = 100)
	public String getTrisome18Value() {
		return trisome18Value;
	}
	public void setTrisome18Value(String trisome18Value) {
		this.trisome18Value = trisome18Value;
	}
	
	@Column(name ="REASON2", length = 100)
	public String getReason2() {
		return reason2;
	}
	public void setReason2(String reason2) {
		this.reason2 = reason2;
	}
	@Column(name ="DIAGNOSIS", length = 200)
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	@Column(name ="IS_INSURE", length = 100)
	public String getIsInsure() {
		return isInsure;
	}
	public void setIsInsure(String isInsure) {
		this.isInsure = isInsure;
	}
	
	@Column(name ="PRIVILEGE_TYPE", length = 100)
	public String getPrivilegeType() {
		return privilegeType;
	}
	public void setPrivilegeType(String privilegeType) {
		this.privilegeType = privilegeType;
	}
	@Column(name ="IS_INVOICE", length = 100)
	public String getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(String isInvoice) {
		this.isInvoice = isInvoice;
	}
	@Column(name ="PAYMENT_UNIT", length = 100)
	public String getPaymentUnit() {
		return paymentUnit;
	}
	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}
	@Column(name ="MONEY", length = 100)
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="gender", length = 100)
	public String getGender(){
		return this.gender;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender){
		this.gender = gender;
	}
	/**
	 *方法: 取得User
	 *@return: User  联系人
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "LINKMAN")
	public String getLinkman(){
		return this.linkman;
	}
	/**
	 *方法: 设置User
	 *@param: User  联系人
	 */
	public void setLinkman(String linkman){
		this.linkman = linkman;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  联系方式
	 */
	@Column(name ="PHONE", length = 20)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系方式
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	@Column(name ="NOTE", length = 200)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 *方法: 取得
	 *@return:   相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	//上传图片
	private FileInfo upLoadAccessory;
	/**
	 * 方法:取得
	 * @return 图片上传
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
}