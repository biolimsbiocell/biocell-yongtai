package com.biolims.sample.model;
import java.util.Date;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 染色体
 * @author lims-platform
 * @date 2015-11-03 16:18:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_CHROMOSOME")
@SuppressWarnings("serial")
public class SampleChromosome extends EntityDao<SampleChromosome> implements java.io.Serializable {
	/**信息录入id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**孕妇姓名*/
	private String patientName;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**地区*/
	private String area;
	/**送检医院*/
	private String hospital;
	/**送检日期*/
	private Date sendDate;
	/**送检医生*/
	private String doctor;
	/**住院号*/
	private String inHosNum;
	/**样本类型*/
	private DicType sampleType;
	/**孕周*/
	private String gestationalAge;
	/**证件类型*/
	private DicType voucherType;
	/**证件号码*/
	private String voucherCode;
	/**手机号码*/
	private String phoneNum;
	/**特殊情况说明*/
	private String specialCircumstances;
	/**家庭住址*/
	private String address;
	/**IVF妊娠*/
	private String gestationIVF;
	/**孕几次*/
	private String pregnancyTime;
	/**产几次*/
	private String parturitionTime;
	/**不良孕产史*/
	private String badMotherhood;
	/**器官移植*/
	private String organGrafting;
	/**外源输血*/
	private String outTransfusion;
	/**干细胞治疗*/
	private String stemCellsCure;
	/**免疫治疗*/
	private String immuneCure;
	/**单/双/多胎*/
	private String embryoType;
	/**此次第几胎*/
	private String howManyfetus;
	/**异常结果描述*/
	private String reason;
	/**21-三体比值*/
	private String trisome21Value;
	/**手机号码2*/
	private String phoneNum2;
	/**检测项目id*/
	private String productId;
	/**检测项目name*/
	private String productName;
	//状态
	private String state;
	//状态名称
	private String stateName;
	/**代理人*/
	private String agentMan;
	/**代理人身份证*/
	private String cardCode;
	/**样本编号*/
	private String sampleNum;
	/**异常结果描述2*/
	private String reason2;
	/**临床诊断*/
	private String diagnosis;
	/**是否购买保险*/
	private String isInsure;
	/**是否需要发票*/
	private String isInvoice;
	/**开票单位*/
	private String paymentUnit;
	/**备注*/
	private String note;
	/**录入人*/
	private String createUser;
	/**金额*/
	private String money;
	/**流产孕周*/
	private String phone;
	//测试项目
	private String testItem;
	/**民族*/
	public String nationality;
	/**个人卵巢肿瘤病史*/
	private String tumorHistory;
	/**电子邮箱*/
	private String emailAddress;
	/**籍贯*/
	public String nativePlace;
	/**核对人1*/
	private User reportMan;
	/**收据*/
	private String receipt;
	/**附加样本*/
	private String attachedSample1;
	/**附加样本*/
	private String attachedSample2;
	/**核心分析*/
	private String coreAnalyze;
	
	public String getCoreAnalyze() {
		return coreAnalyze;
	}
	public void setCoreAnalyze(String coreAnalyze) {
		this.coreAnalyze = coreAnalyze;
	}
	public String getAttachedSample1() {
		return attachedSample1;
	}
	public void setAttachedSample1(String attachedSample1) {
		this.attachedSample1 = attachedSample1;
	}
	public String getAttachedSample2() {
		return attachedSample2;
	}
	public void setAttachedSample2(String attachedSample2) {
		this.attachedSample2 = attachedSample2;
	}
	public String getHowManyfetus() {
		return howManyfetus;
	}
	public void setHowManyfetus(String howManyfetus) {
		this.howManyfetus = howManyfetus;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	/**核对人2*/
	private User auditMan;
//	/**下一步流向*/
//	private String nextStepFlow;
//	public String getNextStepFlow() {
//		return nextStepFlow;
//	}
//	public void setNextStepFlow(String nextStepFlow) {
//		this.nextStepFlow = nextStepFlow;
//	}
	//相关主表
	private SampleInfo sampleInfo;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPhoneNum2() {
		return phoneNum2;
	}
	public void setPhoneNum2(String phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}
	public String getAgentMan() {
		return agentMan;
	}
	public void setAgentMan(String agentMan) {
		this.agentMan = agentMan;
	}
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getTestItem() {
		return testItem;
	}
	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getTumorHistory() {
		return tumorHistory;
	}
	public void setTumorHistory(String tumorHistory) {
		this.tumorHistory = tumorHistory;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getNativePlace() {
		return nativePlace;
	}
	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}
	public String getSpecialCircumstances() {
		return specialCircumstances;
	}
	public void setSpecialCircumstances(String specialCircumstances) {
		this.specialCircumstances = specialCircumstances;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTMAN")
	public User getReportMan() {
		return reportMan;
	}
	public void setReportMan(User reportMan) {
		this.reportMan = reportMan;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITMAN")
	public User getAuditMan() {
		return auditMan;
	}
	public void setAuditMan(User auditMan) {
		this.auditMan = auditMan;
	}
	/**
	 *方法: 取得String
	 *@return: String  信息录入id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  信息录入id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME", length = 100)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 100)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 100)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  地区
	 */
	@Column(name ="AREA", length = 100)
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
	@Column(name ="HOSPITAL", length = 100)
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  送检日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  送检医生
	 */
	@Column(name ="DOCTOR", length = 100)
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}
	@Column(name ="INHOS_NUM", length = 100)
	public String getInHosNum() {
		return inHosNum;
	}
	public void setInHosNum(String inHosNum) {
		this.inHosNum = inHosNum;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}
	
	@Column(name ="GESTATIONAL_AGE", length = 100)
	public String getGestationalAge() {
		return gestationalAge;
	}
	public void setGestationalAge(String gestationalAge) {
		this.gestationalAge = gestationalAge;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "VOUCHER_TYPE")
	public DicType getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(DicType voucherType) {
		this.voucherType = voucherType;
	}
	@Column(name ="VOUCHER_CODE", length = 100)
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	@Column(name ="ADDRESS", length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name ="GESTATION_IVF", length = 100)
	public String getGestationIVF() {
		return gestationIVF;
	}
	public void setGestationIVF(String gestationIVF) {
		this.gestationIVF = gestationIVF;
	}
	@Column(name ="PREGNANCY_TIME", length = 100)

	public String getPregnancyTime() {
		return pregnancyTime;
	}
	public void setPregnancyTime(String pregnancyTime) {
		this.pregnancyTime = pregnancyTime;
	}
	@Column(name ="PARTURITION_TIME", length = 100)
	public String getParturitionTime() {
		return parturitionTime;
	}
	public void setParturitionTime(String parturitionTime) {
		this.parturitionTime = parturitionTime;
	}
	@Column(name ="BAD_MOTHER_HOOD", length = 100)
	public String getBadMotherhood() {
		return badMotherhood;
	}
	public void setBadMotherhood(String badMotherhood) {
		this.badMotherhood = badMotherhood;
	}
	@Column(name ="ORGAN_GRAFTING", length = 100)
	public String getOrganGrafting() {
		return organGrafting;
	}
	public void setOrganGrafting(String organGrafting) {
		this.organGrafting = organGrafting;
	}
	@Column(name ="OUT_TRANSFUSION", length = 100)
	public String getOutTransfusion() {
		return outTransfusion;
	}
	public void setOutTransfusion(String outTransfusion) {
		this.outTransfusion = outTransfusion;
	}
	
	@Column(name ="STEMCELLS_CURE", length = 100)
	public String getStemCellsCure() {
		return stemCellsCure;
	}
	public void setStemCellsCure(String stemCellsCure) {
		this.stemCellsCure = stemCellsCure;
	}
	@Column(name ="IMMUNE_CURE", length = 100)
	public String getImmuneCure() {
		return immuneCure;
	}
	public void setImmuneCure(String immuneCure) {
		this.immuneCure = immuneCure;
	}
	@Column(name ="EMBRYO_TYPE", length = 100)
	public String getEmbryoType() {
		return embryoType;
	}
	public void setEmbryoType(String embryoType) {
		this.embryoType = embryoType;
	}
	
	@Column(name ="REASON", length = 100)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name ="TRISOME_21VALUE", length = 100)
	public String getTrisome21Value() {
		return trisome21Value;
	}
	public void setTrisome21Value(String trisome21Value) {
		this.trisome21Value = trisome21Value;
	}
	
	@Column(name ="REASON2", length = 100)
	public String getReason2() {
		return reason2;
	}
	public void setReason2(String reason2) {
		this.reason2 = reason2;
	}
	@Column(name ="DIAGNOSIS", length = 200)
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	@Column(name ="IS_INSURE", length = 100)
	public String getIsInsure() {
		return isInsure;
	}
	public void setIsInsure(String isInsure) {
		this.isInsure = isInsure;
	}
	@Column(name ="IS_INVOICE", length = 100)
	public String getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(String isInvoice) {
		this.isInvoice = isInvoice;
	}
	@Column(name ="PAYMENT_UNIT", length = 100)
	public String getPaymentUnit() {
		return paymentUnit;
	}
	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}
	@Column(name ="MONEY", length = 100)
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  联系方式
	 */
	@Column(name ="PHONE", length = 20)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系方式
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	@Column(name ="NOTE", length = 200)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	/**
	 * 方法：获取PhoneNum
	 * @return String 手机号码
	 */
	@Column(name="PHONE_NUM",length=11)
	public String getPhoneNum() {
		return phoneNum;
	}
	/**
	 * 方法：设置PhoneNum
	 * @param phoneNum String 手机号码
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	/**
	 *方法: 取得
	 *@return:   相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	/**上传附件*/
	private FileInfo upLoadAccessory;
	/**
	 * 方法: 取得String
	 * @return String  上传附件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	/**
	 * 方法: 设置String
	 * @param  String  上传附件
	 */
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
}