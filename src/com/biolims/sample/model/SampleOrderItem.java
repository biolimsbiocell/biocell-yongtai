package com.biolims.sample.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.Empty2Null;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 订单用药信息表
 * @author lims-platform
 * @date 2016-03-11 12:11:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_ORDER_ITEM")
@SuppressWarnings("serial")
public class SampleOrderItem extends EntityDao<SampleOrderItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**用药时间*/
	private Date drugDate;
	/**曾用药*/
	private String useDrugName;
	/**姓名*/
	private String name;
	/**性别*/
	private String gender;
	/**药效进展*/
	private String effectOfProgress;
	/**药效进展快慢*/
	private String effectOfProgressSpeed;
	/**基因检测史*/
	private String geneticTestHistory;
	/**检测基因名*/
	private String sampleDetectionName;
	/**基因外显子区*/
	private String sampleExonRegion;
	/**检测最终结果*/
	private String sampleDetectionResult;
	/**检测最终结果*/
	private SampleOrder sampleOrder;
	//样本编号
	private String sampleCode;
	//样本类型
	private DicSampleType sampleType;
	//家系描述
	private String family;
	//核型分析
	private String hxfx;
	/**样本状态ID*/
	private String state;
	/**样本状态*/
	private String stateName;
	/**备注*/
	private String note;
	/**接收时间*/
	private Date receiveDate;
	/**备注*/
	private User receiveUser;
	/**条码号*/
	private String slideCode;
	/**放行结果*/
	private String releaseResults;
	
	
	
	public String getReleaseResults() {
		return releaseResults;
	}
	public void setReleaseResults(String releaseResults) {
		this.releaseResults = releaseResults;
	}
	/**采样日期*/
	private Date samplingDate;
	
	
	public Date getSamplingDate() {
		return samplingDate;
	}
	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicSampleType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicSampleType sampleType) {
		this.sampleType = sampleType;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public String getHxfx() {
		return hxfx;
	}
	public void setHxfx(String hxfx) {
		this.hxfx = hxfx;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  用药时间
	 */
	@Column(name ="DRUG_DATE", length = 100)
	public Date getDrugDate(){
		return this.drugDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  用药时间
	 */
	public void setDrugDate(Date drugDate){
		this.drugDate = drugDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  曾用药
	 */
	@Column(name ="USE_DRUG_NAME", length = 100)
	public String getUseDrugName(){
		return this.useDrugName;
	}
	/**
	 *方法: 设置String
	 *@param: String  曾用药
	 */
	public void setUseDrugName(String useDrugName){
		this.useDrugName = useDrugName;
	}
	/**
	 *方法: 取得String
	 *@return: String  药效进展
	 */
	@Column(name ="EFFECT_OF_PROGRESS", length = 50)
	public String getEffectOfProgress(){
		return this.effectOfProgress;
	}
	/**
	 *方法: 设置String
	 *@param: String  药效进展
	 */
	public void setEffectOfProgress(String effectOfProgress){
		this.effectOfProgress = effectOfProgress;
	}
	/**
	 *方法: 取得String
	 *@return: String  药效进展快慢
	 */
	@Column(name ="EFFECT_OF_PROGRESS_SPEED", length = 50)
	public String getEffectOfProgressSpeed(){
		return this.effectOfProgressSpeed;
	}
	/**
	 *方法: 设置String
	 *@param: String  药效进展快慢
	 */
	public void setEffectOfProgressSpeed(String effectOfProgressSpeed){
		this.effectOfProgressSpeed = effectOfProgressSpeed;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因检测史
	 */
	@Column(name ="GENETIC_TEST_HISTORY", length = 500)
	public String getGeneticTestHistory(){
		return this.geneticTestHistory;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因检测史
	 */
	public void setGeneticTestHistory(String geneticTestHistory){
		this.geneticTestHistory = geneticTestHistory;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测基因名
	 */
	@Column(name ="SAMPLE_DETECTION_NAME", length = 50)
	public String getSampleDetectionName(){
		return this.sampleDetectionName;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测基因名
	 */
	public void setSampleDetectionName(String sampleDetectionName){
		this.sampleDetectionName = sampleDetectionName;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因外显子区
	 */
	@Column(name ="SAMPLE_EXON_REGION", length = 50)
	public String getSampleExonRegion(){
		return this.sampleExonRegion;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因外显子区
	 */
	public void setSampleExonRegion(String sampleExonRegion){
		this.sampleExonRegion = sampleExonRegion;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测最终结果
	 */
	@Column(name ="SAMPLE_DETECTION_RESULT", length = 50)
	public String getSampleDetectionResult(){
		return this.sampleDetectionResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测最终结果
	 */
	public void setSampleDetectionResult(String sampleDetectionResult){
		this.sampleDetectionResult = sampleDetectionResult;
	}
	/**
	 *方法: 取得SampleOrder
	 *@return: SampleOrder  检测最终结果
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder(){
		return this.sampleOrder;
	}
	/**
	 *方法: 设置SampleOrder
	 *@param: SampleOrder  检测最终结果
	 */
	public void setSampleOrder(SampleOrder sampleOrder){
		this.sampleOrder = sampleOrder;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECEIVE_USER")
	public User getReceiveUser() {
		return receiveUser;
	}
	public void setReceiveUser(User receiveUser) {
		this.receiveUser = receiveUser;
	}
	
	
	public String getSlideCode() {
		return slideCode;
	}
	public void setSlideCode(String slideCode) {
		this.slideCode = slideCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}