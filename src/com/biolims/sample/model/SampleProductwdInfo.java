package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmFamilyPatientShip;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 样本主数据检测项目位点
 * @author lims-platform
 * @date 2015-11-03 16:21:36
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_PRODUCT_WD_INFO")
@SuppressWarnings("serial")
public class SampleProductwdInfo extends EntityDao<SampleProductwdInfo> implements java.io.Serializable {
	/** 编码 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 样本名称 */
	private String name;
	/** 病人姓名 */
	private String patientName;
	/** 项目编号 */
	private String productId;
	/** 项目名称 */
	private String productName;
	/** 项目关联位点 */
	private String plCode;
	/** 已做实验位点 */
	private String testPlCode;
	/** 位点检测结果 */
	private String testResult;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 备注 */
	private String note;
	/** 订单实体 */
	private SampleOrder sampleOrder;
	/** 订单号 */
	private String orderNum;
	/** 病历号 */
	private String patientId;
	/**报告文件 */
	private FileInfo fileInfo;
	/**下载次数*/
	private String downNum;
	/**下载IP*/
	private String downIp;
	/**下载人*/
	private String downUser;
	/**下载时间*/
	private String downDate;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")   @JoinColumn(name = "FIlE_INFO")
	public FileInfo getFileInfo() {
		return fileInfo;
	}
	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
	public String getPlCode() {
		return plCode;
	}

	public void setPlCode(String plCode) {
		this.plCode = plCode;
	}

	public String getTestPlCode() {
		return testPlCode;
	}

	public void setTestPlCode(String testPlCode) {
		this.testPlCode = testPlCode;
	}


	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 姓名
	 */
	@Column(name = "NAME", length = 60)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 姓名
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "PATIENT_NAME", length = 120)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}


	@Column(name = "STATE", length = 120)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "STATE_NAME", length = 120)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "PRODUCT_ID", length = 200)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/** 订单实体 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	   @ForeignKey(name = "none")   @JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/** 订单号 */
	@Column(name = "orderNum", length = 50)
	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	/** 病历号 */
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getDownNum() {
		return downNum;
	}
	public void setDownNum(String downNum) {
		this.downNum = downNum;
	}
	public String getDownIp() {
		return downIp;
	}
	public void setDownIp(String downIp) {
		this.downIp = downIp;
	}
	public String getDownUser() {
		return downUser;
	}
	public void setDownUser(String downUser) {
		this.downUser = downUser;
	}
	public String getDownDate() {
		return downDate;
	}
	public void setDownDate(String downDate) {
		this.downDate = downDate;
	}
}