package com.biolims.sample.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
/**   
 * @Title: Model
 * @Description: 肿瘤
 * @author lims-platform
 * @date 2015-11-03 16:18:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_TUMOR")
@SuppressWarnings("serial")
public class SampleTumor extends EntityDao<SampleTumor> implements java.io.Serializable {
	/**信息录入id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**孕妇姓名*/
	private String patientName;
	/**姓名拼音*/
	private String patientNameSpell;
	/**年龄*/
	private String age;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**地区*/
	private String area;
	/**送检医院*/
	private String hospital;
	/**送检日期*/
	private Date sendDate;
	/**送检医生*/
	private String doctor;	
	/**样本类型*/
	private DicType sampleType;
	/**体重*/
	private String weight;
	/**孕周*/
	private String gestationalAge;
	/**证件类型*/
	private DicType voucherType;
	/**证件号码*/
	private String voucherCode;
	/**烟*/
	private String cigarette;
	/**酒*/
	private String wine;
	/**药物*/
	private String medicine;	
	/**放射线*/
	private String radioactiveRays;
	/**农药*/
	private String pesticide;
	/**铅*/
	private String plumbane;
	/**汞*/
	private String mercury;
	/**镉*/
	private String cadmium;
	/**血压*/
	private String bloodPressure;
	/**心电图*/
	private String electrocardiogram;
	/**备注1*/
	private String note1;
	/**备注2*/
	private String note2;
	/**备注3*/
	private String note3;
	/**备注4*/
	private String note4;
	/**备注5*/
	private String note5;
	/**备注6*/
	private String note6;
	/**备注7*/
	private String note7;
	/**备注8*/
	private String note8;
	/**备注9*/
	private String note9;
	/**备注10*/
	private String note10;
	/**是否有过手术*/
	private String isSurgery;
	/**其他*/
	private String other;
	/**是否用过药物*/
	private String isPharmacy;
	/**是否存在下列疾病*/
	private String isAilment;
	/**直系亲属中是否患有肿瘤*/
	private String isSickenTumour;
	/**SP*/
	private String sp;
	/**个人血液病史*/
	private String bloodHistory;
	/**确诊年龄（1）*/
	private String confirmAgeOne;
	/**确诊年龄（2）*/
	private String confirmAgeTwo;
	/**确认年龄（3）*/
	private String confirmAgeThree;
	/**亲属关系*/
	private String relationship;
	/**家庭住址*/
	private String address;
	/**IVF妊娠*/
	private String gestationIVF;
	/**产几次*/
	private String parturitionTime;
	/**异常结果描述*/
	private String reason;
	/**样本编号*/
	private String sampleNum;
	//检测内容
	private String detectionContent;
	/**临床诊断*/
	private String diagnosis;
	/**用药史*/
	private String pharmacy;
	/**病例描述*/
	private String caseDescribe;
	/**简要病史*/
	private String medicalHistory;
	/**是否购买保险*/
	private String isInsure;
	/**是否收费*/
	private String isFee;
	/**优惠类型*/
	private String privilegeType;
	/**推荐人*/
	private String linkman;
	/**是否需要发票*/
	private String isInvoice;
	/**开票单位*/
	private String paymentUnit;
	/**备注*/
	private String note;
	/**录入人*/
	private String createUser;
	/**金额*/
	private String money;
	/**性别*/
	private String gender;
	/**联系方式*/
	private String phone;
	//检测项目
	private String testItem;
	/**检测项目id*/
	private String productId;
	/**检测项目name*/
	private String productName;
	/**民族*/
	public String nationality;	
	/**电子邮箱*/
	private String emailAddress;
	/**身高*/
	public String heights;
	/**籍贯*/
	public String nativePlace;
	/**报告者*/
	private User reportMan;
	/**审核者*/
	private User auditMan;
	/**病历号*/
	private String serialNum;
//	/**下一步流向*/
//	private String nextStepFlow;
//	
//	public String getNextStepFlow() {
//		return nextStepFlow;
//	}
//	public void setNextStepFlow(String nextStepFlow) {
//		this.nextStepFlow = nextStepFlow;
//	}
	//相关主表
	private SampleInfo sampleInfo;
	public String getPharmacy() {
		return pharmacy;
	}
	public void setPharmacy(String pharmacy) {
		this.pharmacy = pharmacy;
	}
	public String getCaseDescribe() {
		return caseDescribe;
	}
	public void setCaseDescribe(String caseDescribe) {
		this.caseDescribe = caseDescribe;
	}
	public String getDetectionContent() {
		return detectionContent;
	}
	public void setDetectionContent(String detectionContent) {
		this.detectionContent = detectionContent;
	}
	
	public String getIsSurgery() {
		return isSurgery;
	}
	public void setIsSurgery(String isSurgery) {
		this.isSurgery = isSurgery;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public String getIsPharmacy() {
		return isPharmacy;
	}
	public void setIsPharmacy(String isPharmacy) {
		this.isPharmacy = isPharmacy;
	}
	public String getIsAilment() {
		return isAilment;
	}
	public void setIsAilment(String isAilment) {
		this.isAilment = isAilment;
	}
	public String getIsSickenTumour() {
		return isSickenTumour;
	}
	public void setIsSickenTumour(String isSickenTumour) {
		this.isSickenTumour = isSickenTumour;
	}
	public String getNote1() {
		return note1;
	}
	public void setNote1(String note1) {
		this.note1 = note1;
	}
	public String getNote2() {
		return note2;
	}
	public void setNote2(String note2) {
		this.note2 = note2;
	}
	public String getNote3() {
		return note3;
	}
	public void setNote3(String note3) {
		this.note3 = note3;
	}
	public String getNote4() {
		return note4;
	}
	public void setNote4(String note4) {
		this.note4 = note4;
	}
	
	public String getNote5() {
		return note5;
	}
	public void setNote5(String note5) {
		this.note5 = note5;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getElectrocardiogram() {
		return electrocardiogram;
	}
	public void setElectrocardiogram(String electrocardiogram) {
		this.electrocardiogram = electrocardiogram;
	}
	public String getCigarette() {
		return cigarette;
	}
	public void setCigarette(String cigarette) {
		this.cigarette = cigarette;
	}
	public String getWine() {
		return wine;
	}
	public void setWine(String wine) {
		this.wine = wine;
	}
	public String getMedicine() {
		return medicine;
	}
	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}
	
	public String getRadioactiveRays() {
		return radioactiveRays;
	}
	public void setRadioactiveRays(String radioactiveRays) {
		this.radioactiveRays = radioactiveRays;
	}
	public String getPesticide() {
		return pesticide;
	}
	public void setPesticide(String pesticide) {
		this.pesticide = pesticide;
	}
	public String getPlumbane() {
		return plumbane;
	}
	public void setPlumbane(String plumbane) {
		this.plumbane = plumbane;
	}
	public String getMercury() {
		return mercury;
	}
	public void setMercury(String mercury) {
		this.mercury = mercury;
	}
	public String getCadmium() {
		return cadmium;
	}
	public void setCadmium(String cadmium) {
		this.cadmium = cadmium;
	}
	
	public String getBloodHistory() {
		return bloodHistory;
	}
	public void setBloodHistory(String bloodHistory) {
		this.bloodHistory = bloodHistory;
	}
	public String getConfirmAgeOne() {
		return confirmAgeOne;
	}
	public void setConfirmAgeOne(String confirmAgeOne) {
		this.confirmAgeOne = confirmAgeOne;
	}
	public String getConfirmAgeTwo() {
		return confirmAgeTwo;
	}
	public void setConfirmAgeTwo(String confirmAgeTwo) {
		this.confirmAgeTwo = confirmAgeTwo;
	}
	
	public String getConfirmAgeThree() {
		return confirmAgeThree;
	}
	public void setConfirmAgeThree(String confirmAgeThree) {
		this.confirmAgeThree = confirmAgeThree;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getTestItem() {
		return testItem;
	}
	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getHeights() {
		return heights;
	}
	public void setHeights(String heights) {
		this.heights = heights;
	}
	public String getNativePlace() {
		return nativePlace;
	}
	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}
	public String getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTMAN")
	public User getReportMan() {
		return reportMan;
	}
	public void setReportMan(User reportMan) {
		this.reportMan = reportMan;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITMAN")
	public User getAuditMan() {
		return auditMan;
	}
	public void setAuditMan(User auditMan) {
		this.auditMan = auditMan;
	}
	/**

	 *方法: 取得String
	 *@return: String  信息录入id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  信息录入id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME", length = 100)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME_SPELL", length = 100)
	public String getPatientNameSpell() {
		return patientNameSpell;
	}
	public void setPatientNameSpell(String patientNameSpell) {
		this.patientNameSpell = patientNameSpell;
	}
	/**
	 *方法: 取得String
	 *@return: String  年龄
	 */
	@Column(name ="AGE", length = 100)
	public String getAge(){
		return this.age;
	}
	/**
	 *方法: 设置String
	 *@param: String  年龄
	 */
	public void setAge(String age){
		this.age = age;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 100)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 100)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  地区
	 */
	@Column(name ="AREA", length = 100)
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
	@Column(name ="HOSPITAL", length = 100)
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  送检日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  送检医生
	 */
	@Column(name ="DOCTOR", length = 100)
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}
	@Column(name ="WEIGHT", length = 100)
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	@Column(name ="GESTATIONAL_AGE", length = 100)
	public String getGestationalAge() {
		return gestationalAge;
	}
	public void setGestationalAge(String gestationalAge) {
		this.gestationalAge = gestationalAge;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "VOUCHER_TYPE")
	public DicType getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(DicType voucherType) {
		this.voucherType = voucherType;
	}
	@Column(name ="VOUCHER_CODE", length = 100)
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	@Column(name ="ADDRESS", length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name ="GESTATION_IVF", length = 100)
	public String getGestationIVF() {
		return gestationIVF;
	}
	public void setGestationIVF(String gestationIVF) {
		this.gestationIVF = gestationIVF;
	}


	@Column(name ="PARTURITION_TIME", length = 100)

	public String getParturitionTime() {
		return parturitionTime;
	}
	public void setParturitionTime(String parturitionTime) {
		this.parturitionTime = parturitionTime;
	}
	@Column(name ="REASON", length = 100)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
	
	@Column(name ="DIAGNOSIS", length = 200)
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	@Column(name ="MEDICAL_HISTORY", length = 200)
	public String getMedicalHistory() {
		return medicalHistory;
	}
	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}
	@Column(name ="IS_INSURE", length = 100)
	public String getIsInsure() {
		return isInsure;
	}
	public void setIsInsure(String isInsure) {
		this.isInsure = isInsure;
	}
	@Column(name ="IS_FEE", length = 100)
	public String getIsFee() {
		return isFee;
	}
	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}
	@Column(name ="PRIVILEGE_TYPE", length = 100)
	public String getPrivilegeType() {
		return privilegeType;
	}
	public void setPrivilegeType(String privilegeType) {
		this.privilegeType = privilegeType;
	}
	@Column(name ="IS_INVOICE", length = 100)
	public String getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(String isInvoice) {
		this.isInvoice = isInvoice;
	}
	@Column(name ="PAYMENT_UNIT", length = 100)
	public String getPaymentUnit() {
		return paymentUnit;
	}
	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}
	@Column(name ="MONEY", length = 100)
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="gender", length = 100)
	public String getGender(){
		return this.gender;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender){
		this.gender = gender;
	}
	/**
	 *方法: 取得User
	 *@return: User  联系人
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "LINKMAN")
	public String getLinkman(){
		return this.linkman;
	}
	/**
	 *方法: 设置User
	 *@param: User  联系人
	 */
	public void setLinkman(String linkman){
		this.linkman = linkman;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  联系方式
	 */
	@Column(name ="PHONE", length = 20)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系方式
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	@Column(name ="NOTE", length = 200)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	private String state;
	private String stateName;

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 *方法: 取得
	 *@return:   相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	/**上传附件*/
	private FileInfo upLoadAccessory;
	/**
	 * 方法: 取得String
	 * @return String  上传附件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	/**
	 * 方法: 设置String
	 * @param  String  上传附件
	 */
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
	public String getNote6() {
		return note6;
	}
	public void setNote6(String note6) {
		this.note6 = note6;
	}
	public String getNote7() {
		return note7;
	}
	public void setNote7(String note7) {
		this.note7 = note7;
	}
	public String getNote8() {
		return note8;
	}
	public void setNote8(String note8) {
		this.note8 = note8;
	}
	public String getNote9() {
		return note9;
	}
	public void setNote9(String note9) {
		this.note9 = note9;
	}
	public String getNote10() {
		return note10;
	}
	public void setNote10(String note10) {
		this.note10 = note10;
	}
	public String getSp() {
		return sp;
	}
	public void setSp(String sp) {
		this.sp = sp;
	}
}