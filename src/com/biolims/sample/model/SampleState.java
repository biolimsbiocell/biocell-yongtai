package com.biolims.sample.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;

/**
 * 样品状态
 * 
 */
@Entity
@Table(name = "T_SAMPLE_STATE")
public class SampleState implements Serializable {

	private static final long serialVersionUID = -407759837281114436L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;// ID

	@Column(name = "CODE", length = 32)
	private String code;// 样本编号

	@Column(name = "SAMPLE_CODE", length = 100)
	private String sampleCode;// 样本编号
	
	@Column(name = "PRODUCT_ID", length = 100)
	private String productId;// 检测类型

	@Column(name = "PRODUCT_NAME", length = 100)
	private String productName;// 检测
	
	@Column(name = "STAGE_TIME", length = 100)
	private String stageTime;//阶段时间

	private String startDate;//开始时间
	private String endDate;//结束时间


	@Column(name = "TABLE_TYPE_ID", length = 32)
	private String tableTypeId;// 阶段id

	@Column(name = "STAGE_NAME", length = 32)
	private String stageName;// 阶段名称

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER_ID")
	private User acceptUser;//操作人

	@Column(name = "TASK_ID", length = 100)
	private String taskId;// 执行单ID
	
	@Column(name = "TASK_METHOD", length = 100)
	private String taskMethod;// 执行处理方式
	
	@Column(name = "TASK_RESULT", length = 100)
	private String taskResult;// 执行结果
	
	@Column(name = "TECH_TASK_ID", length = 100)
	private String techTaskId;// 科技服务任务单ID
	
	@Column(name = "Note", length = 200)
	private String note;// 说明

	
	@Column(name = "Note2", length = 200)
	private String note2;// 说明
	
	
	@Column(name = "Note3", length = 200)
	private String note3;// 说明
	
	
	@Column(name = "Note4", length = 200)
	private String note4;// 说明
	
	
	@Column(name = "Note5", length = 200)
	private String note5;// 说明

	@Column(name = "Note6", length = 200)
	private String note6;// 说明
	

	@Column(name = "Note7", length = 200)
	private String note7;// 说明
	
	/**关联订单*/
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	private SampleOrder sampleOrder;
	
	
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public String getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(String taskResult) {
		this.taskResult = taskResult;
	}


	public String getStageTime() {
		return stageTime;
	}

	public void setStageTime(String stageTime) {
		this.stageTime = stageTime;
	}

	

	

	public String getTableTypeId() {
		return tableTypeId;
	}

	public void setTableTypeId(String tableTypeId) {
		this.tableTypeId = tableTypeId;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public User getAcceptUser() {
		return acceptUser;
	}

	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}


	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getNote2() {
		return note2;
	}

	public void setNote2(String note2) {
		this.note2 = note2;
	}

	public String getNote3() {
		return note3;
	}

	public void setNote3(String note3) {
		this.note3 = note3;
	}

	public String getNote4() {
		return note4;
	}

	public void setNote4(String note4) {
		this.note4 = note4;
	}

	public String getNote5() {
		return note5;
	}

	public void setNote5(String note5) {
		this.note5 = note5;
	}

	public String getNote6() {
		return note6;
	}

	public void setNote6(String note6) {
		this.note6 = note6;
	}

	public String getNote7() {
		return note7;
	}

	public void setNote7(String note7) {
		this.note7 = note7;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	public String getTaskMethod() {
		return taskMethod;
	}

	public void setTaskMethod(String taskMethod) {
		this.taskMethod = taskMethod;
	}

}
