package com.biolims.sample.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 订单表
 * @author lims-platform
 * @date 2016-03-07 11:01:47
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_ORDER_CHANGE_ITEM")
@SuppressWarnings("serial")
public class SampleOrderChangeItem extends EntityDao<SampleOrderChangeItem> implements
		java.io.Serializable {
	/** 编码 */
	@Id
	private String id;
	/** 原来订单的编码*/
	private String oldId;
	/**更新后的id*/
	private String updateId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	private SampleOrder sampleOrder;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER_CHANGE")
	private SampleOrderChange sampleOrderChange;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public String getUpdateId() {
		return updateId;
	}

	public void setUpdateId(String updateId) {
		this.updateId = updateId;
	}

	
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public SampleOrderChange getSampleOrderChange() {
		return sampleOrderChange;
	}

	public void setSampleOrderChange(SampleOrderChange sampleOrderChange) {
		this.sampleOrderChange = sampleOrderChange;
	}

	
}
