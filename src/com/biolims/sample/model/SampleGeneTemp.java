package com.biolims.sample.model;
import java.util.Date;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 基因审核
 * @author lims-platform
 * @date 2015-11-03 16:18:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_GENE_TEMP")
@SuppressWarnings("serial")
public class SampleGeneTemp extends EntityDao<SampleGeneTemp> implements java.io.Serializable {
	/**信息录入id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**孕妇姓名*/
	private String patientName;
	/**姓名拼音*/
	private String patientNameSpell;
	/**年龄*/
	private String age;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**地区*/
	private String area;
	/**送检医院*/
	private String hospital;
	/**送检日期*/
	private Date sendDate;
	/**送检医生*/
	private String doctor;
	/**样本类型*/
	private DicType sampleType;	
	/**孕周*/
	private String gestationalAge;
	/**证件类型*/
	private DicType voucherType;
	/**证件号码*/
	private String voucherCode;
	/**个人其他肿瘤病史*/
	private String otherTumorHistory;
	/**烟*/
	private String cigarette;
	/**酒*/
	private String wine;
	/**药物*/
	private String medicine;	
	/**个人血液病史*/
	private String bloodHistory;
	/**确认年龄（3）*/
	private String confirmAgeThree;
	/**确诊年龄（2）*/
	private String confirmAgeTwo;
	/**亲属关系*/
	private String relationship;
	/**家庭住址*/
	private String address;
	/**异常结果描述*/
	private String reason;
	/**样本编号*/
	private String sampleNum;
	/**简要病史*/
	private String medicalHistory;
	/**是否购买保险*/
	private String isInsure;
	/**是否收费*/
	private String isFee;
	/**优惠类型*/
	private String privilegeType;
	/**推荐人*/
	private String linkman;
	/**是否需要发票*/
	private String isInvoice;
	/**开票单位*/
	private String paymentUnit;
	/**备注*/
	private String note;
	/**备注1*/
	private String note1;
	/**备注2*/
	private String note2;
	/**备注3*/
	private String note3;
	/**备注4*/
	private String note4;
	/**SP*/
	private String SP;
	/**录入人*/
	private String createUser;
	/**金额*/
	private double money;
	/**性别*/
	private String gender;
	/**联系方式*/
	private String phone;
	//测试项目
	private String testItem;
	/**电子邮箱*/
	private String emailAddress;
	/**籍贯*/
	public String nativePlace;
	/**检测项目id*/
	private String productId;
	/**检测项目name*/
	private String productName;
	//状态
	private String state;
	//状态名称
	private String stateName;
	/**核对人1*/
	private User reportMan;
	/**核对人2*/
	private User auditMan;
	/**民族*/
	private String nationality;
	/**下一步流向*/
	private String nextStepFlow;
	//铅
	private String plumbane;
	//汞
	private String mercury;
	//农药
	private String pesticide;
	//放射线
	private String radioactiveRays;
	//备注
	private String reason2;
	//个人生育史
	private String tumorHistory;
	//镉
	private String cadmium;
	//临床诊断
	private String diagnose;
	//用药史
	private String pharmacy;
	//病例描述
	private String caseDescribe;

	public String getDiagnose() {
		return diagnose;
	}
	public void setDiagnose(String diagnose) {
		this.diagnose = diagnose;
	}
	public String getPharmacy() {
		return pharmacy;
	}
	public void setPharmacy(String pharmacy) {
		this.pharmacy = pharmacy;
	}
	public String getCaseDescribe() {
		return caseDescribe;
	}
	public void setCaseDescribe(String caseDescribe) {
		this.caseDescribe = caseDescribe;
	}
	public String getSP() {
		return SP;
	}
	public void setSP(String sP) {
		SP = sP;
	}
	public String getCadmium() {
		return cadmium;
	}
	public void setCadmium(String cadmium) {
		this.cadmium = cadmium;
	}
	public String getPlumbane() {
		return plumbane;
	}
	public void setPlumbane(String plumbane) {
		this.plumbane = plumbane;
	}
	public String getMercury() {
		return mercury;
	}
	public void setMercury(String mercury) {
		this.mercury = mercury;
	}
	public String getPesticide() {
		return pesticide;
	}
	public void setPesticide(String pesticide) {
		this.pesticide = pesticide;
	}
	public String getRadioactiveRays() {
		return radioactiveRays;
	}
	public void setRadioactiveRays(String radioactiveRays) {
		this.radioactiveRays = radioactiveRays;
	}
	public String getReason2() {
		return reason2;
	}
	public void setReason2(String reason2) {
		this.reason2 = reason2;
	}
	public String getTumorHistory() {
		return tumorHistory;
	}
	public void setTumorHistory(String tumorHistory) {
		this.tumorHistory = tumorHistory;
	}
	public String getNextStepFlow() {
		return nextStepFlow;
	}
	public void setNextStepFlow(String nextStepFlow) {
		this.nextStepFlow = nextStepFlow;
	}

	//相关主表
	private SampleInfo sampleInfo;

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getNote1() {
		return note1;
	}
	public void setNote1(String note1) {
		this.note1 = note1;
	}
	
	public String getNote2() {
		return note2;
	}
	public void setNote2(String note2) {
		this.note2 = note2;
	}
	public String getNote3() {
		return note3;
	}
	public void setNote3(String note3) {
		this.note3 = note3;
	}
	public String getNote4() {
		return note4;
	}
	public void setNote4(String note4) {
		this.note4 = note4;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTMAN")
	public User getReportMan() {
		return reportMan;
	}
	public void setReportMan(User reportMan) {
		this.reportMan = reportMan;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITMAN")
	public User getAuditMan() {
		return auditMan;
	}
	public void setAuditMan(User auditMan) {
		this.auditMan = auditMan;
	}
	public String getOtherTumorHistory() {
		return otherTumorHistory;
	}
	public void setOtherTumorHistory(String otherTumorHistory) {
		this.otherTumorHistory = otherTumorHistory;
	}
	public String getCigarette() {
		return cigarette;
	}
	public void setCigarette(String cigarette) {
		this.cigarette = cigarette;
	}
	public String getWine() {
		return wine;
	}
	public void setWine(String wine) {
		this.wine = wine;
	}
	public String getMedicine() {
		return medicine;
	}
	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}

	public String getBloodHistory() {
		return bloodHistory;
	}
	public void setBloodHistory(String bloodHistory) {
		this.bloodHistory = bloodHistory;
	}
	
	private String confirmAgeOne;

	public String getConfirmAgeOne() {
		return confirmAgeOne;
	}
	public void setConfirmAgeOne(String confirmAgeOne) {
		this.confirmAgeOne = confirmAgeOne;
	}
	public String getConfirmAgeTwo() {
		return confirmAgeTwo;
	}
	public void setConfirmAgeTwo(String confirmAgeTwo) {
		this.confirmAgeTwo = confirmAgeTwo;
	}
	public String getConfirmAgeThree() {
		return confirmAgeThree;
	}
	public void setConfirmAgeThree(String confirmAgeThree) {
		this.confirmAgeThree = confirmAgeThree;
	}
	
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getTestItem() {
		return testItem;
	}
	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getNativePlace() {
		return nativePlace;
	}
	public void setNativePlace(String nativePlace) {
		this.nativePlace = nativePlace;
	}
	
	/**

	 *方法: 取得String
	 *@return: String  信息录入id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  信息录入id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME", length = 100)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME_SPELL", length = 100)
	public String getPatientNameSpell() {
		return patientNameSpell;
	}
	public void setPatientNameSpell(String patientNameSpell) {
		this.patientNameSpell = patientNameSpell;
	}
	/**
	 *方法: 取得String
	 *@return: String  年龄
	 */
	@Column(name ="AGE", length = 100)
	public String getAge(){
		return this.age;
	}
	/**
	 *方法: 设置String
	 *@param: String  年龄
	 */
	public void setAge(String age){
		this.age = age;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 100)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 100)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  地区
	 */
	@Column(name ="AREA", length = 100)
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
	@Column(name ="HOSPITAL", length = 100)
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  送检日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  送检医生
	 */
	@Column(name ="DOCTOR", length = 100)
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	@Column(name ="GESTATIONAL_AGE", length = 100)
	public String getGestationalAge() {
		return gestationalAge;
	}
	public void setGestationalAge(String gestationalAge) {
		this.gestationalAge = gestationalAge;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "VOUCHER_TYPE")
	public DicType getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(DicType voucherType) {
		this.voucherType = voucherType;
	}
	@Column(name ="VOUCHER_CODE", length = 100)
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	@Column(name ="ADDRESS", length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name ="REASON", length = 100)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name ="MEDICAL_HISTORY", length = 200)
	public String getMedicalHistory() {
		return medicalHistory;
	}
	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}
	@Column(name ="IS_INSURE", length = 100)
	public String getIsInsure() {
		return isInsure;
	}
	public void setIsInsure(String isInsure) {
		this.isInsure = isInsure;
	}
	@Column(name ="IS_FEE", length = 100)
	public String getIsFee() {
		return isFee;
	}
	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}
	@Column(name ="PRIVILEGE_TYPE", length = 100)
	public String getPrivilegeType() {
		return privilegeType;
	}
	public void setPrivilegeType(String privilegeType) {
		this.privilegeType = privilegeType;
	}
	@Column(name ="IS_INVOICE", length = 100)
	public String getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(String isInvoice) {
		this.isInvoice = isInvoice;
	}
	@Column(name ="PAYMENT_UNIT", length = 100)
	public String getPaymentUnit() {
		return paymentUnit;
	}
	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}
	@Column(name ="MONEY", length = 100)
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="gender", length = 100)
	public String getGender(){
		return this.gender;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender){
		this.gender = gender;
	}
	/**
	 *方法: 取得User
	 *@return: User  联系人
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "LINKMAN")
	public String getLinkman(){
		return this.linkman;
	}
	/**
	 *方法: 设置User
	 *@param: User  联系人
	 */
	public void setLinkman(String linkman){
		this.linkman = linkman;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  联系方式
	 */
	@Column(name ="PHONE", length = 20)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系方式
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	@Column(name ="NOTE", length = 200)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	/**
	 *方法: 取得
	 *@return:   相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	/**上传附件*/
	private FileInfo upLoadAccessory;
	/**
	 * 方法: 取得String
	 * @return String  上传附件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	/**
	 * 方法: 设置String
	 * @param  String  上传附件
	 */
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
}