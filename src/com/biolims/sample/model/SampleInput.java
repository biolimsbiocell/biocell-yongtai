package com.biolims.sample.model;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 信息录入
 * @author lims-platform
 * @date 2015-11-03 16:18:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_INPUT")
@SuppressWarnings("serial")
public class SampleInput extends EntityDao<SampleInput> implements java.io.Serializable {
	/**信息录入id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**孕妇姓名*/
	private String patientName;
	/**姓名拼音*/
	private String patientNameSpell;
	/**年龄*/
	private String age;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**地区*/
	private String area;
	/**送检医院*/
	private String hospital;
	/**送检日期*/
	private Date sendDate;
	/**送检医生*/
	private String doctor;
	/**住院号*/
	private String inHosNum;
	/**样本类型*/
	private DicType sampleType;
	/**体重*/
	private String weight;
	/**孕周*/
	private String gestationalAge;
	/**证件类型*/
	private DicType voucherType;
	/**证件号码*/
	private String voucherCode;
	/**手机号码*/
	private String phoneNum;
	/**补充协议*/
	private String suppleAgreement;
	/**家庭住址*/
	private String address;
	/**末次月经*/
	private Date endMenstruationDate;
	/**IVF妊娠*/
	private String gestationIVF;
	/**孕几次*/
	private String pregnancyTime;
	/**产几次*/
	private String parturitionTime;
	/**不良孕产史*/
	private String badMotherhood;
	/**器官移植*/
	private String organGrafting;
	/**外源输血*/
	private String outTransfusion;
	/**最后一次外源输血时间*/
	private Date firstTransfusionDate;
	/**干细胞治疗*/
	private String stemCellsCure;
	/**免疫治疗*/
	private String immuneCure;
	/**最后一次免疫治疗时间*/
	private Date endImmuneCureDate;
	/**单/双/多胎*/
	private String embryoType;
	/**NT值*/
	private String NT;
	/**异常结果描述*/
	private String reason;
	/**筛查模式*/
	private String testPattern;
	/**21-三体比值*/
	private String trisome21Value;
	/**18-三体比值*/
	private String trisome18Value;
	/**13-三体比值*/
	private String trisome13Value;
	/**检测项目id*/
	private String productId;
	/**检测项目name*/
	private String productName;
	/**处理意见*/
	private String handlingSuggestion;
	/**下一步流向*/
	private String nextStepFlow;
	/**确认执行*/
	private String confirmCarry;
	/**夫妻双方染色体*/
	private String coupleChromosome;
	/**样本编号*/
	private String sampleNum;
	/**异常结果描述2*/
	private String reason2;
	/**临床诊断*/
	private String diagnosis;
	/**简要病史*/
	private String medicalHistory;
	/**是否购买保险*/
	private String isInsure;
	/**是否收费*/
	private String isFee;
	/**优惠类型*/
	private String privilegeType;
	/**推荐人*/
	private User linkman;
	/**是否需要发票*/
	private String isInvoice;
	/**开票单位*/
	private String paymentUnit;
	/**备注*/
	private String note;
	/**录入人*/
	private User createUser;
	/**金额*/
	private String money;
	/**收据类型*/
	private DicType receiptType;
	/**性别*/
	private String gender;
	/**出生日期*/
	private Date birthday;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**联系方式*/
	private String phone;
	/**21-三体（参考范围）*/
	private String reference21Range;
	/**报告者*/
	private User reportMan;
	/**审核者*/
	private User auditMan;
	
	private User detectionMan;
	/**18-三体（参考范围）*/
	private String reference18Range;
	
	/**13-三体（参考范围）*/
	private String reference13Range;
	/**上传附件*/
	private FileInfo upLoadAccessory;
	//取样打包明细Id	
	private String sid;
	
	public String getHandlingSuggestion() {
		return handlingSuggestion;
	}

	public void setHandlingSuggestion(String handlingSuggestion) {
		this.handlingSuggestion = handlingSuggestion;
	}

	public String getNextStepFlow() {
		return nextStepFlow;
	}

	public void setNextStepFlow(String nextStepFlow) {
		this.nextStepFlow = nextStepFlow;
	}

	public String getConfirmCarry() {
		return confirmCarry;
	}

	public void setConfirmCarry(String confirmCarry) {
		this.confirmCarry = confirmCarry;
	}
	@Column(name ="PRODUCT_ID", length = 100)
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	@Column(name ="PRODUCT_NAME", length = 100)
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getReference21Range() {
		return reference21Range;
	}
	public void setReference21Range(String reference21Range) {
		this.reference21Range = reference21Range;
	}
	public String getReference18Range() {
		return reference18Range;
	}
	public void setReference18Range(String reference18Range) {
		this.reference18Range = reference18Range;
	}
	public String getReference13Range() {
		return reference13Range;
	}
	public void setReference13Range(String reference13Range) {
		this.reference13Range = reference13Range;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTMAN")
	public User getReportMan() {
		return reportMan;
	}
	public void setReportMan(User reportMan) {
		this.reportMan = reportMan;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITMAN")
	public User getAuditMan() {
		return auditMan;
	}
	public void setAuditMan(User auditMan) {
		this.auditMan = auditMan;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DETECTIONMAN")
	public User getDetectionMan() {
		return detectionMan;
	}
	public void setDetectionMan(User detectionMan) {
		this.detectionMan = detectionMan;
	}
	
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	private String messages;
	
	public String getMessages() {
		return messages;
	}
	public void setMessages(String messages) {
		this.messages = messages;
	}
	/**

	 *方法: 取得String
	 *@return: String  信息录入id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  信息录入id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME", length = 100)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME_SPELL", length = 100)
	public String getPatientNameSpell() {
		return patientNameSpell;
	}
	public void setPatientNameSpell(String patientNameSpell) {
		this.patientNameSpell = patientNameSpell;
	}
	/**
	 *方法: 取得String
	 *@return: String  年龄
	 */
	@Column(name ="AGE", length = 100)
	public String getAge(){
		return this.age;
	}
	/**
	 *方法: 设置String
	 *@param: String  年龄
	 */
	public void setAge(String age){
		this.age = age;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 100)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 100)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  地区
	 */
	@Column(name ="AREA", length = 100)
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
	@Column(name ="HOSPITAL", length = 100)
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  送检日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  送检医生
	 */
	@Column(name ="DOCTOR", length = 100)
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}
	@Column(name ="INHOS_NUM", length = 100)
	public String getInHosNum() {
		return inHosNum;
	}
	public void setInHosNum(String inHosNum) {
		this.inHosNum = inHosNum;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}
	@Column(name ="GESTATIONAL_AGE", length = 100)
	public String getGestationalAge() {
		return gestationalAge;
	}
	public void setGestationalAge(String gestationalAge) {
		this.gestationalAge = gestationalAge;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "VOUCHER_TYPE")
	public DicType getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(DicType voucherType) {
		this.voucherType = voucherType;
	}
	@Column(name ="VOUCHER_CODE", length = 100)
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	@Column(name ="ADDRESS", length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Column(name ="END_MENSTRUATION_DATE", length = 100)
	public Date getEndMenstruationDate() {
		return endMenstruationDate;
	}
	public void setEndMenstruationDate(Date endMenstruationDate) {
		this.endMenstruationDate = endMenstruationDate;
	}
	@Column(name ="GESTATION_IVF", length = 100)
	public String getGestationIVF() {
		return gestationIVF;
	}
	public void setGestationIVF(String gestationIVF) {
		this.gestationIVF = gestationIVF;
	}
	@Column(name ="BAD_MOTHER_HOOD", length = 100)
	public String getBadMotherhood() {
		return badMotherhood;
	}
	public void setBadMotherhood(String badMotherhood) {
		this.badMotherhood = badMotherhood;
	}
	@Column(name ="ORGAN_GRAFTING", length = 100)
	public String getOrganGrafting() {
		return organGrafting;
	}
	public void setOrganGrafting(String organGrafting) {
		this.organGrafting = organGrafting;
	}
	@Column(name ="OUT_TRANSFUSION", length = 100)
	public String getOutTransfusion() {
		return outTransfusion;
	}
	public void setOutTransfusion(String outTransfusion) {
		this.outTransfusion = outTransfusion;
	}
	@Column(name ="FIRST_TRANSFUSION_DATE", length = 100)
	public Date getFirstTransfusionDate() {
		return firstTransfusionDate;
	}
	public void setFirstTransfusionDate(Date firstTransfusionDate) {
		this.firstTransfusionDate = firstTransfusionDate;
	}
	@Column(name ="STEMCELLS_CURE", length = 100)
	public String getStemCellsCure() {
		return stemCellsCure;
	}
	public void setStemCellsCure(String stemCellsCure) {
		this.stemCellsCure = stemCellsCure;
	}
	@Column(name ="IMMUNE_CURE", length = 100)
	public String getImmuneCure() {
		return immuneCure;
	}
	public void setImmuneCure(String immuneCure) {
		this.immuneCure = immuneCure;
	}
	@Column(name ="END_IMMUNE_DATE", length = 100)
	public Date getEndImmuneCureDate() {
		return endImmuneCureDate;
	}
	public void setEndImmuneCureDate(Date endImmuneCureDate) {
		this.endImmuneCureDate = endImmuneCureDate;
	}
	@Column(name ="EMBRYO_TYPE", length = 100)
	public String getEmbryoType() {
		return embryoType;
	}
	public void setEmbryoType(String embryoType) {
		this.embryoType = embryoType;
	}
	@Column(name ="NT", length = 100)
	public String getNT() {
		return NT;
	}
	public void setNT(String nT) {
		NT = nT;
	}
	@Column(name ="REASON", length = 100)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name ="TEST_PATTERN", length = 100)
	public String getTestPattern() {
		return testPattern;
	}
	public void setTestPattern(String testPattern) {
		this.testPattern = testPattern;
	}
	@Column(name ="COUPLE_CHROMOSOME", length = 100)
	public String getCoupleChromosome() {
		return coupleChromosome;
	}
	public void setCoupleChromosome(String coupleChromosome) {
		this.coupleChromosome = coupleChromosome;
	}
	@Column(name ="REASON2", length = 100)
	public String getReason2() {
		return reason2;
	}
	public void setReason2(String reason2) {
		this.reason2 = reason2;
	}
	@Column(name ="DIAGNOSIS", length = 200)
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	@Column(name ="MEDICAL_HISTORY", length = 200)
	public String getMedicalHistory() {
		return medicalHistory;
	}
	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}
	@Column(name ="IS_INSURE", length = 100)
	public String getIsInsure() {
		return isInsure;
	}
	public void setIsInsure(String isInsure) {
		this.isInsure = isInsure;
	}
	@Column(name ="IS_FEE", length = 100)
	public String getIsFee() {
		return isFee;
	}
	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}
	@Column(name ="PRIVILEGE_TYPE", length = 100)
	public String getPrivilegeType() {
		return privilegeType;
	}
	public void setPrivilegeType(String privilegeType) {
		this.privilegeType = privilegeType;
	}
	@Column(name ="IS_INVOICE", length = 100)
	public String getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(String isInvoice) {
		this.isInvoice = isInvoice;
	}
	@Column(name ="PAYMENT_UNIT", length = 100)
	public String getPaymentUnit() {
		return paymentUnit;
	}
	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECEIPT_TYPE")
	public DicType getReceiptType() {
		return receiptType;
	}
	public void setReceiptType(DicType receiptType) {
		this.receiptType = receiptType;
	}
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="gender", length = 100)
	public String getGender(){
		return this.gender;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender){
		this.gender = gender;
	}
	/**
	 *方法: 取得User
	 *@return: User  联系人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "LINKMAN")
	public User getLinkman(){
		return this.linkman;
	}
	/**
	 *方法: 设置User
	 *@param: User  联系人
	 */
	public void setLinkman(User linkman){
		this.linkman = linkman;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  出生日期
	 */
	@Column(name ="BIRTHDAY", length = 50)
	public Date getBirthday(){
		return this.birthday;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  出生日期
	 */
	public void setBirthday(Date birthday){
		this.birthday = birthday;
	}
	/**
	 *方法: 取得String
	 *@return: String  联系方式
	 */
	@Column(name ="PHONE", length = 20)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系方式
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	@Column(name ="NOTE", length = 200)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 * 方法：获取PhoneNum
	 * @return String 手机号码
	 */
	@Column(name="PHONE_NUM",length=11)
	public String getPhoneNum() {
		return phoneNum;
	}
	/**
	 * 方法：设置PhoneNum
	 * @param phoneNum String 手机号码
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getSuppleAgreement() {
		return suppleAgreement;
	}

	public void setSuppleAgreement(String suppleAgreement) {
		this.suppleAgreement = suppleAgreement;
	}
	/**
	 * 方法: 取得String
	 * @return String  上传附件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	/**
	 * 方法: 设置String
	 * @param  String  上传附件
	 */
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}

	public String getPregnancyTime() {
		return pregnancyTime;
	}

	public void setPregnancyTime(String pregnancyTime) {
		this.pregnancyTime = pregnancyTime;
	}

	public String getParturitionTime() {
		return parturitionTime;
	}

	public void setParturitionTime(String parturitionTime) {
		this.parturitionTime = parturitionTime;
	}

	public String getTrisome21Value() {
		return trisome21Value;
	}

	public void setTrisome21Value(String trisome21Value) {
		this.trisome21Value = trisome21Value;
	}

	public String getTrisome18Value() {
		return trisome18Value;
	}

	public void setTrisome18Value(String trisome18Value) {
		this.trisome18Value = trisome18Value;
	}

	public String getTrisome13Value() {
		return trisome13Value;
	}

	public void setTrisome13Value(String trisome13Value) {
		this.trisome13Value = trisome13Value;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
	
}