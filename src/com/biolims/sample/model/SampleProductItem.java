package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.family.model.Family;



@SuppressWarnings("serial")
@Entity
@Table(name="SAMPLE_PRODUCT_ITEM")
public class SampleProductItem extends EntityDao<SampleProductItem>{
	/**编号*/
	private String id;
	/**名字*/
	private String name;
	/**名字修正*/
	private String amendName;
	/**关联的订单主表*/
	private SampleOrder sampleOrder;
	/**绑定产品*/
	private String productId;
	private String productName;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	private String patientId;
	
	/** 性别 */
	private java.lang.String gender;
	/** 出生年月 */
	private java.util.Date dateOfBirth;
	/** 年龄 */
	private java.lang.String age;
	/** 籍贯 */
	private java.lang.String placeOfBirth;
	/**是否患病*/	
	private DicType phenotype;
	/** 录入人 */
	private User createUser;
	/** 录入日期 */
	private Date createDate;
	/** 修改时间*/
	private Date modifyDate;
	/**关联家庭*/
	private Family family;
	/**微信号*/
	private String weichatId;
	/** 家庭住址 */
	private java.lang.String address;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "phenotype")
	public DicType getPhenotype() {
		return phenotype;
	}
	public void setPhenotype(DicType phenotype) {
		this.phenotype = phenotype;
	}
	public String getWeichatId() {
		return weichatId;
	}
	public void setWeichatId(String weichatId) {
		this.weichatId = weichatId;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "ADDRESS", length = 200)
	public java.lang.String getAddress() {
		return this.address;
	}

	
	public void setAddress(java.lang.String address) {
		this.address = address;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "family")
	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 鏂规硶: 璁剧疆User
	 * 
	 * @param: User 鍒涘缓浜�
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}
	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鎬у埆
	 */
	@Column(name = "GENDER", length = 100)
	public java.lang.String getGender() {
		return this.gender;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鎬у埆
	 */
	public void setGender(java.lang.String gender) {
		this.gender = gender;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鍑虹敓骞存湀
	 */
	@Column(name = "DATE_OF_BIRTH", length = 100)
	public java.util.Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date 鍑虹敓骞存湀
	 */
	public void setDateOfBirth(java.util.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 骞撮緞
	 */
	@Column(name = "AGE", length = 100)
	public java.lang.String getAge() {
		return this.age;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 骞撮緞
	 */
	public void setAge(java.lang.String age) {
		this.age = age;
	}

	@Column(name = "PLACE_OF_BIRTH", length = 200)
	public java.lang.String getPlaceOfBirth() {
		return this.placeOfBirth;
	}
	@Column(name = "CREATE_DATE", length = 60)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 绫嶈疮
	 */
	public void setPlaceOfBirth(java.lang.String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	public String getAmendName() {
		return amendName;
	}
	public void setAmendName(String amendName) {
		this.amendName = amendName;
	}


	@Column(name = "MODIFY_DATE", length = 50)
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	

}
