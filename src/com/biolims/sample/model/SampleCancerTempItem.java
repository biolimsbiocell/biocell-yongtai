package com.biolims.sample.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 检测订单-用药信息
 * @author lims-platform
 * @date 2016-03-07 11:11:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_CANCER_TEMP_ITEM")
@SuppressWarnings("serial")
public class SampleCancerTempItem extends EntityDao<SampleCancerTempItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**用药时间*/
	private Date drugDate;
	/**曾用药*/
	private String useDrugName;
	/**药效进展*/
	private String effectOfProgress;
	/**药效进展快慢*/
	private String effectOfProgressSpeed;
	/**基因检测史*/
	private String geneticTestHistory;
	/**
	 * 新增字段
	 */
	/**检测基因名*/
	private String sampleDetectionName;
	
	/**基因外显子区*/
	private String sampleExonRegion;
	
	/**检测最终结果*/
	private String sampleDetectionResult;
	
	/**相关主表*/
	private SampleCancerTemp sampleCancerTemp;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  用药时间
	 */
	@Column(name ="DRUG_DATE", length = 100)
	public Date getDrugDate(){
		return this.drugDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  用药时间
	 */
	public void setDrugDate(Date drugDate){
		this.drugDate = drugDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  曾用药
	 */
	@Column(name ="USE_DRUG_NAME", length = 100)
	public String getUseDrugName(){
		return this.useDrugName;
	}
	/**
	 *方法: 设置String
	 *@param: String  曾用药
	 */
	public void setUseDrugName(String useDrugName){
		this.useDrugName = useDrugName;
	}
	/**
	 *方法: 取得String
	 *@return: String  药效进展
	 */
	@Column(name ="EFFECT_OF_PROGRESS", length = 50)
	public String getEffectOfProgress(){
		return this.effectOfProgress;
	}
	/**
	 *方法: 设置String
	 *@param: String  药效进展
	 */
	public void setEffectOfProgress(String effectOfProgress){
		this.effectOfProgress = effectOfProgress;
	}
	/**
	 *方法: 取得String
	 *@return: String  药效进展快慢
	 */
	@Column(name ="EFFECT_OF_PROGRESS_SPEED", length = 50)
	public String getEffectOfProgressSpeed(){
		return this.effectOfProgressSpeed;
	}
	/**
	 *方法: 设置String
	 *@param: String  药效进展快慢
	 */
	public void setEffectOfProgressSpeed(String effectOfProgressSpeed){
		this.effectOfProgressSpeed = effectOfProgressSpeed;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因检测史
	 */
	@Column(name ="GENETIC_TEST_HISTORY", length = 500)
	public String getGeneticTestHistory(){
		return this.geneticTestHistory;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因检测史
	 */
	public void setGeneticTestHistory(String geneticTestHistory){
		this.geneticTestHistory = geneticTestHistory;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  检测基因名
	 */
	@Column(name ="SAMPLE_DETECTION_NAME", length = 50)
	public String getSampleDetectionName() {
		return sampleDetectionName;
	}
	public void setSampleDetectionName(String sampleDetectionName) {
		this.sampleDetectionName = sampleDetectionName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  检测外显子区
	 */
	@Column(name ="SAMPLE_EXON_REGION", length = 50)
	public String getSampleExonRegion() {
		return sampleExonRegion;
	}
	public void setSampleExonRegion(String sampleExonRegion) {
		this.sampleExonRegion = sampleExonRegion;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  检测最终结果
	 */
	@Column(name ="SAMPLE_DETECTION_RESULT", length = 50)
	public String getSampleDetectionResult() {
		return sampleDetectionResult;
	}
	public void setSampleDetectionResult(String sampleDetectionResult) {
		this.sampleDetectionResult = sampleDetectionResult;
	}
	/**
	 *方法: 取得SampleCancerTemp
	 *@return: SampleCancerTemp  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_CANCER_TEMP")
	public SampleCancerTemp getSampleCancerTemp(){
		return this.sampleCancerTemp;
	}
	/**
	 *方法: 设置SampleCancerTemp
	 *@param: SampleCancerTemp  相关主表
	 */
	public void setSampleCancerTemp(SampleCancerTemp sampleCancerTemp){
		this.sampleCancerTemp = sampleCancerTemp;
	}
}