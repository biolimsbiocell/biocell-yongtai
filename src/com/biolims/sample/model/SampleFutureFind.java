package com.biolims.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 样本预排表
 * @author lims-platform
 * @date 2015-11-03 16:21:36
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_FUTURE_FIND")
@SuppressWarnings("serial")
public class SampleFutureFind extends EntityDao<SampleFutureFind> implements java.io.Serializable,Cloneable {
	/** 编码 */
	private String id;
	/** 筛选号 */
	private String filtrateCode;
	/** 时间 */
	private String sampleTime;
	/** 轮次 */
	private String round;
	/** 事件（1采血2回输3采血+回输） */
	private String happenEvent;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;

	private String dingDanCode;
	//状态  0在组 1剔除 2脱落 3复发 4死亡5 其它
	private String sampleState;
	
	//时间属于哪    预排时间0黑色    红色真实1红色  预排和真实相同2黄色
	private String timeState;
	//能否点击 1能0不能
	private String timeShowState;
		
	

	public String getTimeShowState() {
		return timeShowState;
	}

	public void setTimeShowState(String timeShowState) {
		this.timeShowState = timeShowState;
	}

	public String getTimeState() {
		return timeState;
	}

	public void setTimeState(String timeState) {
		this.timeState = timeState;
	}

	public String getSampleState() {
		return sampleState;
	}

	public void setSampleState(String sampleState) {
		this.sampleState = sampleState;
	}

	public String getDingDanCode() {
		return dingDanCode;
	}

	public void setDingDanCode(String dingDanCode) {
		this.dingDanCode = dingDanCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getFiltrateCode() {
		return filtrateCode;
	}

	public void setFiltrateCode(String filtrateCode) {
		this.filtrateCode = filtrateCode;
	}

	public String getSampleTime() {
		return sampleTime;
	}

	public void setSampleTime(String sampleTime) {
		this.sampleTime = sampleTime;
	}

	public String getRound() {
		return round;
	}

	public void setRound(String round) {
		this.round = round;
	}

	public String getHappenEvent() {
		return happenEvent;
	}

	public void setHappenEvent(String happenEvent) {
		this.happenEvent = happenEvent;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}