package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
/**   
 * @Title: Model
 * @Description: 科技服务信息录入
 * @author lims-platform
 * @date 2016-02-18 11:49:32
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_INPUT_TECHNOLOGY")
@SuppressWarnings("serial")
public class SampleInputTechnology extends EntityDao<SampleInputTechnology> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**接受日期*/
	private Date sendDate;
	/**合同编号*/
	private String contractNum;
	/**项目名称*/
	private String projectName;
	/**客户名称*/
	private String clientName;
	/**客户单位*/
	private String clientUnit;
	/**样本状态*/
	private String sampleState;
	/**物种*/
	private String species;
	/**器官类型*/
	private String organType;
	/**样本名称*/
	private String sampleName;
	/**样本类型*/
	private DicType sampleType;
	/**样本编码*/
	private String sampleCode;
	/**开箱接收人*/
	private String openBoxUser;
	/**录入人*/
	private User createUser;
	/**录入时间*/
	private Date createDate;
	/**备注*/
	private String note;
	/**是否合格*/
	private String isQualified;
	/**相关主表*/
	private SampleInfo sampleInfo;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  接受日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  接受日期
	 */
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  合同编号
	 */
	@Column(name ="CONTRACT_NUM", length = 100)
	public String getContractNum() {
		return contractNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  合同编号
	 */
	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  项目名称
	 */
	@Column(name ="PROJECT_NAME", length = 100)
	public String getProjectName(){
		return this.projectName;
	}
	/**
	 *方法: 设置String
	 *@param: String  项目名称
	 */
	public void setProjectName(String projectName){
		this.projectName = projectName;
	}
	/**
	 *方法: 取得String
	 *@return: String  客户名称
	 */
	@Column(name ="CLIENT_NAME", length = 50)
	public String getClientName(){
		return this.clientName;
	}
	/**
	 *方法: 设置String
	 *@param: String  客户名称
	 */
	public void setClientName(String clientName){
		this.clientName = clientName;
	}
	/**
	 *方法: 取得String
	 *@return: String  客户单位
	 */
	@Column(name ="CLIENT_UNIT", length = 50)
	public String getClientUnit(){
		return this.clientUnit;
	}
	/**
	 *方法: 设置String
	 *@param: String  客户单位
	 */
	public void setClientUnit(String clientUnit){
		this.clientUnit = clientUnit;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本状态
	 */
	@Column(name ="SAMPLE_STATE", length = 50)
	public String getSampleState(){
		return this.sampleState;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本状态
	 */
	public void setSampleState(String sampleState){
		this.sampleState = sampleState;
	}
	/**
	 *方法: 取得String
	 *@return: String  物种
	 */
	@Column(name ="SPECIES", length = 50)
	public String getSpecies(){
		return this.species;
	}
	/**
	 *方法: 设置String
	 *@param: String  物种
	 */
	public void setSpecies(String species){
		this.species = species;
	}
	/**
	 *方法: 取得String
	 *@return: String  器官类型
	 */
	@Column(name ="ORGAN_TYPE", length = 50)
	public String getOrganType(){
		return this.organType;
	}
	/**
	 *方法: 设置String
	 *@param: String  器官类型
	 */
	public void setOrganType(String organType){
		this.organType = organType;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本名称
	 */
	@Column(name ="SAMPLE_NAME", length = 50)
	public String getSampleName(){
		return this.sampleName;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本名称
	 */
	public void setSampleName(String sampleName){
		this.sampleName = sampleName;
	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType(){
		return this.sampleType;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  样本类型
	 */
	public void setSampleType(DicType sampleType){
		this.sampleType = sampleType;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编码
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编码
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  开箱接收人
	 */
	@Column(name ="OPEN_BOX_USER", length = 50)
	public String getOpenBoxUser(){
		return this.openBoxUser;
	}
	/**
	 *方法: 设置String
	 *@param: String  开箱接收人
	 */
	public void setOpenBoxUser(String openBoxUser){
		this.openBoxUser = openBoxUser;
	}
	/**
	 *方法: 取得User
	 *@return: User  录入人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  录入人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  录入时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  录入时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="IS_QUALIFIED", length = 50)
	public String getIsQualified(){
		return this.isQualified;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsQualified(String isQualified){
		this.isQualified = isQualified;
	}
	/**
	 *方法: 取得String
	 *@return: String  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	/**
	 *方法: 设置String
	 *@param: String  相关主表
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
}