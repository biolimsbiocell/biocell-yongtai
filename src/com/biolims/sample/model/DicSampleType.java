package com.biolims.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 样本类型
 * @author lims-platform
 * @date 2016-03-08 09:59:34
 * @version V1.0
 * 
 */
@Entity
@Table(name = "DIC_SAMPLE_TYPE")
@SuppressWarnings("serial")
public class DicSampleType extends EntityDao<DicSampleType> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 姓名 */
	private String name;
	/** 标记 */
	private String code;
	/** 说明 */
	private String note;
	/** 排序号 */
	private int orderNumber;
	/** 类型 */
	private DicType type;
	/** 下一步流向 */
	private String nextFlow;
	/** 状态 */
	private String state;
	/** 状态 */
	private String stateName;
	// 下一步流向ID
	private String nextFlowId;
	//是否先进行核酸提取
	private String first;
	//单位
	private String dw;
	/** 图标*/
	private String picCode;
	/** 图标名称*/
	private String picCodeName;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 姓名
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 姓名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 标记
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 标记
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 说明
	 */
	@Column(name = "NOTE", length = 100)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 说明
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得int
	 * 
	 * @return: int 排序号
	 */
	@Column(name = "ORDER_NUMBER", length = 50)
	public int getOrderNumber() {
		return this.orderNumber;
	}

	/**
	 * 方法: 设置int
	 * 
	 * @param: int 排序号
	 */
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}


	/**
	 * @return the type
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设置
	 */
	@Column(name = "NEXT_FlOW", length = 100)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设置
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 10)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getDw() {
		return dw;
	}

	public void setDw(String dw) {
		this.dw = dw;
	}

	public String getPicCode() {
		return picCode;
	}

	public void setPicCode(String picCode) {
		this.picCode = picCode;
	}

	public String getPicCodeName() {
		return picCodeName;
	}

	public void setPicCodeName(String picCodeName) {
		this.picCodeName = picCodeName;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
}