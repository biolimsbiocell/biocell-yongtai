package com.biolims.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.goods.sample.model.SampleInfoSystem;
/**   
 * @Title: Model
 * @Description: 开箱检验体系明细
 * @author lims-platform
 * @date 2015-11-03 16:18:39
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_SYSTEM_ITEM")
@SuppressWarnings("serial")
public class SampleSystemItem extends EntityDao<SampleSystemItem> implements java.io.Serializable {
	/**序号*/
	private String id;
	
	private String name;
	
	private String num;
	
	private String note;
	
	private SampleReceive sampleReceive;
	/**取样打包明细样本来源*/
	private SampleInfoSystem sampleInfoSystem;
	
	/**
	 *方法: 取得String
	 *@return: String  序号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  序号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得SampleReceive
	 *@return: SampleReceive  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_RECEIVE")
	public SampleReceive getSampleReceive(){
		return this.sampleReceive;
	}
	/**
	 *方法: 设置SampleReceive
	 *@param: SampleReceive  相关主表
	 */
	public void setSampleReceive(SampleReceive sampleReceive){
		this.sampleReceive = sampleReceive;
	}
	
	@Column(name ="NAME", length = 50)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name ="NUM", length = 50)
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	
	@Column(name ="NOTE", length = 50)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO_SYSTEM")
	public SampleInfoSystem getSampleInfoSystem() {
		return sampleInfoSystem;
	}
	public void setSampleInfoSystem(SampleInfoSystem sampleInfoSystem) {
		this.sampleInfoSystem = sampleInfoSystem;
	}
	
	
	/**
	 * 方法：获取FlieNum
	 * @return String 附件数量
	 */
}