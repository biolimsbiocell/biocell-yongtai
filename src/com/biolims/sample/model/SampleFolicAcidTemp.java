package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
/**   
 * @Title: Model
 * @Description: 叶酸审核
 * @author lims-platform
 * @date 2015-11-03 16:18:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_FOLIC_ACID_TEMP")
@SuppressWarnings("serial")
public class SampleFolicAcidTemp extends EntityDao<SampleFolicAcidTemp> implements java.io.Serializable {
	/**信息录入id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**孕妇姓名*/
	private String patientName;
	/**姓名拼音*/
	private String patientNameSpell;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**地区*/
	private String area;
	/**送检医院*/
	private String hospital;
	/**送检日期*/
	private Date sendDate;
	/**样本类型*/
	private DicType sampleType;
	/**孕周*/
	private String gestationalAge;
	/**手机号码*/
	private String phoneNum;
	/**个人情况*/
	private String otherTumorHistory;
	/**检测项目id*/
	private String productId;
	/**检测项目name*/
	private String productName;
	/**确诊年龄（1）*/
	private String confirmAgeOne;
	/**家庭住址*/
	private String address;
	/**孕几次*/
	private String pregnancyTime;
	/**产几次*/
	private String parturitionTime;
	/**不良孕产史*/
	private String badMotherhood;
	/**异常结果描述*/
	private String reason;
	/**是否购买保险*/
	private String isInsure;
	/**是否需要发票*/
	private String isInvoice;
	/**是否收费*/
	private String isPay;
	/**开票单位*/
	private String paymentUnit;
	/**收据*/
	private String receipt;
	/**录入人*/
	private String createUser;
	/**金额*/
	private String money;
	//状态
	private String state;
	//状态名称
	private String stateName;
	/**报告者*/
	private User reportMan;
	/**审核者*/
	private User auditMan;
	/**下一步流向*/
	private String nextStepFlow;
	public String getNextStepFlow() {
		return nextStepFlow;
	}
	public void setNextStepFlow(String nextStepFlow) {
		this.nextStepFlow = nextStepFlow;
	}
	//相关主表
	private SampleInfo sampleInfo;
	
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getOtherTumorHistory() {
		return otherTumorHistory;
	}
	public void setOtherTumorHistory(String otherTumorHistory) {
		this.otherTumorHistory = otherTumorHistory;
	}
	public String getConfirmAgeOne() {
		return confirmAgeOne;
	}
	public void setConfirmAgeOne(String confirmAgeOne) {
		this.confirmAgeOne = confirmAgeOne;
	}
	public String getReceipt() {
		return receipt;
	}
	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	private String testItem;
	
	public String getTestItem() {
		return testItem;
	}
	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}
	
	public String getIsPay() {
		return isPay;
	}
	public void setIsPay(String isPay) {
		this.isPay = isPay;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTMAN")
	public User getReportMan() {
		return reportMan;
	}
	public void setReportMan(User reportMan) {
		this.reportMan = reportMan;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITMAN")
	public User getAuditMan() {
		return auditMan;
	}
	public void setAuditMan(User auditMan) {
		this.auditMan = auditMan;
	}
	/**

	 *方法: 取得String
	 *@return: String  信息录入id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  信息录入id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME", length = 100)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  孕妇姓名
	 */
	@Column(name ="PATIENT_NAME_SPELL", length = 100)
	public String getPatientNameSpell() {
		return patientNameSpell;
	}
	public void setPatientNameSpell(String patientNameSpell) {
		this.patientNameSpell = patientNameSpell;
	}
	
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 100)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 100)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  地区
	 */
	@Column(name ="AREA", length = 100)
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
	@Column(name ="HOSPITAL", length = 100)
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  送检日期
	 */
	@Column(name ="SEND_DATE", length = 100)
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	
	@Column(name ="GESTATIONAL_AGE", length = 100)
	public String getGestationalAge() {
		return gestationalAge;
	}
	public void setGestationalAge(String gestationalAge) {
		this.gestationalAge = gestationalAge;
	}


	@Column(name ="ADDRESS", length = 100)
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}


	@Column(name ="PREGNANCY_TIME", length = 100)

	public String getPregnancyTime() {
		return pregnancyTime;
	}
	public void setPregnancyTime(String pregnancyTime) {
		this.pregnancyTime = pregnancyTime;
	}

	public String getParturitionTime() {
		return parturitionTime;
	}
	public void setParturitionTime(String parturitionTime) {
		this.parturitionTime = parturitionTime;
	}
	@Column(name ="BAD_MOTHER_HOOD", length = 100)
	public String getBadMotherhood() {
		return badMotherhood;
	}
	public void setBadMotherhood(String badMotherhood) {
		this.badMotherhood = badMotherhood;
	}

	@Column(name ="REASON", length = 100)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Column(name ="IS_INSURE", length = 100)
	public String getIsInsure() {
		return isInsure;
	}
	public void setIsInsure(String isInsure) {
		this.isInsure = isInsure;
	}

	@Column(name ="IS_INVOICE", length = 100)
	public String getIsInvoice() {
		return isInvoice;
	}
	public void setIsInvoice(String isInvoice) {
		this.isInvoice = isInvoice;
	}
	@Column(name ="PAYMENT_UNIT", length = 100)
	public String getPaymentUnit() {
		return paymentUnit;
	}
	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}
	@Column(name ="MONEY", length = 100)
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CREATE_USER")
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
	@Column(name="PHONE_NUM",length=11)
	public String getPhoneNum() {
		return phoneNum;
	}
	/**
	 * 方法：设置PhoneNum
	 * @param phoneNum String 手机号码
	 */
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	/**
	 *方法: 取得
	 *@return:   相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	/**上传附件*/
	private FileInfo upLoadAccessory;
	/**
	 * 方法: 取得String
	 * @return String  上传附件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	/**
	 * 方法: 设置String
	 * @param  String  上传附件
	 */
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
}