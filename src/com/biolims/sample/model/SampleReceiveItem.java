package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmFamilyPatientShip;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.abnomal.model.AbnomalType;
import com.biolims.system.family.model.Family;
import com.biolims.system.work.model.UnitGroupNew;

/**
 * @Title: Model
 * @Description: 开箱检验明细
 * @author lims-platform
 * @date 2015-11-03 16:18:39
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_RECEIVE_ITEM")
@SuppressWarnings("serial")
public class SampleReceiveItem extends EntityDao<SampleReceiveItem> implements java.io.Serializable {
	/** 序号 */
	private String id;
	/** 名称 （分组号） */
	private String name;
	/** 操作时间 */
	private Date hangdleDate;
	/** 反馈时间 */
	private Date feedBackTime;
	/** 执行时间 */
	private Date runTime;
	/** 样本编号 */
	private String sampleCode;
	/** 订单编号 */
	private String orderCode;
	/** 核对 */
	private String checked;
	/** 样本录入情况 */
	private String condition;
	/** 是否正常 */
	private String isGood;
	/** 异常类型 */
	private AbnomalType unusual;
	/** 存放区域 */
	private String location;
	/** 处理方法 */
	private String method;
	/** 处理意见 */
	private String advice;
	/** 信息录入是否完整 */
	private String isFull;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private SampleReceive sampleReceive;
	/** 检测方法 */
	private String sequenceFun;
	/** 取样时间 */
	private String inspectDate;
	/** 接收时间 */
	private Date acceptDate;
	/** 患者姓名 */
	private String patientName;
	/** 患者姓名缩写 */
	private String abbreviation;
	/** 样本类型 */
	private DicSampleType dicSampleType;
	/** 采样日期 */
	private Date samplingDate;
	/** 采样部位 */
	private DicType dicType;
	/** 采样部位 */
	private String dicTypeName;
	// 单位
	private String unit;
	// 样本说明
	private String sampleNote;
	// 单位说明
	private String unitNote;

	// 实验室样本号（实际外部号）
	private String labCode;
	/** 家系 */
	private CrmFamilyPatientShip personShip;
	// 数据来源
	private String source;
	// 物种
	private String species;
	// 浓度
	private Double concentration;
	// 体积
	private Double volume;
	// 快递单号
	private String expressId;
	// 销售
	private User sellPerson;
	// 分析类型
	private String analysisType;
	/** 单位组 */
	private UnitGroupNew unitGroup;
	// 是否有染片
	private String isRp;
	/** 条形码 */
	private String barCode;
	/** 数据通量 */
	private String dataTraffic;
	/** 送检医院 */
	private CrmCustomer crmCustomer;
	/** 成本中心ID */
	private String scopeId;
	/** 成本中心 */
	private String scopeName;
	/** 样本来源 */
	private String type;
	/** EMR ID */
	private String EMRId;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 产品类型 1科研2临床 特定情况下使用 */
	private String sampleStyle;

	/** 家族编号 */
	private Family familyId;

	// 新加字段
	/** 血样颜色检验 0正常 1艳红 2暗红 */
	private String bloodColourTest;
	/** 血样颜色检验备注 */
	private String bloodColourNotes;
	/** 凝血检验 0是 1否 */
	private String coagulationTest;
	/** 凝血检验 备注 */
	private String coagulationNotes;
	/** 采血管密封检验 0是 1否 采血管是否发生渗漏 */
	private String sealingBloodVessels;
	/** 采血管密封检验 备注 */
	private String sealingBloodNotes;
	/** 采血管采血量检验 0是 1否 */
	private String bloodSamplingVolume;
	/** 采血管是否在有效期内0是 1否 */
	private String bloodVesselTime;
	/** 采血管有效期 */
	private Date bloodTime;
	/** 是否用沾有1：100杀狍子剂的无尘纸擦拭采血管表面 */
	private String stainedWith;
	/** 其他 */
	private String other;
	/** 其他情况说明 */
	private String otherInformationNotes;
	/** 批次号 */
	private String batch;
	/** 血量 */
	private String bloodVolume;
	/** 是否接收 */
	private String receiveState;
	
	

	public String getReceiveState() {
		return receiveState;
	}

	public void setReceiveState(String receiveState) {
		this.receiveState = receiveState;
	}

	public String getBloodVolume() {
		return bloodVolume;
	}

	public void setBloodVolume(String bloodVolume) {
		this.bloodVolume = bloodVolume;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getBloodTime() {
		return bloodTime;
	}

	public void setBloodTime(Date bloodTime) {
		this.bloodTime = bloodTime;
	}

	public String getBloodColourTest() {
		return bloodColourTest;
	}

	public void setBloodColourTest(String bloodColourTest) {
		this.bloodColourTest = bloodColourTest;
	}

	public String getBloodColourNotes() {
		return bloodColourNotes;
	}

	public void setBloodColourNotes(String bloodColourNotes) {
		this.bloodColourNotes = bloodColourNotes;
	}

	public String getCoagulationTest() {
		return coagulationTest;
	}

	public void setCoagulationTest(String coagulationTest) {
		this.coagulationTest = coagulationTest;
	}

	public String getCoagulationNotes() {
		return coagulationNotes;
	}

	public void setCoagulationNotes(String coagulationNotes) {
		this.coagulationNotes = coagulationNotes;
	}

	public String getSealingBloodVessels() {
		return sealingBloodVessels;
	}

	public void setSealingBloodVessels(String sealingBloodVessels) {
		this.sealingBloodVessels = sealingBloodVessels;
	}

	public String getSealingBloodNotes() {
		return sealingBloodNotes;
	}

	public void setSealingBloodNotes(String sealingBloodNotes) {
		this.sealingBloodNotes = sealingBloodNotes;
	}

	public String getBloodSamplingVolume() {
		return bloodSamplingVolume;
	}

	public void setBloodSamplingVolume(String bloodSamplingVolume) {
		this.bloodSamplingVolume = bloodSamplingVolume;
	}

	public String getBloodVesselTime() {
		return bloodVesselTime;
	}

	public void setBloodVesselTime(String bloodVesselTime) {
		this.bloodVesselTime = bloodVesselTime;
	}

	public String getStainedWith() {
		return stainedWith;
	}

	public void setStainedWith(String stainedWith) {
		this.stainedWith = stainedWith;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getOtherInformationNotes() {
		return otherInformationNotes;
	}

	public void setOtherInformationNotes(String otherInformationNotes) {
		this.otherInformationNotes = otherInformationNotes;
	}

	/**
	 * @return the familyId
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public Family getFamilyId() {
		return familyId;
	}

	public void setFamilyId(Family familyId) {
		this.familyId = familyId;
	}

	public String getSampleStyle() {
		return sampleStyle;
	}

	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	/** 性别 */
	private String gender;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getIsRp() {
		return isRp;
	}

	public void setIsRp(String isRp) {
		this.isRp = isRp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public UnitGroupNew getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(UnitGroupNew unitGroup) {
		this.unitGroup = unitGroup;
	}

	public String getAnalysisType() {
		return analysisType;
	}

	public void setAnalysisType(String analysisType) {
		this.analysisType = analysisType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	public User getSellPerson() {
		return sellPerson;
	}

	public void setSellPerson(User sellPerson) {
		this.sellPerson = sellPerson;
	}

	public String getExpressId() {
		return expressId;
	}

	public void setExpressId(String expressId) {
		this.expressId = expressId;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PERSON_TYPE")
	public CrmFamilyPatientShip getPersonShip() {
		return personShip;
	}

	public void setPersonShip(CrmFamilyPatientShip personShip) {
		this.personShip = personShip;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public String getUnitNote() {
		return unitNote;
	}

	public void setUnitNote(String unitNote) {
		this.unitNote = unitNote;
	}

	public String getDicTypeName() {
		return dicTypeName;
	}

	public void setDicTypeName(String dicTypeName) {
		this.dicTypeName = dicTypeName;
	}

	public String getSampleNote() {
		return sampleNote;
	}

	public void setSampleNote(String sampleNote) {
		this.sampleNote = sampleNote;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Date getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE")
	public DicType getDicType() {
		return dicType;
	}

	public void setDicType(DicType dicType) {
		this.dicType = dicType;
	}

	private String sampleType;

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/** 储位 */
	private String binLocation;
	/** 附件数量 */
	private String fileNum;
	// 样本状态
	private String state;
	// 样本状态
	private String stateName;
	// 任务单
	private String orderId;
	// 检测项目ID
	private String productId;
	// 检测项目
	private String productName;
	// 手机号
	private String phone;
	// 身份证号(外部样本编号)
	private String idCard;
	// reportDate应出报告日期
	private Date reportDate;

	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	/** 下一步流向 */
	private String nextFlow;
	/** 下一步流向Id */
	private String nextFlowId;
	// 样本数量
	private String sampleNum;
	// 样本量
	private Double sampleSize;

	public Double getSampleSize() {
		return sampleSize;
	}

	public void setSampleSize(Double sampleSize) {
		this.sampleSize = sampleSize;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	@Column(name = "NEXT_FLOW", length = 20)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 序号
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	// /**
	// *方法: 取得String
	// *@return: String 样本编号
	// */
	// @Column(name ="CODE", length = 50)
	// public String getCode(){
	// return this.code;
	// }
	// /**
	// *方法: 设置String
	// *@param: String 样本编号
	// */
	// public void setCode(String code){
	// this.code = code;
	// }
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对
	 */
	@Column(name = "CHECKED", length = 50)
	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本录入情况
	 */
	@Column(name = "CONDITION_", length = 50)
	public String getCondition() {
		return this.condition;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             样本录入情况
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否正常
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             是否正常
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 异常类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNUSUAL")
	public AbnomalType getUnusual() {
		return unusual;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             异常类型
	 */
	public void setUnusual(AbnomalType unusual) {
		this.unusual = unusual;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 存放区域
	 */
	@Column(name = "LOCATION", length = 50)
	public String getLocation() {
		return this.location;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             存放区域
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理方法
	 */
	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return this.method;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             处理方法
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理意见
	 */
	@Column(name = "ADVICE", length = 200)
	public String getAdvice() {
		return this.advice;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             处理意见
	 */
	public void setAdvice(String advice) {
		this.advice = advice;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 信息录入是否完整
	 */
	@Column(name = "IS_FULL", length = 200)
	public String getIsFull() {
		return this.isFull;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             信息录入是否完整
	 */
	public void setIsFull(String isFull) {
		this.isFull = isFull;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得SampleReceive
	 * 
	 * @return: SampleReceive 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_RECEIVE")
	public SampleReceive getSampleReceive() {
		return this.sampleReceive;
	}

	/**
	 * 方法: 设置SampleReceive
	 * 
	 * @param: SampleReceive
	 *             相关主表
	 */
	public void setSampleReceive(SampleReceive sampleReceive) {
		this.sampleReceive = sampleReceive;
	}

	/**
	 * 方法：获取BusinessType String
	 * 
	 * @return BusinessType 检测方法
	 */
	@Column(name = "SEQUENCE_FUN", length = 50)
	public String getSequenceFun() {
		return sequenceFun;
	}

	/**
	 * 方法：设置BusinessType
	 * 
	 * @param businessType
	 *            String 检测方法
	 */
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	/**
	 * 方法：获取SickerName
	 * 
	 * @return String 患者姓名
	 */
	@Column(name = "PATIENT_NAME", length = 50)
	public String getPatientName() {
		return patientName;
	}

	/**
	 * 方法：设置SickerName
	 * 
	 * @param sickerName
	 *            String 患者姓名
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * 方法: 取得DicSampleType
	 * 
	 * @return: DicSampleType 样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	/**
	 * 方法: 设置DicSampleType
	 * 
	 * @param: DicSampleType
	 *             样本类型
	 */
	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	/**
	 * 方法：获取BinLocation
	 * 
	 * @return String 储位
	 */
	@Column(name = "BIN_LOCATION", length = 50)
	public String getBinLocation() {
		return binLocation;
	}

	/**
	 * 方法：设置BinLocation
	 * 
	 * @param binLocation
	 *            String 储位
	 */
	public void setBinLocation(String binLocation) {
		this.binLocation = binLocation;
	}

	/**
	 * 方法：获取FlieNum
	 * 
	 * @return String 附件数量
	 */
	@Column(name = "FILE_NUM", length = 50)
	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public Date getHangdleDate() {
		return hangdleDate;
	}

	public void setHangdleDate(Date hangdleDate) {
		this.hangdleDate = hangdleDate;
	}

	public Date getFeedBackTime() {
		return feedBackTime;
	}

	public void setFeedBackTime(Date feedBackTime) {
		this.feedBackTime = feedBackTime;
	}

	public Date getRunTime() {
		return runTime;
	}

	public void setRunTime(Date runTime) {
		this.runTime = runTime;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	/**
	 * 样本名称
	 * 
	 * @return
	 */
	@Column(name = "NAME", length = 10)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * @return dataTraffic
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic
	 *            the dataTraffic to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId
	 *            the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName
	 *            the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * @return the eMRId
	 */
	public String getEMRId() {
		return EMRId;
	}

	/**
	 * @param eMRId
	 *            the eMRId to set
	 */
	public void setEMRId(String eMRId) {
		EMRId = eMRId;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

}