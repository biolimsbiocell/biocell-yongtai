package com.biolims.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.goods.sample.model.SampleInfoMain;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 开箱检验
 * @author lims-platform
 * @date 2015-11-03 16:18:52
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_RECEIVE")
@SuppressWarnings("serial")
public class SampleReceive extends EntityDao<SampleReceive> implements java.io.Serializable {
	/** 开箱检验id */
	private String id;
	/** 编号 */
	private String code;
	/** 描述 */
	private String name;
	/** 检测方法 */
	private WorkType businessType;
	/** 快递单号 */
	private String expreceCode;
	/** 接收人 */
	private User acceptUser;
	/** 接收日期 */
	private Date acceptDate;
	/** 工作流编号 */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 取样打包 */
	private SampleInfoMain sampleInfoMain;
	/** 科研项目 */
	private Project project;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private String classify;
	// 审核人
	private User confirmUser;
	/** 完成时间 */
	private Date confirmDate;
	
	/** 创建日期 */
	private Date createDate;
	// 类型
	private String type;
	// 客户
	private CrmCustomer crmCustomer;
	// 医生.
	private CrmDoctor crmDoctor;
	// 扫描快递单号
	private String expressNum;
	/** 成本中心Id */
	private String scopeId;
	/** 成本中心 */
	private String scopeName;
	/** 实验组 */
	private UserGroup acceptUserGroup;
	// 快递公司
	private ExpressCompany expressCompany;
	// 样品运输方式
	private DicType ysfs;
	/** 实验组 */
	private UserGroup userGroup;
	/** QA审核人 */
	private User approvalUser;
	/** QC审核人*/
	private User qualityCheckUser;
	/** 生产审核人 */
	private User productionUser;
	/** 订单编号 */
	private String sampleOrder;
	/** 运输设备编号 */
	private String equimentNumber;
	/**运送设备温度*/
	private String temperature;
	/**订单信息核实*/
	private String verify;
	/**快递员*/
	private String courier;
	/**温度记录仪编号*/
	private String recorder;
	/** 预计回输结束日期 */
	private Date feedBackTime;
	
	/** 预计回输开始日期 */
	private Date startBackTime;
	
	/**运输温度是否合格*/
	private String qualified;
	/**CCOI*/
	private String ccoi;
	/**产品批号*/
	private String barcode;
	
	//温度计开始/结束时间
	private Date thermometerTime;
	private Date thermometerTime2;
	
	//最高温度,最低温度,平均温度
	private String highTemperature;
	private String lowTemperature;
	private String avgTemperature;
	
	
	
	
	
//	private SampleOrder sampleOrder1;
//	public Date getFeedBackTime() {
//		return feedBackTime;
//	}
//
//	public void setFeedBackTime(Date feedBackTime) {
//		this.feedBackTime = feedBackTime;
//	}

	public String getHighTemperature() {
		return highTemperature;
	}

	public void setHighTemperature(String highTemperature) {
		this.highTemperature = highTemperature;
	}

	public String getLowTemperature() {
		return lowTemperature;
	}

	public void setLowTemperature(String lowTemperature) {
		this.lowTemperature = lowTemperature;
	}

	public String getAvgTemperature() {
		return avgTemperature;
	}

	public void setAvgTemperature(String avgTemperature) {
		this.avgTemperature = avgTemperature;
	}

	public Date getThermometerTime() {
		return thermometerTime;
	}

	public void setThermometerTime(Date thermometerTime) {
		this.thermometerTime = thermometerTime;
	}

	public Date getThermometerTime2() {
		return thermometerTime2;
	}

	public void setThermometerTime2(Date thermometerTime2) {
		this.thermometerTime2 = thermometerTime2;
	}

	public String getCcoi() {
		return ccoi;
	}

	public void setCcoi(String ccoi) {
		this.ccoi = ccoi;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getQualified() {
		return qualified;
	}

	public void setQualified(String qualified) {
		this.qualified = qualified;
	}

	public String getRecorder() {
		return recorder;
	}

	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getVerify() {
		return verify;
	}

	public void setVerify(String verify) {
		this.verify = verify;
	}

	public String getCourier() {
		return courier;
	}

	public void setCourier(String courier) {
		this.courier = courier;
	}

	@Column(name = "EQUIMENT_NUM", length = 50)
	public String getEquimentNumber() {
		return equimentNumber;
	}

	public void setEquimentNumber(String equimentNumber) {
		this.equimentNumber = equimentNumber;
	}

	public String getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(String sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getApprovalUser() {
		return approvalUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getProductionUser() {
		return productionUser;
	}

	public void setApprovalUser(User approvalUser) {
		this.approvalUser = approvalUser;
	}

	public void setProductionUser(User productionUser) {
		this.productionUser = productionUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_GROUP")
	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	/**
	 * 方法: 取得
	 * 
	 * @return: ysfs
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "YSFS")
	public DicType getYsfs() {
		return ysfs;
	}

	public void setYsfs(DicType ysfs) {
		this.ysfs = ysfs;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPRESS_COMPANY")
	public ExpressCompany getExpressCompany() {
		return expressCompany;
	}

	public void setExpressCompany(ExpressCompany expressCompany) {
		this.expressCompany = expressCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUserGroup() {
		return acceptUserGroup;
	}

	public void setAcceptUserGroup(UserGroup acceptUserGroup) {
		this.acceptUserGroup = acceptUserGroup;
	}

	public String getExpressNum() {
		return expressNum;
	}

	public void setExpressNum(String expressNum) {
		this.expressNum = expressNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 开箱检验id
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             开箱检验id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 检测方法
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSINESS_TYPE")
	public WorkType getBusinessType() {
		return businessType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             检测方法
	 */
	public void setBusinessType(WorkType businessType) {
		this.businessType = businessType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 快递单号
	 */
	@Column(name = "EXPRECE_CODE", length = 100)
	public String getExpreceCode() {
		return this.expreceCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             快递单号
	 */
	public void setExpreceCode(String expreceCode) {
		this.expreceCode = expreceCode;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 接收人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             接收人
	 */
	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 接收日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             接收日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流ID
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             工作流ID
	 */
	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO_MAIN")
	public SampleInfoMain getSampleInfoMain() {
		return sampleInfoMain;
	}

	public void setSampleInfoMain(SampleInfoMain sampleInfoMain) {
		this.sampleInfoMain = sampleInfoMain;
	}

	// 新增字段
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流ID
	 */
	@Column(name = "CLASSIFY", length = 50)
	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId
	 *            the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName
	 *            the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public Date getStartBackTime() {
		return startBackTime;
	}

	public void setStartBackTime(Date startBackTime) {
		this.startBackTime = startBackTime;
	}

	public Date getFeedBackTime() {
		return feedBackTime;
	}

	public void setFeedBackTime(Date feedBackTime) {
		this.feedBackTime = feedBackTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getQualityCheckUser() {
		return qualityCheckUser;
	}

	public void setQualityCheckUser(User qualityCheckUser) {
		this.qualityCheckUser = qualityCheckUser;
	}

	

}
