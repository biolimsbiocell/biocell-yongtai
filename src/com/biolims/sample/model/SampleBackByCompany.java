package com.biolims.sample.model;

import java.util.Date;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.GeneratedValue;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 物流跟踪
 * @author lims-platform
 * @date 2015-11-03 16:21:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_BACK_BY_COMPANY")
@SuppressWarnings("serial")
public class SampleBackByCompany extends EntityDao<SampleBackByCompany> implements java.io.Serializable {
	/**编码*/
	private String id;
	//样本编号
	private String sampleCode;
	//样本编号
	private SampleInfo sampleInfo;
	/*患者姓名*/
	private String patientName;
	/*检测项目*/
	private String productId;
	/*检测项目*/
	private String productName;
	/*取样日期*/
	private String inspectDate;
	/*接收日期*/
	private String acceptDate;
	/*身份证*/
	private String idCard;
	/*手机号*/
	private String phone;
	/*关联任务单*/
	private String orderId;
	/*检测方法*/
	private String sequenceFun;
	/*应出报告日期*/
	private String reportDate;
	/**返回时间*/
	private Date returnDate;
	/*状态*/
	private String state;
	/**处理意见*/
	private String method;
	/**备注*/
	private String note;

	//区分临床还是科技服务      0   临床   1   科技服务
	private String classify;
	
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	
	public String getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(String acceptDate) {
		this.acceptDate = acceptDate;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSequenceFun() {
		return sequenceFun;
	}
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
	
}