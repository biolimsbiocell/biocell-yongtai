package com.biolims.sample.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/sampleSearchList")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleSearchListAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "901515901";
	@Autowired
	private SampleSearchService sampleSearchService;
	
	@Resource
	private CommonService commonService;
	@Resource
	private CommonDAO commonDAO;


	@Action(value = "showSampleSearch")
	public String showSampleSearch() throws Exception {
		String style = getParameterFromRequest("style");
		putObjToContext("style", style);
		String time = getParameterFromRequest("time");
		putObjToContext("time", time);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/search/sampleMain.jsp");
	}
	
	@Action(value = "showSampleSearchEdit")
	public String showSampleSearchEdit() throws Exception {
		
		
		String searchnum = getParameterFromRequest("searchnum");
		putObjToContext("searchnum", searchnum);
//		sampleSearchService.insertLog(searchnum,"");
		
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/search/sampleSearch.jsp");
	}
	

	@Action(value = "showSampleSearchJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleSearchJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleSearchService.showSampleOrdersJson(start, length, query, col, sort);
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("batchStateName", "");
		map.put("barcode", "");
		map.put("name", "");
		map.put("round", "");
		map.put("filtrateCode", "");
		map.put("randomCode", "");
		map.put("ccoi", "");
		map.put("medicalNumber", "");
		map.put("productName", "");
		map.put("crmCustomer-name", "");
		
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}


}
