﻿
package com.biolims.sample.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleRepeatService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/sample/repeat")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleRepeatAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2111";
	@Autowired
	private SampleRepeatService sampleRepeatService;
	

	@Resource
	private FileInfoService fileInfoService;
	

	@Action(value = "showSampleRepeatPlasmaList")
	public String showSampleRepeatPlasmaList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleRepeatPlasma.jsp");
	}
	

	@Action(value = "showSampleRepeatPlasmaListJson")
	public void showSampleRepeatPlasmaListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleRepeatService.findRepeatPlasmaList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		//List<SamplePlasmaInfo> list = (List<SamplePlasmaInfo>) result.get("list");
		List<PlasmaAbnormal> list = (List<PlasmaAbnormal>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("nextFlow", "");
		map.put("method", "");
		map.put("state", "");
		map.put("note", "");
		map.put("result", "");
		
		map.put("productId", "");
		map.put("productName", "");
		map.put("submit", "");
		
		map.put("patient", "");
		map.put("idCard", "");
		map.put("sequencingFun", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		
		map.put("reportDate", "");
		map.put("orderId", "");
		map.put("phone", "");
		map.put("classify", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	
	
	//保存重抽血
	@Action(value = "savePlasmaRepeat")
	public void savePlasmaRepeat() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			sampleRepeatService.savePlasmaRepeat(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
//	//根据条件检索重抽血
//	@Action(value = "selectPlasmaRepeat")
//	public void selectPlasmaRepeat() throws Exception {
//		String code1 = getParameterFromRequest("sampleCode");
//		String code2 = getParameterFromRequest("bloodCode");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.sampleRepeatService.selectBloodRepeat(code1, code2);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	public String getRightsId() {
		return rightsId;
	}


	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}


	public SampleRepeatService getSampleRepeatService() {
		return sampleRepeatService;
	}


	public void setSampleRepeatService(SampleRepeatService sampleRepeatService) {
		this.sampleRepeatService = sampleRepeatService;
	}
	
	
	
}
