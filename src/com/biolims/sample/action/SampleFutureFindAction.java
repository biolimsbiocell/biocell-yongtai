package com.biolims.sample.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

import net.sf.json.JSONObject;
import net.sourceforge.jeval.function.math.Round;

@Namespace("/sample/sampleFutureFind")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleFutureFindAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";

	@Resource
	private CommonService commonService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleReceiveService sampleReceiveService;

	@Action(value = "showSampleFutureFind")
	public String showSampleSearch() throws Exception {
		rightsId = "9015159001";

//		String searchnum = getParameterFromRequest("searchnum");
//		putObjToContext("searchnum", searchnum);

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/futureFind/showSampleFutureFind.jsp");
	}

	@Action(value = "showSampleFutureFindList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleFutureFindList() throws Exception {
		rightsId = "9015159002";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/futureFind/sampleFutureFindList.jsp");
	}

	@Action(value = "showSampleFutureFindListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleFutureFindListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleReceiveService.showSampleFutureFindTableJson(start, length, query, col,
					sort);
			List<SampleFutureFind> list = (List<SampleFutureFind>) result.get("list");
//			List<Object[]> list2 = sampleReceiveService.findIDAndStates();
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("filtrateCode", "");
			map.put("sampleTime", "");
			map.put("round", "");
			map.put("happenEvent", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("dingDanCode", "");
			map.put("sampleState", "");
			
			map.put("timeState", "");
			map.put("timeShowState", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 预排表生成初始数据1-20轮 Exception
	 */
	@Action(value = "checkU")
	public synchronized void checkU() throws Exception {
		rightsId = "9015159002";
		String sxh = getParameterFromRequest("sxh");
		String cxsj = getParameterFromRequest("cxsj");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<SampleFutureFind> sffs = commonService.get(SampleFutureFind.class, "filtrateCode", sxh);
			if (sffs.size() > 0) {
				map.put("cz", true);
			} else {
				sampleReceiveService.checkU(sxh, cxsj);
				map.put("cz", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 保存修改
	 */
	@Action(value = "saveSampleFutureFind")
	public void saveSampleFutureFind() throws Exception {
		String datas = getParameterFromRequest("datas");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sampleReceiveService.saveSampleFutureFind(datas);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 按照时间查询预排表 Exception
	 */
	@Action(value = "chaxunsj")
	public void chaxunsj() throws Exception {
		rightsId = "9015159001";
		String kssj = getParameterFromRequest("kssj");
		String jssj = getParameterFromRequest("jssj");
		String filtrateCode = getParameterFromRequest("filtrateCode");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<SampleFutureFind> sffs = sampleReceiveService.chaxunsj(kssj, jssj);
			map.put("sj", sffs);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 预排表查询
	@Action(value = "showSampleFutureFindsjListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleFutureFindsjListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String sjbb1 = getParameterFromRequest("sjbb");
		String[] sjbb = sjbb1.split(",");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			List<String> list1 = sampleReceiveService.chaxunsjd(sjbb);

			List<Map<String, String>> list = new ArrayList<Map<String, String>>();

			for (String sff : list1) {
				Map<String, String> a = new HashMap<String, String>();
//				a.put("id", sff.getFiltrateCode());
				a.put("id", sff);
				List<Object[]> sffs = sampleReceiveService.chaxunsjAndid(sjbb, sff);// sff.getFiltrateCode());
				for (String zzz : sjbb) {
//					List<Object[]> sffs = sampleReceiveService.chaxunsjid(zzz, sff);//sff.getFiltrateCode());
					if (sffs.size() > 0) {
						String ms = "";
						for (Object[] obj : sffs) {
							if (zzz.equals(obj[2])) {
//								System.out.println(obj[0]+" "+obj[1]);
//								JSONObject jaC = net.sf.json.JSONObject.fromObject(sf);
								String happenEvent = (String) obj[0];// (String) jaC.get("happenEvent");
								String docTitle = (String) obj[1];// (String) jaC.get("round");
								String dingdanCode = (String) obj[3];
								String timeState = (String) obj[5];
								String timeShowState = (String) obj[6];
								
								//是不是实际采血
								if ("1".equals(happenEvent)) {
									//预采血时间判断   0黑色1红色2黄色
									if("0".equals(timeState)) {
										//能否点击
										if("0".equals(timeShowState)) {
											if ("".equals(ms)) {
												ms = ms + "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer'><a style='color:blue'><br />第" + docTitle
														+ "次采血</a></span>";
											} else {
												ms = ms + "<br />";
												ms = ms + "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer'><a style='color:blue'><br />第" + docTitle
														+ "次采血</a></span>";
											}
										}else {
											if ("".equals(ms)) {
												ms = ms + "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:blue'><br />第" + docTitle
														+ "次采血</a></span>";
											} else {
												ms = ms + "<br />";
												ms = ms + "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:blue'><br />第" + docTitle
														+ "次采血</a></span>";
											}
										}
									}else if("1".equals(timeState)){
										//能否点击
										if("0".equals(timeShowState)) {
											if ("".equals(ms)) {
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer'><a style='color:red'><br />第" + docTitle
														+ "次采血</a></span>";
											} else {
												ms = ms + "<br />";
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer'><a style='color:red'><br />第" + docTitle
														+ "次采血</a></span>";
											}
										}else {
											if ("".equals(ms)) {
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:red'><br />第" + docTitle
														+ "次采血</a></span>";
											} else {
												ms = ms + "<br />";
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:red'><br />第" + docTitle
														+ "次采血</a></span>";
											}
										}
									}else if("2".equals(timeState)){
										//能否点击
										if("0".equals(timeShowState)) {
											if ("".equals(ms)) {
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer'><a style='color:yellow'><br />第" + docTitle
														+ "次采血</a></span>";
											} else {
												ms = ms + "<br />";
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer'><a style='color:yellow'><br />第" + docTitle
														+ "次采血</a></span>";
											}
										}else {
											if ("".equals(ms)) {
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:yellow'><br />第" + docTitle
														+ "次采血</a></span>";
											} else {
												ms = ms + "<br />";
												ms = ms +""+dingdanCode+"；"
														+ "<span class='"+dingdanCode+"' id='" + docTitle
														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:yellow'><br />第" + docTitle
														+ "次采血</a></span>";
											}
										}
									}
								}else {
									if ("".equals(ms)) {
										ms = ms + "第" + docTitle + "次回输";
									} else {
										ms = ms + "；<br />第" + docTitle + "次回输";
									}
								}
								
								
								
//								if(dingdanCode!=null) {// (String) jaC.get("dingdanCode");
//									if ("1".equals(happenEvent)) {
//										if ("".equals(ms)) {
//											if(docTitle.equals("1")) {
//												ms = ms +""+dingdanCode+"；"
//														+ "<span class='"+dingdanCode+"' id='" + docTitle
//														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:red'><br />第" + docTitle
//														+ "次采血</a></span>";
//											}else {
//												ms = ms +""+dingdanCode+"；"
//														+ "<span class='"+dingdanCode+"' id='" + docTitle
//														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a><br />第" + docTitle
//														+ "次采血</a></span>";
//											}
//											
//										} else {
//											ms = ms + "<br />第" + docTitle + "次采血";
//										}
//									} else if ("2".equals(happenEvent)) {
//										if ("".equals(ms)) {
//											ms = ms + "<br />第" + docTitle + "次回输";
//										} else {
//											ms = ms + "；<br />第" + docTitle + "次回输";
//										}
//									}
//								}else {
//									if ("1".equals(happenEvent)) {
//										if("1".equals(timeState)) {
//											if ("".equals(ms)) {
//												ms = ms + "<span id='" + docTitle
//														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:red'>第" + docTitle
//														+ "次采血</a></span>";
//											} else {
//												ms = ms + "；<br />第" + docTitle + "次采血";
//											}
//										}else if("2".equals(timeState)) {
//											if ("".equals(ms)) {
//												ms = ms + "<span id='" + docTitle
//														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a style='color:yellow '>第" + docTitle
//														+ "次采血</a></span>";
//											} else {
//												ms = ms + "；<br />第" + docTitle + "次采血";
//											}
//										}else {
//											if ("".equals(ms)) {
//												ms = ms + "<span id='" + docTitle
//														+ "' style='cursor: pointer' onclick=hrefFunc(this)><a>第" + docTitle
//														+ "次采血</a></span>";
//											} else {
//												ms = ms + "；<br />第" + docTitle + "次采血";
//											}
//										}
//									} else if ("2".equals(happenEvent)) {
//										if ("".equals(ms)) {
//											ms = ms + "<br />第" + docTitle + "次回输";
//										} else {
//											ms = ms + "；<br />第" + docTitle + "次回输";
//										}
//									}
//								}
								
//								if("1".equals(sf.getHappenEvent())) {
//									if("".equals(ms)) {
//										ms = ms +"第"+sf.getRound()+"次采血";
//									}else {
//										ms = ms +"；第"+sf.getRound()+"次采血";
//									}
//								}else if("2".equals(sf.getHappenEvent())) {
//									if("".equals(ms)) {
//										ms = ms +"第"+sf.getRound()+"次回输";
//									}else {
//										ms = ms +"；第"+sf.getRound()+"次回输";
//									}
//								}
							}
							a.put("sampleState",(String)obj[4]);
						}
						a.put(zzz, ms);
					} else {
						a.put(zzz, "");
					}
				}
				list.add(a);
			}

			result.put("recordsTotal", list.size());
			result.put("recordsFiltered", 0);
			result.put("list", list);

//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			for(String zz:sjbb) {
//				map.put(zz, "");
//			}
//			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, JsonUtils.toJsonString(list)));
//			HttpUtils.write(JsonUtils.toJsonString(list));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 预排表根据筛选号查询是否有订单
	 * 
	 * @throws Exception
	 */
	@Action(value = "findSampleOrderId")
	public void findSampleOrderId() throws Exception {
		String filtrateCode = getParameterFromRequest("filtrateCode");
		String round = getParameterFromRequest("round");
		String ro = "";
		if (round.length() == 1) {
			ro = "0" + round;
		} else {
			ro = round;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 1.查询当前筛选号的当前轮次绑没绑定订单
			SampleFutureFind s = sampleReceiveService.findFiltrateCodeOfRound(filtrateCode, round);
			if (s.getDingDanCode() != null && !"".equals(s.getDingDanCode())) {
				// 2.有,进订单
				map.put("id", s.getDingDanCode());
				map.put("round", ro);
				map.put("filtrateCode", filtrateCode);
				map.put("success", true);
			} else {
				// 3.没有绑定订单,根据筛选号查询订单表,有没有其他订单
				List<SampleOrder> list = sampleReceiveService.findSampleOrderId(filtrateCode);
				if (!list.isEmpty()) {
					int rounds = Integer.parseInt(round);
					int roundss = rounds - 1;
					String roundsss = String.valueOf(roundss);
					String roundssss = "";
					if (roundsss.length() == 1) {
						roundssss = "0" + roundsss;
					} else {
						roundssss = roundsss;
					}
					// 4.有,查询当前轮次的上一轮次有没有绑订单
					SampleOrder soo = sampleReceiveService.findRound(roundssss,filtrateCode);
					if (soo != null) {
						// 4-1.有绑订单 将上一轮次的订单数据Copy到当前轮次(订单号为NEW,轮次,筛选号为当前轮次的)
						map.put("id", "NEW");
						map.put("idCode", soo.getId());
						map.put("round", ro);
						map.put("filtrateCode", filtrateCode);
						map.put("success", true);
					} else {
						// 4-2.没有 不建单子并提示
						map.put("success", false);
					}

				} else {
					// 5.没有其他订单 ,新建单子,只要筛选号,轮次,订单号为NEW
//					if(round.equals("1")) {
						map.put("id", "NEW");
						map.put("round", ro);
						map.put("filtrateCode", filtrateCode);
						map.put("success", true);
//					}else {
//						map.put("success", false);
//					}
						
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
