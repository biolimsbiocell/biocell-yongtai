﻿
package com.biolims.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleBackByCompany;
import com.biolims.sample.service.SampleBackByCompanyService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/sample/sampleBackByCompany")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleBackByCompanyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "21010";
	@Autowired
	private SampleBackByCompanyService sampleBackByCompanyService;
	private SampleBackByCompany sampleBackByCompany = new SampleBackByCompany();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showSampleBackByCompanyList")
	public String showSampleBackByCompanyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleBackByCompany.jsp");
	}

	@Action(value = "showSampleBackByCompanyListJson")
	public void showSampleBackByCompanyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleBackByCompanyService.findSampleBackByCompany(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleBackByCompany> list = (List<SampleBackByCompany>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("reportDate", "");
		map.put("returnDate", "yyyy-MM-dd");
		
		map.put("sampleInfo-id", "");
		map.put("method", "");
		map.put("state", "");
		map.put("note", "");
		map.put("classify", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleBackByCompanyService getSampleBackByCompanyService() {
		return sampleBackByCompanyService;
	}

	public void setSampleBackByCompanyService(
			SampleBackByCompanyService sampleBackByCompanyService) {
		this.sampleBackByCompanyService = sampleBackByCompanyService;
	}

	public SampleBackByCompany getSampleBackByCompany() {
		return sampleBackByCompany;
	}

	public void setSampleBackByCompany(SampleBackByCompany sampleBackByCompany) {
		this.sampleBackByCompany = sampleBackByCompany;
	}
	/**
	 * 保存公司反馈列表
	 * @throws Exception
	 */
	@Action(value = "saveSampleBackByCompany")
	public void saveSampleBackByCompany() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			sampleBackByCompanyService.saveSampleBackByCompany(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
