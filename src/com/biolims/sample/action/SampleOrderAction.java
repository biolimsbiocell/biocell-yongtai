package com.biolims.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/sampleOrder")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleOrderAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2113";
	@Autowired
	private SampleOrderService sampleOrderService;
	private SampleOrder sampleOrder = new SampleOrder();
	@Resource
	private CodingRuleService codingRuleService;
	// 注入癌症信息的service
	@Autowired
	private SampleCancerTempService sampleCancerTempService;
	private SampleCancerTemp sampleCancerTempOne = new SampleCancerTemp();// 一录
	private SampleCancerTemp sampleCancerTempTwo = new SampleCancerTemp();// 二录
	// 注入电子病历的service
	@Autowired
	private CrmPatientService crmPatientService;

	@Resource
	private FileInfoService fileInfoService;

//	@Action(value = "showSampleOrderList")
//	public String showSampleOrderList() throws Exception {
//
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/sample/sampleOrder.jsp");
//	}
//
//	@Action(value = "showSampleOrderListJson")
//	public void showSampleOrderListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = sampleOrderService.findSampleOrderList(
//				map2Query, startNum, limitNum, dir, sort);
//		Long count = (Long) result.get("total");
//		List<SampleOrder> list = (List<SampleOrder>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("gender", "");
//		map.put("idcard", "");
//		map.put("birthDate", "yyyy-MM-dd");
//		map.put("diagnosisDate", "");
//		map.put("dicType-id", "");
//		map.put("dicType-name", "");
//		map.put("sampleStage", "");
//		map.put("inspectionDepartment-id", "");
//		map.put("inspectionDepartment-name", "");
//		map.put("crmProduct-id", "");
//		map.put("crmProduct-name", "");
//		map.put("samplingDate", "yyyy-MM-dd");
//		map.put("samplingLocation-id", "");
//		map.put("samplingLocation-name", "");
//		map.put("samplingNumber", "");
//		map.put("pathologyConfirmed", "");
//		map.put("bloodSampleDate", "yyyy-MM-dd");
//		map.put("plasmapheresisDate", "yyyy-MM-dd");
//		map.put("commissioner-id", "");
//		map.put("commissioner-name", "");
//		map.put("receivedDate", "yyyy-MM-dd");
//		map.put("sampleTypeId", "");
//		map.put("sampleTypeName", "");
//		map.put("sampleCode", "");
//		map.put("medicalNumber", "");
//		map.put("family", "");
//		map.put("familyPhone", "");
//		map.put("familySite", "");
//		map.put("medicalInstitutions", "");
//		map.put("medicalInstitutionsPhone", "");
//		map.put("medicalInstitutionsSite", "");
//		map.put("attendingDoctor", "");
//		map.put("attendingDoctorPhone", "");
//		map.put("attendingDoctorSite", "");
//		map.put("note", "");
//		map.put("createUser-id", "");
//		map.put("createUser-name", "");
//		map.put("createDate", "yyyy-MM-dd");
//		map.put("confirmUser-id", "");
//		map.put("confirmUser-name", "");
//		map.put("confirmDate", "yyyy-MM-dd");
//		map.put("state", "");
//		map.put("stateName", "");
//		map.put("sampleFlag", "");
//		map.put("successFlag", "");
//		// map.put("cancerType-id", "");
//		// map.put("cancerType-name", "");
//		// map.put("cancerTypeSeedOne-id", "");
//		// map.put("cancerTypeSeedOne-name", "");
//		// map.put("cancerTypeSeedTwo-id", "");
//		// map.put("cancerTypeSeedTwo-name", "");
//		map.put("age", "");
//		map.put("orderType", "");
//
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

//	@Action(value = "sampleOrderSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showDialogSampleOrderList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/sample/sampleOrderDialog.jsp");
//	}
//
//	@Action(value = "showDialogSampleOrderListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogSampleOrderListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = sampleOrderService.findSampleOrderList(
//				map2Query, startNum, limitNum, dir, sort);
//		Long count = (Long) result.get("total");
//		List<SampleOrder> list = (List<SampleOrder>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("gender", "");
//		map.put("birthDate", "yyyy-MM-dd");
//		map.put("diagnosisDate", "");
//		map.put("dicType-id", "");
//		map.put("dicType-name", "");
//		map.put("sampleStage", "");
//		map.put("inspectionDepartment-id", "");
//		map.put("inspectionDepartment-name", "");
//		map.put("crmProduct-id", "");
//		map.put("crmProduct-name", "");
//		map.put("samplingDate", "yyyy-MM-dd");
//		map.put("samplingLocation-id", "");
//		map.put("samplingLocation-name", "");
//		map.put("samplingNumber", "");
//		map.put("pathologyConfirmed", "");
//		map.put("bloodSampleDate", "yyyy-MM-dd");
//		map.put("plasmapheresisDate", "yyyy-MM-dd");
//		map.put("commissioner-id", "");
//		map.put("commissioner-name", "");
//		map.put("receivedDate", "yyyy-MM-dd");
//		map.put("sampleTypeId", "");
//		map.put("sampleTypeName", "");
//		map.put("sampleCode", "");
//		map.put("medicalNumber", "");
//		map.put("family", "");
//		map.put("familyPhone", "");
//		map.put("familySite", "");
//		map.put("medicalInstitutions", "");
//		map.put("medicalInstitutionsPhone", "");
//		map.put("medicalInstitutionsSite", "");
//		map.put("attendingDoctor", "");
//		map.put("attendingDoctorPhone", "");
//		map.put("attendingDoctorSite", "");
//		map.put("note", "");
//		map.put("createUser-id", "");
//		map.put("createUser-name", "");
//		map.put("createDate", "yyyy-MM-dd");
//		map.put("confirmUser-id", "");
//		map.put("confirmUser-name", "");
//		map.put("confirmDate", "yyyy-MM-dd");
//		map.put("state", "");
//		map.put("stateName", "");
//		map.put("sampleFlag", "");
//		map.put("successFlag", "");
//		// map.put("cancerType-id", "");
//		// map.put("cancerType-name", "");
//		// map.put("cancerTypeSeedOne-id", "");
//		// map.put("cancerTypeSeedOne-name", "");
//		// map.put("cancerTypeSeedTwo-id", "");
//		// map.put("cancerTypeSeedTwo-name", "");
//		map.put("age", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
	
	

	@Action(value = "editSampleOrder")
	public String editSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			this.sampleOrder = sampleOrderService.get(id);
//			if (sampleOrder.getSuccessFlag() == null
//					|| "".equals(sampleOrder.getSuccessFlag())) {
//				List<SampleCancerTemp> list = this.sampleCancerTempService
//						.querySampleCancerList(id);
//				sampleCancerTempOne = list.get(0);
//				sampleCancerTempTwo = list.get(1);
//			} else {
//				sampleCancerTempOne = this.sampleOrderService
//						.fuzZhi(sampleOrder);
//				sampleCancerTempTwo = this.sampleOrderService
//						.fuzZhi(sampleOrder);
//
//			}

			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleOrder");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleOrder.setCreateUser(user);
			sampleOrder.setCreateDate(new Date());
			sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		// 复选框之全血样本
		List<DicSampleType> listCheck = sampleCancerTempService
				.findDicSampleType(null);
		putObjToContext("dicSampleTypeListByOrder", listCheck);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/sampleOrderEdit.jsp");
	}

	@Action(value = "copySampleOrder")
	public String copySampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleOrder = sampleOrderService.get(id);
		sampleOrder.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/sampleOrderEdit.jsp");
	}

	/**
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleOrder.getId();
		String number = sampleOrder.getMedicalNumber();

		if (id != null && id.equals("")) {
			sampleOrder.setId(null);
		}
		boolean result = crmPatientService.queryByCount(number);
		CrmPatient r = null;
		// 电子病历号为空,创建一个
		if (!"".equals(sampleOrder.getMedicalNumber())) {

			r = crmPatientService.get(sampleOrder.getMedicalNumber());
			// 该电子病历记录存在: 修改电子病历
			r.setId(sampleOrder.getMedicalNumber());
			r.setName(sampleOrder.getName());
			r.setGender(sampleOrder.getGender());
			r.setDateOfBirth(sampleOrder.getBirthDate());
			r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
			r.setCreateDate(sampleOrder.getCreateDate());
			r.setCreateUser(sampleOrder.getCreateUser());
			r.setCancerType(sampleOrder.getCancerType());
			r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
			r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());

			crmPatientService.save(r);

		} else {
			String modelName = "CrmPatient";
			String markCode = "P";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleOrder.setMedicalNumber(autoID);
			// 该电子记录不存在:添加电子病历
			r = new CrmPatient();
			r.setId(autoID);
			r.setId(sampleOrder.getMedicalNumber());
			r.setName(sampleOrder.getName());
			r.setGender(sampleOrder.getGender());
			r.setDateOfBirth(sampleOrder.getBirthDate());
			r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
			r.setCreateDate(sampleOrder.getCreateDate());
			r.setCreateUser(sampleOrder.getCreateUser());
			r.setCancerType(sampleOrder.getCancerType());
			r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
			r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
			crmPatientService.save(r);
		}

		Map aMap = new HashMap();
		// 保存病史和用药信息
		// aMap.put("sampleOrderPersonnel",
		// getParameterFromRequest("sampleOrderPersonnelJson"));
		// aMap.put("sampleOrderItem",
		// getParameterFromRequest("sampleOrderItemJson"));

		sampleOrderService.save(sampleOrder, aMap,null,null,null,null);
		// 保存一条空的纪录
		// 创建一个空的收费纪录
		CrmConsumerMarket market = new CrmConsumerMarket();
		SampleOrder mk = new SampleOrder();
		mk.setId(sampleOrder.getId());
		market.setSampleOrder(mk);
		this.sampleCancerTempService.savemaket(market);

		// 保存订单子表的关联信息
		// List<SampleOrderItem> listSampleOrderItem =
		// this.sampleOrderService.querySampleItem(sampleOrder.getId());
		// List<SampleOrderPersonnel>listSampleOrderPersonnel
		// =this.sampleOrderService.querySamplePersonnel(sampleOrder.getId());
		// if(listSampleOrderItem.size()>0){
		// for(SampleOrderItem order:listSampleOrderItem) {
		// this.crmPatientService.saveCrmPatientItem2(order,r);
		// }
		// }
		// if(listSampleOrderPersonnel.size()>0){
		// for(SampleOrderPersonnel sol:listSampleOrderPersonnel) {
		// this.crmPatientService.saveCrmPatientPersonnel2(sol,r);
		// }
		// }

		return redirect("/sample/sampleOrder/editSampleOrder.action?id="
				+ sampleOrder.getId());

	}

	@Action(value = "viewSampleOrder")
	public String toViewSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOrder = sampleOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/sampleOrderEdit.jsp");
	}

	/**
	 * 
	 * 子表的相关方法
	 * 
	 * @return
	 */
//	@Action(value = "showSampleOrderPersonnelList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showSampleOrderPersonnelList() throws Exception {
//		return dispatcher("/WEB-INF/page/sample/sampleOrderPersonnel.jsp");
//	}
//
//	@Action(value = "showSampleOrderPersonnelListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showSampleOrderPersonnelListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = sampleOrderService
//					.findSampleOrderPersonnelList(scId, startNum, limitNum,
//							dir, sort);
//			Long total = (Long) result.get("total");
//			List<SampleOrderPersonnel> list = (List<SampleOrderPersonnel>) result
//					.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("checkOutTheAge", "");
//			map.put("sampleOrder-name", "");
//			map.put("sampleOrder-id", "");
//			map.put("tumorCategory-name", "");
//			map.put("tumorCategory-id", "");
//			map.put("familyRelation", "");
//			new SendData().sendDateJson(map, list, total,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOrderPersonnel")
	public void delSampleOrderPersonnel() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleOrderService.delSampleOrderPersonnel(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

//	@Action(value = "showSampleOrderItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showSampleOrderItemList() throws Exception {
//		return dispatcher("/WEB-INF/page/sample/sampleOrderItem.jsp");
//	}
//
//	@Action(value = "showSampleOrderItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showSampleOrderItemListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = sampleOrderService
//					.findSampleOrderItemList(scId, startNum, limitNum, dir,
//							sort);
//			Long total = (Long) result.get("total");
//			List<SampleOrderItem> list = (List<SampleOrderItem>) result
//					.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("drugDate", "yyyy-MM-dd");
//			map.put("useDrugName", "");
//			map.put("effectOfProgress", "");
//			map.put("effectOfProgressSpeed", "");
//			map.put("geneticTestHistory", "");
//			map.put("sampleDetectionName", "");
//			map.put("sampleExonRegion", "");
//			map.put("sampleDetectionResult", "");
//			map.put("sampleOrder-name", "");
//			map.put("sampleOrder-id", "");
//			new SendData().sendDateJson(map, list, total,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOrderItem")
	public void delSampleOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleOrderService.delSampleOrderItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 根据主数据加载子表明细
	@Action(value = "setSamplePerson")
	public void setTemplateItem() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sampleOrderService
					.setTemplateItem(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据主数据加载子表明细2
	@Action(value = "setSampleItem")
	public void setSampleOrderItem() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sampleOrderService
					.setSampleOrderItem2(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 保存订单
	@Action(value = "saveUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveUpload() throws Exception {
		String itemDataJson = getParameterFromRequest("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sampleOrderService.saveUpload(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {

			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleOrderService getSampleOrderService() {
		return sampleOrderService;
	}

	public void setSampleOrderService(SampleOrderService sampleOrderService) {
		this.sampleOrderService = sampleOrderService;
	}

	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/**
	 * 癌症信息审核的get和set方法
	 * 
	 * @return
	 */
	public SampleCancerTempService getSampleCancerTempService() {
		return sampleCancerTempService;
	}

	public void setSampleCancerTempService(
			SampleCancerTempService sampleCancerTempService) {
		this.sampleCancerTempService = sampleCancerTempService;
	}

	public SampleCancerTemp getSampleCancerTempOne() {
		return sampleCancerTempOne;
	}

	public void setSampleCancerTempOne(SampleCancerTemp sampleCancerTempOne) {
		this.sampleCancerTempOne = sampleCancerTempOne;
	}

	public SampleCancerTemp getSampleCancerTempTwo() {
		return sampleCancerTempTwo;
	}

	public CrmPatientService getCrmPatientService() {
		return crmPatientService;
	}

	public void setCrmPatientService(CrmPatientService crmPatientService) {
		this.crmPatientService = crmPatientService;
	}

	public void setSampleCancerTempTwo(SampleCancerTemp sampleCancerTempTwo) {
		this.sampleCancerTempTwo = sampleCancerTempTwo;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}

	/**
	 * 通过项目名称得到id
	 * @throws Exception
	 */
	@Action(value = "GetProductIdByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void GetProductIdByName() throws Exception {
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("id", sampleOrderService.GetProductId(itemDataJson));
			result.put("success", true);
		} catch (Exception e) {

			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 通过字典名称及类型得到id
	 * 
	 * @throws Exception
	 */
	@Action(value = "GetDicTypeIdByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void GetDicTypeIdByName() throws Exception {
		String name = getRequest().getParameter("name");
		String type = getRequest().getParameter("type");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("dictid", sampleOrderService.GetDicTypeId(name,type));
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 通过销售代表名称得到id
	 * 
	 * @throws Exception
	 */
	@Action(value = "GetUserIdByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void GetUserIdByName() throws Exception {
		String name = getRequest().getParameter("name");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("id", sampleOrderService.GetUserId(name));
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 通过样本类型名称得到id
	 * 
	 * @throws Exception
	 */
	@Action(value = "GetsampleTypeIdByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void GetsampleTypeIdByName() throws Exception {
		String name = getRequest().getParameter("name");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("sampleTypeid", sampleOrderService.GetsampleTypeId(name));
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 通过样本单位名称得到id
	 * 
	 * @throws Exception
	 */
	@Action(value = "GetsampleUnitIdByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void GetsampleUnitIdByName() throws Exception {
		String name = getRequest().getParameter("name");
		String type = getRequest().getParameter("type");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("sampleUnitid", sampleOrderService.GetsampleUnitId(name, type));
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
