package com.biolims.sample.action;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;
import com.biolims.weichat.service.WeiChatCancerTypeService;
@Namespace("/sample/sampleCancerTemp")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleCancerTempAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2112";
	@Autowired
	private SampleCancerTempService sampleCancerTempService;
	private SampleCancerTemp sampleCancerTemp = new SampleCancerTemp();
	//注入电子病历的service
	@Autowired
	private CrmPatientService crmPatientService;
	
	@Autowired
	private WeiChatCancerTypeService weiChatCancerTypeService;
	@Resource
	private FileInfoService fileInfoService;
	
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	 	
	@Action(value = "showSampleCancerTempList")
	public String showSampleCancerTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleCancerTemp.jsp");
	}

	@Action(value = "showSampleCancerTempListJson")
	public void showSampleCancerTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleCancerTempService.findSampleCancerTempList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleCancerTemp> list = (List<SampleCancerTemp>) result.get("list");

		
		Map<String, String> map = new HashMap<String, String>();
		map.put("orderNumber", "");
		map.put("id", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("birthDate", "yyyy-MM-dd");
		map.put("diagnosisDate", "yyyy-MM-dd");
		map.put("dicType", "");
		map.put("sampleStage", "");
		map.put("inspectionDepartment-id", "");
		map.put("inspectionDepartment-name", "");
		map.put("crmProduct-id", "");
		map.put("crmProduct-name", "");
		map.put("samplingDate", "yyyy-MM-dd");
		map.put("samplingLocation-id", "");
		map.put("samplingLocation-name", "");
		map.put("samplingNumber", "");
		
//		map.put("collectionManner-id", "");
//		map.put("collectionManner-name", "");
//		map.put("chargeNote", "");
		
		map.put("pathologyConfirmed", "");
		map.put("bloodSampleDate", "yyyy-MM-dd");
		map.put("plasmapheresisDate", "yyyy-MM-dd");
		map.put("commissioner-id", "");
		map.put("commissioner-name", "");
		map.put("receivedDate", "yyyy-MM-dd");
		map.put("sampleTypeId", "");
		map.put("sampleTypeName", "");
		map.put("sampleOrder-id", "");
		map.put("sampleOrder-name", "");
		map.put("sampleCode", "");
		map.put("family", "");
		map.put("familyPhone", "");
		map.put("familySite", "");
		map.put("medicalInstitutions", "");
		map.put("medicalInstitutionsPhone", "");
		map.put("medicalInstitutionsSite", "");
		map.put("attendingDoctor", "");
		map.put("attendingDoctorPhone", "");
		map.put("attendingDoctorSite", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("sampleInfoMain", "");
//		map.put("cancerType-id", "");
//		map.put("cancerType-name", "");
//		map.put("cancerTypeSeedOne-id", "");
//		map.put("cancerTypeSeedOne-name", "");
//		map.put("cancerTypeSeedTwo-id", "");
//		map.put("cancerTypeSeedTwo-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	
	@Action(value = "sampleCancerTempSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleCancerTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleCancerTempDialog.jsp");
	}

	@Action(value = "showDialogSampleCancerTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleCancerTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleCancerTempService.findSampleCancerTempList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleCancerTemp> list = (List<SampleCancerTemp>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("orderNumber", "");
		map.put("id", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("birthDate", "yyyy-MM-dd");
		map.put("diagnosisDate", "yyyy-MM-dd");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleStage", "");
		map.put("inspectionDepartment-id", "");
		map.put("inspectionDepartment-name", "");
		map.put("crmProduct-id", "");
		map.put("crmProduct-name", "");
		map.put("samplingDate", "yyyy-MM-dd");
		map.put("samplingLocation-id", "");
		map.put("samplingLocation-name", "");
		map.put("samplingNumber", "");
		map.put("pathologyConfirmed", "");
		map.put("bloodSampleDate", "yyyy-MM-dd");
		map.put("plasmapheresisDate", "yyyy-MM-dd");
		map.put("commissioner-id", "");
		map.put("commissioner-name", "");
		map.put("receivedDate", "yyyy-MM-dd");
		map.put("sampleTypeId", "");
		map.put("sampleTypeName", "");
		map.put("sampleOrder-id", "");
		map.put("sampleOrder-name", "");
		map.put("medicalNumber", "");
		map.put("sampleCode", "");
		map.put("family", "");
		map.put("familyPhone", "");
		map.put("familySite", "");
		map.put("medicalInstitutions", "");
		map.put("medicalInstitutionsPhone", "");
		map.put("medicalInstitutionsSite", "");
		map.put("attendingDoctor", "");
		map.put("attendingDoctorPhone", "");
		map.put("attendingDoctorSite", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("sampleInfoMain", "");
//		map.put("cancerType-id", "");
//		map.put("cancerType-name", "");
//		map.put("cancerTypeSeedOne-id", "");
//		map.put("cancerTypeSeedOne-name", "");
//		map.put("cancerTypeSeedTwo-id", "");
//		map.put("cancerTypeSeedTwo-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editSampleCancerTemp")
	public String editSampleCancerTemp() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleCancerTemp = sampleCancerTempService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleCancerTemp");
		} else {
			sampleCancerTemp.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleCancerTemp.setCreateUser(user);
			sampleCancerTemp.setCreateDate(new Date());
			sampleCancerTemp.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleCancerTemp.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
 
		}
		//保存图片
		String imgId="";
		if(sampleCancerTemp.getOrderNumber()!=null){
			SampleOrder so = commonService.get(SampleOrder.class, sampleCancerTemp.getOrderNumber());
		}
		putObjToContext("imgId", imgId);
		//复选框之全血样本
		List<DicSampleType> list = sampleCancerTempService.findDicSampleType(null);	
		putObjToContext("dicSampleTypeList", list);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/sampleCancerTempEdit.jsp");
	}

	@Action(value = "copySampleCancerTemp")
	public String copySampleCancerTemp() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleCancerTemp = sampleCancerTempService.get(id);
		sampleCancerTemp.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/sampleCancerTempEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleCancerTemp.getId();
		//DicType a	= sampleCancerTemp.getInspectionDepartment();
		if(id!=null&&id.equals("NEW")){
			sampleCancerTemp.setId(null);
			String modelName = "SampleCancerTemp";
			String markCode = "PO";
			String prefix = sampleCancerTemp.getOrderNumber();
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, prefix, 00, 2, null);
			sampleCancerTemp.setId(autoID);
		}
		//保存子表
		Map aMap = new HashMap();
			//aMap.put("sampleCancerTempPersonnel",getParameterFromRequest("sampleCancerTempPersonnelJson"));
			//aMap.put("sampleCancerTempItem",getParameterFromRequest("sampleCancerTempItemJson"));
	
			sampleCancerTempService.save(sampleCancerTemp,aMap);
		if(sampleCancerTemp.getOrderNumber()!=null){
			
			SampleOrder so = commonService.get(SampleOrder.class, sampleCancerTemp.getOrderNumber());
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			if(so==null){
				so = new SampleOrder();
				so.setId(sampleCancerTemp.getOrderNumber());
				so.setSampleFlag("1");
				so.setState("2");
				so.setStateName("一录完成");
				so.setInfoUserStr(so.getInfoUserStr()+","+user.getId());
				
			}else{
				
				if(so.getSampleFlag()==null){
					so.setSampleFlag("1");
					so.setStateName("一录完成");
				}
				else{
					so.setSampleFlag("2");
					so.setStateName("二录完成");
					
				}
				so.setState("2");
				
				so.setInfoUserStr(so.getInfoUserStr()+","+user.getId());
			}
			commonService.saveOrUpdate(so);
			
			if(so.getSampleFlag().equals("2")){
				//二录完成，进行信息比对，如果大部分记录相同，就不需要进行审核
				boolean tof = this.sampleCancerTempService.compareAndTransmit(sampleCancerTemp);
				if(tof){
					//sampleOrder对象
					SampleOrder sampleOrderUnique = this.sampleCancerTempService.getSampleOrder(sampleCancerTemp.getOrderNumber());
					//创建一个空的收费纪录
					CrmConsumerMarket market = new CrmConsumerMarket();
					SampleOrder mk = new SampleOrder();
					mk.setId(sampleOrderUnique.getId());
					market.setSampleOrder(mk);
					this.sampleCancerTempService.savemaket(market);
					// 判断电子病历是否存在
					boolean result = this.crmPatientService.queryByCount(sampleOrderUnique.getMedicalNumber());
					CrmPatient r = null;
					/**
					 * 接下来判断电子病历是否存在，如果存在则添加其子表纪录
					 */
					if(!"".equals(sampleCancerTemp.getMedicalNumber())){
						r = crmPatientService.get(sampleOrderUnique.getMedicalNumber());
						//该电子病历记录存在: 修改电子病历
						r.setId(sampleOrderUnique.getMedicalNumber());
						r.setName(sampleOrderUnique.getName());
						r.setGender(sampleOrderUnique.getGender());
					    r.setDateOfBirth(sampleOrderUnique.getBirthDate());
					    r.setTelphoneNumber1(sampleOrderUnique.getFamilyPhone());
					    r.setCreateDate(sampleOrderUnique.getCreateDate());
					    r.setCreateUser(sampleOrderUnique.getCreateUser());
						//保存电子病历
					    crmPatientService.save(r);
					 
					}else{
						String modelName = "CrmPatient";
						String markCode = "P";
						String autoID = codingRuleService.genTransID(modelName, markCode);
						 
						//电子病历不存在
						r = new CrmPatient();
					    r.setId(autoID);
					    r.setName(sampleOrderUnique.getName());
					    r.setGender(sampleOrderUnique.getGender());
					    r.setDateOfBirth(sampleOrderUnique.getBirthDate());
					    r.setTelphoneNumber1(sampleOrderUnique.getFamilyPhone());
					    r.setCreateDate(sampleOrderUnique.getCreateDate());
					    r.setCreateUser(sampleOrderUnique.getCreateUser());
					    crmPatientService.save(r);
					    sampleCancerTemp.setMedicalNumber(autoID);
					    this.sampleCancerTempService.save(sampleCancerTemp);
					   
					}
					
					//判断子表的纪录数量是否一致
					//List<SampleCancerTemp> list =this.sampleCancerTempService.querySampleCancerList(sampleCancerTemp.getOrderNumber());
					//if(list.size()==2){
						//String id1 = list.get(0).getId();
						//String id2 = list.get(1).getId();
//						//根据癌症编号，查询子表纪录1
//						 List<SampleCancerTempPersonnel> list1= this.sampleCancerTempService.getPersonnel(id1);
//						 List<SampleCancerTempPersonnel> list2= this.sampleCancerTempService.getPersonnel(id2);
//						//根据癌症编号，查询子表纪录2
//						 List<SampleCancerTempItem> list3= this.sampleCancerTempService.getCancerTempItem(id1);
//						 List<SampleCancerTempItem> list4= this.sampleCancerTempService.getCancerTempItem(id1);
//						if(list1.size()==list2.size()&&list3.size()==list4.size()){
//							//一录和二录的子表纪录数量对应相等，把一录的子表纪录添加到订单的子表纪录
//							for(SampleCancerTempPersonnel p: list1){
//								SampleOrderPersonnel order = new SampleOrderPersonnel();
//								this.sampleCancerTempService.copyPersonnel(order, p,sampleOrderUnique);
//							}
//							for(SampleCancerTempItem item: list3){
//								SampleOrderItem order = new SampleOrderItem();
//								this.sampleCancerTempService.copyItem(order, item,sampleOrderUnique);
//							}
//						}	
							
							 /**
							  * 循环添加电子病历子表纪录
							  * */
//							if(list1.size()==list2.size()&&list3.size()==list4.size()){
//								//一录和二录的子表纪录数量对应相等，把一录的子表纪录添加到订单的子表纪录
//								for(SampleCancerTempPersonnel p: list1){
//									CrmPatientPersonnel person = new CrmPatientPersonnel();
//									this.sampleCancerTempService.copycmrPerson(person, p,r);
//								}
//								for(SampleCancerTempItem item: list3){
//									CrmPatientItem t = new CrmPatientItem();
//									this.sampleCancerTempService.copycmrItem(t, item,r);
//								}
//							}	
			
						
					//}
				}
				
				
			}
			
		}
		
		return redirect("/sample/sampleCancerTemp/editSampleCancerTemp.action?id=" + sampleCancerTemp.getId());

	}

	@Action(value = "viewSampleCancerTemp")
	public String toViewSampleCancerTemp() throws Exception {
		String id = getParameterFromRequest("id");
		sampleCancerTemp = sampleCancerTempService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/sampleCancerTempEdit.jsp");
	}
	

	@Action(value = "showSampleCancerTempPersonnelList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleCancerTempPersonnelList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/sampleCancerTempPersonnel.jsp");
	}

	@Action(value = "showSampleCancerTempPersonnelListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleCancerTempPersonnelListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleCancerTempService.findSampleCancerTempPersonnelList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("checkOutTheAge", "");
			map.put("sampleCancerTemp-name", "");
			map.put("sampleCancerTemp-id", "");
			map.put("tumorCategory-name", "");
			map.put("tumorCategory-id", "");
			map.put("familyRelation", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleCancerTempPersonnel")
	public void delSampleCancerTempPersonnel() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleCancerTempService.delSampleCancerTempPersonnel(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showSampleCancerTempItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleCancerTempItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/sampleCancerTempItem.jsp");
	}

	@Action(value = "showSampleCancerTempItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleCancerTempItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleCancerTempService.findSampleCancerTempItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("drugDate", "yyyy-MM-dd");
			map.put("useDrugName", "");
			map.put("effectOfProgress", "");
			map.put("effectOfProgressSpeed", "");
			map.put("geneticTestHistory", "");
			map.put("sampleDetectionName", "");
			map.put("sampleExonRegion", "");
			map.put("sampleDetectionResult", "");
			map.put("sampleCancerTemp-name", "");
			map.put("sampleCancerTemp-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleCancerTempItem")
	public void delSampleCancerTempItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleCancerTempService.delSampleCancerTempItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	/**
	 * 查询肿瘤类型的action
	 * @return
	 * @throws Exception
	 */
	@Action(value = "weiChatCancerTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWeiChatCancerTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeDialog.jsp");
	}

	@Action(value = "showDialogWeiChatCancerTypeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWeiChatCancerTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = weiChatCancerTypeService.findWeiChatCancerTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WeiChatCancerType> list = (List<WeiChatCancerType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("cancerTypeName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	/**
	 * 查询肿瘤类型一的action
	 * @return
	 * @throws Exception
	 */
	@Action(value = "weiChatCancerTypeSelectOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String weiChatCancerTypeSelectOne() throws Exception {
		String code =getRequest().getParameter("code");
		putObjToContext("code",code);
		System.out.println("+++++++++++++++++++++++++"+code);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeDialogOne.jsp");
	}

	@Action(value = "weiChatCancerTypeSelectOneJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void weiChatCancerTypeSelectOneJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = weiChatCancerTypeService.findWeiChatCancerTypeSeedOneList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<WeiChatCancerTypeSeedOne> list = (List<WeiChatCancerTypeSeedOne>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("cancerTypeName", "");
		/*	map.put("cancerTypeId-name", "");
			map.put("cancerTypeId-id", "");*/
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *  查询肿瘤类型二的action
	 * @return
	 * @throws Exception
	 */
	@Action(value = "weiChatCancerTypeSelectTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String weiChatCancerTypeSelectTwo() throws Exception {
		String code =getRequest().getParameter("code");
		putObjToContext("code",code);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeDialogTwo.jsp");
	}

	@Action(value = "weiChatCancerTypeSelectTwoJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void weiChatCancerTypeSelectTwoJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = weiChatCancerTypeService.findWeiChatCancerTypeSeedTwoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<WeiChatCancerTypeSeedTwo> list = (List<WeiChatCancerTypeSeedTwo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("cancerTypeName", "");
//			map.put("cancerTypeId-name", "");
//			map.put("cancerTypeId-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleCancerTempService getSampleCancerTempService() {
		return sampleCancerTempService;
	}

	public void setSampleCancerTempService(SampleCancerTempService sampleCancerTempService) {
		this.sampleCancerTempService = sampleCancerTempService;
	}

	public SampleCancerTemp getSampleCancerTemp() {
		return sampleCancerTemp;
	}

	public void setSampleCancerTemp(SampleCancerTemp sampleCancerTemp) {
		this.sampleCancerTemp = sampleCancerTemp;
	}
	/**
	 * 电子病历的service
	 * @return
	 */
	public CrmPatientService getCrmPatientService() {
		return crmPatientService;
	}

	public void setCrmPatientService(CrmPatientService crmPatientService) {
		this.crmPatientService = crmPatientService;
	}

	public WeiChatCancerTypeService getWeiChatCancerTypeService() {
		return weiChatCancerTypeService;
	}

	public void setWeiChatCancerTypeService(
			WeiChatCancerTypeService weiChatCancerTypeService) {
		this.weiChatCancerTypeService = weiChatCancerTypeService;
	}
	
	


}
