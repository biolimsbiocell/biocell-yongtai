package com.biolims.sample.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/sampleSearch")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleSearchAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9015159";
	@Autowired
	private SampleSearchService sampleSearchService;
	
	@Resource
	private CommonService commonService;
	@Resource
	private CommonDAO commonDAO;


	@Action(value = "showSampleSearch")
	public String showSampleSearch() throws Exception {
		
		String searchnum = getParameterFromRequest("searchnum");
		putObjToContext("searchnum", searchnum);
		
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/search/sampleSearch.jsp");
	}
	
	/**
	 * 查询相应信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "sampleSearchInfo")
	public void sampleSearchInfo() throws Exception {
		//批次号
		String batch = getRequest().getParameter("batch");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//申请单信息
			List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", batch);
			if(sos.size()>0) {
				map.put("sampleOrder", sos.get(0));
			}else {
				map.put("sampleOrder", null);
			}
			if(!sos.isEmpty()) {
				sampleSearchService.insertLog(batch,"");
			}

			//样本接收记录
			List<SampleReceive> srs = commonService.get(SampleReceive.class, "barcode", batch);
			if(srs.size()>0) {
				map.put("sampleReceive", srs.get(0));
			}else {
				map.put("sampleReceive", null);
			}
			//生产记录
			List<CellPassageTemplate> cpts = sampleSearchService.searchCellPassageTemplates(batch);//;commonService.get(CellPassageTemplate.class, "cellPassage.batch", batch);
			if(cpts.size()>0) {
				List<Map<String, Object>> cellproducts = new ArrayList<Map<String,Object>>();
				for(CellPassageTemplate cpt:cpts) {
					Map<String, Object> cellItem = new HashMap<String, Object>();
					cellItem.put("id", cpt.getId());
					cellItem.put("cid", cpt.getCellPassage().getId());
					cellItem.put("orderNum", cpt.getOrderNum());
					cellItem.put("mc", cpt.getName().replace(" ",""));
					cellItem.put("estimatedDate", cpt.getEstimatedDate());
					cellItem.put("productStartTime", cpt.getProductStartTime());
					cellItem.put("productEndTime", cpt.getProductEndTime());
//					cellItem.put("planWorkDate", cpt.getPlanWorkDate());
					cellItem.put("state", cpt.getStepState());
					List<CellProductionRecord> cprs = sampleSearchService.searchCellPassageRecord(cpt.getCellPassage().getId(), batch, cpt.getOrderNum());
					if(cprs.size()>0) {
						if(cprs.get(0).getTempleOperator()!=null) {
							cellItem.put("templeOperatorName", cprs.get(0).getTempleOperator().getName());
						}else {
							cellItem.put("templeOperatorName", null);
						}
						cellItem.put("operatingRoomName", cprs.get(0).getOperatingRoomName());
						cellItem.put("planWorkDate", cprs.get(0).getPlanWorkDate());
					}else {
						cellItem.put("templeOperatorName", null);
						cellItem.put("operatingRoomName", null);
						cellItem.put("planWorkDate", null);
					}
					
					List<CellPassageQualityItem> CellPassageQualityItemss = sampleSearchService.getCellPassageQualityItemUnquality(cpt.getCellPassage().getId(), cpt.getOrderNum());
					if(CellPassageQualityItemss.size()>0) {
						cellItem.put("unstar", "1");
					}else {
						cellItem.put("unstar", "0");
					}
					cellproducts.add(cellItem);
				}
				map.put("scjl", cellproducts);
			}else {
				map.put("scjl", null);
			}
			//放行审核
			List<PlasmaReceiveItem> pris = commonService.get(PlasmaReceiveItem.class, "sampleCode", batch);
			if(pris.size()>0) {
				map.put("fxsh", pris.get(0));
			}else {
				map.put("fxsh", null);
			}
			//运输安排  查询没有作废的运输计划明细
			List<TransportOrderCell> tocs = sampleSearchService.getTransportOrderCells(batch);
			if(tocs.size()>0) {
				map.put("ysap", tocs.get(0));
			}else {
				map.put("ysap", null);
			}
			//偏差不合格（list）  查询不是作废的偏
			List<InfluenceProduct> pips = sampleSearchService.getPlanInfluenceProducts(batch);
			if(pips.size()>0) {
				
				List<Map<String, Object>> planInfluenceProducts = new ArrayList<Map<String,Object>>();
				for(InfluenceProduct pip:pips) {
					Map<String, Object> cellItem = new HashMap<String, Object>();
					cellItem.put("itemid", pip.getId());
					cellItem.put("id", pip.getDeviationHandlingReport().getId());
					cellItem.put("note", pip.getDeviationHandlingReport().getName());
					cellItem.put("no", pip.getDeviationHandlingReport().getNo());
					if(pip.getDeviationHandlingReport().getKindId()!=null
							&&pip.getDeviationHandlingReport().getKindId()!="") {
						String[] types = pip.getDeviationHandlingReport().getKindId().split(",");
						String type = "";
						for(String ty:types) {
							DicType dt = commonDAO.get(DicType.class, ty);
							if(dt!=null) {
								if("".equals(type)) {
									type = dt.getName();
								}else {
									type = type + "," + dt.getName();
								}
							}
						}
						cellItem.put("type", type);
					}else {
						cellItem.put("type", null);
					}
					cellItem.put("fssj", pip.getDeviationHandlingReport().getHappenDate());
					if(pip.getDeviationHandlingReport().getDiscoverer()!=null) {
						cellItem.put("fxr", pip.getDeviationHandlingReport().getDiscoverer().getName());
					}else {
						cellItem.put("fxr", null);
					}
					if("完成".equals(pip.getDeviationHandlingReport().getStateName())) {
						cellItem.put("cljd", "完成");
					}else {
						cellItem.put("cljd", "处理中");
					}
					planInfluenceProducts.add(cellItem);
				}
				map.put("pc", planInfluenceProducts);
			}else {
				map.put("pc", null);
			}
			map.put("success", true);

		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "showQualityTestTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQualityTestTable() throws Exception {
		String code = getParameterFromRequest("code");
		String barCod = getParameterFromRequest("barCod");
		String ordernum = getParameterFromRequest("ordernum");
		sampleSearchService.insertLog(barCod, "查看质检结果");
		putObjToContext("ordernum", ordernum);
		putObjToContext("flag", code);
		return dispatcher("/WEB-INF/page/sample/search/showQualityTestTable.jsp");
	}

	@Action(value = "showQualityTestTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQualityTestTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String batch = getParameterFromRequest("id");
		String ordernum = getParameterFromRequest("ordernum");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleSearchService.showCellPassageQualityItemJson(start, length, query, col, sort,
				batch, ordernum);
		List<CellPassageQualityItem> list = (List<CellPassageQualityItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		/** 编码 */
		map.put("id", "");
		/** 提交表编码 */
		map.put("tempId", "");
		/** 样本编号 */
		map.put("code", "");
		/** 批次号 */
		map.put("batch", "");
		/** 订单编号 */
		map.put("orderCode", "");
		/** 检测项目 */
		map.put("productId", "");
		/** 检测项目 */
		map.put("productName", "");
		/** 状态 */
		map.put("state", "");
		/** 备注 */
		map.put("note", "");
		/** 浓度 */
		map.put("concentration", "");
		/** 样本数量 */
		map.put("sampleNum", "");
		/** 相关主表 */
		map.put("cellPassage-id", "");
		map.put("cellPassage-name", "");
		/** 体积 */
		map.put("volume", "");
		/** 任务单id */
		map.put("orderId", "");
		/** 中间产物数量 */
		map.put("productNum", "");
		/** 中间产物类型编号 */
		map.put("dicSampleTypeId", "");
		/** 中间产物类型 */
		map.put("dicSampleTypeName", "");
		/** 样本类型 */
		map.put("sampleType", "");
		/** 样本主数据 */
		map.put("sampleInfo-id", "");
		map.put("sampleInfo-name", "");
		/** 实验的步骤号 */
		map.put("stepNum", "");
		/** 检测项 */
		map.put("sampleDeteyion-id", "");
		map.put("sampleDeteyion-name", "");
		/** 关联订单 */
		map.put("sampleOrder-id", "");
		map.put("sampleOrder-name", "");
		/** 是否合格 */
		map.put("qualified", "");
		/** 质检结果表id */
		map.put("qualityInfoId", "");
		/** 质检是否接收 */
		map.put("qualityReceive", "");
		/** 质检提交时间 */
		map.put("qualitySubmitTime", "yyyy-MM-dd HH:mm:ss");
		/** 质检接收时间 */
		map.put("qualityReceiveTime", "yyyy-MM-dd HH:mm:ss");
		/** 质检完成时间 */
		map.put("qualityFinishTime", "yyyy-MM-dd HH:mm:ss");

		map.put("sampleOrder-filtrateCode", "");
		
		/** 质检提交人 */
		map.put("qualitySubmitUser", "");
		/** 质检接收人 */
		map.put("qualityReceiveUser", "");
		/** 检测方式 */
		map.put("cellType", "");
		/** 是否提交 */
		map.put("submit", "");
		
		
		map.put("sampleNumUnit", "");
		
		map.put("experimentalStepsName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	
	/**
	 * 扫细胞编号查询关联批次编号
	 * 
	 * @throws Exception
	 */
	@Action(value = "findBatchNumByCellCode")
	public void findBatchNumByCellCode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String batch = getParameterFromRequest("batch");
			List<SampleOrderItem> in = sampleSearchService.findBatchNumByCellCode(batch);
			if (in.size()>0) {
				map.put("batch", in.get(0).getSampleOrder().getBarcode());
				map.put("success", true);
			} else {
				map.put("batch", "");
				map.put("success", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}


}
