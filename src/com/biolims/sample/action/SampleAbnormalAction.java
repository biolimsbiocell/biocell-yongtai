﻿package com.biolims.sample.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.service.SampleAbnormalService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/sampleAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";//2103
	@Autowired
	private SampleAbnormalService sampleAbnormalService;
	private SampleAbnormal sampleAbnormal = new SampleAbnormal();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showSampleAbnormalTable")
	public String showSampleAbnormalList() throws Exception {
		String sampleStyle = getParameterFromRequest("sampleStyle");// 判断是什么类型样本
		putObjToContext("sampleStyle", sampleStyle);
//		if ("1".equals(sampleStyle)) {// 科研试验样本
//			setRightsId("2103");
//		} else if ("".equals(sampleStyle) || "2".equals(sampleStyle)) {// 临床样本
//			setRightsId("2103");
//		}
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleAbnormalTable.jsp");
	}
	@Action(value = "showSampleAbnormalTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleAbnormalTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleAbnormalService
				.showSampleAbnormalTableJson(start, length, query, col, sort);
		List<SampleAbnormal> list = (List<SampleAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("hangdleDate", "yyyy-MM-dd");
		map.put("patientName", "");
		map.put("advice", "");
		map.put("note", "");
		map.put("idCard", "");
		map.put("sampleCode", "");
		map.put("phone", "");
		map.put("runTime", "yyyy-MM-dd");
		map.put("feedBackTime", "yyyy-MM-dd");
		map.put("method", "");
		map.put("reportDate", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("isExecute", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("state", "");
		map.put("itemId", "");
		map.put("sampleNum", "");
		map.put("sampleType", "");
		map.put("unusual-id", "");
		map.put("unusual-name", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmDoctor-id", "");
		map.put("crmDoctor-name", "");
		map.put("species", "");
		map.put("orderId", "");
		map.put("sampleStyle", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result,data));
	}
//	@Action(value = "showAbnormalType")
//	public String showAbnormalType() {
//		return dispatcher("/WEB-INF/page/sample/sampleAbnomalType.jsp");
//	}

//	@Action(value = "showDicUnusualMethod")
//	public String showDicUnusualMethod() {
//		return dispatcher("/WEB-INF/page/sample/sampleDicUnusualMethod.jsp");
//	}
//
//	@Action(value = "showshowDicUnusualMethodAllJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showshowDicUnusualMethodAllJson() throws Exception {
//		Map<String, Object> result = sampleAbnormalService
//				.selectDicUnusualMethodAll();
//
//		Long count = (Long) result.get("total");
//
//		List<DicType> list = (List<DicType>) result.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		 map.put("id", "");
//		 map.put("name", "");
//		 map.put("note", "");
//		 map.put("code", "");
//		 map.put("orderNumber", "");
//		 map.put("sysCode", "");
//		 map.put("type-id", "");
//		 map.put("state", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

//	@Action(value = "showAbnormalTypeAllJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showAbnormalTypeAllJson() throws Exception {
//		Map<String, Object> result = sampleAbnormalService
//				.selectAbnormalTypeAll();
//
//		Long count = (Long) result.get("total");
//
//		List<AbnomalType> list = (List<AbnomalType>) result.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("parent", "");
//		map.put("method", "");
//		map.put("state", "");
//		map.put("stateName", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

//	@Action(value = "showSampleAbnormalListJson")
//	public void showSampleAbnormalListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		String sampleStyle = getParameterFromRequest("sampleStyle");// 判断是什么类型样本
//		putObjToContext("sampleStyle", sampleStyle);
//		if ("1".equals(sampleStyle)) {// 科研试验样本
//			setRightsId("2103");
//		} else if ("".equals(sampleStyle) || "2".equals(sampleStyle)) {// 临床样本
//			setRightsId("2103");
//		}
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0) {
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		}
//		try {
////			Map<String, Object> result = sampleAbnormalService
////					.findSampleAbnormalList(map2Query, startNum, limitNum, dir,
////							sort);
//			Map<String, Object> result = sampleAbnormalService
//					.findSampleAbnormalList(map2Query, startNum, limitNum, dir,
//							sort, sampleStyle);
//			Long count = (Long) result.get("total");
//			List<SampleAbnormal> list = (List<SampleAbnormal>) result
//					.get("list");
//
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("name", "");
//			map.put("hangdleDate", "yyyy-MM-dd");
//			map.put("patientName", "");
//			map.put("advice", "");
//			map.put("note", "");
//			map.put("idCard", "");
//			map.put("sampleCode", "");
//			map.put("phone", "");
//			map.put("runTime", "yyyy-MM-dd");
//			map.put("feedBackTime", "yyyy-MM-dd");
//			map.put("methods", "");
//			map.put("reportDate", "");
//			map.put("dicSampleType-id", "");
//			map.put("dicSampleType-name", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("inspectDate", "");
//			map.put("acceptDate", "yyyy-MM-dd");
//			map.put("isExecute", "");
//			map.put("nextFlow", "");
//			map.put("nextFlowId", "");
//			map.put("state", "");
//			map.put("itemId", "");
//			map.put("sampleNum", "");
//			map.put("sampleType", "");
//			map.put("unusual-id", "");
//			map.put("unusual-name", "");
//			map.put("dicType-id", "");
//			map.put("dicType-name", "");
//			map.put("project-id", "");
//			map.put("project-name", "");
//			map.put("crmCustomer-id", "");
//			map.put("crmCustomer-name", "");
//			map.put("crmDoctor-id", "");
//			map.put("crmDoctor-name", "");
//			map.put("species", "");
//			
//			map.put("sampleStyle", "");
//			new SendData().sendDateJson(map, list, count,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

//	@Action(value = "sampleAbnormalSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showDialogSampleAbnormalList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/sample/sampleAbnormalDialog.jsp");
//	}
//
//	@Action(value = "showDialogSampleAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogSampleAbnormalListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = sampleAbnormalService
//				.findSampleAbnormalList(map2Query, startNum, limitNum, dir,
//						sort);
//		Long count = (Long) result.get("total");
//		List<SampleAbnormal> list = (List<SampleAbnormal>) result.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("hangdleDate", "yyyy-MM-dd");
//		map.put("patientName", "");
//		map.put("advice", "");
//		map.put("note", "");
//		map.put("sampleCode", "");
//		map.put("runTime", "yyyy-MM-dd");
//		map.put("feedBackTime", "yyyy-MM-dd");
//		map.put("methods", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

//	@Action(value = "editSampleAbnormal")
//	public String editSampleAbnormal() throws Exception {
//		String id = getParameterFromRequest("id");
//		long num = 0;
//		if (id != null && !id.equals("")) {
//			sampleAbnormal = sampleAbnormalService.get(id);
//			putObjToContext("handlemethod",
//					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			toToolBar(rightsId, "", "",
//					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			num = fileInfoService.findFileInfoCount(id, "sampleAbnormal");
//		} else {
//			sampleAbnormal = new SampleAbnormal();
//			sampleAbnormal.setId(SystemCode.DEFAULT_SYSTEMCODE);
//			User user = (User) this
//					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			// sampleAbnormal.setCreateUser(user);
//			// sampleAbnormal.setCreateDate(new Date());
//			putObjToContext("handlemethod",
//					SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//		}
//		putObjToContext("fileNum", num);
//		return dispatcher("/WEB-INF/page/sample/sampleAbnormalEdit.jsp");
//	}
	/**
	 * 
	 * @Title: save  
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年5月3日下午2:50:36
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("dataJson");
			sampleAbnormalService.saveSampleItem(dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: executeAbnormal  
	 * @Description: 保存并执行
	 * @author : shengwei.wang
	 * @date 2018年2月1日下午2:03:05
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="executeAbnormal")
	public void executeAbnormal() throws Exception {
		String [] ids=getRequest().getParameterValues("ids[]");
		String dataJson=getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			sampleAbnormalService.executeAbnormal(ids,dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));		
	}
	/**
	 * 
	 * @Title: feedbackAbnormal  
	 * @Description: 反馈至项目组  
	 * @author : shengwei.wang
	 * @date 2018年5月4日上午10:01:54
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="feedbackAbnormal")
	public void feedbackAbnormal() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			sampleAbnormalService.feedbackAbnormal(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));	
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleAbnormalService getSampleAbnormalService() {
		return sampleAbnormalService;
	}

	public void setSampleAbnormalService(
			SampleAbnormalService sampleAbnormalService) {
		this.sampleAbnormalService = sampleAbnormalService;
	}

	public SampleAbnormal getSampleAbnormal() {
		return sampleAbnormal;
	}

	public void setSampleAbnormal(SampleAbnormal sampleAbnormal) {
		this.sampleAbnormal = sampleAbnormal;
	}

}
