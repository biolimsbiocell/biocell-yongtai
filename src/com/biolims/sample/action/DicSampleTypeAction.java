package com.biolims.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.DicSampleTypeService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/sample/dicSampleType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DicSampleTypeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "1199";
	@Autowired
	private DicSampleTypeService dicSampleTypeService;
	private DicSampleType dicSampleType = new DicSampleType();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	private String log = "";
	
/*NEW	*/
	@Action(value = "showDicSampleTypeList")
	public String showDicSampleTypeList() throws Exception {
		rightsId = "1199";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleType.jsp");
	}

	@Action(value = "showDicSampleTypeEditList")
	public String showDicSampleTypeEditList() throws Exception {
		rightsId = "1199";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEditList.jsp");
	}
	
	@Action(value = "showDicSampleTypeTableJson")
	public void showDicSampleTypeTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = dicSampleTypeService.findDicSampleTypeTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<DicSampleType> list = (List<DicSampleType>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("name", "");
			map.put("note", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("first", "");
			map.put("picCode", "");
			map.put("picCodeName", "");
			map.put("state", "");
			map.put("stateName", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("DicSampleType");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Action(value = "dicSampleTypeSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDicSampleTypeList() throws Exception {
		rightsId = "1199";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeSelectTable.jsp");
	}

	@Action(value = "copyDicSampleType")
	public String copyDicSampleType() throws Exception {
		rightsId = "1199";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dicSampleType = dicSampleTypeService.get(id);
		dicSampleType.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEdit.jsp");
	}
	@Action(value = "save")
	public void save() throws Exception {
		String msg = "";
		String zId = "";
		boolean bool = true;	
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			for (Map<String, Object> map : list) {
				String orderNumberI = map.get("orderNumber").toString();
				map.remove("orderNumber");
				Integer orderNumber = Integer.parseInt(orderNumberI);
				map.put("orderNumber", orderNumber);
				dicSampleType = (DicSampleType)commonDAO.Map2Bean(map, dicSampleType);
			}
			String id = dicSampleType.getId();
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				log="123";
				String modelName = "DicSampleType";
				String markCode = "C";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				dicSampleType.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			
			dicSampleTypeService.save(dicSampleType,aMap,changeLog,lMap,log);
			
			zId = dicSampleType.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "viewDicSampleType")
	public String toViewDicSampleType() throws Exception {
		String id = getParameterFromRequest("id");
		rightsId = "1199";
		dicSampleType = dicSampleTypeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEdit.jsp");
	}
	@Action(value = "saveDicSampleTypeTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDicSampleTypeTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		
		String str = "["+dataValue+"]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
		Map<String, Object> map=new HashMap<String, Object>();
		try {
		for (Map<String, Object> map1 : list) {
			DicSampleType a = new DicSampleType();
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (DicSampleType)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			dicSampleTypeService.save(a,aMap,changeLog,lMap,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
/*	Old*/
/*	@Action(value = "showDicSampleTypeList")
	public String showDicSampleTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleType.jsp");

	}*/
	/**
	 * 
	 * @Title: showDialogDicSampleTypeTable  
	 * @Description: 选择样本类型（多选）
	 * @author : shengwei.wang
	 * @date 2018年3月12日下午2:52:14
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "showDialogDicSampleTypeTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDicSampleTypeTable() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeSelectTable.jsp");
	}
	@Action(value = "showDialogDicSampleTypeTableJson")
	public void showDialogDicSampleTypeTableJson() throws Exception {
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result = dicSampleTypeService.showDialogDicSampleTypeTableJson(start,length,query,col,sort);
		List<DicSampleType> list = (List<DicSampleType>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	/**
	 * 
	 * @Title: selectDicSampleTypeOne  
	 * @Description: 单选  
	 * @author : shengwei.wang
	 * @date 2018年3月12日下午4:01:27
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "selectDicSampleTypeOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectDicSampleTypeOne() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeOne.jsp");

	}
//	@Action(value = "selectDicSampleTypeOneJson")
//	public void selectDicSampleTypeOneJson() throws Exception {
//		String query=getParameterFromRequest("query");
//		String colNum=getParameterFromRequest("order[0][column]");
//		String col=getParameterFromRequest("columns["+colNum+"][data]");
//		String sort=getParameterFromRequest("order[0][dir]");
//		Integer start=Integer.valueOf(getParameterFromRequest("start"));
//		Integer length=Integer.valueOf(getParameterFromRequest("length"));
//		String draw=getParameterFromRequest("draw");
//		Map<String,Object> result = dicSampleTypeService.showDialogDicSampleTypeTableJson(start,length,query,col,sort);
//		List<DicSampleType> list = (List<DicSampleType>) result
//				.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("code", "");
//		String data = new SendData().getDateJsonForDatatable(map, list);
//		HttpUtils.write(PushData.pushData(draw, result, data));
//	}
	@Action(value = "showDicSampleTypeListJson")
	public void showDicSampleTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dicSampleTypeService
				.findDicSampleTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DicSampleType> list = (List<DicSampleType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("note", "");
		map.put("orderNumber", "");
		map.put("type", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("first", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	@Action(value = "showDicSampleTypeListJson1")
	public void showDicSampleTypeListJson1() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dicSampleTypeService
				.findDicSampleTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DicSampleType> list = (List<DicSampleType>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("note", "");
		map.put("orderNumber", "");
		map.put("type", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("first", "");
		map.put("dw", "");
//		map.put("dwname", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "dicSampleTypeSelect1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDicSampleTypeList1() throws Exception {
		String a = getParameterFromRequest("a");
		putObjToContext("a", a);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeDialog1.jsp");
	}

	@Action(value = "showDialogDicSampleTypeListJson1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDicSampleTypeListJson1() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dicSampleTypeService
				.findDicSampleTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DicSampleType> list = (List<DicSampleType>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("note", "");
		map.put("orderNumber", "");
		map.put("type", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("dw", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	@Action(value = "editDicSampleType")
	public String editDicSampleType() throws Exception {
		rightsId = "1199";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dicSampleType = dicSampleTypeService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dicSampleType");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
/*			dicSampleType.setCreateUser(user);
			dicSampleType.setCreateDate(new Date());*/
			dicSampleType.setId("NEW");
			dicSampleType.setState(SystemConstants.DIC_STATE_NEW);
			dicSampleType.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			dicSampleType.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			dicSampleType.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEdit.jsp");
	}
/*	@Action(value = "dicSampleTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDicSampleTypeList() throws Exception {
		String a = getParameterFromRequest("a");
		putObjToContext("a", a);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeDialog.jsp");
	}*/

	@Action(value = "showDialogDicSampleTypeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDicSampleTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dicSampleTypeService
				.findDicSampleTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DicSampleType> list = (List<DicSampleType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("note", "");
		map.put("orderNumber", "");
		map.put("type", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

/*	@Action(value = "editDicSampleType")
	public String editDicSampleType() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dicSampleType = dicSampleTypeService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dicSampleType");
		} else {
			dicSampleType.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// dicSampleType.setCreateUser(user);
			// dicSampleType.setCreateDate(new Date());
			dicSampleType.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			dicSampleType.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEdit.jsp");
	}*/

/*	@Action(value = "copyDicSampleType")
	public String copyDicSampleType() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dicSampleType = dicSampleTypeService.get(id);
		dicSampleType.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEdit.jsp");
	}*/

/*	@Action(value = "save")
	public String save() throws Exception {
		String id = dicSampleType.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "DicSampleType";
			String markCode = "C";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			dicSampleType.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("dicSampleTypeItem",
				getParameterFromRequest("dicSampleTypeItemJson"));

		dicSampleTypeService.save(dicSampleType, aMap);
		return redirect("/sample/dicSampleType/editDicSampleType.action?id="
				+ dicSampleType.getId());

	}*/

/*	@Action(value = "viewDicSampleType")
	public String toViewDicSampleType() throws Exception {
		String id = getParameterFromRequest("id");
		dicSampleType = dicSampleTypeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeEdit.jsp");
	}*/

	@Action(value = "showDicSampleTypeItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWorkOrderItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/dicSampleTypeItem.jsp");
	}

	@Action(value = "showDicSampleTypeItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWorkOrderItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dicSampleTypeService
					.findDicSampleTypeItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WorkOrderItem> list = (List<WorkOrderItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("dicSampleType-name", "");
			map.put("dicSampleType-id", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("dnextId", "");
			map.put("dnextName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDicSampleTypeItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delWorkOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dicSampleTypeService.delDicSampleTypeItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "dicSampleTypeCobJson")
	public String dicTypeCobJson() throws Exception {

		String outStr = "{results:" + dicSampleTypeService.getDicTypeJson()
				+ "}";

		return renderText(outStr);
	}
	// 根据类型名称查询
	@Action(value = "setDicSampleTypeByname", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setDicSampleTypeByname() throws Exception {
		String sampleType = getRequest().getParameter("sampleType");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			DicSampleType type = this.dicSampleTypeService
					.selectDicSampleTypeByName(sampleType);
			result.put("success", true);
			result.put("data", type);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DicSampleTypeService getDicSampleTypeService() {
		return dicSampleTypeService;
	}

	public void setDicSampleTypeService(
			DicSampleTypeService dicSampleTypeService) {
		this.dicSampleTypeService = dicSampleTypeService;
	}

	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

}
