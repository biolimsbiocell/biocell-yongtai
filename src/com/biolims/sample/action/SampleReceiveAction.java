﻿package com.biolims.sample.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleSystemItem;
import com.biolims.sample.service.DicSampleTypeService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.system.code.SystemConstants;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.product.model.Product;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
import com.opensymphony.xwork2.ActionContext;
import com.sun.jersey.core.util.Base64;

@Namespace("/sample/sampleReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2116";
	@Autowired
	private SampleSearchService sampleSearchService;
	@Autowired
	private SampleReceiveService sampleReceiveService;
	private SampleReceive sampleReceive = new SampleReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private DicSampleTypeService dicSampleTypeService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	@Resource
	private CellPassageDao cellPassageDao;

	@Action(value = "showSampleReceiveTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReceiveList() throws Exception {
		rightsId = "2116";
		String type = getParameterFromRequest("type");// 判断是什么类型样本
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleReceive.jsp");
	}

	@Action(value = "showSampleReceiveTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReceiveTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleReceiveService.showSampleReceiveTableJson(start, length, query, col,
					sort);
			List<SampleReceive> list = (List<SampleReceive>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("expreceCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleInfoMain-id", "");
			map.put("sampleInfoMain-expressCode", "");
			map.put("project-id", "");
			map.put("project-name", "");
			map.put("classify", "");
			map.put("acceptUser-id", "");
			map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
			map.put("type", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			map.put("expressNum", "");
			map.put("userGroup-id", "");
			map.put("userGroup-name", "");

			map.put("ccoi", "");
			map.put("barcode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "sampleReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleReceiveDialog.jsp");
	}

	@Action(value = "showDialogSampleReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleReceiveService.findSampleReceiveList(map2Query, startNum, limitNum, dir,
				sort, null);
		Long count = (Long) result.get("total");
		List<SampleReceive> list = (List<SampleReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("name", "");
		map.put("businessType-id", "");
		map.put("businessType-name", "");
		map.put("expreceCode", "");
		// map.put("expreceCode-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("sampleInfoMain-id", "");
		map.put("type", "");
		map.put("userGroup-id", "");
		map.put("userGroup-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editSampleReceive")
	public String editSampleReceive() throws Exception {
		rightsId = "2115";
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		String proId = getParameterFromRequest("id");
		String crmId = getParameterFromRequest("crmId");
		String mainId = getParameterFromRequest("mainId");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");

		String barCod = getParameterFromRequest("barCod");

		putObjToContext("id", id);
		putObjToContext("bpmTaskId", bpmTaskId);
		long num = 0;
		try {
			if (id != null && !id.equals("")) {
				sampleSearchService.insertLog(barCod, "样本接收记录");
				sampleReceive = sampleReceiveService.get(id);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "sampleReceive");
				String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
				putObjToContext("taskName", tName);
				// putObjToContext("modelName", "SampleReceive");
			} else {
				sampleSearchService.insertLog(barCod, "样本接收记录");
				sampleReceive.setId("NEW");
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				sampleReceive.setAcceptUser(user);
				sampleReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sampleReceive.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				sampleReceive.setAcceptDate(new Date());
				sampleReceive.setAcceptDate(new Date());
				sampleReceive.setCreateDate(new Date());

				Project p = commonDAO.get(Project.class, proId);
				sampleReceive.setProject(p);
				CrmCustomer cc = commonDAO.get(CrmCustomer.class, crmId);
				sampleReceive.setCrmCustomer(cc);
				CrmDoctor cd = commonDAO.get(CrmDoctor.class, mainId);
				sampleReceive.setCrmDoctor(cd);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if ("".equals(type)) {
			putObjToContext("type", sampleReceive.getType());
		} else {
			putObjToContext("type", type);
		}

		// toState(sampleReceive.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/sampleReceiveEdit.jsp");
	}

	@Action(value = "editSampleReceiveNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editSampleReceiveNew() throws Exception {
		HttpServletRequest req = ServletActionContext.getRequest();

		ActionContext ctx = ActionContext.getContext();

		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String format2 = format.format(date);
		String st = getParameterFromRequest("st");
		byte[] Bty = Base64.decode(st);
		String ste = new String(Bty, "utf-8");

		if (format2.equals(ste)) {

			String BaseId = getParameterFromRequest("id");
			byte[] asBytes = Base64.decode(BaseId);
			String id = new String(asBytes, "utf-8");
			String type = getParameterFromRequest("type");
			String proId = getParameterFromRequest("id");
			String crmId = getParameterFromRequest("crmId");
			String mainId = getParameterFromRequest("mainId");
			String bpmTaskId = getParameterFromRequest("bpmTaskId");

			String barCod = getParameterFromRequest("barCod");

			putObjToContext("id", id);
			putObjToContext("bpmTaskId", bpmTaskId);
			long num = 0;
			try {
				if (id != null && !id.equals("")) {
					sampleSearchService.insertLog(barCod, "样本接收记录");
					sampleReceive = sampleReceiveService.get(id);
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					num = fileInfoService.findFileInfoCount(id, "sampleReceive");
					String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
					putObjToContext("taskName", tName);
					// putObjToContext("modelName", "SampleReceive");
				} else {
					sampleSearchService.insertLog(barCod, "样本接收记录");
					sampleReceive.setId("NEW");
					User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
					sampleReceive.setAcceptUser(user);
					sampleReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
					sampleReceive.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
					sampleReceive.setAcceptDate(new Date());
					sampleReceive.setAcceptDate(new Date());
					sampleReceive.setCreateDate(new Date());

					Project p = commonDAO.get(Project.class, proId);
					sampleReceive.setProject(p);
					CrmCustomer cc = commonDAO.get(CrmCustomer.class, crmId);
					sampleReceive.setCrmCustomer(cc);
					CrmDoctor cd = commonDAO.get(CrmDoctor.class, mainId);
					sampleReceive.setCrmDoctor(cd);
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if ("".equals(type)) {
				putObjToContext("type", sampleReceive.getType());
			} else {
				putObjToContext("type", type);
			}

			// toState(sampleReceive.getState());
			putObjToContext("fileNum", num);
			return dispatcher("/WEB-INF/page/sample/sampleReceiveEdit.jsp");
		} else {
			HashMap<String, String> map = new HashMap();
			map.put("message", "您没有权限,请联系管理员!");
			ctx.put("messageMap", map);
			req.setAttribute("message", "您没有权限,请联系管理员!");
			return "global.checkrights.exception";
		}
	}

	@Action(value = "viewSampleReceive")
	public String toViewSampleReceive() throws Exception {
		String id = getParameterFromRequest("id");
		sampleReceive = sampleReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/sampleReceiveEdit.jsp");
	}

	@Action(value = "showSampleReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/sampleReceiveItem.jsp");
	}

	@Action(value = "showSampleReceiveItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReceiveItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleReceiveService.showSampleReceiveItemListJson(scId, start, length, query,
					col, sort);
			List<SampleReceiveItem> list = (List<SampleReceiveItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			SampleReceiveItem sampleReceiveItem = new SampleReceiveItem();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(sampleReceiveItem);
			map.put("unusual-id", "");
			map.put("unusual-name", "");
			map.put("acceptDate", "yyyy-MM-dd HH:mm");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("samplingDate", "yyyy-MM-dd HH:mm");
			map.put("dicType-id", "");
			map.put("dicType-name", "");
			map.put("personShip-id", "");
			map.put("abbreviation", "");
			map.put("personShip-name", "");
			map.put("familyId-id", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("bloodTime", "yyyy-MM-dd");
			map.put("bloodVolume", "");

			map.put("receiveState", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "sampleItemSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String sampleItemSelect() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleItemDialog.jsp");
	}

	/** 样本明细条件查询 */
	@Action(value = "showDialogSampleItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleItemListJson() throws Exception {
		int startNum = 0;
		int limitNum = 1000;
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleReceiveService.findSampleItemList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleReceiveItem> list = (List<SampleReceiveItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("name", "");
		map.put("source", "");
		map.put("condition", "");
		map.put("isGood", "");
		map.put("unusual-id", "");
		map.put("unusual-name", "");
		map.put("location", "");
		// map.put("expreceCode-name", "");
		map.put("method", "");
		map.put("advice", "");
		map.put("isFull", "");
		map.put("note", "");
		map.put("sequenceFun", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("phone", "");
		map.put("idCard", "");
		map.put("reportDate", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");

		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("orderCode", "");
		map.put("analysisType", "");
		map.put("isRp", "");
		map.put("gender", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleReceiveItem")
	public void delSampleReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleReceiveService.delSampleReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 打包体系
	 */
	@Action(value = "showSampleSystemItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleSystemItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/sampleSystemItem.jsp");
	}

	@Action(value = "showsampleSystemItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showsampleSystemItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleReceiveService.findSampleSystemItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleSystemItem> list = (List<SampleSystemItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("num", "");
			// map.put("temperature", "");
			map.put("sampleReceive-name", "");
			map.put("sampleReceive-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除体系信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delsampleSystemItem")
	public void delsampleSystemItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleReceiveService.delSampleSystemItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 开箱检验是否合格
	@Action(value = "showSampleReceiveItemListByIsGood", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReceiveItemListIsGood() throws Exception {
		return dispatcher("/WEB-INF/page/sample/sampleReceiveItem1.jsp");
	}

	@Action(value = "showSampleReceiveItemByIsGoodJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReceiveItemListJsonIsGood() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		Map<String, String> map2Query = new HashMap<String, String>();
		map2Query.put("method", "1");
		map2Query.put("isFull", "1");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleReceiveService.findSampleReceiveItemListByIsgood(map2Query, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleReceiveItem> list = (List<SampleReceiveItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("source", "");
			map.put("checked", "");
			map.put("condition", "");
			map.put("isGood", "");
			map.put("unusual-name", "");
			map.put("unusual-id", "");
			map.put("location", "");
			map.put("method", "");
			map.put("advice", "");
			map.put("isFull", "");
			map.put("note", "");
			map.put("sampleReceive-name", "");
			map.put("sampleReceive-id", "");
			map.put("sequenceFun", "");
			map.put("patientName", "");
			map.put("sampleType-id", "");
			map.put("sampleType-name", "");
			map.put("binLocation", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("orderCode", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("idCard", "");
			map.put("reportDate", "");
			map.put("inspectDate", "");

			map.put("classify", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 去到样本处理的样本
	@Action(value = "showSampleReceiveItemListToBlood", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReceiveItemListToBlood() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/sample/sampleReceiveItemToBlood.jsp");
	}

	// @Action(value = "showSampleReceiveItemListToBloodJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleReceiveItemListToBloodJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// Map<String, Object> result = sampleReceiveService
	// .selectPlasmaReceiveItemList(startNum, limitNum, dir, sort);
	// Long total = (Long) result.get("total");
	// List<PlasmaReceiveTemp> list = (List<PlasmaReceiveTemp>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("sampleCode", "");
	// map.put("patientName", "");
	// map.put("name", "");
	// map.put("state", "");
	// map.put("sampleType", "");
	// map.put("productId", "");
	// map.put("productName", "");
	//
	// map.put("inspectDate", "");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("volume", "");
	// map.put("unit", "");
	// map.put("state", "");
	// map.put("note", "");
	// map.put("concentration", "");
	//
	// map.put("sampleInfo-id", "");
	// map.put("sampleInfo-idCard", "");
	// map.put("orderId", "");
	// map.put("orderCode", "");
	// map.put("result", "");
	// map.put("reason", "");
	// map.put("idCard", "");
	// map.put("sequencingFun", "");
	// map.put("reportDate", "yyyy-MM-dd");
	// map.put("orderId", "");
	// map.put("phone", "");
	// map.put("classify", "");
	// map.put("sampleNum", "");
	// map.put("labCode", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	// 去到核酸提取的样本
	@Action(value = "showSampleReceiveItemListToDNA", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReceiveItemListToDNA() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/sample/sampleReceiveItemToDNA.jsp");
	}

	// @Action(value = "showSampleReceiveItemListToDNAJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleReceiveItemListToDNAJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// Map<String, Object> result = sampleReceiveService
	// .selectDnaReceiveTempList(startNum, limitNum, dir, sort);
	// Long total = (Long) result.get("total");
	// List<DnaReceiveTemp> list = (List<DnaReceiveTemp>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("sampleCode", "");
	// map.put("patientName", "");
	// map.put("state", "");
	// map.put("productName", "");
	// map.put("productId", "");
	// map.put("inspectDate", "");
	// map.put("sampleType", "");
	// map.put("acceptDate", "");
	// map.put("idCard", "");
	// map.put("reportDate", "");
	// map.put("phone", "");
	// map.put("orderId", "");
	// map.put("sequenceFun", "");
	// map.put("classify", "");
	// map.put("sampleNum", "");
	// map.put("labCode", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 查询上传文件信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "uploada", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploada() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String sampleType = getParameterFromRequest("sampleType");
			DicSampleType dic = this.dicSampleTypeService.selectDicSampleTypeByName(sampleType);
			map.put("success", true);
			map.put("sampleType", dic);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 打印
	// @Action(value = "makeCode", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void makeCode() throws Exception {
	// String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
	// String id = getParameterFromRequest("id");// 打印机id
	// CodeMain c = this.codeMainService.get(id);
	// String printStr = "";
	// String context = dy(sampleCode, c);
	// printStr = context;
	// Socket socket = null;
	// OutputStream os;
	// try {
	// socket = new Socket();
	// // SocketAddress sa = new InetSocketAddress("10.21.9.172", 9100);
	// // SocketAddress sa = new InetSocketAddress("10.21.9.222", 9100);
	// SocketAddress sa = new InetSocketAddress(c.getIp(), 9100);
	// socket.connect(sa);
	// os = socket.getOutputStream();
	// os.write(printStr.getBytes("GBK"));
	// os.flush();
	// } catch (UnknownHostException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// } finally {
	// if (socket != null) {
	// try {
	// socket.close();
	// } catch (IOException e) {
	// }
	// }
	// }
	// }
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String id = getParameterFromRequest("id");
		String name = getParameterFromRequest("name");
		String printCode = getParameterFromRequest("printCode");

		CodeMain codeMain = null;
		codeMain = codeMainService.get(printCode);
		if (codeMain != null) {
			String printStr = "";
			String codeFull = id;
			printStr = codeMain.getCode();
			String code1 = id.substring(0, 9);
			String code2 = id.substring(9);
			printStr = printStr.replaceAll("@@code1@@", code1);
			printStr = printStr.replaceAll("@@code2@@", code2);
			printStr = printStr.replaceAll("@@code@@", codeFull);
			printStr = printStr.replaceAll("@@name@@", name);
			String ip = codeMain.getIp();
			Socket socket = null;
			OutputStream os;
			try {
				System.out.println(printStr);
				socket = new Socket();
				SocketAddress sa = new InetSocketAddress(ip, 9100);
				socket.connect(sa);
				os = socket.getOutputStream();
				os.write(printStr.getBytes("UTF-8"));
				os.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	public String dy(String[] code, CodeMain c) {
		int length = code.length;
		int n = 0;
		if (length % 3 > 0) {
			n = length / 3 + 1;
		} else {
			n = length / 3;
		}
		String context = "";
		int a = 0;
		for (int i = 0; i < n; i++) {
			String str = "";
			if (length > a) {
				str += "#" + code[a] + "&" + code[a].substring(0, 8) + "&" + code[a].substring(8, code[a].length());
				a++;
				if (length > a) {
					str += "&" + code[a] + "&" + code[a].substring(0, 8) + "&" + code[a].substring(8, code[a].length());
					a++;
					if (length > a) {
						str += "&" + code[a] + "&" + code[a].substring(0, 8) + "&"
								+ code[a].substring(8, code[a].length());
						a++;
					} else {
						str += "&&&";
					}
				} else {
					str += "&&&&&&";
				}
			}

			String dyCode = c.getCode();
			String d = dyCode.replaceAll("~~~AAA~~~", str);
			context += d;
		}

		return context;
	}

	public static void main(String[] args) throws Exception {
		String[] code = { "WB123456723", "WB123456543", "WB12345676543" };
		int length = code.length;
		int n = 0;
		if (length % 3 > 0) {
			n = length / 3 + 1;
		} else {
			n = length / 3;
		}
		String context = "";
		int a = 0;
		for (int i = 0; i < n; i++) {
			context += "INPUT ON\r\n";
			context += "CLL\r\n";
			context += "FORMAT INPUT \"#\",\"@\",\"&\"\r\n";
			context += "LAYOUT INPUT \"tmp:label1\"\r\n";

			context += "PP 160,90:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 6\r\n";
			context += "PT \"NextCODE Sample\"\r\n";
			context += "PP 30,100:AN7\r\n";
			context += "BARSET \"QRCODE\",1,1,4,2,1\r\n";
			context += "PB VAR1$\r\n";
			context += "PP 160,80:NASC 6\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR2$\r\n";
			context += "PP 160,50:NASC 6\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR3$\r\n";

			context += "PP 560,120:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 6\r\n";
			context += "PT \"NextCODE Sample\"\r\n";
			context += "PP 440,100:AN7\r\n";
			context += "BARSET \"QRCODE\",1,1,4,2,1\r\n";
			context += "PB VAR4$\r\n";
			context += "PP 560,80:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR5$\r\n";
			context += "PP 560,50:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR6$\r\n";

			context += "PP 970,120:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 6\r\n";
			context += "PT \"NextCODE Sample\"\r\n";
			context += "PP 850,100:AN7\r\n";
			context += "BARSET \"QRCODE\",1,1,4,2,1\r\n";
			context += "PB VAR7$\r\n";
			context += "PP 970,80:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR8$\r\n";
			context += "PP 970,50:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR9$\r\n";

			context += "LAYOUT END$\r\n";
			context += "SETUP \"MEDIA,MEDIA SIZE,XSTART,0\"\r\n";
			context += "SETUP 'Printing,Media,Print Area,Media Width,1505\"\r\n";
			context += "SETUP \"Printing,Media,Print Area,Media Length,140\"\r\n";
			context += "LAYOUT RUN \"tmp:label1\"\r\n";
			if (length > a) {
				context += "#" + code[a] + "&" + code[a].substring(0, 8) + "&" + code[a].substring(8, code[a].length());
				a++;
				if (length > a) {
					context += "&" + code[a] + "&" + code[a].substring(0, 8) + "&"
							+ code[a].substring(8, code[a].length());
					a++;
					if (length > a) {
						context += "&" + code[a] + "&" + code[a].substring(0, 8) + "&"
								+ code[a].substring(8, code[a].length());
						a++;
					} else {
						context += "&&&";
					}
				} else {
					context += "&&&&&&";
				}
			}
			context += "&@\r\n";
			context += "PF\r\n";
			context += "KILL \"tmp:label1\"\r\n";
			context += "INPUT OFF\r\n";
		}
		Socket socket = null;
		OutputStream os;
		try {
			socket = new Socket();
			// SocketAddress sa = new InetSocketAddress("10.21.9.172", 9100);
			SocketAddress sa = new InetSocketAddress("10.21.9.222", 9100);
			socket.connect(sa);
			os = socket.getOutputStream();
			os.write(context.getBytes("GBK"));
			os.flush();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	// 生成分组号
	@Action(value = "setfenzu", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setfenzu() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			this.sampleReceiveService.fenzu(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleReceiveService getSampleReceiveService() {
		return sampleReceiveService;
	}

	public void setSampleReceiveService(SampleReceiveService sampleReceiveService) {
		this.sampleReceiveService = sampleReceiveService;
	}

	public SampleReceive getSampleReceive() {
		return sampleReceive;
	}

	public void setSampleReceive(SampleReceive sampleReceive) {
		this.sampleReceive = sampleReceive;
	}

	/**
	 * 查询医生内关联客户单位
	 * 
	 * @throws Exception
	 */
	@Action(value = "selDoctor", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selDoctor() throws Exception {
		String gid = getParameterFromRequest("dockerId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String crmId = sampleReceiveService.selDoctor(gid);

			map.put("success", false);
			map.put("crmId", crmId);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 验证并加载条形码信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "checkSampleCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void checkSampleCode() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<SampleOrderItem> list = this.sampleReceiveService.getSampleOrderItemList(code);
			if (list.size() > 0) {
				List<Map<String, String>> dataListMap = this.sampleReceiveService.getSampleOrderItemMapList(code);
				map.put("success", true);
				map.put("data", dataListMap);
			} else {
				map.put("success", false);
				map.put("str", code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: scanCodeInsertpanduan @Description: 扫码录入 前判断条码是否重复 @author : @date
	 *         2 @throws Exception void @throws
	 */
	@Action(value = "scanCodeInsertpanduan")
	public void scanCodeInsertpanduan() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		String sampleCode = getParameterFromRequest("sampleCode");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 查询样本接收子表是否有该批次
			List<SampleReceiveItem> srisPici = commonService.get(SampleReceiveItem.class, "batch", sampleCode);
			// 查询样本接收子表是否有该条码号
			List<SampleReceiveItem> sris = commonService.get(SampleReceiveItem.class, "barCode", sampleCode);
			if (sris.size() > 0) {
				SampleReceiveItem sri = sris.get(0);
				if (sri != null) {
					if (id != null && !"".equals(id) && !"NEW".equals(id)) {
						SampleReceive sr = commonDAO.get(SampleReceive.class, id);
						if ((sri.getOrderCode()).equals(sr.getSampleOrder())) {
							if ("1".equals(sri.getReceiveState())) {
								map.put("jszt", true);// 样本已接收
							} else {
								sampleReceiveService.saveSamplereceiveItemBycan(sri);
								map.put("jszt", false);// 接收成功
							}
							map.put("state", true);
							map.put("sfdqdd", true);
						} else {
							map.put("state", true);
							map.put("sfdqdd", false);
						}
					} else {
						map.put("state", true);
						map.put("sfdqdd", false);
					}
				} else {
					map.put("state", true);
				}
			} else if (srisPici.size() > 0) {
				SampleReceiveItem sri = srisPici.get(0);
				if (sri != null) {
					if (id != null && !"".equals(id) && !"NEW".equals(id)) {
						SampleReceive sr = commonDAO.get(SampleReceive.class, id);
						if ((sri.getOrderCode()).equals(sr.getSampleOrder())) {
							if ("1".equals(sri.getReceiveState())) {
								map.put("jszt", true);// 样本已接收
							} else {
								map.put("jszt", false);// 接收成功
							}
							map.put("state", true);
							map.put("sfdqdd", true);
						} else {
							map.put("state", true);
							map.put("sfdqdd", false);
						}
					} else {
						map.put("state", true);
						map.put("sfdqdd", false);
					}
				} else {
					map.put("state", true);
				}
			} else {
				if (id != null && !"".equals(id) && !"NEW".equals(id)) {
					SampleReceive sr = commonDAO.get(SampleReceive.class, id);
					if (sr.getSampleOrder() != null && !"".equals(sr.getSampleOrder())) {
						map.put("state", false);
						map.put("sfdqdd", false);
					} else {
						map.put("state", false);
						map.put("sfdqdd", true);
					}
				} else {
					map.put("state", false);
					map.put("sfdqdd", true);
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: scanCodeInsert @Description: 扫码录入 @author : shengwei.wang @date
	 *         2018年1月31日上午9:35:23 @throws Exception void @throws
	 */
	@Action(value = "scanCodeInsert")
	public void scanCodeInsert() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		String sampleCode = getParameterFromRequest("sampleCode");

		String sj = getParameterFromRequest("sj");
		Map<String, String> map = new HashMap<String, String>();
		try {
			map = sampleReceiveService.scanCodeInsert(id, type, sampleCode, sj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: uploadCsvFile @Description: 上传csv @author : shengwei.wang @date
	 *         2018年2月6日下午2:54:53 @throws Exception void @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = sampleReceiveService.getCsvContent(id, type, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveSampleReceive @Description: 保存 @author : shengwei.wang @date
	 *         2018年2月1日下午2:49:59 @throws Exception void @throws
	 */
	@Action(value = "saveSampleReceive")
	public void saveSampleReceive() throws Exception {
		String ids = getParameterFromRequest("ids");
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String main = getParameterFromRequest("main");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = sampleReceiveService.saveSampleReceive(main, itemJson, user, ids, changeLog, changeLogItem);
			map.put("id", id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "changeSampleOrderReceive")
	public void changeSampleOrderReceive() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("zid");
		try {
			sampleReceiveService.changeSampleOrderReceive(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveProductionUser")
	public void saveProductionUser() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String userId = getParameterFromRequest("userId");
		try {
			sampleReceiveService.saveProductionUser(id, userId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "changeSampleOrderState")
	public void changeSampleOrderState() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			sampleReceiveService.changeSampleOrderState(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 明细下载CSV模板文件 @Title: downloadCsvFile
	 * 
	 * @throws
	 */
	@Action(value = "downloadCsvFileTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadCsvFileTwo() throws Exception {
		String mainTable_id = getParameterFromRequest("id");
		String sampleId = getParameterFromRequest("sampleId");

		Map<String, Object> result = new HashMap<String, Object>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");

		properties.load(is);

		String filePath = properties.getProperty("sampleReceiveItemZhiJian.path") + "\\";// 写入csv路径
		String fileName = filePath + "sampleReceiveItemZhiJian" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "质检条码" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();

		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, sampleId);
		String codes = "";
		if (sampleOrder != null) {
			String productId = sampleOrder.getProductId();
			if (!"".equals(productId) && productId != null) {
				Product product = commonDAO.get(Product.class, productId);
				if (product != null) {
					if (product.getTemplate() != null) {
						Template template = commonDAO.get(Template.class, product.getTemplate().getId());
						Double stepsNum = template.getStepsNum();
						DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
						String format = decimalFormat.format(stepsNum);
						int stepsN = Integer.parseInt(format);

						for (int i = 1; i <= stepsN; i++) {
							List<TemplateItem> tpi = cellPassageDao.getTemplateItems(template, String.valueOf(i));

							String printLabelCoding = tpi.get(0).getPrintLabelCoding();
							List<TempleProducingCell> templeProductingCell = cellPassageDao
									.getTempleProductingCell(template.getId(), String.valueOf(i));

							if (templeProductingCell != null) {
								String s = "";
								for (TempleProducingCell templeProducingCell : templeProductingCell) {
									if ("".equals(s)) {
										s += "'" + templeProducingCell.getId() + "'";
									} else {
										s += "," + "'" + templeProducingCell.getId() + "'";
									}

								}
								String aa = "in (" + s + ")";
								List<ZhiJianItem> findCellZhiJianAll = cellPassageDao.findCellZhiJianAll(aa,
										String.valueOf(i));
								for (ZhiJianItem zhiJianItem : findCellZhiJianAll) {
									List<ZhiJianItem> zhiJian = cellPassageDao.findZhiJianNewById(aa,
											zhiJianItem.getCode(), String.valueOf(i));
									for (int j = 1; j <= zhiJian.size(); j++) {

										SampleDeteyion sd = commonDAO.get(SampleDeteyion.class,
												zhiJian.get(j - 1).getCode());
										DicSampleType dd = commonDAO.get(DicSampleType.class,
												zhiJian.get(j - 1).getSampleTypeId());

										// 质检条形码
										// 姓名缩写-筛选好-监测项目-随机号-采血轮次-生产工序-监测项缩写
										codes = sampleOrder.getAbbreviation() + "-" + sampleOrder.getFiltrateCode()
												+ "-" + sampleOrder.getProductId() + "-" + sampleOrder.getRandomCode()
												+ "-" + sampleOrder.getRound() + "-" + printLabelCoding + "-"
												+ sd.getDeteyionName() + "-" + j;
										StringBuffer sb = new StringBuffer();
										setMolecularMarkersData(codes, sb);
										String rowStr = sb.toString();
										csvWtriter.write(rowStr);
										csvWtriter.newLine();
										result.put("success", true);
									}
								}
							}
						}

					}
				}

			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		downLoadTemp3("sampleReceiveItemZhiJian", filePath);
	}

	/**
	 * 明细下载CSV模板文件 @Title: downloadCsvFile
	 * 
	 * @throws
	 */
	@Action(value = "downloadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadCsvFile() throws Exception {
		String mainTable_id = getParameterFromRequest("id");
		String sampleId = getParameterFromRequest("sampleId");

		Map<String, Object> result = new HashMap<String, Object>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");

		properties.load(is);

		String filePath = properties.getProperty("sampleReceiveItem.path") + "\\";// 写入csv路径
		String fileName = filePath + "sampleReceiveItem" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "生产条码"};
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();

		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, sampleId);
		String codes = "";

		if (sampleOrder != null) {
			String productId = sampleOrder.getProductId();
			if (!"".equals(productId) && productId != null) {
				Product product = commonDAO.get(Product.class, productId);
				if (product != null) {
					if (product.getTemplate() != null) {
						Template template = commonDAO.get(Template.class, product.getTemplate().getId());
						Double stepsNum = template.getStepsNum();
						DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
						String format = decimalFormat.format(stepsNum);
						int stepsN = Integer.parseInt(format);
						for (int i = 1; i <= stepsN; i++) {

							List<TemplateItem> tpi = cellPassageDao.getTemplateItems(template, String.valueOf(i));

							String printLabelCoding = tpi.get(0).getPrintLabelCoding();
//									if (sos.size() > 0 && tis.size() > 0) {
							codes = sampleOrder.getAbbreviation() + "-" + sampleOrder.getFiltrateCode() + "-"
									+ sampleOrder.getProductId() + "-" + sampleOrder.getRandomCode() + "-"
									+ sampleOrder.getRound() + "-" + printLabelCoding;

							StringBuffer sb = new StringBuffer();
							setMolecularMarkersData(codes, sb);
							String rowStr = sb.toString();
							csvWtriter.write(rowStr);
							csvWtriter.newLine();
							result.put("success", true);

						}
					}

				}
			}
		}
//				// 质检条形码
//				// 姓名缩写-筛选好-监测项目-随机号-采血轮次-生产工序-监测项缩写
//				if (sos.size() > 0 && tis.size() > 0) {
//					productCodes = sos.get(0).getAbbreviation() + "-" + sos.get(0).getFiltrateCode()
//							+ "-" + sos.get(0).getProductId() + "-" + sos.get(0).getRandomCode() + "-"
//							+ sos.get(0).getRound() + "-" + printLabelCoding + "-"
//							+ sd.getDeteyionName() + "-" + i;
//				}
//				for (int i = 0; i < list.size(); i++) {
//					StringBuffer sb = new StringBuffer();
//					setMolecularMarkersData(list.get(i), sb);
//					String rowStr = sb.toString();
//					csvWtriter.write(rowStr);
//					csvWtriter.newLine();
//					result.put("success", true);
//
//			}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3("sampleReceiveItem", filePath);
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData
	 * @Description: 数据添加到csv内
	 * @author qi.yan
	 * @date 2018-12-24上午11:53:44
	 * 
	 */
	public void setMolecularMarkersData(String codes, StringBuffer sb) throws Exception {
		if (!"".equals(codes) && codes != null) {
			sb.append("\"").append(codes).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}

	}

	/**
	 * 
	 * @Title: downLoadTemp3 @Description: 下载文件 @author qi.yan @date
	 *         2018-12-24上午11:55:26 @param a @param filePath2 @throws Exception
	 *         void @throws
	 */
	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: toUtf8String @Description: 解决乱码问题 @author qi.yan @date
	 *         2018-12-24上午11:55:11 @param s @return String @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}

}
