﻿
package com.biolims.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTechnology;
import com.biolims.sample.service.SampleInputTechnologyService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/sample/sampleInputTechnology")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleInputTechnologyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2119";
	@Autowired
	private SampleInputTechnologyService sampleInputTechnologyService;
	@Autowired
	private CommonService commonService;
	private SampleInputTechnology sampleInputTechnology = new SampleInputTechnology();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showSampleInputTechnologyList")
	public String showSampleInputTechnologyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleInputTechnology.jsp");
	}

	@Action(value = "showSampleInputTechnologyListJson")
	public void showSampleInputTechnologyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)	
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleInputTechnologyService.findSampleInputTechnologyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("name", "");
		map.put("patientName", "");
		map.put("idCard", "");
		map.put("sampleType", "");
		map.put("businessType", "");
		map.put("hospital", "");
		map.put("price", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("createUser1", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("upLoadAccessory-id", "");
		map.put("upLoadAccessory-fileName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "sampleInputTechnologySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleInputTechnologyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/sampleInputTechnologyDialog.jsp");
	}

	@Action(value = "showDialogSampleInputTechnologyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleInputTechnologyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleInputTechnologyService.findSampleInputTechnologyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleInputTechnology> list = (List<SampleInputTechnology>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sendDate", "");
		map.put("contractNum", "");
		map.put("projectName", "");
		map.put("clientName", "");
		map.put("clientUnit", "");
		map.put("sampleState", "");
		map.put("species", "");
		map.put("organType", "");
		map.put("sampleName", "");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("sampleCode", "");
		map.put("openBoxUser", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("note", "");
		map.put("isQualified", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editSampleInputTechnology")
	public String editSampleInputTechnology() throws Exception {
		String id = getParameterFromRequest("id");
		String code = getParameterFromRequest("code");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		if (id != null && !id.equals("")) {
			
			List<SampleInputTechnology> l  = commonService.get(SampleInputTechnology.class, "sampleCode", code);
			if(l.size()>0){
				sampleInputTechnology = l.get(0);
				sampleInputTechnology.setCreateUser(user);
				sampleInputTechnology.setCreateDate(new Date());
			}
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			
		}else{
			
			//SampleInfo list = sampleInputTechnologyService.findSampleInfoByCode(code);
			//sampleInputTechnology.setSampleInfo(list);
			sampleInputTechnology.setCreateUser(user);
			sampleInputTechnology.setCreateDate(new Date());
			sampleInputTechnology.setSendDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			
			
			
		}
		
		
		
		return dispatcher("/WEB-INF/page/sample/sampleInputTechnologyEdit.jsp");
	}

	@Action(value = "copySampleInputTechnology")
	public String copySampleInputTechnology() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleInputTechnology = sampleInputTechnologyService.get(id);
		sampleInputTechnology.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/sampleInputTechnologyEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleInputTechnology.getId();
		if(id!=null&&id.equals("")){
			sampleInputTechnology.setId(null);
			String modelName = "SampleInputTechnology";
			String markCode=sampleInputTechnology.getSampleType().getId();
			String autoSampleCode = sampleInputTechnologyService.genTransID(modelName,markCode);
			sampleInputTechnology.setSampleCode(autoSampleCode);
		}
		Map aMap = new HashMap();
		sampleInputTechnologyService.save(sampleInputTechnology,aMap);
		return redirect("/sample/sampleInputTechnology/showSampleInputTechnologyList.action");
	}

	@Action(value = "viewSampleInputTechnology")
	public String toViewSampleInputTechnology() throws Exception {
		String id = getParameterFromRequest("id");
		sampleInputTechnology = sampleInputTechnologyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/sampleInputTechnologyEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleInputTechnologyService getSampleInputTechnologyService() {
		return sampleInputTechnologyService;
	}

	public void setSampleInputTechnologyService(SampleInputTechnologyService sampleInputTechnologyService) {
		this.sampleInputTechnologyService = sampleInputTechnologyService;
	}

	public SampleInputTechnology getSampleInputTechnology() {
		return sampleInputTechnology;
	}

	public void setSampleInputTechnology(SampleInputTechnology sampleInputTechnology) {
		this.sampleInputTechnology = sampleInputTechnology;
	}


}
