package com.biolims.sample.encod.model;

import java.lang.Integer;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 订单编码打印明细
 * @author lims-platform
 * @date 2016-03-15 11:18:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "ORDER_ENCOD_DETAILS")
@SuppressWarnings("serial")
public class OrderEncodDetails extends EntityDao<OrderEncodDetails> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 订单编码明细 */
	private String codeDetails;
	/** 相关主表 */
	private OrderEncod orderEncod;
	/** 打印数量 */
	private Integer num;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 订单编码明细
	 */
	@Column(name = "CODE_DETAILS", length = 50)
	public String getCodeDetails() {
		return this.codeDetails;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 订单编码明细
	 */
	public void setCodeDetails(String codeDetails) {
		this.codeDetails = codeDetails;
	}

	/**
	 * 方法: 取得OrderEncod
	 * 
	 * @return: OrderEncod 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ORDER_ENCOD")
	public OrderEncod getOrderEncod() {
		return this.orderEncod;
	}

	/**
	 * 方法: 设置OrderEncod
	 * 
	 * @param: OrderEncod 相关主表
	 */
	public void setOrderEncod(OrderEncod orderEncod) {
		this.orderEncod = orderEncod;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 打印数量
	 */
	@Column(name = "NUM", length = 50)
	public Integer getNum() {
		return this.num;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer 打印数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}
}