package com.biolims.sample.encod.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.encod.model.OrderEncod;
import com.biolims.sample.encod.model.OrderEncodDetails;
import com.biolims.sample.encod.model.OrderEncodItem;

@Repository
@SuppressWarnings("unchecked")
public class OrderEncodDao extends BaseHibernateDao {
	public Map<String, Object> selectOrderEncodList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from OrderEncod where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<OrderEncod> list = new ArrayList<OrderEncod>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectOrderEncodDetailsList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OrderEncodDetails where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and orderEncod.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OrderEncodDetails> list = new ArrayList<OrderEncodDetails>();
		if (total > 0) {
			// if (dir != null && dir.length() > 0 && sort != null
			// && sort.length() > 0) {
			// if (sort.indexOf("-") != -1) {
			// sort = sort.substring(0, sort.indexOf("-"));
			// }
			// key = key + " order by " + sort + " " + dir;
			// }
			// if (startNum == null || limitNum == null) {
			// list = this.getSession().createQuery(hql + key).list();
			// } else {
			// list = this.getSession().createQuery(hql + key)
			// .setFirstResult(startNum).setMaxResults(limitNum)
			// .list();
			// }
			list = this.getSession().createQuery(hql + key).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOrderEncodItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OrderEncodItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and orderEncod.id='" + scId + "' order by code asc";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OrderEncodItem> list = new ArrayList<OrderEncodItem>();
		if (total > 0) {
			// if (dir != null && dir.length() > 0 && sort != null
			// && sort.length() > 0) {
			// if (sort.indexOf("-") != -1) {
			// sort = sort.substring(0, sort.indexOf("-"));
			// }
			// key = key + " order by " + sort + " " + dir;
			// }
			// if (startNum == null || limitNum == null) {
			// list = this.getSession().createQuery(hql + key).list();
			// } else {
			// list = this.getSession().createQuery(hql + key)
			// .setFirstResult(startNum).setMaxResults(limitNum)
			// .list();
			// }
			list = this.getSession().createQuery(hql + key).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询最大订单编码
	 * 
	 * @return
	 * @throws Exception
	 */
	public String selectMaxCode() throws Exception {
		String hql = "select max(code) from OrderEncodItem";
		String max = (String) this.getSession().createQuery(hql).uniqueResult();

		return max;
	}

	/**
	 * 根据主表编号查询子表的条数
	 * 
	 * @param id
	 * @return
	 */
	public Long selectItemByoidCount(String id) {
		String hql = "from OrderEncodItem t where 1=1 and t.orderEncod.id='"
				+ id + "'";

		Long count = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		return count;
	}
}