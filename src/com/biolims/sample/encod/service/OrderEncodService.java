package com.biolims.sample.encod.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.encod.dao.OrderEncodDao;
import com.biolims.sample.encod.model.OrderEncod;
import com.biolims.sample.encod.model.OrderEncodDetails;
import com.biolims.sample.encod.model.OrderEncodItem;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.log.SysoLogger;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class OrderEncodService {
	@Resource
	private OrderEncodDao orderEncodDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findOrderEncodList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return orderEncodDao.selectOrderEncodList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OrderEncod i) throws Exception {

		orderEncodDao.saveOrUpdate(i);

	}

	public OrderEncod get(String id) {
		OrderEncod orderEncod = commonDAO.get(OrderEncod.class, id);
		return orderEncod;
	}

	public Map<String, Object> findOrderEncodDetailsList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = orderEncodDao.selectOrderEncodDetailsList(
				scId, startNum, limitNum, dir, sort);
		List<OrderEncodDetails> list = (List<OrderEncodDetails>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findOrderEncodItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = orderEncodDao.selectOrderEncodItemList(
				scId, startNum, limitNum, dir, sort);
		List<OrderEncodItem> list = (List<OrderEncodItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrderEncodDetails(OrderEncod sc, String itemDataJson)
			throws Exception {
		List<OrderEncodDetails> saveItems = new ArrayList<OrderEncodDetails>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OrderEncodDetails scp = new OrderEncodDetails();
			// 将map信息读入实体类
			scp = (OrderEncodDetails) orderEncodDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOrderEncod(sc);

			saveItems.add(scp);
		}
		orderEncodDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOrderEncodDetails(String[] ids) throws Exception {
		for (String id : ids) {
			OrderEncodDetails scp = orderEncodDao.get(OrderEncodDetails.class,
					id);
			orderEncodDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrderEncodItem(OrderEncod sc, String itemDataJson)
			throws Exception {
		List<OrderEncodItem> saveItems = new ArrayList<OrderEncodItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OrderEncodItem scp = new OrderEncodItem();
			// 将map信息读入实体类
			scp = (OrderEncodItem) orderEncodDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOrderEncod(sc);

			saveItems.add(scp);
		}
		orderEncodDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOrderEncodItem(String[] ids) throws Exception {
		for (String id : ids) {
			OrderEncodItem scp = orderEncodDao.get(OrderEncodItem.class, id);
			orderEncodDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OrderEncod sc, Map jsonMap) throws Exception {
		if (sc != null) {
			orderEncodDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("orderEncodDetails");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOrderEncodDetails(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("orderEncodItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOrderEncodItem(sc, jsonStr);
			}
			if (sc.getNum() != null && !sc.getNum().equals("")) {
				atuoCode(sc);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void atuoCode(OrderEncod sc) throws Exception {
		// 自动生成编码信息
		Long count = orderEncodDao.selectItemByoidCount(sc.getId());
		for (int i = 1; i <= sc.getNum() - count; i++) {
			OrderEncodItem scp = new OrderEncodItem();
			String code = codingRuleService.getCode("OrderEncodItem");
			scp.setCode(code);
			scp.setOrderEncod(sc);
			orderEncodDao.saveOrUpdate(scp);
		}
	}

	/**
	 * 查询最大订单编码
	 * 
	 * @return
	 * @throws Exception
	 */
	public String selectMaxCode() throws Exception {
		String max = orderEncodDao.selectMaxCode();
		return max;
	}
}
