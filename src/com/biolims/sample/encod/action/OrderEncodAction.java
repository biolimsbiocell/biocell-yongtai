package com.biolims.sample.encod.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.encod.model.OrderEncod;
import com.biolims.sample.encod.model.OrderEncodDetails;
import com.biolims.sample.encod.model.OrderEncodItem;
import com.biolims.sample.encod.service.OrderEncodService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/encod/orderEncod")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class OrderEncodAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2114";
	@Autowired
	private OrderEncodService orderEncodService;
	private OrderEncod orderEncod = new OrderEncod();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showOrderEncodList")
	public String showOrderEncodList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/encod/orderEncod.jsp");
	}

	@Action(value = "showOrderEncodListJson")
	public void showOrderEncodListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = orderEncodService.findOrderEncodList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OrderEncod> list = (List<OrderEncod>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("maxCode", "");
		map.put("num", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "orderEncodSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogOrderEncodList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/encod/orderEncodDialog.jsp");
	}

	@Action(value = "showDialogOrderEncodListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogOrderEncodListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = orderEncodService.findOrderEncodList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OrderEncod> list = (List<OrderEncod>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("maxCode", "");
		map.put("num", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editOrderEncod")
	public String editOrderEncod() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			orderEncod = orderEncodService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "orderEncod");
		} else {
			orderEncod.setId("NEW");
			String maxCode = orderEncodService.selectMaxCode();
			if (maxCode == null || maxCode.equals("")) {
				orderEncod.setMaxCode("00000000");
			} else {
				orderEncod.setMaxCode(maxCode);
			}
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			orderEncod.setCreateUser(user);
			orderEncod.setCreateDate(new Date());
			orderEncod.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			orderEncod.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/encod/orderEncodEdit.jsp");
	}

	@Action(value = "copyOrderEncod")
	public String copyOrderEncod() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		orderEncod = orderEncodService.get(id);
		orderEncod.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/encod/orderEncodEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = orderEncod.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "OrderEncod";
			String markCode = "BM";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			orderEncod.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("orderEncodDetails",
				getParameterFromRequest("orderEncodDetailsJson"));

		aMap.put("orderEncodItem",
				getParameterFromRequest("orderEncodItemJson"));

		orderEncodService.save(orderEncod, aMap);
		return redirect("/sample/encod/orderEncod/editOrderEncod.action?id="
				+ orderEncod.getId());

	}

	@Action(value = "viewOrderEncod")
	public String toViewOrderEncod() throws Exception {
		String id = getParameterFromRequest("id");
		orderEncod = orderEncodService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/encod/orderEncodEdit.jsp");
	}

	@Action(value = "showOrderEncodDetailsList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOrderEncodDetailsList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/encod/orderEncodDetails.jsp");
	}

	@Action(value = "showOrderEncodDetailsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOrderEncodDetailsListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = orderEncodService
					.findOrderEncodDetailsList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<OrderEncodDetails> list = (List<OrderEncodDetails>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("codeDetails", "");
			map.put("orderEncod-name", "");
			map.put("orderEncod-id", "");
			map.put("num", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOrderEncodDetails")
	public void delOrderEncodDetails() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			orderEncodService.delOrderEncodDetails(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showOrderEncodItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOrderEncodItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/encod/orderEncodItem.jsp");
	}

	@Action(value = "showOrderEncodItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOrderEncodItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = orderEncodService
					.findOrderEncodItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<OrderEncodItem> list = (List<OrderEncodItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderEncod-name", "");
			map.put("orderEncod-id", "");
			map.put("num", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOrderEncodItem")
	public void delOrderEncodItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			orderEncodService.delOrderEncodItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public OrderEncodService getOrderEncodService() {
		return orderEncodService;
	}

	public void setOrderEncodService(OrderEncodService orderEncodService) {
		this.orderEncodService = orderEncodService;
	}

	public OrderEncod getOrderEncod() {
		return orderEncod;
	}

	public void setOrderEncod(OrderEncod orderEncod) {
		this.orderEncod = orderEncod;
	}

}
