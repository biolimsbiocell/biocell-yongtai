package com.biolims.sample.update.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 修改记录
 * @author lims-platform
 * @date 2015-11-12 11:06:36
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_UPDATE_RECORDS")
@SuppressWarnings("serial")
public class SampleUpdateRecords extends EntityDao<SampleUpdateRecords> implements java.io.Serializable {
	/**样本编号*/
	private String id;
	/**姓名*/
	private String name;
	/**样本类型*/
	private String sampleType;
	/**性别*/
	private String sex;
	/**备注*/
	private String note;
	/**相关主表*/
	private SampleDetail sampleDetail;
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  姓名
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  姓名
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本类型
	 */
	@Column(name ="SAMPLE_TYPE", length = 50)
	public String getSampleType(){
		return this.sampleType;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本类型
	 */
	public void setSampleType(String sampleType){
		this.sampleType = sampleType;
	}
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="SEX", length = 50)
	public String getSex(){
		return this.sex;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setSex(String sex){
		this.sex = sex;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得SampleDetail
	 *@return: SampleDetail  相关主表
	 */
	@Column(name ="SAMPLE_DETAIL", length = 50)
	public SampleDetail getSampleDetail(){
		return this.sampleDetail;
	}
	/**
	 *方法: 设置SampleDetail
	 *@param: SampleDetail  相关主表
	 */
	public void setSampleDetail(SampleDetail sampleDetail){
		this.sampleDetail = sampleDetail;
	}
}