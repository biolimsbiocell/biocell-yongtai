package com.biolims.sample.update.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 样本明细
 * @author lims-platform
 * @date 2015-11-12 11:06:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_DETAIL")
@SuppressWarnings("serial")
public class SampleDetail extends EntityDao<SampleDetail> implements java.io.Serializable {
	/**样本编号*/
	private String id;
	/**姓名*/
	private String name;
	/**样本类型*/
	private String sampleType;
	/**性别*/
	private String sex;
	/**结果判定*/
	private String resultDecide;
	/**下一步流向*/
	private String nextFlow;
	/**处理意见*/
	private String handleIdea;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  姓名
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  姓名
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本类型
	 */
	@Column(name ="SAMPLE_TYPE", length = 50)
	public String getSampleType(){
		return this.sampleType;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本类型
	 */
	public void setSampleType(String sampleType){
		this.sampleType = sampleType;
	}
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="SEX", length = 50)
	public String getSex(){
		return this.sex;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setSex(String sex){
		this.sex = sex;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="RESULT_DECIDE", length = 50)
	public String getResultDecide(){
		return this.resultDecide;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setResultDecide(String resultDecide){
		this.resultDecide = resultDecide;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理意见
	 */
	@Column(name ="HANDLE_IDEA", length = 50)
	public String getHandleIdea(){
		return this.handleIdea;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理意见
	 */
	public void setHandleIdea(String handleIdea){
		this.handleIdea = handleIdea;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 150)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
}