package com.biolims.sample.update.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.update.dao.SampleDetailDao;
import com.biolims.sample.update.model.SampleDetail;
import com.biolims.sample.update.model.SampleUpdateRecords;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleDetailService {
	@Resource
	private SampleDetailDao sampleDetailDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSampleDetailList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleDetailDao.selectSampleDetailList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleDetail i) throws Exception {

		sampleDetailDao.saveOrUpdate(i);

	}
	public SampleDetail get(String id) {
		SampleDetail sampleDetail = commonDAO.get(SampleDetail.class, id);
		return sampleDetail;
	}
	public Map<String, Object> findSampleUpdateRecordsList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleDetailDao.selectSampleUpdateRecordsList(scId, startNum, limitNum, dir, sort);
		List<SampleUpdateRecords> list = (List<SampleUpdateRecords>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleUpdateRecords(SampleDetail sc, String itemDataJson) throws Exception {
		List<SampleUpdateRecords> saveItems = new ArrayList<SampleUpdateRecords>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleUpdateRecords scp = new SampleUpdateRecords();
			// 将map信息读入实体类
			scp = (SampleUpdateRecords) sampleDetailDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleDetail(sc);

			saveItems.add(scp);
		}
		sampleDetailDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleUpdateRecords(String[] ids) throws Exception {
		for (String id : ids) {
			SampleUpdateRecords scp =  sampleDetailDao.get(SampleUpdateRecords.class, id);
			 sampleDetailDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleDetail sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleDetailDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("sampleUpdateRecords");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleUpdateRecords(sc, jsonStr);
			}
	}
   }
}
