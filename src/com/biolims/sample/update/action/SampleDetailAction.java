﻿
package com.biolims.sample.update.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.sample.update.model.SampleDetail;
import com.biolims.sample.update.model.SampleUpdateRecords;
import com.biolims.sample.update.service.SampleDetailService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/sample/update/sampleDetail")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleDetailAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private SampleDetailService sampleDetailService;
	private SampleDetail sampleDetail = new SampleDetail();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showSampleDetailList")
	public String showSampleDetailList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/update/sampleDetail.jsp");
	}

	@Action(value = "showSampleDetailListJson")
	public void showSampleDetailListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleDetailService.findSampleDetailList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleDetail> list = (List<SampleDetail>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleType", "");
		map.put("sex", "");
		map.put("resultDecide", "");
		map.put("nextFlow", "");
		map.put("handleIdea", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "sampleDetailSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleDetailList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/update/sampleDetailDialog.jsp");
	}

	@Action(value = "showDialogSampleDetailListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleDetailListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleDetailService.findSampleDetailList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleDetail> list = (List<SampleDetail>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleType", "");
		map.put("sex", "");
		map.put("resultDecide", "");
		map.put("nextFlow", "");
		map.put("handleIdea", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editSampleDetail")
	public String editSampleDetail() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleDetail = sampleDetailService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleDetail");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			sampleDetail.setCreateUser(user);
//			sampleDetail.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/update/sampleDetailEdit.jsp");
	}

	@Action(value = "copySampleDetail")
	public String copySampleDetail() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleDetail = sampleDetailService.get(id);
		sampleDetail.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/update/sampleDetailEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleDetail.getId();
		if(id!=null&&id.equals("")){
			sampleDetail.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("sampleUpdateRecords",getParameterFromRequest("sampleUpdateRecordsJson"));
		
		sampleDetailService.save(sampleDetail,aMap);
		return redirect("/sample/update/sampleDetail/editSampleDetail.action?id=" + sampleDetail.getId());

	}

	@Action(value = "viewSampleDetail")
	public String toViewSampleDetail() throws Exception {
		String id = getParameterFromRequest("id");
		sampleDetail = sampleDetailService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/update/sampleDetailEdit.jsp");
	}
	

	@Action(value = "showSampleUpdateRecordsList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleUpdateRecordsList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/update/sampleUpdateRecords.jsp");
	}

	@Action(value = "showSampleUpdateRecordsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleUpdateRecordsListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleDetailService.findSampleUpdateRecordsList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleUpdateRecords> list = (List<SampleUpdateRecords>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleType", "");
			map.put("sex", "");
			map.put("note", "");
			map.put("sampleDetail", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleUpdateRecords")
	public void delSampleUpdateRecords() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleDetailService.delSampleUpdateRecords(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleDetailService getSampleDetailService() {
		return sampleDetailService;
	}

	public void setSampleDetailService(SampleDetailService sampleDetailService) {
		this.sampleDetailService = sampleDetailService;
	}

	public SampleDetail getSampleDetail() {
		return sampleDetail;
	}

	public void setSampleDetail(SampleDetail sampleDetail) {
		this.sampleDetail = sampleDetail;
	}


}
