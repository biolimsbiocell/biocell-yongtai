package com.biolims.sample.storage.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.service.SampleInService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 样本入库
 * 
 * @author admin
 * 
 */
@Namespace("/sample/storage/sampleIn")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleInAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2109";
	@Autowired
	private SampleInService sampleInService;
	private SampleIn sampleIn = new SampleIn();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleReceiveService sampleReceiveService;

	/**
	 * 菜单栏链接
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleInList")
	public String showSampleInList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleIn.jsp");
	}

	@Action(value = "showSampleInListJson")
	public void showSampleInListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleInService.findSampleInList(
					map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleIn> list = (List<SampleIn>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("location", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleInApply-id", "");
			map.put("sampleInApply-name", "");
			map.put("sampleType", "");
			// map.put("sampleType-name", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("workOrder-id", "");
			map.put("workOrder-name", "");
			

			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "sampleInSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleInList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInDialog.jsp");
	}

	@Action(value = "showDialogSampleInListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleInListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleInService.findSampleInList(
					map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleIn> list = (List<SampleIn>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("location-id", "");
			map.put("location-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleInApply-id", "");
			map.put("sampleInApply-name", "");
			map.put("sampleType", "");
			// map.put("sampleType-name", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("workOrder-id", "");
			map.put("workOrder-name", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 编辑
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editSampleIn")
	public String editSampleIn() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleIn = sampleInService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleIn");
		} else {
			// sampleIn=new SampleIn();
			// sampleIn.setId(SystemCode.DEFAULT_SYSTEMCODE);
			sampleIn.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleIn.setCreateUser(user);
			sampleIn.setCreateDate(new Date());
			sampleIn.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleIn.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(sampleIn.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInEdit.jsp");
	}

	/**
	 * 复制
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "copySampleIn")
	public String copySampleIn() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleIn = sampleInService.get(id);
		sampleIn.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/storage/sampleInEdit.jsp");
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleIn.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleIn";
			String markCode = "YBRK";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleIn.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("sampleInItem", getParameterFromRequest("sampleInItemJson"));

		sampleInService.save(sampleIn, aMap);
		return redirect("/sample/storage/sampleIn/editSampleIn.action?id="
				+ sampleIn.getId());

	}

	/**
	 * 视图编辑
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "viewSampleIn")
	public String toViewSampleIn() throws Exception {
		String id = getParameterFromRequest("id");
		sampleIn = sampleInService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInEdit.jsp");
	}

	/**
	 * 左侧待处理样本
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleInItemTempList")
	public String showSampleInItemTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInItemTemp.jsp");
	}

	@Action(value = "showSampleInItemTempListJson")
	public void showSampleInItemTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleInService.findSampleInLeftList(
					map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleInItemTemp> list = (List<SampleInItemTemp>) result
					.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("num", "");
			map.put("sampleTypeId", "");
			map.put("sampleCode", "");
			map.put("location", "");
			map.put("barCode", "");

			map.put("infoFrom", "");
			map.put("sampleType", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("state", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("groupNo", "");
			map.put("isRp", "");
			map.put("sampleState","");
			map.put("type","");
			map.put("productId", "");
			map.put("productName", "");
			map.put("samplingDate","");
			map.put("dataTraffic","");
			
			map.put("sampleStyle","");
			map.put("qpcrConcentration","");
			map.put("indexa","");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 明细
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleInItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/storage/sampleInItem.jsp");
	}

	@Action(value = "showSampleInItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleInService.findSampleInItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleInItem> list = (List<SampleInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("checked", "");
			map.put("name", "");
			map.put("location", "");
			map.put("state", "");
			map.put("dataTraffic", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("sampleTypeId", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("infoFrom", "");
			map.put("num", "");
			map.put("sampleIn-name", "");
			map.put("sampleIn-id", "");
			map.put("upLocation", "");
			map.put("patientName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("customer-street", "");
			map.put("customer-postcode", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("fkUser-id", "");
			map.put("fkUser-name", "");
			map.put("isRp", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("groupNo", "");
			map.put("sampleState","");
			map.put("type","");
			map.put("productId","");
			map.put("productName","");
			map.put("samplingDate","");
			map.put("barCode","");
			
			map.put("sampleStyle","");
			map.put("qpcrConcentration","");
			map.put("indexa","");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 带出库样本
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleWaitOutItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleWaitOutItemList() throws Exception {
		String cid = getRequest().getParameter("cid");
		putObjToContext("cid", cid);
		String type = getRequest().getParameter("type");// 判断搜索项是否应该存在
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/sample/storage/sampleWaitOutItem.jsp");
	}

	@Action(value = "showSampleWaitOutItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleWaitOutItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
//			String scId = getRequest().getParameter("id");
			String cid = getRequest().getParameter("cid");

			String type = getRequest().getParameter("type");
			Map<String, Object> result = sampleInService
					.findSampleWaitOutItemList(cid,start, length, query,
							col, sort, type);
			List<SampleInfoIn> list = (List<SampleInfoIn>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("pronoun", "");
			map.put("code", "");
			map.put("checked", "");
			map.put("dataTraffic", "");
			map.put("name", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("classify", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("num", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");
			map.put("customer-street", "");
			map.put("customer-postcode", "");
			map.put("sampleInfo-project-id", "");
			map.put("sampleInfo-project-name", "");
			map.put("sampleInfo-crmCustomer-id", "");
			map.put("sampleInfo-crmCustomer-name", "");
			map.put("sampleInfo-crmDoctor-id", "");
			map.put("sampleInfo-crmDoctor-name", "");
			map.put("sampleInfo-productId", "");
			map.put("sampleInfo-productName", "");
			map.put("sampleInfo-patientName", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-name", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-idCard", "");
			map.put("isRp", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-createUser-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-orderId", "");
			map.put("sampleState", "");
			map.put("type", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("samplingDate","");
			map.put("barCode","");
			map.put("dataTraffic","");
			
			map.put("sampleStyle","");
			map.put("qpcrConcentration","");
			map.put("indexa","");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleInItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleInItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleInService.delSampleInItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据样本号匹配临时表
	 * 
	 * @throws Exception
	 */
	@Action(value = "isSampleInThere", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void isSampleInThere() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String vals = getParameterFromRequest("vals");
			Map<String, Object> result1 = new HashMap<String, Object>();
			List<SampleInItem> list = new ArrayList<SampleInItem>();
			List<String> list1 = new ArrayList<String>();
			result1 = sampleInService.isTempByCode(vals);
			list = (List<SampleInItem>) result1.get("list");
			list1 = (List<String>) result1.get("list1");
			result.put("isSave", list);
			result.put("list1", list1);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			result.put("isSave", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据入库明细查询信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "setSampleInItemByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setSampleInItemByid() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			SampleInItem s = this.sampleInService.getSampleInItem(id);
			result.put("success", true);
			result.put("data", s);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询左侧表添加样本是信息
	@Action(value = "setSampleInItemByCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setSampleInItemByCode() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String code = getParameterFromRequest("code");
			String sampleCode = getParameterFromRequest("sampleCode");
			// 查询入库样本是否入过库
			SampleInfoIn s = this.sampleInService.getSampleInItemByCode(code);
			// 根据样本号查询销售 和 客户信息
			// Map<String, Object> map = sampleReceiveService
			// .selBySampleCode(sampleCode);

			result.put("success", true);
			result.put("data", s);
			// result.put("map", map);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleInService getSampleInService() {
		return sampleInService;
	}

	public void setSampleInService(SampleInService sampleInService) {
		this.sampleInService = sampleInService;
	}

	public SampleIn getSampleIn() {
		return sampleIn;
	}

	public void setSampleIn(SampleIn sampleIn) {
		this.sampleIn = sampleIn;
	}

}
