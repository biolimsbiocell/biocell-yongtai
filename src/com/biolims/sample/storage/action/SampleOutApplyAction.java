package com.biolims.sample.storage.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.model.SampleOutApplyItem;
import com.biolims.sample.storage.service.SampleOutApplyService;
import com.biolims.system.code.SystemConstants;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 出库申请
 * 
 * @author admin
 * 
 */
@Namespace("/sample/storage/sampleOutApply")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleOutApplyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "210501";
	@Autowired
	private SampleOutApplyService sampleOutApplyService;
	private SampleOutApply sampleOutApply = new SampleOutApply();
	@Resource
	private CommonService commonService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	
	@Resource
	private CommonDAO commonDAO;
	

	/**
	 * 菜单栏链接
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleOutApplyList")
	public String showSampleOutApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApply.jsp");
	}

	@Action(value = "showSampleOutApplyListJson")
	public void showSampleOutApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleOutApplyService
					.findSampleOutApplyList(map2Query, startNum, limitNum, dir,
							sort);
			Long count = (Long) result.get("total");
			List<SampleOutApply> list = (List<SampleOutApply>) result
					.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleType-id", "");
			map.put("sampleType-name", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("workOrder-id", "");
			map.put("workOrder-name", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("fyDate", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("experimentType-id", "");
			map.put("experimentType-name", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toSampleOutApplyMain", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toSampleOutApplyMain() throws Exception {
		String reqMethodType = getRequest().getParameter("reqMethodType");
		String outApplyId = getRequest().getParameter("outApplyId");
		if (outApplyId != null) {
			SampleOutApply task = sampleOutApplyService.get(outApplyId);
			if (!SystemConstants.WORK_FLOW_NEW.equals(task.getState()))
				reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toState(task.getState());
		}

		getRequest().setAttribute("outApplyId", outApplyId);
		putObjToContext("handlemethod", reqMethodType);
		toToolBar(rightsId, "", "", reqMethodType);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyMain.jsp");
	}

	/**
	 * 查询
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "sampleOutApplySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleOutApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyDialog.jsp");
	}

	@Action(value = "showDialogSampleOutApplyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleOutApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String outType = getParameterFromRequest("outType");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		map2Query.put("stateName", com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		map2Query.put("type", outType);
		try {
			Map<String, Object> result = sampleOutApplyService
					.findSampleOutApplyList(map2Query, startNum, limitNum, dir,
							sort);
			Long count = (Long) result.get("total");
			List<SampleOutApply> list = (List<SampleOutApply>) result
					.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleType-id", "");
			map.put("sampleType-name", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("workOrder-id", "");
			map.put("workOrder-name", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("experimentType-id", "");
			map.put("experimentType-name", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 编辑
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editSampleOutApply")
	public String editSampleOutApply() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleOutApply = sampleOutApplyService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleOutApply");
		} else {
			// sampleOutApply=new SampleOutApply();
			// sampleOutApply.setId(SystemCode.DEFAULT_SYSTEMCODE);
			sampleOutApply.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleOutApply.setCreateUser(user);
			sampleOutApply.setCreateDate(new Date());
			sampleOutApply.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOutApply.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(sampleOutApply.getState());
		putObjToContext("fileNum", num);
		List<DicType> list = commonService
				.find("from DicType where type = 'yblx'");// 复选框list
		putObjToContext("DicCountTableList", list);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	}

	/**
	 * 复制
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "copySampleOutApply")
	public String copySampleOutApply() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleOutApply = sampleOutApplyService.get(id);
		sampleOutApply.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleOutApply.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleOutApply";
			String markCode = "CKSQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleOutApply.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("sampleOutApplyItem",
				getParameterFromRequest("sampleOutApplyItemJson"));

		sampleOutApplyService.save(sampleOutApply, aMap);
		return redirect("/sample/storage/sampleOutApply/editSampleOutApply.action?id="
				+ sampleOutApply.getId());

	}

	/**
	 * 视图编辑
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "viewSampleOutApply")
	public String toViewSampleOutApply() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOutApply = sampleOutApplyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	}

	/**
	 * 明细
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleOutApplyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOutApplyItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyItem.jsp");
	}

	@Action(value = "showSampleOutApplyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOutApplyItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleOutApplyService
					.findSampleOutApplyItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("state", "");
			map.put("code", "");
			map.put("dataTraffic", "");
			map.put("num", "");
			map.put("sampleCode", "");
			map.put("note", "");
			map.put("sampleInItemId", "");
			map.put("sampleType", "");
			map.put("location", "");
			map.put("sampleOutApply-name", "");
			map.put("sampleOutApply-id", "");
			map.put("sendTime", "yyyy-MM-dd");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("ystj", "");
			map.put("fyVolume", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-createUser-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("samplingDate", "");
			map.put("barCode", "");
			map.put("patientName", "");
			
			map.put("sampleStyle", "");
			map.put("qpcrConcentration", "");
			map.put("indexa", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOutApplyItem")
	public void delSampleOutApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleOutApplyService.delSampleOutApplyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据申请加载子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "showSampleOutItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOutItemList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> dataListMap = this.sampleOutApplyService
					.showSampleOutItemList(code);
			List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) dataListMap
					.get("list");
			result.put("success", true);
			result.put("data", list);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleOutApplyService getSampleOutApplyService() {
		return sampleOutApplyService;
	}

	public void setSampleOutApplyService(
			SampleOutApplyService sampleOutApplyService) {
		this.sampleOutApplyService = sampleOutApplyService;
	}

	public SampleOutApply getSampleOutApply() {
		return sampleOutApply;
	}

	public void setSampleOutApply(SampleOutApply sampleOutApply) {
		this.sampleOutApply = sampleOutApply;
	}
	
	
	
	
	/**
	 * 
	 * @Title: showSampleInfoInTableJson  
	 * @Description: 出库申请查询待出库样本
	 * @author qi.yan
	 * @date 2018-4-10下午3:55:04
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showSampleInfoInTableJson")
	public void showSampleInfoInTableJson() throws Exception {
		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOutApplyService.selSampleInfoInTable(start, length, query, col,
					sort);
			List<SampleInfoIn> list = (List<SampleInfoIn>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("checked", "");
			map.put("dataTraffic", "");
			map.put("name", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("classify", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("num", "");
//			map.put("concentration", "");
//			map.put("volume", "");
//			map.put("sumTotal", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");
			map.put("customer-street", "");
			map.put("customer-postcode", "");
			map.put("sampleInfo-project-id", "");
			map.put("sampleInfo-project-name", "");
			map.put("sampleInfo-crmCustomer-id", "");
			map.put("sampleInfo-crmCustomer-name", "");
			map.put("sampleInfo-crmDoctor-id", "");
			map.put("sampleInfo-crmDoctor-name", "");
			map.put("sampleInfo-productId", "");
			map.put("sampleInfo-productName", "");
			map.put("sampleInfo-patientName", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-name", "");
			map.put("sampleInfo-idCard", "");
			map.put("isRp", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-createUser-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-orderId", "");
			map.put("sampleState", "");
			map.put("type", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("samplingDate","");
//			map.put("barCode","");
			map.put("dataTraffic","");
			
			map.put("sampleStyle","");
			/*map.put("qpcrConcentration","");
			map.put("indexa","");*/

			String data = new SendData().getDateJsonForDatatable(map, list);
//			// 根据模块查询自定义字段数据
//			Map<String, Object> mapField = fieldService
//					.findFieldByModuleValue("Storage");
//			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * 
	 * @Title: showSampleOutApplyItemTableJson  
	 * @Description: 出库申请明细
	 * @author qi.yan
	 * @date 2018-4-10下午4:15:11
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showSampleOutApplyItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOutApplyItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleOutApplyService
					.selSampleOutApplyItemTable(scId, start, length, query, col,
							sort);
			List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("name", "");
			map.put("state", "");
			map.put("code", "");
//			map.put("dataTraffic", "");
			map.put("num", "");
			map.put("sampleCode", "");
			map.put("note", "");
			map.put("sampleInItemId", "");
			map.put("sampleType", "");
			map.put("location", "");
			map.put("sampleOutApply-name", "");
			map.put("sampleOutApply-id", "");
			map.put("sendTime", "yyyy-MM-dd");
//			map.put("concentration", "");
//			map.put("volume", "");
//			map.put("sumTotal", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("ystj", "");
			map.put("fyVolume", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-createUser-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("samplingDate", "");
//			map.put("barCode", "");
			map.put("patientName", "");
			
			map.put("sampleStyle", "");
//			map.put("qpcrConcentration", "");
//			map.put("indexa", "");
			
			map.put("sumTotal", "");
			map.put("sampleDeteyionId", "");
			map.put("sampleDeteyionName", "");
			map.put("infoFrom", "");
			map.put("infoFromId", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 
	 * @Title: showSampleOutApplyTable  
	 * @Description: 展示出库申请主表 
	 * @author qi.yan
	 * @date 2018-4-10下午4:37:39
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "showSampleOutApplyTable")
	public String showSampleOutApplyTable() throws Exception {
		rightsId="210501";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyTable.jsp");
	}
	@Action(value = "showSampleOutApplyTableJson")
	public void showSampleOutApplyTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOutApplyService.selSampleOutApplyTable(
					start, length, query, col, sort);
			List<SampleOutApply> list = (List<SampleOutApply>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleType-id", "");
			map.put("sampleType-name", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("workOrder-id", "");
			map.put("workOrder-name", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("fyDate", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("experimentType-id", "");
			map.put("experimentType-name", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 添加、修改出库申请页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditSampleOutApply")
	public String toEditSampleOutApply() throws Exception {
		rightsId="210502";
		String outApplyId = super.getRequest().getParameter("id");
		String handlemethod = "";
		if (outApplyId != null) {
			this.sampleOutApply = this.sampleOutApplyService.findSampleOutApplyById(outApplyId);
			toState(sampleOutApply.getState());
			if (sampleOutApply.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			showSampleOutApplyItemList(handlemethod, handlemethod);

		} else {
			this.sampleOutApply = new SampleOutApply();
			this.sampleOutApply.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User createUser = (User) super.getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			sampleOutApply.setCreateUser(createUser);
			sampleOutApply.setCreateDate(new Date());
			sampleOutApply.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOutApply.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(sampleOutApply.getState());
			showSampleOutApplyItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
					outApplyId);
		}
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	}

	
	public void showSampleOutApplyItemList(String handleMethod, String outId)
			throws Exception {
		putObjToContext("outId", outId);
		putObjToContext("handlemethod", handleMethod);
	}
	
	
	/**
	 * 
	 * @Title: addSampleOutApplyItem  
	 * @Description: 添加库存样本到出库申请明细
	 * @author qi.yan
	 * @date 2018-4-11下午3:01:25
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "addSampleOutApplyItem")
	public void addSampleOutApplyItem() throws Exception {
		String note = getParameterFromRequest("note");
		String type = getParameterFromRequest("type");
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String createUser = getParameterFromRequest("createUser");
		String[] ids = getRequest().getParameterValues("ids[]");
		
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String sampleOutId = sampleOutApplyService.addSampleOutApplyItem(ids,note,type,id,createUser,createDate);
			result.put("success", true);
			result.put("data", sampleOutId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	
	
	/**
	 * 
	 * @Title: saveSampleOutApplyAndItem  
	 * @Description: 大保存
	 * @author qi.yan
	 * @date 2018-4-11下午3:01:56
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "saveSampleOutApplyAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleOutApplyAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");
		
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		String note = getParameterFromRequest("note");


		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
				List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				SampleOutApply soa = new SampleOutApply();
				Map aMap = new HashMap();
				soa = (SampleOutApply) commonDAO.Map2Bean(map1, soa);
				//保存主表信息
				SampleOutApply newSoa = sampleOutApplyService.saveSampleOutApplyById(soa,id,type,note,changeLog);
				
				newSoa.setScopeId((String) ActionContext.getContext().getSession()
						.get("scopeId"));
				newSoa.setScopeName((String) ActionContext.getContext().getSession()
						.get("scopeName"));

				//保存子表信息
				aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
				sampleOutApplyService.saveSampleOutApplyAndItem(newSoa, aMap, changeLog, changeLogItem);
				map.put("id",newSoa.getId());
			}
			map.put("success", true);
			
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	
	
	@Action(value = "delSampleOutApplyItems")
	public void delSampleOutApplyItems() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sampleOutApplyService.delSampleOutApplyItem(ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	
	
	
	
}
