package com.biolims.sample.storage.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleReturn;
import com.biolims.sample.storage.model.SampleReturnItem;
import com.biolims.sample.storage.service.SampleReturnService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/sample/storage/sampleReturn")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleReturnAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2107";
	@Autowired
	private SampleReturnService sampleReturnService;
	private SampleReturn sampleReturn = new SampleReturn();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showSampleReturnList")
	public String showSampleReturnList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleReturn.jsp");
	}

	@Action(value = "showSampleReturnListJson")
	public void showSampleReturnListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try{
			Map<String, Object> result = sampleReturnService.findSampleReturnList(map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleReturn> list = (List<SampleReturn>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("location-id", "");
			map.put("location-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "sampleReturnSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleReturnList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleReturnDialog.jsp");
	}

	@Action(value = "showDialogSampleReturnListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleReturnListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try{
			Map<String, Object> result = sampleReturnService.findSampleReturnList(map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleReturn> list = (List<SampleReturn>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("location-id", "");
			map.put("location-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@Action(value = "editSampleReturn")
	public String editSampleReturn() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleReturn = sampleReturnService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleReturn");
		} else {
			sampleReturn=new SampleReturn();
			sampleReturn.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleReturn.setCreateUser(user);
			sampleReturn.setCreateDate(new Date());
			sampleReturn.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleReturn.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(sampleReturn.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/storage/sampleReturnEdit.jsp");
	}

	@Action(value = "copySampleReturn")
	public String copySampleReturn() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleReturn = sampleReturnService.get(id);
		sampleReturn.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/storage/sampleReturnEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleReturn.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleReturn";
			String markCode="YBFK";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			sampleReturn.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("sampleReturnItem",getParameterFromRequest("sampleReturnItemJson"));
		
		sampleReturnService.save(sampleReturn,aMap);
		return redirect("/sample/storage/sampleReturn/editSampleReturn.action?id=" + sampleReturn.getId());

	}

	@Action(value = "viewSampleReturn")
	public String toViewSampleReturn() throws Exception {
		String id = getParameterFromRequest("id");
		sampleReturn = sampleReturnService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/storage/sampleReturnEdit.jsp");
	}
	

	@Action(value = "showSampleReturnItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReturnItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/storage/sampleReturnItem.jsp");
	}

	@Action(value = "showSampleReturnItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReturnItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleReturnService.findSampleReturnItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleReturnItem> list = (List<SampleReturnItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("check", "");
			map.put("name", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleReturn-name", "");
			map.put("sampleReturn-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleReturnItem")
	public void delSampleReturnItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleReturnService.delSampleReturnItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleReturnService getSampleReturnService() {
		return sampleReturnService;
	}

	public void setSampleReturnService(SampleReturnService sampleReturnService) {
		this.sampleReturnService = sampleReturnService;
	}

	public SampleReturn getSampleReturn() {
		return sampleReturn;
	}

	public void setSampleReturn(SampleReturn sampleReturn) {
		this.sampleReturn = sampleReturn;
	}


}
