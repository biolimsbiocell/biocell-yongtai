package com.biolims.sample.storage.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleInApply;
import com.biolims.sample.storage.model.SampleInApplyItem;
import com.biolims.sample.storage.service.SampleInApplyService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/sample/storage/sampleInApply")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleInApplyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2108";
	@Autowired
	private SampleInApplyService sampleInApplyService;
	private SampleInApply sampleInApply = new SampleInApply();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showSampleInApplyList")
	public String showSampleInApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInApply.jsp");
	}

	@Action(value = "showSampleInApplyListJson")
	public void showSampleInApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try{
			Map<String, Object> result = sampleInApplyService.findSampleInApplyList(map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleInApply> list = (List<SampleInApply>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("taskCode-id", "");
			map.put("taskCode-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value = "sampleInApplySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleInApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInApplyDialog.jsp");
	}

	@Action(value = "showDialogSampleInApplyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleInApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try{
			Map<String, Object> result = sampleInApplyService.findSampleInApplyList(map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleInApply> list = (List<SampleInApply>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("taskCode-id", "");
			map.put("taskCode-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@Action(value = "editSampleInApply")
	public String editSampleInApply() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleInApply = sampleInApplyService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleInApply");
		} else {
			//sampleInApply=new SampleInApply();
			sampleInApply.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleInApply.setCreateUser(user);
			sampleInApply.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInApplyEdit.jsp");
	}

	@Action(value = "copySampleInApply")
	public String copySampleInApply() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleInApply = sampleInApplyService.get(id);
		sampleInApply.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/storage/sampleInApplyEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleInApply.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleInApply";
			String markCode="RKSQ";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			sampleInApply.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("sampleInApplyItem",getParameterFromRequest("sampleInApplyItemJson"));
		
		sampleInApplyService.save(sampleInApply,aMap);
		return redirect("/sample/storage/sampleInApply/editSampleInApply.action?id=" + sampleInApply.getId());

	}

	@Action(value = "viewSampleInApply")
	public String toViewSampleInApply() throws Exception {
		String id = getParameterFromRequest("id");
		sampleInApply = sampleInApplyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInApplyEdit.jsp");
	}
	

	@Action(value = "showSampleInApplyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInApplyItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/storage/sampleInApplyItem.jsp");
	}

	@Action(value = "showSampleInApplyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInApplyItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleInApplyService.findSampleInApplyItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleInApplyItem> list = (List<SampleInApplyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("check", "");
			map.put("name", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleInApply-name", "");
			map.put("sampleInApply-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleInApplyItem")
	public void delSampleInApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleInApplyService.delSampleInApplyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleInApplyService getSampleInApplyService() {
		return sampleInApplyService;
	}

	public void setSampleInApplyService(SampleInApplyService sampleInApplyService) {
		this.sampleInApplyService = sampleInApplyService;
	}

	public SampleInApply getSampleInApply() {
		return sampleInApply;
	}

	public void setSampleInApply(SampleInApply sampleInApply) {
		this.sampleInApply = sampleInApply;
	}


}
