package com.biolims.sample.storage.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.service.SampleInService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 库存样本
 * 
 * @author admin
 * 
 */
@Namespace("/sample/storage/sampleInInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleInInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "210901";
	@Autowired
	private SampleInService sampleInService;
	private SampleIn sampleIn = new SampleIn();

	/**
	 * 库存样本
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleInInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInInfoList() throws Exception {
		String dicSampleType = getParameterFromRequest("dicSampleType");
		putObjToContext("dicSampleType", dicSampleType);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInItemInfo.jsp");
	}

	@Action(value = "showSampleInInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInInfoListJson() throws Exception {
		
		//选择查看的样本类型
		String type = getParameterFromRequest("type");
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		if("".equals(data)){
			data = "{\"outState\": \"%4%\"}";
		}
		String dicSampleType = getParameterFromRequest("dicSampleType");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		// else
		// map2Query.put("state", "1");
		try {
			Map<String, Object> result = sampleInService
					.selectSampleInfoInist(map2Query, startNum, limitNum,
							dir, sort, dicSampleType);
			Long total = (Long) result.get("total");
			List<SampleInItem> list = (List<SampleInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("primaryCode", "");
			map.put("pronoun", "");
			map.put("checked", "");
			map.put("name", "");
			map.put("orderId", "");
			map.put("sampleType", "");
//			map.put("dicSampleType-id", "");
//			map.put("dicSampleType-name", "");
			map.put("classify", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("num", "");
//			map.put("patientName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("customer-street", "");

//			map.put("unitGroup-id", "");
//			map.put("unitGroup-name", "");
//			map.put("fkUser-id", "");
//			map.put("fkUser-name", "");
			map.put("isRp", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("sampleInfo-idCard", "");// 外部样本号
			map.put("sampleInfo-cardNumber", "");// 实际外部号
//			map.put("outState", "");
			
			map.put("sampleStyle", "");
			map.put("qpcrConcentration", "");
			map.put("indexa", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleInService getSampleInService() {
		return sampleInService;
	}

	public void setSampleInService(SampleInService sampleInService) {
		this.sampleInService = sampleInService;
	}

	public SampleIn getSampleIn() {
		return sampleIn;
	}

	public void setSampleIn(SampleIn sampleIn) {
		this.sampleIn = sampleIn;
	}

	
	
	/**
	 * 库存样本展示
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleInInfoTableList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInInfoTableList() throws Exception {
		String dicSampleType = getParameterFromRequest("dicSampleType");
		putObjToContext("dicSampleType", dicSampleType);
		return dispatcher("/WEB-INF/page/sample/storage/sampleInfoInTable.jsp");
	}
	@Action(value = "showSampleInInfoTableListJson")
	public void showSampleInInfoTableListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleInService.selSampleInfoInTable(
					start, length, query, col, sort);
			List<SampleInItem> list = (List<SampleInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("checked", "");
			map.put("name", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("classify", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("num", "");
			map.put("patientName", "");
//			map.put("concentration", "");
//			map.put("volume", "");
//			map.put("sumTotal", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("customer-street", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("fkUser-id", "");
			map.put("fkUser-name", "");
			map.put("isRp", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("sampleInfo-idCard", "");// 外部样本号
			map.put("sampleInfo-cardNumber", "");// 实际外部号
			map.put("outState", "");
			
			map.put("sampleStyle", "");
//			map.put("qpcrConcentration", "");
//			map.put("indexa", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
}
