﻿package com.biolims.sample.storage.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.model.SampleOutItem;
import com.biolims.sample.storage.model.SampleOutTaskId;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.sample.storage.service.SampleOutService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/sample/storage/sampleOut")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleOutAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "210601";
	@Autowired
	private SampleOutService sampleOutService;
	private SampleOut sampleOut = new SampleOut();

	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonDAO commonDAO;
	
	

	@Action(value = "showSampleOutList")
	public String showSampleOutList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOut.jsp");
	}

	@Action(value = "showSampleOutListJson")
	public void showSampleOutListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleOutService.findSampleOutList(
					map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleOut> list = (List<SampleOut>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("sampleOutApply-id", "");
			map.put("sampleOutApply-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd HH:mm:ss");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd HH:mm:ss");
			map.put("applyUser-id", "");
			map.put("applyUser-name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("outType", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "sampleOutSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleOutList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutDialog.jsp");
	}

	@Action(value = "showDialogSampleOutListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleOutListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleOutService.findSampleOutList(
					map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<SampleOut> list = (List<SampleOut>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("sampleOutApply-id", "");
			map.put("sampleOutApply-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("applyUser-id", "");
			map.put("applyUser-name", "");
			map.put("state", "");
			map.put("stateName", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "editSampleOut")
	public String editSampleOut() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleOut = sampleOutService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleOut");
		} else {
			sampleOut = new SampleOut();
			sampleOut.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleOut.setCreateUser(user);
			sampleOut.setCreateDate(new Date());
			sampleOut.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOut.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(sampleOut.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutEdit.jsp");
	}

	@Action(value = "copySampleOut")
	public String copySampleOut() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleOut = sampleOutService.get(id);
		sampleOut.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleOut.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleOut";
			String markCode = "YBCK";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleOut.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("sampleOutItem", getParameterFromRequest("sampleOutItemJson"));

		sampleOutService.save(sampleOut, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/sample/storage/sampleOut/editSampleOut.action?id="
				+ sampleOut.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return redirect("/sample/storage/sampleOut/editSampleOut.action?id="
		// + sampleOut.getId());

	}

	@Action(value = "viewSampleOut")
	public String toViewSampleOut() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOut = sampleOutService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutEdit.jsp");
	}

	@Action(value = "showSampleOutItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOutItemList() throws Exception {
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutItem.jsp");
	}

	@Action(value = "showSampleOutItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOutItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleOutService
					.findSampleOutItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleOutItem> list = (List<SampleOutItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("location", "");
			map.put("name", "");
			map.put("num", "");
			map.put("dataTraffic", "");
			map.put("sampleNum", "");
			map.put("state", "");
			map.put("note", "");
			map.put("code", "");
			map.put("sampleInItemId", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("sampleOut-name", "");
			map.put("sampleOut-id", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("orderCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("patientName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmCustomer-street", "");
			map.put("crmCustomer-postcode", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			map.put("supplier-linkMan", "");
			map.put("supplier-linkTel", "");
			map.put("expreceCode", "");
			map.put("applyUser-name", "");
			map.put("applyUser-id", "");
			map.put("isDepot", "");
			map.put("expressCompany-name", "");
			map.put("expressCompany-id", "");
			map.put("infoFrom", "");
			map.put("samplingDate", "");
			map.put("barCode", "");
			
			map.put("sampleStyle", "");
			map.put("qpcrConcentration", "");
			map.put("indexa", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOutItem")
	public void delSampleOutItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleOutService.delSampleOutItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleOutService getSampleOutService() {
		return sampleOutService;
	}

	public void setSampleOutService(SampleOutService sampleOutService) {
		this.sampleOutService = sampleOutService;
	}

	public SampleOut getSampleOut() {
		return sampleOut;
	}

	public void setSampleOut(SampleOut sampleOut) {
		this.sampleOut = sampleOut;
	}

	/**
	 * 出库左侧中间表
	 */
	@Action(value = "showSampleOutTempList")
	public String showSampleOutTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutTemp.jsp");
	}

	@Action(value = "showSampleOutTempListJson")
	public void showSampleOutTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = sampleOutService
					.findSampleOutTempList(map2Query, startNum, limitNum, dir,
							sort);
			Long count = (Long) result.get("total");
			List<SampleOutTemp> list = (List<SampleOutTemp>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("dataTraffic", "");
			map.put("num", "");
			map.put("sampleCode", "");
			map.put("state", "");
			map.put("location", "");
			map.put("sampleInItemId", "");
			map.put("method", "");
			map.put("sampleType", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("applyUser-name", "");
			map.put("applyUser-id", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmCustomer-street", "");
			map.put("crmCustomer-postcode", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			map.put("supplier-linkMan", "");
			map.put("supplier-linkTel", "");
			map.put("type", "");
			map.put("taskId", "");
			map.put("infoFrom", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("barCode", "");
			map.put("samplingDate", "");
			map.put("patientName", "");
			
			map.put("sampleStyle", "");
			map.put("qpcrConcentration", "");
			map.put("indexa", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 出库任务单
	@Action(value = "sampleOutTaskIdDialog", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String sampleOutTaskIdDialog() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutTaskIdDialog.jsp");
	}

	@Action(value = "showsampleOutTaskIdDialogListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showsampleOutTaskIdDialogListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String type = getParameterFromRequest("type");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		map2Query.put("type", type);
		try {
			Map<String, Object> result = sampleOutService
					.findSampleOutTaskIdList(map2Query, startNum, limitNum,
							dir, sort);
			Long count = (Long) result.get("total");
			List<SampleOutTaskId> list = (List<SampleOutTaskId>) result
					.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("taskId", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd HH:mm:ss");
			map.put("state", "");
			map.put("stateName", "");
			map.put("type", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
	/**
	 * 
	 * @Title: showSampleOutTempTableJson  
	 * @Description: 待出库样本  
	 * @author qi.yan
	 * @date 2018-4-12上午9:58:35
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showSampleOutTempTableJson")
	public void showSampleOutTempTableJson() throws Exception {
		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOutService.selSampleOutTempTable(start, length, query, col,
					sort);
			List<SampleOutTemp> list = (List<SampleOutTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("code", "");
//			map.put("dataTraffic", "");
			map.put("num", "");
			map.put("sampleCode", "");
			map.put("state", "");
			map.put("location", "");
			map.put("sampleInItemId", "");
			map.put("method", "");
			map.put("sampleType", "");
//			map.put("concentration", "");
//			map.put("volume", "");
//			map.put("sumTotal", "");
			map.put("applyUser-name", "");
			map.put("applyUser-id", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmCustomer-street", "");
			map.put("crmCustomer-postcode", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			map.put("supplier-linkMan", "");
			map.put("supplier-linkTel", "");
			map.put("type", "");
			map.put("taskId", "");
			map.put("infoFrom", "");
			map.put("productId", "");
			map.put("productName", "");
//			map.put("barCode", "");
			map.put("samplingDate", "");
			map.put("patientName", "");
			
			map.put("sampleStyle", "");
			/*map.put("qpcrConcentration", "");
			map.put("indexa", "");*/

			String data = new SendData().getDateJsonForDatatable(map, list);
//			// 根据模块查询自定义字段数据
//			Map<String, Object> mapField = fieldService
//					.findFieldByModuleValue("Storage");
//			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 
	 * @Title: showSampleOutItemTableJson  
	 * @Description: 出库明细样本
	 * @author qi.yan
	 * @date 2018-4-12上午10:07:00
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showSampleOutItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOutItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleOutService
					.selSampleOutItemTable(scId, start, length, query, col,
							sort);
			List<SampleOutItem> list = (List<SampleOutItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("location", "");
			map.put("pronoun", "");
			map.put("name", "");
			map.put("num", "");
//			map.put("dataTraffic", "");
			map.put("sampleNum", "");
			map.put("state", "");
			map.put("note", "");
			map.put("code", "");
			map.put("sampleInItemId", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("sampleOut-name", "");
			map.put("sampleOut-id", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("orderCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("patientName", "");
//			map.put("concentration", "");
//			map.put("volume", "");
//			map.put("sumTotal", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmCustomer-street", "");
			map.put("crmCustomer-postcode", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			map.put("supplier-linkMan", "");
			map.put("supplier-linkTel", "");
			map.put("expreceCode", "");
			map.put("applyUser-name", "");
			map.put("applyUser-id", "");
			map.put("isDepot", "");
			map.put("expressCompany-name", "");
			map.put("expressCompany-id", "");
			map.put("infoFrom", "");
			map.put("samplingDate", "");
//			map.put("barCode", "");
			
			map.put("sampleStyle", "");
//			map.put("qpcrConcentration", "");
//			map.put("indexa", "");
			
			
			map.put("sampleDeteyionName", "");
			map.put("sampleDeteyionId", "");
			map.put("infoFrom", "");
			map.put("infoFromId", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 
	 * @Title: addSampleOutItem  
	 * @Description: 添加样本到出库明细 
	 * @author qi.yan
	 * @date 2018-4-12上午10:15:05
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "addSampleOutItem")
	public void addSampleOutItem() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String note = getParameterFromRequest("note");
		String createUser = getParameterFromRequest("createUser");
		String createDate = getParameterFromRequest("createDate");
		String outTypes = getParameterFromRequest("outTypes");
		String id = getParameterFromRequest("id");
		String fyDate = getParameterFromRequest("fyDate");
		String applyUser = getParameterFromRequest("applyUser");
		String acceptUser = getParameterFromRequest("acceptUser");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String sampleOutApplyId = sampleOutService.addSampleOutItem(ids,note,createUser,createDate,outTypes,id,fyDate,applyUser,acceptUser);
			result.put("success", true);
			result.put("data", sampleOutApplyId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	
	/**
	 * 
	 * @Title: saveSampleOutAndItem  
	 * @Description: 大保存
	 * @author qi.yan
	 * @date 2018-4-12上午10:39:00
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "saveSampleOutAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleOutAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String id = getParameterFromRequest("id");
		String note = getParameterFromRequest("note");
		String outTypes = getParameterFromRequest("outTypes");
		String fyDate = getParameterFromRequest("fyDate");
		String applyUser = getParameterFromRequest("applyUser");
		String acceptUser = getParameterFromRequest("acceptUser");
		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
				List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				SampleOut so = new SampleOut();
				Map aMap = new HashMap();
				so = (SampleOut) commonDAO.Map2Bean(map1, so);
				//保存主表信息
				SampleOut newSo = sampleOutService.saveSampleOutById(so,note,outTypes,id,fyDate,applyUser,acceptUser,changeLog);
				
				newSo.setScopeId((String) ActionContext.getContext().getSession()
						.get("scopeId"));
				newSo.setScopeName((String) ActionContext.getContext().getSession()
						.get("scopeName"));

				//保存子表信息
				aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
				sampleOutService.saveSampleOutAndItem(newSo, aMap, changeLog, changeLogItem);
				map.put("id",newSo.getId());
			}
			map.put("success", true);
			
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	
	/**
	 * 
	 * @Title: showSampleOutTable  
	 * @Description: 展示出库主表
	 * @author qi.yan
	 * @date 2018-4-12上午10:47:12
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "showSampleOutTable")
	public String showSampleOutTable() throws Exception {
		rightsId="210601";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutTable.jsp");
	}
	@Action(value = "showSampleOutTableJson")
	public void showSampleOutTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOutService.selSampleOutTable(
					start, length, query, col, sort);
			List<SampleOut> list = (List<SampleOut>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("sampleOutApply-id", "");
			map.put("sampleOutApply-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("applyUser-id", "");
			map.put("applyUser-name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("outType", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("outTypes-id", "");
			map.put("outTypes-name", "");
			map.put("fyDate", "yyyy-MM-dd");
			
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 
	 * @Title: toEditSampleOut  
	 * @Description: 新建、编辑页面
	 * @author qi.yan
	 * @date 2018-4-12上午10:49:17
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "toEditSampleOut")
	public String toEditSampleOut() throws Exception {
		rightsId="210602";
		String outId = super.getRequest().getParameter("id");
		String handlemethod = "";
		if (outId != null) {
			this.sampleOut = this.sampleOutService.findSampleOutById(outId);
			toState(sampleOut.getState());
			if (sampleOut.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			showSampleOutItemList(handlemethod, handlemethod);

		} else {
			this.sampleOut = new SampleOut();
			this.sampleOut.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User createUser = (User) super.getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			sampleOut.setCreateUser(createUser);
			sampleOut.setCreateDate(new Date());
			sampleOut.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOut.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(sampleOut.getState());
			showSampleOutItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
					outId);
		}
		
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		
		return dispatcher("/WEB-INF/page/sample/storage/sampleOutEdit.jsp");
	}
	
	public void showSampleOutItemList(String handleMethod, String outId)
			throws Exception {
		putObjToContext("outId", outId);
		putObjToContext("handlemethod", handleMethod);
	}
	
	
	@Action(value = "delSampleOutItems")
	public void delSampleOutItems() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sampleOutService.delSampleOutItem(ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	
	/**
	 * 保存明细
	 */
	@Action(value = "saveSampleOutItemTable")
	public void saveSampleOutItemTable() throws Exception {
		String sampleOut_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sampleOutService.saveStorageOutItemTable(sampleOut_id, item,logInfo);
			result.put("success", true);
			result.put("id", sampleOut_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
}
