package com.biolims.sample.storage.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 返库明细
 * @author lims-platform
 * @date 2015-11-03 16:20:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_RETURN_ITEM")
@SuppressWarnings("serial")
public class SampleReturnItem extends EntityDao<SampleReturnItem> implements java.io.Serializable {
	/**返库明细id*/
	private String id;
	/**核对*/
	private String check;
	/**描述*/
	private String name;
	/**储位*/
	private String location;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/**相关主表*/
	private SampleReturn sampleReturn;
	/**
	 *方法: 取得String
	 *@return: String  返库明细id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  返库明细id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  核对
	 */
	@Column(name ="CHECKED", length = 50)
	public String getCheck(){
		return this.check;
	}
	/**
	 *方法: 设置String
	 *@param: String  核对
	 */
	public void setCheck(String check){
		this.check = check;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  储位
	 */
	@Column(name ="LOCATION", length = 50)
	public String getLocation(){
		return this.location;
	}
	/**
	 *方法: 设置String
	 *@param: String  储位
	 */
	public void setLocation(String location){
		this.location = location;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得SampleReturn
	 *@return: SampleReturn  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_RETURN")
	public SampleReturn getSampleReturn(){
		return this.sampleReturn;
	}
	/**
	 *方法: 设置SampleReturn
	 *@param: SampleReturn  相关主表
	 */
	public void setSampleReturn(SampleReturn sampleReturn){
		this.sampleReturn = sampleReturn;
	}
}