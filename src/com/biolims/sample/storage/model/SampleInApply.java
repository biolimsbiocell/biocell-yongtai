package com.biolims.sample.storage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.technology.wk.model.TechJkServiceTask;
/**   
 * @Title: Model
 * @Description: 入库申请
 * @author lims-platform
 * @date 2015-11-03 16:19:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_IN_APPLY")
@SuppressWarnings("serial")
public class SampleInApply extends EntityDao<SampleInApply> implements java.io.Serializable {
	/**样本入库id*/
	private String id;
	/**编号*/
	private String code;
	/**描述*/
	private String name;
	/**所属任务单*/
	private DicType taskCode;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**审核人*/
	private User acceptUser;
	/**审核日期*/
	private Date acceptDate;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	// 科技服务
		private TechJkServiceTask techJkServiceTask;
	/**
	 *方法: 取得String
	 *@return: String  样本入库id
	 */
		@ManyToOne(fetch = FetchType.LAZY)
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name = "TECH_JK_SERVICE_TASK")
		public TechJkServiceTask getTechJkServiceTask() {
			return techJkServiceTask;
		}

		public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
			this.techJkServiceTask = techJkServiceTask;
		}
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本入库id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得MODEL
	 *@return: MODEL  所属任务单
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TASK_CODE")
	public DicType getTaskCode() {
		return taskCode;
	}
	/**
	 *方法: 设置MODEL
	 *@param: MODEL  所属任务单
	 */
	
	public void setTaskCode(DicType taskCode) {
		this.taskCode = taskCode;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 100)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser(){
		return this.acceptUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setAcceptUser(User acceptUser){
		this.acceptUser = acceptUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审核日期
	 */
	@Column(name ="ACCEPT_DATE", length = 50)
	public Date getAcceptDate(){
		return this.acceptDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审核日期
	 */
	public void setAcceptDate(Date acceptDate){
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}