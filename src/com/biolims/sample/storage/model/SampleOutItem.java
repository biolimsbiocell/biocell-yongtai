package com.biolims.sample.storage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.express.model.ExpressCompany;

/**
 * @Title: Model
 * @Description: 出库明细
 * @author lims-platform
 * @date 2015-11-03 16:19:32
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_OUT_ITEM")
@SuppressWarnings("serial")
public class SampleOutItem extends EntityDao<SampleOutItem> implements
		java.io.Serializable {
	/** 出库明细id */
	private String id;
	/** 核对 */
	private String checked;
	/** 数据通量 */
	private String dataTraffic;
	/** 储位 */
	private String location;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 样本库存量 */
	private Double sampleNum;
	/** 样本用量 */
	private Double num;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本类型 */
	private String sampleType;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private SampleOut sampleOut;
	/** 左侧表ID */
	private String tempId;
	// 入库明细id
	private String sampleInItemId;

	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 订单编号 */
	private String orderCode;
	// 检测项目ID
	private String productId;
	// 检测项目
	private String productName;
	/** 患者姓名 */
	private String patientName;
	// 浓度
	private Double concentration;
	// 体积
	private Double volume;
	// 总量
	private Double sumTotal;
	// 客户信息
	private CrmCustomer crmCustomer;
	// 供应商信息
	private Supplier supplier;
	// 快递单号
	private String expreceCode;
	// 申请人
	private User applyUser;
	// 是否返库
	private String isDepot;
	// 快递公司
	private ExpressCompany expressCompany;
	// 来源
	private String infoFrom;

	//采样日期
	private String samplingDate;
	//条形码
	private String barCode;
	//产品类型  1科研2临床
	private String sampleStyle;
	/** scopeId*/
	private String scopeId;
	/** scopeName*/
	private String scopeName;
	/** 代次 */
	private String pronoun;
	/** 父级 */
	private String parentId;
	/**关联订单*/
	private SampleOrder sampleOrder;
	//检测项id
	private String sampleDeteyionId;
	//检测项名称
	private String sampleDeteyionName;
	private String infoFromId;
	
	
	
	public String getSampleDeteyionId() {
		return sampleDeteyionId;
	}

	public void setSampleDeteyionId(String sampleDeteyionId) {
		this.sampleDeteyionId = sampleDeteyionId;
	}

	public String getSampleDeteyionName() {
		return sampleDeteyionName;
	}

	public void setSampleDeteyionName(String sampleDeteyionName) {
		this.sampleDeteyionName = sampleDeteyionName;
	}

	public String getInfoFromId() {
		return infoFromId;
	}

	public void setInfoFromId(String infoFromId) {
		this.infoFromId = infoFromId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}


	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}
	public String getSampleStyle() {
		return sampleStyle;
	}

	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}

	
	//qPCR浓度（nM）
			private String qpcrConcentration;
			//index
			private String indexa;
			
			
			

			public String getQpcrConcentration() {
				return qpcrConcentration;
			}

			public void setQpcrConcentration(String qpcrConcentration) {
				this.qpcrConcentration = qpcrConcentration;
			}

			public String getIndexa() {
				return indexa;
			}

			public void setIndexa(String indexa) {
				this.indexa = indexa;
			}
	
	public String getInfoFrom() {
		return infoFrom;
	}

	public void setInfoFrom(String infoFrom) {
		this.infoFrom = infoFrom;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPRESS_COMPANY")
	public ExpressCompany getExpressCompany() {
		return expressCompany;
	}

	public void setExpressCompany(ExpressCompany expressCompany) {
		this.expressCompany = expressCompany;
	}

	public String getIsDepot() {
		return isDepot;
	}

	public void setIsDepot(String isDepot) {
		this.isDepot = isDepot;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SUPPLIER")
	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPLY_USER")
	public User getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(User applyUser) {
		this.applyUser = applyUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	public String getExpreceCode() {
		return expreceCode;
	}

	public void setExpreceCode(String expreceCode) {
		this.expreceCode = expreceCode;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	@Column(name = "NEXT_FLOW", length = 20)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 出库明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 出库明细id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对
	 */
	@Column(name = "CHECKED", length = 50)
	public String getChecked() {
		return this.checked;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "CODE", length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	@Column(name = "LOCATION", length = 50)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得SampleOut
	 * 
	 * @return: SampleOut 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_OUT")
	public SampleOut getSampleOut() {
		return this.sampleOut;
	}

	/**
	 * 方法: 设置SampleOut
	 * 
	 * @param: SampleOut 相关主表
	 */
	public void setSampleOut(SampleOut sampleOut) {
		this.sampleOut = sampleOut;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public String getSampleInItemId() {
		return sampleInItemId;
	}

	public void setSampleInItemId(String sampleInItemId) {
		this.sampleInItemId = sampleInItemId;
	}

	public String getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(String samplingDate) {
		this.samplingDate = samplingDate;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}