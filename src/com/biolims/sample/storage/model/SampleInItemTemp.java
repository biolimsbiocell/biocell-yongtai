package com.biolims.sample.storage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 入库中间表
 * @author lims-platform
 * @date 2015-11-03 16:19:13
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_IN_ITEM_TEMP")
@SuppressWarnings("serial")
public class SampleInItemTemp extends EntityDao<SampleInItemTemp> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 材料编号 */
	private String code;
	/** 入库量 */
	private Double num;
	/** 样本编号 */
	private String sampleCode;
	/** 储位 */
	private String location;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 相关主表 */
	// private SampleInputTechnology sampleInputTechnology;
	// 样本类型
	private String sampleType;

	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	
	private String sampleTypeId;
	// 来源
	private String infoFrom;
	// 来源id
	private String infoFromId;
	/** 患者姓名 */
	private String patientName;
	// 浓度
	private Double concentration;
	// 体积
	private Double volume;
	// 总量
	private Double sumTotal;
	// 销售
	private User sellPerson;

	/** 单位组 */
	private DicType unitGroup;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;
	// 样本收样分组号
	private String groupNo;
	// 是否有染片
	private String isRp;
	//条形码
	private String barCode;
	
	//认领状态
	private String sampleState;
	//数据通量
	private String dataTraffic;
	/** 成本中心Id */
	private String scopeId;
	/** 成本中心 */
	private String scopeName;
	
	//样本来源
	private String type;
	
	//采样日期
	private String samplingDate;
	// 检测项目
	private String productName;
	// 检测项目ID
	private String productId;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 产品类型
	private String sampleStyle;
	/** 代次 */
	private String pronoun;
	/** 父级 */
	private String parentId;
	/**关联订单*/
	private SampleOrder sampleOrder;
	//qPCR浓度（nM）
	private String qpcrConcentration;
	//index
	private String indexa;
	//检测项id
	private String sampleDeteyionId;
	//检测项名称
	private String sampleDeteyionName;
	
	
	
	public String getSampleDeteyionId() {
		return sampleDeteyionId;
	}

	public void setSampleDeteyionId(String sampleDeteyionId) {
		this.sampleDeteyionId = sampleDeteyionId;
	}

	public String getSampleDeteyionName() {
		return sampleDeteyionName;
	}

	public void setSampleDeteyionName(String sampleDeteyionName) {
		this.sampleDeteyionName = sampleDeteyionName;
	}

	public String getInfoFromId() {
		return infoFromId;
	}

	public void setInfoFromId(String infoFromId) {
		this.infoFromId = infoFromId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}

	public String getSampleStyle() {
		return sampleStyle;
	}

	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}
	
	public String getQpcrConcentration() {
		return qpcrConcentration;
	}

	public void setQpcrConcentration(String qpcrConcentration) {
		this.qpcrConcentration = qpcrConcentration;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSampleState() {
		return sampleState;
	}

	public void setSampleState(String sampleState) {
		this.sampleState = sampleState;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getIsRp() {
		return isRp;
	}

	public void setIsRp(String isRp) {
		this.isRp = isRp;
	}

	public String getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	public User getSellPerson() {
		return sellPerson;
	}

	public void setSellPerson(User sellPerson) {
		this.sellPerson = sellPerson;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	public String getPatientName() {
		return patientName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 入库明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 入库明细id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 储位
	 */
	@Column(name = "LOCATION", length = 50)
	public String getLocation() {
		return this.location;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 储位
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	// /**
	// *方法: 取得SampleIn
	// *@return: SampleIn 相关主表
	// */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "SAMPLE_IN")
	// public SampleIn getSampleIn(){
	// return this.sampleIn;
	// }
	// /**
	// *方法: 设置SampleIn
	// *@param: SampleIn 相关主表
	// */
	// public void setSampleIn(SampleIn sampleIn){
	// this.sampleIn = sampleIn;
	// }
	/**
	 * 方法: 取得sampleInputTechnology
	 * 
	 * @return: sampleInputTechnology 相关主表
	 */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "SAMPLE_INPUT_TECHNOLOGY")
	// public SampleInputTechnology getSampleInputTechnology() {
	// return sampleInputTechnology;
	// }
	/**
	 * 方法: 设置sampleInputTechnology
	 * 
	 * @param: sampleInputTechnology 相关主表
	 */
	// public void setSampleInputTechnology(SampleInputTechnology
	// sampleInputTechnology) {
	// this.sampleInputTechnology = sampleInputTechnology;
	// }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public String getSampleTypeId() {
		return sampleTypeId;
	}

	public void setSampleTypeId(String sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}

	public String getInfoFrom() {
		return infoFrom;
	}

	public void setInfoFrom(String infoFrom) {
		this.infoFrom = infoFrom;
	}
	
	public String getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(String samplingDate) {
		this.samplingDate = samplingDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}