package com.biolims.sample.storage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * @Title: Model
 * @Description: 出库申请明细
 * @author lims-platform
 * @date 2015-11-03 16:19:50
 * @version V1.0
 * 
 */
/**
 * @author Biolims
 *
 */
@Entity
@Table(name = "SAMPLE_OUT_APPLY_ITEM")
@SuppressWarnings("serial")
public class SampleOutApplyItem extends EntityDao<SampleOutApplyItem> implements
		java.io.Serializable {
	/** 申请明细id */
	private String id;
	/** 描述 */
	private String name;
	private String sampleType;
	private String code;
	private Double num;
	// 入库明细
	private String sampleInItemId;
	/** 储位 */
	private String location;
	/** 数据通量*/
	private String dataTraffic;
	private String sampleCode;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private SampleOutApply sampleOutApply;
	/** 送达时间 */
	private Date sendTime;
	/** 关联样本 */
	private SampleReceiveItem sampleReceiveItem;
	// 浓度
	private Double concentration;
	// 体积
	private Double volume;
	// 总量
	private Double sumTotal;

	/** 单位组 */
	private DicType unitGroup;
	// 运输条件
	private String ystj;
	// 返样体积
	private Double fyVolume;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	
	//采样日期
	private String samplingDate;
	// 检测项目
	private String productName;
	// 检测项目ID
	private String productId;
	// 条形码
	private String barCode;
	// 姓名/名称
	private String patientName;
	// 产品类型  1科研2临床
	private String sampleStyle;
	/** scopeId*/
	private String scopeId;
	/** scopeName*/
	private String scopeName;
	/** 父级 */
	private String parentId;
	/**关联订单*/
	private SampleOrder sampleOrder;
	/**库存类型 自体细胞库 公共细胞库 样本库*/
	private String stockType;
	/** 代次 */
	private String pronoun;
	//检测项id
	private String sampleDeteyionId;
	//检测项名称
	private String sampleDeteyionName;
	private String infoFrom;
	private String infoFromId;
	
	
	
	public String getSampleDeteyionId() {
		return sampleDeteyionId;
	}

	public void setSampleDeteyionId(String sampleDeteyionId) {
		this.sampleDeteyionId = sampleDeteyionId;
	}

	public String getSampleDeteyionName() {
		return sampleDeteyionName;
	}

	public void setSampleDeteyionName(String sampleDeteyionName) {
		this.sampleDeteyionName = sampleDeteyionName;
	}

	public String getInfoFrom() {
		return infoFrom;
	}

	public void setInfoFrom(String infoFrom) {
		this.infoFrom = infoFrom;
	}

	public String getInfoFromId() {
		return infoFromId;
	}

	public void setInfoFromId(String infoFromId) {
		this.infoFromId = infoFromId;
	}

	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}

	public String getStockType() {
		return stockType;
	}

	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getSampleStyle() {
		return sampleStyle;
	}

	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}
	
	//qPCR浓度（nM）
			private String qpcrConcentration;
			//index
			private String indexa;

			public String getQpcrConcentration() {
				return qpcrConcentration;
			}

			public void setQpcrConcentration(String qpcrConcentration) {
				this.qpcrConcentration = qpcrConcentration;
			}

			public String getIndexa() {
				return indexa;
			}

			public void setIndexa(String indexa) {
				this.indexa = indexa;
			}
		
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	public String getYstj() {
		return ystj;
	}

	public void setYstj(String ystj) {
		this.ystj = ystj;
	}

	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}

	public Double getFyVolume() {
		return fyVolume;
	}

	public void setFyVolume(Double fyVolume) {
		this.fyVolume = fyVolume;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 申请明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 申请明细id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "CODE", length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得SampleOutApply
	 * 
	 * @return: SampleOutApply 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_OUT_APPLY")
	public SampleOutApply getSampleOutApply() {
		return this.sampleOutApply;
	}

	/**
	 * 方法: 设置SampleOutApply
	 * 
	 * @param: SampleOutApply 相关主表
	 */
	public void setSampleOutApply(SampleOutApply sampleOutApply) {
		this.sampleOutApply = sampleOutApply;
	}

	/**
	 * 方法：获取SendTime
	 * 
	 * @return Date 送达时间
	 */
	@Column(name = "SEND_TIME", length = 50)
	public Date getSendTime() {
		return sendTime;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	/**
	 * 方法：设置SendTime
	 * 
	 * @param sendTime
	 *            Date 送达时间
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_RECEIVE_ITEM")
	public SampleReceiveItem getSampleReceiveItem() {
		return sampleReceiveItem;
	}

	public void setSampleReceiveItem(SampleReceiveItem sampleReceiveItem) {
		this.sampleReceiveItem = sampleReceiveItem;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "LOCATION_", length = 50)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getSampleInItemId() {
		return sampleInItemId;
	}

	public void setSampleInItemId(String sampleInItemId) {
		this.sampleInItemId = sampleInItemId;
	}

	public String getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(String samplingDate) {
		this.samplingDate = samplingDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
	
}