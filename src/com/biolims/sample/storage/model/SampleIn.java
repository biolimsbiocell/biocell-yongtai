package com.biolims.sample.storage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 样本入库
 * @author lims-platform
 * @date 2015-11-03 16:19:26
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_IN")
@SuppressWarnings("serial")
public class SampleIn extends EntityDao<SampleIn> implements
		java.io.Serializable {
	/** 样本入库id */
	private String id;
	/** 编号 */
	private String code;
	/** 描述 */
	private String name;
	/** 储位提示 */
	private String location;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 审核人 */
	private User acceptUser;
	/** 审核日期 */
	private Date acceptDate;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 入库申请单 */
	private SampleInApply sampleInApply;
	/** 样本类型 */
	private String sampleType;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 业务类型 */
	private WorkType businessType;
	/** 所属任务 */
	private WorkOrder workOrder;
	// 成本中心Id
	private String scopeId;
	// 成本中心
	private String scopeName;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本入库id
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本入库id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 100)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审核人
	 */
	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审核日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审核日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_IN_APPLY")
	public SampleInApply getSampleInApply() {
		return sampleInApply;
	}

	public void setSampleInApply(SampleInApply sampleInApply) {
		this.sampleInApply = sampleInApply;
	}

	/**
	 * 方法：获取SampleType
	 * 
	 * @return String 样本类型
	 */

	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "SAMPLE_TYPE")
	// public DicType getSampleType() {
	// return sampleType;
	// }
	// /**
	// * 方法：设置SampleType
	// * @param sampleType DicType 样本类型
	// */
	// public void setSampleType(DicType sampleType) {
	// this.sampleType = sampleType;
	// }
	/**
	 * 方法：获取BusinessType
	 * 
	 * @return String 业务类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSINESS_TYPE_ID")
	public WorkType getBusinessType() {
		return businessType;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public void setBusinessType(WorkType businessType) {
		this.businessType = businessType;
	}

	/**
	 * 方法：设置BusinessType
	 * 
	 * @param businessType
	 *            WorkType 业务类型
	 */

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_ORDER")
	public WorkOrder getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}