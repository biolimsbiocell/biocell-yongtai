package com.biolims.sample.storage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 出库申请
 * @author lims-platform
 * @date 2015-11-03 16:20:05
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_OUT_APPLY")
@SuppressWarnings("serial")
public class SampleOutApply extends EntityDao<SampleOutApply> implements
		java.io.Serializable {
	/** 样本出库申请id */
	private String id;
	/** 编号 */
	private String code;
	/** 描述 */
	private String name;
	/** 所属任务单 */
	private String taskCode;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 审核人 */
	private User acceptUser;
	/** 审核日期 */
	private Date acceptDate;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 样本类型 */
	private DicType sampleType;
	/** 业务类型 */
	private WorkType businessType;
	/** 所属任务 */
	private WorkOrder workOrder;
	/** 供应商 */
	private Supplier supplier;
	// 选择类型
	private DicType type;
	// 客户
	private CrmCustomer customer;
	// 取样人
	private User obtain;
	// 返回客户: 期望返样时间
	private Date fyDate;
	
	
	private String scopeId;
	private String scopeName;
	
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	// 选择类型
	private DicType experimentType;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPERIMENT_TYPE")
	public DicType getExperimentType() {
		return experimentType;
	}

	public void setExperimentType(DicType experimentType) {
		this.experimentType = experimentType;
	}

	public Date getFyDate() {
		return fyDate;
	}

	public void setFyDate(Date fyDate) {
		this.fyDate = fyDate;
	}

	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CUSTOMER")
	public CrmCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CrmCustomer customer) {
		this.customer = customer;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本出库申请id
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本出库申请id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 所属任务单
	 */
	@Column(name = "TASK_CODE", length = 100)
	public String getTaskCode() {
		return this.taskCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 所属任务单
	 */
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 100)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审核人
	 */
	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审核日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审核日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法：获取SampleType
	 * 
	 * @return String 样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}

	/**
	 * 方法：设置SampleType
	 * 
	 * @param sampleType
	 *            String 样本类型
	 */
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSINESS_TYPE")
	public WorkType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(WorkType businessType) {
		this.businessType = businessType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_ORDER")
	public WorkOrder getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SUPPLIER")
	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	/**
	 * @return the obtain
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OBTAIN")
	public User getObtain() {
		return obtain;
	}

	/**
	 * @param obtain
	 *            the obtain to set
	 */
	public void setObtain(User obtain) {
		this.obtain = obtain;
	}

}