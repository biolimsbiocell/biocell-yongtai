package com.biolims.sample.storage.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.sample.storage.service.SampleReturnService;

public class SampleReturnEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleReturnService srService = (SampleReturnService) ctx.getBean("sampleReturnService");
		srService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
