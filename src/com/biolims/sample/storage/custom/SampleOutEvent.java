package com.biolims.sample.storage.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.sample.storage.service.SampleOutService;

public class SampleOutEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleOutService soService = (SampleOutService) ctx.getBean("sampleOutService");
		soService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
