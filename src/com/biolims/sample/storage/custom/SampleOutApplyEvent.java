package com.biolims.sample.storage.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.sample.storage.service.SampleOutApplyService;

public class SampleOutApplyEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleOutApplyService soaService = (SampleOutApplyService) ctx.getBean("sampleOutApplyService");
		soaService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
