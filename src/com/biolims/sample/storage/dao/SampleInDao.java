package com.biolims.sample.storage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.system.storage.model.StorageBox;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本入库
 * 
 * @author admin
 * 
 */
@Repository
@SuppressWarnings("unchecked")
public class SampleInDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleIn where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		// 集团的查询条件
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleIn> list = new ArrayList<SampleIn>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSampleInItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleInItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleIn.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInItem> list = new ArrayList<SampleInItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 库存样本
	 */
	public Map<String, Object> selectSampleInItemLInfoist(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String dicSampleType)
			throws Exception {
		String hql = "from SampleInItem where 1=1 and state='1' ";
		String key = "";
		String sampletype = "";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		if (dicSampleType != null && !"".equals(dicSampleType)) {
			String ids[] = dicSampleType.split(",");
			if (ids.length > 1) {
				sampletype = " and ((dic_sample_type not in('" + ids[0]
						+ "', '" + ids[1] + "' )) or dic_sample_type is null) ";
			} else {
				sampletype = " and dic_sample_type = '" + ids[0] + "'";
			}

		}
		// else {
		// key = " and num <> 0";
		// }
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key + sampletype)
				.uniqueResult();
		List<SampleInItem> list = new ArrayList<SampleInItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key + sampletype)
						.list();
			} else {
				list = this.getSession().createQuery(hql + key + sampletype)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 库存样本
	 */
	public Map<String, Object> selectSampleInfoInist(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String dicSampleType)
			throws Exception {
		String hql = "from SampleInfoIn where 1=1 and state='1' ";
		String key = "";
		String sampletype = "";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		// if(dicSampleType != null && !"".equals(dicSampleType)){
		// String ids[] = dicSampleType.split(",");
		// if(ids.length>1){
		// sampletype =
		// " and ((dic_sample_type not in('"+ids[0]+"', '"+ids[1]+"' )) or dic_sample_type is null) ";
		// }else{
		// sampletype = " and dic_sample_type = '"+ids[0] +"'" ;
		// }
		//
		// }
		// else {
		// key = " and num <> 0";
		// }
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key + sampletype)
				.uniqueResult();
		List<SampleInItem> list = new ArrayList<SampleInItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key + sampletype)
						.list();
			} else {
				list = this.getSession().createQuery(hql + key + sampletype)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 可出库样本
	 */
	public Map<String, Object> selectSampleWaitOutItemList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String cid, String type)
			throws Exception {
		String hql = "";

		if ("".equals(cid)) {
			hql = "from SampleInfoIn where 1=1 and state='1'";
		}
		if (!"".equals(cid)) {
			hql = "from SampleInfoIn where 1=1 and state='1' and customer.id='"
					+ cid + "'";
		}

		if ("2".equals(type) && "".equals(cid)) {
			hql = "from SampleInfoIn where 1=1 and state='1' and techJkServiceTask is not null ";
		}
		if ("2".equals(type) && !"".equals(cid)) {
			hql = "from SampleInfoIn where 1=1 and state='1' and customer.id='"
					+ cid + "' and techJkServiceTask is not null ";
		}

		String key = "";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		// else {
		// key = " and num <> 0";
		// }
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfoIn> list = new ArrayList<SampleInfoIn>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSampleWaitOutItemList(String cid,
			Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		String countHql = "";

		if ("".equals(cid) || cid == null) {
			countHql = "from SampleInfoIn where 1=1 and state='1'";
		}
		if (!"".equals(cid) && cid != null) {
			countHql = "from SampleInfoIn where 1=1 and state='1' and customer.id='"
					+ cid + "'";
		}

		if ("2".equals(type) && ("".equals(cid) || cid == null)) {
			countHql = "from SampleInfoIn where 1=1 and state='1' and techJkServiceTask is not null ";
		}
		if ("2".equals(type) && !"".equals(cid) && cid != null) {
			countHql = "from SampleInfoIn where 1=1 and state='1' and customer.id='"
					+ cid + "' and techJkServiceTask is not null ";
		}

		Map<String, Object> map = new HashMap<String, Object>();
		String key = "";
		if (query != null && !"".equals(query)) {
			key = key + map2Where(query);
		}
		// 集团的查询条件
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		if (!"".equals(type) && type != null) {
			key = key + " and type='" + type + "' ";
		}

		Long sumCount = (Long) getSession().createQuery(
				"select count(*) " + countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(
					"select count(*) " + countHql + key).uniqueResult();
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInfoIn> list = getSession().createQuery(countHql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	/**
	 * 根据主表ID查询明细表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public List<SampleInItem> selectSampleInItemListById(String scId)
			throws Exception {
		String hql = "from SampleInItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleIn.id='" + scId + "'";
		List<SampleInItem> list = new ArrayList<SampleInItem>();
		list = this.getSession().createQuery(hql + key).list();
		if (list.size() > 0)
			return list;
		return null;
	}

	// 样本入库左侧
	public Map<String, Object> selectSampleInLeftList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleInItemTemp where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInItemTemp> list = new ArrayList<SampleInItemTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询已完成入库的样本且状态为“1”
	 * 
	 * @param code
	 * @return
	 */
	public List<SampleInItem> getSampleInState(String code) {
		String hql = "from SampleInItem t where 1=1 and t.sampleCode='"
				+ code
				+ "' and t.sampleIn in (select id from SampleIn where state='1')";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据主表ID查询子表集合
	 */
	public List<SampleInItem> getSampleInItemList(String id) {
		String hql = "from SampleInItem t where t.sampleIn='" + id + "'";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInItem> getSampleInItemByCode(String code, String location) {
		String hql = "from SampleInItem t where 1=1 and t.code='" + code
				+ "' and t.location ='" + location + "' and t.state='1'";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();

		return list;
	}

	public SampleInfoIn getSampleInItemByCode(String code) {
		String hql = "from SampleInfoIn t where 1=1 and t.code='" + code + "'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 根据储位盒查找位置中的样本
	 * 
	 */
	public List<SampleInfoIn> getSampleInItemByStorage(String stId) {
		String hql = "from SampleInfoIn t where 1=1 and t.location like '%"
				+ stId + "%'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list;
		return null;
	}

	public Long getSampleInItemByCodeState(String stId) {
		String hql = "from SampleInfoIn t where 1=1 and state <>'0' and t.code = '"
				+ stId + "'";
		Long l = (Long) this.getSession()
				.createQuery(" select count(*) " + hql).uniqueResult();

		return l;
	}

	/**
	 * 查询已完成入库的样本且状态为“1” 最后两位是like code的样本
	 * 
	 * @param scode
	 * @param code
	 * @return
	 */
	public SampleInItem getSampleInItemByMark(String scode, String code) {
		String hql = "from SampleInItem t where 1=1 and t.sampleCode='"
				+ scode
				+ "' and code like '%"
				+ code
				+ "'  and t.sampleIn in (select id from SampleIn where state='1')";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		else
			return null;
	}

	public SampleInItem selectTempByCode(String code) throws Exception {
		String hql = "from SampleInItem where code = '" + code
				+ "' and state='1'";
		SampleInItem list = (SampleInItem) this.getSession().createQuery(hql)
				.uniqueResult();
		return list;
	}

	public Map<String, Object> findSampleInfoInTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleInItem where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		 String scopeId=(String)
		 ActionContext.getContext().getSession().get("scopeId");
		 if(!"all".equals(scopeId)){
		 key+=" and scopeId='"+scopeId+"'";
		 }

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleInItem where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<StorageBox> selectStorageBoxByLocation(String location) {
		String hql = " from StorageBox where 1=1 and newLocationId ='"
				+ location + "'";
		List<StorageBox> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInfoIn> findSampleByBox(String boxId) {
		String hql = " from SampleInfoIn where 1=1 and storageBox.id ='"
				+ boxId + "'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInfoIn> selectSampleInfoInByCode(String code) {
		String hql = " from SampleInfoIn where 1=1 and code ='" + code + "'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInItemTemp> selectSampleInItemTempByCode(String code) {
		String hql = " from SampleInItemTemp where 1=1 and code ='" + code
				+ "'";
		List<SampleInItemTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public String selectPicCode(String sampleTypeId) {
		String hql = " select pic_code from dic_sample_type t where 1=1 and t.id='"
				+ sampleTypeId + "'";
		String picCode = this.getSession().createSQLQuery(hql).uniqueResult()
				.toString();
		return picCode;
	}
}