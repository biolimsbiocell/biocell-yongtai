package com.biolims.sample.storage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.model.SampleOutApplyItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本出库申请
 * 
 * @author admin
 * 
 */
@Repository
@SuppressWarnings("unchecked")
public class SampleOutApplyDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleOutApplyList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleOutApply where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleOutApply> list = new ArrayList<SampleOutApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/** 根据主表ID查询子表明细 */
	public List<SampleOutApplyItem> selectSampleItemById(String id)
			throws Exception {
		String key = " ";
		String hql = " from SampleOutApplyItem where 1=1 ";
		List<SampleOutApplyItem> list = new ArrayList<SampleOutApplyItem>();
		if (id != null && !id.equals("")) {
			key = "and sampleOutApply.id = '" + id + "'";
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Map<String, Object> selectSampleOutApplyItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleOutApplyItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleOutApply.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOutApplyItem> list = new ArrayList<SampleOutApplyItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 选择打包单加载子表信息
	public Map<String, Object> setSampleOutItemList(String code)
			throws Exception {
		String hql = "from SampleOutApplyItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.sampleOutApply like '%" + code + "%'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOutApplyItem> list = this.getSession()
				.createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> findSampleInfoInTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleInfoIn where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		// 集团的查询条件
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleInfoIn  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInfoIn> list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOutApplyItemTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOutApplyItem where 1=1 and sampleOutApply.id='"
				+ scId + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleOutApplyItem  where 1=1  and sampleOutApply.id='"
					+ scId + "'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOutApplyItem> list = this.getSession()
					.createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleOutApplyTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOutApply where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleOutApply where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOutApply> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}