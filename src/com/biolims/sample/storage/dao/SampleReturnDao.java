package com.biolims.sample.storage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.storage.model.SampleReturn;
import com.biolims.sample.storage.model.SampleReturnItem;

@Repository
@SuppressWarnings("unchecked")
public class SampleReturnDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleReturnList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleReturn where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleReturn> list = new ArrayList<SampleReturn>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSampleReturnItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleReturnItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleReturn.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleReturnItem> list = new ArrayList<SampleReturnItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}