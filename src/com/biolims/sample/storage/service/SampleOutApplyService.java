package com.biolims.sample.storage.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleOutApplyDao;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.model.SampleOutApplyItem;
import com.biolims.sample.storage.model.SampleOutTaskId;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本出库申请
 * 
 * @author admin
 * 
 */
@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleOutApplyService {
	@Resource
	private SampleOutApplyDao sampleOutApplyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;

	@Resource
	private SystemCodeService systemCodeService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleOutApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleOutApplyDao.selectSampleOutApplyList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOutApply i) throws Exception {

		sampleOutApplyDao.saveOrUpdate(i);

	}

	public SampleOutApply get(String id) {
		SampleOutApply sampleOutApply = commonDAO.get(SampleOutApply.class, id);
		return sampleOutApply;
	}

	public Map<String, Object> findSampleOutApplyItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleOutApplyDao.selectSampleOutApplyItemList(scId, startNum, limitNum, dir,
				sort);
		List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOutApplyItem(SampleOutApply sc, String itemDataJson) throws Exception {
		List<SampleOutApplyItem> saveItems = new ArrayList<SampleOutApplyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> map : list) {
			SampleOutApplyItem scp = new SampleOutApplyItem();
			// 将map信息读入实体类
			scp = (SampleOutApplyItem) sampleOutApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleOutApply(sc);
			// 改变出库样本状态
			SampleInfoIn out = commonDAO.get(SampleInfoIn.class, scp.getSampleInItemId());
			out.setState("0");
			commonDAO.saveOrUpdate(out);

			/*
			 * sampleStateService.saveSampleState1(scp.getSampleOrder(), scp.getCode(),
			 * scp.getSampleCode(), null, null, "", format.format(sc.getCreateDate()),
			 * format.format(new Date()), "SampleOutApplyItem", "出库申请明细", (User)
			 * ServletActionContext.getRequest().getSession()
			 * .getAttribute(SystemConstants.USER_SESSION_KEY), sc.getId(), null, "", null,
			 * null, null, null, null, null, null, null);
			 */
			saveItems.add(scp);
		}
		sampleOutApplyDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOutApplyItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOutApplyItem scp = sampleOutApplyDao.get(SampleOutApplyItem.class, id);
			sampleOutApplyDao.delete(scp);
			// 改变出库样本状态
			SampleInfoIn out = commonDAO.get(SampleInfoIn.class, scp.getSampleInItemId());
			out.setState("1");
			String ck = "出库申请:" + "ID:'" + id + "',样本编号:'" + scp.getCode() + "'已删除";
			if (id != null && !"".equals(id)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("SampleOutApply");
				li.setFileId(scp.getSampleOutApply().getId());
				li.setModifyContent(ck);
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
			commonDAO.saveOrUpdate(out);
		}
	}

	// 根据申请单加载子表
	public Map<String, Object> showSampleOutItemList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sampleOutApplyDao.setSampleOutItemList(code);
		List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result.get("list");

		// if (list != null && list.size() > 0) {
		// for (SampleOutApplyItem srai : list) {
		// Double num = srai.getNum();
		// Map<String, String> map = new HashMap<String, String>();
		// map.put("id", srai.getId());
		// map.put("sampleCode", srai.getSampleCode());
		// map.put("code", srai.getCode());
		// map.put("sampleType", srai.getSampleType());
		// map.put("location", srai.getLocation());// 得到储位
		// if (num != null) {
		// String numStr = num.toString();
		// map.put("num", numStr);// 得到样本存储量
		// }
		// mapList.add(map);
		// }
		// }
		return result;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOutApply sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleOutApplyDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleOutApplyItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleOutApplyItem(sc, jsonStr);
			}
		}
	}

	// 审批完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		SampleOutApply sr = sampleOutApplyDao.get(SampleOutApply.class, id);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		// sr.setConfirmUser(user);
		// sr.setConfirmDate(new Date());
		List<SampleOutApplyItem> soaiList = sampleOutApplyDao.selectSampleItemById(sr.getId());
		if (soaiList.size() > 0) {
			for (SampleOutApplyItem soai : soaiList) {
				SampleOutTemp sot = new SampleOutTemp();
				sot.setStockType(soai.getStockType());
				sot.setPronoun(soai.getPronoun());
				sot.setCode(soai.getCode());
				sot.setSampleType(soai.getSampleType());
				sot.setSampleInItemId(soai.getSampleInItemId());
				sot.setSampleCode(soai.getSampleCode());
				sot.setLocation(soai.getLocation());
				sot.setNum(soai.getNum());
				sot.setConcentration(soai.getConcentration());
				sot.setVolume(soai.getVolume());
				sot.setSumTotal(soai.getSumTotal());
				sot.setProductId(soai.getProductId());
				sot.setProductName(soai.getProductName());
				sot.setSamplingDate(soai.getSamplingDate());
				sot.setBarCode(soai.getBarCode());
				sot.setDataTraffic(soai.getDataTraffic());
				sot.setState("1");
				sot.setApplyUser(sr.getCreateUser());
				sot.setCrmCustomer(soai.getSampleOutApply().getCustomer());
//				sot.setType(sr.getType());
				sot.setSupplier(sr.getSupplier());
				sot.setTaskId(id);
				sot.setPatientName(soai.getPatientName());
				sot.setSampleStyle(soai.getSampleStyle());
				sot.setInfoFrom(soai.getInfoFrom());
				sot.setTechJkServiceTask(soai.getTechJkServiceTask());
				sot.setQpcrConcentration(soai.getQpcrConcentration());
				sot.setIndexa(soai.getIndexa());
				sot.setScopeId(soai.getScopeId());
				sot.setScopeName(soai.getScopeName());
				if (soai.getSampleOrder() != null) {
					sot.setSampleOrder(commonDAO.get(SampleOrder.class, soai.getSampleOrder().getId()));
				}
				
				
				sot.setSampleDeteyionId(soai.getSampleDeteyionId());
				sot.setSampleDeteyionName(soai.getSampleDeteyionName());
				sot.setInfoFromId(soai.getInfoFromId());
				commonDAO.saveOrUpdate(sot);
				// SampleInItem in = commonDAO.get(SampleInItem.class,
				// soai.getSampleInItemId());
				// in.setState("0");
				// commonDAO.update(in);
				if (soai.getSampleInItemId() != null) {
					SampleInfoIn out = commonDAO.get(SampleInfoIn.class, soai.getSampleInItemId());
					out.setState("0");
					commonDAO.saveOrUpdate(out);
				}

				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if (soai.getSampleOrder() != null) {
					soai.getSampleOrder().setNewTask(id);
				}
				sampleStateService.saveSampleState1(soai.getSampleOrder(), soai.getCode(), soai.getSampleCode(),
						soai.getProductId(), soai.getProductName(), "", sr.getCreateDate().toString(),
						format.format(new Date()), "CellPrimaryCulture", "出库申请",
						(User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY),
						id, null, null, null, null, null, null, null, null, null, null);
			}
		}
		// 添加到待出库任务单
		SampleOutTaskId soti = new SampleOutTaskId();
		soti.setTaskId(id);
		soti.setName("出库申请");
		soti.setCreateUser(sr.getCreateUser());
		soti.setCreateDate(new Date());
		soti.setState("1");
		soti.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
//		soti.setType(sr.getType());
		commonDAO.saveOrUpdate(soti);
		sampleOutApplyDao.update(sr);
	}

	public Map<String, Object> selSampleInfoInTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOutApplyDao.findSampleInfoInTable(start, length, query, col, sort);
	}

	public Map<String, Object> selSampleOutApplyItemTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return sampleOutApplyDao.findSampleOutApplyItemTable(scId, start, length, query, col, sort);
	}

	public Map<String, Object> selSampleOutApplyTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOutApplyDao.findSampleOutApplyTable(start, length, query, col, sort);
	}

	public SampleOutApply findSampleOutApplyById(String outApplyId) {
		return sampleOutApplyDao.get(SampleOutApply.class, outApplyId);
	}

	// 主数据内选择数据添加到入库明细表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addSampleOutApplyItem(String[] ids, String note, String type, String storageOutApplyId,
			String createUser, String createDate) throws Exception {

		// 保存主表信息
		SampleOutApply soa = new SampleOutApply();
		if (storageOutApplyId == null || storageOutApplyId.length() <= 0 || "NEW".equals(storageOutApplyId)) {
			String code = systemCodeService.getCodeByPrefix("SampleOutApply",
					"MI" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
			soa.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User u = commonDAO.get(User.class, createUser);

			DicType d = commonDAO.get(DicType.class, type);
			soa.setCreateUser(u);
			soa.setExperimentType(d);
			soa.setCreateDate(sdf.parse(createDate));
			soa.setState("3");
			soa.setStateName("NEW");
			soa.setName(note);
			soa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			soa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			storageOutApplyId = soa.getId();
			commonDAO.saveOrUpdate(soa);
		} else {
			soa = commonDAO.get(SampleOutApply.class, storageOutApplyId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			// 通过id查询库存样本
			SampleInfoIn sii = commonDAO.get(SampleInfoIn.class, id);
			SampleOutApplyItem soai = new SampleOutApplyItem();
			// 将数据放到出库申请明细表
			soai.setSampleOutApply(soa);
			soai.setCode(sii.getCode());
			soai.setSampleCode(sii.getSampleCode());
			soai.setSampleInItemId(sii.getId());
			soai.setLocation(sii.getLocation());
			soai.setBarCode(sii.getBarCode());
			soai.setDataTraffic(sii.getDataTraffic());
			soai.setProductId(sii.getProductId());
			soai.setProductName(sii.getProductName());
			soai.setSamplingDate(sii.getSamplingDate());
			soai.setSampleType(sii.getSampleType());
			soai.setConcentration(sii.getConcentration());
			soai.setBarCode(sii.getBarCode());
			soai.setVolume(sii.getVolume());
			soai.setSumTotal(sii.getSumTotal());
			soai.setNote(sii.getNote());
			soai.setScopeId(sii.getScopeId());
			soai.setScopeName(sii.getScopeName());
			sii.setState("2");
			soai.setStockType(sii.getStockType());
			if (sii.getSampleOrder() != null) {
				soai.setSampleOrder(commonDAO.get(SampleOrder.class, sii.getSampleOrder().getId()));
			}
			
			
			soai.setSampleDeteyionId(sii.getSampleDeteyionId());
			soai.setSampleDeteyionName(sii.getSampleDeteyionName());
			soai.setInfoFrom(sii.getInfoFrom());
			soai.setInfoFromId(sii.getInfoFromId());
			
			
			soai.setPronoun(sii.getPronoun());
			String ck = "出库申请:"
						+"样本编号"+sii.getCode()
						+",原始样本编号"+sii.getSampleCode()
						+",样本位置为"+sii.getLocation()
						+"的样本已添加到明细";
			if (id != null && !"".equals(id)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(soa.getId());
				li.setClassName("SampleOutApply");
				li.setModifyContent(ck);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
			commonDAO.saveOrUpdate(soai);
			commonDAO.saveOrUpdate(sii);
		}
		return storageOutApplyId;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public SampleOutApply saveSampleOutApplyById(SampleOutApply soa, String id, String type, String note,
			String logInfo) {
		SampleOutApply newSoa = commonDAO.get(SampleOutApply.class, id);

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(id);
			if(id.equals("NEW")) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			li.setClassName("SampleOutApply");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		newSoa.setName(note);
		DicType d = commonDAO.get(DicType.class, type);
		newSoa.setType(d);
		newSoa.setExperimentType(d);
		commonDAO.saveOrUpdate(newSoa);
		return newSoa;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOutApplyAndItem(SampleOutApply newSoa, Map jsonMap, String logInfo, String logInfoItem)
			throws Exception {
		if (newSoa != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(newSoa.getId());
				li.setClassName("SampleOutApply");
				li.setModifyContent(logInfo);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("ImteJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveItem(newSoa, jsonStr, logInfoItem);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveItem(SampleOutApply newSoa, String itemDataJson, String logInfo) throws Exception {
		List<SampleOutApplyItem> saveItems = new ArrayList<SampleOutApplyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOutApplyItem scp = new SampleOutApplyItem();
			// 将map信息读入实体类
			scp = (SampleOutApplyItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleOutApply(newSoa);

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSoa.getId());
			li.setClassName("SampleOutApply");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}
}
