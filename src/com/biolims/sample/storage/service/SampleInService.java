package com.biolims.sample.storage.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.dao.SampleOutDao;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOutTaskId;
import com.biolims.storage.container.dao.ContainerDao;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.service.StorageBoxService;
import com.biolims.util.JsonUtils;

/**
 * 样本入库
 * 
 * @author admin
 * 
 */
@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleInService {
	@Resource
	private SampleInDao sampleInDao;
	
	@Resource
	private SampleOutDao sampleOutDao;
	
	
	
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private ContainerDao containerDao;
	@Resource
	private StoragePositionDao storagePositionDao;

	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private StoragePositionService storagePositionService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private SampleInputService sampleInputService;
	@Autowired
	private StorageBoxService storageBoxService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleInList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleInDao.selectSampleInList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	public Map<String, Object> findSampleInLeftList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleInDao.selectSampleInLeftList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleIn i) throws Exception {

		sampleInDao.saveOrUpdate(i);

	}

	public SampleIn get(String id) {
		SampleIn sampleIn = commonDAO.get(SampleIn.class, id);
		return sampleIn;
	}

	public Map<String, Object> findSampleInItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleInDao.selectSampleInItemList(scId,
				startNum, limitNum, dir, sort);
		List<SampleInItem> list = (List<SampleInItem>) result.get("list");
		return result;
	}

	public Map<String, Object> selectSampleInItemLInfoist(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort, String dicSampleType) throws Exception {
		Map<String, Object> result = sampleInDao.selectSampleInItemLInfoist(
				map2Query, startNum, limitNum, dir, sort, dicSampleType);
		List<SampleInItem> list = (List<SampleInItem>) result.get("list");
		return result;
	}

	public Map<String, Object> selectSampleInfoInist(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort, String dicSampleType) throws Exception {
		Map<String, Object> result = sampleInDao.selectSampleInfoInist(
				map2Query, startNum, limitNum, dir, sort, dicSampleType);
		List<SampleInItem> list = (List<SampleInItem>) result.get("list");
		return result;
	}
	
	public Map<String, Object> findSampleWaitOutItemList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort, String cid,String type) throws Exception {
		Map<String, Object> result = sampleInDao.selectSampleWaitOutItemList(
				map2Query, startNum, limitNum, dir, sort, cid,type);
		List<SampleInfoIn> list = (List<SampleInfoIn>) result.get("list");
		return result;
	}

	public Map<String, Object> findSampleWaitOutItemList(String cid,
			Integer start, Integer length,
			String query, String col, String sort, String type) throws Exception {
		return sampleInDao.selectSampleWaitOutItemList(cid,start, length, query, col, sort,
				 type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleInItem(SampleIn sc, String itemDataJson)
			throws Exception {
		List<SampleInItem> saveItems = new ArrayList<SampleInItem>();
		List<SampleInItem> saveItemsNotNull = new ArrayList<SampleInItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> map : list) {
			SampleInItem scp = new SampleInItem();
			// 将map信息读入实体类
			scp = (SampleInItem) sampleInDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleIn(sc);

			if(scp.getSampleOrder()!=null) {
				scp.getSampleOrder().setNewTask(sc.getId());
			}
			sampleStateService.saveSampleState1(scp.getSampleOrder(),null, null, null, null, "",
					format.format(sc.getCreateDate()),
					format.format(new Date()), "SampleReturnItem", "返库明细",
					(User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY),
					sc.getId(), null, null, null, null, null, null, null, null,
					null, null);
			saveItems.add(scp);
			
			// 改变左侧样本状态
			SampleInItemTemp st = commonDAO.get(SampleInItemTemp.class, scp.getTempId());
			if (st != null) {
				st.setState("2");
			}

			if (scp.getLocation() == null
					|| (scp.getLocation() != null && scp.getLocation().equals(
							""))) {
				saveItemsNotNull.add(scp);
			}

		}
		// 盒子类型
		StorageContainer subSc = null;
		// 架子类型
		StorageContainer upSc = null;

		// 获得存储位置提示
		String oldlocation = sc.getLocation();

		// 解到数组
		String[] a = oldlocation.split("-");

		if (a.length == 2) {
			String parentLocation = a[0];
			StorageBox sp = storagePositionDao.get(StorageBox.class,
					parentLocation);
			if (sp.getOldLocationId() != null
					&& !sp.getOldLocationId().equals("")) {
				parentLocation = sp.getOldLocationId();
				oldlocation = parentLocation + "-" + a[1];
			} else {

			}

			if (sp.getStorageContainer() != null
					&& sp.getStorageContainer().getId() != null
					&& sp.getStorageContainer().getId().length() > 0) {
				subSc = sp.getStorageContainer();
			} else {
				subSc = new StorageContainer();
				subSc.setRowNum(0);
				subSc.setColNum(0);
			}
			// 孔位置
			String s = a[1];
			int s1 = 0;
			for (int i = 0; i < subSc.getRowNum(); i++) {
				for (int j = 1; j <= subSc.getColNum(); j++) {

					String spId = "";

					if (j < 10) {
						spId = parentLocation + "-"
								+ String.valueOf((char) (65 + i)) + "0"
								+ String.valueOf(j);
					} else {
						spId = parentLocation + "-"
								+ String.valueOf((char) (65 + i))
								+ String.valueOf(j);
					}

					// 假如大于起始位置
					if (spId.compareTo(oldlocation) >= 0) {
						// 设置位置
						if (containerDao.findIsUse(spId) == 0) {
							if (s1 < saveItemsNotNull.size()) {
								saveItemsNotNull.get(s1).setLocation(spId);
								s1++;
							}
						}
					}

				}
			}

		} else if(a.length == 3){
		}else{
			// 盒子位置
			String parentLocation = a[0] + "-" + a[1] + "-" + a[2];
			// 孔位置
			String parentLocation2 = a[0] + "-" + a[1] + "-" + a[2] + "-"
					+ a[3];

			List<StorageBox> l = storageBoxService
					.findStorageBoxList(parentLocation2);

			if (l.size() > 0) {

				subSc = l.get(0).getStorageContainer();
			} else {
				StoragePosition sp = storagePositionDao.get(
						StoragePosition.class, parentLocation);
				if (sp.getSubStorageContainer() != null
						&& sp.getSubStorageContainer().getId() != null
						&& sp.getSubStorageContainer().getId().length() > 0) {
					subSc = sp.getSubStorageContainer();
				} else {
					subSc = new StorageContainer();
					subSc.setRowNum(0);
					subSc.setColNum(0);
				}

				if (sp.getStorageContainer() != null
						&& sp.getStorageContainer().getId() != null
						&& sp.getStorageContainer().getId().length() > 0) {
					upSc = sp.getStorageContainer();
				} else {
					upSc = new StorageContainer();
					upSc.setRowNum(0);
					upSc.setColNum(0);
				}
			}
			int s1 = 0;
			for (int i = 0; i < subSc.getRowNum(); i++) {
				for (int j = 1; j <= subSc.getColNum(); j++) {

					String spId = "";

					if (j < 10) {
						spId = parentLocation2 + "-"
								+ String.valueOf((char) (65 + i)) + "0"
								+ String.valueOf(j);
					} else {
						spId = parentLocation2 + "-"
								+ String.valueOf((char) (65 + i))
								+ String.valueOf(j);
					}

					// 假如大于起始位置
					if (spId.compareTo(oldlocation) >= 0) {
						// 设置位置
						if (containerDao.findIsUse(spId) == 0) {
							if (s1 < saveItemsNotNull.size()) {
								saveItemsNotNull.get(s1).setLocation(spId);
								s1++;
							}
						}
					}

				}
			}
			/*
			 * // 孔位置 String s = a[3];
			 * 
			 * while (s1 < saveItemsNotNull.size()) { char[] ca =
			 * s.toCharArray(); String spId = ""; // 孔行 char ca1 =
			 * s.substring(0, 1).toCharArray()[0]; // 孔列 String ca2 =
			 * s.substring(1);
			 * 
			 * boolean flag = true; // 假如孔列已经是最大值 if (Integer.valueOf(ca2) ==
			 * upSc.getColNum()) { // 孔行+1 // 假如孔行也已经是最大值 if (ca1 == (char) (65
			 * + (upSc.getRowNum()))) {
			 * 
			 * // 退出 flag = false; return;
			 * 
			 * } else { ca1 = (char) (ca1 + 1); spId = parentLocation + "-" +
			 * String.valueOf(ca1) + "0" + String.valueOf(1);
			 * 
			 * } } else {
			 * 
			 * ca2 = String.valueOf(Integer.valueOf(ca2) + 1); if (ca2.length()
			 * > 1) { } else { ca2 = "0" + ca2; } spId = parentLocation + "-" +
			 * String.valueOf(ca1) + ca2;
			 * 
			 * }
			 * 
			 * if (flag == true) {
			 * 
			 * int rowNum = 0; int colNum = 0;
			 * 
			 * List<StorageBox> l1 = storageBoxService
			 * .findStorageBoxList(parentLocation);
			 * 
			 * if (l1.size() > 0) {
			 * 
			 * subSc = l1.get(0).getStorageContainer(); }
			 * 
			 * for (int i = 0; i < subSc.getRowNum(); i++) { for (int j = 1; j
			 * <= subSc.getColNum(); j++) { // 假如大于起始位置 String sp1 = ""; if (j <
			 * 10) { sp1 = spId + "-" + String.valueOf((char) (65 + i)) + "0" +
			 * String.valueOf(j); } else { sp1 = spId + "-" +
			 * String.valueOf((char) (65 + i)) + String.valueOf(j); }
			 * 
			 * if (s1 < saveItemsNotNull.size()) { if
			 * (containerDao.findIsUse(sp1) == 0) {
			 * saveItemsNotNull.get(s1).setLocation(sp1); s1++; s =
			 * String.valueOf(ca1) + ca2; } } }
			 * 
			 * } }
			 * 
			 * }
			 */
		}
		sampleInDao.saveOrUpdateAll(saveItems);
		sampleInDao.saveOrUpdateAll(saveItemsNotNull);

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleInItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleInItem scp = sampleInDao.get(SampleInItem.class, id);
			if (scp != null) {
				sampleInDao.delete(scp);
				// 改变左侧状态
				SampleInItemTemp siit = commonDAO.get(SampleInItemTemp.class,
						scp.getTempId());
				siit.setState("1");
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleIn sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleInDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleInItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleInItem(sc, jsonStr);
			}
		}
	}

	// 审批完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SampleIn sr = sampleInDao.get(SampleIn.class, id);
		sr.setState("1");
		sr.setStateName("完成");

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sr.setAcceptDate(new Date());
		sr.setAcceptUser(user);
		sampleInDao.update(sr);

		// saveToMainCell(id);
		changeTempState(id);// -----------改变左侧表的状态

	}

	// 更新主数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void updateMainData(String id, SampleIn sr) throws Exception {
		List<SampleInItem> list = sampleInDao.selectSampleInItemListById(id);
		for (SampleInItem mc : list) {
			if (sr.getSampleType().equals("1")) {
				List<SamplePlasmaInfo> spil = commonService.get(
						SamplePlasmaInfo.class, "code", mc.getSampleCode());
				if (spil.size() > 0) {
					SamplePlasmaInfo s = spil.get(0);
					s.setTechJkServiceTask(mc.getTechJkServiceTask());
					commonService.saveOrUpdate(s);
				}
			}
			if (sr.getSampleType().equals("0")) {
				List<SampleInfo> spil = commonService.get(SampleInfo.class,
						"code", mc.getSampleCode());
				if (spil.size() > 0) {
					SampleInfo s = spil.get(0);
					s.setLocation(mc.getLocation());
					commonService.saveOrUpdate(s);
				}
			}
			if (sr.getSampleType().equals("2")) {
				List<DnaTaskInfo> spil = commonService.get(
						DnaTaskInfo.class, "code", mc.getSampleCode());
				if (spil.size() > 0) {
					DnaTaskInfo s = spil.get(0);
					s.setTechJkServiceTask(mc.getTechJkServiceTask());
					commonService.saveOrUpdate(s);
				}
			}
			if (sr.getSampleType().equals("3")) {
				List<WkTaskInfo> spil = commonService.get(WkTaskInfo.class,
						"code", mc.getSampleCode());
				if (spil.size() > 0) {
					WkTaskInfo s = spil.get(0);
					s.setTechJkServiceTask(mc.getTechJkServiceTask());
					commonService.saveOrUpdate(s);
				}
			}
			storagePositionService.chengeLocationState(mc.getLocation(),
					mc.getId());
		}
	}

	// 细胞进入主数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToMainCell(String id) throws Exception {
		List<SampleInItem> list = sampleInDao.selectSampleInItemListById(id);
		for (SampleInItem mc : list) {
			storagePositionService.chengeLocationState(mc.getLocation(),
					mc.getId());
		}
	}

	/**
	 * 根据完成的入库单改变左侧表的状态
	 * 
	 * @throws Exception
	 */
	public void changeTempState(String id) throws Exception {
		// 获取子表集合
		List<SampleInItem> list = sampleInDao.getSampleInItemList(id);
		if (list.size() > 0) {
			// 如果子表的数据不为空就循环读取子表集合
			for (SampleInItem si : list) {
				// 查询样本是否是返库
				SampleInItem item = this.sampleInDao.selectTempByCode(si.getCode());
				// 改变左侧状态
				SampleInItemTemp siit = commonDAO.get(SampleInItemTemp.class,
						si.getTempId());
				siit.setState("2");
				if (item == null) {// 新入库样本
					// 把子表对象的数据改成“1”
					si.setState("1");
					// 样本状态 4:在库
					si.setOutState("4");
				} else {// 返库样本
					item.setConcentration(si.getConcentration());
					item.setVolume(si.getVolume());
					item.setSumTotal(si.getSumTotal());
					// 样本状态 4:在库
					item.setOutState("4");
					commonDAO.saveOrUpdate(item);
				}
				// 出库样本信息
				SampleInfoIn sif = null;
				sif = this.sampleInDao.getSampleInItemByCode(si.getCode());
				if (sif == null) {
					sif = new SampleInfoIn();
					sampleInputService.copy(sif, si);
				} else {
					sif.setState("1");
					sif.setConcentration(si.getConcentration());
					sif.setVolume(si.getVolume());
					sif.setSumTotal(si.getSumTotal());
					sif.setIndexa(si.getIndexa());
					sif.setDataTraffic(si.getDataTraffic());
					sif.setQpcrConcentration(si.getQpcrConcentration());
					sif.setTechJkServiceTask(si.getTechJkServiceTask());
				}

				if (si.getSampleCode() != null) {
					String[] sampleCode = si.getSampleCode().split(",");
					for (int i = 0; i < sampleCode.length; i++) {
						SampleInfo sifi = commonDAO.findByProperty(
								SampleInfo.class, "code", sampleCode[i]).get(0);
						if (sifi.getCrmCustomer() != null) {
							sif.setCustomer(sifi.getCrmCustomer());
							si.setCustomer(sifi.getCrmCustomer());
						}
						sif.setSampleInfo(sifi);
						si.setSampleInfo(sifi);
					}
				}
//				if (si.getSampleType() != null
//						&& si.getSampleType().equals("SampleDnaInfo")) {
//					if (si.getCode() != null) {
//						DnaTaskInfo sdif = commonDAO.findByProperty(
//								DnaTaskInfo.class, "code", si.getCode()).get(
//								0);
//						sif.setSampleDnaInfo(sdif);
//					}
//				}
				// by 吴迪 2017-09-28
				if(si.getDicSampleType() != null){
					sif.setSampleTypeId(si.getDicSampleType().getId());
					sif.setSampleType(si.getDicSampleType().getName());
				}
				commonDAO.saveOrUpdate(sif);

			}
		}
	}

	/**
	 * 改变子表对象的状态
	 * 
	 * @param id
	 */
	public void changeItemState(String id) {
		// 获取子表集合
		List<SampleInItem> list = sampleInDao.getSampleInItemList(id);
		if (list.size() > 0) {
			// 如果子表的数据不为空就循环读取子表集合
			for (SampleInItem si : list) {
				// 把子表对象的数据改成“1”
				si.setState("1");
			}
		}
	}

	public Map<String, Object> isTempByCode(String vals) throws Exception {
		List<SampleInItem> list = new ArrayList<SampleInItem>();
		List<String> sample = new ArrayList<String>();
		Map<String, Object> result = new HashMap<String, Object>();
		String[] codes = vals.split(",");
		for (String code : codes) {
			SampleInItem sot = this.sampleInDao.selectTempByCode(code);
			if (sot != null)
				list.add(sot);
			else
				sample.add(code);
		}
		result.put("list", list);
		result.put("list1", sample);
		return result;
	}

	// 根据入库明细id查询入库信息
	public SampleInItem getSampleInItem(String id) {
		SampleInItem si = this.sampleInDao.get(SampleInItem.class, id);
		return si;
	}

	// 根据样本编号查询库存
	public SampleInfoIn getSampleInItemByCode(String code) {
		SampleInfoIn si = this.sampleInDao.getSampleInItemByCode(code);
		if (si != null && si.getState() != null && !"".equals(si.getState())) {
			if (si.getState().equals("2")) {
				return si;
			} else {
				return null;
			}
		} else {
			return null;
		}

	}
	
	
	// 查询待出库列表是否存在
		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void selIsSampleOutTaskId(String taskId, String taskName) {
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			SampleOutTaskId s = sampleOutDao.selectSampleOutTask(taskId);
			if (s != null) {
				s.setState("1");
				s.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
				s.setType("0");// 0：实验
				this.sampleInDao.saveOrUpdate(s);
			} else {
				SampleOutTaskId so = new SampleOutTaskId();
				so.setTaskId(taskId);
				so.setName(taskName);
				so.setCreateUser(user);
				so.setCreateDate(new Date());
				so.setState("1");
				so.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
				so.setType("0");// 0：实验
				this.sampleInDao.saveOrUpdate(so);
			}

		}
	
	
	

	public Long getSampleInItemByCodeState(String stId) {
		return sampleInDao.getSampleInItemByCodeState(stId);

	}

	public SampleInItem selectTempByCode(String code) throws Exception {
		return this.sampleInDao.selectTempByCode(code);
	}

	public Map<String, Object> selSampleInfoInTable(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return sampleInDao.findSampleInfoInTable(start, length, query, col, sort);
	}
}
