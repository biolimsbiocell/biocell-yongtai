package com.biolims.sample.storage.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleReturnDao;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.model.SampleReturn;
import com.biolims.sample.storage.model.SampleReturnItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleReturnService {
	@Resource
	private SampleReturnDao sampleReturnDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private  SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSampleReturnList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleReturnDao.selectSampleReturnList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleReturn i) throws Exception {

		sampleReturnDao.saveOrUpdate(i);

	}
	public SampleReturn get(String id) {
		SampleReturn sampleReturn = commonDAO.get(SampleReturn.class, id);
		return sampleReturn;
	}
	public Map<String, Object> findSampleReturnItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleReturnDao.selectSampleReturnItemList(scId, startNum, limitNum, dir, sort);
		List<SampleReturnItem> list = (List<SampleReturnItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleReturnItem(SampleReturn sc, String itemDataJson) throws Exception {
		List<SampleReturnItem> saveItems = new ArrayList<SampleReturnItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> map : list) {
			SampleReturnItem scp = new SampleReturnItem();
			// 将map信息读入实体类
			scp = (SampleReturnItem) sampleReturnDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleReturn(sc);
			sampleStateService.saveSampleState(null, null ,null, null, "", format.format(sc.getCreateDate()),format.format(new Date()) , "SampleReturnItem", "返库明细", (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY), sc.getId(), scp.getCheck(), null, null, null, null, null, null, null, null, null);
			saveItems.add(scp);
		}
		sampleReturnDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleReturnItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleReturnItem scp =  sampleReturnDao.get(SampleReturnItem.class, id);
			 sampleReturnDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleReturn sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleReturnDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("sampleReturnItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleReturnItem(sc, jsonStr);
			}
		}
   }
	
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		SampleReturn sr = sampleReturnDao.get(SampleReturn.class, id);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		//sct.setConfirmUser(user);
		//sct.setConfirmDate(new Date());
		sampleReturnDao.update(sr);
	}
}
