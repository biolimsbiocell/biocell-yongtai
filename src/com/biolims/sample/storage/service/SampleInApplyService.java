package com.biolims.sample.storage.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleInApplyDao;
import com.biolims.sample.storage.model.SampleInApply;
import com.biolims.sample.storage.model.SampleInApplyItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleInApplyService {
	@Resource
	private SampleInApplyDao sampleInApplyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private  SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSampleInApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleInApplyDao.selectSampleInApplyList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInApply i) throws Exception {

		sampleInApplyDao.saveOrUpdate(i);

	}
	public SampleInApply get(String id) {
		SampleInApply sampleInApply = commonDAO.get(SampleInApply.class, id);
		return sampleInApply;
	}
	public Map<String, Object> findSampleInApplyItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = sampleInApplyDao.selectSampleInApplyItemList(scId, startNum, limitNum, dir, sort);
		List<SampleInApplyItem> list = (List<SampleInApplyItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleInApplyItem(SampleInApply sc, String itemDataJson) throws Exception {
		List<SampleInApplyItem> saveItems = new ArrayList<SampleInApplyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> map : list) {
			SampleInApplyItem scp = new SampleInApplyItem();
			// 将map信息读入实体类
			scp = (SampleInApplyItem) sampleInApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleInApply(sc);
			sampleStateService.saveSampleState(null, null ,null, null, "", format.format(sc.getCreateDate()),format.format(new Date()) , "SampleInItem", "入库明细", (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY), sc.getId(), scp.getCheck(), null, null, null, null, null, null, null, null, null);
			saveItems.add(scp);
		}
		sampleInApplyDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleInApplyItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleInApplyItem scp =  sampleInApplyDao.get(SampleInApplyItem.class, id);
			 sampleInApplyDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInApply sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleInApplyDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("sampleInApplyItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleInApplyItem(sc, jsonStr);
			}
	}
   }
}
