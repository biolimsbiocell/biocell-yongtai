package com.biolims.sample.storage.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.massarray.model.MassarrayTaskTemp;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.DicSampleTypeItem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.dao.SampleOutApplyDao;
import com.biolims.sample.storage.dao.SampleOutDao;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.model.SampleOutApplyItem;
import com.biolims.sample.storage.model.SampleOutItem;
import com.biolims.sample.storage.model.SampleOutTaskId;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleOutService {
	@Resource
	private SampleOutDao sampleOutDao;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleOutApplyDao sampleOutApplyDao;
	@Resource
	private CommonService commonService;
	@Resource
	private StoragePositionService storagePositionService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private SampleStateService sampleStateService;

	@Resource
	private SystemCodeService systemCodeService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleOutList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleOutDao.selectSampleOutList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// 出库任务单
	public Map<String, Object> findSampleOutTaskIdList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleOutDao.selectSampleOutTaskIdList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	/**
	 * 出库左侧中间表
	 * 
	 * @param i
	 * @throws Exception
	 */
	public Map<String, Object> findSampleOutTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleOutDao.selectSampleOutTempList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOut i) throws Exception {

		sampleOutDao.saveOrUpdate(i);

	}

	public SampleOut get(String id) {
		SampleOut sampleOut = commonDAO.get(SampleOut.class, id);
		return sampleOut;
	}

	public Map<String, Object> findSampleOutItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleOutDao.selectSampleOutItemList(scId,
				startNum, limitNum, dir, sort);
		List<SampleOutItem> list = (List<SampleOutItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOutItem(SampleOut sc, String itemDataJson)
			throws Exception {
		List<SampleOutItem> saveItems = new ArrayList<SampleOutItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOutItem scp = new SampleOutItem();
			// 将map信息读入实体类
			scp = (SampleOutItem) sampleOutDao.Map2Bean(map, scp);
			if (scp != null) {
				if (scp.getId() != null && scp.getId().equals("")) {
					scp.setId(null);
				}

				// 出库类型判断是否返库
				if (scp.getIsDepot() == null || "".equals(scp.getIsDepot())) {
					if (sc.getOutType().equals("0")) {
						scp.setIsDepot("1");
					} else {
						scp.setIsDepot("0");
					}
				}

				scp.setSampleOut(sc);
				if (scp.getSampleType() != null) {
					// 根据入库明细id查询入库信息
					SampleInItem sii = dicSampleTypeDao.get(SampleInItem.class,
							scp.getSampleInItemId());
					// if (sii.getInfoFrom() != null
					// && sii.getInfoFrom().equals("SampleDnaInfo")) {
					// SampleDnaInfo sd = dicSampleTypeDao.findByProperty(
					// SampleDnaInfo.class, "code", sii.getCode()).get(0);
					// if (scp.getProductId() == null) {
					// scp.setProductId(sd.getProductId());
					// scp.setProductName(sd.getProductName());
					// }
					//
					// }
					if (sii.getInfoFrom() != null
							&& sii.getInfoFrom().equals("SampleInfo")) {
						SampleInfo sd = dicSampleTypeDao.findByProperty(
								SampleInfo.class, "code", sii.getCode()).get(0);
						if (scp.getProductId() == null) {
							scp.setProductId(sd.getProductId());
							scp.setProductName(sd.getProductName());
						}

					}
					if (scp.getNextFlow() == null && scp.getProductId() != null
							&& !scp.getProductId().equals("")) {
						DicSampleType type = this.dicSampleTypeDao.get(
								DicSampleType.class, sii.getSampleTypeId());
						String[] productId = scp.getProductId().split(",");

						String pid = "";
						for (int i = 0; i < productId.length; i++) {
							pid += "and productId like '%" + productId[i]
									+ "%' ";
						}

						List<DicSampleTypeItem> list_typeItem = this.dicSampleTypeDao
								.selectDicSampleTypeItemByid(type.getId(), pid);

						for (DicSampleTypeItem d : list_typeItem) {
							scp.setNextFlowId(d.getDnextId());
							scp.setNextFlow(d.getDnextName());
						}
					}
				}

				// 改变左侧表状态
				SampleOutTemp st = commonDAO.get(SampleOutTemp.class,
						scp.getTempId());
				st.setState("2");
				saveItems.add(scp);

			}
			sampleOutDao.saveOrUpdateAll(saveItems);
		}
	}

	/**
	 * 根据选择的出库申请带出样本
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReceiveItem(SampleOut sc) throws Exception {
		if (sc.getSampleOutApply() != null) {
			List<SampleOutApplyItem> saveItems = (List<SampleOutApplyItem>) sampleOutApplyDao
					.selectSampleItemById(sc.getSampleOutApply().getId());
			if (saveItems.size() > 0) {
				for (SampleOutApplyItem sri : saveItems) {
					SampleOutItem scp = new SampleOutItem();
					// 将map信息读入实体类
					scp.setSampleOut(sc);
					// scp.setSampleOutApplyItem(sri);
					sampleOutDao.saveOrUpdate(scp);
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOutItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOutItem scp = sampleOutDao.get(SampleOutItem.class, id);
			sampleOutDao.delete(scp);
			// 改变左侧表状态
			SampleOutTemp st = commonDAO.get(SampleOutTemp.class,
					scp.getTempId());
			st.setState("1");
			String ck = "样本出库:" + "ID:'" + id + "',样本编号:'" + scp.getCode() + "'已删除";
			if (id != null && !"".equals(id)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("SampleOut");
				li.setFileId(scp.getSampleOut().getId());
				li.setState("2");
				li.setStateName("数据删除");
				li.setModifyContent(ck);
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOut sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleOutDao.saveOrUpdate(sc);
			// saveReceiveItem(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleOutItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleOutItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws ParseException
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SampleOut sr = sampleOutDao.get(SampleOut.class, id);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sr.setAcceptDate(new Date());
		sr.setAcceptUser(user);
		sampleOutDao.saveOrUpdate(sr);

		// 从左侧表中选择的样本执行
		changeTempState(id);// -------------改变左侧表状态

		setSampleToReceive(id);// ----------传值到样本交接

		updateMainData(id, sr);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void updateMainData(String id, SampleOut sr) throws Exception {
		List<SampleOutItem> list = sampleOutDao.getSampleOutItemList(id);
		for (SampleOutItem mc : list) {
			List<SampleInfo> spil = commonService.get(SampleInfo.class, "code",
					mc.getCode());
			if (spil.size() > 0) {
				SampleInfo s = spil.get(0);
				s.setLocation(null);
				commonService.saveOrUpdate(s);
			}

			// 改变库存样本的样本状态
			SampleInItem inItem = this.sampleInDao.selectTempByCode(mc
					.getCode());
			if (inItem != null) {
				inItem.setOutState(sr.getOutType());
				commonService.saveOrUpdate(inItem);
			}
		}
	}

	/**
	 * 
	 * 改变子表中相关左侧表的状态
	 */
	public void changeTempState(String id) {
		SampleOut so = sampleOutDao.get(SampleOut.class, id);
		// 改变待出库状态
		if (so.getTaskId() != null) {
			List<SampleOutTaskId> s = this.commonService.get(
					SampleOutTaskId.class, "taskId", so.getTaskId());
			for (SampleOutTaskId sot : s) {
				sot.setState("2");
				sot.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_STORAGE_OUT_COMPLETE_NAME);
				commonDAO.saveOrUpdate(sot);
			}
		}
		// 根据主表ID获取相关子表集合
		List<SampleOutItem> list = sampleOutDao.getSampleOutItemList(id);
		if (list.size() > 0) {
			// 如果相关子表数据不为空，就循环获取左侧表ID
			for (SampleOutItem sot : list) {
				String inItemId = sot.getSampleInItemId();
				if (inItemId != null) {
//					// 改变库存样本的样本状态
//					SampleInItem si = this.commonDAO.get(SampleInItem.class,
//							inItemId);
//					if (si != null) {
//						si.setOutState(so.getOutType());
//					}
					// 出库样本
					SampleInfoIn a = commonService.get(SampleInfoIn.class,
							inItemId);
					if (a != null) {
						// 0：出库不返库 2：出库返库
						if ("1".equals(sot.getIsDepot())) {
							a.setState("0");
						} else {
							a.setState(sot.getIsDepot());
						}
					}
					// 清空储位
					// if (so.getOutType().equals("3")) {// 样本销毁
					if (so.getOutType() != null) {// 清空储位
						// SampleInfoIn a = sampleInfoMainDao
						// .findSampleInfoIn(scp.getCode());
						a.setLocation("");
						//si.setLocation("");
					}
					sampleOutDao.saveOrUpdate(a);
					//sampleOutDao.saveOrUpdate(si);
				}
			}
		}
	}

	/**
	 * 将出库的样本根据样本类型传到样本交接
	 * 
	 * @throws ParseException
	 */
	public void setSampleToReceive(String id) throws Exception {
		SampleOut so = sampleOutDao.get(SampleOut.class, id);

		// 根据主表ID获取相关子表集合
		List<SampleOutItem> list = sampleOutDao.getSampleOutItemList(id);
		if (list.size() > 0) {
			// 如果相关子表数据不为空，就循环读取子表数据
			for (SampleOutItem scp : list) {
//				if (scp.getInfoFrom().equals("SampleOutApply")) {
					// SampleOrder so = sampleReceiveDao.get(SampleOrder.class,
					// scp.getOrderCode());
					// if (so == null) {
					// so = new SampleOrder();
					// so.setId(scp.getOrderCode());
					// so.setProductId(scp.getProductId());
					// so.setProductName(scp.getProductName());
					// so.setState("3");
					// so.setBgState("1");
					// so.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
					// commonService.saveOrUpdate(so);
					// }
					// // 样本订单表
					// SampleOrderInfoItem soii = new SampleOrderInfoItem();
					// soii.setCode(scp.getOrderCode());
					// soii.setSampleCode(scp.getSampleCode());
					// sampleReceiveDao.saveOrUpdate(soii);
//					SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
//							.getSampleCode());
					
					SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
							.getSampleOrder().getId());
					if (scp != null) {
						if (scp.getNextFlowId() != null) {
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setNum(scp.getSampleNum());
								st.setSampleType(scp.getSampleType());
								st.setState("1");
								st.setSampleStyle(scp.getSampleStyle());
								if(scp.getSampleOrder()!=null) {
									st.setSampleOrder(commonDAO.get(SampleOrder.class,
											scp.getSampleOrder().getId()));
								}
								st.setPronoun(scp.getPronoun());
								sampleOutDao.saveOrUpdate(st);
							} else if (scp.getNextFlowId().equals("0029")) {// 样本处理
								MassarrayTaskTemp cpt=new MassarrayTaskTemp();
								scp.setState("1");
								cpt.setState("1");
								if (scp.getProductId() != null) {
									Product p = commonDAO.get(Product.class,
											scp.getProductId());
								}
								cpt.setOrderCode(sf.getOrderNum());
								scp.setParentId(scp.getCode());
								cpt.setSampleCode(scp.getSampleCode());
								cpt.setCode(scp.getCode());
								cpt.setProductId(scp.getProductId());
								cpt.setProductName(scp.getProductName());
								cpt.setConcentration(scp.getConcentration());
								cpt.setVolume(scp.getVolume());
								cpt.setOrderId(so.getId());
								cpt.setSampleType(scp.getSampleType());
								cpt.setSampleInfo(sf);
								cpt.setScopeId(scp.getScopeId());
								cpt.setScopeName(scp.getScopeName());
								if(scp.getSampleOrder()!=null) {
									cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
											scp.getSampleOrder().getId()));
								}
								sampleReceiveDao.saveOrUpdate(cpt);
								/*PlasmaTaskTemp d = new PlasmaTaskTemp();
								scp.setState("1");
								sampleInputService.copy(d, scp);
								d.setTechJkServiceTask(scp.getSampleOut()
										.getTechJkServiceTask());
								d.setSampleInfo(sf);
								// by 吴迪
								if (scp.getSumTotal() != null) {
									d.setSampleNum(scp.getSumTotal().toString());
								}
								d.setOrderCode(sf.getOrderNum());
								sampleReceiveDao.saveOrUpdate(d);*/
							} else if (scp.getNextFlowId().equals("0073")) {// 传代
								
								CellPassageTemp cpt=new CellPassageTemp();
								scp.setState("1");
								cpt.setState("1");
								if (scp.getProductId() != null) {
									Product p = commonDAO.get(Product.class,
											scp.getProductId());
								}
								cpt.setOrderCode(sf.getOrderNum());
								scp.setParentId(scp.getCode());
								cpt.setSampleCode(scp.getSampleCode());
								cpt.setCode(scp.getCode());
								cpt.setProductId(scp.getProductId());
								cpt.setProductName(scp.getProductName());
								cpt.setConcentration(scp.getConcentration());
								cpt.setVolume(scp.getVolume());
								cpt.setOrderId(so.getId());
								cpt.setSampleType(scp.getSampleType());
								cpt.setSampleInfo(sf);
								cpt.setScopeId(scp.getScopeId());
								cpt.setScopeName(scp.getScopeName());
								cpt.setPronoun(scp.getPronoun());
								if(scp.getSampleOrder()!=null) {
									cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
											scp.getSampleOrder().getId()));
									cpt.setBatch(scp.getSampleOrder().getBarcode());
									cpt.setPatientName(scp.getSampleOrder().getName());
								}
								sampleReceiveDao.saveOrUpdate(cpt);
							} else if(scp.getNextFlowId().equals("0094")){//运输
								TransportOrderTemp cpt=new TransportOrderTemp();
								cpt.setId(null);
								/** 原始样本编号 */
								cpt.setSampleCode(scp.getCode());
								/** 样本编号 */
								cpt.setCode(scp.getCode());
								if(scp.getSampleOrder()!=null) {
									if(so!=null){
										/** 患者姓名 */
										cpt.setPatientName(scp.getSampleOrder().getName());
										/** 检测项目 */
										cpt.setProductId(scp.getSampleOrder().getProductId());
										/** 检测项目 */
										cpt.setProductName(scp.getSampleOrder().getProductName());
										/**关联订单*/
										cpt.setSampleOrder(scp.getSampleOrder());
										/** 订单编号 */
										cpt.setOrderCode(scp.getSampleOrder().getId());
									}
								}
								
								/** 备注 */
								cpt.setNote(scp.getNote());
								/** 状态ID */
								cpt.setState("1");
								/**产品运输左侧表的状态 1放行审核通过0放行审核未通过*/
								cpt.setPlanState("1");
								/** 体积 */
								cpt.setVolume(scp.getVolume());
								/** 任务单id */
								cpt.setOrderId(so.getId());
								cpt.setSampleType(scp.getSampleType());
//								/** 样本数量 */
//								cpt.setSampleNum(scp.getCellCount());
//								/** 样本类型 */
//								if(scp.getDicSampleType()!=null){
//									cpt.setSampleType(scp.getDicSampleType().getName());
//								}
//								/** 样本主数据 */
//								cpt.setSampleInfo(scp.getSampleInfo());
								sampleReceiveDao.saveOrUpdate(cpt);
							}else if (scp.getNextFlowId().equals("0018")) {// 文库构建
								WkTaskTemp d = new WkTaskTemp();
								scp.setState("1");
								sampleInputService.copy(d, scp);
								if (scp.getProductId() != null) {
									Product p = commonDAO.get(Product.class,
											scp.getProductId());
								}
							} else if (scp.getNextFlowId().equals("0012")) {// 暂停
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
							} else if (scp.getNextFlowId().equals("0082")) {// 过程细胞检测(自主)
								/**质检类型  自主1:过程干细胞		2:产品干细胞		3:产品免疫细胞（改成试剂质检）		4:产品细胞因子
								 * 第三方5:过程干细胞		6:产品干细胞		7:产品免疫细胞（改成试剂质检）		8:产品细胞因子*/
								QualityTestTemp qtt = new QualityTestTemp();
								if(scp.getInfoFromId()!=null
										&&!"".equals(scp.getInfoFromId())) {
									QualityTestTemp qt = commonDAO.get(QualityTestTemp.class, scp.getInfoFromId());
									qtt = (QualityTestTemp) qt.clone();
									qtt.setId(null);
									qtt.setCellType("1");
									qtt.setState("1");
									commonDAO.saveOrUpdate(qtt);
								}else {
									qtt.setId(null);
									qtt.setCellType("1");
									qtt.setState("1");
									
									qtt.setCode(scp.getCode());
									qtt.setSampleNumber(scp.getCode());
									qtt.setSampleOrder(scp.getSampleOrder());
									SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyionId());
									if(sd!=null) {
										qtt.setSampleDeteyion(sd);
									}
									if(scp.getSampleOrder()!=null) {
										qtt.setBatch(scp.getSampleOrder().getBarcode());
									}
									qtt.setSampleType(scp.getSampleType());
									if(scp.getSumTotal()!=null) {
										qtt.setSampleNum(String.valueOf(scp.getSumTotal()));
									}
									qtt.setQualitySubmitTime(new Date());
									commonDAO.saveOrUpdate(qtt);
								}
							} else if (scp.getNextFlowId().equals("0083")) {// 产品细胞检测(自主)
								QualityTestTemp qtt = new QualityTestTemp();
								if(scp.getInfoFromId()!=null
										&&!"".equals(scp.getInfoFromId())) {
									QualityTestTemp qt = commonDAO.get(QualityTestTemp.class, scp.getInfoFromId());
									qtt = (QualityTestTemp) qt.clone();
									qtt.setId(null);
									qtt.setCellType("2");
									qtt.setState("1");
									commonDAO.saveOrUpdate(qtt);
								}else {
									qtt.setId(null);
									qtt.setCellType("2");
									qtt.setState("1");
									
									qtt.setCode(scp.getCode());
									qtt.setSampleNumber(scp.getCode());
									qtt.setSampleOrder(scp.getSampleOrder());
									SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyionId());
									if(sd!=null) {
										qtt.setSampleDeteyion(sd);
									}
									if(scp.getSampleOrder()!=null) {
										qtt.setBatch(scp.getSampleOrder().getBarcode());
									}
									qtt.setSampleType(scp.getSampleType());
									if(scp.getSumTotal()!=null) {
										qtt.setSampleNum(String.valueOf(scp.getSumTotal()));
									}
									qtt.setQualitySubmitTime(new Date());
									commonDAO.saveOrUpdate(qtt);
								}
							} else if (scp.getNextFlowId().equals("0086")) {// 过程细胞检测(第三方)
								QualityTestTemp qtt = new QualityTestTemp();
								if(scp.getInfoFromId()!=null
										&&!"".equals(scp.getInfoFromId())) {
									QualityTestTemp qt = commonDAO.get(QualityTestTemp.class, scp.getInfoFromId());
									qtt = (QualityTestTemp) qt.clone();
									qtt.setId(null);
									qtt.setCellType("5");
									qtt.setState("1");
									commonDAO.saveOrUpdate(qtt);
								}else {
									qtt.setId(null);
									qtt.setCellType("5");
									qtt.setState("1");
									
									qtt.setCode(scp.getCode());
									qtt.setSampleNumber(scp.getCode());
									qtt.setSampleOrder(scp.getSampleOrder());
									SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyionId());
									if(sd!=null) {
										qtt.setSampleDeteyion(sd);
									}
									if(scp.getSampleOrder()!=null) {
										qtt.setBatch(scp.getSampleOrder().getBarcode());
									}
									qtt.setSampleType(scp.getSampleType());
									if(scp.getSumTotal()!=null) {
										qtt.setSampleNum(String.valueOf(scp.getSumTotal()));
									}
									qtt.setQualitySubmitTime(new Date());
									commonDAO.saveOrUpdate(qtt);
								}
							} else if (scp.getNextFlowId().equals("0087")) {// 产品细胞检测(第三方)
								QualityTestTemp qtt = new QualityTestTemp();
								if(scp.getInfoFromId()!=null
										&&!"".equals(scp.getInfoFromId())) {
									QualityTestTemp qt = commonDAO.get(QualityTestTemp.class, scp.getInfoFromId());
									qtt = (QualityTestTemp) qt.clone();
									qtt.setId(null);
									qtt.setCellType("6");
									qtt.setState("1");
									commonDAO.saveOrUpdate(qtt);
								}else {
									qtt.setId(null);
									qtt.setCellType("6");
									qtt.setState("1");
									
									qtt.setCode(scp.getCode());
									qtt.setSampleNumber(scp.getCode());
									qtt.setSampleOrder(scp.getSampleOrder());
									SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyionId());
									if(sd!=null) {
										qtt.setSampleDeteyion(sd);
									}
									if(scp.getSampleOrder()!=null) {
										qtt.setBatch(scp.getSampleOrder().getBarcode());
									}
									qtt.setSampleType(scp.getSampleType());
									if(scp.getSumTotal()!=null) {
										qtt.setSampleNum(String.valueOf(scp.getSumTotal()));
									}
									qtt.setQualitySubmitTime(new Date());
									commonDAO.saveOrUpdate(qtt);
								}
							} else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									// scp.setReportDate(so.getReportDate());
									sampleInputService.copy(o, scp);
								}
							}
						}
					}
//				}

				if(scp.getSampleOrder()!=null) {
					scp.getSampleOrder().setNewTask(id);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String[] sampleCode = scp.getSampleCode().split(",");
				for (int i = 0; i < sampleCode.length; i++) {
					sampleStateService.saveSampleState1(
							scp.getSampleOrder(),
							scp.getCode(),
							sampleCode[i],
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(so.getAcceptDate()),
							format.format(new Date()),
							"SampleOut",
							"样本出库",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							so.getId(), scp.getNextFlow(), "1", null, null,
							null, null, null, null, null, null);
				}
				sampleReceiveDao.saveOrUpdate(scp);

			}
		}
	}

	public Map<String, Object> selSampleOutTempTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return sampleOutDao.findSampleOutTempTable(start, length, query, col,
				sort);
	}

	public Map<String, Object> selSampleOutItemTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sampleOutDao.findSampleOutItemTable(scId, start, length, query,
				col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addSampleOutItem(String[] ids, String note,
			String createUser, String createDate, String outTypes,
			String sampleOutId, String fyDate, String applyUser,
			String acceptUser) throws Exception {

		// 保存主表信息
		SampleOut so = new SampleOut();
		if (sampleOutId == null || sampleOutId.length() <= 0
				|| "NEW".equals(sampleOutId)) {
			String code = systemCodeService.getCodeByPrefix("SampleOut", "YBCK"
					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
					000, 3, null);
			so.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User creataUsers = commonDAO.get(User.class, createUser);
			User applyUsers = commonDAO.get(User.class, applyUser);
			User acceptUsers = commonDAO.get(User.class, acceptUser);
			DicType d = commonDAO.get(DicType.class, outTypes);
			so.setCreateUser(creataUsers);
			so.setApplyUser(applyUsers);
			so.setAcceptUser(acceptUsers);
			so.setName(note);
			so.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			so.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			so.setCreateDate(sdf.parse(createDate));
			if(!"".equals(fyDate)){
				so.setFyDate(sdf.parse(fyDate));
			}
			
			so.setOutTypes(d);
			so.setState("3");
			so.setStateName("NEW");
			sampleOutId = so.getId();
			commonDAO.saveOrUpdate(so);
		} else {
			so = commonDAO.get(SampleOut.class, sampleOutId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			// 通过id查询待出库样本
			SampleOutTemp sot = commonDAO.get(SampleOutTemp.class, id);
			SampleOutItem soi = new SampleOutItem();
			// 将数据放到出库申请明细表
			soi.setSampleOut(so);
			soi.setPronoun(sot.getPronoun());
			soi.setCode(sot.getCode());
			soi.setSampleCode(sot.getSampleCode());
			soi.setLocation(sot.getLocation());
			soi.setTempId(sot.getId());
			soi.setSampleInItemId(sot.getSampleInItemId());
			soi.setBarCode(sot.getBarCode());
			soi.setDataTraffic(sot.getDataTraffic());
			soi.setProductId(sot.getProductId());
			soi.setSampleType(sot.getSampleType());
			soi.setProductName(sot.getProductName());
			soi.setSamplingDate(sot.getSamplingDate());
			soi.setConcentration(sot.getConcentration());
			soi.setPatientName(sot.getPatientName());
			soi.setVolume(sot.getVolume());
			soi.setSumTotal(sot.getSumTotal());
			soi.setApplyUser(sot.getApplyUser());
			soi.setCrmCustomer(sot.getCrmCustomer());
			soi.setSupplier(sot.getSupplier());
			soi.setInfoFrom(sot.getInfoFrom());
			soi.setScopeId(sot.getScopeId());
			soi.setScopeName(sot.getScopeName());
			soi.setSampleStyle(sot.getSampleStyle());
			soi.setQpcrConcentration(sot.getQpcrConcentration());
			soi.setIndexa(sot.getIndexa());
			if(sot.getSampleOrder()!=null) {
				soi.setSampleOrder(commonDAO.get(SampleOrder.class, sot.getSampleOrder().getId()));
			}
			
			soi.setNum(sot.getNum());
			soi.setSampleDeteyionId(sot.getSampleDeteyionId());
			soi.setSampleDeteyionName(sot.getSampleDeteyionName());
			soi.setInfoFromId(sot.getInfoFromId());
			
			if ("DnaTaskItem".equals(sot.getInfoFrom())) {
				soi.setNextFlowId("0017");
				soi.setNextFlowId("核酸提取");
			} else if ("PlasmaTaskItem".equals(sot.getInfoFrom())) {
				soi.setNextFlowId("0016");
				soi.setNextFlowId("样本处理");
			} else if ("TechCheckServiceTaskItem".equals(sot.getInfoFrom())) {
				soi.setNextFlowId("0019");
				soi.setNextFlowId("纯化质检");
			} else if ("WkTaskItem".equals(sot.getInfoFrom())) {
				soi.setNextFlowId("0018");
				soi.setNextFlowId("文库构建");
			} else if ("WkBlendTaskItem".equals(sot.getInfoFrom())) {
				soi.setNextFlowId("0032");
				soi.setNextFlowId("文库混合");
			}
			sot.setState("2");
			String ck = "样本出库:"
					+"样本编号"+sot.getCode()
					+",原始样本编号"+sot.getSampleCode()
					+",样本位置为"+sot.getLocation()
					+"的样本已添加到明细";
		if (id != null && !"".equals(id)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(so.getId());
			li.setClassName("SampleOut");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(ck);
			commonDAO.saveOrUpdate(li);
		}
			commonDAO.saveOrUpdate(soi);
			commonDAO.saveOrUpdate(sot);
		}
		return sampleOutId;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public SampleOut saveSampleOutById(SampleOut so, String note,
			String outTypes, String id, String fyDate, String applyUser,
			String acceptUser, String logInfo) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		User createUser = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		SampleOut newSo = null;
		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
			newSo = new SampleOut();
			String code = systemCodeService.getCodeByPrefix("SampleOut", "YBCK"
					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
					000, 3, null);
			newSo.setId(code);
			User applyUsers = commonDAO.get(User.class, applyUser);
			User acceptUsers = commonDAO.get(User.class, acceptUser);
			DicType d = commonDAO.get(DicType.class, outTypes);
			newSo.setCreateUser(createUser);
			newSo.setApplyUser(applyUsers);
			newSo.setAcceptUser(acceptUsers);
			newSo.setName(note);
			newSo.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			newSo.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			newSo.setCreateDate(new Date());
			if(!"".equals(fyDate)){
				newSo.setFyDate(sdf.parse(fyDate));
			}
			
			newSo.setOutTypes(d);
			newSo.setState("3");
			newSo.setStateName("新建");
			
			newSo.setScopeId(createUser.getScopeId());
			newSo.setScopeName(createUser.getScopeName());
			
			id = newSo.getId();
			commonDAO.saveOrUpdate(newSo);

		} else {
			newSo = commonDAO.get(SampleOut.class, id);
			newSo.setName(note);
			DicType d = commonDAO.get(DicType.class, outTypes);
			newSo.setOutTypes(d);
			User applyUsers = commonDAO.get(User.class, applyUser);
			User acceptUsers = commonDAO.get(User.class, acceptUser);
			newSo.setApplyUser(applyUsers);
			newSo.setAcceptUser(acceptUsers);
			newSo.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			newSo.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			if(!"".equals(fyDate)){
				newSo.setFyDate(sdf.parse(fyDate));
			}
		
			commonDAO.saveOrUpdate(newSo);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setClassName("SampleOut");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		
		return newSo;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOutAndItem(SampleOut newSo, Map aMap, String logInfo,
			String logInfoItem) throws Exception {
		if (newSo != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(newSo.getId());
				li.setClassName("SampleOut");
				li.setState("3");
				li.setStateName("数据修改");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			jsonStr = (String) aMap.get("ImteJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveItem(newSo, jsonStr, logInfoItem);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveItem(SampleOut newSo, String itemDataJson, String logInfo)
			throws Exception {
		List<SampleOutItem> saveItems = new ArrayList<SampleOutItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOutItem scp = new SampleOutItem();
			// 将map信息读入实体类
			scp = (SampleOutItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setScopeId(newSo.getScopeId());
			scp.setScopeName(newSo.getScopeName());
			scp.setSampleOut(newSo);
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSo.getId());
			li.setClassName("SampleOut");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	public Map<String, Object> selSampleOutTable(Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return sampleOutDao.findSampleOutTable(start, length, query, col, sort);
	}

	public SampleOut findSampleOutById(String outId) {
		return sampleOutDao.get(SampleOut.class, outId);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageOutItemTable(String id, String item,
			String logInfo) throws Exception {
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		SampleOut ep = commonDAO.get(SampleOut.class,id);
		
		for (Map<String, Object> map : list) {
			SampleOutItem em = new SampleOutItem();

			// 将map信息读入实体类
			em = (SampleOutItem) commonDAO.Map2Bean(map, em);
			if (em.getId() != null && em.getId().equals("")) {
				em.setId(null);
			}
//			if (em.getStorage() == null) {
//				Storage s = storageService.getStorageByName((String) map
//						.get("storage-name"));
//				em.setStorage(s);
//			}
			em.setScopeId(ep.getScopeId());
			em.setScopeName(ep.getScopeName());
			em.setSampleOut(ep);
			commonDAO.saveOrUpdate(em);
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(ep.getCreateUser().getId());
			li.setFileId(id);
			li.setClassName("SampleOut");
			if(id.equals("NEW")) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}
}