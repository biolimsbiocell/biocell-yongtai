package com.biolims.zkqreport.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
import com.biolims.zkqreport.model.ZkqReport;
import com.biolims.zkqreport.model.ZkqReportItem;
import com.biolims.zkqreport.service.ZkqReportService;

@Namespace("/zkqreport/zkqReport")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ZkqReportAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2022";
	@Autowired
	private ZkqReportService zkqReportService;
	private ZkqReport zkqReport = new ZkqReport();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showZkqReportList")
	public String showZkqReportList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/zkqreport/zkqReport.jsp");
	}

	@Action(value = "showZkqReportListJson")
	public void showZkqReportListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = zkqReportService.findZkqReportList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ZkqReport> list = (List<ZkqReport>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("flowCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "zkqReportSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogZkqReportList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/zkqreport/zkqReportDialog.jsp");
	}

	@Action(value = "showDialogZkqReportListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogZkqReportListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = zkqReportService.findZkqReportList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ZkqReport> list = (List<ZkqReport>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editZkqReport")
	public String editZkqReport() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			zkqReport = zkqReportService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "zkqReport");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			zkqReport.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			zkqReport.setCreateUser(user);
			zkqReport.setCreateDate(new Date());
			zkqReport.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			zkqReport.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(zkqReport.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/zkqreport/zkqReportEdit.jsp");
	}

	@Action(value = "copyZkqReport")
	public String copyZkqReport() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		zkqReport = zkqReportService.get(id);
		zkqReport.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/zkqreport/zkqReportEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = zkqReport.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "ZkqReport";
			String markCode = "ZKQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			zkqReport.setId(autoID);
		}

		Map aMap = new HashMap();
		aMap.put("zkqReportItem", getParameterFromRequest("zkqReportItemJson"));

		zkqReportService.save(zkqReport, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/zkqreport/zkqReport/editZkqReport.action?id="
				+ zkqReport.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewZkqReport")
	public String toViewZkqReport() throws Exception {
		String id = getParameterFromRequest("id");
		zkqReport = zkqReportService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/zkqreport/zkqReportEdit.jsp");
	}

	@Action(value = "showZkqReportItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showZkqReportItemList() throws Exception {
		return dispatcher("/WEB-INF/page/zkqreport/zkqReportItem.jsp");
	}

	@Action(value = "showZkqReportItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showZkqReportItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = zkqReportService
					.findZkqReportItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<ZkqReportItem> list = (List<ZkqReportItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("project-id", "");
			map.put("crmcustomer-id", "");
			map.put("crmdoctor-id", "");
			map.put("project-name", "");
			map.put("crmcustomer-name", "");
			map.put("crmdoctor-name", "");
			map.put("tp", "");
			map.put("genotype", "");
			map.put("zkqreport-name", "");
			map.put("zkqreport-id", "");
			map.put("note", "");
			map.put("state", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delZkqReportItem")
	public void delZkqReportItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			zkqReportService.delZkqReportItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ZkqReportService getZkqReportService() {
		return zkqReportService;
	}

	public void setZkqReportService(ZkqReportService zkqReportService) {
		this.zkqReportService = zkqReportService;
	}

	public ZkqReport getZkqReport() {
		return zkqReport;
	}

	public void setZkqReport(ZkqReport zkqReport) {
		this.zkqReport = zkqReport;
	}

}
