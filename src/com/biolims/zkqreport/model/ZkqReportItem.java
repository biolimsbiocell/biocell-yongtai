package com.biolims.zkqreport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 报告明细
 * @author lims-platform
 * @date 2016-11-03 15:08:51
 * @version V1.0
 * 
 */
@Entity
@Table(name = "ZKQREPORT_ITEM")
@SuppressWarnings("serial")
public class ZkqReportItem extends EntityDao<ZkqReportItem> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 项目名称 */
	private Project project;
	/** 客户名称 */
	private CrmCustomer crmcustomer;
	/** 医生名称 */
	private CrmDoctor crmdoctor;
	/** 数据是否已释放 */
	private String tp;
	/** 是否已发送报告 */
	private String genotype;
	/** 相关主表 */
	private ZkqReport zkqreport;
	/** 备注 */
	private String note;
	/** 状态 */
	private String state;
	// 人
	private User sellPerson;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	public User getSellPerson() {
		return sellPerson;
	}

	public void setSellPerson(User sellPerson) {
		this.sellPerson = sellPerson;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@Column(name = "ID", length = 255)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得Project
	 * 
	 * @return: Project 项目名称
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT")
	public Project getProject() {
		return this.project;
	}

	/**
	 * 方法: 设置Project
	 * 
	 * @param: Project 项目名称
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * 方法: 取得CrmCustomer
	 * 
	 * @return: CrmCustomer 客户名称
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmcustomer() {
		return this.crmcustomer;
	}

	/**
	 * 方法: 设置CrmCustomer
	 * 
	 * @param: CrmCustomer 客户名称
	 */
	public void setCrmcustomer(CrmCustomer crmcustomer) {
		this.crmcustomer = crmcustomer;
	}

	/**
	 * 方法: 取得CrmDoctor
	 * 
	 * @return: CrmDoctor 医生名称
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmdoctor() {
		return this.crmdoctor;
	}

	/**
	 * 方法: 设置CrmDoctor
	 * 
	 * @param: CrmDoctor 医生名称
	 */
	public void setCrmdoctor(CrmDoctor crmdoctor) {
		this.crmdoctor = crmdoctor;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 数据是否已释放
	 */
	@Column(name = "TP", length = 255)
	public String getTp() {
		return this.tp;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 数据是否已释放
	 */
	public void setTp(String tp) {
		this.tp = tp;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否已发送报告
	 */
	@Column(name = "GENOTYPE", length = 255)
	public String getGenotype() {
		return this.genotype;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否已发送报告
	 */
	public void setGenotype(String genotype) {
		this.genotype = genotype;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 取得Zkqreport
	 * 
	 * @return: Zkqreport 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ZKQREPORT")
	public ZkqReport getZkqreport() {
		return zkqreport;
	}

	public void setZkqreport(ZkqReport zkqreport) {
		this.zkqreport = zkqreport;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 255)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}
}