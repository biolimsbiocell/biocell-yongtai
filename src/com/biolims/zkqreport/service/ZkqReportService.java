package com.biolims.zkqreport.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.SystemCodeService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.SampleReport;
import com.biolims.report.model.SampleReportItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.zkqreport.dao.ZkqReportDao;
import com.biolims.zkqreport.model.ZkqReport;
import com.biolims.zkqreport.model.ZkqReportItem;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ZkqReportService {
	@Resource
	private ZkqReportDao zkqReportDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findZkqReportList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return zkqReportDao.selectZkqReportList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ZkqReport i) throws Exception {

		zkqReportDao.saveOrUpdate(i);

	}

	// public ZkqReport get(String id) {
	// ZkqReport zkqReport = commonDAO.get(ZkqReport.class, id);
	// return zkqReport;
	// }
	//
	public ZkqReport get(String id) throws Exception {
		return zkqReportDao.get(ZkqReport.class, id);
	}

	public Map<String, Object> findZkqReportItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = zkqReportDao.selectZkqReportItemList(scId,
				startNum, limitNum, dir, sort);
		List<ZkqReportItem> list = (List<ZkqReportItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveZkqReportItem(ZkqReport sc, String itemDataJson)
			throws Exception {
		List<ZkqReportItem> saveItems = new ArrayList<ZkqReportItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ZkqReportItem scp = new ZkqReportItem();
			// 将map信息读入实体类
			scp = (ZkqReportItem) zkqReportDao.Map2Bean(map, scp);
			scp.setZkqreport(sc);

			if (scp.getId() == null) {
				String code = systemCodeService.getCodeByPrefix(
						"ZkqReportItem",
						"XJQ"
								+ DateUtil.dateFormatterByPattern(new Date(),
										"yyMMdd"), 00000, 5, null);
				scp.setId(code);
			}
			scp.setZkqreport(sc);

			zkqReportDao.saveOrUpdate(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delZkqReportItem(String[] ids) {
		for (String id : ids) {
			// ZkqReportItem scp = zkqReportDao.get(ZkqReportItem.class, id);
			// zkqReportDao.delete(scp);
			ZkqReportItem sri = new ZkqReportItem();
			sri.setId(id);
			zkqReportDao.delete(sri);

		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ZkqReport sc, Map jsonMap) throws Exception {
		if (sc != null) {
			zkqReportDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("zkqReportItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveZkqReportItem(sc, jsonStr);
			}
		}
	}
}
