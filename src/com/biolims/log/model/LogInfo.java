package com.biolims.log.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;

/**
 * 日志信息
 * 
 */
@Entity
@Table(name = "T_LOG_INFO")
public class LogInfo extends EntityDao<LogInfo> implements Serializable {

	private static final long serialVersionUID = -5234430496355539041L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "FILE_NAME", length = 100)
	private String fileName;// 文件名称

	@Column(name = "FILE_ID", length = 100)
	private String fileId;// 文件ID

	@Column(name = "USER_ID", length = 32)
	private String userId;// 关联项目

	@Column(name = "LOG_DATE")
	private Date logDate;// 上传时间

	@Column(name = "LOG_CONTENT", length = 32)
	private String logContent;//

	@Column(name = "CLASS_NAME", length = 100)
	private String className;// 文件名称

	@Column(name = "MODIFY_CONTENT", length = 4000)
	private String modifyContent;//

	@Column(name = "MODIFY_ITEM_CONTENT", length = 4000)
	private String modifyItemContent;//

	@Column(name = "CODE", length = 255)
	private String code;// 编号
	
	@Column(name = "REASON", length = 1000)
	private String reason;// 修改原因
	
	private String state;
	private String stateName;
	
	
	

	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getLogDate() {
		return logDate;
	}

	public void setLogDate(Date logDate) {
		this.logDate = logDate;
	}

	public String getLogContent() {
		return logContent;
	}

	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getModifyContent() {
		return modifyContent;
	}

	public void setModifyContent(String modifyContent) {
		this.modifyContent = modifyContent;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getModifyItemContent() {
		return modifyItemContent;
	}

	public void setModifyItemContent(String modifyItemContent) {
		this.modifyItemContent = modifyItemContent;
	}

}
