package com.biolims.log.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.log.model.LogInfo;
import com.opensymphony.xwork2.ActionContext;

@Repository
public class LogInfoDao extends BaseHibernateDao {
	public Map<String, Object> selectLogInfoList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from LogInfo where 1=1 ";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<LogInfo> list = new ArrayList<LogInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by logDate DESC";
			}
			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询日志表
	 * 
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public LogInfo getLogInfo(String id) {
		String hql = "select id from t_log_info where file_id='" + id
				+ "' order by log_Date desc";
		List<String> list = new ArrayList<String>();
		list = this.getSession().createSQLQuery(hql).list();
		if (list.size() > 0) {
			LogInfo li = new LogInfo();
			li = get(LogInfo.class, list.get(0));
			return li;
		}
		return null;
	}

	/**
	 * 根据id和程序名称查询（ 两个不同的实体id相同时用到）
	 * 
	 * @author xierenjie
	 * @param id
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public LogInfo getLogInfobyidandclassName(String id, String className) {
		String hql = "select id from t_log_info where file_id='" + id
				+ "' and class_Name='" + className + "' order by log_Date desc";
		List<String> list = new ArrayList<String>();
		list = this.getSession().createSQLQuery(hql).list();
		if (list.size() > 0) {
			LogInfo li = new LogInfo();
			li = get(LogInfo.class, list.get(0));
			return li;
		}
		return null;
	}

	/**
	 * @throws Exception
	 * @Title: findLogInfoList
	 * @Description: 展示列表
	 * @author : shengwei.wang
	 * @date 2018年7月12日下午2:17:29
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findLogInfoList(Integer start, Integer length,
			String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from LogInfo where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from LogInfo where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key += " order by " + col + " " + sort;
			}
			List<DnaTask> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

}
