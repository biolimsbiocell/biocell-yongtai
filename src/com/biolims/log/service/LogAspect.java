package com.biolims.log.service;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.aspectj.lang.JoinPoint;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.BaseUser;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;

/**
 * ***********************************************
 * 
 *Copyright (C), 2008-2009, DHCC Tech. Co., Ltd. 
 *File name: LogAspect.java
 * Version: 
 * Date: 2012-6-21
 * Description: 日志切面类，完成各种情况的日志记录工作
 ************************************************ 
 */
public class LogAspect {

	@Resource
	private CommonDAO commonDAO;
	private static DateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	Logger logger = Logger.getLogger(LogAspect.class);
	private static Logger operInfologger = Logger.getLogger("operInfo");

	/*
	 * @Pointcut("execution(* com.dhcc.dthealth.test.service..*.insert*(..)) "
	 * +"|| execution(* com.dhcc.dthealth.test.service..*.update*(..)) " +
	 * "|| execution(* com.dhcc.dthealth.test.service..*.remove*(..))" +
	 * "|| execution(* com.dhcc.dthealth.test.service..*.delete*(..))") public
	 * void writeOperLogPointcut() { }
	 *  
	 * @AfterReturning("writeOperLogPointcut()")
	 */

	/**
	 * 系统运行正常的时候，负责把用户的操作记录到数据库中（只针对关键操作）
	 * 
	 * @param joinPoint
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void writeOperLogInfo(JoinPoint joinPoint) throws Exception {

		//		System.out.println(joinPoint.toString());
		//		System.out.println(joinPoint.toShortString());
		//		System.out.println(joinPoint.toLongString());
		//		System.out.println(joinPoint.getThis().toString());
		//		System.out.println(joinPoint.getTarget().toString());
		//		System.out.println(joinPoint.getArgs().toString());
		//		System.out.println(joinPoint.getKind());
		//		System.out.println(joinPoint.getStaticPart());
		try {
			Object[] objs = joinPoint.getArgs();
			String params = operParams(objs);
			String OperType = joinPoint.getSignature().getName();
			HttpSession session = ServletActionContext.getRequest().getSession();
			BaseUser user = (BaseUser) session.getAttribute("userSession");
			operInfologger.info("[" + joinPoint.getTarget().toString() + "] 正常[操作日志][" + sdf.format(new Date()) + "]"
					+ user.getId() + "做了" + OperType + "操作!!");

			String objId = "";
			String objModifyInfo = "";
			for (Object obj : objs) {
				if (!(obj instanceof String)) {
					try {
						objId = (String) BeanUtils.getFieldValue(obj, "id");
						operInfologger.info(obj.getClass().getName() + " ID为'" + objId + "'");

						objModifyInfo = (String) BeanUtils.getFieldValue(obj, "modifyInfo");
						if (!objModifyInfo.equals(""))
							operInfologger.info(" 修改信息为'" + objModifyInfo + "'");

					} catch (Exception e) {
						//e.printStackTrace();

					}

				} else {

					if (!obj.equals("")) {
						operInfologger.info(" 保存明细内容为'" + obj + "'");

					}
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("[" + joinPoint.getTarget().toString() + "] 异常[操作日志][" + sdf.format(new Date()) + "]");

		}

	}

	public void writeOperLogInfoTable(JoinPoint joinPoint) throws Exception {

		//		System.out.println(joinPoint.toString());
		//		System.out.println(joinPoint.toShortString());
		//		System.out.println(joinPoint.toLongString());
		//		System.out.println(joinPoint.getThis().toString());
		//		System.out.println(joinPoint.getTarget().toString());
		//		System.out.println(joinPoint.getArgs().toString());
		//		System.out.println(joinPoint.getKind());
		//		System.out.println(joinPoint.getStaticPart());
		try {
			Object[] objs = joinPoint.getArgs();
			String params = operParams(objs);
			String OperType = joinPoint.getSignature().getName();
			HttpSession session = ServletActionContext.getRequest().getSession();
			BaseUser user = (BaseUser) session.getAttribute("userSession");

			String objId = "";
			String objModifyInfo = "";
			for (Object obj : objs) {
				if (!(obj instanceof String)) {
					try {
						objId = (String) BeanUtils.getFieldValue(obj, "id");
						objModifyInfo = (String) BeanUtils.getFieldValue(obj, "modifyInfo");
						if (!objModifyInfo.equals("")) {
							LogInfo li = new LogInfo();
							li.setFileName(joinPoint.getTarget().toString());
							li.setLogContent(OperType);
							li.setUserId(user.getId());
							li.setLogDate(new Date());
							li.setClassName(obj.getClass().getName());
							li.setFileId(objId);
							li.setModifyContent(objModifyInfo);
							commonDAO.saveOrUpdate(li);
						}

					} catch (Exception e) {
						//e.printStackTrace();

					}

				}
			}
		} catch (Exception e) {
			//e.printStackTrace();

		}

	}

	/**
	 * 系统运行异常的时候，负责把用户的操作记录到数据库中（只针对关键操作）
	 * 
	 * @param joinPoint
	 * @param throwable
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void writeExcepOperLogInfo(JoinPoint joinPoint, Throwable throwable) throws Exception {
		try {
			Object[] objs = joinPoint.getArgs();
			String params = operParams(objs);
			String OperType = joinPoint.getSignature().getName();
			HttpSession session = ServletActionContext.getRequest().getSession();
			BaseUser user = (BaseUser) session.getAttribute("userSession");
			logger.error("[" + joinPoint.getTarget().toString() + "] 异常[操作日志][" + sdf.format(new Date()) + "]"
					+ user.getId() + "做了" + OperType + "操作!!但是出了异常：" + throwable.getMessage());
			logger.error(OperType + "：参数或ID为(" + params + ")");
			throwable.printStackTrace();
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("[" + joinPoint.getTarget().toString() + "] 异常[操作日志][" + sdf.format(new Date()) + "]");

		}
	}

	/*
	 * @Pointcut("execution(* com.dhcc.dthealth.test.service..*.*(..)) ") public
	 * void writeExcepLogPointcut() { }
	 * 
	 * @AfterThrowing("writeExcepLogPointcut()")
	 */

	/**
	 * 系统运行异常的时候，负责把异常的信息记录到日志文件中
	 * 
	 * @param joinPoint
	 * @param throwable
	 * @throws ClassNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void writeExcepLogInfo(JoinPoint joinPoint, Throwable throwable) throws Exception {

		try {
			Object[] objs = joinPoint.getArgs();
			String params = operParams(objs);
			String OperType = joinPoint.getSignature().getName();
			HttpSession session = ServletActionContext.getRequest().getSession();
			BaseUser user = (BaseUser) session.getAttribute("userSession");
			logger.error("[" + joinPoint.getTarget().toString() + "] 异常[系统日志][" + sdf.format(new Date()) + "]"
					+ user.getId() + "做了" + OperType + "操作!!但是出了异常：" + throwable.getMessage());
			logger.error(OperType + "：参数或ID为(" + params + ")");

			throwable.printStackTrace();
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("[" + joinPoint.getTarget().toString() + "] 异常[操作日志][" + sdf.format(new Date()) + "]");

		}
	}

	private String operParams(Object[] objs) throws Exception {
		String params = "";
		for (Object obj : objs) {
			if (!(obj instanceof String)) {
				try {
					params = (String) BeanUtils.getFieldValue(obj, "id");
					break;
				} catch (Exception e) {
					//e.printStackTrace();
					if (obj != null) {
						params = params + obj.toString() + ",";
					} else {
						params = params + "null,";
					}
				}

			} else {
				params = params + obj + ",";
			}
		}
		return params;
	}

}
