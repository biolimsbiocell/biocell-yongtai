package com.biolims.log.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.GetObjectName;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.dao.LogInfoDao;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class LogInfoService extends CommonService {
	@Resource
	private LogInfoDao logInfoDao;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 字典主类的列表
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findLogInfoList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		return logInfoDao.selectLogInfoList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	/**
	 * 表单类型的保存和修改
	 * 
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLogInfoList(String itemDataJson) throws Exception {
		List<LogInfo> saveItems = new ArrayList<LogInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			String orderNumber = (String) map.get("orderNumber");
			if (orderNumber != null && orderNumber.length() > 0) {
				map.put("orderNumber", Integer.valueOf(orderNumber));
			} else {
				map.put("orderNumber", null);
			}
			LogInfo dmt = new LogInfo();
			// 将map信息读入实体类
			dmt = (LogInfo) logInfoDao.Map2Bean(map, dmt);
			saveItems.add(dmt);
		}
		logInfoDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 表单类型的删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delLogInfo(String[] ids) throws Exception {
		for (String id : ids) {
			LogInfo at = logInfoDao.get(LogInfo.class, id);
			/* at.setState("0"); */
			logInfoDao.saveOrUpdate(at);
		}
	}

	/**
	 * 解析对象
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> explain(Object oldObject) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		Field[] inputFields = oldObject.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			try {
				Object olds = BeanUtils.getFieldValue(oldObject, fd.getName());
				String data = JsonUtils.toJsonString(olds);
				if (data.contains("{")) {
					String[] oo = data.split("\"");
					data = oo[3];
					if ("".equals(data) || data == null || "\"\"".equals(data)) {

					} else {
						if (fd.getAnnotation(GetObjectName.class) != null)
							inputMap.put(fd.getAnnotation(GetObjectName.class)
									.name(), data);
					}
				} else {
					if ("".equals(data) || data == null || "\"\"".equals(data)) {

					} else {
						if (fd.getAnnotation(GetObjectName.class) != null)
							inputMap.put(fd.getAnnotation(GetObjectName.class)
									.name(), olds);
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return inputMap;
	}

	/**
	 * 两个对象之间的比较
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, String> compare(Object oldObject, Object newObject) {
		Map<String, String> inputMap = new HashMap<String, String>();
		Field[] inputFields = oldObject.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			try {
				Object olds = BeanUtils.getFieldValue(oldObject, fd.getName());
				Object fresh = BeanUtils.getFieldValue(newObject, fd.getName());
				String o = JsonUtils.toJsonString(olds);
				String f = JsonUtils.toJsonString(fresh);
				if (!o.equals(f)) {
					if (o.contains("{")) {
						String[] oo = o.split("\"");
						String[] ff = f.split("\"");
						if (oo.length > 2)
							o = oo[3];
						if (ff.length > 2)
							f = ff[3];
					}
					if (("".equals(o) && "".equals(f))
							|| (o == null && f == null)
							|| ("\"\"".equals(o) && "\"\"".equals(f))) {

					} else {
						if (fd.getAnnotation(GetObjectName.class) != null) {
							String change = "由" + o + "变为" + f;
							inputMap.put(fd.getAnnotation(GetObjectName.class)
									.name(), change);
						}

					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return inputMap;
	}

	/**
	 * 删除通用
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delLogInfo(Object scp, String id, String userIds, String code,
			String title, String reason) throws Exception {
		// String str = "文库信息,科研认领,临床信息一录,临床信息二录";
		LogInfo lif = null;
		// if (str.indexOf(title) > -1) {
		lif = logInfoDao.getLogInfobyidandclassName(id, title);
		// } else {
		// lif = logInfoDao.getLogInfo(id);
		// }

		if (lif != null) {
			Map<String, Object> map1 = explain(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setReason(reason);
			li.setClassName(title);
			li.setFileId(id);
			li.setCode(code);
			li.setUserId(userIds);
			li.setModifyItemContent(JsonUtils.toJsonString(scp));
			li.setModifyContent("删除编号为：" + id + "的记录:"
					+ JsonUtils.toJsonString(map1).replace("\\", ""));
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 新建 通用
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLogInfo(Object scp, String id, String userIds, String code,
			String title, String reason) throws Exception {
		// String str = "文库信息,科研认领,临床信息一录,临床信息二录";
		LogInfo lif = null;
		// if (str.indexOf(title) > -1) {
		lif = logInfoDao.getLogInfobyidandclassName(id, title);
		// } else {
		// lif = logInfoDao.getLogInfo(id);
		// }
		if (lif == null) {
			Map<String, Object> map1 = explain(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setClassName(title);
			li.setFileId(id);
			li.setCode(code);
			li.setUserId(userIds);
			li.setReason(reason);
			li.setModifyItemContent(JsonUtils.toJsonString(scp));
			li.setModifyContent("新建记录："
					+ JsonUtils.toJsonString(map1).replace("\\", ""));
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 修改 通用
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void updateLogInfo(Object scp, String id, String userIds,
			String code, String title, String reason, Object prmi)
			throws Exception {
		if (prmi == null) {
			Map<String, Object> map1 = explain(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setClassName(title);
			li.setFileId(id);
			li.setCode(code);
			li.setUserId(userIds);
			li.setReason(reason);
			li.setModifyItemContent(JsonUtils.toJsonString(scp));
			li.setModifyContent("新建记录："
					+ JsonUtils.toJsonString(map1).replace("\\", ""));
			commonDAO.saveOrUpdate(li);
		} else {
			// String str = "文库信息,科研认领,临床信息一录,临床信息二录";
			LogInfo lif = null;
			// if (str.indexOf(title) > -1) {
			lif = logInfoDao.getLogInfobyidandclassName(id, title);
			// } else {
			// lif = logInfoDao.getLogInfo(id);
			// }
			if (lif != null) {
				if (!JsonUtils.toJsonString(scp).equals(
						lif.getModifyItemContent())) {
					Map<String, String> map2 = compare(prmi, scp);
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setClassName(title);
					li.setFileId(id);
					li.setCode(code);
					li.setUserId(userIds);
					li.setReason(reason);
					li.setModifyItemContent(JsonUtils.toJsonString(scp));
					li.setModifyContent("修改编号为：" + id + "的记录中:"
							+ JsonUtils.toJsonString(map2).replace("\\", ""));
					commonDAO.saveOrUpdate(li);
				}
			}
		}

	}

	/**
	 * @throws Exception
	 * @Title: findLogInfoList
	 * @Description: 展示日志列表
	 * @author : shengwei.wang
	 * @date 2018年7月12日下午2:15:58
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findLogInfoList(Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return logInfoDao.findLogInfoList(start, length, query, col, sort);
	}
}
