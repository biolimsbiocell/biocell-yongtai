package com.biolims.log.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.log.model.LogInfo;
import com.biolims.log.service.LogInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

;

@Namespace("/logInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class LogInfoAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 184141464911926413L;

	private String rightsId = "logInfo";

	private LogInfoAction dmt;

	@Resource
	private LogInfoService logInfoService;

	/**
	 * 日志列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showLogInfoList")
	public String showLogInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/log/showLogInfoList.jsp");
	}

	/**
	 * 字典主类别列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showLogInfoListJson")
	public void showLogInfoListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = this.logInfoService.findLogInfoList(
					start, length, query, col, sort);
			List<LogInfo> list = (List<LogInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fileName", "");
			map.put("fileId", "");
			map.put("userId", "");
			map.put("logDate", "yyyy-MM-dd HH:mm:ss");
			map.put("logContent", "");
			map.put("className", "");
			map.put("modifyContent", "");
			map.put("modifyItemContent", "");
			map.put("code", "");
			map.put("reason", "");
			map.put("state", "");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存字典主类别
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveLogInfoList")
	public void saveLogInfoList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			logInfoService.saveLogInfoList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除表单类型
	 * 
	 * @throws Exception
	 */
	@Action(value = "delLogInfo")
	public void delLogInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			logInfoService.delLogInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public LogInfoAction getDmt() {
		return dmt;
	}

	public void setDmt(LogInfoAction dmt) {
		this.dmt = dmt;
	}

	/**
	 * @return the logInfoService
	 */
	public LogInfoService getLogInfoService() {
		return logInfoService;
	}

	/**
	 * @param logInfoService
	 *            the logInfoService to set
	 */
	public void setLogInfoService(LogInfoService logInfoService) {
		this.logInfoService = logInfoService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
