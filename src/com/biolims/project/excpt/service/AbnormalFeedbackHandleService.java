package com.biolims.project.excpt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.excpt.dao.AbnormalFeedbackHandleDao;
import com.biolims.project.excpt.model.AbnormalFeedbackHandle;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AbnormalFeedbackHandleService {
	@Resource
	private AbnormalFeedbackHandleDao abnormalFeedbackHandleDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAbnormalFeedbackHandleList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return abnormalFeedbackHandleDao.selectAbnormalFeedbackHandleList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AbnormalFeedbackHandle i) throws Exception {

		abnormalFeedbackHandleDao.saveOrUpdate(i);

	}
	public AbnormalFeedbackHandle get(String id) {
		AbnormalFeedbackHandle abnormalFeedbackHandle = commonDAO.get(AbnormalFeedbackHandle.class, id);
		return abnormalFeedbackHandle;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AbnormalFeedbackHandle sc, Map jsonMap) throws Exception {
		if (sc != null) {
			abnormalFeedbackHandleDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
