﻿
package com.biolims.project.excpt.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.excpt.model.AbnormalFeedbackHandle;
import com.biolims.project.excpt.service.AbnormalFeedbackHandleService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/project/excpt/abnormalFeedbackHandle")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AbnormalFeedbackHandleAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "22011";
	@Autowired
	private AbnormalFeedbackHandleService abnormalFeedbackHandleService;
	private AbnormalFeedbackHandle abnormalFeedbackHandle = new AbnormalFeedbackHandle();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showAbnormalFeedbackHandleList")
	public String showAbnormalFeedbackHandleList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/excption/abnormalFeedbackHandle.jsp");
	}

	@Action(value = "showAbnormalFeedbackHandleListJson")
	public void showAbnormalFeedbackHandleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = abnormalFeedbackHandleService.findAbnormalFeedbackHandleList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AbnormalFeedbackHandle> list = (List<AbnormalFeedbackHandle>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("bluk", "");
		map.put("resultDecide-id", "");
		map.put("resultDecide-name", "");
		map.put("nextFlow", "");
		map.put("handleIdea", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "abnormalFeedbackHandleSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAbnormalFeedbackHandleList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/excption/abnormalFeedbackHandleDialog.jsp");
	}

	@Action(value = "showDialogAbnormalFeedbackHandleListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAbnormalFeedbackHandleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = abnormalFeedbackHandleService.findAbnormalFeedbackHandleList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AbnormalFeedbackHandle> list = (List<AbnormalFeedbackHandle>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("bluk", "");
		map.put("resultDecide-id", "");
		map.put("resultDecide-name", "");
		map.put("nextFlow", "");
		map.put("handleIdea", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editAbnormalFeedbackHandle")
	public String editAbnormalFeedbackHandle() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			abnormalFeedbackHandle = abnormalFeedbackHandleService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "abnormalFeedbackHandle");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			abnormalFeedbackHandle.setCreateUser(user);
//			abnormalFeedbackHandle.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/excption/abnormalFeedbackHandleEdit.jsp");
	}

	@Action(value = "copyAbnormalFeedbackHandle")
	public String copyAbnormalFeedbackHandle() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		abnormalFeedbackHandle = abnormalFeedbackHandleService.get(id);
		abnormalFeedbackHandle.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/excption/abnormalFeedbackHandleEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = abnormalFeedbackHandle.getId();
		if(id!=null&&id.equals("")){
			abnormalFeedbackHandle.setId(null);
		}
		Map aMap = new HashMap();
		abnormalFeedbackHandleService.save(abnormalFeedbackHandle,aMap);
		return redirect("/project/excpt/abnormalFeedbackHandle/editAbnormalFeedbackHandle.action?id=" + abnormalFeedbackHandle.getId());

	}

	@Action(value = "viewAbnormalFeedbackHandle")
	public String toViewAbnormalFeedbackHandle() throws Exception {
		String id = getParameterFromRequest("id");
		abnormalFeedbackHandle = abnormalFeedbackHandleService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/excption/abnormalFeedbackHandleEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AbnormalFeedbackHandleService getAbnormalFeedbackHandleService() {
		return abnormalFeedbackHandleService;
	}

	public void setAbnormalFeedbackHandleService(AbnormalFeedbackHandleService abnormalFeedbackHandleService) {
		this.abnormalFeedbackHandleService = abnormalFeedbackHandleService;
	}

	public AbnormalFeedbackHandle getAbnormalFeedbackHandle() {
		return abnormalFeedbackHandle;
	}

	public void setAbnormalFeedbackHandle(AbnormalFeedbackHandle abnormalFeedbackHandle) {
		this.abnormalFeedbackHandle = abnormalFeedbackHandle;
	}


}
