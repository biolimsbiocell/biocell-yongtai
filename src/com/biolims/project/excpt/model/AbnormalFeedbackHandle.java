package com.biolims.project.excpt.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 异常反馈处理
 * @author lims-platform
 * @date 2015-11-12 11:07:39
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ABNORMAL_FEEDBACK_HANDLE")
@SuppressWarnings("serial")
public class AbnormalFeedbackHandle extends EntityDao<AbnormalFeedbackHandle> implements java.io.Serializable {
	/**血浆编号*/
	private String id;

	/**体积*/
	private String bluk;

	private String resultDecide;
	/**下一步流向*/
	/**下一步流向*/
	private String nextFlow;
	/**处理意见*/
	/**处理意见*/
	private String handleIdea;
	/**备注*/
	/**备注*/
	private String note;
	
	public String getResultDecide() {
		return resultDecide;
	}
	public void setResultDecide(String resultDecide) {
		this.resultDecide = resultDecide;
	}
	public String getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	public String getHandleIdea() {
		return handleIdea;
	}
	public void setHandleIdea(String handleIdea) {
		this.handleIdea = handleIdea;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  血浆编号
	 */
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  血浆编号
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  体积
	 */
	@Column(name ="BLUK", length = 100)
	public String getBluk(){
		return this.bluk;
	}
	/**
	 *方法: 设置String
	 *@param: String  体积
	 */
	public void setBluk(String bluk){
		this.bluk = bluk;
	}
	
	
}