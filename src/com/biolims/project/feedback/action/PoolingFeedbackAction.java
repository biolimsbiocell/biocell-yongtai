﻿
package com.biolims.project.feedback.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.feedback.model.FeedbackPooling;
import com.biolims.project.feedback.service.PoolingFeedbackService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/project/feedback/poolingFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PoolingFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private PoolingFeedbackService poolingFeedbackService;
	private FeedbackPooling poolingFeedback = new FeedbackPooling();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showPoolingFeedbackList")
	public String showPoolingFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/poolingFeedback.jsp");
	}

	@Action(value = "showPoolingFeedbackListJson")
	public void showPoolingFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = poolingFeedbackService.findPoolingFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackPooling> list = (List<FeedbackPooling>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("libraryType", "");
		map.put("mixAmount", "");
		
		map.put("mixVolume", "");
		map.put("qpcrConcentration", "");
		map.put("note", "");
		map.put("volume", "");
		map.put("concentrer", "");
		
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		
		map.put("poolingCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "");
		map.put("phone", "");
		
		map.put("orderId", "");
		map.put("others", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		
		map.put("sequencingReadLong", "");
		map.put("sequencingType", "");
		map.put("sequencingPlatform", "");
		map.put("totalAmount", "");
		map.put("totalVolume", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "poolingFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPoolingFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/poolingFeedbackDialog.jsp");
	}

	@Action(value = "showDialogPoolingFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPoolingFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = poolingFeedbackService.findPoolingFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackPooling> list = (List<FeedbackPooling>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("bluk", "");
		map.put("concentrer", "");
		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editPoolingFeedback")
	public String editPoolingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			poolingFeedback = poolingFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "poolingFeedback");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			poolingFeedback.setCreateUser(user);
//			poolingFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/poolingFeedbackEdit.jsp");
	}

	@Action(value = "copyPoolingFeedback")
	public String copyPoolingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		poolingFeedback = poolingFeedbackService.get(id);
		poolingFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/poolingFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = poolingFeedback.getId();
		if(id!=null&&id.equals("")){
			poolingFeedback.setId(null);
		}
		Map aMap = new HashMap();
		poolingFeedbackService.save(poolingFeedback,aMap);
		return redirect("/project/feedback/poolingFeedback/editPoolingFeedback.action?id=" + poolingFeedback.getId());

	}

	@Action(value = "viewPoolingFeedback")
	public String toViewPoolingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		poolingFeedback = poolingFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/poolingFeedbackEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PoolingFeedbackService getPoolingFeedbackService() {
		return poolingFeedbackService;
	}

	public void setPoolingFeedbackService(PoolingFeedbackService poolingFeedbackService) {
		this.poolingFeedbackService = poolingFeedbackService;
	}

	public FeedbackPooling getPoolingFeedback() {
		return poolingFeedback;
	}

	public void setPoolingFeedback(FeedbackPooling poolingFeedback) {
		this.poolingFeedback = poolingFeedback;
	}


}
