
package com.biolims.project.feedback.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackQuality;
import com.biolims.project.feedback.service.QualityFeedbackService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/project/feedback/qualityFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private QualityFeedbackService qualityFeedbackService;
	private FeedbackQuality qualityFeedback = new FeedbackQuality();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showQualityFeedbackList")
	public String showQualityFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/qualityFeedback.jsp");
	}

	@Action(value = "showQualityFeedbackListJson")
	public void showQualityFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityFeedbackService.findQualityFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleQc2100Info> list = (List<SampleQc2100Info>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("orderId", "");
		map.put("wkId", "");
		map.put("indexa", "");
		map.put("fragmentSize", "");
		map.put("concentration", "");
		map.put("qpcr", "");
		map.put("specific", "");
		map.put("result", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("nextFlow", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("wKQualitySampleTask-name", "");
		map.put("wKQualitySampleTask-id", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	
	
	@Action(value = "qualityFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQualityFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/qualityFeedbackDialog.jsp");
	}

	@Action(value = "showDialogQualityFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQualityFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityFeedbackService.findQualityFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackQuality> list = (List<FeedbackQuality>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
	
		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editQualityFeedback")
	public String editQualityFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qualityFeedback = qualityFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qualityFeedback");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			qualityFeedback.setCreateUser(user);
//			qualityFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/qualityFeedbackEdit.jsp");
	}

	@Action(value = "copyQualityFeedback")
	public String copyQualityFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qualityFeedback = qualityFeedbackService.get(id);
		qualityFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/qualityFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = qualityFeedback.getId();
		if(id!=null&&id.equals("")){
			qualityFeedback.setId(null);
		}
		Map aMap = new HashMap();
		qualityFeedbackService.save(qualityFeedback,aMap);
		return redirect("/project/feedback/qualityFeedback/editQualityFeedback.action?id=" + qualityFeedback.getId());

	}

	@Action(value = "viewQualityFeedback")
	public String toViewQualityFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		qualityFeedback = qualityFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/qualityFeedbackEdit.jsp");
	}
	/**
	 * 保存质控
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveQualityFeedback", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveQualityFeedback() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			//SampleQc2100Info a = new SampleQc2100Info();
			qualityFeedbackService.saveQualityFeedback(dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 保存质控
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveQpcrFeedback", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveQpcrFeedback() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			//SampleQcQpcrInfo a = new SampleQcQpcrInfo();
			qualityFeedbackService.saveQpcrFeedback(dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityFeedbackService getQualityFeedbackService() {
		return qualityFeedbackService;
	}

	public void setQualityFeedbackService(QualityFeedbackService qualityFeedbackService) {
		this.qualityFeedbackService = qualityFeedbackService;
	}

	public FeedbackQuality getQualityFeedback() {
		return qualityFeedback;
	}

	public void setQualityFeedback(FeedbackQuality qualityFeedback) {
		this.qualityFeedback = qualityFeedback;
	}


}
