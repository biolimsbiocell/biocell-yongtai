﻿
package com.biolims.project.feedback.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.feedback.model.FeedbackPlasma;
import com.biolims.project.feedback.service.PlasmaFeedbackService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.file.service.FileInfoService;
@Namespace("/project/feedback/plasmaFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PlasmaFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private PlasmaFeedbackService plasmaFeedbackService;
	private FeedbackPlasma plasmaFeedback = new FeedbackPlasma();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showPlasmaFeedbackList")
	public String showPlasmaFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/plasmaFeedback.jsp");
	}

	@Action(value = "showPlasmaFeedbackListJson")
	public void showPlasmaFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = plasmaFeedbackService.findPlasmaFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackPlasma> list = (List<FeedbackPlasma>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("bloodCode", "");
		map.put("sampleCode", "");
		map.put("volume", "");
		
		map.put("state", "");
		map.put("method", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("patientName", "");
		
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("backDate", "");
		map.put("treatDate", "");
		
		map.put("sequenceFun", "");
		map.put("reportDate", "");
		map.put("phone", "");
		map.put("orderId", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "plasmaFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPlasmaFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/plasmaFeedbackDialog.jsp");
	}

	@Action(value = "showDialogPlasmaFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPlasmaFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = plasmaFeedbackService.findPlasmaFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackPlasma> list = (List<FeedbackPlasma>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("bloodCode", "");
		map.put("sampleCode", "");
		map.put("volume", "");
		
		map.put("state", "");
		map.put("method", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("patientName", "");
		
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		
		map.put("sequenceFun", "");
		map.put("reportDate", "");
		map.put("phone", "");
		map.put("orderId", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editPlasmaFeedback")
	public String editPlasmaFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			plasmaFeedback = plasmaFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "plasmaFeedback");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			plasmaFeedback.setCreateUser(user);
//			plasmaFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/plasmaFeedbackEdit.jsp");
	}

	@Action(value = "copyPlasmaFeedback")
	public String copyPlasmaFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		plasmaFeedback = plasmaFeedbackService.get(id);
		plasmaFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/plasmaFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = plasmaFeedback.getId();
		if(id!=null&&id.equals("")){
			plasmaFeedback.setId(null);
		}
		Map aMap = new HashMap();
		plasmaFeedbackService.save(plasmaFeedback,aMap);
		return redirect("/project/feedback/plasmaFeedback/editPlasmaFeedback.action?id=" + plasmaFeedback.getId());

	}

	@Action(value = "viewPlasmaFeedback")
	public String toViewPlasmaFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		plasmaFeedback = plasmaFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/plasmaFeedbackEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PlasmaFeedbackService getPlasmaFeedbackService() {
		return plasmaFeedbackService;
	}

	public void setPlasmaFeedbackService(PlasmaFeedbackService plasmaFeedbackService) {
		this.plasmaFeedbackService = plasmaFeedbackService;
	}

	public FeedbackPlasma getPlasmaFeedback() {
		return plasmaFeedback;
	}

	public void setPlasmaFeedback(FeedbackPlasma plasmaFeedback) {
		this.plasmaFeedback = plasmaFeedback;
	}


}
