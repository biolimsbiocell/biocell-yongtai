﻿
package com.biolims.project.feedback.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.project.feedback.service.WkFeedbackService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/project/feedback/wkFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private WkFeedbackService wkFeedbackService;
	private FeedbackWk wkFeedback = new FeedbackWk();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showWkFeedbackList")
	public String showWkFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/wkFeedback.jsp");
	}

	@Action(value = "showWkFeedbackListJson")
	public void showWkFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkFeedbackService.findWkFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkTaskInfo> list = (List<WkTaskInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		map.put("sampleCode", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("reason", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("note", "");
		map.put("state", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("wk-name", "");
		map.put("wk-id", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "wkFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWkFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/wkFeedbackDialog.jsp");
	}

	@Action(value = "showDialogWkFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWkFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkFeedbackService.findWkFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackWk> list = (List<FeedbackWk>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("bluk", "");
		map.put("indexs", "");
		map.put("concentrer", "");
		map.put("qctype", "");
		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editWkFeedback")
	public String editWkFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wkFeedback = wkFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wkFeedback");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			wkFeedback.setCreateUser(user);
//			wkFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/wkFeedbackEdit.jsp");
	}

	@Action(value = "copyWkFeedback")
	public String copyWkFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wkFeedback = wkFeedbackService.get(id);
		wkFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/wkFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = wkFeedback.getId();
		if(id!=null&&id.equals("")){
			wkFeedback.setId(null);
		}
		Map aMap = new HashMap();
		wkFeedbackService.save(wkFeedback,aMap);
		return redirect("/project/feedback/wkFeedback/editWkFeedback.action?id=" + wkFeedback.getId());

	}

	@Action(value = "viewWkFeedback")
	public String toViewWkFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		wkFeedback = wkFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/wkFeedbackEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WkFeedbackService getWkFeedbackService() {
		return wkFeedbackService;
	}

	public void setWkFeedbackService(WkFeedbackService wkFeedbackService) {
		this.wkFeedbackService = wkFeedbackService;
	}

	public FeedbackWk getWkFeedback() {
		return wkFeedback;
	}

	public void setWkFeedback(FeedbackWk wkFeedback) {
		this.wkFeedback = wkFeedback;
	}


}
