
package com.biolims.project.feedback.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackDeSequencing;
import com.biolims.project.feedback.service.DeSequencingFeedbackService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/project/feedback/deSequencingFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DeSequencingFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private DeSequencingFeedbackService deSequencingFeedbackService;
	private FeedbackDeSequencing deSequencingFeedback = new FeedbackDeSequencing();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDeSequencingFeedbackList")
	public String showDeSequencingFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/deSequencingFeedback.jsp");
	}

	@Action(value = "showDeSequencingFeedbackListJson")
	public void showDeSequencingFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = deSequencingFeedbackService.findDeSequencingFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackDeSequencing> list = (List<FeedbackDeSequencing>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		
		map.put("submit", "");
		map.put("result", "");
		map.put("indexs", "");
		map.put("code", "");
		map.put("wkNum", "");
		
		map.put("estimate", "");
		map.put("backDate", "");
		map.put("errorRate", "");
		map.put("isRun", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "deSequencingFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDeSequencingFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/deSequencingFeedbackDialog.jsp");
	}

	@Action(value = "showDialogDeSequencingFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDeSequencingFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = deSequencingFeedbackService.findDeSequencingFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackDeSequencing> list = (List<FeedbackDeSequencing>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		
		map.put("submit", "");
		map.put("result", "");
		map.put("indexs", "");
		map.put("code", "");
		map.put("wkNum", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editDeSequencingFeedback")
	public String editDeSequencingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			deSequencingFeedback = deSequencingFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "deSequencingFeedback");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			deSequencingFeedback.setCreateUser(user);
//			deSequencingFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/deSequencingFeedbackEdit.jsp");
	}

	@Action(value = "copyDeSequencingFeedback")
	public String copyDeSequencingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		deSequencingFeedback = deSequencingFeedbackService.get(id);
		deSequencingFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/deSequencingFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = deSequencingFeedback.getId();
		if(id!=null&&id.equals("")){
			deSequencingFeedback.setId(null);
		}
		Map aMap = new HashMap();
		deSequencingFeedbackService.save(deSequencingFeedback,aMap);
		return redirect("/project/feedback/deSequencingFeedback/editDeSequencingFeedback.action?id=" + deSequencingFeedback.getId());

	}

	@Action(value = "viewDeSequencingFeedback")
	public String toViewDeSequencingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		deSequencingFeedback = deSequencingFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/deSequencingFeedbackEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DeSequencingFeedbackService getDeSequencingFeedbackService() {
		return deSequencingFeedbackService;
	}

	public void setDeSequencingFeedbackService(DeSequencingFeedbackService deSequencingFeedbackService) {
		this.deSequencingFeedbackService = deSequencingFeedbackService;
	}

	public FeedbackDeSequencing getDeSequencingFeedback() {
		return deSequencingFeedback;
	}

	public void setDeSequencingFeedback(FeedbackDeSequencing deSequencingFeedback) {
		this.deSequencingFeedback = deSequencingFeedback;
	}


}
