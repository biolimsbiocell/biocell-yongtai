﻿
package com.biolims.project.feedback.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.feedback.model.FeedbackDna;
import com.biolims.project.feedback.service.DnaFeedbackService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/project/feedback/dnaFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DnaFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private DnaFeedbackService dnaFeedbackService;
	private FeedbackDna dnaFeedback = new FeedbackDna();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDnaFeedbackList")
	public String showDnaFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/dnaFeedback.jsp");
	}

	@Action(value = "showDnaFeedbackListJson")
	public void showDnaFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dnaFeedbackService.findDnaFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackDna> list = (List<FeedbackDna>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("inspectDate", "");
		map.put("idCard", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "");
		map.put("volume", "");
		map.put("od230", "");
		map.put("od280", "");
		map.put("concentrer", "");
		map.put("result", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		map.put("classify", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "dnaFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDnaFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/dnaFeedbackDialog.jsp");
	}

	@Action(value = "showDialogDnaFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDnaFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dnaFeedbackService.findDnaFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackDna> list = (List<FeedbackDna>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("dnaCode", "");
		map.put("sampleCode", "");
		map.put("bluk", "");
		map.put("od230", "");
		map.put("od280", "");
		map.put("concentrer", "");
		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editDnaFeedback")
	public String editDnaFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dnaFeedback = dnaFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dnaFeedback");
		} else {
//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			dnaFeedback.setCreateUser(user);
//			dnaFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/dnaFeedbackEdit.jsp");
	}

	@Action(value = "copyDnaFeedback")
	public String copyDnaFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dnaFeedback = dnaFeedbackService.get(id);
		dnaFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/dnaFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = dnaFeedback.getId();
		if(id!=null&&id.equals("")){
			dnaFeedback.setId(null);
		}
		Map aMap = new HashMap();
		dnaFeedbackService.save(dnaFeedback,aMap);
		return redirect("/project/feedback/dnaFeedback/editDnaFeedback.action?id=" + dnaFeedback.getId());

	}

	@Action(value = "viewDnaFeedback")
	public String toViewDnaFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		dnaFeedback = dnaFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/dnaFeedbackEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DnaFeedbackService getDnaFeedbackService() {
		return dnaFeedbackService;
	}

	public void setDnaFeedbackService(DnaFeedbackService dnaFeedbackService) {
		this.dnaFeedbackService = dnaFeedbackService;
	}

	public FeedbackDna getDnaFeedback() {
		return dnaFeedback;
	}

	public void setDnaFeedback(FeedbackDna dnaFeedback) {
		this.dnaFeedback = dnaFeedback;
	}


}
