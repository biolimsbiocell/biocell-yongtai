
package com.biolims.project.feedback.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackSequencing;
import com.biolims.project.feedback.service.SequencingFeedbackService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/project/feedback/sequencingFeedback")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SequencingFeedbackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private SequencingFeedbackService sequencingFeedbackService;
	private FeedbackSequencing sequencingFeedback = new FeedbackSequencing();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showSequencingFeedbackList")
	public String showSequencingFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/sequencingFeedback.jsp");
	}

	@Action(value = "showSequencingFeedbackListJson")
	public void showSequencingFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingFeedbackService.findSequencingFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackSequencing> list = (List<FeedbackSequencing>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("fcCode", "");
		map.put("laneCode", "");
		map.put("machineCode", "");
		map.put("poolingCode", "");
		map.put("sampleCode", "");
		map.put("name", "");
		map.put("code", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("method", "");
		map.put("patientName", "");
		map.put("inspectDate", "yyyy-MM-dd");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("orderId", "");
		map.put("phone", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("productId", "");
		map.put("productName", "");
		map.put("note", "");
		map.put("state", "");
		map.put("molarity", "");
		map.put("quantity", "");
		map.put("concentration", "");
		map.put("computerDate", "yyyy-MM-dd");
		map.put("pfPercent", "");
		map.put("sequencing-name", "");
		map.put("sequencing-id", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "sequencingFeedbackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSequencingFeedbackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/feedback/sequencingFeedbackDialog.jsp");
	}

	@Action(value = "showDialogSequencingFeedbackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSequencingFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingFeedbackService.findSequencingFeedbackList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackSequencing> list = (List<FeedbackSequencing>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("mixVolume", "");
		map.put("fcCode", "");
		map.put("laneCode", "");
		map.put("machineNum", "");
		map.put("sequencingDate", "yyyy-MM-dd");
		map.put("density", "");
		map.put("pf", "");
		map.put("note", "");

		map.put("isgood", "");
		map.put("nextflow", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editSequencingFeedback")
	public String editSequencingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sequencingFeedback = sequencingFeedbackService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sequencingFeedback");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			sequencingFeedback.setCreateUser(user);
//			sequencingFeedback.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/feedback/sequencingFeedbackEdit.jsp");
	}

	@Action(value = "copySequencingFeedback")
	public String copySequencingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sequencingFeedback = sequencingFeedbackService.get(id);
		sequencingFeedback.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/feedback/sequencingFeedbackEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = sequencingFeedback.getId();
		if(id!=null&&id.equals("")){
			sequencingFeedback.setId(null);
		}
		Map aMap = new HashMap();
		sequencingFeedbackService.save(sequencingFeedback,aMap);
		return redirect("/project/feedback/sequencingFeedback/editSequencingFeedback.action?id=" + sequencingFeedback.getId());

	}

	@Action(value = "viewSequencingFeedback")
	public String toViewSequencingFeedback() throws Exception {
		String id = getParameterFromRequest("id");
		sequencingFeedback = sequencingFeedbackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/feedback/sequencingFeedbackEdit.jsp");
	}
	

	/**
	 * 保存上机
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveSequencingFeedback", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSequencingFeedback() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			SequencingTaskInfo a = new SequencingTaskInfo();
			sequencingFeedbackService.saveSequencingFeedback(a, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SequencingFeedbackService getSequencingFeedbackService() {
		return sequencingFeedbackService;
	}

	public void setSequencingFeedbackService(SequencingFeedbackService sequencingFeedbackService) {
		this.sequencingFeedbackService = sequencingFeedbackService;
	}

	public FeedbackSequencing getSequencingFeedback() {
		return sequencingFeedback;
	}

	public void setSequencingFeedback(FeedbackSequencing sequencingFeedback) {
		this.sequencingFeedback = sequencingFeedback;
	}


}
