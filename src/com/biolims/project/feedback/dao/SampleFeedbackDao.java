package com.biolims.project.feedback.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.project.feedback.model.FeedbackSample;
import com.biolims.sample.model.SampleBloodDiseaseTemp;

import com.biolims.sample.model.SampleChromosomeTemp;
import com.biolims.sample.model.SampleFolicAcidTemp;
import com.biolims.sample.model.SampleGeneTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleTumorTemp;
import com.biolims.sample.model.SampleVisitTemp;

@Repository
@SuppressWarnings("unchecked")
public class SampleFeedbackDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FeedbackSample  where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state = '1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FeedbackSample> list = new ArrayList<FeedbackSample>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 根据Code查询样本异常
	public FeedbackSample findFeedbaceById(String id) {
		String hql = "from FeedbackSample t where 1=1 and t.id='" + id + "'";
		FeedbackSample feedbackSample = (FeedbackSample) this.getSession()
				.createQuery(hql).uniqueResult();
		return feedbackSample;
	}

	public SampleInfo findSampleInfoById(String code) {
		String hql = "from SampleInfo t where 1=1 and t.code ='" + code + "'";
		SampleInfo sampleInfo = (SampleInfo) this.getSession().createQuery(hql)
				.uniqueResult();
		return sampleInfo;
	}

	public FeedbackSample findFeedBackSampleByCode(String id) throws Exception {
		String hql = "from FeedbackSample t where 1=1 and t.id='" + id + "'";
		FeedbackSample feedbackSample = (FeedbackSample) this.getSession()
				.createQuery(hql).uniqueResult();
		return feedbackSample;
	}

	public List<SampleInputTemp> findSampleInputTempById(String code) {
		String hql = "from SampleInputTemp t where 1=1 and t.code ='" + code
				+ "' and t.nextStepFlow='0'";
		List<SampleInputTemp> sampleInputTemp = this.getSession()
				.createQuery(hql).list();
		return sampleInputTemp;
	}

	/**
	 * 样本异常总数
	 * 
	 * @return
	 */
	public Long selectCount() {
		String hql = "from FeedbackSample t where 1=1 and state ='1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 样本处理异常总数
	 * 
	 * @return
	 */
	public Long selectCount1() {
		String hql = "from FeedbackPlasma t where 1=1 and state ='1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * DNA异常总数
	 * 
	 * @return
	 */
	public Long selectCount2() {
		String hql = "from FeedbackDna t where 1=1 and state = '1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 文库异常总数
	 * 
	 * @return
	 */
	public Long selectCount3() {
		String hql = "from FeedbackWk t where 1=1 and t.state='1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * pooling异常总数
	 * 
	 * @return
	 */
	public Long selectCount4() {
		String hql = "from FeedbackPooling t where 1=1 and (method is null or method = '3')";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 2100质控异常总数
	 * 
	 * @return
	 */
	public Long selectCount5() {
		String hql = "from FeedbackQuality t where 1=1 and t.state = '1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * QPCR质控异常总数
	 * 
	 * @return
	 */
	public Long selectCount11() {
		String hql = "from FeedbackQpcrQuality t where 1=1 and t.state = '1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 上机异常总数
	 * 
	 * @return
	 */
	public Long selectCount6() {
		String hql = "from FeedbackSequencing t where 1=1 and t.nextFlow = '3' and t.method is null ";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 下机异常总数
	 * 
	 * @return
	 */
	public Long selectCount7() {
		String hql = "from FeedbackDeSequencing t where 1=1 and (method is null or method='3')";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 分析异常总数
	 * 
	 * @return
	 */
	public Long selectCount8() {
		String hql = "from FeedbackAnalysis t where 1=1 and state='63' and nextflow is null and (isExecute ='0' or isExecute is null)";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 解读异常总数
	 * 
	 * @return
	 */
	public Long selectCount9() {
		String hql = "from FeedbackInterpret t where 1=1 and (method is null or method='3')";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 核酸检测异常总数
	 * 
	 * @return
	 */
	public Long selectCount12() {
		String hql = "from TechCheckServiceException t where 1=1 and t.state = '1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 文库检测异常总数
	 * 
	 * @return
	 */
	public Long selectCount13() {
		String hql = "from TechCheckServiceWkException t where 1=1 and t.state = '1'";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	// 根据样本编号和状态为暂停的数据
	public List<SampleInputTemp> findByCodeAndState(String code)
			throws Exception {
		String hql = "from SampleInputTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号和状态为暂停的数据
	public List<SampleInputTemp> findByCodeAndNextStepFlow(String code)
			throws Exception {
		String hql = "from SampleInputTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleBloodDiseaseTemp> sampleBloodDiseaseTempByCodeAndState(
			String code) throws Exception {
		String hql = "from SampleBloodDiseaseTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleBloodDiseaseTemp> sampleBloodDiseaseTempByCodeAndNextStepFlow(
			String code) throws Exception {
		String hql = "from SampleBloodDiseaseTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleChromosomeTemp> sampleChromosomeTempByCodeAndState(
			String code) throws Exception {
		String hql = "from SampleChromosomeTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleChromosomeTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleChromosomeTemp> sampleChromosomeTempByCodeAndNextStepFlow(
			String code) throws Exception {
		String hql = "from SampleChromosomeTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleChromosomeTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}



	public List<SampleTumorTemp> sampleTumorTempByCodeAndState(String code)
			throws Exception {
		String hql = "from SampleTumorTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleTumorTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleTumorTemp> sampleTumorTempByCodeAndNextStepFlow(
			String code) throws Exception {
		String hql = "from SampleTumorTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleTumorTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleFolicAcidTemp> sampleFolicAcidTempByCodeAndState(
			String code) throws Exception {
		String hql = "from SampleFolicAcidTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleFolicAcidTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleFolicAcidTemp> sampleFolicAcidTempByCodeAndNextStepFlow(
			String code) throws Exception {
		String hql = "from SampleFolicAcidTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleFolicAcidTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleGeneTemp> sampleGeneTempByCodeAndState(String code)
			throws Exception {
		String hql = "from SampleGeneTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleGeneTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleGeneTemp> sampleGeneTempByCodeAndNextStepFlow(String code)
			throws Exception {
		String hql = "from SampleGeneTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleGeneTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleVisitTemp> sampleVisitTempByCodeAndState(String code)
			throws Exception {
		String hql = "from SampleVisitTemp t where t.code='" + code
				+ "' and t.state='57'";
		List<SampleVisitTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleVisitTemp> sampleVisitTempByCodeAndNextStepFlow(
			String code) throws Exception {
		String hql = "from SampleVisitTemp t where t.code='" + code
				+ "' and t.nextStepFlow is not null";
		List<SampleVisitTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询审核时保存的数据
	public SampleInputTemp findAuditByCodeAndState(String code)
			throws Exception {
		String hql = "from SampleInputTemp t where t.code='" + code
				+ "' and t.nextStepFlow ='0'";
		SampleInputTemp sampleInputTemp = (SampleInputTemp) this.getSession()
				.createQuery(hql).uniqueResult();
		return sampleInputTemp;
	}

	// 根据样本编号查所有录入，审核以后的数据，改变其状态
	public List<SampleInputTemp> findListForCode(String code) throws Exception {
		String hql = "from SampleInputTemp t where t.code='" + code + "'";
		List<SampleInputTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleTumorTemp> findListForCodeZl(String code)
			throws Exception {
		String hql = "from SampleTumorTemp t where t.code='" + code + "'";
		List<SampleTumorTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleBloodDiseaseTemp> findListForCodeXyb(String code)
			throws Exception {
		String hql = "from SampleBloodDiseaseTemp t where t.code='" + code
				+ "'";
		List<SampleBloodDiseaseTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleChromosomeTemp> findListForCodeRst(String code)
			throws Exception {
		String hql = "from SampleChromosomeTemp t where t.code='" + code + "'";
		List<SampleChromosomeTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	

	public List<SampleFolicAcidTemp> findListForCodeYs(String code)
			throws Exception {
		String hql = "from SampleFolicAcidTemp t where t.code='" + code + "'";
		List<SampleFolicAcidTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleGeneTemp> findListForCodeDjy(String code)
			throws Exception {
		String hql = "from SampleGeneTemp t where t.code='" + code + "'";
		List<SampleGeneTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleVisitTemp> findListForCodeJytb(String code)
			throws Exception {
		String hql = "from SampleVisitTemp t where t.code='" + code + "'";
		List<SampleVisitTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

}