package com.biolims.project.feedback.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.project.feedback.model.FeedbackPlasma;
import com.biolims.sample.model.SampleInfo;

@Repository
@SuppressWarnings("unchecked")
public class PlasmaFeedbackDao extends BaseHibernateDao {
	public Map<String, Object> selectPlasmaFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FeedbackPlasma where 1=1 and state='1'";
		// if (mapForQuery.size() > 0) {
		// key = map2where(mapForQuery);
		// }
		// else{
		// key=" and state='1'";
		// }
		// and (method is null or submit is null or submit='0')
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FeedbackPlasma> list = new ArrayList<FeedbackPlasma>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectPlasmaFeedbackList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FeedbackPlasma where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and (state='1' or state='2')";
		}
		// and (isRun = '0' or isRun is null or method is null)
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FeedbackPlasma> list = new ArrayList<FeedbackPlasma>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 根据样本编号查询子表信息
	public List<SampleInfo> setSampleInfoBySampleCode(String code) {
		String hql = "from SampleInfo t where 1=1 and t.code='" + code + "'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}