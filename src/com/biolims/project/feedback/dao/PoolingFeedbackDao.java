package com.biolims.project.feedback.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.project.feedback.model.FeedbackPooling;

@Repository
@SuppressWarnings("unchecked")
public class PoolingFeedbackDao extends BaseHibernateDao {
	public Map<String, Object> selectPoolingFeedbackList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FeedbackPooling where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key=" and method is null or method = '3' ";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FeedbackPooling> list = new ArrayList<FeedbackPooling>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
}