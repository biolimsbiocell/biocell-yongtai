package com.biolims.project.feedback.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.experiment.sequencing.model.SequencingTask;
/**   
 * @Title: Model
 * @Description: 上机异常反馈
 * @author lims-platform
 * @date 2015-12-05 16:25:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FEEDBACK_SEQUENCING")
@SuppressWarnings("serial")
public class FeedbackSequencing extends EntityDao<FeedbackSequencing> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/*FC号*/
	private String fcCode;
	/*LANE号*/
	private String laneCode;
	/**POOLING号*/
	private String poolingCode;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	/*机器号*/
	private String machineCode;
	/*摩尔浓度*/
	private String molarity;
	/*上机定量*/
	private String quantity;
	/**体积*/
	private String volume;
	/*总密度*/
	private String concentration;
	/*上机日期*/
	private Date computerDate;
	/*PF%*/
	private String pfPercent;
	/**单位*/
	private String unit;
	/**结果*/
	private String result;
	/**下一步流向*/
	private String nextFlow;
	/**处理意见*/
	private String method;
	/*确认执行*/
	private String isExecute;
	/*患者姓名*/
	private String patientName;
	/*检测项目*/
	private String productId;
	/*检测项目*/
	private String productName;
	/*取样日期*/
	private String inspectDate;
	/*接收日期*/
	private Date acceptDate;
	/*身份证*/
	private String idCard;
	/*手机号*/
	private String phone;
	/*任务单*/
	private String orderId;
	/*检测方法*/
	private String sequenceFun;
	/*应出报告日期*/
	private Date reportDate;
	/**备注*/
	private String note;
	/*状态*/
	private String state;
	/**相关主表*/
	private SequencingTask sequencing;
	/*数据需求量*/
	private String dataDemand;

	//区分临床还是科技服务      0   临床   1   科技服务
	private String classify;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	@Column(name ="FC_CODE", length = 50)
	public String getFcCode() {
		return fcCode;
	}
	public void setFcCode(String fcCode) {
		this.fcCode = fcCode;
	}
	@Column(name ="LANE_CODE", length = 50)
	public String getLaneCode() {
		return laneCode;
	}
	public void setLaneCode(String laneCode) {
		this.laneCode = laneCode;
	}
	@Column(name ="POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return poolingCode;
	}
	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}
	@Column(name ="CODE", length = 50)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	@Column(name ="MACHINE_CODE", length = 50)
	public String getMachineCode() {
		return machineCode;
	}
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	@Column(name ="MOLARITY", length = 50)
	public String getMolarity() {
		return molarity;
	}
	public void setMolarity(String molarity) {
		this.molarity = molarity;
	}
	@Column(name ="QUANTITY", length = 50)
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	/**
	 *方法: 取得String
	 *@return: String  体积
	 */
	@Column(name ="VOLUME", length = 50)
	public String getVolume(){
		return this.volume;
	}
	/**
	 *方法: 设置String
	 *@param: String  体积
	 */
	public void setVolume(String volume){
		this.volume = volume;
	}
	@Column(name ="CONCENTRATION", length = 50)
	public String getConcentration() {
		return concentration;
	}
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	@Column(name ="COMPUTER_DATE", length = 50)
	public Date getComputerDate() {
		return computerDate;
	}
	public void setComputerDate(Date computerDate) {
		this.computerDate = computerDate;
	}
	@Column(name ="PF_PERCENT", length = 50)
	public String getPfPercent() {
		return pfPercent;
	}
	public void setPfPercent(String pfPercent) {
		this.pfPercent = pfPercent;
	}
	/**
	 *方法: 取得String
	 *@return: String  单位
	 */
	@Column(name ="UNIT", length = 50)
	public String getUnit(){
		return this.unit;
	}
	/**
	 *方法: 设置String
	 *@param: String  单位
	 */
	public void setUnit(String unit){
		this.unit = unit;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setResult(String result){
		this.result = result;
	}
	@Column(name ="NEXT_FLOW", length = 20)
	public String getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	@Column(name ="METHOD", length = 20)
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	@Column(name ="IS_EXECUTE", length = 20)
	public String getIsExecute() {
		return isExecute;
	}
	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}
	@Column(name ="PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	@Column(name = "PRODUCT_ID", length = 200)
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@Column(name ="INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	@Column(name ="ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	@Column(name ="ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	@Column(name ="PHONE", length = 20)
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Column(name ="ORDER_ID", length = 20)
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	@Column(name ="SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	@Column(name ="REPORT_DATE", length = 50)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@Column(name ="STATE", length = 20)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**
	 *方法: 取得Sequencing
	 *@return: Sequencing  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SEQUENCING")
	public SequencingTask getSequencing(){
		return this.sequencing;
	}
	/**
	 *方法: 设置Sequencing
	 *@param: Sequencing  相关主表
	 */
	public void setSequencing(SequencingTask sequencing){
		this.sequencing = sequencing;
	}
	public String getDataDemand() {
		return dataDemand;
	}
	public void setDataDemand(String dataDemand) {
		this.dataDemand = dataDemand;
	}
	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
	
}