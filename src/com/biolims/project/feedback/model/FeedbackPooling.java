package com.biolims.project.feedback.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: Pooling异常反馈
 * @author lims-platform
 * @date 2015-12-05 16:26:05
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FEEDBACK_POOLING")
@SuppressWarnings("serial")
public class FeedbackPooling extends EntityDao<FeedbackPooling> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	/**文库类型*/
	private String libraryType;
	/**混合量*/
	private Integer mixAmount;
	/**混合体积*/
	private String mixVolume;
	/**理论QPCR浓度*/
	private String qpcrConcentration;
	/**备注*/
	private String note;
	/**体积*/
	private String volume;
	/**浓度*/
	private String concentrer;
	/**结果*/
	private String result;
	/**下一步流向*/
	private String nextFlow;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**处理意见*/
	private String method;
	/**Pooling编号*/
	private String poolingCode;
	/*患者姓名*/
	private String patientName;
	/*检测项目*/
	private String productId;
	/*检测项目*/
	private String productName;
	/*取样日期*/
	private String inspectDate;
	/*接收日期*/
	private Date acceptDate;
	/*身份证*/
	private String idCard;
	/*检测方法*/
	private String sequenceFun;
	/*应出报告日期*/
	private Date reportDate;
	/**手机号*/
	private String phone;
	//任务单
	private String orderId;
	
	/**测序读长*/
	private String sequencingReadLong;
	/**测序类型*/
	private String sequencingType;
	/**测序平台*/
	private String sequencingPlatform;
	/**总数据量*/
	private Integer totalAmount;
	/**总体积*/
	private Double totalVolume;
	/**其他*/
	private String others;
	/**实验组*/
	private UserGroup acceptUser;

	//区分临床还是科技服务      0   临床   1   科技服务
	private String classify;
	public String getLibraryType() {
		return libraryType;
	}
	public void setLibraryType(String libraryType) {
		this.libraryType = libraryType;
	}
	public Integer getMixAmount() {
		return mixAmount;
	}
	public void setMixAmount(Integer mixAmount) {
		this.mixAmount = mixAmount;
	}
	public String getMixVolume() {
		return mixVolume;
	}
	public void setMixVolume(String mixVolume) {
		this.mixVolume = mixVolume;
	}
	public String getQpcrConcentration() {
		return qpcrConcentration;
	}
	public void setQpcrConcentration(String qpcrConcentration) {
		this.qpcrConcentration = qpcrConcentration;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  pooling编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  pooling编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 100)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  浓度
	 */
	@Column(name ="CONCENTRER", length = 50)
	public String getConcentrer(){
		return this.concentrer;
	}
	/**
	 *方法: 设置String
	 *@param: String  浓度
	 */
	public void setConcentrer(String concentrer){
		this.concentrer = concentrer;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	public String getPoolingCode() {
		return poolingCode;
	}
	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getSequenceFun() {
		return sequenceFun;
	}
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSequencingReadLong() {
		return sequencingReadLong;
	}
	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}
	public String getSequencingType() {
		return sequencingType;
	}
	public void setSequencingType(String sequencingType) {
		this.sequencingType = sequencingType;
	}
	public String getSequencingPlatform() {
		return sequencingPlatform;
	}
	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}
	public Integer getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getTotalVolume() {
		return totalVolume;
	}
	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}
	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}
	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
}