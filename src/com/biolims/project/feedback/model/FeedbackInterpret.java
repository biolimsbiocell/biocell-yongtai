package com.biolims.project.feedback.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 解读异常反馈
 * @author lims-platform
 * @date 2015-12-05 16:25:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FEEDBACK_INTERPRET")
@SuppressWarnings("serial")
public class FeedbackInterpret extends EntityDao<FeedbackInterpret> implements java.io.Serializable {
	/**文库编号*/
	private String id;
	/**名称*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	
	/**index*/
	private String indexs;
	/**Result1*/
	private String result1;
	/**Result2*/
	private String result2;
	/**CNV*/
	private String CNV;
	/**解读结果*/
	private String interpretedResult;
	/**备注*/
	private String note;


	
	public String getResult1() {
		return result1;
	}
	public void setResult1(String result1) {
		this.result1 = result1;
	}
	public String getResult2() {
		return result2;
	}
	public void setResult2(String result2) {
		this.result2 = result2;
	}
	public String getCNV() {
		return CNV;
	}
	public void setCNV(String cNV) {
		CNV = cNV;
	}
	public String getInterpretedResult() {
		return interpretedResult;
	}
	public void setInterpretedResult(String interpretedResult) {
		this.interpretedResult = interpretedResult;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	/**是否合格*/
	private String isgood;
	/**下一步流向*/
	private String nextflow;
	/**处理意见*/
	private String advice;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**处理结果*/
	private String method;
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 100)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  index
	 */
	@Column(name ="INDEXS", length = 100)
	public String getIndexs(){
		return this.indexs;
	}
	/**
	 *方法: 设置String
	 *@param: String  index
	 */
	public void setIndexs(String indexs){
		this.indexs = indexs;
	}
	
	
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="ISGOOD", length = 50)
	public String getIsgood(){
		return this.isgood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsgood(String isgood){
		this.isgood = isgood;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXTFLOW", length = 50)
	public String getNextflow(){
		return this.nextflow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextflow(String nextflow){
		this.nextflow = nextflow;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理意见
	 */
	@Column(name ="ADVICE", length = 50)
	public String getAdvice(){
		return this.advice;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理意见
	 */
	public void setAdvice(String advice){
		this.advice = advice;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}