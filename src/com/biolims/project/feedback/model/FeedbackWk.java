package com.biolims.project.feedback.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 文库异常反馈
 * @author lims-platform
 * @date 2015-12-05 16:25:55
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FEEDBACK_WK")
@SuppressWarnings("serial")
public class FeedbackWk extends EntityDao<FeedbackWk> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	private String name;
	/** 原始样本编号 */
	private String sampleCode;
	/** 文库编号 */
	private String wkCode;
	/** 样本编号 */
	private String code;
	/** 体积 */
	private Double volume;
	/** index */
	private String indexs;
	/** 浓度 */
	private Double concentration;
	/** 质控类型 */
	private String qctype;
	/** 处理结果 */
	private String result;
	/** 下一步流向 */
	private String nextflow;
	/** 处理意见 */
	private String method;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 关联任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/** 备注 */
	private String note;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 是否执行 */
	private String isRun;
	/** 是否提交 */
	private String submit;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;

	/** 科技服务任务单 */
	private String techTaskId;
	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	/** 批次号 */
	private String batch;
	// 样本类型
	private String sampleType;

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 100)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "VOLUME", length = 20)
	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库编号
	 */
	@Column(name = "WK_CODE", length = 100)
	public String getWkCode() {
		return this.wkCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库编号
	 */
	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index
	 */
	@Column(name = "INDEXS", length = 100)
	public String getIndexs() {
		return this.indexs;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index
	 */
	public void setIndexs(String indexs) {
		this.indexs = indexs;
	}

	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 指控类型
	 */
	@Column(name = "QCTYPE", length = 50)
	public String getQctype() {
		return this.qctype;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 指控类型
	 */
	public void setQctype(String qctype) {
		this.qctype = qctype;
	}

	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXTFLOW", length = 50)
	public String getNextflow() {
		return this.nextflow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextflow(String nextflow) {
		this.nextflow = nextflow;
	}

	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "ORDER_ID", length = 20)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getIsRun() {
		return isRun;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public void setIsRun(String isRun) {
		this.isRun = isRun;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

}