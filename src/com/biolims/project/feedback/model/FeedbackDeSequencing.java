package com.biolims.project.feedback.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 下机异常反馈
 * @author lims-platform
 * @date 2015-12-05 16:25:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FEEDBACK_DESEQUENCING")
@SuppressWarnings("serial")
public class FeedbackDeSequencing extends EntityDao<FeedbackDeSequencing> implements java.io.Serializable {
	/**文库编号*/
	private String id;
	/**名称*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	
	/**index*/
	private String indexs;
	/**pooling编号*/
	private String code;
	/**文库号*/
	private String wkNum;
	/**测试评估*/
	private String estimate;
	/**反馈时间*/
	private Date backDate;
	/**错误率*/
	private String errorRate;
	/**是否合格*/
	private String isgood;
	/**下一步流向*/
	private String nextflow;
	/**处理意见*/
	private String advice;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**处理结果*/
	private String method;
	//是否提交
	private String submit;
	/**结果*/
	private String result;
	
	//是否执行
	private String isRun;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getWkNum() {
		return wkNum;
	}
	public void setWkNum(String wkNum) {
		this.wkNum = wkNum;
	}
	public String getEstimate() {
		return estimate;
	}
	public void setEstimate(String estimate) {
		this.estimate = estimate;
	}
	@Column(name ="BACK_DATE", length = 50)
	public Date getBackDate() {
		return backDate;
	}
	public void setBackDate(Date backDate) {
		this.backDate = backDate;
	}
	public String getErrorRate() {
		return errorRate;
	}
	public void setErrorRate(String errorRate) {
		this.errorRate = errorRate;
	}
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 100)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  index
	 */
	@Column(name ="INDEXS", length = 100)
	public String getIndexs(){
		return this.indexs;
	}
	/**
	 *方法: 设置String
	 *@param: String  index
	 */
	public void setIndexs(String indexs){
		this.indexs = indexs;
	}
	
	
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="ISGOOD", length = 50)
	public String getIsgood(){
		return this.isgood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsgood(String isgood){
		this.isgood = isgood;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXTFLOW", length = 50)
	public String getNextflow(){
		return this.nextflow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextflow(String nextflow){
		this.nextflow = nextflow;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理意见
	 */
	@Column(name ="ADVICE", length = 50)
	public String getAdvice(){
		return this.advice;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理意见
	 */
	public void setAdvice(String advice){
		this.advice = advice;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getIsRun() {
		return isRun;
	}
	public void setIsRun(String isRun) {
		this.isRun = isRun;
	}
}