package com.biolims.project.feedback.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.WkFeedbackDao;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.sample.service.SampleInputService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WkFeedbackService {
	@Resource
	private WkFeedbackDao wkFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	// 项目组
	public Map<String, Object> findWkFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkFeedbackDao.selectWkFeedbackList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// 异常
	public Map<String, Object> findWkFeedbackList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkFeedbackDao.selectWkFeedbackList1(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackWk i) throws Exception {

		wkFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackWk get(String id) {
		FeedbackWk wkFeedback = commonDAO.get(FeedbackWk.class, id);
		return wkFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackWk sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wkFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkFeedback(String itemDataJson) throws Exception {
		List<FeedbackWk> saveItems = new ArrayList<FeedbackWk>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackWk sbti = new FeedbackWk();
			// 将map信息读入实体类
			sbti = (FeedbackWk) wkFeedbackDao.Map2Bean(map, sbti);
			if (sbti.getId() != null && sbti.getId().equals(""))
				sbti.setId(null);
			saveItems.add(sbti);
			if (sbti != null && sbti.getSubmit().equals("1")) {
				// 根据处理意见，把数据反馈到文库异常界面
				sbti.setState("2");
//				WkAbnormalBack wka = new WkAbnormalBack();
//				sampleInputService.copy(wka, sbti);
			}
		}
//		wkAbnormalDao.saveOrUpdateAll(saveItems);
	}
}
