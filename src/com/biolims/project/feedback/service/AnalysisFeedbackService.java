package com.biolims.project.feedback.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.AnalysisFeedbackDao;
import com.biolims.project.feedback.model.FeedbackAnalysis;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AnalysisFeedbackService {
	@Resource
	private AnalysisFeedbackDao analysisFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAnalysisFeedbackList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return analysisFeedbackDao.selectAnalysisFeedbackList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackAnalysis i) throws Exception {

		analysisFeedbackDao.saveOrUpdate(i);

	}
	public FeedbackAnalysis get(String id) {
		FeedbackAnalysis analysisFeedback = commonDAO.get(FeedbackAnalysis.class, id);
		return analysisFeedback;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackAnalysis sc, Map jsonMap) throws Exception {
		if (sc != null) {
			analysisFeedbackDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	
	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAnalysisFeedback(FeedbackAnalysis taskId, String itemDataJson) throws Exception {
		List<FeedbackAnalysis> saveItems = new ArrayList<FeedbackAnalysis>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			FeedbackAnalysis sbti = new FeedbackAnalysis();
			// 将map信息读入实体类
			sbti = (FeedbackAnalysis) analysisFeedbackDao.Map2Bean(map, sbti);
			analysisFeedbackDao.saveOrUpdate(sbti);
			String method=sbti.getMethod();
			if(method!=null){
//				if(!method.equals("3")){
//					AnalysisAbnormal eda = new AnalysisAbnormal();
//					eda.setCode(sbti.getCode());
//					eda.setSampleCode(sbti.getSampleCode());
//					eda.setNextFlow(sbti.getNextflow());
//					eda.setMethod(sbti.getIsgood());
//					eda.setReason(sbti.getAdvice());
//					eda.setMethod(sbti.getMethod());
//					Date date=new Date();
//					DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					String stime=format.format(date);
//					eda.setFeedbackTime(stime);
//					analysisFeedbackDao.saveOrUpdate(eda);
//				}
			}
		}

	}
}
