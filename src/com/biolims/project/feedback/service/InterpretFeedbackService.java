package com.biolims.project.feedback.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.interpret.interpretation.model.InterpretationInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.InterpretFeedbackDao;
import com.biolims.project.feedback.model.FeedbackInterpret;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class InterpretFeedbackService {
	@Resource
	private InterpretFeedbackDao interpretFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findInterpretFeedbackList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return interpretFeedbackDao.selectInterpretFeedbackList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackInterpret i) throws Exception {

		interpretFeedbackDao.saveOrUpdate(i);

	}
	public FeedbackInterpret get(String id) {
		FeedbackInterpret interpretFeedback = commonDAO.get(FeedbackInterpret.class, id);
		return interpretFeedback;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackInterpret sc, Map jsonMap) throws Exception {
		if (sc != null) {
			interpretFeedbackDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	
	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInterpretFeedback(FeedbackInterpret taskId, String itemDataJson) throws Exception {
		List<FeedbackInterpret> saveItems = new ArrayList<FeedbackInterpret>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			FeedbackInterpret sbti = new FeedbackInterpret();
			// 将map信息读入实体类
			sbti = (FeedbackInterpret) interpretFeedbackDao.Map2Bean(map, sbti);
			interpretFeedbackDao.saveOrUpdate(sbti);
			String method = sbti.getMethod();
			if(method !=null){
				if(!method.equals("3")){
					InterpretationInfo eda = new InterpretationInfo();
					eda.setSampleCode(sbti.getSampleCode());
					eda.setMethod(sbti.getMethod());
					interpretFeedbackDao.saveOrUpdate(eda);
					
					
				}
			}
		}

	}
}
