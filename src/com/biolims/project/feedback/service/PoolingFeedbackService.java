package com.biolims.project.feedback.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.pooling.model.PoolingAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.PoolingFeedbackDao;
import com.biolims.project.feedback.model.FeedbackPooling;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PoolingFeedbackService {
	@Resource
	private PoolingFeedbackDao poolingFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private PoolingDao poolingDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findPoolingFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return poolingFeedbackDao.selectPoolingFeedbackList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackPooling i) throws Exception {

		poolingFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackPooling get(String id) {
		FeedbackPooling poolingFeedback = commonDAO.get(FeedbackPooling.class,
				id);
		return poolingFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackPooling sc, Map jsonMap) throws Exception {
		if (sc != null) {
			poolingFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存
	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void savePoolingFeedback(FeedbackPooling taskId, String itemDataJson)
//			throws Exception {
//		List<FeedbackPooling> saveItems = new ArrayList<FeedbackPooling>();
//		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map map : list) {
//			FeedbackPooling sbti = new FeedbackPooling();
//			// 将map信息读入实体类
//			sbti = (FeedbackPooling) poolingFeedbackDao.Map2Bean(map, sbti);
//			poolingFeedbackDao.saveOrUpdate(sbti);
//			PoolingTask pt =poolingDao.get(PoolingTask.class, sbti.getPoolingCode());
//			String method = sbti.getMethod();
//			if (method != null && sbti != null) {//回到pooling异常页面
//				PoolingAbnormal pa = new PoolingAbnormal();
//				pa.setName(sbti.getName());
//				pa.setMethod(sbti.getMethod());
//				pa.setCode(sbti.getPoolingCode());
//				pa.setQpcrConcentrtion(sbti.getQpcrConcentration());
//				pa.setNextFlow(sbti.getNextFlow());
//				pa.setMixAmount(sbti.getMixAmount());
//				pa.setMixVolume(sbti.getMixVolume());
//				pa.setNote(sbti.getNote());
//				pa.setResult(sbti.getResult());
//				pa.setAcceptDate(sbti.getAcceptDate());
//				pa.setIdCard(sbti.getIdCard());
//				pa.setInspectDate(sbti.getInspectDate());
//				pa.setLibraryType(sbti.getLibraryType());
//				pa.setNextFlow(sbti.getNextFlow());
//				pa.setPatient(sbti.getPatientName());
//				pa.setPhone(sbti.getPhone());
//				pa.setProductId(sbti.getProductId());
//				pa.setProductName(sbti.getProductName());
//				pa.setQpcrConcentrtion(sbti.getQpcrConcentration());
//				pa.setReportDate(sbti.getReportDate());
//				pa.setSequencingFun(sbti.getSequenceFun());
//				pa.setOrderId(sbti.getOrderId());
//				
//				pa.setOthers(pt.getOthers());
//				pa.setSequencingPlatform(pt.getSequencingPlatform());
//				pa.setSequencingReadLong(pt.getSequencingReadLong());
//				pa.setSequencingType(pt.getSequencingType());
//				pa.setTotalAmount(pt.getTotalAmount());
//				pa.setTotalVolume(pt.getTotalVolume());
//				this.poolingFeedbackDao.saveOrUpdate(pa);
//			}
//		}
//	}

}
