package com.biolims.project.feedback.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.sequencing.dao.SequencingTaskDao;
import com.biolims.experiment.sequencing.model.SequencingTaskAbnormal;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.SequencingFeedbackDao;
import com.biolims.project.feedback.model.FeedbackSequencing;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SequencingFeedbackService {
	@Resource
	private SequencingFeedbackDao sequencingFeedbackDao;
	@Resource
	private SequencingTaskDao sequencingAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSequencingFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sequencingFeedbackDao.selectSequencingFeedbackList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackSequencing i) throws Exception {

		sequencingFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackSequencing get(String id) {
		FeedbackSequencing sequencingFeedback = commonDAO.get(
				FeedbackSequencing.class, id);
		return sequencingFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackSequencing sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sequencingFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSequencingFeedback(SequencingTaskInfo taskId,
			String itemDataJson) throws Exception {
		List<FeedbackSequencing> saveItems = new ArrayList<FeedbackSequencing>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			FeedbackSequencing sbti = new FeedbackSequencing();
			// 将map信息读入实体类
			sbti = (FeedbackSequencing) sequencingFeedbackDao.Map2Bean(map,
					sbti);
			sequencingFeedbackDao.saveOrUpdate(sbti);
			String method = sbti.getMethod();
			// 将修改意见传到上机异常表
			if (method != null && sbti != null) {
				if(method.equals("0")){//處理意見：重测序
					SequencingTaskAbnormal sa = new SequencingTaskAbnormal();
//					sa.setAcceptDate(sbti.getAcceptDate());
					sa.setCode(sbti.getCode());
//					sa.setFcCode(sbti.getFcCode());
//					sa.setIdCard(sbti.getIdCard());
//					sa.setInspectDate(sbti.getInspectDate());
					
//					sa.setLaneCode(sbti.getLaneCode());
//					sa.setMachineNum(sbti.getMachineCode());
//					sa.setName(sbti.getName());
					sa.setNextFlow(sbti.getNextFlow());
					sa.setNote(sbti.getNote());
					
					sa.setOrderId(sbti.getOrderId());
//					sa.setPatientName(sbti.getPatientName());
//					sa.setPf(sbti.getPfPercent());
//					sa.setPhone(sbti.getPhone());
//					sa.setPoolingCode(sbti.getPoolingCode());
					
					sa.setProductId(sbti.getProductId());
					sa.setProductName(sbti.getProductName());
					sa.setReportDate(sbti.getReportDate());
					sa.setResult(sbti.getResult());
					sa.setSampleCode(sbti.getSampleCode());
					
//					sa.setSequenceFun(sbti.getSequenceFun());
					sbti.setState("3");
					this.sequencingFeedbackDao.saveOrUpdate(sa);
				}else if(method.equals("1")){//處理意見：退費
					sbti.setState("3");
				}else if(method.equals("2")){//處理意見：合格
					sbti.setResult("1");
					sbti.setState("3");
					this.sequencingFeedbackDao.saveOrUpdate(sbti);
				}else if(method.equals("3")){//處理意見：待反饋
					sbti.setState("3");
				}
			}
		}
	}
			
}
