package com.biolims.project.feedback.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.qc.dao.WKExceptionDao;
import com.biolims.experiment.qc.model.Qc2100Abnormal;
import com.biolims.experiment.qc.model.QcQpcrAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.QualityFeedbackDao;
import com.biolims.project.feedback.model.FeedbackQpcrQuality;
import com.biolims.project.feedback.model.FeedbackQuality;
import com.biolims.sample.service.SampleInputService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QualityFeedbackService {
	@Resource
	private QualityFeedbackDao qualityFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WKExceptionDao wKExceptionDao;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQualityFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityFeedbackDao.selectQualityFeedbackList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findQualityFeedbackList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityFeedbackDao.selectQualityFeedbackList1(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findQpcrFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityFeedbackDao.selectQpcrFeedbackList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findQpcrFeedbackList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityFeedbackDao.selectQpcrFeedbackList1(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackQuality i) throws Exception {

		qualityFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackQuality get(String id) {
		FeedbackQuality qualityFeedback = commonDAO.get(FeedbackQuality.class,
				id);
		return qualityFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackQuality sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qualityFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQualityFeedback(String itemDataJson) throws Exception {
		List<FeedbackQuality> saveItems = new ArrayList<FeedbackQuality>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackQuality sbti = new FeedbackQuality();
			// 将map信息读入实体类
			sbti = (FeedbackQuality) qualityFeedbackDao.Map2Bean(map, sbti);
			if (sbti.getId() != null && sbti.getId().equals(""))
				sbti.setId(null);
			saveItems.add(sbti);
			if (sbti != null && sbti.getSubmit().equals("1")) {
				// 将修改后的结果，反馈到质控异常界面
				sbti.setState("2");
				Qc2100Abnormal qc = new Qc2100Abnormal();

				sampleInputService.copy(qc, sbti);
				qc.setState("1");
				wKExceptionDao.saveOrUpdate(qc);
				// QcAbnormal qca = new QcAbnormal();
				// qca.setCode(sbti.getCode());
				// qca.setSampleCode(sbti.getSampleCode());
				// qca.setWkCode(sbti.getWkCode());
				// qca.setIndexa(sbti.getIndexa());
				// qca.setName(sbti.getName());
				// qca.setWkType(sbti.getWkType());
				// qca.setLength(sbti.getLength());
				// qca.setQualityConcentrer(sbti.getQualityConcentrer());
				// qca.setReason(sbti.getReason());
				// qca.setNextFlow(sbti.getNextFlow());
				// qca.setResult(sbti.getResult());
				// qca.setMethod(sbti.getMethod());
				// qca.setNote(sbti.getNote());
				// qca.setPatientName(sbti.getPatientName());
				// qca.setProductId(sbti.getProductId());
				// qca.setProductName(sbti.getProductName());
				// qca.setInspectDate(sbti.getInspectDate());
				// qca.setAcceptDate(sbti.getAcceptDate());
				// qca.setIdCard(sbti.getIdCard());
				// qca.setPhone(sbti.getPhone());
				// qca.setOrderId(sbti.getOrderId());
				// qca.setReportDate(sbti.getReportDate());
				// wKExceptionDao.saveOrUpdate(qca);
			}
		}
		qualityFeedbackDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrFeedback(String itemDataJson) throws Exception {
		List<FeedbackQpcrQuality> saveItems = new ArrayList<FeedbackQpcrQuality>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackQpcrQuality sbti = new FeedbackQpcrQuality();
			// 将map信息读入实体类
			sbti = (FeedbackQpcrQuality) qualityFeedbackDao.Map2Bean(map, sbti);
			if (sbti.getId() != null && sbti.getId().equals(""))
				sbti.setId(null);
			saveItems.add(sbti);
			// qualityFeedbackDao.saveOrUpdate(sbti);
			if (sbti != null && sbti.getSubmit().equals("1")) {
				// 将修改后的结果，反馈到质控异常界面
				sbti.setState("2");
				QcQpcrAbnormal qqa = new QcQpcrAbnormal();

				sampleInputService.copy(qqa, sbti);
				qqa.setState("1");
				qualityFeedbackDao.saveOrUpdate(qqa);
			}
		}
		qualityFeedbackDao.saveOrUpdateAll(saveItems);
	}
}
