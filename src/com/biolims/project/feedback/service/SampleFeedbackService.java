package com.biolims.project.feedback.service;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.dao.CodingRuleDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.SampleFeedbackDao;
import com.biolims.project.feedback.model.FeedbackSample;
import com.biolims.sample.dao.SampleInputTechnologyDao;
import com.biolims.sample.dao.SampleInputTempDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleBackByCompany;
import com.biolims.sample.model.SampleBloodDisease;
import com.biolims.sample.model.SampleBloodDiseaseTemp;
import com.biolims.sample.model.SampleChromosome;
import com.biolims.sample.model.SampleChromosomeTemp;
import com.biolims.sample.model.SampleFolicAcid;
import com.biolims.sample.model.SampleFolicAcidTemp;
import com.biolims.sample.model.SampleGene;
import com.biolims.sample.model.SampleGeneTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleInputTechnology;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleTumor;
import com.biolims.sample.model.SampleTumorTemp;
import com.biolims.sample.model.SampleVisit;
import com.biolims.sample.model.SampleVisitTemp;
import com.biolims.sample.service.SampleInputService;
import com.biolims.system.product.model.Product;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleFeedbackService {
	@Resource
	private SampleFeedbackDao sampleFeedbackDao;
	@Resource
	private SampleInputTempDao sampleInputTempDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private SampleInputTechnologyDao sampleInputTechnologyDao;
	@Resource
	private CodingRuleDao codingRuleDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private CommonDAO commonDAO;
	private SampleInfo sampleInfo = new SampleInfo();
	private SampleAbnormal sampleAbnormal = new SampleAbnormal();
	private FeedbackSample feedbackSample = new FeedbackSample();
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleFeedbackDao.selectSampleFeedbackList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public SampleInfo findSampleInfo(String id) {
		SampleInfo findSampleInfoById = sampleFeedbackDao
				.findSampleInfoById(id);
		return findSampleInfoById;
	}

	public SampleInputTemp findSampleInputTemp(String id) {
		List<SampleInputTemp> findSampleInputTempById = sampleFeedbackDao
				.findSampleInputTempById(id);
		if (findSampleInputTempById.size() > 0)
			return findSampleInputTempById.get(0);
		return null;
	}

	// 查询是否有状态为暂停的数据
	public List<SampleInputTemp> findByCodeAndState(String code)
			throws Exception {
		List<SampleInputTemp> list = sampleFeedbackDao.findByCodeAndState(code);
		return list;
	}

	public List<SampleInputTemp> findByCodeAndNextStepFlow(String code)
			throws Exception {
		List<SampleInputTemp> list = sampleFeedbackDao
				.findByCodeAndNextStepFlow(code);
		return list;
	}

	public List<SampleBloodDiseaseTemp> sampleBloodDiseaseTempByCodeAndState(
			String code) throws Exception {
		List<SampleBloodDiseaseTemp> list = sampleFeedbackDao
				.sampleBloodDiseaseTempByCodeAndState(code);
		return list;
	}

	public List<SampleBloodDiseaseTemp> sampleBloodDiseaseTempByCodeAndNextStepFlow(
			String code) throws Exception {
		List<SampleBloodDiseaseTemp> list = sampleFeedbackDao
				.sampleBloodDiseaseTempByCodeAndNextStepFlow(code);
		return list;
	}

	public List<SampleChromosomeTemp> sampleChromosomeTempByCodeAndState(
			String code) throws Exception {
		List<SampleChromosomeTemp> list = sampleFeedbackDao
				.sampleChromosomeTempByCodeAndState(code);
		return list;
	}

	public List<SampleChromosomeTemp> sampleChromosomeTempByCodeAndNextStepFlow(
			String code) throws Exception {
		List<SampleChromosomeTemp> list = sampleFeedbackDao
				.sampleChromosomeTempByCodeAndNextStepFlow(code);
		return list;
	}



	public List<SampleTumorTemp> sampleTumorTempByCodeAndState(String code)
			throws Exception {
		List<SampleTumorTemp> list = sampleFeedbackDao
				.sampleTumorTempByCodeAndState(code);
		return list;
	}

	public List<SampleTumorTemp> sampleTumorTempByCodeAndNextStepFlow(
			String code) throws Exception {
		List<SampleTumorTemp> list = sampleFeedbackDao
				.sampleTumorTempByCodeAndNextStepFlow(code);
		return list;
	}

	public List<SampleFolicAcidTemp> sampleFolicAcidTempByCodeAndState(
			String code) throws Exception {
		List<SampleFolicAcidTemp> list = sampleFeedbackDao
				.sampleFolicAcidTempByCodeAndState(code);
		return list;
	}

	public List<SampleFolicAcidTemp> sampleFolicAcidTempByCodeAndNextStepFlow(
			String code) throws Exception {
		List<SampleFolicAcidTemp> list = sampleFeedbackDao
				.sampleFolicAcidTempByCodeAndNextStepFlow(code);
		return list;
	}

	public List<SampleGeneTemp> sampleGeneTempByCodeAndState(String code)
			throws Exception {
		List<SampleGeneTemp> list = sampleFeedbackDao
				.sampleGeneTempByCodeAndState(code);
		return list;
	}

	public List<SampleGeneTemp> sampleGeneTempByCodeAndNextStepFlow(String code)
			throws Exception {
		List<SampleGeneTemp> list = sampleFeedbackDao
				.sampleGeneTempByCodeAndNextStepFlow(code);
		return list;
	}

	public List<SampleVisitTemp> sampleVisitTempByCodeAndState(String code)
			throws Exception {
		List<SampleVisitTemp> list = sampleFeedbackDao
				.sampleVisitTempByCodeAndState(code);
		return list;
	}

	public List<SampleVisitTemp> sampleVisitTempByCodeAndNextStepFlow(
			String code) throws Exception {
		List<SampleVisitTemp> list = sampleFeedbackDao
				.sampleVisitTempByCodeAndNextStepFlow(code);
		return list;
	}

	// 查询审核数据
	public SampleInputTemp findAuditByCodeAndState(String code)
			throws Exception {
		SampleInputTemp sampleInputTemp = sampleFeedbackDao
				.findAuditByCodeAndState(code);
		return sampleInputTemp;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackSample i) throws Exception {

		sampleFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackSample get(String id) {
		FeedbackSample sampleFeedback = commonDAO.get(FeedbackSample.class, id);
		return sampleFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTech(SampleInputTechnology sc, Map jsonMap, String code)
			throws Exception {
		// 合格以后样本去到入库
		// SampleInItemTemp sampleInItemTemp = new SampleInItemTemp();
		// sampleInItemTemp.setSampleInputTechnology(sc);
		// sampleInItemTemp.setState("1");//标示改为1
		// sampleInItemTemp.setSampleCode(sc.getSampleCode());
		// sampleInputTechnologyDao.saveOrUpdate(sampleInItemTemp);
		// 把SampleInfo的状态改变
		FeedbackSample info = sampleFeedbackDao.findFeedBackSampleByCode(code);
		info.setNextflow("");
		sampleInputTechnologyDao.saveOrUpdate(info);
	}

	/**
	 * 血液病
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveXyb(SampleBloodDiseaseTemp sc, Map jsonMap)
			throws Exception {
		String sampleInputTempCode = sc.getCode();
		String code = "";
		if (sampleInputTempCode == null) {
			code = sc.getSampleInfo().getCode();
		} else {
			code = sc.getCode();
		}
		sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
		sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
		// 保存完以后new出一个smpleAbnormal对象
		sampleAbnormal.setSampleCode(code);
		sampleAbnormal.setPatientName(sc.getPatientName());
		DicType name = sc.getSampleType();
		if (name != null) {
			// sampleAbnormal.setSampleType(sc.getSampleType().getName());
		}
		if (sc.getNextStepFlow().equals("0")) {
			SampleBloodDisease sampleBloodDisease = new SampleBloodDisease();
			sc.setCode(code);
			copy(sampleBloodDisease, sc);
			sampleBloodDisease.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sampleBloodDisease
					.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			sampleInfo = sc.getSampleInfo();
			sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
			sampleInfo.setPhone(sc.getPhone()); // 手机号
			sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
			sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
			sampleInfo.setHospital(sc.getHospital()); // 送检医院
			sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
			sampleInfo.setConfirmUser1(sc.getCreateUser()); // 审核人
			// 病人姓名
			String patientName = sc.getPatientName();
			if (patientName != null) {
				sampleInfo.setPatientName(patientName);
			}
			// 取样时间
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date sendDate = sc.getSendDate();
			if (sendDate != null) {
				String date = formatter.format(sendDate);
				sampleInfo.setSampleTime(date);
			}
			// 接收日期
			Date acceptDate = sc.getAcceptDate();
			if (acceptDate != null) {
				String data2 = formatter.format(sc.getAcceptDate());
				sampleInfo.setReceiveDate(acceptDate);
			}
			feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
			feedbackSample.setNextflow(""); // 下一步流向改为空
			// 查出开箱检验数据判断如果大于或等于1就把他的是否录入完成改为1
			publicService(code);
			sampleFeedbackDao.saveOrUpdate(sc);
			List<SampleBloodDiseaseTemp> list = sampleFeedbackDao
					.findListForCodeXyb(code);
			if (list.size() > 0) {
				SampleBloodDiseaseTemp s0 = list.get(0);
				SampleBloodDiseaseTemp s1 = list.get(1);
				s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			}
		} else if (sc.getNextStepFlow().equals("1")) {// 退费
			// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
			sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
			sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
			feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
			feedbackSample
					.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
			// 把sampleInfo的信息传到公司反馈中
			setSampleBackByCompany(sampleInfo);
		} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
			// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
			sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
			sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
			SampleBackByCompany sbc = new SampleBackByCompany();
			// 把sampleInfo的信息传到公司反馈中
			if (sampleInfo.getOrderId() != null) {
				sbc.setOrderId(sampleInfo.getOrderId().getId());
			} else {
				sbc.setOrderId("");
			}
			sbc.setSampleInfo(sampleInfo);
			sbc.setSampleCode(sampleInfo.getCode());
			// sbc.setAcceptDate(sampleInfo.getReceiveDate());
			sbc.setIdCard(sampleInfo.getIdCard());
			sbc.setInspectDate(sampleInfo.getInspectDate());
			sbc.setPatientName(sampleInfo.getPatientName());
			sbc.setPhone(sampleInfo.getPhone());
			sbc.setProductId(sampleInfo.getProductId());
			sbc.setProductName(sampleInfo.getProductName());
			sbc.setReportDate(sampleInfo.getReportDate());
			sbc.setReturnDate(new Date());
			sbc.setSequenceFun(sampleInfo.getSequenceFun());
			sbc.setMethod("2");
			sbc.setState("1");
			commonDAO.saveOrUpdate(sbc);
		}
		// else if (sc.getNextStepFlow().equals("3")) {// 暂停
		// //点进去获取不到code手动的给到
		// sc.setCode(sampleInfo.getCode());
		// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
		// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
		// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
		// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
		// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
		// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
		// sampleInputTempDao.save(sc);
		// }
	}

	/**
	 * 项目管理基因突变的保存
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveJytb(SampleVisitTemp sc, Map jsonMap) throws Exception {
		if (sc != null) {
			String sampleInputTempCode = sc.getCode();
			String code = "";
			if (sampleInputTempCode == null) {
				code = sc.getSampleInfo().getCode();
			} else {
				code = sc.getCode();
			}
			sampleInfo = sampleFeedbackDao.findSampleInfoById(code);
			sc.setSampleInfo(sampleInfo);
			sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			// 保存完以后new出一个smpleAbnormal对象
			sampleAbnormal.setSampleCode(code);
			sampleAbnormal.setPatientName(sc.getPatientName());
			DicType name = sc.getSampleType();
			if (name != null) {
				// sampleAbnormal.setSampleType(sc.getSampleType().getName());
			}
			if (sc.getNextStepFlow().equals("0")) {
				SampleVisit sampleVisit = new SampleVisit();
				sc.setCode(code);
				copy(sampleVisit, sc);
				sampleVisit.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleVisit
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo = sc.getSampleInfo();
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
				sampleInfo.setPhone(sc.getPhoneNum()); // 手机号
				sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
				sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
				sampleInfo.setHospital(sc.getHospital()); // 送检医院
				sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
				sampleInfo.setConfirmUser1(sc.getCreateUser()); // 审核人
				// 病人姓名
				String patientName = sc.getPatientName();
				if (patientName != null) {
					sampleInfo.setPatientName(patientName);
				}
				// 取样时间
				Date sendDate = sc.getSendDate();
				if (sendDate != null) {
					String date = formatter.format(sendDate);
					sampleInfo.setSampleTime(date);
				}
				// 接收日期
				Date acceptDate = sc.getAcceptDate();
				if (acceptDate != null) {
					String data2 = formatter.format(sc.getAcceptDate());
					sampleInfo.setReceiveDate(acceptDate);
				}
				feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
				feedbackSample.setNextflow(""); // 下一步流向改为空
				publicService(code);
				sampleFeedbackDao.saveOrUpdate(sc);
				List<SampleVisitTemp> list = sampleFeedbackDao
						.findListForCodeJytb(code);
				if (list.size() > 0) {
					SampleVisitTemp s0 = list.get(0);
					SampleVisitTemp s1 = list.get(1);
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				}
			} else if (sc.getNextStepFlow().equals("1")) {// 退费
				// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				feedbackSample
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				// 把sampleInfo的信息传到公司反馈中
				setSampleBackByCompany(sampleInfo);
			} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
				// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
				sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
				SampleBackByCompany sbc = new SampleBackByCompany();
				// 把sampleInfo的信息传到公司反馈中
				if (sampleInfo.getOrderId() != null) {
					sbc.setOrderId(sampleInfo.getOrderId().getId());
				} else {
					sbc.setOrderId("");
				}
				sbc.setSampleInfo(sampleInfo);
				sbc.setSampleCode(sampleInfo.getCode());
				// sbc.setAcceptDate(sampleInfo.getReceiveDate());
				sbc.setIdCard(sampleInfo.getIdCard());
				sbc.setInspectDate(sampleInfo.getInspectDate());
				sbc.setPatientName(sampleInfo.getPatientName());
				sbc.setPhone(sampleInfo.getPhone());
				sbc.setProductId(sampleInfo.getProductId());
				sbc.setProductName(sampleInfo.getProductName());
				sbc.setReportDate(sampleInfo.getReportDate());
				sbc.setReturnDate(new Date());
				sbc.setSequenceFun(sampleInfo.getSequenceFun());
				sbc.setMethod("2");
				sbc.setState("1");
				commonDAO.saveOrUpdate(sbc);
			}
			// else if (sc.getNextStepFlow().equals("3")) {// 暂停
			// //点进去获取不到code手动的给到
			// sc.setCode(sampleInfo.getCode());
			// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
			// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
			// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// sampleInputTempDao.save(sc);
			// }
		}
	}

	/**
	 * 项目管理单基因的保存
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDjy(SampleGeneTemp sc, Map jsonMap) throws Exception {
		if (sc != null) {
			String sampleInputTempCode = sc.getCode();
			String code = "";
			if (sampleInputTempCode == null) {
				code = sc.getSampleInfo().getCode();
			} else {
				code = sc.getCode();
			}
			sampleInfo = sampleFeedbackDao.findSampleInfoById(code);
			sc.setSampleInfo(sampleInfo);
			sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			// 保存完以后new出一个smpleAbnormal对象
			sampleAbnormal.setSampleCode(code);
			sampleAbnormal.setPatientName(sc.getPatientName());
			DicType name = sc.getSampleType();
			if (name != null) {
				// sampleAbnormal.setSampleType(sc.getSampleType().getName());
			}
			if (sc.getNextStepFlow().equals("0")) {
				SampleGene sampleGene = new SampleGene();
				sc.setCode(code);
				copy(sampleGene, sc);
				sampleGene.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleGene
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo = sc.getSampleInfo();
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
				sampleInfo.setPhone(sc.getPhone()); // 手机号
				sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
				sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
				sampleInfo.setHospital(sc.getHospital()); // 送检医院
				sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
				sampleInfo.setConfirmUser1(sc.getCreateUser()); // 审核人
				// 病人姓名
				String patientName = sc.getPatientName();
				if (patientName != null) {
					sampleInfo.setPatientName(patientName);
				}
				// 取样时间
				Date sendDate = sc.getSendDate();
				if (sendDate != null) {
					String date = formatter.format(sendDate);
					sampleInfo.setSampleTime(date);
				}
				// 接收日期
				Date acceptDate = sc.getAcceptDate();
				if (acceptDate != null) {
					String data2 = formatter.format(sc.getAcceptDate());
					sampleInfo.setReceiveDate(acceptDate);
				}
				feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
				feedbackSample.setNextflow(""); // 下一步流向改为空
				publicService(code);
				sampleFeedbackDao.saveOrUpdate(sc);
				List<SampleGeneTemp> list = sampleFeedbackDao
						.findListForCodeDjy(code);
				if (list.size() > 0) {
					SampleGeneTemp s0 = list.get(0);
					SampleGeneTemp s1 = list.get(1);
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				}
			} else if (sc.getNextStepFlow().equals("1")) {// 退费
				// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				feedbackSample
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				// 把sampleInfo的信息传到公司反馈中
				setSampleBackByCompany(sampleInfo);
			} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
				// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
				sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
				SampleBackByCompany sbc = new SampleBackByCompany();
				// 把sampleInfo的信息传到公司反馈中
				if (sampleInfo.getOrderId() != null) {
					sbc.setOrderId(sampleInfo.getOrderId().getId());
				} else {
					sbc.setOrderId("");
				}
				sbc.setSampleInfo(sampleInfo);
				sbc.setSampleCode(sampleInfo.getCode());
				// sbc.setAcceptDate(sampleInfo.getReceiveDate());
				sbc.setIdCard(sampleInfo.getIdCard());
				sbc.setInspectDate(sampleInfo.getInspectDate());
				sbc.setPatientName(sampleInfo.getPatientName());
				sbc.setPhone(sampleInfo.getPhone());
				sbc.setProductId(sampleInfo.getProductId());
				sbc.setProductName(sampleInfo.getProductName());
				sbc.setReportDate(sampleInfo.getReportDate());
				sbc.setReturnDate(new Date());
				sbc.setSequenceFun(sampleInfo.getSequenceFun());
				sbc.setMethod("2");
				sbc.setState("1");
				commonDAO.saveOrUpdate(sbc);
			}
			// else if (sc.getNextStepFlow().equals("3")) {// 暂停
			// //点进去获取不到code手动的给到
			// sc.setCode(sampleInfo.getCode());
			// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
			// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
			// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// sampleInputTempDao.save(sc);
			// }
		}
	}

	/**
	 * 项目管理叶酸的保存
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveYs(SampleFolicAcidTemp sc, Map jsonMap) throws Exception {
		if (sc != null) {
			String sampleInputTempCode = sc.getCode();
			String code = "";
			if (sampleInputTempCode == null) {
				code = sc.getSampleInfo().getCode();
			} else {
				code = sc.getCode();
			}
			sampleInfo = sampleFeedbackDao.findSampleInfoById(code);
			sc.setSampleInfo(sampleInfo);
			sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			// 保存完以后new出一个smpleAbnormal对象
			sampleAbnormal.setSampleCode(code);
			sampleAbnormal.setPatientName(sc.getPatientName());
			DicType name = sc.getSampleType();
			if (name != null) {
				// sampleAbnormal.setSampleType(sc.getSampleType().getName());
			}
			if (sc.getNextStepFlow().equals("0")) {
				SampleFolicAcid sampleFolicAcid = new SampleFolicAcid();
				sc.setCode(code);
				copy(sampleFolicAcid, sc);
				sampleFolicAcid.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleFolicAcid
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo = sc.getSampleInfo();
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
				sampleInfo.setPhone(sc.getPhoneNum()); // 手机号
				sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
				sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
				sampleInfo.setHospital(sc.getHospital()); // 送检医院
				sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
				sampleInfo.setConfirmUser1(sc.getCreateUser()); // 审核人
				// 病人姓名
				String patientName = sc.getPatientName();
				if (patientName != null) {
					sampleInfo.setPatientName(patientName);
				}
				// 取样时间
				Date sendDate = sc.getSendDate();
				if (sendDate != null) {
					String date = formatter.format(sendDate);
					sampleInfo.setSampleTime(date);
				}
				// 接收日期
				Date acceptDate = sc.getAcceptDate();
				if (acceptDate != null) {
					String data2 = formatter.format(sc.getAcceptDate());
					sampleInfo.setReceiveDate(acceptDate);
				}
				feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
				feedbackSample.setNextflow(""); // 下一步流向改为空
				publicService(code);
				sampleFeedbackDao.saveOrUpdate(sc);
				List<SampleFolicAcidTemp> list = sampleFeedbackDao
						.findListForCodeYs(code);
				if (list.size() > 0) {
					SampleFolicAcidTemp s0 = list.get(0);
					SampleFolicAcidTemp s1 = list.get(1);
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				}
			} else if (sc.getNextStepFlow().equals("1")) {// 退费
				// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				feedbackSample
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				// 把sampleInfo的信息传到公司反馈中
				setSampleBackByCompany(sampleInfo);
			} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
				// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
				sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
				SampleBackByCompany sbc = new SampleBackByCompany();
				// 把sampleInfo的信息传到公司反馈中
				if (sampleInfo.getOrderId() != null) {
					sbc.setOrderId(sampleInfo.getOrderId().getId());
				} else {
					sbc.setOrderId("");
				}
				sbc.setSampleInfo(sampleInfo);
				sbc.setSampleCode(sampleInfo.getCode());
				// sbc.setAcceptDate(sampleInfo.getReceiveDate());
				sbc.setIdCard(sampleInfo.getIdCard());
				sbc.setInspectDate(sampleInfo.getInspectDate());
				sbc.setPatientName(sampleInfo.getPatientName());
				sbc.setPhone(sampleInfo.getPhone());
				sbc.setProductId(sampleInfo.getProductId());
				sbc.setProductName(sampleInfo.getProductName());
				sbc.setReportDate(sampleInfo.getReportDate());
				sbc.setReturnDate(new Date());
				sbc.setSequenceFun(sampleInfo.getSequenceFun());
				sbc.setMethod("2");
				sbc.setState("1");
				commonDAO.saveOrUpdate(sbc);
			}
			// else if (sc.getNextStepFlow().equals("3")) {// 暂停
			// //点进去获取不到code手动的给到
			// sc.setCode(sampleInfo.getCode());
			// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
			// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
			// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// sampleInputTempDao.save(sc);
			// }
		}
	}

	/**
	 * 项目管理乳腺癌的保存
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	
	/**
	 * 项目管理染色体的保存
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRst(SampleChromosomeTemp sc, Map jsonMap) throws Exception {
		if (sc != null) {
			String sampleInputTempCode = sc.getCode();
			String code = "";
			if (sampleInputTempCode == null) {
				code = sc.getSampleInfo().getCode();
			} else {
				code = sc.getCode();
			}
			sampleInfo = sampleFeedbackDao.findSampleInfoById(code);
			sc.setSampleInfo(sampleInfo);
			sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			// 保存完以后new出一个smpleAbnormal对象
			sampleAbnormal.setSampleCode(code);
			sampleAbnormal.setPatientName(sc.getPatientName());
			DicType name = sc.getSampleType();
			if (name != null) {
				// sampleAbnormal.setSampleType(sc.getSampleType().getName());
			}
			if (sc.getNextStepFlow().equals("0")) {
				SampleChromosome sampleChromosome = new SampleChromosome();
				sc.setCode(code);
				copy(sampleChromosome, sc);
				sampleChromosome
						.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleChromosome
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo = sc.getSampleInfo();
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
				sampleInfo.setPhone(sc.getPhone()); // 手机号
				sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
				sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
				sampleInfo.setHospital(sc.getHospital()); // 送检医院
				sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
				sampleInfo.setConfirmUser1(sc.getCreateUser()); // 审核人
				// 病人姓名
				String patientName = sc.getPatientName();
				if (patientName != null) {
					sampleInfo.setPatientName(patientName);
				}
				// 取样时间
				Date sendDate = sc.getSendDate();
				if (sendDate != null) {
					String date = formatter.format(sendDate);
					sampleInfo.setSampleTime(date);
				}
				// 接收日期
				Date acceptDate = sc.getAcceptDate();
				if (acceptDate != null) {
					String data2 = formatter.format(sc.getAcceptDate());
					sampleInfo.setReceiveDate(acceptDate);
				}
				feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
				feedbackSample.setNextflow(""); // 下一步流向改为空
				publicService(code);
				sampleFeedbackDao.saveOrUpdate(sc);
				List<SampleChromosomeTemp> list = sampleFeedbackDao
						.findListForCodeRst(code);
				if (list.size() > 0) {
					SampleChromosomeTemp s0 = list.get(0);
					SampleChromosomeTemp s1 = list.get(1);
					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				}
			} else if (sc.getNextStepFlow().equals("1")) {// 退费
				// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				sampleInfo
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
				feedbackSample
						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
				// 把sampleInfo的信息传到公司反馈中
				setSampleBackByCompany(sampleInfo);
			} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
				// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
				sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
				SampleBackByCompany sbc = new SampleBackByCompany();
				// 把sampleInfo的信息传到公司反馈中
				if (sampleInfo.getOrderId() != null) {
					sbc.setOrderId(sampleInfo.getOrderId().getId());
				} else {
					sbc.setOrderId("");
				}
				sbc.setSampleInfo(sampleInfo);
				sbc.setSampleCode(sampleInfo.getCode());
				// sbc.setAcceptDate(sampleInfo.getReceiveDate());
				sbc.setIdCard(sampleInfo.getIdCard());
				sbc.setInspectDate(sampleInfo.getInspectDate());
				sbc.setPatientName(sampleInfo.getPatientName());
				sbc.setPhone(sampleInfo.getPhone());
				sbc.setProductId(sampleInfo.getProductId());
				sbc.setProductName(sampleInfo.getProductName());
				sbc.setReportDate(sampleInfo.getReportDate());
				sbc.setReturnDate(new Date());
				sbc.setSequenceFun(sampleInfo.getSequenceFun());
				sbc.setMethod("2");
				sbc.setState("1");
				commonDAO.saveOrUpdate(sbc);
			}
			// else if (sc.getNextStepFlow().equals("3")) {// 暂停
			// //点进去获取不到code手动的给到
			// sc.setCode(sampleInfo.getCode());
			// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
			// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
			// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
			// sampleInputTempDao.save(sc);
			// }
		}
	}

	/**
	 * 肿瘤
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveZl(SampleTumorTemp sc, Map jsonMap) throws Exception {
		String sampleInputTempCode = sc.getCode();
		String code = "";
		if (sampleInputTempCode == null) {
			code = sc.getSampleInfo().getCode();
		} else {
			code = sc.getCode();
		}
		sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
		sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
		// 保存完以后new出一个smpleAbnormal对象
		sampleAbnormal.setSampleCode(code);
		sampleAbnormal.setPatientName(sc.getPatientName());
		DicType name = sc.getSampleType();
		if (name != null) {
			// sampleAbnormal.setSampleType(sc.getSampleType().getName());
		}
		if (sc.getNextStepFlow().equals("0")) {
			SampleTumor sampleTumor = new SampleTumor();
			sc.setCode(code);
			copy(sampleTumor, sc);
			sampleTumor.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sampleTumor
					.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			sampleInfo = sc.getSampleInfo();
			sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
			sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
			sampleInfo.setPhone(sc.getPhone()); // 手机号
			sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
			sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
			sampleInfo.setHospital(sc.getHospital()); // 送检医院
			sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
			sampleInfo.setConfirmUser1(sc.getCreateUser()); // 审核人
			// 病人姓名
			String patientName = sc.getPatientName();
			if (patientName != null) {
				sampleInfo.setPatientName(patientName);
			}
			// 取样时间
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date sendDate = sc.getSendDate();
			if (sendDate != null) {
				String date = formatter.format(sendDate);
				sampleInfo.setSampleTime(date);
			}
			// 接收日期
			Date acceptDate = sc.getAcceptDate();
			if (acceptDate != null) {
				String data2 = formatter.format(sc.getAcceptDate());
				sampleInfo.setReceiveDate(acceptDate);
			}
			feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
			feedbackSample.setNextflow(""); // 下一步流向改为空
			publicService(code);
			sampleFeedbackDao.saveOrUpdate(sc);
			List<SampleTumorTemp> list = sampleFeedbackDao
					.findListForCodeZl(code);
			if (list.size() > 0) {
				SampleTumorTemp s0 = list.get(0);
				SampleTumorTemp s1 = list.get(1);
				s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
				s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
				s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
			}
		} else if (sc.getNextStepFlow().equals("1")) {// 退费
			// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
			sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
			sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
			feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
			feedbackSample
					.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
			// 把sampleInfo的信息传到公司反馈中
			setSampleBackByCompany(sampleInfo);
		} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
			// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
			sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
			sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
			SampleBackByCompany sbc = new SampleBackByCompany();
			// 把sampleInfo的信息传到公司反馈中
			if (sampleInfo.getOrderId() != null) {
				sbc.setOrderId(sampleInfo.getOrderId().getId());
			} else {
				sbc.setOrderId("");
			}
			sbc.setSampleInfo(sampleInfo);
			sbc.setSampleCode(sampleInfo.getCode());
			// sbc.setAcceptDate(sampleInfo.getReceiveDate());
			sbc.setIdCard(sampleInfo.getIdCard());
			sbc.setInspectDate(sampleInfo.getInspectDate());
			sbc.setPatientName(sampleInfo.getPatientName());
			sbc.setPhone(sampleInfo.getPhone());
			sbc.setProductId(sampleInfo.getProductId());
			sbc.setProductName(sampleInfo.getProductName());
			sbc.setReportDate(sampleInfo.getReportDate());
			sbc.setReturnDate(new Date());
			sbc.setSequenceFun(sampleInfo.getSequenceFun());
			sbc.setMethod("2");
			sbc.setState("1");
			commonDAO.saveOrUpdate(sbc);
		}
		// else if (sc.getNextStepFlow().equals("3")) {// 暂停
		// //点进去获取不到code手动的给到
		// sc.setCode(sampleInfo.getCode());
		// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
		// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
		// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
		// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
		// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
		// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
		// sampleInputTempDao.save(sc);
		// }
	}

	/**
	 * 项目管理产前的保存
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(SampleInputTemp sc, Map jsonMap) throws Exception {
//		if (sc != null) {
//			String sampleInputTempCode = sc.getCode();
//			String code = "";
//			if (sampleInputTempCode == null) {
//				code = sc.getSampleInfo().getCode();
//			} else {
//				code = sc.getCode();
//			}
//			sampleInfo = sampleFeedbackDao.findSampleInfoById(code);
//			sc.setSampleInfo(sampleInfo);
//			sc.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
//			sc.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//			// 保存完以后new出一个smpleAbnormal对象
//			sampleAbnormal.setSampleCode(code);
//			sampleAbnormal.setPatientName(sc.getPatientName());
//			DicType name = sc.getSampleType();
//			if (name != null) {
//				// sampleAbnormal.setSampleType(sc.getSampleType().getName());
//			}
//			if (sc.getNextStepFlow().equals("0")) {
//				// 项目管理的异常合格时把值给到SampleInput中
//				SampleInput sampleInput = new SampleInput();
//				copy(sampleInput, sc);
//				sampleInput.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
//				sampleInput
//						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
//				// 把信息保存到SampleInfo中
//				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
//				sampleInfo
//						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
//				sampleInfo.setIdCard(sc.getSampleInfo().getIdCard()); // 身份证号
//				sampleInfo.setPhone(sc.getPhoneNum()); // 手机号
//				sampleInfo.setProductId(sc.getProductId()); // 检测项目ID
//				sampleInfo.setProductName(sc.getProductName()); // 检测项目名称
//				String patientName = sc.getPatientName();
//				if (patientName != null) {
//					sampleInfo.setPatientName(patientName); // 病人姓名
//				}
//				// 取样时间
//				Date sendDate = sc.getSendDate();
//				if (sendDate != null) {
//					String date = formatter.format(sendDate);
//					sampleInfo.setSampleTime(date);
//				}
//				// 接收日期
//				Date acceptDate = sc.getAcceptDate();
//				if (acceptDate != null) {
//					String data2 = formatter.format(sc.getAcceptDate());
//					sampleInfo.setReceiveDate(acceptDate);
//				}
//				sampleInfo.setHospital(sc.getHospital()); // 送检医院
//				sampleInfo.setFamilyAddress(sc.getAddress()); // 家庭地址
//				sampleInfo.setCreateUser(sc.getCreateUser()); // 录入人
//				sampleInfo.setConfirmUser1(sc.getCreateUser1()); // 审核人
//				// feedbackSample.setId(code);
//				feedbackSample = sampleFeedbackDao.findFeedbaceById(code);
//				feedbackSample.setNextflow(""); // 下一步流向改为空
//				// 查出开箱检验数据判断如果大于或等于1就把他的是否录入完成改为1
//				List<SampleReceiveItem> receiveItemByCode = sampleInputTempDao
//						.findSampleReceiveItemByCode(code);
//				if (receiveItemByCode.size() >= 1) {
//					SampleReceiveItem sampleReceiveItem = receiveItemByCode
//							.get(0);
//					sampleReceiveItem.setIsFull("1");
//				}
//				String sampleCode = sampleInfo.getCode();// 样本编号
//				SampleReceiveItem sr = sampleReceiveDao
//						.getSampleReceiveItem(sampleCode);
//				WkReceiveTemp wat = new WkReceiveTemp(); // 文库样本接收
//				QcReceiveTemp qrt = new QcReceiveTemp();// 文库质控接收
//				String mark1 = sampleCode.substring(1, 2);
//				if (sr != null) {
//					if (sampleInfo.getClassify() != null
//							&& !sampleInfo.getClassify().equals("")
//							&& sampleInfo.getClassify().equals("0")) {
//						if (sr.getState() != null && !sr.getState().equals("")
//								&& sr.getState().equals("59")) {
//							String[] productIds = sampleInfo.getProductId()
//									.split(",");
//							for (String productId : productIds) {
//								Product product = commonDAO.get(Product.class,
//										productId);
//								String productLine = "";
//								if (product.getTestMethod() != null
//										&& !product.getTestMethod().equals(""))
//									productLine = product.getTestMethod()
//											.getId();
//								if (productLine.equals("antenatal")) {
//								} else {
//								}
//
//							}
//						}
//					}
//				}
//				sampleFeedbackDao.saveOrUpdate(sc);
//				List<SampleInputTemp> list = sampleFeedbackDao
//						.findListForCode(code);
//				if (list.size() > 0) {
//					SampleInputTemp s0 = list.get(0);
//					SampleInputTemp s1 = list.get(1);
//					s0.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
//					s0.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
//					s1.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE);
//					s1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_INPUT_COMPLETE_NAME);
//				}
//			} else if (sc.getNextStepFlow().equals("1")) {// 退费
//				// 如果是下一步流向是退费，把SampleInfo的状态改成已退费
//				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
//				sampleInfo
//						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
//				feedbackSample.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
//				feedbackSample
//						.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
//				SampleBackByCompany sbc = new SampleBackByCompany();
//				// 把sampleInfo的信息传到公司反馈中
//				sbc.setSampleInfo(sampleInfo);
//				sbc.setSampleCode(sampleInfo.getCode());
//				// sbc.setAcceptDate(sampleInfo.getReceiveDate());
//				sbc.setIdCard(sampleInfo.getIdCard());
//				sbc.setInspectDate(sampleInfo.getInspectDate());
//
//				if (sampleInfo.getOrderId() != null) {
//					sbc.setOrderId(sampleInfo.getOrderId().getId());
//				} else {
//					sbc.setOrderId("");
//				}
//				sbc.setPatientName(sampleInfo.getPatientName());
//				sbc.setPhone(sampleInfo.getPhone());
//				sbc.setProductId(sampleInfo.getProductId());
//				sbc.setProductName(sampleInfo.getProductName());
//				sbc.setReportDate(sampleInfo.getReportDate());
//				sbc.setReturnDate(new Date());
//				sbc.setSequenceFun(sampleInfo.getSequenceFun());
//				sbc.setMethod("1");
//				sbc.setState("1");
//
//				commonDAO.saveOrUpdate(sbc);
//			} else if (sc.getNextStepFlow().equals("2")) {// 重抽血
//				// 如果是下一步流向是重抽血，把SampleInfo的状态改成重抽血
//				sampleInfo.setState(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN);
//				sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.BLOOD_AGAIN_NAME);
//				SampleBackByCompany sbc = new SampleBackByCompany();
//				// 把sampleInfo的信息传到公司反馈中
//				sbc.setSampleInfo(sampleInfo);
//				sbc.setSampleCode(sampleInfo.getCode());
//				// sbc.setAcceptDate(sampleInfo.getReceiveDate());
//				sbc.setIdCard(sampleInfo.getIdCard());
//				sbc.setInspectDate(sampleInfo.getInspectDate());
//
//				if (sampleInfo.getOrderId() != null) {
//					sbc.setOrderId(sampleInfo.getOrderId().getId());
//				} else {
//					sbc.setOrderId("");
//				}
//				sbc.setPatientName(sampleInfo.getPatientName());
//				sbc.setPhone(sampleInfo.getPhone());
//				sbc.setProductId(sampleInfo.getProductId());
//				sbc.setProductName(sampleInfo.getProductName());
//				sbc.setReportDate(sampleInfo.getReportDate());
//				sbc.setReturnDate(new Date());
//				sbc.setSequenceFun(sampleInfo.getSequenceFun());
//				sbc.setMethod("2");
//				sbc.setState("1");
//
//				commonDAO.saveOrUpdate(sbc);
//			}
//			// else if (sc.getNextStepFlow().equals("3")) {// 暂停
//			// //点进去获取不到code手动的给到
//			// sc.setCode(sampleInfo.getCode());
//			// sc.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
//			// sc.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//			// sc.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//			// // 如果是下一步流向是暂停，把SampleInfo的状态改成暂停并且保存
//			// sampleInfo.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//			// sampleInfo.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//			// sampleInputTempDao.save(sc);
//			// }
//		}
//	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void copy(Object input, Object inputTemp) {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		Field[] inputFields = input.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			try {
				inputMap.put(fd.getName(),
						BeanUtils.getFieldValue(inputTemp, fd.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Field[] inputTempFields = input.getClass().getDeclaredFields();
		for (Field fd : inputTempFields) {
			if (inputMap.get(fd.getName()) == null) {
				continue;
			}
			try {
				// 给属性赋值
				BeanUtils.setFieldValue(input, fd.getName(),
						inputMap.get(fd.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		sampleInputTempDao.save(input);
	}

	public void publicService(String code) throws Exception {
		List<SampleReceiveItem> receiveItemByCode = sampleInputTempDao
				.findSampleReceiveItemByCode(code);
		if (receiveItemByCode.size() >= 1) {
			SampleReceiveItem sampleReceiveItem = receiveItemByCode.get(0);
			sampleReceiveItem.setIsFull("1");
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SampleReceiveItem sr = sampleReceiveDao.getSampleReceiveItem(code);
//		WkReceiveTemp wat = new WkReceiveTemp(); // 文库样本接收
		String mark1 = code.substring(1, 2);
		if (sr != null) {
			if (sampleInfo.getClassify() != null
					&& !sampleInfo.getClassify().equals("")
					&& sampleInfo.getClassify().equals("0")) {
				if (sr.getState() != null && !sr.getState().equals("")
						&& sr.getState().equals("59")) {
					Product product = commonDAO.get(Product.class,
							sampleInfo.getProductId());
					if (product.getTestMethod() != null
							&& !product.getTestMethod().equals("")
							&& product.getTestMethod().getId()
									.equals("antenatal")) {
						if (mark1 != null && !mark1.equals("")
								&& mark1.equals("B")) {} else if (mark1 != null && !mark1.equals("")
								&& mark1.equals("P")) {
//							wat.setCode(sampleInfo.getCode());
//							wat.setSampleCode(sampleInfo.getCode());
//							wat.setProductName(sampleInfo.getProductName());
//							wat.setProductId(sampleInfo.getProductId());
//							wat.setPatientName(sampleInfo.getPatientName());
//							wat.setInspectDate(sampleInfo.getInspectDate());
//							wat.setPhone(sampleInfo.getPhone());
//							wat.setIdCard(sampleInfo.getIdCard());
//							wat.setClassify(sampleInfo.getClassify());
//							if (sampleInfo.getReportDate() != null) {
//								String date = sampleInfo.getReportDate();
//								Date d = new Date(date);
//								wat.setReportDate(d);
//							}
//							wat.setSequenceFun(sampleInfo.getSequenceFun());
//							if (sampleInfo.getOrderId() != null) {
//								wat.setOrderId(sampleInfo.getOrderId().getId());
//							} else {
//								wat.setOrderId("");
//							}
//							wat.setState("1");
//							commonDAO.saveOrUpdate(wat);
						}
					} else {
						if (mark1 != null && !mark1.equals("")
								&& mark1.equals("B")) {
//							drt.setSampleCode(sampleInfo.getCode());
//							drt.setProductName(sampleInfo.getProductName());
//							drt.setProductId(sampleInfo.getProductId());
//							drt.setPatientName(sampleInfo.getPatientName());
//							// drt.setAcceptDate(sampleInfo.getReceiveDate());
//							drt.setSequenceFun(sampleInfo.getSequenceFun());
//							drt.setInspectDate(sampleInfo.getInspectDate());
//							drt.setPhone(sampleInfo.getPhone());
//							if (sampleInfo.getOrderId() != null) {
//								drt.setOrderId(sampleInfo.getOrderId().getId());
//							} else {
//								drt.setOrderId("");
//							}
//							drt.setIdCard(sampleInfo.getIdCard());
//							drt.setClassify(sampleInfo.getClassify());
//							drt.setReportDate(sampleInfo.getReportDate());
//							drt.setState("1");
//							commonDAO.saveOrUpdate(drt);

						} else if (mark1 != null && !mark1.equals("")
								&& mark1.equals("P")) {
//							wat.setCode(sampleInfo.getCode());
//							wat.setSampleCode(sampleInfo.getCode());
//							wat.setProductName(sampleInfo.getProductName());
//							wat.setProductId(sampleInfo.getProductId());
//							wat.setPatientName(sampleInfo.getPatientName());
//							wat.setInspectDate(sampleInfo.getInspectDate());
//							wat.setPhone(sampleInfo.getPhone());
//							wat.setClassify(sampleInfo.getClassify());
//							wat.setIdCard(sampleInfo.getIdCard());
//							if (sampleInfo.getReportDate() != null) {
//								String date = sampleInfo.getReportDate();
//								Date d = new Date(date);
//								wat.setReportDate(d);
//							}
//							wat.setSequenceFun(sampleInfo.getSequenceFun());
//							if (sampleInfo.getOrderId() != null) {
//								wat.setOrderId(sampleInfo.getOrderId().getId());
//							} else {
//								wat.setOrderId("");
//							}
//							wat.setState("1");
//							commonDAO.saveOrUpdate(wat);
						}
					}
				}
			}
		}
	}

	/**
	 * 把信息给到公司反馈
	 * 
	 * @param s
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setSampleBackByCompany(SampleInfo s) {
		// 把sampleInfo的信息传到公司反馈中
		SampleBackByCompany sbc = new SampleBackByCompany();
		if (s.getOrderId() != null) {
			sbc.setOrderId(sampleInfo.getOrderId().getId());
		} else {
			sbc.setOrderId("");
		}
		sbc.setSampleInfo(s);
		sbc.setSampleCode(s.getCode());
		// sbc.setAcceptDate(s.getReceiveDate());
		sbc.setIdCard(s.getIdCard());
		sbc.setInspectDate(s.getInspectDate());
		sbc.setPatientName(s.getPatientName());
		sbc.setPhone(s.getPhone());
		sbc.setProductId(s.getProductId());
		sbc.setProductName(s.getProductName());
		sbc.setReportDate(s.getReportDate());
		sbc.setReturnDate(new Date());
		sbc.setSequenceFun(s.getSequenceFun());
		sbc.setMethod("1");
		sbc.setState("1");
		commonDAO.saveOrUpdate(sbc);
	}

	// 生成编码日期+5位
	public String genTransID(String modelName, String markCode)
			throws Exception {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		String s = format.format(date);
		String stime = s.substring(2, 4);
		String autoID = getCodeByPrefix(modelName, markCode, stime, 00000, 5,
				null);
		return autoID;
	}

	public String getCodeByPrefix(String modelName, String markCode,
			String prefix, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = markCode + prefix;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("sampleCode", "like##@@##'" + val + "%'");

		String maxId = sampleInputTechnologyDao.selectModelTotalByType(
				modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "",
				flowLength);

		return val + parseId;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleFeedback(String itemDataJson) throws Exception {
		List<FeedbackSample> saveItems = new ArrayList<FeedbackSample>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackSample sbti = new FeedbackSample();
			// 将map信息读入实体类
			sbti = (FeedbackSample) sampleFeedbackDao.Map2Bean(map, sbti);

			if (sbti != null && sbti.getSubmit() != null) {
				if (sbti.getSubmit().equals("1")) {
					sbti.setState("2");// 改变状态
					SampleAbnormal sa = new SampleAbnormal();
					sampleInputService.copy(sa, sbti);
					sa.setState("1");
					this.sampleFeedbackDao.saveOrUpdate(sa);
				}
			}
			sampleFeedbackDao.saveOrUpdate(sbti);
		}
	}

}
