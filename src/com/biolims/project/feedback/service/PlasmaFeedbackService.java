package com.biolims.project.feedback.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.PlasmaFeedbackDao;
import com.biolims.project.feedback.model.FeedbackPlasma;
import com.biolims.sample.service.SampleInputService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PlasmaFeedbackService {
	@Resource
	private PlasmaFeedbackDao plasmaFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findPlasmaFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return plasmaFeedbackDao.selectPlasmaFeedbackList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findPlasmaFeedbackList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return plasmaFeedbackDao.selectPlasmaFeedbackList1(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackPlasma i) throws Exception {

		plasmaFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackPlasma get(String id) {
		FeedbackPlasma plasmaFeedback = commonDAO.get(FeedbackPlasma.class, id);
		return plasmaFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackPlasma sc, Map jsonMap) throws Exception {
		if (sc != null) {
			plasmaFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePlasmaFeedback(String itemDataJson) throws Exception {
		List<FeedbackPlasma> saveItems = new ArrayList<FeedbackPlasma>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackPlasma sbti = new FeedbackPlasma();
			// 将map信息读入实体类
			sbti = (FeedbackPlasma) plasmaFeedbackDao.Map2Bean(map, sbti);
			if (sbti.getId() != null && sbti.getId().equals(""))
				sbti.setId(null);
			saveItems.add(sbti);
			if (sbti != null && !sbti.getSubmit().equals("")) {
				if (sbti.getSubmit().equals("1")) {
					sbti.setState("2");// 改变状态
					// 反馈时间
					Date date = new Date();
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					String stime = format.format(date);
					sbti.setBackDate(stime);

					PlasmaAbnormal pa = new PlasmaAbnormal();// 样本异常
					sampleInputService.copy(pa, sbti);
					// pa.setCode(sbti.getCode());
					// pa.setSampleCode(sbti.getSampleCode());
					// pa.setSampleType(sbti.getSampleType());
					// pa.setProductName(sbti.getProductName());
					// pa.setIdCard(sbti.getIdCard());
					// pa.setPhone(sbti.getPhone());
					// pa.setProductId(sbti.getProductId());
					// pa.setProductName(sbti.getProductName());
					// pa.setAcceptDate(sbti.getAcceptDate());
					// pa.setNote(sbti.getNote());
					// pa.setInspectDate(sbti.getInspectDate());
					// pa.setPatient(sbti.getPatientName());
					// pa.setResult(sbti.getResult());
					// pa.setVolume(sbti.getVolume());
					// pa.setOrderId(sbti.getOrderId());
					// pa.setClassify(sbti.getClassify());
					// pa.setBackTime(sbti.getBackDate());
					// pa.setState("2");
					// plasmaFeedbackDao.saveOrUpdate(pa);
				}

				// PlasmaAbnormal db=new PlasmaAbnormal();
				// db.setCode(sbti.getBloodCode());
				// db.setSampleCode(sbti.getSampleCode());
				// db.setPatient(sbti.getPatientName());
				// db.setPhone(sbti.getPhone());
				//
				// db.setProductName(sbti.getProductName());
				// db.setSequencingFun(sbti.getSequenceFun());
				// db.setIdCard(sbti.getIdCard());
				// db.setInspectDate(sbti.getInspectDate());
				// // db.setReportDate(sbti.getReportDate());
				//
				// db.setOrderId(sbti.getOrderId());
				// db.setResult(sbti.getResult());
				// db.setMethod(sbti.getMethod());
				// db.setIsExecute("1");
				// // db.setVolume(Double.parseDouble(sbti.getVolume()));
				// Date date=new Date();
				//
				// DateFormat format=new
				// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				// String stime=format.format(date);
				// db.setBackTime(stime);
				// db.setAcceptDate(sbti.getAcceptDate());
				// db.setIdCard(sbti.getIdCard());
				//
				// db.setName(sbti.getName());
				// db.setNextFlow(sbti.getNextFlow());
				// db.setPatient(sbti.getPatientName());
				// db.setPhone(sbti.getPhone());
				//
				// db.setProductId(sbti.getProductId());
				// db.setProductName(sbti.getProductName());
				// db.setSequencingFun(sbti.getSequenceFun());
				// // if(sbti.getVolume() !=null){
				// // db.setVolume(Double.parseDouble(sbti.getVolume()));
				// // }else{
				// // db.setVolume(0d);
				// // }
				// db.setOrderId(sbti.getOrderId());
				// plasmaFeedbackDao.saveOrUpdate(db);
			}
			// String method=sbti.getMethod();
			// if(method!=null){
			// String patientName="";
			// String idCard="";
			// //String state="";
			// //String stateName="";
			// List<SampleInfo>
			// sList=plasmaFeedbackDao.setSampleInfoBySampleCode(sbti.getSampleCode());
			// if(sList.size()>0){
			// for(SampleInfo si:sList){
			// patientName=si.getPatientName();
			// idCard=si.getIdCard();
			// //state=si.getState();
			// //stateName=si.getStateName();
			// }
			// }
			// if(method.equals("0")){ //重抽血,到样本中心的异常样本
			// SampleAbnormal sa=new SampleAbnormal();
			// sa.setSampleCode(sbti.getSampleCode());
			// sa.setSampleType("全血");
			// sa.setPatientName(patientName);
			// sa.setIdCard(idCard);
			// plasmaFeedbackDao.saveOrUpdate(sa);
			// }else if(method.equals("1")){ //退费,给sampleInfo中样本状态设置“退费”状态。
			// if(sList.size()>0){
			// for(SampleInfo si:sList){
			// si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY);
			// si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_RETURNMONEY_NAME);
			// }
			// }
			// }else if(method.equals("2")){ //合格,反馈到实验中异常管理
			// DNAAbnormalBack abnormal = new DNAAbnormalBack();
			// abnormal.setBackTime(new Date());
			// abnormal.setCode(sbti.getId());
			// abnormal.setSampleCode(sbti.getSampleCode());
			// abnormal.setNextFlow(sbti.getNextflow());
			// abnormal.setName(sbti.getName());
			// abnormal.setResultDecide(sbti.getIsgood());
			// abnormal.setMethod(sbti.getMethod());
			// abnormal.setHandleIdea(sbti.getAdvice());
			// plasmaFeedbackDao.saveOrUpdate(abnormal);
			// }
			// }
		}
		plasmaFeedbackDao.saveOrUpdateAll(saveItems);
	}
}
