package com.biolims.project.feedback.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.model.DeSequencingAbnormal;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.DeSequencingFeedbackDao;
import com.biolims.project.feedback.model.FeedbackDeSequencing;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DeSequencingFeedbackService {
	@Resource
	private DeSequencingFeedbackDao deSequencingFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findDeSequencingFeedbackList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return deSequencingFeedbackDao.selectDeSequencingFeedbackList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackDeSequencing i) throws Exception {

		deSequencingFeedbackDao.saveOrUpdate(i);

	}
	public FeedbackDeSequencing get(String id) {
		FeedbackDeSequencing deSequencingFeedback = commonDAO.get(FeedbackDeSequencing.class, id);
		return deSequencingFeedback;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackDeSequencing sc, Map jsonMap) throws Exception {
		if (sc != null) {
			deSequencingFeedbackDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	
	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDeSequencingFeedback(FeedbackDeSequencing taskId, String itemDataJson) throws Exception {
		List<FeedbackDeSequencing> saveItems = new ArrayList<FeedbackDeSequencing>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			FeedbackDeSequencing sbti = new FeedbackDeSequencing();
			// 将map信息读入实体类
			sbti = (FeedbackDeSequencing) deSequencingFeedbackDao.Map2Bean(map, sbti);
			deSequencingFeedbackDao.saveOrUpdate(sbti);
			String method = sbti.getMethod();
			if(method!=null && sbti.getSubmit() !=null && sbti.getSubmit().equals("1")){
				if(method.equals("2")){//处理意见：合格
					sbti.setState("2");
				}else{
					sbti.setState("3");
				}
				this.deSequencingFeedbackDao.saveOrUpdate(sbti);
			}else{
				sbti.setState("3");
			}
		}

	}
}
