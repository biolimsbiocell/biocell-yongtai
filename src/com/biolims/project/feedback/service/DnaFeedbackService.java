package com.biolims.project.feedback.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.DnaFeedbackDao;
import com.biolims.project.feedback.model.FeedbackDna;
import com.biolims.sample.service.SampleInputService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DnaFeedbackService {
	@Resource
	private DnaFeedbackDao dnaFeedbackDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDnaFeedbackList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dnaFeedbackDao.selectDnaFeedbackList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackDna i) throws Exception {

		dnaFeedbackDao.saveOrUpdate(i);

	}

	public FeedbackDna get(String id) {
		FeedbackDna dnaFeedback = commonDAO.get(FeedbackDna.class, id);
		return dnaFeedback;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FeedbackDna sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dnaFeedbackDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnaFeedback(String itemDataJson) throws Exception {
		List<FeedbackDna> saveItems = new ArrayList<FeedbackDna>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackDna sbti = new FeedbackDna();
			// 将map信息读入实体类
			sbti = (FeedbackDna) dnaFeedbackDao.Map2Bean(map, sbti);

			if (sbti != null && sbti.getIsExecute() != null) {
				if (sbti.getIsExecute().equals("1")) {
					sbti.setState("2");// 改变状态
					DnaTaskAbnormal da = new DnaTaskAbnormal();// 样本异常 状态为2在异常中显示
					sampleInputService.copy(da, sbti);
					this.dnaFeedbackDao.saveOrUpdate(da);
				}
			}
			dnaFeedbackDao.saveOrUpdate(sbti);
		}
	}
}