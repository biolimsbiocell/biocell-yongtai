package com.biolims.project.report.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.report.dao.ReportConfirmDao;
import com.biolims.project.report.model.ReportConfirm;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ReportConfirmService {
	@Resource
	private ReportConfirmDao reportConfirmDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findReportConfirmList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return reportConfirmDao.selectReportConfirmList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ReportConfirm i) throws Exception {

		reportConfirmDao.saveOrUpdate(i);

	}
	public ReportConfirm get(String id) {
		ReportConfirm reportConfirm = commonDAO.get(ReportConfirm.class, id);
		return reportConfirm;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ReportConfirm sc, Map jsonMap) throws Exception {
		if (sc != null) {
			reportConfirmDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
