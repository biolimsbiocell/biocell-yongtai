package com.biolims.project.report.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.report.dao.ReportSendDao;
import com.biolims.project.report.model.ReportSend;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ReportSendService {
	@Resource
	private ReportSendDao reportSendDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findReportSendList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return reportSendDao.selectReportSendList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ReportSend i) throws Exception {

		reportSendDao.saveOrUpdate(i);

	}
	public ReportSend get(String id) {
		ReportSend reportSend = commonDAO.get(ReportSend.class, id);
		return reportSend;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ReportSend sc, Map jsonMap) throws Exception {
		if (sc != null) {
			reportSendDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
