﻿
package com.biolims.project.report.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.report.model.ReportConfirm;
import com.biolims.project.report.service.ReportConfirmService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/project/report/reportConfirm")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ReportConfirmAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "22021";
	@Autowired
	private ReportConfirmService reportConfirmService;
	private ReportConfirm reportConfirm = new ReportConfirm();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showReportConfirmList")
	public String showReportConfirmList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/report/reportConfirm.jsp");
	}

	@Action(value = "showReportConfirmListJson")
	public void showReportConfirmListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = reportConfirmService.findReportConfirmList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ReportConfirm> list = (List<ReportConfirm>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
//		map.put("age-id", "");
		map.put("age", "");
		map.put("sex", "");
		map.put("linkMan", "");
		map.put("linkManWay", "");
		map.put("sampleType", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "reportConfirmSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogReportConfirmList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/report/reportConfirmDialog.jsp");
	}

	@Action(value = "showDialogReportConfirmListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogReportConfirmListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = reportConfirmService.findReportConfirmList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ReportConfirm> list = (List<ReportConfirm>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
//		map.put("age-id", "");
		map.put("age", "");
		map.put("sex", "");
		map.put("linkMan", "");
		map.put("linkManWay", "");
		map.put("sampleType", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editReportConfirm")
	public String editReportConfirm() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			reportConfirm = reportConfirmService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "reportConfirm");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			reportConfirm.setCreateUser(user);
//			reportConfirm.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/report/reportConfirmEdit.jsp");
	}

	@Action(value = "copyReportConfirm")
	public String copyReportConfirm() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		reportConfirm = reportConfirmService.get(id);
		reportConfirm.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/report/reportConfirmEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = reportConfirm.getId();
		if(id!=null&&id.equals("")){
			reportConfirm.setId(null);
		}
		Map aMap = new HashMap();
		reportConfirmService.save(reportConfirm,aMap);
		return redirect("/project/report/reportConfirm/editReportConfirm.action?id=" + reportConfirm.getId());

	}

	@Action(value = "viewReportConfirm")
	public String toViewReportConfirm() throws Exception {
		String id = getParameterFromRequest("id");
		reportConfirm = reportConfirmService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/report/reportConfirmEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ReportConfirmService getReportConfirmService() {
		return reportConfirmService;
	}

	public void setReportConfirmService(ReportConfirmService reportConfirmService) {
		this.reportConfirmService = reportConfirmService;
	}

	public ReportConfirm getReportConfirm() {
		return reportConfirm;
	}

	public void setReportConfirm(ReportConfirm reportConfirm) {
		this.reportConfirm = reportConfirm;
	}


}
