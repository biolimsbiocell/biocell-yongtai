﻿
package com.biolims.project.report.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.report.model.ReportSend;
import com.biolims.project.report.service.ReportSendService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/project/report/reportSend")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ReportSendAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "22022";
	@Autowired
	private ReportSendService reportSendService;
	private ReportSend reportSend = new ReportSend();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showReportSendList")
	public String showReportSendList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/report/reportSend.jsp");
	}

	@Action(value = "showReportSendListJson")
	public void showReportSendListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = reportSendService.findReportSendList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ReportSend> list = (List<ReportSend>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleId", "");
//		map.put("name-id", "");
		map.put("name", "");
		map.put("testItem", "");
		map.put("isSend", "");
		map.put("sendAddress", "");
		map.put("sender", "");
		map.put("linkWay", "");
//		map.put("expressNews-id", "");
		map.put("expressNews", "");
		map.put("sendDate", "yyyy-MM-dd");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "reportSendSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogReportSendList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/project/report/reportSendDialog.jsp");
	}

	@Action(value = "showDialogReportSendListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogReportSendListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = reportSendService.findReportSendList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ReportSend> list = (List<ReportSend>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleId", "");
//		map.put("name-id", "");
		map.put("name", "");
		map.put("testItem", "");
		map.put("isSend", "");
		map.put("sendAddress", "");
		map.put("sender", "");
		map.put("linkWay", "");
//		map.put("expressNews-id", "");
		map.put("expressNews", "");
		map.put("sendDate", "yyyy-MM-dd");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editReportSend")
	public String editReportSend() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			reportSend = reportSendService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "reportSend");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			reportSend.setCreateUser(user);
//			reportSend.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/project/report/reportSendEdit.jsp");
	}

	@Action(value = "copyReportSend")
	public String copyReportSend() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		reportSend = reportSendService.get(id);
		reportSend.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/project/report/reportSendEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = reportSend.getId();
		if(id!=null&&id.equals("")){
			reportSend.setId(null);
		}
		Map aMap = new HashMap();
		reportSendService.save(reportSend,aMap);
		return redirect("/project/report/reportSend/editReportSend.action?id=" + reportSend.getId());

	}

	@Action(value = "viewReportSend")
	public String toViewReportSend() throws Exception {
		String id = getParameterFromRequest("id");
		reportSend = reportSendService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/project/report/reportSendEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ReportSendService getReportSendService() {
		return reportSendService;
	}

	public void setReportSendService(ReportSendService reportSendService) {
		this.reportSendService = reportSendService;
	}

	public ReportSend getReportSend() {
		return reportSend;
	}

	public void setReportSend(ReportSend reportSend) {
		this.reportSend = reportSend;
	}


}
