package com.biolims.project.report.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 报告审核
 * @author lims-platform
 * @date 2015-11-12 11:07:01
 * @version V1.0   
 *
 */
@Entity
@Table(name = "REPORT_CONFIRM")
@SuppressWarnings("serial")
public class ReportConfirm extends EntityDao<ReportConfirm> implements java.io.Serializable {
	/**样本编号*/
	private String id;
	/**姓名*/
	private String name;
	/**年龄*/
	private Integer age;
	/**性别*/
	private String sex;
	/**联系人*/
	private String linkMan;
	/**联系人方式*/
	private String linkManWay;
	/**样本类型*/
	private String sampleType;
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  姓名
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  姓名
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  年龄
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "AGE")
	@Column(name="age",length=10)
	public Integer getAge(){
		return this.age;
	}
	/**
	 *方法: 设置String
	 *@param: String  年龄
	 */
	public void setAge(Integer age){
		this.age = age;
	}
	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name ="SEX", length = 50)
	public String getSex(){
		return this.sex;
	}
	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setSex(String sex){
		this.sex = sex;
	}
	/**
	 *方法: 取得String
	 *@return: String  联系人
	 */
	@Column(name ="LINK_MAN", length = 50)
	public String getLinkMan(){
		return this.linkMan;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系人
	 */
	public void setLinkMan(String linkMan){
		this.linkMan = linkMan;
	}
	/**
	 *方法: 取得String
	 *@return: String  联系人方式
	 */
	@Column(name ="LINK_MAN_WAY", length = 120)
	public String getLinkManWay(){
		return this.linkManWay;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系人方式
	 */
	public void setLinkManWay(String linkManWay){
		this.linkManWay = linkManWay;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本类型
	 */
	@Column(name ="SAMPLE_TYPE", length = 50)
	public String getSampleType(){
		return this.sampleType;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本类型
	 */
	public void setSampleType(String sampleType){
		this.sampleType = sampleType;
	}
}