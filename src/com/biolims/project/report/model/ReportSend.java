package com.biolims.project.report.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 报告发送
 * @author lims-platform
 * @date 2015-11-12 11:07:12
 * @version V1.0   
 *
 */
@Entity
@Table(name = "REPORT_SEND")
@SuppressWarnings("serial")
public class ReportSend extends EntityDao<ReportSend> implements java.io.Serializable {
	/**报告编号*/
	private String id;
	/**样本编号*/
	private String sampleId;
	/**姓名*/
	private String name;
	/**检测项目*/
	private String testItem;
	/**是否发送*/
	private String isSend;
	/**寄送地址*/
	private String sendAddress;
	/**寄送人*/
	private String sender;
	/**联系方式*/
	private String linkWay;
	/**快递信息*/
	private String expressNews;
	/**发送日期*/
	private Date sendDate;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  报告编号
	 */
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  报告编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_ID", length = 100)
	public String getSampleId(){
		return this.sampleId;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleId(String sampleId){
		this.sampleId = sampleId;
	}
	/**
	 *方法: 取得String
	 *@return: String  姓名
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "NAME")
	@Column(name="NAME")
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  姓名
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目
	 */
	@Column(name ="TEST_ITEM", length = 50)
	public String getTestItem(){
		return this.testItem;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目
	 */
	public void setTestItem(String testItem){
		this.testItem = testItem;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否发送
	 */
	@Column(name ="IS_SEND", length = 50)
	public String getIsSend(){
		return this.isSend;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否发送
	 */
	public void setIsSend(String isSend){
		this.isSend = isSend;
	}
	/**
	 *方法: 取得String
	 *@return: String  寄送地址
	 */
	@Column(name ="SEND_ADDRESS", length = 50)
	public String getSendAddress(){
		return this.sendAddress;
	}
	/**
	 *方法: 设置String
	 *@param: String  寄送地址
	 */
	public void setSendAddress(String sendAddress){
		this.sendAddress = sendAddress;
	}
	/**
	 *方法: 取得String
	 *@return: String  寄送人
	 */
	@Column(name ="SENDER", length = 50)
	public String getSender(){
		return this.sender;
	}
	/**
	 *方法: 设置String
	 *@param: String  寄送人
	 */
	public void setSender(String sender){
		this.sender = sender;
	}
	/**
	 *方法: 取得String
	 *@return: String  联系方式
	 */
	@Column(name ="LINK_WAY", length = 100)
	public String getLinkWay(){
		return this.linkWay;
	}
	/**
	 *方法: 设置String
	 *@param: String  联系方式
	 */
	public void setLinkWay(String linkWay){
		this.linkWay = linkWay;
	}
	/**
	 *方法: 取得String
	 *@return: String  快递信息
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "EXPRESS_NEWS")
	public String getExpressNews(){
		return this.expressNews;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递信息
	 */
	public void setExpressNews(String expressNews){
		this.expressNews = expressNews;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  发送日期
	 */
	@Column(name ="SEND_DATE", length = 50)
	public Date getSendDate(){
		return this.sendDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  发送日期
	 */
	public void setSendDate(Date sendDate){
		this.sendDate = sendDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 150)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
}