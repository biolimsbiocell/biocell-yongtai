package com.biolims.openapi4.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import org.apache.xmlbeans.SystemProperties;

import com.biolims.openapi4.util.PropUtil;

public class BaseService implements Serializable {

	private static final long serialVersionUID = -8280794717935626188L;

	protected String access_token;

	protected String createURL(String apiName, Map<String, String> paramMap) {

//		Properties prop = PropUtil.getProperties("/config.properties");
//		String baseURL = prop.getProperty("baseURL");
//		String from_account = prop.getProperty("from_account");
//		String app_key = prop.getProperty("app_key");
		InputStream is = Thread.currentThread().getContextClassLoader()
			    .getResourceAsStream("system.properties");
		Properties prop = new Properties();
		try {
			prop.load(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String baseURL = prop.getProperty("baseURL");
		String from_account = prop.getProperty("from_account");
		String app_key = prop.getProperty("app_key");

		StringBuffer url = new StringBuffer();
		url.append(baseURL);
		url.append(apiName);
		url.append("?from_account=" + from_account + "&");

		url.append("app_key=" + app_key + "&");
		url.append("token=" + access_token + "&");
		if (paramMap != null && paramMap.size() > 0) {
			for (String key : paramMap.keySet()) {
				String value = paramMap.get(key);
				url.append(key + "=" + value + "&");
			}

		}
		url.deleteCharAt(url.length() - 1);
		return url.toString();
	}

}
