package com.biolims.openapi4.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.biolims.openapi4.commons.TokenManager;
import com.biolims.openapi4.exception.OpenAPIException;
import com.biolims.openapi4.model.Record;
import com.biolims.openapi4.util.HttpUtil;

/**
 * 用户
 */
public class UserU8Service extends BaseService {
	
	final static Logger logger = LogManager.getLogger(UserU8Service.class);

	public UserU8Service() {
		this.access_token = TokenManager.getToKenId();
	}

	public UserU8Service(String token) {
		this.access_token = token;
	}
	
	/**
	 * 用户登陆
	 * @param userId   用户id
	 * @param password	密码
	 * @param to_account  提供方企业code
	 * @return			登陆结果
	 * @throws OpenAPIException
	 */
	public JSONObject login(String userId,String password, String to_account) throws OpenAPIException {
		JSONObject record;
		try {
			
			String jsonBody = String.format("{{\"user\":{{\"user_id\":\"%s\",\"password\":\"%s\"}}}}"
                    , userId
                    , password);
			Map<String, String> paramMap = new HashMap();
			paramMap.put("to_account", to_account);
			
			String url = this.createURL("user/login", paramMap);
			logger.debug(url);
			String resultStr = HttpUtil.post(url, jsonBody);
			
			JSONObject resultRecord = Record.parseObject(resultStr);
			record = resultRecord;
			
		} catch (Exception e) {
			throw new OpenAPIException(e.getMessage(), e);
		}
		return record;
	}

}
