package com.biolims.openapi4.service;

import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;
import com.biolims.openapi4.commons.TokenManager;
import com.biolims.openapi4.exception.OpenAPIException;
import com.biolims.openapi4.util.HttpUtil;

/**
 * Copyright(c) 2015-2015 by yonyouup. All Rights Reserved 项目信息
 * 
 * @author liujl
 * @version <类版本> , 2015年12月20日
 * @see <相关类/方法>
 * @since <产品/模块版本>
 */
public class FitemService extends BaseService{
	final static Logger logger = LogManager.getLogger(FitemService.class);
	
	public FitemService() {
		this.access_token = TokenManager.getToKenId();
	}

	public FitemService(String token) {
		this.access_token = token;
	}
	/**
	 * 批量获取项目信息
	 * @param paramMap
	 * @return
	 * @throws OpenAPIException
	 */
	public JSONObject batchGet(Map<String, String> paramMap) throws OpenAPIException {
		JSONObject record;
		try {
			String url = this.createURL("fitem/batch_get", paramMap);
			logger.debug(url);
			record = JSONObject.parseObject(HttpUtil.get(url));
		} catch (Exception e) {
			
			throw new OpenAPIException(e.getMessage(), e);
		}
		return record;
	}
}
