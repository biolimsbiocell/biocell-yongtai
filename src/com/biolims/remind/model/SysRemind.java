package com.biolims.remind.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;

/**
 * 事件提醒
 * 
 * @author 倪毅
 * 
 */
@Entity
@Table(name = "T_SYS_REMIND")
@SuppressWarnings("serial")
public class SysRemind extends EntityDao<SysRemind> {
	// 主键
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;
	// 提醒类型
	@Column(name = "TYPE", length = 32)
	private String type;
	// 事件标题
	@Column(name = "TITLE", length = 200)
	private String title;
	// 发起人（系统为SYSTEM）
	@Column(name = "REMIND_USER", length = 32)
	private String remindUser;
	// 阅读人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "USER_READ_USER_ID")
	private User readUser;
	// 发起时间
	private Date startDate;
	// 查阅提醒时间
	private Date readDate;
	// 提醒内容
	@Column(name = "CONTENT", length = 2000)
	private String content;

	// 状态 0.草稿  1.已阅 
	@Column(name = "STATE", length = 32)
	private String state;

	//提醒的用户
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "USER_HANDLE_USER_ID")
	private User handleUser;

	// 表单id
	@Column(name = "CONTENT_ID", length = 32)
	private String contentId;

	@Column(name = "APPLICATION_TYPE_TABLE_ID", length = 32)
	private String tableId;

	public SysRemind() {

	}

	public SysRemind(SysRemind remind) {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getRemindUser() {
		return remindUser;
	}

	public void setRemindUser(String remindUser) {
		this.remindUser = remindUser;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}

	public User getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(User handleUser) {
		this.handleUser = handleUser;
	}

	public User getReadUser() {
		return readUser;
	}

	public void setReadUser(User readUser) {
		this.readUser = readUser;
	}

}
