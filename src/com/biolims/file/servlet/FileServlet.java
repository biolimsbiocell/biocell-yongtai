package com.biolims.file.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.techreport.service.TechReportMainService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.OperFileService;

public class FileServlet extends HttpServlet {

	// Initialize global variables
	public void init() throws ServletException {
	}

	// Process the HTTP Get request
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {// 解析 request，判断是否有上传文件
			WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());

			OperFileService operFileService = (OperFileService) ctx.getBean("operFileService");

			TechReportMainService reportService = (TechReportMainService) ctx.getBean("techReportMainService");

			String modelType = request.getParameter("modelType");
			String useType = request.getParameter("useType");
			String id = request.getParameter("picUserId");
			String contentId = request.getParameter("contentId");
			String type = request.getParameter("type");
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (isMultipart) {

				DiskFileItemFactory factory = new DiskFileItemFactory();// 设置最多只允许在内存中存储的数据,单位:字节//
				factory.setSizeThreshold(4096);// 设置文件临时存储路径//
				factory.setRepository(new File("D:\\Temp"));// 产生一新的文件上传处理程式
				ServletFileUpload upload = new ServletFileUpload(factory);// 设置路径、文件名的字符集
				upload.setHeaderEncoding("UTF-8");// 设置允许用户上传文件大小,单位:字节
				upload.setSizeMax(-1);// upload.setSizeMax(1024 * 1024);//

				BufferedInputStream in = null;
				List fileItems = upload.parseRequest(request);// 依次处理请求
				Iterator iter = fileItems.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (!item.isFormField()) {// 如果item是正常的表单域

						String fileName = item.getName();
						fileName = java.net.URLDecoder.decode(fileName, "utf-8");
						if (fileName != null) {

							User user = (User) request.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
							FileInfo fi = new FileInfo();
							fi.setUploadUser(user);
							// String a = (String)super.getRequest().getAttribute("fileFileName");
							fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
							fi.setFileName(fileName);
							fi.setUploadTime(new Date());
							fi.setOwnerModel(modelType);
							fi.setUseType(useType);
							fi.setModelContentId(contentId);
							int extIndex = fileName.lastIndexOf(".");
							String ext = "";
							if (extIndex != -1) {
								ext = fileName.substring(extIndex + 1);
							}
							fi.setFileType(ext);

							in = new BufferedInputStream(item.getInputStream());// 获得文件输入流

							String fileRetId = operFileService.uploadFileStream(in, fi);

							reportService.saveSampleReportItemById(contentId, fileRetId);

						}
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();//

		}

	}

	// Process the HTTP Post request
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	// Clean up resources
	public void destroy() {
	}
}
