package com.biolims.file.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 文档管理
 * 
 * @author wangting
 * 
 */
@Namespace("/file/fileInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class FileInfoAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7234363909423733821L;
	// 该action权限id
	private String rightsId = "1127";
	private FileInfo fif;

	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 文档管理的遍历
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showFileInfoList")
	public String showFileInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/file/fileInfo/showFileInfoList.jsp");
	}
	

	@Action(value = "showFileInfoListJson")
	public void showFileInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String queryData = getRequest().getParameter("data");
		try {
			Map<String, String> map2Query = new HashMap<String, String>();

			if (queryData != null && queryData.length() > 0)
				map2Query = JsonUtils.toObjectByJson(queryData, Map.class);

			Map<String, Object> result = fileInfoService.findFileInfoList(
					map2Query, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FileInfo> list = (List<FileInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fileName", "");
			map.put("relatedModelType-name", "");
			map.put("modelContentId", "");
			map.put("studyDirection-name", "");
			map.put("project", "");
			map.put("projectTask", "");
			map.put("experiment", "");
			map.put("function", "");
			map.put("equipment", "");
			map.put("stateName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 主页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toMainframe")
	public String toMainframe() throws Exception {
		String reqMethodType = getRequest().getParameter("reqMethodType");
		String id = getRequest().getParameter("id");
		if (id != null) {
			FileInfo fif = fileInfoService.getFileInfoById(id);
			if (!SystemConstants.DIC_STATE_NEW.equals(fif.getState()))
				reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
		}
		getRequest().setAttribute("id", id);
		putObjToContext("handlemethod", reqMethodType);
		toToolBar(rightsId, "", "", reqMethodType);
		return dispatcher("/WEB-INF/page/file/fileInfo/mainframe.jsp");
	}

	/**
	 * 文档管理中心编辑页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editFileInfo")
	public String editFileInfo() throws Exception {
		String id = getRequest().getParameter("id");
		if (id != null && id.length() > 0) {
			fif = fileInfoService.getFileInfoById(id);
		} else {
			fif = new FileInfo();
			fif.setUploadTime(new Date());
		}
		return dispatcher("/WEB-INF/page/file/fileInfo/editFileInfo.jsp");
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		try {
			fileInfoService.save(fif);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirect("/file/fileInfo/toMainframe.action?id=" + fif.getId()
				+ "&reqMethodType=" + SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	}

	/**
	 * 加载图片
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "loadPic", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String loadPic() throws Exception {
		String id = getRequest().getParameter("id");
		String model = getRequest().getParameter("model");
		List<FileInfo> flist = this.fileInfoService.findFileInfoList(id, model);
		putObjToContext("flist", flist);
		return dispatcher("/WEB-INF/page/file/showFileImg.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FileInfo getFif() {
		return fif;
	}

	public void setFif(FileInfo fif) {
		this.fif = fif;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}

}
