package com.biolims.file.action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.OperFileService;
import com.biolims.util.DateUtil;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.ZipUtil;

@Namespace("/operfile")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings({ "unchecked", "static-access" })
public class OperFileAction extends BaseActionSupport {

	private static final long serialVersionUID = 3744274337814794059L;

	private File file;

	private String fileFileName;

	private String fileContentType;

	private FileInfo fileInfo;

	@Resource
	private OperFileService operFileService;

	/**
	 * <br/>
	 * <b>说明</b>:初始化文件数据库<br/>
	 * <b>注意事项:</b><br/>
	 * 
	 * @return
	 * @throws Exception
	 * 
	 */
	@Action(value = "initFileListOld", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String initFileList() throws Exception {
		// 用于接收通用查询的参数
		String modelType = getParameterFromRequest("modelType");
		String modelId = getParameterFromRequest("id");
		String flag = getParameterFromRequest("flag");
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("sel", new String[] { "", "string", "", "选择", "40", "true", "", "", "", "", "", "" });
		map.put("fileName", new String[] { "", "string", "", "文件名", "240", "true", "", "", "", "", "", "" });
		map.put("ownerModel", new String[] { "", "string", "", "所属模块", "120", "false", "", "", "", "", "", "" });
		map.put("uploadTime", new String[] { "", "string", "", "上传日期", "180", "false", "", "", "", "", "", "" });
		map.put("userStr", new String[] { "", "string", "", "上传人", "120", "true", "", "", "", "", "", "" });
		map.put("filePath", new String[] { "", "string", "", "图片路径", "190", "true", "true", "", "", "", "", "" });
		map.put("state", new String[] { "", "string", "", "状态", "50", "true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		super.getRequest().setAttribute("path", super.getRequest().getContextPath()
				+ "/operfile/getFileData.action?flag=" + flag + "&id=" + modelId + "&modelType=" + modelType);

		User user = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		String id = "";
		if (user != null)
			id = user.getId();
		super.getRequest().setAttribute("userId", id);
		return dispatcher("/WEB-INF/page/file/fileList.jsp");
	}

	/**
	 * <br/>
	 * <b>说明</b>:质检任务单<br/>
	 * <b>注意事项:</b><br/>
	 * 
	 * @return
	 * @throws Exception
	 * 
	 */
	@Action(value = "initFileZjList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String initFileZjList() throws Exception {
		// 用于接收通用查询的参数
		String modelType = getParameterFromRequest("modelType");
		String modelId = getParameterFromRequest("reportId");
		String flag = getParameterFromRequest("flag");
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("sel", new String[] { "", "string", "", "选择", "40", "true", "", "", "", "", "", "" });
		map.put("fileName", new String[] { "", "string", "", "文件名", "240", "true", "", "", "", "", "", "" });
		map.put("ownerModel", new String[] { "", "string", "", "所属模块", "120", "false", "", "", "", "", "", "" });
		map.put("uploadTime", new String[] { "", "string", "", "上传日期", "180", "false", "", "", "", "", "", "" });
		map.put("userStr", new String[] { "", "string", "", "上传人", "120", "true", "", "", "", "", "", "" });
		map.put("filePath", new String[] { "", "string", "", "图片路径", "190", "true", "true", "", "", "", "", "" });
		map.put("state", new String[] { "", "string", "", "状态", "50", "true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		super.getRequest().setAttribute("path", super.getRequest().getContextPath()
				+ "/operfile/getFileZjData.action?flag=" + flag + "&reportId=" + modelId + "&modelType=" + modelType);

		User user = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		String id = "";
		if (user != null)
			id = user.getId();
		super.getRequest().setAttribute("userId", id);
		return dispatcher("/WEB-INF/page/file/fileList.jsp");
	}

	/**
	 * 
	 * @Title: getFileZjData @Description: TODO(质检任务单附件查询) @param @throws
	 *         Exception    设定文件 @return void    返回类型 @author
	 *         zhiqiang.yang@biolims.cn @date 2017-11-22 下午6:22:31 @throws
	 */
	@Action(value = "getFileZjData", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getFileZjData() throws Exception {

		int start = Integer.parseInt(super.getRequest().getParameter("start"));
		int limit = Integer.parseInt(super.getRequest().getParameter("limit"));
		String modelType = getParameterFromRequest("modelType");
		// String modelId = getParameterFromRequest("id");
		// String modelId = "HSTQ1711160002";
		String modelId = getParameterFromRequest("reportId");
		String flag = getParameterFromRequest("flag");
		String dir = super.getRequest().getParameter("dir");// 排序方式
		String sort = super.getRequest().getParameter("sort");// 排序字段
		dir = dir == null || dir.trim().length() <= 0 ? "DESC" : dir;
		sort = sort == null || sort.trim().length() <= 0 ? "uploadTime" : sort;

		Map<String, Object> valueMap = this.operFileService.queryFileZjInfo(null, start, limit, dir, sort, modelType,
				modelId, flag);
		List<FileInfo> list = (List<FileInfo>) valueMap.get("result");
		Long count = (Long) valueMap.get("count");
		if (list != null) {
			for (FileInfo fi : list) {
				fi.setSel("<input type=\"checkbox\" name=\"sel\" value=\"" + fi.getId() + "\">");
				if (fi.getUploadUser() != null)
					fi.setUserStr(fi.getUploadUser().getName());
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("sel", "sel");
		map.put("fileName", "");
		map.put("ownerModel", "");
		map.put("uploadTime", "yyyy-MM-dd HH:mm:ss");
		map.put("userStr", "");
		map.put("filePath", "");
		new SendData().sendDateJson(map, list, count, super.getResponse());
	}

	@Action(value = "getFileData", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getFileData() throws Exception {

		int start = Integer.parseInt(super.getRequest().getParameter("start"));
		int limit = Integer.parseInt(super.getRequest().getParameter("limit"));
		String modelType = getParameterFromRequest("modelType");
		String modelId = getParameterFromRequest("id");
		String flag = getParameterFromRequest("flag");
		String dir = super.getRequest().getParameter("dir");// 排序方式
		String sort = super.getRequest().getParameter("sort");// 排序字段
		dir = dir == null || dir.trim().length() <= 0 ? "DESC" : dir;
		sort = sort == null || sort.trim().length() <= 0 ? "uploadTime" : sort;

		Map<String, Object> valueMap = this.operFileService.queryFileInfo(null, start, limit, dir, sort, modelType,
				modelId, flag);
		List<FileInfo> list = (List<FileInfo>) valueMap.get("result");
		Long count = (Long) valueMap.get("count");
		if (list != null) {
			for (FileInfo fi : list) {
				fi.setSel("<input type=\"checkbox\" name=\"sel\" value=\"" + fi.getId() + "\">");
				if (fi.getUploadUser() != null)
					fi.setUserStr(fi.getUploadUser().getName());
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("sel", "sel");
		map.put("fileName", "");
		map.put("ownerModel", "");
		map.put("uploadTime", "yyyy-MM-dd HH:mm:ss");
		map.put("userStr", "");
		map.put("filePath", "");
		new SendData().sendDateJson(map, list, count, super.getResponse());
	}

	/**
	 * <br/>
	 * <b>说明</b>:<br/>
	 * <b>注意事项:</b><br/>
	 * 
	 * @return
	 * @throws Exception
	 * 
	 */
	@Action(value = "upload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void upload() throws Exception {
		String modelType = super.getRequest().getParameter("modelType");
		String useType = super.getRequest().getParameter("useType");
		String id = super.getRequest().getParameter("picUserId");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		FileInfo fi = new FileInfo();
		fi.setUploadUser(user);
		// String a = (String)super.getRequest().getAttribute("fileFileName");
		fi.setFileName(this.fileFileName);
		fi.setUploadTime(new Date());
		fi.setOwnerModel(modelType);
		fi.setUseType(useType);
		fi.setModelContentId(id);
		int extIndex = this.fileFileName.lastIndexOf(".");
		String ext = "";
		if (extIndex != -1) {
			ext = this.fileFileName.substring(extIndex + 1);
		}
		fi.setFileType(ext);
		try {
			this.operFileService.uploadFile(file, fi);
			HttpUtils.write("successed");
		} catch (Exception e) {
			e.printStackTrace();
			HttpUtils.write("error");
		}
	}

	@SuppressWarnings("rawtypes")
	@Action(value = "ajaxUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void ajaxUpload() throws Exception {
		String modelType = super.getRequest().getParameter("modelType");

		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		FileInfo fi = new FileInfo();
		fi.setUploadUser(user);
		// String a = (String)super.getRequest().getAttribute("fileFileName");
		fi.setFileName(this.fileFileName);
		fi.setUploadTime(new Date());
		fi.setOwnerModel(modelType);
		fi.setUseType("1");
		int extIndex = this.fileFileName.lastIndexOf(".");
		String ext = "";
		if (extIndex != -1) {
			ext = this.fileFileName.substring(extIndex + 1);
		}
		fi.setFileType(ext);
		try {
			String fileId = this.operFileService.uploadFile(file, fi);
			Map map = new HashMap();
			map.put("fileId", fileId);
			map.put("fileName", fi.getFileName());
			map.put("uploadDate", DateUtil.format(fi.getUploadTime(), "yyyy-MM-dd HH:mm:ss"));
			HttpUtils.write(JsonUtils.toJsonString(map));
		} catch (Exception e) {
			e.printStackTrace();
			HttpUtils.write("error");
		}
	}

	@Action(value = "showImg", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showImg() throws Exception {
		String fileId = getRequest().getParameter("fileId");
		FileInfo fileInfo = this.operFileService.queryFileInfoById(fileId);
		String path = fileInfo.getFilePath();
		File file = new File(path);
		ServletOutputStream out = super.getResponse().getOutputStream();
		InputStream inStream = new FileInputStream(file);
		// 循环取出流中的数据
		byte[] b = new byte[1024];
		int len;
		while ((len = inStream.read(b)) > 0)
			out.write(b, 0, len);
		super.getResponse().setStatus(super.getResponse().SC_OK);
		super.getResponse().flushBuffer();
	}

	@Action(value = "del", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void del() throws Exception {
		String[] ids = this.getRequest().getParameterValues("ids[]");
		String delStr = getParameterFromRequest("del");
		String id = getParameterFromRequest("id");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			this.operFileService.delFileInfoByIds(ids, delStr, id, user);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 下载文件
	 * 
	 * @throws Exception
	 *             void
	 */
	@Action(value = "download", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void download() throws Exception {

		String ids = super.getRequest().getParameter("ids");

		List<FileInfo> list = this.operFileService.queryFileInfoByIds(ids);

		if (list.size() == 1) {
			FileInfo fi = list.get(0);
			String pathSaveFile = fi.getFilePath();// 要下载的文件
			String fileName = fi.getFileName();// 保存窗口中显示的文件名
			super.getResponse().reset();
			super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

			/*
			 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
			 */
			ServletOutputStream out = null;
			InputStream inStream = null;
			try {
				fileName = super.getResponse().encodeURL(new String(fileName.getBytes("GBK"), "ISO8859_1"));//

				super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				// inline
				out = super.getResponse().getOutputStream();
				inStream = new FileInputStream(pathSaveFile);

				// 循环取出流中的数据
				byte[] b = new byte[1024];
				int len;
				while ((len = inStream.read(b)) > 0)
					out.write(b, 0, len);
				super.getResponse().setStatus(super.getResponse().SC_OK);
				super.getResponse().flushBuffer();

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (out != null)
					out.close();
				if (inStream != null)
					inStream.close();
			}
		} else if (list.size() > 1) {
			this.downloadFiles(list);
		}
	}
	@Action(value = "downloadById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadById() throws Exception {

		String id = super.getRequest().getParameter("id");

		FileInfo fi = this.operFileService.queryFileInfoById(id);

		String pathSaveFile = fi.getFilePath();// 要下载的文件
		String fileName = fi.getFileName();// 保存窗口中显示的文件名
		super.getResponse().reset();
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("GBK"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();
			inStream = new FileInputStream(pathSaveFile);

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			if (inStream != null)
				inStream.close();
		}

	}

	@Action(value = "downloadByContentId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadByContentId() throws Exception {

		String id = super.getRequest().getParameter("id");

		FileInfo fi = this.operFileService.queryFileInfoByContentId(id);

		String pathSaveFile = fi.getFilePath();// 要下载的文件
		String fileName = fi.getFileName();// 保存窗口中显示的文件名
		super.getResponse().reset();
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("GBK"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();
			inStream = new FileInputStream(pathSaveFile);

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			if (inStream != null)
				inStream.close();
		}

	}

	@Action(value = "downloadByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadByName() throws Exception {

		String name = super.getRequest().getParameter("name");

		FileInfo fi = this.operFileService.queryFileInfoByName(name);

		String pathSaveFile = fi.getFilePath();// 要下载的文件
		String fileName = fi.getFileName();// 保存窗口中显示的文件名
		super.getResponse().reset();
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("GBK"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();
			inStream = new FileInputStream(pathSaveFile);

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			if (inStream != null)
				inStream.close();
		}

	}

	private void downloadFiles(List<FileInfo> list) throws Exception {

		if (list != null && list.size() > 0) {
			Map<String, String> temp = new HashMap<String, String>();
			File[] fileArray = new File[list.size()];
			Map<String, String> newName = new HashMap<String, String>();
			for (int i = 0; i < list.size(); i++) {
				FileInfo fi = list.get(i);
				String deskName = new File(fi.getFilePath()).getName();
				String fileName = fi.getFileName();
				String fn = temp.get(fileName);
				if (fn != null) {// 打包下载版本文件保证文件不同名
					String uploadDate = DateUtil.format(fi.getUploadTime(), "yyyyMMddHHmmss");
					fileName = uploadDate + fileName;
					if (i == 1) {
						FileInfo _fi = list.get(0);
						String _deskName = new File(_fi.getFilePath()).getName();
						String _fileName = _fi.getFileName();
						String _uploadDate = DateUtil.format(_fi.getUploadTime(), "yyyyMMddHHmmss");
						_fileName = _uploadDate + _fileName;
						newName.put(_deskName, _fileName);
					}
				}
				temp.put(fileName, "");
				newName.put(deskName, fileName);
				File f = new File(fi.getFilePath());
				fileArray[i] = f;
			}
			File zipFile = new File(new Date().getTime() + ".zip");
			ZipUtil.zip(fileArray, zipFile, newName);

			String pathSaveFile = zipFile.getAbsolutePath();// 要下载的文件
			String fileName = "files.zip";// 保存窗口中显示的文件名
			super.getResponse().reset();
			super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

			/*
			 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
			 */
			ServletOutputStream out = null;
			InputStream inStream = null;
			try {
				fileName = super.getResponse().encodeURL(new String(fileName.getBytes("GBK"), "ISO8859_1"));//

				super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
				// inline
				out = super.getResponse().getOutputStream();
				inStream = new FileInputStream(pathSaveFile);

				// 循环取出流中的数据
				byte[] b = new byte[1024];
				int len;
				while ((len = inStream.read(b)) > 0)
					out.write(b, 0, len);
				super.getResponse().setStatus(super.getResponse().SC_OK);
				super.getResponse().flushBuffer();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (out != null)
					out.close();
				if (inStream != null)
					inStream.close();
				zipFile.delete();
			}
		}
	}

	@Action(value = "toUploadView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toUploadPageView() {
		String module = getRequest().getParameter("module");
		getRequest().setAttribute("module", module);
		return dispatcher("/WEB-INF/page/file/upload.jsp");
	}

	/**
	 * 通用用上传页
	 * 
	 * @return
	 */
	@Action(value = "toCommonUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toCommonUpload() {
		String fileId = getRequest().getParameter("fileId");
		String isUpload = getRequest().getParameter("isUpload");
		if (fileId != null && fileId.length() > 0)
			fileInfo = this.operFileService.queryFileInfoById(fileId);
		String module = getRequest().getParameter("module");
		getRequest().setAttribute("module", module);
		getRequest().setAttribute("isUpload", isUpload);
		return dispatcher("/WEB-INF/page/file/commonUpload.jsp");
	}

	@Action(value = "downloadFileByPath", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadFile() throws Exception {

		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		Properties pro = new Properties();
		pro.load(is);
		String filePath = pro.getProperty("file.save.path");

		String fileName = super.getRequest().getParameter("fileName");

		fileName = filePath + fileName;
		super.getResponse().reset();
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("GBK"), "ISO8859_1"));//
			String fileName1 = fileName;
			String fileNames[] = fileName.split("\\\\");

			if (fileNames.length > 0) {

				fileName1 = fileNames[fileNames.length - 1];
			}
			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName1 + "\"");
			// inline
			out = super.getResponse().getOutputStream();
			inStream = new FileInputStream(new File(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			if (inStream != null)
				inStream.close();
		}

	}

	/**
	 * <br/>
	 * <b>说明</b>:<br/>
	 * <b>注意事项:</b><br/>
	 * 
	 * @return
	 * @throws Exception
	 * 
	 */
	@Action(value = "uploadOffice", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadOffice() throws Exception {

		try {// 解析 request，判断是否有上传文件

			String modelType = super.getRequest().getParameter("modelType");
			String useType = super.getRequest().getParameter("useType");
			String id = super.getRequest().getParameter("picUserId");
			boolean isMultipart = ServletFileUpload.isMultipartContent(getRequest());
			if (isMultipart) {

				DiskFileItemFactory factory = new DiskFileItemFactory();// 设置最多只允许在内存中存储的数据,单位:字节//
				factory.setSizeThreshold(4096);// 设置文件临时存储路径//
				factory.setRepository(new File("D:\\Temp"));// 产生一新的文件上传处理程式
				ServletFileUpload upload = new ServletFileUpload(factory);// 设置路径、文件名的字符集
				upload.setHeaderEncoding("UTF-8");// 设置允许用户上传文件大小,单位:字节
				upload.setSizeMax(-1);// upload.setSizeMax(1024 * 1024);//

				BufferedInputStream in = null;
				List fileItems = upload.parseRequest(getRequest());// 依次处理请求
				Iterator iter = fileItems.iterator();
				while (iter.hasNext()) {
					FileItem item = (FileItem) iter.next();
					if (!item.isFormField()) {// 如果item是正常的表单域

						String fileName = item.getName();
						if (fileName != null) {

							User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
							FileInfo fi = new FileInfo();
							fi.setUploadUser(user);
							// String a =
							// (String)super.getRequest().getAttribute("fileFileName");
							fi.setFileName(this.fileFileName);
							fi.setUploadTime(new Date());
							fi.setOwnerModel(modelType);
							fi.setUseType(useType);
							fi.setModelContentId(id);
							int extIndex = this.fileFileName.lastIndexOf(".");
							String ext = "";
							if (extIndex != -1) {
								ext = this.fileFileName.substring(extIndex + 1);
							}
							fi.setFileType(ext);

							in = new BufferedInputStream(item.getInputStream());// 获得文件输入流

							operFileService.uploadFileStream(in, fi);

						}
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();//

		}

	}

	/**
	 * 
	 * @Title: initFileTable @Description: 查看附件 @author : shengwei.wang @date
	 *         2018年2月9日下午4:07:51 @return String @throws
	 */
	// @Action(value = "initFileTable")
	@Action(value = "initFileList")
	public String initFileTable() {
		String modelType = getParameterFromRequest("modelType");
		String id = getParameterFromRequest("id");
		String flag = getParameterFromRequest("flag");
		putObjToContext("modelType", modelType);
		putObjToContext("id", id);
		putObjToContext("flag", flag);
		return dispatcher("/WEB-INF/page/file/fileInfo/initFileTable.jsp");
	}

	@Action(value = "initFileTableJson")
	public void initFileTableJson() throws Exception {
		String modelType = getParameterFromRequest("modelType");
		String modelId = getParameterFromRequest("id");
		String flag = getParameterFromRequest("flag");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = operFileService.initFileTableJson(start, length, query, col, sort, modelType,
					modelId, flag);
			List<FileInfo> list = (List<FileInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("fileName", "");
			map.put("uploadTime", "yyyy-MM-dd HH:mm:ss");
			map.put("uploadUser-name", "");
			map.put("ownerModel", "");
			map.put("id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public OperFileService getOperFileService() {
		return operFileService;
	}

	public void setOperFileService(OperFileService operFileService) {
		this.operFileService = operFileService;
	}

	public FileInfo getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
}
