package com.biolims.file.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.file.dao.FileInfoDao;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.util.JsonUtils;

@Service
public class FileInfoService {
	@Resource
	private FileInfoDao fileInfoDao;
	/**
	 * 文档管理的遍历
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findFileInfoList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		return fileInfoDao.selectFileInfoList(mapForQuery, startNum, limitNum, dir, sort);
	}

	public List<FileInfo> findFileInfoList(String sId, String type) throws Exception {
		return fileInfoDao.selectFileInfoList(sId, type);
	}

	public long findFileInfoCount(String sId, String type) throws Exception {
		return fileInfoDao.selectFileInfoCount(sId, type);
	}
	
	public long findFileInfoCount(String sId, String type, String usertype) throws Exception {
		return fileInfoDao.selectFileInfoCount(sId, type, usertype);
	}

	public FileInfo getFileInfoById(String id) throws Exception {
		return fileInfoDao.get(FileInfo.class, id);
	}

	/**
	 * 增加文档中心模块
	 * @param et
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FileInfo fif) throws Exception {
		if (fif != null) {
			fif.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			this.fileInfoDao.saveOrUpdate(fif);
		}
	}
	
	
}
