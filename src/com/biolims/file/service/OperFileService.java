package com.biolims.file.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.model.user.User;
import com.biolims.file.dao.OperFileDao;
import com.biolims.file.model.FileInfo;
import com.biolims.log.model.LogInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;

@Service
public class OperFileService {

	@Resource
	private OperFileDao operFileDao;

	public Map<String, Object> queryFileInfo(FileInfo fi, int start, int limit, String dir, String sort,
			String modelType, String modelId, String flag) throws Exception {
		return operFileDao.selectFileInfo(fi, start, limit, dir, sort, modelType, modelId, flag);
	}

	/**
	 * 
	 * @Title: queryFileZjInfo @Description: TODO(质检任务单报告查询) @param @param
	 *         fi @param @param start @param @param limit @param @param
	 *         dir @param @param sort @param @param modelType @param @param
	 *         modelId @param @param flag @param @return @param @throws Exception   
	 *         设定文件 @return Map<String,Object>    返回类型 @author
	 *         zhiqiang.yang@biolims.cn @date 2017-11-22 下午5:19:43 @throws
	 */
	public Map<String, Object> queryFileZjInfo(FileInfo fi, int start, int limit, String dir, String sort,
			String modelType, String modelId, String flag) throws Exception {
		return operFileDao.selectFileZjInfo(fi, start, limit, dir, sort, modelType, modelId, flag);
	}

	public FileInfo queryFileInfoById(String fileId) {
		return operFileDao.get(FileInfo.class, fileId);
	}

	public FileInfo queryFileInfoByName(String fileName) {

		FileInfo a = null;
		List list = operFileDao.findByProperty(FileInfo.class, "fileName", fileName);
		if (list.size() > 0) {

			a = (FileInfo) list.get(0);
		}

		return a;
	}

	@Transactional
	public String uploadFile(File file, FileInfo fi) throws Exception {
		InputStream is = null;
		OutputStream os = null;
		is = new FileInputStream(file);
		String root = ConfigFileUtil.getRootPath() + File.separator + DateUtil.format(new Date(), "yyyyMMdd");
		if (!new File(root).exists()) {
			new File(root).mkdirs();
		}
		// 磁盘文件文件名称
		String deskFileName = UUID.randomUUID().toString();
		File deskFile = new File(root, deskFileName + "." + fi.getFileType());

		fi.setFilePath(deskFile.getAbsolutePath());
		// 将文件保存到数据库
		this.operFileDao.save(fi);
		// 将文件写入磁盘
		os = new FileOutputStream(deskFile);
		byte[] bytefer = new byte[400];
		int length = 0;
		while ((length = is.read(bytefer)) > 0) {
			os.write(bytefer, 0, length);
		}

		if (os != null)
			os.close();
		if (is != null)
			is.close();
		return fi.getId();
	}

	@Transactional
	public String uploadFileStream(InputStream is, FileInfo fi) throws Exception {

		OutputStream os = null;

		String root = ConfigFileUtil.getRootPath() + File.separator + DateUtil.format(new Date(), "yyyyMMdd");
		if (!new File(root).exists()) {
			new File(root).mkdirs();
		}
		// 磁盘文件文件名称
		String deskFileName = UUID.randomUUID().toString();
		File deskFile = new File(root, deskFileName + "." + fi.getFileType());

		fi.setFilePath(deskFile.getAbsolutePath());
		// 将文件保存到数据库
		this.operFileDao.save(fi);
		// 将文件写入磁盘
		os = new FileOutputStream(deskFile);
		byte[] bytefer = new byte[400];
		int length = 0;
		while ((length = is.read(bytefer)) > 0) {
			os.write(bytefer, 0, length);
		}

		if (os != null)
			os.close();
		if (is != null)
			is.close();
		return fi.getId();
	}

	public List<FileInfo> queryFileInfoByIds(String ids) throws Exception {

		return this.operFileDao.selectFileInfoByIds(ids);
	}

	public String getPicByIdModel(String id, String modelType) {
		List<FileInfo> list = operFileDao.selectFileInfoByIdModel(id, modelType, "pic");
		if (list.size() > 0) {
			FileInfo fi = list.get(0);

			return fi.getFilePath();
		}
		return null;
	}

	@Transactional
	public void delFileInfoByIds(String ids) throws Exception {
		List<FileInfo> list = this.operFileDao.selectFileInfoByIds(ids);
		if (list != null && list.size() > 0) {
			this.operFileDao.deleteFileInfoByIds(list);
			this.delFile(list);
		}
	}

	private void delFile(List<FileInfo> list) throws Exception {
		if (list != null) {
			for (FileInfo fi : list) {
				new File(fi.getFilePath()).delete();
			}
		}
	}

	// 查询删除文件的信息
	public FileInfo getFileInfo(String ids) {
		List<FileInfo> list = this.operFileDao.selectFileInfoByIds(ids);
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	public Map<String, Object> initFileTableJson(Integer start, Integer length, String query, String col, String sort,
			String modelType, String modelId, String flag) throws Exception {
		return operFileDao.initFileTableJson(start, length, query, col, sort, modelType, modelId, flag);
	}

	public FileInfo queryFileInfoByContentId(String id) {
		return operFileDao.queryFileInfoByContentId(id);
	}

	@Transactional
	public void delFileInfoByIds(String[] ids, String delStr, String mainId,
			User user) throws Exception {
		String delId = "";
		for (String id : ids) {
			FileInfo scp = operFileDao.get(FileInfo.class, id);
			if (scp.getId() != null) {
				this.operFileDao.delete(scp);
				this.delFile(scp);
			}
			delId += scp.getId() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr + delId);
			operFileDao.saveOrUpdate(li);
		}
	}

	private void delFile(FileInfo fileInfo) throws Exception {
		new File(fileInfo.getFilePath()).delete();
	}
}
