package com.biolims.file.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;

@Repository
@SuppressWarnings("unchecked")
public class OperFileDao extends BaseHibernateDao {

	public List<FileInfo> selectFileInfoByIds(String ids) {
		String hql = "from FileInfo where id in(" + ids + ")";
		List<FileInfo> list = find(hql);
		return list;
	}

	public List<FileInfo> selectFileInfoByIdModel(String ownerModelId, String model, String useType) {
		String hql = "from FileInfo where ownerModel ='" + model + "' and modelContentId ='" + ownerModelId
				+ "' and useType = '" + useType + "'";
		List<FileInfo> list = find(hql);
		return list;
	}

	public void deleteFileInfoByIds(List<FileInfo> list) {
		deleteAll(list);
	}

	public Map<String, Object> selectFileInfo(FileInfo fi, int start, int limit, String dir, String sort,
			String modelType, String modelId, String flag) {
		String hql = "from FileInfo where 1=1";

		if (fi != null) {
			String name = fi.getFileName();
			if (name != null)
				hql = hql + " and fileName like '%" + name + "%'";
			String uploadUser = fi.getUserStr();
			if (uploadUser != null && uploadUser.trim().length() > 0)
				hql = hql + " and uploadUser.realName like '%" + uploadUser + "%'";

		}
		if (modelType != null && !modelType.equals(""))
			hql = hql + " and ownerModel = '" + modelType + "'";
		if (modelId != null)
			hql = hql + " and modelContentId = '" + modelId + "'";

		if (flag != null && !flag.equals(""))
			hql = hql + " and useType = '" + flag + "'";

		List<FileInfo> list = this.getSession().createQuery(hql + " order by " + sort + " " + dir).setFirstResult(start)
				.setMaxResults(limit).list();

		String _hql = "select count(id) " + hql;
		Long count = (Long) this.getSession().createQuery(_hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", list);
		map.put("count", count);
		return map;
	}

	/**
	 * 
	 * @Title: selectFileZjInfo @Description: TODO(质检任务单附件查询) @param @param
	 * fi @param @param start @param @param limit @param @param dir @param @param
	 * sort @param @param modelType @param @param modelId @param @param
	 * flag @param @return    设定文件 @return Map<String,Object>    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-11-22 下午5:21:28 @throws
	 */
	public Map<String, Object> selectFileZjInfo(FileInfo fi, int start, int limit, String dir, String sort,
			String modelType, String modelId, String flag) {
		String[] coid = modelId.split(",");

		String hql = "from FileInfo where 1=1";

		if (fi != null) {
			String name = fi.getFileName();
			if (name != null) {
				hql = hql + " and fileName like '%" + name + "%'";
			}
			String uploadUser = fi.getUserStr();
			if (uploadUser != null && uploadUser.trim().length() > 0) {
				hql = hql + " and uploadUser.realName like '%" + uploadUser + "%'";
			}
		}
		// if (modelType != null && !modelType.equals("")) {
		// hql = hql + " and ownerModel = '" + modelType + "'";
		// }
		String tString = "";
		for (int i = 0; i < coid.length; i++) {

			String newid = coid[i];
			if (i <= 0) {

				tString = "'" + newid + "'";
			} else {
				tString = tString + "," + "'" + newid + "'";
			}
		}
		if (modelId != null) {
			hql = hql + " and modelContentId IN (" + tString + ")";
		}
		// if (flag != null && !flag.equals("")) {
		// hql = hql + " and useType = '" + flag + "'";
		// }
		List<FileInfo> list = this.getSession().createQuery(hql + " order by " + sort + " " + dir).setFirstResult(start)
				.setMaxResults(limit).list();

		String _hql = "select count(id) " + hql;
		Long count = (Long) this.getSession().createQuery(_hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", list);
		map.put("count", count);
		return map;
	}

	public Map<String, Object> initFileTableJson(Integer start, Integer length, String query, String col, String sort,
			String modelType, String modelId, String flag) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FileInfo where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (modelType != null && !modelType.equals(""))
			key = key + " and ownerModel = '" + modelType + "'";
		if (modelId != null)
			key = key + " and modelContentId = '" + modelId + "'";

		if (flag != null && !flag.equals(""))
			key = key + " and useType = '" + flag + "'";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from FileInfo where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<FileInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public FileInfo queryFileInfoByContentId(String id) {
		String hql = " from FileInfo where 1=1 and modelContentId='" + id + "' order by uploadTime desc";
		List<FileInfo> list = new ArrayList<FileInfo>();
		list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
}
