package com.biolims.file.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;

@Repository
@SuppressWarnings("unchecked")
public class FileInfoDao extends BaseHibernateDao {
	//查询主表信息 修改
	public Map<String, Object> selectFileInfoList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from FileInfo where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);

		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<FileInfo> list = new ArrayList<FileInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<FileInfo> selectFileInfoList(String sId, String type) throws Exception {
		String key = " and ownerModel =  '" + type + "' and modelContentId = '" + sId + "' order by uploadTime DESC";
		String hql = "from FileInfo where 1=1 ";

		List<FileInfo> result = this.getSession().createQuery(hql + key).list();

		return result;
	}

	public long selectFileInfoCount(String sId, String type) throws Exception {

		return this.getCount("from FileInfo where ownerModel = '" + type + "' and modelContentId = '" + sId + "'");
	}
	
	public long selectFileInfoCount(String sId, String type, String usertype) throws Exception {

		return this.getCount("from FileInfo where ownerModel = '" + type + "' and modelContentId = '" + sId + "' and useType='"+usertype+"' ");
	}

}
