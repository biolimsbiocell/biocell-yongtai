package com.biolims.file.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 文件信息
 *
 */
@Entity
@Table(name = "T_FILE_INFO")
public class FileInfo extends EntityDao<FileInfo> implements Serializable {

	private static final long serialVersionUID = -5234430496355539041L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "FILE_NAME", length = 100)
	private String fileName;//文件名称

	@Column(name = "CONTENT_NOTE", length = 200)
	private String contentNote;//文件说明

	@Column(name = "FILE_TYPE", length = 50)
	private String fileType;//文件类型 word，excel

	@Column(name = "FILE_PATH", length = 500)
	private String filePath;//文件路径

	@Column(name = "UPLOAD_TIME")
	private Date uploadTime;//上传时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_UPLOAD_USER_ID")
	private User uploadUser;//上传人

	@Column(name = "OWNER_MODEL", length = 100)
	private String ownerModel;//所属模块

	@Column(name = "MODEL_CONTENT_ID", length = 32)
	private String modelContentId;//所属模块内容ID

	@Column(name = "USE_TYPE", length = 32)
	private String useType;//文件使用类型 0 附件 1图片

	@Column(name = "KEY_WORD", length = 400)
	private String keyWord;//关键词

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STUDY_DIRECTION")
	private DicType studyDirection;//研究方向

	@Column(name = "STATE", length = 32)
	private String state;//状态 3 新建  2审批中 1 已归档

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;//状态

	@Column(name = "FILE_NOTE", length = 2000)
	private String fileNote;//文件说明

	@Column(name = "VERSION_NO", length = 32)
	private String versionNo;//版本号
	
	@Column(name = "UP_ID", length = 32)
	private String upId;// 上级编码

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILE_INFO_UP_ID")
	private FileInfo upFileInfo;// 上级位置


	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DOC_TYPE")
	private DicType docType;//文档类型：设计图形、设计文档

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RELATED_MODEL_TYPE")
	private DicType relatedModelType;//关联模块类型： 项目、实验、任务、方法、设备

	@Column(name = "PROJECT_ID", length = 32)
	private String project;// 关联项目

	@Column(name = "PROJECT_TASK_ID", length = 32)
	private String projectTask;//关联任务

	@Column(name = "EXPERIMENT_ID", length = 32)
	private String experiment;//关联实验

	@Column(name = "FUNCTION_ID", length = 32)
	private String function;//关联方法

	@Column(name = "EQUIPMENT_ID", length = 32)
	private String equipment;//关联设备
	//非持久化属性
	@Transient
	private String userStr;
	@Transient
	private String sel;
	@Transient
	private String picPath;
	
	public String getUpId() {
		return upId;
	}

	public void setUpId(String upId) {
		this.upId = upId;
	}

	public FileInfo getUpFileInfo() {
		return upFileInfo;
	}

	public void setUpFileInfo(FileInfo upFileInfo) {
		this.upFileInfo = upFileInfo;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUseType() {
		return useType;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getModelContentId() {
		return modelContentId;
	}

	public void setModelContentId(String modelContentId) {
		this.modelContentId = modelContentId;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}

	public User getUploadUser() {
		return uploadUser;
	}

	public void setUploadUser(User uploadUser) {
		this.uploadUser = uploadUser;
	}

	public String getOwnerModel() {
		return ownerModel;
	}

	public void setOwnerModel(String ownerModel) {
		this.ownerModel = ownerModel;
	}

	public String getUserStr() {
		return userStr;
	}

	public void setUserStr(String userStr) {
		this.userStr = userStr;
	}

	public String getSel() {
		return sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getContentNote() {
		return contentNote;
	}

	public void setContentNote(String contentNote) {
		this.contentNote = contentNote;
	}

	public String getFileNote() {
		return fileNote;
	}

	public void setFileNote(String fileNote) {
		this.fileNote = fileNote;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public DicType getStudyDirection() {
		return studyDirection;
	}

	public void setStudyDirection(DicType studyDirection) {
		this.studyDirection = studyDirection;
	}

	public DicType getDocType() {
		return docType;
	}

	public void setDocType(DicType docType) {
		this.docType = docType;
	}

	public DicType getRelatedModelType() {
		return relatedModelType;
	}

	public void setRelatedModelType(DicType relatedModelType) {
		this.relatedModelType = relatedModelType;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getProjectTask() {
		return projectTask;
	}

	public void setProjectTask(String projectTask) {
		this.projectTask = projectTask;
	}

	public String getExperiment() {
		return experiment;
	}

	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}
