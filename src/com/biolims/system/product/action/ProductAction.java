﻿package com.biolims.system.product.action;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.product.model.Product;
import com.biolims.system.product.model.ProductItem;
import com.biolims.system.product.service.ProductService;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/com/biolims/system/product")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ProductAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "900702";
	@Autowired
	private ProductService productService;
	private Product product = new Product();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showProductItemListJson")
	public void showProductItemListJson() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		String wid = "";
		if(id!=null) {
			Product p = commonDAO.get(Product.class, id);
			if(p!=null) {
				WorkOrder workOrder = p.getWorkOrder();
				if(workOrder!=null) {
					wid = workOrder.getId();
				}
			}
//			wid = commonDAO.get(Product.class, id).getWorkOrder().getId();
		}
		try {
			Map<String,Object> result = productService.showProductItemListJson(id, wid, start, 
					length, query, col, sort);
			List<ProductItem> list = (List<ProductItem>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("applicationId", "");
			map.put("application", "");
			map.put("nextStepId", "");
			map.put("cycle", "");
			map.put("template-id", "");
			map.put("template-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @Title: showProductList
	 * @Description: 显示产品列表
	 * @author : shengwei.wang
	 * @date 2018年2月6日下午1:42:24
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showProductList")
	public String showProductList() throws Exception {
		rightsId = "900702";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/product/productBootstrapTree.jsp");
	}

	@Action(value = "showProductListJson")
	public void showProductListJson() throws Exception {
		String query = getParameterFromRequest("query");
		try {
			List<Product> list = productService.showProductSelTreeJson(query);
			HttpUtils.write(JsonUtils.toJsonString(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "productSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogProductList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/product/productDialog.jsp");
	}

	@Action(value = "showDialogProductListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogProductListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = productService.findProductList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Product> list = (List<Product>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testTime", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("productFee", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testMethod-id", "");
		map.put("testMethod-name", "");
		map.put("mark", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("personName", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("sampleNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editProduct")
	public String editProduct() throws Exception {
		rightsId = "900701";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			product = productService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "product");
		} else {
			// product.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			product.setCreateUser(user);
			product.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		// List<DicType> listReadLong = workOrderDao.getReadLongList();
		List<DicType> listPlatForm = workOrderDao.getPlatForm();
		List<DicType> listReadType = workOrderDao.getReadTypeList();
		List<DicType> listSequencingType = workOrderDao
				.getDicTypeList("sequencingtype");
		List<DicType> listLibraryType = workOrderDao
				.getDicTypeList("librarytype");
		List<DicType> listMethodology = workOrderDao
				.getDicTypeList("methodology");
		List<DicType> listLCIIndex = workOrderDao.getDicTypeList("lciindex");
		List<DicType> listIndex = workOrderDao.getDicTypeList("index");
		List<DicType> listSequencingCycle = workOrderDao
				.getDicTypeList("sequencingcycle");
		List<DicType> listClientIndex = workOrderDao
				.getDicTypeList("clientindex");
		// putObjToContext("listReadLong", listReadLong);
		putObjToContext("listPlatForm", listPlatForm);
		putObjToContext("listReadType", listReadType);
		putObjToContext("listSequencingType", listSequencingType);
		putObjToContext("listLibraryType", listLibraryType);
		putObjToContext("listMethodology", listMethodology);
		putObjToContext("listLCIIndex", listLCIIndex);
		putObjToContext("listIndex", listIndex);
		putObjToContext("listSequencingCycle", listSequencingCycle);
		putObjToContext("listClientIndex", listClientIndex);
		return dispatcher("/WEB-INF/page/com/biolims/system/product/productEditNew.jsp");
	}

	@Action(value = "copyProduct")
	public String copyProduct() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		product = productService.get(id);
		product.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/com/biolims/system/product/productEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String itemJson = getParameterFromRequest("productItemJson");
		String id = product.getId();
		if (id != null && id.equals("")) {
			product.setId(null);
		}
		productService.save(product,changeLog,itemJson);
		return redirect("/com/biolims/system/product/editProduct.action?id="
				+ product.getId());

	}
	/**
	 * 
	 * @Title: toViewProduct  
	 * @Description: 查看产品主数据  
	 * @author : shengwei.wang
	 * @date 2018年3月1日上午10:13:54
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "viewProduct")
	public String toViewProduct() throws Exception {
		String id = getParameterFromRequest("id");
		product = productService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/product/productEditNew.jsp");
	}
	/**
	 * 
	 * @Title: viewWorkOrder  
	 * @Description:查看流程图
	 * @author : shengwei.wang
	 * @date 2018年3月1日上午10:39:55
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="viewWorkOrder")
	public String viewWorkOrder(){
		String workOrderId=getParameterFromRequest("workOrderId");
		String content=null;
		try {
			content=	productService.viewWorkOrderJson(workOrderId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		putObjToContext("content", content);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workOrderFlowchart.jsp");
	}
	// 配置表中关联业务类型
	@Action(value = "productSelectParent", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String productSelectParent() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/product/productParentDialog.jsp");
	}

	@Action(value = "showDialogProductParentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogProductParentListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = productService.findProductLevelList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Product> list = (List<Product>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testTime", "");
		map.put("productFee", "");
		map.put("testMethod-id", "");
		map.put("testMethod-name", "");
		map.put("mark", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("personName", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 访问 任务树
	 */
	@Action(value = "showProductTree", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProductTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/com/biolims/system/product/showProductTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/com/biolims/system/product/productTree.jsp");
	}

	@Action(value = "showProductTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<Product> aList = null;
		/*
		 * if (upId.equals("0")) {
		 * 
		 * aList = productService.findProductList(); } else {
		 * 
		 * }
		 */
		aList = productService.findProductList(upId);

		String a = productService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showProductChildTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductChildTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<Product> aList = productService.findProductList(upId);
		String a = productService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showProductSelectTree", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProductSelectTree() throws Exception {

		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/com/biolims/system/product/showProductSelectTreeJson.action");

		return dispatcher("/WEB-INF/page/com/biolims/system/product/showProductSelectTree.jsp");
	}

	@Action(value = "showProductSelectTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductSelectTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		String selectId = getParameterFromRequest("selectId");
		selectId = selectId + ",";
		List<Product> aList = null;
		if (upId.equals("0") || upId.equals("")) {

			aList = productService.findProductList();
		} else {

		}
		for (Product p : aList) {

			if (selectId.contains(p.getId() + ",")) {
				p.setChecked("1");
			}
		}
		// aList = productService.findProductList(upId);
		// List<Product> aList = productService.findProductList();

		String a = productService.getTreeJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/**
	 * 异步加载产品名称
	 * 
	 * @return
	 */
	@Action(value = "findProductToSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductToSample() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> dataListMap = this.productService
					.findProductToSample(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 通过标识码查找产品类型
	 * 
	 * @return
	 */
	@Action(value = "findProductByMark", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductByMark() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.productService
					.findProductByMark(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 通过id查找区块
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "findOrderBlockById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findOrderBlockById() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, String> dataListMap = this.productService
					.findOrderBlockById(code);
			result.put("success", true);
			result.put("data", dataListMap.get("result"));

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showProductOrderBlockTree")
	public String showProductOrderBlockTree() {
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/com/biolims/system/product/showProductOrderBlockTreeJson.action");

		return dispatcher("/WEB-INF/page/com/biolims/system/product/showProductOrderBlockTree.jsp");
	}

	@Action(value = "showProductOrderBlockTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductOrderBlockTreeJson() throws Exception {
		List<DicType> list = productService.findDicTypeByType("orderBlock");
		String treeStr = productService.getTreeJsonDictype(list);
		new SendData()
				.sendDataJson(treeStr, ServletActionContext.getResponse());

	}

	/**
	 * 
	 * @Title: showProductSelTree
	 * @Description: 选择产品
	 * @author : shengwei.wang
	 * @date 2018年2月6日上午11:20:21
	 * @return String
	 * @throws
	 */
	@Action(value = "showProductSelTree", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProductSelTree() {
		return dispatcher("/WEB-INF/page/com/biolims/system/product/showProductBootstrapTree.jsp");
	}
	@Action(value = "showProductHideFields")
	public void showProductHideFields() throws Exception {
		String id = getRequest().getParameter("id");
		Product product =new Product();
		product= productService.showProductHideFields(id);
		Map<String, Object> result = new HashMap<String, Object>();

		if(product!=null) {
			result.put("hideFields", product.getHideFields());
		}
		HttpUtils.write(JsonUtils.toJsonString(result));

	}
	@Action(value = "fidProductHideFields")
	public void fidProductHideFields() throws Exception {
		List<Product> list = new ArrayList<Product>();
		list= productService.fidProductHideFields();
		Map<String, Object> result = new HashMap<String, Object>();

		if(!list.isEmpty()) {
			result.put("hideFieldsList", list);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));

	}

	@Action(value = "showProductSelTreeJson")
	public void showProductSelTreeJson() throws Exception {
		try {
			List<Product> list = productService.showProductSelTreeJson(null);
			HttpUtils.write(JsonUtils.toJsonString(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showProductTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmCustomerTableJson() throws Exception {
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=productService.showProductTableJson(start,length,query,col,sort);
		List<Product> list = (List<Product>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("productFee", "");
		map.put("hideFields","");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
