package com.biolims.system.product.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.product.dao.ProductDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.product.model.ProductItem;
import com.biolims.system.sys.model.SamplePackSystem;
import com.biolims.system.sys.model.SamplePackSystemItem;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ProductService {
	@Resource
	private ProductDao productDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findProductList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return productDao.selectProductList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	/**
	 * 获取第二级
	 */
	public Map<String, Object> findProductLevelList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return productDao.selectProductLevelList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	/**
	 * 选择product最顶级
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> findParentProductList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return productDao.selectParentProductList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Product i, String changeLog, String itemJson) throws Exception {
		 String p=productDao.getWorkOrder(i.getId());
		if(p!=null&&!p.equals("")){
			WorkOrder wo=commonDAO.get(WorkOrder.class, p);
			String [] productId=wo.getProductId().split(",");
			String  bproductId="";
			for(int j=0;j<productId.length;j++){
				if(productId[j].equals(i.getId())){
				}else{
					bproductId+=productId[j]+",";
				}
			}
			wo.setProductId(bproductId);
			String [] productName=wo.getProductName().split(",");
			String  bproductName="";
			for(int j=0;j<productName.length;j++){
				if(productName[j].equals(i.getName())){
				}else{
					bproductName+=productName[j]+",";
				}
			}
			wo.setProductName(bproductName);
			productDao.saveOrUpdate(wo);
		}
		productDao.saveOrUpdate(i);
		if(i.getWorkOrder()!=null){
			WorkOrder wo=commonDAO.get(WorkOrder.class, i.getWorkOrder().getId());
			if(wo.getProductId()!=null&&!"".equals(wo.getProductId())){
				String[] productId=wo.getProductId().split(",");
				Boolean boo=true;
				for(String pId:productId){
					if(pId.equals(i.getId())){
						boo=false;
					}
				}
				if(boo){
					if(wo.getProductId()!=null&&!wo.getProductId().equals("")){
						wo.setProductId(wo.getProductId()+","+i.getId());
						wo.setProductName(wo.getProductName()+","+i.getName());
					}else{
						wo.setProductId(i.getId());
						wo.setProductName(i.getName());
					}
				}
			}else{
				wo.setProductId(i.getId());
				wo.setProductName(i.getName());
			}
			productDao.saveOrUpdate(wo);
		}
		List<ProductItem> pList = productDao.findProductItemListById(i.getId());
		for (ProductItem item : pList) {
			commonDAO.delete(item);
		}
		List<Map<String,Object>> itemList = JsonUtils.toListByJsonArray(itemJson, List.class);
		for (Map<String, Object> map : itemList) {
			ProductItem productItem = new ProductItem();
			productItem = (ProductItem) commonDAO.Map2Bean(map, productItem);
			if(productItem.getId()!=null&&"".equals(productItem.getId())) {
				productItem.setId(null);
			}
			if(i.getWorkOrder()!=null) {
				productItem.setWorkOrderId(i.getWorkOrder().getId());
			}
			productItem.setProduct(i);
			commonDAO.merge(productItem);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(i.getId());
			li.setClassName("Product");
			li.setModifyContent(changeLog);
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}
	}

	public Product get(String id) {
		Product product = commonDAO.get(Product.class, id);
		return product;
	}

//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(Product sc, String changeLog) throws Exception {
//		if (sc != null) {
//			productDao.saveOrUpdate(sc);
//			if (changeLog != null && !"".equals(changeLog)) {
//				LogInfo li = new LogInfo();
//				li.setLogDate(new Date());
//				User u = (User) ServletActionContext.getRequest().getSession()
//						.getAttribute(SystemConstants.USER_SESSION_KEY);
//				li.setUserId(u.getId());
//				li.setFileId(sc.getId());
//				li.setModifyContent(changeLog);
//				commonDAO.saveOrUpdate(li);
//			}
//			// 若项目定价为空则计算项目定价
//			// 设置保留两位小数
//			DecimalFormat df = new DecimalFormat("######0.00");
//			if ("".equals(sc.getProductFee()) || sc.getProductFee() == null) {
//				if (sc.getMoney() != null && !"".equals(sc.getMoney())
//						&& sc.getDiscount() != null
//						&& !"".equals(sc.getDiscount())) {
//					Double b = Double.parseDouble(sc.getMoney())
//							* Double.parseDouble(sc.getDiscount()) / 100;
//					sc.setProductFee((df.format(b).toString()));
//				}
//			}
//			String jsonStr = "";
//		}
//	}

	/**
	 * 获得子节点列表信息
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public List<Product> getChildList(List<Product> list, Product node)
			throws Exception { // 得到子节点列表
		List<Product> li = new ArrayList<Product>();
		Iterator<Product> it = list.iterator();
		while (it.hasNext()) {
			Product n = (Product) it.next();
			if (n.getParent() != null && n.getParent().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<Product> list, Product node) throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public boolean hasChildCount(Product node) throws Exception {

		Long c = commonDAO.getCount("from Product where parent.id='"
				+ node.getId() + "'");
		return c > 0 ? true : false;
	}

	public String getJson(List<Product> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<Product> nodeList0 = new ArrayList<Product>();
		Iterator<Product> it1 = list.iterator();
		while (it1.hasNext()) {
			Product node = (Product) it1.next();
			// if (node.getLevel() == 0) {
			nodeList0.add(node);
			// }
		}
		Iterator<Product> it = nodeList0.iterator();
		while (it.hasNext()) {
			Product node = (Product) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	/**
	 * 利用treePanel复选框
	 * 
	 * @throws Exception
	 * 
	 */
	public String getTreeJson(List<Product> list) throws Exception {

		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<Product> nodeList0 = new ArrayList<Product>();
		Iterator<Product> it1 = list.iterator();
		while (it1.hasNext()) {
			Product node = (Product) it1.next();
			if (node.getParent() == null || node.getParent().equals("")) {
				nodeList0.add(node);
			}
		}
		Iterator<Product> it = nodeList0.iterator();
		while (it.hasNext()) {
			Product node = (Product) it.next();
			constrctorTreeJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}

	/**
	 * 
	 * 构建treeJson
	 * 
	 * @throws Exception
	 * 
	 */
	public void constrctorTreeJson(List<Product> list, Product treeNode)
			throws Exception {

		if (hasChild(list, treeNode)) {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");

			json.append(",\"leaf\":false");
			if (treeNode.getChecked() != null
					&& treeNode.getChecked().equals("1"))
				json.append(",\"checked\":true");
			else
				json.append(",\"checked\":false");
			json.append(",\"children\":[");
			List<Product> childList = getChildList(list, treeNode);
			Iterator<Product> iterator = childList.iterator();
			while (iterator.hasNext()) {
				Product node = (Product) iterator.next();
				constrctorTreeJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");
			json.append(",\"leaf\":true");
			if (treeNode.getChecked() != null
					&& treeNode.getChecked().equals("1"))
				json.append(",\"checked\":true");
			else
				json.append(",\"checked\":false");
			json.append("},");

		}

	}

	public String getTreeJsonDictype(List<DicType> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<DicType> nodeList0 = new ArrayList<DicType>();
		Iterator<DicType> it1 = list.iterator();
		while (it1.hasNext()) {
			DicType node = (DicType) it1.next();
			nodeList0.add(node);
		}
		Iterator<DicType> it = nodeList0.iterator();
		while (it.hasNext()) {
			DicType node = (DicType) it.next();
			constrctorTreeJson2(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}

	/**
	 * 
	 * 构建treeJson
	 * 
	 * @throws Exception
	 * 
	 */
	public void constrctorTreeJson2(List<DicType> list, DicType treeNode)
			throws Exception {

		json.append("{id:\"");
		json.append(treeNode.getId() + "\"");
		json.append(",text:\"");
		json.append(treeNode.getName() + "\"");
		json.append(",leaf:true");
		json.append(",checked:false");
		json.append("},");

	}

	/**
	 * 构建Json文件
	 * 
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<Product> list, Product treeNode)
			throws Exception {
		json.append("{");
		json.append("\"id\":'");
		json.append(JsonUtils.formatStr(treeNode.getId() == null ? ""
				: treeNode.getId()) + "");
		json.append("',");
		json.append("\"name\":'");
		json.append(JsonUtils.formatStr(treeNode.getName() == null ? ""
				: treeNode.getName()) + "");
		json.append("',");
		json.append("\"testTime\":'");
		json.append(JsonUtils.formatStr(treeNode.getTestTime() == null ? ""
				: treeNode.getTestTime()) + "");
		json.append("',");
		// json.append("\"workType\":'");
		// json.append(JsonUtils.formatStr(treeNode.getWorkType() == null ? "" :
		// treeNode.getWorkType().getName()) + "");
		// json.append("',");
		json.append("\"productFee\":'");
		json.append(JsonUtils.formatStr(treeNode.getProductFee() == null ? ""
				: treeNode.getProductFee()) + "");
		json.append("',");
		json.append("\"createUser\":'");
		json.append(JsonUtils.formatStr(treeNode.getCreateUser() == null ? ""
				: treeNode.getCreateUser().getName()) + "");
		json.append("',");
		/*
		 * json.append("\"createDate\":'");
		 * json.append(JsonUtils.formatStr((String) (treeNode.getCreateDate() ==
		 * null ? "" : treeNode.getCreateDate().getTime())) + "");
		 * json.append("',");
		 */
		json.append("\"state\":'");
		json.append(JsonUtils.formatStr(treeNode.getState() == null ? ""
				: treeNode.getState()) + "");
		json.append("',");
		json.append("\"stateName\":'");
		json.append(JsonUtils.formatStr(treeNode.getStateName() == null ? ""
				: treeNode.getStateName()) + "");
		json.append("',");
		json.append("\"personName\":'");
		json.append(JsonUtils.formatStr(treeNode.getPersonName() == null ? ""
				: treeNode.getPersonName()) + "");
		json.append("',");
		json.append("\"parent\":'");
		json.append(JsonUtils.formatStr(treeNode.getParent() == null ? ""
				: treeNode.getParent()) + "");
		json.append("',");
		if (hasChildCount(treeNode)) {
			json.append("\"leaf\":false");
		} else {
			json.append("\"leaf\":true");
		}
		json.append(",\"upId\":'");
		json.append((treeNode.getParent() == null ? "" : treeNode.getParent())
				+ "");
		json.append("'},");

	}

	public List<Product> findProductList(String upId) throws Exception {
		List<Product> list = null;
		if (upId.equals("0") || upId.equals("")) {
			list = commonDAO
					.find("from Product where  (parent = '' or parent is null ) order by id asc");
		} else {

			list = commonDAO.find("from Product where parent.id='" + upId
					+ "' order by id asc");
		}
		return list;
	}

	public List<Product> findProductList() throws Exception {
		List<Product> list = commonDAO
				.find("from Product where state='1' order by id asc");
		return list;
	}

	/**
	 * 根据编号查询产品
	 */
	public List<Map<String, Object>> findProductToSample(String code)
			throws Exception {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = findProduct(code);
		List<Product> list = (List<Product>) result.get("list");

		if (list != null && list.size() > 0) {
			for (Product srai : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("testTime", srai.getTestTime());

				map.put("libraryType", srai.getLibraryType());
				map.put("methodology", srai.getMethodology());
				map.put("lciIndex", srai.getLciIndex());
				map.put("insertSize", srai.getInsertSize());
				map.put("targetYield", srai.getTargetYield());
				map.put("species", srai.getSpecies());
				map.put("lciGc", srai.getLciGc());
				map.put("notes", srai.getNotes());
				map.put("sequencePlatform", srai.getSequencePlatform());
				map.put("sequenceType", srai.getSequenceType());
				map.put("sequenceLength", srai.getSequenceLength());
				map.put("siGc", srai.getSiGc());
				map.put("siIndex", srai.getSiIndex());
				map.put("clientIndex", srai.getClientIndex());
				map.put("phix", srai.getPhix());
				map.put("exclusiveLane", srai.getExclusiveLane());
				map.put("exclusiveFlowcell", srai.getExclusiveFlowcell());
				map.put("qcRequirements", srai.getQcRequirements());
				map.put("deliveryMethod", srai.getDeliveryMethod());
				map.put("dataAnalysis", srai.getDataAnalysis());
				map.put("analysisDetails", srai.getAnalysisDetails());
				map.put("extraction", srai.getExtraction());
				map.put("qcProtocol", srai.getQcProtocol());
				/* 增加了产品里没有选择组的验证，防止报错 */
				if (srai.getAcceptUserGroup() != null) {
					map.put("acceptUserGroupId", srai.getAcceptUserGroup()
							.getId());
					map.put("acceptUserGroupName", srai.getAcceptUserGroup()
							.getName());
				} else {
					map.put("acceptUserGroupId", "");
					map.put("acceptUserGroupName", "");
				}

				map.put("sequencingApplication",
						srai.getSequencingApplication());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 通过标识码查找产品类型
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> findProductByMark(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = productDao.findProductByMark(code
				.substring(0, 1));
		List<Product> list = (List<Product>) result.get("list");
		DicType bloodTube = productDao.get(DicType.class, code.substring(2, 4));
		Map<String, Object> sampleTypeResult = productDao
				.findDicTypeByMark(code.substring(1, 2));
		List<DicType> sampleTypeList = (List<DicType>) sampleTypeResult
				.get("list");
		if (list != null && list.size() > 0) {
			SamplePackSystem sps = new SamplePackSystem();
			sps = sampleInfoMainDao.selectSystem(list.get(0).getId(),
					sampleTypeList.get(0).getId(), bloodTube.getId());
			List<SamplePackSystemItem> list3 = sampleInfoMainDao.saveSystem(sps
					.getId());
			for (SamplePackSystemItem spsi : list3) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("mark", list.get(0).getId());
				map.put("id", spsi.getId());
				map.put("name", spsi.getName());
				if (spsi.getNum() != null)
					map.put("num", spsi.getNum().toString());
				else
					map.put("num", null);
				map.put("scope", spsi.getScope());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 通过Id查找订单区块
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> findOrderBlockById(String code) throws Exception {
		String[] codeArr = code.split(",");
		String orderBlock = " ";
		Map<String, Object> map = new HashMap<String, Object>();
		for (int i = 0; i < codeArr.length; i++) {
			map = productDao.findOrderBlockById(codeArr[i]);
			List<Product> list = new ArrayList<Product>();
			list = (List<Product>) map.get("list");
			for (int j = 0; j < list.size(); j++) {
				if (list.get(j).getOrderBlock() != null) {
					orderBlock += list.get(j).getOrderBlockId() + ",";
				} else {
					orderBlock += ",";
				}
			}
		}
		Map<String, String> result = new HashMap<String, String>();
		result.put("result", orderBlock);
		return result;

	}

	public List<DicType> findDicTypeByType(String type) throws Exception {
		List<DicType> list = new ArrayList<DicType>();
		list = productDao.findDicTypeByType(type);
		return list;

	}
	public Product showProductHideFields(String id) throws Exception {
		return productDao.showProductHideFields(id);
		

	}
	public List<Product> fidProductHideFields() throws Exception {
		return productDao.fidProductHideFields();
		

	}

	public Map<String, Object> findProduct(String code) throws Exception {
		String[] codes = code.split(",");

		List<Product> list = new ArrayList<Product>();
		for (int i = 0; i < codes.length; i++) {
			Product pro = commonDAO.get(Product.class, codes[i].trim());
			list.add(pro);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	/**
	 * 
	 * @Title: showProductSelTreeJson
	 * @Description: 展示产品
	 * @author : shengwei.wang
	 * @date 2018年2月7日下午3:47:24
	 * @param query
	 * @return
	 * @throws Exception
	 *             List<Product>
	 * @throws
	 */
	public List<Product> showProductSelTreeJson(String query) throws Exception {
		List<Product> result=new ArrayList<Product>();
		List<Product> pList = productDao.showProductSelTreeJson(query);
		List<Product> alist=new ArrayList<Product>();
		result.addAll(pList);
		List<String> strList = new ArrayList<String>();
		for (Product pd : pList) {
			strList.add(pd.getId());
		}
		for (Product p : pList) {
			if (p.getParent() != null&&!"".equals(p.getParent())) {
				List<Product> list = findProductParent(alist, p.getParent());
				for (Product pds : list) {
					if (!strList.contains(pds.getId())) {
						result.add(pds);
						strList.add(pds.getId());
					}
				}
			}
		}
		return result;
	}
	public Map<String, Object> showProductTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return productDao.showProductTableJson(start, length, query, col, sort);
	}

	public List<Product> findProductParent(List<Product> list, String id) {
		Product p = productDao.get(Product.class, id);
		if (p.getParent() != null&&!"".equals(p.getParent())) {
			list.add(p);
			findProductParent(list, p.getParent());
		} else {
			list.add(p);
		}
		return list;
	}

	public String viewWorkOrderJson(String workOrderId) {
		WorkOrder wo=commonDAO.get(WorkOrder.class, workOrderId);
		return wo.getContent();
	}

	public Map<String, Object> showProductItemListJson(String id, String wid, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return productDao.showProductItemListJson(id, wid, start, 
				length, query, col, sort);
	}
}
