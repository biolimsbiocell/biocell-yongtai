package com.biolims.system.product.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.product.model.Product;
import com.biolims.system.product.model.ProductItem;

@Repository
@SuppressWarnings("unchecked")
public class ProductDao extends BaseHibernateDao {
	public Map<String, Object> selectProductList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from Product where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state ='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Product> list = new ArrayList<Product>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}

		for (int i = 0; i < list.size(); i++) {
			String id = list.get(i).getId();
			String num = selSampleCodeNum(id);
			list.get(i).setSampleCodeNum(num);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 查询产品关联样本数量
	public String selSampleCodeNum(String id) {
		String hql = "from SampleInfo t where 1=1 and t.productId='" + id + "'";
		String num = this.getSession().createQuery("select count(*) " + hql)
				.uniqueResult().toString();
		return num;
	}

	/**
	 * 获取第二级
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectProductLevelList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from Product where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and parent is not null ";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Product> list = new ArrayList<Product>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 选择product顶级
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectParentProductList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from Product where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and (parent = '' or parent is null ) ";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Product> list = new ArrayList<Product>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 通过标识码查找product
	 */
	public Map<String, Object> findProductByMark(String code) throws Exception {
		String key = " ";
		String hql = " from Product a where a.mark ='" + code
				+ "' and a.state = '1'";
		List<Product> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	/**
	 * 通过标识码查找product
	 */
	public Map<String, Object> findDicTypeByMark(String code) throws Exception {
		String key = " ";
		String hql = " from DicType a where a.sysCode ='" + code + "'";
		List<Product> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	/**
	 * 通过Id查找订单区块
	 */
	public Map<String, Object> findOrderBlockById(String code) throws Exception {
		String key = " ";
		String hql = " from Product a where a.id ='" + code + "'";
		List<Product> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;

	}

	/**
	 * 查找订单区块
	 */
	public List<DicType> findDicTypeByType(String type) {
		String hql = " from DicType where 1=1 and type='" + type + "'";//
		String key = "";

		key = key + " order by orderNumber ASC";
		List<DicType> list = new ArrayList<DicType>();
		list = this.getSession().createQuery(hql + key).list();
		return list;

	}

	// 根据编号查询数据
	public Product setProductList(String id) {
		String hql = "from Product where 1=1 and product.id='" + id + "'";
		Product p = (Product) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}
	public Product showProductHideFields(String id) {
		String hql = "from Product p where 1=1 and p.id='" + id + "'";
		Product p = (Product) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}
	public List<Product> fidProductHideFields() {
		String hql = "from Product  where 1=1 ";
		List<Product> list = new ArrayList<Product>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<Product> showProductSelTreeJson(String query) throws Exception {
		String key = " ";
		String hql = " from Product where 1=1";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		List<Product> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	
	public Map<String, Object> showProductTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Product where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Product where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Product> list = new ArrayList<Product>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public String getWorkOrder(String id) {
		String hql = "select work_order from sys_product where 1=1 and id='"
				+ id + "'";
		String p = (String) this.getSession().createSQLQuery(hql)
				.uniqueResult();
		return p;
	}

	public Product getProductByName(String name) {
		String hql = "from Product where name='" + name + "'";
		Product p = (Product) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}

	public Map<String, Object> showProductItemListJson(String id, String wid, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ProductItem where 1=1 and product.id ='"+id+"'"
				+ " and workOrderId ='"+wid+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ProductItem where 1=1 and product.id ='"+id+"'"
					+ " and workOrderId ='"+wid+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ProductItem> list = new ArrayList<ProductItem>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<ProductItem> findProductItemListById(String pid) {
		String hql = "from ProductItem where product.id='" + pid + "'";
		List<ProductItem>list = new ArrayList<ProductItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
}