package com.biolims.system.product.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;

/**
     * @ClassName: ProductItem  
     * @Description: TODO  
     * @author 孙灵达  
     * @date 2018年8月20日
 */
@Entity
@Table(name = "SYS_PRODUCT_ITEM")
@SuppressWarnings("serial")
public class ProductItem extends EntityDao<ProductItem> implements java.io.Serializable {
	/** 编码 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	private String id;
	/** 步骤名称 */
	private String name;
	/** 备注 */
	private String note;
    /**生产模块ID*/
	private String applicationId;
	/**生产模块*/
	private String application;
	/**下一步实验ID*/
	private String nextStepId;
	/**周期*/
	private String cycle;
	/**主表**/
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	private Product product;
	/**关联流程ID*/
	private String workOrderId;
	/**模板*/
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	private Template template;
	
	
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	public String getWorkOrderId() {
		return workOrderId;
	}
	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getNextStepId() {
		return nextStepId;
	}
	public void setNextStepId(String nextStepId) {
		this.nextStepId = nextStepId;
	}
	public String getCycle() {
		return cycle;
	}
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	
}