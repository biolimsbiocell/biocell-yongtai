package com.biolims.system.user.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.enmonitor.template.model.RoomTemplate;
import com.biolims.system.user.dao.UserGroupUserDao;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class UserGroupUserService {
	@Resource
	private UserGroupUserDao userGroupUserDao;
	@Resource
	private CommonDAO commonDAO;

	StringBuffer json = new StringBuffer();

	// 根据人员组id查询人员
	public Map<String, Object> getUserGroupUserBygroupId(String userGroupId) {
		Map<String, Object> result = this.userGroupUserDao
				.getUserGroupUserBygroupId(userGroupId);
		return result;
	}

//	public List<UserGroupUser> getSelUserGroupUser(String testUserId,
//			List<UserGroupUser> userList) {
//		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
//		for (int i = 0; i < userList.size(); i++) {
//			if (testUserId != null) {
//				if (testUserId.indexOf(userList.get(i).getUser().getId()) > -1) {
//					selUser.add(userList.get(i));
//					userList.remove(i);
//					i--;
//				}
//			}
//		}
//		return selUser;
//	}
}
