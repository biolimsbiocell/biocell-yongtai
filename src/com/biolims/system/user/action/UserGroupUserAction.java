package com.biolims.system.user.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.user.service.UserService;
import com.biolims.experiment.uf.model.UfTask;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/user/userGroupUser")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class UserGroupUserAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private UserGroupUserService userGroupUserService;
	@Autowired
	private UserService userService;
	private UserGroupUser userGroupUser = new UserGroupUser();

	@Action(value = "userGroupUserSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String userGroupUserSelect() throws Exception {
		String gid = getParameterFromRequest("gid");
		putObjToContext("gid", gid);
		return dispatcher("/WEB-INF/page/system/user/showUserGroupUser.jsp");
	}

	@Action(value = "showuserGroupUserSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showuserGroupUserSelectJson() throws Exception {
		// int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// String dir = getParameterFromRequest("dir");
		// String sort = getParameterFromRequest("sort");
		// String data = getParameterFromRequest("data");
		// Map<String, String> map2Query = new HashMap<String, String>();
		// if (data != null && data.length() > 0)
		// map2Query = JsonUtils.toObjectByJson(data, Map.class);
		String gid = getParameterFromRequest("gid");
		gid="SA";
		Map<String, Object> result = userGroupUserService
				.getUserGroupUserBygroupId(gid);
		Long count = (Long) result.get("total");
		List<UserGroupUser> list = (List<UserGroupUser>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("userGroup-id", "");
		map.put("userGroup-name", "");
		map.put("user-id", "");
		map.put("user-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public UserGroupUserService getUserGroupUserService() {
		return userGroupUserService;
	}

	public void setUserGroupUserService(
			UserGroupUserService userGroupUserService) {
		this.userGroupUserService = userGroupUserService;
	}

	public UserGroupUser getUserGroupUser() {
		return userGroupUser;
	}

	public void setUserGroupUser(UserGroupUser userGroupUser) {
		this.userGroupUser = userGroupUser;
	}

}
