package com.biolims.system.user.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class UserGroupUserDao extends BaseHibernateDao {

	// 根据人员组id查询人员
	public Map<String, Object> getUserGroupUserBygroupId(String userGroupId) {
		String hql = "from UserGroupUser where 1=1 and userGroup.id='"
				+ userGroupId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<UserGroupUser> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}