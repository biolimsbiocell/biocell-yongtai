package com.biolims.system.code;

public class SystemCode{

	public static final String DEFAULT_SYSTEMCODE = "NEW";
	/**
	 * 库存主数据-明细 0000-9999
	 */
	public static final long STORAGE_ITEM_CODE = 000000;
	/**
	 * 库存主数据-明细
	 */
	public static final String STORAGE_ITEM_NAME = "StorageReagentBuySerial";
	/**
	 * 库存主数据-领用管理100000-199999
	 */
	public static final long STORAGE_APPLE_CODE = 100000;
	/**
	 * 库存主数据-领用管理
	 */
	public static final String STORAGE_APPLE_NAME = "StorageApply";

	/**
	 * 库存主数据-出库管理300000-399999
	 */
	public static final long STORAGE_OUT_CODE = 300000;
	/**
	 * 库存主数据-出库管理
	 */
	public static final String STORAGE_OUT_NAME = "StorageOut";
	/**
	 * 库存主数据-退库管理400000-499999
	 */
	public static final long STORAGE_QUIT_CODE = 400000;
	/**
	 * 库存主数据-退库管理
	 */
	public static final String STORAGE_QUIT_NAME = "StorageQuit";
	/**
	 * 库存主数据-报废管理10000-19999
	 */
	public static final long STORAGE_CANCEL_CODE = 10000;
	/**
	 * 库存主数据-报废管理
	 */
	public static final String STORAGE_CANCEL_NAME = "StorageCancel";
	/**
	 * 库存主数据-移库管理20000-29999
	 */
	public static final long STORAGE_MOVE_CODE = 20000;
	/**
	 * 库存主数据-移库管理
	 */
	public static final String STORAGE_MOVE_NAME = "StorageMove";
	/**
	 * 库存主数据-盘点管理9000-9999
	 */
	public static final long STORAGE_CHECK_CODE = 9000;
	/**
	 * 库存主数据-盘点管理
	 */
	public static final String STORAGE_CHECK_NAME = "StorageCheck";
	/**
	 * 库存主数据-调整管理8000-8999
	 */
	public static final long STORAGE_MODIFY_CODE = 8000;
	/**
	 * 库存主数据-调整管理
	 */
	public static final String STORAGE_MODIFY_NAME = "StorageModify";
	/**
	 * 库存主数据-调度管理9000-9999
	 */
	public static final long STORAGE_ITEM_MODIFY_CODE = 9000;
	/**
	 * 库存主数据-调整管理
	 */
	public static final String STORAGE_ITEM_MODIFY_NAME = "StorageItemModify";
	/**
	 * 实验材料-领用管理200000-299999
	 */
	public static final long MATERIALS_APPLY_CODE = 200000;
	/**
	 * 实验材料-领用管理
	 */
	public static final String MATERIALS_APPLY_NAME = "MaterialsApply";
	/**
	 * 实验材料-制备单管理600000-699999
	 */
	public static final long MATERIALS_ORDER_CODE = 600000;
	/**
	 * 实验材料-制备单管理
	 */
	public static final String MATERIALS_ORDER_NAME = "MaterialsOrder";
	/**
	 * 采购-采购申请管理500000-599999
	 */
	public static final long PURCHASE_APPLY_CODE = 500000;
	/**
	 * 采购-采购申请
	 */
	public static final String PURCHASE_APPLY_NAME = "PurchaseApply";
	/**
	 * 采购-采购订单管理700000-799999
	 */
	public static final long PURCHASE_ORDER_CODE = 000;//原7000000
	/**
	 * 采购-采购定单
	 */
	public static final String PURCHASE_ORDER_NAME = "PurchaseOrder";
	/**
	 * 采购-采购入库管理900000-999999
	 */
	public static final long PURCHASE_STORAGEIN_CODE = 900000;
	/**
	 * 采购-采购入库
	 */
	public static final String PURCHASE_STORAGEIN_NAME = "StorageIn";
	/**
	 * 采购-采购退货管理3000-39999
	 */
	public static final long PURCHASE_CANCEL_CODE = 3000;
	/**
	 * 采购-采购退货
	 */
	public static final String PURCHASE_CANCEL_NAME = "PurchaseCancel";

	/**
	 * 采购-采购付款管理800000-899999
	 */
	public static final long PURCHASE_PAYMENT_CODE = 800000;
	/**
	 * 采购-采购付款
	 */
	public static final String PURCHASE_PAYMENT_NAME = "PurchasePayment";
	/**
	 * 设备-设备故障管理FN000-FN999
	 */
	public static final long INSTRUMENT_PAYMENT_CODE = 000;
	/**
	 * 设备-设备故障
	 */
	public static final String INSTRUMENT_PAYMENT_NAME = "";
	/**
	 * 设备-设备维修管理W0000-W9999
	 */
	public static final long INSTRUMENT_REPAIR_CODE = 0000;
	/**
	 * 设备-设备维修
	 */
	public static final String INSTRUMENT_REPAIR_NAME = "InstrumentRepair";
	/**
	 * 设备-设备维修管理前缀
	 */
	public static final String INSTRUMENT_REPAIR_CODE_PREFIX = "W";
	/**
	 * 设备-设备维修管理去除前缀后的位数
	 */
	public static final Integer INSTRUMENT_REPAIR_CODE_LENGTH = 4;
	/**
	 * 设备-预防性维修管理PM000-999
	 */
	public static final long INSTRUMENT_REPAIRPLAN_CODE = 000;
	/**
	 * 设备-预防性维修
	 */
	public static final String INSTRUMENT_REPAIRPLAN_NAME = "InstrumentRepairPlan";
	/**
	 * 设备-预防性维修前缀
	 */
	public static final String INSTRUMENT_REPAIRPLAN_CODE_PREFIX = "PM";
	/**
	 * 设备-设备维修管理去除前缀后的位数
	 */
	public static final Integer INSTRUMENT_REPAIRPLAN_CODE_LENGTH = 3;

	/**
	 * 设备-借用调拨管理100-999
	 */
	public static final long INSTRUMENT_BORROW_CODE = 100;
	/**
	 * 设备-借用调拨
	 */
	public static final String INSTRUMENT_BORROW_NAME = "InstrumentBorrow";
	/**
	 * 项目-预算管理B00000-B99999
	 */
	public static final long PROJECT_BUDGET_CODE = 00000;
	/**
	 * 项目-预算管理
	 */
	public static final String PROJECT_BUDGET_NAME = "ProjectBudget";
	/**
	 * 项目-预算管理前缀
	 */
	public static final String PROJECT_BUDGET_NAME_PREFIX = "B";
	/**
	 * 项目-预算管理B00000-B99999
	 */
	public static final Integer PROJECT_BUDGET_CODE_CODELENGTH = 5;
	/**
	 * 项目-项目管理40000-59999
	 */
	public static final long PROJECT_PROJECT_CODE = 1000000;
	/**
	 * 项目-项目管理
	 */
	public static final String PROJECT_PROJECT_NAME = "Project";
	/**
	 * 项目-决算管理FA0000-FA9999
	 */
	public static final long PROJECT_FINAL_CODE = 0000;
	/**
	 * 项目-决算管理
	 */
	public static final String PROJECT_FINAL_NAME = "ProjectFinal";
	/**
	 * 项目-决算管理前缀
	 */
	public static final String PROJECT_FINAL_NAME_PREFIX = "FA";
	/**
	 * 项目-决算管理前缀
	 */
	public static final Integer PROJECT_FINAL_CODE_CODELENGTH = 4;
	/**
	 * 实验-实验管理60000-79999
	 */
	public static final long EXPERIMENT_EXPERIMENT_CODE = 60000;
	/**
	 * 实验-实验管理
	 */
	public static final String EXPERIMENT_EXPERIMENT_NAME = "Experiment";
	/**
	 * 实验-实验模板EX000-EX499
	 */
	public static final long EXPERIMENT_TEMPLATE_CODE = 000;
	/**
	 * 实验-实验模板
	 */
	public static final String EXPERIMENT_TEMPLATE_NAME = "ExperimentTemplate";
	/**
	 * 实验-实验模板前缀
	 */
	public static final String EXPERIMENT_TEMPLATE_NAME_PREFIX = "EX";
	/**
	 * 实验-实验模板长度
	 */
	public static final Integer EXPERIMENT_TEMPLATE_CODE_CODELENGTH = 3;
	/**
	 * 检测检定-样本检验80000-99999
	 */
	public static final long SAMPLE_CHECK_CODE = 80000;
	/**
	 * 检测检定-样本检验
	 */
	public static final String SAMPLE_CHECK_NAME = "SampleCheck";
	/**
	 * 检测检定-检验模板EX500-EX999
	 */
	public static final long SAMPLE_TEMPLATE_CODE = 500;
	/**
	 * 检测检定-检验模板
	 */
	public static final String SAMPLE_TEMPLATE_NAME = "SampleCheckTemplate";

	/**
	 * 检测检定-检验模板前缀
	 */
	public static final String SAMPLE_TEMPLATE_NAME_PREFIX = "EX";
	/**
	 * 检测检定-检验模板
	 */
	public static final Integer SAMPLE_TEMPLATE_CODE_CODELENGTH = 3;

	//产前检测相关编码
	/**
	 * 海外业务
	 */
	public static final String KNOWLEDGE_BASE_NAME = "KnowledgeBase";
	/**
	 * 海外业务code
	 */
	public static final long KNOWLEDGE_BASE_CODE = 000;
	/**
	 * 海外业务
	 */
	public static final String OVERSEAS_MEDICAL_NAME = "CrmOverseasMedical";
	/**
	 * 海外业务code
	 */
	public static final long OVERSEAS_MEDICAL_CODE = 000;
	/**
	 * 样本接收
	 */
	public static final String SAMPLE_RECEIVER_NAME = "SampleBloodReceive";
	/**
	 * 样本接收code
	 */
	public static final long SAMPLE_RECEIVER_CODE = 000;
	/**
	 * 样本接收
	 */
	public static final String SAMPLE_DOCTOR_NAME = "CrmDoctor";
	/**
	 * 样本接收code
	 */
	public static final long SAMPLE_DOCTOR_CODE = 0000;
	/**
	 * 样本接收
	 */
	public static final String SAMPLE_GEN_NAME = "SampleGen1Ver";
	/**
	 * 样本接收code
	 */
	public static final long SAMPLE_GEN_CODE = 0000;
	/**
	 * 报告
	 */
	public static final String PEPORT_NAME = "SampleReportItem";
	/**
	 * 样本接收code
	 */
	public static final long PEPORT_CODE = 000;
	/**
	 * QPCR检测任务
	 */
	public static final String QPCR_NAME = "SampleQPCRTaskItem";
	/**
	 * 样本接收code
	 */
	public static final long QPCR_CODE = 0;
	/**
	 * PCR检测任务
	 */
	public static final String PCR_NAME = "SamplePCRTaskItem";
	/**
	 * 样本接收code
	 */
	public static final long PCR_CODE = 0;
	/**
	 * 样本接收Item
	 */
	public static final String SAMPLE_RECEIVER_ITEM_NAME = "SampleBloodInfo";
	/**
	 * 样本接收Itemcode
	 */
	public static final long SAMPLE_RECEIVER_ITEM_CODE = 00;
	/**
	 * 病人
	 */
	public static final String PATIENT_NAME = "CrmPatient";
	/**
	 * 病人code
	 */
	public static final long PATIENT_CODE = 000;
	/**
	 * Item
	 */
	public static final String SAMPLE_PLASAMA_ITEM_NAME = "SamplePlasmaInfoTemp";
	/**
	 * Itemcode
	 */
	public static final long SAMPLE_PLASAMA_ITEM_CODE = 0;
	/**
	 * Item
	 */
	public static final String SAMPLE_DNA_TASK_ITEM_NAME_A = "SampleDNATaskItem";
	/**
	 * Itemcode
	 */
	public static final long SAMPLE_DNA_TASK_ITEM_CODE_A = 0;
	/**
	 * Item
	 */
	public static final String SAMPLE_DNA_TASK_ITEM_NAME = "SampleDNATaskItem";
	/**
	 * Itemcode
	 */
	public static final long SAMPLE_DNA_TASK_ITEM_CODE = 00;

	public static final String SAMPLE_DNA_ITEM_NAME = "SampleDNAInfo";
	/**
	 * Itemcode
	 */
	public static final long SAMPLE_DNA_ITEM_CODE = 00;

	/**
	 * Item
	 */
	public static final String SAMPLE_WK_ITEM_NAME = "SampleWKInfo";
	/**
	 * Itemcode
	 */
	public static final long SAMPLE_WK_ITEM_CODE = 0;
	public static final String SAMPLE_WK_TASK_ITEM_NAME = "SampleWKTaskItem";
	/**
	 * Itemcode
	 */
	public static final long SAMPLE_WK_TASK_ITEM_CODE = 00;
	/**
	 * 样本接收全血前缀
	 */
	public static final String SAMPLE_RECEIVER_BLOOD_PREFIX = "QXJS";
	/**
	 * 样本接收血浆前缀
	 */
	public static final String SAMPLE_RECEIVER_PLASMA_PREFIX = "XJJS";
	/**
	 * 样本接收流水码长度
	 */
	public static final Integer SAMPLE_RECEIVER_CODE_CODELENGTH = 2;

	/**
	 * 全血领用
	 */
	public static final String SAMPLE_BLOOD_APPL_NAME = "SampleCommonApply";
	/**
	 * 全血领用编码前缀
	 */
	public static final String SAMPLE_BLOOD_APPL_PREFIX = "QXLY";
	/**
	 * 全血领用流水号长度
	 */
	public static final Integer SAMPLE_BLOOD_APPL_CODE_CODELENGTH = 000;

	/**
	 * 血浆入库申请
	 */
	public static final String SAMPLE_PLASMA_IN_NAME = "SamplePlasmaStorageIn";
	/**
	 * 血浆入库申请前缀
	 */
	public static final String SAMPLE_PLASMA_IN_PREFIX = "XJSQ";
	/**
	 * 血浆入库申请流水号长度
	 */
	public static final Integer SAMPLE_PLASMA_IN_CODE_CODELENGTH = 2;

	/**
	 * 血浆制备
	 */
	public static final String SAMPLE_PLASMA_NAME = "SampleCommonTask";
	/**
	 * 血浆制备前缀
	 */
	public static final String SAMPLE_PLASMA_PREFIX = "PR";
	/**
	 * 血浆制备流水号长度
	 */
	public static final Integer SAMPLE_PLASMA_CODE_CODELENGTH = 2;

	/**
	 * 血浆领用
	 */
	public static final String SAMPLE_PLASMA_APPL_NAME = "SampleCommonApply";
	/**
	 * 血浆领用
	 */
	public static final String SAMPLE_PLASMA_APPL_PREFIX = "XJLY";

	/**
	 * 血浆领用流水号长度
	 */
	public static final Integer SAMPLE_PLASMA_APPL_CODE_CODELENGTH = 2;

	/**
	 * DNA提取任务
	 */
	public static final String SAMPLE_DNA_TASK_NAME = "SampleCommonTask";

	/**
	 * DNA提取任务前缀
	 */
	public static final String SAMPLE_DNA_TASK_PREFIX = "DNA";

	/**
	 * DNA提取任务流水号
	 */
	public static final Integer SAMPLE_DNA_TASK_CODE_CODELENGTH = 2;

	/**
	 * 文库提取任务
	 */
	public static final String SAMPLE_WK_TASK_NAME = "SampleCommonTask";
	/**
	 * 文库构建前缀
	 */
	public static final String SAMPLE_WK_TASK_PREFIX = "JK";
	/**
	 * 文库构建流水码长度
	 */
	public static final Integer SAMPLE_WK_TASK_CODE_CODELENGTH = 2;

	/**
	 * QC检测接收任务
	 */
	public static final String SAMPLE_QC_TASK_NAME = "SampleQCTask";
	/**
	 * QC检测接收任务前缀
	 */
	public static final String SAMPLE_QC_TASK_PREFIX = "QC";
	/**
	 *  QC检测接收任务流水号长度
	 */
	public static final Integer SAMPLE_QC_TASK_CODELENGTH = 2;

	/**
	 * 2100检测接收任务
	 */
	public static final String SAMPLE_2100_TASK_NAME = "Sample2100Task";
	/**
	 * 2100检测接收任务前缀
	 */
	public static final String SAMPLE_2100_TASK_PREFIX = "QC";
	/**
	 *  2100检测接收任务流水号长度
	 */
	public static final Integer SAMPLE_2100_TASK_CODE_CODELENGTH = 2;

	/**
	 * QPCR检测接收任务
	 */
	public static final String SAMPLE_QPCR_TASK_NAME = "SampleQPCRTask";
	/**
	 * QPCR检测接收任务前缀
	 */
	public static final String SAMPLE_QPCR_TASK_PREFIX = "DT";
	/**
	 *  QPCR检测接收任务流水号长度
	 */
	public static final Integer SAMPLE_QPCR_TASK_CODE_CODELENGTH = 2;
	/**
	 * 外包任务
	 */
	public static final String SAMPLE_OUT_TASK_PREFIX = "OT";

	public static final String SAMPLE_OUT_TASK_NAME = "SampleOutSourcing";

	public static final Integer SAMPLE_OUT_TASK_CODE_CODELENGTH = 2;

	/**
	 * QPCR检测接收任务前缀
	 */

	/**
	 *  QPCR检测接收任务流水号长度
		/**
	 * QPCR检测接收任务
	 */
	public static final String SAMPLE_PCR_TASK_PREFIX = "ML";
	public static final String SAMPLE_PCR_TASK_NAME = "SamplePCRTask";
	public static final Integer SAMPLE_PCR_TASK_CODE_CODELENGTH = 2;

	public static final String SAMPLE_G_TASK_PREFIX = "G";
	public static final String SAMPLE_G_TASK_NAME = "SampleGenerationTask";
	public static final Integer SAMPLE_G_TASK_CODE_CODELENGTH = 2;
	/**
	 * 杂交
	 */
	public static final String SAMPLE_CROSS_TASK_NAME = "SampleCrossTask";
	public static final long SAMPLE_CROSS_TASK_CODE = 000;
	/**
	 * Pooling检测接收任务
	 */
	public static final String SAMPLE_POOLING_TASK_NAME = "SamplePoolingTask";
	public static final long SAMPLE_POOLING_TASK_CODE = 000;
	/**
	 * Pooling检测接收任务前缀
	 */
	public static final String SAMPLE_POOLING_TASK_PREFIX = "A";
	/**
	 *  Pooling检测接收任务流水号长度
	 */
	public static final Integer SAMPLE_POOLING_TASK_CODE_CODELENGTH = 4;
	/**
	 * 调度排片检测接收任务
	 */
	public static final String SAMPLE_SCHEDULING_TASK_NAME = "SampleSchedulingTask";
	public static final long SAMPLE_SCHEDULING_TASK_CODE = 0;
	/**
	 * 调度排片检测接收任务前缀
	 */
	public static final String SAMPLE_SCHEDULING_TASK_PREFIX = "CX";
	/**
	 *  调度排片检测接收任务流水号长度
	 */
	public static final Integer SAMPLE_SCHEDULING_TASK_CODE_CODELENGTH = 2;

	/**
	 * 样本出库
	 */
	public static final String SAMPLE_RECEIVE_OUT_NAME = "SampleBloodOut";

	public static final String SAMPLE_RESULT_BACK = "SampleResultBack";
	public static final String SAMPLE_RESULT_NAME = "SampleGen1Task";

	public static final Integer SAMPLE_RESULT_CODE = 000;

	public static final Integer SAMPLE_RESULT_BACK_CODE = 000;
	public static final String SAMPLE_GEN2_NAME = "SampleGen2Task";
	public static final Integer SAMPLE_GEN2_CODE = 000;
	/**
	 * 样本出库前缀
	 */
	public static final String SAMPLE_RECEIVE_OUT_PREFIX = "CX";
	/**
	 * 样本出库流水号长充
	 */
	public static final Integer SAMPLE_RECEIVE_OUT_CODELENGTH = 000;

	/**
	 * 数据质控
	 */
	public static final String SAMPLE_DATA_QUALITY_CONTROL_NAME = "SampleDataQualityControl";
	/**
	 * 数据质控前缀
	 */
	public static final String SAMPLE_DATA_QUALITY_CONTROL_PREFIX = "SJZK";
	/**
	 * 数据质控流水号长充
	 */
	public static final Integer SAMPLE_DATA_QUALITY_CONTROL_CODELENGTH = 2;

	/**
	 * 结果反馈
	 */
	public static final String SAMPLE_RESULT_BACK_NAME = "SampleResultBack";
	/**
	 * 结果反馈前缀
	 */
	public static final String SAMPLE_RESULT_BACK_PREFIX = "JGFK";
	/**
	 * 结果反馈流水号长充
	 */
	public static final Integer SAMPLE_RESULT_BACK_CODELENGTH = 3;

	/**
	 * 报告发送
	 */
	public static final String SAMPLE_REPORT_NAME = "SampleReport";
	/**
	 * 报告发送前缀
	 */
	public static final String SAMPLE_REPORT_PREFIX = "BGFS";
	/**
	 * 报告发送流水号长充
	 */
	public static final Integer SAMPLE_REPORT_CODELENGTH = 2;
	/**
	 * 生物分析任务单
	 */
	public static final String SAMPLE_INFORMATION_TASK_NAME = "SampleInformationTask";
	/**
	 * 生物分析任务单
	 */
	public static final Integer SAMPLE_INFORMATION_TASK_CODE = 000;
	public static final long SAMPLE_KY_CODE = 00000;
	public static final String SAMPLE_KY_NAME = "Project";
}
