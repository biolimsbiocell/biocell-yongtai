package com.biolims.system.code;

public class SystemConstants extends com.biolims.common.constants.SystemConstants{


	public static final String COOKIE_SESSION_KEY = "session_key";

	public static final boolean SESSION_CLUSTERID = false;
	/**
	 * session中user的key
	 */
	public static final String USER_SESSION_KEY = "userSession";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS = "userRights";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_PORTLET_CONTENT = "userPortlets";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_ACTION_RIGHTS = "userActionRights";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS_DEPARTMENT = "userRightsDepartment";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS_APPLICATION_TYPE_ACTION = "userRightsApplicationTypeAction";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS_ACTION = "userRightsAction";
	/**
	 * session中user的部门列表
	 */
	public static final String USER_SESSION_DEPARTMENT = "userDepartment";

	/**
	 * session中user的部门范围是否全部
	 */
	public static final String USER_SESSION_DEPARTMENT_ALL = "userDepartmentAll";
	/**
	 * 国际化文件对应的路径
	 */
	public static final String I18N_RESOURCES = getProperty("i18n_resources", "/i18n/messages");

	/**
	 * 下机质控文件路径
	 */
	public static final String SEQ_QC_PATH = getProperty("seq_qc_path", "d:/qcData/in");
	/**
	 * 下机质控文件路径
	 */
	public static final String SEQ_QC_OUT_PATH = getProperty("seq_qc_out_path", "d:/qcData/out");
	
	/**
	 * 信息分析文件路径
	 */
	public static final String INFO_QC_PATH = getProperty("info_qc_path", "d:/qcData/");
	
	/**
	 * 下机质控写入文件路径
	 */
	public static final String WRITE_QC_PATH = getProperty("write_qc_path", "d:/qcData/");
	
	/**
	 * 信息分析写入文件路径
	 */
	public static final String INFO_WRITE_PATH = getProperty("info_write_path", "d:/qcData/");
	
	public static final String INFO_WRITE_PATH_TEMP = getProperty("info_write_path_temp", "E:\\");
	
	
	/**
	 * 工作流文件存储路径
	 */
	public static final String WORKFLOW_FILE_PATH = getProperty("workflow_file_path", "d:/wfData/");

	/**
	 * 男性
	 */
	public static final String DIC_USER_MALE = "1";
	/**
	 * 女性
	 */
	public static final String DIC_USER_FEMALE = "0";
	/**
	 * 用户状态:生效
	 */
	public static final String DIC_USER_IN_USE = "1";
	/**
	 * 用户状态:失效
	 */
	public static final String DIC_USER_NOT_USE = "0";
	/**
	 * 状态:生效
	 */
	public static final String DIC_TYPE_IN_USE = "1";
	/**
	 * 状态:失效
	 */
	public static final String DIC_TYPE_NOT_USE = "0";

	public static final String DIC_TYPE_INSTRUMENT_BORROW_JY = "jy";
	public static final String DIC_TYPE_INSTRUMENT_BORROW_DB = "db";

	/**
	 * 字典表-教育程度
	 */
	public static final String DIC_TYPE_EDU_LEVEL = "edulevel";
	/**
	 * 字典表-判断类型
	 */
	public static final String DIC_TYPE_JUDGE = "judge";
	/**
	 * 字典表-判断类型
	 */
	public static final String DIC_TYPE_RESULT = "result";
	/**
	 * 字典表-单位:时间
	 */
	public static final String DIC_UNIT_TIME = "time";
	/**
	 * 字典表-单位:计量
	 */
	public static final String DIC_UNIT_WEIGHT = "weight";
	/**
	 * 字典表-部门类型
	 */
	public static final String DIC_TYPE_DEPARTMENT = "departmenttype";

	/**
	 * 字典表-合同类型-收款
	 */
	public static final String DIC_TYPE_CONTRACT_SHOU = "shou";

	/**
	 * 字典表-合同类型-付款
	 */
	public static final String DIC_TYPE_CONTRACT_FU = "fu";

	/**
	 * 页面方式-填加
	 */
	public static final String PAGE_HANDLE_METHOD_ADD = "add";
	/**
	 * 填加权限号
	 */
	public static final String PAGE_HANDLE_METHOD_ADD_NUM = "3";
	/**
	 * 页面方式-修改
	 */
	public static final String PAGE_HANDLE_METHOD_MODIFY = "modify";
	/**
	 * 修改权限号
	 */
	public static final String PAGE_HANDLE_METHOD_MODIFY_NUM = "4";
	/**
	 * 页面方式-列表显示
	 */
	public static final String PAGE_HANDLE_METHOD_LIST = "list";

	/**
	 * 列表权限号
	 */
	public static final String PAGE_HANDLE_METHOD_LIST_NUM = "1";

	/**
	 * 页面方式-页面显示
	 */
	public static final String PAGE_HANDLE_METHOD_VIEW = "view";

	/**
	 * 1增加权限1新建 2修改3提交审批4删除5检索6其他7改变状态8复制9打印
	 */
	public static final String RIGHTS_ADD = "1";

	/**
	 *  2修改
	 */
	public static final String RIGHTS_MODIFY = "2";
	/**
	 *  3提交审批
	 */
	public static final String RIGHTS_SUBMIT = "3";
	/**
	 *  4删除
	 */
	public static final String RIGHTS_DEL = "4";
	/**
	 *  5检索
	 */
	public static final String RIGHTS_SEARCH = "5";
	/**
	 *  6其他
	 */
	public static final String RIGHTS_ORTHER = "6";
	/**
	 *  7改变状态
	 */
	public static final String RIGHTS_CHANGE = "7";
	/**
	 *  8复制
	 */
	public static final String RIGHTS_COPY = "8";

	/**
	 *  9打印
	 */
	public static final String RIGHTS_PRINT = "9";

	/**
	 * 0状态失效
	 */
	public static final String DIC_STATE_NO = "0";
	/**
	 * 1状态生效(审批通过)
	 */
	public static final String DIC_STATE_YES = "1";

	/**
	 * 2状态 审批中
	 */
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS = "2";
	/**
	 * 1状态 审批通过
	 */
	public static final String DIC_STATE_WORKFLOW_YES_PROCESS = "1";
	/**
	 * 3状态 新建
	 */
	public static final String DIC_STATE_NEW = "3";

	public static final String DIC_STATE_NEW_NAME = com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME;
	/**
	 * 20状态 审批中，需编辑
	 */
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS_EDIT = "20";

	/**
	 * 0状态 
	 */
	public static final String DIC_STATE_WORKFLOW_NO_PROCESS = "0";

	/**
	 * 显示权限号
	 */
	public static final String PAGE_HANDLE_METHOD_VIEW_NUM = "2";

	//用于归类普通操作,在列表外,如填加,编辑,提交审批
	public static final String PAGE_HANDLE_COMMON_NUM = "0";
	//用于归类动作操作,在选择动作列表内,如生效，失效等
	public static final String PAGE_HANDLE_ACTION_NUM = "1";
	//用于归类检索操作,在选择检索列表内
	public static final String PAGE_HANDLE_SEARCH_NUM = "2";

	/**
	 * 页面方式-页面处理变量名
	 */
	public static final String PAGE_HANDLE_METHOD_NAME = "handlemethod";
	//耗材
	public static final String DIC_STORAGE_HAOCAI = "1";
	//实验材料
	public static final String DIC_STORAGE_SHIYANCAILIAO = "2";
	//设备
	public static final String DIC_STORAGE_YIQI = "3";
	//特殊动作类型 
	public static final String ACTION_RIGHTS_SPEC_TYPE = "1";
	//普通动作类型 
	public static final String ACTION_RIGHTS_COMMON_TYPE = "0";
	//供应商
	public static final String DIC_SUPPLIER_TYPE = "1";
	//客户
	public static final String DIC_CUSTOMER_TYPE = "0";
	/**
	 * 1器具（无批次）
	 */
	public static final String DIC_STORAGE_TYPE_HAOCAI_QIJU = "11";
	/**
	 * 2试剂（有批次）
	 */
	public static final String DIC_STORAGE_TYPE_HAOCAI_CGSJ = "12";

	/**
	 * 1（无批次）
	 */
	public static final String DIC_STORAGE_SHIYANCAILIAO_NPC = "11";
	/**
	 * 2试剂（有批次）
	 */
	public static final String DIC_STORAGE_SHIYANCAILIAO_YPC = "12";

	/**
	 * dicType表的类型为项目类型
	 */
	public static final String DIC_TYPE_XMLX = "xmlx";

	/**
	 * dicType表的类型为研究类型
	 */
	public static final String DIC_TYPE_YJLX = "yjlx";
	/**
	 * dicType表的类型为预算类型
	 */
	public static final String DIC_TYPE_YSLX = "yslx";
	/**
	 * dicType表的类型为合同类型
	 */
	public static final String DIC_TYPE_HTLX = "htlx";
	/**
	 * dicType表的类型为课题方向
	 */
	public static final String DIC_TYPE_KTFX = "ktfx";
	/**
	 * dicType表的类型为任务类型
	 */
	public static final String DIC_TYPE_RWLX = "rwlx";
	/**
	 * dicType表的类型为来源
	 */
	public static final String DIC_TYPE_SOURCE_TYPE_HAOCAI = "sourcehaocai";
	/**
	 * dicType采购单ID系统生成
	 */
	public static final String DIC_TYPE_PURCHASEORDER_AUTO = "zdsc";

	/**
	 * 事件提醒
	 */
	public static final String DIC_TYPE_REMIND = "1";
	/**
	 * 预警：库存不够，产生订单，预维护
	 */
	public static final String DIC_TYPE_PREREMIND = "2";
	//审批提示
	public static final String DIC_SYS_REMIND_SP = "3";
	//审批提示
	public static final String DIC_SYS_REMIND_DSP = "4";
	/**
	 * 检测事件提醒
	 */
	public static final String DIC_TYPE_REMIND_CHECK = "5";

	/**
	 * dicType字典中位置类型
	 */
	public static final String DIC_TYPE_LOCATION_POSITION = "location";

	/**
	 * 状态字典表中为耗材库存的
	 */
	public static final String DIC_STATE_TYPE_HAOCAIKUCUN = "haocaikucun";
	/**
	 * DicState表中代表存储位置的位置状态
	 */
	public static final String DIC_STATE_TYPE_STORAGE_POSITION = "storagePosition";

	/**
	 * 状表字典表id为1表示生效
	 */
	public static final String DIC_STATE_YES_ID = "1";
	/**
	 * 状态字典表ID为0的表示报废
	 */
	public static final String DIC_STATE_NO_ID = "0";

	//提示表未读
	public static final String DIC_SYS_REMIND_NO_LOOK = "0";

	/**
	* 库存位置是否占用
	* 1:是
	*/
	public static final String POSITION_USE_YES = "1";
	/**
	 * 库存位置是否占用
	 * 1:否
	 */
	public static final String POSITION_USE_NO = "0";

	/**
	 * 是否跳过发起人(1:跳过;0:不跳过)
	 */
	public static final String IS_SKIP_SPONSOR = getProperty("is_skip_sponsor", "0");

	//产前检测 相关常量设置

	/**
	 * 工作流动作-wk
	 */
	public static final String WORK_ACTION_ID_WK = "wk";
	/**
	 * 工作流动作-blood
	 */
	public static final String WORK_ACTION_ID_BLOOD = "blood";
	/**
	 * 工作流动作-dna
	 */
	public static final String WORK_ACTION_ID_DNA_TASK = "dnaTask";
	/**
	 * 工作流动作-plasmaTask
	 */
	public static final String WORK_ACTION_ID_PLASMA_TASK = "plasmaTask";
	/**
	 * 工作流动作-qpcr
	 */
	public static final String WORK_ACTION_ID_QPCR = "qcQpcr";
	/**
	 * 工作流动作-pooling
	 */
	public static final String WORK_ACTION_ID_POOLING = "pooling";
	/**
	 * 工作流动作-2100
	 */
	public static final String WORK_ACTION_ID_2100 = "qc2100";

	/**
	 * 是否合格-是
	 */
	public static final String IS_GOOD_YES = "1";
	public static final String IS_GOOD_YES_NAME = "是";
	/**
	 * 是否合格-否
	 */
	public static final String IS_GOOD_NO = "0";
	public static final String IS_GOOD_NO_NAME = "否";

	/**
	 * 优先级-1
	 */
	public static final String ORDER_LEVEL_1 = "1";
	/**
	 * 优先级-2
	 */
	public static final String ORDER_LEVEL_2 = "2";
	/**
	 * 优先级-3
	 */
	public static final String ORDER_LEVEL_3 = "3";

	/**
	 * 血浆编码前缀
	 */
	public static final String PLASMA_CODE_PREFIX = "AP";

	/**
	 * QC角色ID
	 */
	public static final String USER_GROUP_ID_QC = "qc";

	/**
	 * 任务样本成功1
	 */
	public static final String TASK_SUCCESS = "1";
	/**
	 * 任务样本失败0
	 */
	public static final String TASK_FAILURE = "0";
	/**
	 * 任务样本处理方式的通过
	 */
	public static final String TASK_METHOD_PASS = "3";
	/**
	 * 任务样本处理方式的重开始（抽血\）
	 */
	public static final String TASK_METHOD_AGAIN = "4";

	/**
	 * 任务样本处理方式的重制备（血浆\）
	 */
	public static final String TASK_METHOD_AGAIN_PREPARATION = "5";

	/**
	 * 样本出库备注类型为重建库
	 */
	public static final String AGAIN_CREATE_LIB = "6";

	/**
	 * 样本出库备注类型为结束实验
	 */
	public static final String TASK_METHOD_END = "7";

	/**
	 * 0工作流状态-无效
	 */
	public static final String WORK_FLOW_INVALID = "0";
	/**
	 * 1工作流状态-完成
	 */
	public static final String WORK_FLOW_COMPLETE = "1";
	/**
	 * 工作流状态名称-完成
	 */
	public static final String WORK_FLOW_COMPLETE_NAME = com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME;
	/**
	 * 2工作流状态-审批中
	 */
	public static final String WORK_FLOW_APPROVALING = "2";

	/**
	 * 3工作流状态-新建
	 */
	public static final String WORK_FLOW_NEW = "3";
	public static final String WORK_FLOW_NEW_NAME = com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME;

	/**
	 * 21工作流状态已下达
	 */
	public static final String WORK_FLOW_ISSUED = "21";
	public static final String WORK_FLOW_ISSUED_NAME = "已下达";
	/**
	 * 22工作流状态已反馈
	 */
	public static final String WORK_FLOW_FEEDBACK = "22";
	public static final String WORK_FLOW_FEEDBACK_NAME = "已实验";

	public static final String WORK_FLOW_YINWU_NAME = "已引物实验";

	public static final String WORK_FLOW_YIDAI_NAME = "已一代实验";

	public static final String WORK_FLOW_YANZHENG_NAME = "已完成一代验证";

	public static final String WORK_FLOW_JIEGUO_NAME = "已填写结果";

	public static final String WORK_FLOW_REPORT_NAME = "已填写报告";

	public static final String WORK_FLOW_FEEDBACK_NAME2 = "已质检";
	public static final String WORK_FLOW_ADD_RESULT = "已填写结果";

	/**
	 * 样本接收-血浆
	 */
	public static final String RECEIVE_TYPE_PLASMA = "plasma";
	/**
	 * 样本接收-全血
	 */
	public static final String RECEIVE_TYPE_BLOOD = "blood";
	/**
	 * 样本接收-分类-无创产前DNA检测
	 */
	public static final String RECEIVE_TYPE = "5";
	/**
	 * 样本接收-后续工作全血领用
	 */
	public static final String RECEIVE_FOLLOW_UP_WORK_BLOOD_APPL = "2";
	/**
	 * 样本接收-后续工作血浆领用
	 */
	public static final String RECEIVE_FOLLOW_UP_WORK_PLASMA_APPL = "3";

	/**
	 * 接收血浆的全血
	 */
	public static final String STATE_BLOOD_PLASMA_RECEIVE = "100";
	public static final String STATE_BLOOD_PLASMA_RECEIVE_NAME = "接收";

	/**
	 * 已完成接收
	 */
	public static final String BLOOD_COMPLETE_NAME = "已完成样本接收";
	/**
	 * 已完成收款
	 */
	public static final String FEE_COMPLETE_NAME = "已完成收款";

	/**
	 * 全血已关联血浆
	 */
	public static final String STATE_BLOOD_PLASMA = "101";
	/**
	 * 全血已关联血浆名称
	 */
	public static final String STATE_BLOOD_PLASMA_NAME = "已完成病理切片";

	/**
	 * 全血等待制备血浆领用
	 */
	public static final String BLOOD_WAIT_APPLY = "102";
	public static final String BLOOD_WAIT_APPLY_NAME = "已入库";

	/**
	 * 全血等待制备血浆已领用
	 */
	public static final String BLOOD_APPLY = "103";
	public static final String BLOOD_APPLY_NAME = "已启动";

	/**
	 * 全血等待制备血浆领用出库/血浆等待制备DNA已出库
	 */
	public static final String BLOOD_APPLY_OUT = "104";
	public static final String BLOOD_APPLY_OUT_NAME = "已出库";

	/**
	 * 等待制备血浆
	 */
	public static final String BLOOD_WAIT_PLASMA = "105";
	public static final String BLOOD_WAIT_PLASMA_NAME = "等待样本制备";

	/**
	 * 待入库血浆
	 */
	public static final String PLASMA_WAIT_IN = "106";
	public static final String PLASMA_WAIT_IN_NAME = "待入库制备样本";
	/**
	 * 血浆工作流入库
	 */
	public static final String PLASMA_APPLY_FOR_IN = "107";
	public static final String PLASMA_APPLY_FOR_IN_NAME = "已归还入库";

	/**
	 * 血浆已入库
	 */
	public static final String PLASMA_IN = "108";
	public static final String PLASMA_IN_NAME = "制备样本已入库";

	/**
	 * 提取DNA血浆已领用
	 */
	public static final String PLASMA_DNA_APPL = "110";
	/**
	 * 提取DNA血浆已领用
	 */
	public static final String PLASMA_DNA_APPL_NAME = "已领用";
	/**
	 * 血浆待提取DNA
	 */
	public static final String PLASMA_WAIT_DNA = "111";
	/**
	 * 血浆待提取DNA
	 */
	public static final String PLASMA_WAIT_DNA_NAME = "待提取基因组DNA";
	/**
	 * DnaInfo 中无效样本
	 */
	public static final String DNA_IN_FO_NO = "0";
	public static final String DNA_IN_FO_NO_NAME = "接受明细中已入库";
	/**
	 * 待建库DNA
	 */
	public static final String DNA_WAIT_LIB = "112";
	public static final String DNA_WAIT_LIB_NAME = "待文库构建";

	/**
	 * 已完成DNA
	 */
	public static final String DNA_COMPLETE_NAME = "已完成基因组DNA";

	public static final String DNA_QC_COMPLETE_NAME = "已完成基因组DNA质检";

	public static final String DNA_QC_FAIL_NAME = "基因组DNA质检失败";

	public static final String DNA_QC_END_NAME = "基因组DNA终止";

	/**
	 * 血浆已关联DNA
	 */
	public static final String PLASMA_LINK_DNA = "113";
	public static final String PLASMA_LINK_DNA_NAME = "已关联DNA";
	/**
	 * 待QC检测
	 */
	public static final String WAIT_QC_CHECK = "114";
	public static final String WAIT_QC_CHECK_NAME = "待QC检测";
	/**
	 * DNA已关联文库
	 */
	public static final String DNA_LINK_WK = "115";
	public static final String DNA_LINK_WK_NAME = "DNA已关联文库";

	/**
	 * QC已接收文库
	 */
	public static final String QC_RECEIVE_WK = "117";
	public static final String QC_RECEIVE_WK_NAME = "已接收文库";

	/**
	 * 文库已完成2100检测
	 */

	public static final String WK_COMPLETE_NAME = "已完成文库构建";

	public static final String WK_QC_COMPLETE_NAME = "已完成文库质检";

	public static final String WK_QC_FAIL_NAME = "文库质检失败";

	public static final String WK_QC_END_NAME = "文库终止";

	/**
	 * 文库已完成2100检测
	 */
	public static final String WK_COMPLETE_2100 = "118";
	public static final String WK_COMPLETE_2100_NAME = "文库已完成2100检测";
	/**
	 * 文库已完成qpcr检测
	 */
	public static final String WK_COMPLETE_QPCR = "119";
	public static final String WK_COMPLETE_QPCR_NAME = "已完成QPCR";
	/**
	 * 文库已完成2100\QPCR检测
	 */
	public static final String WK_COMPLETE_2100_QPCR = "120";
	public static final String WK_COMPLETE_2100_QPCR_NAME = "已完成QPCR";

	/**
	 * POOLING文库已完成检测
	 */
	public static final String WK_POOLING_CHECK_COMPLETE = "121";
	public static final String WK_POOLING_CHECK_COMPLETE_NAME = "已完成Pooling检测";

	/**
	 * 文库已完成上机结果
	 */
	public static final String WK_POOLING_ROW_PIECE = "122";
	public static final String WK_POOLING_ROW_PIECE_NAME = "已完成上机前文库";

	/**
	 * 已完成数据质控
	 */
	public static final String WK_POOLING_QC = "123";
	public static final String WK_POOLING_QC_NAME = "已完成数据质控";

	/**
	 * 结果已反馈
	 */
	public static final String ON_RESULT_COMPLETE = "124";
	public static final String ON_RESULT_COMPLETE_NAME = "结果已反馈";

	/**
	 * 待发送报告
	 */
	public static final String WAIT_SEND_REPORT = "125";
	public static final String WAIT_SEND_REPORT_NAME = "待发送报告";
	/**
	 * QC已接收Pooling文库
	 */
	public static final String QC_RECEIVE_POOLING_WK = "126";
	public static final String QC_RECEIVE_POOLING_WK_NAME = "已接收Pooling文库";
	/**
	 * 待qPCR
	 */
	//	public static final String WAIT_CHECK_QPCR = "129";
	//	public static final String WAIT_CHECK_QPCR_NAME = "待qPCR";

	/**
	 * 结束实验
	 */
	public static final String END_EXPERIMENT = "127";
	public static final String END_EXPERIMENT_NAME = "不通过";
	public static final String END_EXP_NAME = "已提取";

	public static final String ERROE_RECEIVE = "128";
	public static final String ERROE_RECEIVE_NAME = "接收不合格";

	public static final String POOLING_COMPLETE_NAME = "已完成捕获";

	public static final String CROSS_COMPLETE_NAME = "已完成上机";

	/**
	 * 待富级文库
	 */
	public static final String WAIT_FJ_CHECK = "129";
	public static final String WAIT_FJ_CHECK_NAME = "待POOLING";

	/**
	 * 待上机前文库
	 */
	/**
	 * 待QPCR检测
	 */
	//	public static final String QPCR_WAIT_LIB = "130";
	//	public static final String QPCR_WAIT_LIB_NAME = "待建QPCR检测";

	public static final String DNA_SJQ_LIB = "130";
	public static final String DNA_WAIT_SJQ_NAME = "待上机前文库";
	public static final String WAIT_OUTSOURCE = "131";
	public static final String WAIT_OUTSOURCE_NAME = "待外包";
	public static final String WK_COMPLETE_PCR = "132";
	public static final String WK_COMPLETE_PCR_NAME = "已完成PCR";
	/**
	 * 1工作流状态-完成
	 */
	public static final String WORK_OUTSOURCING = "133";
	/**
	 * 工作流状态名称-完成
	 */
	public static final String WORK_OUTSOURCING_NAME = "外包中";
	public static final String SJQ_COMPLETE_NAME = "已完成Pooling";
	public static final String SJCX_COMPLETE_NAME = "已完成上机测序";
	public static final String XXFX_COMPLETE_NAME = "已完成信息分析";
	public static final String BG_COMPLETE_CODE = "166";
	public static final String BG_COMPLETE_NAME = "已出报告";
	public static final String FP_SEND_CODE = "168";
	public static final String FP_SEND_NAME = "已填写发票";
	public static final String BG_SEND_CODE = "167";
	public static final String BG_SEND_NAME = "已发送报告";
	public static final String WAIT_REPORT_CODE = "146";
	public static final String WAIT_REPORT_NAME = "待出报告";
	/**
	 * 待上机前文库
	 */

	public static final String WAIT_MLPA_CODE = "135";
	public static final String WAIT_MLPA_NAME = "MLPA";

	public static final String WAIT_MUTATION_CODE = "136";
	public static final String WAIT_MUTATION_NAME = "动态突变";

	public static final String WAIT_WK_CODE = "137";
	public static final String WAIT_WK_NAME = "已构建文库";

	public static final String FINISH_MUTATION_CODE = "138";
	public static final String FINISH_MUTATION_NAME = "已动态突变";

	public static final String WAIT_ZJ_CODE = "139";
	public static final String WAIT_ZJ_NAME = "待Pooling";

	public static final String WAIT_SJ_CODE = "140";
	public static final String WAIT_SJ_NAME = "待上机";

	public static final String QC_RECEIVE_CROSS_CODE = "141";
	public static final String QC_RECEIVE_CROSS_NAME = "已杂交";

	public static final String WAIT_SJ_CROSS_CODE = "142";
	public static final String WAIT_SJ_CROSS_NAME = "待Pooling";

	public static final String COMPLETE_SJ_CODE = "143";
	public static final String COMPLETE_SJ_NAME = "已完成上机";

	public static final String WAIT_RESEARCH_CODE = "144";
	public static final String WAIT_RESEARCH_NAME = "科研任务";

	public static final String COMPLETE_RESEARCH_CODE = "145";
	public static final String COMPLETE_RESEARCH_NAME = "已完成科研任务";
	public static final String COMPLETE_QT_NAME = "已完成其他任务";

	public static final String WAIT_REPORTS_CODE = "146";
	public static final String WAIT_REPORTS_NAME = "待出报告";

	public static final String READY_REPORTS_CODE = "165";

	public static final String WAIT_GENERATION_CODE = "147";
	public static final String WAIT_GENERATION_NAME = "一代测序任务";

	public static final String DNA_OUT_CODE = "148";
	public static final String DNA_OUT_NAME = "DNA以出库";

	public static final String COMPLETE_GENERATION_CODE = "149";
	public static final String COMPLETE_GENERATION_NAME = "已完成一代任务";

	public static final String COMPLETE_MLPA_CODE = "150";
	public static final String COMPLETE_MLPA_NAME = "已完成MLPA";

	public static final String WAIT_QC_CODE = "151";
	public static final String WAIT_QC_NAME = "待质检";

	public static final String WAIT_QC_AL_CODE = "156";
	public static final String WAIT_QC_AL_NAME = "已质检";

	public static final String WAIT_VERIFY_CODE = "152";
	public static final String WAIT_VERIFY_NAME = "等待家属验证";

	public static final String SAMPLE_NULLITY_CODE = "153";
	public static final String SAMPLE_NULLITY_NAME = "样本无效";

	public static final String SAMPLE_OUT_CODE = "154";
	public static final String SAMPLE_OUT_NAME = "主样本已启动";

	public static final String WAIT_ALALYZE = "155";
	public static final String WAIT_ALALYZE_NAME = "待分析";

	public static final String WAIT_JS_CODE = "157";
	public static final String WAIT_JS_NAME = "待技术分析";

	public static final String COMPLETE_JS_CODE = "159";
	public static final String COMPLETE_JS_NAME = "已完成技术分析";

	public static final String WAIT_YD_CODE = "158";
	public static final String WAIT_YD_NAME = "一代不通过";

	/**
	/**
	 * 出库类型-血浆
	 */
	public static final String OUT_TYPE_PLASMA = "plasma";
	/**
	 * 出库类型-DNA
	 */
	public static final String OUT_TYPE_BLOOD = "blood";

	/**
	 * 任务类型-血浆
	 */
	public static final String TASK_TYPE_PLASMA = "DoBlood";
	/**
	 * 任务类型-DNA
	 */
	public static final String TASK_TYPE_DNA = "DoDna";
	/**
	 * 任务类型-QPCR
	 */
	public static final String TASK_TYPE_QPCR = "DoQpcr";
	/**
	 * 任务类型-测序
	 */
	public static final String TASK_TYPE_GENERATION = "DoGeneration";
	/**
	 * 任务类型-文库
	 */
	public static final String TASK_TYPE_WK = "DoWk";

	/**
	 * INDEX号字典的ID
	 */
	public static final String DIC_INDEX_ID = "index";

	public static final String CERATE_TASK_EXECL_FILE_NAME = "wkTaskExcel.xls";
	public static final String NDJS_CERATE_TASK_EXECL_FILE_NAME = "ndjs.xls";
	public static final String CXJLD_CERATE_TASK_EXECL_FILE_NAME = "cxjld.xls";

	public static final String SAMPLE_REPORT_FILE_NAME = "report-1.pdf";

	public static final String FSZ1_1_CDOE = "1";//合同
	public static final String FSZ1_2_CDOE = "2";//付款
	public static final String FSZ1_3_CDOE = "3";//样本入库
	public static final String FSZ1_4_CDOE = "4";//病理切片
	public static final String FSZ1_5_CDOE = "5";//基因组DNA
	public static final String FSZ1_50_CDOE = "50";//基因组DNA质检
	public static final String FSZ1_6_CDOE = "6";//文库
	public static final String FSZ1_60_CDOE = "60";//文库质检
	public static final String FSZ1_7_CDOE = "7";//捕获
	public static final String FSZ1_70_CDOE = "70";//pooling质检
	public static final String FSZ1_8_CDOE = "8";//pooling
	public static final String FSZ1_80_CDOE = "80";//杂交质检
	public static final String FSZ1_9_CDOE = "9";//上机
	public static final String FSZ1_10_CDOE = "100";//生物信息分析
	public static final String FSZ1_101_CDOE = "101";//技术分析
	public static final String FSZ1_102_CDOE = "102";//一代
	public static final String FSZ1_11_CDOE = "110";//报告交付
	public static final String FSZ1_51_CDOE = "51";//QPCR
	public static final String FSZ1_52_CDOE = "52";//PCR
	public static final String FSZ1_53_CDOE = "53";//外包
	public static final String FSZ1_NAME = "泛生子1号";
	public static final String FSZ2_1_CDOE = "1";
	public static final String FSZ2_2_CDOE = "2";
	public static final String FSZ2_3_CDOE = "3";
	public static final String FSZ2_4_CDOE = "4";
	public static final String FSZ2_5_CDOE = "5";
	public static final String FSZ2_6_CDOE = "6";
	public static final String FSZ2_7_CDOE = "7";
	public static final String FSZ2_8_CDOE = "8";
	public static final String FSZ2_9_CDOE = "9";
	public static final String FSZ2_10_CDOE = "10";
	public static final String FSZ2_11_CDOE = "11";
	public static final String FSZ2_NAME = "泛生子2号";
	public static final String FSZ3_1_CDOE = "1";
	public static final String FSZ3_2_CDOE = "2";
	public static final String FSZ3_3_CDOE = "3";
	public static final String FSZ3_4_CDOE = "4";
	public static final String FSZ3_5_CDOE = "5";
	public static final String FSZ3_6_CDOE = "6";
	public static final String FSZ3_7_CDOE = "7";
	public static final String FSZ3_8_CDOE = "8";
	public static final String FSZ3_9_CDOE = "9";
	public static final String FSZ3_10_CDOE = "10";
	public static final String FSZ3_11_CDOE = "11";
	public static final String FSZ3_NAME = "泛生子3号";
	public static final String QPCR_1_CDOE = "1";
	public static final String QPCR_2_CDOE = "2";
	public static final String QPCR_3_CDOE = "3";
	public static final String QPCR_4_CDOE = "4";
	public static final String QPCR_5_CDOE = "5";
	public static final String QPCR_NAME = "qPCR";
	public static final String FISH_1_CDOE = "1";
	public static final String FISH_2_CDOE = "2";
	public static final String FISH_3_CDOE = "3";
	public static final String FISH_4_CDOE = "4";
	public static final String FISH_5_CDOE = "5";
	public static final String FISH_NAME = "FISH";
	public static final String JLS_1_CDOE = "1";
	public static final String JLS_2_CDOE = "2";
	public static final String JLS_3_CDOE = "3";
	public static final String JLS_4_CDOE = "4";
	public static final String JLS_5_CDOE = "5";
	public static final String JLS_NAME = "JLS";


}
