package com.biolims.system.finalProduct.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.dic.model.DicType;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.finalProduct.dao.FinalProductDao;
import com.biolims.system.finalProduct.model.FinalProduct;
import com.biolims.system.sys.model.SamplePackSystem;
import com.biolims.system.sys.model.SamplePackSystemItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FinalProductService {
	@Resource
	private FinalProductDao finalProductDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findProductList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return finalProductDao.selectProductList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	/**
	 * 获取第二级
	 */
	public Map<String, Object> findProductLevelList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return finalProductDao.selectProductLevelList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	/**
	 * 选择product最顶级
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> findParentProductList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return finalProductDao.selectParentProductList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FinalProduct i) throws Exception {

		finalProductDao.saveOrUpdate(i);

	}

	public FinalProduct get(String id) {
		FinalProduct product = commonDAO.get(FinalProduct.class, id);
		return product;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FinalProduct sc, Map jsonMap) throws Exception {
		if (sc != null) {
			finalProductDao.saveOrUpdate(sc);

			// 若项目定价为空则计算项目定价
			// 设置保留两位小数
			DecimalFormat df = new DecimalFormat("######0.00");
			if ("".equals(sc.getProductFee()) || sc.getProductFee() == null) {
				if (sc.getMoney() != null && !"".equals(sc.getMoney())
						&& sc.getDiscount() != null
						&& !"".equals(sc.getDiscount())) {
					Double b = Double.parseDouble(sc.getMoney())
							* Double.parseDouble(sc.getDiscount()) / 100;
					sc.setProductFee((df.format(b).toString()));
				}
			}
			String jsonStr = "";
		}
	}

	/**
	 * 获得子节点列表信息
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public List<FinalProduct> getChildList(List<FinalProduct> list, FinalProduct node)
			throws Exception { // 得到子节点列表
		List<FinalProduct> li = new ArrayList<FinalProduct>();
		Iterator<FinalProduct> it = list.iterator();
		while (it.hasNext()) {
			FinalProduct n = (FinalProduct) it.next();
			if (n.getParent() != null
					&& n.getParent().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<FinalProduct> list, FinalProduct node) throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public boolean hasChildCount(FinalProduct node) throws Exception {

		Long c = commonDAO.getCount("from FinalProduct where parent.id='"
				+ node.getId() + "'");
		return c > 0 ? true : false;
	}

	public String getJson(List<FinalProduct> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<FinalProduct> nodeList0 = new ArrayList<FinalProduct>();
		Iterator<FinalProduct> it1 = list.iterator();
		while (it1.hasNext()) {
			FinalProduct node = (FinalProduct) it1.next();
			// if (node.getLevel() == 0) {
			nodeList0.add(node);
			// }
		}
		Iterator<FinalProduct> it = nodeList0.iterator();
		while (it.hasNext()) {
			FinalProduct node = (FinalProduct) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	/**
	 * 利用treePanel复选框
	 * 
	 * @throws Exception
	 * 
	 */
	public String getTreeJson(List<FinalProduct> list) throws Exception {

		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<FinalProduct> nodeList0 = new ArrayList<FinalProduct>();
		Iterator<FinalProduct> it1 = list.iterator();
		while (it1.hasNext()) {
			FinalProduct node = (FinalProduct) it1.next();
			if (node.getParent() == null || node.getParent().equals("")) {
				nodeList0.add(node);
			}
		}
		Iterator<FinalProduct> it = nodeList0.iterator();
		while (it.hasNext()) {
			FinalProduct node = (FinalProduct) it.next();
			constrctorTreeJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}

	/**
	 * 
	 * 构建treeJson
	 * 
	 * @throws Exception
	 * 
	 */
	public void constrctorTreeJson(List<FinalProduct> list, FinalProduct treeNode)
			throws Exception {

		if (hasChild(list, treeNode)) {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");

			json.append(",\"leaf\":false");
			if (treeNode.getChecked() != null
					&& treeNode.getChecked().equals("1"))
				json.append(",\"checked\":true");
			else
				json.append(",\"checked\":false");
			json.append(",\"children\":[");
			List<FinalProduct> childList = getChildList(list, treeNode);
			Iterator<FinalProduct> iterator = childList.iterator();
			while (iterator.hasNext()) {
				FinalProduct node = (FinalProduct) iterator.next();
				constrctorTreeJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");
			json.append(",\"leaf\":true");
			if (treeNode.getChecked() != null
					&& treeNode.getChecked().equals("1"))
				json.append(",\"checked\":true");
			else
				json.append(",\"checked\":false");
			json.append("},");

		}

	}

	public String getTreeJsonDictype(List<DicType> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<DicType> nodeList0 = new ArrayList<DicType>();
		Iterator<DicType> it1 = list.iterator();
		while (it1.hasNext()) {
			DicType node = (DicType) it1.next();
			nodeList0.add(node);
		}
		Iterator<DicType> it = nodeList0.iterator();
		while (it.hasNext()) {
			DicType node = (DicType) it.next();
			constrctorTreeJson2(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}

	/**
	 * 
	 * 构建treeJson
	 * 
	 * @throws Exception
	 * 
	 */
	public void constrctorTreeJson2(List<DicType> list, DicType treeNode)
			throws Exception {

		json.append("{id:\"");
		json.append(treeNode.getId() + "\"");
		json.append(",text:\"");
		json.append(treeNode.getName() + "\"");
		json.append(",leaf:true");
		json.append(",checked:false");
		json.append("},");

	}

	/**
	 * 构建Json文件
	 * 
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<FinalProduct> list, FinalProduct treeNode)
			throws Exception {
		json.append("{");
		json.append("\"id\":'");
		json.append(JsonUtils.formatStr(treeNode.getId() == null ? ""
				: treeNode.getId()) + "");
		json.append("',");
		json.append("\"name\":'");
		json.append(JsonUtils.formatStr(treeNode.getName() == null ? ""
				: treeNode.getName()) + "");
		json.append("',");
		json.append("\"testTime\":'");
		json.append(JsonUtils.formatStr(treeNode.getTestTime() == null ? ""
				: treeNode.getTestTime()) + "");
		json.append("',");
		// json.append("\"workType\":'");
		// json.append(JsonUtils.formatStr(treeNode.getWorkType() == null ? "" :
		// treeNode.getWorkType().getName()) + "");
		// json.append("',");
		json.append("\"productFee\":'");
		json.append(JsonUtils.formatStr(treeNode.getProductFee() == null ? ""
				: treeNode.getProductFee()) + "");
		json.append("',");
		json.append("\"createUser\":'");
		json.append(JsonUtils.formatStr(treeNode.getCreateUser() == null ? ""
				: treeNode.getCreateUser().getName()) + "");
		json.append("',");
		/*
		 * json.append("\"createDate\":'");
		 * json.append(JsonUtils.formatStr((String) (treeNode.getCreateDate() ==
		 * null ? "" : treeNode.getCreateDate().getTime())) + "");
		 * json.append("',");
		 */
		json.append("\"state\":'");
		json.append(JsonUtils.formatStr(treeNode.getState() == null ? ""
				: treeNode.getState()) + "");
		json.append("',");
		json.append("\"stateName\":'");
		json.append(JsonUtils.formatStr(treeNode.getStateName() == null ? ""
				: treeNode.getStateName()) + "");
		json.append("',");
		json.append("\"personName\":'");
		json.append(JsonUtils.formatStr(treeNode.getPersonName() == null ? ""
				: treeNode.getPersonName()) + "");
		json.append("',");
		json.append("\"parent\":'");
		json.append(JsonUtils.formatStr(treeNode.getParent() == null ? ""
				: treeNode.getParent().getName()) + "");
		json.append("',");
		if (hasChildCount(treeNode)) {
			json.append("\"leaf\":false");
		} else {
			json.append("\"leaf\":true");
		}
		json.append(",\"upId\":'");
		json.append((treeNode.getParent() == null ? "" : treeNode.getParent()
				.getId()) + "");
		json.append("'},");

	}

	public List<FinalProduct> findProductList(String upId) throws Exception {
		List<FinalProduct> list = null;
		if (upId.equals("0") || upId.equals("")) {
			list = commonDAO
					.find("from FinalProduct where  (parent = '' or parent is null ) order by id asc");
		} else {

			list = commonDAO.find("from FinalProduct where parent.id='" + upId
					+ "' order by id asc");
		}
		return list;
	}

	public List<FinalProduct> findProductList() throws Exception {
		List<FinalProduct> list = commonDAO
				.find("from FinalProduct where state='1' order by id asc");
		return list;
	}

	/**
	 * 根据编号查询产品
	 */
	public List<Map<String, Object>> findProductToSample(String code)
			throws Exception {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = findProduct(code);
		List<FinalProduct> list = (List<FinalProduct>) result.get("list");

		if (list != null && list.size() > 0) {
			for (FinalProduct srai : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("testTime", srai.getTestTime());

				map.put("libraryType", srai.getLibraryType());
				map.put("methodology", srai.getMethodology());
				map.put("lciIndex", srai.getLciIndex());
				map.put("insertSize", srai.getInsertSize());
				map.put("targetYield", srai.getTargetYield());
				map.put("species", srai.getSpecies());
				map.put("lciGc", srai.getLciGc());
				map.put("notes", srai.getNotes());
				map.put("sequencePlatform", srai.getSequencePlatform());
				map.put("sequenceType", srai.getSequenceType());
				map.put("sequenceLength", srai.getSequenceLength());
				map.put("siGc", srai.getSiGc());
				map.put("siIndex", srai.getSiIndex());
				map.put("clientIndex", srai.getClientIndex());
				map.put("phix", srai.getPhix());
				map.put("exclusiveLane", srai.getExclusiveLane());
				map.put("exclusiveFlowcell", srai.getExclusiveFlowcell());
				map.put("qcRequirements", srai.getQcRequirements());
				map.put("deliveryMethod", srai.getDeliveryMethod());
				map.put("dataAnalysis", srai.getDataAnalysis());
				map.put("analysisDetails", srai.getAnalysisDetails());
				map.put("extraction", srai.getExtraction());
				map.put("qcProtocol", srai.getQcProtocol());
				/* 增加了产品里没有选择组的验证，防止报错 */
				if (srai.getAcceptUserGroup() != null) {
					map.put("acceptUserGroupId", srai.getAcceptUserGroup()
							.getId());
					map.put("acceptUserGroupName", srai.getAcceptUserGroup()
							.getName());
				} else {
					map.put("acceptUserGroupId", "");
					map.put("acceptUserGroupName", "");
				}

				map.put("sequencingApplication",
						srai.getSequencingApplication());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 通过标识码查找产品类型
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> findProductByMark(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = finalProductDao.findProductByMark(code
				.substring(0, 1));
		List<FinalProduct> list = (List<FinalProduct>) result.get("list");
		DicType bloodTube = finalProductDao.get(DicType.class, code.substring(2, 4));
		Map<String, Object> sampleTypeResult = finalProductDao
				.findDicTypeByMark(code.substring(1, 2));
		List<DicType> sampleTypeList = (List<DicType>) sampleTypeResult
				.get("list");
		if (list != null && list.size() > 0) {
			SamplePackSystem sps = new SamplePackSystem();
			sps = sampleInfoMainDao.selectSystem(list.get(0).getId(),
					sampleTypeList.get(0).getId(), bloodTube.getId());
			List<SamplePackSystemItem> list3 = sampleInfoMainDao.saveSystem(sps
					.getId());
			for (SamplePackSystemItem spsi : list3) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("mark", list.get(0).getId());
				map.put("id", spsi.getId());
				map.put("name", spsi.getName());
				if (spsi.getNum() != null)
					map.put("num", spsi.getNum().toString());
				else
					map.put("num", null);
				map.put("scope", spsi.getScope());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 通过Id查找订单区块
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> findOrderBlockById(String code) throws Exception {
		String[] codeArr = code.split(",");
		String orderBlock = " ";
		Map<String, Object> map = new HashMap<String, Object>();
		for (int i = 0; i < codeArr.length; i++) {
			map = finalProductDao.findOrderBlockById(codeArr[i]);
			List<FinalProduct> list = new ArrayList<FinalProduct>();
			list = (List<FinalProduct>) map.get("list");
			for (int j = 0; j < list.size(); j++) {
				if (list.get(j).getOrderBlock() != null) {
					orderBlock += list.get(j).getOrderBlockId() + ",";
				} else {
					orderBlock += ",";
				}
			}
		}
		Map<String, String> result = new HashMap<String, String>();
		result.put("result", orderBlock);
		return result;

	}

	public List<DicType> findDicTypeByType(String type) throws Exception {
		List<DicType> list = new ArrayList<DicType>();
		list = finalProductDao.findDicTypeByType(type);
		return list;

	}

	public Map<String, Object> findProduct(String code) throws Exception {
		String[] codes = code.split(",");

		List<FinalProduct> list = new ArrayList<FinalProduct>();
		for (int i = 0; i < codes.length; i++) {
			FinalProduct pro = commonDAO.get(FinalProduct.class, codes[i].trim());
			list.add(pro);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}
}
