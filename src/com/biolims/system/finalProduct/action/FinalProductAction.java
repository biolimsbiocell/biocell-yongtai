﻿package com.biolims.system.finalProduct.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.finalProduct.model.FinalProduct;
import com.biolims.system.finalProduct.service.FinalProductService;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/com/biolims/system/finalProduct")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FinalProductAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "900701";
	@Autowired
	private FinalProductService finalProductService;
	private FinalProduct finalProduct = new FinalProduct();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkOrderDao workOrderDao;

	@Action(value = "showProductList")
	public String showProductList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProduct.jsp");
	}

	@Action(value = "showProductListJson")
	public void showProductListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = finalProductService.findProductList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FinalProduct> list = (List<FinalProduct>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testTime", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("productFee", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testMethod-id", "");
		map.put("testMethod-name", "");
		map.put("mark", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("personName", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("money", "");
		map.put("discount", "");
		map.put("way", "");
		map.put("flux", "");

		map.put("libraryType", "");
		map.put("methodology", "");
		map.put("lciIndex", "");
		map.put("insertSize", "");
		map.put("targetYield", "");
		map.put("species", "");
		map.put("lciGc", "");
		map.put("notes", "");
		map.put("sequencePlatform", "");
		map.put("sequenceType", "");
		map.put("sequenceLength", "");
		map.put("siGc", "");
		map.put("siIndex", "");
		map.put("clientIndex", "");
		map.put("phix", "");
		map.put("exclusiveLane", "");
		map.put("exclusiveFlowcell", "");
		map.put("qcRequirements", "");
		map.put("deliveryMethod", "");
		map.put("dataAnalysis", "");
		map.put("analysisDetails", "");
		map.put("extraction", "");
		map.put("qcProtocol", "");
		map.put("sequencingApplication", "");
		map.put("acceptUserGroup-id", "");
		map.put("acceptUserGroup-name", "");
		map.put("sampleCodeNum", "");
		
		map.put("sampleNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "productSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogProductList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProductDialog.jsp");
	}

	@Action(value = "showDialogProductListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogProductListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = finalProductService.findProductList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FinalProduct> list = (List<FinalProduct>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testTime", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("productFee", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testMethod-id", "");
		map.put("testMethod-name", "");
		map.put("mark", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("personName", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("sampleNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editProduct")
	public String editProduct() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			finalProduct = finalProductService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "finalProduct");
		} else {
			// finalProduct.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			finalProduct.setCreateUser(user);
			finalProduct.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		// List<DicType> listReadLong = workOrderDao.getReadLongList();
		List<DicType> listPlatForm = workOrderDao.getPlatForm();
		List<DicType> listReadType = workOrderDao.getReadTypeList();
		List<DicType> listSequencingType = workOrderDao
				.getDicTypeList("sequencingtype");
		List<DicType> listLibraryType = workOrderDao
				.getDicTypeList("librarytype");
		List<DicType> listMethodology = workOrderDao
				.getDicTypeList("methodology");
		List<DicType> listLCIIndex = workOrderDao.getDicTypeList("lciindex");
		List<DicType> listIndex = workOrderDao.getDicTypeList("index");
		List<DicType> listSequencingCycle = workOrderDao
				.getDicTypeList("sequencingcycle");
		List<DicType> listClientIndex = workOrderDao
				.getDicTypeList("clientindex");
		// putObjToContext("listReadLong", listReadLong);
		putObjToContext("listPlatForm", listPlatForm);
		putObjToContext("listReadType", listReadType);
		putObjToContext("listSequencingType", listSequencingType);
		putObjToContext("listLibraryType", listLibraryType);
		putObjToContext("listMethodology", listMethodology);
		putObjToContext("listLCIIndex", listLCIIndex);
		putObjToContext("listIndex", listIndex);
		putObjToContext("listSequencingCycle", listSequencingCycle);
		putObjToContext("listClientIndex", listClientIndex);
		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProductEdit.jsp");
	}

	@Action(value = "copyProduct")
	public String copyProduct() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		finalProduct = finalProductService.get(id);
		finalProduct.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProductEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = finalProduct.getId();
		if (id != null && id.equals("")) {
			finalProduct.setId(null);
		}
		Map aMap = new HashMap();
		finalProductService.save(finalProduct, aMap);
		return redirect("/com/biolims/system/finalProduct/editProduct.action?id="
				+ finalProduct.getId());

	}

	@Action(value = "viewProduct")
	public String toViewProduct() throws Exception {
		String id = getParameterFromRequest("id");
		finalProduct = finalProductService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProductEdit.jsp");
	}

	// 配置表中关联业务类型
	@Action(value = "productSelectParent", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String finalProductSelectParent() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProductParentDialog.jsp");
	}

	@Action(value = "showDialogProductParentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogProductParentListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = finalProductService.findProductLevelList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FinalProduct> list = (List<FinalProduct>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testTime", "");
		map.put("productFee", "");
		map.put("testMethod-id", "");
		map.put("testMethod-name", "");
		map.put("mark", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("personName", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 访问 任务树
	 */
	@Action(value = "showProductTree", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProductTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/com/biolims/system/finalProduct/showProductTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/finalProductTree.jsp");
	}

	@Action(value = "showProductTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<FinalProduct> aList = null;
		/*
		 * if (upId.equals("0")) {
		 * 
		 * aList = finalFinalProductService.findProductList(); } else {
		 * 
		 * }
		 */
		aList = finalProductService.findProductList(upId);

		String a = finalProductService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showProductChildTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductChildTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<FinalProduct> aList = finalProductService.findProductList(upId);
		String a = finalProductService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showProductSelectTree", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProductSelectTree() throws Exception {

		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/com/biolims/system/finalProduct/showProductSelectTreeJson.action");

		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/showProductSelectTree.jsp");
	}

	@Action(value = "showProductSelectTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductSelectTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		String selectId = getParameterFromRequest("selectId");
		selectId = selectId + ",";
		List<FinalProduct> aList = null;
		if (upId.equals("0") || upId.equals("")) {

			aList = finalProductService.findProductList();
		} else {

		}
		for (FinalProduct p : aList) {

			if (selectId.contains(p.getId() + ",")) {
				p.setChecked("1");
			}
		}
		// aList = finalProductService.findProductList(upId);
		// List<FinalProduct> aList = finalProductService.findProductList();

		String a = finalProductService.getTreeJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/**
	 * 异步加载产品名称
	 * 
	 * @return
	 */
	@Action(value = "findProductToSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductToSample() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> dataListMap = this.finalProductService
					.findProductToSample(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 通过标识码查找产品类型
	 * 
	 * @return
	 */
	@Action(value = "findProductByMark", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductByMark() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.finalProductService
					.findProductByMark(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 通过id查找区块
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "findOrderBlockById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findOrderBlockById() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, String> dataListMap = this.finalProductService
					.findOrderBlockById(code);
			result.put("success", true);
			result.put("data", dataListMap.get("result"));

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showProductOrderBlockTree")
	public String showProductOrderBlockTree() {
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/com/biolims/system/finalProduct/showProductOrderBlockTreeJson.action");

		return dispatcher("/WEB-INF/page/com/biolims/system/finalProduct/showProductOrderBlockTree.jsp");
	}

	@Action(value = "showProductOrderBlockTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProductOrderBlockTreeJson() throws Exception {
		List<DicType> list = finalProductService.findDicTypeByType("orderBlock");
		String treeStr = finalProductService.getTreeJsonDictype(list);
		new SendData()
				.sendDataJson(treeStr, ServletActionContext.getResponse());

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FinalProductService getFinalProductService() {
		return finalProductService;
	}

	public void setFinalProductService(FinalProductService finalProductService) {
		this.finalProductService = finalProductService;
	}

	public FinalProduct getFinalProduct() {
		return finalProduct;
	}

	public void setFinalProduct(FinalProduct finalProduct) {
		this.finalProduct = finalProduct;
	}

}
