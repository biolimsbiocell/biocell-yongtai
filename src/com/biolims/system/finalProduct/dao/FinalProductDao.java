package com.biolims.system.finalProduct.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.finalProduct.model.FinalProduct;

@Repository
@SuppressWarnings("unchecked")
public class FinalProductDao extends BaseHibernateDao {
	public Map<String, Object> selectProductList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FinalProduct where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state ='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FinalProduct> list = new ArrayList<FinalProduct>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		
		
		for(int i=0;i<list.size();i++){
			String id = list.get(i).getId();
			String num = selSampleCodeNum(id);
			list.get(i).setSampleCodeNum(num);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	
	//查询产品关联样本数量
	public String selSampleCodeNum(String id) {
		String hql = "from SampleInfo t where 1=1 and t.productId='"+id+"'";
		String num = this.getSession().createQuery("select count(*) "+hql).uniqueResult().toString();
		return num;
	}
	
	
	/**
	 * 获取第二级
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectProductLevelList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FinalProduct where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and parent is not null ";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FinalProduct> list = new ArrayList<FinalProduct>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 选择product顶级
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectParentProductList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FinalProduct where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and (parent = '' or parent is null ) ";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FinalProduct> list = new ArrayList<FinalProduct>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 通过标识码查找product
	 */
	public Map<String, Object> findProductByMark(String code) throws Exception {
		String key = " ";
		String hql = " from FinalProduct a where a.mark ='" + code
				+ "' and a.state = '1'";
		List<FinalProduct> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	/**
	 * 通过标识码查找product
	 */
	public Map<String, Object> findDicTypeByMark(String code) throws Exception {
		String key = " ";
		String hql = " from DicType a where a.sysCode ='" + code + "'";
		List<FinalProduct> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	/**
	 * 通过Id查找订单区块
	 */
	public Map<String, Object> findOrderBlockById(String code) throws Exception {
		String key = " ";
		String hql = " from FinalProduct a where a.id ='" + code + "'";
		List<FinalProduct> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;

	}

	/**
	 * 查找订单区块
	 */
	public List<DicType> findDicTypeByType(String type) {
		String hql = " from DicType where 1=1 and type='" + type + "'";//
		String key = "";

		key = key + " order by orderNumber ASC";
		List<DicType> list = new ArrayList<DicType>();
		list = this.getSession().createQuery(hql + key).list();
		return list;

	}

	// 根据编号查询数据
	public FinalProduct setProductList(String id) {
		String hql = "from FinalProduct where 1=1 and product.id='" + id + "'";
		FinalProduct p = (FinalProduct) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}
}