package com.biolims.system.finalProduct.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 终产品（父级）
 * @author lims-platform
 * @date 2015-11-23 14:14:57
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_FINAl_PRODUCT")
@SuppressWarnings("serial")
public class FinalProduct extends EntityDao<FinalProduct> implements java.io.Serializable {
	/** 编码 */
	private String id;
	/** 名称 */
	private String name;
	/** 检测周期 */
	private String testTime;
	/** 标识码 */
	private String mark;
	/** 业务类型 */
	private WorkType workType;
	/** 项目定价 */
	private String productFee;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 检测方法 */
	private DicType testMethod;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 负责人 */
	private String personName;
	/** 父级 */
	private FinalProduct parent;
	/** 报价 */
	private String money;
	/** 扣率 */
	private String discount;
	// 结款方式
	private String way;

	private String checked;//
	// 通量
	private String flux;
	
	//用量
	private String sampleNum;
	//终产品样本类型
	private DicSampleType dicSampleType;
	//代次
	private String pronoun;
	
	
	
	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}
	
	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	// Library Type
	private String libraryType;
	// Methodology
	private String methodology;
	// Index
	private String lciIndex;
	// Insert Size
	private String insertSize;
	// Target Yield
	private String targetYield;
	/** 物种类型 */
	private String species;
	// GC
	private String lciGc;
	// Notes
	private String notes;
	/** 测序平台 */
	private String sequencePlatform;
	/** 测序方式 */
	private String sequenceType;
	/** 测序读长 */
	private String sequenceLength;
	// GC
	private String siGc;
	// Index
	private String siIndex;
	// Client Index
	private String clientIndex;
	// phix
	private String phix;
	// Exclusive Lane
	private String exclusiveLane;
	// Exclusive Flowcell
	private String exclusiveFlowcell;
	// QC Requirements
	private String qcRequirements;
	// Delivery Method
	private String deliveryMethod;
	// Data Analysis
	private String dataAnalysis;
	// Analysis Details 分析细节
	private String analysisDetails;

	// Extraction
	private String extraction;
	// QC Protocol
	private String qcProtocol;
	// Sequencing Application
	private String sequencingApplication;
	// 订单区块
	private String orderBlockId;
	private String orderBlock;
	
	//关联样本数量
	private String sampleCodeNum;
	

	public String getSampleCodeNum() {
		return sampleCodeNum;
	}

	public void setSampleCodeNum(String sampleCodeNum) {
		this.sampleCodeNum = sampleCodeNum;
	}

	public String getOrderBlockId() {
		return orderBlockId;
	}

	public void setOrderBlockId(String orderBlockId) {
		this.orderBlockId = orderBlockId;
	}

	public String getOrderBlock() {
		return orderBlock;
	}

	public void setOrderBlock(String orderBlock) {
		this.orderBlock = orderBlock;
	}

	/** 实验组 */
	private UserGroup acceptUserGroup;

	public String getLibraryType() {
		return libraryType;
	}

	public void setLibraryType(String libraryType) {
		this.libraryType = libraryType;
	}

	public String getMethodology() {
		return methodology;
	}

	public void setMethodology(String methodology) {
		this.methodology = methodology;
	}

	public String getLciIndex() {
		return lciIndex;
	}

	public void setLciIndex(String lciIndex) {
		this.lciIndex = lciIndex;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getTargetYield() {
		return targetYield;
	}

	public void setTargetYield(String targetYield) {
		this.targetYield = targetYield;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getLciGc() {
		return lciGc;
	}

	public void setLciGc(String lciGc) {
		this.lciGc = lciGc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getSequencePlatform() {
		return sequencePlatform;
	}

	public void setSequencePlatform(String sequencePlatform) {
		this.sequencePlatform = sequencePlatform;
	}

	public String getSequenceType() {
		return sequenceType;
	}

	public void setSequenceType(String sequenceType) {
		this.sequenceType = sequenceType;
	}

	public String getSequenceLength() {
		return sequenceLength;
	}

	public void setSequenceLength(String sequenceLength) {
		this.sequenceLength = sequenceLength;
	}

	public String getSiGc() {
		return siGc;
	}

	public void setSiGc(String siGc) {
		this.siGc = siGc;
	}

	public String getSiIndex() {
		return siIndex;
	}

	public void setSiIndex(String siIndex) {
		this.siIndex = siIndex;
	}

	public String getClientIndex() {
		return clientIndex;
	}

	public void setClientIndex(String clientIndex) {
		this.clientIndex = clientIndex;
	}

	public String getPhix() {
		return phix;
	}

	public void setPhix(String phix) {
		this.phix = phix;
	}

	public String getExclusiveLane() {
		return exclusiveLane;
	}

	public void setExclusiveLane(String exclusiveLane) {
		this.exclusiveLane = exclusiveLane;
	}

	public String getExclusiveFlowcell() {
		return exclusiveFlowcell;
	}

	public void setExclusiveFlowcell(String exclusiveFlowcell) {
		this.exclusiveFlowcell = exclusiveFlowcell;
	}

	public String getQcRequirements() {
		return qcRequirements;
	}

	public void setQcRequirements(String qcRequirements) {
		this.qcRequirements = qcRequirements;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getDataAnalysis() {
		return dataAnalysis;
	}

	public void setDataAnalysis(String dataAnalysis) {
		this.dataAnalysis = dataAnalysis;
	}

	public String getAnalysisDetails() {
		return analysisDetails;
	}

	public void setAnalysisDetails(String analysisDetails) {
		this.analysisDetails = analysisDetails;
	}

	public String getExtraction() {
		return extraction;
	}

	public void setExtraction(String extraction) {
		this.extraction = extraction;
	}

	public String getQcProtocol() {
		return qcProtocol;
	}

	public void setQcProtocol(String qcProtocol) {
		this.qcProtocol = qcProtocol;
	}

	public String getSequencingApplication() {
		return sequencingApplication;
	}

	public void setSequencingApplication(String sequencingApplication) {
		this.sequencingApplication = sequencingApplication;
	}

	public String getFlux() {
		return flux;
	}

	public void setFlux(String flux) {
		this.flux = flux;
	}

	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 36)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测周期
	 */
	@Column(name = "TEST_TIME", length = 36)
	public String getTestTime() {
		return this.testTime;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测周期
	 */
	public void setTestTime(String testTime) {
		this.testTime = testTime;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 项目定价
	 */
	@Column(name = "PRODUCT_FEE", length = 36)
	public String getProductFee() {
		return this.productFee;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 项目定价
	 */
	public void setProductFee(String productFee) {
		this.productFee = productFee;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 36)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 36)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 负责人
	 */
	@Column(name = "PERSON_NAME", length = 36)
	public String getPersonName() {
		return this.personName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 负责人
	 */
	public void setPersonName(String personName) {
		this.personName = personName;
	}

	/**
	 * 方法: 取得Product
	 * 
	 * @return: Product 父级
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "PARENT")
	public FinalProduct getParent() {
		return this.parent;
	}

	/**
	 * 方法: 设置Product
	 * 
	 * @param: Product 父级
	 */
	public void setParent(FinalProduct parent) {
		this.parent = parent;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "TEST_METHOD")
	public DicType getTestMethod() {
		return testMethod;
	}

	public void setTestMethod(DicType testMethod) {
		this.testMethod = testMethod;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "WORK_TYPE")
	public WorkType getWorkType() {
		return workType;
	}

	public void setWorkType(WorkType workType) {
		this.workType = workType;
	}

	@Column(name = "MARK", length = 36)
	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	@Transient
	public String getChecked() {
		return checked;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "ACCEPT_USER_GROUP")
	public UserGroup getAcceptUserGroup() {
		return acceptUserGroup;
	}

	public void setAcceptUserGroup(UserGroup acceptUserGroup) {
		this.acceptUserGroup = acceptUserGroup;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

}