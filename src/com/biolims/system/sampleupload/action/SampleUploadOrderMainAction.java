package com.biolims.system.sampleupload.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleUploadOrder;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleUploadOrderService;
import com.biolims.system.sampleupload.service.SampleUploadOrderMainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/sample/sampleUploadOrder")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleUploadOrderMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9809";
	@Autowired
	private SampleUploadOrderService sampleUploadOrderService;
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	private SampleUploadOrderMainService sampleUploadOrderMainService;
	private SampleOrder sampleOrder = new SampleOrder();
	// 注入癌症信息的service
	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private SampleCancerTempService sampleCancerTempService;
	private SampleCancerTemp sampleCancerTempOne = new SampleCancerTemp();// 一录
	private SampleCancerTemp sampleCancerTempTwo = new SampleCancerTemp();// 二录
	// 注入电子病历的service
	@Autowired
	private CrmPatientService crmPatientService;

	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showSampleUploadOrderList")
	public String showSampleUploadOrderList() throws Exception {

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleUploadOrderMain.jsp");
	}
	//查询订单
	@Action(value = "showSampleOrderListJson")
	public void showSampleOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleUploadOrderService.findSampleOrderList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleUploadOrder> list = (List<SampleUploadOrder>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("birthDate", "yyyy-MM-dd");
		map.put("diagnosisDate", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleStage", "");
		map.put("inspectionDepartment-id", "");
		map.put("inspectionDepartment-name", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("samplingDate", "yyyy-MM-dd");
		map.put("samplingLocation-id", "");
		map.put("samplingLocation-name", "");
		map.put("samplingNumber", "");
		map.put("pathologyConfirmed", "");
		map.put("bloodSampleDate", "yyyy-MM-dd");
		map.put("plasmapheresisDate", "yyyy-MM-dd");
		map.put("commissioner-id", "");
		map.put("commissioner-name", "");
		map.put("receivedDate", "yyyy-MM-dd");
		map.put("sampleTypeId", "");
		map.put("sampleTypeName", "");
		map.put("sampleCode", "");
		map.put("medicalNumber", "");
		map.put("familyCode", "");
		map.put("family", "");
		map.put("familyPhone", "");
		map.put("familySite", "");
		map.put("medicalInstitutions", "");
		map.put("medicalInstitutionsPhone", "");
		map.put("medicalInstitutionsSite", "");
		map.put("attendingDoctor", "");
		map.put("attendingDoctorPhone", "");
		map.put("attendingDoctorSite", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("bedNo", "");
		map.put("hospitalPatientID", "");
		map.put("age", "");
		map.put("diagnosis", "");
	
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	// 保存订单
	@Action(value = "saveUploadOrderUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveUploadOrderUpload() throws Exception {
		String itemDataJson = getParameterFromRequest("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//判断批量上传的订单编号是否与原有订单重复
			result = sampleUploadOrderService.selSampleOrder(itemDataJson);
			if(!"1".equals(result.get("flag"))){
				sampleUploadOrderService.saveOrderUpload(itemDataJson);
			}
			result.put("success", true);
		} catch (Exception e) {
			
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 删除订单信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleUploadInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleUploadInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids");
			
			sampleUploadOrderService.delSampleInItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 异步加载代理商名称
	 * 
	 * @return
	 */
	@Action(value = "findPrimaryToSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findPrimaryToSample() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> dataListMap = this.sampleUploadOrderMainService
					.findPrimaryToSample(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "sampleOrderSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainDialog.jsp");
	}

	@Action(value = "showDialogSampleOrderListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleUploadOrderService.findSampleOrderList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("birthDate", "yyyy-MM-dd");
		map.put("diagnosisDate", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleStage", "");
		map.put("inspectionDepartment-id", "");
		map.put("inspectionDepartment-name", "");
		map.put("crmProduct-id", "");
		map.put("crmProduct-name", "");
		map.put("samplingDate", "yyyy-MM-dd");
		map.put("samplingLocation-id", "");
		map.put("samplingLocation-name", "");
		map.put("samplingNumber", "");
		map.put("pathologyConfirmed", "");
		map.put("bloodSampleDate", "yyyy-MM-dd");
		map.put("plasmapheresisDate", "yyyy-MM-dd");
		map.put("commissioner-id", "");
		map.put("commissioner-name", "");
		map.put("receivedDate", "yyyy-MM-dd");
		map.put("sampleTypeId", "");
		map.put("sampleTypeName", "");
		map.put("sampleCode", "");
		map.put("medicalNumber", "");
		map.put("familyCode", "");
		map.put("family", "");
		map.put("familyPhone", "");
		map.put("familySite", "");
		map.put("medicalInstitutions", "");
		map.put("medicalInstitutionsPhone", "");
		map.put("medicalInstitutionsSite", "");
		map.put("attendingDoctor", "");
		map.put("attendingDoctorPhone", "");
		map.put("attendingDoctorSite", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("sampleFlag", "");
		map.put("successFlag", "");
		map.put("age", "");

		map.put("barcode", "");
		map.put("subjectID", "");
		map.put("visit", "");
		map.put("CRCName", "");
		map.put("CRCPhone", "");
		map.put("CRCEmail", "");
		map.put("CRAName", "");
		map.put("CRAPhone", "");
		map.put("CRAEmail", "");
		map.put("sponsor", "");
		map.put("sponsorProjectID", "");
		map.put("specimenPurpose", "");
		map.put("blockID", "");
		map.put("collectionTime", "yyyy-MM-dd");
		map.put("sectionTime", "yyyy-MM-dd");
		map.put("tissueType", "");
		map.put("siteCollection", "");
		map.put("collectionMethod", "");
		map.put("fixedBuffer", "");
		map.put("histologyType", "");
		map.put("bloodCollectionTime", "yyyy-MM-dd");

		map.put("ill", "");
		map.put("transfusion", "");
		map.put("sameTime", "");
		map.put("nativePlace", "");
		map.put("subjectEmail", "");
		map.put("subjectPhone", "");
		map.put("adopted", "");
		map.put("summary", "");
		map.put("phenotype", "");
		map.put("familyHistory", "");
		map.put("familyHistorysummary", "");
		map.put("prenatal", "");
		map.put("Institute", "");
		map.put("hospitalPatientID", "");
		map.put("customerAdress", "");
		map.put("physicianName", "");
		map.put("physicianPhone", "");
		map.put("physicianFax", "");
		map.put("physicianEmail", "");
		map.put("type", "");
		
		map.put("bedNo", "");
		map.put("diagnosis", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	@Action(value = "showHistorySelectTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showHistorySelectTreeJson() throws Exception {
		List<DicType> listCheck1 = sampleCancerTempService.findDicSampleType1(null);

		String a = sampleCancerTempService.getTreeJson(listCheck1);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}
	
	@Action(value = "showHistorySelectTree")
	public String showHistorySelectTree() throws Exception{
		
		
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/system/sample/sampleOrder/showHistorySelectTreeJson.action");
		
		return dispatcher("/WEB-INF/page/system/sample/sampleHistorySelectTree.jsp");
	}
	
	@Action(value = "showYongYaoSelectTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showYongYaoSelectTreeJson() throws Exception {
		List<DicType> listCheck2 = sampleCancerTempService.findDicSampleType2(null);

		String a = sampleCancerTempService.getTreeJson(listCheck2);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}
	
	@Action(value = "showYongYaoSelectTree")
	public String showYongYaoSelectTree(){
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/system/sample/sampleOrder/showYongYaoSelectTreeJson.action");
		
		return dispatcher("/WEB-INF/page/system/sample/sampleYongYaoSelectTree.jsp");
	}
	
	@Action(value = "showzhengzhuangSelectTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showzhengzhuangSelectTreeJson() throws Exception {
		List<DicType> listCheck3 = sampleCancerTempService.findDicSampleType3(null);

		String a = sampleCancerTempService.getTreeJson(listCheck3);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}
	
	@Action(value = "showzhengzhuangSelectTree")
	public String showzhengzhuangSelectTree(){
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/system/sample/sampleOrder/showzhengzhuangSelectTreeJson.action");
		
		return dispatcher("/WEB-INF/page/system/sample/samplezhengzhuangSelectTree.jsp");
	}

	@Action(value = "editSampleOrder")
	public String editSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		long num = 0;
		if (id != null && !id.equals("")) {
			this.sampleOrder = sampleUploadOrderService.get(id);
			if (sampleOrder == null) {
				sampleOrder = new SampleOrder();
				sampleOrder.setId(id);
				User user = (User) this
						.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				sampleOrder.setCreateUser(user);
				sampleOrder.setCreateDate(new Date());
				sampleOrder.setReceivedDate(new Date());
				sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "sampleOrder");
			}

		} else {
			sampleOrder.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleOrder.setCreateUser(user);
			sampleOrder.setCreateDate(new Date());
			sampleOrder.setReceivedDate(new Date());
			sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		// 复选框之全血样本
		List<DicSampleType> listCheck = sampleCancerTempService
				.findDicSampleType(null);
		
		
		List<DicType> listCheck3 = sampleCancerTempService
				.findDicSampleType3(null);
		putObjToContext("dicSampleTypeListByOrder", listCheck);
		putObjToContext("dicSampleTypeListByOrder3", listCheck3);
		putObjToContext("fileNum", num);
		putObjToContext("type", type);
		toState(sampleOrder.getState());
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	}

	@Action(value = "copySampleOrder")
	public String copySampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleOrder = sampleUploadOrderService.get(id);
		sampleOrder.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderEdit.jsp");
	}

	/**
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleOrder.getId();
		String number = sampleOrder.getMedicalNumber();

		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleOrder";
			String markCode = "DD";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName,
					markCode, stime, 000000, 6, null);
			sampleOrder.setId(autoID);
		}
		boolean result = crmPatientService.queryByCount(number);
		CrmPatient r = null;
		if (!"".equals(sampleOrder.getMedicalNumber())) {
			r = crmPatientService.get(sampleOrder.getMedicalNumber());
			if (r == null)
				r = new CrmPatient();
			// 该电子病历记录存在: 修改电子病历

			r.setId(sampleOrder.getMedicalNumber());
			r.setName(sampleOrder.getName());
			r.setGender(sampleOrder.getGender());
			r.setDateOfBirth(sampleOrder.getBirthDate());
			r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
			r.setCreateDate(sampleOrder.getCreateDate());
			r.setCreateUser(sampleOrder.getCreateUser());

			crmPatientService.save(r);

		} else {
			// 该电子记录不存在:添加电子病历
			String modelName = "CrmPatient";
			String markCode = "P";
			String autoID = codingRuleService.genTransID(modelName, markCode);

			sampleOrder.setMedicalNumber(autoID);
			r = new CrmPatient();
			r.setId(autoID);
			r.setName(sampleOrder.getName());
			r.setGender(sampleOrder.getGender());
			r.setDateOfBirth(sampleOrder.getBirthDate());
			r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
			r.setCreateDate(sampleOrder.getCreateDate());
			r.setCreateUser(sampleOrder.getCreateUser());
			crmPatientService.save(r);
		}

		Map aMap = new HashMap();
		// 保存病史和用药信息
		// aMap.put("sampleOrderPersonnel",
		// getParameterFromRequest("sampleOrderPersonnelJson"));
		// aMap.put("sampleOrderItem",
		// getParameterFromRequest("sampleOrderItemJson"));
		sampleUploadOrderService.save(sampleOrder, aMap);
		// 保存订单子表的关联信息
		// List<SampleOrderItem> listSampleOrderItem =
		// this.sampleUploadOrderService.querySampleItem(sampleOrder.getId());
		// List<SampleOrderPersonnel>listSampleOrderPersonnel
		// =this.sampleUploadOrderService.querySamplePersonnel(sampleOrder.getId());
		// if(listSampleOrderItem.size()>0){
		// for(SampleOrderItem order:listSampleOrderItem) {
		// this.crmPatientService.saveCrmPatientItem2(order,r);
		// }
		// }
		// if(listSampleOrderPersonnel.size()>0){
		// for(SampleOrderPersonnel sol:listSampleOrderPersonnel) {
		// this.crmPatientService.saveCrmPatientPersonnel2(sol,r);
		// }
		// }

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/system/sample/sampleOrder/editSampleOrder.action?id="
				+ sampleOrder.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

		// return
		// redirect("/system/sample/sampleOrder/editSampleOrder.action?id=" +
		// sampleOrder.getId());

	}

	@Action(value = "viewSampleOrder")
	public String toViewSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOrder = sampleUploadOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	}

	/**
	 * 
	 * 子表的相关方法
	 * 
	 * @return
	 */
	@Action(value = "showSampleOrderPersonnelList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOrderPersonnelList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainPersonnel.jsp");
	}

	@Action(value = "showSampleOrderPersonnelListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOrderPersonnelListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleUploadOrderService
					.findSampleOrderPersonnelList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<SampleOrderPersonnel> list = (List<SampleOrderPersonnel>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("checkOutTheAge", "");
			map.put("sampleOrder-name", "");
			map.put("sampleOrder-id", "");
			map.put("tumorCategory-name", "");
			map.put("tumorCategory-id", "");
			map.put("familyRelation", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOrderPersonnel")
	public void delSampleOrderPersonnel() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleUploadOrderService.delSampleOrderPersonnel(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSampleOrderItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOrderItemList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainItem.jsp");
	}

	@Action(value = "showSampleOrderItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOrderItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleUploadOrderService
					.findSampleOrderItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleOrderItem> list = (List<SampleOrderItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("drugDate", "yyyy-MM-dd");
			map.put("useDrugName", "");
			map.put("effectOfProgress", "");
			map.put("effectOfProgressSpeed", "");
			map.put("geneticTestHistory", "");
			map.put("sampleDetectionName", "");
			map.put("sampleExonRegion", "");
			map.put("sampleDetectionResult", "");
			map.put("sampleOrder-name", "");
			map.put("sampleOrder-id", "");
			map.put("sampleCode", "");
			map.put("sampleType-id", "");
			map.put("sampleType-name", "");
			map.put("family", "");
			map.put("hxfx", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("note", "");
			
			map.put("samplingDate", "yyyy-MM-dd");
			map.put("receiveDate", "yyyy-MM-dd");
			map.put("receiveUser-id", "");
			map.put("receiveUser-name", "");
			map.put("slideCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showSampleOrderInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOrderInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderInfoMain.jsp");
	}

	@Action(value = "showSampleOrderInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOrderInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleUploadOrderService
					.findSampleOrderInfoList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("patientName", "");
			map.put("note", "");
			map.put("orderNum", "");
			map.put("patientId", "");
			map.put("sampleOrder", "");

			map.put("location", "");
			map.put("dicType-id", "");
			map.put("dicType-name", "");

			map.put("sampleStage", "");
			map.put("project-id", "");
			map.put("project-name", "");
			map.put("personShip-id", "");
			map.put("personShip-name", "");

			map.put("idCard", "");
			map.put("businessType", "");
			map.put("price", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("upLoadAccessory-id", "");
			map.put("upLoadAccessory-fileName", "");
			// 产前
			map.put("receiveDate", "");
			map.put("si-area", "");
			map.put("hospital", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("si-doctor", "");
			map.put("si-inHosNum", "");

			map.put("sampleType-id", "");
			map.put("sampleType-name", "");

			map.put("age", "");
			map.put("weight", "");
			map.put("si-gestationalAge", "");
			map.put("si-voucherType-id", "");
			map.put("si-voucherType-name", "");
			map.put("si-voucherCode", "");
			map.put("phone", "");
			map.put("familyAddress", "");
			map.put("si-endMenstruationDate", "yyyy-MM-dd");
			map.put("si-gestationIVF", "");
			map.put("si-pregnancyTime", "");
			map.put("si-parturitionTime", "");
			map.put("si-badMotherhood", "");
			map.put("si-organGrafting", "");
			map.put("si-outTransfusion", "");
			map.put("si-firstTransfusionDate", "yyyy-MM-dd");
			map.put("si-stemCellsCure", "");
			map.put("si-immuneCure", "");
			map.put("si-endImmuneCureDate", "yyyy-MM-dd");
			map.put("si-embryoType", "");
			map.put("si-NT", "");
			map.put("si-reason", "");
			map.put("si-testPattern", "");
			map.put("si-trisome21Value", "");
			map.put("si-trisome18Value", "");
			map.put("si-coupleChromosome", "");
			map.put("si-reason2", "");
			map.put("si-diagnosis", "");
			map.put("si-medicalHistory", "");
			map.put("si-isInsure", "");
			map.put("si-isFee", "");
			map.put("si-privilegeType", "");
			map.put("si-linkman-id", "");
			map.put("si-linkman-name", "");
			map.put("si-isInvoice", "");
			map.put("si-paymentUnit", "");
			map.put("createUser1", "");
			map.put("createUser2", "");
			map.put("si-suppleAgreement", "");
			map.put("si-money", "");
			map.put("si-receiptType-id", "");
			map.put("si-receiptType-name", "");
			map.put("sampleNum", "");
			map.put("sampleType2", "");
			map.put("samplingDate", "yyyy-MM-dd");
			map.put("unit", "");
			map.put("sampleNote", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOrderItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleUploadOrderService.delSampleOrderItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 根据主数据加载子表明细
	@Action(value = "setSamplePerson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setTemplateItem() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sampleUploadOrderService
					.setTemplateItem(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据主数据加载子表明细2
	@Action(value = "setSampleItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setSampleOrderItem() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sampleUploadOrderService
					.setSampleOrderItem2(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询订单编号是否存在 并查询 是否使用
	@Action(value = "selSampleOrderId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleOrderId() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			int total = this.sampleUploadOrderService.selSampleOrderId(id);
			result.put("success", true);
			result.put("data", total);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleUploadOrderService getsampleUploadOrderService() {
		return sampleUploadOrderService;
	}

	public void setsampleUploadOrderService(SampleUploadOrderService sampleUploadOrderService) {
		this.sampleUploadOrderService = sampleUploadOrderService;
	}

	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/**
	 * 癌症信息审核的get和set方法
	 * 
	 * @return
	 */
	public SampleCancerTempService getSampleCancerTempService() {
		return sampleCancerTempService;
	}

	public void setSampleCancerTempService(
			SampleCancerTempService sampleCancerTempService) {
		this.sampleCancerTempService = sampleCancerTempService;
	}

	public SampleCancerTemp getSampleCancerTempOne() {
		return sampleCancerTempOne;
	}

	public void setSampleCancerTempOne(SampleCancerTemp sampleCancerTempOne) {
		this.sampleCancerTempOne = sampleCancerTempOne;
	}

	public SampleCancerTemp getSampleCancerTempTwo() {
		return sampleCancerTempTwo;
	}

	public CrmPatientService getCrmPatientService() {
		return crmPatientService;
	}

	public void setCrmPatientService(CrmPatientService crmPatientService) {
		this.crmPatientService = crmPatientService;
	}

	public void setSampleCancerTempTwo(SampleCancerTemp sampleCancerTempTwo) {
		this.sampleCancerTempTwo = sampleCancerTempTwo;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}
	/**
	 * 保存订单明细
	 * @throws Exception
	 */
	@Action(value = "saveSampleOrderItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleOrderItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sampleOrder=commonDAO.get(SampleOrder.class, id);
			if(sampleOrder!=null){
				this.sampleUploadOrderService.saveSampleOrderItem(sampleOrder, itemDataJson);
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
