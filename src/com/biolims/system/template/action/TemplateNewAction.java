package com.biolims.system.template.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.service.DicTypeService;
import com.biolims.file.service.FileInfoService;
import com.biolims.file.service.OperFileService;
import com.biolims.system.code.SystemConstants;
import com.biolims.system.model.BillTemplateDefine;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleBeforeReagent;
import com.biolims.system.template.model.TempleFinished;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Namespace("/system/template/templeNew")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TemplateNewAction extends BaseActionSupport {
	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "900902";
	@Autowired
	private TemplateService templateService;
	private Template template = new Template();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private OperFileService operFileService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private DicTypeService dicTypeService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * @Title: showTemplateList @Description: 模板列表 @author : shengwei.wang @date
	 *         2018年2月9日上午11:25:20 @return @throws Exception String @throws
	 */
	@Action(value = "showTemplateTable")
	public String showTemplateList() throws Exception {
		rightsId = "900902";
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/template/template.jsp");
	}

	/**
	 * @Title: chooseTemplateItem @Description: 选择字段 @author : shengwei.wang @date
	 *         2018年4月10日下午2:19:57 @throws Exception void @throws
	 */
	@Action(value = "chooseTemplateItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String chooseTemplateItem() throws Exception {
		String taName = getParameterFromRequest("taName");
		putObjToContext("taName", taName);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/template/templateItemChoose.jsp");
	}

	@Action(value = "showChooseTemplateItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showChooseTemplateItemList() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String taName = getParameterFromRequest("taName");

		Map<String, Object> result = templateService.findBillTemplateDefineList(taName, start, length, query, col,
				sort);
		List<BillTemplateDefine> list = (List<BillTemplateDefine>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("tableName", "");
		map.put("columnName", "");
		map.put("columnComment", "");
		map.put("isdisplay", "");
		map.put("controltype", "");
		map.put("isrequire", "");
		map.put("colsort", "");
		map.put("controlwidth", "");
		map.put("isreadonly", "");
		map.put("nullable", "");
		map.put("dataPrecision", "");
		map.put("dataScale", "");
		map.put("dataLength", "");
		map.put("dataType", "");
		map.put("modeltype", "");
		map.put("modeltypeclass", "");
		map.put("fieldType", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));

	}

	/**
	 * 
	 * @Title: editTemplate @Description: 新建、编辑 @author : shengwei.wang @date
	 *         2018年2月9日上午11:26:02 @return @throws Exception String @throws
	 */
	@Action(value = "editTemplate")
	public String editTemplate() throws Exception {
		rightsId = "900901";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			template = templateService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "template");
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			template.setId("NEW");
			template.setType("1");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			template.setCreateUser(user);
			template.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			template.setState("3");
			template.setStateName("新建");
		}
		toState(template.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep1.jsp");
	}

	@Action(value = "viewTemplate")
	public String viewTemplate() throws Exception {
		String id = getParameterFromRequest("id");
		template = templateService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep1.jsp");
	}

	/**
	 * 选择历史的SOP的保存 @Title: saveByHistory @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2019年4月18日 @throws
	 */
	@Action(value = "saveNewByHistory", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String saveByHistory() throws Exception {
		String choseId = getParameterFromRequest("choseId");
		String id = template.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "Template";
			String markCode = "SYMB";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			template.setId(autoID);
		}
		templateService.saveNewByHistory(template, choseId);
		return redirect("/system/template/template/editTemplate.action?id=" + template.getId());

	}

	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String id = template.getId();
		String log = "";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log = "123";
			String modelName = "Template";
			String markCode = "SYMB";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			template.setId(autoID);
		}
		templateService.save(template, changeLog, log);
		return redirect("/system/template/template/editTemplate.action?id=" + template.getId());

	}

	/**
	 * 
	 * @Title: delTemplateItem @Description: 删除步骤 @author : shengwei.wang @date
	 *         2018年2月11日下午6:30:21 @throws Exception void @throws
	 */
	@Action(value = "delTemplateItem")
	public void delTemplateItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String itemId = getParameterFromRequest("itemId");
			templateService.delTemplateItem(id, itemId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: templateEditStepTwo @Description: 第二步配置 @author : shengwei.wang @date
	 *         2018年2月9日上午11:32:40 @return @throws Exception String @throws
	 */
	@Action("templateEditStepTwo")
	public String templateEditStepTwo() throws Exception {
		String id = getParameterFromRequest("id");
		template = templateService.get(id);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep2.jsp");
	}

	/**
	 * @Title: templateEditStepTwo
	 * @Description: 获取实验模板第一步数据 获取实验前准备数据
	 * @author : qingsong.liu
	 * @date 2019年3月8日上午11:32:40
	 */
	@Action("templateEditStepTwoTableJson")
	public void templateEditStepTwoJson() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		List<TemplateItem> itemList = new ArrayList<TemplateItem>();
		itemList = templateService.getTemplateItem(id, orderNum);
		String custom = "[{'label':'操作间名称 ','fieldName':'operatingRoomName','defaultValue':'','type':'text','readOnly':'false','required':'false'}"
				+ ",{'label':'环境温度(℃)','fieldName':'ambientTemperature','defaultValue':'','type':'text','readOnly':'false','required':'false'}"
				+ ",{'label':'环境湿度(%)','fieldName':'ambientHumidity','defaultValue':'','type':'text','readOnly':'false','required':'false'}"
				+ ",{'label':'操作人','fieldName':'templeOperator','defaultValue':'','type':'text','readOnly':'false','required':'false'}"
				+ ",{'label':'审核人','fieldName':'templeReviewer','defaultValue':'','type':'text','readOnly':'false','required':'false'}]";
		// 获取自定义字段
		if (itemList.size() == 0) {
			map.put("itemList", itemList);
			map.put("content", custom.toString().replaceAll("\'", "\""));
		} else {
			map.put("itemList", itemList);
			map.put("content", itemList.get(0).getContent().toString().replaceAll("\'", "\""));
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: saveTempleItem
	 * @Description: 保存第一步实验实验前准备和检查
	 * @author : 刘青松
	 * @date 2019年3月11日上午11:25:20
	 * @return @throws Exception String @throws
	 */
	@Action(value = "saveTempleItem")
	public void saveItem() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			User user = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
			Template templateInfo = templateService.get(id);
			JSONObject json = JSONObject.fromObject(itemJson);
			TemplateItem templateItem = new TemplateItem();
			if (!"".equals(json.getString("id"))) {// 修改
				templateItem = templateService.getTemplateItemInfo(json.getString("id"));
			}
			templateItem.setOrderNum(Integer.parseInt(orderNum));
			templateItem.setContent(json.getString("content"));
			templateItem.setCode(json.getString("code"));
			templateItem.setName(json.getString("name"));
			templateItem.setNote(json.getString("note"));
			templateItem.setEstimatedTime(json.getString("estimatedTime"));
			templateItem.setBlend(json.getString("blend"));
			templateItem.setBottleOrBag(json.getString("bottleOrBag"));
			templateItem.setReinfusionPlan(json.getString("reinfusionPlan"));
			templateItem.setHarvest(json.getString("harvest"));
			templateItem.setIncubator(json.getString("incubator"));
			templateItem.setCellObservation(json.getString("cellObservation"));
//			templateItem.setTransportPlan(json.getString("transportPlan"));

			templateItem.setPrintLabelCoding(json.getString("printLabelCoding"));

			templateItem.setTemplate(templateInfo);
			// 处理自定义字段 标识为0表示可用
			JSONArray jsonCon = JSONArray.fromObject(json.getString("content"));
			if (jsonCon != null) {
				for (int j = 0; j < jsonCon.size(); j++) {
					JSONObject jsoncz = JSONObject.fromObject(jsonCon.get(j));
					// 操作人
					if ("templeOperator".equals(jsoncz.getString("fieldName"))) {
						templateItem.setTempleOperator("0");
						continue;
					}
					// 审核人
					if ("templeReviewer".equals(jsoncz.getString("fieldName"))) {
						templateItem.setTempleReviewer("0");
						continue;
					}
					// operatingRoomName 操作间名称
					if ("operatingRoomName".equals(jsoncz.getString("fieldName"))) {
						templateItem.setOperatingRoomName("0");
						continue;
					}
					// ambientTemperature 环境温度
					if ("ambientTemperature".equals(jsoncz.getString("fieldName"))) {
						templateItem.setAmbientTemperature("0");
						continue;
					}
					// ambientHumidity 环境湿度
					if ("ambientHumidity".equals(jsoncz.getString("fieldName"))) {
						templateItem.setAmbientHumidity("0");
						continue;
					}
				}
			}
			templateService.saveOrUpdateInfo(templateItem);
			JSONArray jsonArray = JSONArray.fromObject(json.getString("operationInstruction"));
			if (jsonArray.size() > 0) {
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject jsonOpera = JSONObject.fromObject(jsonArray.get(i));
					if (jsonOpera.getString("id") != null && !"".equals(jsonOpera.getString("id"))) {
						TempleNstructions templeNstructions = commonDAO.get(TempleNstructions.class,
								jsonOpera.getString("id"));
//						templeNstructions.setTemplateItem(templateInfo);
						templeNstructions.setTemplateItem(templateInfo.getId());
						templeNstructions.setCreateUser(user);
						templeNstructions.setCreateDate(new Date());
						templeNstructions.setName(jsonOpera.getString("name"));
						templeNstructions.setSort(jsonOpera.getString("sort"));
						templeNstructions.setState("0");
						templeNstructions.setItemId(orderNum);
						templeNstructions.setTempleProduction("0");
						templeNstructions.setStepId(json.getString("stepId"));
						templeNstructions.setOperationNote(jsonOpera.getString("operationNote"));
						templateService.saveOrUpdateInfo(templeNstructions);
					} else {
						TempleNstructions templeNstructions = new TempleNstructions();
						templeNstructions.setTemplateItem(templateInfo.getId());
//						templeNstructions.setTemplateItem(templateInfo);
						templeNstructions.setCreateUser(user);
						templeNstructions.setCreateDate(new Date());
						templeNstructions.setName(jsonOpera.getString("name"));
						templeNstructions.setSort(jsonOpera.getString("sort"));
						templeNstructions.setState("0");
						templeNstructions.setItemId(orderNum);
						templeNstructions.setTempleProduction("0");
						templeNstructions.setStepId(json.getString("stepId"));
						templeNstructions.setOperationNote(jsonOpera.getString("operationNote"));
						templateService.saveOrUpdateInfo(templeNstructions);
					}
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: templateNstructionTableJson
	 * @Description: 获取操作实验前指令字表信息
	 * @author : 刘青松
	 * @date 2019年3月11日上午11:25:20
	 * @return @throws Exception String @throws
	 */
	@Action(value = "templateNstructionTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void templateNstructionTableJson() throws Exception {
		String itemId = getRequest().getParameter("orderNum");
		String query = getRequest().getParameter("query");
		String id = getRequest().getParameter("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = templateService.findTempleNstructionsTableJson(id, itemId, start, length, query,
				col, sort);
		List<TempleNstructions> list = (List<TempleNstructions>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		TempleNstructions templeNstructions = new TempleNstructions();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(templeNstructions);
		String data = new SendData().getDateJsonForDatatable1(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * @Title: templateBeforeReagentTableJson
	 * @Description: 获取工前准备试剂准备量
	 * @return @throws Exception String @throws
	 */
	@Action(value = "templateBeforeReagentTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void templateBeforeReagentTableJson() throws Exception {
		String itemId = getRequest().getParameter("orderNum");
		String query = getRequest().getParameter("query");
		String id = getRequest().getParameter("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = templateService.findTempleBeforeReagentTableJson(id, itemId, start, length, query,
				col, sort);
		List<TempleBeforeReagent> list = (List<TempleBeforeReagent>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		TempleBeforeReagent templeBeforeReagent = new TempleBeforeReagent();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(templeBeforeReagent);
		map.put("createUser-id", "");
		map.put("createDate", "yyyy-MM-dd");
		String data = new SendData().getDateJsonForDatatable1(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 刷新工前准备试剂准备表
	 * 
	 * @throws Exception
	 */
	@Action(value = "beforeReagentRefresh")
	public void beforeReagentRefresh() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String orderNum = getRequest().getParameter("orderNum");
		String stepId = getRequest().getParameter("stepId");
		String orderid = getRequest().getParameter("orderid");
		String id = getRequest().getParameter("id");
		try {
			if (!"".equals(id)) {
				templateService.beforeReagentRefresh(id);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 保存工前准备试剂准备表
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveBeforeReagent")
	public void saveBeforeReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String beforeReagentdata = getParameterFromRequest("beforeReagentdata");
		String changeLogItemLast = getParameterFromRequest("changeLogItemLast");
		String id = getParameterFromRequest("id");
		try {
			templateService.saveBeforeReagentRefresh(id, beforeReagentdata, changeLogItemLast);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除生产记录
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTempleRecord")
	public void delTempleRecord() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String orderNum = getRequest().getParameter("orderNum");
		String id = getRequest().getParameter("orderId");
		String content = getRequest().getParameter("content");
		try {
			if (!"".equals(id)) {
				TemplateItem templateItemInfo = templateService.getTemplateItemInfo(id);
				templateItemInfo.setContent(content);
				templateService.templeItemSaveOrUpdate(templateItemInfo);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除工期准备操作指令
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTempleItemNstructions")
	public void delTempleItemNstructions() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONArray JsonArray = JSONArray.fromObject(getRequest().getParameter("ids"));
		try {
			if (JsonArray.size() > 0) {
				for (int i = 0; i < JsonArray.size(); i++) {
					TempleNstructions templeNstructions = templateService
							.getTempleNstructions(JsonArray.get(i).toString());
					if (templeNstructions != null) {
						templeNstructions.setState("1");
						templateService.deleteListPageAll(templeNstructions);
					}
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除完工操作指令
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTempleItemNstructionsEnd")
	public void delTempleItemNstructionsEnd() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONArray JsonArray = JSONArray.fromObject(getRequest().getParameter("ids"));
		try {
			if (JsonArray.size() > 0) {
				for (int i = 0; i < JsonArray.size(); i++) {
					TempleNstructionsEnd templeNstructionsEnd = templateService
							.getTempleNstructionsEnd(JsonArray.get(i).toString());
					if (templeNstructionsEnd != null) {
						templeNstructionsEnd.setState("1");
						templateService.deleteListPageAll(templeNstructionsEnd);
					}
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title:
	 * @Description: 获取质检实验数据
	 * @author : qingsong.liu
	 * @date 2019年3月8日上午11:32:40
	 */
	@Action("templatezhijianListPage")
	public void templatezhijianListPage() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String stepId = getParameterFromRequest("stepId");
		List<ZhiJianItem> zhijianList = new ArrayList<ZhiJianItem>();
//		zhijianList = templateService.getZjItem(id, orderNum);
		zhijianList = templateService.getZjItemByOrderNum(id, orderNum);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("zhijianList", zhijianList);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action("templatezhijianListPageNew")
	public void templatezhijianListPageNew() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String stepId = getParameterFromRequest("stepId");
		List<ZhiJianItem> zhijianList = new ArrayList<ZhiJianItem>();
		zhijianList = templateService.getZjItem(id, orderNum);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("zhijianList", zhijianList);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: saveQualityTesting
	 * @Description: 保存第二步实验质检
	 * @author : 刘青松
	 * @date 2019年3月11日上午11:25:20
	 * @return @throws Exception String @throws
	 */
	@Action(value = "saveQualityTesting")
	public void saveQualityTesting() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			User user = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
			Template template = templateService.get(id);
			JSONObject json = JSONObject.fromObject(itemJson);
			JSONArray jsonArray = JSONArray.fromObject(json.getString("zhijian"));
			List<ZhiJianItem> zhijianList = templateService.getZjItem(id, orderNum);// 先删除原来的
			if (zhijianList.size() > 0) {
				for (int j = 0; j < zhijianList.size(); j++) {
					templateService.deleteQualityTesting(zhijianList.get(j));
				}
			}
			if (jsonArray.size() > 0) {
				for (int i = 0; i < jsonArray.size(); i++) {
					// 获取质检信息
					JSONObject jsonarr = JSONObject.fromObject(jsonArray.get(i));
					ZhiJianItem zhiJianItem = new ZhiJianItem();
					zhiJianItem.setName(jsonarr.getString("name"));
					zhiJianItem.setCode(jsonarr.getString("code"));
					zhiJianItem.setItemId(orderNum);
					zhiJianItem.setTemplate(template);
					zhiJianItem.setTypeId(jsonarr.getString("typeId"));
					zhiJianItem.setTypeName(jsonarr.getString("typeName"));
					templateService.saveOrUpdateInfo(zhiJianItem);
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title:
	 * @Description: 删除质检
	 * @author : qingsong.liu
	 * @date 2019年3月8日上午11:32:40
	 */
	@Action("deltemplatezhijian")
	public void deltemplatezhijian() throws Exception {
		String ids = getParameterFromRequest("ids");
		ZhiJianItem zhijianList = new ZhiJianItem();
		zhijianList = templateService.getObjectBean(ZhiJianItem.class, ids);
		Map<String, Object> map = new HashMap<String, Object>();
		if (zhijianList != null) {
			zhijianList.setState("1");
			templateService.deleteQualityTestingNew(zhijianList);
			map.put("success", true);
		} else {
			map.put("success", false);
			map.put("msg", "删除失败，该数据不存在");
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title:
	 * @Description: 保存第三步生产
	 * @author : qingsong.liu
	 * @date 2019年3月13日上午11:32:40
	 */
	@Action("saveTempleManufacture")
	public void saveTempleManufacture() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String itemJson = getParameterFromRequest("itemJson");
		JSONObject json = JSONObject.fromObject(itemJson);
		// 获取用户
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		// 通过主表id获取主表数据
		Template template = templateService.get(id);
		if (template != null) {
			if (json != null) {
				// 获取生产记录信息 数组
				JSONArray jsonArray = JSONArray.fromObject(json.getString("content"));
				for (int i = 0; i < jsonArray.size(); i++) {
					// 获取数组中的每一个对象 -->每一条生产结果
					JSONObject jsonObjectTem = JSONObject.fromObject(jsonArray.get(i));
					TempleProducingCell templeProducingCell = new TempleProducingCell();
					if (!"".equals(jsonObjectTem.getString("id"))) {// 有id 修改
																	// 没id新增
						templeProducingCell = templateService.getObjectBean(TempleProducingCell.class,
								jsonObjectTem.getString("id"));
					} else {
						templeProducingCell.setCreateDate(ObjectToMapUtils.getTimeString());
					}
					templeProducingCell.setOrderNum(orderNum);
					templeProducingCell.setCreateUserId(user.getId().toString());
					templeProducingCell.setCreateUserName(user.getName().toString());
					templeProducingCell.setTemplate(template);
					templeProducingCell.setProducingName(jsonObjectTem.getString("text"));
					templeProducingCell.setProducingTitle(jsonObjectTem.getString("title"));
					templeProducingCell.setHideSmalleTime(jsonObjectTem.getString("hideTime"));

//					templeProducingCell.setCustomTest(json.getString("result").toString());

					templeProducingCell.setCustomTest(jsonObjectTem.getString("customTest").toString());

					templateService.saveOrUpdateInfo(templeProducingCell);
					// 保存子表信息 试剂
					JSONArray jsonReagent = JSONArray.fromObject(jsonObjectTem.getString("reagent"));
					if (jsonReagent != null) {
						for (int n = 0; n < jsonReagent.size(); n++) {
							// 获取数组中的每一个对象 -->每一条试剂明细
							JSONObject jsonObjRea = JSONObject.fromObject(jsonReagent.get(n));
							TempleProducingReagent templeProducingReagent = new TempleProducingReagent();
							if (!"".equals(jsonObjRea.getString("id"))) {// 有就更新
																			// 无折新增
								templeProducingReagent = templateService.getObjectBean(TempleProducingReagent.class,
										jsonObjRea.getString("id"));
							} else {
								templeProducingReagent.setCreateDate(ObjectToMapUtils.getTimeString());
							}
							templeProducingReagent.setCode(jsonObjRea.getString("code"));
							templeProducingReagent.setName(jsonObjRea.getString("name"));
							// 使用量
							templeProducingReagent.setAmount(jsonObjRea.getString("amount"));
							// 单位
							templeProducingReagent.setUnit(jsonObjRea.getString("unit"));
							templeProducingReagent.setState("0");
							templeProducingReagent.setCreateUserId(user.getId().toString());
							templeProducingReagent.setCreateUserName(user.getName().toString());
							templeProducingReagent.setTempleProducingCell(templeProducingCell.getId());
							templateService.saveOrUpdateInfo(templeProducingReagent);
						}
					}
					// 保存子表信息 设备
					JSONArray jsonCos = JSONArray.fromObject(jsonObjectTem.getString("cos"));
					if (jsonCos != null) {
						for (int n = 0; n < jsonCos.size(); n++) {
							// 获取数组中的每一个对象 -->每一条设备明细
							JSONObject jsonObjRea = JSONObject.fromObject(jsonCos.get(n));
							// 获取设备类型
							/*
							 * DicType dicType = templateService.getObjectBean(DicType.class,
							 * jsonObjRea.getString("typeId"));
							 */
							TempleProducingCos templeProducingCos = new TempleProducingCos();
							if (!"".equals(jsonObjRea.getString("id"))) {
								templeProducingCos = templateService.getObjectBean(TempleProducingCos.class,
										jsonObjRea.getString("id"));
							} else {
								templeProducingCos.setCreateDate(ObjectToMapUtils.getTimeString());
							}
							templeProducingCos.setTypeId(jsonObjRea.getString("typeId").toString());
							templeProducingCos.setTypeName(jsonObjRea.getString("typeName"));
							templeProducingCos.setCreateUserId(user.getId().toString());
							templeProducingCos.setState("0");
							templeProducingCos.setCreateUserName(user.getName().toString());
							templeProducingCos.setTempleProducingCell(templeProducingCell.getId());
							templateService.saveOrUpdateInfo(templeProducingCos);
						}
					}
					// 保存子表信息质检
					JSONArray jsonArray1 = JSONArray.fromObject(jsonObjectTem.getString("zhijian"));
//					List<ZhiJianItem> zhijianList = templateService.getZjItemNew(id, templeProducingCell.getId());// 先删除原来的
//					if (zhijianList.size() > 0) {
//						for (int j = 0; j < zhijianList.size(); j++) {
//							templateService.deleteQualityTesting(zhijianList.get(j));
//						}
//					}
					if (jsonArray1.size() > 0) {
						for (int j = 0; j < jsonArray1.size(); j++) {
							// 获取质检信息
							JSONObject jsonarr = JSONObject.fromObject(jsonArray1.get(j));
							ZhiJianItem zhiJianItem = new ZhiJianItem();
							if (!"".equals(jsonarr.getString("id"))) {// 有就更新
								zhiJianItem = templateService.getObjectBean(ZhiJianItem.class,
										jsonarr.getString("id"));
							} 
							zhiJianItem.setName(jsonarr.getString("name"));
							zhiJianItem.setCode(jsonarr.getString("code"));
							// 样本量
							zhiJianItem.setSampleNum(jsonarr.getString("sum"));
							// 单位
							zhiJianItem.setSampleNumUnit(jsonarr.getString("unit"));
							// 样本类型
							zhiJianItem.setSampleType(jsonarr.getString("sname"));
							//样本类型Id
							zhiJianItem.setSampleTypeId(jsonarr.getString("snameId"));
							zhiJianItem.setItemId(orderNum);
							zhiJianItem.setTemplate(template);
							zhiJianItem.setState("0");
							zhiJianItem.setTypeId(jsonarr.getString("type"));
							zhiJianItem.setTypeName(jsonarr.getString("typeName"));
							zhiJianItem.setTempleProducingCell(templeProducingCell.getId());
							templateService.saveOrUpdateInfo(zhiJianItem);
						}
					}
				}
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title:
	 * @Description: 删除第三步的自定义字段 生产结果
	 * @author : qingsong.liu
	 * @date 2019年3月14日上午11:32:40
	 */
	@Action("deltemplateProductiongCentent")
	public void deltemplateProductiongCentent() throws Exception {
		String id = getParameterFromRequest("ids");
		String orderNum = getParameterFromRequest("orderNum");
		String orderId = getParameterFromRequest("orderId");
		String stepId = getParameterFromRequest("stepId");
		String content = getParameterFromRequest("content");
		List<TempleProducingCell> cell = templateService.getTempleProducingCellListPage(id, orderNum);
		if (cell.size() > 0) {
			for (TempleProducingCell cellValue : cell) {
				cellValue.setCustomTest(content);
				templateService.saveOrUpdateInfo(cellValue);
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title:
	 * @Description: 删除第三步的试剂明细
	 * @author : qingsong.liu
	 * @date 2019年3月14日上午11:32:40
	 */
	@Action("deltemProductiongReanger")
	public void deltemProductiongReanger() throws Exception {
		String ids = getParameterFromRequest("ids");
		TempleProducingReagent tempReagent = new TempleProducingReagent();
		tempReagent = templateService.getObjectBean(TempleProducingReagent.class, ids);
		Map<String, Object> map = new HashMap<String, Object>();
		if (tempReagent != null) {
			tempReagent.setState("1");
			templateService.deleteTempleReagent(tempReagent);
			map.put("success", true);
		} else {
			map.put("success", false);
			map.put("msg", "删除失败，该数据不存在");
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title:
	 * @Description: 删除第三步的设备明细
	 * @author : qingsong.liu
	 * @date 2019年3月14日上午11:32:40
	 */
	@Action("deltemProductiongCos")
	public void deltemProductiongCos() throws Exception {
		String ids = getParameterFromRequest("ids");
		TempleProducingCos templeCos = new TempleProducingCos();
		templeCos = templateService.getObjectBean(TempleProducingCos.class, ids);
		Map<String, Object> map = new HashMap<String, Object>();
		if (templeCos != null) {
			templeCos.setState("1");
			templateService.deleteTempleCos(templeCos);
			map.put("success", true);
		} else {
			map.put("success", false);
			map.put("msg", "删除失败，该数据不存在");
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: templateEditStepTwo
	 * @Description: 获取实验模板第三步数据 获取生产模板
	 * @author : qingsong.liu
	 * @date 2019年3月14日上午11:32:40
	 */
	@Action("templateProducingCellList")
	public void templateProducingCellList() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		JSONArray jsonArray = new JSONArray();
		Map<String, Object> mapCentent = new HashMap<String, Object>();// 自定义字段
		List<TempleProducingCell> cell = new ArrayList<TempleProducingCell>();
		cell = templateService.getTempleProducingCellListPage(id, orderNum);
		if (cell.size() > 0) {
			for (int i = 0; i < cell.size(); i++) {
				Map<String, Object> mapjson = new HashMap<String, Object>();
				mapjson.put("id", cell.get(i).getId());
				mapjson.put("title", cell.get(i).getProducingTitle());
				mapjson.put("hideTime", cell.get(i).getHideSmalleTime());
				mapjson.put("name", cell.get(i).getProducingName());
				// jsonArray.add(mapCentent);
				// 获取试剂列表
				List<TempleProducingReagent> listReagent = templateService
						.getTempleProducingReagentListPage(cell.get(i).getId());
				mapjson.put("reagent", listReagent);
				// 获取设备主表
				List<TempleProducingCos> listCos = templateService.getTempleProducingCosListPage(cell.get(i).getId());
				mapjson.put("cos", listCos);
				// 获取质检
//				List<ZhiJianItem>  zhijianList = templateService.getZjItem(id, orderNum);				
				List<ZhiJianItem> zhijianList = templateService.getZjItemNew(id, cell.get(i).getId());
				mapjson.put("zhijianList", zhijianList);

				mapjson.put("centent", cell.get(i).getCustomTest());
				// 將每一条生产记录放入一个数组中
				jsonArray.add(mapjson);
			}
			mapCentent.put("centent", cell.get(0).getCustomTest());
			jsonArray.add(mapCentent);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("producCell", jsonArray.toString());
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * 删除生产步骤
	 */
	@Action(value = "delTempleProducingCell")
	public void delTempleProducingCell() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			templateService.delTempleProducingCell(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: templateEditStepTwo
	 * @Description: 获取实验模板第四步数据 获取实验前准备数据
	 * @author : qingsong.liu
	 * @date 2019年3月8日上午11:32:40
	 */
	@Action("findTempleFinishedListPage")
	public void findTempleFinishedListPage() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String stepId = getParameterFromRequest("stepId");
		String custom = "[{'label':'操作人','fieldName':'templeOperator','defaultValue':'','type':'text','readOnly':'false','required':'false'}"
				+ ",{'label':'复核人','fieldName':'templeReviewer','defaultValue':'','type':'text','readOnly':'false','required':'false'}]";
		List<TempleFinished> itemList = new ArrayList<TempleFinished>();
		itemList = templateService.getTempleFinished(id, orderNum);
		Map<String, Object> map = new HashMap<String, Object>();
		JSONArray jsonArray = new JSONArray();
		if (itemList.size() == 0) {
			// 将单引号换成双引号 别问我为什么json格式需要
			map.put("itemList", itemList);
			map.put("content", custom.toString().replaceAll("\'", "\""));
		} else {
			// 将单引号换成双引号 别问我为什么json格式需要
			map.put("itemList", itemList);
			map.put("content", itemList.get(0).getContent().toString().replaceAll("\'", "\""));
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: templateFinishedTableJson
	 * @Description: 获取操作实验后指令字表信息 实验完工
	 * @author : 刘青松
	 * @date 2019年3月11日上午11:25:20
	 * @return @throws Exception String @throws
	 */
	@Action(value = "templateFinishedTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void templateFinishedTableJson() throws Exception {
		String itemId = getRequest().getParameter("orderNum");
		String query = getRequest().getParameter("query");
		String id = getRequest().getParameter("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = templateService.findTemplefinishedTableJson(id, itemId, start, length, query, col,
				sort);
		List<TempleNstructionsEnd> list = (List<TempleNstructionsEnd>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		TempleNstructionsEnd templeFinished = new TempleNstructionsEnd();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(templeFinished);
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * @Title: saveTemplefinished
	 * @Description: 保存第四步实验实验后 操作指令數據
	 * @author : 刘青松
	 * @date 2019年3月11日上午11:25:20
	 * @return @throws Exception String @throws
	 */
	@Action(value = "saveTemplefinished")
	public void saveTemplefinished() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			User user = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
			Template templateInfo = templateService.get(id);
			// 获取所有数据
			JSONObject json = JSONObject.fromObject(itemJson);
			TempleFinished templeFinished = new TempleFinished();
			// 创建第四步主表对象
			List<TempleFinished> itemList = new ArrayList<TempleFinished>();
			itemList = templateService.getTempleFinished(id, orderNum);
			if (itemList.size() > 0) {// 修改
				templeFinished = templateService.getObjectBean(TempleFinished.class, itemList.get(0).getId());
			}
			templeFinished.setOrderNum(orderNum);
			templeFinished.setCreateDate(ObjectToMapUtils.getTimeString());
			templeFinished.setCreateUser(user.getId());
			templeFinished.setTemplateItem(templateInfo.getId().toString());

			// 处理自定义字段 使用默认的那么 自定义字段中为空 有新增的 需要去掉默认的操作人和复核人
			JSONArray jsonCon = JSONArray.fromObject(json.getString("content"));
			if (jsonCon != null) {
				for (int j = 0; j < jsonCon.size(); j++) {
					JSONObject jsoncz = JSONObject.fromObject(jsonCon.get(j));
					// templeFinished.setTempleOperator("1");
					if ("templeOperator".equals(jsoncz.getString("fieldName"))) {
						templeFinished.setTempleOperator("0");
						continue;
					}
					// templeFinished.setTempleReviewer("1");
					if ("templeReviewer".equals(jsoncz.getString("fieldName"))) {
						templeFinished.setTempleReviewer("0");
						continue;
					}
				}
			}
			templeFinished.setContent(json.getString("content"));
			templateService.saveOrUpdateInfo(templeFinished);
			JSONArray jsonArray = JSONArray.fromObject(json.getString("operationInstruction"));
			if (jsonArray.size() > 0) {
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject jsonOpera = JSONObject.fromObject(jsonArray.get(i));
					TempleNstructionsEnd templeNstructionsend = new TempleNstructionsEnd();
					if (!"".equals(jsonOpera.getString("id"))) {
						templeNstructionsend = templateService.getObjectBean(TempleNstructionsEnd.class,
								jsonOpera.getString("id"));
					}
					templeNstructionsend.setTemplateItem(templateInfo);
					templeNstructionsend.setCreateUser(user);
					templeNstructionsend.setCreateDate(new Date());
					templeNstructionsend.setState("0");
					templeNstructionsend.setName(jsonOpera.getString("name"));
					templeNstructionsend.setSort(jsonOpera.getString("sort"));
					templeNstructionsend.setOrderNum(orderNum);
					templeNstructionsend.setTempleProduction("0");
					templeNstructionsend.setStepId(json.getString("stepId"));
					templateService.saveOrUpdateInfo(templeNstructionsend);
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TemplateService getTemplateService() {
		return templateService;
	}

	public void setTemplateService(TemplateService templateService) {
		this.templateService = templateService;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}
}
