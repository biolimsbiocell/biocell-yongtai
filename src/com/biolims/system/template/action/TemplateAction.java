﻿package com.biolims.system.template.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.file.service.OperFileService;
import com.biolims.system.code.SystemConstants;
import com.biolims.system.model.BillTemplateDefine;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/template/template")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TemplateAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "1100003";
	@Autowired
	private TemplateService templateService;
	private Template template = new Template();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private OperFileService operFileService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "getHistoryTemplate")
	public void getHistoryTemplate() throws Exception {
		String id = getRequest().getParameter("id");
		Template tem = templateService.get(id);
		if (tem != null) {
			HttpUtils.write(JsonUtils.toJsonString(tem));
		}
	}

	/**
	 * 展示模板的Dialog @Title: showTemplateDialogList @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2018年8月22日 @throws
	 */
	@Action(value = "showTemplateDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateDialogList() throws Exception {
		String type = getRequest().getParameter("type");
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/template/showTemplateDialog.jsp");
	}

	@Action(value = "showTemplateDialogListJson")
	public void showTemplateDialogListJson() throws Exception {
		String type = getRequest().getParameter("type");
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = templateService.showTemplateDialogListJson(start, length, query, col, sort, type);
		List<Template> list = (List<Template>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testType-id", "");
		map.put("testType-name", "");
		map.put("versionNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 
	 * @Title: showTemplateList @Description: 模板列表 新得一套sop生产 @author :
	 * shengwei.wang @date 2018年2月9日上午11:25:20 @return @throws Exception
	 * String @throws
	 */
	/*
	 * @Action(value = "showTemplateTable") public String showTemplateList() throws
	 * Exception { rightsId = "900902"; putObjToContext("handlemethod",
	 * SystemConstants.PAGE_HANDLE_METHOD_LIST); toToolBar(rightsId, "", "",
	 * SystemConstants.PAGE_HANDLE_METHOD_LIST); return
	 * dispatcher("/WEB-INF/page/system/template/template.jsp"); }
	 */

	/**
	 * 
	 * @Title: showTemplateList @Description: 模板列表 老的一套sop生产 @author :
	 * shengwei.wang @date 2018年2月9日上午11:25:20 @return @throws Exception
	 * String @throws
	 */
	@Action(value = "showTemplateTableOld")
	public String showTemplateTableOld() throws Exception {
		rightsId = "1100003";
		String type = getRequest().getParameter("type");
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/template/templateOld.jsp");
	}

	@Action(value = "showTemplateTableJson")
	public void showTemplateTableJson() throws Exception {
		String type = getRequest().getParameter("type");
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = templateService.showTemplateTableJson(start, length, query, col, sort, type);
		List<Template> list = (List<Template>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("qcNum", "");
		map.put("sampleNum", "");
		map.put("testType-id", "");
		map.put("testType-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("codeMain-id", "");
		map.put("codeMain-name", "");
		map.put("storageContainer-id", "");
		map.put("storageContainer-name", "");
		map.put("fileInfo-id", "");
		map.put("fileInfo-fileName", "");
		map.put("duringDays", "");
		map.put("remindDays", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("productNum", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("templateFields", "");
		map.put("templateFieldsCode", "");
		map.put("templateFieldsItem", "");
		map.put("templateFieldsItemCode", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	// @Action(value = "templateSelect", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showDialogTemplateList() throws Exception {
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/template/templateDialog.jsp");
	// }

	// @Action(value = "showDialogTemplateTableJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showDialogTemplateListJson() throws Exception {
	// String query = new HashMap<String, String>();
	// String query = getParameterFromRequest("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String flag=getParameterFromRequest("flag");
	// queryMap.put("query", query);
	// Map<String, Object> map = templateService.showDialogTemplateListJson(
	// start, length, queryMap, col, sort,flag);
	// HttpUtils.write(PushData.pushData("0", map));
	// }
	/**
	 * 
	 * @Title: chooseTemplateItem @Description: 选择字段 @author : shengwei.wang @date
	 * 2018年4月10日下午2:19:57 @throws Exception void @throws
	 */
	@Action(value = "chooseTemplateItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String chooseTemplateItem() throws Exception {
		String taName = getParameterFromRequest("taName");
		putObjToContext("taName", taName);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/template/templateItemChoose.jsp");
	}

	@Action(value = "showChooseTemplateItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showChooseTemplateItemList() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String taName = getParameterFromRequest("taName");

		Map<String, Object> result = templateService.findBillTemplateDefineList(taName, start, length, query, col,
				sort);
		List<BillTemplateDefine> list = (List<BillTemplateDefine>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("tableName", "");
		map.put("columnName", "");
		map.put("columnComment", "");
		map.put("isdisplay", "");
		map.put("controltype", "");
		map.put("isrequire", "");
		map.put("colsort", "");
		map.put("controlwidth", "");
		map.put("isreadonly", "");
		map.put("nullable", "");
		map.put("dataPrecision", "");
		map.put("dataScale", "");
		map.put("dataLength", "");
		map.put("dataType", "");
		map.put("modeltype", "");
		map.put("modeltypeclass", "");
		map.put("fieldType", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));

	}

	// // 根据类型加载模板
	// @Action(value = "templateSelectByType", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showDialogTemplateListByType() throws Exception {
	// String type = getParameterFromRequest("type");
	// putObjToContext("type", type);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/template/templateDialogByType.jsp");
	// }
	//
	// @Action(value = "showDialogTemplateListJsonByType", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showDialogTemplateListJsonByType() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// Map<String, String> map2Query = new HashMap<String, String>();
	// if (data != null && data.length() > 0) {
	// map2Query = JsonUtils.toObjectByJson(data, Map.class);
	// }
	// // map2Query.put("type", SystemConstants.TASK_TYPE_DNA);
	// String type = getRequest().getParameter("type");
	// Map<String, Object> result = templateService.findTemplateListByType(
	// map2Query, startNum, limitNum, dir, sort, type);
	// Long count = (Long) result.get("total");
	// List<Template> list = (List<Template>) result.get("list");
	//
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("testType-id", "");
	// map.put("testType-name", "");
	// map.put("createUser-id", "");
	// map.put("createUser-name", "");
	// map.put("createDate", "yyyy-MM-dd");
	// map.put("state", "");
	// map.put("stateName", "");
	// map.put("storageContainer-id", "");
	// map.put("storageContainer-name", "");
	// // map.put("isUsed", "");
	// map.put("fileInfo-id", "");
	// map.put("fileInfo-fileName", "");
	// map.put("duringDays", "");
	// map.put("remindDays", "");
	// map.put("dicSampleType-id", "");
	// map.put("dicSampleType-name", "");
	// map.put("dicSampleTypeId", "");
	// map.put("dicSampleTypeName", "");
	// map.put("productNum", "");
	// map.put("productNum1", "");
	// map.put("acceptUser-id", "");
	// map.put("acceptUser-name", "");
	// map.put("sampleNum", "");
	// map.put("templateFields", "");
	// map.put("templateFieldsCode", "");
	// map.put("templateFieldsItem", "");
	// map.put("templateFieldsItemCode", "");
	// new SendData().sendDateJson(map, list, count,
	// ServletActionContext.getResponse());
	// }

	/**
	 * 
	 * @Title: editTemplate @Description: 新建、编辑 @author : shengwei.wang @date
	 * 2018年2月9日上午11:26:02 @return @throws Exception String @throws
	 */
	@Action(value = "editTemplateOld")
	public String editTemplateOld() throws Exception {
		rightsId = "1100002";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			template = templateService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "template");
		} else {
			template.setId("NEW");
			template.setType("0");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			template.setCreateUser(user);
			template.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep1Old.jsp");
	}

	/**
	 * 
	 * @Title: editTemplate @Description: 新建、编辑 @author : shengwei.wang @date
	 * 2018年2月9日上午11:26:02 @return @throws Exception String @throws
	 */
	@Action(value = "editTemplate")
	public String editTemplate() throws Exception {
		rightsId = "900901";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			template = templateService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "template");
		} else {
			template.setId("NEW");
			template.setType("1");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			template.setCreateUser(user);
			template.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep1.jsp");
	}

	// @Action(value = "copyTemplate")
	// public String copyTemplate() throws Exception {
	// String id = getParameterFromRequest("id");
	// String handlemethod = getParameterFromRequest("handlemethod");
	// template = templateService.get(id);
	// template.setId("");
	// handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
	// toToolBar(rightsId, "", "", handlemethod);
	// toSetStateCopy();
	// return dispatcher("/WEB-INF/page/system/template/templateEdit.jsp");
	// }

	/**
	 * 选择历史的SOP然后进行保存 @Title: saveByHistory @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2019年4月19日 @throws
	 */
	@Action(value = "saveByHistory")
	public String saveByHistory() throws Exception {
		String choseId = getParameterFromRequest("choseId");
		String id = template.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "Template";
			String markCode = "SYMB";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			template.setId(autoID);
			template.setState("3");
			template.setStateName("新建");
		}
		templateService.saveByHistory(template, choseId);
		return redirect("/system/template/template/editTemplateOld.action?id=" + template.getId());

	}

	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String id = template.getId();
		String log="";
		log="123";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "Template";
			String markCode = "SYMB";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			template.setId(autoID);
			template.setState("3");
			template.setStateName("新建");
		}
		templateService.save(template, changeLog,log);
		return redirect("/system/template/template/editTemplateOld.action?id=" + template.getId());

	}

	@Action(value = "viewTemplate")
	public String toViewTemplate() throws Exception {
		String id = getParameterFromRequest("id");
		template = templateService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep1Old.jsp");
	}

	// @Action(value = "showTemplateItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showTemplateItemList() throws Exception {
	// return dispatcher("/WEB-INF/page/system/template/templateItem.jsp");
	// }
	//
	// @Action(value = "showTemplateItemListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showTemplateItemListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = templateService.findTemplateItemList(
	// scId, startNum, limitNum, dir, sort);
	// Long total = (Long) result.get("total");
	// List<TemplateItem> list = (List<TemplateItem>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("name", "");
	// map.put("note", "");
	// map.put("template-name", "");
	// map.put("template-id", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	// @Action(value = "showTemplateItemTableJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showTemplateItemTableJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = templateService.findTemplateItemList(
	// scId, startNum, limitNum, dir, sort);
	// Long total = (Long) result.get("total");
	// List<TemplateItem> list = (List<TemplateItem>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("name", "");
	// map.put("note", "");
	// map.put("template-name", "");
	// map.put("template-id", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 
	 * @Title: delTemplateItem @Description: 删除步骤 @author : shengwei.wang @date
	 * 2018年2月11日下午6:30:21 @throws Exception void @throws
	 */
	@Action(value = "delTemplateItem")
	public void delTemplateItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String itemId = getParameterFromRequest("itemId");
			templateService.delTemplateItem(id, itemId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// @Action(value = "showReagentItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showReagentItemList() throws Exception {
	// String itemId = getParameterFromRequest("itemId");
	// putObjToContext("itemId", itemId);
	// return dispatcher("/WEB-INF/page/system/template/reagentItem.jsp");
	// }
	//
	// // 根据试剂编号查询试剂明细
	// @Action(value = "showReagentItemByCodeList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showReagentItemByCodeList() throws Exception {
	// // String code=getParameterFromRequest("code");
	// // putObjToContext("code",code);
	// String tid = getParameterFromRequest("tid");
	// putObjToContext("tid", tid);
	// return dispatcher("/WEB-INF/page/system/template/reagentItem1.jsp");
	// }
	//
	// @Action(value = "showReagentItemListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showReagentItemListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// String code = getParameterFromRequest("code");
	// String tid = getParameterFromRequest("tid");
	// String t = getParameterFromRequest("t");
	// String itemId = getParameterFromRequest("itemId");
	// Map<String, Object> result = new HashMap<String, Object>();
	// // result = templateService.findReagentItemList(scId, startNum,
	// // limitNum, dir,sort);
	// if (itemId.equals("")) {
	// result = templateService.findReagentItemList(scId, startNum,
	// limitNum, dir, sort);
	// } else {
	// result = templateService.findReagentItemListByItemId(scId,
	// startNum, limitNum, dir, sort, itemId);
	// }
	// Long total = (Long) result.get("total");
	// List<ReagentItem> list = (List<ReagentItem>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("name", "");
	// map.put("kitName", "");
	// map.put("batch", "");
	// map.put("isGood", "");
	// map.put("note", "");
	// map.put("num", "");
	// map.put("itemId", "");
	// map.put("template-name", "");
	// map.put("template-id", "");
	// map.put("sn", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 删除质检明细 @Title: delZhijianItem @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2018年9月10日 @throws
	 */
	@Action(value = "delZhijianItem")
	public void delZhijianItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			templateService.delZhijianItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delReagentItem")
	public void delReagentItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			templateService.delReagentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// // 上传文件
	// @Action(value = "toSampeUpload", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String toSampeUpload() {
	// String fileId = getRequest().getParameter("fileId");
	// String isUpload = getRequest().getParameter("isUpload");
	// if (fileId != null && fileId.length() > 0)
	// fileInfo = this.operFileService.queryFileInfoById(fileId);
	// String module = getRequest().getParameter("module");
	// getRequest().setAttribute("module", module);
	// getRequest().setAttribute("isUpload", isUpload);
	// return dispatcher("/WEB-INF/page/sample/task/sampleUpload.jsp");
	// }
	//
	// // 上传文件
	// @Action(value = "toSampeUpload1", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String toSampeUpload1() {
	// String fileId = getRequest().getParameter("fileId");
	// String isUpload = getRequest().getParameter("isUpload");
	// if (fileId != null && fileId.length() > 0)
	// fileInfo = this.operFileService.queryFileInfoById(fileId);
	// String module = getRequest().getParameter("module");
	// getRequest().setAttribute("module", module);
	// getRequest().setAttribute("isUpload", isUpload);
	// return dispatcher("/WEB-INF/page/sample/task/sampleUpload1.jsp");
	// }
	//
	// @Action(value = "showCosItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showCosItemList() throws Exception {
	// String itemId = getParameterFromRequest("itemId");
	// putObjToContext("itemId", itemId);
	// return dispatcher("/WEB-INF/page/system/template/cosItem.jsp");
	// }
	/**
	 * 
	 * @Title: showContentTable @Description: 自定义字段 @author : shengwei.wang @date
	 * 2018年2月9日上午11:28:09 @return @throws Exception String @throws
	 */
	@Action(value = "showContentItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showContentTable() throws Exception {
		String itemId = getParameterFromRequest("itemId");
		putObjToContext("itemId", itemId);
		return dispatcher("/WEB-INF/page/system/template/contentItem.jsp");
	}

	@Action(value = "showContentTableJson")
	public void showContentTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String itemId = getParameterFromRequest("itemId");
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = templateService.showContentTableJson(id, itemId);
		String data = "";
		if (result.get("data") != null) {
			data = result.get("data").toString();
		} else {
			data = "{}";
		}
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	public void saveContentTable() throws Exception {

	}

	// @Action(value = "showCosItemListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showCosItemListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// String itemId = getParameterFromRequest("itemId");
	// Map<String, Object> result = new HashMap<String, Object>();
	// // result = templateService.findCosItemList(scId, startNum,
	// // limitNum,
	// // dir, sort);
	// if (itemId.equals("")) {
	// result = templateService.findCosItemList(scId, startNum,
	// limitNum, dir, sort);
	// } else {
	// result = templateService.findCosItemListByItemId(scId,
	// startNum, limitNum, dir, sort, itemId);
	// }
	// Long total = (Long) result.get("total");
	// List<CosItem> list = (List<CosItem>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// // map.put("code", "");
	// // map.put("name", "");
	// map.put("isGood", "");
	// map.put("itemId", "");
	// map.put("type-id", "");
	// map.put("type-name", "");
	// map.put("instrument-state-name", "");
	// map.put("instrument-name", "");
	// map.put("instrument-id", "");
	// //
	// map.put("temperature", "");
	// map.put("speed", "");
	// map.put("time", "");
	// map.put("note", "");
	// //
	// map.put("template-name", "");
	// map.put("template-id", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCosItem")
	public void delCosItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			templateService.delCosItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// // 加载库存主数据
	// @Action(value = "showStorageList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showStorageList() throws Exception {
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/template/showStorageList.jsp");
	// }
	//
	// @Action(value = "showStorageListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showStorageListJson() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// Map<String, String> map2Query = new HashMap<String, String>();
	// if (data != null && data.length() > 0)
	// map2Query = JsonUtils.toObjectByJson(data, Map.class);
	// Map<String, Object> result = templateService.findStorageList(map2Query,
	// startNum, limitNum, dir, sort);
	// Long count = (Long) result.get("total");
	// List<Storage> list = (List<Storage>) result.get("list");
	//
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("spec", "");
	// map.put("kit-id", "");
	// map.put("kit-name", "");
	// map.put("type-name", "");
	// map.put("studyType-name", "");
	// map.put("position-name", "");
	// // map.put("producer-name", "");
	// map.put("num", "");
	// map.put("unit-name", "");
	// map.put("note", "");
	// map.put("state-name", "");
	// map.put("dutyUser-name", "");
	// map.put("useDesc", "");
	// map.put("searchCode", "");
	// map.put("source-name", "");
	// map.put("barCode", "");
	// map.put("createUser-name", "");
	// map.put("createDate", "");
	// map.put("ifCall", "");
	// new SendData().sendDateJson(map, list, count,
	// ServletActionContext.getResponse());
	// }
	//
	// // 根据试剂号加载采购批次
	// @Action(value = "showStorageReagentBuyList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showStorageReagentBuyList() throws Exception {
	// String codes = getParameterFromRequest("codes");
	// putObjToContext("codes", codes);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return
	// dispatcher("/WEB-INF/page/system/template/showStorageReagentBuyList.jsp");
	// }
	//
	// @Action(value = "showStorageReagentBuyListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showStorageReagentBuyListJson() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// Map<String, String> map2Query = new HashMap<String, String>();
	// if (data != null && data.length() > 0)
	// map2Query = JsonUtils.toObjectByJson(data, Map.class);
	// String scId = getRequest().getParameter("codes");
	// Map<String, Object> result = new HashMap<String, Object>();
	// result = templateService.findStorageReagentBuySerialList(map2Query,
	// startNum, limitNum, dir, sort, scId);
	// Long total = (Long) result.get("total");
	// List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("serial", "");
	// map.put("spec", "");
	// map.put("rankType-id", "");
	// map.put("code", "");
	// map.put("productDate", "yyyy-MM-dd");
	// map.put("expireDate", "yyyy-MM-dd");
	// map.put("remindDate", "yyyy-MM-dd");
	// map.put("inDate", "yyyy-MM-dd");
	// map.put("outPrice", "#.####");
	// map.put("storage-unit-name", "");
	// map.put("num", "");
	// map.put("useNum", "");
	// map.put("isGood", "");
	// map.put("position-id", "");
	// map.put("position-name", "");
	// map.put("purchasePrice", "");
	//
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// }

	// // 根据主数据加载子表明细
	// @Action(value = "setTemplateItem", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setTemplateItem() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.templateService
	// .setTemplateItem(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// @Action(value = "setTemplateReagent", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setTemplateReagent() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.templateService
	// .setTemplateReagent(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 根据选中的步骤异步加载相关的试剂数据
	// @Action(value = "setReagent", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setReagent() throws Exception {
	// String code = getRequest().getParameter("code");
	// String id = getRequest().getParameter("tid");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.templateService
	// .setReagent(id, code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// @Action(value = "setTemplateCos", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setTemplateCos() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.templateService
	// .setTemplateCos(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 根据选中的步骤异步加载相关的设备数据
	// @Action(value = "setCos", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setCos() throws Exception {
	// String code = getRequest().getParameter("code");
	// String id = getRequest().getParameter("tid");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.templateService
	// .setCos(id, code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 打开excel
	// @Action(value = "openTemplate", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String openTemplate() throws Exception {
	// String code = getRequest().getParameter("id");
	// List<Template> list = templateDao.tempList(code);
	// String fid = "";
	// String fname = "";
	// for (Template t : list) {
	// fid = t.getFileInfo().getId();
	// fname = t.getFileInfo().getFileName();
	// }
	// putObjToContext("fileId", fid);
	// putObjToContext("", fname);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/template/openTemplate.jsp");
	// }
	//
	// // 判断所选模板的相关设备是否是占用状态
	// @Action(value = "findInstrument", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void findInstrument() throws Exception {
	// String code = getParameterFromRequest("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// boolean flag = false;
	// List<Template> tList = templateDao.tempList(code);
	// for (Template tl : tList) {
	// List<CosItem> cList = templateDao.setCosList(tl.getId());
	// for (CosItem ci : cList) {
	// String isFull = "";
	// if (null != ci.getInstrument()) {
	//
	// List<Instrument> list = templateDao.findInstrument(ci
	// .getInstrument().getId());
	//
	// for (Instrument si : list) {
	// isFull = si.getIsFull();
	// if (isFull != null && isFull.equals("1")) {
	// flag = true;
	// break;
	// }
	// }
	//
	// }
	// }
	// }
	// result.put("success", true);
	// result.put("data", flag);
	// } catch (Exception e) {
	// // result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// @Action(value = "setReagentByCode", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setReagentByCode() throws Exception {
	// // String code = getRequest().getParameter("code");
	// String template = getRequest().getParameter("template");
	// // String id = getRequest().getParameter("tid");
	// List<ReagentItem> listReagent = this.templateDao
	// .selectReagentByCode(template);
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// // List<Map<String, String>> dataListMap =
	// // this.templateDao.selectReagentByCode(code);
	// result.put("success", true);
	// result.put("data", listReagent);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	/**
	 * 
	 * @Title: templateEditStepTwo @Description: 第二步配置 @author : shengwei.wang @date
	 * 2018年2月9日上午11:32:40 @return @throws Exception String @throws
	 */
	@Action(value="templateEditStepTwo", interceptorRefs =@InterceptorRef("biolimsDefaultStack"))
	public String templateEditStepTwo() throws Exception {
		String id = getParameterFromRequest("id");
		template = templateService.get(id);
		/*
		 * String type = getParameterFromRequest("type");
		 * if(!type.equals("")&&type.equals("1")){//1是质检 2是生产 return
		 * dispatcher("/WEB-INF/page/system/template/templateEditStep2Old.jsp"); }else{
		 * return dispatcher("/WEB-INF/page/system/template/templateEditStep2.jsp"); }
		 */
		return dispatcher("/WEB-INF/page/system/template/templateEditStep2.jsp");
	}

	/**
	 * 
	 * @Title: templateEditStepTwo @Description: 第二步配置 老的sop生产 @author :
	 * shengwei.wang @date 2018年2月9日上午11:32:40 @return @throws Exception
	 * String @throws
	 */
	@Action("templateEditStepTwoOld")
	public String templateEditStepTwoOld() throws Exception {
		String id = getParameterFromRequest("id");
		template = templateService.get(id);
		return dispatcher("/WEB-INF/page/system/template/templateEditStep2Old.jsp");
	}

	/**
	 * @Title: templateEditStepTwo
	 * @Description: 获取实验模板
	 * @author : qingsong.liu
	 * @date 2019年3月8日上午11:32:40
	 */
	@Action("templateEditStepTwoJson")
	public void templateEditStepTwoJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		// 获取实验模板字表第一列部分
		List<TemplateItem> itemList = new ArrayList<TemplateItem>();
		itemList = templateService.getTemplateItem(id, orderNum);
		// 获取实验试剂明细
		List<ReagentItem> reagentList = new ArrayList<ReagentItem>();
		reagentList = templateService.getReagentItem(id, orderNum);
		// 获取设备明细
		List<CosItem> cosTypeList = new ArrayList<CosItem>();
		cosTypeList = templateService.getCosItem(id, orderNum);
		// 质检明细
		List<ZhiJianItem> zhijianList = new ArrayList<ZhiJianItem>();
		zhijianList = templateService.getZjItem(id, orderNum);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("itemList", itemList);
		map.put("reagentList", reagentList);
		map.put("cosTypeList", cosTypeList);
		map.put("zhijianList", zhijianList);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 保存第一步工前准备和检查
	@Action(value = "saveItem")
	public void saveItem() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String itemJson = getParameterFromRequest("itemJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String zjianJson = getParameterFromRequest("zjianJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			templateService.saveItem(id, orderNum, itemJson, reagentJson, cosJson, zjianJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TemplateService getTemplateService() {
		return templateService;
	}

	public void setTemplateService(TemplateService templateService) {
		this.templateService = templateService;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}
}
