package com.biolims.system.template.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 模版明细
 * @author lims-platform
 * @date 2015-11-18 17:00:21
 * @version V1.0   
 *添加预计日期字段带入实验模块的实验步骤显示(用于审批流程) 20180821 nan.jiang
 */
@Entity
@Table(name = "SYS_TEMPLATE_ITEM")
@SuppressWarnings("serial")
public class TemplateItem extends EntityDao<TemplateItem> implements java.io.Serializable,Cloneable {
	/**步骤id*/
	private String id;
	/**步骤编号*/
	private String code;
	/**步骤名称*/
	private String name;
	/**备注*/
	private String note;
	/**排序号*/
	private int orderNum;
	/**关联主表*/
	private Template template;
	/** 自定义字段      生产记录 */
	private String  content;
	/** 自定义字段 */
	private String testUserList;
	/** 预计时间(天) */
	private String estimatedTime;
	
	//新加字段
	/** 操作间名称 */
	@Column(name ="operating_room_name", length = 50)
	private String operatingRoomName;
	/** 环境温度 */
	@Column(name ="ambient_temperature", length = 50)
	private String ambientTemperature;
	/** 环境湿度 */
	@Column(name ="ambient_humidity", length = 50)
	private String ambientHumidity;
	/**操作人*/
	@Column(name = "temple_operator", length = 50)
	private String templeOperator;
	/**复核人*/
	@Column(name = "temple_reviewer", length = 50)
	private String templeReviewer;
	/**是否混合*/
	private String blend;
	/**瓶/袋*/
	private String bottleOrBag;
	
	/**是否流入回输计划*/
	private String reinfusionPlan;
	/**是否收获*/
	private String harvest;
	/**是否流到运输计划*/
	private String transportPlan;
	
	/** 打印标签编码 */
	private String printLabelCoding;
	
	/**是否入二氧化碳培养箱*/
	private String incubator;
	
	
	/**是否细胞观察*/
	private String cellObservation;
	
	
	
	public String getCellObservation() {
		return cellObservation;
	}
	public void setCellObservation(String cellObservation) {
		this.cellObservation = cellObservation;
	}
	public String getIncubator() {
		return incubator;
	}
	public void setIncubator(String incubator) {
		this.incubator = incubator;
	}
	public String getPrintLabelCoding() {
		return printLabelCoding;
	}
	public void setPrintLabelCoding(String printLabelCoding) {
		this.printLabelCoding = printLabelCoding;
	}
	public String getReinfusionPlan() {
		return reinfusionPlan;
	}
	public void setReinfusionPlan(String reinfusionPlan) {
		this.reinfusionPlan = reinfusionPlan;
	}
	public String getHarvest() {
		return harvest;
	}
	public void setHarvest(String harvest) {
		this.harvest = harvest;
	}
	public String getTransportPlan() {
		return transportPlan;
	}
	public void setTransportPlan(String transportPlan) {
		this.transportPlan = transportPlan;
	}
	public String getBlend() {
		return blend;
	}
	public void setBlend(String blend) {
		this.blend = blend;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	public String getOperatingRoomName() {
		return operatingRoomName;
	}
	public void setOperatingRoomName(String operatingRoomName) {
		this.operatingRoomName = operatingRoomName;
	}
	public String getAmbientTemperature() {
		return ambientTemperature;
	}
	public void setAmbientTemperature(String ambientTemperature) {
		this.ambientTemperature = ambientTemperature;
	}
	public String getAmbientHumidity() {
		return ambientHumidity;
	}
	public void setAmbientHumidity(String ambientHumidity) {
		this.ambientHumidity = ambientHumidity;
	}
	public String getTempleOperator() {
		return templeOperator;
	}
	public void setTempleOperator(String templeOperator) {
		this.templeOperator = templeOperator;
	}
	public String getTempleReviewer() {
		return templeReviewer;
	}
	public void setTempleReviewer(String templeReviewer) {
		this.templeReviewer = templeReviewer;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 4000)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得Template
	 *@return: Template  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate(){
		return this.template;
	}
	/**
	 *方法: 设置Template
	 *@param: Template  关联主表
	 */
	public void setTemplate(Template template){
		this.template = template;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the orderNum
	 */
	public int getOrderNum() {
		return orderNum;
	}
	/**
	 * @param orderNum the orderNum to set
	 */
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	public String getTestUserList() {
		return testUserList;
	}
	public void setTestUserList(String testUserList) {
		this.testUserList = testUserList;
	}
	public String getEstimatedTime() {
		return estimatedTime;
	}
	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}
	
	@Override
	public TemplateItem clone() throws CloneNotSupportedException {
		return (TemplateItem) super.clone();
	}
	public String getBottleOrBag() {
		return bottleOrBag;
	}
	public void setBottleOrBag(String bottleOrBag) {
		this.bottleOrBag = bottleOrBag;
	}
	
}