package com.biolims.system.template.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2015-11-18 17:00:24
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_REAGENT_ITEM")
@SuppressWarnings("serial")
public class ReagentItem extends EntityDao<ReagentItem> implements
		java.io.Serializable,Cloneable {
	/** 试剂id */
	private String id;
	/** 试剂编号 */
	private String code;
	/** 试剂名称 */
	private String name;
	/** 试剂盒名称 */
	private String kitName;
	/** 批次 */
	private String batch;
	/** 是否通过检验 */
	private String isGood;
	/** 备注 */
	private String note;
	/** 关联主表 */
	private Template template;
	/** 单个用量*/
	private Double num;
	/** sn */
	private String sn;
	/** 关联步骤*/
	private String itemId;


	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂名称
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批次
	 */
	@Column(name = "BATCH", length = 50)
	public String getBatch() {
		return this.batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批次
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}

	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 关联主表
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * @return the kitName
	 */
	public String getKitName() {
		return kitName;
	}

	/**
	 * @param kitName
	 *            the kitName to set
	 */
	public void setKitName(String kitName) {
		this.kitName = kitName;
	}

	@Override
	public ReagentItem clone() throws CloneNotSupportedException {
		return (ReagentItem) super.clone();
	}
	

}