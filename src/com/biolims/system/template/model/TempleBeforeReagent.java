package com.biolims.system.template.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.system.nextFlow.model.NextFlow;

/**
 * @Title: Model
 * @Description: 实验模板工前准备试剂
 * @author lims-platform
 * @date 2019-03-18 17:00:44
 * @version V1.0  qingsong.liu
 */
@Entity
@Table(name = "SYS_BEFORE_REAGENT")
@SuppressWarnings("serial")
public class TempleBeforeReagent extends EntityDao<TempleBeforeReagent> implements
java.io.Serializable,Cloneable{
	/**
	 * 方法: 取得String
	 * @return: String 开始指令id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	private String id;
	/** 创建人 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	private User createUser;
	/** 创建日期 */
	@Column(name = "CREATE_DATE", length = 50)
	private Date createDate;
	/** 关联主表     模板明细 */
	private String templateItem;
	/** 关联步骤*/
	@Column(name = "item_id", length = 50)
	private String itemId;
	/** 导航栏的步骤*/
	@Column(name = "step_id", length = 50)
	private String stepId;
	/** 试剂名称*/
	@Column(name = "reagent_name", length = 50)
	private String reagentName;
	/** 试剂id*/
	@Column(name = "reagent_id", length = 50)
	private String reagentId;
	/** 准备量*/
	@Column(name = "reagent_num", length = 50)
	private String reagentNum;
	/** 单位*/
	@Column(name = "reagent_nuit", length = 50)
	private String reagentNuit;
	
	
	
	public String getReagentNuit() {
		return reagentNuit;
	}
	public void setReagentNuit(String reagentNuit) {
		this.reagentNuit = reagentNuit;
	}
	public String getReagentName() {
		return reagentName;
	}
	public void setReagentName(String reagentName) {
		this.reagentName = reagentName;
	}
	public String getReagentId() {
		return reagentId;
	}
	public void setReagentId(String reagentId) {
		this.reagentId = reagentId;
	}
	public String getReagentNum() {
		return reagentNum;
	}
	public void setReagentNum(String reagentNum) {
		this.reagentNum = reagentNum;
	}
	public String getTemplateItem() {
		return templateItem;
	}
	public void setTemplateItem(String templateItem) {
		this.templateItem = templateItem;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
//	public Template getTemplateItem() {
//		return templateItem;
//	}
//	public void setTemplateItem(Template templateItem) {
//		this.templateItem = templateItem;
//	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public User getCreateUser() {
		return createUser;
	}
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public TempleBeforeReagent clone() throws CloneNotSupportedException {
		return (TempleBeforeReagent) super.clone();
	}
	
}
