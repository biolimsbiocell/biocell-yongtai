package com.biolims.system.template.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;

/**
 * @Title: Model
 * @Description: 常用设备
 * @author 刘青松
 * @date 2019-3-13 17:00:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_PRODUCING_COS")
@SuppressWarnings("serial")
public class TempleProducingCos extends EntityDao<TempleProducingCos> implements java.io.Serializable,Cloneable {
	/** id */
	private String id;
	/** 设备编号 */
	@Column(name="code")
	private String code;
	/** 设备名称 */
	@Column(name="type_name")
	private String typeName;
	/** 设备id */
	@Column(name="type_id")
	private String typeId;
	/** 是否通过检验 */
	@Column(name="is_good")
	private String isGood;
	/** 创建人ID*/
	@Column(name="create_user_id")
	private String createUserId;
	/** 创建人姓名*/
	@Column(name="create_user_name")
	private String createUserName;
	/** 创建人姓名 0正常使用   1已删除*/
	@Column(name="state")
	private String state;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/*TempleProducingCell 关联主表*/
	@Column(name="temple_producing_cell")
	private String templeProducingCell;
	public String getTempleProducingCell() {
		return templeProducingCell;
	}
	public void setTempleProducingCell(String templeProducingCell) {
		this.templeProducingCell = templeProducingCell;
	}
	public String getTypeId() {
		return typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	/**
	 * @return 设备类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	private DicType type;
	/**关联主表
	 * 方法: 取得Template
	 * @return: TempleProducingCell 关联主表
	 */
/*	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_producing_cell")*/
/*	@Column(name="temple_producing_cell")
	private TempleProducingCell templeProducingCell;*/
	/** 创建时间*/
	@Column(name="create_date")
	private String createDate;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备id
	 */
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIsGood() {
		return isGood;
	}
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}
	public DicType getType() {
		return type;
	}
	public void setType(DicType type) {
		this.type = type;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	@Override
	public TempleProducingCos clone() throws CloneNotSupportedException {
		return (TempleProducingCos) super.clone();
	}
	

}