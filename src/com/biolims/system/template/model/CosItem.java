package com.biolims.system.template.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;

/**
 * @Title: Model
 * @Description: 设备明细
 * @author lims-platform
 * @date 2015-11-18 17:00:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_COS_ITEM")
@SuppressWarnings("serial")
public class CosItem extends EntityDao<CosItem> implements java.io.Serializable,Cloneable {
	/** 设备id */
	private String id;
	/** 设备编号 */
	private String code;
	/** 设备名称 */
	private String name;
	/** 是否通过检验 */
	private String isGood;
	/** 关联主表 */
	private Template template;
	/** 设备类型*/
	private DicType type;
	// //是否占用
	// private String isUsed;
	//
	// public String getIsUsed() {
	// return isUsed;
	// }
	// public void setIsUsed(String isUsed) {
	// this.isUsed = isUsed;
	// }
	// 温度
	private Double temperature;
	// 转速
	private Double speed;
	// 时间
	private Double time;
	// 备注
	private String note;
	// 选择
	private Instrument instrument;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSTRUMENT")
	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	// 关联步骤的id
	private String itemId;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备名称
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}

	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 关联主表
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * @return the type
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	@Override
	public CosItem clone() throws CloneNotSupportedException {
		return (CosItem) super.clone();
	}
	
	
}