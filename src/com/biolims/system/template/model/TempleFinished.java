package com.biolims.system.template.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.system.nextFlow.model.NextFlow;

/**
 * @Title: Model
 * @Description: 实验模板最后一步完工清除
 * @author lims-platform
 * @date 2019-03-18 17:00:44
 * @version V1.0  qingsong.liu
 */
@Entity
@Table(name = "SYS_FINISHED")
@SuppressWarnings("serial")
public class TempleFinished extends EntityDao<TempleFinished> implements
java.io.Serializable,Cloneable{
	/**
	 * 方法: 取得String
	 * @return: String 实验模板最后一步完工清除id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	private String id;
	/** 排序 */
	@Column(name = "sort", length = 50)
	private String sort;
	/** 创建人 */
	private String createUser;
	/** 创建日期 */
	@Column(name = "CREATE_DATE", length = 50)
	private String createDate;
	/** 主表id*/
	@Column(name = "template_item", length = 50)
	private String templateItem;
	/** 关联步骤*/
	@Column(name = "order_num", length = 50)
	private String orderNum;
	/** 导航栏的步骤*/
	@Column(name = "step_id", length = 50)
	private String stepId;
	/** 生产检查    操作人*/
	@Column(name = "temple_operator", length = 50)
	private String templeOperator;
	/** 生产检查   复核人*/
	@Column(name = "temple_reviewer", length = 50)
	private String templeReviewer;
	/** 生产检查   自定义字段*/
	@Column(name = "content")
	private String content;
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getTemplateItem() {
		return templateItem;
	}
	public void setTemplateItem(String templateItem) {
		this.templateItem = templateItem;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public String getCreateDate() {
		return createDate;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getTempleOperator() {
		return templeOperator;
	}
	public void setTempleOperator(String templeOperator) {
		this.templeOperator = templeOperator;
	}
	public String getTempleReviewer() {
		return templeReviewer;
	}
	public void setTempleReviewer(String templeReviewer) {
		this.templeReviewer = templeReviewer;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	/** 关联主表     */
/*	@ManyToOne(fetch =FetchType.LAZY)
	@NotFound(action =NotFoundAction.IGNORE)
	@ForeignKey(name = "none")  @JoinColumn(name ="TEMPLATE_ITEM")
	public Template getTemplateItem() {
		return templateItem;
	}
	public void setTemplateItem(Template templateItem) {
		this.templateItem = templateItem;
	}*/
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
	@Override
	public TempleFinished clone() throws CloneNotSupportedException {
		return (TempleFinished) super.clone();
	}
	
}
