package com.biolims.system.template.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.DicSampleType;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.model.Storage;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.syscode.model.CodeMain;

/**
 * @Title: Model
 * @Description: 实验模版
 * @author lims-platform
 * @date 2015-11-18 17:00:44
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_TEMPLATE")
@SuppressWarnings("serial")
public class Template extends EntityDao<Template> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	private String name;
	/** 实验类型 */
	private NextFlow testType;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 质控品个数 */
	private Integer qcNum;
	/** 样本用量 */
	private Double sampleNum;
	/** 步骤数 */
	private Double stepsNum;
	/** 状态id */
	private String state;
	/** 是否混样 */
	private String isBlend;
	/** 是否按位点分样 */
	private String isSeparate;
	/** 工作流状态 */
	private String stateName;
	/** 条码模版 */
	private CodeMain codeMain;
	/** 容器 */
	private StorageContainer storageContainer;
	/** 实验天数 */
	private Integer duringDays;
	/** 实验提前预警天数 */
	private Integer remindDays;
	/**审核人*/
	
	private User confirmUser;
	
	/** 状态id */
	private String status;
	/**历史模板ID */
	private String historyId;
	
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */
	// 文件
	private FileInfo fileInfo;

	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 中间产物类型id */
	private String dicSampleTypeId;
	/** 中间产物类型name */
	private String dicSampleTypeName;
	/** 中间产物数量 */
	private String productNum;
	/** 中间产物数量 */
	private String productNum1;
	// 实验组
	private UserGroup acceptUser;
	/** 模板包含的字段 */
	private String templateFields;
	/** 字段 函数名 */
	private String templateFieldsCode;
	/** 子表 模板包含的字段 */
	private String templateFieldsItem;
	/** 子表 字段 函数名 */
	private String templateFieldsItemCode;
	/**模块类型*/
	private String moduleType;
	
	//新加字段
	//样本类型
	private Storage storage;
	//是否是试剂 0 是     1否
	@Column(name="right_storage")
	private String rightStorage;
	/**版本号*/
	private String versionNum;
	/**依据文件编号*/
	private String documentNum;
	/**依据文件名称*/
	private String documentName;
	/**用户区分检测模板 还是 生产模板 0:检测  1:生产*/
	private String type;
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVersionNum() {
		return versionNum;
	}

	public void setVersionNum(String versionNum) {
		this.versionNum = versionNum;
	}

	public String getDocumentNum() {
		return documentNum;
	}

	public void setDocumentNum(String documentNum) {
		this.documentNum = documentNum;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getRightStorage() {
		return rightStorage;
	}

	public void setRightStorage(String rightStorage) {
		this.rightStorage = rightStorage;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	//@ForeignKey(name = "none")
	@JoinColumn(name = "storage")
	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public String getTemplateFieldsItem() {
		return templateFieldsItem;
	}

	public void setTemplateFieldsItem(String templateFieldsItem) {
		this.templateFieldsItem = templateFieldsItem;
	}

	public String getTemplateFieldsItemCode() {
		return templateFieldsItemCode;
	}

	public void setTemplateFieldsItemCode(String templateFieldsItemCode) {
		this.templateFieldsItemCode = templateFieldsItemCode;
	}

	public String getTemplateFieldsCode() {
		return templateFieldsCode;
	}

	public void setTemplateFieldsCode(String templateFieldsCode) {
		this.templateFieldsCode = templateFieldsCode;
	}
	
	/**
	 * 方法: 取得UserGroup
	 * 
	 * @return: UserGroup 实验组
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "ACCEPT_USER")
	public UserGroup getAcceptUser() {
		return this.acceptUser;
	}

	public String getTemplateFields() {
		return templateFields;
	}

	public void setTemplateFields(String templateFields) {
		this.templateFields = templateFields;
	}

	/**
	 * 方法: 设置UserGroup
	 * 
	 * @param: UserGroup 实验组
	 */
	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "FILE_INFO")
	public FileInfo getFileInfo() {
		return fileInfo;
	}
	
	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the testType
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEST_TYPE")
	public NextFlow getTestType() {
		return testType;
	}

	/**
	 * @param testType the testType to set
	 */
	public void setTestType(NextFlow testType) {
		this.testType = testType;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getQcNum() {
		return qcNum;
	}

	public void setQcNum(Integer qcNum) {
		this.qcNum = qcNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "STORAGE_CONTAINER")
	public StorageContainer getStorageContainer() {
		return storageContainer;
	}
    
	public void setStorageContainer(StorageContainer storageContainer) {
		this.storageContainer = storageContainer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "CODE_MAIN")
	public CodeMain getCodeMain() {
		return codeMain;
	}

	public void setCodeMain(CodeMain codeMain) {
		this.codeMain = codeMain;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Integer getDuringDays() {
		return duringDays;
	}

	public void setDuringDays(Integer duringDays) {
		this.duringDays = duringDays;
	}

	public Integer getRemindDays() {
		return remindDays;
	}

	public void setRemindDays(Integer remindDays) {
		this.remindDays = remindDays;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	public String getDicSampleTypeId() {
		return dicSampleTypeId;
	}

	public void setDicSampleTypeId(String dicSampleTypeId) {
		this.dicSampleTypeId = dicSampleTypeId;
	}

	public String getDicSampleTypeName() {
		return dicSampleTypeName;
	}

	public void setDicSampleTypeName(String dicSampleTypeName) {
		this.dicSampleTypeName = dicSampleTypeName;
	}

	public String getProductNum1() {
		return productNum1;
	}

	public void setProductNum1(String productNum1) {
		this.productNum1 = productNum1;
	}

	/**
	 * @return the stepsNum
	 */
	public Double getStepsNum() {
		return stepsNum;
	}

	/**
	 * @param stepsNum the stepsNum to set
	 */
	public void setStepsNum(Double stepsNum) {
		this.stepsNum = stepsNum;
	}

	/**
	 * @return the isBlend
	 */
	public String getIsBlend() {
		return isBlend;
	}

	/**
	 * @param isBlend the isBlend to set
	 */
	public void setIsBlend(String isBlend) {
		this.isBlend = isBlend;
	}

	/**
	 * @return the isSeparate
	 */
	public String getIsSeparate() {
		return isSeparate;
	}

	/**
	 * @param isSeparate the isSeparate to set
	 */
	public void setIsSeparate(String isSeparate) {
		this.isSeparate = isSeparate;
	}

	public String getHistoryId() {
		return historyId;
	}

	public void setHistoryId(String historyId) {
		this.historyId = historyId;
	}


}