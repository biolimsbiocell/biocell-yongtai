package com.biolims.system.template.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.detecyion.model.SampleDeteyion;
/**
 * @Title: Model
 * @Description: 质检明细
 * @author lims-platform
 * @date 2015-11-18 17:00:24
 * @version V1.0
 */
@Entity
@Table(name = "ZHI_JIAN_ITEM")
@SuppressWarnings("serial")
public class ZhiJianItem extends EntityDao<ZhiJianItem> implements java.io.Serializable,Cloneable {
	/** 编号 */
	private String id;
	/**质检编号*/
	private String code;
	/** 名称 */
	private String name;
	/** 关联主表 */
	private Template template;
	/** 质检类型 */
	private String type;
	// 备注
	private String note;
	// 选择
	private SampleDeteyion sampleDeteyion;
	/** 关联步骤*/
	private String itemId;
	/**下一步id*/
	private String nextId;
	/**质检类型id*/
	private String typeId;
	/**质检类型名称*/
	private String typeName;
	/**样本量*/
	private String sampleNum;
	/**单位*/
	private String sampleNumUnit;
	/**样本名称*/
	private String sampleType;
	/**样本名称id*/
	private String sampleTypeId;
	/**0显示 1 删除*/
	private String state;
	
	/*TempleProducingCell 关联主表*/
	@Column(name="temple_producing_cell")
	private String templeProducingCell;
	public String getTempleProducingCell() {
		return templeProducingCell;
	}
	public void setTempleProducingCell(String templeProducingCell) {
		this.templeProducingCell = templeProducingCell;
	}
	

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getNextId() {
		return nextId;
	}

	public void setNextId(String nextId) {
		this.nextId = nextId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETEYION")
	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}

	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}

	@Override
	public ZhiJianItem clone() throws CloneNotSupportedException {
		return (ZhiJianItem) super.clone();
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getSampleNumUnit() {
		return sampleNumUnit;
	}
	public void setSampleNumUnit(String sampleNumUnit) {
		this.sampleNumUnit = sampleNumUnit;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getSampleTypeId() {
		return sampleTypeId;
	}
	public void setSampleTypeId(String sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}

	
}