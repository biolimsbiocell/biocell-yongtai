package com.biolims.system.template.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;

/**
 * @Title: Model
 * @Description: 生产实验操作
 * @author 刘青松
 * @date 2019-3-13 17:00:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_PRODUCING_CELL")
@SuppressWarnings("serial")
public class TempleProducingCell extends EntityDao<TempleProducingCell> implements java.io.Serializable,Cloneable {
	/** id */
	private String id;
	/** 生产步骤 */
	@Column(name="producing_name")
	private String producingName;
	/** 生产标题 */
	@Column(name="producing_title")
	private String producingTitle;
	/** 步骤*/
	@Column(name="order_num")
	private String orderNum;
	/** 关联主表 */
	@Column(name="template")
	private Template template;
	/** 创建时间*/
	@Column(name="create_date")
	private String createDate;
	/** 自定义字段*/
	@Column(name="custom_test")
	private String customTest;
	/** 创建人ID*/
	@Column(name="create_user_id")
	private String createUserId;
	/** 创建人姓名*/
	@Column(name="create_user_name")
	private String createUserName;
	
	//开始时间结束时间
	private String hideSmalleTime;
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCustomTest() {
		return customTest;
	}
	public void setCustomTest(String customTest) {
		this.customTest = customTest;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}
	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 关联主表
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}
	
	public String getProducingName() {
		return producingName;
	}
	public void setProducingName(String producingName) {
		this.producingName = producingName;
	}
	public String getProducingTitle() {
		return producingTitle;
	}
	public void setProducingTitle(String producingTitle) {
		this.producingTitle = producingTitle;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	@Override
	public TempleProducingCell clone() throws CloneNotSupportedException {
		return (TempleProducingCell) super.clone();
	}
	public String getHideSmalleTime() {
		return hideSmalleTime;
	}
	public void setHideSmalleTime(String hideSmalleTime) {
		this.hideSmalleTime = hideSmalleTime;
	}
	
}