package com.biolims.system.template.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.system.nextFlow.model.NextFlow;

/**
 * @Title: Model
 * @Description: 实验模板开始指令明细
 * @author lims-platform
 * @date 2019-03-18 17:00:44
 * @version V1.0  qingsong.liu
 */
@Entity
@Table(name = "SYS_NSTRUCTIONS")
@SuppressWarnings("serial")
public class TempleNstructions extends EntityDao<TempleNstructions> implements
java.io.Serializable,Cloneable{
	/**
	 * 方法: 取得String
	 * @return: String 开始指令id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	private String id;
	/** 指令名称 */
	@Column(name = "name", length = 50)
	private String name;
	/** 排序 */
	@Column(name = "sort", length = 50)
	private String sort;
	/** 创建人 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	private User createUser;
	/** 创建日期 */
	@Column(name = "CREATE_DATE", length = 50)
	private Date createDate;
	/** 关联主表     模板明细 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "template_item")
//	private Template templateItem;
	private String templateItem;
	/** 关联步骤*/
	@Column(name = "item_id", length = 50)
	private String itemId;
	/** 导航栏的步骤*/
	@Column(name = "step_id", length = 50)
	private String stepId;
	/** 生产检查*/
	@Column(name = "temple_production", length = 50)
	private String templeProduction;
	/** 操作记录*/
	@Column(name = "operation_note", length = 50)
	private String operationNote;
	/** 状态 0正常   1已删除*/
	@Column(name = "state", length = 50)
	private String state;
	
	
	
	public String getTemplateItem() {
		return templateItem;
	}
	public void setTemplateItem(String templateItem) {
		this.templateItem = templateItem;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTempleProduction() {
		return templeProduction;
	}
	public void setTempleProduction(String templeProduction) {
		this.templeProduction = templeProduction;
	}
	public String getOperationNote() {
		return operationNote;
	}
	public void setOperationNote(String operationNote) {
		this.operationNote = operationNote;
	}

	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
//	public Template getTemplateItem() {
//		return templateItem;
//	}
//	public void setTemplateItem(Template templateItem) {
//		this.templateItem = templateItem;
//	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public User getCreateUser() {
		return createUser;
	}
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public TempleNstructions clone() throws CloneNotSupportedException {
		return (TempleNstructions) super.clone();
	}
	
}
