package com.biolims.system.template.service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleBeforeReagent;
import com.biolims.system.template.model.TempleFinished;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TemplateService {
	@Resource
	private TemplateDao templateDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	// public Map<String, Object> findTemplateList(
	// Map<String, String> mapForQuery, Integer startNum,
	// Integer limitNum, String dir, String sort) {
	// return templateDao.selectTemplateList(mapForQuery, startNum, limitNum,
	// dir, sort);
	// }

	// public Map<String, Object> findTemplateListByType(
	// Map<String, String> mapForQuery, Integer startNum,
	// Integer limitNum, String dir, String sort, String type) {
	// return templateDao.selectTemplateListByType(mapForQuery, startNum,
	// limitNum, dir, sort, type);
	// }

	// 加载库存主数据
	// public Map<String, Object> findStorageList(Map<String, String>
	// mapForQuery,
	// Integer startNum, Integer limitNum, String dir, String sort) {
	// return templateDao.selectStorageList(mapForQuery, startNum, limitNum,
	// dir, sort);
	// }

	// 根据试剂号加载采购批次
	// public Map<String, Object> findStorageReagentBuySerialList(
	// Map<String, String> mapForQuery, Integer startNum,
	// Integer limitNum, String dir, String sort, String scId)
	// throws Exception {
	// Map<String, Object> result = templateDao
	// .selectStorageReagentBuySerialList(mapForQuery, startNum,
	// limitNum, dir, sort, scId);
	// List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>)
	// result
	// .get("list");
	// return result;
	// }

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Template i) throws Exception {

		templateDao.saveOrUpdate(i);

	}

	public Template get(String id) {
		Template template = commonDAO.get(Template.class, id);
		return template;
	}

	// public Map<String, Object> findTemplateItemList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = templateDao.selectTemplateItemList(scId,
	// startNum, limitNum, dir, sort);
	// List<TemplateItem> list = (List<TemplateItem>) result.get("list");
	// return result;
	// }

	// public Map<String, Object> findReagentItemList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = templateDao.selectReagentItemList(scId,
	// startNum, limitNum, dir, sort);
	// List<ReagentItem> list = (List<ReagentItem>) result.get("list");
	// return result;
	// }

	// public Map<String, Object> findCosItemList(String scId, Integer startNum,
	// Integer limitNum, String dir, String sort) throws Exception {
	// Map<String, Object> result = templateDao.selectCosItemList(scId,
	// startNum, limitNum, dir, sort);
	// List<CosItem> list = (List<CosItem>) result.get("list");
	// return result;
	// }

	// 根据关联item查询
	// public Map<String, Object> findReagentItemListByItemId(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort,
	// String itemId) throws Exception {
	// Map<String, Object> result = templateDao.selectReagentItemListByItemId(
	// scId, startNum, limitNum, dir, sort, itemId);
	// List<ReagentItem> list = (List<ReagentItem>) result.get("list");
	// return result;
	// }

	// public Map<String, Object> findCosItemListByItemId(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort,
	// String itemId) throws Exception {
	// Map<String, Object> result = templateDao.selectCosItemListByItemId(
	// scId, startNum, limitNum, dir, sort, itemId);
	// List<CosItem> list = (List<CosItem>) result.get("list");
	// return result;
	// }
	/**
	 * 
	 * @Title: saveTemplateItem @Description: 保存模板明细 @author : shengwei.wang @date
	 * 2018年2月11日下午6:57:37 @param sc @param itemDataJson @throws Exception
	 * void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTemplateItem(Template sc, String itemDataJson) throws Exception {
		List<TemplateItem> saveItems = new ArrayList<TemplateItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TemplateItem scp = new TemplateItem();
			// 将map信息读入实体类
			scp = (TemplateItem) templateDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTemplate(sc);

			saveItems.add(scp);
		}
		templateDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: delTemplateItem @Description:删除步骤 @author : shengwei.wang @date
	 * 2018年2月11日下午6:30:42 @param id @param itemId @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItem(String id, String itemId) throws Exception {
		Template t = commonDAO.get(Template.class, id);
		t.setStepsNum(t.getStepsNum() - 1);
		TemplateItem ti = commonDAO.get(TemplateItem.class, itemId);
		if (ti != null) {
			int orderNum = ti.getOrderNum();
			List<ReagentItem> ri = templateDao.getReagentItem(id, String.valueOf(orderNum));
			List<CosItem> ci = templateDao.getCosItem(id, String.valueOf(orderNum));
			List<TemplateItem> tiNext = templateDao.getNextTemplateItem(id, orderNum);
			List<ReagentItem> riNext = templateDao.getNextReagent(id, orderNum);
			List<CosItem> ciNext = templateDao.getNextCos(id, orderNum);
			for (TemplateItem tii : tiNext) {
				tii.setOrderNum(tii.getOrderNum() - 1);
				templateDao.saveOrUpdate(tii);
			}
			for (ReagentItem rii : riNext) {
				rii.setItemId(String.valueOf(Integer.valueOf(rii.getItemId()) - 1));
				templateDao.saveOrUpdate(rii);
			}
			for (CosItem cii : ciNext) {
				cii.setItemId(String.valueOf(Integer.valueOf(cii.getItemId()) - 1));
				templateDao.saveOrUpdate(cii);
			}

			commonDAO.deleteAll(ri);
			commonDAO.deleteAll(ci);
			commonDAO.delete(ti);
		}
	}
	
	/**
	 * 
	 * 删除生产操作
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTempleProducingCell(String id) throws Exception {
		TempleProducingCell ti = commonDAO.get(TempleProducingCell.class, id);
		if (ti != null) {
			// 获取试剂列表
			List<TempleProducingReagent> listReagent = getTempleProducingReagentListPage(ti.getId());
			if(listReagent.size()>0){
				for(TempleProducingReagent Reagent:listReagent){
					commonDAO.delete(Reagent);
				}
			}
			// 获取设备主表
			List<TempleProducingCos> listCos = getTempleProducingCosListPage(ti.getId());
			if(listCos.size()>0){
				for(TempleProducingCos cos:listCos){
					commonDAO.delete(cos);
				}
			}
			commonDAO.delete(ti);
		}
	}

	/**
	 * 
	 * @Title: saveReagentItem @Description: 保存试剂 @author : shengwei.wang @date
	 * 2018年2月11日下午6:45:45 @param sc @param itemDataJson @throws Exception
	 * void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReagentItem(Template sc, String itemDataJson) throws Exception {
		List<ReagentItem> saveItems = new ArrayList<ReagentItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ReagentItem scp = new ReagentItem();
			// 将map信息读入实体类
			scp = (ReagentItem) templateDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTemplate(sc);

			saveItems.add(scp);
		}
		templateDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: delReagentItem @Description:删除试剂 @author : shengwei.wang @date
	 * 2018年2月11日下午6:57:53 @param ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItem(String[] ids) throws Exception {
		for (String id : ids) {
			ReagentItem scp = templateDao.get(ReagentItem.class, id);
			templateDao.delete(scp);
		}
	}

	/**
	 * 
	 * @Title: saveCosItem @Description: 保存设备 @author : shengwei.wang @date
	 * 2018年2月11日下午6:46:26 @param sc @param itemDataJson @throws Exception
	 * void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCosItem(Template sc, String itemDataJson) throws Exception {
		List<CosItem> saveItems = new ArrayList<CosItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CosItem scp = new CosItem();
			// 将map信息读入实体类
			scp = (CosItem) templateDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTemplate(sc);

			saveItems.add(scp);
		}
		templateDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: delCosItem @Description: 删除设备 @author : shengwei.wang @date
	 * 2018年2月11日下午6:58:08 @param ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCosItem(String[] ids) throws Exception {
		for (String id : ids) {
			CosItem scp = templateDao.get(CosItem.class, id);
			templateDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Template sc, String changeLog,String log) throws Exception {
		if (sc != null) {
			templateDao.saveOrUpdate(sc);
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("Template");
				li.setModifyContent(changeLog);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
			/*
			 * String jsonStr = ""; jsonStr = (String) jsonMap.get("templateItem"); if
			 * (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			 * saveTemplateItem(sc, jsonStr); } jsonStr = (String)
			 * jsonMap.get("reagentItem"); if (jsonStr != null && !jsonStr.equals("{}") &&
			 * !jsonStr.equals("")) { saveReagentItem(sc, jsonStr); } jsonStr = (String)
			 * jsonMap.get("cosItem"); if (jsonStr != null && !jsonStr.equals("{}") &&
			 * !jsonStr.equals("")) { saveCosItem(sc, jsonStr); }
			 */
		}
	}

	// // 根据模板ID加载子表明细
	// public List<Map<String, String>> setTemplateItem(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = templateDao.setTemplateItem(code);
	// List<TemplateItem> list = (List<TemplateItem>) result.get("list");
	// if (list != null && list.size() > 0) {
	// for (TemplateItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("code", ti.getCode());
	// map.put("name", ti.getName());
	// map.put("note", ti.getNote());
	// map.put("tId", ti.getTemplate().getId());
	// map.put("tName", ti.getTemplate().getName());
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }

	// public List<Map<String, String>> setTemplateReagent(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = templateDao.setTemplateReagent(code);
	// List<ReagentItem> list = (List<ReagentItem>) result.get("list");
	// if (list != null && list.size() > 0) {
	// for (ReagentItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("code", ti.getCode());
	// map.put("name", ti.getName());
	// map.put("batch", ti.getBatch());
	// map.put("isGood", ti.getIsGood());
	// map.put("note", ti.getNote());
	// map.put("sn", ti.getSn());
	// if (ti.getNum() != null) {
	// map.put("note", ti.getNum().toString());
	// } else {
	// map.put("note", "");
	// }
	// map.put("itemId", ti.getItemId());
	// map.put("tId", ti.getTemplate().getId());
	// map.put("tName", ti.getTemplate().getName());
	// if (ti.getNum() != null) {
	// map.put("num", ti.getNum().toString());
	// } else {
	// map.put("num", "");
	// }
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }

	// // 根据选中的步骤查询相关的试剂明细
	// public List<Map<String, String>> setReagent(String id, String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = templateDao.setReagent(id, code);
	// List<ReagentItem> list = (List<ReagentItem>) result.get("list");
	// if (list != null && list.size() > 0) {
	// for (ReagentItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("code", ti.getCode());
	// map.put("name", ti.getName());
	// map.put("batch", ti.getBatch());
	// map.put("isGood", ti.getIsGood());
	// map.put("note", ti.getNote());
	// if (ti.getNum() != null) {
	// map.put("num", ti.getNum().toString());
	// } else {
	// map.put("num", "");
	// }
	// map.put("itemId", ti.getItemId());
	// map.put("tId", ti.getTemplate().getId());
	// map.put("tName", ti.getTemplate().getName());
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }

	// // 根据选中的步骤查询相关的设备明细
	// public List<Map<String, String>> setCos(String id, String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = templateDao.setCos(id, code);
	// List<CosItem> list = (List<CosItem>) result.get("list");
	// if (list != null && list.size() > 0) {
	// for (CosItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// // map.put("code",ti.getCode());
	// // map.put("name",ti.getName());
	// map.put("isGood", ti.getIsGood());
	// if (ti.getTemperature() != null) {
	// map.put("temperature", ti.getTemperature().toString());
	// } else {
	// map.put("temperature", "");
	// }
	//
	// if (ti.getSpeed() != null) {
	// map.put("speed", ti.getSpeed().toString());
	// } else {
	// map.put("speed", "");
	// }
	//
	// if (ti.getTime() != null) {
	// map.put("time", ti.getTime().toString());
	// } else {
	// map.put("time", "");
	// }
	//
	// map.put("instrumentId", ti.getInstrument().getId());
	// map.put("instrumentName", ti.getInstrument().getNote());
	// map.put("itemId", ti.getItemId());
	// map.put("note", ti.getNote());
	// map.put("tId", ti.getTemplate().getId());
	// map.put("tName", ti.getTemplate().getName());
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }

	// public List<Map<String, String>> setTemplateCos(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = templateDao.setTemplateCos(code);
	// List<CosItem> list = (List<CosItem>) result.get("list");
	// if (list != null && list.size() > 0) {
	// for (CosItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// /*
	// * map.put("code", ti.getInstrument().getId()); map.put("name",
	// * ti.getInstrument().getNote());
	// */
	// map.put("isGood", ti.getIsGood());
	// map.put("itemId", ti.getItemId());
	// map.put("note", ti.getNote());
	// map.put("itemId", ti.getItemId());
	// //
	// if (ti.getTemperature() != null) {
	// map.put("temperature", ti.getTemperature().toString());
	// } else {
	// map.put("temperature", "");
	// }
	//
	// if (ti.getSpeed() != null) {
	// map.put("speed", ti.getSpeed().toString());
	// } else {
	// map.put("speed", "");
	// }
	// if (ti.getTime() != null) {
	// map.put("time", ti.getTime().toString());
	// } else {
	// map.put("time", "");
	// }
	// if (ti.getType() != null) {
	// map.put("typeId", ti.getType().getId());
	// map.put("typeName", ti.getType().getName());
	// } else {
	// map.put("typeId", "");
	// map.put("typeName", "");
	// }
	// // map.put("speed", "");
	// // map.put("time", "");
	// //
	// map.put("tId", ti.getTemplate().getId());
	// map.put("tName", ti.getTemplate().getName());
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }

	// 实验保存的时候设置设备的占用状态
	public void setCosIsUsedSave(String code) {
		// <---------------- 保存的时候设备占用---------------->
		List<Template> list = templateDao.tempList(code);
		for (Template ti : list) {

			List<CosItem> cList = templateDao.setCosList(ti.getId());
			if (cList.size() > 0) {
				for (CosItem ci : cList) {
					ci.getInstrument().setIsFull("1");
				}
			}
		}
	}

	// // 实验保存的时候设置设备的占用状态
	public void setCosIsUsedOver(String code) {
		// // <---------------- 完成的时候设备解除占用---------------->
		// List<Template> list = templateDao.tempList(code);
		// for (Template ti : list) {
		// List<CosItem> cList = templateDao.setCosList(ti.getId());
		// if (cList.size() > 0) {
		// for (CosItem ci : cList) {
		// ci.getInstrument().setIsFull("0");
		// }
		// }
		// }
	}

	// // 根据试剂编号查询试剂明细
	// public Map<String, Object> findReagentItemListByCode(String tid,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = templateDao.selectReagentItemListByCode(
	// tid, startNum, limitNum, dir, sort);
	// List<ReagentItem> list = (List<ReagentItem>) result.get("list");
	// return result;
	// }

	// public Map<String, Object> findReagentItemListByCode(Map<String, String>
	// mapForQuery, Integer startNum,
	// Integer limitNum, String dir, String sort) {
	// return templateDao.selectTemplateList(mapForQuery, startNum, limitNum,
	// dir, sort);
	// }

	public Map<String, Object> findBillTemplateDefineList(String taName, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> result = templateDao.selectBillTemplateDefine(taName, start, length, query, col, sort);
		return result;

	}

	// 根据模板ID查询实验模板
	public List<Template> finTemplateTask(String templateId) throws Exception {
		return templateDao.selTemplateTask(templateId);
	}

	public List<Template> showDialogTemplateSCTableJson(String flag, String mark) throws Exception {
		List<Template> list = templateDao.showDialogTemplateSCTableJson(flag, mark);
		return list;
	}

	public List<Template> showDialogTemplateTableJson(String flag, String mark) throws Exception {
		List<Template> list = templateDao.showDialogTemplateTableJson(flag, mark);
		return list;
	}

	/**
	 * 
	 * @Title: getTemplateItem @Description: 获取模板明细 @author : shengwei.wang @date
	 * 2018年2月11日上午11:53:55 @param id @param orderNum @return
	 * List<TemplateItem> @throws
	 */
	public List<TemplateItem> getTemplateItem(String id, String orderNum) {
		List<TemplateItem> list = templateDao.getTemplateItem(id, orderNum);
		return list;
	}

	/**
	 * 
	 * @Title: getTemplateItem @Description: 获取第四步完工清場 @author : 刘青松 @date
	 * 2019年3月15日上午11:53:55 @param id @param orderNum @return
	 * List<TemplateItem> @throws
	 */
	public List<TempleFinished> getTempleFinished(String id, String orderNum) {
		List<TempleFinished> list = templateDao.getTempleFinished(id, orderNum);
		return list;
	}

	/**
	 * 
	 * @Title: showContentTableJson @Description: 显示自定义字段 @author :
	 * shengwei.wang @date 2018年2月11日下午6:48:18 @param id @param itemId @return
	 * Map<String,Object> @throws
	 */
	public Map<String, Object> showContentTableJson(String id, String itemId) {
		return templateDao.showContentTableJson(id, itemId);
	}

	/**
	 * 
	 * @throws Exception 
	 * @Title: showTemplateTableJson @Description: 显示模板列表 @author :
	 * shengwei.wang @date 2018年2月11日下午6:49:58 @param start @param length @param
	 * query @param col @param sort @return Map<String,Object> @throws
	 */
	public Map<String, Object> showTemplateTableJson(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		return templateDao.showTemplateTableJson(start, length, query, col, sort, type);
	}

	/**
	 * 
	 * @Title: getReagentItem @Description: 展示试剂 @author : shengwei.wang @date
	 * 2018年2月11日下午6:50:38 @param id @param orderNum @return
	 * List<ReagentItem> @throws
	 */
	public List<ReagentItem> getReagentItem(String id, String orderNum) {
		return templateDao.getReagentItem(id, orderNum);
	}

	/**
	 * 
	 * @Title: getReagentItem @Description: 展示设备 @author : shengwei.wang @date
	 * 2018年2月11日下午6:50:38 @param id @param orderNum @return
	 * List<ReagentItem> @throws
	 */
	public List<CosItem> getCosItem(String id, String orderNum) {
		return templateDao.getCosItem(id, orderNum);
	}

	/**
	 * 
	 * @Title: saveItem @Description: 保存明细 @author : shengwei.wang @date
	 * 2018年2月11日下午6:52:21 @param id @param orderNum @param itemJson @param
	 * reagentJson @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(String id, String orderNum, String itemJson, String reagentJson, String cosJson,
			String zjianJson) {
		Template t = commonDAO.get(Template.class, id);
		if (itemJson != null && !itemJson.equals("{}") && !itemJson.equals("")) {
			saveTemplate(t, orderNum, itemJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}") && !reagentJson.equals("")) {
			saveReagent(t, orderNum, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(t, orderNum, cosJson);
		}
		if (zjianJson != null && !zjianJson.equals("{}") && !zjianJson.equals("")) {
			saveZjian(t, orderNum, zjianJson);
		}
	}

	/**
	 * 保存质检项目 @Title: saveZjian @Description: TODO @param @param t @param @param
	 * orderNum @param @param zjianJson @return void @author 孙灵达 @date
	 * 2018年9月10日 @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveZjian(Template t, String orderNum, String zjianJson) {
		JSONArray array = JSONArray.fromObject(zjianJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String zjId = (String) object.get("id");
			ZhiJianItem ptz = null;
			if (zjId != null && !"".equals(zjId)) {
				ptz = commonDAO.get(ZhiJianItem.class, zjId);
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptz.setName(name);
				}
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptz.setCode(code);
				}
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					ptz.setTypeId(typeId);
				}
				String typeName = (String) object.get("typeName");
				if (typeName != null && !"".equals(typeName)) {
					ptz.setTypeName(typeName);
				}

			} else {
				ptz = new ZhiJianItem();
				if (orderNum != null && !"".equals(orderNum)) {
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptz.setName(name);
					}
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptz.setCode(code);
					}
					String typeId = (String) object.get("typeId");
					if (typeId != null && !"".equals(typeId)) {
						ptz.setTypeId(typeId);
					}
					String typeName = (String) object.get("typeName");
					if (typeName != null && !"".equals(typeName)) {
						ptz.setTypeName(typeName);
					}
					ptz.setItemId(orderNum);
				}
				ptz.setTemplate(t);

			}
			templateDao.saveOrUpdate(ptz);
		}

	}

	/**
	 * 
	 * @Title: saveCos @Description: 保存设备 @author : shengwei.wang @date
	 * 2018年2月11日下午6:53:14 @param t @param orderNum @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(Template t, String orderNum, String cosJson) {
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			CosItem ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(CosItem.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new CosItem();
				if (orderNum != null && !"".equals(orderNum)) {
					String typeId = (String) object.get("typeId");
					if (typeId != null && !"".equals(typeId)) {
						DicType dt = commonDAO.get(DicType.class, typeId);
						ptc.setType(dt);
					}
					ptc.setItemId(orderNum);
				}
				ptc.setTemplate(t);

			}
			templateDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: saveReagent @Description: 保存试剂 @author : shengwei.wang @date
	 * 2018年2月11日下午6:53:28 @param t @param orderNum @param reagentJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(Template t, String orderNum, String reagentJson) {
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			ReagentItem ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(ReagentItem.class, reagentId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptr.setName(name);
				}
				
				String num = (String) object.get("num");
				if (num != null && !"".equals(num)) {
					ptr.setNum(Double.valueOf(num));
				}else{
					ptr.setNum(null);
				}
			} else {
				ptr = new ReagentItem();
				if (orderNum != null && !"".equals(orderNum)) {
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptr.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					ptr.setItemId(orderNum);
					
					String num = (String) object.get("num");
					if (num != null && !"".equals(num)) {
						ptr.setNum(Double.valueOf(num));
					}else{
						ptr.setNum(null);
					}
				}
				ptr.setTemplate(t);

			}
			templateDao.saveOrUpdate(ptr);
		}
	}



	
	/**
	 * 
	 * @Title: saveTemplate @Description: 保存模板明细 @author : shengwei.wang @date
	 * 2018年2月11日下午6:53:48 @param t @param orderNum @param itemJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(Template t, String orderNum, String itemJson) {
		JSONArray array = JSONArray.fromObject("[" + itemJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		TemplateItem ti = null;
		if (templateId != null && !"".equals(templateId)) {
			ti = commonDAO.get(TemplateItem.class, templateId);
			String content = object.get("content").toString();
			if (content != null && !"".equals(content)) {
				ti.setContent(content);
			}
			String code = object.get("code").toString();
			if (code != null && !"".equals(code)) {
				ti.setCode(code);
			}
			String name = object.get("name").toString();
			if (name != null && !"".equals(name)) {
				ti.setName(name);
			}
			String note = object.get("note").toString();
			if (note != null && !"".equals(note)) {
				ti.setNote(note);
			}
			/*
			 * String testUserList = object.get("testUserList").toString(); if (testUserList
			 * != null && !"".equals(testUserList)) { ti.setTestUserList(testUserList); }
			 */
			String estimatedTime = object.get("estimatedTime").toString();
			if (estimatedTime != null && !"".equals(estimatedTime)) {
				ti.setEstimatedTime(estimatedTime);
			}
		} else {
			ti = new TemplateItem();
			if (orderNum != null && !"".equals(orderNum)) {
				String content = object.get("content").toString();
				if (content != null && !"".equals(content)) {
					ti.setContent(content);
				}
				String code = object.get("code").toString();
				if (code != null && !"".equals(code)) {
					ti.setCode(code);
				}
				String name = object.get("name").toString();
				if (name != null && !"".equals(name)) {
					ti.setName(name);
				}
				String note = object.get("note").toString();
				if (note != null && !"".equals(note)) {
					ti.setNote(note);
				}
				String estimatedTime = object.get("estimatedTime").toString();
				if (estimatedTime != null && !"".equals(estimatedTime)) {
					ti.setEstimatedTime(estimatedTime);
				}
				ti.setOrderNum(Integer.valueOf(orderNum));
			}
			ti.setTemplate(t);
		}
		templateDao.saveOrUpdate(ti);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCreateUserAndTime(String businessKey) {
		Template sct = commonDAO.get(Template.class, businessKey);
		if(sct!=null) {
			sct.setState("20");
			sct.setStateName("审核中");
			commonDAO.saveOrUpdate(sct);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCreateUserAndTimeSubmit(String businessKey) {
		Template sct = commonDAO.get(Template.class, businessKey);
		if(sct!=null) {
			sct.setState("20");
			sct.setStateName("提交人修改");
			commonDAO.saveOrUpdate(sct);
		}
	}
	public Map<String, Object> showTemplateDialogListJson(Integer start, Integer length, String query, String col,
			String sort, String type) {
		return templateDao.showTemplateDialogListJson(start, length, query, col, sort, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delZhijianItem(String[] ids) {
		for (String id : ids) {
			ZhiJianItem zhiJianItem = commonDAO.get(ZhiJianItem.class, id);
			commonDAO.delete(zhiJianItem);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ZhiJianItem> getZjItem(String id, String orderNum) {
		return templateDao.getZjItem(id, orderNum);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ZhiJianItem> getZjItemByOrderNum(String id, String orderNum) {
		return templateDao.getZjItemByOrderNum(id, orderNum);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ZhiJianItem> getZjItemNew(String id, String cellProductId) {
		return templateDao.getZjItemNew(id, cellProductId);
	}

	/* @title工期准备及检查 */
	public Map<String, Object> findTempleNstructionstableJson(Integer start, Integer length, String query, String col,
			String sort) {
		return templateDao.findTempleNstructionstableJson(start, length, query, col, sort);
	}

	/*
	 * @title 新的实验模板
	 * 
	 * @Data 2019-03-11
	 * 
	 * @author 刘青松
	 */

	/* 保存工期准备及检查 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public <T> void saveOrUpdateInfo(T bean) {
		Field[] inputFields = bean.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			try {
				Object olds = BeanUtils.getFieldValue(bean, fd.getName());
				String data = JsonUtils.toJsonString(olds);
				if (data.contains("{")) {
					String[] oo = data.split("\"");
					data = oo[3];
					if ("".equals(data) || data == null || "\"\"".equals(data)) {
						BeanUtils.setFieldValue(bean, fd.getName(), null);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		templateDao.saveOrUpdate(bean);
	}

	/**
	 * 
	 * @Title: findTempleNstructionsTableJson @Description: 实验前指令列表 @author :
	 * liuqingsong @date 2019年3月11日下午6:49:58 @param start @param length @param
	 * query @param col @param sort @return Map<String,Object> @throws
	 */
	public Map<String, Object> findTempleNstructionsTableJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) {
		return templateDao.findTempleNstructionsTableJson(id, itemId, start, length, query, col, sort);
	}
	
	public Map<String, Object> findTempleBeforeReagentTableJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) {
		return templateDao.findTempleBeforeReagentTableJson(id, itemId, start, length, query, col, sort);
	}

	
	/**
	 * 
	 * @Title: findTempleNstructionsTableJson @Description: 实验后指令列表 实验完工 @author :
	 * liuqingsong @date 2019年3月11日下午6:49:58 @param start @param length @param
	 * query @param col @param sort @return Map<String,Object> @throws
	 */
	public Map<String, Object> findTemplefinishedTableJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) {
		return templateDao.findTemplefinishedTableJson(id, itemId, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: getTemplateItem @Description: 获取模板明细 @author : liuqingsong @date
	 * 2019年3月11日上午11:53:55 @param id @param orderNum @return
	 * List<TemplateItem> @throws
	 */
	public TemplateItem getTemplateItemInfo(String id) {
		TemplateItem list = templateDao.get(TemplateItem.class, id);
		return list;
	}

	/**
	 * 
	 * @Title: getTempleFinishedInfo @Description: 获取第四步试验后指令 @author :
	 * liuqingsong @date 2019年3月11日上午11:53:55 @param id @param orderNum @return
	 * List<TemplateItem> @throws
	 */
	public TempleFinished getTempleFinishedInfo(String id) {
		TempleFinished list = templateDao.get(TempleFinished.class, id);
		return list;
	}

	/**
	 * 
	 * @Title: delReagentItem @Description:更新生产记录- 删除生产记录 @author :
	 * shengwei.wang @date 2018年2月11日下午6:57:53 @param ids @throws Exception
	 * void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void templeItemSaveOrUpdate(TemplateItem templateItemInfo) throws Exception {
		templateDao.saveOrUpdate(templateItemInfo);
	}

	/**
	 * 
	 * @Title: delete @Description:删除通用方法 @author : 刘青松 @date
	 * 2019年3月11日下午6:57:53 @param bean @throws Exception @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteListPageAll(TempleNstructions templeNstructions) {
		templateDao.saveOrUpdate(templeNstructions);
	}

	// 删除工后操作指令
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteListPageAll(TempleNstructionsEnd templeNstructionsEnd) {
		templateDao.saveOrUpdate(templeNstructionsEnd);
	}

	// 获取工期准备指令对象
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public TempleNstructions getTempleNstructions(String id) {
		TempleNstructions list = templateDao.get(TempleNstructions.class, id);
		return list;
	}

	// 通过模板主表id 获取工期准备指令list
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleNstructions> getTempleNstructionsListPage(String templeId) {
		return templateDao.getTempleNstructionsListPage(templeId);
	}
	
	// 通过模板主表id和步骤号   获取工期准备指令list
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleNstructions> getTempleNstructionsListPageAndOrderNum(String templeId,String orderNum) {
		return templateDao.getTempleNstructionsListPageAndOrderNum(templeId, orderNum);
	}
	
	// 通过模板主表id和实体id 获取工期准备指令list
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellProductionRecord> getCellProductionRecords(String templeId,String id) {
		return templateDao.getCellProductionRecords(templeId, id);
	}

	// 通过模板主表id 获取完工指令list
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleNstructionsEnd> getTempleNstructionsEndListPage(String templeId) {
		return templateDao.getTempleNstructionsEndListPage(templeId);
	}
	
	// 通过模板主表id和步骤号  获取完工指令list
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleNstructionsEnd> getTempleNstructionsEndListPageAndOrderNum(String templeId, String orderNum) {
		return templateDao.getTempleNstructionsEndListPageAndOrderNum(templeId, orderNum);
	}

	// 获取完工指令对象
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public TempleNstructionsEnd getTempleNstructionsEnd(String id) {
		TempleNstructionsEnd list = templateDao.get(TempleNstructionsEnd.class, id);
		return list;
	}

	/**
	 * 
	 * @Title: delete @Description:删除質檢 @author : 刘青松 @date
	 * 2019年3月11日下午6:57:53 @param bean @throws Exception @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteQualityTesting(ZhiJianItem zhiJianItem) {
		templateDao.delete(zhiJianItem);
	}
	/**
	 * 
	 * @Title: delete @Description:删除質檢 @author : 刘青松 @date
	 * 2019年3月11日下午6:57:53 @param bean @throws Exception @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteQualityTestingNew(ZhiJianItem zhiJianItem) {
		templateDao.saveOrUpdate(zhiJianItem);
	}

	/**
	 * 
	 * @Title: delete @Description:删除试剂 @author : 刘青松 @date
	 * 2019年3月11日下午6:57:53 @param bean @throws Exception @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteTempleReagent(TempleProducingReagent templeReage) {
		templateDao.saveOrUpdate(templeReage);
	}

	/**
	 * 
	 * @Title: delete @Description:删除设备 @author : 刘青松 @date
	 * 2019年3月11日下午6:57:53 @param bean @throws Exception @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteTempleCos(TempleProducingCos templeCos) {
		templateDao.saveOrUpdate(templeCos);
	}

	/**
	 * 用主键检索记录,通用DAO
	 * 
	 * @param persistentClass
	 *            实体类
	 * @param id
	 *            检索主键字段值
	 * @return T，实体类数据
	 */
	public <T> T getObjectBean(Class<T> persistentClass, Serializable id) {
		T t = (T) templateDao.get(persistentClass, id);
		return t;
	}

	/**
	 * @param
	 * @param 通过模板主表获取生产模板List主表数据
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingCell> getTempleProducingCellListPage(String id, String orderNum) {
		return templateDao.getTempleProducingCellListPage(id, orderNum);
	}

	/**
	 * @param
	 * @param 通过主数据id
	 *            试剂List主表数据
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingReagent> getTempleProducingReagentListPage(String id) {
		return templateDao.getTempleProducingReagentListPage(id);
	}
	
	/**
	 * @param
	 * @param 通过主数据id
	 *            试剂List主表数据
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingReagent> getTempleProducingReagentListPage1(String id) {
		return templateDao.getTempleProducingReagentListPage1(id);
	}

	/**
	 * @param
	 * @param 通过主数据id
	 *            试剂List主表数据
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingReagent> getTempleProducingReagentListPages(String id) {
		return templateDao.getTempleProducingReagentListPages(id);
	}

	/**
	 * @param
	 * @param 通过主数据id
	 *            设备List主表数据
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingCos> getTempleProducingCosListPage(String id) {
		return templateDao.getTempleProducingCosListPage(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingCos> getTempleProducingCosListPage2(String id) {
		return templateDao.getTempleProducingCosListPage2(id);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ZhiJianItem> getTempleProducingZhiJianItem1(String id) {
		return templateDao.getTempleProducingZhiJianItem1(id);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ZhiJianItem> getTempleProducingZhiJianItem(String id) {
		return templateDao.getTempleProducingZhiJianItem(id);
	}
	/**
	 * @param
	 * @param 通过主数据id
	 *            获取模板明细list
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TemplateItem> getTemplateItemListPage(String id) {
		return templateDao.getTemplateItemListPage(id);
	}
	
	/**
	 * @param 实验模板从当前实验取值
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellPassageTemplate> getCellPassageTemplateListPage(String id) {
		return templateDao.getCellPassageTemplateListPage(id);
	}

	/**
	 * @param
	 * @param 通过主数据id和步骤号获取操作指令列表数据
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleNstructions> findTemNstructionsListPage(String id, String number) {
		return templateDao.findTemNstructionsListPage(id, number);
	}

	/**
	 * @param
	 * @param 通过id获取第三步操作模板
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingCell> findByIdTempleProducingCell(String id) {
		return templateDao.findByIdTempleProducingCell(id);
	}
	
	/**
	 * @param
	 * @param 通过id获取第三步操作模板
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingCell> findByIdTempleProducingCells(String id, String orderNum) {
		return templateDao.findByIdTempleProducingCells(id, orderNum);
	}
	
	/**
	 * @param
	 * @param 通过id和步骤编号 获取第三步操作模板
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleProducingCell> findByIdTempleProducingCellAndOrderNum(String id, String orderNum) {
		return templateDao.findByIdTempleProducingCellAndOrderNum(id, orderNum);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String contentId) {
		Template sct = commonDAO.get(Template.class, contentId);
		if(sct!=null) {
			sct.setState("1");
			sct.setStateName("完成");
			//通过历史模板建立的模板 完成后 历史模板作废
			if(!"".equals(sct.getHistoryId())&&sct.getHistoryId()!=null) {
				Template historySct = commonDAO.get(Template.class, sct.getHistoryId());
				if(historySct!=null) {
					historySct.setStatus("0");
					commonDAO.saveOrUpdate(historySct);
				}
				
			}
			commonDAO.saveOrUpdate(sct);
		}
	}
	/**
	 * @param
	 * @param 通过id获取第三步操作模板
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleFinished> findByTempleIdFinishedInfo(String id) {
		return templateDao.findByTempleIdFinishedInfo(id);
	}
	
	/**
	 * @param
	 * @param 通过id和步骤号 获取第三步操作模板
	 * @return T，实体类数据
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TempleFinished> findByTempleIdFinishedInfoAndOrderNum(String id,String orderNum) {
		return templateDao.findByTempleIdFinishedInfoAndOrderNum(id, orderNum);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveNewByHistory(Template template, String choseId) throws CloneNotSupportedException {
		if (template != null) {
			Template tem = templateDao.get(Template.class, choseId);
			template.setType("1");
			template.setHistoryId(choseId);
			templateDao.saveOrUpdate(template);
			if (tem != null) {
				List<TemplateItem> itemList = templateDao.findTemplateItemListByTemplateId(tem.getId());
				if (itemList != null && itemList.size() > 0) {
					for (TemplateItem item : itemList) {
						TemplateItem itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<TempleFinished> finishList = templateDao.findTemplateFinishedListByTemplateId(tem.getId());
				if (finishList != null && finishList.size() > 0) {
					for (TempleFinished item : finishList) {
						TempleFinished itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplateItem(template.getId());
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<TempleNstructions> strusList = templateDao.findTemplateNstructionsListByTemplateId(tem.getId());
				if (strusList != null && strusList.size() > 0) {
					for (TempleNstructions item : strusList) {
						TempleNstructions itemClone = item.clone();
						itemClone.setId(null);
//						itemClone.setTemplateItem(template);
						itemClone.setTemplateItem(template.getId());
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<TempleNstructionsEnd> endList = templateDao
						.findTemplateNstructionsEndListByTemplateId(tem.getId());
				if (endList != null && endList.size() > 0) {
					for (TempleNstructionsEnd item : endList) {
						TempleNstructionsEnd itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplateItem(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<TempleProducingCell> cellList = templateDao.findTemplateProducingCellListByTemplateId(tem.getId());
				if (cellList != null && cellList.size() > 0) {
					for (TempleProducingCell item : cellList) {
						TempleProducingCell itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						List<TempleProducingCos> cosList = templateDao.findTemplateProducingCosListById(item.getId());
						for (TempleProducingCos cos : cosList) {
							TempleProducingCos cosClone = cos.clone();
							cosClone.setId(null);
							cosClone.setTempleProducingCell(itemClone.getId());
							templateDao.saveOrUpdate(cosClone);
						}
						List<TempleProducingReagent> reagentList = templateDao
								.findTemplateProducingReagentListById(item.getId());
						for (TempleProducingReagent reagent : reagentList) {
							TempleProducingReagent reagentClone = reagent.clone();
							reagentClone.setId(null);
							reagentClone.setTempleProducingCell(itemClone.getId());
							templateDao.saveOrUpdate(reagentClone);
						}
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<ZhiJianItem> zjList = templateDao.findTemplateZhiJianItemListByTemplateId(tem.getId());
				if (zjList != null && zjList.size() > 0) {
					for (ZhiJianItem item : zjList) {
						ZhiJianItem itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveByHistory(Template template, String choseId) throws CloneNotSupportedException {
		if (template != null) {
			Template tem = templateDao.get(Template.class, choseId);
			template.setType("0");
			templateDao.saveOrUpdate(template);
			if (tem != null) {
				List<TemplateItem> itemList = templateDao.findTemplateItemListByTemplateId(tem.getId());
				if (itemList != null && itemList.size() > 0) {
					for (TemplateItem item : itemList) {
						TemplateItem itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<CosItem> cosList = templateDao.findTemplateCosItemListByTemplateId(tem.getId());
				if (cosList != null && cosList.size() > 0) {
					for (CosItem item : cosList) {
						CosItem itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<ReagentItem> reaList = templateDao.findTemplateReagentItemListByTemplateId(tem.getId());
				if (reaList != null && reaList.size() > 0) {
					for (ReagentItem item : reaList) {
						ReagentItem itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
				List<ZhiJianItem> zjList = templateDao.findTemplateZhiJianItemListByTemplateId(tem.getId());
				if (zjList != null && zjList.size() > 0) {
					for (ZhiJianItem item : zjList) {
						ZhiJianItem itemClone = item.clone();
						itemClone.setId(null);
						itemClone.setTemplate(template);
						templateDao.saveOrUpdate(itemClone);
					}
				}
			}

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void beforeReagentRefresh(String id) throws ParseException {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<TempleBeforeReagent> tbrs = commonService.get(TempleBeforeReagent.class, "templateItem", id);
		if(tbrs.size()>0) {
			commonDAO.deleteAll(tbrs);
		}
		TemplateItem tem = commonDAO.get(TemplateItem.class, id);
		if(tem!=null) {
			//先查生产操作
			List<TempleProducingCell> tpcs = templateDao.findTempleProducingCellByIdAndOrdernum(tem.getTemplate().getId(),tem.getOrderNum());
			//再通过生产操作查试剂表 state=1
			if(tpcs.size()>0) {
				String scids = "";
				for(TempleProducingCell tpc:tpcs) {
					if("".equals(scids)) {
						scids = scids+"'"+tpc.getId()+"'";
					}else {
						scids = scids+",'"+tpc.getId()+"'";
					}
				}
				//先查生产操作
				List<TempleProducingReagent> tprs = templateDao.findTempleProducingReagentById(scids);
				if(tprs.size()>0) {
					List<TempleBeforeReagent> list = new ArrayList<TempleBeforeReagent>();
					for(TempleProducingReagent tpr:tprs) {
						TempleBeforeReagent tbr = new TempleBeforeReagent();
						tbr.setId(null);
						tbr.setCreateDate(sdf1.parse(sdf1.format(new Date())));
						tbr.setCreateUser(u);
						tbr.setReagentId(tpr.getCode());
						tbr.setReagentName(tpr.getName());
						tbr.setReagentNuit(tpr.getUnit());
						tbr.setStepId(String.valueOf(tem.getOrderNum()));
						tbr.setTemplateItem(tem.getId());
						commonDAO.saveOrUpdate(tbr);
					}
//					commonDAO.saveOrUpdate(list);
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBeforeReagentRefresh(String id, String item, String logInfo) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<TempleBeforeReagent> saveItems = new ArrayList<TempleBeforeReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		TemplateItem pt = commonDAO.get(TemplateItem.class, id);
		TempleBeforeReagent scp = new TempleBeforeReagent();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (TempleBeforeReagent) templateDao.Map2Bean(map, scp);
			TempleBeforeReagent tbr = commonDAO.get(TempleBeforeReagent.class,
					scp.getId());
			tbr.setReagentNum(scp.getReagentNum());
			commonDAO.saveOrUpdate(tbr);
//			saveItems.add(tbr);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setState("3");
			li.setStateName("数据修改");
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
//		templateDao.saveOrUpdateAll(saveItems);
	}

	public List<TempleBeforeReagent> getTempleBeforeReagents(String id, String code) {
		return templateDao.getTempleBeforeReagents(id,code);
	}

}
