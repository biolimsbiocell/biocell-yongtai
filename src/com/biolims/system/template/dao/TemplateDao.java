package com.biolims.system.template.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleBeforeReagent;
import com.biolims.system.template.model.TempleFinished;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;

@Repository
@SuppressWarnings("unchecked")
public class TemplateDao extends BaseHibernateDao {
	public Map<String, Object> selectTemplateList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from Template where 1=1 ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state='1'";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Template> list = new ArrayList<Template>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTemplateItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from TemplateItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and template.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TemplateItem> list = new ArrayList<TemplateItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by" + castStrId("code") + "";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectReagentItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from ReagentItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and template.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联itemId查询Reagent
	public Map<String, Object> selectReagentItemListByItemId(String scId, Integer startNum, Integer limitNum,
			String dir, String sort, String itemId) throws Exception {
		String hql = "from ReagentItem where 1=1 and itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and template.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCosItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CosItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and template.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CosItem> list = new ArrayList<CosItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联的item查询
	public Map<String, Object> selectCosItemListByItemId(String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from CosItem where 1=1 and itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and template.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CosItem> list = new ArrayList<CosItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据类型查询模板主数据
	public Map<String, Object> selectTemplateListByType(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		String key = " ";
		String hql = " from Template where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = "  and testType='" + type + "' and state='1'";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Template> list = new ArrayList<Template>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 加载库存主数据
	public Map<String, Object> selectStorageList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from Storage where 1=1";

		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Storage> list = new ArrayList<Storage>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by studyType.id asc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 加载库存主数据
	public Map<String, Object> selectStorageReagentBuySerialList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String scId) {
		String key = " ";
		String hql = " from StorageReagentBuySerial where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and storage.id='" + scId + "'";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据试剂号加载采购试剂批次
	/*
	 * public Map<String, Object> selectStorageReagentBuySerialList(String scId,
	 * Integer startNum, Integer limitNum, String dir, String sort) throws Exception
	 * { String hql = "from StorageReagentBuySerial where 1=1 "; String key = ""; if
	 * (scId != null) key = key + " and storage.id='" + scId + "'"; Long total =
	 * (Long) this.getSession() .createQuery( "select count(*) " + hql +
	 * key).uniqueResult(); List<StorageReagentBuySerial> list = new
	 * ArrayList<StorageReagentBuySerial>(); if (total > 0) { if (dir != null &&
	 * dir.length() > 0 && sort != null && sort.length() > 0) { if
	 * (sort.indexOf("-") != -1) { sort = sort.substring(0, sort.indexOf("-")); }
	 * key = key + " order by " + sort + " " + dir; } if (startNum == null ||
	 * limitNum == null) { list = this.getSession().createQuery(hql + key).list(); }
	 * else { list = this.getSession().createQuery(hql + key)
	 * .setFirstResult(startNum).setMaxResults(limitNum) .list(); } } Map<String,
	 * Object> result = new HashMap<String, Object>(); result.put("total", total);
	 * result.put("list", list); return result; }
	 */

	// 根据主表ID加载子表明细数据
	public Map<String, Object> setTemplateItem(String code) {
		String hql = "from TemplateItem t where 1=1 and t.template='" + code + "'";
		String key = "order by" + castStrId("code") + "";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<TemplateItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	//
	public Map<String, Object> setTemplateReagent(String code) {
		String hql = "from ReagentItem t where 1=1 and t.template='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql).uniqueResult();
		List<ReagentItem> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据选中的步骤查询相关的试剂明细
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from ReagentItem t where 1=1 and t.template='" + id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql).uniqueResult();
		List<ReagentItem> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据选中的步骤查询相关的设备明细
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from CosItem t where 1=1 and t.template='" + id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql).uniqueResult();
		List<CosItem> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> setTemplateCos(String code) {
		String hql = "from CosItem t where 1=1 and t.template='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql).uniqueResult();
		List<CosItem> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据主数据Id查询试验模板
	public List<Template> tempList(String code) {
		String hql = " from Template t where 1=1 and t.id='" + code + "'";
		List<Template> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据模板主表ID查询试剂明细
	 * 
	 * @param code
	 * @return
	 */
	public List<ReagentItem> setReagentList(String code) {
		String hql = "from ReagentItem where 1=1 and template.id='" + code + "'";
		List<ReagentItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID查ReagentItem
	 * 
	 * @param code
	 * @return
	 */
	public List<ReagentItem> setReagentListById(String code) {
		String hql = "from ReagentItem where 1=1 and id='" + code + "'";
		List<ReagentItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据模板主表ID查询设备明细
	 * 
	 * @param code
	 * @return
	 */
	public List<CosItem> setCosList(String code) {
		String hql = "from CosItem where 1=1 and template.id='" + code + "'";
		List<CosItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据ID相关设备的信息
	public List<Instrument> findInstrument(String code) {
		String hql = "from Instrument t where 1=1 and id='" + code + "'";
		List<Instrument> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据试剂编号 查询试剂明细
	public Map<String, Object> selectReagentItemListByCode(String tid, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		// String hql =
		// "from ReagentItem t where 1=1 and t.code='"+code+"' and t.template =
		// '"+tid+"'";
		String hql = "from ReagentItem t where 1=1 and  t.template = '" + tid + "'";
		String key = "";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据试剂编号查询试剂明细
	// public List<ReagentItem> selectReagentByCode(String code,String
	// template){
	// String hql =
	// "from ReagentItem t where t.code = '"+code+"' and t.template =
	// '"+template+"'";
	// List<ReagentItem> list = this.getSession().createQuery(hql).list();
	// return list;
	// }
	// 查询试剂明细
	public List<ReagentItem> selectReagentByCode(String template) {
		String hql = "from ReagentItem t where  t.template = '" + template + "'";
		List<ReagentItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询模板字段
	public Map<String, Object> selectBillTemplateDefine(String taName, Integer start, Integer length, String query,
			String col, String sort) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from BillTemplateDefine where 1=1 and tableName='" + taName + "' ";
		;
		String key = "";
		// if(query!=null&&!"".equals(query)){
		// key=map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from BillTemplateDefine where 1=1 and tableName='" + taName + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Template> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 根据主表id判断结果是否已经生成
	public List<Template> selTemplateTask(String templateId) throws Exception {
		String hql = "from Template t where 1=1 and t.id = '" + templateId + "'";
		List<Template> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 生产模板
	public List<Template> showDialogTemplateSCTableJson(String flag, String mark) throws Exception {
		List<Template> list = new ArrayList<Template>();
		if (mark != null && !"".equals(mark)) {
			flag = flag + mark;
		}
		String countHql = "select count(*) from Template where 1=1 and status='1' and testType.mainTable.id='" + flag
				+ "' and type='1'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from Template where 1=1 and status='1' and testType.mainTable.id='" + flag + "' and type='1'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<Template> showDialogTemplateTableJson(String flag, String mark) throws Exception {
		List<Template> list = new ArrayList<Template>();
		if (mark != null && !"".equals(mark)) {
			flag = flag + mark;
		}
		String countHql = "select count(*) from Template where 1=1 and status='1' and testType.mainTable.id='" + flag
				+ "' and type='0'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from Template where 1=1 and status='1' and testType.mainTable.id='" + flag + "' and type='0'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<TemplateItem> getTemplateItem(String id, String orderNum) {

		List<TemplateItem> list = new ArrayList<TemplateItem>();
		String countHql = "select count(*) from TemplateItem where 1=1 and template.id='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and orderNum ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TemplateItem where 1=1 and template.id='" + id + "'";
			key += " order by orderNum ";
			list = getSession().createQuery(hql + key).list();

		}
		return list;

	}

	// 获取第四步完工清场
	public List<TempleFinished> getTempleFinished(String id, String orderNum) {
		List<TempleFinished> list = new ArrayList<TempleFinished>();
		String countHql = "select count(*) from TempleFinished where 1=1 and templateItem='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and orderNum ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TempleFinished where 1=1 and templateItem='" + id + "'";
			key += " order by orderNum ";
			list = getSession().createQuery(hql + key).list();

		}
		return list;
	}

	public Map<String, Object> showContentTableJson(String id, String itemId) {
		Map<String, Object> map = new HashMap<String, Object>();
		String content = null;
		String countHql = "select count(*) from TemplateItem where 1=1 and template.id='" + id + "'";
		String key = "";
		if (itemId != null && !"".equals(itemId)) {
			key += " and orderNum='" + itemId + "'";
		} else {
			key += " and orderNum='1'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "select content from TemplateItem where 1=1 and template.id='" + id + "'";
			content = (String) getSession().createQuery(hql + key).uniqueResult();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("data", content);
		}
		return map;
	}

	public Map<String, Object> showTemplateTableJson(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Template where 1=1";
		String key = " and status = '1'";
		if(query !=null && !"".equals(query)) {
			key += map2Where(query);
		}
		if (type != null && !"".equals(type)) {
			key += " and type = '" + type + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Template where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Template> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<ReagentItem> getReagentItem(String id, String orderNum) {
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		String countHql = "select count(*) from ReagentItem where 1=1 and template.id='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and itemId ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ReagentItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<CosItem> getCosItem(String id, String orderNum) {
		List<CosItem> list = new ArrayList<CosItem>();
		String countHql = "select count(*) from CosItem where 1=1 and template.id='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and itemId ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from CosItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<TemplateItem> getNextTemplateItem(String id, Integer orderNum) {

		List<TemplateItem> list = new ArrayList<TemplateItem>();
		String countHql = "select count(*) from TemplateItem where 1=1 and template.id='" + id + "'  and orderNum >'"
				+ orderNum + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TemplateItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();

		}
		return list;

	}

	public List<ReagentItem> getNextReagent(String id, Integer orderNum) {
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		String countHql = "select count(*) from ReagentItem where 1=1 and template.id='" + id + "' and itemId >'"
				+ orderNum + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ReagentItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<CosItem> getNextCos(String id, Integer orderNum) {
		List<CosItem> list = new ArrayList<CosItem>();
		String countHql = "select count(*) from CosItem where 1=1 and template.id='" + id + "' and itemId >'" + orderNum
				+ "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from CosItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Map<String, Object> showTemplateDialogListJson(Integer start, Integer length, String query, String col,
			String sort, String type) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Template where 1=1";
		String key = " and status = '1'";
		// if(query!=null&&!"".equals(query)){
		// key=map2Where(query);
		// }
		if (type != null && !"".equals(type)) {
			key += "and type = '" + type + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Template where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Template> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<ZhiJianItem> getZjItem(String id, String orderNum) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String countHql = "select count(*) from ZhiJianItem where 1=1 and template.id='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and itemId ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}
	public List<ZhiJianItem> getZjItemByOrderNum(String id, String orderNum) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String countHql = "select count(*) from ZhiJianItem where 1=1 and state!='1' and  template.id='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and itemId ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1and state!='1' and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}
	public List<ZhiJianItem> getZjItemNew(String id, String cellProductId) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String countHql = "select count(*) from ZhiJianItem where 1=1 and state!='1' and  template.id='" + id + "'";
		String key = "";
		if (cellProductId != null && !"".equals(cellProductId)) {
			key += " and templeProducingCell ='" + cellProductId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1 and state!='1' and  template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	// 根据模板父表id和步骤查询实验工期准备和检查
	public Map<String, Object> findTempleNstructionstableJson(Integer start, Integer length, String query, String col,
			String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TempleNstructions where 1=1";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from TempleNstructions where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Template> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 通过主表id查询实验前操作指令table列表
	public Map<String, Object> findTempleNstructionsTableJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TempleNstructions where 1=1 and state !='1' and templateItem='" + id// templateItem.id='"
																													// +
																													// id
				+ "'";
		String key = "and itemId='" + itemId + "'";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TempleNstructions where 1=1 and state !='1' and templateItem='" + id + "'";// templateItem.id='"
																											// + id +
																											// "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				// key += " order by " + col + " " + sort;
				// 比较特殊
				if ("desc".equals(sort)) {
					key += " order by id  asc";
				} else {
					key += " order by id " + sort;
				}
			}
			List<TempleNstructions> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	// 通过主表id查询实验前操作指令table列表
	public Map<String, Object> findTempleBeforeReagentTableJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) {
		
		List<TemplateItem> tis = findTemplateItem(id, itemId);
		
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TempleBeforeReagent where 1=1 and templateItem='" + tis.get(0).getId()// templateItem.id='"
																													// +
																													// id
				+ "'";
		String key = "";//" and itemId='" + itemId + "'";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TempleBeforeReagent where 1=1 and templateItem='" + tis.get(0).getId() + "'";// templateItem.id='"
																											// + id +
																											// "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				// key += " order by " + col + " " + sort;
				// 比较特殊
				if ("desc".equals(sort)) {
					key += " order by id  asc";
				} else {
					key += " order by id " + sort;
				}
			}
			List<TempleBeforeReagent> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	private List<TemplateItem> findTemplateItem(String id, String itemId) {
		List<TemplateItem> list = new ArrayList<TemplateItem>();
		String hql = "from TemplateItem where 1=1 and template.id='" + id + "' and orderNum = '"+itemId+"' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 第四步 通过主表id查询实验后操作指令table列表
	public Map<String, Object> findTemplefinishedTableJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TempleNstructionsEnd where 1=1 and state !='1' and templateItem.id='"
				+ id + "'";
		String key = "and orderNum ='" + itemId + "'";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TempleNstructionsEnd where 1=1 and state !='1' and templateItem.id ='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				// key += " order by " + col + " " + sort;
				// 比较特殊
				if ("desc".equals(sort)) {
					key += " order by id asc";
				} else {
					key += " order by id " + sort;
				}
			}
			List<TempleNstructionsEnd> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 通过步骤号和主表id查询第三步生产模板记录主表
	public List<TempleProducingCell> getTempleProducingCellListPage(String id, String orderNum) {
		List<TempleProducingCell> list = new ArrayList<TempleProducingCell>();
		String countHql = "select count(*) from TempleProducingCell where 1=1 and template.id='" + id + "'";
		String key = "";
		if (orderNum != null && !"".equals(orderNum)) {
			key += " and orderNum ='" + orderNum + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TempleProducingCell where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	// 通过模板主表id 获取试剂列表
	public List<TempleProducingReagent> getTempleProducingReagentListPage(String id) {
		List<TempleProducingReagent> list = new ArrayList<TempleProducingReagent>();
		String countHql = "select count(*) from TempleProducingReagent where 1=1 and state !='1' and templeProducingCell='"
				+ id + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TempleProducingReagent where 1=1 and state !='1' and templeProducingCell='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	// 通过模板主表id 获取试剂列表
	public List<TempleProducingReagent> getTempleProducingReagentListPage1(String id) {
		List<TempleProducingReagent> list = new ArrayList<TempleProducingReagent>();
		String countHql = "select count(*) from TempleProducingReagent where 1=1 and state !='1' and templeProducingCell "
				+ id + " ";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TempleProducingReagent where 1=1 and state !='1' and templeProducingCell " + id + " ";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}
	// 通过模板主表id 获取质检
	public List<ZhiJianItem> getTempleProducingZhiJianItem1(String id) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String countHql = "select count(*) from ZhiJianItem where 1=1 and  state!='1' and templeProducingCell "
				+ id + " ";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1 and  state!='1' and templeProducingCell " + id + " ";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}
	// 通过模板主表id 获取质检
	public List<ZhiJianItem> getTempleProducingZhiJianItem(String id) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String countHql = "select count(*) from ZhiJianItem where 1=1 and state!='1' and templeProducingCell='"+id+"'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1 and state!='1' and templeProducingCell='" + id + "' ";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	// 通过模板主表id 获取试剂列表
	public List<TempleProducingReagent> getTempleProducingReagentListPages(String id) {
		List<TempleProducingReagent> list = new ArrayList<TempleProducingReagent>();
//		String countHql = "select count(*) from TempleProducingReagent where 1=1 and state !='1' and templeProducingCell in("
//				+ id + ") group by code";
//		String key = "";
//		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
//		if (0l != sumCount) {
		String hql = "from TempleProducingReagent where 1=1 and state !='1' and templeProducingCell in (" + id
				+ ") group by code";
		list = getSession().createQuery(hql).list();
//		}
		return list;
	}

	// 通过模板主表id 获取设备列表
	public List<TempleProducingCos> getTempleProducingCosListPage(String id) {
		List<TempleProducingCos> list = new ArrayList<TempleProducingCos>();
		String countHql = "select count(*) from TempleProducingCos where 1=1 and state !='1' and templeProducingCell='"
				+ id + "'";
		String key = "";
		/*
		 * if (orderNum != null && !"".equals(orderNum)) { key += " and orderNum ='" +
		 * orderNum + "'"; }
		 */
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from TempleProducingCos where 1=1 and state !='1' and templeProducingCell='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}
	// 通过模板主表id 获取设备列表
		public List<TempleProducingCos> getTempleProducingCosListPage2(String id) {
			List<TempleProducingCos> list = new ArrayList<TempleProducingCos>();
			String countHql = "select count(*) from TempleProducingCos where 1=1 and state !='1' and templeProducingCell "
					+ id + " ";
			String key = "";
			/*
			 * if (orderNum != null && !"".equals(orderNum)) { key += " and orderNum ='" +
			 * orderNum + "'"; }
			 */
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if (0l != sumCount) {
				String hql = "from TempleProducingCos where 1=1 and state !='1' and templeProducingCell " + id + " ";
				list = getSession().createQuery(hql + key).list();
			}
			return list;
		}

	// 通过模板主表id查询主表信息
	public List<TemplateItem> getTemplateItemListPage(String id) {
		List<TemplateItem> list = new ArrayList<TemplateItem>();
		String key = " order by orderNum";
		String hql = "from TemplateItem where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id查找当前实验模板
	public List<CellPassageTemplate> getCellPassageTemplateListPage(String id) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String key = " order by orderNum";
		String hql = "from CellPassageTemplate where 1=1 and cellPassage.id='" + id
				+ "' and (state is null or state='1') ";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过模板主表id和步骤号查询操作指令
	public List<TempleNstructions> findTemNstructionsListPage(String id, String number) {
		List<TempleNstructions> list = new ArrayList<TempleNstructions>();
		String key = "and itemId='" + number + "'";
		String hql = "from TempleNstructions where 1=1 and templateItem='" + id + "'";// templateItem.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过模板主表id获取模板操作主表数据
	public List<TempleProducingCell> findByIdTempleProducingCell(String id) {
		List<TempleProducingCell> list = new ArrayList<TempleProducingCell>();
		String hql = "from TempleProducingCell where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id获取模板操作主表数据
	public List<TempleProducingCell> findByIdTempleProducingCells(String id, String orderNum) {
		List<TempleProducingCell> list = new ArrayList<TempleProducingCell>();
		String hql = "from TempleProducingCell where 1=1 and template.id='" + id + "' and orderNum='" + orderNum + "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id和步骤号 获取模板操作主表数据
	public List<TempleProducingCell> findByIdTempleProducingCellAndOrderNum(String id, String orderNum) {
		List<TempleProducingCell> list = new ArrayList<TempleProducingCell>();
		String hql = "from TempleProducingCell where 1=1 and template.id='" + id + "' and orderNum='" + orderNum + "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id获取模板操作主表数据
	public List<TempleNstructions> getTempleNstructionsListPage(String templeId) {
		List<TempleNstructions> list = new ArrayList<TempleNstructions>();
		String hql = "from TempleNstructions where 1=1 and state !='1' and templateItem='" + templeId + "'";// templateItem.id='"
																											// +
																											// templeId
																											// + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id和步骤号 获取模板操作主表数据
	public List<TempleNstructions> getTempleNstructionsListPageAndOrderNum(String templeId, String orderNum) {
		List<TempleNstructions> list = new ArrayList<TempleNstructions>();
		String hql = "from TempleNstructions where 1=1 and state !='1' and templateItem='" + templeId + "' and itemId='"
				+ orderNum + "' ";// templateItem.id='" + templeId + "' and itemId='"+orderNum+"' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id和主表id 获取模板操作主表数据
	public List<CellProductionRecord> getCellProductionRecords(String templeId, String id) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and templeItem.id='" + templeId + "' and cellPassage.id='"
				+ id + "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id获取完工清除 列表
	public List<TempleNstructionsEnd> getTempleNstructionsEndListPage(String templeId) {
		List<TempleNstructionsEnd> list = new ArrayList<TempleNstructionsEnd>();
		String hql = "from TempleNstructionsEnd where 1=1 and state !='1' and templateItem.id='" + templeId + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id和步骤号 获取完工清除 列表
	public List<TempleNstructionsEnd> getTempleNstructionsEndListPageAndOrderNum(String templeId, String orderNum) {
		List<TempleNstructionsEnd> list = new ArrayList<TempleNstructionsEnd>();
		String hql = "from TempleNstructionsEnd where 1=1 and state !='1' and templateItem.id='" + templeId
				+ "' and orderNum='" + orderNum + "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id 第四步 获取完工清除 生产检查
	public List<TempleFinished> findByTempleIdFinishedInfo(String templeId) {
		List<TempleFinished> list = new ArrayList<TempleFinished>();
		String hql = "from TempleFinished where 1=1 and templateItem='" + templeId + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过模板主表id和步骤号 第四步 获取完工清除 生产检查
	public List<TempleFinished> findByTempleIdFinishedInfoAndOrderNum(String templeId, String orderNum) {
		List<TempleFinished> list = new ArrayList<TempleFinished>();
		String hql = "from TempleFinished where 1=1 and templateItem='" + templeId + "' and orderNum='" + orderNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TemplateItem> findTemplateItemListByTemplateId(String id) {
		List<TemplateItem> list = new ArrayList<TemplateItem>();
		String hql = "from TemplateItem where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleFinished> findTemplateFinishedListByTemplateId(String id) {
		List<TempleFinished> list = new ArrayList<TempleFinished>();
		String hql = "from TempleFinished where 1=1 and templateItem='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleNstructions> findTemplateNstructionsListByTemplateId(String id) {
		List<TempleNstructions> list = new ArrayList<TempleNstructions>();
		String hql = "from TempleNstructions where 1=1 and templateItem='" + id + "'";// templateItem.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleNstructionsEnd> findTemplateNstructionsEndListByTemplateId(String id) {
		List<TempleNstructionsEnd> list = new ArrayList<TempleNstructionsEnd>();
		String hql = "from TempleNstructionsEnd where 1=1 and templateItem.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleProducingCell> findTemplateProducingCellListByTemplateId(String id) {
		List<TempleProducingCell> list = new ArrayList<TempleProducingCell>();
		String hql = "from TempleProducingCell where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleProducingCos> findTemplateProducingCosListById(String id) {
		List<TempleProducingCos> list = new ArrayList<TempleProducingCos>();
		String hql = "from TempleProducingCos where 1=1 and templeProducingCell='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleProducingReagent> findTemplateProducingReagentListById(String id) {
		List<TempleProducingReagent> list = new ArrayList<TempleProducingReagent>();
		String hql = "from TempleProducingReagent where 1=1 and templeProducingCell='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ZhiJianItem> findTemplateZhiJianItemListByTemplateId(String id) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String hql = "from ZhiJianItem where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CosItem> findTemplateCosItemListByTemplateId(String id) {
		List<CosItem> list = new ArrayList<CosItem>();
		String hql = "from CosItem where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentItem> findTemplateReagentItemListByTemplateId(String id) {
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		String hql = "from ReagentItem where 1=1 and template.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleProducingCell> findTempleProducingCellByIdAndOrdernum(String id, int orderNum) {
		List<TempleProducingCell> list = new ArrayList<TempleProducingCell>();
		String hql = "from TempleProducingCell where 1=1 and template.id='" + id + "' and orderNum='"+orderNum+"' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleProducingReagent> findTempleProducingReagentById(String scids) {
		List<TempleProducingReagent> list = new ArrayList<TempleProducingReagent>();
		String hql = "from TempleProducingReagent where 1=1 and templeProducingCell in (" + scids + ") and state='0' group by code ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<TempleBeforeReagent> getTempleBeforeReagents(String id, String code) {
		List<TempleBeforeReagent> list = new ArrayList<TempleBeforeReagent>();
		String hql = "from TempleBeforeReagent where 1=1 and templateItem='"+id+"' and reagentId='"+code+"' ";
		list = getSession().createQuery(hql).list();
		return list;
	}
}