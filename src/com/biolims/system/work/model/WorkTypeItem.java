package com.biolims.system.work.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 检测方法明细
 * @author lims-platform
 * @date 2015-11-03 18:10:38
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_WORK_TYPE_ITEM")
@SuppressWarnings("serial")
public class WorkTypeItem extends EntityDao<WorkTypeItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**物料包编号*/
	private String code;
	/**物料包名称*/
	private String name;
	/**描述*/
	private String description;
	/**备注*/
	private String note;
	/**相关主表*/
	private WorkType workType;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  物料包编号
	 */
	@Column(name ="CODE", length = 60)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  物料包编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  物料包名称
	 */
	@Column(name ="NAME", length = 60)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  物料包名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="DESCRIPTION", length = 120)
	public String getDescription(){
		return this.description;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setDescription(String description){
		this.description = description;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 120)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得WorkType
	 *@return: WorkType  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_TYPE")
	public WorkType getWorkType(){
		return this.workType;
	}
	/**
	 *方法: 设置WorkType
	 *@param: WorkType  相关主表
	 */
	public void setWorkType(WorkType workType){
		this.workType = workType;
	}
}