package com.biolims.system.work.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.system.product.model.Product;

/**
 * @Title: Model
 * @Description: 任务单
 * @author lims-platform
 * @date 2015-11-09 18:04:49
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_WORK_ORDER")
@SuppressWarnings("serial")
public class WorkOrder extends EntityDao<WorkOrder> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 审核日期 */
	private Date createDate;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 实验负责人 */
	private String experimentUser;
	/** 信息负责人 */
	private User informationUser;
	/** 检测项目Id */
	private String productId;
	// 检测项目
	private String productName;
	/** 实验周期 */
	private String experimentalPeriod;
	/** 分析类型 */
	private String analysisType;
	/** 分析周期 */
	private String analysisPeriod;
	/** 测序方式 */
	private String sequencingMethod;
	/** 测序读长 */
	private String sequencingReadLong;
	/** 测序平台 */
	private String sequencingPlatform;
	/** 单位 */
	private DicUnit unit;
	/** 是否解读 */
	private String whetherReading;
	/** 解读类型 */
	private DicType readType;

	/** 预期密度 */
	private String density;
	/** 引物 */
	private String yw;
	/** 流程图数据 */
	private String content;
	/** 数据量 */
	private Double amount;
	/** 混合比例 */
	private Double mixRatio;
	/** 系数 */
	private Double coefficient;
	/** ReadsOne */
	private String readsOne;
	/** ReadsTwo */
	private String readsTwo;
	/** Index1 */
	private String index1;
	/** Index2 */
	private String index2;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "READ_TYPE")
	public DicType getReadType() {
		return readType;
	}

	public void setReadType(DicType readType) {
		this.readType = readType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INFORMATION_USER")
	public User getInformationUser() {
		return informationUser;
	}

	public String getExperimentUser() {
		return experimentUser;
	}

	public void setExperimentUser(String experimentUser) {
		this.experimentUser = experimentUser;
	}

	public void setInformationUser(User informationUser) {
		this.informationUser = informationUser;
	}

	public String getExperimentalPeriod() {
		return experimentalPeriod;
	}

	public void setExperimentalPeriod(String experimentalPeriod) {
		this.experimentalPeriod = experimentalPeriod;
	}

	public String getAnalysisType() {
		return analysisType;
	}

	public void setAnalysisType(String analysisType) {
		this.analysisType = analysisType;
	}

	public String getAnalysisPeriod() {
		return analysisPeriod;
	}

	public void setAnalysisPeriod(String analysisPeriod) {
		this.analysisPeriod = analysisPeriod;
	}

	public String getSequencingMethod() {
		return sequencingMethod;
	}

	public void setSequencingMethod(String sequencingMethod) {
		this.sequencingMethod = sequencingMethod;
	}

	public String getSequencingReadLong() {
		return sequencingReadLong;
	}

	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}

	public String getSequencingPlatform() {
		return sequencingPlatform;
	}

	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT")
	public DicUnit getUnit() {
		return unit;
	}

	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getWhetherReading() {
		return whetherReading;
	}

	public void setWhetherReading(String whetherReading) {
		this.whetherReading = whetherReading;
	}

	public String getWhetherPlasmapheresis() {
		return whetherPlasmapheresis;
	}

	public void setWhetherPlasmapheresis(String whetherPlasmapheresis) {
		this.whetherPlasmapheresis = whetherPlasmapheresis;
	}

	/** 是否血浆分离 */
	private String whetherPlasmapheresis;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审核日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getDensity() {
		return density;
	}

	public void setDensity(String density) {
		this.density = density;
	}

	public String getYw() {
		return yw;
	}

	public void setYw(String yw) {
		this.yw = yw;
	}

	public Double getMixRatio() {
		return mixRatio;
	}

	public void setMixRatio(Double mixRatio) {
		this.mixRatio = mixRatio;
	}

	public String getReadsOne() {
		return readsOne;
	}

	public void setReadsOne(String readsOne) {
		this.readsOne = readsOne;
	}

	public String getReadsTwo() {
		return readsTwo;
	}

	public void setReadsTwo(String readsTwo) {
		this.readsTwo = readsTwo;
	}

	public String getIndex1() {
		return index1;
	}

	public void setIndex1(String index1) {
		this.index1 = index1;
	}

	public String getIndex2() {
		return index2;
	}

	public void setIndex2(String index2) {
		this.index2 = index2;
	}

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
}