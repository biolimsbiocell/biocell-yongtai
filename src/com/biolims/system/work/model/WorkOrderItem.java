package com.biolims.system.work.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 任务明细
 * @author lims-platform
 * @date 2015-11-09 18:04:34
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_WORK_ORDER_ITEM")
@SuppressWarnings("serial")
public class WorkOrderItem extends EntityDao<WorkOrderItem> implements
		java.io.Serializable {
	/** 任务明细id */
	private String id;
	/** 描述 */
	private String name;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private WorkOrder workOrder;

	/** 下一步ID */
	private String nextStepId;
	/** 下一步name */
	private String nextStepName;

	private ApplicationTypeTable applicationTypeTable;
	// 排序号
	private Integer sort;

	// 默认下一步ID
	private String dnextId;
	// 默认下一步name
	private String dnextName;
	//判断下一步流向是在明细还是在结果
	private String process;
	

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getDnextId() {
		return dnextId;
	}

	public void setDnextId(String dnextId) {
		this.dnextId = dnextId;
	}

	public String getDnextName() {
		return dnextName;
	}

	public void setDnextName(String dnextName) {
		this.dnextName = dnextName;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务明细id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得WorkOrder
	 * 
	 * @return: WorkOrder 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_ORDER")
	public WorkOrder getWorkOrder() {
		return this.workOrder;
	}

	/**
	 * 方法: 设置WorkOrder
	 * 
	 * @param: WorkOrder 相关主表
	 */
	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPLICATION_TYPE_TABLE_ID")
	public ApplicationTypeTable getApplicationTypeTable() {
		return applicationTypeTable;
	}

	public void setApplicationTypeTable(
			ApplicationTypeTable applicationTypeTable) {
		this.applicationTypeTable = applicationTypeTable;
	}

	public String getNextStepId() {
		return nextStepId;
	}

	public void setNextStepId(String nextStepId) {
		this.nextStepId = nextStepId;
	}

	public String getNextStepName() {
		return nextStepName;
	}

	public void setNextStepName(String nextStepName) {
		this.nextStepName = nextStepName;
	}

}