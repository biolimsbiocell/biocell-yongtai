package com.biolims.system.work.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
/**   
 * @Title: Model
 * @Description: 业务类型
 * @author lims-platform
 * @date 2015-11-03 18:10:48
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_WORK_TYPE")
@SuppressWarnings("serial")
public class WorkType extends EntityDao<WorkType> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**检测周期*/
	private Integer cycle;
	/**父级编号*/
	private WorkType parent;
	/**状态*/
	private String state;
	/**负责人*/
	private User person;
	/**标识码*/
	private String mark;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PERSON")
	public User getPerson() {
		return person;
	}
	public void setPerson(User person) {
		this.person = person;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  检测周期
	 */
	@Column(name ="CYCLE", length = 120)
	public Integer getCycle(){
		return this.cycle;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  检测周期
	 */
	public void setCycle(Integer cycle){
		this.cycle = cycle;
	}
	/**
	 *方法: 取得WorkType
	 *@return: WorkType  父级编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PARENT")
	public WorkType getParent(){
		return this.parent;
	}
	/**
	 *方法: 设置WorkType
	 *@param: WorkType  父级编号
	 */
	public void setParent(WorkType parent){
		this.parent = parent;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}