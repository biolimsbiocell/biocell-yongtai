package com.biolims.system.work.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.system.work.model.WorkType;
import com.biolims.system.work.model.WorkTypeItem;

@Repository
@SuppressWarnings("unchecked")
public class WorkTypeDao extends BaseHibernateDao {
	public Map<String, Object> selectWorkTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WorkType where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WorkType> list = new ArrayList<WorkType>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectWorkTypeItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from WorkTypeItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and workType.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<WorkTypeItem> list = new ArrayList<WorkTypeItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//ajax根据标识码加载检测方法
	public Map<String, Object> setWorkType(String code) throws Exception {
		String hql = "from WorkType t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.mark='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<WorkType> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//根据标识码加载检测方法，来查询相关项目
	public List<WorkType> selectWorkTypeList(String code){
		String hql = "from WorkType t where 1=1 and t.mark='"+code+"'";
		List<WorkType> list = this.getSession().createQuery(hql).list();
		return list;
	}
}