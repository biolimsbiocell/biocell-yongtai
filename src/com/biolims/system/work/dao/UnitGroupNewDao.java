package com.biolims.system.work.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.system.work.model.UnitGroupNew;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class UnitGroupNewDao extends BaseHibernateDao {
	public Map<String, Object> selectWorkTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from UnitGroupNew where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<UnitGroupNew> list = new ArrayList<UnitGroupNew>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	public Map<String, Object> selectWorkTypeList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UnitGroupNew where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=key +map2Where(query);
		}
//		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
//		if(!"all".equals(scopeId)){
//			key+=" and scopeId='"+scopeId+"'";
//		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from UnitGroupNew where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<UnitGroupNew> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
	public Map<String, Object> selectWorkTypeListByUnit(Integer start,
			Integer length, String query, String col, String sort,String unit) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UnitGroupNew where 1=1 and mark2.name='"+unit+"' ";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=key +map2Where(query);
		}
//		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
//		if(!"all".equals(scopeId)){
//			key+=" and scopeId='"+scopeId+"'";
//		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from UnitGroupNew where 1=1 and mark2.name='"+unit+"'";
//			String hql = "from UnitGroupNew where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<UnitGroupNew> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	
//		public Map<String, Object> selectWorkTypeItemList(String scId, Integer startNum, Integer limitNum,
//			String dir, String sort) throws Exception {
//		String hql = "from WorkTypeItem where 1=1 ";
//		String key = "";
//		if (scId != null)
//			key = key + " and workType.id='" + scId + "'";
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<WorkTypeItem> list = new ArrayList<WorkTypeItem>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			} 
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
//	//ajax根据标识码加载检测方法
//	public Map<String, Object> setWorkType(String code) throws Exception {
//		String hql = "from WorkType t where 1=1 ";
//		String key = "";
//		if (code != null)
//			key = key + " and  t.mark='" + code + "'";
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<WorkType> list = this.getSession().createQuery(hql + key).list();
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
//	//根据标识码加载检测方法，来查询相关项目
//	public List<WorkType> selectWorkTypeList(String code){
//		String hql = "from WorkType t where 1=1 and t.mark='"+code+"'";
//		List<WorkType> list = this.getSession().createQuery(hql).list();
//		return list;
//	}
}