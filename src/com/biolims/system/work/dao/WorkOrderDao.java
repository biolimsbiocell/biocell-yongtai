package com.biolims.system.work.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkOrderItem;

@Repository
@SuppressWarnings("unchecked")
public class WorkOrderDao extends BaseHibernateDao {
	public Map<String, Object> selectWorkOrderList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WorkOrder where 1=1 ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WorkOrder> list = new ArrayList<WorkOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWorkOrderItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WorkOrderItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and workOrder.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + "  order by sort asc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public WorkOrder findWorkOrderByProduct(String productId) {
		String key = " ";

		String hql = " from WorkOrder where 1=1 and productId like '%"
				+ productId + "%' and state='1'";

		List<WorkOrder> list = new ArrayList<WorkOrder>();
		list = this.getSession().createQuery(hql + key).list();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public Map<String, Object> selectApplicationTypeTableList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from ApplicationTypeTable where state='1'";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		key += " and applicationType.id='sample' ";
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<ApplicationTypeTable> list = new ArrayList<ApplicationTypeTable>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据实验表名和任务单ID查询下一步流向
	public List<WorkOrderItem> selectNextFlow(String model, String productId)
			throws Exception {
		String[] productIds = productId.split(",");
		String pro ="";
		for(int i=0;i<productIds.length;i++){
			if(i==0){
				pro+="productId like '%"+productIds[i]+"%' ";
			}else if(i==(productIds.length-1)){
				pro+="or productId like '%"+productIds[i]+"%' ";
			}
			
		}
		String hql = "from WorkOrderItem where workOrder.id in (select id from WorkOrder where ("
				+ pro
				+ ") and state='1') and applicationTypeTable='"
				+ model + "'";
		// String hql = "from WorkOrderItem where workOrder='" + orderId
		// + "' and applicationTypeTable='" + model + "'";
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		list = this.getSession().createQuery(hql).list();

		return list;
	}

	// 根据实验表名和测序平台查询下一步流向
	public List<WorkOrderItem> selectNextFlowBymodel(String model,
			String sequencingPlatform) throws Exception {
		// String str =
		// "from DicType t where 1=1 and t.type='sequencePlatform' and t.name='"
		// + sequencingPlatform + "'";
		// String tId = (String) this.getSession().createQuery("select id " +
		// str)
		// .uniqueResult();
		String hql = "from WorkOrderItem where workOrder.id in (select id from WorkOrder where state='1' and sequencingPlatform like '%"
				+ sequencingPlatform
				+ "%'"
				+ ") and applicationTypeTable='"
				+ model + "'";

		// WorkOrderItem w = new WorkOrderItem();
		// w = (WorkOrderItem)
		// this.getSession().createQuery(hql).uniqueResult();
		// return w;
		List<WorkOrderItem> list1 = new ArrayList<WorkOrderItem>();
		list1 = this.getSession().createQuery(hql).list();

		return list1;
	}

	// 根据ID查询下一步流向实体
	public List<NextFlow> selectNextFlowNameById(String id) {
		List<NextFlow> list = new ArrayList<NextFlow>();
		String hql = "from NextFlow where id='" + id + "'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public NextFlow selectNextFlowNameById2(String id) {
		NextFlow list = new NextFlow();
		String hql = "from NextFlow where id='" + id + "'";
		list = (NextFlow) this.getSession().createQuery(hql).uniqueResult();
		return list;
	}

	// 根据样本类型查询下一步流向
	public List<DicSampleType> selectNextFlowBysampleTypeId(String sampleTypeId)
			throws Exception {
		String hql = "from DicSampleType where id='" + sampleTypeId
				+ "' order by orderNumber ASC";
		List<DicSampleType> list = new ArrayList<DicSampleType>();
		list = this.getSession().createQuery(hql).list();

		return list;
	}

	/**
	 * 获取字典表的测序读长
	 * 
	 * @return
	 */
	public List<DicType> getReadLongList() throws Exception {
		String hql = "from DicType t where 1=1 and t.type='sequenceLength'";
		List<DicType> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 获取字典表的测序平台
	 * 
	 * @return
	 */
	public List<DicType> getPlatForm() throws Exception {
		String hql = "from DicType t where 1=1 and t.type='sequencePlatform'";
		List<DicType> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 获取字典表的测序读长
	 * 
	 * @return
	 */
	public List<DicType> getReadTypeList() throws Exception {
		String hql = "from DicType t where 1=1 and t.type='sequenceType'";
		List<DicType> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 通用获取字典表信息
	public List<DicType> getDicTypeList(String type) throws Exception {
		String hql = "from DicType t where 1=1 and t.type='" + type + "'";
		List<DicType> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<WorkOrderItem> selectNextFlow(String model, String productId,
			Integer start, Integer length, String query, String col, String sort) {

		String[] productIds = productId.split(",");
		String pro ="";
		for(int i=0;i<productIds.length;i++){
			if(i==0){
				pro+="productId like '%"+productIds[i]+"%' ";
			}else if(i==(productIds.length-1)){
				pro+="or productId like '%"+productIds[i]+"%' ";
			}
			
		}
		String hql = "from WorkOrderItem where workOrder.id in (select id from WorkOrder where ("
				+ pro
				+ ") and state='1') and applicationTypeTable='"
				+ model + "'";
		String key="";
		if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
			col=col.replace("-", ".");
			key+=" order by "+col+" "+sort;
		}
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		list = this.getSession().createQuery(hql+key).list();

		return list;
	
	}
	
	public List<WorkOrderItem> selectNextFlow1(String model, String productId) {

		String[] productIds = productId.split(",");
		String pro ="";
		for(int i=0;i<productIds.length;i++){
			if(i==0){
				pro+="productId like '%"+productIds[i]+"%' ";
			}else if(i==(productIds.length-1)){
				pro+="or productId like '%"+productIds[i]+"%' ";
			}
			
		}
		String hql = "from WorkOrderItem where workOrder.id in (select id from WorkOrder where ("
				+ pro
				+ ") and state='1') and applicationTypeTable='"
				+ model + "'";
		String key="";
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		list = this.getSession().createQuery(hql+key).list();

		return list;
	
	}

	public Map<String, Object> findWorkOrderTable(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WorkOrder where 1=1 ";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WorkOrder where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WorkOrder> list = new ArrayList<WorkOrder>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}

	public Map<String, Object> findWorkOrderItemTable(String scId,
			Integer start, Integer length, String query, String col, String sort) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WorkOrderItem where 1=1 and workOrder.id='"+scId+"'";
		String key = "";
//		if(query!=null&&!"".equals(query)){
//			key=map2Where(query);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WorkOrderItem where 1=1 and workOrder.id='"+scId+"' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}

	public List<WorkOrderItem> selectWorkOrderItemList(String id) {

		String hql = "from WorkOrderItem where 1=1 and workOrder.id='"+ id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		if (total > 0) {
				list = this.getSession().createQuery(hql).list();
		}
		return list;
	}

	public Map<String, Object> shownextFlowSampleOutTableJson(String sampleType, String model) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<WorkOrderItem> selectSampleOutNextFlow(String model) {
		String hql = "from WorkOrderItem where applicationTypeTable='"
				+ model + "'";
		String key="";
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		list = this.getSession().createQuery(hql+key).list();

		return list;
	}

	public List<WorkOrderItem> findWorkOrderItemListById(String id) {
		String hql = "from WorkOrderItem where 1=1 and workOrder.id='"+ id + "'";
		String key="";
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		list = this.getSession().createQuery(hql+key).list();
		return list;
	}

}