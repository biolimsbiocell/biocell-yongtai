﻿package com.biolims.system.work.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.system.work.service.WorkOrderService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/com/biolims/system/work/workOrder")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WorkOrderAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "900402";
	@Autowired
	private WorkOrderService workOrderService;
	private WorkOrder workOrder = new WorkOrder();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private CodingRuleService codingRuleService;
	
	
	/**
	 * @throws Exception 
		 *	保存子表明细
	     * @Title: saveItem  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月31日
	     * @throws
	 */
	@Action(value = "saveItem")
	public void saveItem() throws Exception {
		String id = getParameterFromRequest("id");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			workOrderService.saveItem(id,itemJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	@Action(value = "findWorkOrderItemListById")
	public void findWorkOrderItemListById() throws Exception {
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			String id = getParameterFromRequest("id");
			List<WorkOrderItem> list = workOrderService.findWorkOrderItemListById(id);
			map.put("success", true);
			map.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: showWorkOrderTable
	 * @Description: 显示实验配置列表
	 * @author : shengwei.wang
	 * @date 2018年2月27日下午4:36:07
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showWorkOrderTable")
	public String showWorkOrderTable() throws Exception {
		rightsId = "900402";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workOrder.jsp");
	}

	@Action(value = "showWorkOrderListJson")
	public void showWorkOrderListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = workOrderService.findWorkOrderTable(
					start, length, query, col, sort);
			List<WorkOrder> list = (List<WorkOrder>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("experimentUser", "");
			map.put("informationUser-id", "");
			map.put("informationUser-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("readType-id", "");
			map.put("readType-name", "");
			map.put("experimentalPeriod", "");
			map.put("analysisType", "");
			map.put("analysisPeriod", "");
			map.put("sequencingMethod", "");
			map.put("sequencingReadLong", "");
			map.put("sequencingPlatform", "");
			map.put("unit", "");
			map.put("whetherReading", "");
			map.put("unit-id", "");
			map.put("unit-name", "");

			map.put("density", "");// 预期密度
			map.put("yw", "");// 引物
			map.put("mixRatio", "");// 混合比例

			map.put("readsOne", "");
			map.put("readsTwo", "");
			map.put("index1", "");
			map.put("index2", "");

			map.put("coefficient", "");
			map.put("amount", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "workOrderSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWorkOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workOrderDialog.jsp");
	}

	@Action(value = "showDialogWorkOrderListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWorkOrderListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = workOrderService.findWorkOrderTable(
					start, length, query, col, sort);
			List<WorkOrder> list = (List<WorkOrder>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("experimentUser", "");
			map.put("informationUser-id", "");
			map.put("informationUser-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("readType-id", "");
			map.put("readType-name", "");
			map.put("experimentalPeriod", "");
			map.put("analysisType", "");
			map.put("analysisPeriod", "");
			map.put("sequencingMethod", "");
			map.put("sequencingReadLong", "");
			map.put("sequencingPlatform", "");
			map.put("unit", "");
			map.put("whetherReading", "");
			map.put("unit-id", "");
			map.put("unit-name", "");

			map.put("density", "");// 预期密度
			map.put("yw", "");// 引物
			map.put("mixRatio", "");// 混合比例

			map.put("readsOne", "");
			map.put("readsTwo", "");
			map.put("index1", "");
			map.put("index2", "");

			map.put("coefficient", "");
			map.put("amount", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * 
	 * @Title: editWorkOrder  
	 * @Description: 新建、编辑实验配置单 
	 * @author : shengwei.wang
	 * @date 2018年2月27日下午4:37:29
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "editWorkOrder")
	public String editWorkOrder() throws Exception {
		rightsId = "900401";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			workOrder = workOrderService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "workOrder");
		} else {
			workOrder.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			workOrder.setCreateUser(user);
			workOrder.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
//		List<DicType> listReadLong = workOrderDao.getReadLongList();
		List<DicType> listPlatForm = workOrderDao.getPlatForm();
//		List<DicType> listReadType = workOrderDao.getReadTypeList();

//		putObjToContext("listReadLong", listReadLong);
		putObjToContext("listPlatForm", listPlatForm);
//		putObjToContext("listReadType", listReadType);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workOrderEdit.jsp");
	}

	//
	// @Action(value = "copyWorkOrder")
	// public String copyWorkOrder() throws Exception {
	// String id = getParameterFromRequest("id");
	// String handlemethod = getParameterFromRequest("handlemethod");
	// workOrder = workOrderService.get(id);
	// workOrder.setId("");
	// handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
	// toToolBar(rightsId, "", "", handlemethod);
	// toSetStateCopy();
	// return
	// dispatcher("/WEB-INF/page/com/biolims/system/work/workOrderEdit.jsp");
	// }

	@Action(value = "save")
	public String save() throws Exception {
		String id = workOrder.getId();
		 if ((id != null && id.equals("")) || id.equals("NEW")) {
		 String modelName = "WorkOrder";
		 String markCode="RWD";
		 String autoID = codingRuleService.genTransID(modelName,markCode);
		 workOrder.setId(autoID);
		 }
		if (id != null && id.equals("")) {
			workOrder.setId(null);
		}
//		Map aMap = new HashMap();
//		aMap.put("workOrderItem", getParameterFromRequest("workOrderItemJson"));

		workOrderService.save(workOrder);
		return redirect("/com/biolims/system/work/workOrder/editWorkOrder.action?id="
				+ workOrder.getId());

	}

	@Action(value = "viewWorkOrder")
	public String toViewWorkOrder() throws Exception {
		String id = getParameterFromRequest("id");
		workOrder = workOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workOrderEdit.jsp");
	}

	// @Action(value = "showWorkOrderItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showWorkOrderItemList() throws Exception {
	// return
	// dispatcher("/WEB-INF/page/com/biolims/system/work/workOrderItem.jsp");
	// }

	@Action(value = "showWorkOrderItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWorkOrderItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = workOrderService
					.findWorkOrderItemTable(scId, start, length, query, col,
							sort);
			List<WorkOrderItem> list = (List<WorkOrderItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("process", "");
			map.put("workOrder-name", "");
			map.put("workOrder-id", "");
			map.put("nextStepId", "");
			map.put("nextStepName", "");
			map.put("name", "");
			map.put("applicationTypeTable-name", "");
			map.put("applicationTypeTable-id", "");
			map.put("sort", "");
			map.put("dnextId", "");
			map.put("dnextName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWorkOrderItem")
	public void delWorkOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			workOrderService.delWorkOrderItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WorkOrderService getWorkOrderService() {
		return workOrderService;
	}

	public void setWorkOrderService(WorkOrderService workOrderService) {
		this.workOrderService = workOrderService;
	}

	public WorkOrder getWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

	/**
	 * 选择后继实验页面的方法
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDialogApplicationTypeTableList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogApplicationTypeTableList() throws Exception {
		return dispatcher("/WEB-INF/page/com/biolims/system/work/showDialogApplicationTypeTableList.jsp");
	}

	/**
	 * 加载后继实验数据的方法
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showApplicationTypeTableListJson")
	public void showApplicationTypeTableListJson() throws Exception {
		int startNum = Integer
				.parseInt(getParameterFromRequest("start").trim());
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		// else {
		//
		// mapForQuery.put("applicationType.id", "sample");
		// }
		try {
			Map<String, Object> result = this.workOrderService
					.findSampleApplicationTypeTableList(mapForQuery, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<ApplicationTypeTable> list = (List<ApplicationTypeTable>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "");
			map.put("pathName", "pathName");
			map.put("applicationType-id", "applicationType-id");
			map.put("classPath", "");
			map.put("workflowUserColumn", "workflowUserColumn");
			map.put("state", "state");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
