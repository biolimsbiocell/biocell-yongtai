
package com.biolims.system.work.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.SystemCodeDao;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.work.model.UnitGroupNew;
import com.biolims.system.work.service.UnitGroupNewService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/com/biolims/system/work/unitGroupNew")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class UnitGroupNewAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9021";
	@Autowired
	private UnitGroupNewService unitGroupNewService;
	private UnitGroupNew unitGroupNew = new UnitGroupNew();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showWorkTypeList")
	public String showWorkTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/unitGroupNew/workType.jsp");
	}

	/*@Action(value = "showWorkTypeListJson")
	public void showWorkTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = unitGroupNewService.findWorkTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<UnitGroupNew> list = (List<UnitGroupNew>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		//map.put("cycle-id", "");
		map.put("cycle", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("person-id", "");
		map.put("person-name", "");
		map.put("mark", "");
		map.put("state", "");
		//map.put("state-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}*/
	
	@Action(value = "showWorkTypeListNewJson")
	public void showWorkTypeListNewJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = unitGroupNewService.findWorkTypeList(
					start, length, query, col, sort);
			List<UnitGroupNew> list = (List<UnitGroupNew>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("cycle", "");
			map.put("parent-id", "");
			map.put("parent-name", "");
			map.put("person-id", "");
			map.put("person-name", "");
			map.put("mark-name", "");
			map.put("mark2-name", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "showWorkTypeDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWorkTypeDialogList() throws Exception {
		String state = getParameterFromRequest("state");
		String unit = getParameterFromRequest("arr");
		putObjToContext("arr", unit);
		putObjToContext("state", state);

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/unitGroupNew/workTypeDialog.jsp");
	}
	@Action(value = "showWorkTypeDialogListJson")
	public void showWorkTypeDialogListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = unitGroupNewService.findWorkTypeList(
					start, length, query, col, sort);
			List<UnitGroupNew> list = (List<UnitGroupNew>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("cycle", "");
			map.put("mark-name", "");
			map.put("mark2-name", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "showWorkTypeDialogListNewJson")
	public void showWorkTypeDialogListNewJson() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = unitGroupNewService.findWorkTypeList(
					start, length, query, col, sort);
			List<UnitGroupNew> list = (List<UnitGroupNew>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("cycle", "");
			map.put("parent-id", "");
			map.put("parent-name", "");
			map.put("person-id", "");
			map.put("person-name", "");
			map.put("mark-id", "");
			map.put("mark-name", "");
			map.put("mark2-id", "");
			map.put("mark2-name", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showWorkTypeDialogListNewJsonByUnit")
	public void showWorkTypeDialogListNewJsonByUnit() {
		String query = getParameterFromRequest("query");
		String unit = getParameterFromRequest("unit");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = unitGroupNewService.findWorkTypeListByUnit(
					start, length, query, col, sort,unit);
			List<UnitGroupNew> list = (List<UnitGroupNew>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("cycle", "");
			map.put("parent-id", "");
			map.put("parent-name", "");
			map.put("person-id", "");
			map.put("person-name", "");
			map.put("mark-id", "");
			map.put("mark-name", "");
			map.put("mark2-id", "");
			map.put("mark2-name", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Action(value = "editWorkType")
	public String editWorkType() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			unitGroupNew = unitGroupNewService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "unitGroupNew");
		} else {
			unitGroupNew=new UnitGroupNew();
			unitGroupNew.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			unitGroupNew.setPerson(user);
//			unitGroupNew.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/com/biolims/system/unitGroupNew/workTypeEdit.jsp");
	}
	@Action(value = "viewWorkType")
	public String viewWorkType() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		unitGroupNew = unitGroupNewService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		putObjToContext("type", type);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/unitGroupNew/workTypeEdit.jsp");	}
//
//	@Action(value = "copyWorkType")
//	public String copyWorkType() throws Exception {
//		String id = getParameterFromRequest("id");
//		String handlemethod = getParameterFromRequest("handlemethod");
//		unitGroupNew = unitGroupNewService.get(id);
//		unitGroupNew.setId("");
//		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
//		toToolBar(rightsId, "", "", handlemethod);
//		toSetStateCopy();
//		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeEdit.jsp");
//	}
//
//
	@Action(value = "save")
	public String save() throws Exception {
		String id = unitGroupNew.getId();
		String changeLog = getParameterFromRequest("changeLog");
		String log = "";
		if(id==null||id.equals("")||id.equals("NEW")){
			log = "123";
			String modelName = "UnitGroupNew";
			String markCode = "DWZ";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyMMDD");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName,
					markCode, stime, 0000, 4, null);
			unitGroupNew.setId(autoID);
		}
//		Map aMap = new HashMap();
			//aMap.put("unitGroupNewItem",getParameterFromRequest("unitGroupNewItemJson"));
		
		unitGroupNewService.save(unitGroupNew,changeLog,log);
		return redirect("/com/biolims/system/work/unitGroupNew/editWorkType.action?id=" + unitGroupNew.getId());

	}
//
//	@Action(value = "viewWorkType")
//	public String toViewWorkType() throws Exception {
//		String id = getParameterFromRequest("id");
//		unitGroupNew = unitGroupNewService.get(id);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeEdit.jsp");
//	}
//	
//	
//	/**
//	 * 访问 任务树
//	 */
//	@Action(value = "showWorkTypeTree")
//	public String showWorkTypeTree() throws Exception {
//
//		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
//				+ "/com/biolims/system/work/workType/showWorkTypeTreeJson.action");
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//
//		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeTree.jsp");
//	}
//
//	@Action(value = "showWorkTypeTreeJson")
//	public void showWorkTypeTreeJson() throws Exception {
//		String upId = getParameterFromRequest("treegrid_id");
//		List<WorkType> aList = null;
//		if (upId.equals("")) {
//
//			aList = workTypeService.findWorkTypeList();
//		} else {
//
//			aList = workTypeService.findWorkTypeList(upId);
//		}
//
//		String a = workTypeService.getJson(aList);
//
//		new SendData().sendDataJson(a, ServletActionContext.getResponse());
//	}
//
//	@Action(value = "showWorkTypeChildTreeJson")
//	public void showWorkTypeChildTreeJson() throws Exception {
//		String upId = getParameterFromRequest("treegrid_id");
//
//		List<WorkType> aList = workTypeService.findWorkTypeList(upId);
//		String a = workTypeService.getJson(aList);
//
//		new SendData().sendDataJson(a, ServletActionContext.getResponse());
//	}
//
//	@Action(value = "showWorkTypeItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showWorkTypeItemList() throws Exception {
//		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeItem.jsp");
//	}
//
//	@Action(value = "showWorkTypeItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showWorkTypeItemListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = workTypeService.findWorkTypeItemList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<WorkTypeItem> list = (List<WorkTypeItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("name", "");
//			map.put("description", "");
//			map.put("note", "");
//			map.put("workType-name", "");
//			map.put("workType-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delWorkTypeItem")
//	public void delWorkTypeItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			workTypeService.delWorkTypeItem(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public UnitGroupNewService getUnitGroupNewService() {
		return unitGroupNewService;
	}

	public void setUnitGroupNewService(UnitGroupNewService unitGroupNewService) {
		this.unitGroupNewService = unitGroupNewService;
	}

	public UnitGroupNew getUnitGroupNew() {
		return unitGroupNew;
	}

	public void setUnitGroupNew(UnitGroupNew unitGroupNew) {
		this.unitGroupNew = unitGroupNew;
	}


}
