﻿
package com.biolims.system.work.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.SystemCodeDao;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.work.model.WorkType;
import com.biolims.system.work.model.WorkTypeItem;
import com.biolims.system.work.service.WorkTypeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/com/biolims/system/work/workType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WorkTypeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9001";
	@Autowired
	private WorkTypeService workTypeService;
	private WorkType workType = new WorkType();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showWorkTypeList")
	public String showWorkTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workType.jsp");
	}

	@Action(value = "showWorkTypeListJson")
	public void showWorkTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = workTypeService.findWorkTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WorkType> list = (List<WorkType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		//map.put("cycle-id", "");
		map.put("cycle", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("person-id", "");
		map.put("person-name", "");
		map.put("mark", "");
		map.put("state", "");
		//map.put("state-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "workTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWorkTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeDialog.jsp");
	}

	@Action(value = "showDialogWorkTypeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWorkTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = workTypeService.findWorkTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WorkType> list = (List<WorkType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		//map.put("cycle-id", "");
		map.put("cycle", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("person-id", "");
		map.put("person-name", "");
		map.put("mark", "");
		map.put("state", "");
		//map.put("state-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editWorkType")
	public String editWorkType() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			workType = workTypeService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "workType");
		} else {
			workType=new WorkType();
			workType.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			workType.setPerson(user);
//			workType.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeEdit.jsp");
	}

	@Action(value = "copyWorkType")
	public String copyWorkType() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		workType = workTypeService.get(id);
		workType.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = workType.getId();
		if(id!=null&&id.equals("")){
			workType.setId(null);
		}
		Map aMap = new HashMap();
			//aMap.put("workTypeItem",getParameterFromRequest("workTypeItemJson"));
		
		workTypeService.save(workType,aMap);
		return redirect("/com/biolims/system/work/workType/editWorkType.action?id=" + workType.getId());

	}

	@Action(value = "viewWorkType")
	public String toViewWorkType() throws Exception {
		String id = getParameterFromRequest("id");
		workType = workTypeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeEdit.jsp");
	}
	
	
	/**
	 * 访问 任务树
	 */
	@Action(value = "showWorkTypeTree")
	public String showWorkTypeTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/com/biolims/system/work/workType/showWorkTypeTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeTree.jsp");
	}

	@Action(value = "showWorkTypeTreeJson")
	public void showWorkTypeTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<WorkType> aList = null;
		if (upId.equals("")) {

			aList = workTypeService.findWorkTypeList();
		} else {

			aList = workTypeService.findWorkTypeList(upId);
		}

		String a = workTypeService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showWorkTypeChildTreeJson")
	public void showWorkTypeChildTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<WorkType> aList = workTypeService.findWorkTypeList(upId);
		String a = workTypeService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showWorkTypeItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWorkTypeItemList() throws Exception {
		return dispatcher("/WEB-INF/page/com/biolims/system/work/workTypeItem.jsp");
	}

	@Action(value = "showWorkTypeItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWorkTypeItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = workTypeService.findWorkTypeItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<WorkTypeItem> list = (List<WorkTypeItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("description", "");
			map.put("note", "");
			map.put("workType-name", "");
			map.put("workType-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delWorkTypeItem")
	public void delWorkTypeItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			workTypeService.delWorkTypeItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WorkTypeService getWorkTypeService() {
		return workTypeService;
	}

	public void setWorkTypeService(WorkTypeService workTypeService) {
		this.workTypeService = workTypeService;
	}

	public WorkType getWorkType() {
		return workType;
	}

	public void setWorkType(WorkType workType) {
		this.workType = workType;
	}


}
