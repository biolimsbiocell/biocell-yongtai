package com.biolims.system.work.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.work.dao.WorkTypeDao;
import com.biolims.system.work.model.WorkType;
import com.biolims.system.work.model.WorkTypeItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WorkTypeService {
	@Resource
	private WorkTypeDao workTypeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findWorkTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return workTypeDao.selectWorkTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WorkType i) throws Exception {

		workTypeDao.saveOrUpdate(i);

	}
	public WorkType get(String id) {
		WorkType workType = commonDAO.get(WorkType.class, id);
		return workType;
	}
	public Map<String, Object> findWorkTypeItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = workTypeDao.selectWorkTypeItemList(scId, startNum, limitNum, dir, sort);
		List<WorkTypeItem> list = (List<WorkTypeItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWorkTypeItem(WorkType sc, String itemDataJson) throws Exception {
		List<WorkTypeItem> saveItems = new ArrayList<WorkTypeItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WorkTypeItem scp = new WorkTypeItem();
			// 将map信息读入实体类
			scp = (WorkTypeItem) workTypeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWorkType(sc);

			saveItems.add(scp);
		}
		workTypeDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWorkTypeItem(String[] ids) throws Exception {
		for (String id : ids) {
			WorkTypeItem scp =  workTypeDao.get(WorkTypeItem.class, id);
			 workTypeDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WorkType sc, Map jsonMap) throws Exception {
		if (sc != null) {
			workTypeDao.saveOrUpdate(sc);
		
			String jsonStr = "";
//			jsonStr = (String)jsonMap.get("workTypeItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveWorkTypeItem(sc, jsonStr);
//			}
	}
   }
   /**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<WorkType> getChildList(List<WorkType> list, WorkType node) throws Exception { //得到子节点列表   
		List<WorkType> li = new ArrayList<WorkType>();
		Iterator<WorkType> it = list.iterator();
		while (it.hasNext()) {
			WorkType n = (WorkType) it.next();
			if (n.getParent() != null && n.getParent().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}
   /**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<WorkType> list, WorkType node) throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}
   	public boolean hasChildCount(WorkType node) throws Exception {

		Long c = commonDAO.getCount("from WorkType where parent.id='"
				+ node.getId() + "'");
		return c > 0 ? true : false;
	}
   
   public String getJson(List<WorkType> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<WorkType> nodeList0 = new ArrayList<WorkType>();
		Iterator<WorkType> it1 = list.iterator();
		while (it1.hasNext()) {
			WorkType node = (WorkType) it1.next();
			//if (node.getLevel() == 0) {
			nodeList0.add(node);
			//}
		}
		Iterator<WorkType> it = nodeList0.iterator();
		while (it.hasNext()) {
			WorkType node = (WorkType) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}
	
	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<WorkType> list, WorkType treeNode) throws Exception {
		json.append("{");
		json.append("\"id\":'");
		json.append(JsonUtils.formatStr(treeNode.getId() == null ? "" : treeNode.getId()) + "");
		json.append("',");
		json.append("\"name\":'");
		json.append(JsonUtils.formatStr(treeNode.getName() == null ? "" : treeNode.getName()) + "");
		json.append("',");
//		json.append("\"cycle\":'");
//		json.append(JsonUtils.formatStr(treeNode.getCycle() == null ? "" : treeNode.getCycle().getName()) + "");
//		json.append("',");
		json.append("\"parent\":'");
		json.append(JsonUtils.formatStr(treeNode.getParent() == null ? "" : treeNode.getParent().getName()) + "");
//		json.append("',");
//		json.append("\"state\":'");
//		json.append(JsonUtils.formatStr(treeNode.getState() == null ? "" : treeNode.getState()) + "");
		json.append("',");
		if (hasChildCount(treeNode)) {
			json.append("\"leaf\":false");
		} else {
			json.append("\"leaf\":true");
		}
		json.append(",\"upId\":'");
		json.append((treeNode.getParent() == null ? "" : treeNode.getParent().getId()) + "");
		json.append("'}");
		
	}
	
   	public List<WorkType> findWorkTypeList(String upId) throws Exception {
		List<WorkType> list = null;
		if (upId.equals("0")) {
			list = commonDAO
					.find("from WorkType where  parent.id is null order by id asc");
		} else {

			list = commonDAO.find("from WorkType where parent.id='"
					+ upId + "' order by id asc");
		}
		return list;
	}
	public List<WorkType> findWorkTypeList() throws Exception {
		List<WorkType> list = commonDAO
				.find("from WorkType where  parent.id is null order by id asc");
		return list;
	}
	//根据标识码加载检测方法
	public List<Map<String, String>> setWorkType(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = workTypeDao.setWorkType(code);
		List<WorkType> list = (List<WorkType>) result.get("list");

		if (list != null && list.size() > 0) {
			for (WorkType srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("state", srai.getState());
				if(srai.getCycle()!=null){
					map.put("cycle", srai.getCycle().toString());
				}else{
					map.put("cycle", "");
				}
				if(srai.getPerson()!=null){
					map.put("personName", srai.getPerson().getName());
					map.put("personId", srai.getPerson().getId());
				}else{
					map.put("personName", "");
					map.put("personId", "");
				}
				
				map.put("mark", srai.getMark());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
