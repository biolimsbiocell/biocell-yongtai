package com.biolims.system.work.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.DicSampleType;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WorkOrderService {
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWorkOrderList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return workOrderDao.selectWorkOrderList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(WorkOrder i) throws Exception {
	//
	// workOrderDao.saveOrUpdate(i);
	//
	// }

	public WorkOrder get(String id) {
		WorkOrder workOrder = commonDAO.get(WorkOrder.class, id);
		return workOrder;
	}

	public Map<String, Object> findWorkOrderItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = workOrderDao.selectWorkOrderItemList(scId,
				startNum, limitNum, dir, sort);
		List<WorkOrderItem> list = (List<WorkOrderItem>) result.get("list");
		return result;
	}

	/**
	 * 
	 * @Title: saveWorkOrderItem
	 * @Description: 保存明细表
	 * @author : shengwei.wang
	 * @date 2018年2月27日下午6:23:00
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWorkOrderItem(WorkOrder sc, String itemDataJson)
			throws Exception {
		Map<String, Object> map = JsonUtils.toObjectByJson(itemDataJson,
				Map.class);
		List<Map<String, Object>> list1 = (List<Map<String, Object>>) map
				.get("nodeDataArray");
		List<Map<String, Object>> list2 = (List<Map<String, Object>>) map
				.get("linkDataArray");
		List<WorkOrderItem> woiList = workOrderDao.selectWorkOrderItemList(sc
				.getId());
		commonDAO.deleteAll(woiList);
		Integer key = -1;
		Map<String, Integer> set = new HashMap<String, Integer>();
		// for (Map<String, Object> map1 : list1) {
		// if (map1.containsKey("category")
		// && map1.get("category").equals("Start")) {
		// key = (Integer) map1.get("key");
		// }
		// }
		set = findPreFlow(set, key, list1, list2);
		Set<String> str = set.keySet();
		for (String s : str) {
			NextFlow nf = commonDAO.get(NextFlow.class, s);
			WorkOrderItem woi = new WorkOrderItem();
			if (nf.getMainTable() != null) {
				woi.setApplicationTypeTable(nf.getMainTable());
				woi.setWorkOrder(sc);
				woi.setName(nf.getName());
				List<Map<String, Object>> nextFlow = findNextFlow(
						(Integer) set.get(s), list1, list2);
				woi.setNextStepId(JsonUtils.toJsonString(nextFlow));
				commonDAO.saveOrUpdate(woi);
			}
		}
	}

	/**
	 * @return
	 * 
	 * @Title: findNextFlow
	 * @Description: 查询下一步流向
	 * @author : shengwei.wang
	 * @date 2018年2月28日下午6:58:45
	 * @param id
	 * @param list1
	 * @param list2
	 *            void
	 * @throws
	 */
	private List<Map<String, Object>> findNextFlow(Integer key,
			List<Map<String, Object>> list1, List<Map<String, Object>> list2) {
		List<Map<String, Object>> nextFlowIds = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		String nextFlowId = "";
		for (Map<String, Object> map2 : list2) {
			if (map2.get("from").equals(key)) {
				Integer t2 = (Integer) map2.get("to");
				for (Map<String, Object> map1 : list1) {
					if (map1.get("key").equals(t2)) {
						if (map1.containsKey("figure")) {
							map.putAll(findNextFlow(t2, map, list1, list2));
						} else if (!map1.containsKey("category")) {
							nextFlowId += map1.get("id")+",";
						}
					}
				}
			}
		}
		map.put("NextFlowId", nextFlowId);
		nextFlowIds.add(map);
		return nextFlowIds;
	}

	/**
	 * 
	 * @Title: findNextFlow
	 * @Description: 查询判断的下一步
	 * @author : shengwei.wang
	 * @date 2018年3月5日上午11:40:01
	 * @param t2
	 * @param map
	 * @param list1
	 * @param list2
	 * @return Map<String,Object>
	 * @throws
	 */
	private Map<String, Object> findNextFlow(Integer t2,
			Map<String, Object> map, List<Map<String, Object>> list1,
			List<Map<String, Object>> list2) {
		for (Map<String, Object> map2 : list2) {
			if (map2.get("from").equals(t2)) {
				Integer tt = (Integer) map2.get("to");
				for (Map<String, Object> map1 : list1) {
					if (map1.get("key").equals(tt)) {
						if (!map1.containsKey("category")) {
							map.put((String) map2.get("id"), map1.get("id"));
						}
					}
				}
			}
		}
		return map;
	}

	/**
	 * 
	 * @Title: findPreFlow
	 * @Description: 查询实验模块
	 * @author : shengwei.wang
	 * @date 2018年2月28日下午6:58:22
	 * @param set
	 * @param key
	 * @param list1
	 * @param list2
	 * @return Set<String>
	 * @throws
	 */
	private Map<String, Integer> findPreFlow(Map<String, Integer> set,
			Integer key, List<Map<String, Object>> list1,
			List<Map<String, Object>> list2) {
		for (Map<String, Object> map2 : list2) {
			if (map2.get("from").equals(key)) {
				Integer t2 = (Integer) map2.get("to");
				for (Map<String, Object> map1 : list1) {
					if (map1.get("key").equals(t2)) {
						if (!map1.containsKey("category")) {
							if (map1.containsKey("figure")) {
								findPreFlow(set, t2, list1, list2);
							} else {
								findPreFlow(set, t2, list1, list2);
								set.put((String) map1.get("id"), t2);
							}
						}
					}
				}
			}
		}
		return set;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWorkOrderItem(String[] ids) throws Exception {
		for (String id : ids) {
			WorkOrderItem scp = workOrderDao.get(WorkOrderItem.class, id);
			workOrderDao.delete(scp);
		}
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 保存实验配置
	 * @author : shengwei.wang
	 * @date 2018年3月1日上午10:32:25
	 * @param sc
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WorkOrder sc) throws Exception {
		workOrderDao.saveOrUpdate(sc);
		String jsonStr = "";
		jsonStr = sc.getContent();
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveWorkOrderItem(sc, jsonStr);
		}
	}

	public Map<String, Object> findSampleApplicationTypeTableList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return workOrderDao.selectApplicationTypeTableList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	// 根据检测项目查询下一步流程
	public Map<String, Object> selectNextFlow(String model, String productId)
			throws Exception {
		List<WorkOrderItem> list = workOrderDao
				.selectNextFlow(model, productId);
		List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
		int total = 0;
		for (WorkOrderItem w : list) {
			String id = w.getNextStepId();
			String[] ids = id.split(",");
			total = ids.length;
			for (int i = 0; i < ids.length; i++) {
				List<NextFlow> nextFlow = workOrderDao
						.selectNextFlowNameById(ids[i]);
				for (NextFlow n : nextFlow) {
					NextFlow nf = new NextFlow();
					nf.setId(n.getId());
					nf.setName(n.getName());
					nf.setApplicationTypeTable(n.getApplicationTypeTable());
					nf.setCreateUser(n.getCreateUser());
					nf.setCreateDate(n.getCreateDate());
					list_nextFlow.add(nf);
				}
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list_nextFlow);
		result.put("total", total);
		return result;
	}

	// 根据样本类型查询
	public Map<String, Object> selectNextFlowBysampleTypeId(String sampleTypeId)
			throws Exception {
		List<DicSampleType> list = workOrderDao
				.selectNextFlowBysampleTypeId(sampleTypeId);
		List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
		int total = 0;
		for (DicSampleType d : list) {
			String id = d.getNextFlowId();
			String[] ids = id.split(",");
			total = ids.length;
			for (int i = 0; i < ids.length; i++) {
				List<NextFlow> nextFlow = workOrderDao
						.selectNextFlowNameById(ids[i]);
				for (NextFlow n : nextFlow) {
					NextFlow nf = new NextFlow();
					nf.setId(n.getId());
					nf.setName(n.getName());
					nf.setApplicationTypeTable(n.getApplicationTypeTable());
					nf.setCreateUser(n.getCreateUser());
					nf.setCreateDate(n.getCreateDate());
					list_nextFlow.add(nf);
				}
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list_nextFlow);
		result.put("total", total);
		return result;
	}

	// 查询默认下一步流向
	public WorkOrderItem selectdnextId(String model, String productId)
			throws Exception {
		List<WorkOrderItem> list = workOrderDao
				.selectNextFlow(model, productId);
		WorkOrderItem w = new WorkOrderItem();
		for (WorkOrderItem woi : list) {
			w.setDnextId(woi.getDnextId());
			w.setDnextName(woi.getDnextName());
		}

		return w;
	}

	// 查询富集后的下一步流向
	// public Map<String, Object> selectNextFlowBymodel(String model)
	// throws Exception {
	// WorkOrderItem w = workOrderDao.selectNextFlowBymodel(model);
	// String[] nid = w.getNextStepId().split(",");
	// List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
	// for (int i = 0; i < nid.length; i++) {
	// NextFlow n = workOrderDao.selectNextFlowNameById2(nid[i]);
	// NextFlow nf = new NextFlow();
	// nf.setId(n.getId());
	// nf.setName(n.getName());
	// nf.setApplicationTypeTable(n.getApplicationTypeTable());
	// nf.setCreateUser(n.getCreateUser());
	// nf.setCreateDate(n.getCreateDate());
	// list_nextFlow.add(nf);
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("list", list_nextFlow);
	// result.put("total", nid.length);
	// return result;
	// }
	// 查询富集后的下一步流向
	public Map<String, Object> selectNextFlowBymodel(String model,
			String sequencingPlatform) throws Exception {
		List<WorkOrderItem> list = workOrderDao.selectNextFlowBymodel(model,
				sequencingPlatform);
		List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
		int total = 0;
		for (WorkOrderItem w : list) {
			String id = w.getNextStepId();
			String[] ids = id.split(",");
			total = ids.length;
			for (int i = 0; i < ids.length; i++) {
				List<NextFlow> nextFlow = workOrderDao
						.selectNextFlowNameById(ids[i]);
				for (NextFlow n : nextFlow) {
					NextFlow nf = new NextFlow();
					nf.setId(n.getId());
					nf.setName(n.getName());
					nf.setApplicationTypeTable(n.getApplicationTypeTable());
					nf.setCreateUser(n.getCreateUser());
					nf.setCreateDate(n.getCreateDate());
					list_nextFlow.add(nf);
				}
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list_nextFlow);
		result.put("total", total);
		return result;
	}

	public Map<String, Object> shownextFlowDialogTableJson(String sampleType,
			String model, String productId, Integer start, Integer length,
			String query, String col, String sort) throws Exception {

		List<WorkOrderItem> list = workOrderDao.selectNextFlow(model,
				productId, start, length, query, col, sort);
		List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
		List<String> nfId = new ArrayList<String>();
		for (WorkOrderItem w : list) {
			String nextFlowJson = w.getNextStepId();
			
			nextFlowJson = nextFlowJson.replaceAll("'", "\"");
			
			List<Map<String, Object>> nlist = JsonUtils.toListByJsonArray(
					nextFlowJson, List.class);
			for (Map<String, Object> map : nlist) {
				String nextFlowId = (String) map.get("NextFlowId");
				if (nextFlowId != null && !nextFlowId.equals("")) {
					String[] ids = nextFlowId.split(",");
					for (int i = 0; i < ids.length; i++) {
						List<NextFlow> nextFlow = workOrderDao
								.selectNextFlowNameById(ids[i]);
						for (NextFlow n : nextFlow) {
							if (!nfId.contains(n.getId())) {
								NextFlow nf = new NextFlow();
								nf.setId(n.getId());
								nf.setName(n.getName());
								nf.setApplicationTypeTable(n
										.getApplicationTypeTable());
								nf.setCreateUser(n.getCreateUser());
								nf.setCreateDate(n.getCreateDate());
								list_nextFlow.add(nf);
								nfId.add(n.getId());
							}
						}
					}
				} else {
					if (sampleType != null && !"".equals(sampleType)&&!"null".equals(sampleType)) {
						DicSampleType dst = commonDAO.get(DicSampleType.class,
								sampleType);
						String typeId = dst.getType().getId();
						if (map.containsKey(typeId)) {
							String typeFlow = (String) map.get(typeId);
							String[] ids = typeFlow.split(",");
							for (int i = 0; i < ids.length; i++) {
								List<NextFlow> nextFlow = workOrderDao
										.selectNextFlowNameById(ids[i]);
								for (NextFlow n : nextFlow) {
									if (!nfId.contains(n.getId())) {
										NextFlow nf = new NextFlow();
										nf.setId(n.getId());
										nf.setName(n.getName());
										nf.setApplicationTypeTable(n
												.getApplicationTypeTable());
										nf.setCreateUser(n.getCreateUser());
										nf.setCreateDate(n.getCreateDate());
										list_nextFlow.add(nf);
										nfId.add(n.getId());
									}
								}
							}
						}
					}

				}
			}

		}
		if("SampleReceive".equals(model)){
		}else if("PlasmaTask".equals(model)){
			
		}else if("DnaTask".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0010");
			list_nextFlow.add(nf1);
		}else if("WkTask".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0010");
			NextFlow nf2=commonDAO.get(NextFlow.class, "0011");
			NextFlow nf3=commonDAO.get(NextFlow.class, "0012");
			NextFlow nf4=commonDAO.get(NextFlow.class, "0013");
			NextFlow nf5=commonDAO.get(NextFlow.class, "0014");
			list_nextFlow.add(nf1);
			list_nextFlow.add(nf2);
			list_nextFlow.add(nf3);
			list_nextFlow.add(nf4);
			list_nextFlow.add(nf5);
		}else if("ZJRWD".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0017");
			NextFlow nf2=commonDAO.get(NextFlow.class, "0019");
			list_nextFlow.add(nf1);
			list_nextFlow.add(nf2);
		}else if("JKRWD".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0072");
			list_nextFlow.add(nf1);
		}else if("SJRWD".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0023");
			list_nextFlow.add(nf1);
		}else if("ZLJC".equals(model)) {//明细下一步质量检测
			NextFlow nf1=commonDAO.get(NextFlow.class, "0082");
			NextFlow nf2=commonDAO.get(NextFlow.class, "0083");
			NextFlow nf3=commonDAO.get(NextFlow.class, "0084");
			NextFlow nf4=commonDAO.get(NextFlow.class, "0085");
			NextFlow nf5=commonDAO.get(NextFlow.class, "0086");
			NextFlow nf6=commonDAO.get(NextFlow.class, "0087");
			NextFlow nf7=commonDAO.get(NextFlow.class, "0088");
			NextFlow nf8=commonDAO.get(NextFlow.class, "0089");
			list_nextFlow.add(nf1);
			list_nextFlow.add(nf2);
			list_nextFlow.add(nf3);
			list_nextFlow.add(nf4);
			list_nextFlow.add(nf5);
			list_nextFlow.add(nf6);
			list_nextFlow.add(nf7);
			list_nextFlow.add(nf8);
		}
		NextFlow nf0=commonDAO.get(NextFlow.class, "0009");
//		NextFlow nf6=commonDAO.get(NextFlow.class, "0024");
		NextFlow nf7=commonDAO.get(NextFlow.class, "0013");
		list_nextFlow.add(nf0);
//		list_nextFlow.add(nf6);
		list_nextFlow.add(nf7);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("recordsTotal", list_nextFlow.size());
		result.put("recordsFiltered", list_nextFlow.size());
		result.put("list", list_nextFlow);
		return result;

	}

	public Map<String, Object> findWorkOrderTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return workOrderDao.findWorkOrderTable(start, length, query, col, sort);
	}

	public Map<String, Object> findWorkOrderItemTable(String scId,
			Integer start, Integer length, String query, String col, String sort) {
		return workOrderDao.findWorkOrderItemTable(scId, start, length, query,
				col, sort);
	}

	public Map<String, Object> shownextFlowSampleOutTableJson(String sampleType, String model) throws Exception {
		List<WorkOrderItem> list = workOrderDao.selectSampleOutNextFlow(model);
		List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
		List<String> nfId = new ArrayList<String>();
		for (WorkOrderItem w : list) {
			String nextFlowJson = w.getNextStepId();
			List<Map<String, Object>> nlist = JsonUtils.toListByJsonArray(
					nextFlowJson, List.class);
			for (Map<String, Object> map : nlist) {
				String nextFlowId = (String) map.get("NextFlowId");
				if (nextFlowId != null && !nextFlowId.equals("")) {
					String[] ids = nextFlowId.split(",");
					for (int i = 0; i < ids.length; i++) {
						List<NextFlow> nextFlow = workOrderDao
								.selectNextFlowNameById(ids[i]);
						for (NextFlow n : nextFlow) {
							if (!nfId.contains(n.getId())) {
								NextFlow nf = new NextFlow();
								nf.setId(n.getId());
								nf.setName(n.getName());
								nf.setApplicationTypeTable(n
										.getApplicationTypeTable());
								nf.setCreateUser(n.getCreateUser());
								nf.setCreateDate(n.getCreateDate());
								list_nextFlow.add(nf);
								nfId.add(n.getId());
							}
						}
					}
				} else {
					if (sampleType != null && !"".equals(sampleType)&&!"null".equals(sampleType)) {
						DicSampleType dst = commonDAO.get(DicSampleType.class,
								sampleType);
						String typeId = dst.getType().getId();
						if (map.containsKey(typeId)) {
							String typeFlow = (String) map.get(typeId);
							String[] ids = typeFlow.split(",");
							for (int i = 0; i < ids.length; i++) {
								List<NextFlow> nextFlow = workOrderDao
										.selectNextFlowNameById(ids[i]);
								for (NextFlow n : nextFlow) {
									if (!nfId.contains(n.getId())) {
										NextFlow nf = new NextFlow();
										nf.setId(n.getId());
										nf.setName(n.getName());
										nf.setApplicationTypeTable(n
												.getApplicationTypeTable());
										nf.setCreateUser(n.getCreateUser());
										nf.setCreateDate(n.getCreateDate());
										list_nextFlow.add(nf);
										nfId.add(n.getId());
									}
								}
							}
						}
					}

				}
			}

		}
		if("SampleReceive".equals(model)){
		}else if("PlasmaTask".equals(model)){
			
		}else if("DnaTask".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0010");
			list_nextFlow.add(nf1);
		}else if("WkTask".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0010");
			NextFlow nf2=commonDAO.get(NextFlow.class, "0011");
			NextFlow nf3=commonDAO.get(NextFlow.class, "0012");
			NextFlow nf4=commonDAO.get(NextFlow.class, "0013");
			NextFlow nf5=commonDAO.get(NextFlow.class, "0014");
			list_nextFlow.add(nf1);
			list_nextFlow.add(nf2);
			list_nextFlow.add(nf3);
			list_nextFlow.add(nf4);
			list_nextFlow.add(nf5);
		}else if("ZJRWD".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0017");
			NextFlow nf2=commonDAO.get(NextFlow.class, "0019");
			list_nextFlow.add(nf1);
			list_nextFlow.add(nf2);
		}else if("JKRWD".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0018");
			list_nextFlow.add(nf1);
		}else if("SJRWD".equals(model)){
			NextFlow nf1=commonDAO.get(NextFlow.class, "0023");
			list_nextFlow.add(nf1);
		}
		NextFlow nf0=commonDAO.get(NextFlow.class, "0009");
//		NextFlow nf6=commonDAO.get(NextFlow.class, "0024");
		list_nextFlow.add(nf0);
//		list_nextFlow.add(nf6);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("recordsTotal", list_nextFlow.size());
		result.put("recordsFiltered", list_nextFlow.size());
		result.put("list", list_nextFlow);
		return result;
	}

	public List<WorkOrderItem> findWorkOrderItemListById(String id) {
		return workOrderDao.findWorkOrderItemListById(id);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(String id, String itemJson) throws Exception {
		List<Map<String,Object>> list = JsonUtils.toListByJsonArray(itemJson, List.class);
		WorkOrder workOrder = commonDAO.get(WorkOrder.class, id);
		for (Map<String, Object> map : list) {
			WorkOrderItem wi = new WorkOrderItem();
			wi = (WorkOrderItem) commonDAO.Map2Bean(map, wi);
			wi.setWorkOrder(workOrder);
			commonDAO.saveOrUpdate(wi);
		}
		
	}


}
