package com.biolims.system.work.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.work.dao.UnitGroupNewDao;
import com.biolims.system.work.model.UnitGroupNew;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class UnitGroupNewService {
	@Resource
	private UnitGroupNewDao unitGroupNewDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findWorkTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return unitGroupNewDao.selectWorkTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	public Map<String, Object> findWorkTypeList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return unitGroupNewDao.selectWorkTypeList(start, length, query, col, sort);
	}
	public Map<String, Object> findWorkTypeListByUnit(Integer start,
			Integer length, String query, String col, String sort,String unit) throws Exception {
		return unitGroupNewDao.selectWorkTypeListByUnit(start, length, query, col, sort,unit);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(UnitGroupNew i) throws Exception {

		unitGroupNewDao.saveOrUpdate(i);

	}
	public UnitGroupNew get(String id) {
		UnitGroupNew unitGroupNew = commonDAO.get(UnitGroupNew.class, id);
		return unitGroupNew;
	}
//	public Map<String, Object> findWorkTypeItemList(String scId, Integer startNum, Integer limitNum, String dir,
//			String sort) throws Exception {
//		Map<String, Object> result = unitGroupNewDao.selectWorkTypeItemList(scId, startNum, limitNum, dir, sort);
//		List<WorkTypeItem> list = (List<WorkTypeItem>) result.get("list");
//		return result;
//	}
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveWorkTypeItem(WorkType sc, String itemDataJson) throws Exception {
//		List<WorkTypeItem> saveItems = new ArrayList<WorkTypeItem>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			WorkTypeItem scp = new WorkTypeItem();
//			// 将map信息读入实体类
//			scp = (WorkTypeItem) unitGroupNewDao.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			scp.setWorkType(sc);
//
//			saveItems.add(scp);
//		}
//		unitGroupNewDao.saveOrUpdateAll(saveItems);
//	}
//	/**
//	 * 删除明细
//	 * @param ids
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delWorkTypeItem(String[] ids) throws Exception {
//		for (String id : ids) {
//			WorkTypeItem scp =  workTypeDao.get(WorkTypeItem.class, id);
//			 workTypeDao.delete(scp);
//		}
//	}
//	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(UnitGroupNew sc, String logInfo,String log) throws Exception {
		if (sc != null) {
			unitGroupNewDao.saveOrUpdate(sc);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("UnitGroupNew");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
	}
   }
//   /**
//	* 获得子节点列表信息
//	* @param list
//	* @param node
//	* @return
//	*/
//	public List<WorkType> getChildList(List<WorkType> list, WorkType node) throws Exception { //得到子节点列表   
//		List<WorkType> li = new ArrayList<WorkType>();
//		Iterator<WorkType> it = list.iterator();
//		while (it.hasNext()) {
//			WorkType n = (WorkType) it.next();
//			if (n.getParent() != null && n.getParent().getId().equals(node.getId())) {
//				li.add(n);
//			}
//		}
//		return li;
//	}
//   /**
//	 * 判断是否有子节点
//	 * @param list
//	 * @param node
//	 * @return
//	 */
//	public boolean hasChild(List<WorkType> list, WorkType node) throws Exception {
//		return getChildList(list, node).size() > 0 ? true : false;
//	}
//   	public boolean hasChildCount(WorkType node) throws Exception {
//
//		Long c = commonDAO.getCount("from WorkType where parent.id='"
//				+ node.getId() + "'");
//		return c > 0 ? true : false;
//	}
//   
//   public String getJson(List<WorkType> list) throws Exception {
//		if (list != null && list.size() > 0) {
//		}
//		json = new StringBuffer();
//		List<WorkType> nodeList0 = new ArrayList<WorkType>();
//		Iterator<WorkType> it1 = list.iterator();
//		while (it1.hasNext()) {
//			WorkType node = (WorkType) it1.next();
//			//if (node.getLevel() == 0) {
//			nodeList0.add(node);
//			//}
//		}
//		Iterator<WorkType> it = nodeList0.iterator();
//		while (it.hasNext()) {
//			WorkType node = (WorkType) it.next();
//			constructorJson(list, node);
//		}
//		String jsonDate = json.toString();
//		return ("[" + jsonDate + "]").replaceAll(",]", "]");
//	}
//	
//	/**
//	 * 构建Json文件
//	 * @param list
//	 * @param node
//	 */
//
//	public void constructorJson(List<WorkType> list, WorkType treeNode) throws Exception {
//		json.append("{");
//		json.append("\"id\":'");
//		json.append(JsonUtils.formatStr(treeNode.getId() == null ? "" : treeNode.getId()) + "");
//		json.append("',");
//		json.append("\"name\":'");
//		json.append(JsonUtils.formatStr(treeNode.getName() == null ? "" : treeNode.getName()) + "");
//		json.append("',");
////		json.append("\"cycle\":'");
////		json.append(JsonUtils.formatStr(treeNode.getCycle() == null ? "" : treeNode.getCycle().getName()) + "");
////		json.append("',");
//		json.append("\"parent\":'");
//		json.append(JsonUtils.formatStr(treeNode.getParent() == null ? "" : treeNode.getParent().getName()) + "");
////		json.append("',");
////		json.append("\"state\":'");
////		json.append(JsonUtils.formatStr(treeNode.getState() == null ? "" : treeNode.getState()) + "");
//		json.append("',");
//		if (hasChildCount(treeNode)) {
//			json.append("\"leaf\":false");
//		} else {
//			json.append("\"leaf\":true");
//		}
//		json.append(",\"upId\":'");
//		json.append((treeNode.getParent() == null ? "" : treeNode.getParent().getId()) + "");
//		json.append("'}");
//		
//	}
//	
//   	public List<WorkType> findWorkTypeList(String upId) throws Exception {
//		List<WorkType> list = null;
//		if (upId.equals("0")) {
//			list = commonDAO
//					.find("from WorkType where  parent.id is null order by id asc");
//		} else {
//
//			list = commonDAO.find("from WorkType where parent.id='"
//					+ upId + "' order by id asc");
//		}
//		return list;
//	}
//	public List<WorkType> findWorkTypeList() throws Exception {
//		List<WorkType> list = commonDAO
//				.find("from WorkType where  parent.id is null order by id asc");
//		return list;
//	}
//	//根据标识码加载检测方法
//	public List<Map<String, String>> setWorkType(String code) throws Exception {
//		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
//		Map<String, Object> result = workTypeDao.setWorkType(code);
//		List<WorkType> list = (List<WorkType>) result.get("list");
//
//		if (list != null && list.size() > 0) {
//			for (WorkType srai : list) {
//				Map<String, String> map = new HashMap<String, String>();
//				map.put("id", srai.getId());
//				map.put("name", srai.getName());
//				map.put("state", srai.getState());
//				if(srai.getCycle()!=null){
//					map.put("cycle", srai.getCycle().toString());
//				}else{
//					map.put("cycle", "");
//				}
//				if(srai.getPerson()!=null){
//					map.put("personName", srai.getPerson().getName());
//					map.put("personId", srai.getPerson().getId());
//				}else{
//					map.put("personName", "");
//					map.put("personId", "");
//				}
//				
//				map.put("mark", srai.getMark());
//				mapList.add(map);
//			}
//		}
//		return mapList;
//	}
}
