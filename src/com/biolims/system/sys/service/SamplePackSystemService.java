package com.biolims.system.sys.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.sys.dao.SamplePackSystemDao;
import com.biolims.system.sys.model.SamplePackSystem;
import com.biolims.system.sys.model.SamplePackSystemItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SamplePackSystemService {
	@Resource
	private SamplePackSystemDao samplePackSystemDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSamplePackSystemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return samplePackSystemDao.selectSamplePackSystemList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SamplePackSystem i) throws Exception {

		samplePackSystemDao.saveOrUpdate(i);

	}
	public SamplePackSystem get(String id) {
		SamplePackSystem samplePackSystem = commonDAO.get(SamplePackSystem.class, id);
		return samplePackSystem;
	}
	public Map<String, Object> findSamplePackSystemItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = samplePackSystemDao.selectSamplePackSystemItemList(scId, startNum, limitNum, dir, sort);
		List<SamplePackSystemItem> list = (List<SamplePackSystemItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplePackSystemItem(SamplePackSystem sc, String itemDataJson) throws Exception {
		List<SamplePackSystemItem> saveItems = new ArrayList<SamplePackSystemItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SamplePackSystemItem scp = new SamplePackSystemItem();
			// 将map信息读入实体类
			scp = (SamplePackSystemItem) samplePackSystemDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSamplePackSystem(sc);

			saveItems.add(scp);
		}
		samplePackSystemDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePackSystemItem(String[] ids) throws Exception {
		for (String id : ids) {
			SamplePackSystemItem scp =  samplePackSystemDao.get(SamplePackSystemItem.class, id);
			 samplePackSystemDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SamplePackSystem sc, Map jsonMap) throws Exception {
		if (sc != null) {
			samplePackSystemDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("samplePackSystemItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSamplePackSystemItem(sc, jsonStr);
			}
	}
   }
}
