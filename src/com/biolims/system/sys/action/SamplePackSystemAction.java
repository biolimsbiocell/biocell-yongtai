﻿
package com.biolims.system.sys.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.sys.model.SamplePackSystem;
import com.biolims.system.sys.model.SamplePackSystemItem;
import com.biolims.system.sys.service.SamplePackSystemService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/com/biolims/system/sys/samplePackSystem")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SamplePackSystemAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9002";
	@Autowired
	private SamplePackSystemService samplePackSystemService;
	private SamplePackSystem samplePackSystem = new SamplePackSystem();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showSamplePackSystemList")
	public String showSamplePackSystemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/sys/samplePackSystem.jsp");
	}

	@Action(value = "showSamplePackSystemListJson")
	public void showSamplePackSystemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = samplePackSystemService.findSamplePackSystemList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SamplePackSystem> list = (List<SamplePackSystem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("bloodTube-id", "");
		map.put("bloodTube-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "samplePackSystemSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSamplePackSystemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/sys/samplePackSystemDialog.jsp");
	}

	@Action(value = "showDialogSamplePackSystemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSamplePackSystemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = samplePackSystemService.findSamplePackSystemList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SamplePackSystem> list = (List<SamplePackSystem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("bloodTube-id", "");
		map.put("bloodTube-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editSamplePackSystem")
	public String editSamplePackSystem() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			samplePackSystem = samplePackSystemService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "samplePackSystem");
		} else {
			//samplePackSystem.setId("NEW");
			samplePackSystem=new SamplePackSystem();
			//samplePackSystem.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			samplePackSystem.setCreateUser(user);
//			samplePackSystem.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/com/biolims/system/sys/samplePackSystemEdit.jsp");
	}

	@Action(value = "copySamplePackSystem")
	public String copySamplePackSystem() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		samplePackSystem = samplePackSystemService.get(id);
		samplePackSystem.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/com/biolims/system/sys/samplePackSystemEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = samplePackSystem.getId();
		if ((id != null && id.equals(""))) {
			//String modelName = "SamplePackSystem";
			//String markCode="DBTX";
			//String autoID = codingRuleService.genTransID(modelName,markCode);
			samplePackSystem.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("samplePackSystemItem",getParameterFromRequest("samplePackSystemItemJson"));
		
		samplePackSystemService.save(samplePackSystem,aMap);
		return redirect("/com/biolims/system/sys/samplePackSystem/editSamplePackSystem.action?id=" + samplePackSystem.getId());

	}

	@Action(value = "viewSamplePackSystem")
	public String toViewSamplePackSystem() throws Exception {
		String id = getParameterFromRequest("id");
		samplePackSystem = samplePackSystemService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/sys/samplePackSystemEdit.jsp");
	}
	

	@Action(value = "showSamplePackSystemItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePackSystemItemList() throws Exception {
		return dispatcher("/WEB-INF/page/com/biolims/system/sys/samplePackSystemItem.jsp");
	}

	@Action(value = "showSamplePackSystemItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePackSystemItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = samplePackSystemService.findSamplePackSystemItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SamplePackSystemItem> list = (List<SamplePackSystemItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("num", "");
			map.put("scope", "");
			map.put("samplePackSystem-name", "");
			map.put("samplePackSystem-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSamplePackSystemItem")
	public void delSamplePackSystemItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			samplePackSystemService.delSamplePackSystemItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SamplePackSystemService getSamplePackSystemService() {
		return samplePackSystemService;
	}

	public void setSamplePackSystemService(SamplePackSystemService samplePackSystemService) {
		this.samplePackSystemService = samplePackSystemService;
	}

	public SamplePackSystem getSamplePackSystem() {
		return samplePackSystem;
	}

	public void setSamplePackSystem(SamplePackSystem samplePackSystem) {
		this.samplePackSystem = samplePackSystem;
	}


}
