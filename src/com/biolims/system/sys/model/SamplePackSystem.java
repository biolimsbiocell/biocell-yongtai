package com.biolims.system.sys.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.product.model.Product;
/**   
 * @Title: Model
 * @Description: 打包体系
 * @author lims-platform
 * @date 2015-11-03 18:10:34
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_SAMPLE_PACK_SYSTEM")
@SuppressWarnings("serial")
public class SamplePackSystem extends EntityDao<SamplePackSystem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**状态*/
	private String state;
	/**业务类型*/
	private Product product;
	/**样本类型*/
	private DicType sampleType;
	/**采血管类型*/
	private DicType bloodTube;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BLOOD_TUBE")
	public DicType getBloodTube() {
		return bloodTube;
	}
	public void setBloodTube(DicType bloodTube) {
		this.bloodTube = bloodTube;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 60)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}