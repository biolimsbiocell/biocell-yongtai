package com.biolims.system.sys.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 打包体系明细
 * @author lims-platform
 * @date 2015-11-03 18:10:24
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_SAMPLE_PACK_SYSTEM_ITEM")
@SuppressWarnings("serial")
public class SamplePackSystemItem extends EntityDao<SamplePackSystemItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**值*/
	private Integer num;
	/**范围*/
	private String scope;
	/**相关主表*/
	private SamplePackSystem samplePackSystem;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 60)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  值
	 */
	@Column(name ="NUM", length = 20)
	public Integer getNum(){
		return this.num;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  值
	 */
	public void setNum(Integer num){
		this.num = num;
	}
	/**
	 *方法: 取得SamplePackSystem
	 *@return: SamplePackSystem  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_PACK_SYSTEM")
	public SamplePackSystem getSamplePackSystem(){
		return this.samplePackSystem;
	}
	/**
	 *方法: 设置SamplePackSystem
	 *@param: SamplePackSystem  相关主表
	 */
	public void setSamplePackSystem(SamplePackSystem samplePackSystem){
		this.samplePackSystem = samplePackSystem;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
}