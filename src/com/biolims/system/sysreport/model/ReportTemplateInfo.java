package com.biolims.system.sysreport.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;

/**
 * 报告模版
 * @author note
 *
 */
@Entity
@Table(name = "SYS_REPORT_TEMPLATE_INFO")
public class ReportTemplateInfo extends EntityDao<ReportTemplateInfo> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9078042130280128778L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "Name", length = 110)
	private String name;

	@Column(name = "NOTE", length = 200)
	private String note;

	@Column(name = "STATE", length = 32)
	private String state;

	@Column(name = "STATE_NAME", length = 110)
	private String stateName;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ATTACH_FILE_INFO_ID")
	private FileInfo attach;//附件
	
	private String type;
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public FileInfo getAttach() {
		return attach;
	}

	public void setAttach(FileInfo attach) {
		this.attach = attach;
	}

}
