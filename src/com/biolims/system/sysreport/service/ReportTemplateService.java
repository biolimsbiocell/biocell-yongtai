package com.biolims.system.sysreport.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.experiment.plasma.model.PlasmaTaskCos;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.system.sysreport.dao.ReportTemplateDao;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings( { "rawtypes", "unchecked" })
public class ReportTemplateService {

	@Resource
	private ReportTemplateDao reportTemplateDao;

	/**
	 * 查询报告模板基本信息列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReportTemplateInfoItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return reportTemplateDao.selectReportTemplateInfoItem(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 批量保存或更新报告模板基本信息
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String itemDataJson) throws Exception {
		List<ReportTemplateInfo> saveItems = new ArrayList<ReportTemplateInfo>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			ReportTemplateInfo rti = new ReportTemplateInfo();
			rti = (ReportTemplateInfo) reportTemplateDao.Map2Bean(map, rti);
			saveItems.add(rti);
		}
		reportTemplateDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 批量删除报告模板基本信息
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delete(String ids) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("id", "in##@@##(" + ids + ")");
		this.reportTemplateDao.delete(mapForQuery);
	}
	
	/**
	 * 查询模板
	 * @param genotype
	 * @return
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<Map<String, String>> selectTypes(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = reportTemplateDao.selectType(code);
		List<ReportTemplateInfo> list = (List<ReportTemplateInfo>) result.get("list");
		if (list != null && list.size() > 0) {
			for (ReportTemplateInfo ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("genotype", ti.getType());
				map.put("actioncolumn", ti.getAttach().toString());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
