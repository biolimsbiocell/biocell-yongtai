package com.biolims.system.sysreport.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.plasma.model.PlasmaTaskCos;
import com.biolims.system.sysreport.model.ReportTemplateInfo;

@Repository
@SuppressWarnings("unchecked")
public class ReportTemplateDao extends BaseHibernateDao {

	public Map<String, Object> selectReportTemplateInfoItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from ReportTemplateInfo where 1 = 1";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ReportTemplateInfo> list = new ArrayList<ReportTemplateInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public void delete(Map<String, String> mapForQuery) throws Exception {
		String key = "";
		String hql = "delete from ReportTemplateInfo where 1=1";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}
	
	/**
	 * 查询模板
	 * @param type
	 * @return
	 */
	public Map<String, Object> selectType(String type){
//		String hql = "from ReportTemplateInfo where 1 = 1 and type='"+type+"'";
//		List<ReportTemplateInfo> list=this.getSession().createQuery(hql).list();
//		return list;
		String hql = "from ReportTemplateInfo where 1 = 1 and type='"+type+"'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<ReportTemplateInfo> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}
