package com.biolims.system.sysreport.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.system.sysreport.service.ReportTemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/sysmanage/report")
@SuppressWarnings("unchecked")
public class ReportTemplateAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8354314800608144980L;

	private String rightsId = "1119";
	@Autowired
	private ReportTemplateService reportTemplateService;

	/**
	 * 查询报告模板基本信息列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showReportTemplateList")
	public String showReportTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sysreport/showReportList.jsp");
	}

	@Action(value = "showReportTemplateListJson")
	public void showReportTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Long total = 0l;
			List<ReportTemplateInfo> list = new ArrayList<ReportTemplateInfo>();
			Map<String, Object> result = this.reportTemplateService.findReportTemplateInfoItem(null, startNum,
					limitNum, dir, sort);
			total = (Long) result.get("total");
			list = (List<ReportTemplateInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "");
			map.put("note", "");
			map.put("state", "");
			map.put("type", "");
			map.put("attach-fileName", "");
			map.put("attach-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 批量保存报告模板基本信息
	 * @throws Exception
	 */
	@Action(value = "save")
	public void save() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			reportTemplateService.save(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除报告模板基本信息
	 * @throws Exception
	 */
	@Action(value = "delReportTemplateInfo")
	public void delReportTemplateInfo() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String ids = getRequest().getParameter("ids");
			this.reportTemplateService.delete(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 选择报告模板列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selReportTemplateList")
	public String selReportTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sysreport/selReportTemplateList.jsp");
	}

	@Action(value = "selReportTemplateListJson")
	public void selReportTemplateListJson() throws Exception {
		// 开始记录数
		Integer startNum = (getParameterFromRequest("start") == "" ? null : Integer
				.parseInt(getParameterFromRequest("start")));
		// limit
		Integer limitNum = (getParameterFromRequest("limit") == "" ? null : Integer
				.parseInt(getParameterFromRequest("limit")));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String data = getRequest().getParameter("data");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (data != null && data.length() > 0) {
			Map<String, String> map = JsonUtils.toObjectByJson(data, Map.class);
			mapForQuery.put("name", "like##@@##'%" + map.get("name") + "%' ");
			if (map.get("note") != null && map.get("note").length() > 0)
				mapForQuery.put("note", "like##@@##'%" + map.get("note") + "%'");
		}
		//		mapForQuery.put("state", "1");
		try {
			Long total = 0l;
			List<ReportTemplateInfo> list = new ArrayList<ReportTemplateInfo>();
			Map<String, Object> result = this.reportTemplateService.findReportTemplateInfoItem(mapForQuery, startNum,
					limitNum, dir, sort);
			total = (Long) result.get("total");
			list = (List<ReportTemplateInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查询模板
	 * @return
	 */
//	@Action(value = "selectType")
//	public List<ReportTemplateInfo> selectTypes(){
//		// 基因型
//		String genotype = getParameterFromRequest("genotype");
//		List<ReportTemplateInfo> list=this.reportTemplateService.selectTypes(genotype);
//		return list;
//	}
	@Action(value = "selectType")
	public void selectType() throws Exception {
		String genotype = getParameterFromRequest("genotype");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> listMap = (List<Map<String, String>>) this.reportTemplateService.selectTypes(genotype);
			result.put("success", true);
			result.put("data", listMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
