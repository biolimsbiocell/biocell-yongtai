package com.biolims.system.nextFlow.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.nextFlow.service.NextFlowService;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.system.work.service.WorkOrderService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/nextFlow/nextFlow")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class NextFlowAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9018";
	@Autowired
	private NextFlowService nextFlowService;
	private NextFlow nextFlow = new NextFlow();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkOrderService workOrderService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	@Action(value = "showNextFlowList")
	public String showNextFlowList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/nextFlow/nextFlow.jsp");
	}

	@Action(value = "showNextFlowListJson")
	public void showNextFlowListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = nextFlowService.findNextFlowList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "nextFlowSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogNextFlowList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/nextFlow/nextFlowDialog.jsp");
	}

	@Action(value = "showDialogNextFlowListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogNextFlowListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = nextFlowService.findNextFlowList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editNextFlow")
	public String editNextFlow() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			nextFlow = nextFlowService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "nextFlow");
		} else {
			nextFlow.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			nextFlow.setCreateUser(user);
			nextFlow.setCreateDate(new Date());
			nextFlow.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_YES);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/nextFlow/nextFlowEdit.jsp");
	}

	@Action(value = "copyNextFlow")
	public String copyNextFlow() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		nextFlow = nextFlowService.get(id);
		nextFlow.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/nextFlow/nextFlowEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = nextFlow.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "NextFlow";
			String autoID = codingRuleService.getCodeByPrefix(modelName, "",
					"", 0000, 4, null);
			nextFlow.setId(autoID);
		}
		Map aMap = new HashMap();
		nextFlowService.save(nextFlow, aMap);
		return redirect("/system/nextFlow/nextFlow/editNextFlow.action?id="
				+ nextFlow.getId());

	}

	@Action(value = "viewNextFlow")
	public String toViewNextFlow() throws Exception {
		String id = getParameterFromRequest("id");
		nextFlow = nextFlowService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/nextFlow/nextFlowEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public NextFlowService getNextFlowService() {
		return nextFlowService;
	}

	public void setNextFlowService(NextFlowService nextFlowService) {
		this.nextFlowService = nextFlowService;
	}

	public NextFlow getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(NextFlow nextFlow) {
		this.nextFlow = nextFlow;
	}

	@Action(value = "showDialogWorkOrderNextFlowList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWorkOrderNextFlowList() throws Exception {
		return dispatcher("/WEB-INF/page/com/biolims/system/work/showDialogNextFlowList.jsp");
	}

	@Action(value = "showDialogWorkOrderNextFlowListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWorkOrderNextFlowListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = nextFlowService.findNextFlowListState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	/**
		 *	展示所有的质检信息dialog
	     * @Title: showAllQualityTestDialogList  
	     * @Description: TODO  
	     * @param @return
	     * @param @throws Exception    
	     * @return String  
		 * @author 孙灵达  
	     * @date 2018年9月11日
	     * @throws
	 */
	@Action(value = "showAllQualityTestDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAllQualityTestDialogList() throws Exception {
		return dispatcher("/WEB-INF/page/system/nextFlow/showAllQualityTestDialog.jsp");
	}
	
	@Action(value = "showAllQualityTestDialogListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAllQualityTestDialogListJson() throws Exception {
		String mark = "QualityTest";
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = nextFlowService.showAllQualityTestDialogList(start, length, 
					query, col, sort, mark);
			List<NextFlow> list = (List<NextFlow>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("NextFlow");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 根据检测项目查询下一步流程
	@Action(value = "shownextFlowDialog", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String shownextFlowDialog() throws Exception {
		String model = getParameterFromRequest("model");
		String productId = getParameterFromRequest("productId");
		String n = getParameterFromRequest("n");
		putObjToContext("n", n);
		return dispatcher("/WEB-INF/page/system/nextFlow/shownextFlowDialog.jsp?model="
				+ model + "&productId=" + productId);
	}

	@Action(value = "shownextFlowDialogJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void shownextFlowDialogJson() throws Exception {
		String model = getParameterFromRequest("model");
		String productId = getParameterFromRequest("productId");
		Map<String, Object> result = workOrderService.selectNextFlow(model,
				productId);
		int count = (Integer) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	/**
	 * 
	 * @Title: selectNextFlow  
	 * @Description: 实验配置选择流程  
	 * @author : shengwei.wang
	 * @date 2018年4月11日上午10:45:22
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="selectNextFlow", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectNextFlow() {
		return dispatcher("/WEB-INF/page/system/nextFlow/selectNextFlow.jsp");
	}
	@Action(value = "selectNextFlowJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectNextFlowJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = nextFlowService.selectNextFlowJson(start, length, query, col, sort);
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("mainTable-moduleType", "");
		/*map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");*/
		map.put("mainTable-id", "");
		map.put("mainTable-name", "");
		map.put("mainTable-enName", "");
		map.put("itemResultTable", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result,data));
		}
	@Action(value="selectAllNextFlow")
	public String selectAllNextFlow() {
		return dispatcher("/WEB-INF/page/system/nextFlow/selectNextFlowDialog.jsp");
	}
	@Action(value = "selectAllNextFlowJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectAllNextFlowJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = nextFlowService.selectNextFlowJson(start, length, query, col, sort);
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("mainTable-id", "");
		map.put("mainTable-name", "");
		map.put("mainTable-enName", "");
		map.put("itemResultTable", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result,data));
		}
	/**
	 * @throws UnsupportedEncodingException 
	 * 
	 * @Title: shownextFlowDialogTable  
	 * @Description: 选择下一步流向 
	 * @author : shengwei.wang
	 * @date 2018年2月27日下午2:58:52
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="shownextFlowDialogTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String shownextFlowDialogTable() throws UnsupportedEncodingException {
		String model = getParameterFromRequest("model");
		String productId = getParameterFromRequest("productId");
//		String sampleType = getParameterFromRequest("sampleType");
		String sampleType = URLDecoder.decode(getParameterFromRequest("sampleType"), "UTF-8");
		String sampleTypeid = getParameterFromRequest("sampleType");
		if(sampleType==null||"".equals(sampleType)){
			sampleType = "";
		}else{
			DicSampleType dst = commonDAO.get(DicSampleType.class, sampleType);
			if(dst!=null){
				sampleType = dst.getId();
			}else{
				List<DicSampleType> dsts = commonService.get(DicSampleType.class,"name",sampleTypeid);
				if(dsts.size()>0){
					DicSampleType ds = dsts.get(0);
					sampleType = ds.getId();
				}else{
					sampleType = "";
				}
			}
		}
		putObjToContext("model", model);
		putObjToContext("productId", productId);
		putObjToContext("sampleType", sampleType);
		return dispatcher("/WEB-INF/page/system/nextFlow/shownextFlowDialogTable.jsp");
	}
	@Action(value = "shownextFlowDialogTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void shownextFlowDialogTableJson() throws Exception {
		String model = getParameterFromRequest("model");
		String productId = getParameterFromRequest("productId");
//		String sampleType = URLDecoder.decode(getParameterFromRequest("sampleType"), "UTF-8");
		String sampleType = getParameterFromRequest("sampleType");
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = workOrderService.shownextFlowDialogTableJson(sampleType,model,
				productId,start, length, query, col, sort);
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("mainTable-id", "");
		map.put("mainTable-name", "");
		map.put("mainTable-enName", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result,data));
		}
			
	// 根据样本类型查询下一步流程
	@Action(value = "shownextFlowDialogBysampleTypeId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String shownextFlowDialogBysampleTypeId() throws Exception {
		String sampleTypeId = getParameterFromRequest("sampleTypeId");
		return dispatcher("/WEB-INF/page/system/nextFlow/shownextFlowDialog.jsp?sampleTypeId="
				+ sampleTypeId);
	}

	@Action(value = "shownextFlowDialogBysampleTypeIdJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void shownextFlowDialogBysampleTypeIdJson() throws Exception {
		String sampleTypeId = getParameterFromRequest("sampleTypeId");
		Map<String, Object> result = workOrderService
				.selectNextFlowBysampleTypeId(sampleTypeId);
		int count = (Integer) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("enName", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 查询全部下一步
	@Action(value = "shownextFlowDialogAll", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String shownextFlowDialogAll() throws Exception {
		return dispatcher("/WEB-INF/page/system/nextFlow/shownextFlowDialog.jsp");
	}

	@Action(value = "shownextFlowDialogAllJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void shownextFlowDialogAllJson() throws Exception {
		Map<String, Object> result = nextFlowService.selectNextFlowAll();
		Long count = (Long) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 查询富集后的下一步
	@Action(value = "showPoolingTaskNextFlow", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPoolingTaskNextFlow() throws Exception {
		String model = getParameterFromRequest("model");
		String sequencingPlatform = getParameterFromRequest("sequencingPlatform");
		// String n = getParameterFromRequest("n");
		// putObjToContext("n", n);
		return dispatcher("/WEB-INF/page/system/nextFlow/shownextFlowDialog.jsp?model="
				+ model + "&sequencingPlatform=" + sequencingPlatform);
	}

	@Action(value = "showPoolingTaskNextFlowJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPoolingTaskNextFlowJson() throws Exception {

		String model = getParameterFromRequest("model");
		String sequencingPlatform = getParameterFromRequest("sequencingPlatform");
		Map<String, Object> result = workOrderService.selectNextFlowBymodel(
				model, sequencingPlatform);
		int count = (Integer) result.get("total");
		List<NextFlow> list = (List<NextFlow>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("applicationTypeTable-id", "");
		map.put("applicationTypeTable-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("sort", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 查询默认下一步流向
	@Action(value = "selectdnextId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectdnextId() throws Exception {
		// String code = getRequest().getParameter("code");
		String model = getParameterFromRequest("model");
		String productId = getParameterFromRequest("productId");
		// Map<String, Object> result = new HashMap<String, Object>();
		WorkOrderItem list = workOrderService.selectdnextId(model, productId);
		// try {
		//
		//
		// result.put("success", true);
		// result.put("data", list);
		//
		// } catch (Exception e) {
		// result.put("success", false);
		// }
		HttpUtils.write(JsonUtils.toJsonString(list));
	}

	/**
	 * 回滚 删除进入下一步的样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "RollBack", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void RollBack() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String code = getParameterFromRequest("code");
			String nextFlowId = getParameterFromRequest("nextFlowId");
			nextFlowService.RollBack(code, nextFlowId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 办理回滚
	 * 
	 * @throws Exception
	 */
	@Action(value = "handleRollBack", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void handleRollBack() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String model = getParameterFromRequest("model");
			String id = getParameterFromRequest("id");
			nextFlowService.handleRollBack(model, id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
