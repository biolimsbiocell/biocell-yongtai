package com.biolims.system.nextFlow.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.cfdna.model.CfdnaTaskTemp;
import com.biolims.experiment.cfdna.service.CfdnaTaskService;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.dnap.model.DnapTaskTemp;
import com.biolims.experiment.dnap.service.DnapTaskService;
import com.biolims.experiment.other.model.OtherTaskTemp;
import com.biolims.experiment.other.service.OtherTaskService;
import com.biolims.experiment.pcr.model.PcrTaskTemp;
import com.biolims.experiment.pcr.service.PcrTaskService;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.pooling.service.PoolingService;
import com.biolims.experiment.qc.agros.model.AgrosTaskTemp;
import com.biolims.experiment.qc.agros.service.AgrosTaskService;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.qc.qpcrjd.service.QpcrjdTaskService;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemp;
import com.biolims.experiment.qc.qpcrxd.service.QpcrxdTaskService;
import com.biolims.experiment.qc.service.WKQpcrSampleTaskService;
import com.biolims.experiment.qc.service.WKQualitySampleTaskService;
import com.biolims.experiment.sequencing.model.SequencingTaskTemp;
import com.biolims.experiment.sj.wkblend.service.WkBlendTaskService;
import com.biolims.experiment.sj.wknd.service.WkndTaskService;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.uf.service.UfTaskService;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.wk.service.WkTaskService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class NextFlowService {
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private BloodSplitService bloodSplitService;
	@Resource
	private UfTaskService ufTaskService;
	@Resource
	private DnaTaskService dnaTaskService;
	@Resource
	private WKQualitySampleTaskService wKQualitySampleTaskService;
	@Resource
	private WKQpcrSampleTaskService wKQpcrSampleTaskService;
	@Resource
	private AgrosTaskService agrosTaskService;
	@Resource
	private QpcrxdTaskService qpcrxdTaskService;
	@Resource
	private WkBlendTaskService wkBlendTaskService;
	@Resource
	private WkndTaskService wkndTaskService;
	@Resource
	private WkTaskService wkTaskService;
	@Resource
	private QpcrjdTaskService qpcrjdTaskService;
	@Resource
	private PoolingService poolingService;
	@Resource
	private DnapTaskService dnapTaskService;
	@Resource
	private PcrTaskService pcrTaskService;
	@Resource
	private CfdnaTaskService cfdnaTaskService;
	@Resource
	private OtherTaskService otherTaskService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findNextFlowList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return nextFlowDao.selectNextFlowList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	public Map<String, Object> findNextFlowListState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		mapForQuery.put("state", "1");
		return nextFlowDao.selectNextFlowList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	public Map<String, Object> selectNextFlowAll() {
		return nextFlowDao.selectNextFlowAll();
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(NextFlow i) throws Exception {

		nextFlowDao.saveOrUpdate(i);

	}

	public NextFlow get(String id) {
		NextFlow nextFlow = commonDAO.get(NextFlow.class, id);
		return nextFlow;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(NextFlow sc, Map jsonMap) throws Exception {
		if (sc != null) {
			nextFlowDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 回滚 删除进入下一步的样本
	 * 
	 * @param code
	 * @param nextFlowId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void RollBack(String code, String nextFlowId) throws Exception {
		if (nextFlowId.equals("0002")) {// 核酸提取样本接收
//			DnaReceiveTemp scp = (DnaReceiveTemp) nextFlowDao.RollBack(code,
//					"DnaReceiveTemp");
//			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0004")) {// 文库质控文库接收
			QcReceiveTemp scp = (QcReceiveTemp) nextFlowDao.RollBack(code,
					"QcReceiveTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0005")) {// PCR扩增
			PcrTaskTemp scp = (PcrTaskTemp) nextFlowDao.RollBack(code,
					"PcrTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0006")) {// DNA纯化
			DnapTaskTemp scp = (DnapTaskTemp) nextFlowDao.RollBack(code,
					"DnapTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0007")) {// cfDNA质量评估
			CfdnaTaskTemp scp = (CfdnaTaskTemp) nextFlowDao.RollBack(code,
					"CfdnaTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0008")) {// 其他实验
			OtherTaskTemp scp = (OtherTaskTemp) nextFlowDao.RollBack(code,
					"OtherTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0009")) {// 样本入库
			SampleInItemTemp scp = (SampleInItemTemp) nextFlowDao.RollBack(
					code, "SampleInItemTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0016")) {// 样本处理
			PlasmaTaskTemp scp = (PlasmaTaskTemp) nextFlowDao.RollBack(code,
					"PlasmaTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0017")) {// 核酸提取
			DnaTaskTemp scp = (DnaTaskTemp) nextFlowDao.RollBack(code,
					"DnaTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0018")) {// 文库构建
			WkTaskTemp scp = (WkTaskTemp) nextFlowDao.RollBack(code,
					"WkTaskTemp");
			nextFlowDao.delete(scp);
		}  else if (nextFlowId.equals("0020")) {// 2100质控
			Qc2100TaskTemp scp = (Qc2100TaskTemp) nextFlowDao.RollBack(code,
					"Qc2100TaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0021")) {// QPCR质控
			QcQpcrTaskTemp scp = (QcQpcrTaskTemp) nextFlowDao.RollBack(code,
					"QcQpcrTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0022")) {// 超声破碎
			UfTaskTemp scp = (UfTaskTemp) nextFlowDao.RollBack(code,
					"UfTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0023")) {// 上机测序
			SequencingTaskTemp scp = (SequencingTaskTemp) nextFlowDao.RollBack(
					code, "SequencingTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0025")) {// 琼脂糖电泳AGROS
			AgrosTaskTemp scp = (AgrosTaskTemp) nextFlowDao.RollBack(code,
					"AgrosTaskTemp");
			nextFlowDao.delete(scp);
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0026")) {// 文库混合QPCR
			QpcrxdTaskTemp scp = (QpcrxdTaskTemp) nextFlowDao.RollBack(code,
					"QpcrxdTaskTemp");
			nextFlowDao.delete(scp);
		} else if (nextFlowId.equals("0027")) {// 文库定量QPCR
			QpcrjdTaskTemp scp = (QpcrjdTaskTemp) nextFlowDao.RollBack(code,
					"QpcrjdTaskTemp");
			nextFlowDao.delete(scp);
		}

	}

	/**
	 * 办理回滚
	 * 
	 * @param code
	 * @param nextFlowId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void handleRollBack(String model, String id) throws Exception {
		if (model.equals("PlasmaTask")) {// 样本处理中的样本处理
			bloodSplitService.changeState("", id);
		} else if (model.equals("UfTask")) {// 超声破碎
			//ufTaskService.changeState("", id);
		} else if (model.equals("ExperimentDnaGet")) {// 核酸提取
			dnaTaskService.changeState("", id);
		} else if (model.equals("WK_TASK")) {// 文库构建
			wkTaskService.changeState("", id);
		} else if (model.equals("Qc2100Task")) {// 2100质控
			wKQualitySampleTaskService.changeState("", id);
		} else if (model.equals("WKQpcr")) {// QPCR质控
			wKQpcrSampleTaskService.changeState("", id);
		} else if (model.equals("AgrosTask")) {// Agros
			agrosTaskService.changeState("", id);
		} else if (model.equals("QpcrxdTask")) {// 文库混合QPCR
//			qpcrxdTaskService.changeState("", id);
		} else if (model.equals("WkBlendTask")) {// 文库混合
			wkBlendTaskService.changeState("", id);
		} else if (model.equals("WkndTask")) {// 文库浓度调整
//			wkndTaskService.changeState("", id);
		} else if (model.equals("QpcrjdTask")) {// 文库定量QPCR
//			qpcrjdTaskService.changeState("", id);
		} else if (model.equals("Pooling")) {// 富集
			poolingService.changeState("", id);
		} else if (model.equals("DnapTask")) {// DNA纯化
			dnapTaskService.changeState("", id);
		} else if (model.equals("PcrTask")) {// PCR
			pcrTaskService.changeState("", id);
		} else if (model.equals("CfdnaTask")) {// cfdna
			cfdnaTaskService.changeState("", id);
		} else if (model.equals("OtherTask")) {// 其他实验
			otherTaskService.changeState("", id);
		}

	}

	public Map<String, Object> selectNextFlowJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return nextFlowDao.selectNextFlowJson(start, length, query, col, sort);
	}

	public Map<String, Object> showAllQualityTestDialogList(Integer start,
			Integer length, String query, String col, String sort, String mark) throws Exception {
		return nextFlowDao.showAllQualityTestDialogList(start, length, 
				query, col, sort, mark);
	}
}
