package com.biolims.system.nextFlow.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 下一步流向
 * @author lims-platform
 * @date 2016-04-05 11:31:43
 * @version V1.0
 * 
 */
@Entity
@Table(name = "NEXT_FLOW")
@SuppressWarnings("serial")
public class NextFlow extends EntityDao<NextFlow> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 下一步流向 */
	private String name;
	/** 英文名称 */
	private String enName;
	/** 关联表单 */
	private ApplicationTypeTable applicationTypeTable;
	/** 实验模块 */
	private ApplicationTypeTable mainTable;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 状态 */
	private String state;
	/** 状态描述 */
	private String stateName;
	/**关联明细及结果*/
	private String itemResultTable;
	/** 备注 */
	private String note;
	// 排序号
	private Integer sort;
	
	private String sysCode;

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得ApplicationTypeTable
	 * 
	 * @return: ApplicationTypeTable 关联表单
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPLICATION_TYPE_TABLE")
	public ApplicationTypeTable getApplicationTypeTable() {
		return this.applicationTypeTable;
	}

	/**
	 * 方法: 设置ApplicationTypeTable
	 * 
	 * @param: ApplicationTypeTable 关联表单
	 */
	public void setApplicationTypeTable(
			ApplicationTypeTable applicationTypeTable) {
		this.applicationTypeTable = applicationTypeTable;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态描述
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态描述
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 500)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}

	/**
	 * @return the mainTable
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MAIN_TABLE")
	public ApplicationTypeTable getMainTable() {
		return mainTable;
	}

	/**
	 * @param mainTable the mainTable to set
	 */
	public void setMainTable(ApplicationTypeTable mainTable) {
		this.mainTable = mainTable;
	}

	/**
	 * @return the itemResultTable
	 */
	public String getItemResultTable() {
		return itemResultTable;
	}

	/**
	 * @param itemResultTable the itemResultTable to set
	 */
	public void setItemResultTable(String itemResultTable) {
		this.itemResultTable = itemResultTable;
	}

	/**
	 * @return the enName
	 */
	public String getEnName() {
		return enName;
	}

	/**
	 * @param enName the enName to set
	 */
	public void setEnName(String enName) {
		this.enName = enName;
	}
	
}