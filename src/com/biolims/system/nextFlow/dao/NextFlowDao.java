package com.biolims.system.nextFlow.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.system.family.model.Family;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.work.model.WorkOrder;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class NextFlowDao extends BaseHibernateDao {
	public Map<String, Object> selectNextFlowList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from NextFlow where 1=1 and state='1' ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<NextFlow> list = new ArrayList<NextFlow>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key += key + " order by sort asc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectNextFlowAll() {
		String key = " ";
		String hql = " from NextFlow where 1=1 and state='1' order by sort asc";

		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<NextFlow> list = new ArrayList<NextFlow>();
		if (total > 0) {

			list = this.getSession().createQuery(hql + key).list();

		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据编号查询
	public List<NextFlow> seletNextFlowById(String id) {
		String hql = "from NextFlow where 1=1 and id='" + id + "'";
		List<NextFlow> list = new ArrayList<NextFlow>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 回滚
	public Object RollBack(String code, String model) {
		String hql = "from " + model + " t where t.code='" + code + "'";

		Object o = (Object) this.getSession().createQuery(hql).uniqueResult();
		return o;
	}

	public Map<String, Object> selectNextFlowJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from NextFlow where 1=1 and mainTable.id is not null and mainTable.id!='' and state = '1'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from NextFlow where 1=1 and mainTable.id is not null and mainTable.id!='' and state = '1'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<NextFlow> list = new ArrayList<NextFlow>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}

	public Map<String, Object> showAllQualityTestDialogList(Integer start,
			Integer length, String query, String col, String sort, String mark) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from NextFlow where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		key += " and applicationTypeTable.id like '%"+mark+"%' and applicationTypeTable.state ='1'";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from NextFlow where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<NextFlow> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}