package com.biolims.system.goodsMaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.product.model.Product;
/**   
 * @Title: Model
 * @Description: 物料主数据
 * @author lims-platform
 * @date 2016-01-20 09:35:43
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_GOODS_MATERIAL_MAIN")
@SuppressWarnings("serial")
public class GoodsMaterialMain extends EntityDao<GoodsMaterialMain> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**业务类型*/
	private Product product;
	/**数量*/
	private String num;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  业务类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  数量
	 */
	@Column(name ="NUM", length = 50)
	public String getNum(){
		return this.num;
	}
	/**
	 *方法: 设置String
	 *@param: String  数量
	 */
	public void setNum(String num){
		this.num = num;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}