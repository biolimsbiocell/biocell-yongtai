package com.biolims.system.goodsMaterial.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.goodsMaterial.dao.GoodsMaterialMainDao;
import com.biolims.system.goodsMaterial.model.GoodsMaterialMain;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialMainService {
	@Resource
	private GoodsMaterialMainDao goodsMaterialMainDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialMainList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialMainDao.selectGoodsMaterialMainList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialMain i) throws Exception {

		goodsMaterialMainDao.saveOrUpdate(i);

	}
	public GoodsMaterialMain get(String id) {
		GoodsMaterialMain goodsMaterialMain = commonDAO.get(GoodsMaterialMain.class, id);
		return goodsMaterialMain;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialMain sc, Map jsonMap) throws Exception {
		if (sc != null) {
			goodsMaterialMainDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
