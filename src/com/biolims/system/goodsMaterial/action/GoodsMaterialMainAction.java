﻿
package com.biolims.system.goodsMaterial.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.goodsMaterial.model.GoodsMaterialMain;
import com.biolims.system.goodsMaterial.service.GoodsMaterialMainService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/system/goodsMaterial/goodsMaterialMain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9013";
	@Autowired
	private GoodsMaterialMainService goodsMaterialMainService;
	private GoodsMaterialMain goodsMaterialMain = new GoodsMaterialMain();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showGoodsMaterialMainList")
	public String showGoodsMaterialMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/goodsMaterial/goodsMaterialMain.jsp");
	}

	@Action(value = "showGoodsMaterialMainListJson")
	public void showGoodsMaterialMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialMainService.findGoodsMaterialMainList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialMain> list = (List<GoodsMaterialMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("num", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "goodsMaterialMainSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/goodsMaterial/goodsMaterialMainDialog.jsp");
	}

	@Action(value = "showDialogGoodsMaterialMainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		else
			map2Query.put("state","1");
		Map<String, Object> result = goodsMaterialMainService.findGoodsMaterialMainList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialMain> list = (List<GoodsMaterialMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("num", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editGoodsMaterialMain")
	public String editGoodsMaterialMain() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			goodsMaterialMain = goodsMaterialMainService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "goodsMaterialMain");
		} else {
			goodsMaterialMain.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			goodsMaterialMain.setCreateUser(user);
//			goodsMaterialMain.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/goodsMaterial/goodsMaterialMainEdit.jsp");
	}

	@Action(value = "copyGoodsMaterialMain")
	public String copyGoodsMaterialMain() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		goodsMaterialMain = goodsMaterialMainService.get(id);
		goodsMaterialMain.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/goodsMaterial/goodsMaterialMainEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = goodsMaterialMain.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "GoodsMaterialMain";
			String markCode="WLB";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			goodsMaterialMain.setId(autoID);
		}

		Map aMap = new HashMap();
		goodsMaterialMainService.save(goodsMaterialMain,aMap);
		return redirect("/system/goodsMaterial/goodsMaterialMain/editGoodsMaterialMain.action?id=" + goodsMaterialMain.getId());

	}

	@Action(value = "viewGoodsMaterialMain")
	public String toViewGoodsMaterialMain() throws Exception {
		String id = getParameterFromRequest("id");
		goodsMaterialMain = goodsMaterialMainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/goodsMaterial/goodsMaterialMainEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialMainService getGoodsMaterialMainService() {
		return goodsMaterialMainService;
	}

	public void setGoodsMaterialMainService(GoodsMaterialMainService goodsMaterialMainService) {
		this.goodsMaterialMainService = goodsMaterialMainService;
	}

	public GoodsMaterialMain getGoodsMaterialMain() {
		return goodsMaterialMain;
	}

	public void setGoodsMaterialMain(GoodsMaterialMain goodsMaterialMain) {
		this.goodsMaterialMain = goodsMaterialMain;
	}


}
