package com.biolims.system.syscode.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.syscode.dao.CodeMainDao;
import com.biolims.system.syscode.model.CodeMain;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CodeMainService {
	@Resource
	private CodeMainDao codeMainDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findCodeMainList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return codeMainDao.selectCodeMainList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CodeMain i) throws Exception {

		codeMainDao.saveOrUpdate(i);

	}
	public CodeMain get(String id) {
		CodeMain codeMain = commonDAO.get(CodeMain.class, id);
		return codeMain;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CodeMain sc, Map jsonMap) throws Exception {
		if (sc != null) {
			codeMainDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	public Map<String, Object> showDialogCodeMainTableJson(Integer start,
			Integer length, String query, String col, String sort) {
		return codeMainDao.showDialogCodeMainTableJson(start,
				length, query, col, sort);
	}
}
