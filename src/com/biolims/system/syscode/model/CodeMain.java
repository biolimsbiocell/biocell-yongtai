package com.biolims.system.syscode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 条码模版
 * @author lims-platform
 * @date 2016-01-27 13:43:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_CODE_MAIN")
@SuppressWarnings("serial")
public class CodeMain extends EntityDao<CodeMain> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**打印指令*/
	private String code;
	/**名字x轴*/
	private String namex;
	/**名字y轴*/
	private String namey;
	/**编码x轴*/
	private String codex;
	/**编码y轴*/
	private String codey;
	/**二维码y轴*/
	private String qrx;
	/**二维码y轴*/
	private String qry;
	/**ip地址*/
	private String ip;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  名字x轴
	 */
	@Column(name ="NAMEX", length = 100)
	public String getNamex(){
		return this.namex;
	}
	/**
	 *方法: 设置String
	 *@param: String  名字x轴
	 */
	public void setNamex(String namex){
		this.namex = namex;
	}
	/**
	 *方法: 取得String
	 *@return: String  名字y轴
	 */
	@Column(name ="NAMEY", length = 50)
	public String getNamey(){
		return this.namey;
	}
	/**
	 *方法: 设置String
	 *@param: String  名字y轴
	 */
	public void setNamey(String namey){
		this.namey = namey;
	}
	/**
	 *方法: 取得String
	 *@return: String  编码x轴
	 */
	@Column(name ="CODEX", length = 50)
	public String getCodex(){
		return this.codex;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码x轴
	 */
	public void setCodex(String codex){
		this.codex = codex;
	}
	/**
	 *方法: 取得String
	 *@return: String  编码y轴
	 */
	@Column(name ="CODEY", length = 50)
	public String getCodey(){
		return this.codey;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码y轴
	 */
	public void setCodey(String codey){
		this.codey = codey;
	}
	/**
	 *方法: 取得String
	 *@return: String  二维码y轴
	 */
	@Column(name ="QRX", length = 50)
	public String getQrx(){
		return this.qrx;
	}
	/**
	 *方法: 设置String
	 *@param: String  二维码y轴
	 */
	public void setQrx(String qrx){
		this.qrx = qrx;
	}
	/**
	 *方法: 取得String
	 *@return: String  二维码y轴
	 */
	@Column(name ="QRY", length = 50)
	public String getQry(){
		return this.qry;
	}
	/**
	 *方法: 设置String
	 *@param: String  二维码y轴
	 */
	public void setQry(String qry){
		this.qry = qry;
	}
	/**
	 *方法: 取得String
	 *@return: String  ip地址
	 */
	@Column(name ="IP", length = 100)
	public String getIp(){
		return this.ip;
	}
	/**
	 *方法: 设置String
	 *@param: String  ip地址
	 */
	public void setIp(String ip){
		this.ip = ip;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 100)
	public String getState(){
		return this.state;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}