﻿package com.biolims.system.syscode.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.storage.model.Storage;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/syscode/codeMain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CodeMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9014";
	@Autowired
	private CodeMainService codeMainService;
	private CodeMain codeMain = new CodeMain();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showCodeMainList")
	public String showCodeMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/syscode/codeMain.jsp");
	}

	@Action(value = "showCodeMainListJson")
	public void showCodeMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = codeMainService.findCodeMainList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CodeMain> list = (List<CodeMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("namex", "");
		map.put("namey", "");
		map.put("codex", "");
		map.put("codey", "");
		map.put("qrx", "");
		map.put("qry", "");
		map.put("ip", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "codeMainSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCodeMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/syscode/codeMainDialog.jsp");
	}

	@Action(value = "showDialogCodeMainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCodeMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = codeMainService.findCodeMainList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CodeMain> list = (List<CodeMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("namex", "");
		map.put("namey", "");
		map.put("codex", "");
		map.put("codey", "");
		map.put("qrx", "");
		map.put("qry", "");
		map.put("ip", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editCodeMain")
	public String editCodeMain() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			codeMain = codeMainService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "codeMain");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// codeMain.setCreateUser(user);
			// codeMain.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/syscode/codeMainEdit.jsp");
	}

	@Action(value = "copyCodeMain")
	public String copyCodeMain() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		codeMain = codeMainService.get(id);
		codeMain.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/syscode/codeMainEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = codeMain.getId();
		if (id != null && id.equals("")) {
			codeMain.setId(null);
		}
		Map aMap = new HashMap();
		codeMainService.save(codeMain, aMap);
		return redirect("/system/syscode/codeMain/editCodeMain.action?id="
				+ codeMain.getId());

	}

	@Action(value = "viewCodeMain")
	public String toViewCodeMain() throws Exception {
		String id = getParameterFromRequest("id");
		codeMain = codeMainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/syscode/codeMainEdit.jsp");
	}

	/**
	 * 
	 * @Title: codeMainTabSelect
	 * @Description: 选择条码模板
	 * @author : shengwei.wang
	 * @date 2018年2月8日下午6:38:01
	 * @return String
	 * @throws
	 */
	@Action(value = "codeMainTabSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String codeMainTabSelect() throws Exception{
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/syscode/showCodeMainTable.jsp");
	}

	@Action(value = "showDialogCodeMainTableJson")
	public void showDialogCodeMainTableJson() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = codeMainService
					.showDialogCodeMainTableJson(start, length, query, col,
							sort);
			List<CodeMain> list = (List<CodeMain>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("ip", "");
			map.put("code", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CodeMainService getCodeMainService() {
		return codeMainService;
	}

	public void setCodeMainService(CodeMainService codeMainService) {
		this.codeMainService = codeMainService;
	}

	public CodeMain getCodeMain() {
		return codeMain;
	}

	public void setCodeMain(CodeMain codeMain) {
		this.codeMain = codeMain;
	}

}
