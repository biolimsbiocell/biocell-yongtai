package com.biolims.system.primers.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.primers.dao.PrimersLibraryDao;
import com.biolims.system.primers.model.PrimersLibrary;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PrimersLibraryService {
	@Resource
	private PrimersLibraryDao primersLibraryDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findPrimersLibraryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return primersLibraryDao.selectPrimersLibraryList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PrimersLibrary i) throws Exception {

		primersLibraryDao.saveOrUpdate(i);

	}
	public PrimersLibrary get(String id) {
		PrimersLibrary primersLibrary = commonDAO.get(PrimersLibrary.class, id);
		return primersLibrary;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PrimersLibrary sc, Map jsonMap) throws Exception {
		if (sc != null) {
			primersLibraryDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
