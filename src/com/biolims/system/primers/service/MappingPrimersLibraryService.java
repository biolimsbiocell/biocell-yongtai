package com.biolims.system.primers.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.primers.dao.MappingPrimersLibraryDao;
import com.biolims.system.primers.model.MappingPrimersLibrary;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MappingPrimersLibraryService {
	@Resource
	private MappingPrimersLibraryDao mappingPrimersLibraryDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findPrimersLibraryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return mappingPrimersLibraryDao.selectPrimersLibraryList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MappingPrimersLibrary i) throws Exception {

		mappingPrimersLibraryDao.saveOrUpdate(i);

	}
	public MappingPrimersLibrary get(String id) {
		MappingPrimersLibrary primersLibrary = commonDAO.get(MappingPrimersLibrary.class, id);
		return primersLibrary;
	}
	public Map<String, Object> findPrimersLibraryItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mappingPrimersLibraryDao.selectPrimersLibraryItemList(scId, startNum, limitNum, dir, sort);
		List<MappingPrimersLibraryItem> list = (List<MappingPrimersLibraryItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePrimersLibraryItem(MappingPrimersLibrary sc, String itemDataJson) throws Exception {
		List<MappingPrimersLibraryItem> saveItems = new ArrayList<MappingPrimersLibraryItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MappingPrimersLibraryItem scp = new MappingPrimersLibraryItem();
			// 将map信息读入实体类
			scp = (MappingPrimersLibraryItem) mappingPrimersLibraryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMappingPrimersLibrary(sc);

			saveItems.add(scp);
		}
		mappingPrimersLibraryDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPrimersLibraryItem(String[] ids) throws Exception {
		for (String id : ids) {
			MappingPrimersLibraryItem scp =  mappingPrimersLibraryDao.get(MappingPrimersLibraryItem.class, id);
			 mappingPrimersLibraryDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MappingPrimersLibrary sc, Map jsonMap) throws Exception {
		if (sc != null) {
			mappingPrimersLibraryDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("mappingPrimersLibraryItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePrimersLibraryItem(sc, jsonStr);
			}
		}
	}
	
	public List<MappingPrimersLibraryItem> getMappingPrimersLibraryItem(String productId){
		return mappingPrimersLibraryDao.getMappingPrimersLibraryItem(productId);
	}
	public Map<String, Object> findMappingPrimersLibraryTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return mappingPrimersLibraryDao.findMappingPrimersLibraryTable(start, length, query, col, sort);
	}
	public Map<String, Object> findMappingPrimersLibraryItemTable(Integer start,
			Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = mappingPrimersLibraryDao.findMappingPrimersLibraryItemTable(start, length, query,
				col, sort, id);
		List<MappingPrimersLibraryItem> list = (List<MappingPrimersLibraryItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMappingPrimersLibraryItem(MappingPrimersLibrary sc, String itemDataJson,String logInfo) throws Exception {
		List<MappingPrimersLibraryItem> saveItems = new ArrayList<MappingPrimersLibraryItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MappingPrimersLibraryItem scp = new MappingPrimersLibraryItem();
			// 将map信息读入实体类
			scp = (MappingPrimersLibraryItem) mappingPrimersLibraryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMappingPrimersLibrary(sc);

			saveItems.add(scp);
		}
		mappingPrimersLibraryDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMappingPrimersLibraryItem(String[] ids) throws Exception {
		for (String id : ids) {
			MappingPrimersLibraryItem scp =  mappingPrimersLibraryDao.get(MappingPrimersLibraryItem.class, id);
			mappingPrimersLibraryDao.delete(scp);
		 	if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setModifyContent("MappingPrimersLibraryItem删除信息"+ids.toString());
				commonDAO.saveOrUpdate(li);
			}
		}
	
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MappingPrimersLibrary sc, Map jsonMap,String logInfo, Map logMap) throws Exception {
		if (sc != null) {
			sc.setScopeId((String)ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String)ActionContext.getContext().getSession().get("scpeName"));
			mappingPrimersLibraryDao.saveOrUpdate(sc);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			String logStr = "";
           	logStr =  (String)logMap.get("mappingPrimersLibraryItem");
			jsonStr = (String)jsonMap.get("mappingPrimersLibraryItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMappingPrimersLibraryItem(sc, jsonStr,logStr);
			}
	}
   }
}
