package com.biolims.system.primers.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.system.primers.model.PrimersLibrary;
import com.biolims.system.primers.service.PrimersLibraryService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/system/primers/primersLibrary")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PrimersLibraryAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9019";
	@Autowired
	private PrimersLibraryService primersLibraryService;
	private PrimersLibrary primersLibrary = new PrimersLibrary();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showPrimersLibraryList")
	public String showPrimersLibraryList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/primersLibrary.jsp");
	}
	
	@Action(value = "showPrimersLibraryList1")
	public String showPrimersLibraryList1() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/primersLibrary1.jsp");
	}

	@Action(value = "showPrimersLibraryListJson")
	public void showPrimersLibraryListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = primersLibraryService.findPrimersLibraryList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PrimersLibrary> list = (List<PrimersLibrary>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("batch", "");
		map.put("edNmole", "");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("referTotal", "");
		map.put("site", "");
		map.put("hasDosage", "");
		map.put("surplus", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "primersLibrarySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPrimersLibraryList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/primersLibraryDialog.jsp");
	}

	@Action(value = "showDialogPrimersLibraryListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPrimersLibraryListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = primersLibraryService.findPrimersLibraryList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PrimersLibrary> list = (List<PrimersLibrary>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("batch", "");
		map.put("edNmole", "");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("referTotal", "");
		map.put("hasDosage", "");
		map.put("surplus", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editPrimersLibrary")
	public String editPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			primersLibrary = primersLibraryService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "primersLibrary");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			//primersLibrary.setCreateUser(user);
			//primersLibrary.setCreateDate(new Date());
			primersLibrary.setState("3");
			primersLibrary.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/primers/primersLibraryEdit.jsp");
	}

	@Action(value = "copyPrimersLibrary")
	public String copyPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		primersLibrary = primersLibraryService.get(id);
		primersLibrary.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/primers/primersLibraryEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = primersLibrary.getId();
		if(id!=null&&id.equals("")){
			primersLibrary.setId(null);
		}
		Map aMap = new HashMap();
		primersLibraryService.save(primersLibrary,aMap);
		return redirect("/system/primers/primersLibrary/editPrimersLibrary.action?id=" + primersLibrary.getId());

	}

	@Action(value = "viewPrimersLibrary")
	public String toViewPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		primersLibrary = primersLibraryService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/primers/primersLibraryEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PrimersLibraryService getPrimersLibraryService() {
		return primersLibraryService;
	}

	public void setPrimersLibraryService(PrimersLibraryService primersLibraryService) {
		this.primersLibraryService = primersLibraryService;
	}

	public PrimersLibrary getPrimersLibrary() {
		return primersLibrary;
	}

	public void setPrimersLibrary(PrimersLibrary primersLibrary) {
		this.primersLibrary = primersLibrary;
	}


}
