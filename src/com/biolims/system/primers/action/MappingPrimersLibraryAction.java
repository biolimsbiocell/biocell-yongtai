package com.biolims.system.primers.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.code.SystemCode;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.primers.model.MappingPrimersLibrary;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/system/primers/mappingPrimersLibrary")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MappingPrimersLibraryAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	private MappingPrimersLibrary mappingPrimersLibrary = new MappingPrimersLibrary();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
/*new*/
	
	@Resource
	private CommonService commonService;	
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Action(value = "showMappingPrimersLibraryList")
	public String showMappingPrimersLibraryList() throws Exception {
		rightsId = "9028";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibrary.jsp");
	}

	@Action(value = "showMappingPrimersLibraryEditList")
	public String showMappingPrimersLibraryEditList() throws Exception {
		rightsId = "9028";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEditList.jsp");
	}
	@Action(value = "showMappingPrimersLibraryTableJson")
	public void showMappingPrimersLibraryTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = mappingPrimersLibraryService.findMappingPrimersLibraryTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<MappingPrimersLibrary> list = (List<MappingPrimersLibrary>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("prodectId", "");
			map.put("prodectName", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("MappingPrimersLibrary");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "mappingPrimersLibrarySelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMappingPrimersLibraryList() throws Exception {
		rightsId = "9028";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibrarySelectTable.jsp");
	}
	
	@Action(value = "editMappingPrimersLibrary")
	public String editMappingPrimersLibrary() throws Exception {
		rightsId = "9027";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			mappingPrimersLibrary = mappingPrimersLibraryService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "mappingPrimersLibrary");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.mappingPrimersLibrary.setId(SystemCode.DEFAULT_SYSTEMCODE);
			mappingPrimersLibrary.setCreateUser(user);
			mappingPrimersLibrary.setCreateDate(new Date());
			mappingPrimersLibrary.setState(SystemConstants.DIC_STATE_NEW);
			mappingPrimersLibrary.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			mappingPrimersLibrary.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			mappingPrimersLibrary.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEdit.jsp");
	}	

	@Action(value = "viewMappingPrimersLibrary")
	public String toViewMappingPrimersLibrary() throws Exception {
		rightsId = "9028";
		String id = getParameterFromRequest("id");
		mappingPrimersLibrary = mappingPrimersLibraryService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEdit.jsp");
	}
	





	@Action(value = "showMappingPrimersLibraryItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMappingPrimersLibraryItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = mappingPrimersLibraryService
				.findMappingPrimersLibraryItemTable(start, length, query, col, sort,
						id);
		List<MappingPrimersLibraryItem> list=(List<MappingPrimersLibraryItem>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
			map.put("mappingPrimersLibrary-name", "");
			map.put("mappingPrimersLibrary-id", "");
			map.put("plName", "");
			map.put("size", "");
			map.put("conclusion1", "");
			map.put("reference", "");
			map.put("state", "");
			map.put("chromosomalLocation", "");
			map.put("leftPrimer", "");
			map.put("rightPrimer", "");
			map.put("ampliconid", "");
			map.put("stateName", "");
			map.put("plId", "");
			map.put("id", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMappingPrimersLibraryItem")
	public void delMappingPrimersLibraryItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mappingPrimersLibraryService.delMappingPrimersLibraryItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "saveMappingPrimersLibraryItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveMappingPrimersLibraryItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		mappingPrimersLibrary=commonService.get(MappingPrimersLibrary.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("mappingPrimersLibraryItem",
				getParameterFromRequest("dataJson"));
		lMap.put("mappingPrimersLibraryItem",
				getParameterFromRequest("changeLog"));
		try {
			mappingPrimersLibraryService.save(mappingPrimersLibrary, aMap,"",lMap);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}



	@Action(value = "saveMappingPrimersLibraryTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveMappingPrimersLibraryTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		
		String str = "["+dataValue+"]";
			
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			
		for (Map<String, Object> map1 : list) {
		
			MappingPrimersLibrary a = new MappingPrimersLibrary();
		
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (MappingPrimersLibrary)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			mappingPrimersLibraryService.save(a,aMap,changeLog,lMap);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "save")
	public void save() throws Exception {
		
		
		String code = "";
		String msg = "";
		String zId = "";
		boolean bool = true;	
		
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				mappingPrimersLibrary = (MappingPrimersLibrary)commonDAO.Map2Bean(map, mappingPrimersLibrary);
			}
			String id = mappingPrimersLibrary.getId();
			if(id!=null&&id.equals("")){
				mappingPrimersLibrary.setId(null);
			}
			if (id == null || id.length() <= 0 || "NEW".equals(id)) {
				code = systemCodeService.getCodeByPrefix("MappingPrimersLibrary", "MPL", 0000, 4,
						null);
				mappingPrimersLibrary.setId(code);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("mappingPrimersLibraryItem",getParameterFromRequest("mappingPrimersLibraryItemJson"));
			
			
			mappingPrimersLibraryService.save(mappingPrimersLibrary,aMap,changeLog,lMap);
			
			zId = mappingPrimersLibrary.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "copyMappingPrimersLibrary")
	public String copyMappingPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		mappingPrimersLibrary = mappingPrimersLibraryService.get(id);
		mappingPrimersLibrary.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEdit.jsp");
	}
/*old*/
	/*@Action(value = "showMappingPrimersLibraryList")
	public String showPrimersLibraryList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibrary.jsp");
	}*/

	@Action(value = "showMappingPrimersLibraryListJson")
	public void showPrimersLibraryListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mappingPrimersLibraryService
				.findPrimersLibraryList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<MappingPrimersLibrary> list = (List<MappingPrimersLibrary>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("prodectId", "");
		map.put("prodectName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "mappingPrimersLibrarySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPrimersLibraryList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryDialog.jsp");
	}

	@Action(value = "showDialogMappingPrimersLibraryListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPrimersLibraryListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mappingPrimersLibraryService
				.findPrimersLibraryList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<MappingPrimersLibrary> list = (List<MappingPrimersLibrary>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("prodectId", "");
		map.put("prodectName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

/*	@Action(value = "editMappingPrimersLibrary")*/
	public String editPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			mappingPrimersLibrary = mappingPrimersLibraryService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService
					.findFileInfoCount(id, "mappingPrimersLibrary");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			mappingPrimersLibrary.setCreateUser(user);
			mappingPrimersLibrary.setCreateDate(new Date());
			mappingPrimersLibrary.setState("3");
			mappingPrimersLibrary.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEdit.jsp");
	}

/*	@Action(value = "copyMappingPrimersLibrary")*/
	public String copyPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		mappingPrimersLibrary = mappingPrimersLibraryService.get(id);
		mappingPrimersLibrary.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEdit.jsp");
	}

	/*@Action(value = "save")
	public String save() throws Exception {
		String id = mappingPrimersLibrary.getId();
		if (id != null && id.equals("")) {
			mappingPrimersLibrary.setId(null);
		}
		Map aMap = new HashMap();
		aMap.put("mappingPrimersLibraryItem",
				getParameterFromRequest("mappingPrimersLibraryItemJson"));

		mappingPrimersLibraryService.save(mappingPrimersLibrary, aMap);
		return redirect("/system/primers/mappingPrimersLibrary/editMappingPrimersLibrary.action?id="
				+ mappingPrimersLibrary.getId());

	}*/

	/*@Action(value = "viewMappingPrimersLibrary")*/
	public String toViewPrimersLibrary() throws Exception {
		String id = getParameterFromRequest("id");
		mappingPrimersLibrary = mappingPrimersLibraryService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryEdit.jsp");
	}

	@Action(value = "showMappingPrimersLibraryItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPrimersLibraryItemList() throws Exception {
		return dispatcher("/WEB-INF/page/system/primers/mappingPrimersLibraryItem.jsp");
	}

	@Action(value = "showMappingPrimersLibraryItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPrimersLibraryItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mappingPrimersLibraryService
					.findPrimersLibraryItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<MappingPrimersLibraryItem> list = (List<MappingPrimersLibraryItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("plName", "");
			map.put("size", "");
			map.put("conclusion1", "");
			map.put("reference", "");
			map.put("state", "");
			map.put("site", "");
			map.put("chromosomalLocation", "");
			map.put("leftPrimer", "");
			map.put("rightPrimer", "");
			map.put("ampliconid", "");

			map.put("stateName", "");
			map.put("plId", "");
			map.put("mappingPrimersLibrary-name", "");
			map.put("mappingPrimersLibrary-id", "");
			map.put("site", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	/*@Action(value = "delMappingPrimersLibraryItem")*/
	public void delPrimersLibraryItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mappingPrimersLibraryService.delPrimersLibraryItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MappingPrimersLibraryService getMappingPrimersLibraryService() {
		return mappingPrimersLibraryService;
	}

	public void setMappingPrimersLibraryService(
			MappingPrimersLibraryService mappingPrimersLibraryService) {
		this.mappingPrimersLibraryService = mappingPrimersLibraryService;
	}

	public MappingPrimersLibrary getMappingPrimersLibrary() {
		return mappingPrimersLibrary;
	}

	public void setMappingPrimersLibrary(
			MappingPrimersLibrary mappingPrimersLibrary) {
		this.mappingPrimersLibrary = mappingPrimersLibrary;
	}
}
