package com.biolims.system.primers.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.system.primers.model.MappingPrimersLibrary;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class MappingPrimersLibraryDao extends BaseHibernateDao {
	public Map<String, Object> selectPrimersLibraryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from MappingPrimersLibrary where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MappingPrimersLibrary> list = new ArrayList<MappingPrimersLibrary>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectPrimersLibraryItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MappingPrimersLibraryItem t where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and t.mappingPrimersLibrary='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MappingPrimersLibraryItem> list = new ArrayList<MappingPrimersLibraryItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<MappingPrimersLibraryItem> getMappingPrimersLibraryItem(
				String mpId) {
			String hql = "";
			if (mpId.contains(",")) {
				String[] a = mpId.split(",");
				mpId = "(";
				for (String a1 : a) {
					if (mpId.equals("("))
						mpId = mpId + "'" + a1 + "'";
					else
						mpId = mpId + ",'" + a1 + "'";
				}
				mpId = mpId + ")";
				hql = "from MappingPrimersLibraryItem  where 1=1 and mappingPrimersLibrary.id in (select id from MappingPrimersLibrary where prodectId in "
						+ mpId + ")";

			} else
				hql = "from MappingPrimersLibraryItem  where 1=1 and mappingPrimersLibrary.id in (select id from MappingPrimersLibrary where instr(prodectId,'"
						+ mpId + "')>0)";
			List<MappingPrimersLibraryItem> list = this.getSession()
					.createQuery(hql).list();
			return list;
		}
		public Map<String, Object> findMappingPrimersLibraryTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from  MappingPrimersLibrary where 1=1";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from MappingPrimersLibrary where 1=1";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<MappingPrimersLibrary> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}

	public Map<String, Object> findMappingPrimersLibraryItemTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from MappingPrimersLibraryItem where 1=1 and mappingPrimersLibrary.id='"+id+"'";
			String key = "";

			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from MappingPrimersLibraryItem where 1=1 and mappingPrimersLibrary.id='"+id+"' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<MappingPrimersLibraryItem> list = new ArrayList<MappingPrimersLibraryItem>();
				list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
}