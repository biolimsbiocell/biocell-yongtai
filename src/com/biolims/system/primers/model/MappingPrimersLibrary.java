package com.biolims.system.primers.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 项目对应引物库
 * @author lims-platform
 * @date 2016-03-09 11:31:36
 * @version V1.0   
 *
 */
@Entity
@Table(name = "MAPPING_PRIMERS_LIBRARY")
@SuppressWarnings("serial")
public class MappingPrimersLibrary extends EntityDao<MappingPrimersLibrary> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/** 位点*/
	private String site;
	/**创建时间*/
	private Date createDate;
	/**工作流id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**检测项目编号*/
	private String prodectId;
	/**检测项目名称*/
	private String prodectName;
	/*	范围Id*/
	private String scopeId;
	/*范围*/
	private String scopeName;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目编号
	 */
	@Column(name ="PRODECT_ID", length = 50)
	public String getProdectId(){
		return this.prodectId;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目编号
	 */
	public void setProdectId(String prodectId){
		this.prodectId = prodectId;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目名称
	 */
	@Column(name ="PRODECT_NAME", length = 50)
	public String getProdectName(){
		return this.prodectName;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目名称
	 */
	public void setProdectName(String prodectName){
		this.prodectName = prodectName;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	public String getScopeName() {
		return scopeName;
	}
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
}