package com.biolims.system.primers.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 项目对应引物库明细
 * @author lims-platform
 * @date 2016-03-09 11:31:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "MAPPING_PRIMERS_LIBRARY_ITEM")
@SuppressWarnings("serial")
public class MappingPrimersLibraryItem extends EntityDao<MappingPrimersLibraryItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**引物编码*/
	private String plId;
	/** 位点*/
	private String site;
	/** 染色体位置*/
	private String chromosomalLocation;
	/**left primer*/
	private String leftPrimer;
	/**right primer*/
	private String rightPrimer;
	/**ampliconid*/
	private String ampliconid;
	/**引物库名称*/
	private String plName;
	/**大小*/
	private String size;
	/**结论*/
	private String conclusion1;
	/**参考依据*/
	private String reference;
	/**使用状态*/
	private String state;
	/**使用状态名称*/
	private String stateName;
	/**想关主表*/
	private MappingPrimersLibrary mappingPrimersLibrary;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  染色体位置
	 */
	@Column(name ="CHROMOSOMAL_LOCATION", length = 100)
	public String getChromosomalLocation(){
		return this.chromosomalLocation;
	}
	/**
	 *方法: 设置String
	 *@param: String  染色体位置
	 */
	public void setChromosomalLocation(String chromosomalLocation){
		this.chromosomalLocation = chromosomalLocation;
	}
	/**
	 *方法: 取得String
	 *@return: String  left primer
	 */
	@Column(name ="LEFT_PRIMER", length = 100)
	public String getLeftPrimer(){
		return this.leftPrimer;
	}
	/**
	 *方法: 设置String
	 *@param: String  left primer
	 */
	public void setLeftPrimer(String leftPrimer){
		this.leftPrimer = leftPrimer;
	}
	/**
	 *方法: 取得String
	 *@return: String  right primer
	 */
	@Column(name ="RIGHT_PRIMER", length = 100)
	public String getRightPrimer(){
		return this.rightPrimer;
	}
	/**
	 *方法: 设置String
	 *@param: String  right primer
	 */
	public void setRightPrimer(String rightPrimer){
		this.rightPrimer = rightPrimer;
	}
	/**
	 *方法: 取得String
	 *@return: String  ampliconid
	 */
	@Column(name ="AMPLICONID", length = 100)
	public String getAmpliconid(){
		return this.ampliconid;
	}
	/**
	 *方法: 设置String
	 *@param: String  ampliconid
	 */
	public void setAmpliconid(String ampliconid){
		this.ampliconid = ampliconid;
	}
	/**
	 *方法: 取得String
	 *@return: String  引物库名称
	 */
	@Column(name ="PL_NAME_", length = 60)
	public String getPlName(){
		return this.plName;
	}
	/**
	 *方法: 设置String
	 *@param: String  引物库名称
	 */
	public void setPlName(String plName){
		this.plName = plName;
	}
	/**
	 *方法: 取得String
	 *@return: String  大小
	 */
	@Column(name ="SIZE_", length = 60)
	public String getSize(){
		return this.size;
	}
	/**
	 *方法: 设置String
	 *@param: String  大小
	 */
	public void setSize(String size){
		this.size = size;
	}
	/**
	 *方法: 取得String
	 *@return: String  结论
	 */
	@Column(name ="CONCLUSION1_", length = 60)
	public String getConclusion1(){
		return this.conclusion1;
	}
	/**
	 *方法: 设置String
	 *@param: String  结论
	 */
	public void setConclusion1(String conclusion1){
		this.conclusion1 = conclusion1;
	}
	/**
	 *方法: 取得String
	 *@return: String  参考依据
	 */
	@Column(name ="REFERENCE_", length = 60)
	public String getReference(){
		return this.reference;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  参考依据
	 */
	public void setReference(String reference){
		this.reference = reference;
	}
	
	/**
	 *方法: 取得primersLibrary
	 *@return: primersLibrary  想关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MAPPING_PRIMERS_LIBRARY_")
	public MappingPrimersLibrary getMappingPrimersLibrary() {
		return mappingPrimersLibrary;
	}
	public void setMappingPrimersLibrary(MappingPrimersLibrary mappingPrimersLibrary) {
		this.mappingPrimersLibrary = mappingPrimersLibrary;
	}
	/**
	 *方法: 取得String
	 *@return: String  使用状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  使用状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  使用状态名称
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  使用状态名称
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  引物编码
	 */
	@Column(name ="PL_ID", length = 50)
	public String getPlId() {
		return plId;
	}
	/**
	 *方法: 设置String
	 *@param: String  引物编码
	 */
	public void setPlId(String plId) {
		this.plId = plId;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
}