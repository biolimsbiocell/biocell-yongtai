package com.biolims.system.primers.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 引物库
 * @author lims-platform
 * @date 2016-03-10 16:12:13
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PRIMERS_LIBRARY")
@SuppressWarnings("serial")
public class PrimersLibrary extends EntityDao<PrimersLibrary> implements
		java.io.Serializable {
	/** 引物编码 */
	private String id;
	/** 领用日期 */
	private Date receiveDate;
	/** 批次 */
	private String batch;
	/** 有效量nmole */
	private String edNmole;
	/** 有效期 */
	private Date expiryDate;
	/** 引物总量 */
	private String referTotal;
	/** 已用量 */
	private String hasDosage;
	/** 剩余量 */
	private String surplus;
	/** 使用状态 */
	private String state;
	/** 使用状态名称 */
	private String stateName;

	/** 位点 */
	private String site;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 引物编码
	 */

	@Id
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 引物编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 领用日期
	 */
	@Column(name = "RECEIVE_DATE", length = 50)
	public Date getReceiveDate() {
		return this.receiveDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 领用日期
	 */
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批次
	 */
	@Column(name = "BATCH", length = 50)
	public String getBatch() {
		return this.batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批次
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 有效量nmole
	 */
	@Column(name = "ED_NMOLE", length = 50)
	public String getEdNmole() {
		return this.edNmole;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 有效量nmole
	 */
	public void setEdNmole(String edNmole) {
		this.edNmole = edNmole;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 有效期
	 */
	@Column(name = "EXPIRY_DATE", length = 50)
	public Date getExpiryDate() {
		return this.expiryDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 有效期
	 */
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 引物总量
	 */
	@Column(name = "REFER_TOTAL", length = 50)
	public String getReferTotal() {
		return this.referTotal;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 引物总量
	 */
	public void setReferTotal(String referTotal) {
		this.referTotal = referTotal;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 已用量
	 */
	@Column(name = "HAS_DOSAGE", length = 50)
	public String getHasDosage() {
		return this.hasDosage;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 已用量
	 */
	public void setHasDosage(String hasDosage) {
		this.hasDosage = hasDosage;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 剩余量
	 */
	@Column(name = "SURPLUS", length = 50)
	public String getSurplus() {
		return this.surplus;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 剩余量
	 */
	public void setSurplus(String surplus) {
		this.surplus = surplus;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 使用状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 使用状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 使用状态名称
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 使用状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}