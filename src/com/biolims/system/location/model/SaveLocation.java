package com.biolims.system.location.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 储位主数据
 * @author lims-platform
 * @date 2015-11-03 18:13:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAVE_LOCATION")
@SuppressWarnings("serial")
public class SaveLocation extends EntityDao<SaveLocation> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**上级编号*/
	private String upId;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  上级编号
	 */
	@Column(name ="UP_ID", length = 120)
	public String getUpId(){
		return this.upId;
	}
	/**
	 *方法: 设置String
	 *@param: String  上级编号
	 */
	public void setUpId(String upId){
		this.upId = upId;
	}
}