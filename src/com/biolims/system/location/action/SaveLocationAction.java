﻿
package com.biolims.system.location.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.system.location.model.SaveLocation;
import com.biolims.system.location.service.SaveLocationService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/system/location/saveLocation")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SaveLocationAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9003";
	@Autowired
	private SaveLocationService saveLocationService;
	private SaveLocation saveLocation = new SaveLocation();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showSaveLocationList")
	public String showSaveLocationList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/location/saveLocation.jsp");
	}

	@Action(value = "showSaveLocationListJson")
	public void showSaveLocationListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = saveLocationService.findSaveLocationList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SaveLocation> list = (List<SaveLocation>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("upId-id", "");
		map.put("upId-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "saveLocationSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSaveLocationList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/location/saveLocationDialog.jsp");
	}

	@Action(value = "showDialogSaveLocationListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSaveLocationListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = saveLocationService.findSaveLocationList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SaveLocation> list = (List<SaveLocation>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("upId-id", "");
		map.put("upId-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editSaveLocation")
	public String editSaveLocation() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			saveLocation = saveLocationService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "saveLocation");
		} else {
			saveLocation=new SaveLocation();
			saveLocation.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			saveLocation.setCreateUser(user);
//			saveLocation.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/location/saveLocationEdit.jsp");
	}

	@Action(value = "copySaveLocation")
	public String copySaveLocation() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		saveLocation = saveLocationService.get(id);
		saveLocation.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/location/saveLocationEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = saveLocation.getId();
		if(id!=null&&id.equals("")){
			saveLocation.setId(null);
		}
		Map aMap = new HashMap();
		saveLocationService.save(saveLocation,aMap);
		return redirect("/system/location/saveLocation/editSaveLocation.action?id=" + saveLocation.getId());

	}

	@Action(value = "viewSaveLocation")
	public String toViewSaveLocation() throws Exception {
		String id = getParameterFromRequest("id");
		saveLocation = saveLocationService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/location/saveLocationEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SaveLocationService getSaveLocationService() {
		return saveLocationService;
	}

	public void setSaveLocationService(SaveLocationService saveLocationService) {
		this.saveLocationService = saveLocationService;
	}

	public SaveLocation getSaveLocation() {
		return saveLocation;
	}

	public void setSaveLocation(SaveLocation saveLocation) {
		this.saveLocation = saveLocation;
	}
	@Action(value = "bloodSplitPadPage", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String pad() throws Exception {
		return dispatcher("/WEB-INF/page/bloodSplitPad/bloodSplitPadItem/bloodSplitPadPage.jsp");
	}

}
