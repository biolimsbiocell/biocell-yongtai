package com.biolims.system.location.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.location.dao.SaveLocationDao;
import com.biolims.system.location.model.SaveLocation;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SaveLocationService {
	@Resource
	private SaveLocationDao saveLocationDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSaveLocationList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return saveLocationDao.selectSaveLocationList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SaveLocation i) throws Exception {

		saveLocationDao.saveOrUpdate(i);

	}
	public SaveLocation get(String id) {
		SaveLocation saveLocation = commonDAO.get(SaveLocation.class, id);
		return saveLocation;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SaveLocation sc, Map jsonMap) throws Exception {
		if (sc != null) {
			saveLocationDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
