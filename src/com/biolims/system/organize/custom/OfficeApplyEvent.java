package com.biolims.system.organize.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.goods.mate.service.GoodsMaterialsSendService;
import com.biolims.system.organize.service.OfficeApplyService;

public class OfficeApplyEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		OfficeApplyService mbService = (OfficeApplyService) ctx.getBean("officeApplyService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
