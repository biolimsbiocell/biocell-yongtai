package com.biolims.system.organize.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.organize.dao.OfficeApplyDao;
import com.biolims.system.organize.model.OfficeApply;
import com.biolims.system.organize.model.OfficeApplyItem;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class OfficeApplyService {
	@Resource
	private OfficeApplyDao officeApplyDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findOfficeApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return officeApplyDao.selectOfficeApplyList(mapForQuery, startNum, limitNum, dir, sort);
	}
	public Map<String, Object> findOfficeApplyList1(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return officeApplyDao.selectOfficeApplyList1(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OfficeApply i) throws Exception {

		officeApplyDao.saveOrUpdate(i);

	}
	public OfficeApply get(String id) {
		OfficeApply officeApply = commonDAO.get(OfficeApply.class, id);
		return officeApply;
	}
	public Map<String, Object> findOfficeApplyItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort,String flag) throws Exception {
		Map<String, Object> result = officeApplyDao.selectOfficeApplyItemList(scId, startNum, limitNum, dir, sort,flag);
		List<OfficeApplyItem> list = (List<OfficeApplyItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOfficeApplyItem(OfficeApply sc, String itemDataJson) throws Exception {
		List<OfficeApplyItem> saveItems = new ArrayList<OfficeApplyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OfficeApplyItem scp = new OfficeApplyItem();
			// 将map信息读入实体类
			scp = (OfficeApplyItem) officeApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOfficeApply(sc);

			saveItems.add(scp);
		}
		officeApplyDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOfficeApplyItem(String[] ids) throws Exception {
		for (String id : ids) {
			OfficeApplyItem scp =  officeApplyDao.get(OfficeApplyItem.class, id);
			 officeApplyDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OfficeApply sc, Map jsonMap) throws Exception {
		if (sc != null) {
			officeApplyDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("officeApplyItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOfficeApplyItem(sc, jsonStr);
			}
	}
   }
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		OfficeApply sct = officeApplyDao.get(OfficeApply.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStatename(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setAcceptUser(user);
		sct.setAcceptDate(new Date());
		officeApplyDao.update(sct);
	}
	
	
}
