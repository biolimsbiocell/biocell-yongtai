package com.biolims.system.organize.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.organize.dao.ApplyOrganizeDao;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ApplyOrganizeService {
	@Resource
	private ApplyOrganizeDao applyOrganizeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findApplyOrganizeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return applyOrganizeDao.selectApplyOrganizeList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ApplyOrganize i) throws Exception {

		applyOrganizeDao.saveOrUpdate(i);

	}
	public ApplyOrganize get(String id) {
		ApplyOrganize applyOrganize = commonDAO.get(ApplyOrganize.class, id);
		return applyOrganize;
	}
	public Map<String, Object> findApplyOrganizeAddressList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = applyOrganizeDao.selectApplyOrganizeAddressList(scId, startNum, limitNum, dir, sort);
		List<ApplyOrganizeAddress> list = (List<ApplyOrganizeAddress>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveApplyOrganizeAddress(ApplyOrganize sc, String itemDataJson) throws Exception {
		List<ApplyOrganizeAddress> saveItems = new ArrayList<ApplyOrganizeAddress>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ApplyOrganizeAddress scp = new ApplyOrganizeAddress();
			// 将map信息读入实体类
			scp = (ApplyOrganizeAddress) applyOrganizeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setApplyOrganize(sc);

			saveItems.add(scp);
		}
		applyOrganizeDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delApplyOrganizeAddress(String[] ids) throws Exception {
		for (String id : ids) {
			ApplyOrganizeAddress scp =  applyOrganizeDao.get(ApplyOrganizeAddress.class, id);
			 applyOrganizeDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ApplyOrganize sc, Map jsonMap) throws Exception {
		if (sc != null) {
			applyOrganizeDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("applyOrganizeAddress");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveApplyOrganizeAddress(sc, jsonStr);
			}
		}
   }
	//ajax
	public List<Map<String, String>> showApplyOrganizeList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = applyOrganizeDao.setApplyOrganizeList(code);
		List<ApplyOrganizeAddress> list = (List<ApplyOrganizeAddress>) result.get("list");

		if (list != null && list.size() > 0) {
			for (ApplyOrganizeAddress srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("pName", srai.getApplyOrganize().getPerson().getName());
				map.put("pId", srai.getApplyOrganize().getPerson().getId());
				map.put("phone", srai.getPhone());
				map.put("receiver", srai.getReceiver());
				map.put("dept", srai.getApplyOrganize().getDept());
				//map.put("agent", srai.getAgent());
				map.put("aName", srai.getApplyOrganize().getName());
				map.put("aId", srai.getApplyOrganize().getId());
				mapList.add(map);
			}
		}
		return mapList;
	}
	//ajax
	public List<Map<String, String>> showApplyOrganizeReceiveList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = applyOrganizeDao.setApplyOrganizeReceiveList(code);
		List<ApplyOrganizeAddress> list = (List<ApplyOrganizeAddress>) result.get("list");

		if (list != null && list.size() > 0) {
			for (ApplyOrganizeAddress srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("pName", srai.getApplyOrganize().getPerson().getName());
				map.put("pId", srai.getApplyOrganize().getPerson().getId());
				map.put("phone", srai.getPhone());
				map.put("receiver", srai.getReceiver());
				map.put("dept", srai.getApplyOrganize().getDept());
				//map.put("agent", srai.getAgent());
				map.put("aName", srai.getApplyOrganize().getName());
				map.put("aId", srai.getApplyOrganize().getId());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
