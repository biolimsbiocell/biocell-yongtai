﻿
package com.biolims.system.organize.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.system.organize.dao.ApplyOrganizeDao;
import com.biolims.system.organize.dao.OfficeApplyDao;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.system.organize.model.OfficeApply;
import com.biolims.system.organize.model.OfficeApplyItem;
import com.biolims.system.organize.service.OfficeApplyService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
@Namespace("/system/organize/officeApply")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class OfficeApplyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9011";
	@Autowired
	private OfficeApplyService officeApplyService;
	@Resource
	private ApplyOrganizeDao applyOrganizeDao;
	@Resource
	private OfficeApplyDao officeApplyDao;
	private OfficeApply officeApply = new OfficeApply();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showOfficeApplyList")
	public String showOfficeApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/organize/officeApply.jsp");
	}

	@Action(value = "showOfficeApplyListJson")
	public void showOfficeApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = officeApplyService.findOfficeApplyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OfficeApply> list = (List<OfficeApply>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		/*map.put("id", "");
		map.put("code", "");
		map.put("num", "");
		map.put("state", "");
		map.put("goodsMaterialsReady-name", "");
		map.put("goodsMaterialsReady-id", "");
		map.put("validity", "");*/
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("statename", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "officeApplySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogOfficeApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/organize/officeApplyDialog.jsp");
	}

	@Action(value = "showDialogOfficeApplyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogOfficeApplyListJson(String flag) throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = officeApplyService.findOfficeApplyList1(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OfficeApply> list = (List<OfficeApply>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("statename", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editOfficeApply")
	public String editOfficeApply() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			officeApply = officeApplyService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "officeApply");
		} else {
			officeApply.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			officeApply.setCreateUser(user);
			officeApply.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(officeApply.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/organize/officeApplyEdit.jsp");
	}

	@Action(value = "copyOfficeApply")
	public String copyOfficeApply() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		officeApply = officeApplyService.get(id);
		officeApply.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/organize/officeApplyEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = officeApply.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "OfficeApply";
			String markCode="BSCSQ";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			officeApply.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("officeApplyItem",getParameterFromRequest("officeApplyItemJson"));
		
		officeApplyService.save(officeApply,aMap);
		return redirect("/system/organize/officeApply/editOfficeApply.action?id=" + officeApply.getId());

	}

	@Action(value = "viewOfficeApply")
	public String toViewOfficeApply() throws Exception {
		String id = getParameterFromRequest("id");
		officeApply = officeApplyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/organize/officeApplyEdit.jsp");
	}
	

	@Action(value = "showOfficeApplyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOfficeApplyItemList() throws Exception {
		String flag = getParameterFromRequest("flag");
		putObjToContext("flag",flag);	
		return dispatcher("/WEB-INF/page/system/organize/officeApplyItem.jsp");
	}

	@Action(value = "showOfficeApplyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOfficeApplyItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String flag=  getParameterFromRequest("flag");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = officeApplyService.findOfficeApplyItemList(scId, startNum, limitNum, dir,
					sort,flag);
			Long total = (Long) result.get("total");
			List<OfficeApplyItem> list = (List<OfficeApplyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("packCode", "");
			map.put("name", "");
			map.put("state", "");
			map.put("officeApply-name", "");
			map.put("officeApply-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delOfficeApplyItem")
	public void delOfficeApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			officeApplyService.delOfficeApplyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//获取同一地区统一物料包下的可重用样本号
	@Action(value = "getReCode")
	public void getReCode() throws Exception {
		boolean flag=true;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			String code = getParameterFromRequest("code");//---物料包编号
			String userId = getParameterFromRequest("createUser");
			GoodsMaterialsApply gma = applyOrganizeDao.get(GoodsMaterialsApply.class, userId);
			ApplyOrganize ao=applyOrganizeDao.showApplyOrganizeList(gma.getCreateUser().getId());//---办事处主数据
			if(ao!=null){
				String mark=ao.getId();//---地区码
				List<String> list=officeApplyDao.getOfficeApplyItemByPcode(code,mark);
//			if(list.size()>0){
//				for(OfficeApplyItem oai:list){
//					String str=oai.getCode().substring(5,7);
//					if(str.equals(mark)){
//						flag=true;
//						num++;
//					}
//				}
//			}
				int size=list.size();//---可重用样本数量
				map.put("date", list);
				map.put("size", size);
			}else{
				map.put("date", null);
				map.put("size", null);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public OfficeApplyService getOfficeApplyService() {
		return officeApplyService;
	}

	public void setOfficeApplyService(OfficeApplyService officeApplyService) {
		this.officeApplyService = officeApplyService;
	}

	public OfficeApply getOfficeApply() {
		return officeApply;
	}

	public void setOfficeApply(OfficeApply officeApply) {
		this.officeApply = officeApply;
	}


}
