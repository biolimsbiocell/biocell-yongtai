﻿
package com.biolims.system.organize.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.system.organize.service.ApplyOrganizeService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/system/organize/applyOrganize")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ApplyOrganizeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9005";
	@Autowired
	private ApplyOrganizeService applyOrganizeService;
	private ApplyOrganize applyOrganize = new ApplyOrganize();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showApplyOrganizeList")
	public String showApplyOrganizeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/organize/applyOrganize.jsp");
	}

	@Action(value = "showApplyOrganizeListJson")
	public void showApplyOrganizeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = applyOrganizeService.findApplyOrganizeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ApplyOrganize> list = (List<ApplyOrganize>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("person-id", "");
		map.put("person-name", "");
		map.put("markCode-id", "");
		map.put("markCode-name", "");
		map.put("dept", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "applyOrganizeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogApplyOrganizeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/organize/applyOrganizeDialog.jsp");
	}

	@Action(value = "showDialogApplyOrganizeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogApplyOrganizeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = applyOrganizeService.findApplyOrganizeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ApplyOrganize> list = (List<ApplyOrganize>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("person-id", "");
		map.put("person-name", "");
		map.put("markCode-id", "");
		map.put("markCode-name", "");
		map.put("dept", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editApplyOrganize")
	public String editApplyOrganize() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			applyOrganize = applyOrganizeService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "applyOrganize");
		} else {
//			applyOrganize.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			applyOrganize.setCreateUser(user);
//			applyOrganize.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/organize/applyOrganizeEdit.jsp");
	}

	@Action(value = "copyApplyOrganize")
	public String copyApplyOrganize() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		applyOrganize = applyOrganizeService.get(id);
		applyOrganize.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/organize/applyOrganizeEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = applyOrganize.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "ApplyOrganize";
			String markCode="SQGL";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			applyOrganize.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("applyOrganizeAddress",getParameterFromRequest("applyOrganizeAddressJson"));
		
		applyOrganizeService.save(applyOrganize,aMap);
		return redirect("/system/organize/applyOrganize/editApplyOrganize.action?id=" + applyOrganize.getId());

	}

	@Action(value = "viewApplyOrganize")
	public String toViewApplyOrganize() throws Exception {
		String id = getParameterFromRequest("id");
		applyOrganize = applyOrganizeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/organize/applyOrganizeEdit.jsp");
	}
	

	@Action(value = "showApplyOrganizeAddressList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showApplyOrganizeAddressList() throws Exception {

		return dispatcher("/WEB-INF/page/system/organize/applyOrganizeAddress.jsp");
	}

	@Action(value = "showApplyOrganizeAddressListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showApplyOrganizeAddressListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = applyOrganizeService.findApplyOrganizeAddressList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<ApplyOrganizeAddress> list = (List<ApplyOrganizeAddress>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("receiver", "");
			map.put("phone", "");
			map.put("transTime", "");
			map.put("applyOrganize-name", "");
			map.put("applyOrganize-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delApplyOrganizeAddress")
	public void delApplyOrganizeAddress() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			applyOrganizeService.delApplyOrganizeAddress(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "applyOrganizeAddressSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogApplyOrganizeAddressList() throws Exception {
		String aId = getRequest().getParameter("aid");
		putObjToContext("aId", aId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/organize/applyOrganizeAddressDialog.jsp");
	}

	@Action(value = "showDialogApplyOrganizeAddressListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogApplyOrganizeAddressListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//      Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		String scId = getRequest().getParameter("aid");
		Map<String, Object> result = applyOrganizeService.findApplyOrganizeAddressList(scId, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ApplyOrganizeAddress> list = (List<ApplyOrganizeAddress>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("receiver", "");
		map.put("phone", "");
		map.put("transTime", "");
		map.put("applyOrganize-name", "");
		map.put("applyOrganize-id", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ApplyOrganizeService getApplyOrganizeService() {
		return applyOrganizeService;
	}

	public void setApplyOrganizeService(ApplyOrganizeService applyOrganizeService) {
		this.applyOrganizeService = applyOrganizeService;
	}

	public ApplyOrganize getApplyOrganize() {
		return applyOrganize;
	}

	public void setApplyOrganize(ApplyOrganize applyOrganize) {
		this.applyOrganize = applyOrganize;
	}


}
