package com.biolims.system.organize.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.system.organize.model.ApplyOrganize;

@Repository
@SuppressWarnings("unchecked")
public class ApplyOrganizeDao extends BaseHibernateDao {
	public Map<String, Object> selectApplyOrganizeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from ApplyOrganize where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<ApplyOrganize> list = new ArrayList<ApplyOrganize>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectApplyOrganizeAddressList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from ApplyOrganizeAddress where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and applyOrganize.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ApplyOrganizeAddress> list = new ArrayList<ApplyOrganizeAddress>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//ajax加载对应字段信息
	public Map<String, Object> setApplyOrganizeList(String code) throws Exception {
		String hql = "from ApplyOrganizeAddress t where 1=1 and t.applyOrganize.person='"+code+"'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<ApplyOrganizeAddress> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//
	public ApplyOrganize showApplyOrganizeList(String code){
		String hql = "from ApplyOrganize t where 1=1 and t.person='"+code+"'";
		List<ApplyOrganize> list = this.getSession().createQuery(hql).list();
		if(list.size()>0)
		return list.get(0);
		return null;
	}
	//ajax加载对应字段信息
	public Map<String, Object> setApplyOrganizeReceiveList(String code) throws Exception {
		String hql = "from ApplyOrganizeAddress t where 1=1 and t.id='"+code+"'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<ApplyOrganizeAddress> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//地址的下拉列表框
	public List<ApplyOrganizeAddress> showApplyOrganizeAddress(String code){
		String hql = "from ApplyOrganizeAddress t where 1=1 and t.applyOrganize='"+code+"'";
		//Long total = (Long) this.getSession().createQuery("select id,name " + hql).uniqueResult();
		List<ApplyOrganizeAddress> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<ApplyOrganize> showApplyOrganize(String code){
		String hql = "from ApplyOrganize t where 1=1 and t.person='"+code+"'";
		List<ApplyOrganize> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据办事处ID查询实体对象
	public ApplyOrganize selectApplyOrganize(String id){
		String hql = "from ApplyOrganize t where 1=1 and t.id='"+id+"'";
		ApplyOrganize list = (ApplyOrganize)this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
}