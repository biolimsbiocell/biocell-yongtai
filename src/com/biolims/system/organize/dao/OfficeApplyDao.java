package com.biolims.system.organize.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.system.organize.model.OfficeApply;
import com.biolims.system.organize.model.OfficeApplyItem;

@Repository
@SuppressWarnings("unchecked")
public class OfficeApplyDao extends BaseHibernateDao {
	public Map<String, Object> selectOfficeApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql=null;
		hql = " from OfficeApply o where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<OfficeApply> list = new ArrayList<OfficeApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * Dialog弹窗
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectOfficeApplyList1(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql=null;
		hql = " from OfficeApply o where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key=" and state='1'";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<OfficeApply> list = new ArrayList<OfficeApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectOfficeApplyItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort,String flag) throws Exception {
			String key = "";
			String hql = null;
			if(flag.equals("101")){
				 hql = "from OfficeApplyItem o where 1=1 and o.state!='56' or o.state is null";
			}else{
				 hql = "from OfficeApplyItem o where 1=1 ";
			}
		if (scId != null)
			key = key + " and officeApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<OfficeApplyItem> list = new ArrayList<OfficeApplyItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		
	//根据Id查主数据信息
	public List<OfficeApplyItem> getOfficeApplyItem(String code){
		String hql = "from OfficeApplyItem t where 1=1 and  t.officeApply='" + code + "'";		
		List<OfficeApplyItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//选择申请单加载子表信息
	public Map<String, Object> setOfficeApplyItemList(String code) throws Exception {
		String hql = "from OfficeApplyItem t where 1=1 and packCode='"+code+"'";
		String key = "";
		if (code != null)
			key = key + " and  t.OfficeApply='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApplyItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//根据物料包编码查可重用样本
	public List<String> getOfficeApplyItemByPcode(String code,String markCode){
		String hql = "from OfficeApplyItem t where 1=1 and  t.packCode='" + code + "' and t.code like '%____"+markCode+"%'";		
		List<String> list = this.getSession().createQuery("select code "+hql).list();
		return list;
	}
}