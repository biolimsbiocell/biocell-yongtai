package com.biolims.system.organize.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 申请明细
 * @author lims-platform
 * @date 2015-12-09 14:37:09
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_OFFICE_APPLY_ITEM")
@SuppressWarnings("serial")
public class OfficeApplyItem extends EntityDao<OfficeApplyItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**采血管编号*/
	private String code;
	/**物料包编号*/
	private String packCode;
	/**名称*/
	private String name;
	/**相关表id*/
	private OfficeApply officeApply;
	/**状态*/
	private String state;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得OfficeApply
	 *@return: OfficeApply  相关表id
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OFFICE_APPLY")
	public OfficeApply getOfficeApply(){
		return this.officeApply;
	}
	/**
	 *方法: 设置OfficeApply
	 *@param: OfficeApply  相关表id
	 */
	public void setOfficeApply(OfficeApply officeApply){
		this.officeApply = officeApply;
	}
	public String getPackCode() {
		return packCode;
	}
	public void setPackCode(String packCode) {
		this.packCode = packCode;
	}
	
}