package com.biolims.system.organize.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
/**   
 * @Title: Model
 * @Description: 申请组织
 * @author lims-platform
 * @date 2015-11-09 19:04:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_APPLY_ORGANIZE")
@SuppressWarnings("serial")
public class ApplyOrganize extends EntityDao<ApplyOrganize> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**负责人*/
	private User person;
	/**单位名称*/
	private String dept;
	/**区域代码*/
	private DicType markCode;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PERSON")
	public User getPerson(){
		return this.person;
	}
	/**
	 *方法: 设置User
	 *@param: User  负责人
	 */
	public void setPerson(User person){
		this.person = person;
	}
	/**
	 *方法: 取得String
	 *@return: String  单位名称
	 */
	@Column(name ="DEPT", length = 36)
	public String getDept(){
		return this.dept;
	}
	
	
	public DicType getMarkCode() {
		return markCode;
	}
	public void setMarkCode(DicType markCode) {
		this.markCode = markCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  单位名称
	 */
	public void setDept(String dept){
		this.dept = dept;
	}
}