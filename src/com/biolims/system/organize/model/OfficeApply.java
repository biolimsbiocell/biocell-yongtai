package com.biolims.system.organize.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 办事处申请表
 * @author lims-platform
 * @date 2015-12-09 14:37:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_OFFICE_APPLY")
@SuppressWarnings("serial")
public class OfficeApply extends EntityDao<OfficeApply> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**创建人*/
	private User createUser;
	/**创建时间*/
	private Date createDate;
	/**审批人*/
	private User acceptUser;
	/**审批时间*/
	private Date acceptDate;
	/**状态*/
	private String state;
	/**状态名称*/
	private String statename;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  审批人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser(){
		return this.acceptUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  审批人
	 */
	public void setAcceptUser(User acceptUser){
		this.acceptUser = acceptUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审批时间
	 */
	@Column(name ="ACCEPT_DATE", length = 50)
	public Date getAcceptDate(){
		return this.acceptDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审批时间
	 */
	public void setAcceptDate(Date acceptDate){
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态名称
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStatename(){
		return this.statename;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态名称
	 */
	public void setStatename(String statename){
		this.statename = statename;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 500)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
}