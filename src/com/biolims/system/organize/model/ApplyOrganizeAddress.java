package com.biolims.system.organize.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 收货地址
 * @author lims-platform
 * @date 2015-11-09 19:04:34
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_APPLY_ORGANIZE_ADDRESS")
@SuppressWarnings("serial")
public class ApplyOrganizeAddress extends EntityDao<ApplyOrganizeAddress> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**收货地址*/
	private String name;
	/**收件人*/
	private String receiver;
	/**收件人电话*/
	private String phone;
	/**运输时间*/
	private Integer transTime;
	/**相关主表*/
	private ApplyOrganize applyOrganize;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  收货地址
	 */
	@Column(name ="NAME", length = 60)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  收货地址
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  收件人
	 */
	@Column(name ="RECEIVER", length = 36)
	public String getReceiver(){
		return this.receiver;
	}
	/**
	 *方法: 设置String
	 *@param: String  收件人
	 */
	public void setReceiver(String receiver){
		this.receiver = receiver;
	}
	/**
	 *方法: 取得String
	 *@return: String  收件人电话
	 */
	@Column(name ="PHONE", length = 36)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  收件人电话
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	
	/**
	 *方法: 取得ApplyOrganize
	 *@return: ApplyOrganize  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPLY_ORGANIZE")
	public ApplyOrganize getApplyOrganize(){
		return this.applyOrganize;
	}
	/**
	 *方法: 设置ApplyOrganize
	 *@param: ApplyOrganize  相关主表
	 */
	public void setApplyOrganize(ApplyOrganize applyOrganize){
		this.applyOrganize = applyOrganize;
	}
	public Integer getTransTime() {
		return transTime;
	}
	public void setTransTime(Integer transTime) {
		this.transTime = transTime;
	}
	
}