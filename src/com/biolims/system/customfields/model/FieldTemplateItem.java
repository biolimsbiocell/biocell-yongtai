package com.biolims.system.customfields.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @ClassName: FieldTemplateItem
 * @Description: TODO(自定义字段明细)
 * @author 尹标舟
 * @date 2018-3-9
 * 
 */
@Entity
@Table(name = "T_FIELD_TEMPLATE_ITEM")
public class FieldTemplateItem extends EntityDao<FieldTemplateItem> implements
		java.io.Serializable {

	private static final long serialVersionUID = -8783340035488564663L;

	/** ID */
	private String id;
	/** 字段编码 */
	private String fieldName;
	/** 字段类型 */
	private String fieldType;
	/** 字段显示名称 */
	private String label;
	/** 是否只读 */
	private String readOnly;
	/** 字段长度 */
	private Integer fieldLength;
	/** 字段长度 */
	private Integer orderNum;
	/** 默认值 */
	private String defaultValue;
	/** 字段提示语 */
	private String typeContent;
	/** 是否必填 */
	private String isRequired;
	/** 主表id */
	private FieldTemplate fieldTemplate;
	/** 选项id */
	private String singleOptionId;
	/** 选项 */
	private String singleOptionName;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "SINGLE_OPTION_ID", length = 255)
	public String getSingleOptionId() {
		return singleOptionId;
	}

	public void setSingleOptionId(String singleOptionId) {
		this.singleOptionId = singleOptionId;
	}

	@Column(name = "SINGLE_OPTION_NAME", length = 255)
	public String getSingleOptionName() {
		return singleOptionName;
	}

	public void setSingleOptionName(String singleOptionName) {
		this.singleOptionName = singleOptionName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FIELD_TEMPLATE")
	public FieldTemplate getFieldTemplate() {
		return fieldTemplate;
	}

	public void setFieldTemplate(FieldTemplate fieldTemplate) {
		this.fieldTemplate = fieldTemplate;
	}

	@Column(name = "FIELD_NAME", length = 255)
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Column(name = "IS_REQUIRED", length = 255)
	public String getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(String isRequired) {
		this.isRequired = isRequired;
	}

	@Column(name = "FIELD_TYPE", length = 255)
	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	@Column(name = "LABEL", length = 255)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Column(name = "READ_ONLY", length = 255)
	public String getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}

	@Column(name = "FIELD_LENGTH", length = 255)
	public Integer getFieldLength() {
		return fieldLength;
	}

	public void setFieldLength(Integer fieldLength) {
		this.fieldLength = fieldLength;
	}

	@Column(name = "DEFAULT_VALUE", length = 255)
	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Column(name = "TYPE_CONTENT", length = 255)
	public String getTypeContent() {
		return typeContent;
	}

	public void setTypeContent(String typeContent) {
		this.typeContent = typeContent;
	}

	/**
	 * @return the orderNum
	 */
	public Integer getOrderNum() {
		return orderNum;
	}

	/**
	 * @param orderNum the orderNum to set
	 */
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

}
