package com.biolims.system.customfields.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @ClassName: FieldTemplate
 * @Description: TODO(自定义字段主表)
 * @author 尹标舟
 * @date 2018-3-12
 * 
 */
@Entity
@Table(name = "T_FIELD_TEMPLATE")
public class FieldTemplate extends EntityDao<FieldTemplate> implements
		java.io.Serializable {

	private static final long serialVersionUID = -9096663081655987691L;

	/** ID */
	private String id;
	/** 检测项目id */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 备注 */
	private String note;
	/** 创建时间 */
	private Date createDate;
	/** 创建人 */
	private User createUser;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 所属模块id */
	private String subordinateModuleId;
	/** 所属模块 */
	private String subordinateModuleName;

	@Id
	@Column(name = "ID", length = 255)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "SUBORDINATE_MODULE_ID", length = 255)
	public String getSubordinateModuleId() {
		return subordinateModuleId;
	}

	public void setSubordinateModuleId(String subordinateModuleId) {
		this.subordinateModuleId = subordinateModuleId;
	}

	@Column(name = "SUBORDINATE_MODULE_NAME", length = 255)
	public String getSubordinateModuleName() {
		return subordinateModuleName;
	}

	public void setSubordinateModuleName(String subordinateModuleName) {
		this.subordinateModuleName = subordinateModuleName;
	}

	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	@Column(name = "STATE", length = 255)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "STATE_NAME", length = 255)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "PRODUCT_ID", length = 255)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 255)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
