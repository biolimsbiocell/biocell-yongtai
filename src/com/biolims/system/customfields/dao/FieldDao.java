package com.biolims.system.customfields.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.Storage;
import com.biolims.system.customfields.model.FieldTemplateItem;
@SuppressWarnings("unchecked")
@Repository
public class FieldDao extends BaseHibernateDao {

	public Map<String, Object> findFieldTemplateTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FieldTemplate where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from FieldTemplate where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findFieldTemplateItemTable(Integer start,
			Integer length, String query, String col, String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FieldTemplateItem where 1=1 and fieldTemplate.id='"
				+ id + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from FieldTemplateItem where 1=1 and fieldTemplate.id='"
					+ id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<FieldTemplateItem> list = new ArrayList<FieldTemplateItem>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findFieldByProductId(String field) {
		Map<String, Object> map = new HashMap<String, Object>();
		String hql = " from FieldTemplateItem where 1=1 and fieldTemplate.id  in("+field+")";
		String key="";
		List<FieldTemplateItem> list;
			list = this.getSession().createQuery(hql+key).list();	
		map.put("result", list);
		return map;
	}

	public Map<String, Object> findFieldByModuleValue(String moduleValue) {
		Map<String, Object> map = new HashMap<String, Object>();
		String hql = " from FieldTemplateItem where 1=1 and fieldTemplate.subordinateModuleId=:mod and fieldTemplate.state='1' order by orderNum";
		List<FieldTemplateItem> list = this.getSession().createQuery(hql)
				.setString("mod", moduleValue).list();
		map.put("result", list);
		return map;
	}
	public Storage findMedium(String id) {
		String hql = " from Storage where 1=1 and id='"+id+"'";
		Storage storage = (Storage) this.getSession().createQuery(hql).uniqueResult();
		return storage;
	}

	public List<FieldTemplateItem> getAll(String modelValue) {
		String hql = " from FieldTemplateItem where fieldTemplate.subordinateModuleId=:mod";
		return this.getSession().createQuery(hql).setString("mod", modelValue)
				.list();
	}

	public Map<String, Object> showCustomDialogJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FieldTemplate where 1=1 and subordinateModuleId='SampleOrder' and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from FieldTemplate where 1=1 and subordinateModuleId='SampleOrder' and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

}
