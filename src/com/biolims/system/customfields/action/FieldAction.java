package com.biolims.system.customfields.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.system.customfields.model.FieldTemplate;
import com.biolims.system.customfields.model.FieldTemplateItem;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Controller
@SuppressWarnings("unchecked")
@Scope("prototype")
@ParentPackage("default")
@Namespace("/system/customfields")
public class FieldAction extends BaseActionSupport {

	private static final long serialVersionUID = 4965720113687207471L;
	private String rightsId = "18031201";

	@Resource
	private FieldService fieldService;

	@Resource
	private CommonService commonService;
	
	@Resource
	private CodingRuleService codingRuleService;

	private FieldTemplate fieldTemplate = new FieldTemplate();

	public FieldTemplate getFieldTemplate() {
		return fieldTemplate;
	}

	public void setFieldTemplate(FieldTemplate fieldTemplate) {
		this.fieldTemplate = fieldTemplate;
	}

	/**
	 * 
	 * @Title: showFieldTable
	 * @Description: TODO(跳转自定义字段主表列表页面)
	 * @author 尹标舟
	 * @date 2018-3-12下午1:27:45
	 * @return
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "showFieldTable")
	public String showFieldTable() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/customfields/showCustomField.jsp");
	}

	/**
	 * 
	 * @Title: showCustomFieldListJson
	 * @Description: TODO(查询主表列表信息)
	 * @author 尹标舟
	 * @date 2018-3-12下午1:31:07
	 * @throws
	 */
	@Action(value = "showCustomFieldListJson")
	public void showCustomFieldListJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = fieldService.findFieldTemplateTable(
					start, length, query, col, sort);
			List<FieldTemplate> list = (List<FieldTemplate>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: editCustomFields
	 * @Description: TODO(进入自定义地段详情)
	 * @author 尹标舟
	 * @date 2018-3-12下午5:23:54
	 * @return
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "editCustomFields")
	public String editCustomFields() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			this.fieldTemplate = commonService.get(FieldTemplate.class, id);
			if (fieldTemplate == null) {
				fieldTemplate = new FieldTemplate();
				fieldTemplate.setId(id);
				User user = (User) this
						.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				fieldTemplate.setCreateUser(user);
				fieldTemplate.setCreateDate(new Date());
				fieldTemplate
						.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				fieldTemplate
						.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			}

		} else {
			fieldTemplate.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			fieldTemplate.setCreateUser(user);
			fieldTemplate.setCreateDate(new Date());
			fieldTemplate
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			fieldTemplate
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/system/customfields/customFieldEdit.jsp");
	}

	@Action(value = "viewCustomFields")
	public String toViewSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		this.fieldTemplate = commonService.get(FieldTemplate.class, id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/customfields/customFieldEdit.jsp");
	}
	@Action(value="delCustomFields")
	public void delCustomFields() throws Exception{

		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String id = getParameterFromRequest("id");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fieldService.delCustomFields(delStr, ids, user, id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	
	}
	
	@Action(value = "showFieldTemplateItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFieldTemplateItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = fieldService.findFieldTemplateItemTable(
				start, length, query, col, sort, id);
		List<FieldTemplateItem> list = (List<FieldTemplateItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("fieldName", "");
		map.put("fieldType", "");
		map.put("label", "");
		map.put("readOnly", "");
		map.put("orderNum", "");
		map.put("fieldTemplate-id", "");
		map.put("fieldTemplate-note", "");
		map.put("fieldLength", "");
		map.put("defaultValue", "");
		map.put("typeContent", "");
		map.put("isRequired", "");
		map.put("singleOptionId", "");
		map.put("singleOptionName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = fieldTemplate.getId();
		String log="";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log="123";
			String modelName = "FieldTemplate";
			String markCode = "CF";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName,
					markCode, stime, 000000, 6, null);
			fieldTemplate.setId(autoID);
		}
		
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		Map aMap = new HashMap();
		aMap.put("fieldTemplateItem",
				getParameterFromRequest("fieldTemplateInfoJson"));
		fieldService.save(fieldTemplate, aMap,changeLog,changeLogItem,log);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/system/customfields/editCustomFields.action?id="
				+ fieldTemplate.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}
	
	/**
	 * 
	 * @Title: findFieldByProductId  
	 * @Description: TODO(根据模块、检测项目查询自定义字段信息)  
	 * @author 尹标舟
	 * @date 2018-3-13下午4:01:31
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "findFieldByProductId")
	public void findFieldByProductId() throws Exception{
		String productId = getRequest().getParameter("productId");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> dataListMap = this.fieldService
					.findFieldByProductId(productId);
			result.put("success", true);
			result.put("data", dataListMap.get("result"));
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 
	 * @Title: findFieldByModuleValue  
	 * @Description: TODO(根据所属模块，查询自定义字段)  
	 * @author 尹标舟
	 * @date 2018-3-14下午5:53:05
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "findFieldByModuleValue")
	public void findFieldByModuleValue() throws Exception{
		String moduleValue = getRequest().getParameter("moduleValue");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> dataListMap = this.fieldService
					.findFieldByModuleValue(moduleValue);
			result.put("success", true);
			result.put("data", dataListMap.get("result"));
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	@Action(value = "checkFieldName")
	public void checkFieldName() throws Exception{
		String fieldTemplateInfoJson = getRequest().getParameter("fieldTemplateInfoJson");
		String ModelValue = getRequest().getParameter("ModelValue");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String,Object> map = this.fieldService
					.checkFieldName(fieldTemplateInfoJson,ModelValue);
			if((Boolean)map.get("bool") == true){
				result.put("bool", true);
			}else{
				result.put("bool", false);
				result.put("msg", "字段编码："+map.get("fieldName")+"已存在");
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	@Action("showCustomDialog")
	public String showCustomDialog(){
		return dispatcher("/WEB-INF/page/system/customfields/customFieldSelTable.jsp");
	}
	
	
	@Action(value = "showCustomDialogJson")
	public void showCustomDialogJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = fieldService.showCustomDialogJson(
					start, length, query, col, sort);
			List<FieldTemplate> list = (List<FieldTemplate>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("subordinateModuleName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
