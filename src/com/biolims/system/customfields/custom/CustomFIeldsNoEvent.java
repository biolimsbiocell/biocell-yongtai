package com.biolims.system.customfields.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.system.customfields.service.FieldService;

public class CustomFIeldsNoEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		FieldService mbService = (FieldService) ctx
				.getBean("fieldService");
		mbService.modify(applicationTypeActionId, contentId);

		return "";
	}
}
