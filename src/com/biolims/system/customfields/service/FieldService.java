package com.biolims.system.customfields.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.Storage;
import com.biolims.system.customfields.dao.FieldDao;
import com.biolims.system.customfields.model.FieldTemplate;
import com.biolims.system.customfields.model.FieldTemplateItem;
import com.biolims.system.product.model.Product;
import com.biolims.util.JsonUtils;
import com.oracle.jrockit.jfr.Producer;

@Service
@Transactional
public class FieldService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldDao fieldDao;

	/**
	 * 
	 * @Title: findFieldTemplateTable
	 * @Description: TODO(查询主表列表信息)
	 * @author 尹标舟
	 * @date 2018-3-12下午1:25:47
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findFieldTemplateTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return fieldDao.findFieldTemplateTable(start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: findFieldTemplateItemTable
	 * @Description: TODO(查询子表的数据)
	 * @author 尹标舟
	 * @date 2018-3-13上午9:38:41
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @param id
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findFieldTemplateItemTable(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return fieldDao.findFieldTemplateItemTable(start, length, query, col,
				sort, id);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FieldTemplate ft, Map jsonMap, String changeLog, String changeLogItem,String log) throws Exception {
		if (ft != null) {
			fieldDao.saveOrUpdate(ft);
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("FieldTemplate");
				li.setFileId(ft.getId());
				li.setModifyContent(changeLog);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("fieldTemplateItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFieldTemplateItem(ft, jsonStr,changeLogItem,log);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFieldTemplateItem(FieldTemplate ft, String itemDataJson, String changeLogItem,String log)
			throws Exception {
		List<FieldTemplateItem> saveItems = new ArrayList<FieldTemplateItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FieldTemplateItem fti = new FieldTemplateItem();
			// 将map信息读入实体类
			fti = (FieldTemplateItem) fieldDao.Map2Bean(map, fti);
			if (fti.getId() != null && fti.getId().equals(""))
				fti.setId(null);
			fti.setFieldTemplate(ft);

			saveItems.add(fti);
		}
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setClassName("FieldTemplate");
			li.setFileId(ft.getId());
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		fieldDao.saveOrUpdateAll(saveItems);
	}

	public Map<String, Object> findFieldByProductId(String productId) {
		String [] product=productId.split(",");
		Map<String, Object> map=new HashMap<String, Object>();
		List<String> list=new ArrayList<String>();
			for(String p:product){
					Product pro=commonDAO.get(Product.class, p);
					if(pro!=null&&!"".equals(pro)){
					if(pro.getFieldTemplate()!=null){
						list.add(pro.getFieldTemplate().getId());
					}
				}
			}
		if(list.size()>0){
			String field="";
			for(int i=0;i<list.size();i++){
				if(i==list.size()-1){
					field+="\'"+list.get(i)+"'";
				}else{
					field+="\'"+list.get(i)+"\',";
				}
			}
		map= fieldDao.findFieldByProductId(field);
		}
		return map;
	}

	public Map<String, Object> findFieldByModuleValue(String moduleValue) {
		return fieldDao.findFieldByModuleValue(moduleValue);
	}
	public Boolean findMedium(String id) {
		Boolean pan=true;
		Storage findMedium = fieldDao.findMedium(id);
		if(findMedium!=null) {
			if(findMedium.getMedium()!=""&&findMedium.getMedium()!=null) {
				if(findMedium.getMedium().equals("1")) {
					return pan;	
				}else {
					pan=false;
				}
			
			}else {
				pan=false;
			}
		}
		return pan;
	}

	public Map<String,Object> checkFieldName(String fieldTemplateInfoJson,String sampleOrder) throws Exception {
		List<FieldTemplateItem> saveItems = new ArrayList<FieldTemplateItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				fieldTemplateInfoJson, List.class);
		Map<String,Object> mapReturn = new HashMap<String,Object>();
		List<FieldTemplateItem> listField = fieldDao.getAll(sampleOrder);
		for (Map<String, Object> map : list) {
			FieldTemplateItem fti = new FieldTemplateItem();
			// 将map信息读入实体类
			fti = (FieldTemplateItem) fieldDao.Map2Bean(map, fti);
			for(int i=0;i<listField.size();i++){
				FieldTemplateItem fieldTemplateItem = listField.get(i);
				if(!fieldTemplateItem.getId().equals(fti.getId())&&fieldTemplateItem.getFieldName().equals(fti.getFieldName())){
					mapReturn.put("bool", false);
					mapReturn.put("fieldName",fti.getFieldName());
					return mapReturn;
				}
			}
		}
		mapReturn.put("bool", true);
		return mapReturn;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String contentId) {
		FieldTemplate ft=new FieldTemplate();
		ft=commonDAO.get(FieldTemplate.class, contentId);
		ft.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		ft.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		commonDAO.saveOrUpdate(ft);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCustomFields(String delStr, String[] ids, User user,
			String field_id) {

		String delId="";
		for (String id : ids) {
			FieldTemplateItem scp = commonDAO.get(FieldTemplateItem.class, id);
			if (scp.getId() != null) {
				fieldDao.delete(scp);
			}
		delId+=scp.getFieldName()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(field_id);
			li.setClassName("FieldTemplate");
			li.setModifyContent(delStr+delId);
			li.setState("2");
			li.setStateName("数据删除");
			commonDAO.saveOrUpdate(li);
		}
	
		
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void modify(String applicationTypeActionId, String contentId) {
		FieldTemplate ft=new FieldTemplate();
		ft=commonDAO.get(FieldTemplate.class, contentId);
		ft.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
		ft.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
		commonDAO.saveOrUpdate(ft);
	}

	public Map<String, Object> showCustomDialogJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return fieldDao.showCustomDialogJson(start, length, query, col, sort);
	}
}
