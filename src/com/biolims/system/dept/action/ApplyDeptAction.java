﻿
package com.biolims.system.dept.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.dept.model.ApplyDept;
import com.biolims.system.dept.service.ApplyDeptService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/system/dept/applyDept")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ApplyDeptAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private ApplyDeptService applyDeptService;
	private ApplyDept applyDept = new ApplyDept();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showApplyDeptList")
	public String showApplyDeptList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/dept/applyDept.jsp");
	}

	@Action(value = "showApplyDeptListJson")
	public void showApplyDeptListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = applyDeptService.findApplyDeptList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ApplyDept> list = (List<ApplyDept>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("user-id", "");
		map.put("user-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "applyDeptSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogApplyDeptList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/com/biolims/system/dept/ApplyDeptDialog.jsp");
	}

	@Action(value = "showDialogApplyDeptListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogApplyDeptListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = applyDeptService.findApplyDeptList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ApplyDept> list = (List<ApplyDept>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("user-id", "");
		map.put("user-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editApplyDept")
	public String editApplyDept() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			applyDept = applyDeptService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "applyDept");
		} else {
			applyDept=new ApplyDept();
			applyDept.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			saveLocation.setCreateUser(user);
//			saveLocation.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/com/biolims/system/dept/applyDeptEdit.jsp");
	}

	@Action(value = "copyApplyDept")
	public String copyApplyDept() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		applyDept = applyDeptService.get(id);
		applyDept.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/com/biolims/system/dept/applyDeptEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = applyDept.getId();
		if(id!=null&&id.equals("")){
			applyDept.setId(null);
		}
		Map aMap = new HashMap();
		applyDeptService.save(applyDept,aMap);
		return redirect("/system/dept/applyDept/editApplyDept.action?id=" + applyDept.getId());

	}

	@Action(value = "viewApplyDept")
	public String toViewApplyDept() throws Exception {
		String id = getParameterFromRequest("id");
		applyDept = applyDeptService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/com/biolims/system/dept/applyDeptEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ApplyDeptService getApplyDeptService() {
		return applyDeptService;
	}

	public void setApplyDeptService(ApplyDeptService applyDeptService) {
		this.applyDeptService = applyDeptService;
	}

	public ApplyDept getApplyDept() {
		return applyDept;
	}

	public void setApplyDept(ApplyDept applyDept) {
		this.applyDept = applyDept;
	}


}
