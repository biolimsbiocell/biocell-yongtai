package com.biolims.system.dept.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.dept.dao.ApplyDeptDao;
import com.biolims.system.dept.model.ApplyDept;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ApplyDeptService {
	@Resource
	private ApplyDeptDao applyDeptDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findApplyDeptList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return applyDeptDao.selectApplyDeptList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ApplyDept i) throws Exception {

		applyDeptDao.saveOrUpdate(i);

	}
	public ApplyDept get(String id) {
		ApplyDept applyDept = commonDAO.get(ApplyDept.class, id);
		return applyDept;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ApplyDept sc, Map jsonMap) throws Exception {
		if (sc != null) {
			applyDeptDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
