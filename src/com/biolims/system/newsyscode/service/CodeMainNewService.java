package com.biolims.system.newsyscode.service;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.newsyscode.dao.CodeMainNewDao;
import com.biolims.system.newsyscode.model.CodeMainNew;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CodeMainNewService {
	@Resource
	private CodeMainNewDao codeMainNewDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findCodeMainNewTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return codeMainNewDao.findCodeMainNewTable(start, length, query, col,
				sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CodeMainNew i) throws Exception {

		codeMainNewDao.saveOrUpdate(i);

	}

	public CodeMainNew get(String id) {
		CodeMainNew codeMainNew = commonDAO.get(CodeMainNew.class, id);
		return codeMainNew;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CodeMainNew sc, Map jsonMap, String logInfo, Map logMap)
			throws Exception {
		if (sc != null) {
			codeMainNewDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setModifyContent(logInfo);
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
		}
	}
}
