﻿package com.biolims.system.newsyscode.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.newsyscode.model.CodeMainNew;
import com.biolims.system.newsyscode.service.CodeMainNewService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/system/newsyscode/codeMainNew")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CodeMainNewAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "19112";
	@Autowired
	private CodeMainNewService codeMainNewService;
	private CodeMainNew codeMainNew = new CodeMainNew();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showCodeMainNewList")
	public String showCodeMainNewList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/newsyscode/codeMainNew.jsp");
	}

	@Action(value = "showCodeMainNewEditList")
	public String showCodeMainNewEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/newsyscode/codeMainNewEditList.jsp");
	}

	@Action(value = "showCodeMainNewTableJson")
	public void showCodeMainNewTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = codeMainNewService
					.findCodeMainNewTable(start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<CodeMainNew> list = (List<CodeMainNew>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("namex", "");
			map.put("namey", "");
			map.put("codex", "");
			map.put("codey", "");
			map.put("qrx", "");
			map.put("qry", "");
			map.put("ip", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService
					.findFieldByModuleValue("CodeMainNew");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "codeMainNewSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCodeMainNewList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/newsyscode/codeMainNewSelectTable.jsp");
	}

	@Action(value = "editCodeMainNew")
	public String editCodeMainNew() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			codeMainNew = codeMainNewService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "codeMainNew");
		} else {
			// User user = (User)
			// this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// codeMainNew.setCreateUser(user);
			// codeMainNew.setCreateDate(new Date());
			// codeMainNew.setState(SystemConstants.DIC_STATE_NEW);
			// codeMainNew.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			codeMainNew.setScopeId((String) ActionContext.getContext()
					.getSession().get("scopeId"));
			codeMainNew.setScopeName((String) ActionContext.getContext()
					.getSession().get("scopeName"));

			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/newsyscode/codeMainNewEdit.jsp");
	}

	@Action(value = "copyCodeMainNew")
	public String copyCodeMainNew() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		codeMainNew = codeMainNewService.get(id);
		codeMainNew.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/newsyscode/codeMainNewEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
					List.class);

			for (Map<String, Object> map : list) {
				codeMainNew = (CodeMainNew) commonDAO
						.Map2Bean(map, codeMainNew);
			}
			String id = codeMainNew.getId();
			if (id != null && id.equals("")) {
				codeMainNew.setId(null);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();

			codeMainNewService.save(codeMainNew, aMap, changeLog, lMap);

			zId = codeMainNew.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewCodeMainNew")
	public String toViewCodeMainNew() throws Exception {
		String id = getParameterFromRequest("id");
		codeMainNew = codeMainNewService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/newsyscode/codeMainNewEdit.jsp");
	}

	@Action(value = "saveCodeMainNewTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCodeMainNewTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
				List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				CodeMainNew a = new CodeMainNew();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (CodeMainNew) commonDAO.Map2Bean(map1, a);
				a.setScopeId((String) ActionContext.getContext().getSession()
						.get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession()
						.get("scopeName"));

				codeMainNewService.save(a, aMap, changeLog, lMap);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CodeMainNewService getCodeMainNewService() {
		return codeMainNewService;
	}

	public void setCodeMainNewService(CodeMainNewService codeMainNewService) {
		this.codeMainNewService = codeMainNewService;
	}

	public CodeMainNew getCodeMainNew() {
		return codeMainNew;
	}

	public void setCodeMainNew(CodeMainNew codeMainNew) {
		this.codeMainNew = codeMainNew;
	}

	// 选择打印机
	@Action(value = "selCodeMainNew")
	public String selCodeMainNew() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/newsyscode/selCodeMainNew.jsp");
	}
}
