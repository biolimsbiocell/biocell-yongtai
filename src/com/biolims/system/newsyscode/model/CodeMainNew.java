package com.biolims.system.newsyscode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 条码模板
 * @author lims-platform
 * @date 2018-12-07 20:32:40
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CODE_MAIN_NEW")
@SuppressWarnings("serial")
public class CodeMainNew extends EntityDao<CodeMainNew> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 打印指令 */
	private String code;
	/** 名字x轴 */
	private String namex;
	/** 名字y轴 */
	private String namey;
	/** 编码x轴 */
	private String codex;
	/** 编码y轴 */
	private String codey;
	/** 二维码x轴 */
	private String qrx;
	/** 二维码y轴 */
	private String qry;
	/** ip地址 */
	private String ip;
	// 状态
	private String state;
	private String scopeId;
	private String scopeName;

	@Column(name = "STATE")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 255)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 255)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 打印指令
	 */
	@Column(name = "CODE", length = 255)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 打印指令
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名字x轴
	 */
	@Column(name = "NAMEX", length = 255)
	public String getNamex() {
		return this.namex;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名字x轴
	 */
	public void setNamex(String namex) {
		this.namex = namex;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名字y轴
	 */
	@Column(name = "NAMEY", length = 255)
	public String getNamey() {
		return this.namey;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名字y轴
	 */
	public void setNamey(String namey) {
		this.namey = namey;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码x轴
	 */
	@Column(name = "CODEX", length = 255)
	public String getCodex() {
		return this.codex;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码x轴
	 */
	public void setCodex(String codex) {
		this.codex = codex;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码y轴
	 */
	@Column(name = "CODEY", length = 255)
	public String getCodey() {
		return this.codey;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码y轴
	 */
	public void setCodey(String codey) {
		this.codey = codey;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 二维码x轴
	 */
	@Column(name = "QRX", length = 255)
	public String getQrx() {
		return this.qrx;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 二维码x轴
	 */
	public void setQrx(String qrx) {
		this.qrx = qrx;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 二维码y轴
	 */
	@Column(name = "QRY", length = 255)
	public String getQry() {
		return this.qry;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 二维码y轴
	 */
	public void setQry(String qry) {
		this.qry = qry;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ip地址
	 */
	@Column(name = "IP", length = 255)
	public String getIp() {
		return this.ip;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ip地址
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
}

/*
 * 
 * 
 * //中文JS配置文件 biolims.codeMainNew={}; biolims.codeMainNew.id="编号";
 * biolims.codeMainNew.name="描述"; biolims.codeMainNew.code="打印指令";
 * biolims.codeMainNew.namex="名字x轴"; biolims.codeMainNew.namey="名字y轴";
 * biolims.codeMainNew.codex="编码x轴"; biolims.codeMainNew.codey="编码y轴";
 * biolims.codeMainNew.qrx="二维码x轴"; biolims.codeMainNew.qry="二维码y轴";
 * biolims.codeMainNew.ip="ip地址";
 * 
 * 
 * 
 * 
 * //英文JS配置文件 biolims.codeMainNew.id="id"; biolims.codeMainNew.name="name";
 * biolims.codeMainNew.code="code"; biolims.codeMainNew.namex="namex";
 * biolims.codeMainNew.namey="namey"; biolims.codeMainNew.codex="codex";
 * biolims.codeMainNew.codey="codey"; biolims.codeMainNew.qrx="qrx";
 * biolims.codeMainNew.qry="qry"; biolims.codeMainNew.ip="ip";
 * 
 * 
 * //中文配置文件 biolims.codeMainNew.id=编号 biolims.codeMainNew.name=描述
 * biolims.codeMainNew.code=打印指令 biolims.codeMainNew.namex=名字x轴
 * biolims.codeMainNew.namey=名字y轴 biolims.codeMainNew.codex=编码x轴
 * biolims.codeMainNew.codey=编码y轴 biolims.codeMainNew.qrx=二维码x轴
 * biolims.codeMainNew.qry=二维码y轴 biolims.codeMainNew.ip=ip地址
 * 
 * 
 * //英文配置文件 biolims.codeMainNew.id=id biolims.codeMainNew.name=name
 * biolims.codeMainNew.code=code biolims.codeMainNew.namex=namex
 * biolims.codeMainNew.namey=namey biolims.codeMainNew.codex=codex
 * biolims.codeMainNew.codey=codey biolims.codeMainNew.qrx=qrx
 * biolims.codeMainNew.qry=qry biolims.codeMainNew.ip=ip
 */