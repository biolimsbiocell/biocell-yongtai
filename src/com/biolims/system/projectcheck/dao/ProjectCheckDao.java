package com.biolims.system.projectcheck.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.system.projectcheck.model.ProjectCheck;
import com.biolims.system.projectcheck.model.ProjectCheckItem;

@Repository
@SuppressWarnings("unchecked")
public class ProjectCheckDao extends BaseHibernateDao {
	public Map<String, Object> selectProjectCheckList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from ProjectCheck where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<ProjectCheck> list = new ArrayList<ProjectCheck>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectProjectCheckItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from ProjectCheckItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and projectCheck.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProjectCheckItem> list = new ArrayList<ProjectCheckItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<ProjectCheckItem> selectProjectItemById(String projectId) throws Exception{
			String hql = "from ProjectCheckItem where 1=1 ";
			String key = "";
			if (projectId != null)
				key = key + " and projectCheck.id='" + projectId + "'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<ProjectCheckItem> list = new ArrayList<ProjectCheckItem>();
			list = this.getSession().createQuery(hql + key).list();
			if(list.size()>0)
			return list;
			return null;
		}
	/**
	 * 根据主表查询子表集合
	 * @param id
	 * @return
	 */
	public List<ProjectCheckItem> getProjectCheckItemList(String id){
		String hql="from ProjectCheckItem t where t.projectCheck='"+id+"'";
		List<ProjectCheckItem> list=getSession().createQuery(hql).list();
		return list;
	}
}