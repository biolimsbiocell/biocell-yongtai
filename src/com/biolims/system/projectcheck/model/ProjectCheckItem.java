package com.biolims.system.projectcheck.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 项目质控明细
 * @author lims-platform
 * @date 2016-03-01 18:40:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "PROJECT_CHECK_ITEM")
@SuppressWarnings("serial")
public class ProjectCheckItem extends EntityDao<ProjectCheckItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**说明*/
	private String note;
	/**状态*/
	private String state;
	/**关联主表*/
	private ProjectCheck projectCheck;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  说明
	 */
	@Column(name ="NOTE", length = 20)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  说明
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 20)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得ProjectCheck
	 *@return: ProjectCheck  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT_CHECK")
	public ProjectCheck getProjectCheck(){
		return this.projectCheck;
	}
	/**
	 *方法: 设置ProjectCheck
	 *@param: ProjectCheck  关联主表
	 */
	public void setProjectCheck(ProjectCheck projectCheck){
		this.projectCheck = projectCheck;
	}
}