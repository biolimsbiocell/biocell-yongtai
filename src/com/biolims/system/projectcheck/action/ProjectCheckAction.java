﻿
package com.biolims.system.projectcheck.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.projectcheck.model.ProjectCheck;
import com.biolims.system.projectcheck.model.ProjectCheckItem;
import com.biolims.system.projectcheck.service.ProjectCheckService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/system/projectcheck/projectCheck")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ProjectCheckAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9017";
	@Autowired
	private ProjectCheckService projectCheckService;
	private ProjectCheck projectCheck = new ProjectCheck();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showProjectCheckList")
	public String showProjectCheckList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/projectcheck/projectCheck.jsp");
	}

	@Action(value = "showProjectCheckListJson")
	public void showProjectCheckListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = projectCheckService.findProjectCheckList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ProjectCheck> list = (List<ProjectCheck>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "projectCheckSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogProjectCheckList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/projectcheck/projectCheckDialog.jsp");
	}

	@Action(value = "showDialogProjectCheckListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogProjectCheckListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = projectCheckService.findProjectCheckList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ProjectCheck> list = (List<ProjectCheck>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editProjectCheck")
	public String editProjectCheck() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			projectCheck = projectCheckService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "projectCheck");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			projectCheck.setCreateUser(user);
//			projectCheck.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/projectcheck/projectCheckEdit.jsp");
	}

	@Action(value = "copyProjectCheck")
	public String copyProjectCheck() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		projectCheck = projectCheckService.get(id);
		projectCheck.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/projectcheck/projectCheckEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = projectCheck.getId();
		if(id!=null&&id.equals("")){
			projectCheck.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("projectCheckItem",getParameterFromRequest("projectCheckItemJson"));
		
		projectCheckService.save(projectCheck,aMap);
		return redirect("/system/projectcheck/projectCheck/editProjectCheck.action?id=" + projectCheck.getId());

	}

	@Action(value = "viewProjectCheck")
	public String toViewProjectCheck() throws Exception {
		String id = getParameterFromRequest("id");
		projectCheck = projectCheckService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/projectcheck/projectCheckEdit.jsp");
	}
	

	@Action(value = "showProjectCheckItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectCheckItemList() throws Exception {
		return dispatcher("/WEB-INF/page/system/projectcheck/projectCheckItem.jsp");
	}

	@Action(value = "showProjectCheckItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProjectCheckItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = projectCheckService.findProjectCheckItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<ProjectCheckItem> list = (List<ProjectCheckItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("state", "");
			map.put("projectCheck-name", "");
			map.put("projectCheck-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delProjectCheckItem")
	public void delProjectCheckItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			projectCheckService.delProjectCheckItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ProjectCheckService getProjectCheckService() {
		return projectCheckService;
	}

	public void setProjectCheckService(ProjectCheckService projectCheckService) {
		this.projectCheckService = projectCheckService;
	}

	public ProjectCheck getProjectCheck() {
		return projectCheck;
	}

	public void setProjectCheck(ProjectCheck projectCheck) {
		this.projectCheck = projectCheck;
	}


}
