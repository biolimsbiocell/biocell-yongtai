package com.biolims.system.projectcheck.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.projectcheck.dao.ProjectCheckDao;
import com.biolims.system.projectcheck.model.ProjectCheck;
import com.biolims.system.projectcheck.model.ProjectCheckItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ProjectCheckService {
	@Resource
	private ProjectCheckDao projectCheckDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findProjectCheckList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return projectCheckDao.selectProjectCheckList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ProjectCheck i) throws Exception {

		projectCheckDao.saveOrUpdate(i);

	}
	public ProjectCheck get(String id) {
		ProjectCheck projectCheck = commonDAO.get(ProjectCheck.class, id);
		return projectCheck;
	}
	public Map<String, Object> findProjectCheckItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = projectCheckDao.selectProjectCheckItemList(scId, startNum, limitNum, dir, sort);
		List<ProjectCheckItem> list = (List<ProjectCheckItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProjectCheckItem(ProjectCheck sc, String itemDataJson) throws Exception {
		List<ProjectCheckItem> saveItems = new ArrayList<ProjectCheckItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProjectCheckItem scp = new ProjectCheckItem();
			// 将map信息读入实体类
			scp = (ProjectCheckItem) projectCheckDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setProjectCheck(sc);

			saveItems.add(scp);
		}
		projectCheckDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProjectCheckItem(String[] ids) throws Exception {
		for (String id : ids) {
			ProjectCheckItem scp =  projectCheckDao.get(ProjectCheckItem.class, id);
			 projectCheckDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ProjectCheck sc, Map jsonMap) throws Exception {
		if (sc != null) {
			projectCheckDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("projectCheckItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProjectCheckItem(sc, jsonStr);
			}
	}
   }
}
