package com.biolims.system.express.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
/**   
 * @Title: Model
 * @Description: 快递公司
 * @author lims-platform
 * @date 2016-01-17 17:06:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_EXPRESS_COMPANY")
@SuppressWarnings("serial")
public class ExpressCompany extends EntityDao<ExpressCompany> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**电话*/
	private String phone;
	/**快递模板*/
	private FileInfo fileInfo;
	/**地址*/
	private String address;
	/**状态*/
	private String state;
	/**打印地址*/
	private String printPath;
	/**	范围ID*/
	private String scopeId;
	/**	范围*/
	private String scopeName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 60)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  电话
	 */
	@Column(name ="PHONE", length = 60)
	public String getPhone(){
		return this.phone;
	}
	/**
	 *方法: 设置String
	 *@param: String  电话
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 *方法: 取得FileInfo
	 *@return: FileInfo  快递模板
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILE_INFO")
	public FileInfo getFileInfo(){
		return this.fileInfo;
	}
	/**
	 *方法: 设置FileInfo
	 *@param: FileInfo  快递模板
	 */
	public void setFileInfo(FileInfo fileInfo){
		this.fileInfo = fileInfo;
	}
	/**
	 *方法: 取得String
	 *@return: String  地址
	 */
	@Column(name ="ADDRESS", length = 60)
	public String getAddress(){
		return this.address;
	}
	/**
	 *方法: 设置String
	 *@param: String  地址
	 */
	public void setAddress(String address){
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPrintPath() {
		return printPath;
	}
	public void setPrintPath(String printPath) {
		this.printPath = printPath;
	}
	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	public String getScopeName() {
		return scopeName;
	}
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}