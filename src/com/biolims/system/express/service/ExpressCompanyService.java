package com.biolims.system.express.service;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.express.dao.ExpressCompanyDao;
import com.biolims.system.express.model.ExpressCompany;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ExpressCompanyService {
	@Resource
	private ExpressCompanyDao expressCompanyDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findExpressCompanyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return expressCompanyDao.selectExpressCompanyList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ExpressCompany i) throws Exception {

		expressCompanyDao.saveOrUpdate(i);

	}
	public ExpressCompany get(String id) {
		ExpressCompany expressCompany = commonDAO.get(ExpressCompany.class, id);
		return expressCompany;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ExpressCompany sc, Map jsonMap) throws Exception {
		if (sc != null) {
			expressCompanyDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	public Map<String, Object> showDialogExpressCompanyJson(Integer start,
			Integer length, String query, String col, String sort) {
		return expressCompanyDao.showDialogExpressCompanyJson(start, length, query, col, sort);
	}
	public Map<String, Object> findExpressCompanyTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return expressCompanyDao.findExpressCompanyTable(start, length, query, col, sort);
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ExpressCompany sc, Map jsonMap,String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String)ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String)ActionContext.getContext().getSession().get("scopeName"));
			expressCompanyDao.saveOrUpdate(sc);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("ExpressCompany");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			String logStr = "";
	}
   }
}
