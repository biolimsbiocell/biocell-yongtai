package com.biolims.system.express.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.system.express.service.ExpressCompanyService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/system/express/expressCompany")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ExpressCompanyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9012";
	@Autowired
	private ExpressCompanyService expressCompanyService;
	private ExpressCompany expressCompany = new ExpressCompany();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
/*new*/
	@Resource
	private CommonService commonService;	
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Action(value = "showExpressCompanyList")
	public String showExpressCompanyList() throws Exception {
		rightsId = "9012";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/express/expressCompany.jsp");
	}

//	@Action(value = "showExpressCompanyEditList")
//	public String showExpressCompanyEditList() throws Exception {
//		rightsId = "9012";
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/system/express/expressCompanyEditList.jsp");
//	}
	@Action(value = "showExpressCompanyTableJson")
	public void showExpressCompanyTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = expressCompanyService.findExpressCompanyTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<ExpressCompany> list = (List<ExpressCompany>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("phone", "");
			map.put("address", "");
			map.put("state", "");
			map.put("printPath", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("ExpressCompany");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "expressCompanySelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogExpressCompanyList() throws Exception {
		rightsId = "9012";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/express/expressCompanySelectTable.jsp");
	}
	@Action(value="showDialogExpressCompanyJson")
	public void showDialogExpressCompanyJson(){
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = expressCompanyService.showDialogExpressCompanyJson(
					start, length, query, col, sort);
			List<SampleOrder> list=(List<SampleOrder>) result.get("list");
			Map<String, String> map=new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("phone", "");
			map.put("fileInfo-id", "");
			map.put("fileInfo-name", "");
			map.put("address", "");
			map.put("state", "");
			map.put("printPath", "");
			String data=new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result,data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "editExpressCompany")
	public String editExpressCompany() throws Exception {
		rightsId = "9012";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			expressCompany = expressCompanyService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "expressCompany");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			expressCompany.setId(SystemCode.DEFAULT_SYSTEMCODE);

			expressCompany.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			expressCompany.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/express/expressCompanyEdit.jsp");
	}
	@Action(value = "save")
	public void save() throws Exception {
		String msg = "";
		String zId = "";
		boolean bool = true;	
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				expressCompany = (ExpressCompany)commonDAO.Map2Bean(map, expressCompany);
			}
			String id = expressCompany.getId();
			String log = "";
			if(id!=null&&id.equals("")){
				expressCompany.setId(null);
			}
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				log = "123";
				String modelName = "ExpressCompany";
				String markCode = "KDGS";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				expressCompany.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			expressCompanyService.save(expressCompany,aMap,changeLog,lMap,log);
			zId = expressCompany.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
/*	old*/
	/*@Action(value = "showExpressCompanyList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showExpressCompanyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/express/expressCompany.jsp");
	}*/

//	@Action(value = "showExpressCompanyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showExpressCompanyListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = expressCompanyService
//				.findExpressCompanyList(map2Query, startNum, limitNum, dir,
//						sort);
//		Long count = (Long) result.get("total");
//		List<ExpressCompany> list = (List<ExpressCompany>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("phone", "");
//		map.put("fileInfo-id", "");
//		map.put("fileInfo-name", "");
//		map.put("address", "");
//		map.put("state", "");
//		map.put("printPath", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
/*	@Action(value = "expressCompanySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogExpressCompanyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/express/expressCompanyDialogTable.jsp");
	}*/
	
//	@Action(value = "showDialogExpressCompanyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogExpressCompanyListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = expressCompanyService
//				.findExpressCompanyList(map2Query, startNum, limitNum, dir,
//						sort);
//		Long count = (Long) result.get("total");
//		List<ExpressCompany> list = (List<ExpressCompany>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("phone", "");
//		map.put("fileInfo-id", "");
//		map.put("fileInfo-name", "");
//		map.put("address", "");
//		map.put("state", "");
//		map.put("printPath", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

/*	@Action(value = "editExpressCompany", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editExpressCompany() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			expressCompany = expressCompanyService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "expressCompany");
		} else {
			expressCompany.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// expressCompany.setCreateUser(user);
			// expressCompany.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/express/expressCompanyEdit.jsp");
	}*/

/*	@Action(value = "copyExpressCompany")
	public String copyExpressCompany() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		expressCompany = expressCompanyService.get(id);
		expressCompany.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/express/expressCompanyEdit.jsp");
	}*/

	/*@Action(value = "save")
	public String save() throws Exception {
		String id = expressCompany.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "ExpressCompany";
			String markCode = "KDGS";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			expressCompany.setId(autoID);
		}
		Map aMap = new HashMap();
		expressCompanyService.save(expressCompany, aMap);
		return redirect("/system/express/expressCompany/editExpressCompany.action?id="
				+ expressCompany.getId());

	}*/

	@Action(value = "viewExpressCompany")
	public String toViewExpressCompany() throws Exception {
		String id = getParameterFromRequest("id");
		expressCompany = expressCompanyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/express/expressCompanyEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ExpressCompanyService getExpressCompanyService() {
		return expressCompanyService;
	}

	public void setExpressCompanyService(
			ExpressCompanyService expressCompanyService) {
		this.expressCompanyService = expressCompanyService;
	}

	public ExpressCompany getExpressCompany() {
		return expressCompany;
	}

	public void setExpressCompany(ExpressCompany expressCompany) {
		this.expressCompany = expressCompany;
	}
}
