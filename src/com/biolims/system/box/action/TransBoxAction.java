package com.biolims.system.box.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.system.box.model.TransBox;
import com.biolims.system.box.service.TransBoxService;
import com.biolims.system.quality.model.QualityProduct;
import com.biolims.system.quality.service.QualityProductService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/system/box/box")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TransBoxAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9016";
	@Autowired
	private TransBoxService transBoxService;
	private TransBox transBox = new TransBox();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showTransBoxList")
	public String showTransBoxList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/box/transBox.jsp");
	}

	@Action(value = "showTransBoxListJson")
	public void showTransBoxListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = transBoxService.findTransBoxList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<TransBox> list = (List<TransBox>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("boxSize", "");
		map.put("num", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "transBoxSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String transBoxSelect() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/box/transBoxDialog.jsp");
	}

	@Action(value = "showDialogTransBoxListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTransBoxListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = transBoxService.findTransBoxList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<TransBox> list = (List<TransBox>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("boxSize", "");
		map.put("num", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editTransBox")
	public String editTransBox() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			transBox = transBoxService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "transBox");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/box/transBoxEdit.jsp");
	}

	@Action(value = "copyTransBox")
	public String copyTransBox() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		transBox = transBoxService.get(id);
		transBox.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/box/transBoxEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = transBox.getId();

		if(id!=null && id.equals("")){
			transBox.setId(null);
		}
		Map aMap = new HashMap();
		transBoxService.save(transBox,aMap);
		return redirect("/system/box/box/editTransBox.action?id=" + transBox.getId());

	}

	@Action(value = "viewTransBox")
	public String toViewTransBox() throws Exception {
		String id = getParameterFromRequest("id");
		transBox = transBoxService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/box/transBoxEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TransBoxService getTransBoxService() {
		return transBoxService;
	}

	public void setTransBoxService(TransBoxService transBoxService) {
		this.transBoxService = transBoxService;
	}

	public TransBox getTransBox() {
		return transBox;
	}

	public void setTransBox(TransBox transBox) {
		this.transBox = transBox;
	}

}
