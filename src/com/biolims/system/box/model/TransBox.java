package com.biolims.system.box.model;

import java.lang.Integer;
import java.lang.String;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;

import javax.persistence.Table;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 运输箱主数据
 * @author lims-platform
 * @date 2015-11-03 18:13:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SYS_TRANS_BOX")
@SuppressWarnings("serial")
public class TransBox extends EntityDao<TransBox> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**规格*/
	private String boxSize;
	/**数量*/
	private Integer num;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	public String getBoxSize() {
		return boxSize;
	}
	public void setBoxSize(String boxSize) {
		this.boxSize = boxSize;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

}
