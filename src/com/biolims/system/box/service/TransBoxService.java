package com.biolims.system.box.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.box.dao.TransBoxDao;
import com.biolims.system.box.model.TransBox;
import com.biolims.system.organize.dao.ApplyOrganizeDao;
import com.biolims.system.organize.model.ApplyOrganize;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TransBoxService {
	@Resource
	private TransBoxDao transBoxDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findTransBoxList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return transBoxDao.selectTransBoxList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TransBox i) throws Exception {

		transBoxDao.saveOrUpdate(i);

	}
	public TransBox get(String id) {
		TransBox transBox = commonDAO.get(TransBox.class, id);
		return transBox;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TransBox sc, Map jsonMap) throws Exception {
		if (sc != null) {
			transBoxDao.saveOrUpdate(sc);
		
			String jsonStr = "";
		}
   }
	//加载子表
	public List<Map<String, String>> showTransBoxList() throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = transBoxDao.setTransBoxList();
		List<TransBox> list = (List<TransBox>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TransBox srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("boxSize", srai.getBoxSize());
				if(srai.getNum()!=null && !srai.getNum().equals("")){
					map.put("num", srai.getNum().toString());
				}else{
					map.put("num", "");
				}
				
				mapList.add(map);
			}
		}
		return mapList;
	}
}
