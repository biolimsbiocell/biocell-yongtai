package com.biolims.system.family.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.family.model.Family;
import com.biolims.system.family.model.FamilyItem;
import com.biolims.system.family.service.FamilyService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

import net.sf.json.JSONArray;

@Namespace("/system/family/family")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FamilyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9019";
	@Autowired
	private FamilyService familyService;
	private Family family = new Family();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showFamilyList")
	public String showFamilyList() throws Exception {
		rightsId ="901702";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/family/family.jsp");
	}

	@Action(value = "showFamilyListJson")
	public void showFamilyListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = familyService.findFamilyTableList(start, length, query, col, sort);
			List<Family> list = (List<Family>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("cancer-id", "");
			map.put("cancer-cancerTypeName", "");
			map.put("crmPatient-id", "");
			map.put("crmPatient-name", "");
			map.put("crmPatient-sampleCode", "");
			map.put("crmPatient-cancerType-id", "");
			map.put("crmPatient-cancerType-cancerTypeName", "");
			map.put("note", "");
			map.put("hospital-id", "");
			map.put("hospital-name", "");
			map.put("patientNo", "");
			map.put("patientName", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Family");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "familySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogFamilyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/family/familyDialog.jsp");
	}

	/*@Action(value = "showDialogFamilyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogFamilyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = familyService.findFamilyList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Family> list = (List<Family>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("cancer-id", "");
		map.put("cancer-name", "");
		map.put("note", "");
		map.put("hospital-id", "");
		map.put("hospital-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}*/

	@Action(value = "editFamily")
	public String editFamily() throws Exception {
		rightsId ="901701";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			family = familyService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "family");
		} else {
			family.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			family.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/family/familyEdit.jsp");
	}

	@Action(value = "copyFamily")
	public String copyFamily() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		family = familyService.get(id);
		family.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/family/familyEdit.jsp");
	}
	@Action(value = "findFamilyByPatientNo")
	public void findFamilyByPatientNo() throws Exception {
		String patientId = getParameterFromRequest("patientId");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			if(patientId!=null) {
				Family family = familyService.findFamilyByPatientNo(patientId);
				if(family!=null) {
					String id = family.getId();
					map.put("id",id);
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "save")
	public void save() throws Exception {
		String ids = getParameterFromRequest("ids");
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogs=new String(changeLog.toString().getBytes("ISO-8859-1"), "UTF-8");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			String changeLogItems=new String(changeLogItem.toString().getBytes("ISO-8859-1"), "UTF-8");
			String main=getParameterFromRequest("main");
			String itemJson=getParameterFromRequest("itemJson");
			Map<String, Object> map=new HashMap<String, Object>();
			HashMap aMap = new HashMap();
			aMap.put("familyItem", itemJson);
			try {
			String id=	familyService.save(main,aMap,ids,changeLogs,changeLogItems);
				map.put("id",id);
				map.put("success", true);
			} catch (Exception e) {
				map.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
			/*Map aMap = new HashMap();
			aMap.put("familyItem", getParameterFromRequest("familyItemJson"));
			
			familyService.save(family, aMap);
			return redirect("/system/family/family/editFamily.action?id="
					+ family.getId());*/

	}
	
	
	@Action(value = "viewFamily")
	public String viewFamily() throws Exception {
		String id = getParameterFromRequest("id");
		family = familyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/family/familyEdit.jsp");
	}
	@Action(value = "insertFamilyItemTable")
	public void insertFamilyItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		String itemJson=getParameterFromRequest("itemJson");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			familyService.insertFamilyItemTable(id,itemJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFamily", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFamily() throws Exception {
		String familyId = super.getRequest().getParameter("id");
		Map<String, Object> result = familyService.findFamilyItemList(familyId,
				null, null, null, null);
		Family familyTest = familyService.get(familyId);
		List<FamilyItem> list = (List<FamilyItem>) result.get("list");
		StringBuffer sb = new StringBuffer();
		String isDisease = "";
		StringBuffer s = new StringBuffer();

		List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
		for (int i = 0; i < list.size(); i++) {
			if (i != (list.size() - 1)) {
				if (list.get(i).getIsDisease() != null
						&& list.get(i).getIsDisease().equals("1")) {
					isDisease = list.get(i).getPatientNo();
				}
				sb.append("{");
				sb.append("key:" + list.get(i).getPatientNo() + ",");
				if("S".equals(list.get(i).getIsDead())){
					
					sb.append("n:\"\\n" + list.get(i).getPatientName() + "(" + list.get(i).getAge() + ")\",");
				}else{
					
					sb.append("n:\"\\n" + list.get(i).getPatientName() + "\",");
				}
				if (list.get(i).getMatherId() != null&&!"".equals(list.get(i).getMatherId())) {
					sb.append("m:" + list.get(i).getMatherId() + ",");
				}
				if (list.get(i).getFathrtId() != null&&!"".equals(list.get(i).getFathrtId())) {
					sb.append("f:" + list.get(i).getFathrtId() + ",");
				}
				if (list.get(i).getWife() != null&&!"".equals(list.get(i).getWife())) {
					sb.append("ux:" + list.get(i).getWife() + ",");
				}
				if (list.get(i).getHusband() != null&&!"".equals(list.get(i).getHusband())) {
					sb.append("vir:" + list.get(i).getHusband() + ",");
				}
				if (list.get(i).getIsDivorce() != null&&!"".equals(list.get(i).getIsDivorce())) {
					if (list.get(i).getIsDivorce().equals("1")) {
						sb.append("di:\"Y\",");
					} else {
						sb.append("di:\"N\",");
					}
				}
				if (list.get(i).getHaveChild() != null&&!"".equals(list.get(i).getHaveChild())) {
					sb.append("hc:\"" + list.get(i).getHaveChild() + "\",");
				}
				if (list.get(i).getIsTwins() != null&&!"".equals(list.get(i).getIsTwins())) {
					sb.append("tw:\"" + list.get(i).getIsTwins() + "\",");
				}
				if (list.get(i).getGroup() != null&&!"".equals(list.get(i).getGroup())) {
					if (!list.get(i).getGroup()
							.equals(list.get(i).getPatientNo())) {
						sb.append("group:\"" + list.get(i).getGroup() + "\",");
					} else {
						sb.append("isGroup:true,");
					}
				}
				if (list.get(i).getIsCustody() != null&&!"".equals(list.get(i).getIsCustody())) {
					if (list.get(i).getIsCustody().equals("1")) {
						sb.append("cus:true,");
					} else {
						sb.append("cus:false,");
					}
				}
				if (list.get(i).getIsDead() != null&&!"".equals(list.get(i).getIsDead())
						&& list.get(i).getIsDead().equals("S")) {
					sb.append("a:[");
					if ("是".equals(list.get(i).getLeftUp())) {
						sb.append(",\"A\"");
					}
					if ("是".equals(list.get(i).getRightUp())) {
						sb.append(",\"D\"");
					}
					if ("是".equals(list.get(i).getLeftDown())) {
						sb.append(",\"J\"");
					}
					if ("是".equals(list.get(i).getRightDown())) {
						sb.append(",\"G\"");
					}
					sb.append(",\"S\"");
					sb.append("],");
				} else {
					sb.append("a:[");
					if ("是".equals(list.get(i).getLeftUp())) {
						sb.append(",\"A\"");
					}
					if ("是".equals(list.get(i).getRightUp())) {
						sb.append(",\"D\"");
					}
					if ("是".equals(list.get(i).getLeftDown())) {
						sb.append(",\"J\"");
					}
					if ("是".equals(list.get(i).getRightDown())) {
						sb.append(",\"G\"");
					}
					sb.append("],");
				}
				if (!list.get(i).getGender().equals("-1")) {
					if (list.get(i).getGender().equals("1")) {
						if (list.get(i).getAge() != null&&!"".equals(list.get(i).getAge())
								&& Integer.parseInt(list.get(i).getAge()) >= 18) {
							sb.append("s:\"M\"");
						} else {
							sb.append("s:\"Boy\"");
						}
					} else {
						if (list.get(i).getAge() != null&&!"".equals(list.get(i).getAge())
								&& Integer.parseInt(list.get(i).getAge()) >= 18) {
							sb.append("s:\"F\"");
						} else {
							sb.append("s:\"Girl\"");
						}
					}
				} else {
					sb.append("s:\"U\"");
				}
				sb.append("},");
			} else {
				if (list.get(i).getIsDisease() != null&&!"".equals(list.get(i).getIsDisease())
						&& list.get(i).getIsDisease().equals("1")) {
					isDisease = list.get(i).getPatientNo();
				}
				sb.append("{");
				sb.append("key:" + list.get(i).getPatientNo() + ",");
				sb.append("n:\"\\n" + list.get(i).getPatientName() + "\",");
				if (list.get(i).getMatherId() != null&&!"".equals(list.get(i).getMatherId())) {
					sb.append("m:" + list.get(i).getMatherId() + ",");
				}
				if (list.get(i).getFathrtId() != null&&!"".equals(list.get(i).getFathrtId())) {
					sb.append("f:" + list.get(i).getFathrtId() + ",");
				}
				if (list.get(i).getWife() != null&&!"".equals(list.get(i).getWife())) {
					sb.append("ux:" + list.get(i).getWife() + ",");
				}
				if (list.get(i).getHusband() != null&&!"".equals(list.get(i).getHusband())) {
					sb.append("vir:" + list.get(i).getHusband() + ",");
				}
				if (list.get(i).getIsDivorce() != null&&!"".equals(list.get(i).getIsDivorce())) {
					if (list.get(i).getIsDivorce().equals("是")) {
						sb.append("di:\"Y\",");
					} else {
						sb.append("di:\"N\",");
					}
				}
				if (list.get(i).getHaveChild() != null&&!"".equals(list.get(i).getHaveChild())) {
					sb.append("hc:\"" + list.get(i).getHaveChild() + "\",");
				}
				if (list.get(i).getIsTwins() != null&&!"".equals(list.get(i).getIsTwins())) {
					sb.append("tw:\"" + list.get(i).getIsTwins() + "\",");
				}
				if (list.get(i).getGroup() != null&&!"".equals(list.get(i).getGroup())) {
					if (!list.get(i).getGroup()
							.equals(list.get(i).getPatientNo())) {
						sb.append("group:\"" + list.get(i).getGroup() + "\",");
					} else {
						sb.append("isGroup:true,");
					}
				}
				if (list.get(i).getIsCustody() != null&&!"".equals(list.get(i).getIsCustody())) {
					if (list.get(i).getIsCustody().equals("1")) {
						sb.append("cus:true,");
					} else {
						sb.append("cus:false,");
					}
				}
				if (list.get(i).getIsDead() != null&&!"".equals(list.get(i).getIsDead())
						&& list.get(i).getIsDead().equals("S")) {
					sb.append("a:[");

					if ("是".equals(list.get(i).getLeftUp())) {
						sb.append(",\"A\"");
					}
					if ("是".equals(list.get(i).getRightUp())) {
						sb.append(",\"D\"");
					}
					if ("是".equals(list.get(i).getLeftDown())) {
						sb.append(",\"J\"");
					}
					if ("是".equals(list.get(i).getRightDown())) {
						sb.append(",\"G\"");
					}
					sb.append(",\"S\"");
					sb.append("],");
				} else {
					sb.append("a:[");
					if ("是".equals(list.get(i).getLeftUp())) {
						sb.append(",\"A\"");
					}
					if ("是".equals(list.get(i).getRightUp())) {
						sb.append(",\"D\"");
					}
					if ("是".equals(list.get(i).getLeftDown())) {
						sb.append(",\"J\"");
					}
					if ("是".equals(list.get(i).getRightDown())) {
						sb.append(",\"G\"");
					}
					sb.append("],");
				}
				if (!list.get(i).getGender().equals("-1")) {
					if (list.get(i).getGender().equals("1")) {
						if (list.get(i).getAge() != null&&!"".equals(list.get(i).getAge())
								&& Integer.parseInt(list.get(i).getAge()) >= 18) {
							sb.append("s:\"M\"");
						} else {
							sb.append("s:\"Boy\"");
						}
					} else {
						if (list.get(i).getAge() != null&&!"".equals(list.get(i).getAge())
								&& Integer.parseInt(list.get(i).getAge()) >= 18) {
							sb.append("s:\"F\"");
						} else {
							sb.append("s:\"Girl\"");
						}
					}
				} else {
					sb.append("s:\"U\"");
				}
				sb.append("}");
			}
			if (true) {
				Map<String, String> itemMap = new HashMap<String, String>();
				itemMap.put("leftUp", list.get(i).getLeftUp());
				itemMap.put("rightUp", list.get(i).getRightUp());
				itemMap.put("leftDown", list.get(i).getLeftDown());
				itemMap.put("rightDown", list.get(i).getRightDown());
				itemMap.put("patientName", list.get(i).getPatientName());
				itemMap.put("gender", list.get(i).getGender());
				itemMap.put("cancer1", familyTest.getCancer1());
				itemMap.put("cancer2", familyTest.getCancer2());
				itemMap.put("cancer3", familyTest.getCancer3());
				itemMap.put("cancer4", familyTest.getCancer4());
				listMap.add(itemMap);
			}
		}
		System.out.println(listMap.size() + sb.toString());
		super.getRequest().setAttribute("ddds",
				JSONArray.fromObject(listMap).toString().trim());
		super.getRequest().setAttribute("sb", sb.toString());
		if (isDisease.equals("")) {
			isDisease = "-1";
		}
		super.getRequest().setAttribute("isDisease", isDisease.toString());
		return dispatcher("/WEB-INF/page/system/family/genogram.jsp");
	}

	@Action(value = "showFamilyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFamilyItemList() throws Exception {
		return dispatcher("/WEB-INF/page/system/family/familyItem.jsp");
	}

	@Action(value = "showFamilyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFamilyItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = familyService.findFamilyItemList(start, length, query, col, sort, id);
			Long total = (Long) result.get("total");
			List<FamilyItem> list = (List<FamilyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("patientNo", "");
			map.put("patientName", "");
			map.put("age", "");
			map.put("birthDate", "yyyy-MM-dd");
			map.put("fathrtId", "");
			map.put("matherId", "");
			map.put("gender", "");
			map.put("wife", "");
			map.put("husband", "");
			map.put("patientId", "");
			map.put("crmPatient-id", "");
			map.put("crmPatient-name", "");
			map.put("family-id", "");
			map.put("family-name", "");
			map.put("isDead", "");
			map.put("isDisease", "");
			map.put("isDivorce", "");
			map.put("isCustody", "");
			map.put("haveChild", "");
			map.put("isTwins", "");
			map.put("leftUp", "");
			map.put("leftDown", "");
			map.put("rightUp", "");
			map.put("rightDown", "");
			map.put("fileNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFamilyItem")
	public void delFamilyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			familyService.delFamilyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Description: 进入向导录入界面
	 * @return   
	 * String  
	 * @throws
	 * @author jianqiang Jia
	 * @date 2017-7-27 上午9:04:38
	 */
	@Action(value = "familyGuide")
	public String familyGuide(){
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("id", getParameterFromRequest("id"));
		try {
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispatcher("/WEB-INF/page/system/family/familyGuide.jsp");
	}
	
	@Action(value = "selectDiseaseValue")
	public void selectDiseaseValue() throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String familyId = getParameterFromRequest("id");
			Map<String, Object> result = familyService.findFamilyItemListByFamilyId(familyId);
			List<FamilyItem> list = (List<FamilyItem>) result.get("list");
			ArrayList<String> nameList = new ArrayList<String>();
			for (FamilyItem familyItem : list) {
				String isDisease = familyItem.getIsDisease();
				String name = familyItem.getPatientName();
				//说明有先症
				if(isDisease.equals("1")) {
					map.put("flag", true);
				}
				nameList.add(name);
			}
			map.put("nameList", nameList);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FamilyService getFamilyService() {
		return familyService;
	}

	public void setFamilyService(FamilyService familyService) {
		this.familyService = familyService;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

}
