package com.biolims.system.family.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.system.family.model.Family;
import com.biolims.system.family.model.FamilyItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class FamilyDao extends BaseHibernateDao {
	public Map<String, Object> findFamilyTableList(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Family where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Family where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Family> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
		/*
		 * String key = " "; String hql = " from Family where 1=1 "; if (mapForQuery !=
		 * null) key = map2where(mapForQuery); Long total = (Long)
		 * this.getSession().createQuery(" select count(*) " + hql +
		 * key).uniqueResult(); List<Family> list = new ArrayList<Family>(); if (total >
		 * 0) { if (dir != null && dir.length() > 0 && sort != null && sort.length() >
		 * 0) { if (sort.indexOf("-") != -1) { sort = sort.substring(0,
		 * sort.indexOf("-")); } key = key + " order by " + sort + " " + dir; } if
		 * (startNum == null || limitNum == null) { list =
		 * this.getSession().createQuery(hql + key).list(); } else { list =
		 * this.getSession().createQuery(hql +
		 * key).setFirstResult(startNum).setMaxResults(limitNum).list(); } } Map<String,
		 * Object> result = new HashMap<String, Object>(); result.put("total", total);
		 * result.put("list", list); return result;
		 */

	}

	public Map<String, Object> selectFamilyItemList(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FamilyItem where 1=1 and family.id='" + id + "'";
		String key = "";
		// if(query!=null&&!"".equals(query)){
		// key=map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from FamilyItem where 1=1 and family.id='" + id + "' ";
			
			 if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) { 
				 col = col.replace("-", "."); 
				 key += " order by " + col + " " + sort; 
				 }
			 
			List<FamilyItem> list = new ArrayList<FamilyItem>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Integer getColumMaxValue(String ziTable,String mainTableColum,String mainId,String targetColum) {
			String hql="from "+ziTable+" where 1=1 and "+mainTableColum+".id='"+mainId+"'";
			Integer maxNum = (Integer) this.getSession()
					.createQuery("select MAX("+targetColum+"+0) " + hql).uniqueResult();
			return maxNum;
	}
	public Map<String, Object> selectFamilyItemListChart(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from FamilyItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and family.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<FamilyItem> list = new ArrayList<FamilyItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				//key = key + " order by " + sort + " " + dir;
			} 
			//key = key + "order by cast (age as integer) desc" ;
			key = key + "order by patientNo ASC" ;
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getIsDead()==null){
				if(list.get(i).getBirthDate()!=null){
					java.util.Date date1=list.get(i).getBirthDate();
					java.util.Date date2=new java.util.Date();
					long day=(date2.getTime()-date1.getTime())/(24*60*60*1000)+1;
					String year=new java.text.DecimalFormat("#").format(day/365f);
					list.get(i).setAge(year);
				}
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> findFamilyItemListByFamilyId(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FamilyItem where 1=1 and family.id='" + id + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from FamilyItem where 1=1 and family.id='" + id + "' ";
			List<FamilyItem> list = new ArrayList<FamilyItem>();
			list = this.getSession().createQuery(hql + key).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public FamilyItem findfamilyByName(String name) {
		String hql = "from FamilyItem  where 1=1 and patientName='"
				+ name + "'";
		FamilyItem item= (FamilyItem) this.getSession().createQuery(hql).uniqueResult();
		return item;
	}

	public Family findFamilyByPatientNo(String patientId) {
		String hql = "from Family  where 1=1 and patientNo='"
				+ patientId + "'";
		Family family= (Family) this.getSession().createQuery(hql).uniqueResult();
		return family;
	}
}