package com.biolims.system.family.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.EntityDao;
import com.biolims.weichat.model.WeiChatCancerType;

/**
 * @Title: Model
 * @Description: 家系主数据
 * @author lims-platform
 * @date 2016-07-16 14:21:04
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FAMILY")
@SuppressWarnings("serial")
public class Family extends EntityDao<Family> implements java.io.Serializable {
	/** 家系编号 */
	private String id;
	private String name;
	/** 先症 */
	private WeiChatCancerType cancer;
	/** 备注 */
	private String note;
	/** 医院 */
	private CrmCustomer hospital;
	/** 疾病1 */
	private String cancer1;
	/** 疾病2 */
	private String cancer2;
	/** 疾病3 */
	private String cancer3;
	/** 疾病4 */
	private String cancer4;
	/** 电子病历号 */
	private String patientNo;
	/** 电子病历 */
	private CrmPatient crmPatient;
	/** 病人姓名 */
	private String patientName;
	/** 状态 */
	private String state;

	@Column(name = "CANCER_1", length = 255)
	public String getCancer1() {
		return this.cancer1;
	}

	public void setCancer1(String cancer1) {
		this.cancer1 = cancer1;
	}

	@Column(name = "CANCER_2", length = 255)
	public String getCancer2() {
		return this.cancer2;
	}

	public void setCancer2(String cancer2) {
		this.cancer2 = cancer2;
	}

	@Column(name = "CANCER_3", length = 255)
	public String getCancer3() {
		return this.cancer3;
	}

	public void setCancer3(String cancer3) {
		this.cancer3 = cancer3;
	}

	@Column(name = "CANCER_4", length = 255)
	public String getCancer4() {
		return this.cancer4;
	}

	public void setCancer4(String cancer4) {
		this.cancer4 = cancer4;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 家系编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 家系编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得WeiChatCancerType
	 * 
	 * @return: WeiChatCancerType 先症
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CANCER")
	public WeiChatCancerType getCancer() {
		return this.cancer;
	}

	/**
	 * 方法: 设置WeiChatCancerType
	 * 
	 * @param: WeiChatCancerType 先症
	 */
	public void setCancer(WeiChatCancerType cancer) {
		this.cancer = cancer;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "NAME", length = 255)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得CrmCustomer
	 * 
	 * @return: CrmCustomer 医院
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "HOSPITAL")
	public CrmCustomer getHospital() {
		return this.hospital;
	}

	/**
	 * 方法: 设置CrmCustomer
	 * 
	 * @param: CrmCustomer 医院
	 */
	public void setHospital(CrmCustomer hospital) {
		this.hospital = hospital;
	}
	
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 电子病历号
	 */
	@Column(name = "PATIENT_NO", length = 50)
	public String getPatientNo() {
		return this.patientNo;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 电子病历号
	 */
	public void setPatientNo(String patientNo) {
		this.patientNo = patientNo;
	}
	
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 病人姓名
	 */
	@Column(name = "PATIENT_NAME", length = 50)
	public String getPatientName() {
		return patientName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 病人姓名
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	public String getState() {
		return state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return crmPatient;
	}

	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}
	
}