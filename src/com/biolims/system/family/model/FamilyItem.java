package com.biolims.system.family.model;

import java.util.Date;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;

/**
 * @Title: Model
 * @Description: 家系明细
 * @author lims-platform
 * @date 2016-07-16 14:21:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FAMILY_ITEM")
@SuppressWarnings("serial")
public class FamilyItem extends EntityDao<FamilyItem> implements
		java.io.Serializable {
	/** 电子病历编号 */
	private String id;
	/** 父亲 */
	private String fathrtId;
	/** 母亲 */
	private String matherId;
	/** 性别 */
	private String gender;
	/** 患者年龄 */
	private String age;
	/** 状态 */
	private String status;
	/** 病案号 */
	private String patientId;
	/** 编号 */
	private String patientNo;
	/** 姓名 */
	private String patientName;

	/** 相关主表 */
	private Family family;
	/** 电子病历 */
	private CrmPatient crmPatient;
	/** 妻子 */
	private String wife;
	/** 丈夫 */
	private String husband;
	/** 是否死亡 */
	private String isDead;
	/** 出生日期 */
	private Date birthDate;
	/** 左上 */
	private String leftUp;
	/** 左下 */
	private String leftDown;
	/** 右上 */
	private String rightUp;
	/** 右下 */
	private String rightDown;
	/** 先症 */
	private String isDisease;
	/** 是否离婚 */
	private String isDivorce;
	/** 离婚后是否有孩子的抚养权 */
	private String isCustody;

	/** 是否有孩子 */
	private String haveChild;

	/** 是否双胞胎 */
	private String isTwins;

	/** 家系类型 */
	private String familyType;

	/** 生活圈 */
	private String groups;
	
	/** 关系关联人 */
	private String relateMan;
	
	/**附件*/
	private FileInfo fuData;
	
	/**文件数量*/
	private String fileNum;

	@Column(name = "GROUPS", length = 20)
	public String getGroup() {
		return groups;
	}

	public void setGroup(String groups) {
		this.groups = groups;
	}

	@Column(name = "IS_TWINS", length = 20)
	public String getIsTwins() {
		return isTwins;
	}

	public void setIsTwins(String isTwins) {
		this.isTwins = isTwins;
	}

	@Column(name = "IS_CUSTODY", length = 50)
	public String getIsCustody() {
		return isCustody;
	}

	public void setIsCustody(String isCustody) {
		this.isCustody = isCustody;
	}

	@Column(name = "HAVE_CHILD", length = 20)
	public String getHaveChild() {
		return haveChild;
	}

	public void setHaveChild(String haveChild) {
		this.haveChild = haveChild;
	}

	@Column(name = "IS_DIVORCE", length = 50)
	public String getIsDivorce() {
		return isDivorce;
	}

	public void setIsDivorce(String isDivorce) {
		this.isDivorce = isDivorce;
	}

	@Column(name = "IS_DISEASE", length = 50)
	public String getIsDisease() {
		return isDisease;
	}

	public void setIsDisease(String isDisease) {
		this.isDisease = isDisease;
	}

	@Column(name = "LEFT_UP", length = 50)
	public String getLeftUp() {
		return leftUp;
	}

	public void setLeftUp(String leftUp) {
		this.leftUp = leftUp;
	}

	@Column(name = "LEFT_DOWN", length = 50)
	public String getLeftDown() {
		return leftDown;
	}

	public void setLeftDown(String leftDown) {
		this.leftDown = leftDown;
	}

	@Column(name = "RIGHT_UP", length = 50)
	public String getRightUp() {
		return rightUp;
	}

	public void setRightUp(String rightUp) {
		this.rightUp = rightUp;
	}

	@Column(name = "RIGHT_DOWN", length = 50)
	public String getRightDown() {
		return rightDown;
	}

	public void setRightDown(String rightDown) {
		this.rightDown = rightDown;
	}

	@Column(name = "BIRTH_DATE", length = 50)
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "IS_DEAD", length = 50)
	public String getIsDead() {
		return isDead;
	}

	public void setIsDead(String isDead) {
		this.isDead = isDead;
	}

	@Column(name = "PATIENT_NAME", length = 50)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "WIFE", length = 50)
	public String getWife() {
		return wife;
	}

	public void setWife(String wife) {
		this.wife = wife;
	}

	@Column(name = "HUSBAND", length = 50)
	public String getHusband() {
		return husband;
	}

	public void setHusband(String husband) {
		this.husband = husband;
	}

	/**
	 * 方法: 取得CrmPatient
	 * 
	 * @return: CrmPatient 电子病历
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	/**
	 * 方法: 设置CrmPatient
	 * 
	 * @param: CrmPatient 电子病历
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 电子病历编号
	 */

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 电子病历编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 父亲
	 */
	@Column(name = "FATHRT_ID", length = 50)
	public String getFathrtId() {
		return this.fathrtId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 父亲
	 */
	public void setFathrtId(String fathrtId) {
		this.fathrtId = fathrtId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 母亲
	 */
	@Column(name = "MATHER_ID", length = 50)
	public String getMatherId() {
		return this.matherId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 母亲
	 */
	public void setMatherId(String matherId) {
		this.matherId = matherId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 性别
	 */
	@Column(name = "GENDER", length = 50)
	public String getGender() {
		return this.gender;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 性别
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 患者年龄
	 */
	@Column(name = "AGE", length = 50)
	public String getAge() {
		return this.age;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 患者年龄
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATUS", length = 50)
	public String getStatus() {
		return this.status;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 病案号
	 */
	@Column(name = "PATIENT_ID", length = 50)
	public String getPatientId() {
		return this.patientId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 病案号
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	@Column(name = "PATIENT_NO", length = 50)
	public String getPatientNo() {
		return this.patientNo;
	}

	public void setPatientNo(String patientNo) {
		this.patientNo = patientNo;
	}

	/**
	 * 方法: 取得Family
	 * 
	 * @return: Family 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FAMILY")
	public Family getFamily() {
		return this.family;
	}

	/**
	 * 方法: 设置Family
	 * 
	 * @param: Family 相关主表
	 */
	public void setFamily(Family family) {
		this.family = family;
	}

	public String getFamilyType() {
		return familyType;
	}

	public void setFamilyType(String familyType) {
		this.familyType = familyType;
	}

	public String getRelateMan() {
		return relateMan;
	}

	public void setRelateMan(String relateMan) {
		this.relateMan = relateMan;
	}
	
	/**
	 *方法: 取得FileInfo
	 *@return: FileInfo  附件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FU_DATA")
	public FileInfo getFuData(){
		return this.fuData;
	}
	/**
	 *方法: 设置FileInfo
	 *@param: FileInfo  附件
	 */
	public void setFuData(FileInfo fuData){
		this.fuData = fuData;
	}
	
	@Column(name ="FILE_NUM", length = 50)
	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
}