package com.biolims.system.family.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sun.security.action.GetBooleanAction;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.file.service.FileInfoService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.DicSampleTypeItem;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.system.family.dao.FamilyDao;
import com.biolims.system.family.model.Family;
import com.biolims.system.family.model.FamilyItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FamilyService {
	@Resource
	private FamilyDao familyDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findFamilyTableList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return familyDao.findFamilyTableList(start, length, query, col,
				sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Family i) throws Exception {

		familyDao.saveOrUpdate(i);

	}
	public Family get(String id) {
		Family family = commonDAO.get(Family.class, id);
		return family;
	}
	public Map<String, Object> findFamilyItemList(Integer start,
			Integer length, String query, String col, String sort, String id) throws Exception {
		Map<String, Object> result = familyDao.selectFamilyItemList(start, length, query,
				col, sort, id);
		List<FamilyItem> list = (List<FamilyItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findFamilyItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = familyDao.selectFamilyItemListChart(scId,
				startNum, limitNum, dir, sort);
		List<FamilyItem> list = (List<FamilyItem>) result.get("list");
		for (FamilyItem fi : list) {
			long num = fileInfoService.findFileInfoCount(fi.getId(),
					"familyItem");
			fi.setFileNum(String.valueOf(num));
		}
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFamilyItem(Family sc, String itemDataJson,String changeLogItem) throws Exception {
		List<FamilyItem> saveItems = new ArrayList<FamilyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			Integer maxNum = familyDao.getColumMaxValue("FamilyItem", "family", sc.getId(), "patientNo");
			Long num=maxNum==null?1l:maxNum+1l;
			FamilyItem scp = new FamilyItem();
			// 将map信息读入实体类
			scp = (FamilyItem) familyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
				scp.setPatientNo(String.valueOf(num));
				num++;
			}
			scp.setFamily(sc);
			
			saveItems.add(scp);
			commonDAO.saveOrUpdate(scp);
		}
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setModifyContent(changeLogItem);
			commonDAO.saveOrUpdate(li);
		}
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFamilyItem(String[] ids) throws Exception {
		for (String id : ids) {
			FamilyItem scp =  familyDao.get(FamilyItem.class, id);
			 familyDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String main, Map itemJson,
			String ids, String changeLog, String changeLogItem) throws Exception {
		String id = null;
		if (main != null) {
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				if(u!=null) {
					li.setUserId(u.getId());
				}
				li.setFileId(ids);
				li.setModifyContent(changeLog);
				commonDAO.saveOrUpdate(li);
			}
			//主表数据
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			Family sr = new Family();
			sr = (Family) commonDAO.Map2Bean(list.get(0), sr);
			if ((sr.getId() != null && "".equals(sr.getId()))
					|| sr.getId().equals("NEW")) {
				String modelName = "Family";
				String markCode = "FA";
				String autoID = codingRuleService.genTransID(modelName,
						markCode);
				id = autoID;
				sr.setId(autoID);
				commonDAO.saveOrUpdate(sr);
				if(sr.getPatientNo()!=null&&!"".equals(sr.getPatientNo())) {
					CrmPatient crmPatient = commonDAO.get(CrmPatient.class, sr.getPatientNo());
					if(crmPatient!=null) {
						crmPatient.setFamilyId(sr);
					}
				}
			} else {
				id = sr.getId();
				commonDAO.saveOrUpdate(sr);
				if(sr.getPatientNo()!=null&&!"".equals(sr.getPatientNo())) {
					CrmPatient crmPatient = commonDAO.get(CrmPatient.class, sr.getPatientNo());
					if(crmPatient!=null) {
						crmPatient.setFamilyId(sr);
					}
				}
			}
			if (sr != null) {
				String jsonStr = "";
				jsonStr = (String)itemJson.get("familyItem");
				if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
					saveFamilyItem(sr, jsonStr,changeLogItem);
				}
		}
	}
			return id;
   }
	public Map<String, Object> findFamilyItemListByFamilyId(String familyId) {
		Map<String, Object> result = familyDao.findFamilyItemListByFamilyId(familyId);
		List<FamilyItem> list = (List<FamilyItem>) result.get("list");
		return result;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertFamilyItemTable(String id, String itemJson) throws Exception {
		String datajson="["+itemJson+"]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(datajson, List.class);
		Integer maxNum = familyDao.getColumMaxValue("FamilyItem", "family", id, "patientNo");
		Long num=maxNum==null?1l:maxNum+1l;
		FamilyItem sr = new FamilyItem();
		sr = (FamilyItem) commonDAO.Map2Bean(list.get(0), sr);
		Family family = commonDAO.get(Family.class, id);
		//关联人
		String patientNo=null;
		String itemId =null;
		if(sr.getRelateMan()!=null&&!"".equals(sr.getRelateMan())) {
			FamilyItem item = familyDao.findfamilyByName(sr.getRelateMan());
			itemId = item.getId();
			patientNo = item.getPatientNo();
		}
		if(family!=null) {
			sr.setFamily(family);
			sr.setPatientNo(String.valueOf(num));
		}
		commonDAO.saveOrUpdate(sr);
		if(itemId!=null&!"".equals(itemId)) {
			FamilyItem familyItem = commonDAO.get(FamilyItem.class, itemId);
			if(sr.getFamilyType()!=null&&!"".equals(sr.getFamilyType())) {
				if(sr.getFamilyType().equals("-3")) {//wife
					sr.setHusband(familyItem.getPatientNo());
					familyItem.setWife(sr.getPatientNo());
				}
				if(sr.getFamilyType().equals("3")) {//husband
					sr.setWife(familyItem.getPatientNo());
					familyItem.setHusband(sr.getPatientNo());
				}
				if(sr.getFamilyType().equals("-1")) {//mother
					familyItem.setMatherId(sr.getPatientNo());
					if(sr.getHusband()!=null&&!"".equals(sr.getHusband())) {
						familyItem.setFathrtId(sr.getHusband());
					}
				}
				if(sr.getFamilyType().equals("1")) {//father
					familyItem.setFathrtId(sr.getPatientNo());
					if(sr.getWife()!=null&&!"".equals(sr.getWife())) {
						familyItem.setMatherId(sr.getWife());
					}
				}
			}
			commonDAO.saveOrUpdate(familyItem);
		}
		
	}
	public Family findFamilyByPatientNo(String patientId) {
		return familyDao.findFamilyByPatientNo(patientId);
	}
}
