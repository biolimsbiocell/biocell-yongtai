﻿
package com.biolims.system.detecyion.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.detecyion.service.SampleDeteyionService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.common.PushData;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.common.PushData;
import com.biolims.common.service.CommonService;
import com.biolims.system.detecyion.model.*;
import net.sf.json.JSONObject;
import com.biolims.common.dao.CommonDAO;
import com.biolims.core.model.user.UserGroupUser;
import com.opensymphony.xwork2.ActionContext;


@Namespace("/system/detecyion/sampleDeteyion")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleDeteyionAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "11991";
	@Autowired
	private SampleDeteyionService sampleDeteyionService;
	private SampleDeteyion sampleDeteyion = new SampleDeteyion();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;	
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	private String log="";
	
	/**
	 * @throws Exception 
		 *	跟据主表差子表list
	     * @Title: fingItemListBySampleDeteyion  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年9月10日
	     * @throws
	 */
	@Action(value = "fingItemListBySampleDeteyion")
	public void fingItemListBySampleDeteyion() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String ,Object>map = new HashMap<String, Object>();
		try {
			List<SampleDeteyionItem> list = sampleDeteyionService.fingItemListBySampleDeteyion(id);
			map.put("success", true);
			map.put("list", list);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		
		HttpUtils.write(JsonUtils.toJsonString(map));
		
	}
	
	
	@Action(value = "showSampleDeteyionList")
	public String showSampleDeteyionList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/detecyion/sampleDeteyion.jsp");
	}

	@Action(value = "showSampleDeteyionEditList")
	public String showSampleDeteyionEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/detecyion/sampleDeteyionEditList.jsp");
	}


	@Action(value = "showSampleDeteyionTableJson")
	public void showSampleDeteyionTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = sampleDeteyionService.findSampleDeteyionTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<SampleDeteyion> list = (List<SampleDeteyion>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("SampleDeteyion");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "sampleDeteyionSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleDeteyionList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/detecyion/sampleDeteyionSelectTable.jsp");
	}

	@Action(value = "editSampleDeteyion")
	public String editSampleDeteyion() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleDeteyion = sampleDeteyionService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleDeteyion");
		} else {
			sampleDeteyion.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleDeteyion.setCreateUser(user);
			sampleDeteyion.setCreateDate(new Date());
			sampleDeteyion.setState(SystemConstants.DIC_STATE_NEW);
			sampleDeteyion.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			sampleDeteyion.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sampleDeteyion.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/detecyion/sampleDeteyionEdit.jsp");
	}

	@Action(value = "copySampleDeteyion")
	public String copySampleDeteyion() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleDeteyion = sampleDeteyionService.get(id);
		sampleDeteyion.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/detecyion/sampleDeteyionEdit.jsp");
	}


	@Action(value = "save")
	public void save() throws Exception {
		
		
		
		String msg = "";
		String zId = "";
		boolean bool = true;	
		
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				sampleDeteyion = (SampleDeteyion)commonDAO.Map2Bean(map, sampleDeteyion);
			}
			String id = sampleDeteyion.getId();
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				log = "123";
				String modelName = "SampleDeteyion";
				String markCode = "SD";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				sampleDeteyion.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("sampleDeteyionItem",getParameterFromRequest("sampleDeteyionItemJson"));
			
			
			sampleDeteyionService.save(sampleDeteyion,aMap,changeLog,lMap,log);
			
			zId = sampleDeteyion.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	

	@Action(value = "viewSampleDeteyion")
	public String toViewSampleDeteyion() throws Exception {
		String id = getParameterFromRequest("id");
		sampleDeteyion = sampleDeteyionService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/detecyion/sampleDeteyionEdit.jsp");
	}
	


	@Action(value = "showSampleDeteyionItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleDeteyionItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleDeteyionService
				.findSampleDeteyionItemTable(start, length, query, col, sort,
						id);
		List<SampleDeteyionItem> list=(List<SampleDeteyionItem>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("nextFlow-id", "");
			map.put("nextFlow-name", "");
			/*map.put("sampleDeteyion-name", "");
			map.put("sampleDeteyion-id", "");*/
			
			map.put("testingCriteria", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleDeteyionItem")
	public void delSampleDeteyionItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleDeteyionService.delSampleDeteyionItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "saveSampleDeteyionItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleDeteyionItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		sampleDeteyion=commonService.get(SampleDeteyion.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("sampleDeteyionItem",
				getParameterFromRequest("dataJson"));
		lMap.put("sampleDeteyionItem",
				getParameterFromRequest("changeLog"));
		try {
			sampleDeteyionService.save(sampleDeteyion, aMap,"",lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: 选择检测项  
	 * @Description: TODO  
	 * @author : nan.jiang
	 * @date 2018-8-31上午9:42:53
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "showSampledetecyion", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampledetecyion() throws Exception {
		
		return dispatcher("/WEB-INF/page/system/detecyion/selectSampleDeteyionTable.jsp");
	}

	@Action(value = "showSampledetecyionJson")
	public void showSampledetecyionJson() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleDeteyionService.showSampledetecyionJson(start,
				length, query, col, sort, groupId);
		List<SampleDeteyion> list = (List<SampleDeteyion>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "saveSampleDeteyionTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleDeteyionTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		
		String str = "["+dataValue+"]";
			
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			
		for (Map<String, Object> map1 : list) {
		
			SampleDeteyion a = new SampleDeteyion();
		
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (SampleDeteyion)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			sampleDeteyionService.save(a,aMap,changeLog,lMap,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleDeteyionService getSampleDeteyionService() {
		return sampleDeteyionService;
	}

	public void setSampleDeteyionService(SampleDeteyionService sampleDeteyionService) {
		this.sampleDeteyionService = sampleDeteyionService;
	}

	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}

	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}


}
