package com.biolims.system.detecyion.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.system.detecyion.model.*;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SampleDeteyionDao extends BaseHibernateDao {

	
		public Map<String, Object> findSampleDeteyionTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from  SampleDeteyion where 1=1";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from SampleDeteyion where 1=1";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<SampleDeteyion> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}

	public Map<String, Object> findSampleDeteyionItemTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from SampleDeteyionItem where 1=1 and sampleDeteyion.id='"+id+"'";
			String key = "";

			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from SampleDeteyionItem where 1=1 and sampleDeteyion.id='"+id+"' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<SampleDeteyionItem> list = new ArrayList<SampleDeteyionItem>();
				list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}

	/**
	 * @throws Exception   
	 * @Title: showSampledetecyionJson  
	 * @Description: TODO  
	 * @author : nan.jiang
	 * @date 2018-8-31上午9:52:40
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @param groupId
	 * @return
	 * Map<String,Object>
	 * @throws  
	 */ 
	public Map<String, Object> showSampledetecyionJson(Integer start,
			Integer length, String query, String col, String sort,
			String groupId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		String countHql = "select count(*) from  SampleDeteyion where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SampleDeteyion where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SampleDeteyion> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleDeteyionItem> fingItemListBySampleDeteyion(String id) {
		List<SampleDeteyionItem> list = new ArrayList<SampleDeteyionItem>();
		String hql = "from SampleDeteyionItem where 1=1 and sampleDeteyion.id = '" +id+"'";
		list= this.getSession().createQuery(hql).list();
		return list;
	}
}