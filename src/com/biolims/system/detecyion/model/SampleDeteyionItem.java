package com.biolims.system.detecyion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.nextFlow.model.NextFlow;

/**   
 * @Title: Model
 * @Description: 检测项明细
 * @author lims-platform
 * @date 2018-08-30 14:10:59
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_DETECYION_ITEM")
@SuppressWarnings("serial")
public class SampleDeteyionItem extends EntityDao<SampleDeteyionItem> implements java.io.Serializable {
	/**任务单*/
	private String id;
	/**名称*/
	private String name;
	/**备注*/
	private String note;
	/**相关主表*/
	private SampleDeteyion sampleDeteyion;
	/**质检类型*/
	private NextFlow nextFlow;
	/** 检测标准 */
	private String testingCriteria;
	
	
	
	public String getTestingCriteria() {
		return testingCriteria;
	}
	public void setTestingCriteria(String testingCriteria) {
		this.testingCriteria = testingCriteria;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "NEXT_FLOW")
	public NextFlow getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(NextFlow nextFlow) {
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  任务单
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  任务单
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 250)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 250)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETECYION")
	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}
	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}
	
	
}


/*


//中文JS配置文件
biolims.sampleDeteyionItem={};	
biolims.sampleDetecyionItem.id="任务单";
biolims.sampleDetecyionItem.name="名称";
biolims.sampleDetecyionItem.note="备注";
biolims.sampleDetecyionItem.sampleDeteyion="相关主表";




//英文JS配置文件
biolims.sampleDetecyionItem.id="id";
biolims.sampleDetecyionItem.name="name";
biolims.sampleDetecyionItem.note="note";
biolims.sampleDetecyionItem.sampleDeteyion="sampleDeteyion";


//中文配置文件
biolims.sampleDetecyionItem.id=任务单
biolims.sampleDetecyionItem.name=名称
biolims.sampleDetecyionItem.note=备注
biolims.sampleDetecyionItem.sampleDeteyion=相关主表


//英文配置文件
biolims.sampleDetecyionItem.id=id
biolims.sampleDetecyionItem.name=name
biolims.sampleDetecyionItem.note=note
biolims.sampleDetecyionItem.sampleDeteyion=sampleDeteyion

*/