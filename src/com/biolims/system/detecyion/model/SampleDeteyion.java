package com.biolims.system.detecyion.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.system.detecyion.model.*;
import com.biolims.system.template.model.Template;

/**
 * @Title: Model
 * @Description: 检测项
 * @author lims-platform
 * @date 2018-08-30 14:11:04
 * @version V1.0
 *
 */
@Entity
@Table(name = "SAMPLE_DETECYION")
@SuppressWarnings("serial")
public class SampleDeteyion extends EntityDao<SampleDeteyion> implements java.io.Serializable {
	/** 主表 */
	private String id;
	/** 描述 */
	private String name;
	/** 操作人 */
	private User createUser;
	/** 操作时间 */
	private Date createDate;
	/** 状态 */
	private String state;
	/** 成本中心id */
	private String scopeId;
	/** 成本中心 */
	private String scopeName;
	/**
	 * 状态名称
	 */
	private String stateName;
	/** 模板 */
	private Template template;
	/** 检测项目编号 */
	private String deteyionName;
	/** 是否推送生产 */
	private String yesOrNo;
	/** 是否提醒 */
	private String yesOrNo2;
	/**提醒日期*/
	private String remindDate;
	
	

	

	public String getYesOrNo2() {
		return yesOrNo2;
	}

	public void setYesOrNo2(String yesOrNo2) {
		this.yesOrNo2 = yesOrNo2;
	}

	public String getRemindDate() {
		return remindDate;
	}

	public void setRemindDate(String remindDate) {
		this.remindDate = remindDate;
	}

	public String getYesOrNo() {
		return yesOrNo;
	}

	public void setYesOrNo(String yesOrNo) {
		this.yesOrNo = yesOrNo;
	}

	public String getDeteyionName() {
		return deteyionName;
	}

	public void setDeteyionName(String deteyionName) {
		this.deteyionName = deteyionName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 主表
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 主表
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 250)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 操作人
	 */

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 操作时间
	 */
	@Column(name = "CREATE_DATE", length = 250)
	public Date getCreateDate() {
		return this.createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public String getStateName() {
		return stateName;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 操作时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 250)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 成本中心id
	 */
	@Column(name = "SCOPE_ID", length = 250)
	public String getScopeId() {
		return this.scopeId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 成本中心id
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 成本中心
	 */
	@Column(name = "SCOPE_NAME", length = 250)
	public String getScopeName() {
		return this.scopeName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 成本中心
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * @return the template
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "template")
	public Template getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

}

/*
 * 
 * 
 * //中文JS配置文件 biolims.sampleDeteyion={}; biolims.sampleDetecyion.id="主表";
 * biolims.sampleDetecyion.name="描述"; biolims.sampleDetecyion.createUser="操作人";
 * biolims.sampleDetecyion.createDate="操作时间";
 * biolims.sampleDetecyion.state="状态"; biolims.sampleDetecyion.scopeId="成本中心id";
 * biolims.sampleDetecyion.scopeName="成本中心";
 * 
 * 
 * 
 * 
 * //英文JS配置文件 biolims.sampleDetecyion.id="id";
 * biolims.sampleDetecyion.name="name";
 * biolims.sampleDetecyion.createUser="createUser";
 * biolims.sampleDetecyion.createDate="createDate";
 * biolims.sampleDetecyion.state="state";
 * biolims.sampleDetecyion.scopeId="scopeId";
 * biolims.sampleDetecyion.scopeName="scopeName";
 * 
 * 
 * //中文配置文件 biolims.sampleDetecyion.id=主表 biolims.sampleDetecyion.name=描述
 * biolims.sampleDetecyion.createUser=操作人
 * biolims.sampleDetecyion.createDate=操作时间 biolims.sampleDetecyion.state=状态
 * biolims.sampleDetecyion.scopeId=成本中心id biolims.sampleDetecyion.scopeName=成本中心
 * 
 * 
 * //英文配置文件 biolims.sampleDetecyion.id=id biolims.sampleDetecyion.name=name
 * biolims.sampleDetecyion.createUser=createUser
 * biolims.sampleDetecyion.createDate=createDate
 * biolims.sampleDetecyion.state=state biolims.sampleDetecyion.scopeId=scopeId
 * biolims.sampleDetecyion.scopeName=scopeName
 * 
 */