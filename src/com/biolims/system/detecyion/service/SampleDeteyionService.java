package com.biolims.system.detecyion.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.detecyion.dao.SampleDeteyionDao;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.biolims.system.detecyion.model.*;
import org.apache.struts2.ServletActionContext;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.log.model.LogInfo;



@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleDeteyionService {
	@Resource
	private SampleDeteyionDao sampleDeteyionDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	
	
	public Map<String, Object> findSampleDeteyionTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return sampleDeteyionDao.findSampleDeteyionTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleDeteyion i) throws Exception {

		sampleDeteyionDao.saveOrUpdate(i);

	}
	
	public SampleDeteyion get(String id) {
		SampleDeteyion sampleDeteyion = commonDAO.get(SampleDeteyion.class, id);
		return sampleDeteyion;
	}
	
	public Map<String, Object> findSampleDeteyionItemTable(Integer start,
			Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = sampleDeteyionDao.findSampleDeteyionItemTable(start, length, query,
				col, sort, id);
		List<SampleDeteyionItem> list = (List<SampleDeteyionItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleDeteyionItem(SampleDeteyion sc, String itemDataJson,String logInfo,String log) throws Exception {
		List<SampleDeteyionItem> saveItems = new ArrayList<SampleDeteyionItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleDeteyionItem scp = new SampleDeteyionItem();
			// 将map信息读入实体类
			scp = (SampleDeteyionItem) sampleDeteyionDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleDeteyion(sc);

			saveItems.add(scp);
		}
		sampleDeteyionDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("SampleDeteyion");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleDeteyionItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleDeteyionItem scp =  sampleDeteyionDao.get(SampleDeteyionItem.class, id);
			sampleDeteyionDao.delete(scp);
		 	if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getSampleDeteyion().getId());
				li.setClassName("SampleDeteyion");
				li.setModifyContent("检测项:"+"名称:"+scp.getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleDeteyion sc, Map jsonMap,String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			sampleDeteyionDao.saveOrUpdate(sc);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("SampleDeteyion");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			String logStr = "";
           	logStr =  (String)logMap.get("sampleDeteyionItem");
			jsonStr = (String)jsonMap.get("sampleDeteyionItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleDeteyionItem(sc, jsonStr,logStr,log);
			}
	}
   }

	/**
	 * @throws Exception   
	 * @Title: showSampledetecyionJson  
	 * @Description: TODO  
	 * @author : nan.jiang
	 * @date 2018-8-31上午9:50:23
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @param groupId
	 * @return
	 * Map<String,Object>
	 * @throws  
	 */ 
	public Map<String, Object> showSampledetecyionJson(Integer start,
			Integer length, String query, String col, String sort,
			String groupId) throws Exception {
		return sampleDeteyionDao.showSampledetecyionJson(start, length, query, col, sort,groupId);
	}

	public List<SampleDeteyionItem> fingItemListBySampleDeteyion(String id) {
		return sampleDeteyionDao.fingItemListBySampleDeteyion(id);
	}
}
