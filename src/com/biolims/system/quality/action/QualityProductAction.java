﻿
package com.biolims.system.quality.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.supplier.common.constants.SystemCode;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.quality.model.QualityProduct;
import com.biolims.system.quality.service.QualityProductService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/system/quality/qualityProduct")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityProductAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "901001";
	@Autowired
	private QualityProductService qualityProductService;
	private QualityProduct qualityProduct = new QualityProduct();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	
	@Resource
	private FieldService fieldService;
	/**
	 * 查询质控品的列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQualityProductList")
	public String showQualityProductList() throws Exception {
		rightsId="901001";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/quality/qualityProduct.jsp");
	}

	@Action(value = "showQualityProductListJson")
	public void showQualityProductListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = qualityProductService.findQualityProductList(start, length, query, col, sort);
			List<QualityProduct> list = (List<QualityProduct>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("experType-id", "");
			map.put("experType-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("expectValue", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("fieldContent", "");
			//根据模块查询自定义字段数据
			String data=new SendData().getDateJsonForDatatable(map, list);
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("QualityProduct");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "qualityProductSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQualityProductList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/quality/qualityProductDialog.jsp");
	}

	@Action(value = "qualityProductSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void qualityProductSelectJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = qualityProductService.qualityProductSelectJson(start, length, query, col, sort);
			List<QualityProduct> list = (List<QualityProduct>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			/*map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("experType-id", "");
			map.put("experType-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("expectValue", "");*/
			map.put("state", "");
//			map.put("stateName", "");
//			map.put("fieldContent", "");
			//根据模块查询自定义字段数据
			String data=new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result,data));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			}



	@Action(value = "editQualityProduct")
	public String editQualityProduct() throws Exception {
		rightsId="901002";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			qualityProduct = qualityProductService.get(id);
			putObjToContext("qualityProductId", id);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);

			num = fileInfoService.findFileInfoCount(id, "qualityProduct");

		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qualityProduct.setId(SystemCode.SUPPLIER_SYSTEMCODE);
			qualityProduct.setCreateUser(user);

			qualityProduct.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/quality/qualityProductEdit.jsp");
	}

	@Action(value = "copyQualityProduct")
	public String copyQualityProduct() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qualityProduct = qualityProductService.get(id);
		qualityProduct.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/quality/qualityProductEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = this.qualityProduct.getId();
		String log="";
		if (id != null && "NEW".equals(id)) {
			log="123";
			String modelName = "QualityProduct";
			String markCode = "Q";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qualityProduct.setId(autoID);
		} 
		String changeLog = getParameterFromRequest("changeLog");
		//changeLog=new String(changeLog.getBytes("ISO-8859-1"),"utf-8");
		qualityProductService.save(qualityProduct,changeLog,log);
		return redirect("/system/quality/qualityProduct/editQualityProduct.action?id="
				+ qualityProduct.getId());

	}

	@Action(value = "viewQualityProduct")
	public String toViewQualityProduct() throws Exception {
		String id = getParameterFromRequest("id");
		qualityProduct = qualityProductService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/quality/qualityProductEdit.jsp");
	}
	
	@Action(value = "showSelectQualityList")
	public String showSelectQualityList() throws Exception {
		return dispatcher("/WEB-INF/page/system/quality/selectQuality.jsp");
	}

	@Action(value = "showSelectQualityListJson")
	public void showSelectQualityListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		Map<String, Object> result = qualityProductService.selectQualityProductList(startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QualityProduct> list = (List<QualityProduct>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("experType-id", "");
		map.put("experType-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("expectValue", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	//根据模版加载质控品明细
	@Action(value = "setQcItem")
	public void setQcItem() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.qualityProductService.setQcItem(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//根据条件查找质控品
	@Action(value = "selectCustomer")
	public void selectCustomer() throws Exception {
		String id = getRequest().getParameter("id");
		String experType = getRequest().getParameter("experType");
		String startDate = getRequest().getParameter("startDate");
		String endDate = getRequest().getParameter("endDate");
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> idList = new ArrayList<String>();
		List<BigDecimal> valueList = new ArrayList<BigDecimal>();
		try {
			List<Map<String, Object>> dataListMap = this.qualityProductService.selectCustomer(id,experType,startDate,endDate);
			if(dataListMap!=null &&dataListMap.size()>0){
				for(Map<String, Object> map : dataListMap){
					idList.add((String)map.get("mid"));
					BigDecimal b =(BigDecimal)map.get("nd");
					valueList.add(b);
				}
				result.put("success", true);
				result.put("idList", idList);
				result.put("valueList", valueList);
			}else{
				result.put("success", false);
				result.put("idList", idList);
				result.put("valueList", valueList);
			}
		

		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityProductService getQualityProductService() {
		return qualityProductService;
	}

	public void setQualityProductService(QualityProductService qualityProductService) {
		this.qualityProductService = qualityProductService;
	}

	public QualityProduct getQualityProduct() {
		return qualityProduct;
	}

	public void setQualityProduct(QualityProduct qualityProduct) {
		this.qualityProduct = qualityProduct;
	}


}
