package com.biolims.system.quality.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.quality.model.QualityProduct;
import com.biolims.system.template.model.TemplateItem;
import com.opensymphony.xwork2.ActionContext;

/*@Repository
@SuppressWarnings("unchecked")
public class QualityProductDao extends BaseHibernateDao {
	public Map<String, Object> selectQualityProductList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QualityProduct where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key=" and state='1'";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QualityProduct> list = new ArrayList<QualityProduct>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}*/

@Repository
@SuppressWarnings("unchecked")
public class QualityProductDao extends BaseHibernateDao {
	public Map<String, Object> selectQualityProductList(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityProduct where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QualityProduct where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				key+=" order by "+col+" "+sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}
	public Map<String, Object> findQualityProductList(Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QualityProduct where 1=1 and state='1'";
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QualityProduct> list = new ArrayList<QualityProduct>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	//根据主表ID加载子表明细数据
	public Map<String,Object> setQcItem(String code){
		String hql="from QualityProduct t where 1=1 and t.id like '%"+code+"%'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<QualityProduct> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//根据主表ID加载子表明细数据
	public List<Map<String,Object>> selectCustomer(String id,String experType,String startDate,String endDate){
		String sql = "";
		if(id!=null && startDate!=null && endDate!=null){
			if(experType!=null && experType.equals("doDna")){
				sql="select t.id as mid,m.contraction as nd from SAMPLE_DNA_INFO m join DNA_TASK t on t.id=m.DNA_TASK where m.CODE like '"
						+id.substring(0,3)+"%' and to_date(t.create_Date,'yyyy-MM-dd HH24:MI:ss')>=to_date('"+startDate+"','yyyy-MM-dd HH24:MI:ss') and " +
						"to_date(t.create_Date,'yyyy-MM-dd HH24:MI:ss')<=to_date('"+endDate+"','yyyy-MM-dd HH24:MI:ss')";
			}else if(experType!=null && experType.equals("doWk")){
				
			}else if(experType!=null && experType.equals("doQua")){
				
			}else if(experType!=null && experType.equals("doQpcr")){
				
			}else if(experType!=null && experType.equals("doDnap")){
				
			}else if(experType!=null && experType.equals("doCheck")){
				
			}else if(experType!=null && experType.equals("doPooling")){
				
			}else if(experType!=null && experType.equals("doGenera")){
				
			}
		}
		List<Map<String, Object>> list = this.findForJdbc(sql);
		if(list!=null && list.size()>0){
			return list;
		}
		return null;
	}
	public Map<String, Object> qualityProductSelectJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityProduct where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		/*String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}*/
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QualityProduct where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				key+=" order by "+col+" "+sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}