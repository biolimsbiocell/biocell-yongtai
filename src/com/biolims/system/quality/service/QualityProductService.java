package com.biolims.system.quality.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.project.model.Project;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.quality.dao.QualityProductDao;
import com.biolims.system.quality.model.QualityProduct;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QualityProductService {
	@Resource
	private QualityProductDao qualityProductDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	@Resource
	private CodingRuleService codingRuleService;

	/*
	 * public Map<String, Object> findQualityProductList(Map<String, String>
	 * mapForQuery, Integer startNum, Integer limitNum, String dir, String sort)
	 * { return qualityProductDao.selectQualityProductList(mapForQuery,
	 * startNum, limitNum, dir, sort); }
	 */

	public Map<String, Object> findQualityProductList(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return qualityProductDao.selectQualityProductList(start, length, query,
				col, sort);
	}

	public Map<String, Object> selectQualityProductList(Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityProductDao.findQualityProductList(startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QualityProduct i) throws Exception {

		qualityProductDao.saveOrUpdate(i);

	}

	public QualityProduct get(String id) {
		QualityProduct qualityProduct = commonDAO.get(QualityProduct.class, id);
		return qualityProduct;
	}

	/*
	 * @WriteOperLogTable
	 * 
	 * @WriteExOperLog
	 * 
	 * @Transactional(rollbackFor = Exception.class) public String save(String
	 * main, String[] tempId, String userId, String templateId, String logInfo)
	 * throws Exception { String id = ""; if (main != null) { String mainJson =
	 * "[" + main + "]"; // String mainJson =main; List<Map<String, Object>>
	 * list = JsonUtils.toListByJsonArray( mainJson, List.class); QualityProduct
	 * qualityProduct = new QualityProduct(); qualityProduct= (QualityProduct)
	 * commonDAO.Map2Bean(list.get(0),qualityProduct); if
	 * ((qualityProduct.getId() != null && qualityProduct.getId().equals("")) ||
	 * qualityProduct.getId().equals("NEW")) { String modelName =
	 * "QualityProduct"; String markCode = "Q"; id =
	 * codingRuleService.genTransID(modelName, markCode);
	 * qualityProduct.setId(id); qualityProduct.setScopeId((String)
	 * ActionContext.getContext().getSession().get("scopeId"));
	 * qualityProduct.setScopeName((String)
	 * ActionContext.getContext().getSession().get("scopeName")); } else { id =
	 * qualityProduct.getId(); } commonDAO.saveOrUpdate(qualityProduct);
	 * 
	 * } if (logInfo != null && !"".equals(logInfo)) { LogInfo li = new
	 * LogInfo(); li.setLogDate(new Date()); User u = (User)
	 * ServletActionContext .getRequest() .getSession() .getAttribute(
	 * SystemConstants.USER_SESSION_KEY); li.setUserId(u.getId());
	 * li.setFileId(id); li.setModifyContent(logInfo);
	 * commonDAO.saveOrUpdate(li); } return id; }
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(QualityProduct pr, String logInfo,String log) throws Exception {
		/*if (pr.getId() != null && "NEW".equals(pr.getId())) {
			String modelName = "QualityProduct";
			String markCode = "Q";
			String id = codingRuleService.genTransID(modelName, markCode);
			pr.setId(id);
		} 
		commonDAO.saveOrUpdate(pr);*/
		if(pr!=null) {
			commonDAO.saveOrUpdate(pr);
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(pr.getId());
			li.setClassName("QualityProduct");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}

	}

	/*
	 * public void save(QualityProduct sc, Map jsonMap) throws Exception { if
	 * (sc != null) { qualityProductDao.saveOrUpdate(sc);
	 * 
	 * String jsonStr = ""; } }
	 */

	// 根据模板ID加载子表明细
	public List<Map<String, String>> setQcItem(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qualityProductDao.setQcItem(code);
		List<QualityProduct> list = (List<QualityProduct>) result.get("list");
		if (list != null && list.size() > 0) {
			for (QualityProduct ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("name", ti.getName());
				map.put("note", ti.getExpectValue());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据条件查找质控品
	public List<Map<String, Object>> selectCustomer(String id,
			String experType, String startDate, String endDate) {
		List<Map<String, Object>> mapList = qualityProductDao.selectCustomer(
				id, experType, startDate, endDate);
		return mapList;
	}

	public Map<String, Object> qualityProductSelectJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		// TODO Auto-generated method stub
		return qualityProductDao.qualityProductSelectJson(start, length, query,
				col, sort);
	}
}
