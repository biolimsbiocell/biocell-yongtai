﻿package com.biolims.system.storage.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.model.StorageBoxItem;
import com.biolims.system.storage.service.StorageBoxService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/storage/storageBox")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class StorageBoxAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9020";
	@Autowired
	private StorageBoxService storageBoxService;
	private StorageBox storageBox = new StorageBox();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showStorageBoxList")
	public String showStorageBoxList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/storage/storageBox.jsp");
	}

	@Action(value = "showStorageBoxListJson")
	public void showStorageBoxListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageBoxService.findStorageBoxList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<StorageBox> list = (List<StorageBox>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("locationId", "");
		map.put("newLocationId", "");
		map.put("oldLocationId", "");
		map.put("locationName", "");
		map.put("state", "");
		map.put("typeId", "");
		map.put("type", "");
		map.put("storageContainer-id", "");
		map.put("storageContainer-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "storageBoxSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogStorageBoxList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/storage/storageBoxDialog.jsp");
	}

	@Action(value = "showDialogStorageBoxListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogStorageBoxListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageBoxService.findStorageBoxList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<StorageBox> list = (List<StorageBox>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("locationId", "");
		map.put("oldLocationId", "");
		map.put("newLocationId", "");
		map.put("locationName", "");
		map.put("state", "");
		map.put("typeId", "");
		map.put("type", "");
		map.put("storageContainer-id", "");
		map.put("storageContainer-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editStorageBox")
	public String editStorageBox() throws Exception {
		String id = getParameterFromRequest("id");
		String locationIds = "";
		long num = 0;
		if (id != null && !id.equals("")) {
			storageBox = storageBoxService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "storageBox");
			locationIds = storageBox.getLocationId();
			List<String> dicList = new ArrayList<String>();
			if (locationIds != null) {
				String[] idarr = locationIds.split(",");
				for (int i = 0; i < idarr.length; i++) {
					if (idarr[i] != null && !idarr[i].equals("")) {
						dicList.add(idarr[i]);
					}
				}
				putObjToContext("dicList", dicList);
			} else {
				putObjToContext("dicList", null);
			}
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// storageBox.setCreateUser(user);
			// storageBox.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/storage/storageBoxEdit.jsp");
	}

	@Action(value = "copyStorageBox")
	public String copyStorageBox() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		storageBox = storageBoxService.get(id);
		storageBox.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/storage/storageBoxEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = storageBox.getId();
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		if (id != null && id.equals("")) {
			storageBox.setId(null);
		}
		Map aMap = new HashMap();
		// if(getParameterFromRequest("storageBoxItemDivJson")!=null){
		// aMap.put("storageBoxItem",
		// getParameterFromRequest("storageBoxItemDivJson"));
		// }
		storageBoxService.save(storageBox, aMap, user);
		return redirect("/system/storage/storageBox/editStorageBox.action?id="
				+ storageBox.getId());

	}

	// 子表信息
	@Action(value = "showStorageBoxItemList")
	public String showStorageBoxItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/storage/storageBoxItem.jsp");
	}

	@Action(value = "showStorageBoxItemListJson")
	public void showStorageBoxItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String scId = getRequest().getParameter("id");
		Map<String, Object> result = storageBoxService.findStorageBoxItemList(
				scId, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<StorageBoxItem> list = (List<StorageBoxItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("locationId", "");
		map.put("changeUser", "");
		map.put("changeDate", "yyyy-MM-dd");
		map.put("locationName", "");
		map.put("state", "");
		map.put("note", "");
		map.put("storageBox-id", "");
		map.put("storageBox-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 异步转移样本位置
	 * */
	@Action(value = "saveNewStorage", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveNewStorage() throws Exception {
		String id = getParameterFromRequest("id");
		String locationId = getParameterFromRequest("locationId");
		String newLocationId = getParameterFromRequest("newLocationId");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			storageBoxService.saveNewStorage(id, locationId, newLocationId,
					user);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "viewStorageBox")
	public String toViewStorageBox() throws Exception {
		String id = getParameterFromRequest("id");
		storageBox = storageBoxService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/storage/storageBoxEdit.jsp");
	}

	@Action(value = "setStorageBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setStorageBox() throws Exception {
		String spId = getParameterFromRequest("spId");
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			storageBoxService.setStorageBox(id, spId);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 保存盒子信息
	@Action(value = "saveStorageBoxGrid")
	public void saveStorageBoxGrid() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			String itemDataJson = getRequest().getParameter("itemDataJson");
			storageBoxService.saveStorageBoxGrid(itemDataJson, user);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public StorageBoxService getStorageBoxService() {
		return storageBoxService;
	}

	public void setStorageBoxService(StorageBoxService storageBoxService) {
		this.storageBoxService = storageBoxService;
	}

	public StorageBox getStorageBox() {
		return storageBox;
	}

	public void setStorageBox(StorageBox storageBox) {
		this.storageBox = storageBox;
	}
	
	
	
	/**
	 * 新页面盒子
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showStorageBoxTableList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageBoxTableList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/storage/storageBoxTable.jsp");
//		return dispatcher("/WEB-INF/page/sample/storage/sampleInfoInTable.jsp");
	}

	/**
	 * 
	 * @Title: showStorageBoxTableListJson  
	 * @Description: 新页面盒子
	 * 	 * @author qi.yan
	 * @date 2018-4-20上午9:55:47
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showStorageBoxTableListJson")
	public void showStorageBoxTableListJson() throws Exception {

		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = storageBoxService.findStorageBoxTableList(
					start, length, query, col, sort);
			List<StorageBoxItem> list = (List<StorageBoxItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("locationId", "");
			map.put("newLocationId", "");
			map.put("oldLocationId", "");
			map.put("locationName", "");
			map.put("state", "");
			map.put("typeId", "");
			map.put("type", "");
			map.put("storageContainer-id", "");
			map.put("storageContainer-name", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	
	/**
	 * 
	 * @Title: saveStorageBoxTableGrid  
	 * @Description: 新页面保存盒子信息)  
	 * @author qi.yan
	 * @date 2018-4-20下午1:20:24
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "saveStorageBoxTableGrid")
	public void saveStorageBoxTableGrid() throws Exception {
		String item = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			storageBoxService.saveStorageBoxTableGrid( user,item,logInfo);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
		
	/**
	 * 
	 * @Title: editStorageBoxTable  
	 * @Description: 编辑盒子页面
	 * @author qi.yan
	 * @date 2018-4-20下午2:28:14
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "editStorageBoxTable")
	public String editStorageBoxTable() throws Exception {
		String id = getParameterFromRequest("id");
		String locationIds = "";
		long num = 0;
		if (id != null && !id.equals("")) {
			storageBox = storageBoxService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "storageBox");
			locationIds = storageBox.getLocationId();
			List<String> dicList = new ArrayList<String>();
			if (locationIds != null) {
				String[] idarr = locationIds.split(",");
				for (int i = 0; i < idarr.length; i++) {
					if (idarr[i] != null && !idarr[i].equals("")) {
						dicList.add(idarr[i]);
					}
				}
				putObjToContext("dicList", dicList);
			} else {
				putObjToContext("dicList", null);
			}
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// storageBox.setCreateUser(user);
			// storageBox.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/storage/storageBoxEditTable.jsp");
	}
	
	
	/**
	 * 
	 * @Title: showStorageBoxItemTableListJson  
	 * @Description: 移动盒子信息
	 * @author qi.yan
	 * @date 2018-4-20下午2:43:42
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showStorageBoxItemTableListJson")
	public void showStorageBoxItemTableListJson() throws Exception {

		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		
		String storageBox_id = getParameterFromRequest("id");
		try {
			Map<String, Object> result = storageBoxService.findStorageBoxItemTableList(start, length, query, col,
					sort,storageBox_id);
			List<StorageBoxItem> list = (List<StorageBoxItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();

			map.put("id", "");
			map.put("name", "");
			map.put("locationId", "");
			map.put("changeUser", "");
			map.put("changeDate", "yyyy-MM-dd");
			map.put("locationName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("storageBox-id", "");
			map.put("storageBox-name", "");
			
//			// 根据模块查询自定义字段数据
//			Map<String, Object> mapField = fieldService
//					.findFieldByModuleValue("Storage");
//			String dataStr = PushData.pushFieldData(data, mapField);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	/**
	 * 
	 * @Title: selectSampleInfoInByBoxId
	 * @Description: 盒内样本信息  
	 * @author qi.yan
	 * @date 2018-4-20下午3:30:37
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "selectSampleInfoInByBoxId")
	public void selectSampleInfoInByBoxId() throws Exception {

		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String id = getRequest().getParameter("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = storageBoxService.selectSampleInfoInByBoxId(start, length, query, col,
					sort,id);
			List<SampleInfoIn> list = (List<SampleInfoIn>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();

		
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("checked", "");
			map.put("dataTraffic", "");
			map.put("name", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("classify", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("num", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("customer-id", "");
			map.put("customer-name", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");
			map.put("customer-street", "");
			map.put("customer-postcode", "");
			map.put("sampleInfo-project-id", "");
			map.put("sampleInfo-project-name", "");
			map.put("sampleInfo-crmCustomer-id", "");
			map.put("sampleInfo-crmCustomer-name", "");
			map.put("sampleInfo-crmDoctor-id", "");
			map.put("sampleInfo-crmDoctor-name", "");
			map.put("sampleInfo-productId", "");
			map.put("sampleInfo-productName", "");
			map.put("sampleInfo-patientName", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-name", "");
			map.put("sampleInfo-idCard", "");
			map.put("isRp", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-createUser-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-orderId", "");
			map.put("sampleState", "");
			map.put("type", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("samplingDate","");
			map.put("barCode","");
			map.put("dataTraffic","");
			
			map.put("sampleStyle","");
			map.put("qpcrConcentration","");
			map.put("indexa","");
			
			map.put("storageBox-id","");
			map.put("storageBox-name","");
			map.put("boxLocation","");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
//			// 根据模块查询自定义字段数据
//			Map<String, Object> mapField = fieldService
//					.findFieldByModuleValue("Storage");
//			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
}
