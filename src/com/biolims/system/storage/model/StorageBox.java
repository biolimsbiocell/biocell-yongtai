package com.biolims.system.storage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.container.model.StorageContainer;

/**
 * @Title: Model
 * @Description: 储位盒子
 * @author lims-platform
 * @date 2016-07-05 17:45:31
 * @version V1.0
 * 
 */
@Entity
@Table(name = "STORAGE_BOX")
@SuppressWarnings("serial")
public class StorageBox extends EntityDao<StorageBox> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 储位号 */
	private String locationId;
	/** 旧储位号 */
	private String oldLocationId;
	/** 新储位号 */
	private String newLocationId;
	/** 储位名称 */
	private String locationName;
	/** 状态 */
	private String state;
	private StorageContainer storageContainer;// 存储容器
	/** 样本类型id */
	private String typeId;
	/** 样本类型 */
	private String type;
	/** scopeId*/
	private String scopeId;
	/** scopeName*/
	private String scopeName;
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 储位号
	 */
	@Column(name = "LOCATION_ID", length = 255)
	public String getLocationId() {
		return this.locationId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 储位号
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getNewLocationId() {
		return newLocationId;
	}

	public void setNewLocationId(String newLocationId) {
		this.newLocationId = newLocationId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 储位名称
	 */
	@Column(name = "LOCATION_NAME", length = 255)
	public String getLocationName() {
		return this.locationName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 储位名称
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getOldLocationId() {
		return oldLocationId;
	}

	public void setOldLocationId(String oldLocationId) {
		this.oldLocationId = oldLocationId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "STORAGE_CONTAINER_ID")
	public StorageContainer getStorageContainer() {
		return storageContainer;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setStorageContainer(StorageContainer storageContainer) {
		this.storageContainer = storageContainer;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}