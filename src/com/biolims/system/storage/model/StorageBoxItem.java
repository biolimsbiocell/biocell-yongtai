package com.biolims.system.storage.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.storage.container.model.StorageContainer;
/**   
 * @Title: Model
 * @Description: 储位盒子
 * @author lims-platform
 * @date 2016-07-05 17:45:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "STORAGE_BOX_ITEM")
@SuppressWarnings("serial")
public class StorageBoxItem extends EntityDao<StorageBoxItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**储位号*/
	private String locationId;
	/**储位名称*/
	private String locationName;
	/**状态*/
	private String state;
	private StorageBox storageBox;// 主表
	/**修改人*/
	private String changeUser;
	/**修改时间*/
	private Date changeDate;
	/** scopeId*/
	private String scopeId;
	/** scopeName*/
	private String scopeName;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  储位号
	 */
	@Column(name ="LOCATION_ID", length = 50)
	public String getLocationId(){
		return this.locationId;
	}
	/**
	 *方法: 取得String
	 *@return: String  储位名称
	 */
	@Column(name ="LOCATION_NAME", length = 50)
	public String getLocationName(){
		return this.locationName;
	}
	/**
	 *方法: 设置String
	 *@param: String  储位名称
	 */
	public void setLocationName(String locationName){
		this.locationName = locationName;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_BOX")
	public StorageBox getStorageBox() {
		return storageBox;
	}
	public void setStorageBox(StorageBox storageBox) {
		this.storageBox = storageBox;
	}
	public String getChangeUser() {
		return changeUser;
	}
	public void setChangeUser(String changeUser) {
		this.changeUser = changeUser;
	}
	public Date getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}
	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}
	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}