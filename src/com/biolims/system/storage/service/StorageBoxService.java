package com.biolims.system.storage.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.storage.dao.StorageBoxDao;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.model.StorageBoxItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class StorageBoxService {
	@Resource
	private StorageBoxDao storageBoxDao;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findStorageBoxList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return storageBoxDao.selectStorageBoxList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findStorageBoxItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = storageBoxDao.selectStorageBoxItemList(
				scId, startNum, limitNum, dir, sort);
		List<StorageBoxItem> list = (List<StorageBoxItem>) result.get("list");
		return result;
	}

	public List<StorageBox> findStorageBoxList(String locationIds) {
		return storageBoxDao.selectStorageBoxList(locationIds);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageBox i) throws Exception {

		storageBoxDao.saveOrUpdate(i);

	}

	public StorageBox get(String id) {
		StorageBox storageBox = commonDAO.get(StorageBox.class, id);
		return storageBox;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageBox sc, Map jsonMap, User user) throws Exception {
		if (sc != null) {
			storageBoxDao.saveOrUpdate(sc);
			// StorageBoxItem sbi = new StorageBoxItem();
			// sbi.setChangeDate(new Date());
			// sbi.setChangeUser(user.getName());
			// sbi.setLocationId(sc.getOldLocationId());
			// sbi.setLocationName(sc.getLocationName());
			// sbi.setStorageBox(sc);
			// storageBoxDao.saveOrUpdate(sbi);
			String jsonStr = "";
			// jsonStr = (String) jsonMap.get("storageBoxItem");
			// if (jsonStr != null && !jsonStr.equals("{}") &&
			// !jsonStr.equals("")) {
			// saveStorageBoxItem(sc, jsonStr,user);
			// }
		}
		if (sc.getLocationId() != null && !sc.getLocationId().equals("")) {
			if (sc.getLocationId().indexOf(sc.getNewLocationId()) == -1) {
				sc.setLocationId(sc.getLocationId() + ","
						+ sc.getNewLocationId() + ",");
			}
		} else {
			sc.setLocationId(sc.getNewLocationId() + ",");
		}
		if (sc.getOldLocationId() != null && !sc.getOldLocationId().equals("")
				&& sc.getNewLocationId() != null
				&& !sc.getNewLocationId().equals("")) {
			List<SampleInfoIn> siiList = sampleInDao
					.getSampleInItemByStorage(sc.getOldLocationId());
			if (siiList != null && siiList.size() > 0) {
				for (SampleInfoIn sii : siiList) {
					String endLocatino = sii.getLocation().substring(
							sii.getLocation().lastIndexOf("-"));
					sii.setLocation(sc.getNewLocationId() + endLocatino);
				}
			}

		}
		sc.setOldLocationId(sc.getNewLocationId());

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageBoxItem(StorageBox sc, String itemDataJson, User user)
			throws Exception {
		List<StorageBoxItem> saveItems = new ArrayList<StorageBoxItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageBoxItem scp = new StorageBoxItem();
			// 将map信息读入实体类
			scp = (StorageBoxItem) storageBoxDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setChangeUser(user.getName());
			scp.setChangeDate(new Date());
			scp.setStorageBox(sc);
			saveItems.add(scp);
		}
		storageBoxDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveNewStorage(String id, String locationId,
			String newLocationId, User user) throws Exception {
		StorageBox sc = storageBoxDao.get(StorageBox.class, id);
		sc.setNewLocationId(newLocationId);
		StorageBoxItem sbi = new StorageBoxItem();
		sbi.setChangeDate(new Date());
		sbi.setChangeUser(user.getName());
		sbi.setLocationId(sc.getOldLocationId());
		sbi.setLocationName(sc.getLocationName());
		sbi.setStorageBox(sc);
		storageBoxDao.saveOrUpdate(sbi);
		if (sc.getOldLocationId() != null && !sc.getOldLocationId().equals("")
				&& sc.getNewLocationId() != null
				&& !sc.getNewLocationId().equals("")) {
			List<SampleInfoIn> siiList = sampleInDao
					.getSampleInItemByStorage(sc.getOldLocationId());
			if (siiList != null && siiList.size() > 0) {
				for (SampleInfoIn sii : siiList) {
					String endLocatino = sii.getLocation().substring(
							sii.getLocation().lastIndexOf("-"));
					sii.setLocation(sc.getNewLocationId() + endLocatino);
				}
			}
			if (sc.getLocationId() != null && !sc.getLocationId().equals("")) {
				if (sc.getLocationId().indexOf(sc.getNewLocationId()) == -1) {
					sc.setLocationId(sc.getLocationId() + sc.getNewLocationId()
							+ ",");
				}
			} else {
				sc.setLocationId(sc.getNewLocationId() + ",");
			}
			sc.setOldLocationId(sc.getNewLocationId());
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setStorageBox(String id, String spId) throws Exception {
		StorageBox sb = commonDAO.get(StorageBox.class, id);
		sb.setOldLocationId(spId);
		storageBoxDao.saveOrUpdate(sb);

	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageBoxGrid(String itemDataJson, User user)
			throws Exception {
		List<StorageBox> saveItems = new ArrayList<StorageBox>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageBox sbi = new StorageBox();
			sbi = (StorageBox) storageBoxDao.Map2Bean(map, sbi);
			if (sbi.getId() == null) {
				String modelName = "StorageBox";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String markCode = "B" + stime;
				String autoID = codingRuleService.get(modelName, markCode,
						00000, 5, null, "id");
				sbi.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sbi.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sbi.setId(autoID);
				sbi.setName(autoID);
			}

			saveItems.add(sbi);
			if (sbi.getNewLocationId() != null
					&& !sbi.getNewLocationId().equals("")) {
				if (sbi.getLocationId() == null
						|| "".equals(sbi.getLocationId())) {
					sbi.setLocationId(sbi.getNewLocationId() + ",");
				} else {
					if (sbi.getLocationId().indexOf(sbi.getNewLocationId()) == -1) {
						sbi.setLocationId(sbi.getLocationId()
								+ sbi.getNewLocationId() + ",");
					}
				}
				sbi.setOldLocationId(sbi.getNewLocationId());
			}
			storageBoxDao.saveOrUpdate(sbi);
		}
		if (saveItems.get(0).getNewLocationId() != null
				&& !"".equals(saveItems.get(0).getNewLocationId())) {
			String[] a = saveItems.get(0).getNewLocationId().split("-");
			// 架子位置
			String jz = a[0] + "-" + a[1] + "-" + a[2];
			// 架子孔位置
			String jz2 = a[3];

			String jz2_1 = jz2.substring(0, 1);
			String jz2_2 = jz2.substring(1, 3);

			char[] b = jz2_1.toCharArray();
			int c = (int) b[0] - 65;
			int d = Integer.valueOf(jz2_2);

			// 清空要添加盒子的存储位置信息（清空批量选择是保存上的相同储位）
			for (int k = 0; k < saveItems.size(); k++) {
				saveItems.get(k).setNewLocationId("");
				this.storageBoxDao.saveOrUpdate(saveItems.get(k));
			}

			StoragePosition sp = this.storageBoxDao.get(StoragePosition.class,
					jz);
			StorageContainer upSc = null;
			if (sp.getStorageContainer() != null) {
				upSc = sp.getStorageContainer();
			}
			int n = 0;
			for (int i = c; i < upSc.getRowNum(); i++) {
				for (int j = d; j <= upSc.getColNum(); j++) {
					String hzId = "";
					if (j < 10) {
						hzId = jz + "-" + String.valueOf((char) (65 + i)) + "0"
								+ String.valueOf(j);
					} else {
						hzId = jz + "-" + String.valueOf((char) (65 + i))
								+ String.valueOf(j);
					}
					// 判断架子上是否有盒子
					if (this.storageBoxDao.findIsUse(hzId) == 0) {
						if (n < saveItems.size()) {
							saveItems.get(n).setNewLocationId(hzId);
							n++;
						}
					}
					if (j == upSc.getColNum()) {
						d = 1;
					}
				}
			}
			if (saveItems.size() - n > 0) {
				for (int k = 0; k < saveItems.size() - n; k++) {
					saveItems.get(n).setNewLocationId("");
					n++;
				}
			}
		}
		storageBoxDao.saveOrUpdateAll(saveItems);
	}

	
	public Map<String, Object> findStorageBoxTableList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return storageBoxDao.selStorageBoxTable(start, length, query, col, sort);
	}

	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageBoxTableGrid(User user, String item, String logInfo) throws Exception {
		String id ="";
		List<StorageBox> saveItems = new ArrayList<StorageBox>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				item, List.class);
		String log = "";
		for (Map<String, Object> map : list) {
			StorageBox sbi = new StorageBox();
			sbi = (StorageBox) storageBoxDao.Map2Bean(map, sbi);
			
			if ("".equals(sbi.getId())) {
				log="123";
				String modelName = "StorageBox";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String markCode = "B" + stime;
				String autoID = codingRuleService.get(modelName, markCode,
						00000, 5, null, "id");
				sbi.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sbi.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sbi.setId(autoID);
				sbi.setName(autoID);
				sbi.setState("1");
				id=autoID;
			}

			saveItems.add(sbi);
			if (sbi.getNewLocationId() != null
					&& !sbi.getNewLocationId().equals("")) {
				if (sbi.getLocationId() == null
						|| "".equals(sbi.getLocationId())) {
					sbi.setLocationId(sbi.getNewLocationId() + ",");
				} else {
					if (sbi.getLocationId().indexOf(sbi.getNewLocationId()) == -1) {
						sbi.setLocationId(sbi.getLocationId()
								+ sbi.getNewLocationId() + ",");
					}
				}
				sbi.setOldLocationId(sbi.getNewLocationId());
			}
			storageBoxDao.saveOrUpdate(sbi);
		}
		if (saveItems.get(0).getNewLocationId() != null
				&& !"".equals(saveItems.get(0).getNewLocationId())) {
			String[] a = saveItems.get(0).getNewLocationId().split("-");
			// 架子位置
			String jz = a[0] + "-" + a[1] + "-" + a[2];
			// 架子孔位置
			String jz2 = a[3];

			String jz2_1 = jz2.substring(0, 1);
			String jz2_2 = jz2.substring(1, 3);

			char[] b = jz2_1.toCharArray();
			int c = (int) b[0] - 65;
			int d = Integer.valueOf(jz2_2);

			// 清空要添加盒子的存储位置信息（清空批量选择是保存上的相同储位）
			for (int k = 0; k < saveItems.size(); k++) {
				saveItems.get(k).setNewLocationId("");
				this.storageBoxDao.saveOrUpdate(saveItems.get(k));
			}

			StoragePosition sp = this.storageBoxDao.get(StoragePosition.class,
					jz);
			StorageContainer upSc = null;
			if (sp.getStorageContainer() != null) {
				upSc = sp.getStorageContainer();
			}
			int n = 0;
			for (int i = c; i < upSc.getRowNum(); i++) {
				for (int j = d; j <= upSc.getColNum(); j++) {
					String hzId = "";
					if (j < 10) {
						hzId = jz + "-" + String.valueOf((char) (65 + i)) + "0"
								+ String.valueOf(j);
					} else {
						hzId = jz + "-" + String.valueOf((char) (65 + i))
								+ String.valueOf(j);
					}
					// 判断架子上是否有盒子
					if (this.storageBoxDao.findIsUse(hzId) == 0) {
						if (n < saveItems.size()) {
							saveItems.get(n).setNewLocationId(hzId);
							n++;
						}
					}
					if (j == upSc.getColNum()) {
						d = 1;
					}
				}
			}
			if (saveItems.size() - n > 0) {
				for (int k = 0; k < saveItems.size() - n; k++) {
					saveItems.get(n).setNewLocationId("");
					n++;
				}
			}
		}
		storageBoxDao.saveOrUpdateAll(saveItems);
		
	
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(id);
			if("123".equals(log)&&!"".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	public Map<String, Object> findStorageBoxItemTableList(Integer start,
			Integer length, String query, String col, String sort, String storageBox_id) throws Exception {
		return storageBoxDao.selectStorageBoxItemTableList(start, length, query, col,
				sort,storageBox_id);
	}

	public Map<String, Object> selectSampleInfoInByBoxId(Integer start,
			Integer length, String query, String col, String sort, String id) throws Exception {
		return storageBoxDao.findSampleInfoInByBoxId(start, length, query, col,
				sort,id);
	}
}
