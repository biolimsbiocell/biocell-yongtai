﻿
package com.biolims.system.abnomal.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.abnomal.model.AbnomalType;
import com.biolims.system.abnomal.service.AbnomalTypeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/system/abnomal/abnomalType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AbnomalTypeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9008";
	@Autowired
	private AbnomalTypeService abnomalTypeService;
	private AbnomalType abnomalType = new AbnomalType();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Action(value = "showAbnomalTypeList")
	public String showAbnomalTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/abnomal/abnomalType.jsp");
	}

	@Action(value = "showAbnomalTypeListJson")
	public void showAbnomalTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = abnomalTypeService.findAbnomalTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AbnomalType> list = (List<AbnomalType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("method", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "abnomalTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAbnomalTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/abnomal/abnomalTypeDialog.jsp");
	}

	@Action(value = "showDialogAbnomalTypeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAbnomalTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = abnomalTypeService.findAbnomalTypeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AbnomalType> list = (List<AbnomalType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("method", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editAbnomalType")
	public String editAbnomalType() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			abnomalType = abnomalTypeService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "abnomalType");
		} else {
			//abnomalType.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			abnomalType.setCreateUser(user);
//			abnomalType.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/system/abnomal/abnomalTypeEdit.jsp");
	}

	@Action(value = "copyAbnomalType")
	public String copyAbnomalType() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		abnomalType = abnomalTypeService.get(id);
		abnomalType.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/system/abnomal/abnomalTypeEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = abnomalType.getId();
//		if ((id != null && id.equals("")) || id.equals("NEW")) {
//			String modelName = "AbnomalType";
//			String markCode="YCLX";
//			String autoID = codingRuleService.genTransID(modelName,markCode);
//			abnomalType.setId(autoID);
//		}
		if(id!=null && id.equals("")){
			abnomalType.setId(null);
		}
		Map aMap = new HashMap();
		abnomalTypeService.save(abnomalType,aMap);
		return redirect("/system/abnomal/abnomalType/editAbnomalType.action?id=" + abnomalType.getId());

	}

	@Action(value = "viewAbnomalType")
	public String toViewAbnomalType() throws Exception {
		String id = getParameterFromRequest("id");
		abnomalType = abnomalTypeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/abnomal/abnomalTypeEdit.jsp");
	}
	
	
	/**
	 * 访问 任务树
	 */
	@Action(value = "showAbnomalTypeTree")
	public String showAbnomalTypeTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/system/abnomal/abnomalType/showAbnomalTypeTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/system/abnomal/abnomalTypeTree.jsp");
	}

	@Action(value = "showAbnomalTypeTreeJson")
	public void showAbnomalTypeTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<AbnomalType> aList = null;
		if (upId.equals("0")) {

			aList = abnomalTypeService.findAbnomalTypeList();
		} else {

			aList = abnomalTypeService.findAbnomalTypeList(upId);
		}

		String a = abnomalTypeService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showAbnomalTypeChildTreeJson")
	public void showAbnomalTypeChildTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<AbnomalType> aList = abnomalTypeService.findAbnomalTypeList(upId);
		String a = abnomalTypeService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}
	/**
	 * 选择异常类型
	 * */
	@Action(value = "showAbnomalTypeTreeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAbnomalTypeTreeSelect() throws Exception {
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/system/abnomal/abnomalType/showAbnomalTypeTreeSelectJson.action");
		return dispatcher("/WEB-INF/page/system/abnomal/abnomalTypeTreeSelect.jsp");
	}
	
	@Action(value = "showAbnomalTypeTreeSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAbnomalTypeTreeSelectJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<AbnomalType> aList = null;
		if (upId.equals("0")||upId.equals("")) {

			aList = abnomalTypeService.findAbnomalTypeList();
		} else {

//			aList = abnomalTypeService.findAbnomalTypeList(upId);
		}
		String a = abnomalTypeService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}
	/**
	 * 异步加载异常名称
	 * @return
	 */
	@Action(value = "findAbnomalTypeName")
	public void findAbnomalTypeName() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.abnomalTypeService.findAbnomalTypeName(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AbnomalTypeService getAbnomalTypeService() {
		return abnomalTypeService;
	}

	public void setAbnomalTypeService(AbnomalTypeService abnomalTypeService) {
		this.abnomalTypeService = abnomalTypeService;
	}

	public AbnomalType getAbnomalType() {
		return abnomalType;
	}

	public void setAbnomalType(AbnomalType abnomalType) {
		this.abnomalType = abnomalType;
	}


}
