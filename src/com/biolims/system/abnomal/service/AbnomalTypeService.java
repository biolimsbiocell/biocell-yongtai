package com.biolims.system.abnomal.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.abnomal.dao.AbnomalTypeDao;
import com.biolims.system.abnomal.model.AbnomalType;
import com.biolims.system.code.SystemConstants;
import com.biolims.system.product.model.Product;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AbnomalTypeService {
	@Resource
	private AbnomalTypeDao abnomalTypeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAbnomalTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return abnomalTypeDao.selectAbnomalTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AbnomalType i) throws Exception {

		abnomalTypeDao.saveOrUpdate(i);

	}
	public AbnomalType get(String id) {
		AbnomalType abnomalType = commonDAO.get(AbnomalType.class, id);
		return abnomalType;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AbnomalType sc, Map jsonMap) throws Exception {
		if (sc != null) {
			abnomalTypeDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
   /**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<AbnomalType> getChildList(List<AbnomalType> list, AbnomalType node) throws Exception { //得到子节点列表   
		List<AbnomalType> li = new ArrayList<AbnomalType>();
		Iterator<AbnomalType> it = list.iterator();
		while (it.hasNext()) {
			AbnomalType n = (AbnomalType) it.next();
			if (n.getParent() != null && n.getParent().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}
   /**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<AbnomalType> list, AbnomalType node) throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}
   	public boolean hasChildCount(AbnomalType node) throws Exception {

		Long c = commonDAO.getCount("from AbnomalType where parent.id='"
				+ node.getId() + "'");
		return c > 0 ? true : false;
	}
   
   public String getJson(List<AbnomalType> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<AbnomalType> nodeList0 = new ArrayList<AbnomalType>();
		Iterator<AbnomalType> it1 = list.iterator();
		while (it1.hasNext()) {
			AbnomalType node = (AbnomalType) it1.next();
			//if (node.getLevel() == 0) {
			nodeList0.add(node);
			//}
		}
		Iterator<AbnomalType> it = nodeList0.iterator();
		while (it.hasNext()) {
			AbnomalType node = (AbnomalType) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}
	
	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<AbnomalType> list, AbnomalType treeNode) throws Exception {
		if (hasChild(list, treeNode)) {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");

			json.append(",\"leaf\":false");
			json.append(",\"checked\":false");
			json.append(",\"children\":[");
			List<AbnomalType> childList = getChildList(list, treeNode);
			Iterator<AbnomalType> iterator = childList.iterator();
			while (iterator.hasNext()) {
				AbnomalType node = (AbnomalType) iterator.next();
				constructorJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");
			json.append(",\"leaf\":true");
			json.append(",\"checked\":false");
			json.append("},");

		}
		
	}
	
   	public List<AbnomalType> findAbnomalTypeList(String upId) throws Exception {
		List<AbnomalType> list = null;
		if (upId.equals("0")) {
			list = commonDAO
					.find("from AbnomalType where  parent.id is null order by id asc");
		} else {

			list = commonDAO.find("from AbnomalType where parent.id='"
					+ upId + "' order by id asc");
		}
		return list;
	}
	public List<AbnomalType> findAbnomalTypeList() throws Exception {
		List<AbnomalType> list = commonDAO
				.find("from AbnomalType order by id asc");
		return list;
	}
	/**
	 * 选择异常类型
	 * */
	public List<AbnomalType> findAbnomalType() throws Exception {

		Object[] a = {};

		List<AbnomalType> list = commonDAO.find("from AbnomalType  where state='" + SystemConstants.DIC_TYPE_IN_USE
				+ "' ", a);
		return list;

	}
	
	/**
	 * 根据编号查询产品
	 */
	public List<Map<String, String>> findAbnomalTypeName(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = findAbnomalType(code);
		List<AbnomalType> list = (List<AbnomalType>) result.get("list");

		if (list != null && list.size() > 0) {
			for (AbnomalType srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name",srai.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	public Map<String, Object> findAbnomalType(String code) throws Exception {
		String[] codes=code.split(",");
		
		List<AbnomalType> list =new ArrayList<AbnomalType>();
		for(int i=0;i<codes.length;i++){
			AbnomalType pro = commonDAO.get(AbnomalType.class,codes[i].trim());
			list.add(pro);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}
}
