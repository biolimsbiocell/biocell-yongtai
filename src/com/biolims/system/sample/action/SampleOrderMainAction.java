package com.biolims.system.sample.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.dao.SampleOrderChangeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderChange;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleState;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.newsyscode.model.CodeMainNew;
import com.biolims.system.newsyscode.service.CodeMainNewService;
import com.biolims.system.product.model.Product;
import com.biolims.system.sample.service.SampleOrderMainService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.sun.jersey.core.util.Base64;

import net.sourceforge.pinyin4j.PinyinHelper;
import oracle.net.aso.c;
import sun.misc.BASE64Encoder;

@Namespace("/system/sample/sampleOrder")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleOrderMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "980702";
	@Autowired
	private SampleOrderService sampleOrderService;
	@Autowired
	private SampleSearchService sampleSearchService;
	@Autowired
	private SampleOrderChangeDao sampleOrderChangeDao;
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	private SampleOrderMainService sampleOrderMainService;
	private SampleOrder sampleOrder = new SampleOrder();
	// 注入癌症信息的service
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	@Autowired
	private SampleCancerTempService sampleCancerTempService;
	private SampleCancerTemp sampleCancerTempOne = new SampleCancerTemp();// 一录
	private SampleCancerTemp sampleCancerTempTwo = new SampleCancerTemp();// 二录
	// 注入电子病历的service
	@Autowired
	private CrmPatientService crmPatientService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;

	@Resource
	private FieldService fieldService;

	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodeMainNewService codeMainNewService;
	@Resource
	private CommonService commonService;

	@Action(value = "showStep1")
	public String showStep1() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOrder = sampleOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/sample/showStep1.jsp");
	}

	@Action(value = "showStep2")
	public String showStep2() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/sample/showStep2.jsp");
	}

	@Action(value = "showStep3")
	public String showStep3() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/sample/showStep3.jsp");
	}

	/**
	 * 任务看板 @Title: showSampleOrderTaskKanban @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2018年9月7日 @throws
	 */
	@Action(value = "showSampleOrderTaskKanban")
	public String showSampleOrderTaskKanban() throws Exception {
		rightsId = "2811";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/taskKanbanTable.jsp");
	}

	@Action(value = "showSampleOrderTaskKanbanJson")
	public void showSampleOrderTaskKanbanJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOrderService.showSampleOrderTaskKanban(start, length, query, col, sort);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("techJkService-id", "");
			map.put("techJkService-confirmDate", "");
			map.put("id", "");
			map.put("productName", "");
			map.put("newTask", "");
			map.put("barcode", "");
			map.put("drawBloodTime", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SampleOrder");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "showSampleStateNewListJson")
	public void showSampleStatenewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleOrderService.findSampleStateNewList(start, length, query, col, sort, id);
		List<SampleState> list = (List<SampleState>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("stageTime", "");
		map.put("startDate", "");
		map.put("endDate", "");
		map.put("tableTypeId", "");
		map.put("stageName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("taskId", "");

		map.put("taskMethod", "");
		map.put("taskResult", "");
		map.put("techTaskId", "");
		map.put("note", "");
		map.put("note2", "");
		map.put("note3", "");

		map.put("note4", "");
		map.put("note5", "");
		map.put("note6", "");
		map.put("note7", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 查看任务看板 @Title: viewSampleOrderKanban @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2018年9月7日 @throws
	 */
	@Action(value = "viewSampleOrderKanban")
	public String viewSampleOrderKanban() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOrder = sampleOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/sample/taskKanbanEdit.jsp");
	}

	/**
	 * 跟据录入的出生日期回填年龄 @Title: fillAgeByBirthDate @Description: TODO @param @return
	 * void @author 孙灵达 @date 2018年9月7日 @throws
	 */
	@Action(value = "fillAgeByBirthDate")
	public void fillAgeByBirthDate() {
		String birthday = getParameterFromRequest("birth");
		Calendar instance = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String nowDate = simpleDateFormat.format(instance.getTime());
		int age = Integer.parseInt(nowDate.substring(0, 4)) - Integer.parseInt(birthday.substring(0, 4)) + 1;
		String strAge = Integer.toString(age);
		HttpUtils.write(strAge);
	}

	/**
	 * 展示状态完成的订单
	 * 
	 * @Title: showStateCompleteSampleOrderList @Description: TODO @param @return
	 *         void @author 孙灵达 @date 2018年8月28日 @throws
	 */
	/*
	 * @Action(value = "showStateCompleteSampleOrderList") public void
	 * showStateCompleteSampleOrderList() { String draw =
	 * getParameterFromRequest("draw"); try { Map<String, Object> result =
	 * sampleOrderService.showStateCompleteSampleOrderList(); List<SampleOrder> list
	 * = (List<SampleOrder>) result.get("list"); Map<String, String> map = new
	 * HashMap<String, String>(); map.put("id", ""); map.put("name", "");
	 * map.put("gender", ""); map.put("age", ""); map.put("medicalNumber", "");
	 * map.put("stateName", ""); String data = new
	 * SendData().getDateJsonForDatatable(map, list);
	 * HttpUtils.write(PushData.pushData(draw, result, data)); } catch (Exception e)
	 * { e.printStackTrace(); } }
	 */

	/**
	 * @throws Exception 通过id查询订单 @Title: findSampleOrderById @Description:
	 *                   TODO @param @return void @author 孙灵达 @date
	 *                   2018年8月14日 @throws
	 */
	@Action(value = "findSampleOrderById")
	public void findSampleOrderById() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		SampleOrder so = commonDAO.get(SampleOrder.class, id);
		Date birthDate = so.getBirthDate();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String birth = format.format(birthDate);
		map.put("sampleOrder", so);
		map.put("birthDate", birth);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/***
	 * Dialog
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleOrderDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOrderDialogList() throws Exception {
		String flag = getParameterFromRequest("flag");
		String mark = getParameterFromRequest("mark");
		putObjToContext("flag", flag);
		putObjToContext("mark", mark);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderDialog.jsp");
	}

	@Action(value = "showSampleOrderDialogListJson")
	public void showSampleOrderDialogListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOrderService.findSampleOrderDialogList(start, length, query, col, sort);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("productName", "");
			map.put("stateName", "");
			map.put("barcode", "");
			map.put("drawBloodTime", "yyyy-MM-dd HH:mm");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 展示所有得订单
	 * 
	 * @Title: showAllSampleOrderDialogListJson @Description: TODO @param @throws
	 *         Exception @return void @author 孙灵达 @date 2018年8月14日 @throws
	 */
	@Action(value = "showAllSampleOrderDialogListJson")
	public void showAllSampleOrderDialogListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOrderService.findSampleOrderTable(null, start, length, query, col, sort);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("productName", "");
			map.put("stateName", "");
			map.put("barcode", "");
			map.put("drawBloodTime", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showSampleOrderSelList")
	public String showSampleOrderSelList() throws Exception {
		String orderNum = getParameterFromRequest("orderNum");
		putObjToContext("orderNum", orderNum);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderSel.jsp");
	}

	@Action(value = "showSampleOrderSelListJson")
	public void showSampleOrderSelListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String orderNum = getParameterFromRequest("orderNum");
		try {
			Map<String, Object> result = sampleOrderService.findSampleOrderSelTable(start, length, query, col, sort,
					orderNum);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("medicalNumber", "");
			map.put("stateName", "");
			map.put("productId", "");
			map.put("productName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***
	 * 添加到Item表
	 * 
	 * @throws Exception
	 */
	@Action(value = "addPurchaseApplyMakeUp")
	public void addPurchaseApplyMakeUp() throws Exception {
		String id = getParameterFromRequest("id");
		String sampleOrderChangId = getParameterFromRequest("sampleOrderChangId");
		SampleOrderChange sa = new SampleOrderChange();
		if ((sampleOrderChangId != null && !"".equals(sampleOrderChangId)) || sampleOrderChangId.equals("NEW")) {
			String modelName = "SampleOrderChange";
			String markCode = "SOC";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
			sa.setId(autoID);
			sa.setChangeType("取消申请单");
			sa.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sa.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sa.setCreateUser(user);
			sa.setCreateDate(new Date());
			commonDAO.saveOrUpdate(sa);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemId = sampleOrderService.addPurchaseApplyMakeUp(id, sa);
			result.put("success", true);
			result.put("itemId", itemId);
			result.put("id", sa.getId());
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 列表页面
	@Action(value = "showSampleOrderTable")
	public String showSampleOrderTable() throws Exception {
		String type = getRequest().getParameter("type");
		if (type.equals("0")) {// 用户订单
			rightsId = "980702";
		} else {// 客服订单
			rightsId = "980802";
		}
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMain.jsp");
	}

	@Action(value = "showSampleOrderTableJson")
	public void showSampleOrderTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String type = getRequest().getParameter("type");
			Map<String, Object> result = sampleOrderService.findSampleOrderTable(type, start, length, query, col, sort);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("nation", "");
			// map.put("orderType", "");
			map.put("email", "");
			map.put("zipCode", "");
			map.put("diagnosisDate", "");
			map.put("commissioner-id", "");
			map.put("commissioner-name", "");
			map.put("nativePlace", "");
			map.put("medicalNumber", "");
			map.put("familyCode", "");
			map.put("family", "");
			map.put("familyPhone", "");
			map.put("familySite", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("stateName", "");
			map.put("barcode", "");
			map.put("subjectID", "");
			map.put("familyHistorysummary", "");
			map.put("prenatal", "");
			map.put("hospitalPatientID", "");
			map.put("weights", "");
			map.put("gestationalWeeks", "");
			map.put("birthDate", "yyyy-MM-dd HH:mm:ss");
			map.put("createDate", "yyyy-MM-dd HH:mm:ss");
			map.put("idCard", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("fieldContent", "");
			map.put("receiveState", "");
			map.put("ccoi", "");
//			map.put("samplingDate","yyyy-MM-dd HH:mm");
			map.put("drawBloodTime", "yyyy-MM-dd");
			map.put("round", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SampleOrder");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 异步加载代理商名称
	 * 
	 * @return
	 */
	@Action(value = "findPrimaryToSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findPrimaryToSample() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> dataListMap = this.sampleOrderMainService.findPrimaryToSample(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// @Action(value = "sampleOrderSelect", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showDialogSampleOrderList() throws Exception {
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return
	// dispatcher("/WEB-INF/page/system/sample/sampleOrderMainDialog.jsp");
	// }
	//
	// @Action(value = "showDialogSampleOrderListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showDialogSampleOrderListJson() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// Map<String, String> map2Query = new HashMap<String, String>();
	// if (data != null && data.length() > 0)
	// map2Query = JsonUtils.toObjectByJson(data, Map.class);
	// Map<String, Object> result = sampleOrderService.findSampleOrderList(
	// map2Query, startNum, limitNum, dir, sort);
	// Long count = (Long) result.get("total");
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	//
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("birthDate", "yyyy-MM-dd");
	// map.put("diagnosisDate", "");
	// map.put("dicType-id", "");
	// map.put("dicType-name", "");
	// map.put("sampleStage", "");
	// map.put("inspectionDepartment-id", "");
	// map.put("inspectionDepartment-name", "");
	// map.put("crmProduct-id", "");
	// map.put("crmProduct-name", "");
	// map.put("samplingDate", "yyyy-MM-dd");
	// map.put("samplingLocation-id", "");
	// map.put("samplingLocation-name", "");
	// map.put("samplingNumber", "");
	// map.put("pathologyConfirmed", "");
	// map.put("bloodSampleDate", "yyyy-MM-dd");
	// map.put("plasmapheresisDate", "yyyy-MM-dd");
	// map.put("commissioner-id", "");
	// map.put("commissioner-name", "");
	// map.put("receivedDate", "yyyy-MM-dd");
	// map.put("sampleTypeId", "");
	// map.put("sampleTypeName", "");
	// map.put("sampleCode", "");
	// map.put("medicalNumber", "");
	// map.put("familyCode", "");
	// map.put("family", "");
	// map.put("familyPhone", "");
	// map.put("familySite", "");
	// map.put("medicalInstitutions", "");
	// map.put("medicalInstitutionsPhone", "");
	// map.put("medicalInstitutionsSite", "");
	// map.put("attendingDoctor", "");
	// map.put("attendingDoctorPhone", "");
	// map.put("attendingDoctorSite", "");
	// map.put("note", "");
	// map.put("createUser-id", "");
	// map.put("createUser-name", "");
	// map.put("createDate", "yyyy-MM-dd");
	// map.put("confirmUser-id", "");
	// map.put("confirmUser-name", "");
	// map.put("confirmDate", "yyyy-MM-dd");
	// map.put("state", "");
	// map.put("stateName", "");
	// map.put("sampleFlag", "");
	// map.put("successFlag", "");
	// map.put("age", "");
	//
	// map.put("barcode", "");
	// map.put("subjectID", "");
	// map.put("visit", "");
	// map.put("CRCName", "");
	// map.put("CRCPhone", "");
	// map.put("CRCEmail", "");
	// map.put("CRAName", "");
	// map.put("CRAPhone", "");
	// map.put("CRAEmail", "");
	// map.put("sponsor", "");
	// map.put("sponsorProjectID", "");
	// map.put("specimenPurpose", "");
	// map.put("blockID", "");
	// map.put("collectionTime", "yyyy-MM-dd");
	// map.put("sectionTime", "yyyy-MM-dd");
	// map.put("tissueType", "");
	// map.put("siteCollection", "");
	// map.put("collectionMethod", "");
	// map.put("fixedBuffer", "");
	// map.put("histologyType", "");
	// map.put("bloodCollectionTime", "yyyy-MM-dd");
	//
	// map.put("ill", "");
	// map.put("transfusion", "");
	// map.put("sameTime", "");
	// map.put("nativePlace", "");
	// map.put("subjectEmail", "");
	// map.put("subjectPhone", "");
	// map.put("adopted", "");
	// map.put("summary", "");
	// map.put("phenotype", "");
	// map.put("familyHistory", "");
	// map.put("familyHistorysummary", "");
	// map.put("prenatal", "");
	// map.put("Institute", "");
	// map.put("hospitalPatientID", "");
	// map.put("customerAdress", "");
	// map.put("physicianName", "");
	// map.put("physicianPhone", "");
	// map.put("physicianFax", "");
	// map.put("physicianEmail", "");
	// map.put("type", "");
	//
	// map.put("bedNo", "");
	// map.put("diagnosis", "");
	// map.put("guomin", "");
	//
	// new SendData().sendDateJson(map, list, count,
	// ServletActionContext.getResponse());
	// }

	// @Action(value = "showHistorySelectTreeJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showHistorySelectTreeJson() throws Exception {
	// List<DicType> listCheck1 =
	// sampleCancerTempService.findDicSampleType1(null);
	//
	// String a = sampleCancerTempService.getTreeJson(listCheck1);
	//
	// new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// }
	//
	// @Action(value = "showHistorySelectTree")
	// public String showHistorySelectTree() throws Exception{
	//
	//
	// putObjToContext(
	// "path",
	// ServletActionContext.getRequest().getContextPath()
	// + "/system/sample/sampleOrder/showHistorySelectTreeJson.action");
	//
	// return
	// dispatcher("/WEB-INF/page/system/sample/sampleHistorySelectTree.jsp");
	// }

	// @Action(value = "showYongYaoSelectTreeJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showYongYaoSelectTreeJson() throws Exception {
	// List<DicType> listCheck2 =
	// sampleCancerTempService.findDicSampleType2(null);
	//
	// String a = sampleCancerTempService.getTreeJson(listCheck2);
	//
	// new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// }
	//
	// @Action(value = "showYongYaoSelectTree")
	// public String showYongYaoSelectTree(){
	// putObjToContext(
	// "path",
	// ServletActionContext.getRequest().getContextPath()
	// + "/system/sample/sampleOrder/showYongYaoSelectTreeJson.action");
	//
	// return
	// dispatcher("/WEB-INF/page/system/sample/sampleYongYaoSelectTree.jsp");
	// }

	// @Action(value = "showzhengzhuangSelectTreeJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showzhengzhuangSelectTreeJson() throws Exception {
	// List<DicType> listCheck3 =
	// sampleCancerTempService.findDicSampleType3(null);
	//
	// String a = sampleCancerTempService.getTreeJson(listCheck3);
	//
	// new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// }
	//
	// @Action(value = "showzhengzhuangSelectTree")
	// public String showzhengzhuangSelectTree(){
	// putObjToContext(
	// "path",
	// ServletActionContext.getRequest().getContextPath()
	// + "/system/sample/sampleOrder/showzhengzhuangSelectTreeJson.action");
	//
	// return
	// dispatcher("/WEB-INF/page/system/sample/samplezhengzhuangSelectTree.jsp");
	// }

	@Action(value = "editSampleOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editSampleOrder() throws Exception {
		String barCod = getParameterFromRequest("barCod");
		sampleSearchService.insertLog(barCod, "申请单详情");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		String BaseId = getParameterFromRequest("id");
//		byte[] asBytes = Base64.decode(BaseId);
//		String id = new String(asBytes, "utf-8");
		String id = getParameterFromRequest("id");

		String changeId = getParameterFromRequest("changeId");
		String type = getParameterFromRequest("type");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String open = getParameterFromRequest("open");
		// 订单追加申请标识
		String flag = getParameterFromRequest("flag");
		if (type.equals("0")) {
			rightsId = "980701";
		} else {
			rightsId = "980801";
		}
		putObjToContext("type", type);
		// 订单追加申请
		if (flag != null && !"".equals(flag)) {
			long num = 0;
			if (id != null && !id.equals("")) {
				this.sampleOrder = sampleOrderService.get(id);
				if (sampleOrder == null) {
					sampleOrder = new SampleOrder();
					sampleOrder.setId(id);
					User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
					sampleOrder.setCreateUser(user);
					sampleOrder.setCreateDate(df.parse(df.format(new Date())));
					sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
					putObjToContext("changeId", changeId);
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				} else {
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					putObjToContext("changeId", changeId);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					num = fileInfoService.findFileInfoCount(id, "sampleOrder");
				}

			} else {
				sampleOrder.setId("NEW");
				sampleOrder.setOrderType(type);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				sampleOrder.setCreateUser(user);
				sampleOrder.setCreateDate(df.parse(df.format(new Date())));
				sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
				sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				// sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				sampleOrder.setStateName("新建");
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			}
			putObjToContext("flag", flag);
			putObjToContext("fileNum", num);
			putObjToContext("type", type);
			toState(sampleOrder.getState());
		} else {
			long num = 0;
			if (id != null && !id.equals("")) {
				this.sampleOrder = sampleOrderService.get(id);
				if (sampleOrder == null) {
					sampleOrder = new SampleOrder();
					sampleOrder.setId(id);
					User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
					sampleOrder.setCreateUser(user);
					sampleOrder.setCreateDate(df.parse(df.format(new Date())));
					sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
					sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
					sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
					putObjToContext("bpmTaskId", bpmTaskId);
					putObjToContext("changeId", changeId);
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				} else {

					putObjToContext("bpmTaskId", bpmTaskId);
					putObjToContext("changeId", changeId);
					putObjToContext("productId", sampleOrder.getProductId());
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					num = fileInfoService.findFileInfoCount(id, "sampleOrder");
				}

			} else {
				sampleOrder.setId("NEW");
				sampleOrder.setOrderType(type);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				sampleOrder.setCreateUser(user);
				sampleOrder.setCreateDate(df.parse(df.format(new Date())));
				sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
				sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			}
			putObjToContext("flag", flag);
			putObjToContext("fileNum", num);
			putObjToContext("type", type);
			putObjToContext("open", open);
			toState(sampleOrder.getState());
		}
		/*
		 * long num = 0; if (id != null && !id.equals("")) { this.sampleOrder =
		 * sampleOrderService.get(id); if (sampleOrder == null) { sampleOrder = new
		 * SampleOrder(); sampleOrder.setId(id); User user = (User)
		 * this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		 * sampleOrder.setCreateUser(user); sampleOrder.setCreateDate(new Date());
		 * sampleOrder.setReceivedDate(new Date()); sampleOrder.setState
		 * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		 * sampleOrder.setStateName
		 * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
		 * putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		 * toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD); } else {
		 * putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		 * toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY); num =
		 * fileInfoService.findFileInfoCount(id, "sampleOrder"); }
		 * 
		 * } else { sampleOrder.setId("NEW"); User user = (User)
		 * this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		 * sampleOrder.setCreateUser(user); sampleOrder.setCreateDate(new Date());
		 * sampleOrder.setReceivedDate(new Date()); sampleOrder.setState
		 * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		 * sampleOrder.setStateName
		 * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
		 * putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		 * toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD); }
		 * putObjToContext("flag", flag); putObjToContext("fileNum", num);
		 * putObjToContext("type", type); toState(sampleOrder.getState());
		 */
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	}

	/**
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		synchronized (SampleOrderMainAction.class) {
			String dtime = getParameterFromRequest("dtime");
			String sjh = getParameterFromRequest("sjh");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			if (dtime != null) {
				sampleOrder.setDrawBloodTime(sdf.parse(dtime));
			}
			String id = sampleOrder.getId();
			String number = sampleOrder.getMedicalNumber();
			if ("".equals(sampleOrder.getCrmCustomer().getId())) {
				CrmCustomer cu = null;
				sampleOrder.setCrmCustomer(cu);
			}
			String vName = "";
			boolean mark = false;
			StringBuffer sb = new StringBuffer();
			String name = sampleOrder.getName();
			String CCOI = "";// vName + sampleOrder.getFiltrateCode();

			if (name == null || "".equals(name)) {
				CCOI = sampleOrder.getAbbreviation() + "-" + sampleOrder.getFiltrateCode();
			} else {
				String substring = name.substring(0, 1);
				mark = substring.matches("[\u4e00-\u9fa5]");
				if (mark) {// 汉字形式的姓名
					char[] charArray = name.toCharArray();
					for (int i = 0; i < charArray.length; i++) {
						// 获取并转成数组
						String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
						// 拼接
						String simpleName = pinyinStringArray[0].substring(0, 1).toUpperCase();
						sb.append(simpleName);
					}
					vName = sb.toString();
				} else {// 字母简称
					vName = name.toUpperCase();
				}
				sampleOrder.setAbbreviation(vName);
				CCOI = vName + "-" + sampleOrder.getFiltrateCode();
			}
//			// SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
//			if (mark) {// 汉字形式的姓名
//				char[] charArray = name.toCharArray();
//				for (int i = 0; i < charArray.length; i++) {
//					// 获取并转成数组
//					String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
//					// 拼接
//					String simpleName = pinyinStringArray[0].substring(0, 1).toUpperCase();
//					sb.append(simpleName);
//				}
//				vName = sb.toString();
//			} else {// 字母简称
//				vName = name.toUpperCase();
//			}

			sampleOrder.setCcoi(CCOI);
			String log = "";
			if ((id != null && id.equals("")) || id.equals("NEW")) {

				String modelName = "SampleOrder";
				String markCode = "DD";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
				log = "123";
				// String productId = sampleOrder.getProductId();
				// String round = sampleOrder.getRound();
				// Date drawBloodTime = sampleOrder.getDrawBloodTime();
				// String drawBloodTimeStr = format.format(drawBloodTime);
				// String autoID = productId + CCOI + "-" + round + "-" + drawBloodTimeStr;
				sampleOrder.setId(autoID);
				sampleOrder.setReceiveState("0");

			}

			boolean result = crmPatientService.queryByCount(number);
			CrmPatient r = null;

			if (!"".equals(sampleOrder.getMedicalNumber())) {
				r = crmPatientService.get(sampleOrder.getMedicalNumber());
				if (r == null)
					r = new CrmPatient();
				// 该电子病历记录存在: 修改电子病历

				r.setId(sampleOrder.getMedicalNumber());
				r.setName(sampleOrder.getName());
				r.setAge(sampleOrder.getAge());
				r.setGender(sampleOrder.getGender());

				r.setDateOfBirth(sampleOrder.getBirthDate());
				r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
				r.setCreateDate(sampleOrder.getCreateDate());
				r.setCreateUser(sampleOrder.getCreateUser());
				r.setFiltrateCode(sampleOrder.getFiltrateCode());
				r.setCcoi(sampleOrder.getCcoi());
				r.setTumourType(sampleOrder.getZhongliu());
				// 新增加
				r.setRace(sampleOrder.getNation());
				r.setAbbreviation(sampleOrder.getAbbreviation());
				r.setApplicationNum(sampleOrder.getId());
				r.setRound(sampleOrder.getRound());
				r.setHospitalPatientID(sampleOrder.getHospitalPatientID());
				r.setCustomer(sampleOrder.getCrmCustomer());
				r.setCustomerDoctor(sampleOrder.getCrmDoctor());
				r.setKs(sampleOrder.getInspectionDepartment());
				r.setCrmPhone(sampleOrder.getCrmPhone());
				r.setRandomCode(sampleOrder.getRandomCode());
				r.setSfz(sampleOrder.getIdCard());
				r.setPlaceOfBirth(sampleOrder.getNativePlace());
				r.setEmail(sampleOrder.getEmail());
				r.setMedicationPlan(sampleOrder.getMedicationPlan());
				// 病
				r.setHepatitisHbv(sampleOrder.getHepatitisHbv());
				r.setHepatitisHcv(sampleOrder.getHepatitisHcv());
				r.setHivVirus(sampleOrder.getHivVirus());
				r.setSyphilis(sampleOrder.getSyphilis());
				r.setWhiteBloodCellNum(sampleOrder.getWhiteBloodCellNum());
				r.setLymphoidCellSeries(sampleOrder.getLymphoidCellSeries());
				r.setPercentageOfLymphocytes(sampleOrder.getPercentageOfLymphocytes());
				r.setCounterDrawBlood(sampleOrder.getCounterDrawBlood());
				r.setHeparinTube(sampleOrder.getHeparinTube());
				// 家
				r.setFamily(sampleOrder.getFamily());
				r.setFamilyPhone(sampleOrder.getFamilyPhone());
				r.setFamilySite(sampleOrder.getFamilySite());
				r.setZipCode(sampleOrder.getZipCode());
				// 肿瘤分期
				r.setTumorStaging(sampleOrder.getTumorStaging());
				r.setZhongliuNote(sampleOrder.getZhongliuNote());
				r.setTumorStagingNote(sampleOrder.getTumorStagingNote());
				// 检测项目
				r.setCrmProduct(sampleOrder.getCrmProduct());
				r.setProductId(sampleOrder.getProductId());
				r.setProductName(sampleOrder.getProductName());
				crmPatientService.save(r);

			} else {
				// 该电子记录不存在:添加电子病历
				String modelName = "CrmPatient";
				String markCode = "P";
				String autoID = codingRuleService.genTransID(modelName, markCode);

//				sampleOrder.setMedicalNumber(CCOI);
				r = new CrmPatient();
				r.setId(autoID);
				r.setName(sampleOrder.getName());
				r.setGender(sampleOrder.getGender());
				r.setDateOfBirth(sampleOrder.getBirthDate());
				r.setAge(sampleOrder.getAge());
				r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
				r.setCreateDate(sampleOrder.getCreateDate());
				r.setCreateUser(sampleOrder.getCreateUser());
				r.setFiltrateCode(sampleOrder.getFiltrateCode());
				r.setCcoi(sampleOrder.getCcoi());
				// 新增加
				r.setRace(sampleOrder.getNation());
				r.setAbbreviation(sampleOrder.getAbbreviation());
				r.setApplicationNum(sampleOrder.getId());
				r.setRound(sampleOrder.getRound());
				r.setHospitalPatientID(sampleOrder.getHospitalPatientID());
				r.setCustomer(sampleOrder.getCrmCustomer());
				r.setCustomerDoctor(sampleOrder.getCrmDoctor());
				r.setKs(sampleOrder.getInspectionDepartment());
				r.setRandomCode(sampleOrder.getRandomCode());
				r.setSfz(sampleOrder.getIdCard());
				r.setPlaceOfBirth(sampleOrder.getNativePlace());
				r.setEmail(sampleOrder.getEmail());
				r.setMedicationPlan(sampleOrder.getMedicationPlan());
				r.setCrmPhone(sampleOrder.getCrmPhone());
				// 病
				r.setHepatitisHbv(sampleOrder.getHepatitisHbv());
				r.setHepatitisHcv(sampleOrder.getHepatitisHcv());
				r.setHivVirus(sampleOrder.getHivVirus());
				r.setSyphilis(sampleOrder.getSyphilis());
				r.setWhiteBloodCellNum(sampleOrder.getWhiteBloodCellNum());
				r.setLymphoidCellSeries(sampleOrder.getLymphoidCellSeries());
				r.setPercentageOfLymphocytes(sampleOrder.getPercentageOfLymphocytes());
				r.setCounterDrawBlood(sampleOrder.getCounterDrawBlood());
				r.setHeparinTube(sampleOrder.getHeparinTube());
				// 家
				r.setFamily(sampleOrder.getFamily());
				r.setFamilyPhone(sampleOrder.getFamilyPhone());
				r.setFamilySite(sampleOrder.getFamilySite());
				r.setZipCode(sampleOrder.getZipCode());
				// 肿瘤分期
				r.setTumorStaging(sampleOrder.getTumorStaging());
				r.setZhongliuNote(sampleOrder.getZhongliuNote());
				r.setTumorStagingNote(sampleOrder.getTumorStagingNote());
				// 检测项目
				r.setCrmProduct(sampleOrder.getCrmProduct());
				r.setProductId(sampleOrder.getProductId());
				r.setProductName(sampleOrder.getProductName());
				crmPatientService.save(r);
				sampleOrder.setMedicalNumber(autoID);
			}
			Map aMap = new HashMap();
			// 保存病史和用药信息
			// aMap.put("sampleOrderPersonnel",
			// getParameterFromRequest("sampleOrderPersonnelJson"));
			aMap.put("sampleOrderItem", getParameterFromRequest("sampleOrderInfoJson"));
			String changeLog = getParameterFromRequest("changeLog");
			String flag = getParameterFromRequest("flag");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			String string = sampleOrderService.save(sampleOrder, aMap, changeLog, changeLogItem, flag, log);
			String[] split = string.split(",");
			String samId = split[0];
			String changeId = split[1];
			String type = split[2];

			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			// String url =
			// "/system/sample/sampleOrder/editSampleOrder.action?id=" +
			// sampleOrder.getId();

//			BASE64Encoder encoder = new BASE64Encoder();
//			String encode = encoder.encode(samId.getBytes());
			if (sjh.equals("1") && !"".equals(sjh) && sjh != null) {
				String url = "/system/sample/sampleOrder/findSampleOrder.action?id=" + samId + "&changeId=" + changeId
						+ "&type="+type;
				if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
					url += "&bpmTaskId=" + bpmTaskId;
				}
				return redirect(url);
			} else {
				String url = "/system/sample/sampleOrder/editSampleOrder.action?id=" + samId + "&changeId=" + changeId
						+ "&type=" + type;
				if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
					url += "&bpmTaskId=" + bpmTaskId;
				}
				return redirect(url);
			}

		}
	}

	@Action(value = "viewSampleOrder")
	public String toViewSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
//		String BaseId = getParameterFromRequest("id");
//		byte[] asBytes = Base64.decode(BaseId);
//		String id = new String(asBytes, "utf-8");
		String type = getParameterFromRequest("type");
		sampleOrder = sampleOrderService.get(id);
		if (sampleOrder != null) {

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			putObjToContext("productId", sampleOrder.getProductId());
			putObjToContext("type", type);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);

		}
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	}

	@Action(value = "showSampleOrderItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOrderItemList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainItem.jsp");
	}

	@Action(value = "showSampleOrderItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOrderItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleOrderService.findSampleOrderItemTable(start, length, query, col, sort, id);
		List<SampleOrderItem> list = (List<SampleOrderItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("sampleCode", "");
		map.put("slideCode", "");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("samplingDate", "yyyy-MM-dd HH:mm");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd HH:mm");
		map.put("stateName", "");
		map.put("releaseResults", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOrderItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleOrderService.delSampleOrderItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 根据主数据加载子表明细
	// @Action(value = "setSamplePerson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setTemplateItem() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.sampleOrderService
	// .setTemplateItem(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

	// 根据主数据加载子表明细2
	// @Action(value = "setSampleItem", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void setSampleOrderItem() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.sampleOrderService
	// .setSampleOrderItem2(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

	// 查询订单编号是否存在 并查询 是否使用
	@Action(value = "selSampleOrderId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleOrderId() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			int total = this.sampleOrderService.selSampleOrderId(id);
			result.put("success", true);
			result.put("data", total);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按年查询订单柱状图
	@Action(value = "selOrderNumberByYear", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selOrderNumberByYear() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYear = getRequest().getParameter("dateYear");
			List list = this.sampleOrderMainService.selOrderNumberByYear(dateYear);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按年查询订单中检测项目病状图
	@Action(value = "selProductByOrderByYear", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selProductByOrderByYear() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYear = getRequest().getParameter("dateYear");
			List list = this.sampleOrderMainService.selProductByOrderByYear(dateYear);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按年查询订单状态
	@Action(value = "selStateByOrderByYear", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selStateByOrderByYear() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYear = getRequest().getParameter("dateYear");
			List list = this.sampleOrderMainService.selStateByOrderByYear(dateYear);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按月查询订单中检测项目病状图
	@Action(value = "selProductByOrderByMonth", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selProductByOrderByMonth() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYearMonth = getRequest().getParameter("dateYearMonth");
			List list = this.sampleOrderMainService.selProductByOrderByMonth(dateYearMonth);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询订单图片路径
	@Action(value = "selPic", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selPic() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = getRequest().getParameter("id");
			List<String> list = this.sampleOrderMainService.selPic(id);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveItem")
	public void saveItem() throws Exception {
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("dataJson");
		sampleOrder = commonDAO.get(SampleOrder.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sampleOrderService.saveSampleOrderItem(sampleOrder, dataJson, changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
		/*
		 * Map<String, Object> aMap = new HashMap<String, Object>(); String id =
		 * getParameterFromRequest("id"); sampleOrder = commonDAO.get(SampleOrder.class,
		 * id); Map<String, Object> map = new HashMap<String, Object>();
		 * aMap.put("sampleOrderItem", getParameterFromRequest("dataJson")); try {
		 * sampleOrderService.save(sampleOrder, aMap); map.put("success", true); } catch
		 * (Exception e) { map.put("success", false); }
		 * HttpUtils.write(JsonUtils.toJsonString(map));
		 */
	}

	// 订单左侧显示图片
	@Action(value = "showPic")
	public String showPic() throws Exception {
		// putObjToContext("handlemethod",
		// SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/changeView/index.html");
	}

	// 批量上传csv
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sampleOrderMainService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "searchSampleOrder")
	public String searchSampleOrder() throws Exception {
		String type = getRequest().getParameter("type");
		if (type.equals("0")) {// 用户
			rightsId = "980703";
		} else {
			rightsId = "980803";
		}
		putObjToContext("type", type);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleMainSearch.jsp");
	}

	@Action(value = "searchOptions")
	public void searchOptions() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String type = getRequest().getParameter("type");
		try {
			map = sampleOrderService.searchOptions(type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 查询子表数据
	@Action(value = "selectSampleOrder")
	public void selectSampleOrder() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<SampleOrderItem> list = sampleOrderService.selectSampleOrder();
			map.put("data", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "sampleOrderChart")
	public void sampleOrderChart() throws Exception {
		String query = getRequest().getParameter("query");
		String year = getParameterFromRequest("year");
		String month = getParameterFromRequest("month");
		String[] product = getRequest().getParameterValues("product[]");
		String[] customer = getRequest().getParameterValues("customer[]");
		String[] sale = getRequest().getParameterValues("sale[]");
		String[] doctor = getRequest().getParameterValues("doctor[]");

		String type = getRequest().getParameter("type");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = sampleOrderService.sampleOrderChart(type, query, year, month, product, customer, sale, doctor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "sampleOrderSearchTable")
	public void sampleOrderSearchTable() {
		String year = getParameterFromRequest("year");
		String month = getParameterFromRequest("month");
		String[] product = getRequest().getParameterValues("product[]");
		String[] customer = getRequest().getParameterValues("customer[]");
		String[] sale = getRequest().getParameterValues("sale[]");
		String[] doctor = getRequest().getParameterValues("doctor[]");
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			Map<String, Object> result = sampleOrderService.sampleOrderSearchTable(type, year, month, product, customer,
					sale, doctor, start, length, query, col, sort);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("nation", "");
			map.put("email", "");
			map.put("zipCode", "");
			map.put("diagnosisDate", "");
			map.put("commissioner-id", "");
			map.put("commissioner-name", "");
			map.put("nativePlace", "");
			map.put("medicalNumber", "");
			map.put("familyCode", "");
			map.put("family", "");
			map.put("familyPhone", "");
			map.put("familySite", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("stateName", "");
			map.put("barcode", "");
			map.put("subjectID", "");
			map.put("familyHistorysummary", "");
			map.put("prenatal", "");
			map.put("hospitalPatientID", "");
			map.put("weights", "");
			map.put("gestationalWeeks", "");
			map.put("birthDate", "");
			map.put("createDate", "");
			map.put("idCard", "");
			map.put("productName", "");
			map.put("fieldContent", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SampleOrder");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "showImg")
	public String showPicture() throws Exception {
		String id = getParameterFromRequest("id");
		List<FileInfo> list = sampleOrderService.showPicture(id);
		putObjToContext("fList", list);
		return dispatcher("/lims/changeView/index.jsp");
	}

	/**
	 * 
	 * @Title: 输入采血管只数动态生成数据 @Description: TODO @param @return @param @throws
	 *         Exception @return String @author @date 2019年3月17日 @throws
	 */
	@Action(value = "saveNumSampleOrderItem")
	public void saveNumSampleOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String barCode = getParameterFromRequest("barCode");
		int number = Integer.parseInt(getParameterFromRequest("number"));
		SampleOrder sampleOrder = sampleOrderService.get(id);
		if (sampleOrder != null) {
			String num = "";
			int nums = 0;
			Integer countItem = sampleOrderService.countSampleOrderItemNum(id);
			if (countItem == 0) {
				barCode = barCode + "-1";
				for (int i = 0; i < number; i++) {
					SampleOrderItem item = new SampleOrderItem();
					item.setName(sampleOrder.getName());
					item.setGender(sampleOrder.getGender());
					item.setSampleCode(sampleOrder.getSampleCode());
					item.setSampleOrder(sampleOrder);
					// 实现样本条码号 自增
					String num2 = barCode.substring(0, barCode.length() - 1);
					int nus = Integer.parseInt(barCode.substring(barCode.length() - 1));
					nums = nus + i;
					if (nums < 10) {
						item.setSlideCode(num2 + "0" + Integer.toString(nums));
					} else {
						item.setSlideCode(num2 + Integer.toString(nums));
					}
					sampleOrderService.saveUploadOrdItem(item);
				}
			} else {
				String maxCode = sampleOrderService.selectMaxSlideCode(id);
				Integer length = maxCode.length();
				String qCode = maxCode.substring(0, length - 2);
				String hCode = maxCode.substring(length - 2);
				String hNewCode = "";
				int a = Integer.valueOf(hCode) + 1;
				if (a < 10) {
					hNewCode = "0" + a;
				} else {
					hNewCode = a + "";
				}
				barCode = qCode + hNewCode;
				for (int i = 0; i < number; i++) {
					SampleOrderItem item = new SampleOrderItem();
					item.setName(sampleOrder.getName());
					item.setGender(sampleOrder.getGender());
					item.setSampleCode(sampleOrder.getSampleCode());
					item.setSampleOrder(sampleOrder);
					// 实现样本条码号 自增
					String num2 = barCode.substring(0, barCode.length() - 2);
					int nus = Integer.parseInt(barCode.substring(barCode.length() - 2));
					nums = nus + i;
					if (nums < 10) {
						item.setSlideCode(num2 + "0" + Integer.toString(nums));
					} else {
						item.setSlideCode(num2 + Integer.toString(nums));
					}
					sampleOrderService.saveUploadOrdItem(item);
				}
			}
		}
		map.put("success", true);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleOrderService getSampleOrderService() {
		return sampleOrderService;
	}

	public void setSampleOrderService(SampleOrderService sampleOrderService) {
		this.sampleOrderService = sampleOrderService;
	}

	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/**
	 * 癌症信息审核的get和set方法
	 * 
	 * @return
	 */
	public SampleCancerTempService getSampleCancerTempService() {
		return sampleCancerTempService;
	}

	public void setSampleCancerTempService(SampleCancerTempService sampleCancerTempService) {
		this.sampleCancerTempService = sampleCancerTempService;
	}

	public SampleCancerTemp getSampleCancerTempOne() {
		return sampleCancerTempOne;
	}

	public void setSampleCancerTempOne(SampleCancerTemp sampleCancerTempOne) {
		this.sampleCancerTempOne = sampleCancerTempOne;
	}

	public SampleCancerTemp getSampleCancerTempTwo() {
		return sampleCancerTempTwo;
	}

	public CrmPatientService getCrmPatientService() {
		return crmPatientService;
	}

	public void setCrmPatientService(CrmPatientService crmPatientService) {
		this.crmPatientService = crmPatientService;
	}

	public void setSampleCancerTempTwo(SampleCancerTemp sampleCancerTempTwo) {
		this.sampleCancerTempTwo = sampleCancerTempTwo;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}

	@Action(value = "changeSampleOrderState", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void changeSampleOrderState() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String result = getParameterFromRequest("result");
		try {
			sampleOrderMainService.changeSampleOrderState(id, result);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		Boolean state = sampleOrderService.findTypeName(id);

		if (state) {
			map.put("sorry", false);
		} else {
			map.put("sorry", true);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "change", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void change() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		Boolean state = sampleOrderService.findTypeName(id);
		if (state) {
			map.put("sorry", false);
		} else {
			map.put("sorry", true);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
		System.out.println("aaaa");

	}

	@Action(value = "getSampleBarCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getSampleBarCode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			String result = sampleOrderMainService.getSampleBarCode(id);
			map.put("result", result);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "selectAgreementTasks", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectAgreementTasks() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/selectAgreementTasks.jsp");
	}

	@Action(value = "selectAgreementTaskTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectAgreementTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleOrderMainService.selectAgreementTaskTableJson(start, length, query, col,
				sort);
		List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("principal", "");
		map.put("createUser-name", "");
		map.put("phone", "");
		map.put("createDate", "yyyy-MM-dd");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 判断条形码是否重复
	 * 
	 **/
	@Action(value = "slideCode")
	public void findSlideCode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			List<SampleOrderItem> result = sampleOrderMainService.getSlideCode(id);
			if (result.size() > 0) {
				map.put("result", true);
			} else {
				map.put("result", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	/**
	 * 判断条形码是否重复 有id的
	 * 
	 **/
	@Action(value = "slideCode1")
	public void findSlideCode1() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String idold = getParameterFromRequest("idold");
		try {
			List<SampleOrderItem> result = sampleOrderMainService.getSlideCode(id);
			if (result.size() > 0) {
				SampleOrderItem soi = result.get(0);
				if (idold.equals(soi.getId())) {
					map.put("result", false);
				} else {
					map.put("result", true);
				}
			} else {
				map.put("result", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "changOrderState", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void changOrderState() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			sampleOrderMainService.changOrderState(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据CCOI判断 产品个数是否重复
	 * 
	 * @throws Exception
	 */
	@Action(value = "panduanBarcodeNum")
	public void panduanBarcodeNum() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String CCOI = getParameterFromRequest("CCOI");
		String barcodeNum = getParameterFromRequest("barcodeNum");
		String productId = getParameterFromRequest("productId");
		String round = getParameterFromRequest("round");
		String drawBloodTime = getParameterFromRequest("drawBloodTime");
		String id = getParameterFromRequest("id");

		String name = getParameterFromRequest("name");
		String abbreviation = getParameterFromRequest("abbreviation");
		String filtrateCode = getParameterFromRequest("filtrateCode");
		if (abbreviation != null && !"".equals(abbreviation)) {
			CCOI = abbreviation + "-" + filtrateCode;
		} else {
			String vName = "";
			boolean mark = false;
			StringBuffer sb = new StringBuffer();
			String substring = name.substring(0, 1);
			mark = substring.matches("[\u4e00-\u9fa5]");
			if (mark) {// 汉字形式的姓名
				char[] charArray = name.toCharArray();
				for (int i = 0; i < charArray.length; i++) {
					// 获取并转成数组
					String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
					// 拼接
					String simpleName = pinyinStringArray[0].substring(0, 1).toUpperCase();
					sb.append(simpleName);
				}
				vName = sb.toString();
			} else {// 字母简称
				vName = name.toUpperCase();
			}
			CCOI = vName + "-" + filtrateCode;
		}

		List<SampleOrder> soList = new ArrayList<SampleOrder>();
		soList = sampleReceiveDao.selSampleOrderCCOIProduct(CCOI, productId, id);
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date drawBloodTimes = sdf1.parse(drawBloodTime);
		String drawBloodTimeStr = sdf.format(drawBloodTimes);
		String lc = "";
		if (round.length() == 1) {
			lc = "0" + round;
		} else {
			lc = round;
		}
		String barcode = "";
		if (barcodeNum != null && !"".equals(barcodeNum)) {

		} else {
			if (soList.size() == 0) {
				barcode = codingRuleService.getSampleOrderBarcode("SampleOrder", productId, 0000, 4, null);
				// Integer countBatch = sampleReceiveDao.selBarcodeLikeProduct(so);
			} else {
				if (soList.get(0).getBarcode() != null && !"".equals(soList.get(0).getBarcode())) {
					barcode = soList.get(0).getBarcode().substring(0, soList.get(0).getBarcode().indexOf("-"));// .getBarCode().substring(so.getBarcode().indexOf("-"));
				} else {
					barcode = codingRuleService.getSampleOrderBarcode("SampleOrder", productId, 0000, 4, null);
				}
			}
			barcodeNum = barcode.substring(barcode.length() - 4, barcode.length());
		}
		try {
			List<SampleOrder> result = sampleOrderMainService.panduanBarcodeNum(productId + barcodeNum);
			if (result.size() > 0) {
				Boolean panduan = true;
				for (SampleOrder so : result) {
					if (CCOI.equals(so.getCcoi())) {

					} else {
						panduan = false;
					}
				}
				map.put("result", panduan);
				map.put("success", true);
			} else {
				map.put("result", true);
				map.put("success", true);
			}

			List<SampleOrder> results = sampleOrderMainService
					.panduanBarcodeAndid(productId + barcodeNum + "-" + lc + "-" + drawBloodTimeStr, id);
			if (results.size() > 0) {
				map.put("barcodeCf", productId + barcodeNum + "-" + lc + "-" + drawBloodTimeStr);
				map.put("cf", false);
			} else {
				map.put("cf", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断姓名简写
	 * 
	 * @throws Exception
	 */
	@Action(value = "abbreviation")
	public void findAbbreviation() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("abbreviation");
		String sid = getRequest().getParameter("sid");

		List<SampleOrder> result = sampleOrderMainService.findAbbreviation(id, sid);
		if (result.size() > 0) {
			map.put("result", true);
			map.put("success", true);
		} else {
			map.put("result", false);
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断筛选号是否相同
	 * 
	 * @throws Exception
	 */
	@Action(value = "filtrateCode")
	public void findFiltrateCode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("filtrateCode");
		String sid = getRequest().getParameter("sid");
		try {
			List<SampleOrder> result = sampleOrderMainService.findFiltrateCode(id, sid);
			if (result.size() > 0) {
				map.put("result", true);
			} else {
				map.put("result", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断随机号是否相同
	 * 
	 * @throws Exception
	 */
	@Action(value = "randomCode")
	public void findRandomCode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("randomCode");
		String sid = getRequest().getParameter("sid");
		try {
			List<SampleOrder> result = sampleOrderMainService.findRandomCode(id, sid);
			if (result.size() > 0) {
				map.put("result", true);
			} else {
				map.put("result", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 打印机
	@Action(value = "dyMakeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String[] ids = getRequest().getParameterValues("ids[]");
		String printCode = getParameterFromRequest("printCode");
		Map<String, Object> map = new HashMap<String, Object>();
		for (String id : ids) {
			SampleOrderItem item = commonDAO.get(SampleOrderItem.class, id);
			String sampleCode = item.getSlideCode();
			String slideCode = item.getSlideCode();
			int printNum = Integer.parseInt("1");
			for (int i = 0; i < printNum; i++) {
				CodeMainNew codeMain = codeMainNewService.get(printCode);
				if (codeMain != null) {
					String codeFull = sampleCode;
					String code1 = sampleCode.substring(0, id.length() - 13);
					String code2 = sampleCode.substring(id.length() - 13);
					String printStr = codeMain.getCode();
					printStr = printStr.replaceAll("@@code1@@", code1);
					printStr = printStr.replaceAll("@@code2@@", code2);
					printStr = printStr.replaceAll("@@code@@", codeFull);
					printStr = printStr.replaceAll("@@name@@", slideCode);
					String ip = codeMain.getIp();
					Socket socket = null;
					OutputStream os;
					try {
						System.out.println(printStr);
						socket = new Socket();
						SocketAddress sa = new InetSocketAddress(ip, 9100);
						socket.connect(sa);
						os = socket.getOutputStream();
						os.write(printStr.getBytes("UTF-8"));
						os.flush();
						map.put("success", true);
					} catch (UnknownHostException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						if (socket != null) {
							try {
								socket.close();
							} catch (IOException e) {
							}
						}
					}
				}
			}
		}

		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	public String dy(String[] code, CodeMain c) {
		int length = code.length;
		int n = 0;
		if (length % 3 > 0) {
			n = length / 3 + 1;
		} else {
			n = length / 3;
		}
		String context = "";
		int a = 0;
		for (int i = 0; i < n; i++) {
			String str = "";
			if (length > a) {
				str += "#" + code[a] + "&" + code[a].substring(0, 8) + "&" + code[a].substring(8, code[a].length());
				a++;
				if (length > a) {
					str += "&" + code[a] + "&" + code[a].substring(0, 8) + "&" + code[a].substring(8, code[a].length());
					a++;
					if (length > a) {
						str += "&" + code[a] + "&" + code[a].substring(0, 8) + "&"
								+ code[a].substring(8, code[a].length());
						a++;
					} else {
						str += "&&&";
					}
				} else {
					str += "&&&&&&";
				}
			}

			String dyCode = c.getCode();
			String d = dyCode.replaceAll("~~~AAA~~~", str);
			context += d;
		}

		return context;
	}

	public static void main(String[] args) throws Exception {
		String[] code = { "WB123456723", "WB123456543", "WB12345676543" };
		int length = code.length;
		int n = 0;
		if (length % 3 > 0) {
			n = length / 3 + 1;
		} else {
			n = length / 3;
		}
		String context = "";
		int a = 0;
		for (int i = 0; i < n; i++) {
			context += "INPUT ON\r\n";
			context += "CLL\r\n";
			context += "FORMAT INPUT \"#\",\"@\",\"&\"\r\n";
			context += "LAYOUT INPUT \"tmp:label1\"\r\n";

			context += "PP 160,90:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 6\r\n";
			context += "PT \"NextCODE Sample\"\r\n";
			context += "PP 30,100:AN7\r\n";
			context += "BARSET \"QRCODE\",1,1,4,2,1\r\n";
			context += "PB VAR1$\r\n";
			context += "PP 160,80:NASC 6\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR2$\r\n";
			context += "PP 160,50:NASC 6\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR3$\r\n";

			context += "PP 560,120:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 6\r\n";
			context += "PT \"NextCODE Sample\"\r\n";
			context += "PP 440,100:AN7\r\n";
			context += "BARSET \"QRCODE\",1,1,4,2,1\r\n";
			context += "PB VAR4$\r\n";
			context += "PP 560,80:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR5$\r\n";
			context += "PP 560,50:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR6$\r\n";

			context += "PP 970,120:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 6\r\n";
			context += "PT \"NextCODE Sample\"\r\n";
			context += "PP 850,100:AN7\r\n";
			context += "BARSET \"QRCODE\",1,1,4,2,1\r\n";
			context += "PB VAR7$\r\n";
			context += "PP 970,80:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR8$\r\n";
			context += "PP 970,50:NASC 8\r\n";
			context += "FT \"Univers\"\r\n";
			context += "FONTSIZE 8\r\n";
			context += "PT VAR9$\r\n";

			context += "LAYOUT END$\r\n";
			context += "SETUP \"MEDIA,MEDIA SIZE,XSTART,0\"\r\n";
			context += "SETUP 'Printing,Media,Print Area,Media Width,1505\"\r\n";
			context += "SETUP \"Printing,Media,Print Area,Media Length,140\"\r\n";
			context += "LAYOUT RUN \"tmp:label1\"\r\n";
			if (length > a) {
				context += "#" + code[a] + "&" + code[a].substring(0, 8) + "&" + code[a].substring(8, code[a].length());
				a++;
				if (length > a) {
					context += "&" + code[a] + "&" + code[a].substring(0, 8) + "&"
							+ code[a].substring(8, code[a].length());
					a++;
					if (length > a) {
						context += "&" + code[a] + "&" + code[a].substring(0, 8) + "&"
								+ code[a].substring(8, code[a].length());
						a++;
					} else {
						context += "&&&";
					}
				} else {
					context += "&&&&&&";
				}
			}
			context += "&@\r\n";
			context += "PF\r\n";
			context += "KILL \"tmp:label1\"\r\n";
			context += "INPUT OFF\r\n";
		}
		Socket socket = null;
		OutputStream os;
		try {
			socket = new Socket();
			// SocketAddress sa = new InetSocketAddress("10.21.9.172", 9100);
			SocketAddress sa = new InetSocketAddress("10.21.9.222", 9100);
			socket.connect(sa);
			os = socket.getOutputStream();
			os.write(context.getBytes("GBK"));
			os.flush();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * 判断是否跨轮次
	 * 
	 * @throws Exception
	 */
	@Action(value = "round")
	public void findRound() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String round = getParameterFromRequest("round");
		String state = getParameterFromRequest("state");
		String filtrateCode = getParameterFromRequest("filtrateCode");
		String id = getParameterFromRequest("id");
		try {
			//查询改筛选号有没有订单
			List<SampleOrder> list = sampleOrderMainService.findSamleDingDan(filtrateCode);
			if(!list.isEmpty()) {
				//查该轮次,该筛选号的订单id
				SampleOrder soOrder = sampleOrderMainService.findDingDanId(round,filtrateCode);
				if(soOrder!=null) {
					if(soOrder.getId().equals(id)) {
						//id一样不查
					}else {
						//查询该筛选号的最大轮次
						Object a = sampleOrderMainService.findRound(round, state,filtrateCode);
						map.put("round", a);
						map.put("success", true);
					}
				}else {
					//查询该筛选号的最大轮次
					Object a = sampleOrderMainService.findRound(round, state,filtrateCode);
					map.put("round", a);
					map.put("success", true);
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "findSampleOrder")
	public String findSampleOrder() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String id = getParameterFromRequest("id");
		String changeId = getParameterFromRequest("changeId");
		String type = getParameterFromRequest("type");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String open = getParameterFromRequest("open");
		String round = getParameterFromRequest("round");
		String filtrateCode = getParameterFromRequest("filtrateCode");
		String idCode = getParameterFromRequest("idCode");
		String sjh = getParameterFromRequest("sjh");
		// 订单追加申请标识
		String flag = getParameterFromRequest("flag");
		if (type.equals("0")) {
			rightsId = "980701";
		} else {
			rightsId = "980801";
		}
		putObjToContext("type", type);
		putObjToContext("sjh", sjh);
		SampleOrder soo = commonService.get(SampleOrder.class, idCode);
		// 订单追加申请
		if (flag != null && !"".equals(flag)) {
			long num = 0;
			if (id != null && !id.equals("")) {
				this.sampleOrder = sampleOrderService.get(id);
				if (sampleOrder == null) {
					sampleOrder = new SampleOrder();
					sampleOrder.setId(id);
					User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
					sampleOrder.setCreateUser(user);
					sampleOrder.setCreateDate(df.parse(df.format(new Date())));
					sampleOrder.setReceivedDate(df.parse(df.format(new Date())));

					putObjToContext("changeId", changeId);
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				} else {
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					putObjToContext("changeId", changeId);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					num = fileInfoService.findFileInfoCount(id, "sampleOrder");
				}

			} else {
				sampleOrder.setId("NEW");
				sampleOrder.setOrderType(type);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				sampleOrder.setCreateUser(user);
				sampleOrder.setCreateDate(df.parse(df.format(new Date())));
				sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
				sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				// sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				sampleOrder.setStateName("新建");
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			}
			putObjToContext("flag", flag);
			putObjToContext("fileNum", num);
			putObjToContext("type", type);
			toState(sampleOrder.getState());
		} else {
			long num = 0;
			if (id != null && !id.equals("")) {
				this.sampleOrder = sampleOrderService.get(id);
				if (sampleOrder == null) {
					sampleOrder = new SampleOrder();
					sampleOrder.setId(id);
					User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
					sampleOrder.setCreateUser(user);
					sampleOrder.setCreateDate(df.parse(df.format(new Date())));
					sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
					sampleOrder.setFiltrateCode(filtrateCode);
					sampleOrder.setRound(round);
					sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
					sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
					if (soo != null) {
						sampleOrder.setProductName(soo.getProductName());
						sampleOrder.setProductId(soo.getProductId());
						sampleOrder.setCrmProduct(soo.getCrmProduct());
						sampleOrder.setMedicalNumber(soo.getMedicalNumber());
						sampleOrder.setName(soo.getName());
						sampleOrder.setAbbreviation(soo.getAbbreviation());
						sampleOrder.setBirthDate(soo.getBirthDate());
						sampleOrder.setNation(soo.getNation());
						sampleOrder.setNativePlace(soo.getNativePlace());
						sampleOrder.setGender(soo.getGender());
						sampleOrder.setAge(soo.getAge());
						sampleOrder.setIdCard(soo.getIdCard());
						sampleOrder.setRandomCode(soo.getRandomCode());
						sampleOrder.setHospitalPatientID(soo.getHospitalPatientID());
						sampleOrder.setDrawBloodTime(soo.getDrawBloodTime());
						sampleOrder.setStartBackTime(soo.getStartBackTime());
						sampleOrder.setFeedBackTime(soo.getFeedBackTime());
						sampleOrder.setHepatitisHbv(soo.getHepatitisHbv());
						sampleOrder.setHepatitisHcv(soo.getHepatitisHcv());
						sampleOrder.setHivVirus(soo.getHivVirus());
						sampleOrder.setSyphilis(soo.getSyphilis());
						sampleOrder.setWhiteBloodCellNum(soo.getWhiteBloodCellNum());
						sampleOrder.setPercentageOfLymphocytes(soo.getPercentageOfLymphocytes());
						sampleOrder.setLymphoidCellSeries(soo.getLymphoidCellSeries());
						sampleOrder.setCounterDrawBlood(soo.getCounterDrawBlood());
						sampleOrder.setHeparinTube(soo.getHeparinTube());
						sampleOrder.setZhongliu(soo.getZhongliu());
						sampleOrder.setZhongliuNote(soo.getZhongliuNote());
						sampleOrder.setTumorStaging(soo.getTumorStaging());
						sampleOrder.setTumorStagingNote(soo.getTumorStagingNote());
						sampleOrder.setCrmCustomer(soo.getCrmCustomer());
						sampleOrder.setCrmDoctor(soo.getCrmDoctor());
						sampleOrder.setInspectionDepartment(soo.getInspectionDepartment());
						sampleOrder.setCrmPhone(soo.getCrmPhone());
						sampleOrder.setMedicalInstitutionsSite(soo.getMedicalInstitutionsSite());
						sampleOrder.setAttendingDoctor(soo.getAttendingDoctor());
						sampleOrder.setAttendingDoctorPhone(soo.getAttendingDoctorPhone());
						sampleOrder.setAttendingDoctorSite(soo.getAttendingDoctorSite());
						sampleOrder.setMedicationPlan(soo.getMedicationPlan());
						sampleOrder.setFamily(soo.getFamily());
						sampleOrder.setFamilyPhone(soo.getFamilyPhone());
						sampleOrder.setFamilySite(soo.getFamilySite());
						sampleOrder.setEmail(soo.getEmail());
						sampleOrder.setZipCode(soo.getZipCode());
						sampleOrder.setConfirmUser(soo.getConfirmUser());
						sampleOrder.setConfirmDate(soo.getConfirmDate());
						sampleOrder.setCancel(soo.getCancel());
					}
					putObjToContext("bpmTaskId", bpmTaskId);
					putObjToContext("changeId", changeId);
					putObjToContext("productId", sampleOrder.getProductId());
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				} else {
					if (filtrateCode != null && !"".equals(filtrateCode) && round != null && !"".equals(round)) {
						sampleOrder.setFiltrateCode(filtrateCode);
						sampleOrder.setRound(round);
					}
					putObjToContext("bpmTaskId", bpmTaskId);
					putObjToContext("changeId", changeId);
					putObjToContext("productId", sampleOrder.getProductId());
					putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
					num = fileInfoService.findFileInfoCount(id, "sampleOrder");
				}

			} else {
				sampleOrder.setId("NEW");
				sampleOrder.setOrderType(type);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				sampleOrder.setCreateUser(user);
				sampleOrder.setCreateDate(df.parse(df.format(new Date())));
				sampleOrder.setReceivedDate(df.parse(df.format(new Date())));
				sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			}
			putObjToContext("flag", flag);
			putObjToContext("fileNum", num);
			putObjToContext("type", type);
			putObjToContext("open", open);
			putObjToContext("sjh", sjh);
			toState(sampleOrder.getState());
		}
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	}
	
	
	
	/**
	 *	明细下载CSV模板文件
     * @Title: downloadCsvFile  
     * 
     * @throws
	 */
	@Action(value = "downloadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadCsvFile() throws Exception {
		String mainTable_id=getParameterFromRequest("id");
				
		Map<String, Object> result = new HashMap<String, Object>();
		Date date= new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("system.properties");

		properties.load(is);
				
		String filePath = properties.getProperty("sampleOrderItem.path")+"\\";// 写入csv路径
		String fileName = filePath + "sampleOrderItem" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
				parent.mkdirs();
		} else {
				parent.delete();
				parent.mkdirs();
		}
			csvFile.createNewFile();
				// GB2312使正确读取分隔符","
				csvWtriter = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(csvFile), "GBK"), 1024);
				// 写入文件头部
				Object[] head = {"样本接收条形码"};
				List<Object> headList = Arrays.asList(head);
				for (Object data : headList) {
					StringBuffer sb = new StringBuffer();
					String rowStr = sb.append("\"").append(data).append("\",")
							.toString();
					csvWtriter.write(rowStr);
				}
				csvWtriter.newLine();
//				SampleOrder sampleOrder2 = commonService.get(SampleOrder.class, mainTable_id);
				List<SampleOrderItem> querySampleItem = sampleOrderService.querySampleItem(mainTable_id);
				for (int i = 0; i < querySampleItem.size(); i++) {
					StringBuffer sb = new StringBuffer();
					setMolecularMarkersData(querySampleItem.get(i), sb);
					String rowStr = sb.toString();
					csvWtriter.write(rowStr);
					csvWtriter.newLine();
					result.put("success", true);

			}
				csvWtriter.flush(); 
				csvWtriter.close();
				//HttpUtils.write(JsonUtils.toJsonString(result));
				downLoadTemp3("sampleOrderItem", filePath);
	}
			
			/**
			 * 
			 * @Title: setMolecularMarkersData  
			 * @Description: 数据添加到csv内
			 * @author qi.yan
			 * @date 2018-12-24上午11:53:44
			 * 
			 */
			public void setMolecularMarkersData(SampleOrderItem sr, StringBuffer sb)
					throws Exception {
				if (!"".equals(sr.getSlideCode())&&sr.getSlideCode() !=null) {
					sb.append("\"").append(sr.getSlideCode()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				} 
			}
			/**
			 * 
			 * @Title: downLoadTemp3  
			 * @Description: 下载文件
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:26
			 * @param a
			 * @param filePath2
			 * @throws Exception
			 * void
			 * @throws
			 */
			@Action(value = "downLoadTemp3")
			public void downLoadTemp3(String a, String filePath2) throws Exception {
				String filePath = filePath2;// 保存窗口中显示的文件名
				String fileName = a + ".csv";// 保存窗口中显示的文件名
				super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

				/*
				 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
				 */
				ServletOutputStream out = null;
				// PrintWriter out = null;
				InputStream inStream = null;
				try {
					fileName = super.getResponse().encodeURL(
							new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

					super.getResponse().setHeader("Content-Disposition",
							"attachment; filename=\"" + fileName + "\"");
					// inline
					out = super.getResponse().getOutputStream();

					inStream = new FileInputStream(filePath + toUtf8String(fileName));

					// 循环取出流中的数据
					byte[] b = new byte[1024];
					int len;
					while ((len = inStream.read(b)) > 0)
						out.write(b, 0, len);
					super.getResponse().setStatus(super.getResponse().SC_OK);
					super.getResponse().flushBuffer();

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (RuntimeException e) {
					e.printStackTrace();
				} finally {
					if (out != null)
						out.close();
					inStream.close();
				}
			}
			/**
			 * 
			 * @Title: toUtf8String  
			 * @Description: 解决乱码问题
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:11
			 * @param s
			 * @return
			 * String
			 * @throws
			 */
			public static String toUtf8String(String s) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < s.length(); i++) {
					char c = s.charAt(i);
					if (c >= 0 && c <= 255) {
						sb.append(c);
					} else {
						byte[] b;
						try {
							b = Character.toString(c).getBytes("utf-8");
						} catch (Exception ex) {
							System.out.println(ex);
							b = new byte[0];
						}
						for (int j = 0; j < b.length; j++) {
							int k = b[j];
							if (k < 0)
								k += 256;
							sb.append("%" + Integer.toHexString(k).toUpperCase());
						}
					}
				}
				return sb.toString();
			}


}
