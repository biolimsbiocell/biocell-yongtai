package com.biolims.system.sample.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.sample.model.SampleState;
import com.biolims.system.sample.service.SampleMainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/sample/newSampleState")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class NewSampleStateAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258673456786L;
	private String rightsId = "newState";
	@Autowired
	private SampleMainService sampleMainService;
	
	
	
	
	//新页面样本状态返回jsp页面
	@Action(value = "showSampleStateForOneList")
	public String showSampleStateForOneList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/samplestateForOne.jsp");
	}
	
	//查询样本状态进度--单条样本
	@Action(value = "selSampleStateForOneList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleStateForOneList() throws Exception {
		String sampleCode = getRequest().getParameter("sampleCode");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SampleState> list = this.sampleMainService
					.selSampleStateForOne(sampleCode);
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	//跟据订单查询状态
	@Action(value = "selectSampleStateOneForOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectSampleStateOneForOrder() throws Exception {
		String sampleOrder = getRequest().getParameter("sampleOrder");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SampleState> list = this.sampleMainService
					.selectSampleStateOneForOrder(sampleOrder);
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	
	
	//查询所有样本进度
	@Action(value = "selNewSampleInfoState")
	public String selNewSampleInfoState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleMainSchedule.jsp");
	}
	
	//查询所有样本进度
	@Action(value = "selNewSampleInfoStateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selNewSampleInfoStateList() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List list = this.sampleMainService.selSampleInfoState();
			result.put("success", true);
			result.put("data", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	
	//根据原始样本号查询该条数据id
	@Action(value = "selSampleInfoId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleInfoId() throws Exception {
		String sampleCode = getRequest().getParameter("sampleCode");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = this.sampleMainService.selSampleInfoId(sampleCode);
			result.put("success", true);
			result.put("data", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 按照申请单id和原始样本号查询 样本状态表
	 * @throws Exception
	 */
	@Action(value = "showSampleStateListJson")
	public void showSampleStateListJson() throws Exception {
		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String sampleCode = getParameterFromRequest("id");
		String taskId = getParameterFromRequest("taskId");
		try {
			String isSaleManage = (String) this
					.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			Map<String, Object> result = sampleMainService.findSampleStateList(start,
					length, query, col, sort, null, sampleCode, taskId);
			Long count = (Long) result.get("total");
			List<SampleState> list = (List<SampleState>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stageTime", "");
			map.put("startDate", "");
			map.put("endDate", "");
			map.put("tableTypeId", "");
			map.put("stageName", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("taskId", "");

			map.put("taskMethod", "");
			map.put("taskResult", "");
			map.put("techTaskId", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("note3", "");

			map.put("note4", "");
			map.put("note5", "");
			map.put("note6", "");
			map.put("note7", "");
			
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//按照申请单和订单查询状态
	@Action(value = "showSampleStateListJsonForOrder")
	public void showSampleStateListJsonForOrder() throws Exception {
		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String sampleOrder = getParameterFromRequest("id");
		String taskId = getParameterFromRequest("taskId");
		try {
			String isSaleManage = (String) this
					.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			Map<String, Object> result = sampleMainService.showSampleStateListJsonForOrder(start,
					length, query, col, sort, null, sampleOrder, taskId);
			Long count = (Long) result.get("total");
			List<SampleState> list = (List<SampleState>) result.get("list");
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stageTime", "");
			map.put("startDate", "");
			map.put("endDate", "");
			map.put("tableTypeId", "");
			map.put("stageName", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("taskId", "");
			
			map.put("taskMethod", "");
			map.put("taskResult", "");
			map.put("techTaskId", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("note3", "");
			
			map.put("note4", "");
			map.put("note5", "");
			map.put("note6", "");
			map.put("note7", "");
			
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}