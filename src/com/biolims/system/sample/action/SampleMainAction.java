package com.biolims.system.sample.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.analysis.filt.model.SampleFiltrateInfo;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cfdna.model.CfdnaTaskResult;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.dnap.model.DnapTaskResult;
import com.biolims.experiment.other.model.OtherTaskResult;
import com.biolims.experiment.pcr.model.PcrTaskResult;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.uf.model.UfTaskResult;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.sample.model.SampleBloodDiseaseTemp;
import com.biolims.sample.model.SampleChromosomeTemp;
import com.biolims.sample.model.SampleFolicAcidTemp;
import com.biolims.sample.model.SampleGeneTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleState;
import com.biolims.sample.model.SampleTumorTemp;
import com.biolims.sample.model.SampleVisitTemp;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.sample.service.SampleMainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;

@Namespace("/system/sample/sampleMain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258673456786L;
	private String rightsId = "9015";
	@Autowired
	private SampleMainService sampleMainService;
	private SampleInput sampleInput = new SampleInput();
	private SampleInputTemp sampleInputTemp = new SampleInputTemp();
	private SampleInfo sampleInfo = new SampleInfo();
	private SampleTumorTemp sampleTumorTemp = new SampleTumorTemp();
	private SampleVisitTemp sampleVisitTemp = new SampleVisitTemp();
	private SampleBloodDiseaseTemp sampleBloodDiseaseTemp = new SampleBloodDiseaseTemp();
	private SampleChromosomeTemp sampleChromosomeTemp = new SampleChromosomeTemp();
	private SampleFolicAcidTemp sampleFolicAcidTemp = new SampleFolicAcidTemp();
	private SampleGeneTemp sampleGeneTemp = new SampleGeneTemp();
	@Resource
	private FieldService fieldService;

	@Action(value = "showSampleMainList")
	public String showSampleMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleMain.jsp");
	}

	/**
	 * 
	 * @Title: showStorageTableJson @Description: TODO(查询主数据列表信息) @author 尹标舟 @date
	 *         2018-3-15下午5:50:38 @throws Exception @throws
	 */
	@Action(value = "showSampleMainNewListJson")
	public void showSampleMainNewListJson() throws Exception {

		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleMainService.findSampleInputNewList(start, length, query, col, sort, null,
					null);
			// Map<String, Object> result = storageService.findStorageTable(start,
			// length, query, col, sort, p_type, null);
			Long count = (Long) result.get("total");
			List<SampleInfo> list = (List<SampleInfo>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("patientName", "");
			map.put("note", "");
			map.put("orderNum", "");
			map.put("patientId", "");
			map.put("sampleOrder", "");

			map.put("location", "");
			map.put("dicType-id", "");
			map.put("dicType-name", "");

			map.put("sampleStage", "");
			map.put("project-id", "");
			map.put("project-name", "");
			map.put("personShip-id", "");
			map.put("personShip-name", "");

			map.put("idCard", "");
			map.put("cardNumber", "");
			map.put("businessType", "");
			map.put("price", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("upLoadAccessory-id", "");
			map.put("upLoadAccessory-fileName", "");
			// 产前
			map.put("receiveDate", "");
			map.put("si-area", "");
			map.put("hospital", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("si-doctor", "");
			map.put("si-inHosNum", "");

			map.put("sampleType-id", "");
			map.put("sampleType-name", "");

			map.put("age", "");
			map.put("weight", "");
			map.put("si-gestationalAge", "");
			map.put("si-voucherType-id", "");
			map.put("si-voucherType-name", "");
			map.put("si-voucherCode", "");
			map.put("phone", "");
			map.put("familyAddress", "");
			map.put("si-endMenstruationDate", "yyyy-MM-dd");
			map.put("si-gestationIVF", "");
			map.put("si-pregnancyTime", "");
			map.put("si-parturitionTime", "");
			map.put("si-badMotherhood", "");
			map.put("si-organGrafting", "");
			map.put("si-outTransfusion", "");
			map.put("si-firstTransfusionDate", "yyyy-MM-dd");
			map.put("si-stemCellsCure", "");
			map.put("si-immuneCure", "");
			map.put("si-endImmuneCureDate", "yyyy-MM-dd");
			map.put("si-embryoType", "");
			map.put("si-NT", "");
			map.put("si-reason", "");
			map.put("si-testPattern", "");
			map.put("si-trisome21Value", "");
			map.put("si-trisome18Value", "");
			map.put("si-coupleChromosome", "");
			map.put("si-reason2", "");
			map.put("si-diagnosis", "");
			map.put("si-medicalHistory", "");
			map.put("si-isInsure", "");
			map.put("si-isFee", "");
			map.put("si-privilegeType", "");
			map.put("si-linkman-id", "");
			map.put("si-linkman-name", "");
			map.put("si-isInvoice", "");
			map.put("si-paymentUnit", "");
			map.put("createUser1", "");
			map.put("createUser2", "");
			map.put("si-suppleAgreement", "");
			map.put("si-money", "");
			map.put("si-receiptType-id", "");
			map.put("si-receiptType-name", "");
			map.put("sampleNum", "");
			map.put("sampleType2", "");
			map.put("samplingDate", "yyyy-MM-dd");
			map.put("sendReportDate", "yyyy-MM-dd");
			map.put("unit", "");
			map.put("sampleNote", "");
			map.put("dicTypeName", "");

			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			map.put("species", "");

			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");

			map.put("sampleOrder-id", "");
			map.put("sampleOrder-name", "");
			map.put("sampleOrder-gender", "");
			map.put("sampleOrder-age", "");
			map.put("sampleOrder-medicalNumber", "");
			map.put("sampleOrder-productName", "");
			map.put("sampleOrder-round", "");
			map.put("sampleOrder-filtrateCode", "");
			map.put("sampleOrder-randomCode", "");
			map.put("sampleOrder-ccoi", "");
			map.put("sampleOrder-crmCustomer-name", "");
			map.put("sampleOrder-barcode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SampleInfo");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showSampleMainListJson")
	public void showSampleMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		User user1 = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

		String thisUser = user1.getName();
		Map<String, Object> result = sampleMainService.findSampleInputList(map2Query, startNum, limitNum, dir, sort,
				thisUser);
		Long count = (Long) result.get("total");
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("name", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("patientName", "");
		map.put("note", "");
		map.put("orderNum", "");
		map.put("patientId", "");
		map.put("sampleOrder", "");

		map.put("location", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");

		map.put("sampleStage", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("personShip-id", "");
		map.put("personShip-name", "");

		map.put("idCard", "");
		map.put("cardNumber", "");
		map.put("businessType", "");
		map.put("price", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("upLoadAccessory-id", "");
		map.put("upLoadAccessory-fileName", "");
		// 产前
		map.put("receiveDate", "");
		map.put("si-area", "");
		map.put("hospital", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("si-doctor", "");
		map.put("si-inHosNum", "");

		map.put("sampleType-id", "");
		map.put("sampleType-name", "");

		map.put("age", "");
		map.put("weight", "");
		map.put("si-gestationalAge", "");
		map.put("si-voucherType-id", "");
		map.put("si-voucherType-name", "");
		map.put("si-voucherCode", "");
		map.put("phone", "");
		map.put("familyAddress", "");
		map.put("si-endMenstruationDate", "yyyy-MM-dd");
		map.put("si-gestationIVF", "");
		map.put("si-pregnancyTime", "");
		map.put("si-parturitionTime", "");
		map.put("si-badMotherhood", "");
		map.put("si-organGrafting", "");
		map.put("si-outTransfusion", "");
		map.put("si-firstTransfusionDate", "yyyy-MM-dd");
		map.put("si-stemCellsCure", "");
		map.put("si-immuneCure", "");
		map.put("si-endImmuneCureDate", "yyyy-MM-dd");
		map.put("si-embryoType", "");
		map.put("si-NT", "");
		map.put("si-reason", "");
		map.put("si-testPattern", "");
		map.put("si-trisome21Value", "");
		map.put("si-trisome18Value", "");
		map.put("si-coupleChromosome", "");
		map.put("si-reason2", "");
		map.put("si-diagnosis", "");
		map.put("si-medicalHistory", "");
		map.put("si-isInsure", "");
		map.put("si-isFee", "");
		map.put("si-privilegeType", "");
		map.put("si-linkman-id", "");
		map.put("si-linkman-name", "");
		map.put("si-isInvoice", "");
		map.put("si-paymentUnit", "");
		map.put("createUser1", "");
		map.put("createUser2", "");
		map.put("si-suppleAgreement", "");
		map.put("si-money", "");
		map.put("si-receiptType-id", "");
		map.put("si-receiptType-name", "");
		map.put("sampleNum", "");
		map.put("sampleType2", "");
		map.put("samplingDate", "yyyy-MM-dd");
		map.put("unit", "");
		map.put("sampleNote", "");
		map.put("dicTypeName", "");

		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmDoctor-id", "");
		map.put("crmDoctor-name", "");
		map.put("species", "");

		map.put("sellPerson-id", "");
		map.put("sellPerson-name", "");
		// 肿瘤
		// 染色体
		// 基因
		// 叶酸
		// 乳腺癌
		// 临检所
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editSampleMain")
	public String editSampleMain() throws Exception {
		String id = getParameterFromRequest("id");
		if (!("").equals(id) && id != null) {
			this.sampleInfo = this.sampleMainService.get(id);
		}
		// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/system/sample/sampleInfoEdit.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewSampleMain")
	public String toViewSampleMain() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			this.sampleInfo = this.sampleMainService.get(id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			// dicState2State(storage.getState().getId());
		}
		return dispatcher("/WEB-INF/page/system/sample/sampleInfoEdit.jsp");
	}

	@Action(value = "save")
	public String saveSampleMain() throws Exception {
		String id = sampleInfo.getId();
		if (!"".equals(id) && id != null) {
			this.sampleMainService.saveOrUpdate(sampleInfo);
		}
		sampleInfo = this.sampleMainService.get(id);

		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		//

		return dispatcher("/WEB-INF/page/system/sample/sampleInfoEdit.jsp");
	}

	/**
	 * 样本状态
	 * 
	 * @return
	 */

	@Action(value = "showSampleStateList")
	public String showSampleStateList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleState.jsp");
	}

	@Action(value = "showSampleStateListJson")
	public void showSampleStateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findCrmPatientStateList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleState> list = (List<SampleState>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stageTime", "");
			map.put("startDate", "");
			map.put("endDate", "");
			map.put("tableTypeId", "");
			map.put("stageName", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("taskId", "");

			map.put("taskMethod", "");
			map.put("taskResult", "");
			map.put("techTaskId", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("note3", "");

			map.put("note4", "");
			map.put("note5", "");
			map.put("note6", "");
			map.put("note7", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 跟据订单展示任务看板 @Title: showSampleStateTaskKanbanJson @Description:
	 * TODO @param @throws Exception @return void @author 孙灵达 @date
	 * 2018年9月7日 @throws
	 */
	@Action(value = "showSampleStateTaskKanbanJson")
	public void showSampleStateTaskKanbanJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.findSampleStateTaskKanban(start, length, query, col, sort, id);
		List<SampleState> list = (List<SampleState>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("stageTime", "");
		map.put("startDate", "");
		map.put("endDate", "");
		map.put("tableTypeId", "");
		map.put("stageName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("taskId", "");

		map.put("taskMethod", "");
		map.put("taskResult", "");
		map.put("techTaskId", "");
		map.put("note", "");
		map.put("note2", "");
		map.put("note3", "");

		map.put("note4", "");
		map.put("note5", "");
		map.put("note6", "");
		map.put("note7", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showSampleStateNewListJson")
	public void showSampleStatenewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.findSampleStateNewList(start, length, query, col, sort, id);
		List<SampleState> list = (List<SampleState>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("stageTime", "");
		map.put("startDate", "");
		map.put("endDate", "");
		map.put("tableTypeId", "");
		map.put("stageName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("taskId", "");

		map.put("taskMethod", "");
		map.put("taskResult", "");
		map.put("techTaskId", "");
		map.put("note", "");
		map.put("note2", "");
		map.put("note3", "");

		map.put("note4", "");
		map.put("note5", "");
		map.put("note6", "");
		map.put("note7", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 核酸提取结果
	 * 
	 * @return
	 */
	@Action(value = "showDnaSampleInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnaSampleInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleDnaInfo.jsp");
	}

	@Action(value = "showDnaSampleInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnaSampleInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findDnaSampleInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DnaTaskInfo> list = (List<DnaTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("dnaCode", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("sumVolume", "");
			map.put("rin", "");
			map.put("contraction", "");
			map.put("od260", "");
			map.put("od280", "");
			map.put("tempId", "");
			map.put("isToProject", "");
			map.put("volume", "");
			map.put("note", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("dnaTask-name", "");
			map.put("dnaTask-id", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("dataBits", "");
			map.put("qbcontraction", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 超声破碎结果
	 * 
	 * @return
	 */
	@Action(value = "showUfSampleInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showUfSampleInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleUfInfo.jsp");
	}

	@Action(value = "showUfSampleInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showUfSampleInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleUfresultList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<UfTaskResult> list = (List<UfTaskResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("ufTask-name", "");
			map.put("ufTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * FFPE，血液文库结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleWkInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleWkInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleWkInfo.jsp");
	}

	@Action(value = "showSampleWkInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleWkInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleWkInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<WkTaskInfo> list = (List<WkTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("wk-name", "");
			map.put("wk-id", "");

			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");

			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("concentration", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleNum", "");
			map.put("tempId", "");

			map.put("chpsConcentration", "");
			map.put("chVolume", "");
			map.put("chSumTotal", "");
			map.put("indexConcentration", "");
			map.put("dxpdConcentration", "");
			map.put("dxpdVolume", "");
			map.put("dxpdSumTotal", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * ctDNA文库构建结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleWkInfoCtDNAList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleWkInfoCtDNAList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleWkInfoCtDNA.jsp");
	}

	// @Action(value = "showSampleWkInfoCtDNAListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleWkInfoCtDNAListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSampleWkInfoCtDNAList(scId, startNum, limitNum, dir,
	// sort);
	// Long total = (Long) result.get("total");
	// List<SampleWkInfoCtDNA> list = (List<SampleWkInfoCtDNA>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("code", "");
	// map.put("indexa", "");
	// map.put("sampleCode", "");
	// map.put("volume", "");
	// map.put("unit", "");
	// map.put("result", "");
	// map.put("nextFlowId", "");
	// map.put("nextFlow", "");
	// map.put("reason", "");
	// map.put("submit", "");
	// map.put("patientName", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("inspectDate", "");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("idCard", "");
	// map.put("phone", "");
	// map.put("orderId", "");
	// map.put("reportDate", "yyyy-MM-dd");
	// map.put("note", "");
	// map.put("state", "");
	// map.put("method", "");
	// map.put("wkTask-name", "");
	// map.put("wkTask-id", "");
	// map.put("sampleType", "");
	// map.put("i5", "");
	// map.put("i7", "");
	// map.put("concentration", "");
	// map.put("loopNum", "");
	// map.put("sumTotal", "");
	// map.put("pcrRatio", "");
	// map.put("expectNum", "");
	// map.put("sampleNum", "");
	// map.put("tempId", "");
	//
	// map.put("totalNum", "");
	// map.put("dpdConcentration", "");
	// map.put("dpdVolume", "");
	// map.put("dpdSumTotal", "");
	// map.put("dpdHsl", "");
	// map.put("xpdConcentration", "");
	// map.put("xpdVolume", "");
	// map.put("xpdSumTotal", "");
	// map.put("xpdHsl", "");
	// map.put("indexConcentration", "");
	// map.put("wkConcentration", "");
	// map.put("wkVolume", "");
	// map.put("wkSumTotal", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * mRNA文库构建结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleWkInfomRNAList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleWkInfomRNAList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleWkInfomRNA.jsp");
	}

	// @Action(value = "showSampleWkInfomRNAListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleWkInfomRNAListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSampleWkInfomRNAList(scId, startNum, limitNum, dir,
	// sort);
	// Long total = (Long) result.get("total");
	// List<SampleWkInfomRNA> list = (List<SampleWkInfomRNA>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("code", "");
	// map.put("indexa", "");
	// map.put("sampleCode", "");
	// map.put("volume", "");
	// map.put("unit", "");
	// map.put("result", "");
	// map.put("nextFlowId", "");
	// map.put("nextFlow", "");
	// map.put("reason", "");
	// map.put("submit", "");
	// map.put("patientName", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("inspectDate", "");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("idCard", "");
	// map.put("phone", "");
	// map.put("orderId", "");
	// map.put("reportDate", "yyyy-MM-dd");
	// map.put("note", "");
	// map.put("state", "");
	// map.put("method", "");
	// map.put("wkTask-name", "");
	// map.put("wkTask-id", "");
	// map.put("sampleType", "");
	// map.put("i5", "");
	// map.put("i7", "");
	// map.put("concentration", "");
	// map.put("loopNum", "");
	// map.put("sumTotal", "");
	// map.put("pcrRatio", "");
	// map.put("expectNum", "");
	// map.put("sampleNum", "");
	// map.put("tempId", "");
	//
	// map.put("rin", "");
	// map.put("nsConcentration", "");
	// map.put("nsVolume", "");
	// map.put("nsSumTotal", "");
	// map.put("qcConcentration", "");
	// map.put("qcVolume", "");
	// map.put("qcSumTotal", "");
	// map.put("qcYield", "");
	// map.put("temperature", "");
	// map.put("time", "");
	// map.put("wkConcentration", "");
	// map.put("wkVolume", "");
	// map.put("wkSumTotal", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * rRNA文库构建结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleWkInforRNAList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleWkInforRNAList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleWkInforRNA.jsp");
	}

	// @Action(value = "showSampleWkInforRNAListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleWkInforRNAListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSampleWkInforRNAList(scId, startNum, limitNum, dir,
	// sort);
	// Long total = (Long) result.get("total");
	// List<SampleWkInforRNA> list = (List<SampleWkInforRNA>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("code", "");
	// map.put("indexa", "");
	// map.put("sampleCode", "");
	// map.put("volume", "");
	// map.put("unit", "");
	// map.put("result", "");
	// map.put("nextFlowId", "");
	// map.put("nextFlow", "");
	// map.put("reason", "");
	// map.put("submit", "");
	// map.put("patientName", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("inspectDate", "");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("idCard", "");
	// map.put("phone", "");
	// map.put("orderId", "");
	// map.put("reportDate", "yyyy-MM-dd");
	// map.put("note", "");
	// map.put("state", "");
	// map.put("method", "");
	// map.put("wkTask-name", "");
	// map.put("wkTask-id", "");
	// map.put("sampleType", "");
	// map.put("i5", "");
	// map.put("i7", "");
	// map.put("concentration", "");
	// map.put("loopNum", "");
	// map.put("sumTotal", "");
	// map.put("pcrRatio", "");
	// map.put("expectNum", "");
	// map.put("sampleNum", "");
	// map.put("tempId", "");
	//
	// map.put("rin", "");
	// map.put("nsConcentration", "");
	// map.put("nsVolume", "");
	// map.put("nsSumTotal", "");
	// map.put("qcConcentration", "");
	// map.put("qcVolume", "");
	// map.put("qcSumTotal", "");
	// map.put("qcYield", "");
	// map.put("temperature", "");
	// map.put("time", "");
	// map.put("wkConcentration", "");
	// map.put("wkVolume", "");
	// map.put("wkSumTotal", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * pooling结果
	 * 
	 * @return
	 */
	@Action(value = "showSamplePoolingInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePoolingInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/samplePoolingInfo.jsp");
	}

	// @Action(value = "showSamplePoolingInfoListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSamplePoolingInfoListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSamplePoolingInfoList(scId, startNum, limitNum, dir,
	// sort);
	// Long total = (Long) result.get("total");
	// List<PoolingInfo> list = (List<PoolingInfo>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("tempId", "");
	// map.put("name", "");
	// map.put("code", "");
	// map.put("sampleCode", "");
	// map.put("volume", "");
	//
	// map.put("unit", "");
	// map.put("result", "");
	// map.put("nextFlow", "");
	// map.put("method", "");
	// map.put("note", "");
	//
	// map.put("poolingTask-name", "");
	// map.put("poolingTask-id", "");
	// map.put("wkIndex", "");
	// map.put("mixAmount", "");
	// map.put("mixVolume", "");
	//
	// map.put("qpcrConcentration", "");
	// map.put("reason", "");
	// map.put("idCard", "");
	// map.put("sequencingFun", "");
	// map.put("inspectDate", "");
	//
	// map.put("acceptDate", "");
	// map.put("reportDate", "");
	// map.put("orderId", "");
	// map.put("submit", "");
	// map.put("patient", "");
	//
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("phone", "");
	// map.put("poolingCode", "");
	//
	// map.put("sequencingReadLong", "");
	// map.put("sequencingType", "");
	// map.put("sequencingPlatform", "");
	// map.put("totalAmount", "");
	// map.put("totalVolume", "");
	//
	// map.put("others", "");
	// map.put("acceptUser-id", "");
	// map.put("acceptUser-name", "");
	//
	// map.put("readsOne", "");
	// map.put("readsTwo", "");
	// map.put("index1", "");
	// map.put("index2", "");
	//
	// map.put("contractId", "");
	// map.put("projectId", "");
	// map.put("orderType", "");
	// map.put("techTaskId", "");
	// map.put("classify", "");
	// map.put("wkType", "");
	// map.put("pdLength", "");
	// map.put("concentration", "");
	// map.put("lane", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 上机测序结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleSequencingInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleSequencingInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleSequencingInfo.jsp");
	}

	// @Action(value = "showSampleSequencingInfoListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleSequencingInfoListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSampleSqquencingInfoList(scId, startNum, limitNum,
	// dir, sort);
	// Long total = (Long) result.get("total");
	// List<SequencingTaskInfo> list = (List<SequencingTaskInfo>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("fcCode", "");
	// map.put("laneCode", "");
	// map.put("machineCode", "");
	// map.put("poolingCode", "");
	// map.put("sampleCode", "");
	// map.put("name", "");
	// map.put("code", "");
	// map.put("volume", "");
	// map.put("unit", "");
	// map.put("result", "");
	// map.put("nextFlow", "");
	// map.put("method", "");
	// map.put("patientName", "");
	// map.put("inspectDate", "yyyy-MM-dd");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("idCard", "");
	// map.put("orderId", "");
	// map.put("phone", "");
	// map.put("sequenceFun", "");
	// map.put("reportDate", "yyyy-MM-dd");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("note", "");
	// map.put("state", "");
	// map.put("molarity", "");
	// map.put("quantity", "");
	// map.put("concentration", "");
	// map.put("computerDate", "yyyy-MM-dd");
	// map.put("expectDate", "yyyy-MM-dd");
	// map.put("pfPercent", "");
	// map.put("sequencing-name", "");
	// map.put("sequencing-id", "");
	// map.put("dataDemand", "");
	//
	// map.put("contractId", "");
	// map.put("projectId", "");
	// map.put("orderType", "");
	// map.put("techTaskId", "");
	// map.put("classify", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 下机质控结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleDesequencingInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleDesequencingInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleDesequencingInfo.jsp");
	}

	@Action(value = "showSampleDesequencingInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleDesequencingInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleDesequencingInfoList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<SampleDeSequencingInfo> list = (List<SampleDeSequencingInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("poolingCode", "");
			map.put("wkCode", "");
			map.put("outPut", "");
			map.put("clusters", "");
			map.put("pf", "");
			map.put("indexLibrary", "");
			map.put("sampleCount", "");
			map.put("reasonType", "");
			map.put("detailed", "");
			map.put("assess", "");
			map.put("backDate", "");
			map.put("errorRate", "");
			map.put("lane", "");
			map.put("project", "");
			map.put("need", "");
			map.put("reads", "");
			map.put("base", "");
			map.put("pct", "");
			map.put("adapter", "");
			map.put("gc", "");
			map.put("q20", "");
			map.put("q30", "");
			map.put("index", "");
			map.put("ind", "");
			map.put("dup", "");
			map.put("ployAT", "");
			map.put("result", "");
			map.put("advice", "");
			map.put("method", "");
			map.put("note", "");
			map.put("desequencingTask-name", "");
			map.put("desequencingTask-id", "");

			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("techTaskId", "");
			map.put("state", "");

			map.put("lane2", "");
			map.put("kit", "");
			map.put("raw_reads", "");
			map.put("raw_len", "");
			map.put("raw_GC", "");
			map.put("raw_size", "");
			map.put("hq_reads", "");
			map.put("hq_len", "");
			map.put("hq_GC", "");
			map.put("hq_size", "");
			map.put("hq_reads_pct", "");
			map.put("hq_data_pct", "");
			map.put("demultiplexing_Reads", "");
			map.put("demultiplexing_PCT", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * PCR扩增结果
	 * 
	 * @return
	 */
	@Action(value = "showSamplePCRInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePCRInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/samplePcrResult.jsp");
	}

	@Action(value = "showSamplePCRInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePCRInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSamplePcrInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<PcrTaskResult> list = (List<PcrTaskResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("pcrTask-name", "");
			map.put("pcrTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * DNA纯化结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleDnaInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleDnaInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleDnaResult.jsp");
	}

	@Action(value = "showSampleDnaInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleDnaInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleDnaInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DnapTaskResult> list = (List<DnapTaskResult>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("dnapTask-name", "");
			map.put("dnapTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 2100质控结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleQc2100InfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleQc2100InfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleQc2100Result.jsp");
	}

	@Action(value = "showSampleQc2100InfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleQc2100InfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSample2100InfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleQc2100Info> list = (List<SampleQc2100Info>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("fragmentSize", "");
			map.put("concentration", "");
			map.put("qpcr", "");
			map.put("specific", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("wKQualitySampleTask-name", "");
			map.put("wKQualitySampleTask-id", "");
			map.put("name", "");
			map.put("wkType", "");

			map.put("volume", "");
			map.put("unit", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("classify", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * QPCR质控结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleQPCRInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleQPCRInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleQpcrResult.jsp");
	}

	@Action(value = "showSampleQPCRInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleQPCRInfoListJsons() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleQPCRInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleQcQpcrInfo> list = (List<SampleQcQpcrInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("fragmentSize", "");
			map.put("concentration", "");
			map.put("qpcr", "");
			map.put("specific", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			map.put("name", "");
			map.put("wkType", "");

			map.put("volume", "");
			map.put("unit", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("her2", "");
			map.put("er", "");
			map.put("pr", "");
			map.put("rs", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 文库检测结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleCheckWkList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleCheckWkList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleTechCheckWkResult.jsp");
	}

	// @Action(value = "showSampleCheckWkListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleCheckWkListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSampleCheckWkInfoList(scId, startNum, limitNum, dir,
	// sort);
	// Long total = (Long) result.get("total");
	// List<TechCheckServiceWkInfo> list = (List<TechCheckServiceWkInfo>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("sampleCode", "");
	// map.put("wkCode", "");
	// map.put("pdSzie", "");
	// map.put("massCon", "");
	// map.put("molarCon", "");
	// map.put("volume", "");
	// map.put("pdResult", "");
	// map.put("molarResult", "");
	// map.put("nextFlow", "");
	// map.put("note", "");
	// map.put("result", "");
	// map.put("itemId", "");
	// map.put("orderId", "");
	// map.put("orderType", "");
	// map.put("submit", "");
	// map.put("state", "");
	// map.put("techCheckServiceTask-name", "");
	// map.put("techCheckServiceTask-id", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 核酸检测结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleCheckServiceList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleCheckServiceList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleTechCheckServiceResult.jsp");
	}

	// @Action(value = "showSampleCheckServiceListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleCheckServiceListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = this.sampleMainService
	// .findSampleCheckServiceInfoList(scId, startNum, limitNum,
	// dir, sort);
	// Long total = (Long) result.get("total");
	// List<TechCheckServiceTaskInfo> list = (List<TechCheckServiceTaskInfo>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("sampleCode", "");
	// map.put("dnaCode", "");
	// map.put("od260", "");
	// map.put("od230", "");
	// map.put("rin", "");
	// map.put("volume", "");
	// map.put("concentration", "");
	// map.put("sumNum", "");
	// map.put("result", "");
	// map.put("nextFlow", "");
	// map.put("note", "");
	// map.put("itemId", "");
	// map.put("orderId", "");
	// map.put("orderType", "");
	// map.put("submit", "");
	// map.put("state", "");
	// map.put("s28", "");
	// map.put("classify", "");
	// map.put("techCheckServiceTask-name", "");
	// map.put("techCheckServiceTask-id", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * 过滤结果
	 * 
	 * @return
	 */
	@Action(value = "showSamplefiltList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplefiltList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/samplefiltResult.jsp");
	}

	@Action(value = "showSamplefiltListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplefiltListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleFiltInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleFiltrateInfo> list = (List<SampleFiltrateInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("wkCode", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("desDataNum", "");
			map.put("filtDataNum", "");
			map.put("normDataNum", "");
			map.put("sequenceType", "");
			map.put("realSequenceType", "");
			map.put("isSequenceType", "");
			map.put("isDataNum", "");
			map.put("addDataNum", "");
			map.put("abnomal", "");
			map.put("idGood", "");
			map.put("nextflow", "");
			map.put("submit", "");
			map.put("note", "");
			map.put("filtrateTask-name", "");
			map.put("filtrateTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * cfDNA质量评估结果
	 * 
	 * @return
	 */
	@Action(value = "showSamplecfDnaList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplecfDnaList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleCfDnaResult.jsp");
	}

	@Action(value = "showSamplecfDnaListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplecfDnaListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleCfdnaInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<CfdnaTaskResult> list = (List<CfdnaTaskResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("cfdnaTask-name", "");
			map.put("cfdnaTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 其他实验结果
	 * 
	 * @return
	 */
	@Action(value = "showSampleOtherList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleOtherList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleOtherResult.jsp");
	}

	@Action(value = "showSampleOtherListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOtherListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleOtherInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<OtherTaskResult> list = (List<OtherTaskResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("otherTask-name", "");
			map.put("otherTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 库存信息
	 * 
	 * @return
	 */
	@Action(value = "showSampleInItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInItemList() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/sampleInItem.jsp");
	}

	@Action(value = "showSampleInItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.sampleMainService.findSampleSampleInItemList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<SampleInItem> list = (List<SampleInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("checked", "");
			map.put("name", "");
			map.put("location", "");
			map.put("state", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("sampleTypeId", "");
			map.put("infoFrom", "");
			map.put("num", "");
			map.put("sampleIn-name", "");
			map.put("sampleIn-id", "");
			map.put("upLocation", "");
			map.put("patientName", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showSampleInItemNewListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInItemNewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.findSampleInItemNewList(start, length, query, col, sort, id);
		List<SampleInfoIn> list = (List<SampleInfoIn>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("code", "");
		map.put("checked", "");
		map.put("name", "");
		map.put("location", "");
		map.put("state", "");
		map.put("note", "");
		map.put("tempId", "");
		map.put("orderId", "");
		map.put("sampleType", "");
		map.put("sampleTypeId", "");
		map.put("infoFrom", "");
		map.put("num", "");
		map.put("sampleIn-name", "");
		map.put("sampleIn-id", "");
		map.put("upLocation", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "updatePatientId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void updatePatientId() throws Exception {
		String id = getRequest().getParameter("id");
		String patientId = getRequest().getParameter("patientId");
		SampleInfo sampleInfo = sampleMainService.get(id);
		sampleInfo.setPatientId(patientId);
		sampleMainService.saveOrUpdate(sampleInfo);
	}

	@Action(value = "updateProjectId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void updateProjectId() throws Exception {
		String id = getRequest().getParameter("id");
		SampleInfo sampleInfo = sampleMainService.get(id);
		String projectId = getRequest().getParameter("projectId");
		String projectName = getRequest().getParameter("projectName");
		Project p = new Project();
		p.setId(projectId);
		p.setName(projectName);
		sampleInfo.setProject(p);
		sampleMainService.saveOrUpdate(sampleInfo);
	}

	@Action(value = "updateCrm", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void updateCrm() throws Exception {
		String id = getRequest().getParameter("id");
		SampleInfo sampleInfo = sampleMainService.get(id);
		String crmId = getRequest().getParameter("crmId");
		String crmName = getRequest().getParameter("crmName");
		CrmCustomer p = new CrmCustomer();
		p.setId(crmId);
		p.setName(crmName);
		sampleInfo.setCrmCustomer(p);
		sampleMainService.saveOrUpdate(sampleInfo);
	}

	@Action(value = "updateDocter", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void updateDocter() throws Exception {
		String id = getRequest().getParameter("id");
		SampleInfo sampleInfo = sampleMainService.get(id);
		String docterId = getRequest().getParameter("docterId");
		String docterName = getRequest().getParameter("docterName");
		CrmDoctor p = new CrmDoctor();
		p.setId(docterId);
		p.setName(docterName);
		sampleInfo.setCrmDoctor(p);
		sampleMainService.saveOrUpdate(sampleInfo);
	}

	// 查询样本状态
	@Action(value = "selSampleStateById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleStateById() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SampleState> list = this.sampleMainService.selSampleStateById(ids);
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showSampleChart")
	public String showSampleChart() throws Exception {
		rightsId = "90150";
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleMainSearch.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public SampleInputTemp getSampleInputTemp() {
		return sampleInputTemp;
	}

	public void setSampleInputTemp(SampleInputTemp sampleInputTemp) {
		this.sampleInputTemp = sampleInputTemp;
	}

	public SampleMainService getSampleMainService() {
		return sampleMainService;
	}

	public void setSampleMainService(SampleMainService sampleMainService) {
		this.sampleMainService = sampleMainService;
	}

	public SampleInput getSampleInput() {
		return sampleInput;
	}

	public void setSampleInput(SampleInput sampleInput) {
		this.sampleInput = sampleInput;
	}

	public SampleTumorTemp getSampleTumorTemp() {
		return sampleTumorTemp;
	}

	public void setSampleTumorTemp(SampleTumorTemp sampleTumorTemp) {
		this.sampleTumorTemp = sampleTumorTemp;
	}

	public SampleVisitTemp getSampleVisitTemp() {
		return sampleVisitTemp;
	}

	public void setSampleVisitTemp(SampleVisitTemp sampleVisitTemp) {
		this.sampleVisitTemp = sampleVisitTemp;
	}

	public SampleBloodDiseaseTemp getSampleBloodDiseaseTemp() {
		return sampleBloodDiseaseTemp;
	}

	public void setSampleBloodDiseaseTemp(SampleBloodDiseaseTemp sampleBloodDiseaseTemp) {
		this.sampleBloodDiseaseTemp = sampleBloodDiseaseTemp;
	}

	public SampleChromosomeTemp getSampleChromosomeTemp() {
		return sampleChromosomeTemp;
	}

	public void setSampleChromosomeTemp(SampleChromosomeTemp sampleChromosomeTemp) {
		this.sampleChromosomeTemp = sampleChromosomeTemp;
	}

	public SampleFolicAcidTemp getSampleFolicAcidTemp() {
		return sampleFolicAcidTemp;
	}

	public void setSampleFolicAcidTemp(SampleFolicAcidTemp sampleFolicAcidTemp) {
		this.sampleFolicAcidTemp = sampleFolicAcidTemp;
	}

	public SampleGeneTemp getSampleGeneTemp() {
		return sampleGeneTemp;
	}

	public void setSampleGeneTemp(SampleGeneTemp sampleGeneTemp) {
		this.sampleGeneTemp = sampleGeneTemp;
	}

	@Action(value = "showSampleReceiveFromOrderNo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReceiveFromOrderNo() throws Exception {
		String query = getParameterFromRequest("query");
		String orderNo = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.showSampleReceiveFromOrderNo(start, length, query, col, sort,
				orderNo);
		List<SampleReceiveItem> list = (List<SampleReceiveItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		SampleReceiveItem sampleReceiveItem = new SampleReceiveItem();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(sampleReceiveItem);
		map.put("unusual-id", "");
		map.put("unusual-name", "");
		map.put("acceptDate", "yyyy-MM-dd HH:mm");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("unitGroup-id", "");
		map.put("unitGroup-name", "");
		map.put("samplingDate", "yyyy-MM-dd HH:mm");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("personShip-id", "");
		map.put("personShip-name", "");
		map.put("familyId-id", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("bloodTime", "yyyy-MM-dd HH:mm");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showCellPassageItemFromOrderNo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCellPassageItemFromOrderNo() throws Exception {
		String query = getParameterFromRequest("query");
		String orderNo = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.showCellPassageItemFromOrderNo(start, length, query, col, sort,
				orderNo);
		List<CellPassageItem> list = (List<CellPassageItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("orderCode", "");
		map.put("stepNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showCellPassageResultFromOrderNo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCellPassageResultFromOrderNo() throws Exception {
		String query = getParameterFromRequest("query");
		String orderNo = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.showCellPassageResultFromOrderNo(start, length, query, col, sort,
				orderNo);
		List<CellPassageInfo> list = (List<CellPassageInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleOrder-id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("pronoun", "");
		map.put("cellCount", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("result", "");
		map.put("parentId", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("method", "");
		map.put("note", "");
		map.put("cellPassage-id", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("submit", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showQualityTestFromOrderNo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQualityTestFromOrderNo() throws Exception {
		String query = getParameterFromRequest("query");
		String orderNo = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleMainService.showQualityTestFromOrderNo(start, length, query, col, sort,
				orderNo);
		List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("qualityTest-id", "");
		map.put("sampleOrder-id", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("experimentalSteps", "");
		map.put("sampleDeteyion-name", "");
		map.put("orderId", "");
		map.put("result", "");
		map.put("note", "");
		map.put("mark", "");
		map.put("qualityTest-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
}