package com.biolims.system.sample.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderChange;
import com.biolims.sample.model.SampleOrderChangeItem;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.service.SampleCancerTempService;
import com.biolims.sample.service.SampleOrderChangeService;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.sample.service.SampleOrderMainService;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/system/sample/sampleOrderChange")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleOrderChangeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private SampleOrderService sampleOrderService;
	@Autowired
	private SampleOrderChangeService sampleOrderChangeService;
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	private SampleOrderMainService sampleOrderMainService;
	private SampleOrder sampleOrder = new SampleOrder();
	private SampleOrderChange sampleOrderChange=new SampleOrderChange();
	// 注入癌症信息的service
	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private SampleCancerTempService sampleCancerTempService;
	private SampleCancerTemp sampleCancerTempOne = new SampleCancerTemp();// 一录
	private SampleCancerTemp sampleCancerTempTwo = new SampleCancerTemp();// 二录
	// 注入电子病历的service
	@Autowired
	private CrmPatientService crmPatientService;

	@Resource
	private FieldService fieldService;

	@Resource
	private FileInfoService fileInfoService;

	// 列表页面
	@Action(value = "showSampleOrderChangeTable")
	public String showSampleOrderChangeTable() throws Exception {
		String type = getRequest().getParameter("type");
		if(type.equals("0")) {//用户
			rightsId = "980902";
		}else {
			rightsId = "981002";
		}
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderChangeMain.jsp");
	}

	@Action(value = "showSampleOrderChangeTableJson")
	public void showSampleOrderChangeTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOrderChangeService.findSampleOrderChangeTable(start, length, query, col, sort);
			List<SampleOrderChange> list = (List<SampleOrderChange>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("stateName", "");
			map.put("name", "");
			map.put("note", "");
			map.put("changeType", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService
					.findFieldByModuleValue("SampleOrderChange");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "showSampleOrderChangeItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleOrderChangeItemListJson() throws Exception{
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleOrderChangeService
				.findSampleOrderChangeItemList(start, length, query, col,
						sort, id);
		List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("oldId", "");
		map.put("updateId", "");
		map.put("sampleOrder-name", "");
		map.put("sampleOrder-gender", "");
		map.put("sampleOrder-age", "");
		map.put("sampleOrder-nation", "");
		map.put("sampleOrder-zipCode", "");
		map.put("sampleOrder-commissioner-id", "");
		map.put("sampleOrder-commissioner-name", "");
		map.put("sampleOrder-nativePlace", "");
		map.put("sampleOrder-medicalNumber", "");
		map.put("sampleOrder-familyCode", "");
		map.put("sampleOrder-family", "");
		map.put("sampleOrder-familyPhone", "");
		map.put("sampleOrder-familySite", "");
		map.put("sampleOrder-crmCustomer-id", "");
		map.put("sampleOrder-crmCustomer-name", "");
		map.put("sampleOrder-createUser-id", "");
		map.put("sampleOrder-createUser-name", "");
		map.put("sampleOrder-stateName", "");
		map.put("sampleOrder-barcode", "");
		map.put("sampleOrder-hospitalPatientID", "");
		map.put("sampleOrder-birthDate", "");
		map.put("sampleOrder-createDate", "");
		map.put("sampleOrder-idCard", "");
		map.put("sampleOrder-productName", "");
		map.put("sampleOrder-note", "");
		
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	/**
	 * 异步加载代理商名称
	 * 
	 * @return
	 */
	@Action(value = "findPrimaryToSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findPrimaryToSample() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> dataListMap = this.sampleOrderMainService
					.findPrimaryToSample(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}


	@Action(value = "editsampleOrderChange")
	public String editsampleOrderChange() throws Exception {
		rightsId = "980901";
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		long num = 0;
		if (id != null && !id.equals("")) {
			this.sampleOrderChange = commonDAO.get(SampleOrderChange.class,id);
			if (sampleOrderChange == null) {
				User user = (User) this
						.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				if(user!=null&&!"".equals(user)) {
					sampleOrderChange.setCreateUser(user);
				}
				sampleOrderChange.setCreateDate(new Date());
				sampleOrderChange
						.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				sampleOrderChange
						.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "sampleOrderChange");
			}

		} else {
			sampleOrderChange.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleOrderChange.setCreateUser(user);
			sampleOrderChange.setCreateDate(new Date());
			sampleOrderChange
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleOrderChange
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		putObjToContext("type", type);
		toState(sampleOrderChange.getState());
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderChangeEdit.jsp");
	}
	/**
	 * @return
	 * @throws Exception
	 */
	/*@Action(value = "save")
		public String save() throws Exception {
			synchronized(SampleOrderChangeAction.class) {
				String id = sampleOrderChange.getId();
				String number = sampleOrderChange.getMedicalNumber();
					if ((id != null && id.equals("")) || id.equals("NEW")) {
						String modelName = "SampleOrderChange";
						String markCode = "SOC";
						Date date = new Date();
						DateFormat format = new SimpleDateFormat("yy");
						String stime = format.format(date);
						String autoID = codingRuleService.getCodeByPrefix(modelName,
								markCode, stime, 000000, 6, null);
						
						sampleOrderChange.setId(autoID);
				}
			
			boolean result = crmPatientService.queryByCount(number);
			CrmPatient r = null;
				
			if (!"".equals(sampleOrderChange.getMedicalNumber())) {
				r = crmPatientService.get(sampleOrderChange.getMedicalNumber());
				if (r == null)
					r = new CrmPatient();
				// 该电子病历记录存在: 修改电子病历
	
				r.setId(sampleOrderChange.getMedicalNumber());
				r.setName(sampleOrderChange.getName());
				r.setGender(sampleOrderChange.getGender());
				r.setDateOfBirth(sampleOrderChange.getBirthDate());
				r.setTelphoneNumber1(sampleOrderChange.getFamilyPhone());
				r.setCreateDate(sampleOrderChange.getCreateDate());
				r.setCreateUser(sampleOrderChange.getCreateUser());
	
				crmPatientService.save(r);
	
			} else {
				// 该电子记录不存在:添加电子病历
				String modelName = "CrmPatient";
				String markCode = "P";
				String autoID = codingRuleService.genTransID(modelName, markCode);
	
				sampleOrderChange.setMedicalNumber(autoID);
				r = new CrmPatient();
				r.setId(autoID);
				r.setName(sampleOrderChange.getName());
				r.setGender(sampleOrderChange.getGender());
				r.setDateOfBirth(sampleOrderChange.getBirthDate());
				r.setTelphoneNumber1(sampleOrderChange.getFamilyPhone());
				r.setCreateDate(sampleOrderChange.getCreateDate());
				r.setCreateUser(sampleOrderChange.getCreateUser());
				crmPatientService.save(r);
			}
			Map aMap = new HashMap();
			// 保存病史和用药信息
			// aMap.put("sampleOrderPersonnel",
			// getParameterFromRequest("sampleOrderPersonnelJson"));
			aMap.put("sampleOrderItem",
					getParameterFromRequest("sampleOrderInfoJson"));
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
//			sampleOrderService.save(sampleOrder, aMap,changeLog,changeLogItem);
			sampleOrderChangeService.save(sampleOrderChange, aMap, changeLog, changeLogItem);
			
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			String url = "/system/sample/sampleOrder/editSampleOrder.action?id="
					+ sampleOrder.getId();
			if (bpmTaskId != null && !bpmTaskId.equals("")
					&& !bpmTaskId.equals("null"))
				url += "&bpmTaskId=" + bpmTaskId;
			return redirect(url);
		}
	}*/

	@Action(value = "viewSampleOrder")
	public String toViewSampleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		sampleOrder = sampleOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	}
	@Action(value = "casualMethod")
	public String casualMethod() throws Exception {
		
		return redirect("/lims/plugins/imageCut/index.html");
	}

	/**
	 * 
	 * 子表的相关方法
	 * 
	 * @return
	 */
	// @Action(value = "showSampleOrderPersonnelList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showSampleOrderPersonnelList() throws Exception {
	// return
	// dispatcher("/WEB-INF/page/system/sample/sampleOrderMainPersonnel.jsp");
	// }
	//
	// @Action(value = "showSampleOrderPersonnelListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleOrderPersonnelListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = sampleOrderService
	// .findSampleOrderPersonnelList(scId, startNum, limitNum,
	// dir, sort);
	// Long total = (Long) result.get("total");
	// List<SampleOrderPersonnel> list = (List<SampleOrderPersonnel>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("checkOutTheAge", "");
	// map.put("sampleOrder-name", "");
	// map.put("sampleOrder-id", "");
	// map.put("tumorCategory-name", "");
	// map.put("tumorCategory-id", "");
	// map.put("familyRelation", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }


//	@Action(value = "showSampleOrderInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showSampleOrderInfoList() throws Exception {
//		return dispatcher("/WEB-INF/page/system/sample/sampleOrderInfoMain.jsp");
//	}
//
//	@Action(value = "showSampleOrderInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showSampleOrderInfoListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = sampleOrderService
//					.findSampleOrderInfoList(scId, startNum, limitNum, dir,
//							sort);
//			Long total = (Long) result.get("total");
//			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("name", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("patientName", "");
//			map.put("note", "");
//			map.put("orderNum", "");
//			map.put("patientId", "");
//			map.put("sampleOrder", "");
//
//			map.put("location", "");
//			map.put("dicType-id", "");
//			map.put("dicType-name", "");
//
//			map.put("sampleStage", "");
//			map.put("project-id", "");
//			map.put("project-name", "");
//			map.put("personShip-id", "");
//			map.put("personShip-name", "");
//
//			map.put("idCard", "");
//			map.put("businessType", "");
//			map.put("price", "");
//			map.put("type-id", "");
//			map.put("type-name", "");
//			map.put("upLoadAccessory-id", "");
//			map.put("upLoadAccessory-fileName", "");
//			// 产前
//			map.put("receiveDate", "");
//			map.put("si-area", "");
//			map.put("hospital", "");
//			map.put("inspectDate", "");
//			map.put("reportDate", "");
//			map.put("si-doctor", "");
//			map.put("si-inHosNum", "");
//
//			map.put("sampleType-id", "");
//			map.put("sampleType-name", "");
//
//			map.put("age", "");
//			map.put("weight", "");
//			map.put("si-gestationalAge", "");
//			map.put("si-voucherType-id", "");
//			map.put("si-voucherType-name", "");
//			map.put("si-voucherCode", "");
//			map.put("phone", "");
//			map.put("familyAddress", "");
//			map.put("si-endMenstruationDate", "yyyy-MM-dd");
//			map.put("si-gestationIVF", "");
//			map.put("si-pregnancyTime", "");
//			map.put("si-parturitionTime", "");
//			map.put("si-badMotherhood", "");
//			map.put("si-organGrafting", "");
//			map.put("si-outTransfusion", "");
//			map.put("si-firstTransfusionDate", "yyyy-MM-dd");
//			map.put("si-stemCellsCure", "");
//			map.put("si-immuneCure", "");
//			map.put("si-endImmuneCureDate", "yyyy-MM-dd");
//			map.put("si-embryoType", "");
//			map.put("si-NT", "");
//			map.put("si-reason", "");
//			map.put("si-testPattern", "");
//			map.put("si-trisome21Value", "");
//			map.put("si-trisome18Value", "");
//			map.put("si-coupleChromosome", "");
//			map.put("si-reason2", "");
//			map.put("si-diagnosis", "");
//			map.put("si-medicalHistory", "");
//			map.put("si-isInsure", "");
//			map.put("si-isFee", "");
//			map.put("si-privilegeType", "");
//			map.put("si-linkman-id", "");
//			map.put("si-linkman-name", "");
//			map.put("si-isInvoice", "");
//			map.put("si-paymentUnit", "");
//			map.put("createUser1", "");
//			map.put("createUser2", "");
//			map.put("si-suppleAgreement", "");
//			map.put("si-money", "");
//			map.put("si-receiptType-id", "");
//			map.put("si-receiptType-name", "");
//			map.put("sampleNum", "");
//			map.put("sampleType2", "");
//			map.put("samplingDate", "yyyy-MM-dd");
//			map.put("unit", "");
//			map.put("sampleNote", "");
//			new SendData().sendDateJson(map, list, total,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleOrderItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleOrderService.delSampleOrderItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	// 查询订单编号是否存在 并查询 是否使用
	@Action(value = "selSampleOrderId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleOrderId() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			int total = this.sampleOrderService.selSampleOrderId(id);
			result.put("success", true);
			result.put("data", total);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按年查询订单柱状图
	@Action(value = "selOrderNumberByYear", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selOrderNumberByYear() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYear = getRequest().getParameter("dateYear");
			List list = this.sampleOrderMainService
					.selOrderNumberByYear(dateYear);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按年查询订单中检测项目病状图
	@Action(value = "selProductByOrderByYear", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selProductByOrderByYear() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYear = getRequest().getParameter("dateYear");
			List list = this.sampleOrderMainService
					.selProductByOrderByYear(dateYear);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 按月查询订单中检测项目病状图
	@Action(value = "selProductByOrderByMonth", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selProductByOrderByMonth() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String dateYearMonth = getRequest().getParameter("dateYearMonth");
			List list = this.sampleOrderMainService
					.selProductByOrderByMonth(dateYearMonth);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询订单图片路径
	@Action(value = "selPic", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selPic() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = getRequest().getParameter("id");
			List<String> list = this.sampleOrderMainService.selPic(id);
			result.put("data", list);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "save")
	public String save() throws Exception {
//		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("dataJson");
//		sampleOrderChange = commonDAO.get(SampleOrderChange.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		String id = sampleOrderChange.getId();
		sampleOrderChangeService.save(sampleOrderChange, dataJson,
				changeLog);
		return redirect("/system/sample/sampleOrderChange/editsampleOrderChange.action?id="+sampleOrderChange.getId());
	}

	// 订单左侧显示图片
	@Action(value = "showPic")
	public String showPic() throws Exception {
		return dispatcher("/WEB-INF/page/system/sample/changeView/index.html");
	}

	// 批量上传csv
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sampleOrderMainService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "searchSampleOrder")
	public String searchSampleOrder() throws Exception {
		rightsId = "9809";
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/sample/sampleOrderSearch.jsp");
	}

	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleOrderService getSampleOrderService() {
		return sampleOrderService;
	}

	public void setSampleOrderService(SampleOrderService sampleOrderService) {
		this.sampleOrderService = sampleOrderService;
	}

	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/**
	 * 癌症信息审核的get和set方法
	 * 
	 * @return
	 */
	public SampleCancerTempService getSampleCancerTempService() {
		return sampleCancerTempService;
	}

	public void setSampleCancerTempService(
			SampleCancerTempService sampleCancerTempService) {
		this.sampleCancerTempService = sampleCancerTempService;
	}

	public SampleCancerTemp getSampleCancerTempOne() {
		return sampleCancerTempOne;
	}

	public void setSampleCancerTempOne(SampleCancerTemp sampleCancerTempOne) {
		this.sampleCancerTempOne = sampleCancerTempOne;
	}

	public SampleCancerTemp getSampleCancerTempTwo() {
		return sampleCancerTempTwo;
	}

	public CrmPatientService getCrmPatientService() {
		return crmPatientService;
	}

	public void setCrmPatientService(CrmPatientService crmPatientService) {
		this.crmPatientService = crmPatientService;
	}

	public void setSampleCancerTempTwo(SampleCancerTemp sampleCancerTempTwo) {
		this.sampleCancerTempTwo = sampleCancerTempTwo;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}

	public SampleOrderChange getSampleOrderChange() {
		return sampleOrderChange;
	}

	public void setSampleOrderChange(SampleOrderChange sampleOrderChange) {
		this.sampleOrderChange = sampleOrderChange;
	}
	
}
