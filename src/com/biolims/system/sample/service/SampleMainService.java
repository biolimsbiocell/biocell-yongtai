package com.biolims.system.sample.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.analysis.filt.model.SampleFiltrateInfo;
import com.biolims.crm.project.dao.ProjectDao;
import com.biolims.experiment.cfdna.model.CfdnaTaskResult;
import com.biolims.experiment.dnap.model.DnapTaskResult;
import com.biolims.experiment.other.model.OtherTaskResult;
import com.biolims.experiment.pcr.model.PcrTaskResult;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.uf.model.UfTaskResult;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleState;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.system.sample.dao.SampleMainDao;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleMainService {
	@Resource
	private SampleMainDao sampleMainDao;
	@Resource
	private ProjectDao projectDao;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleInputList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String createUser1) {

		Map<String, Object> n = sampleMainDao.selectSampleInputList(mapForQuery, startNum, limitNum, dir, sort,
				createUser1);
		List<SampleInfo> l = (List<SampleInfo>) n.get("list");
		for (SampleInfo s : l) {
			SampleInput si = sampleMainDao.findListBySampleInfo(s.getCode());
			s.setSi(si);
		}

		// List<SampleInfo> list5 = new ArrayList<SampleInfo>();
		// // 当前登录用户
		// User user = (User) ServletActionContext.getRequest().getSession()
		// .getAttribute(SystemConstants.USER_SESSION_KEY);
		// // 获取当前登录人id
		// String userid = user.getId();
		// // 获取部门ID
		// Department department = user.getDepartment();
		// String departmentid = department.getId();
		//
		// // 通过部门id查询到组织机构
		// List<Department> list1 = projectDao.selectlevelID(departmentid);
		// for (int i = 0; i < list1.size(); i++) {
		// // 通过获得的组织机构的levelid查询下属levelid
		// String id1 = list1.get(i).getLevelId();
		// List<Department> list2 = projectDao.selectdownlevelID(id1);
		// for (int j = 0; j < list2.size(); j++) {
		// // 通过下属的levelId查询该组的部门id
		// String id2 = list2.get(j).getLevelId();
		// List<Department> list3 = projectDao.selectdowndepartmentId(id2);
		// for (int k = 0; k < list3.size(); k++) {
		// // 通过该组的部门id查询到该组的用户
		// String id3 = list3.get(k).getId();
		// List<User> list4 = projectDao.selectUser(id3);
		// for (int m = 0; m < list4.size(); m++) {
		// String id4 = list4.get(m).getId();
		// List<SampleInfo> list6 = new ArrayList<SampleInfo>();
		// list6 = projectDao.selectSampleInfo(id4);
		// list5.addAll(list6);
		// }
		// }
		//
		// }
		// }
		// // 查询销售为空的
		// List<SampleInfo> list7 = new ArrayList<SampleInfo>();
		// list7 = projectDao.selectSampleInfoNull();
		// list5.addAll(list7);
		n.put("list", l);
		return n;
	}

	public Map<String, Object> findSampleInputNewList(Integer start, Integer length, String query, String col,
			String sort, String p_type, String type) throws Exception {

		return sampleMainDao.selectSampleInputNewList(start, length, query, col, sort, p_type, type);
	}

	/**
	 * 样本状态
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param createUser1
	 * @return
	 */

	public Map<String, Object> findCrmPatientStateList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleStateList(scId, startNum, limitNum, dir, sort);
		List<SampleState> list = (List<SampleState>) result.get("list");
		return result;
	}

	/**
	 * 核酸提取结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findDnaSampleInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectDnaSampleInfoList(scId, startNum, limitNum, dir, sort);
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");
		return result;
	}

	/**
	 * 超声破碎结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleUfresultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleUfresultList(scId, startNum, limitNum, dir, sort);
		List<UfTaskResult> list = (List<UfTaskResult>) result.get("list");
		return result;
	}

	/**
	 * FFPE，血液文库结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleWkInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleWkInfoList(scId, startNum, limitNum, dir, sort);
		List<WkTaskInfo> list = (List<WkTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * CtDNA文库结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSampleWkInfoCtDNAList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSampleWkInfoCtDNAList(scId, startNum, limitNum, dir,
	// sort);
	// List<SampleWkInfoCtDNA> list = (List<SampleWkInfoCtDNA>) result
	// .get("list");
	// return result;
	// }

	/**
	 * mRNA文库结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSampleWkInfomRNAList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSampleWkInfomRNAList(scId, startNum, limitNum, dir, sort);
	// List<SampleWkInfomRNA> list = (List<SampleWkInfomRNA>) result
	// .get("list");
	// return result;
	// }

	/**
	 * rRNA文库结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSampleWkInforRNAList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSampleWkInforRNAList(scId, startNum, limitNum, dir, sort);
	// List<SampleWkInforRNA> list = (List<SampleWkInforRNA>) result
	// .get("list");
	// return result;
	// }

	/**
	 * pooling结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSamplePoolingInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSamplePoolingInfoList(scId, startNum, limitNum, dir,
	// sort);
	// List<SamplePoolingInfo> list = (List<SamplePoolingInfo>) result
	// .get("list");
	// return result;
	// }

	/**
	 * 上级测序结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSampleSqquencingInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSamplesequencingInfoList(scId, startNum, limitNum, dir,
	// sort);
	// List<SampleSequencingInfo> list = (List<SampleSequencingInfo>) result
	// .get("list");
	// return result;
	// }

	/**
	 * 下机质控结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleDesequencingInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleDesequencingInfoList(scId, startNum, limitNum, dir,
				sort);
		List<SampleDeSequencingInfo> list = (List<SampleDeSequencingInfo>) result.get("list");
		return result;
	}

	/**
	 * pcr扩增结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSamplePcrInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSamplePcrInfoList(scId, startNum, limitNum, dir, sort);
		List<PcrTaskResult> list = (List<PcrTaskResult>) result.get("list");
		return result;
	}

	/**
	 * Dna纯化结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleDnaInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleDnaInfoList(scId, startNum, limitNum, dir, sort);
		List<DnapTaskResult> list = (List<DnapTaskResult>) result.get("list");
		return result;
	}

	/**
	 * 2100质控结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSample2100InfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSample2100InfoList(scId, startNum, limitNum, dir, sort);
		List<SampleQc2100Info> list = (List<SampleQc2100Info>) result.get("list");
		return result;
	}

	/**
	 * QPCR质控结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleQPCRInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleQpcrInfoList(scId, startNum, limitNum, dir, sort);
		List<SampleQcQpcrInfo> list = (List<SampleQcQpcrInfo>) result.get("list");
		return result;
	}

	/**
	 * 文库检测结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSampleCheckWkInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSampleTechCheckInfoList(scId, startNum, limitNum, dir,
	// sort);
	// List<TechCheckServiceWkInfo> list = (List<TechCheckServiceWkInfo>) result
	// .get("list");
	// return result;
	// }

	/**
	 * 核酸检测结果
	 * 
	 * @param id
	 * @return
	 */
	// public Map<String, Object> findSampleCheckServiceInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = this.sampleMainDao
	// .selectSampleTechCheckServiceInfoList(scId, startNum, limitNum,
	// dir, sort);
	// List<TechCheckServiceInfo> list = (List<TechCheckServiceInfo>) result
	// .get("list");
	// return result;
	// }

	/**
	 * 过滤结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleFiltInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSamplefiltInfoList(scId, startNum, limitNum, dir, sort);
		List<CfdnaTaskResult> list = (List<CfdnaTaskResult>) result.get("list");
		return result;
	}

	/**
	 * cfdna质量评估结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleCfdnaInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleCfdnaInfoList(scId, startNum, limitNum, dir, sort);
		List<SampleFiltrateInfo> list = (List<SampleFiltrateInfo>) result.get("list");
		return result;
	}

	/**
	 * 其他实验结果
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleOtherInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleOtherInfoList(scId, startNum, limitNum, dir, sort);
		List<OtherTaskResult> list = (List<OtherTaskResult>) result.get("list");
		return result;
	}

	/**
	 * 库存信息
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> findSampleSampleInItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.sampleMainDao.selectSampleInItemList(scId, startNum, limitNum, dir, sort);
		List<SampleInItem> list = (List<SampleInItem>) result.get("list");
		return result;
	}

	// 根据id 查询纪录
	public SampleInfo get(String id) {
		SampleInfo sampleInfo = this.sampleMainDao.get(SampleInfo.class, id);
		return sampleInfo;
	}

	// saveOrUpdate
	public void saveOrUpdate(SampleInfo si) {
		this.sampleMainDao.saveOrUpdate(si);
	}

	// 查询订单记录
	public Map<String, Object> findSampleOrderList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		return sampleMainDao.selectSampleOrderList(mapForQuery, startNum, limitNum, dir, sort);
	}

	public SampleInfo findSampleInfoByOrderNumber(String oId) {
		return this.sampleMainDao.findSampleInfoByOrderNumber(oId);
	}

	// 查询样本状态信息
	public List<SampleState> selSampleStateById(String[] ids) {
		List<SampleState> list = new ArrayList<SampleState>();
		for (String id : ids) {
			SampleState s = this.sampleMainDao.get(SampleState.class, id);
			list.add(s);
		}
		return list;
	}

	// 查询单个样本状态
	public List<SampleState> selSampleStateForOne(String sampleCode) {
		return this.sampleMainDao.findSampleStateForOne(sampleCode);

	}

	// 查询所有样本状态
	public List selSampleInfoState() {
		return this.sampleMainDao.findSampleInfoState();

	}

	// 根据原始样本号查询该条数据id
	public String selSampleInfoId(String sampleCode) {
		return this.sampleMainDao.findSampleInfoId(sampleCode);

	}

	// 根据样本编号和任务单号 查询样本状态表
	public Map<String, Object> findSampleStateList(Integer start, Integer length, String query, String col, String sort,
			Object object, String sampleCode, String taskId) {
		return sampleMainDao.selectSampleStateList(start, length, query, col, sort, null, sampleCode, taskId);
	}

	public Map<String, Object> showSampleStateListJsonForOrder(Integer start, Integer length, String query, String col,
			String sort, Object object, String sampleOrder, String taskId) {
		return sampleMainDao.showSampleStateListJsonForOrder(start, length, query, col, sort, null, sampleOrder,
				taskId);
	}

	public Map<String, Object> findSampleStateNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return sampleMainDao.selectSampleStateNewList(start, length, query, col, sort, id);
	}

	public Map<String, Object> findSampleStateTaskKanban(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return sampleMainDao.findSampleStateTaskKanban(start, length, query, col, sort, id);
	}

	public Map<String, Object> findSampleInItemNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return sampleMainDao.selectSampleInItemNewList(start, length, query, col, sort, id);
	}

	public List<SampleState> selectSampleStateOneForOrder(String sampleOrder) {
		return sampleMainDao.selectSampleStateOneForOrder(sampleOrder);
	}

	public Map<String, Object> showSampleReceiveFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		return sampleMainDao.showSampleReceiveFromOrderNo(start, length, query, col, sort, orderNo);
	}

	public Map<String, Object> showCellPassageItemFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		return sampleMainDao.showCellPassageItemFromOrderNo(start, length, query, col, sort, orderNo);
	}

	public Map<String, Object> showCellPassageResultFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		return sampleMainDao.showCellPassageResultFromOrderNo(start, length, query, col, sort, orderNo);
	}

	public Map<String, Object> showQualityTestFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		return sampleMainDao.showQualityTestFromOrderNo(start, length, query, col, sort, orderNo);
	}

}
