package com.biolims.system.sample.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.views.xslt.StringAdapter;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;

import net.sourceforge.pinyin4j.PinyinHelper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleOrderMainService {
	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private CrmPatientService crmPatientService;
	@Resource
	private SampleOrderDao sampleOrderDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrder i) throws Exception {

		sampleOrderDao.saveOrUpdate(i);

	}

	public SampleOrder get(String id) {
		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, id);
		return sampleOrder;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(SampleOrder sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleOrderDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}

	// 查询子表记录
	public List<SampleOrderItem> querySampleItem(String id) {
		return this.sampleOrderDao.querySampleItem(id);
	}

	public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
		return this.sampleOrderDao.querySamplePersonnel(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderPersonnel(SampleOrder sc, String itemDataJson) throws Exception {
		List<SampleOrderPersonnel> saveItems = new ArrayList<SampleOrderPersonnel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderPersonnel scp = new SampleOrderPersonnel();
			// 将map信息读入实体类
			scp = (SampleOrderPersonnel) sampleOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null) {
				if (scp.getId().equals(""))
					scp.setId(null);
			}
			scp.setSampleOrder(sc);
			saveItems.add(scp);
		}
		sampleOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOrderPersonnel(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOrderPersonnel scp = sampleOrderDao.get(SampleOrderPersonnel.class, id);
			sampleOrderDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleOrderItem(SampleOrder sc, String itemDataJson) throws Exception {
		List<SampleOrderItem> saveItems = new ArrayList<SampleOrderItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleOrderItem scp = new SampleOrderItem();
			// 将map信息读入实体类
			scp = (SampleOrderItem) sampleOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleOrder(sc);

			saveItems.add(scp);
		}
		sampleOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleOrderItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleOrderItem scp = sampleOrderDao.get(SampleOrderItem.class, id);
			sampleOrderDao.delete(scp);
		}
	}

	// 根据模板ID加载子表明细
	public List<Map<String, String>> setTemplateItem(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.sampleOrderDao.setTemplateItem(code);
		List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SampleCancerTempPersonnel ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("checkOutTheAge", ti.getCheckOutTheAge());
				if (ti.getSampleCancerTemp().getOrderNumber() != null)
					map.put("sampleorder", ti.getSampleCancerTemp().getOrderNumber());
				else
					map.put("sampleorder", "");
				if (ti.getTumorCategory() != null)
					map.put("tumorCategory", ti.getTumorCategory().getId());
				else
					map.put("tumorCategory", "");
				map.put("familyRelation", ti.getFamilyRelation());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据模板ID加载子表明细2
	public List<Map<String, String>> setSampleOrderItem2(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.sampleOrderDao.setSampleOrderItem(code);
		List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SampleCancerTempItem ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("drugDate", ti.getDrugDate().toString());
				map.put("useDrugName", ti.getUseDrugName());
				map.put("effectOfProgress", ti.getEffectOfProgress());
				map.put("effectOfProgressSpeed", ti.getEffectOfProgressSpeed());
				map.put("geneticTestHistory", ti.getGeneticTestHistory());
				map.put("sampleDetectionName", ti.getSampleDetectionName());
				map.put("sampleExonRegion", ti.getSampleExonRegion());
				map.put("sampleDetectionResult", ti.getSampleDetectionResult());

				map.put("sampleOrder", ti.getSampleCancerTemp().getOrderNumber());

				mapList.add(map);
			}

		}
		return mapList;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleOrder sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleOrderDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleOrderPersonnel");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleOrderPersonnel(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sampleOrderItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleOrderItem(sc, jsonStr);
			}
		}
	}

	public SampleCancerTemp fuzZhi(SampleOrder so) throws Exception {
		SampleCancerTemp a = new SampleCancerTemp();
		a.setOrderNumber(so.getId());
		a.setId(so.getId());
		a.setName(so.getName());
		a.setGender(so.getGender());
		a.setBirthDate(so.getBirthDate());
		a.setDiagnosisDate(DateUtil.parse(so.getDiagnosisDate()));
		a.setDicType(so.getDicType());
		a.setSampleStage(so.getSampleStage());
		a.setInspectionDepartment(so.getInspectionDepartment());
		a.setCrmProduct(so.getCrmProduct());
		a.setProductId(so.getProductId());
		a.setProductName(so.getProductName());
		a.setSamplingDate(so.getSamplingDate());
		a.setSamplingLocation(so.getSamplingLocation());
		a.setSamplingNumber(so.getSamplingNumber());
		a.setPathologyConfirmed(so.getPathologyConfirmed());
		// a.setBloodSampleDate(so.getBloodSampleDate().toString());
		// a.setPlasmapheresisDate(so.getPlasmapheresisDate());
		a.setCommissioner(so.getCommissioner());
		a.setReceivedDate(so.getReceivedDate());
		a.setSampleTypeId(so.getSampleTypeId());
		a.setSampleTypeName(so.getSampleTypeName());
		a.setMedicalNumber(so.getMedicalNumber());
		a.setSampleCode(so.getSampleCode());
		a.setFamily(so.getFamily());
		a.setFamilyPhone(so.getFamilyPhone());
		a.setFamilySite(so.getFamilySite());
		a.setCrmCustomer(so.getCrmCustomer());
		a.setMedicalInstitutions(so.getMedicalInstitutions());
		a.setMedicalInstitutionsPhone(so.getMedicalInstitutionsPhone());
		a.setMedicalInstitutionsSite(so.getMedicalInstitutionsSite());
		a.setCrmDoctor(so.getCrmDoctor());
		a.setAttendingDoctor(so.getAttendingDoctor());
		a.setAttendingDoctorPhone(so.getAttendingDoctorPhone());
		a.setAttendingDoctorSite(so.getAttendingDoctorSite());
		a.setNote(so.getNote());
		a.setCreateUser(so.getCreateUser());
		a.setCreateDate(so.getCreateDate());
		a.setConfirmUser(so.getConfirmUser());
		a.setConfirmDate(so.getConfirmDate());
		a.setState(so.getState());
		a.setStateName(so.getStateName());

		return a;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void chengeState(String sampleOrderId) throws Exception {

		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, sampleOrderId);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		CrmPatient r = null;
		// 电子病历号为空,创建一个
		if (sampleOrder.getMedicalNumber() != null && !"".equals(sampleOrder.getMedicalNumber())) {
			r = crmPatientService.get(sampleOrder.getMedicalNumber());
			// 该电子病历记录存在: 修改电子病历
			r.setId(sampleOrder.getMedicalNumber());
			r.setName(sampleOrder.getName());
			r.setGender(sampleOrder.getGender());
			r.setDateOfBirth(sampleOrder.getBirthDate());
			r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
			r.setCreateDate(sampleOrder.getCreateDate());
			r.setCustomer(sampleOrder.getCrmCustomer());
			r.setCustomerDoctor(sampleOrder.getCrmDoctor());
			r.setCreateUser(sampleOrder.getCreateUser());
			r.setCancerType(sampleOrder.getCancerType());
			r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
			r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
			r.setKs(sampleOrder.getInspectionDepartment());
			crmPatientService.save(r);

		} else {
			String modelName = "CrmPatient";
			String markCode = "P";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleOrder.setMedicalNumber(autoID);
			// 该电子记录不存在:添加电子病历
			r = new CrmPatient();
			r.setId(autoID);
			r.setId(sampleOrder.getMedicalNumber());
			r.setCustomer(sampleOrder.getCrmCustomer());
			r.setCustomerDoctor(sampleOrder.getCrmDoctor());
			r.setName(sampleOrder.getName());
			r.setGender(sampleOrder.getGender());
			r.setDateOfBirth(sampleOrder.getBirthDate());
			r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
			r.setCreateDate(sampleOrder.getCreateDate());
			r.setCreateUser(sampleOrder.getCreateUser());
			r.setCancerType(sampleOrder.getCancerType());
			r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
			r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
			r.setKs(sampleOrder.getInspectionDepartment());
			crmPatientService.save(r);
		}
		sampleOrder.setConfirmUser(user);
		sampleOrder.setConfirmDate(new Date());
		sampleOrder.setState("3");
		sampleOrder.setStateName("预订单");
		SampleInfo si = new SampleInfo();
		si.setSampleOrder(sampleOrder);
		si.setCode(sampleOrder.getBarcode());
		sampleOrder.setBatchState("0");
		sampleOrder.setBatchStateName("订单录入完成");
		commonDAO.saveOrUpdate(si);
		commonDAO.saveOrUpdate(sampleOrder);
	}

	/**
	 * 根据编号查询代理商
	 */
	public List<Map<String, Object>> findPrimaryToSample(String code) throws Exception {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = findPrimary(code);
		List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");

		if (list != null && list.size() > 0) {
			for (PrimaryTask srai : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public Map<String, Object> findPrimary(String code) throws Exception {
		String[] codes = code.split(",");

		List<PrimaryTask> list = new ArrayList<PrimaryTask>();
		for (int i = 0; i < codes.length; i++) {
			PrimaryTask pro = commonDAO.get(PrimaryTask.class, codes[i].trim());
			list.add(pro);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	// 按年查询订单柱状图
	public List selOrderNumberByYear(String dateYear) {
		return sampleOrderDao.findOrderNumberByYear(dateYear);
	}

	// 按年查询订单中检测项目病状图
	public List selProductByOrderByYear(String dateYear) {
		return sampleOrderDao.findProductByOrderByYear(dateYear);
	}
	
	// 按年查询订单状态
	public List selStateByOrderByYear(String dateYear) {
		return sampleOrderDao.findStateByOrderByYear(dateYear);
	}

	// 按月查询订单中检测项目病状图
	public List selProductByOrderByMonth(String dateYearMonth) {
		return sampleOrderDao.findProductByOrderByMonth(dateYearMonth);
	}

	// 查询订单中图片路径
	public List<String> selPic(String id) {
		return sampleOrderDao.findPic(id);
	}

	public List<FileInfo> findFileInfoList(String sId) throws Exception {
		return sampleOrderDao.selectFileInfoList(sId);
	}

	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = sampleOrderDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			SampleOrder so = sampleOrderDao.get(SampleOrder.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					SampleOrderItem sampleOrderItem = new SampleOrderItem();
					sampleOrderItem.setName(reader.get(0));
					if ("男".equals(reader.get(1))) {
						sampleOrderItem.setGender("1");
					} else if ("女".equals(reader.get(0))) {
						sampleOrderItem.setGender("0");
					}
					sampleOrderItem.setSampleCode(reader.get(2));
					sampleOrderItem.setSlideCode(reader.get(3));
					DicSampleType dst = dicSampleTypeDao.selectDicSampleTypeByName(reader.get(4));
					sampleOrderItem.setSampleType(dst);
					sampleOrderItem.setSamplingDate(sdf.parse(reader.get(5)));
					sampleOrderItem.setSampleOrder(so);
					sampleOrderDao.saveOrUpdate(sampleOrderItem);
				}
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeSampleOrderState(String id, String result) throws Exception {

		SampleOrder so = new SampleOrder();
		so = commonDAO.get(SampleOrder.class, id);
		boolean state = sampleOrderDao.findTypeName(id);
		
		if ("9980".equals(result)) {
			so.setState("3");
			if(state) {
			so.setStateName("预订单");
			}else {
			 
		    so.setStateName("新建");
				
			}
		}
		
//		if ("9980".equals(result)) {
//			so.setState("3");
//			so.setStateName("预订单");
//		}
		if ("9981".equals(result)) {
			if (so.getBarcode() != null && !"".equals(so.getBarcode())) {
				so.setBarcode("X-" + so.getBarcode());
			}
			if (so.getFiltrateCode() != null && !"".equals(so.getFiltrateCode())) {
				so.setFiltrateCode("X-" + so.getFiltrateCode());
			}
			if (so.getRound() != null && !"".equals(so.getRound())) {
				so.setRound("X-" + so.getRound());
			}
			if (so.getCcoi() != null && !"".equals(so.getCcoi())) {
				so.setCcoi("X-" + so.getCcoi());
			}
			if (so.getRandomCode()!= null && !"".equals(so.getRandomCode())) {
				so.setRandomCode("X-" + so.getRandomCode());
			}
			if(so.getId()!= null && !"".equals(so.getId())) {
				List<SampleOrderItem> items =new ArrayList<SampleOrderItem>();
				items=sampleOrderDao.getSampleOrderItemListPage(so.getId());
				for (SampleOrderItem sampleOrderItem : items) {
					
					if (sampleOrderItem.getSlideCode()!= null && !"".equals(sampleOrderItem.getSlideCode())) {
						sampleOrderItem.setSlideCode("X-" + sampleOrderItem.getSlideCode());
					}
					commonDAO.saveOrUpdate(sampleOrderItem);
				}
				
			}
			
//			if (so.getRandomCode()!= null && !"".equals(so.getRandomCode())) {
//				so.setRandomCode("X-" + so.getRandomCode());
//			}
			so.setState("1");
			so.setStateName("作废");
		}
		if ("9982".equals(result)) {
			so.setState("3");
			so.setStateName("实际订单");
			Integer countProduct = sampleOrderDao.countPoducFromId(so);
			String productNum = "";
			if (countProduct == 0) {
				productNum = "0001";
			} else if (countProduct > 0 && countProduct < 9) {
				productNum = "000" + (countProduct + 1);
			} else if (countProduct >= 9 && countProduct < 99) {
				productNum = "00" + (countProduct + 1);
			} else if (countProduct >= 99 && countProduct < 999) {
				productNum = "0" + (countProduct + 1);
			} else if (countProduct >= 999) {
				productNum = (countProduct + 1) + "";
			}
			Date drawBloodTime = so.getDrawBloodTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
			String drawBloodTimeStr = sdf.format(drawBloodTime);
			String lc = "";
			if (so.getRound().length() == 1) {
				lc = "0" + so.getRound();
			} else {
				lc = so.getRound();
			}
			String barcode = so.getProductId() + productNum + "-" + lc + "-" + drawBloodTimeStr;
			so.setMedicalNumber(so.getProductId() + productNum);
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			CrmPatient r = new CrmPatient();
			r.setId(so.getProductId() + productNum);
			r.setName(so.getName());
			r.setGender(so.getGender());
			r.setDateOfBirth(so.getBirthDate());
			r.setTelphoneNumber1(so.getFamilyPhone());
			r.setCreateDate(new Date());
			r.setCreateUser(user);
			r.setFiltrateCode(so.getFiltrateCode());
			r.setCcoi(so.getCcoi());
			r.setState("1");
			r.setAge(so.getAge());
			commonDAO.save(r);
			so.setBarcode(barcode);
		}
		commonDAO.merge(so);
	}

	public String getSampleBarCode(String id) {
		SampleOrder so = new SampleOrder();
		so = commonDAO.get(SampleOrder.class, id);
		String name = so.getName();
		//简写
		String abbreviation = so.getAbbreviation();
		String filtrateCode = so.getFiltrateCode();
		String productId = so.getProductId();
		String randomCode = so.getRandomCode();
		String round = so.getRound();
		String vName = "";
		boolean mark = false;
		StringBuffer sb = new StringBuffer();
		if(name!=null&&!"".equals(name)) {
			String substring = name.substring(0, 1);
			mark = substring.matches("[\u4e00-\u9fa5]");
			// SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
			if (mark) {// 汉字形式的姓名
				char[] charArray = name.toCharArray();
//				if (charArray.length == 2) {
//					for (int i = 0; i < charArray.length; i++) {
//						String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
//						String simpleName = pinyinStringArray[0].substring(0, 2).toUpperCase();
//						sb.append(simpleName);
//					}
//					vName = sb.toString();
//				}
//				if (charArray.length == 3) {
//					for (int i = 0; i < 2; i++) {
//						String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
//						String simpleName = pinyinStringArray[0].substring(0, 1).toUpperCase();
//						sb.append(simpleName);
//					}
//					String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[2]);
//					String simpleName = pinyinStringArray[0].substring(0, 2).toUpperCase();
//					sb.append(simpleName);
//					vName = sb.toString();
//				}
//				if (charArray.length == 4) {
//					for (int i = 0; i < 4; i++) {
//						String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
//						String simpleName = pinyinStringArray[0].substring(0, 1).toUpperCase();
//						sb.append(simpleName);
//					}
//					vName = sb.toString();
//				}
//				if (charArray.length > 4) {
					for (int i = 0; i < charArray.length; i++) {
						String[] pinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i]);
						String simpleName = pinyinStringArray[0].substring(0, 1).toUpperCase();
						sb.append(simpleName);
					}
					vName = sb.toString();
//				}
			} else {// 字母简称
				vName = abbreviation;
			}
		}else {
			vName = abbreviation;
		}
		
		if (round.length() == 1) {
			round = "0" + round;
		}
		String result = vName + "-" + filtrateCode + "-" + productId + "-" + randomCode + "-" + round;
		return result;
	}

	public Map<String, Object> selectAgreementTaskTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleOrderDao.selectAgreementTaskTableJson(start, length, query, col, sort);
	}
	/**判断条形码是否重复**/
	public List<SampleOrderItem> getSlideCode(String id) {
		return sampleOrderDao.findSlideCode(id);
	}

	public void changOrderState(String id) {
		if(id!=null
				&&!"".equals(id)) {
			SampleOrder so = commonDAO.get(SampleOrder.class, id);
			if(so!=null) {
				if("1".equals(so.getState())&&"完成".equals(so.getStateName())) {
					so.setState("3");
					so.setStateName("预订单");
					commonDAO.saveOrUpdate(so);
				}
			}
		}
	}
	

	/**
	 * 判断筛选号
	 * @param abbreviation
	 * @return
	 */
	public List<SampleOrder> findFiltrateCode(String id,String sid) {
		return sampleOrderDao.findFiltrateCode(id,sid);
	}
	
	/**
	 * 判断随机号是否重复
	 * @param id
	 * @return
	 */
	public List<SampleOrder> findRandomCode(String id,String sid) {
		return sampleOrderDao.findRandomCode(id,sid);
	}

	/**
	 * 判断姓名简写
	 * @param id
	 * @return
	 */
	public List<SampleOrder> findAbbreviation(String id,String sid) {
		return sampleOrderDao.findAbbreviation(id,sid);
	}

	/**
	 * 查询产品+个数是否存在  不存在可以使用，存在要判断CCOI是否一致
	 * @param string
	 * @return
	 */
	public List<SampleOrder> panduanBarcodeNum(String produc) {
		return sampleOrderDao.panduanBarcodeNum(produc);
	}

	/**
	 * 判断除当前订单，是否存在相同批次号订单
	 * @param string
	 * @param id
	 * @return
	 */
	public List<SampleOrder> panduanBarcodeAndid(String barcode, String id) {
		return sampleOrderDao.panduanBarcodeAndid(barcode,id);
	}

	public Object findRound(String round,String state,String filtrateCode) {
		return sampleOrderDao.findRound(round,state,filtrateCode);
	}

	public List<SampleOrder> roundAndFiltrateCode(String sid,String round,String filtrateCode) {
		
		return sampleOrderDao.roundAndFiltrateCode(sid,round,filtrateCode);
	}

	public List<SampleFutureFind> findId() {
		
		return sampleOrderDao.findId();
	}

	public List<SampleOrder> findSamleDingDan(String filtrateCode) {
		return sampleOrderDao.findSamleDingDan(filtrateCode);
	}

	public SampleOrder findDingDanId(String round, String filtrateCode) {
		return sampleOrderDao.findDingDanId(round,filtrateCode);
	}

}
