package com.biolims.system.sample.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.sample.service.SampleOrderChangeService;

public class SampleOrderChangeEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleOrderChangeService siService = (SampleOrderChangeService) ctx.getBean("sampleOrderChangeService");
		siService.changeState(contentId);
		return "";
	}
}
