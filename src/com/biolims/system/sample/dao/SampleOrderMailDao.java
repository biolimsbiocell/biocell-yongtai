package com.biolims.system.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;

@Repository
@SuppressWarnings("unchecked")
public class SampleOrderMailDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleOrder where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by createDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	//添加子表的dao
	public Map<String, Object> selectSampleOrderPersonnelList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleOrderPersonnel where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleOrder.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrderPersonnel> list = new ArrayList<SampleOrderPersonnel>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectSampleOrderItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleOrderItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleOrder.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//根据主表ID加载子表明细数据
		public Map<String,Object> setTemplateItem(String code){
			String hql="from SampleCancerTempPersonnel t where 1=1 and t.sampleCancerTemp.orderNumber='"+code+"'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
			List<SampleCancerTempPersonnel> list = this.getSession().createQuery(hql).list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		//根据主表ID加载子表明细数据2
				public Map<String,Object> setSampleOrderItem(String code){
					String hql="from SampleCancerTempItem t where 1=1 and t.sampleCancerTemp.orderNumber='"+code+"'";
					Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
					List<SampleCancerTempItem> list = this.getSession().createQuery(hql).list();
					Map<String, Object> result = new HashMap<String, Object>();
					result.put("total", total);
					result.put("list", list);
					return result;
				}
		//根据主表ID加载子表明细数据3
				public List<SampleOrderItem> querySampleItem(String id){
					String hql="from SampleOrderItem t where 1=1 and t.sampleOrder.id='"+id+"'";
					List<SampleOrderItem> list = this.getSession().createQuery(hql).list();
					return list;
				}
		//根据主表ID加载子表明细数据4
				public List<SampleOrderPersonnel> querySamplePersonnel(String id){
					String hql="from SampleOrderPersonnel t where 1=1 and t.sampleOrder.id='"+id+"'";
					List<SampleOrderPersonnel> list = this.getSession().createQuery(hql).list();
					return list;
				}
		
}