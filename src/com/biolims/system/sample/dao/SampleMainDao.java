package com.biolims.system.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.analysis.filt.model.SampleFiltrateInfo;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cfdna.model.CfdnaTaskResult;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.dnap.model.DnapTaskResult;
import com.biolims.experiment.other.model.OtherTaskResult;
import com.biolims.experiment.pcr.model.PcrTaskResult;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.uf.model.UfTaskResult;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleProductwdInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.model.SampleState;
import com.biolims.sample.storage.model.SampleInItem;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;

@Repository
@SuppressWarnings("unchecked")
public class SampleMainDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInputList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String createUser1) {
		String key = " ";
		String hql = " from SampleInfo t where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSampleInputNewList(Integer start, Integer length, String query, String col,
			String sort, String p_type, String type) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  SampleInfo where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		// if(p_type!=null){
		// key+=" and p_type='"+p_type+"' ";
		// }
		// if (type != null) {
		// key += " and studyType='" + type + "' ";
		// }

		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleInfo where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	/**
	 * 样本状态
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param createUser1
	 * @return
	 */
	public Map<String, Object> selectSampleStateList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleState where 1=1  ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "' order by endDate desc";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleState> list = new ArrayList<SampleState>();
		if (total > 0) {
			// if (dir != null && dir.length() > 0 && sort != null &&
			// sort.length() > 0) {
			// if (sort.indexOf("-") != -1) {
			// sort = sort.substring(0, sort.indexOf("-"));
			// }
			// key = key + " order by " + sort + " " + dir;
			// }
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 核酸提取结果
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDnaSampleInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from DnaTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<DnaTaskInfo> list = new ArrayList<DnaTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 超声破碎结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleUfresultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from UfTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<UfTaskResult> list = new ArrayList<UfTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * FFPE，血液文库结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleWkInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from WkTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkTaskInfo> list = new ArrayList<WkTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * CtDNA文库结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSampleWkInfoCtDNAList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// String hql = "from SampleWkInfoCtDNA where 1=1 ";
	// String key = "";
	// if (scId != null)
	// key = key + " and sampleCode ='" + scId + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SampleWkInfoCtDNA> list = new ArrayList<SampleWkInfoCtDNA>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * mRNA文库结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSampleWkInfomRNAList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// String hql = "from SampleWkInfomRNA where 1=1 ";
	// String key = "";
	// if (scId != null)
	// key = key + " and sampleCode ='" + scId + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SampleWkInfomRNA> list = new ArrayList<SampleWkInfomRNA>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * rRNA文库结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSampleWkInforRNAList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// String hql = "from SampleWkInforRNA where 1=1 ";
	// String key = "";
	// if (scId != null)
	// key = key + " and sampleCode ='" + scId + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SampleWkInforRNA> list = new ArrayList<SampleWkInforRNA>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * Pooling结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSamplePoolingInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// String hql = "from SamplePoolingInfo where poolingTask.id in (select
	// poolingTask.id from PoolingTaskItem where 1=1 and sampleCode ='"
	// + scId + "')";
	// String key = "";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SamplePoolingInfo> list = new ArrayList<SamplePoolingInfo>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * 上机测序结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSamplesequencingInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// // 根据样本编号查询poolingItem明细
	// String hqlpoolingItem = "from PoolingTaskItem where 1=1 ";
	// String keypoolingItem = "";
	// if (scId != null) {
	// keypoolingItem = keypoolingItem + " and sampleCode ='" + scId + "'";
	// }
	// List<PoolingTaskItem> poolingList = new ArrayList<PoolingTaskItem>();
	// poolingList = this.getSession()
	// .createQuery(hqlpoolingItem + keypoolingItem).list();
	// PoolingTaskItem p = poolingList.get(0);
	// // 根据样本编号查询上机测序结果
	// String hql = "from SampleSequencingInfo where 1=1 ";
	// String key = "";
	// if (p.getPoolingCode() != null)
	// key = key + " and poolingId ='" + p.getPoolingTask().getId() + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SampleSequencingInfo> list = new ArrayList<SampleSequencingInfo>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	//
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * 下机质控结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleDesequencingInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		// 根据样本编号查询poolingItem明细
		String hqlItem = "from DeSequencingTaskItem where 1=1 ";
		String keyItem = "";
		if (scId != null) {
			keyItem = keyItem + " and sampleCode ='" + scId + "'";
		}
		List<DeSequencingTaskItem> deseList = new ArrayList<DeSequencingTaskItem>();
		deseList = this.getSession().createQuery(hqlItem + keyItem).list();
		Long total = (long) 0;
		List<SampleDeSequencingInfo> list = new ArrayList<SampleDeSequencingInfo>();
		if (deseList.size() > 0) {
			list = null;
		} else {
			DeSequencingTaskItem d = deseList.get(0);
			// 查询Fc号结果
			String hql = "from SampleDeSequencingInfo where 1=1 ";
			String key = "";
			if (d.getPoolingCode() != null)
				key = key + " and poolingCode ='" + d.getPoolingCode() + "'";
			total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();

			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				}
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum)
							.list();
				}
			}
		}

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * Pcr扩增结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSamplePcrInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from PcrTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<PcrTaskResult> list = new ArrayList<PcrTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * Dna扩增结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleDnaInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from DnapTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<DnapTaskResult> list = new ArrayList<DnapTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 2100质控结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSample2100InfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleQc2100Info where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleQc2100Info> list = new ArrayList<SampleQc2100Info>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * QPCR质控结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleQpcrInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleQcQpcrInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleQcQpcrInfo> list = new ArrayList<SampleQcQpcrInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 文库检测结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSampleTechCheckInfoList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// String hql = "from TechCheckServiceWkInfo where 1=1 ";
	// String key = "";
	// if (scId != null)
	// key = key + " and sampleCode ='" + scId + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<TechCheckServiceWkInfo> list = new ArrayList<TechCheckServiceWkInfo>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * 核酸检测结果
	 * 
	 * @param scId
	 */
	// public Map<String, Object> selectSampleTechCheckServiceInfoList(
	// String scId, Integer startNum, Integer limitNum, String dir,
	// String sort) throws Exception {
	// String hql = "from TechCheckServiceInfo where 1=1 ";
	// String key = "";
	// if (scId != null)
	// key = key + " and sampleCode ='" + scId + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<TechCheckServiceInfo> list = new ArrayList<TechCheckServiceInfo>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	/**
	 * 过滤结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSamplefiltInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleFiltrateInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleFiltrateInfo> list = new ArrayList<SampleFiltrateInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * cfDna质量评估结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleCfdnaInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CfdnaTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CfdnaTaskResult> list = new ArrayList<CfdnaTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 其他实验结果
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleOtherInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from OtherTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<OtherTaskResult> list = new ArrayList<OtherTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 库存信息
	 * 
	 * @param scId
	 */
	public Map<String, Object> selectSampleInItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleInItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleCode ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInItem> list = new ArrayList<SampleInItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询两次记录
	public SampleInput findListBySampleInfo(String id) {
		String hql = "from SampleInput t where 1=1 and t.code ='" + id + "'";
		List<SampleInput> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	// 查询两次记录
	public SampleInfo findSampleInfoByOrderNumber(String oId) {
		String hql = "from SampleInfo t where 1=1 and t.orderNum ='" + oId + "'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	// 查询订单记录
	public Map<String, Object> selectSampleOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from sampleOrder where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 根据检测项目ID和样本号查询检测项目位点信息
	 * 
	 * @param id
	 * @return
	 */
	public List<SampleProductwdInfo> getSampleProductwdInfoList(String pid, String code) {
		String hql = "from SampleProductwdInfo t where 1=1 and t.productId='" + pid + "' and t.code ='" + code + "'";
		List<SampleProductwdInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询单个样本状态
	public List<SampleState> findSampleStateForOne(String sampleCode) {
		String hql = "from SampleState where 1=1 and sampleCode ='" + sampleCode
				+ "'group by taskId order by endDate desc";
		List<SampleState> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 样本主数据所有样本
	public List findSampleInfoState() {
		String sql = "select  t.sample_code,t.stage_name from t_sample_state t where 1=1 group by t.sample_code,t.stage_name order by t.sample_code ";
		List<Object[]> list = this.getSession().createSQLQuery(sql).list();
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < list.size(); i++) {
			list.get(i);
			String sampleCode = list.get(i)[0].toString();
			String stageName = list.get(i)[1].toString();
			// map.put(sampleCode,stageName);
			if (map.keySet().contains(sampleCode)) {
				map.put(sampleCode, map.get(sampleCode) + "," + stageName);
			} else {
				map.put(sampleCode, stageName);
			}
		}
		// JSONObject jsonObject = JSONObject.fromObject(map);
		JSONArray jsonObject = JSONArray.fromObject(map);
		return jsonObject;
	}

	// 根据原始样本号查询该条数据id
	public String findSampleInfoId(String sampleCode) {
		String sql = "select t.id from sample_info t where 1=1 and t.code='" + sampleCode + "'";
		String id = (String) this.getSession().createSQLQuery(sql).uniqueResult();
		return id;
	}

	// 根据样本编号和任务单号 查询样本状态表
	public Map<String, Object> selectSampleStateList(Integer start, Integer length, String query, String col,
			String sort, Object object, String sampleCode, String taskId) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleState where 1=1 ";
		String key = "";
		if (!sampleCode.equals(""))
			key = key + " and sampleCode like '%" + sampleCode + "%' ";
		else
			key = key + " and 1=2";
		if (!taskId.equals(""))
			key = key + " and taskId ='" + taskId + "' ";
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleState where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			} else {
				key += " order by code ";
			}
			List<SampleState> list = new ArrayList<SampleState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showSampleStateListJsonForOrder(Integer start, Integer length, String query, String col,
			String sort, Object object, String sampleOrder, String taskId) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleState where 1=1 ";
		String key = "";
		if (sampleOrder != null && !sampleOrder.equals(""))
			key = key + " and sampleOrder.id = '" + sampleOrder + "' ";
		else
			key = key + " and 1=2";
		if (!taskId.equals(""))
			key = key + " and taskId ='" + taskId + "' ";
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleState where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			} else {
				key += " order by code ";
			}
			List<SampleState> list = new ArrayList<SampleState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectSampleStateNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleState where 1=1 ";
		String key = "";
		if (!id.equals(""))
			key = key + "and sampleCode ='" + id + "' ";
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleState where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			} else {
				key += " order by endDate desc ";
			}
			List<SampleState> list = new ArrayList<SampleState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleStateTaskKanban(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleState where 1=1 ";
		String key = "";
		if (id != null && !"".equals(id)) {

			key = key + "and sampleOrder.id ='" + id + "' ";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleState where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			} else {
				key += " order by endDate desc ";
			}
			List<SampleState> list = new ArrayList<SampleState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectSampleInItemNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleInfoIn where 1=1 ";
		String key = "";
		if (!id.equals(""))
			key = key + "and sampleCode ='" + id + "' and state='1' ";
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleInfoIn where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			} else {

			}
			List<SampleState> list = new ArrayList<SampleState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleState> selectSampleStateOneForOrder(String sampleOrder) {
		String hql = "from SampleState where 1=1 and sampleOrder.id ='" + sampleOrder
				+ "'group by taskId order by endDate desc";
		List<SampleState> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showSampleReceiveFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReceiveItem where 1=1 and orderCode='" + orderNo + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleReceiveItem  where 1=1 and orderCode='" + orderNo + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
			}
			key += " order by " + col + " " + sort;
			List<SampleReceiveItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showCellPassageItemFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageItem where 1=1 and orderCode='" + orderNo + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageItem  where 1=1 and orderCode='" + orderNo + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
			}
			if ("id".equals(col)) {
				key += " order by stepNum+0 asc";
			} else {
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showCellPassageResultFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageInfo where 1=1 and sampleOrder='" + orderNo + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageInfo  where 1=1 and sampleOrder='" + orderNo + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
			}
			key += " order by " + col + " " + sort;
			List<CellPassageInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showQualityTestFromOrderNo(Integer start, Integer length, String query, String col,
			String sort, String orderNo) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1 and sampleOrder='" + orderNo + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1 and sampleOrder='" + orderNo + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
			}
			key += " order by " + col + " " + sort;
			List<QualityTestInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}