package com.biolims.abnormal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.system.abnomal.model.AbnomalType;

/**
 * @Title: Model
 * @Description: 异常样本
 * @author lims-platform
 * @date 2015-11-03 16:18:35
 * @version V1.0
 * 
 */
@Entity
@Table(name = "ABNORMAL_ITEM")
@SuppressWarnings("serial")
public class AbnormalItem extends EntityDao<AbnormalItem> implements
		java.io.Serializable {

	// 编号
	private String id;
	// 收样样本id
	private String itemId;
	// 分组号
	private String name;
	// 处理时间
	private Date hangdleDate;
	private String state;
	// 病人姓名
	private String patientName;
	// 处理意见
	private String advice;
	// 备注
	private String note;
	// 样本号
	private String sampleCode;
	// 身份证号
	private String idCard;
	/* 手机号 */
	private String phone;
	// 科研项目
	private Project project;
	// 客户
	private CrmCustomer crmCustomer;
	// 医生
	private CrmDoctor crmDoctor;
	/** 产品类型 1科研 2临床 */
	private String sampleStyle;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 关联任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/** 执行时间 */
	private Date runTime;
	/** 反馈时间 */
	private Date feedBackTime;
	// 处理结果
	private String method;
	/** 确认执行 */
	private String isExecute;
	/** 下一步流向 */
	private String nextFlow;
	/** 下一步流向Id */
	private String nextFlowId;
	// 样本量
	private String sampleNum;
	// /** 异常类型 */
	private AbnomalType unusual;
	// 异常处理
	private DicType dicType;
	// 物种
	private String species;
	/* 应出报告日期 */
	private String reportDate;
	/** 样本类型 */
	private DicSampleType dicSampleType;

	private String sampleType;

	public String getSampleStyle() {
		return sampleStyle;
	}

	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	/**
	 * 科研项目
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 异常类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNUSUAL")
	public AbnomalType getUnusual() {
		return unusual;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 异常类型
	 */
	public void setUnusual(AbnomalType unusual) {
		this.unusual = unusual;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE")
	public DicType getDicType() {
		return dicType;
	}

	public void setDicType(DicType dicType) {
		this.dicType = dicType;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	/**
	 * 方法: 取得DicSampleType
	 * 
	 * @return: DicSampleType 样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	/**
	 * 方法: 设置DicSampleType
	 * 
	 * @param: DicSampleType 样本类型
	 */
	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	// private SampleReceiveItem sampleReceiveItem;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 信息录入id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理时间
	 */
	@Column(name = "HANDLE_DATE", length = 50)
	public Date getHangdleDate() {
		return hangdleDate;
	}

	public void setHangdleDate(Date hangdleDate) {
		this.hangdleDate = hangdleDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理意见
	 */
	@Column(name = "ADVICE", length = 50)
	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	/**
	 * 方法：获取RunTime
	 * 
	 * @return Date 执行时间
	 */
	@Column(name = "RUN_TIME", length = 50)
	public Date getRunTime() {
		return runTime;
	}

	/**
	 * 方法：设置RunTime
	 * 
	 * @param runTime
	 *            Date 执行时间
	 */
	public void setRunTime(Date runTime) {
		this.runTime = runTime;
	}

	/**
	 * 方法：获取FeedBackTime
	 * 
	 * @return Date 反馈时间
	 */
	@Column(name = "FEED_BACK_TIME", length = 50)
	public Date getFeedBackTime() {
		return feedBackTime;
	}

	/**
	 * 方法：设置FeedBackTime
	 * 
	 * @param feedBackTime
	 *            Date 反馈时间
	 */
	public void setFeedBackTime(Date feedBackTime) {
		this.feedBackTime = feedBackTime;
	}

	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "SAMPLE_RECEIVE_ITEM")
	// public SampleReceiveItem getSampleReceiveItem() {
	// return sampleReceiveItem;
	// }
	// public void setSampleReceiveItem(SampleReceiveItem sampleReceiveItem) {
	// this.sampleReceiveItem = sampleReceiveItem;
	// }

	@Column(name = "PATINET_NAME", length = 50)
	public String getPatientName() {
		return patientName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method
	 *            the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

}