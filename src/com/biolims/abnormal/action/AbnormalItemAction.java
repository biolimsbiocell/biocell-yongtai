﻿package com.biolims.abnormal.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.abnormal.model.AbnormalItem;
import com.biolims.abnormal.service.AbnormalItemService;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/abnormal/abnormalItem")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AbnormalItemAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";//2103
	@Autowired
	private AbnormalItemService abnormalItemService;
	private AbnormalItem abnormalItem = new AbnormalItem();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showAbnormalItemTable")
	public String showAbnormalItemTable() throws Exception {
		rightsId="xmzycgl";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/abnormal/abnormalItemTable.jsp");
	}
	@Action(value = "showAbnormalItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAbnormalItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = abnormalItemService
				.showAbnormalItemTableJson(start, length, query, col, sort);
		List<AbnormalItem> list = (List<AbnormalItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("hangdleDate", "yyyy-MM-dd");
		map.put("patientName", "");
		map.put("advice", "");
		map.put("note", "");
		map.put("idCard", "");
		map.put("sampleCode", "");
		map.put("phone", "");
		map.put("runTime", "yyyy-MM-dd");
		map.put("feedBackTime", "yyyy-MM-dd");
		map.put("method", "");
		map.put("reportDate", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("isExecute", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("state", "");
		map.put("itemId", "");
		map.put("sampleNum", "");
		map.put("sampleType", "");
		map.put("unusual-id", "");
		map.put("unusual-name", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmDoctor-id", "");
		map.put("crmDoctor-name", "");
		map.put("species", "");
		map.put("orderId", "");
		map.put("sampleStyle", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result,data));
	}
	/**
	 * 
	 * @Title: save  
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年5月3日下午2:50:36
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("dataJson");
			abnormalItemService.saveSampleItem(dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: executeAbnormal  
	 * @Description: 保存并执行
	 * @author : shengwei.wang
	 * @date 2018年2月1日下午2:03:05
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="executeAbnormal")
	public void executeAbnormal() throws Exception {
		String [] ids=getRequest().getParameterValues("ids[]");
		String dataJson=getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			abnormalItemService.executeAbnormal(ids,dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));		
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
	/**
	 * @return the abnormalItemService
	 */
	public AbnormalItemService getAbnormalItemService() {
		return abnormalItemService;
	}
	/**
	 * @param abnormalItemService the abnormalItemService to set
	 */
	public void setAbnormalItemService(AbnormalItemService abnormalItemService) {
		this.abnormalItemService = abnormalItemService;
	}
	/**
	 * @return the abnormalItem
	 */
	public AbnormalItem getAbnormalItem() {
		return abnormalItem;
	}
	/**
	 * @param abnormalItem the abnormalItem to set
	 */
	public void setAbnormalItem(AbnormalItem abnormalItem) {
		this.abnormalItem = abnormalItem;
	}
}
