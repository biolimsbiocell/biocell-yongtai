package com.biolims.abnormal.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.abnormal.dao.AbnormalItemDao;
import com.biolims.abnormal.model.AbnormalItem;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class AbnormalItemService {
	@Resource
	private AbnormalItemDao abnormalItemDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;

	StringBuffer json = new StringBuffer();

//	public SampleAbnormal get(String id) {
//		SampleAbnormal sampleAbnormal = commonDAO.get(SampleAbnormal.class, id);
//		return sampleAbnormal;
//	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleItem(String itemDataJson,String changeLog)
			throws Exception {
		List<SampleAbnormal> saveItems1 = new ArrayList<SampleAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		SampleAbnormal scp = new SampleAbnormal();
		for(Map<String, Object> map:list){
			scp = (SampleAbnormal) abnormalItemDao.Map2Bean(map, scp);
			SampleAbnormal sbi=commonDAO.get(SampleAbnormal.class, scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setMethod(scp.getMethod());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId("");
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		
		abnormalItemDao.saveOrUpdateAll(saveItems1);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void next(AbnormalItem sbi) throws Exception {
		if (sbi.getNextFlowId().equals("0009")) {// 样本入库
			SampleInItemTemp st = new SampleInItemTemp();
			st.setCode(sbi.getSampleCode());
			st.setSampleCode(sbi.getSampleCode());
			if (sbi.getSampleNum() != null) {
				st.setSumTotal(Double.parseDouble(sbi.getSampleNum()));
			}
			// DicSampleType t = commonDAO.get(DicSampleType.class, sri
			// .getDicSampleType().getId());
			st.setSampleType(sbi.getDicSampleType().getName());
			st.setState("2");
			st.setPatientName(sbi.getPatientName());
			st.setSampleTypeId(sbi.getDicSampleType().getId());
			st.setInfoFrom("SampleInfo");
			sampleReceiveDao.saveOrUpdate(st);
		} else if (sbi.getNextFlowId().equals("0017")) {// 核酸提取
			DnaTaskTemp d = new DnaTaskTemp();
			sbi.setState("2");
			sampleInputService.copy(d, sbi);
			if (sbi.getSampleNum() != null) {
			}
			d.setSampleCode(sbi.getSampleCode());
			d.setCode(sbi.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sbi
					.getDicSampleType().getId());
			d.setSampleType(t.getName());
			sampleReceiveDao.saveOrUpdate(d);
		} else if (sbi.getNextFlowId().equals("0018")) {// 文库构建
			WkTaskTemp d = new WkTaskTemp();
			sbi.setState("2");
			sampleInputService.copy(d, sbi);
			d.setSampleCode(sbi.getSampleCode());
			d.setCode(sbi.getSampleCode());
			DicSampleType t = commonDAO.get(DicSampleType.class, sbi
					.getDicSampleType().getId());
			d.setSampleType(t.getName());
			sampleReceiveDao.saveOrUpdate(d);
		} else {
			// 得到下一步流向的相关表单
			List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(sbi
					.getNextFlowId());
			for (NextFlow n : list_nextFlow) {
				Object o = Class.forName(
						n.getApplicationTypeTable().getClassPath())
						.newInstance();
				sbi.setState("2");
				DicSampleType t = commonDAO.get(DicSampleType.class, sbi
						.getDicSampleType().getId());
				sbi.setSampleType(t.getName());
				try {
					BeanUtils.getDeclaredField(o, "code");
					BeanUtils.setFieldValue(o, "code", sbi.getSampleCode());

				} catch (NoSuchFieldException e) {
				}
				sampleInputService.copy(o, sbi);
			}
		}

	}

//	public Map<String, Object> selectAbnormalTypeAll() {
//		return abnormalItemDao.selectAbnormalTypeAll();
//	}

//	public Map<String, Object> selectDicUnusualMethodAll() {
//		return abnormalItemDao.selectDicUnusualMethodAll();
//	}

	public Map<String, Object> showAbnormalItemTableJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return abnormalItemDao.showAbnormalItemTableJson(start, length, query, col, sort);
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson, String changeLog) throws Exception {

		List<AbnormalItem> saveItems1 = new ArrayList<AbnormalItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				dataJson, List.class);
		AbnormalItem scp = new AbnormalItem();
		for(Map<String, Object> map:list){
			scp = (AbnormalItem) abnormalItemDao.Map2Bean(map, scp);
			AbnormalItem sbi=commonDAO.get(AbnormalItem.class, scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setMethod(scp.getMethod());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId("");
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		abnormalItemDao.saveOrUpdateAll(saveItems1);
		List<AbnormalItem> saveItems = new ArrayList<AbnormalItem>();
		for(String id:ids){
			AbnormalItem sbi=commonDAO.get(AbnormalItem.class, id);
			sbi.setIsExecute("1");
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {
					String itemId = sbi.getItemId();
					SampleReceiveItem sri = abnormalItemDao.get(
							SampleReceiveItem.class, itemId);
					if(sbi.getMethod().equals("1")){
						next(sbi);
					}else{
					}
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					sampleStateService
							.saveSampleState(
									sri.getSampleCode(),
									sri.getSampleCode(),
									sri.getProductId(),
									sri.getProductName(),
									"",
									format.format(sri.getAcceptDate()),
									format.format(new Date()),
									"AbnormalItem",
									"项目组异常",
									(User) ServletActionContext
											.getRequest()
											.getSession()
											.getAttribute(
													SystemConstants.USER_SESSION_KEY),
									sri.getId(), sbi.getNextFlow(),
									sri.getMethod(), null, null, null,
									null, null, null, null, null);
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		abnormalItemDao.saveOrUpdateAll(saveItems);
	
	}
}
