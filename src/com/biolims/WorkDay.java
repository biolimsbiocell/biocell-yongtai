package com.biolims;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.biolims.util.DateUtil;

public class WorkDay {
	/**
	 * 显示工作日
	 * @param date1
	 * @param date2
	* @return 
	 */

	private static List<Calendar> holidayList;
	private static boolean holidayFlag;

	/**
	 * 计算工作日
	 * 具体节日包含哪些,可以在HolidayMap中修改
	 * @param src 日期(源)
	 * @param adddays 要加的天数
	 * @exception throws [违例类型] [违例说明]
	 * @version  [s001, 2010-11-19]
	 * @author  shenjunjie
	 */
	public static Calendar addDateByWorkDay(Calendar src, int adddays) {
		//	        Calendar result = null;
		holidayFlag = false;
		for (int i = 0; i < adddays; i++) {
			//把源日期加一天
			src.add(Calendar.DAY_OF_MONTH, 1);
			holidayFlag = checkHoliday(src);
			if (holidayFlag) {
				i--;
			}
			System.out.println(src.getTime());
		}
		System.out.println("Final Result:" + src.getTime());
		return src;
	}

	public static int showTime(Date date1, Date date2) {
		//  这里要判断第二个参数日期要比第一个参数日期大先继续运行
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat holidaysdf = new SimpleDateFormat("MM-dd");
		//工作日
		int workDay = 0;
		try {
			//		   Date d1 = sdf.parse(date1);
			//		   Date d2 = sdf.parse(date2);
			//		   gc.setTime(d1);
			//   System.out.println(sdf2.format(d1));
			long time = date2.getTime() - date1.getTime();
			long day = time / 3600000 / 24 + 1;
			//   System.out.println(day);
			for (int i = 0; i < day; i++) {
				if (gc.get(GregorianCalendar.DAY_OF_WEEK) != GregorianCalendar.SATURDAY
						&& gc.get(GregorianCalendar.DAY_OF_WEEK) != GregorianCalendar.SUNDAY) {
					//		     System.out.println(holidaysdf.format(gc.getTime()));
					if (!holidayList(holidaysdf.format(gc.getTime())) && !holidayOfCN(sdf.format(gc.getTime())))
						workDay++;
				}
				//天数加1
				gc.add(gc.DATE, 1);
			}
			//   gc.add(gc.DATE,1);
			//   System.out.println(sdf.format(gc.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		//  System.out.println(workDay);
		return workDay;
	}

	public static Date addTime(Date date1, int day) {
		//  这里要判断第二个参数日期要比第一个参数日期大先继续运行
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat holidaysdf = new SimpleDateFormat("MM-dd");

		try {
			//		   Date d1 = sdf.parse(date1);
			int a = 0;
			//   System.out.println(sdf2.format(d1));
			//   System.out.println(day);
			for (int i = 0; i < day; i++) {
				Date d2 = date1;
				gc.setTime(d2);
				if (gc.get(GregorianCalendar.DAY_OF_WEEK) != GregorianCalendar.SATURDAY
						&& gc.get(GregorianCalendar.DAY_OF_WEEK) != GregorianCalendar.SUNDAY) {
					//		     System.out.println(holidaysdf.format(gc.getTime()));

					date1 = DateUtil.addDay(date1, 1);
					//if (!holidayList(holidaysdf.format(gc.getTime())) && !holidayOfCN(sdf.format(gc.getTime())))
					//	date1 = DateUtil.addDay(date1, 1);
				} else {
					date1 = DateUtil.addDay(date1, 1);
					i = i - 1;

				}
				//天数加1

			}
			//   gc.add(gc.DATE,1);
			//   System.out.println(sdf.format(gc.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		//  System.out.println(workDay);
		return date1;
	}

	//春节放假三天，定义到2020年
	public static boolean holidayOfCN(String year) {
		List ls = new ArrayList();
		ls.add("2005-02-09");
		ls.add("2005-02-10");
		ls.add("2005-02-11");
		ls.add("2006-01-29");
		ls.add("2006-01-30");
		ls.add("2006-01-31");
		ls.add("2007-02-18");
		ls.add("2007-02-19");
		ls.add("2007-02-21");
		ls.add("2008-02-07");
		ls.add("2008-02-08");
		ls.add("2008-02-09");
		ls.add("2009-01-26");
		ls.add("2009-01-27");
		ls.add("2009-01-28");
		ls.add("2010-02-14");
		ls.add("2010-02-15");
		ls.add("2010-02-16");
		ls.add("2011-02-03");
		ls.add("2011-02-04");
		ls.add("2011-02-05");
		ls.add("2012-01-23");
		ls.add("2012-01-24");
		ls.add("2012-01-25");
		ls.add("2013-02-10");
		ls.add("2013-02-11");
		ls.add("2013-02-12");
		ls.add("2014-01-31");
		ls.add("2014-02-01");
		ls.add("2014-02-02");
		ls.add("2015-02-19");
		ls.add("2015-02-20");
		ls.add("2015-02-21");
		ls.add("2006-02-08");
		ls.add("2006-02-09");
		ls.add("2006-02-10");
		ls.add("2017-01-28");
		ls.add("2017-01-29");
		ls.add("2017-01-30");
		ls.add("2018-02-16");
		ls.add("2018-02-17");
		ls.add("2018-02-18");
		ls.add("2019-02-05");
		ls.add("2019-02-06");
		ls.add("2019-02-07");
		ls.add("2020-01-25");
		ls.add("2020-01-26");
		ls.add("2020-01-27");
		if (ls.contains(year))
			return true;
		return false;
	}

	//法定假日，五一和国庆
	public static boolean holidayList(String findDate) {
		List ls = new ArrayList();
		ls.add("05-01");
		ls.add("05-02");
		ls.add("05-03");
		ls.add("10-01");
		ls.add("10-02");
		ls.add("10-03");
		if (ls.contains(findDate))
			return true;
		return false;
	}

	public static boolean checkHoliday(Calendar src) {
		boolean result = false;
		if (holidayList == null) {
			initHolidayList();
		}
		//先检查是否是周六周日(有些国家是周五周六)
		if (src.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || src.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			return true;
		}
		for (Calendar c : holidayList) {
			if (src.get(Calendar.MONTH) == c.get(Calendar.MONTH)
					&& src.get(Calendar.DAY_OF_MONTH) == c.get(Calendar.DAY_OF_MONTH)) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 初始化节日List,如果需要加入新的节日,请在这里添加
	 * 加的时候请尽量使用Calendar自带的常量而不是魔鬼数字
	 * 注:年份可以随便写,因为比的时候只比月份和天
	 * @version  [s001, 2010-11-19]
	 * @author  shenjunjie
	 */
	private static void initHolidayList() {
		holidayList = new ArrayList();

		//五一劳动节
		Calendar may1 = Calendar.getInstance();
		may1.set(Calendar.MONTH, Calendar.MAY);
		may1.set(Calendar.DAY_OF_MONTH, 1);
		holidayList.add(may1);

		Calendar may2 = Calendar.getInstance();
		may2.set(Calendar.MONTH, Calendar.MAY);
		may2.set(Calendar.DAY_OF_MONTH, 2);
		holidayList.add(may2);

		Calendar may3 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.MAY);
		may3.set(Calendar.DAY_OF_MONTH, 3);
		holidayList.add(may3);

		Calendar oct1 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 1);
		holidayList.add(oct1);

		Calendar oct2 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 2);
		holidayList.add(oct2);

		Calendar oct3 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 3);
		holidayList.add(oct3);

		Calendar oct4 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 4);
		holidayList.add(oct4);

		Calendar oct5 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 5);
		holidayList.add(oct5);

		Calendar oct6 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 6);
		holidayList.add(oct6);

		Calendar oct7 = Calendar.getInstance();
		may3.set(Calendar.MONTH, Calendar.OCTOBER);
		may3.set(Calendar.DAY_OF_MONTH, 7);
		holidayList.add(oct7);
		//		Calendar h3 = Calendar.getInstance();
		//		h3.set(2000, 1, 1);
		//		holidayList.add(h3);
		//
		//		Calendar h4 = Calendar.getInstance();
		//		h4.set(2000, 12, 25);
		//		holidayList.add(h4);

		//		//中国母亲节：五月的第二个星期日
		//		Calendar may5 = Calendar.getInstance();
		//		//设置月份为5月
		//		may5.set(Calendar.MONTH, Calendar.MAY);
		//		//设置星期:第2个星期
		//		may5.set(Calendar.DAY_OF_WEEK_IN_MONTH, 2);
		//		//星期日
		//		may5.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		//		//        System.out.println(may5.getTime());

		//	holidayList.add(may5);
	}
}
