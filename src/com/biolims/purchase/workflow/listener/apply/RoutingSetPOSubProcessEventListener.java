package com.biolims.purchase.workflow.listener.apply;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.model.user.User;
import com.biolims.purchase.apply.service.PurchaseApplyService;
import com.biolims.purchase.common.constants.SystemConstants;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * @author niyi
 *
 */
public class RoutingSetPOSubProcessEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		String businessKey = execution.getProcessBusinessKey();
		PurchaseApplyService purchaseApplyService = (PurchaseApplyService) ctx.getBean("purchaseApplyService");
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		String purchaseOrderId = purchaseApplyService.savePurchaseOrder(businessKey, user);
		execution.setVariable("formId", purchaseOrderId);
		execution.setVariable("formName", "purchaseOrder");
		execution.setVariable("subSendMan", "wangshuang");

	}

}
