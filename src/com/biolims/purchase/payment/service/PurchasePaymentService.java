/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：PurchasePaymentService
 * 类描述：采购申请Service
 * 创建人：阿川
 * 创建时间：2011-12-07
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.payment.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.purchase.model.PurchasePayment;
import com.biolims.purchase.model.PurchasePaymentInvoice;
import com.biolims.purchase.model.PurchasePaymentItem;
import com.biolims.purchase.model.PurchasePaymentOrder;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
public class PurchasePaymentService {

	@Resource
	private CommonDAO commonDAO;

	@Autowired
	CommonService commonService;

	/**
	 * 检索list,采用map方式传递检索参数
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findPurchasePaymentList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);

		}

		//将map值传入，获取列表

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchasePayment.class, mapForQuery, mapForCondition);
		return controlMap;
	}

	/**
	 * 根据id获得申请单对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PurchasePayment getPurchasePayment(String id) throws Exception {
		return commonDAO.get(PurchasePayment.class, id);
	}

	/**
	 * 检索申请单明细
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchasePaymentId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showPurchasePaymentItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchasePaymentId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchasePaymentId != null && !purchasePaymentId.equals("")) {
			mapForQuery.put("purchasePayment.id", purchasePaymentId);
			mapForCondition.put("purchasePayment.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchasePaymentItem.class, mapForQuery, mapForCondition);

		List<PurchasePaymentItem> list = (List<PurchasePaymentItem>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> showPurchasePaymentOrderList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchasePaymentId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchasePaymentId != null && !purchasePaymentId.equals("")) {
			mapForQuery.put("purchasePayment.id", purchasePaymentId);
			mapForCondition.put("purchasePayment.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchasePaymentOrder.class, mapForQuery, mapForCondition);

		List<PurchasePaymentOrder> list = (List<PurchasePaymentOrder>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> showPurchasePaymentInvoiceList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchasePaymentId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchasePaymentId != null && !purchasePaymentId.equals("")) {
			mapForQuery.put("purchasePayment.id", purchasePaymentId);
			mapForCondition.put("purchasePayment.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchasePaymentInvoice.class, mapForQuery, mapForCondition);

		List<PurchasePaymentInvoice> list = (List<PurchasePaymentInvoice>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 新增或更新采购申请
	 * @param pa
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(PurchasePayment pa) throws Exception {
		commonDAO.saveOrUpdate(pa);
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId
	 *            用户id
	 * @param jsonString
	 *            用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchasePaymentItem(String animalId, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			PurchasePaymentItem em = new PurchasePaymentItem();

			// 将map信息读入实体类
			em = (PurchasePaymentItem) commonDAO.Map2Bean(map, em);
			PurchasePayment ep = new PurchasePayment();
			ep.setId(animalId);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setPurchasePayment(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchasePaymentItem(String id) {
		PurchasePaymentItem ppi = new PurchasePaymentItem();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchasePaymentOrder(String id, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			PurchasePaymentOrder em = new PurchasePaymentOrder();

			// 将map信息读入实体类 
			em = (PurchasePaymentOrder) commonDAO.Map2Bean(map, em);
			PurchasePayment ep = new PurchasePayment();
			ep.setId(id);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setPurchasePayment(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchasePaymentOrder(String id) {
		PurchasePaymentOrder ppi = new PurchasePaymentOrder();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchasePaymentInvoice(String id, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			PurchasePaymentInvoice em = new PurchasePaymentInvoice();

			// 将map信息读入实体类 
			em = (PurchasePaymentInvoice) commonDAO.Map2Bean(map, em);
			PurchasePayment ep = new PurchasePayment();
			ep.setId(id);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setPurchasePayment(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchasePaymentInvoice(String id) {
		PurchasePaymentInvoice ppi = new PurchasePaymentInvoice();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}
}
