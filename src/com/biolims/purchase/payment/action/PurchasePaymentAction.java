/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：PurchaseApplyAction
 * 类描述：采购申请
 * 创建人：阿川
 * 创建时间：2011-12-07
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.payment.action;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.model.PurchasePayment;
import com.biolims.purchase.model.PurchasePaymentInvoice;
import com.biolims.purchase.model.PurchasePaymentItem;
import com.biolims.purchase.model.PurchasePaymentOrder;
import com.biolims.purchase.payment.service.PurchasePaymentService;
import com.biolims.util.SendData;

@Namespace("/purchase/payment")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class PurchasePaymentAction extends BaseActionSupport {

	private static final long serialVersionUID = 6480754183198344386L;

	@Autowired
	private PurchasePaymentService paService;

	@Resource
	private SystemCodeService systemCodeService;

	//用于页面上显示模块名称
	private String title = "付款管理";

	//该action权限id
	private String rightsId = "305";

	private PurchasePayment purchasePayment = new PurchasePayment();

	/**
	 * 申请列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchasePaymentList")
	public String showPaymentList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "付款编号", "150", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "150", "true", "", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "付款类型", "100", "true", "", "", "", "", "", "" });
		map.put("createUser-name", new String[] { "", "string", "", "付款申请人", "100", "true", "", "", "", "", "", "" });
		map.put("createDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "申请日期", "100", "true", "", "", "", "",
				"formatDate", "" });
		map.put("confirmUser-name", new String[] { "", "string", "", "批准人", "100", "true", "", "", "", "", "", "" });
		map.put("stateName", new String[] { "", "string", "", "工作流状态", "100", "true", "", "", "", "", "", "" });
		map.put("invoiceState", new String[] { "", "string", "", "发票状态", "150", "true", "true", "", "", "", "", "" });
		map.put("paymentState", new String[] { "", "string", "", "付款状态", "150", "true", "true", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		//生成toolbar
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/purchase/payment/showPurchasePaymentListJson.action");
		//导向jsp页面显示
		return dispatcher("/WEB-INF/page/purchase/payment/showPurchasePaymentList.jsp");
	}

	@Action(value = "showPurchasePaymentListJson")
	public void showPaymentListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		//取出session中检索用的对象
		//User user = (User)this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = paService.findPurchasePaymentList(startNum, limitNum, dir, sort,
				getContextPath(), data, "", "");
		// Map<String, Object> controlMap = userService.findUser(startNum,
		// limitNum, dir, sort, getContextPath(), "", "", "");
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<PurchasePayment> list = (List<PurchasePayment>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-name", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-name", "");
		map.put("stateName", "");
		map.put("invoiceState", "");
		map.put("paymentState", "");

		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 编辑页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditPurchasePayment")
	public String toEditPurchasePayment() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			purchasePayment = paService.getPurchasePayment(id);
			putObjToContext("purchasePaymentId", id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toState(purchasePayment.getState());

			if (purchasePayment.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			showPaymentItemList(handlemethod, id);
			showPaymentOrderList(handlemethod, id);
			showPaymentInvoiceList(handlemethod, id);
			toToolBar(rightsId, "", "", handlemethod);

		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.purchasePayment.setId(SystemCode.DEFAULT_SYSTEMCODE);
			purchasePayment.setCreateUser(user);
			purchasePayment.setCreateDate(new Date());
			purchasePayment.setState(SystemConstants.DIC_STATE_NEW);
			purchasePayment.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			showPaymentItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
			showPaymentOrderList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
			showPaymentInvoiceList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
			toState(purchasePayment.getState());
		}
		return dispatcher("/WEB-INF/page/purchase/payment/editPurchasePayment.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewPurchasePayment")
	public String toViewPurchasePayment() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			purchasePayment = paService.getPurchasePayment(id);
			putObjToContext("purchasePaymentId", id);
			showPaymentItemList(SystemConstants.PAGE_HANDLE_METHOD_VIEW, id);
			showPaymentOrderList(SystemConstants.PAGE_HANDLE_METHOD_VIEW, id);
			showPaymentInvoiceList(SystemConstants.PAGE_HANDLE_METHOD_VIEW, id);
		}

		return dispatcher("/WEB-INF/page/purchase/payment/editPurchasePayment.jsp");
	}

	public void showPaymentItemList(String handlemethod, String paId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
			editflag = "false";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("payCondition", new String[] { "", "string", "", "付款条件", "200", "true", "false", "", "", editflag, "",
				"payCondition" });
		map.put("scale",
				new String[] { "", "string", "", "付款比例", "100", "true", "false", "", "", editflag, "", "scale" });
		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);

		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/purchase/payment/showPurchasePaymentItemListJson.action?purchasePaymentId=" + paId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		if (paId != null)
			putObjToContext("purchasePaymentId", paId);
		putObjToContext("handlemethod", handlemethod);

	}

	public void showPaymentOrderList(String handlemethod, String paId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
			editflag = "false";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("purchaseOrder-id", new String[] { "", "string", "", "采购订单号", "200", "true", "false", "", "", editflag,
				"", "purchaseOrder" });
		map.put("purchaseOrder-createUser-name", new String[] { "", "string", "", "采购申请人", "100", "true", "false", "",
				"", "", "", "" });
		map.put("purchaseOrder-fee", new String[] { "", "string", "", "采购金额", "100", "true", "false", "", "", "", "",
				"" });
		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type1", exttype);
		putObjToContext("col1", extcol);
		putObjToContext("path1", ServletActionContext.getRequest().getContextPath()
				+ "/purchase/payment/showPaymentOrderListJson.action?purchasePaymentId=" + paId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		if (paId != null)
			putObjToContext("purchasePaymentId", paId);
		putObjToContext("handlemethod", handlemethod);

	}

	@Action(value = "showPaymentOrderListJson")
	public void showPaymentOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchasePaymentId = getParameterFromRequest("purchasePaymentId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<PurchasePaymentOrder> list = null;
		long totalCount = 0;
		if (purchasePaymentId != null && !purchasePaymentId.equals("")) {
			Map<String, Object> controlMap = paService.showPurchasePaymentOrderList(startNum, limitNum, dir, sort,
					getContextPath(), purchasePaymentId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<PurchasePaymentOrder>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("purchaseOrder-id", "");
		map.put("purchaseOrder-createUser-name", "");
		map.put("purchaseOrder-fee", "");

		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	public void showPaymentInvoiceList(String handlemethod, String paId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
			editflag = "false";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("invoiceCode", new String[] { "", "string", "", "发票号", "200", "true", "false", "", "", editflag, "",
				"invoiceCode" });
		map.put("fee", new String[] { "", "float", "", "发票金额", "100", "true", "false", "", "", editflag, "", "fee" });
		map.put("payFee", new String[] { "", "float", "", "付款金额", "100", "true", "false", "", "", editflag, "",
				"payFee" });
		map.put("payDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "付款日期", "100", "true", "false", "", "",
				editflag, "formatDate", "payDate" });

		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);

		putObjToContext("type2", exttype);
		putObjToContext("col2", extcol);
		putObjToContext("path2", ServletActionContext.getRequest().getContextPath()
				+ "/purchase/payment/showPaymentInvoiceListJson.action?purchasePaymentId=" + paId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		if (paId != null)
			putObjToContext("purchasePaymentId", paId);
		putObjToContext("handlemethod", handlemethod);

	}

	@Action(value = "showPaymentInvoiceListJson")
	public void showPaymentInvoiceListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchasePaymentId = getParameterFromRequest("purchasePaymentId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<PurchasePaymentInvoice> list = null;
		long totalCount = 0;
		if (purchasePaymentId != null && !purchasePaymentId.equals("")) {
			Map<String, Object> controlMap = paService.showPurchasePaymentInvoiceList(startNum, limitNum, dir, sort,
					getContextPath(), purchasePaymentId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<PurchasePaymentInvoice>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("invoiceCode", "");
		map.put("fee", "#.##");
		map.put("payFee", "#.##");
		map.put("payDate", "yyyy-MM-dd");

		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	@Action(value = "showPurchasePaymentItemListJson")
	public void showPaymentItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchasePaymentId = getParameterFromRequest("purchasePaymentId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<PurchasePaymentItem> list = null;
		long totalCount = 0;
		if (purchasePaymentId != null && !purchasePaymentId.equals("")) {
			Map<String, Object> controlMap = paService.showPurchasePaymentItemList(startNum, limitNum, dir, sort,
					getContextPath(), purchasePaymentId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<PurchasePaymentItem>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("payCondition", "");
		map.put("scale", "");

		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 新增采购申请。
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {

		String data = getParameterFromRequest("jsonDataStr");
		String data1 = getParameterFromRequest("jsonDataStr1");
		String data2 = getParameterFromRequest("jsonDataStr2");
		String id = this.purchasePayment.getId();
		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			String code = systemCodeService.getSystemCode(SystemCode.PURCHASE_PAYMENT_NAME,
					SystemCode.PURCHASE_PAYMENT_CODE, null, null);
			this.purchasePayment.setId(code);
		}
		paService.save(purchasePayment);
		if (data != null && !data.equals(""))
			savePurchasePaymentItem(purchasePayment.getId(), data);
		if (data1 != null && !data1.equals(""))
			paService.savePurchasePaymentOrder(purchasePayment.getId(), data1);
		if (data2 != null && !data2.equals(""))
			paService.savePurchasePaymentInvoice(purchasePayment.getId(), data2);
		return redirect("/purchase/payment/toEditPurchasePayment.action?id=" + purchasePayment.getId());
	}

	public void savePurchasePaymentItem(String purchasePaymentId, String json) throws Exception {

		paService.savePurchasePaymentItem(purchasePaymentId, json);

	}

	/**
	 * 保存明细
	 */
	@Action(value = "savePurchasePaymentItem")
	public void savePurchasePaymentItem() throws Exception {
		String purchasePaymentId = getParameterFromRequest("purchasePaymentId");
		String json = getParameterFromRequest("data");
		paService.savePurchasePaymentItem(purchasePaymentId, json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delPurchasePaymentItem")
	public void delPurchasePaymentItem() throws Exception {
		String id = getParameterFromRequest("id");
		paService.delPurchasePaymentItem(id);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchasePayment getPurchasePayment() {
		return purchasePayment;
	}

	public void setPurchasePayment(PurchasePayment purchasePayment) {
		this.purchasePayment = purchasePayment;
	}

}
