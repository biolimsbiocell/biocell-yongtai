/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：PurchaseCancelAction
 * 类描述：退货申请
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.cancel.action;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.purchase.cancel.service.PurchaseCancelService;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.model.PurchaseCancel;
import com.biolims.purchase.model.PurchaseCancelItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.storage.storageIn.service.StorageInService;
import com.biolims.util.SendData;

@Namespace("/purchase/cancel")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class PurchaseCancelAction extends BaseActionSupport {

	private static final long serialVersionUID = 6480754183198344386L;
	@Autowired
	private StorageInService siService;
	@Autowired
	private PurchaseCancelService paService;

	//用于页面上显示模块名称
	private String title = "退货管理";

	//该action权限id
	private String rightsId = "304";

	private PurchaseCancel purchaseCancel = new PurchaseCancel();

	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 申请列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseCancelList")
	public String showCancelList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "退货编号", "150", "true", "", "", "", "", "", "" });
		map.put("reason", new String[] { "", "string", "", "退货原因", "150", "true", "", "", "", "", "", "" });
		map.put("purchaseOrder-id", new String[] { "", "string", "", "采购订单", "100", "true", "", "", "", "", "", "" });
		map.put("createUser-name", new String[] { "", "string", "", "申请人", "100", "true", "", "", "", "", "", "" });

		map.put("canelDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "退货日期", "100", "true", "", "", "", "",
				"formatDate", "" });
		map.put("stateName", new String[] { "", "string", "", "工作流状态", "100", "true", "", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		//生成toolbar
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/purchase/cancel/showPurchaseCancelListJson.action");
		//导向jsp页面显示
		return dispatcher("/WEB-INF/page/purchase/cancel/showPurchaseCancelList.jsp");
	}

	@Action(value = "showPurchaseCancelListJson")
	public void showCancelListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		//取出session中检索用的对象
		//User user = (User)this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = paService.findPurchaseCancelList(startNum, limitNum, dir, sort,
				getContextPath(), data, "", "");
		// Map<String, Object> controlMap = userService.findUser(startNum,
		// limitNum, dir, sort, getContextPath(), "", "", "");
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<PurchaseCancel> list = (List<PurchaseCancel>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("reason", "");
		map.put("purchaseOrder-id", "");
		map.put("createUser-name", "");

		map.put("canelDate", "yyyy-MM-dd");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 编辑页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditPurchaseCancel")
	public String toEditPurchaseCancel() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			purchaseCancel = paService.getPurchaseCancel(id);
			putObjToContext("purchaseCancelId", id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toState(purchaseCancel.getState());

			if (purchaseCancel.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			showCancelItemList(handlemethod, id);
			toToolBar(rightsId, "", "", handlemethod);

		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.purchaseCancel.setId(SystemCode.DEFAULT_SYSTEMCODE);
			purchaseCancel.setCreateUser(user);
			purchaseCancel.setCreateDate(new Date());
			purchaseCancel.setCanelDate(new Date());
			purchaseCancel.setState(SystemConstants.DIC_STATE_NEW);
			purchaseCancel.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			showCancelItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
			toState(purchaseCancel.getState());
		}
		return dispatcher("/WEB-INF/page/purchase/cancel/editPurchaseCancel.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewPurchaseCancel")
	public String toViewPurchaseCancel() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			purchaseCancel = paService.getPurchaseCancel(id);
			putObjToContext("purchaseCancelId", id);
		}
		return dispatcher("/WEB-INF/page/purchase/cancel/editPurchaseCancel.jsp");
	}

	public void showCancelItemList(String handlemethod, String paId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "false";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (!handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
			editflag = "true";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "申请对象", "120", "true", "false", "", "", "", "", "" });
		map.put("storage-name", new String[] { "", "string", "", "名称", "120", "true", "false", "", "", "", "", "" });
		//map.put("storage-searchCode", new String[] { "", "string", "", "检索码", "100", "true", "false", "", "", "", "",
		//		"" });
		map.put("num", new String[] { "", "float", "", "退货数量", "100", "true", "false", "", "", editflag, "", "num" });
		map.put("serial", new String[] { "", "string", "", "批次ID", "100", "true", "false", "", "", editflag, "",
				"serial" });
		map.put("price", new String[] { "", "float", "", "价格", "100", "true", "true", "", "", "", "", "" });
		map.put("storage-unit-name",
				new String[] { "", "string", "", "单位", "100", "true", "false", "", "", "", "", "" });

		//	map.put("storage-supplierName", new String[] { "", "string", "", "生产厂商", "100", "true", "false", "", "", "",
		//			"", "" });

		//	map.put("costCenter-id",
		//			new String[] { "", "string", "", " 成本中心ID", "100", "true", "true", "", "", "", "", "" });
		//		map.put("costCenter-name", new String[] { "", "string", "", "成本中心名称", "100", "true", "false", "", "", "", "",
		//				"" });

		map.put("storageInItem-num",
				new String[] { "", "float", "", "已入库数量", "100", "true", "true", "", "", "", "", "" });
		map.put("storageInItem-id", new String[] { "", "string", "", "入库明细ID", "100", "true", "true", "", "", "", "",
				"" });
		map.put("storageNum", new String[] { "", "float", "", "库明细数量", "100", "true", "true", "", "", "", "", "" });
		map.put("storage-num", new String[] { "", "float", "", "库存数量", "100", "true", "true", "", "", "", "", "" });
		map.put("storage-type-id",
				new String[] { "", "string", "", "库存类型ID", "100", "true", "true", "", "", "", "", "" });
		//声明
		String statement = "";
		//声明一个lisener
		String statementLisener = "";
		//生成ext用type字符串
		exttype = generalexttype(map);
		//生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/purchase/cancel/showPurchaseCancelItemListJson.action?purchaseCancelId=" + paId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		if (paId != null)
			putObjToContext("purchaseCancelId", paId);
		putObjToContext("handlemethod", handlemethod);
	}

	@Action(value = "showPurchaseCancelItemListJson")
	public void showCancelItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchaseCancelId = getParameterFromRequest("purchaseCancelId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<PurchaseCancelItem> list = null;
		long totalCount = 0;
		if (purchaseCancelId != null && !purchaseCancelId.equals("")) {
			Map<String, Object> controlMap = paService.showPurchaseCancelItemList(startNum, limitNum, dir, sort,
					getContextPath(), purchaseCancelId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<PurchaseCancelItem>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("storage-searchCode", "");
		map.put("price", "");
		map.put("num", "");
		map.put("serial", "");
		map.put("storageInItem-num", "");
		map.put("storage-unit-name", "");

		map.put("storageInItem-id", "");

		map.put("storage-supplierName", "");
		map.put("costCenter-id", "");
		map.put("costCenter-name", "");
		map.put("storageNum", "");
		map.put("storage-num", "#.##");
		map.put("storage-type-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	@Action(value = "showPurchaseOrderItemListJson")
	public void showApplyItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchaseOrderId = getParameterFromRequest("purchaseOrderId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<PurchaseOrderItem> list = null;
		long totalCount = 0;
		if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
			Map<String, Object> controlMap = paService.showPurchaseOrderItemList(startNum, limitNum, dir, sort,
					getContextPath(), purchaseOrderId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<PurchaseOrderItem>) controlMap.get("list");
		}
		StringBuffer str = new StringBuffer();

		for (int i = 0; i < list.size(); i++) {
			PurchaseOrderItem poi = list.get(i);
			str.append("{");

			str.append("'storage-id':'").append(poi.getStorage().getId()).append("',");
			str.append("'storage-name':'").append(poi.getStorage().getName() == null ? "" : poi.getStorage().getName())
					.append("',");
			str.append("'storage-unit-name':'").append(
					poi.getStorage().getUnit() == null ? "" : poi.getStorage().getUnit().getName()).append("',");
			str.append("'storage-searchCode':'").append(
					poi.getStorage().getSearchCode() == null ? "" : poi.getStorage().getSearchCode()).append("',");
			str.append("'storage-spec':'").append(poi.getStorage().getSpec() == null ? "" : poi.getStorage().getSpec())
					.append("',");
			str.append("'storage-breed':'").append(
					poi.getStorage().getBreed() == null ? "" : poi.getStorage().getBreed()).append("',");

			str.append("'price':'").append(poi.getPrice() == null ? "" : poi.getPrice()).append("',");
			str.append("'purchaseOrderNum':'").append(poi.getNum() == null ? "" : poi.getNum()).append("',");
			str.append("'inNum':'").append(
					siService.getStorageInSum(poi.getPurchaseOrder().getId(), poi.getStorage().getId())).append("',");
			str.append("'cancelNum':'").append(
					paService.getPurchaseCancelSum(poi.getPurchaseOrder().getId(), poi.getStorage().getId())).append(
					"'");

			str.append("}");
		}

		StringBuffer sb = new StringBuffer("{'total':").append(totalCount).append(",'results':[").append(str).append(
				"]}");
		String a = sb.toString().replace("}{", "},{");

		new SendData().sendDataJson(a, ServletActionContext.getResponse());

	}

	/**
	 * 新增采购申请。
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {

		String data = getParameterFromRequest("jsonDataStr");
		String id = this.purchaseCancel.getId();
		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			String code = systemCodeService.getSystemCode(SystemCode.PURCHASE_CANCEL_NAME,
					SystemCode.PURCHASE_CANCEL_CODE, null, null);
			this.purchaseCancel.setId(code);
		}
		paService.save(purchaseCancel);
		if (data != null && !data.equals(""))
			savePurchaseCancelItem(purchaseCancel.getId(), data);

		//具体操作，如删除，填加动作，应用redirect
		return redirect("/purchase/cancel/toEditPurchaseCancel.action?id=" + purchaseCancel.getId());
	}

	public void savePurchaseCancelItem(String purchaseCancelId, String json) throws Exception {

		paService.savePurchaseCancelItem(purchaseCancelId, json);

	}

	/**
	 * 保存明细
	 */
	@Action(value = "savePurchaseCancelItem")
	public void saveAnimalMed() throws Exception {
		String purchaseCancelId = getParameterFromRequest("purchaseCancelId");
		String json = getParameterFromRequest("data");
		paService.savePurchaseCancelItem(purchaseCancelId, json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delPurchaseCancelItem")
	public void delPurchaseCancelItem() throws Exception {
		String id = getParameterFromRequest("id");
		paService.delPurchaseCancelItem(id);
		//具体操作，如删除，填加动作，应用redirect
		//return redirect("/sysmanage/user/userVaccineShow.action");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchaseCancel getPurchaseCancel() {
		return purchaseCancel;
	}

	public void setPurchaseCancel(PurchaseCancel purchaseCancel) {
		this.purchaseCancel = purchaseCancel;
	}

}
