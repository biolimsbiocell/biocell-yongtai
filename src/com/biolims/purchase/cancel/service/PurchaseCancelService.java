/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：退货申请Service
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.cancel.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.purchase.cancel.dao.PurchaseCancelDao;
import com.biolims.purchase.model.PurchaseCancel;
import com.biolims.purchase.model.PurchaseCancelItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.common.service.StorageCommonService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
public class PurchaseCancelService extends ApplicationTypeService {

	@Resource
	private CommonDAO commonDAO;

	@Resource
	private PurchaseCancelDao purchaseCancelDAO;
	@Resource
	private StorageCommonService storageCommonService;

	/**
	 * 检索list,采用map方式传递检索参数
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findPurchaseCancelList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);

		}

		//设置组织机构管理范围Map
		Map<String, String> mapForManageScope = new HashMap<String, String>();

		//生成管理范围map
		mapForManageScope = BeanUtils.configueDepartmentMap("department", "createUser.id", rightsId, curUserId);

		mapForQuery.put("mapForManageScope", mapForManageScope);
		//将map值传入，获取列表

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchaseCancel.class, mapForQuery, mapForCondition);
		return controlMap;
	}

	/**
	 * 根据id获得申请单对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PurchaseCancel getPurchaseCancel(String id) throws Exception {
		return commonDAO.get(PurchaseCancel.class, id);
	}

	public Map<String, Object> showPurchaseOrderItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchaseOrderId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
			mapForQuery.put("purchaseOrder.id", purchaseOrderId);
			mapForCondition.put("purchaseOrder.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchaseOrderItem.class, mapForQuery, mapForCondition);

		return controlMap;
	}

	/**
	 * 检索申请单明细
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchaseCancelId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showPurchaseCancelItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchaseCancelId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchaseCancelId != null && !purchaseCancelId.equals("")) {
			mapForQuery.put("purchaseCancel.id", purchaseCancelId);
			mapForCondition.put("purchaseCancel.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchaseCancelItem.class, mapForQuery, mapForCondition);

		List<PurchaseCancelItem> list = (List<PurchaseCancelItem>) controlMap.get("list");

		//for (PurchaseCancelItem pci : list) {

		//Double n = selectStorageReagentBuyCount(pci.getStorage().getId(), pci.getStorageInItem().getId());
		//pci.setStorageNum(n);
		//}

		controlMap.put("list", list);
		return controlMap;
	}

	public Double selectStorageReagentBuyCount(String storageId, String inItemId) throws Exception {
		return purchaseCancelDAO.selectStorageReagentBuyCount(storageId, inItemId);
	}

	/**
	 * 新增或更新采购申请
	 * @param pa
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(PurchaseCancel pa) throws Exception {
		commonDAO.saveOrUpdate(pa);
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId
	 *            用户id
	 * @param jsonString
	 *            用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseCancelItem(String animalId, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			PurchaseCancelItem em = new PurchaseCancelItem();

			// 将map信息读入实体类
			em = (PurchaseCancelItem) commonDAO.Map2Bean(map, em);
			PurchaseCancel ep = new PurchaseCancel();
			ep.setId(animalId);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setPurchaseCancel(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseCancelItem(String id) {
		PurchaseCancelItem ppi = new PurchaseCancelItem();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}

	public List<PurchaseCancelItem> findPurchaseCancelItemById(String id) throws Exception {
		List<PurchaseCancelItem> list = this.purchaseCancelDAO.findPurchaseCancelItemById(id);
		return list;
	}

	public Double getPurchaseCancelSum(String orderId, String storageId) throws Exception {
		return this.purchaseCancelDAO.getPurchaseCancelSum(orderId, storageId);
	}

	/**
	 * 退货动作
	 * @param formId
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public String purchaseCancelSetStorage(String applicationTypeActionId, String formId) throws Exception {
		String message = "";
		//int addNum = 0;
		if (storageCommonService.ifInStorageCheckProcess() == true) {
			message = "正在盘点期，不能操作！";
			return message;
		}

		List<PurchaseCancelItem> list = findPurchaseCancelItemById(formId);
		for (PurchaseCancelItem em : list) {

			Storage s = commonDAO.get(em.getStorage().getClass(), em.getStorage().getId());
			//Double sn = 0.0;
			Double nn = 0.0;
			Double oldNum = 0.0;

			oldNum = s.getNum();
			if (em.getNum() != null) {

				//库存数量=原数量-退库数量
				if (s.getNum() != null) {
					nn = s.getNum();
					nn -= em.getNum();
					s.setNum(nn);
				}

				Double ydPrice = 0.0;
				//使用移动加权平均法计算出库价格（出库价格=（原出库价格*原库存数量-退货物资的入库价格*退货数量）/新在库数量）
				if (s.getOutPrice() != null && s.getOutPrice() != 0) {
					if (nn == 0)
						ydPrice = 0.0;
					else
						ydPrice = (s.getOutPrice() * oldNum - em.getPrice() * em.getNum()) / nn;
					if ((s.getOutPrice() * oldNum - em.getPrice() * em.getNum()) < 0)
						ydPrice = 0.0;
					s.setOutPrice(ydPrice);
					commonDAO.saveOrUpdate(s);
					Double rn = 0.0;
					Double srn = 0.0;
					if (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {

						StorageReagentBuySerial srb = new StorageReagentBuySerial();
						if (em.getStorageInItem() != null)
							srb = commonDAO.findByProperty(StorageReagentBuySerial.class, "storageInItem",
									em.getStorageInItem()).get(0);
						else {

							if (em.getSerial() != null)
								srb = commonDAO.get(StorageReagentBuySerial.class, em.getSerial());
						}
						if (em.getNum() != null && srb.getNum() != null) {
							rn = em.getNum();
							srn = srb.getNum();
							srb.setNum(srn - rn);
						}
						//库存数量=原数量-退货数量
						commonDAO.saveOrUpdate(srb);
					}
				}
			}

		}
		//setObjectState(StorageIn.class, formId, SystemConstants.DIC_STATE_YES);
		//setObjectStateName(applicationTypeActionId, formId, "");
		return "";
	}

}
