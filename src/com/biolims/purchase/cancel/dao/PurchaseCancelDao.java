/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：DAO
 * 类描述：退库
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.cancel.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.purchase.model.PurchaseCancelItem;
import com.biolims.storage.common.constants.SystemConstants;

@Repository
@SuppressWarnings("unchecked")
public class PurchaseCancelDao extends BaseHibernateDao {

	public List<PurchaseCancelItem> findPurchaseCancelItemById(String id) throws Exception {
		String hql = "from PurchaseCancelItem where purchaseCancel.id='" + id + "'";
		List<PurchaseCancelItem> list = this.getSession().createQuery(hql).list();

		return list;
	}

	public Double selectStorageReagentBuyCount(String storageId, String storageInItemId) throws Exception {

		String hql = "select sum(num) from StorageReagentBuySerial where storage.id='" + storageId
				+ "' and storageInItem.id ='" + storageInItemId + "'";
		Double srb = (Double) this.getSession().createQuery(hql).uniqueResult();
		return srb;
	}

	public Double getPurchaseCancelSum(String orderId, String storageId) throws Exception {
		String hql = "from PurchaseCancelItem where purchaseCancel.purchaseOrder.id = '" + orderId
				+ "' and purchaseCancel.state = '" + SystemConstants.DIC_STATE_YES_ID + "' and storage.id='"
				+ storageId + "'";
		hql = "select sum(num) " + hql;
		Double sum = (Double) this.getSession().createQuery(hql).uniqueResult();
		return sum;
	}
}
