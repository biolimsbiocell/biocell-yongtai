package com.biolims.purchase.common.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.common.dao.SystemCodeDao;
import com.biolims.dic.model.DicType;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;

@Service
public class PurchaseSystemCodeService extends com.biolims.common.service.SystemCodeService {
	@Resource
	private SystemCodeDao systemCodeDao;

	/**
	 * 生成采购申请单号
	 * @param typeId 分类 id
	 * @return
	 * @throws Exception
	 */
	public String getPurchaseApplyCode(String typeId) throws Exception {
		DicType type = this.systemCodeDao.findOneByProperty(DicType.class, "id", typeId);
		String sysCode = null;
		long code = 0l;
		String whereSql = "1=1";
		if (type != null) {
			sysCode = type.getSysCode();
			if (sysCode != null && sysCode.length() > 0) {
				whereSql = "type.sysCode ='" + sysCode + "'";
			}
			if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_STORAGE_SYSCODE.equals(type.getSysCode())) {
				code = SystemCode.PURCHASE_APPLY_STORAGE_CODE;
			} else if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_EQUIPMENT_SYSCODE.equals(type.getSysCode())) {
				code = SystemCode.PURCHASE_APPLY_EQUIPMENT_CODE;
			} else if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_SERVICE_SYSCODE.equals(type.getSysCode())) {
				code = SystemCode.PURCHASE_APPLY_SERVICE_CODE;
			}
		}
		return this.getSystemCode(SystemCode.PURCHASE_APPLY_NAME, code, null, null, whereSql, 0);
	}

	/**
	 * 生成采购订单单号
	 * @param typeId 分类 id
	 * @return
	 * @throws Exception
	 */
	public String getPurchaseOrderCode(String typeId) throws Exception {
		DicType type = this.systemCodeDao.findOneByProperty(DicType.class, "id", typeId);
		String sysCode = null;
		long code = 0l;
		String whereSql = "1=1";

		code = SystemCode.PURCHASE_ORDER_STORAGE_CODE;

		return this.getSystemCode(SystemCode.PURCHASE_ORDER_NAME, code, null, null, whereSql, 0);
	}

}
