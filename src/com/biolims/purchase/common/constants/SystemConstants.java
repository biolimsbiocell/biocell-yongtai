package com.biolims.purchase.common.constants;

public class SystemConstants extends com.biolims.common.constants.SystemConstants {
	/**
	 * 采购申请-字典类型-设备
	 */
	public static final String PURCHASE_APPLY_DIC_TYPE_EQUIPMENT_SYSCODE = "equipment";

	/**
	 * 采购申请-字典类型-服务
	 */
	public static final String PURCHASE_APPLY_DIC_TYPE_SERVICE_SYSCODE = "service";
	/**
	 * 采购申请-字典类型-物资
	 */
	public static final String PURCHASE_APPLY_DIC_TYPE_STORAGE_SYSCODE = "storage";

	/**
	 * 采购申请-字典类型-自动创建物资ID
	 */
	public static final String PURCHASE_APPLY_DIC_TYPE_STORAGE_AUTO_ID = "19";
}
