package com.biolims.purchase.common.constants;

//系统业务数据的编码
public class SystemCode extends com.biolims.common.constants.SystemCode {

	/**
	 * 采购-采购申请
	 */
	public static final String PURCHASE_APPLY_NAME = "PurchaseApply";

	/**
	 * 采购-采购库存申请600000-699999
	 */
	public static final long PURCHASE_APPLY_STORAGE_CODE = 600000;

	/**
	 * 采购-采购设备申请700000-799999
	 */
	public static final long PURCHASE_APPLY_EQUIPMENT_CODE = 700000;

	/**
	 * 采购-采购服务申请800000-899999
	 */
	public static final long PURCHASE_APPLY_SERVICE_CODE = 800000;

	/**
	 * 采购-采购订单管理700000-799999
	 */
	public static final long PURCHASE_ORDER_CODE = 700000;

	/**
	 * 采购-采购定单
	 */
	public static final String PURCHASE_ORDER_NAME = "PurchaseOrder";

	/**
	 * 采购-采购库存定单
	 */
	public static final long PURCHASE_ORDER_STORAGE_CODE = 600000;

	/**
	 * 采购-采购设备定单
	 */
	public static final long PURCHASE_ORDER_EQUIPMENT_CODE = 400000;

	/**
	 * 采购-采购服务定单
	 */
	public static final long PURCHASE_ORDER_SERVICE_CODE = 800000;
	/**
	 * 采购-采购入库管理900000-999999
	 */
	public static final long PURCHASE_STORAGEIN_CODE = 900000;
	/**
	 * 采购-采购入库
	 */
	public static final String PURCHASE_STORAGEIN_NAME = "StorageIn";
	/**
	 * 采购-采购退货管理3000-39999
	 */
	public static final long PURCHASE_CANCEL_CODE = 3000;
	/**
	 * 采购-采购退货
	 */
	public static final String PURCHASE_CANCEL_NAME = "PurchaseCancel";

	/**
	 * 采购-采购付款管理800000-899999
	 */
	public static final long PURCHASE_PAYMENT_CODE = 800000;
	/**
	 * 采购-采购付款
	 */
	public static final String PURCHASE_PAYMENT_NAME = "PurchasePayment";

}
