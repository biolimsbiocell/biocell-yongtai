package com.biolims.purchase.inquiry.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.purchase.model.PurchaseInquiry;
import com.biolims.purchase.model.PurchaseInquiryEquipmentItem;
import com.biolims.purchase.model.PurchaseInquiryItem;
import com.biolims.purchase.model.PurchaseInquiryServiceItem;
import com.biolims.purchase.model.PurchaseInquirySupplier;

@Repository
@SuppressWarnings("unchecked")
public class PurchaseInquiryDao extends BaseHibernateDao {

	public Map<String, Object> selectPurchaseInquiryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseInquiry where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseInquiry> list = new ArrayList<PurchaseInquiry>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectPurchaseInquirySupplierList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseInquirySupplier where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseInquirySupplier> list = new ArrayList<PurchaseInquirySupplier>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectPurchaseInquiryItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseInquiryItem where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseInquiryItem> list = new ArrayList<PurchaseInquiryItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectPurchaseInquiryEquipmentItemList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseInquiryEquipmentItem where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseInquiryEquipmentItem> list = new ArrayList<PurchaseInquiryEquipmentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectPurchaseInquiryServiceItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseInquiryServiceItem where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseInquiryServiceItem> list = new ArrayList<PurchaseInquiryServiceItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public void delPurchaseInquirySupplierListById(Map<String, String> mapForQuery) {
		String hql = "delete from PurchaseInquirySupplier where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public void delPurchaseInquiryItemListById(Map<String, String> mapForQuery) {
		String hql = "delete from PurchaseInquiryItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public void delPurchaseInquiryServiceItemListById(Map<String, String> mapForQuery) {
		String hql = "delete from PurchaseInquiryServiceItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public void delPurchaseInquiryEquipmentItemListById(Map<String, String> mapForQuery) {
		String hql = "delete from PurchaseInquiryEquipmentItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

}
