package com.biolims.purchase.inquiry.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.purchase.apply.service.PurchaseApplyService;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.inquiry.dao.PurchaseInquiryDao;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.purchase.model.PurchaseInquiry;
import com.biolims.purchase.model.PurchaseInquiryEquipmentItem;
import com.biolims.purchase.model.PurchaseInquiryItem;
import com.biolims.purchase.model.PurchaseInquiryServiceItem;
import com.biolims.purchase.model.PurchaseInquirySupplier;
import com.biolims.util.JsonUtils;

@Service
public class PurchaseInquiryService {

	@Autowired
	private PurchaseInquiryDao purchaseInquiryDao;

	@Resource
	private PurchaseApplyService purchaseApplyService;

	/**
	 * 单查-询价基础
	 * @param id	询价基础ID
	 * @return
	 * @throws Exception
	 */
	public PurchaseInquiry getPurchaseInquiryById(String id) throws Exception {
		return purchaseInquiryDao.get(PurchaseInquiry.class, id);
	}

	/**
	 * 询价基础列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseInquiryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseInquiryDao.selectPurchaseInquiryList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 询价客户明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseInquirySupplierList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseInquiryDao.selectPurchaseInquirySupplierList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 库存询价明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseInquiryItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseInquiryDao.selectPurchaseInquiryItemList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 服务询价明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseInquiryServiceItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseInquiryDao.selectPurchaseInquiryServiceItemList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 设备询价明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseInquiryEquipmentItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseInquiryDao.selectPurchaseInquiryEquipmentItemList(mapForQuery, startNum, limitNum, dir,
				sort);
	}

	/**
	 * 大保存
	 * @param purchaseInquiry
	 * @param supplierDataJson
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PurchaseInquiry purchaseInquiry, String supplierDataJson, String itemDataJson,
			String equipmentItemDataJson, String serviceItemDataJson) throws Exception {
		this.savePurchaseInquiry(purchaseInquiry);
		this.savePurchaseInquirySupplierList(purchaseInquiry, supplierDataJson);
		PurchaseApply purchaseApply = this.purchaseApplyService.getPurchaseApply(purchaseInquiry.getPurchaseApply()
				.getId());
		if (purchaseApply != null && purchaseApply.getType() != null) {
			String sysCode = purchaseApply.getType().getSysCode();
			if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_STORAGE_SYSCODE.equals(sysCode)) {
				this.savePurchaseInquiryItemList(purchaseInquiry, itemDataJson);
			} else if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_EQUIPMENT_SYSCODE.equals(sysCode)) {
				this.savePurchaseInquiryEquipmentItemList(purchaseInquiry, equipmentItemDataJson);
			} else if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_SERVICE_SYSCODE.equals(sysCode)) {
				this.savePurchaseInquiryServiceItemList(purchaseInquiry, serviceItemDataJson);
			}
		}
	}

	/**
	 * 保存询价基础
	 * @param purchaseInquiry
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseInquiry(PurchaseInquiry purchaseInquiry) throws Exception {
		this.purchaseInquiryDao.saveOrUpdate(purchaseInquiry);
	}

	/**
	 * 保存询价客户明细
	 * @param purchaseInquiry
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseInquirySupplierList(PurchaseInquiry purchaseInquiry, String itemDataJson) throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			List<PurchaseInquirySupplier> pisList = new ArrayList<PurchaseInquirySupplier>();
			for (Map<String, Object> map : list) {
				PurchaseInquirySupplier pis = new PurchaseInquirySupplier();
				pis = (PurchaseInquirySupplier) this.purchaseInquiryDao.Map2Bean(map, pis);
				pis.setPurchaseInquiry(purchaseInquiry);
				pisList.add(pis);
			}
			this.purchaseInquiryDao.saveOrUpdateAll(pisList);
		}
	}

	/**
	 * 库存保存询价明细
	 * @param purchaseInquiry
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseInquiryItemList(PurchaseInquiry purchaseInquiry, String itemDataJson) throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			List<PurchaseInquiryItem> piiList = new ArrayList<PurchaseInquiryItem>();
			for (Map<String, Object> map : list) {
				PurchaseInquiryItem pii = new PurchaseInquiryItem();
				pii = (PurchaseInquiryItem) this.purchaseInquiryDao.Map2Bean(map, pii);
				pii.setPurchaseInquiry(purchaseInquiry);
				piiList.add(pii);
			}
			this.purchaseInquiryDao.saveOrUpdateAll(piiList);
		}
	}

	/**
	 * 设备保存询价明细
	 * @param purchaseInquiry
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseInquiryEquipmentItemList(PurchaseInquiry purchaseInquiry, String itemDataJson)
			throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			List<PurchaseInquiryEquipmentItem> pieiList = new ArrayList<PurchaseInquiryEquipmentItem>();
			for (Map<String, Object> map : list) {
				PurchaseInquiryEquipmentItem piei = new PurchaseInquiryEquipmentItem();
				piei = (PurchaseInquiryEquipmentItem) this.purchaseInquiryDao.Map2Bean(map, piei);
				piei.setPurchaseInquiry(purchaseInquiry);
				pieiList.add(piei);
			}
			this.purchaseInquiryDao.saveOrUpdateAll(pieiList);
		}
	}

	/**
	 * 服务保存询价明细
	 * @param purchaseInquiry
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseInquiryServiceItemList(PurchaseInquiry purchaseInquiry, String itemDataJson)
			throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			List<PurchaseInquiryServiceItem> pisiList = new ArrayList<PurchaseInquiryServiceItem>();
			for (Map<String, Object> map : list) {
				PurchaseInquiryServiceItem pisi = new PurchaseInquiryServiceItem();
				pisi = (PurchaseInquiryServiceItem) this.purchaseInquiryDao.Map2Bean(map, pisi);
				pisi.setPurchaseInquiry(purchaseInquiry);
				pisiList.add(pisi);
			}
			this.purchaseInquiryDao.saveOrUpdateAll(pisiList);
		}
	}

	/**
	 * 删除询价客户明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseInquirySupplierListById(String ids) throws Exception {
		if (ids != null && ids.length() > 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "in##@@##(" + ids + ")");
			this.purchaseInquiryDao.delPurchaseInquirySupplierListById(map);
		}
	}

	/**
	 * 删除库存询价明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseInquiryItemListById(String ids) throws Exception {
		if (ids != null && ids.length() > 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "in##@@##(" + ids + ")");
			this.purchaseInquiryDao.delPurchaseInquiryItemListById(map);
		}
	}

	/**
	 * 删除设备询价明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseInquiryEquipmentItemListById(String ids) throws Exception {
		if (ids != null && ids.length() > 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "in##@@##(" + ids + ")");
			this.purchaseInquiryDao.delPurchaseInquiryEquipmentItemListById(map);
		}
	}

	/**
	 * 删除服务询价明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseInquiryServiceItemListById(String ids) throws Exception {
		if (ids != null && ids.length() > 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "in##@@##(" + ids + ")");
			this.purchaseInquiryDao.delPurchaseInquiryServiceItemListById(map);
		}
	}

	/**
	 * 根据申请单创建询价服务明细
	 * @param purchaseApplyId
	 * @return
	 * @throws Exception
	 */
	public List<Object> createPurchaseInquiryServiceItem(String purchaseApplyId) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("purchaseApply.id", purchaseApplyId);
		return this.purchaseApplyService.findPurchaseServiceApplyItem(mapForQuery);
	}

	/**
	 * 根据申请单创建询价设备明细
	 * @param purchaseApplyId
	 * @return
	 * @throws Exception
	 */
	public List<Object> createPurchaseInquiryEquipmentItem(String purchaseApplyId) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("purchaseApply.id", purchaseApplyId);
		return this.purchaseApplyService.findPurchaseEquipmentApplyItem(mapForQuery);
	}

	/**
	 * 根据申请单创建询库存器明细
	 * @param purchaseApplyId
	 * @return
	 * @throws Exception
	 */
	public List<Object> createPurchaseInquiryItem(String purchaseApplyId) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("purchaseApply.id", purchaseApplyId);
		return this.purchaseApplyService.findPurchaseApplyItem(mapForQuery);
	}

}
