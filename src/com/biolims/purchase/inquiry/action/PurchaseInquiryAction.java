package com.biolims.purchase.inquiry.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.inquiry.service.PurchaseInquiryService;
import com.biolims.purchase.model.PurchaseInquiry;
import com.biolims.purchase.model.PurchaseInquiryEquipmentItem;
import com.biolims.purchase.model.PurchaseInquiryItem;
import com.biolims.purchase.model.PurchaseInquiryServiceItem;
import com.biolims.purchase.model.PurchaseInquirySupplier;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/purchase/inquiry")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class PurchaseInquiryAction extends BaseActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6091088388453423550L;

	@Autowired
	private PurchaseInquiryService piService;

	//该action权限id
	private String rightsId = "306";

	private PurchaseInquiry purchaseInquiry;

	/**
	 * 询价列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseInquiryList")
	public String showPurchaseInquiryList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/purchase/inquiry/showPurchaseInquiryList.jsp");
	}

	@Action(value = "showPurchaseInquiryListJosn")
	public void showPurchaseInquiryListJosn() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		Map<String, Object> result = this.piService.findPurchaseInquiryList(null, startNum, limitNum, dir, sort);
		long totalCount = Long.parseLong(result.get("total").toString());
		List<PurchaseInquiry> list = (List<PurchaseInquiry>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("remark", "");
		map.put("type-name", "");
		map.put("applyUser-name", "");
		map.put("applyDate", "yyyy-MM-dd");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 编辑页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditPurchaseInquiry")
	public String toEditPurchaseInquiry() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		if (id != null && !id.equals("")) {
			purchaseInquiry = piService.getPurchaseInquiryById(id);
			putObjToContext("taskId", id);
			if (SystemConstants.DIC_STATE_YES.equals(purchaseInquiry.getState())
					|| SystemConstants.PAGE_HANDLE_METHOD_VIEW.equals(handlemethod)) {
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			} else {
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			}

		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			purchaseInquiry = new PurchaseInquiry();
			//this.purchaseInquiry.setId(SystemCode.DEFAULT_SYSTEMCODE);
			purchaseInquiry.setApplyUser(user);
			purchaseInquiry.setApplyDate(new Date());
			purchaseInquiry.setState(SystemConstants.DIC_STATE_NEW);
			purchaseInquiry.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		}
		toState(purchaseInquiry.getState());
		toToolBar(rightsId, "", "", handlemethod);
		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/purchase/inquiry/editPurchaseInquiry.jsp");
	}

	/**
	 * 询价客户列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseInquirySupplierList")
	public String showPurchaseInquirySupplierList() throws Exception {
		return dispatcher("/WEB-INF/page/purchase/inquiry/showPurchaseInquirySupplierList.jsp");
	}

	@Action(value = "showPurchaseInquirySupplierListJosn")
	public void showPurchaseInquirySupplierListJosn() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		String id = getRequest().getParameter("id");

		List<PurchaseInquirySupplier> list = new ArrayList<PurchaseInquirySupplier>();
		Long totalCount = 0l;
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (id != null && id.length() > 0) {
			mapForQuery.put("purchaseInquiry.id", id);
			Map<String, Object> result = this.piService.findPurchaseInquirySupplierList(mapForQuery, startNum,
					limitNum, dir, sort);
			totalCount = Long.parseLong(result.get("total").toString());
			list = (List<PurchaseInquirySupplier>) result.get("list");
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("supplier-id", "supplier-id");
		map.put("supplier-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 库存询价明细列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseInquiryItemList")
	public String showPurchaseInquiryItemList() throws Exception {
		return dispatcher("/WEB-INF/page/purchase/inquiry/showPurchaseInquiryItemList.jsp");
	}

	@Action(value = "showPurchaseInquiryItemListJosn")
	public void showPurchaseInquiryItemListJosn() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		String id = getRequest().getParameter("id");

		List<PurchaseInquiryItem> list = new ArrayList<PurchaseInquiryItem>();
		Long totalCount = 0l;
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (id != null && id.length() > 0) {
			mapForQuery.put("purchaseInquiry.id", id);
			Map<String, Object> result = this.piService.findPurchaseInquiryItemList(mapForQuery, startNum, limitNum,
					dir, sort);
			totalCount = Long.parseLong(result.get("total").toString());
			list = (List<PurchaseInquiryItem>) result.get("list");
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("supplier1Price", "supplier1Price");
		map.put("supplier2Price", "supplier2Price");
		map.put("supplier3Price", "supplier3Price");
		map.put("storage-currencyType-name", "");
		map.put("storage-id", "storage-id");
		map.put("storage-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 设备询价明细列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseInquiryEquipmentItemList")
	public String showPurchaseInquiryEquipmentItemList() throws Exception {
		return dispatcher("/WEB-INF/page/purchase/inquiry/showPurchaseInquiryEquipmentItemList.jsp");
	}

	@Action(value = "showPurchaseInquiryEquipmentItemListJosn")
	public void showPurchaseInquiryEquipmentItemListJosn() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		String id = getRequest().getParameter("id");
		List<PurchaseInquiryEquipmentItem> list = new ArrayList<PurchaseInquiryEquipmentItem>();
		Long totalCount = 0l;
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (id != null && id.length() > 0) {
			mapForQuery.put("purchaseInquiry.id", id);
			Map<String, Object> result = this.piService.findPurchaseInquiryEquipmentItemList(mapForQuery, startNum,
					limitNum, dir, sort);
			totalCount = Long.parseLong(result.get("total").toString());
			list = (List<PurchaseInquiryEquipmentItem>) result.get("list");
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("supplier1Price", "supplier1Price");
		map.put("supplier2Price", "supplier2Price");
		map.put("supplier3Price", "supplier3Price");
		map.put("equipment-currencyType-name", "");
		map.put("equipment-id", "equipment-id");
		map.put("equipment-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 服务询价明细列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseInquiryServiceItemList")
	public String showPurchaseInquiryServiceItemList() throws Exception {
		return dispatcher("/WEB-INF/page/purchase/inquiry/showPurchaseInquiryServiceItemList.jsp");
	}

	@Action(value = "showPurchaseInquiryServiceItemListJosn")
	public void showPurchaseInquiryServiceItemListJosn() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		String id = getRequest().getParameter("id");
		List<PurchaseInquiryServiceItem> list = new ArrayList<PurchaseInquiryServiceItem>();
		Long totalCount = 0l;
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (id != null && id.length() > 0) {
			mapForQuery.put("purchaseInquiry.id", id);
			Map<String, Object> result = this.piService.findPurchaseInquiryServiceItemList(mapForQuery, startNum,
					limitNum, dir, sort);
			totalCount = Long.parseLong(result.get("total").toString());
			list = (List<PurchaseInquiryServiceItem>) result.get("list");
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("supplier1Price", "supplier1Price");
		map.put("supplier2Price", "supplier2Price");
		map.put("supplier3Price", "supplier3Price");
		map.put("unit", "");
		map.put("serviceCode", "serviceCode");
		map.put("serviceName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	@Action(value = "save")
	public String save() throws Exception {
		String supplierDataJson = getParameterFromRequest("supplierDataJson");
		String itemDataJson = getParameterFromRequest("itemDataJson");
		String equipmentItemDataJson = getParameterFromRequest("equipmentItemDataJson");
		String serviceItemDataJson = getParameterFromRequest("serviceItemDataJson");
		piService
				.save(this.purchaseInquiry, supplierDataJson, itemDataJson, equipmentItemDataJson, serviceItemDataJson);
		//具体操作，如删除，填加动作，应用redirect
		return redirect("/purchase/inquiry/toEditPurchaseInquiry.action?id=" + this.purchaseInquiry.getId());
	}

	/**
	 * 删除库存询价明细
	 * @throws Exception
	 */
	@Action(value = "delPurchaseInquiryItemList")
	public void delPurchaseInquiryItemList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String ids = super.getRequest().getParameter("ids");
		try {
			this.piService.delPurchaseInquiryItemListById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除客户询价明细
	 * @throws Exception
	 */
	@Action(value = "delPurchaseInquirySupplierList")
	public void delPurchaseInquirySupplierList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String ids = super.getRequest().getParameter("ids");
		try {
			this.piService.delPurchaseInquirySupplierListById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备询价明细
	 * @throws Exception
	 */
	@Action(value = "delPurchaseInquiryEquipmentItemList")
	public void delPurchaseInquiryEquipmentItemList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String ids = super.getRequest().getParameter("ids");
		try {
			this.piService.delPurchaseInquiryEquipmentItemListById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除服务询价明细
	 * @throws Exception
	 */
	@Action(value = "delPurchaseInquiryServiceItemList")
	public void delPurchaseInquiryServiceItemList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String ids = super.getRequest().getParameter("ids");
		try {
			this.piService.delPurchaseInquiryServiceItemListById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 生成询价库存明细
	 * @throws Exception
	 */
	@Action(value = "createPurchaseInquiryItem")
	public void createPurchaseInquiryItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String applyId = super.getRequest().getParameter("id");
		try {
			List<Object> list = this.piService.createPurchaseInquiryItem(applyId);
			map.put("success", true);
			map.put("result", list);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 生成询价设备明细
	 * @throws Exception
	 */
	@Action(value = "createPurchaseInquiryEquipmentItem")
	public void createPurchaseInquiryEquipmentItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String applyId = super.getRequest().getParameter("id");
		try {
			List<Object> list = this.piService.createPurchaseInquiryEquipmentItem(applyId);
			map.put("success", true);
			map.put("result", list);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 生成询价服务明细
	 * @throws Exception
	 */
	@Action(value = "createPurchaseInquiryServiceItem")
	public void createPurchaseInquiryServiceItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String applyId = super.getRequest().getParameter("id");
		try {
			List<Object> list = this.piService.createPurchaseInquiryServiceItem(applyId);
			map.put("success", true);
			map.put("result", list);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchaseInquiry getPurchaseInquiry() {
		return purchaseInquiry;
	}

	public void setPurchaseInquiry(PurchaseInquiry purchaseInquiry) {
		this.purchaseInquiry = purchaseInquiry;
	}

}
