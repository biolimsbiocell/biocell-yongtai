package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicUnit;
import com.biolims.equipment.model.Instrument;

@Entity
@Table(name = "T_PURCHASE_APPLY_EQUIP_ITEM")
public class PurchaseApplyEquipmentItem extends EntityDao<PurchaseApplyEquipmentItem> implements Serializable {

	private static final long serialVersionUID = -3944336384849583537L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "EQUIPMENT_ID")
	private Instrument equipment; //设备

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_APPLY_ID")
	private PurchaseApply purchaseApply; //申请单

	@Column(name = "NUM")
	private Integer num; //数量
	@Column(name = "APPROVE_NUM")
	private Integer approveNum; //批准数量

	@Column(name = "UNIT", length = 50)
	private String unit; //单位

	@Column(name = "NOTE", length = 500)
	private String note; //描述

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Instrument getEquipment() {
		return equipment;
	}

	public void setEquipment(Instrument equipment) {
		this.equipment = equipment;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * @return the unit
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public PurchaseApply getPurchaseApply() {
		return purchaseApply;
	}

	public void setPurchaseApply(PurchaseApply purchaseApply) {
		this.purchaseApply = purchaseApply;
	}

	/**
	 * @return the approveNum
	 */
	public Integer getApproveNum() {
		return approveNum;
	}

	/**
	 * @param approveNum the approveNum to set
	 */
	public void setApproveNum(Integer approveNum) {
		this.approveNum = approveNum;
	}

}
