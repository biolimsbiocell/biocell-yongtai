package com.biolims.purchase.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.StorageIn;

/**
 * 退货管理
 * @author Vera
 */
@Entity
@Table(name = "T_PURCHASE_CANCEL")
public class PurchaseCancel extends EntityDao<PurchaseCancel> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//退货ID
	@Column(name = "NOTE", length = 110)
	private String note;//描述
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_ORDER_ID")
	private PurchaseOrder purchaseOrder;//采购订单号
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "STORAGE_IN_ID")
	private StorageIn storageIn;//入库单号

	private Date confirmDate;//批准时间
	@Column(name = "REASON", length = 300)
	private String reason;//退货原因
	private Date canelDate;//退货日期
	private Date createDate;//创建日期	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人
	@Column(name = "STATE", length = 32)
	private String state;//状态
	@Column(name = "STATE_NAME", length = 200)
	private String stateName;
	@Column(name = "TYPE", length = 32)
	private String type;//0 采购订单退货 1 入库订单退货 

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getCanelDate() {
		return canelDate;
	}

	public void setCanelDate(Date canelDate) {
		this.canelDate = canelDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public StorageIn getStorageIn() {
		return storageIn;
	}

	public void setStorageIn(StorageIn storageIn) {
		this.storageIn = storageIn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
