package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

@Entity
@Table(name = "T_PURCHASE_INQUIRY_SUPPLIER")
public class PurchaseInquirySupplier extends EntityDao<PurchaseApplyServiceItem> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3673522727139068054L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "NOTE", length = 110)
	private String note; //备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_INQUIRY_ID")
	private PurchaseInquiry purchaseInquiry; //询价管理单

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public PurchaseInquiry getPurchaseInquiry() {
		return purchaseInquiry;
	}

	public void setPurchaseInquiry(PurchaseInquiry purchaseInquiry) {
		this.purchaseInquiry = purchaseInquiry;
	}

}
