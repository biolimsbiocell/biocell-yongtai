package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;

/**
 * 采购申请明细
 * @author Vera
 */
@Entity
@Table(name = "T_PURCHASE_APPLY_ITEM")
public class PurchaseApplyItem extends EntityDao<PurchaseApplyItem> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//ID

	@Column(name = "NAME", length = 110)
	private String name;//名称

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "STORAGE_ID")
	private Storage storage;//申请对象

	@Column(name = "UNIT", length = 110)
	private String unit;//单位名称

	private Double num;//数量
	
	private Double approveNum;//批准数量

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_APPLY_ID")
	private PurchaseApply purchaseApply;//所属申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_STORAGE_TYPE_ID")
	private DicStorageType type;//类型

	@Column(name = "NOTE", length = 400)
	private String note;//生产商

	@Column(name = "STATE", length = 32)
	private String state;//状态
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public PurchaseApply getPurchaseApply() {
		return purchaseApply;
	}

	public void setPurchaseApply(PurchaseApply purchaseApply) {
		this.purchaseApply = purchaseApply;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public DicStorageType getType() {
		return type;
	}

	public void setType(DicStorageType type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the approveNum
	 */
	public Double getApproveNum() {
		return approveNum;
	}

	/**
	 * @param approveNum the approveNum to set
	 */
	public void setApproveNum(Double approveNum) {
		this.approveNum = approveNum;
	}

}
