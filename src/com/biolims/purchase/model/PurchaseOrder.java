package com.biolims.purchase.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicType;
import com.biolims.core.model.user.Department;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 采购订单
 * @author Vera
 */
@Entity
@Table(name = "T_PURCHASE_ORDER")
public class PurchaseOrder extends EntityDao<PurchaseOrder> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//ID

	@Column(name = "NOTE", length = 110)
	private String note;//描述

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DEPARTMENT_ID")
	private Department department;//采购组织

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_APPLY_ID")
	private PurchaseApply purchaseApply;//采购申请号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_APPLY_USER_ID")
	private User applyUser;//申请创建人

	private Date applyDate;//申请创建日期	

	private Date reachDate;//到货日期


	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_FINANCE_ITEM_ID")
	private DicFinanceItem financeCode;//财务科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//订单创建人

	private Date createDate;//订单创建日期

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//订单批准人
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "CAIGOU_USER_ID")
	private User caigouUser;//采购人

	private Date confirmDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicType currencyType;//bizhong
	
	@Column(name = "STATE", length = 32)
	
	private String state;//状态
	@Column(name = "STATE_NAME", length = 200)
	private String stateName;

	private Double fee;

	@Column(name = "IS_SYS_SEND", length = 32)
	private String isSysSend; //是否是系统自动创建
	@Transient
	private String ifAllIn;//是否完全入库 0 否 1是
	
	@Column(name = "ZHU_SI", length = 200)
	private String zhuSi;//备注
	
	@Column(name = "SH_ADRESS", length = 200)
	private String shAdress;//收货地址
	
	@Column(name = "FP_ADRESS", length = 200)
	private String fpAdress;//发票寄送地址
	
	@Column(name = "FK_WAYS", length = 200)
	private String fkWays;//付款方式
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null") @JoinColumn(name = "FUKUAN_USER")
	private User fukuanUser;//付款人
	

	public String getZhuSi() {
		return zhuSi;
	}

	public void setZhuSi(String zhuSi) {
		this.zhuSi = zhuSi;
	}

	public String getShAdress() {
		return shAdress;
	}

	public void setShAdress(String shAdress) {
		this.shAdress = shAdress;
	}

	public String getFpAdress() {
		return fpAdress;
	}

	public void setFpAdress(String fpAdress) {
		this.fpAdress = fpAdress;
	}

	public String getFkWays() {
		return fkWays;
	}

	public void setFkWays(String fkWays) {
		this.fkWays = fkWays;
	}

	public User getFukuanUser() {
		return fukuanUser;
	}

	public void setFukuanUser(User fukuanUser) {
		this.fukuanUser = fukuanUser;
	}

	public DicType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicType currencyType) {
		this.currencyType = currencyType;
	}

	public User getCaigouUser() {
		return caigouUser;
	}

	public void setCaigouUser(User caigouUser) {
		this.caigouUser = caigouUser;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public PurchaseApply getPurchaseApply() {
		return purchaseApply;
	}

	public void setPurchaseApply(PurchaseApply purchaseApply) {
		this.purchaseApply = purchaseApply;
	}

	public User getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(User applyUser) {
		this.applyUser = applyUser;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public Date getReachDate() {
		return reachDate;
	}

	public void setReachDate(Date reachDate) {
		this.reachDate = reachDate;
	}

	public DicFinanceItem getFinanceCode() {
		return financeCode;
	}

	public void setFinanceCode(DicFinanceItem financeCode) {
		this.financeCode = financeCode;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getIsSysSend() {
		return isSysSend;
	}

	public void setIsSysSend(String isSysSend) {
		this.isSysSend = isSysSend;
	}

	public String getIfAllIn() {
		return ifAllIn;
	}

	public void setIfAllIn(String ifAllIn) {
		this.ifAllIn = ifAllIn;
	}

}
