package com.biolims.purchase.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 付款管理
 * @author Vera
 */
@Entity
@Table(name = "T_PURCHASE_PAYMENT")
public class PurchasePayment extends EntityDao<PurchasePayment> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//ID

	private Date appleDate;//申请日期

	private Double scale;//发票比例

	@Column(name = "NOTE", length = 110)
	private String note;//描述

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_APPLY_USER_ID")
	private User applyUser;//付款申请人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//付款批准人

	private Date confirmDate;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型
	@Column(name = "STATE", length = 32)
	private String state;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;

	private Date createDate;
	@Column(name = "STATE_NAME", length = 110)
	private String stateName;
	@Column(name = "INVOICE_STATE", length = 110)
	private String invoiceState;
	@Column(name = "PAYMENT_STATE", length = 110)
	private String paymentState;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getAppleDate() {
		return appleDate;
	}

	public void setAppleDate(Date appleDate) {
		this.appleDate = appleDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Double getScale() {
		return scale;
	}

	public void setScale(Double scale) {
		this.scale = scale;
	}

	public String getNote() {
		return note;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public User getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(User applyUser) {
		this.applyUser = applyUser;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getInvoiceState() {
		return invoiceState;
	}

	public void setInvoiceState(String invoiceState) {
		this.invoiceState = invoiceState;
	}

	public String getPaymentState() {
		return paymentState;
	}

	public void setPaymentState(String paymentState) {
		this.paymentState = paymentState;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

}
