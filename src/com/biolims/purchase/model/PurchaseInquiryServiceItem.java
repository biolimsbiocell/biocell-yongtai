package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

@Entity
@Table(name = "T_PURCHASE_INQUIRY_SE_ITEM")
public class PurchaseInquiryServiceItem extends EntityDao<PurchaseInquiryServiceItem> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4033260068022263908L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "SUPPLIER1_PRICE")
	private Double supplier1Price; //供应商1价格

	@Column(name = "SUPPLIER2_PRICE")
	private Double supplier2Price; //供应商3价格

	@Column(name = "SUPPLIER3_PRICE")
	private Double supplier3Price; //供应商2价格

	@Column(name = "UNIT", length = 32)
	private String unit; //单位

	@Column(name = "SERVICE_CODE")
	private String serviceCode; //服务编号

	@Column(name = "SERVICE_NAME")
	private String serviceName; //服务描述

	@Column(name = "NOTE", length = 110)
	private String note; //备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_INQUIRY_ID")
	private PurchaseInquiry purchaseInquiry; //询价管理单

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getSupplier1Price() {
		return supplier1Price;
	}

	public void setSupplier1Price(Double supplier1Price) {
		this.supplier1Price = supplier1Price;
	}

	public Double getSupplier2Price() {
		return supplier2Price;
	}

	public void setSupplier2Price(Double supplier2Price) {
		this.supplier2Price = supplier2Price;
	}

	public Double getSupplier3Price() {
		return supplier3Price;
	}

	public void setSupplier3Price(Double supplier3Price) {
		this.supplier3Price = supplier3Price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public PurchaseInquiry getPurchaseInquiry() {
		return purchaseInquiry;
	}

	public void setPurchaseInquiry(PurchaseInquiry purchaseInquiry) {
		this.purchaseInquiry = purchaseInquiry;
	}

}
