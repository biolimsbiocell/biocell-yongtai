package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.supplier.model.Supplier;

@Entity
@Table(name = "T_PURCHASE_ORDER_SERVICE_ITEM")
public class PurchaseOrderServiceItem extends EntityDao<PurchaseOrderServiceItem> implements Serializable {

	private static final long serialVersionUID = 6106933423332328852L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//ID

	@Column(name = "SERVICE_CODE", length = 32)
	private String serviceCode; //采购项目检索码

	@Column(name = "SERVICE_NAME", length = 110)
	private String serviceName;//采购项目名称

	@Column(name = "NUM")
	private Double num; //采购数量

	@Column(name = "UNIT", length = 110)
	private String unit; //单位名称
	/**供应商*/
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "SUPPLIER_ID")
	private Supplier supplier;
	
	

	/**
	 * @return the supplier
	 */
	public Supplier getSupplier() {
		return supplier;
	}

	/**
	 * @param supplier the supplier to set
	 */
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	@Column(name = "PRICE")
	private Double price; //采购单价

	@Column(name = "FEE")
	private Double fee; //采购合计

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_ORDER_ID")
	private PurchaseOrder purchaseOrder;//采购订单

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_APPLY_ID")
	private PurchaseApply purchaseApply;//采购订单

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_APPLY_SERVICE_ITEM_ID")
	private PurchaseApplyServiceItem purchaseApplyServiceItem;//采购订单

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public PurchaseApply getPurchaseApply() {
		return purchaseApply;
	}

	public void setPurchaseApply(PurchaseApply purchaseApply) {
		this.purchaseApply = purchaseApply;
	}

	public PurchaseApplyServiceItem getPurchaseApplyServiceItem() {
		return purchaseApplyServiceItem;
	}

	public void setPurchaseApplyServiceItem(PurchaseApplyServiceItem purchaseApplyServiceItem) {
		this.purchaseApplyServiceItem = purchaseApplyServiceItem;
	}

}
