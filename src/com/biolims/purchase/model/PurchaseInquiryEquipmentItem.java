package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.equipment.model.Instrument;

@Entity
@Table(name = "T_PURCHASE_INQUIRY_ET_ITEM")
public class PurchaseInquiryEquipmentItem extends EntityDao<PurchaseInquiryEquipmentItem> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7602158082787068639L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "SUPPLIER1_PRICE")
	private Double supplier1Price; //供应商1价格

	@Column(name = "SUPPLIER2_PRICE")
	private Double supplier2Price; //供应商3价格

	@Column(name = "SUPPLIER3_PRICE")
	private Double supplier3Price; //供应商2价格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "EQUIPMENT_ID")
	private Instrument equipment; //物资

	@Column(name = "NOTE", length = 110)
	private String note; //备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_INQUIRY_ID")
	private PurchaseInquiry purchaseInquiry; //询价管理单

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getSupplier1Price() {
		return supplier1Price;
	}

	public void setSupplier1Price(Double supplier1Price) {
		this.supplier1Price = supplier1Price;
	}

	public double getSupplier2Price() {
		return supplier2Price;
	}

	public void setSupplier2Price(Double supplier2Price) {
		this.supplier2Price = supplier2Price;
	}

	public double getSupplier3Price() {
		return supplier3Price;
	}

	public void setSupplier3Price(Double supplier3Price) {
		this.supplier3Price = supplier3Price;
	}

	public Instrument getEquipment() {
		return equipment;
	}

	public void setEquipment(Instrument equipment) {
		this.equipment = equipment;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public PurchaseInquiry getPurchaseInquiry() {
		return purchaseInquiry;
	}

	public void setPurchaseInquiry(PurchaseInquiry purchaseInquiry) {
		this.purchaseInquiry = purchaseInquiry;
	}

}
