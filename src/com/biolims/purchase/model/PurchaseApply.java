package com.biolims.purchase.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.Department;
import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicType;

/**
 * 采购申请
 * @author Vera
 */
@Entity
@Table(name = "T_PURCHASE_APPLY")
public class PurchaseApply extends EntityDao<PurchaseApply> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//ID

	@Column(name = "NOTE", length = 110)
	private String note;//描述

	@Column(name = "SPEC", length = 110)
	private String spec;//规格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DEPARTMENT_ID")
	private Department department;//所属部门

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//批准人

	private Date confirmDate;

	private Date createDate;//创建日期	
	
	@Column(name = "EXPECT_DATE", length = 110)
	private Date expectDate;//期望到货日期	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_FINANCE_ITEM_ID")
	private DicFinanceItem financeCode;//财务科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "DIC_COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心

	@Column(name = "IS_SYS_SEND", length = 32)
	private String isSysSend; //是否是系统自动创建

	@Column(name = "STATE", length = 32)
	private String state;//状态
	@Column(name = "STATE_NAME", length = 200)
	private String stateName;
	
	@Column(name = "NOTES", length = 400)
	private String notes;//备注
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public DicFinanceItem getFinanceCode() {
		return financeCode;
	}

	public void setFinanceCode(DicFinanceItem financeCode) {
		this.financeCode = financeCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public String getIsSysSend() {
		return isSysSend;
	}

	public void setIsSysSend(String isSysSend) {
		this.isSysSend = isSysSend;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the expectDate
	 */
	public Date getExpectDate() {
		return expectDate;
	}

	/**
	 * @param expectDate the expectDate to set
	 */
	public void setExpectDate(Date expectDate) {
		this.expectDate = expectDate;
	}

	
}
