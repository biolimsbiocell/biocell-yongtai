package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageInItem;

/**
 * 退货明细
 * @author Vera
 */
@Entity
@Table(name = "T_PURCHASE_CANCEL_ITEM")
public class PurchaseCancelItem extends EntityDao<PurchaseCancelItem> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_CANCEL_ID")
	private PurchaseCancel purchaseCancel;//退货单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "STORAGE_ID")
	private Storage storage;//退货项目

	@Column(name = "SEARCH_CODE", length = 32)
	private String searchCode;//退货项目检索码

	@Column(name = "SERIAL", length = 32)
	private String serial;//批号

	@Column(name = "NAME", length = 110)
	private String name;//退货项目名称

	private Double num;//退货数量
	private Double price;//入库价格
	private Double purchaseNum;//采购数量

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "STORAGE_IN_ITEM_ID")
	private StorageInItem storageInItem;//入库明细
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心
	//非持久化属性
	@Transient
	private Double purchaseOrderNum;//采购数量

	@Transient
	private Double inNum;//入库数量

	private Double storageNum;//库存数量

	@Transient
	private Double cancelNum;//退货数量

	@Transient
	private Double scale;//退货比例

	@Transient
	private String condition;//退货情况

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PurchaseCancel getPurchaseCancel() {
		return purchaseCancel;
	}

	public void setPurchaseCancel(PurchaseCancel purchaseCancel) {
		this.purchaseCancel = purchaseCancel;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public Double getScale() {
		return scale;
	}

	public void setScale(Double scale) {
		this.scale = scale;
	}
	@Column(name = "CONDITION_", length = 60)
	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Double getPurchaseNum() {
		return purchaseNum;
	}

	public void setPurchaseNum(Double purchaseNum) {
		this.purchaseNum = purchaseNum;
	}

	public Double getPurchaseOrderNum() {
		return purchaseOrderNum;
	}

	public void setPurchaseOrderNum(Double purchaseOrderNum) {
		this.purchaseOrderNum = purchaseOrderNum;
	}

	public Double getInNum() {
		return inNum;
	}

	public void setInNum(Double inNum) {
		this.inNum = inNum;
	}

	public Double getCancelNum() {
		return cancelNum;
	}

	public void setCancelNum(Double cancelNum) {
		this.cancelNum = cancelNum;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public StorageInItem getStorageInItem() {
		return storageInItem;
	}

	public void setStorageInItem(StorageInItem storageInItem) {
		this.storageInItem = storageInItem;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public Double getStorageNum() {
		return storageNum;
	}

	public void setStorageNum(Double storageNum) {
		this.storageNum = storageNum;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

}
