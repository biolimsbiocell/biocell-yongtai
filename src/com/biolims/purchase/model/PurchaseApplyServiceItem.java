package com.biolims.purchase.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

@Entity
@Table(name = "T_PURCHASE_APPLY_SERVICE_ITEM")
public class PurchaseApplyServiceItem extends EntityDao<PurchaseApplyServiceItem> implements Serializable {

	private static final long serialVersionUID = 2932198078201367676L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "SERVICE_CODE", length = 80)
	private String serviceCode;//服务编号

	@Column(name = "SERVICE_NAME", length = 80)
	private String serviceName;//服务描述

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")  @JoinColumn(name = "PURCHASE_APPLY_ID")
	private PurchaseApply purchaseApply; //申请单

	@Column(name = "NUM")
	private Integer num; //数量
	private Double approveNum;//批准数量

	@Column(name = "UNIT", length = 32)
	private String unit; //单位

	@Column(name = "NOTE", length = 500)
	private String note; //描述

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public PurchaseApply getPurchaseApply() {
		return purchaseApply;
	}

	public void setPurchaseApply(PurchaseApply purchaseApply) {
		this.purchaseApply = purchaseApply;
	}

	/**
	 * @return the approveNum
	 */
	public Double getApproveNum() {
		return approveNum;
	}

	/**
	 * @param approveNum the approveNum to set
	 */
	public void setApproveNum(Double approveNum) {
		this.approveNum = approveNum;
	}

}
