/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：DAO
 * 类描述：采购订单
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.order.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.contract.model.PurchaseContract;
import com.biolims.purchase.model.PurchaseApplyEquipmentItem;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.purchase.model.PurchaseApplyServiceItem;
import com.biolims.purchase.model.PurchaseOrder;
import com.biolims.purchase.model.PurchaseOrderEquipmentItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.purchase.model.PurchaseOrderServiceItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class PurchaseOrderDao extends BaseHibernateDao {

	/**
	 * 查询入库数量
	 * 
	 * @throws Exception
	 */
	public Double getStorageInSum(String orderId, String storageId) throws Exception {
		String hql = "from StorageInItem where storageIn.purchaseOrder.id = '" + orderId + "' and storageIn.state = '"
				+ SystemConstants.DIC_STATE_YES_ID + "' and storage.id='" + storageId + "'";

		hql = "select sum(num) " + hql;
		Double sum = (Double) this.getSession().createQuery(hql).uniqueResult();

		return sum;
	}

	/**
	 * 查询入库数量
	 * 
	 * @throws Exception
	 */
	public Double getPurchaseCancelSum(String orderId, String storageId) throws Exception {
		String hql = "from PurchaseCancelItem where purchaseCancel.purchaseOrder.id = '" + orderId
				+ "' and purchaseCancel.state = '" + SystemConstants.DIC_STATE_YES_ID + "' and storage.id='"
				+ storageId + "'";
		//String hql = "from PurchaseCancelItem where purchaseCancel.purchaseOrder.id = '" + orderId
		//		+ "' and storage.id='" + storageId + "'";
		hql = "select sum(num) " + hql;
		Double sum = (Double) this.getSession().createQuery(hql).uniqueResult();

		return sum;
	}

	/**
	 * 查询采购数量
	 * 
	 * @throws Exception
	 */
	public Double getPurchaseOrderSum(String orderId, String storageId) throws Exception {
		String hql = "from PurchaseOrderItem where purchaseOrder.id = '" + orderId + "' and storage.id='" + storageId
				+ "'";

		hql = "select sum(num) " + hql;
		Double sum = (Double) this.getSession().createQuery(hql).uniqueResult();

		return sum;
	}

	public Double selectOrderPriceSum(String orderId, String className) {
		String hql = "select sum(price*num) from " + className + " where purchaseOrder.id='" + orderId + "'";
		return (Double) this.getSession().createQuery(hql).uniqueResult();
	}

	/**
	 * 采购订单明细对象
	 * 
	 * @param outId
	 * @return
	 */
	public List<PurchaseOrderItem> findStorageOutItemList(String orderId) {
		String hql = "from PurchaseOrderItem where purchaseOrder.id='" + orderId + "'";
		List<PurchaseOrderItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> selectProducerList(String in, int start, int limit, String dir, String sort) {

		throw new RuntimeException("供应商已经删除，请修改页面，删除供应商信息，并删除此处代码！");
	}

	public Map<String, Object> selectApplyItemList(String in, int start, int limit, String dir, String sort) {

		//String hql = "from PurchaseApplyItem pai where pai.purchaseApply.state = '1' and pai.purchaseApply.id =" + in;

		String hql = "from PurchaseApplyItem pai where pai.purchaseApply.state = '1' and pai.purchaseApply.id in("
				+ in
				+ ") and  pai.id not in (select purchaseApplyItem.id from PurchaseOrderItem where (purchaseOrder.state  like '1%' or purchaseOrder.state  like '2%') and purchaseApplyItem.id is not null)";

		List<PurchaseApplyItem> dataList = new ArrayList<PurchaseApplyItem>();

		Long count = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		if (count > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
				hql = hql + " order by " + sort + " " + dir;

			dataList = this.getSession().createQuery(hql).setFirstResult(start).setMaxResults(limit).list();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("list", dataList);
		return map;
	}

	public List<PurchaseOrderItem> findPurchaseOrderItemById(String id) throws Exception {
		String hql = "from PurchaseOrderItem where purchaseOrder.id='" + id + "'";
		List<PurchaseOrderItem> list = this.getSession().createQuery(hql).list();

		return list;
	}

	public Map<String, Object> selectPurchaseOrderEquipmentItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseOrderEquipmentItem  where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseOrderEquipmentItem> list = new ArrayList<PurchaseOrderEquipmentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public void delPurchaseOrderEquipmentItem(Map<String, String> mapForQuery) throws Exception {
		String hql = " delete from PurchaseOrderEquipmentItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public Map<String, Object> selectPurchaseOrderServiceItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseOrderServiceItem  where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseOrderServiceItem> list = new ArrayList<PurchaseOrderServiceItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public void delPurchaseOrderServiceItem(Map<String, String> mapForQuery) throws Exception {
		String hql = "delete from PurchaseOrderServiceItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public List<PurchaseApplyEquipmentItem> selectPurchaseApplyEquipmentItemByPurchaseOrderId(String purchaseOrderId)
			throws Exception {
		String hql = "select a from PurchaseApplyEquipmentItem a, PurchaseOrderApply o "
				+ "where a.purchaseApply.id = o.purchaseApply.id and o.purchaseOrder.id='" + purchaseOrderId
				+ "' and o.purchaseApply.type.sysCode='" + SystemConstants.PURCHASE_APPLY_DIC_TYPE_EQUIPMENT_SYSCODE
				+ "'";
		return this.getSession().createQuery(hql).list();
	}

	public List<PurchaseApplyServiceItem> selectPurchaseApplyServiceItemByPurchaseOrderId(String purchaseOrderId)
			throws Exception {
		String hql = "select a from PurchaseApplyServiceItem a, PurchaseOrderApply o "
				+ "where a.purchaseApply.id = o.purchaseApply.id and o.purchaseOrder.id='" + purchaseOrderId
				+ "' and o.purchaseApply.type.sysCode ='" + SystemConstants.PURCHASE_APPLY_DIC_TYPE_SERVICE_SYSCODE
				+ "'";
		;
		return this.getSession().createQuery(hql).list();
	}

	public List<PurchaseOrderEquipmentItem> showPurchaseOrderEquipmetItem(
			String formId) {
		String hql=" from PurchaseOrderEquipmentItem where 1=1 and purchaseOrder.id='"+formId+"'";
		List<PurchaseOrderEquipmentItem> list=null;
		list=this.getSession().createQuery(hql).list();
		return list;
	}

	public List<PurchaseOrderServiceItem> showPurchaseOrderServiceItem(
			String formId) {
		String hql=" from PurchaseOrderServiceItem where 1=1 and purchaseOrder.id='"+formId+"'";
		List<PurchaseOrderServiceItem> list=null;
		list=this.getSession().createQuery(hql).list();
		return list;
	}

	public PurchaseContract findPc(String poId, String suppliserId) {
		String hql=" from PurchaseContract where purchaseOrder='"+poId+"' and supplierId='"+suppliserId+"'";
		List<PurchaseContract> list=this.getSession().createQuery(hql).list();
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}

	public Map<String, Object> showPurchaseOrderListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseOrder where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseOrder where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseOrder> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showPurchaseOrderItemListJson(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseOrderItem where 1=1 and purchaseOrder.id = '"+id+"'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseOrderItem where 1=1 and purchaseOrder.id = '"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseOrderItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showPurchaseOrderEquipmentItemListJson(String id, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseOrderEquipmentItem where 1=1 and purchaseOrder.id = '"+id+"'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseOrderEquipmentItem where 1=1 and purchaseOrder.id = '"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseOrderEquipmentItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showPurchaseOrderServiceItemListJson(String id, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseOrderServiceItem where 1=1 and purchaseOrder.id = '"+id+"'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseOrderServiceItem where 1=1 and purchaseOrder.id = '"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseOrderServiceItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

}
