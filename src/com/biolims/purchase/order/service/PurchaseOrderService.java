/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：采购订单
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.order.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.user.dao.UserDAO;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.contract.model.PurchaseContract;
import com.biolims.purchase.contract.model.PurchaseContractItem;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.purchase.model.PurchaseApplyEquipmentItem;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.purchase.model.PurchaseApplyServiceItem;
import com.biolims.purchase.model.PurchaseOrder;
import com.biolims.purchase.model.PurchaseOrderApply;
import com.biolims.purchase.model.PurchaseOrderEquipmentItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.purchase.model.PurchaseOrderServiceItem;
import com.biolims.purchase.order.dao.PurchaseOrderDao;
import com.biolims.remind.model.SysRemind;
import com.biolims.storage.common.dao.StorageCommonDao;
import com.biolims.storage.model.Storage;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
public class PurchaseOrderService extends ApplicationTypeService {

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private UserDAO userDAO;

	@Resource
	private StorageCommonDao storageCommonDAO;

	@Autowired
	private PurchaseOrderDao purchaseOrderDao;

	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 检索list,采用map方式传递检索参数
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findPurchaseOrderList(int startNum,
			int limitNum, String dir, String sort, String contextPath,
			String data, String rightsId, String curUserId) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		String startDate = "";
		String endDate = "";
		if (data != null && !data.equals("")) {
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForQuery.remove("queryStartDate");
			mapForQuery.remove("queryEndDate");
			mapForCondition.remove("queryStartDate");
			mapForCondition.remove("queryEndDate");

		}
		// 设置组织机构管理范围Map
		Map<String, String> mapForManageScope = new HashMap<String, String>();

		// 生成管理范围map
		// mapForManageScope = BeanUtils.configueDepartmentMap("department",
		// "applyUser.id", rightsId, curUserId);
		//
		mapForQuery.put("mapForManageScope", mapForManageScope);
		// // 将map值传入，获取列表
		// if (!curUserId.equals("all")) {
		// mapForQuery.put("scopeType", curUserId);
		// mapForCondition.put("scopeType", "=");
		// }
		Map<String, Object> criteriaMap = commonDAO.findObjectCriteria(
				startNum, limitNum, dir, sort, PurchaseOrder.class,
				mapForQuery, mapForCondition);
		Criteria cr = (Criteria) criteriaMap.get("criteria");
		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
		Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
		if (data != null && !data.equals("")) {
			if (map.get("queryStartDate") != null
					&& !map.get("queryStartDate").equals("")) {

				startDate = (String) map.get("queryStartDate");
				cr.add(Restrictions.ge("createDate", DateUtil.parse(startDate)));
				cc.add(Restrictions.ge("createDate", DateUtil.parse(startDate)));
			}
			if (map.get("queryEndDate") != null
					&& !map.get("queryEndDate").equals("")) {

				endDate = (String) map.get("queryEndDate");
				cr.add(Restrictions.le("createDate", DateUtil.parse(endDate)));
				cc.add(Restrictions.le("createDate", DateUtil.parse(endDate)));
			}

		}
		Map<String, Object> controlMap = commonDAO.findObjectList(startNum,
				limitNum, dir, sort, PurchaseOrder.class, mapForQuery,
				mapForCondition, cc, cr);
		return controlMap;
	}

	public Map<String, Object> findPurchaseOrderListByIfAllIn(int startNum,
			int limitNum, String dir, String sort, String contextPath,
			String data, String rightsId, String curUserId) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		String startDate = "";
		String endDate = "";
		if (data != null && !data.equals("")) {
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);

		}
		// 设置组织机构管理范围Map
		Map<String, String> mapForManageScope = new HashMap<String, String>();

		// 生成管理范围map
		mapForManageScope = BeanUtils.configueDepartmentMap("department",
				"applyUser.id", rightsId, curUserId);

		mapForQuery.put("mapForManageScope", mapForManageScope);
		// 将map值传入，获取列表
		Map<String, Object> criteriaMap = commonDAO.findObjectCriteria(
				startNum, limitNum, dir, sort, PurchaseOrder.class,
				mapForQuery, mapForCondition);
		Criteria cr = (Criteria) criteriaMap.get("criteria");
		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
		Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
		if (data != null && !data.equals("")) {
			if (map.get("queryStartDate") != null
					&& !map.get("queryStartDate").equals("")) {

				startDate = (String) map.get("queryStartDate");
				cr.add(Restrictions.ge("createDate", DateUtil.parse(startDate)));
				cc.add(Restrictions.ge("createDate", DateUtil.parse(startDate)));
			}
			if (map.get("queryEndDate") != null
					&& !map.get("queryEndDate").equals("")) {

				endDate = (String) map.get("queryEndDate");
				cr.add(Restrictions.le("createDate", DateUtil.parse(endDate)));
				cc.add(Restrictions.le("createDate", DateUtil.parse(endDate)));
			}

		}
		Map<String, Object> controlMap = commonDAO.findObjectList(startNum,
				limitNum, dir, sort, PurchaseOrder.class, mapForQuery,
				mapForCondition, cc, cr);

		List<PurchaseOrder> list = (List<PurchaseOrder>) controlMap.get("list");

		for (PurchaseOrder po : list) {
			boolean flag = true;
			List<PurchaseOrderItem> poiList = purchaseOrderDao
					.findPurchaseOrderItemById(po.getId());

			for (PurchaseOrderItem poii : poiList) {
				Double sum = purchaseOrderDao.getPurchaseOrderSum(po.getId(),
						poii.getStorage().getId());

				Double inSum = purchaseOrderDao.getStorageInSum(po.getId(),
						poii.getStorage().getId());
				if (inSum != null) {
					if (sum - inSum > 0) {
						flag = false;
						break;
					}
				} else {

					flag = false;
					break;
				}

			}
			if (flag == true) {
				po.setIfAllIn("已完整入库");

			} else {
				po.setIfAllIn("未完整入库");
			}

		}

		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 根据id获得申请单对象
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PurchaseOrder getPurchaseOrder(String id) throws Exception {
		return commonDAO.get(PurchaseOrder.class, id);
	}

	/**
	 * 检索申请单明细
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchaseOrderId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showPurchaseOrderItemList(int startNum,
			int limitNum, String dir, String sort, String contextPath,
			String purchaseOrderId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
			mapForQuery.put("purchaseOrder.id", purchaseOrderId);
			mapForCondition.put("purchaseOrder.id", "=");
		}
		Map<String, Object> controlMap = commonDAO.findObjectCondition(
				startNum, limitNum, dir, sort, PurchaseOrderItem.class,
				mapForQuery, mapForCondition);
		List<PurchaseOrderItem> list = (List<PurchaseOrderItem>) controlMap
				.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 新增或更新采购申请
	 * 
	 * @param pa
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(PurchaseOrder pa) throws Exception {
		commonDAO.saveOrUpdate(pa);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseOrderItem(String jsonString, PurchaseOrder po)
			throws Exception {
		if (jsonString != null && jsonString.length() > 0) {
			// 将json读入map
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					jsonString, List.class);
			for (Map<String, Object> map : list) {
				PurchaseOrderItem em = new PurchaseOrderItem();
				// 将map信息读入实体类
				em = (PurchaseOrderItem) commonDAO.Map2Bean(map, em);
				if (em.getId() != null && em.getId().equals("")) {
					em.setId(null);
				}
				em.setPurchaseOrder(po);

				commonDAO.saveOrUpdate(em);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseOrderApply(String jsonString, PurchaseOrder po)
			throws Exception {
		if (jsonString.length() > 0 && jsonString != null) {
			// 将json读入map
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					jsonString, List.class);
			for (Map<String, Object> map : list) {
				PurchaseOrderApply em = new PurchaseOrderApply();
				// 将map信息读入实体类
				em = (PurchaseOrderApply) commonDAO.Map2Bean(map, em);
				if (em.getId() != null && em.getId().equals("")) {
					em.setId(null);
				}
				em.setPurchaseOrder(po);
				commonDAO.saveOrUpdate(em);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseOrderItem(String[] ids) {
		for (String id : ids) {
			PurchaseOrderItem item = commonDAO.get(PurchaseOrderItem.class, id);
			commonDAO.delete(item);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("PurchaseOrder");
				li.setFileId(item.getPurchaseOrder().getId());
				li.setModifyContent("物资采购明细:"+"物资编号:"+item.getStorage().getId()+",物资名称:"+item.getStorage().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<PurchaseOrderItem> showPurchaseOrderItemList(String id)
			throws Exception {
		List<PurchaseOrderItem> list = purchaseOrderDao
				.findStorageOutItemList(id);
		return list;
	}

	public Map<String, Object> selectProducerList(String inStr, int startNum,
			int limitNum, String dir, String sort) throws Exception {
		String[] a = inStr.split("\\|");
		String sql = "";
		for (String a1 : a) {
			if (sql.equals(""))
				sql = "'" + a1 + "'";
			else
				sql += ",'" + a1 + "'";
		}

		Map<String, Object> controlMap = purchaseOrderDao.selectProducerList(
				sql, startNum, limitNum, dir, sort);
		return controlMap;
	}

	/**
	 * 检索申请单明细
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchaseApplyId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showPurchaseApplyItemList(String inStr,
			int startNum, int limitNum, String dir, String sort)
			throws Exception {
		String[] a = inStr.split("\\|");
		String sql = "";
		for (String a1 : a) {
			if (sql.equals(""))
				sql = "'" + a1 + "'";
			else
				sql += ",'" + a1 + "'";
		}

		Map<String, Object> controlMap = purchaseOrderDao.selectApplyItemList(
				sql, startNum, limitNum, dir, sort);
		return controlMap;
	}

	/**
	 * 采购订单动作
	 * 
	 * @param formId
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void purchaseOrderSetStorage(String applicationTypeActionId,
			String formId) throws Exception {
		PurchaseOrder po=new PurchaseOrder();
		po=purchaseOrderDao.get(PurchaseOrder.class, formId);
		po.setState("1");
		po.setStateName("完成");
		purchaseOrderDao.saveOrUpdate(po);
		if(po.getType()!=null) {
			if("2".equals(po.getType().getId())){
				List<PurchaseOrderItem> list = showPurchaseOrderItemList(formId);
				List<String> listr=new ArrayList<String>();
				for(PurchaseOrderItem poi:list){
					if(!listr.contains(poi.getSupplier().getId())){
						PurchaseContract pc=new PurchaseContract();
						String autoID = codingRuleService.genTransID("PurchaseContract", "PC");
						pc.setId(autoID);
						pc.setCreateDate(new Date());
						pc.setState("3");
						pc.setStateName("新建");
						pc.setSupplierId(poi.getSupplier());
						pc.setCreateUser(po.getConfirmUser());
						pc.setPurchaseOrder(po.getId());
						PurchaseContractItem pci=new PurchaseContractItem();
						pci.setPurchaseContract(pc);
						pci.setStorageId(poi.getStorage().getId());
						pci.setStorageName(poi.getStorage().getName());
						pci.setNum(poi.getNum());
						pci.setPrice(poi.getPrice());
						pci.setFee(poi.getFee());
						purchaseOrderDao.saveOrUpdate(pc);
						purchaseOrderDao.saveOrUpdate(pci);
						listr.add(poi.getSupplier().getId());
					}else{
						PurchaseContract pc= null;
						pc=purchaseOrderDao.findPc(po.getId(),poi.getSupplier().getId());
						PurchaseContractItem pci=new PurchaseContractItem();
						pci.setPurchaseContract(pc);
						pci.setStorageId(poi.getStorage().getId());
						pci.setStorageName(poi.getStorage().getName());
						pci.setNum(poi.getNum());
						pci.setPrice(poi.getPrice());
						pci.setFee(poi.getFee());
						purchaseOrderDao.saveOrUpdate(pci);
					}
				}
			}else if("77".equals(po.getType().getId())){
				List<PurchaseOrderEquipmentItem> list2=purchaseOrderDao.showPurchaseOrderEquipmetItem(formId);
				List<String> listr2=new ArrayList<String>();
				for(PurchaseOrderEquipmentItem poei:list2){
					if(!listr2.contains(poei.getSupplier().getId())){
						PurchaseContract pc=new PurchaseContract();
						String autoID = codingRuleService.genTransID("PurchaseContract", "PC");
						pc.setId(autoID);
						pc.setCreateDate(new Date());
						pc.setState("3");
						pc.setStateName("新建");
						pc.setSupplierId(poei.getSupplier());
						pc.setCreateUser(po.getConfirmUser());
						pc.setPurchaseOrder(po.getId());
						PurchaseContractItem pci=new PurchaseContractItem();
						pci.setPurchaseContract(pc);
						pci.setStorageId(poei.getEquipment().getId());
						pci.setStorageName(poei.getEquipment().getName());
						pci.setNum(poei.getNum());
						pci.setPrice(poei.getPrice());
						pci.setFee(poei.getFee());
						purchaseOrderDao.saveOrUpdate(pc);
						purchaseOrderDao.saveOrUpdate(pci);
						listr2.add(poei.getSupplier().getId());
					}else{
						PurchaseContract pc= null;
						pc=purchaseOrderDao.findPc(po.getId(),poei.getSupplier().getId());
						PurchaseContractItem pci=new PurchaseContractItem();
						pci.setPurchaseContract(pc);
						pci.setStorageId(poei.getEquipment().getId());
						pci.setStorageName(poei.getEquipment().getName());
						pci.setNum(poei.getNum());
						pci.setPrice(poei.getPrice());
						pci.setFee(poei.getFee());
						purchaseOrderDao.saveOrUpdate(pci);
					}
				}
			}else if("88".equals(po.getType().getId())){
				List<PurchaseOrderServiceItem> list3=purchaseOrderDao.showPurchaseOrderServiceItem(formId);
				List<String> listr3=new ArrayList<String>();
				for(PurchaseOrderServiceItem posi:list3){
					if(!listr3.contains(posi.getSupplier().getId())){
						PurchaseContract pc=new PurchaseContract();
						String autoID = codingRuleService.genTransID("PurchaseContract", "PC");
						pc.setId(autoID);
						pc.setCreateDate(new Date());
						pc.setState("3");
						pc.setStateName("新建");
						pc.setSupplierId(posi.getSupplier());
						pc.setCreateUser(po.getConfirmUser());
						pc.setPurchaseOrder(po.getId());
						PurchaseContractItem pci=new PurchaseContractItem();
						pci.setPurchaseContract(pc);
						pci.setStorageId(posi.getServiceCode());
						pci.setStorageName(posi.getServiceName());
						pci.setNum(posi.getNum());
						pci.setPrice(posi.getPrice());
						pci.setFee(posi.getFee());
						purchaseOrderDao.saveOrUpdate(pc);
						purchaseOrderDao.saveOrUpdate(pci);
						listr3.add(posi.getSupplier().getId());
					}else{
						PurchaseContract pc= null;
						pc=purchaseOrderDao.findPc(po.getId(),posi.getSupplier().getId());
						PurchaseContractItem pci=new PurchaseContractItem();
						pci.setPurchaseContract(pc);
						pci.setStorageId(posi.getServiceCode());
						pci.setStorageName(posi.getServiceName());
						pci.setNum(posi.getNum());
						purchaseOrderDao.saveOrUpdate(pci);
					}
		}
			}
		}
 
//		for (PurchaseOrderItem em : list) {
//
//			Storage aStorage = commonDAO.get(em.getStorage().getClass(), em
//					.getStorage().getId());
//			Double num = 0.0;
//			// Double oldNum = 0.0;
//			// oldNum = aStorage.getOnWayNum();
//			if (aStorage.getOnWayNum() != null)
//				num = aStorage.getOnWayNum();// 原在途数量
//			if (em.getNum() != null) {
//				num = num + em.getNum();// 新在途数量
//				aStorage.setOnWayNum(num);
//
//			}
//			commonDAO.saveOrUpdate(aStorage);
//
//		}
	}

	/**
	 * 查询设备采购订单明细列表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseOrderEquipmentItem(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseOrderDao.selectPurchaseOrderEquipmentItem(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@Transactional(rollbackFor = Exception.class)
	public String batchStorageOrderItem() throws Exception {

		List<Storage> list = storageCommonDAO.getSafeNumStorageList();

		String message = "";
		if (list.size() > 0) {
			int addNum = 0;
			PurchaseOrder po = new PurchaseOrder();
			String code = systemCodeService.getSystemCode(
					SystemCode.PURCHASE_ORDER_NAME,
					SystemCode.PURCHASE_ORDER_CODE, null, null, "1=1", addNum);
			po.setId(code);
			po.setApplyDate(new Date());
			po.setCreateDate(new Date());
			// 设置物资采购类型
			DicType dt = new DicType();
			dt.setId("2");
			po.setType(dt);

			po.setIsSysSend(SystemConstants.DIC_STATE_YES_ID);
			po.setState(SystemConstants.DIC_STATE_NEW);
			po.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			po.setNote("系统自动创建");
			commonDAO.saveOrUpdate(po);

			SysRemind sr = new SysRemind();
			List<User> userList = userDAO.getUserByUserPurchaseJob();
			User dutyUser = null;
			if (userList.size() > 0)
				dutyUser = userList.get(0);

			sr.setHandleUser(dutyUser);

			sr.setTitle(new StringBuffer("库存已生成采购单,请处理！").toString());
			sr.setRemindUser("SYSTEM");
			sr.setType("2");
			sr.setStartDate(new Date());
			sr.setState(SystemConstants.DIC_STATE_NO_ID);

			sr.setTableId("purchaseOrder");
			sr.setContentId(po.getId());
			commonDAO.saveOrUpdate(sr);
			for (Storage s : list) {

				if (s.getSafeNum() != null && s.getSafeNum() != 0) {
					Double n = 0.0;
					if (s.getNum() != null) {
						n = s.getNum();
					}
					Double onwayNum = 0.0;
					if (s.getOnWayNum() != null) {
						onwayNum = s.getOnWayNum();
					}
					if (s.getIfSecondBuy() != null) {
						if (s.getIfSecondBuy().equals(
								SystemConstants.DIC_STATE_YES_ID)
								&& (n + onwayNum) < s.getSafeNum()) {

							Double cgNum = 0.0;// 采购数量
							Double sNum = 0.0;// 库存数量
							if (s.getNum() != null)
								sNum = s.getNum();

							// if (s.getPurchaseNum() != null)
							// cgNum = s.getPurchaseNum();
							// 采购数量-库存数量-在途数量
							Double ceNum = cgNum - sNum - onwayNum;
							if (ceNum < 0)
								ceNum = 0.0;

							if (ceNum >= 0) {
								PurchaseOrderItem poi = new PurchaseOrderItem();
								poi.setStorage(s);
								poi.setNum(ceNum);

								poi.setPurchaseOrder(po);

								commonDAO.saveOrUpdate(poi);

								SysRemind sr1 = new SysRemind();

								User dutyUser1 = s.getDutyUser();

								sr1.setHandleUser(dutyUser1);

								sr1.setTitle(new StringBuffer(s.getId())
										.append(s.getName())
										.append("库存已经报警,请处理！").toString());
								sr1.setRemindUser("SYSTEM");
								sr1.setType("1");
								sr1.setStartDate(new Date());
								sr1.setState(SystemConstants.DIC_STATE_NO_ID);
								sr1.setContentId(po.getId());
								commonDAO.saveOrUpdate(sr1);
							}
						}
					}
				}
			}
		}
		return message;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseOrderEquipmentItem(PurchaseOrder purchaseOrder,
			String itemDataJson) throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					itemDataJson, List.class);
			List<PurchaseOrderEquipmentItem> poeiList = new ArrayList<PurchaseOrderEquipmentItem>();
			for (Map<String, Object> map : list) {
				PurchaseOrderEquipmentItem poei = new PurchaseOrderEquipmentItem();
				poei = (PurchaseOrderEquipmentItem) this.purchaseOrderDao
						.Map2Bean(map, poei);
				if (poei.getId() != null && poei.getId().equals(""))
					poei.setId(null);
				if (poei.getNum() != null && poei.getPrice() != null) {
					poei.setFee(poei.getPrice() * poei.getNum());
				}
				poei.setPurchaseOrder(purchaseOrder);
				poeiList.add(poei);
			}
			this.purchaseOrderDao.saveOrUpdateAll(poeiList);
		}
	}

	/**
	 * 删除设备采购订单明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseOrderEquipmentItemById(String ids) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (ids != null && ids.length() > 0) {
			mapForQuery.put("id", "in##@@##(" + ids + ")");
		}
		this.purchaseOrderDao.delPurchaseOrderEquipmentItem(mapForQuery);
	}

	/**
	 * 查询服务采购订单明细列表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseOrderServiceItem(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseOrderDao.selectPurchaseOrderServiceItem(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 保存服务采购订单明细
	 * 
	 * @param purchaseOrder
	 * @param itemDataJson
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseOrderServiceItem(PurchaseOrder purchaseOrder,
			String itemDataJson) throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					itemDataJson, List.class);
			List<PurchaseOrderServiceItem> posiList = new ArrayList<PurchaseOrderServiceItem>();
			for (Map<String, Object> map : list) {
				PurchaseOrderServiceItem posi = new PurchaseOrderServiceItem();
				posi = (PurchaseOrderServiceItem) this.purchaseOrderDao
						.Map2Bean(map, posi);
				if (posi.getId() != null && posi.getId().equals(""))
					posi.setId(null);
				if (posi.getNum() != null && posi.getPrice() != null) {
					posi.setFee(posi.getPrice() * posi.getNum());
				}
				posi.setPurchaseOrder(purchaseOrder);
				posiList.add(posi);
			}
			this.purchaseOrderDao.saveOrUpdateAll(posiList);
		}
	}

	/**
	 * 删除服务采购订单明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseOrderServiceItemById(String ids) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (ids != null && ids.length() > 0) {
			mapForQuery.put("id", "in##@@##(" + ids + ")");
		}
		this.purchaseOrderDao.delPurchaseOrderServiceItem(mapForQuery);
	}

	/**
	 * 新增或更新采购申请(大保存)
	 * 
	 * @param pa
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String mainJson,String changeLog,Map<String ,String> dataMap)
			throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		mainJson = "["+mainJson+"]";
		List<Map<String,Object>> mainList = JsonUtils.toListByJsonArray(mainJson, List.class);
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder = (PurchaseOrder) commonDAO.Map2Bean(mainList.get(0), purchaseOrder);
		String log = "";
		if((purchaseOrder.getId()!=null&&"".equals(purchaseOrder.getId()))||purchaseOrder.getId().equals("NEW")) {
			log ="123";
			String modelName = "PurchaseOrder";
			String markCode = "PO";
			String autoID = codingRuleService.genTransID(modelName,
					markCode);
			purchaseOrder.setId(autoID);
			purchaseOrder.setCreateDate(new Date());
			purchaseOrder.setCreateUser(u);
		}
		commonDAO.saveOrUpdate(purchaseOrder);
		String id = purchaseOrder.getId();
		
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			if(u!=null) {
				li.setUserId(u.getId());
			}
			li.setFileId(id);
			li.setClassName("PurchaseOrder");
			li.setModifyContent(changeLog);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		
		//保存物资申请明细
		String goodsJson = (String) dataMap.get("goodsJson");
		String goodsChangeLogItem = dataMap.get("goodsChangeLogItem");
		if(goodsJson != null && !goodsJson.equals("[]") && !goodsJson.equals("")) {
			saveGoodsItemTable(purchaseOrder,goodsJson,goodsChangeLogItem,log);
		}
		
		//保存设备明细
		String instrumentJson = (String) dataMap.get("instrumentJson");
		String instrumentChangeLogItem = dataMap.get("instrumentChangeLogItem");
		if(instrumentJson != null && !instrumentJson.equals("[]") && !instrumentJson.equals("")) {
			saveInstrumentItemTable(purchaseOrder,instrumentJson,instrumentChangeLogItem,log);
		}
		
		//保存服务明细
		String serviceJson = (String) dataMap.get("serviceJson");
		String serviceChangeLogItem = dataMap.get("serviceChangeLogItem");
		if(serviceJson != null && !serviceJson.equals("[]") && !serviceJson.equals("")) {
			saveServiceItemTable(purchaseOrder,serviceJson,serviceChangeLogItem,log);
		}
		return id;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveServiceItemTable(PurchaseOrder purchaseOrder, String serviceJson, String serviceChangeLogItem,String log) throws Exception {
		List<PurchaseOrderServiceItem> saveItems = new ArrayList<PurchaseOrderServiceItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(serviceJson, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			PurchaseOrderServiceItem scp = new PurchaseOrderServiceItem();
			scp = (PurchaseOrderServiceItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setPurchaseOrder(purchaseOrder);
			if((scp.getPrice()!=null&&!"".equals(scp.getPrice()))&&(scp.getNum()!=null&&!"".equals(scp.getNum()))) {
				scp.setFee(scp.getPrice()*scp.getNum());
			}
			
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (serviceChangeLogItem != null && !"".equals(serviceChangeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(purchaseOrder.getId());
			li.setClassName("PurchaseOrder");
			li.setModifyContent(serviceChangeLogItem);
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveInstrumentItemTable(PurchaseOrder purchaseOrder, String instrumentJson,
			String instrumentChangeLogItem,String log) throws Exception {
		List<PurchaseOrderEquipmentItem> saveItems = new ArrayList<PurchaseOrderEquipmentItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(instrumentJson, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			PurchaseOrderEquipmentItem scp = new PurchaseOrderEquipmentItem();
			scp = (PurchaseOrderEquipmentItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setPurchaseOrder(purchaseOrder);
			if((scp.getPrice()!=null&&!"".equals(scp.getPrice()))&&(scp.getNum()!=null&&!"".equals(scp.getNum()))) {
				scp.setFee(scp.getPrice()*scp.getNum());
			}
			
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (instrumentChangeLogItem != null && !"".equals(instrumentChangeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(purchaseOrder.getId());
			li.setClassName("PurchaseOrder");
			li.setModifyContent(instrumentChangeLogItem);
			commonDAO.saveOrUpdate(li);
		}
		
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveGoodsItemTable(PurchaseOrder purchaseOrder, String goodsJson, String goodsChangeLogItem,String log) throws Exception {
		List<PurchaseOrderItem> saveItems = new ArrayList<PurchaseOrderItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(goodsJson, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			PurchaseOrderItem scp = new PurchaseOrderItem();
			scp = (PurchaseOrderItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setPurchaseOrder(purchaseOrder);
			if((scp.getPrice()!=null&&!"".equals(scp.getPrice()))&&(scp.getNum()!=null&&!"".equals(scp.getNum()))) {
				scp.setFee(scp.getPrice()*scp.getNum());
			}
			
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (goodsChangeLogItem != null && !"".equals(goodsChangeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(purchaseOrder.getId());
			li.setClassName("PurchaseOrder");
			li.setModifyContent(goodsChangeLogItem);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 根据申请号，生成订单设备明细
	 * 
	 * @param id
	 *            订单号
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> createPurchaseOrderEquipmentItem(String id)
			throws Exception {
		List<PurchaseApplyEquipmentItem> peaiList = this.purchaseOrderDao
				.selectPurchaseApplyEquipmentItemByPurchaseOrderId(id);
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for (PurchaseApplyEquipmentItem peai : peaiList) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("equipment-id", peai.getEquipment().getId());
			map.put("equipment-name", peai.getEquipment().getName());
			map.put("equipment-searchCode", peai.getEquipment().getName());
			map.put("equipment-spec", peai.getEquipment().getName());
			map.put("note", peai.getNote());
			map.put("num", peai.getNum().toString());
			if (peai.getUnit() != null) {
				map.put("unit", peai.getUnit());
			} else {
				map.put("unit", "");
			}
			map.put("purchaseApply-id", peai.getPurchaseApply().getId());
			map.put("purchaseApplyEquipmentItem-id", peai.getId());
			result.add(map);
		}
		return result;
	}

	/**
	 * 根据申请号，生成订单服务明细
	 * 
	 * @param id
	 *            订单号
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> createPurchaseOrderServiceItem(String id)
			throws Exception {
		List<PurchaseApplyServiceItem> pasiList = this.purchaseOrderDao
				.selectPurchaseApplyServiceItemByPurchaseOrderId(id);
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for (PurchaseApplyServiceItem pasi : pasiList) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("serviceCode", pasi.getServiceCode());
			map.put("serviceName", pasi.getNote());
			map.put("num", pasi.getNum().toString());
			map.put("unit", pasi.getUnit());
			map.put("purchaseApply-id", pasi.getPurchaseApply().getId());
			map.put("purchaseApplyServiceItem-id", pasi.getId());
			result.add(map);
		}
		return result;
	}

	public Map<String, Object> showPurchaseOrderListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return purchaseOrderDao.showPurchaseOrderListJson(start, length, query, col, sort);
	}

	public Map<String, Object> showPurchaseOrderItemListJson(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return purchaseOrderDao.showPurchaseOrderItemListJson(id, start, length, query, col,
				sort);
	}

	public Map<String, Object> showPurchaseOrderEquipmentItemListJson(String id, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return purchaseOrderDao.showPurchaseOrderEquipmentItemListJson(id,
				start, length, query, col, sort);
	}

	public Map<String, Object> showPurchaseOrderServiceItemListJson(String id, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return purchaseOrderDao.showPurchaseOrderServiceItemListJson(id,
				start, length, query, col, sort);
	}
}
