/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：采购订单
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.order.action;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.file.service.FileInfoService;
import com.biolims.purchase.apply.service.PurchaseApplyService;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.common.service.PurchaseSystemCodeService;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.purchase.model.PurchaseOrder;
import com.biolims.purchase.model.PurchaseOrderApply;
import com.biolims.purchase.model.PurchaseOrderEquipmentItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.purchase.model.PurchaseOrderServiceItem;
import com.biolims.purchase.order.service.PurchaseOrderService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.DateUtil;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/purchase/order")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class PurchaseOrderAction extends BaseActionSupport {

	private static final long serialVersionUID = 6480754183198344386L;
	@Autowired
	private PurchaseApplyService paService;
	@Autowired
	private PurchaseOrderService poService;
	@Resource
	private FieldService fieldService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Autowired
	private CommonService commonService;
	// 用于页面上显示模块名称
	private String title = "采购订单";

	// 该action权限id
	private String rightsId = "30202";

	private PurchaseOrder purchaseOrder = new PurchaseOrder();

	@Resource
	private PurchaseSystemCodeService purchaseSystemCodeService;

	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private FileInfoService fileInfoService;
	
	@Action(value = "showPurchaseOrderListTable")
	public String showPurchaseOrderListTable() throws Exception {
		rightsId = "30202";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/purchase/order/showPurchaseOrderList.jsp");
	}

	@Action(value = "showPurchaseOrderListTableJson")
	public void showPurchaseOrderListTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = poService.showPurchaseOrderListJson(start, length, query, col, sort);
			List<PurchaseOrder> list = (List<PurchaseOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("type-name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmUser-name", "");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseOrder");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 申请列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseOrderList")
	public String showApplyList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		// 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		// id type format header width sort hide align xtype editable renderer
		// editor
		// 列的id 当"common" 时该列为扩展列 type类型 format格式 header列头 width列宽 sort是否排序
		// hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器
		map.put("id", new String[] { "", "string", "", "订单编号", "150", "true",
				"", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "350", "true",
				"", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "订单类型", "100",
				"true", "", "", "", "", "", "" });
		// map.put("purchaseApply-id", new String[] { "", "string", "",
		// "采购申请单编号", "100", "true", "", "", "", "", "", "" });
		map.put("createUser-name", new String[] { "", "string", "", "采购人",
				"100", "true", "", "", "", "", "", "" });
		map.put("createDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
				"订单日期", "100", "true", "", "", "", "", "formatDate", "" });
		map.put("confirmUser-name", new String[] { "", "string", "", "批准人",
				"100", "true", "true", "", "", "", "", "" });
		map.put("stateName", new String[] { "", "string", "", "工作流状态", "100",
				"true", "", "", "", "", "", "" });
		map.put("department-name", new String[] { "", "string", "", "采购组织",
				"150", "true", "true", "", "", "", "", "" });
		// map.put("financeCode-name",
		// new String[] { "", "string", "", "财务科目", "150", "true", "true", "",
		// "", "", "", "" });
		map.put("reachDate", new String[] { "", "string", "", "到货日期", "150",
				"true", "true", "", "", "", "", "" });
		map.put("currencyType-name", new String[] { "", "string", "", "币种",
				"150", "true", "true", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/purchase/order/showPurchaseOrderListJson.action?queryMethod="
						+ getParameterFromRequest("queryMethod"));
		return dispatcher("/WEB-INF/page/purchase/order/showPurchaseOrderList.jsp");
	}

	@Action(value = "showPurchaseOrderListJson")
	public void showApplyListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "PurchaseOrder", dir, sort,
				queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		// 取出session中检索用的对象
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = poService
				.findPurchaseOrderList(
						startNum,
						limitNum,
						dir,
						sort,
						getContextPath(),
						data,
						rightsId,
						(String) this
								.getObjFromSession(SystemConstants.USER_SESSION_SCOPE_KEY));
		long totalCount = Long.parseLong(controlMap.get("totalCount")
				.toString());
		List<PurchaseOrder> list = (List<PurchaseOrder>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-name", "");
		// map.put("purchaseApply-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-name", "");
		map.put("stateName", "");
		map.put("department-name", "");
		map.put("financeCode-name", "");
		map.put("reachDate", "yyyy-MM-dd");
		map.put("currencyType-name", "");

		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}

	/**
	 * 编辑页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditPurchaseOrder")
	public String toEditPurchaseOrder() throws Exception {
		rightsId = "30201";
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		long num = 0;
		if (id != null && !id.equals("")) {
			purchaseOrder = poService.getPurchaseOrder(id);
			toState(purchaseOrder.getState());
			num = fileInfoService.findFileInfoCount(id, "purchaseOrder");
			if (purchaseOrder.getState() != null
					&& purchaseOrder.getState().equals(
							SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
//			showPurchaseOrderItemList(handlemethod, id);
			showPurchaseOrderApplyList(handlemethod, id);
			toToolBar(rightsId, "", "", handlemethod);
			putObjToContext("instanceId",
					workflowProcessInstanceService.findInstanceIdById(id));
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.purchaseOrder.setId(SystemCode.DEFAULT_SYSTEMCODE);
			purchaseOrder.setCreateUser(user);
			purchaseOrder.setCreateDate(new Date());
			// DicType dt = (DicType) poService.get(DicType.class, "sysCode",
			// "storage").get(0);

			purchaseOrder.setState(SystemConstants.DIC_STATE_NEW);
			purchaseOrder.setStateName(SystemConstants.DIC_STATE_NEW_NAME);

			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(purchaseOrder.getState());
//			showPurchaseOrderItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
//					id);
			showPurchaseOrderApplyList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
					id);

		}
		putObjToContext("hiddenId", purchaseOrder.getId());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/purchase/order/editPurchaseOrder.jsp");
	}

	/**
	 * 复制页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toCopyPurchaseOrder")
	public String toCopyPurchaseOrder() throws Exception {
		String id = getParameterFromRequest("id");
		purchaseOrder = poService.getPurchaseOrder(id);
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		this.purchaseOrder.setId(SystemCode.DEFAULT_SYSTEMCODE);
		purchaseOrder.setCreateUser(user);
		purchaseOrder.setCreateDate(new Date());
		purchaseOrder.setState(SystemConstants.DIC_STATE_NEW);
		purchaseOrder.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		toState(purchaseOrder.getState());
//		showPurchaseOrderItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		showPurchaseOrderApplyList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		toSetStateCopy();
		putObjToContext("hiddenId", id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		return dispatcher("/WEB-INF/page/purchase/order/editPurchaseOrder.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewPurchaseOrder")
	public String toViewPurchaseOrder() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			purchaseOrder = poService.getPurchaseOrder(id);
//			showPurchaseOrderItemList(SystemConstants.PAGE_HANDLE_METHOD_VIEW,
//					id);
			putObjToContext("purchaseOrderId", id);
		}

		return dispatcher("/WEB-INF/page/purchase/order/editPurchaseOrder.jsp");
	}


	/**
		 *	展示物料列表明细
	     * @Title: showPurchaseOrderItemListJson  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月9日
	     * @throws
	 */
	@Action(value = "showPurchaseOrderItemListJson")
	public void showPurchaseOrderItemListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String,Object> result = poService.showPurchaseOrderItemListJson(id, start, length, query, col,
					sort);
			List<PurchaseOrderItem> list = (List<PurchaseOrderItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("storage-spec", "");
			map.put("price", "");
			map.put("fee", "");
			map.put("num", "");
			map.put("unit", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseOrderItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 申请单明细列表
	 * 
	 * @return
	 * @throws Exception
	 */
	public void showPurchaseOrderApplyList(String handlemethod, String poId)
			throws Exception {

		String exttype = "";
		String extcol = "";
		// String editflag = "false";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_MODIFY)) {
			// editflag = "true";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "true",
				"true", "", "", "", "", "" });
		map.put("purchaseApply-id", new String[] { "", "string", "", "采购申请单",
				"120", "true", "false", "", "", "", "", "" });
		map.put("purchaseApply-note", new String[] { "", "string", "", "描述",
				"250", "true", "false", "", "", "", "", "" });
		map.put("purchaseApply-createUser-name", new String[] { "", "string",
				"", "申请人", "120", "true", "false", "", "", "", "", "" });
		map.put("purchaseApply-stateName", new String[] { "", "string", "",
				"状态", "120", "true", "false", "", "", "", "", "" });
		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type1", exttype);
		putObjToContext("col1", extcol);
		putObjToContext(
				"path1",
				ServletActionContext.getRequest().getContextPath()
						+ "/purchase/order/showPurchaseOrderApplyListJson.action?purchaseOrderId="
						+ poId);
		if (poId != null)
			putObjToContext("purchaseOrderId", poId);
		putObjToContext("handlemethod", handlemethod);

	}

	@Action(value = "showPurchaseOrderApplyListJson")
	public void showPurchaseOrderApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchaseOrderId = getParameterFromRequest("purchaseOrderId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<PurchaseOrderApply> list = null;
		long totalCount = 0;
		if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
			Map<String, Object> controlMap = paService
					.showPurchaseOrderApplyList(startNum, limitNum, dir, sort,
							getContextPath(), purchaseOrderId);
			totalCount = Long
					.parseLong(controlMap.get("totalCount").toString());
			list = (List<PurchaseOrderApply>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("purchaseApply-id", "");
		map.put("purchaseApply-note", "note");
		map.put("purchaseApply-createUser-name", "");
		map.put("purchaseApply-stateName", "");

		if (list != null)
			new SendData().sendDateJson(map, list, totalCount,
					ServletActionContext.getResponse());
	}

	/**
	 * 新增采购申请。
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "savePurchaseOrderItem")
	public String savePurchaseOrderItem() throws Exception {
		String data = getParameterFromRequest("data");
		String purchaseOrderId = getParameterFromRequest("purchaseOrderId");
		PurchaseOrder po = commonService.get(PurchaseOrder.class,
				purchaseOrderId);

		if (data != null && !data.equals(""))
			poService.savePurchaseOrderItem(data, po);

		return redirect("/purchase/order/toEditPurchaseOrder.action?id="
				+ purchaseOrder.getId());
	}

	/**
	 * 新增采购申请。
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String mainData = getParameterFromRequest("main");
		String changeLog = getParameterFromRequest("changeLog");
		String goodsJson = getParameterFromRequest("goodsJson");
		String goodsChangeLogItem = getParameterFromRequest("goodsChangeLogItem");
		String instrumentJson = getParameterFromRequest("instrumentJson");
		String instrumentChangeLogItem = getParameterFromRequest("instrumentChangeLogItem");
		String serviceJson = getParameterFromRequest("serviceJson");
		String serviceChangeLogItem = getParameterFromRequest("serviceChangeLogItem");
		Map<String, String> dataMap = new HashMap<String,String>();
		dataMap.put("goodsJson", goodsJson);
		dataMap.put("goodsChangeLogItem", goodsChangeLogItem);
		dataMap.put("instrumentJson", instrumentJson);
		dataMap.put("instrumentChangeLogItem", instrumentChangeLogItem);
		dataMap.put("serviceJson", serviceJson);
		dataMap.put("serviceChangeLogItem", serviceChangeLogItem);
		
		Map<String, Object> map = new HashMap<String,Object>();
		try {
			String id = poService.save(mainData, changeLog, dataMap);
			map.put("success", true);
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPurchaseApplyItemListJson")
	public void showPurchaseApplyItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchaseApplyInStr = getParameterFromRequest("purchaseApplyInStr");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		Map<String, Object> controlMap = poService.showPurchaseApplyItemList(
				purchaseApplyInStr, startNum, limitNum, dir, sort);
		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<PurchaseApplyItem> list = (List<PurchaseApplyItem>) controlMap
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("storage-searchCode", "");
		map.put("storage-spec", "");
		map.put("storage-breed", "");

		map.put("storage-supplierName", "");

		map.put("purchaseApply-id", "");
		map.put("num", "");
		map.put("unit", "");

		if (list != null)
			new SendData().sendDateJson(map, list, totalCount,
					ServletActionContext.getResponse());
	}

	/**
	 * 删除明细
	 */
	@Action(value = "delPurchaseOrderItem")
	public void delPurchaseOrderItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids[]");
		try {
			this.poService.delPurchaseOrderItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "batchStorageOrderItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void batchStorageOrderItem() throws Exception {
		poService.batchStorageOrderItem();
	}

	@Action(value = "showProducerList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSupplierList() throws Exception {
		String purchaseApplyInStr = getParameterFromRequest("purchaseApplyInStr");
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "供应商编码", "120", "true",
				"", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "供应商名称", "200",
				"true", "", "", "", "", "", "" });
		map.put("way", new String[] { "", "string", "", "行业", "120", "true",
				"", "", "", "", "", "" });
		map.put("entKind-name", new String[] { "", "string", "", "企业性质", "150",
				"true", "", "", "", "", "", "" });
		map.put("address1", new String[] { "", "string", "", "国家", "100",
				"true", "", "", "", "", "", "" });
		map.put("linkMan", new String[] { "", "string", "", "联系人", "100",
				"true", "", "", "", "", "", "" });
		map.put("linkTel", new String[] { "", "string", "", "联系电话", "100",
				"true", "", "", "", "", "", "" });
		map.put("fax", new String[] { "", "string", "", "传真", "100", "true",
				"", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/purchase/order/showProducerListJson.action?purchaseApplyInStr="
						+ purchaseApplyInStr);
		return dispatcher("/WEB-INF/page/common/showSupplierList.jsp");
	}

	@Action(value = "showProducerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSupplierListJson() throws Exception {
		throw new RuntimeException("供应商已经删除请修改页面显示！");
	}

	/**
	 * 申请列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "purchaseOrderSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String purchaseOrderSelect() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		// 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		// id type format header width sort hide align xtype editable renderer
		// editor
		// 列的id 当"common" 时该列为扩展列 type类型 format格式 header列头 width列宽 sort是否排序
		// hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器
		map.put("id", new String[] { "", "string", "", "订单编号", "150", "true",
				"", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "150", "true",
				"", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "订单类型", "100",
				"true", "", "", "", "", "", "" });
		map.put("createUser-name", new String[] { "", "string", "", "申请人",
				"100", "true", "", "", "", "", "", "" });
		map.put("createDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
				"申请日期", "100", "true", "", "", "", "", "formatDate", "" });
		map.put("reachDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
				"到货日期", "100", "true", "", "", "", "", "formatDate", "" });
		map.put("confirmUser-name", new String[] { "", "string", "", "批准人",
				"100", "true", "", "", "", "", "", "" });
		map.put("supplier-id", new String[] { "", "string", "", "供应商id", "100",
				"true", "", "", "", "", "", "" });
		map.put("supplier-name", new String[] { "", "string", "", "供应商名称",
				"100", "true", "", "", "", "", "", "" });
		map.put("supplier-linkMan", new String[] { "", "string", "", "供应商联系人",
				"100", "true", "", "", "", "", "", "" });
		map.put("supplier-linkTel", new String[] { "", "string", "", "供应商联系电话",
				"100", "true", "", "", "", "", "", "" });
		map.put("supplier-email", new String[] { "", "string", "", "Email",
				"100", "true", "", "", "", "", "", "" });
		map.put("supplier-accountBank", new String[] { "", "string", "", "开户行",
				"100", "true", "", "", "", "", "", "" });
		map.put("supplier-account", new String[] { "", "string", "", "帐号",
				"100", "true", "", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 生成toolbar

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/purchase/order/purchaseOrderSelectJson.action?id="
				+ getParameterFromRequest("id"));
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/purchase/order/purchaseOrderSelect.jsp");
	}

	@Action(value = "purchaseOrderSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPurchaseOrderListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式supplier-id {"id": "20140722"}
		String sort = getParameterFromRequest("sort");
		String data = null;
		if (getParameterFromRequest("id") != null) {
			String oid = getParameterFromRequest("id");
			data = "{\"supplier.id\":\"" + oid + "\",\"state\":\"1\"}";
		} else {
			data = getParameterFromRequest("data");
		}
		// System.out.print("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqq" + data);
		// if(data==null||data.equals(""))
		// data = "{\"state\":\""+SystemConstants.DIC_STATE_YES+"\"}";
		Map<String, Object> controlMap = poService
				.findPurchaseOrderList(
						startNum,
						limitNum,
						dir,
						sort,
						getContextPath(),
						data,
						"",
						(String) this
								.getObjFromSession(SystemConstants.USER_SESSION_SCOPE_KEY));
		// Map<String, Object> controlMap = userService.findUser(startNum,
		// limitNum, dir, sort, getContextPath(), "", "", "");
		long totalCount = Long.parseLong(controlMap.get("totalCount")
				.toString());
		List<PurchaseOrder> list = (List<PurchaseOrder>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-name", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("reachDate", "yyyy-MM-dd");
		map.put("supplier-id", "");
		map.put("supplier-name", "");
		map.put("supplier-linkMan", "");
		map.put("supplier-linkTel", "");
		map.put("supplier-email", "");
		map.put("supplier-accountBank", "");
		map.put("supplier-account", "");
		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}

	@Action(value = "showPurchaseOrderEquipmentItemListJson")
	public void showPurchaseOrderEquipmentItemListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String,Object> result = poService.showPurchaseOrderEquipmentItemListJson(id,
					start, length, query, col, sort);
			List<PurchaseOrderEquipmentItem> list = (List<PurchaseOrderEquipmentItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("num", "");
			map.put("price", "");
			map.put("fee", "");
			map.put("equipment-id", "");
			map.put("equipment-name", "");
			map.put("equipment-searchCode", "");
			map.put("equipment-spec", "");
			map.put("unit", "unit");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseOrderEquipmentItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*@Action(value = "showPurchaseOrderEquipmentItemListJosn", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPurchaseOrderEquipmentItemListJosn() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String id = super.getRequest().getParameter("id");

		Map<String, Object> result = new HashMap<String, Object>();

		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (id != null && id.length() > 0) {
			mapForQuery.put("purchaseOrder.id", id);
			result = this.poService.findPurchaseOrderEquipmentItem(mapForQuery,
					startNum, limitNum, dir, sort);
		}
		long totalCount = Long.parseLong(result.get("total").toString());

		List<PurchaseOrderEquipmentItem> list = (List<PurchaseOrderEquipmentItem>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("note", "note");
		map.put("num", "num");
		map.put("price", "price");
		map.put("fee", "fee");
		map.put("equipment-id", "equipment-id");
		map.put("equipment-name", "");
		map.put("equipment-searchCode", "");
		map.put("equipment-spec", "");
		map.put("unit", "unit");
		map.put("supplier-id", "");
		map.put("supplier-name", "");
		map.put("purchaseApply-id", "purchaseApply-id");
		map.put("purchaseApplyEquipmentItem-id",
				"purchaseApplyEquipmentItem-id");
		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}*/

	/**
	 * 保存设备采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "savePurchaseOrderEquipmentItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePurchaseOrderEquipmentItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String itemDataJson = super.getRequest().getParameter("itemDataJson");
		String id = super.getRequest().getParameter("id");
		try {
			this.purchaseOrder.setId(id);
			this.poService.savePurchaseOrderEquipmentItem(purchaseOrder,
					itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPurchaseOrderEquipmentItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delPurchaseOrderEquipmentItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String ids = super.getRequest().getParameter("ids");
		try {
			this.poService.delPurchaseOrderEquipmentItemById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPurchaseOrderServiceItemListJson")
	public void showPurchaseOrderServiceItemListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String,Object> result = poService.showPurchaseOrderServiceItemListJson(id,
					start, length, query, col, sort);
			List<PurchaseOrderServiceItem> list = (List<PurchaseOrderServiceItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("serviceCode", "");
			map.put("serviceName", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			map.put("num", "num");
			map.put("price", "price");
			map.put("fee", "fee");
			map.put("unit", "unit");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseOrderServiceItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*@Action(value = "showPurchaseOrderServiceItemListJosn", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPurchaseOrderServiceItemListJosn() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String id = super.getRequest().getParameter("id");

		Map<String, Object> result = new HashMap<String, Object>();

		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (id != null && id.length() > 0) {
			mapForQuery.put("purchaseOrder.id", id);
			result = this.poService.findPurchaseOrderServiceItem(mapForQuery,
					startNum, limitNum, dir, sort);
		}

		long totalCount = Long.parseLong(result.get("total").toString());

		List<PurchaseOrderServiceItem> list = (List<PurchaseOrderServiceItem>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("serviceCode", "serviceCode");
		map.put("serviceName", "");
		map.put("supplier-id", "");
		map.put("supplier-name", "");
		map.put("num", "num");
		map.put("price", "price");
		map.put("fee", "fee");
		map.put("unit", "unit");
		map.put("purchaseApply-id", "purchaseApply-id");
		map.put("purchaseApplyServiceItem-id", "purchaseApplyServiceItem-id");
		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}*/

	/**
	 * 保存服务采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "savePurchaseOrderServiceItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePurchaseOrderServiceItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String itemDataJson = super.getRequest().getParameter("itemDataJson");
		String id = super.getRequest().getParameter("id");
		try {
			this.purchaseOrder.setId(id);
			this.poService.savePurchaseOrderServiceItem(purchaseOrder,
					itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除服务采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPurchaseOrderServiceItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delPurchaseOrderServiceItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String ids = super.getRequest().getParameter("ids");
		try {
			this.poService.delPurchaseOrderServiceItemById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据设备申请生成订单明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "createPurchaseOrderEquipmentItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createPurchaseOrderEquipmentItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = super.getRequest().getParameter("id");
		try {
			List<Map<String, String>> result = this.poService
					.createPurchaseOrderEquipmentItem(id);
			map.put("result", result);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据服务申请生成订单明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "createPurchaseOrderServiceItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createPurchaseOrderServiceItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = super.getRequest().getParameter("id");
		try {
			List<Map<String, String>> result = this.poService
					.createPurchaseOrderServiceItem(id);
			map.put("result", result);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

}
