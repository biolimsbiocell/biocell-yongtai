package com.biolims.purchase.payable.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.purchase.payable.model.PurchasePayable;
import com.biolims.purchase.payable.service.PurchasePayableService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/purchase/payable/purchasePayable")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PurchasePayableAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "307";
	@Autowired
	private PurchasePayableService purchasePayableService;
	private PurchasePayable purchasePayable = new PurchasePayable();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showPurchasePayableList")
	public String showPurchasePayableList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/purchase/payable/purchasePayable.jsp");
	}

	@Action(value = "showPurchasePayableListJson")
	public void showPurchasePayableListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = purchasePayableService.findPurchasePayableList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PurchasePayable> list = (List<PurchasePayable>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("contractId", "");
		map.put("contractName", "");
		map.put("totalPrice", "");
		map.put("paid", "");
		map.put("unpaid", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "purchasePayableSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPurchasePayableList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/purchase/payable/purchasePayableDialog.jsp");
	}

	@Action(value = "showDialogPurchasePayableListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPurchasePayableListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = purchasePayableService.findPurchasePayableList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PurchasePayable> list = (List<PurchasePayable>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("contractId", "");
		map.put("contractName", "");
		map.put("totalPrice", "");
		map.put("paid", "");
		map.put("unpaid", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editPurchasePayable")
	public String editPurchasePayable() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			purchasePayable = purchasePayableService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "purchasePayable");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			purchasePayable.setCreateUser(user);
			purchasePayable.setCreateDate(new Date());
			purchasePayable.setState(SystemConstants.DIC_STATE_NEW);
			purchasePayable.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/purchase/payable/purchasePayableEdit.jsp");
	}

	@Action(value = "copyPurchasePayable")
	public String copyPurchasePayable() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		purchasePayable = purchasePayableService.get(id);
		purchasePayable.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/purchase/payable/purchasePayableEdit.jsp");
	}


	@Action(value = "save")
	public void save() throws Exception {
		String dataJson=getParameterFromRequest("record");
		Map<String,Object> map =new HashMap<String, Object>();
		try {
			purchasePayableService.save(dataJson);
			map.put("success", "true");
		} catch (Exception e) {
			map.put("success", "false");
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewPurchasePayable")
	public String toViewPurchasePayable() throws Exception {
		String id = getParameterFromRequest("id");
		purchasePayable = purchasePayableService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/purchase/payable/purchasePayableEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchasePayableService getPurchasePayableService() {
		return purchasePayableService;
	}

	public void setPurchasePayableService(PurchasePayableService purchasePayableService) {
		this.purchasePayableService = purchasePayableService;
	}

	public PurchasePayable getPurchasePayable() {
		return purchasePayable;
	}

	public void setPurchasePayable(PurchasePayable purchasePayable) {
		this.purchasePayable = purchasePayable;
	}


}
