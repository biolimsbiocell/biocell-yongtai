package com.biolims.purchase.payable.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 应付账款
 * @author lims-platform
 * @date 2017-11-28 17:06:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_PURCHASE_PAYABLE")
@SuppressWarnings("serial")
public class PurchasePayable extends EntityDao<PurchasePayable> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/**创建时间*/
	private Date createDate;
	/**采购合同编码*/
	private String contractId;
	/**采购合同描述*/
	private String contractName;
	/**总价*/
	private Double totalPrice;
	/**已付*/
	private Double paid;
	/**未付*/
	private Double unpaid;
	/**状态*/
	private String state;
	/**状态名称*/
	private String stateName;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  采购合同编码
	 */
	@Column(name ="CONTRACT_ID", length = 50)
	public String getContractId(){
		return this.contractId;
	}
	/**
	 *方法: 设置String
	 *@param: String  采购合同编码
	 */
	public void setContractId(String contractId){
		this.contractId = contractId;
	}
	/**
	 *方法: 取得String
	 *@return: String  采购合同描述
	 */
	@Column(name ="CONTRACT_NAME", length = 50)
	public String getContractName(){
		return this.contractName;
	}
	/**
	 *方法: 设置String
	 *@param: String  采购合同描述
	 */
	public void setContractName(String contractName){
		this.contractName = contractName;
	}
	/**
	 *方法: 取得Float
	 *@return: Float  总价
	 */
	@Column(name ="TOTAL_PRICE", length = 50)
	public Double getTotalPrice(){
		return this.totalPrice;
	}
	/**
	 *方法: 设置Float
	 *@param: Float  总价
	 */
	public void setTotalPrice(Double totalPrice){
		this.totalPrice = totalPrice;
	}
	/**
	 *方法: 取得Float
	 *@return: Float  已付
	 */
	@Column(name ="PAID", length = 50)
	public Double getPaid(){
		return this.paid;
	}
	/**
	 *方法: 设置Float
	 *@param: Float  已付
	 */
	public void setPaid(Double paid){
		this.paid = paid;
	}
	/**
	 *方法: 取得Float
	 *@return: Float  未付
	 */
	@Column(name ="UNPAID", length = 50)
	public Double getUnpaid(){
		return this.unpaid;
	}
	/**
	 *方法: 设置Float
	 *@param: Float  未付
	 */
	public void setUnpaid(Double unpaid){
		this.unpaid = unpaid;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态名称
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态名称
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 100)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
}