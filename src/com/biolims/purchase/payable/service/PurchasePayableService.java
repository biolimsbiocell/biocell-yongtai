package com.biolims.purchase.payable.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.purchase.contract.model.PurchaseContractItem;
import com.biolims.purchase.payable.dao.PurchasePayableDao;
import com.biolims.purchase.payable.model.PurchasePayable;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PurchasePayableService {
	@Resource
	private PurchasePayableDao purchasePayableDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findPurchasePayableList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return purchasePayableDao.selectPurchasePayableList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PurchasePayable i) throws Exception {

		purchasePayableDao.saveOrUpdate(i);

	}
	public PurchasePayable get(String id) {
		PurchasePayable purchasePayable = commonDAO.get(PurchasePayable.class, id);
		return purchasePayable;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PurchasePayable sc, Map jsonMap) throws Exception {
		if (sc != null) {
			purchasePayableDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	public void save(String dataJson) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		for (Map<String, Object> map : list) {
		PurchasePayable pp=new PurchasePayable();
		pp = (PurchasePayable) purchasePayableDao.Map2Bean(map, pp);
		purchasePayableDao.saveOrUpdate(pp);
		}
	}
}
