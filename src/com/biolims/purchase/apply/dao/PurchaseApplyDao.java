package com.biolims.purchase.apply.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.purchase.model.PurchaseApplyEquipmentItem;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.purchase.model.PurchaseApplyServiceItem;
import com.biolims.sample.model.SampleOrder;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class PurchaseApplyDao extends BaseHibernateDao {

	public Map<String, Object> selectPurchaseApplyList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseApply where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseApply> list = new ArrayList<PurchaseApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by createDate DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectPurchaseEquipmentApplyItem(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseApplyEquipmentItem where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseApplyEquipmentItem> list = new ArrayList<PurchaseApplyEquipmentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public void delPurchaseEquipmentApplyItem(Map<String, String> mapForQuery)
			throws Exception {
		String hql = "delete from PurchaseApplyEquipmentItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public Map<String, Object> selectPurchaseServiceApplyItem(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from PurchaseApplyServiceItem where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseApplyServiceItem> list = new ArrayList<PurchaseApplyServiceItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public void delPurchaseServiceApplyItem(Map<String, String> mapForQuery)
			throws Exception {
		String hql = "delete from PurchaseApplyServiceItem where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		this.getSession().createQuery(hql + key).executeUpdate();
	}

	public Map<String, Object> selectPurchaseApplyList(String type, int start,
			int limit, String dir, String sort) {

		String hql = "from PurchaseApply pa where pa.state = '"
				+ SystemConstants.DIC_STATE_YES_ID + "'";
		if (!type.equals(""))
			hql += " and pa.type.sysCode ='" + type + "'";
		List<PurchaseApply> dataList = new ArrayList<PurchaseApply>();

		Long count = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		if (count > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0)
				hql = hql + " order by " + sort + " " + dir;

			dataList = this.getSession().createQuery(hql).setFirstResult(start)
					.setMaxResults(limit).list();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("list", dataList);
		return map;
	}

	public Map<String, Object> selectPurchaseApplyItem(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String hql = " from PurchaseApplyItem where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = key + map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql + key).uniqueResult();
		List<PurchaseApplyItem> list = new ArrayList<PurchaseApplyItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public List<Object> selectPurchaseApplyItem(Map<String, String> mapForQuery)
			throws Exception {
		String key = "";
		String hql = "select t.storage.id,t.storage.name,t.storage.currencyType.name from PurchaseApplyItem t where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);

		return this.getSession().createQuery(hql + key).list();
	}

	public List<Object> selectPurchaseEquipmentApplyItem(
			Map<String, String> mapForQuery) throws Exception {
		String key = "";
		String hql = "select t.equipment.id,t.equipment.name,t.equipment.currencyType.name from PurchaseApplyEquipmentItem t where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);

		return this.getSession().createQuery(hql + key).list();
	}

	public List<Object> selectPurchaseServiceApplyItem(
			Map<String, String> mapForQuery) throws Exception {
		String key = "";
		String hql = "select t.serviceCode,t.serviceName,t.unit from PurchaseApplyServiceItem t where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);

		return this.getSession().createQuery(hql + key).list();
	}

	public List<PurchaseApplyItem> findPurchaseApplyItemList(String id) {
		String hql = "";
		List<PurchaseApplyItem> listItem = null;
		hql = "from PurchaseApplyItem where 1=1 and purchaseApply.id='" + id
				+ "'";
		listItem = this.getSession().createQuery(hql).list();
		return listItem;
	}

	public List<PurchaseApplyEquipmentItem> findPurchaseApplyEItemList(String id) {
		String hql = "";
		List<PurchaseApplyEquipmentItem> listEItem = null;
		hql = "from PurchaseApplyEquipmentItem where 1=1 and purchaseApply.id='"
				+ id + "'";
		listEItem = this.getSession().createQuery(hql).list();
		return listEItem;
	}

	public List<PurchaseApplyServiceItem> findPurchaseApplySItemList(String id) {
		String hql = "";
		List<PurchaseApplyServiceItem> listSItem = null;
		hql = "from PurchaseApplyServiceItem where 1=1 and purchaseApply.id='"
				+ id + "'";
		listSItem = this.getSession().createQuery(hql).list();
		return listSItem;
	}

	public Map<String, Object> showPurchaseApplyListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseApply where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseApply where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseApply> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showInstrumentItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseApplyEquipmentItem where 1=1 and purchaseApply.id = '"+id+"'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseApplyEquipmentItem where 1=1 and purchaseApply.id = '"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseApplyEquipmentItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showGoodsListJson(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseApplyItem where 1=1 and purchaseApply.id = '"+id+"'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseApplyItem where 1=1 and purchaseApply.id = '"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseApplyItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showServiceItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseApplyServiceItem where 1=1 and purchaseApply.id = '"+id+"'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseApplyServiceItem where 1=1 and purchaseApply.id = '"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseApplyServiceItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showPurchaseApplyDialogListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseApply where 1=1 and state = '1'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseApply where 1=1 and state = '1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseApply> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<PurchaseApplyItem> findGoodsItemList(String id) {
		String hql = "from PurchaseApplyItem where 1=1 and purchaseApply.id = '"+id+"'";
		List<PurchaseApplyItem> list = new ArrayList<PurchaseApplyItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<PurchaseApplyEquipmentItem> findInstrumentItemList(String id) {
		String hql = "from PurchaseApplyEquipmentItem where 1=1 and purchaseApply.id = '"+id+"'";
		List<PurchaseApplyEquipmentItem> list = new ArrayList<PurchaseApplyEquipmentItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<PurchaseApplyServiceItem> findServiceItemList(String id) {
		String hql = "from PurchaseApplyServiceItem where 1=1 and purchaseApply.id = '"+id+"'";
		List<PurchaseApplyServiceItem> list = new ArrayList<PurchaseApplyServiceItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<PurchaseApplyItem> findGoodsItemListByType(String id, String type) {
		String hql = "from PurchaseApplyItem where 1=1 and purchaseApply.id = '"+id+"'";
		String key = "";
		if(type!=null) {
			key = " and purchaseApply.type.id = '"+type+"'";
		}
		List<PurchaseApplyItem> list = new ArrayList<PurchaseApplyItem>();
		list = this.getSession().createQuery(hql+key).list();
		return list;
	}

	public List<PurchaseApplyEquipmentItem> findInstrumentItemListByType(String id, String type) {
		String hql = "from PurchaseApplyEquipmentItem where 1=1 and purchaseApply.id = '"+id+"'";
		String key = "";
		if(type!=null) {
			key = " and purchaseApply.type.id = '"+type+"'";
		}
		List<PurchaseApplyEquipmentItem> list = new ArrayList<PurchaseApplyEquipmentItem>();
		list = this.getSession().createQuery(hql+key).list();
		return list;
	}

	public List<PurchaseApplyServiceItem> findServiceItemListByType(String id, String type) {
		String hql = "from PurchaseApplyServiceItem where 1=1 and purchaseApply.id = '"+id+"'";
		String key = "";
		if(type!=null) {
			key = " and purchaseApply.type.id = '"+type+"'";
		}
		List<PurchaseApplyServiceItem> list = new ArrayList<PurchaseApplyServiceItem>();
		list = this.getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showPurchaseApplyDialogListByTypeJson(String type, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurchaseApply where 1=1 and state = '1'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		key += " and type.id = '"+type+"'";
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from PurchaseApply where 1=1 and state = '1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PurchaseApply> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

}
