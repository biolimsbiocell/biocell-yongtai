/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：PurchaseApplyService
 * 类描述：采购申请Service
 * 创建人：阿川
 * 创建时间：2011-12-07
 * 修改人：倪毅
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.apply.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.purchase.apply.dao.PurchaseApplyDao;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.common.service.PurchaseSystemCodeService;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.purchase.model.PurchaseApplyEquipmentItem;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.purchase.model.PurchaseApplyServiceItem;
import com.biolims.purchase.model.PurchaseOrder;
import com.biolims.purchase.model.PurchaseOrderApply;
import com.biolims.purchase.model.PurchaseOrderEquipmentItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.purchase.model.PurchaseOrderServiceItem;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
public class PurchaseApplyService {

	@Autowired
	private CommonDAO commonDAO;

	@Autowired
	private PurchaseApplyDao purchaseApplyDao;

	@Resource
	private PurchaseSystemCodeService purchaseSystemCodeService;
	
	@Resource
	private CodingRuleService codingRuleService;
	
	/**
	 * 检索list,采用map方式传递检索参数
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> showPurchaseApplyListJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return purchaseApplyDao.showPurchaseApplyListJson(start, length, query, col, sort);
	}

	/**
	 * 检索list,采用map方式传递检索参数
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> purchaseApplyListSelect(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId, String type) throws Exception {

		Map<String, Object> controlMap = purchaseApplyDao.selectPurchaseApplyList(type, startNum, limitNum, dir, sort);
		return controlMap;
	}

	/**
	 * 根据id获得申请单对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PurchaseApply getPurchaseApply(String id) throws Exception {
		return purchaseApplyDao.get(PurchaseApply.class, id);
	}

	/**
	 * 检索申请单明细
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchaseApplyId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showPurchaseApplyItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchaseApplyId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchaseApplyId != null && !purchaseApplyId.equals("")) {
			mapForQuery.put("purchaseApply.id", purchaseApplyId);
			mapForCondition.put("purchaseApply.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchaseApplyItem.class, mapForQuery, mapForCondition);

		List<PurchaseApplyItem> list = (List<PurchaseApplyItem>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 检索明细
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchaseApplyId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showPurchaseOrderApplyList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String purchaseOrderId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
			mapForQuery.put("purchaseOrder.id", purchaseOrderId);
			mapForCondition.put("purchaseOrder.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				PurchaseOrderApply.class, mapForQuery, mapForCondition);

		List<PurchaseOrderApply> list = (List<PurchaseOrderApply>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseApplyItem(String[] ids) {
		for (String id : ids) {
			PurchaseApplyItem item = commonDAO.get(PurchaseApplyItem.class, id);
			commonDAO.delete(item);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("PurchaseApply");
				li.setFileId(item.getId());
				li.setModifyContent("物资采购明细:"+"名称:"+item.getStorage().getName()+",JDE编码:"+item.getStorage().getJdeCode()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseOrderApply(String id, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			PurchaseOrderApply em = new PurchaseOrderApply();

			// 将map信息读入实体类
			em = (PurchaseOrderApply) purchaseApplyDao.Map2Bean(map, em);
			PurchaseOrder ep = new PurchaseOrder();
			ep.setId(id);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setPurchaseOrder(ep);
			purchaseApplyDao.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseOrderApply(String id) {
		PurchaseOrderApply ppi = new PurchaseOrderApply();
		ppi.setId(id);
		purchaseApplyDao.delete(ppi);
	}

	/**
	 * 采购申请--自动生成采购订单
	 * @param purchaseApplyId 采购申请号
	 * @param UserId 用户ID
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String savePurchaseOrder(String purchaseApplyId, User user) throws Exception {
		PurchaseApply purchaseApply = this.purchaseApplyDao.get(PurchaseApply.class, purchaseApplyId);
		String sysCode = purchaseApply.getType().getSysCode();
		//1.新建采购订单
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		String code = purchaseSystemCodeService.getSystemCode(SystemCode.PURCHASE_ORDER_NAME,
				SystemCode.PURCHASE_ORDER_CODE, null, null);
		purchaseOrder.setId(code);
		purchaseOrder.setType(purchaseApply.getType());
		purchaseOrder.setIsSysSend(SystemConstants.DIC_STATE_YES_ID);
		purchaseOrder.setDepartment(purchaseApply.getDepartment());
		purchaseOrder.setCreateUser(user);
		purchaseOrder.setCreateDate(new Date());
		purchaseOrder.setState(SystemConstants.DIC_STATE_NEW);
		purchaseOrder.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
		purchaseApplyDao.saveOrUpdate(purchaseOrder);
		//2.采购申请
		PurchaseApply pa = purchaseApplyDao.get(PurchaseApply.class, purchaseApplyId);
		PurchaseOrderApply poa = new PurchaseOrderApply();
		poa.setPurchaseApply(pa);
		poa.setPurchaseOrder(purchaseOrder);
		purchaseApplyDao.saveOrUpdate(poa);
		//3.申请明细
		if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_STORAGE_SYSCODE.equals(sysCode)) {
			List<PurchaseOrderItem> poiList = new ArrayList<PurchaseOrderItem>();
			List<PurchaseApplyItem> paiList = purchaseApplyDao.findByProperty(PurchaseApplyItem.class,
					"purchaseApply.id", purchaseApplyId);
			for (PurchaseApplyItem em : paiList) {
				PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
				purchaseOrderItem.setStorage(em.getStorage());
				purchaseOrderItem.setUnit(em.getUnit());
				purchaseOrderItem.setNum(em.getNum());
				purchaseOrderItem.setCostCenter(pa.getCostCenter());
				purchaseOrderItem.setPurchaseOrder(purchaseOrder);
				purchaseOrderItem.setPurchaseApply(pa);
				purchaseOrderItem.setPurchaseApplyItem(em);
				poiList.add(purchaseOrderItem);
			}
			purchaseApplyDao.saveOrUpdateAll(poiList);
		}
		if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_EQUIPMENT_SYSCODE.equals(sysCode)) {
			List<PurchaseOrderEquipmentItem> poeiList = new ArrayList<PurchaseOrderEquipmentItem>();
			List<PurchaseApplyEquipmentItem> paeiList = purchaseApplyDao.findByProperty(
					PurchaseApplyEquipmentItem.class, "purchaseApply.id", purchaseApplyId);
			for (PurchaseApplyEquipmentItem em : paeiList) {
				PurchaseOrderEquipmentItem poei = new PurchaseOrderEquipmentItem();
				poei.setEquipment(em.getEquipment());
				poei.setNum(Double.valueOf(em.getNum()));
					poei.setUnit(em.getUnit());
				poei.setNote(em.getNote());
				poei.setPurchaseOrder(purchaseOrder);
				poei.setPurchaseApply(pa);
				poei.setPurchaseApplyEquipmentItem(em);
				poeiList.add(poei);
			}
			purchaseApplyDao.saveOrUpdateAll(poeiList);
		}
		if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_SERVICE_SYSCODE.equals(sysCode)) {
			List<PurchaseOrderServiceItem> posiList = new ArrayList<PurchaseOrderServiceItem>();
			List<PurchaseApplyServiceItem> paiList = purchaseApplyDao.findByProperty(PurchaseApplyServiceItem.class,
					"purchaseApply.id", purchaseApplyId);
			for (PurchaseApplyServiceItem em : paiList) {
				PurchaseOrderServiceItem posi = new PurchaseOrderServiceItem();
				posi.setServiceCode(em.getServiceCode());
				posi.setServiceName(em.getServiceName());
				if (em.getUnit() != null) {
					posi.setUnit(em.getUnit());
				}
				posi.setNum(Double.valueOf(em.getNum()));
				posi.setPurchaseOrder(purchaseOrder);
				posi.setPurchaseApply(pa);
				posi.setPurchaseApplyServiceItem(em);
				posiList.add(posi);
			}
			purchaseApplyDao.saveOrUpdateAll(posiList);
		}
		return purchaseOrder.getId();
	}

	/**
	 * 采购库存申请明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseApplyItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseApplyDao.selectPurchaseApplyItem(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 采购设备申请明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseEquipmentApplyItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseApplyDao.selectPurchaseEquipmentApplyItem(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 根据ID 删除设备申请明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseEquipmentApplyItemById(String[] ids) throws Exception {
		for (String id : ids) {
			PurchaseApplyEquipmentItem item = commonDAO.get(PurchaseApplyEquipmentItem.class, id);
			commonDAO.delete(item);
		}
	}

	/**
	 * 采购服务申请明细列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseServiceApplyItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseApplyDao.selectPurchaseServiceApplyItem(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 根据ID 删除服务申请明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseServiceApplyItemById(String[] ids) throws Exception {
		for (String id : ids) {
			PurchaseApplyServiceItem item = commonDAO.get(PurchaseApplyServiceItem.class, id);
			commonDAO.delete(item);
		}
	}

	/**
		 *	大保存
	     * @Title: save  
	     * @Description: TODO  
	     * @param @param mainJson
	     * @param @param changeLog
	     * @param @param dataMap
	     * @param @return
	     * @param @throws Exception    
	     * @return String  
		 * @author 孙灵达  
	     * @date 2018年8月3日
	     * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String mainJson,String changeLog,Map<String ,String> dataMap)
			throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		mainJson = "["+mainJson+"]";
		List<Map<String,Object>> mainList = JsonUtils.toListByJsonArray(mainJson, List.class);
		PurchaseApply purchaseApply = new PurchaseApply();
		purchaseApply = (PurchaseApply) commonDAO.Map2Bean(mainList.get(0), purchaseApply);
		String log = "";
		if((purchaseApply.getId()!=null&&"".equals(purchaseApply.getId()))||purchaseApply.getId().equals("NEW")) {
			log="123";
			String modelName = "PurchaseApply";
			String markCode = "PURA";
			String autoID = codingRuleService.genTransID(modelName,
					markCode);
			purchaseApply.setId(autoID);
			purchaseApply.setCreateDate(new Date());
			purchaseApply.setCreateUser(u);
		}
		commonDAO.saveOrUpdate(purchaseApply);
		String id = purchaseApply.getId();
		
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			if(u!=null) {
				li.setUserId(u.getId());
			}
			li.setFileId(id);
			li.setClassName("PurchaseApply");
			li.setModifyContent(changeLog);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		
		//保存物资申请明细
		String goodsJson = (String) dataMap.get("goodsJson");
		String goodsChangeLogItem = dataMap.get("goodsChangeLogItem");
		if(goodsJson != null && !goodsJson.equals("[]") && !goodsJson.equals("")) {
			saveGoodsItemTable(purchaseApply,goodsJson,goodsChangeLogItem,log);
		}
		
		//保存设备明细
		String instrumentJson = (String) dataMap.get("instrumentJson");
		String instrumentChangeLogItem = dataMap.get("instrumentChangeLogItem");
		if(instrumentJson != null && !instrumentJson.equals("[]") && !instrumentJson.equals("")) {
			saveInstrumentItemTable(purchaseApply,instrumentJson,instrumentChangeLogItem,log);
		}
		
		//保存服务明细
		String serviceJson = (String) dataMap.get("serviceJson");
		String serviceChangeLogItem = dataMap.get("serviceChangeLogItem");
		if(serviceJson != null && !serviceJson.equals("[]") && !serviceJson.equals("")) {
			saveServiceItemTable(purchaseApply,serviceJson,serviceChangeLogItem,log);
		}
		return id;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveServiceItemTable(PurchaseApply purchaseApply, String serviceJson, String serviceChangeLogItem,String log) throws Exception {
		List<PurchaseApplyServiceItem> saveItems = new ArrayList<PurchaseApplyServiceItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(serviceJson, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			PurchaseApplyServiceItem scp = new PurchaseApplyServiceItem();
			scp = (PurchaseApplyServiceItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setPurchaseApply(purchaseApply);
			
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (serviceChangeLogItem != null && !"".equals(serviceChangeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(purchaseApply.getId());
			li.setClassName("PurchaseApply");
			li.setModifyContent(serviceChangeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveInstrumentItemTable(PurchaseApply purchaseApply, String instrumentJson,
			String instrumentChangeLogItem,String log) throws Exception {
		List<PurchaseApplyEquipmentItem> saveItems = new ArrayList<PurchaseApplyEquipmentItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(instrumentJson, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			PurchaseApplyEquipmentItem scp = new PurchaseApplyEquipmentItem();
			scp = (PurchaseApplyEquipmentItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setPurchaseApply(purchaseApply);
			
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (instrumentChangeLogItem != null && !"".equals(instrumentChangeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(purchaseApply.getId());
			li.setClassName("PurchaseApply");
			li.setModifyContent(instrumentChangeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveGoodsItemTable(PurchaseApply purchaseApply, String goodsJson, String goodsChangeLogItem,String log) throws Exception {
		List<PurchaseApplyItem> saveItems = new ArrayList<PurchaseApplyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(goodsJson, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			PurchaseApplyItem scp = new PurchaseApplyItem();
			scp = (PurchaseApplyItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setPurchaseApply(purchaseApply);
			
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (goodsChangeLogItem != null && !"".equals(goodsChangeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(purchaseApply.getId());
			li.setClassName("PurchaseApply");
			li.setModifyContent(goodsChangeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 保存采购申请单
	 * @param purchaseApply
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseApply(PurchaseApply purchaseApply) throws Exception {
		String id = purchaseApply.getId();
		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			String code = purchaseSystemCodeService.getPurchaseApplyCode(purchaseApply.getType().getId());
			purchaseApply.setId(code);
		}
		this.purchaseApplyDao.saveOrUpdate(purchaseApply);
	}

	/**
	 * 
	 * 保存库存申请明细
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseApplyItem(PurchaseApply purchaseApply, String jsonString) throws Exception {
		if (jsonString != null && jsonString.length() > 0) {
			// 将json读入map
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
			this.savePurchaseApplyStroage(purchaseApply, list);
		}
	}

	/**
	 * 保存设备申请明细
	 * @param equipment 
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseEquipmentApplyItem(PurchaseApply purchaseApply, String itemDataJson) throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			this.savePurchaseApplyEquipment(purchaseApply, list);
		}
	}

	/**
	 * 保存服务申请明细
	 * @param equipment 
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseServiceApplyItem(PurchaseApply purchaseApply, String itemDataJson) throws Exception {
		if (itemDataJson != null && itemDataJson.length() > 0) {
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			this.savePurchaseApplyService(purchaseApply, list);
		}
	}

	/**
	 * 自动创建采购申请
	 * @param mapList
	 * @throws Exception
	 * 注意：savePurchaseApply 中 自动生成申请单 ID
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String createPurchaseApply(List<Map<String, Object>> mapList, User user) throws Exception {
		String message = "";
		if (mapList != null && mapList.size() > 0) {
			PurchaseApply purchaseApply = new PurchaseApply();
			DicType type = new DicType();
			type.setId(SystemConstants.PURCHASE_APPLY_DIC_TYPE_STORAGE_AUTO_ID);
			purchaseApply.setType(type);
			purchaseApply.setIsSysSend(SystemConstants.DIC_STATE_YES_ID);
			purchaseApply.setCreateDate(new Date());
			purchaseApply.setCreateUser(user);
			purchaseApply.setState(SystemConstants.DIC_STATE_NEW);
			purchaseApply.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			this.savePurchaseApply(purchaseApply);
			message = "采购申请单号为" + purchaseApply.getId() + ";";
			// 库存
			this.savePurchaseApplyStroage(purchaseApply, mapList);
		}
		return message;
	}

	/*--------------------------------采购申请公用程序-------------------------------------*/

	/**
	 * 公用-保存库存明细
	 * @param mapList
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseApplyStroage(PurchaseApply purchaseApply, List<Map<String, Object>> list) throws Exception {
		List<PurchaseApplyItem> paiList = new ArrayList<PurchaseApplyItem>();
		for (Map<String, Object> map : list) {
			PurchaseApplyItem purchaseApplyItem = new PurchaseApplyItem();
			purchaseApplyItem = (PurchaseApplyItem) this.purchaseApplyDao.Map2Bean(map, purchaseApplyItem);
			if ("".equals(purchaseApplyItem.getId())) {
				purchaseApplyItem.setId(null);
			}
			if (purchaseApplyItem.getStorage().getUnit() != null) {
				purchaseApplyItem.setUnit(purchaseApplyItem.getStorage().getUnit().getName());
			}
			purchaseApplyItem.setPurchaseApply(purchaseApply);
			paiList.add(purchaseApplyItem);
		}
		this.purchaseApplyDao.saveOrUpdateAll(paiList);
	}

	/**
	 * 公用-保存设备明细
	 * @param purchaseApply
	 * @param mapList
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseApplyEquipment(PurchaseApply purchaseApply, List<Map<String, Object>> list)
			throws Exception {
		List<PurchaseApplyEquipmentItem> peaiList = new ArrayList<PurchaseApplyEquipmentItem>();
		for (Map<String, Object> map : list) {
			PurchaseApplyEquipmentItem peai = new PurchaseApplyEquipmentItem();
			peai = (PurchaseApplyEquipmentItem) this.purchaseApplyDao.Map2Bean(map, peai);
			peai.setPurchaseApply(purchaseApply);
			peaiList.add(peai);
		}
		this.purchaseApplyDao.saveOrUpdateAll(peaiList);
	}

	/**
	 * 公用-保存服务明细
	 * @param purchaseApply
	 * @param mapList
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseApplyService(PurchaseApply purchaseApply, List<Map<String, Object>> list) throws Exception {
		List<PurchaseApplyServiceItem> psaiList = new ArrayList<PurchaseApplyServiceItem>();
		for (Map<String, Object> map : list) {
			PurchaseApplyServiceItem psai = new PurchaseApplyServiceItem();
			psai = (PurchaseApplyServiceItem) this.purchaseApplyDao.Map2Bean(map, psai);
			psai.setPurchaseApply(purchaseApply);
			psaiList.add(psai);
		}
		this.purchaseApplyDao.saveOrUpdateAll(psaiList);
	}

	/**
	 * 采购订单参照列表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @throws Exception
	 */
	public Map<String, Object> findPurchaseApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.purchaseApplyDao.selectPurchaseApplyList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 根据申请单创建询库存器明细
	 * @param mapForQuery
	 * @throws Exception
	 * @return
	 */
	public List<Object> findPurchaseApplyItem(Map<String, String> mapForQuery) throws Exception {
		return this.purchaseApplyDao.selectPurchaseApplyItem(mapForQuery);
	}

	/**
	 * 根据申请单创建询价设备明细
	 * @param mapForQuery
	 * @throws Exception
	 * @return
	 */
	public List<Object> findPurchaseEquipmentApplyItem(Map<String, String> mapForQuery) throws Exception {
		return this.purchaseApplyDao.selectPurchaseEquipmentApplyItem(mapForQuery);
	}

	/**
	 * 根据申请单创建询价服务明细
	 * @param mapForQuery
	 * @throws Exception
	 * @return
	 */
	public List<Object> findPurchaseServiceApplyItem(Map<String, String> mapForQuery) throws Exception {
		return this.purchaseApplyDao.selectPurchaseServiceApplyItem(mapForQuery);
	}
	/**
	 * 采购审批
	 * */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void commonSetState(String applicationTypeActionId, String formId,
			String dicStateYesId) {
		PurchaseApply pa=purchaseApplyDao.get(PurchaseApply.class, formId);
		pa.setState("1");
		pa.setStateName("完成");
		pa.setConfirmDate(new Date());
		purchaseApplyDao.saveOrUpdate(pa);
	}

	public List<PurchaseApplyItem> findPurchaseApplyItemList(String id) {
			return purchaseApplyDao.findPurchaseApplyItemList(id);
	}

	public List<PurchaseApplyEquipmentItem> findPurchaseApplyEItemList(String id) {
		return purchaseApplyDao.findPurchaseApplyEItemList(id);
	}

	public List<PurchaseApplyServiceItem> findPurchaseApplySItemList(String id) {
		return purchaseApplyDao.findPurchaseApplySItemList(id);
	}

	public Map<String, Object> showInstrumentItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return purchaseApplyDao.showInstrumentItemListJson(id, start, length, query, col,
				sort);
	}

	public Map<String, Object> showGoodsListJson(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return purchaseApplyDao.showGoodsListJson(id, start, length, query, col,
				sort);
	}

	public Map<String, Object> showServiceItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return purchaseApplyDao.showServiceItemListJson(id, start, length, query, col,
				sort);
	}

	public Map<String, Object> showPurchaseApplyDialogListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return purchaseApplyDao.showPurchaseApplyDialogListJson(start, length, query, col,
				sort);
	}

	public List<PurchaseApplyItem> findGoodsItemList(String id) {
		return purchaseApplyDao.findGoodsItemList(id);
	}
	public List<PurchaseApplyEquipmentItem> findInstrumentItemList(String id) {
		return purchaseApplyDao.findInstrumentItemList(id);
	}
	public List<PurchaseApplyServiceItem> findServiceItemList(String id) {
		return purchaseApplyDao.findServiceItemList(id);
	}

	public List<PurchaseApplyItem> findGoodsItemListByType(String id, String type) {
		return purchaseApplyDao.findGoodsItemListByType(id,type);
	}

	public List<PurchaseApplyEquipmentItem> findInstrumentItemListByType(String id, String type) {
		return purchaseApplyDao.findInstrumentItemListByType(id,type);
	}

	public List<PurchaseApplyServiceItem> findServiceItemListByType(String id, String type) {
		return purchaseApplyDao.findServiceItemListByType(id,type);
	}

	public Map<String, Object> showPurchaseApplyDialogListByTypeJson(String type, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return purchaseApplyDao.showPurchaseApplyDialogListByTypeJson(type, 
				start, length, query, col, sort);
	}
}
