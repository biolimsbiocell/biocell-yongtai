/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：PurchaseApplyAction
 * 类描述：采购申请
 * 创建人：阿川
 * 创建时间：2011-12-07
 * 修改人：倪毅
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.purchase.apply.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.purchase.apply.service.PurchaseApplyService;
import com.biolims.purchase.common.constants.SystemCode;
import com.biolims.purchase.common.constants.SystemConstants;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.purchase.model.PurchaseApplyEquipmentItem;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.purchase.model.PurchaseApplyServiceItem;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/purchase/apply")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class PurchaseApplyAction extends BaseActionSupport {

	private static final long serialVersionUID = 6480754183198344386L;

	@Autowired
	private PurchaseApplyService paService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;

	// 用于页面上显示模块名称
	private String title = "采购申请";

	// 该action权限id
	private String rightsId = "30102";

	private PurchaseApply purchaseApply = new PurchaseApply();
	
	@Action(value = "findItemListJsonByType")
	public void findItemListJsonByType() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("typeId");
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			if(type.equals("2")) {
				List<PurchaseApplyItem> list = paService.findGoodsItemListByType(id,type);
				if(list.size()>0) {
					map.put("list", list);
				}
			}else if(type.equals("77")) {
				List<PurchaseApplyEquipmentItem> list = paService.findInstrumentItemListByType(id,type);
				if(list.size()>0) {
					map.put("list", list);
				}
			}else if(type.equals("88")) {
				List<PurchaseApplyServiceItem> list = paService.findServiceItemListByType(id,type);
				if(list.size()>0) {
					map.put("list", list);
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "findItemListJson")
	public void findItemListJson() throws Exception {
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("typeId");
		Map<String,Object> map = new HashMap<String,Object>();
		try {
			if(type.equals("2")) {
				List<PurchaseApplyItem> list = paService.findGoodsItemList(id);
				if(list.size()>0) {
					map.put("list", list);
				}
			}else if(type.equals("77")) {
				List<PurchaseApplyEquipmentItem> list = paService.findInstrumentItemList(id);
				if(list.size()>0) {
					map.put("list", list);
				}
			}else if(type.equals("88")) {
				List<PurchaseApplyServiceItem> list = paService.findServiceItemList(id);
				if(list.size()>0) {
					map.put("list", list);
				}
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
		 *	展示物资明细列表
	     * @Title: showGoodsListJson  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月3日
	     * @throws
	 */
	@Action(value = "showGoodsListJson")
	public void showGoodsListJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String,Object> result = paService.showGoodsListJson(id, start, length, query, col,
					sort);
			List<PurchaseApplyItem> list = (List<PurchaseApplyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("storage-producer-id", "");
			map.put("storage-producer-name", "");
			map.put("num", "");
			map.put("approveNum", "");
			map.put("unit", "");
			map.put("storage-jdeCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseApplyItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
		 *	展示设备明细列表
	     * @Title: showInstrumentItemListJson  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月3日
	     * @throws
	 */
	@Action(value = "showInstrumentItemListJson")
	public void showInstrumentItemListJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String,Object> result = paService.showInstrumentItemListJson(id, start, length, query, col,
					sort);
			List<PurchaseApplyEquipmentItem> list = (List<PurchaseApplyEquipmentItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("num", "");
			map.put("approveNum", "");
			map.put("equipment-id", "");
			map.put("equipment-name", "");
			map.put("equipment-serialNumber", "");
			map.put("equipment-producer-id", "");
			map.put("equipment-producer-name", "");
			map.put("equipment-spec", "");
			map.put("unit", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseApplyEquipmentItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
		 *	展示服务明细列表
	     * @Title: showServiceItemListJson  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月3日
	     * @throws
	 */
	@Action(value = "showServiceItemListJson")
	public void showServiceItemListJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String,Object> result = paService.showServiceItemListJson(id, start, length, query, col,
					sort);
			List<PurchaseApplyServiceItem> list = (List<PurchaseApplyServiceItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("serviceCode", "");
			map.put("serviceName", "");
			map.put("num", "");
			map.put("approveNum", "");
			map.put("note", "");
			map.put("unit", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseApplyServiceItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
		 *	用于询价管理中采购申请的Dialog
	     * @Title: showPurchaseApplyDialogList  
	     * @Description: TODO  
	     * @param @return
	     * @param @throws Exception    
	     * @return String  
		 * @author 孙灵达  
	     * @date 2018年8月7日
	     * @throws
	 */
	@Action(value = "showPurchaseApplyDialogList")
	public String showPurchaseApplyDialogList() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/purchase/apply/showPurchaseApplyDialogList.jsp");
	}
	
	@Action(value = "showPurchaseApplyDialogListJson")
	public void showPurchaseApplyDialogListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = paService.showPurchaseApplyDialogListJson(start, length, query, col, sort);
			List<PurchaseApply> list = (List<PurchaseApply>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("type-name", "");
			map.put("type-id", "");
			map.put("type-sysCode", "");
			map.put("department-name", "");
			map.put("department-id", "");
			map.put("costCenter-id", "");
			map.put("costCenter-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseApply");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "showPurchaseApplyDialogListByTypeJson")
	public void showPurchaseApplyDialogListByTypeJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			Map<String, Object> result = paService.showPurchaseApplyDialogListByTypeJson(type, 
					start, length, query, col, sort);
			List<PurchaseApply> list = (List<PurchaseApply>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("type-name", "");
			map.put("type-id", "");
			map.put("type-sysCode", "");
			map.put("department-name", "");
			map.put("department-id", "");
			map.put("costCenter-id", "");
			map.put("costCenter-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseApply");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * 申请列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPurchaseApplyList")
	public String showPurchaseApplyList() throws Exception {
		rightsId = "30102";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/purchase/apply/showPurchaseApplyListJson.action");
		return dispatcher("/WEB-INF/page/purchase/apply/showPurchaseApplyList.jsp");
	}

	@Action(value = "showPurchaseApplyListJson")
	public void showPurchaseApplyListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = paService.showPurchaseApplyListJson(start, length, query, col, sort);
			List<PurchaseApply> list = (List<PurchaseApply>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("type-name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmUser-name", "");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PurchaseApply");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 编辑页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditPurchaseApply")
	public String toEditPurchaseApply() throws Exception {
		rightsId = "30101";
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			purchaseApply = paService.getPurchaseApply(id);
			putObjToContext("purchaseApplyId", id);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);

		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.purchaseApply.setId(SystemCode.DEFAULT_SYSTEMCODE);
			purchaseApply.setCreateUser(user);
			purchaseApply.setCreateDate(new Date());
			purchaseApply.setState(SystemConstants.DIC_STATE_NEW);
			purchaseApply.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(purchaseApply.getState());
		return dispatcher("/WEB-INF/page/purchase/apply/editPurchaseApply.jsp");
	}

	/**
	 * 复制页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toCopyPurchaseApply")
	public String toCopyPurchaseApply() throws Exception {
		String id = getParameterFromRequest("id");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		purchaseApply = paService.getPurchaseApply(id);
		purchaseApply.setId(SystemCode.DEFAULT_SYSTEMCODE);
		purchaseApply.setCreateUser(user);
		purchaseApply.setCreateDate(new Date());
		purchaseApply.setState(SystemConstants.DIC_STATE_NEW);
		purchaseApply.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
		if (SystemConstants.PURCHASE_APPLY_DIC_TYPE_STORAGE_SYSCODE
				.equals(purchaseApply.getType().getSysCode())) {
		}
		putObjToContext("copyId", id);
		toSetStateCopy();
		toState(purchaseApply.getState());
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		return dispatcher("/WEB-INF/page/purchase/apply/editPurchaseApply.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewPurchaseApply")
	public String toViewPurchaseApply() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			purchaseApply = paService.getPurchaseApply(id);
			putObjToContext("purchaseApplyId", id);
		}

		return dispatcher("/WEB-INF/page/purchase/apply/editPurchaseApply.jsp");
	}

	/**
	 * 新增采购申请。
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String mainData = getParameterFromRequest("main");
		String changeLog = getParameterFromRequest("changeLog");
		String goodsJson = getParameterFromRequest("goodsJson");
		String goodsChangeLogItem = getParameterFromRequest("goodsChangeLogItem");
		String instrumentJson = getParameterFromRequest("instrumentJson");
		String instrumentChangeLogItem = getParameterFromRequest("instrumentChangeLogItem");
		String serviceJson = getParameterFromRequest("serviceJson");
		String serviceChangeLogItem = getParameterFromRequest("serviceChangeLogItem");
		Map<String, String> dataMap = new HashMap<String,String>();
		dataMap.put("goodsJson", goodsJson);
		dataMap.put("goodsChangeLogItem", goodsChangeLogItem);
		dataMap.put("instrumentJson", instrumentJson);
		dataMap.put("instrumentChangeLogItem", instrumentChangeLogItem);
		dataMap.put("serviceJson", serviceJson);
		dataMap.put("serviceChangeLogItem", serviceChangeLogItem);
		
		Map<String, Object> map = new HashMap<String,Object>();
		try {
			String id = paService.save(mainData, changeLog, dataMap);
			map.put("success", true);
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 保存明细
	 */
	@Action(value = "savePurchaseApplyItem")
	public void saveAnimalMed() throws Exception {
		String purchaseApplyId = getParameterFromRequest("purchaseApplyId");
		String json = getParameterFromRequest("data");
		this.purchaseApply.setId(purchaseApplyId);
		paService.savePurchaseApplyItem(purchaseApply, json);
	}

	/**
		 *	删除物资采购明细
	     * @Title: delPurchaseApplyItem  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月3日
	     * @throws
	 */
	@Action(value = "delPurchaseGoodsItem")
	public void delPurchaseGoodsItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids[]");
		try {
			this.paService.delPurchaseApplyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "purchaseApplySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String purchaseApplySelect() throws Exception {
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		String applyType = getParameterFromRequest("applyType");
		// 导向jsp页面显示
		putObjToContext("applyType", applyType);
		return dispatcher("/WEB-INF/page/purchase/apply/selPurchaseApplyList.jsp");
	}

	@Action(value = "purchaseApplySelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void purchaseApplySelectJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		if (data == null || data.equals(""))
			data = "{\"state\":\"" + SystemConstants.DIC_STATE_YES + "\"}";
		String applyType = getParameterFromRequest("applyType");
		Map<String, Object> controlMap = paService.purchaseApplyListSelect(
				startNum, limitNum, dir, sort, getContextPath(), data, "", "",
				applyType);
		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<PurchaseApply> list = (List<PurchaseApply>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		// map.put("costCenter-id", "");
		// map.put("costCenter-name", "");
		// map.put("department-name", "");
		// map.put("financeCode-name", "");
		map.put("type", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-name", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}

	/**
	 * 自动生成采购订单
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "savePurchaseOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePurchaseOrder() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String purchaseApplyId = getParameterFromRequest("purchaseApplyId");
		try {
			String id = paService.savePurchaseOrder(purchaseApplyId, user);
			map.put("id", id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
		// return redirect("/purchase/order/toEditPurchaseOrder.action?id=" + id
		// + "&reqMethodType="
		// + SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	}


	/**
	 * 保存设备采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "savePurchaseEquipmentApplyItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePurchaseEquipmentApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String itemDataJson = super.getRequest().getParameter("itemDataJson");
		String id = super.getRequest().getParameter("id");
		try {
			this.purchaseApply.setId(id);
			this.paService.savePurchaseEquipmentApplyItem(purchaseApply,
					itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPurchaseEquipmentApplyItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delPurchaseEquipmentApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids[]");
		try {
			this.paService.delPurchaseEquipmentApplyItemById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	/**
	 * 保存服务采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "savePurchaseServiceApplyItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePurchaseServiceApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String itemDataJson = super.getRequest().getParameter("itemDataJson");
		String id = super.getRequest().getParameter("id");
		try {
			this.purchaseApply.setId(id);
			this.paService.savePurchaseServiceApplyItem(purchaseApply,
					itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除服务采购申请明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPurchaseServiceApplyItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delPurchaseServiceApplyItem() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			this.paService.delPurchaseServiceApplyItemById(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 采购申请单参照列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selPurchaseApplyList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selPurchaseApplyList() throws Exception {
		String applyType = getParameterFromRequest("applyType");
		putObjToContext("applyType", applyType);
		return dispatcher("/WEB-INF/page/purchase/apply/selPurchaseApplyList.jsp");
	}

	@Action(value = "selPurchaseApplyListJosn", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selPurchaseApplyListJosn() throws Exception {
		// 开始记录数
		Integer startNum = (getParameterFromRequest("start") == "" ? null
				: Integer.parseInt(getParameterFromRequest("start")));
		// limit
		Integer limitNum = (getParameterFromRequest("limit") == "" ? null
				: Integer.parseInt(getParameterFromRequest("limit")));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String sysCode = getRequest().getParameter("sysCode");

		String data = getRequest().getParameter("data");

		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("state", SystemConstants.DIC_STATE_YES);
		if (sysCode != null && sysCode.length() > 0) {
			mapForQuery.put("type.sysCode", sysCode);
		}
		if (data != null && data.length() > 0) {
			Map<String, String> map = JsonUtils.toObjectByJson(data, Map.class);
			if (map.get("id") != null && map.get("id").length() > 0) {
				mapForQuery.put("id", "like##@@##'%" + map.get("id") + "%' ");
			}
			if (map.get("note") != null && map.get("note").length() > 0) {
				mapForQuery
						.put("name", "like##@@##'%" + map.get("note") + "%'");
			}
		}

		Map<String, Object> result = this.paService.findPurchaseApplyList(
				mapForQuery, startNum, limitNum, dir, sort);

		Long totalCount = Long.parseLong(result.get("total").toString());

		List<PurchaseApply> list = (List<PurchaseApply>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("note", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("costCenter-id", "");
		map.put("costCenter-name", "");
		map.put("department-id", "");
		map.put("department-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("type-sysCode", "");
		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}
	@Action(value="findPurchaseApplyItemList")
	public void findPurchaseApplyItemList() throws Exception  {
		String id=getParameterFromRequest("id");
		String type=getParameterFromRequest("type");
		List<PurchaseApplyItem> listItem=null;
		List<PurchaseApplyEquipmentItem> listEItem=null;
				List<PurchaseApplyServiceItem> listSItem=null;
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			if("2".equals(type)){
				listItem= paService.findPurchaseApplyItemList(id);
			}else if("77".equals(type)){
				listEItem= paService.findPurchaseApplyEItemList(id);
			}else if("88".equals(type)){
				listSItem= paService.findPurchaseApplySItemList(id);
			}
			if(listItem!=null){
				map.put("list", listItem);
			}else if(listEItem!=null){
				map.put("list", listEItem);
			}else if(listSItem!=null){
				map.put("list", listSItem);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchaseApply getPurchaseApply() {
		return purchaseApply;
	}

	public void setPurchaseApply(PurchaseApply purchaseApply) {
		this.purchaseApply = purchaseApply;
	}

}
