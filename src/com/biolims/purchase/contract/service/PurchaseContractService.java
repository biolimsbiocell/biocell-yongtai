package com.biolims.purchase.contract.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.purchase.contract.dao.PurchaseContractDao;
import com.biolims.purchase.contract.model.PurchaseContract;
import com.biolims.purchase.contract.model.PurchaseContractItem;
import com.biolims.purchase.payable.model.PurchasePayable;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PurchaseContractService {
	@Resource
	private PurchaseContractDao purchaseContractDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findPurchaseContractList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return purchaseContractDao.selectPurchaseContractList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PurchaseContract i) throws Exception {

		purchaseContractDao.saveOrUpdate(i);

	}
	public PurchaseContract get(String id) {
		PurchaseContract purchaseContract = commonDAO.get(PurchaseContract.class, id);
		return purchaseContract;
	}
	public Map<String, Object> findPurchaseContractItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = purchaseContractDao.selectPurchaseContractItemList(scId, startNum, limitNum, dir, sort);
		List<PurchaseContractItem> list = (List<PurchaseContractItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePurchaseContractItem(PurchaseContract sc, String itemDataJson) throws Exception {
		List<PurchaseContractItem> saveItems = new ArrayList<PurchaseContractItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PurchaseContractItem scp = new PurchaseContractItem();
			// 将map信息读入实体类
			scp = (PurchaseContractItem) purchaseContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFee(scp.getPrice()*scp.getNum());
			scp.setPurchaseContract(sc);

			saveItems.add(scp);
		}
		purchaseContractDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurchaseContractItem(String[] ids) throws Exception {
		for (String id : ids) {
			PurchaseContractItem scp =  purchaseContractDao.get(PurchaseContractItem.class, id);
			 purchaseContractDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PurchaseContract sc, Map jsonMap) throws Exception {
		if (sc != null) {
			purchaseContractDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("purchaseContractItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePurchaseContractItem(sc, jsonStr);
			}
	}
   }
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String formId) throws Exception {
		PurchaseContract pc=purchaseContractDao.get(PurchaseContract.class, formId);
		pc.setState("1");
		pc.setStateName("完成");
		List<PurchaseContractItem> pcis=purchaseContractDao.findPCI(formId);
		if(pcis.size()>0){
			Double total=0.0;
			for(PurchaseContractItem pci:pcis){
				total+=pci.getFee();
			}
			PurchasePayable pp=new PurchasePayable();
			String autoID = codingRuleService.genTransID("PurchasePayable", "PP");
			pp.setId(autoID);
			pp.setContractId(pc.getId());
			pp.setContractName(pc.getName());
			pp.setCreateDate(new Date());
			pp.setCreateUser(pc.getCreateUser());
			pp.setTotalPrice(total);
			pp.setPaid(0.0);
			pp.setUnpaid(total);
			purchaseContractDao.saveOrUpdate(pp);
		}
	}
}
