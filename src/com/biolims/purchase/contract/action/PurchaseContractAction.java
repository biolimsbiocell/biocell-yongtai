package com.biolims.purchase.contract.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.purchase.contract.model.PurchaseContract;
import com.biolims.purchase.contract.model.PurchaseContractItem;
import com.biolims.purchase.contract.service.PurchaseContractService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/purchase/contract/purchaseContract")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PurchaseContractAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "306";
	@Autowired
	private PurchaseContractService purchaseContractService;
	private PurchaseContract purchaseContract = new PurchaseContract();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showPurchaseContractList")
	public String showPurchaseContractList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/purchase/contract/purchaseContract.jsp");
	}

	@Action(value = "showPurchaseContractListJson")
	public void showPurchaseContractListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = purchaseContractService.findPurchaseContractList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PurchaseContract> list = (List<PurchaseContract>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("supplierId-id", "");
		map.put("supplierId-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "purchaseContractSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPurchaseContractList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/purchase/contract/purchaseContractDialog.jsp");
	}

	@Action(value = "showDialogPurchaseContractListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPurchaseContractListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = purchaseContractService.findPurchaseContractList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PurchaseContract> list = (List<PurchaseContract>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("supplierId-id", "");
		map.put("supplierId-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editPurchaseContract")
	public String editPurchaseContract() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			purchaseContract = purchaseContractService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "purchaseContract");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			purchaseContract.setCreateUser(user);
			purchaseContract.setCreateDate(new Date());
			purchaseContract.setState(SystemConstants.DIC_STATE_NEW);
			purchaseContract.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/purchase/contract/purchaseContractEdit.jsp");
	}

	@Action(value = "copyPurchaseContract")
	public String copyPurchaseContract() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		purchaseContract = purchaseContractService.get(id);
		purchaseContract.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/purchase/contract/purchaseContractEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = purchaseContract.getId();
		if(id!=null&&id.equals("")){
			purchaseContract.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("purchaseContractItem",getParameterFromRequest("purchaseContractItemJson"));
		
		purchaseContractService.save(purchaseContract,aMap);
		return redirect("/purchase/contract/purchaseContract/editPurchaseContract.action?id=" + purchaseContract.getId());

	}

	@Action(value = "viewPurchaseContract")
	public String toViewPurchaseContract() throws Exception {
		String id = getParameterFromRequest("id");
		purchaseContract = purchaseContractService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/purchase/contract/purchaseContractEdit.jsp");
	}
	

	@Action(value = "showPurchaseContractItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPurchaseContractItemList() throws Exception {
		return dispatcher("/WEB-INF/page/purchase/contract/purchaseContractItem.jsp");
	}

	@Action(value = "showPurchaseContractItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPurchaseContractItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = purchaseContractService.findPurchaseContractItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<PurchaseContractItem> list = (List<PurchaseContractItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storageId", "");
			map.put("storageName", "");
			map.put("num", "");
			map.put("price", "");
			map.put("fee", "");
			map.put("note", "");
			map.put("purchaseContract-name", "");
			map.put("purchaseContract-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delPurchaseContractItem")
	public void delPurchaseContractItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			purchaseContractService.delPurchaseContractItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PurchaseContractService getPurchaseContractService() {
		return purchaseContractService;
	}

	public void setPurchaseContractService(PurchaseContractService purchaseContractService) {
		this.purchaseContractService = purchaseContractService;
	}

	public PurchaseContract getPurchaseContract() {
		return purchaseContract;
	}

	public void setPurchaseContract(PurchaseContract purchaseContract) {
		this.purchaseContract = purchaseContract;
	}


}
