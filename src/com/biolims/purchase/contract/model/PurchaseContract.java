package com.biolims.purchase.contract.model;

import java.util.Date;
import java.lang.String;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.supplier.model.Supplier;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 采购合同
 * @author lims-platform
 * @date 2017-11-28 16:36:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_PURCHASE_CONTRACT")
@SuppressWarnings("serial")
public class PurchaseContract extends EntityDao<PurchaseContract> implements java.io.Serializable {
	/**合同编码*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/**创建时间*/
	private Date createDate;
	/**供应商*/
	private Supplier supplierId;
	/**状态*/
	private String state;
	/**状态名称*/
	private String stateName;
	/**备注*/
	private String note;
	/**采购订单*/
	private String purchaseOrder;
	/**
	 *方法: 取得String
	 *@return: String  合同编码
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  合同编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	@ForeignKey(name="null")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得Supplier
	 *@return: Supplier  供应商
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SUPPLIER_ID")
	@ForeignKey(name="null")
	public Supplier getSupplierId(){
		return this.supplierId;
	}
	/**
	 *方法: 设置Supplier
	 *@param: Supplier  供应商
	 */
	public void setSupplierId(Supplier supplierId){
		this.supplierId = supplierId;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态名称
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态名称
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 100)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 * @return the purchaseOrder
	 */
	public String getPurchaseOrder() {
		return purchaseOrder;
	}
	/**
	 * @param purchaseOrder the purchaseOrder to set
	 */
	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	
}