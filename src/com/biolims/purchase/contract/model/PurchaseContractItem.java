package com.biolims.purchase.contract.model;

import java.io.Serializable;
import java.lang.String;
import java.lang.Double;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 合同明细
 * @author lims-platform
 * @date 2017-11-28 16:36:23
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_PURCHASE_CONTRACT_ITEM")
@SuppressWarnings("serial")
public class PurchaseContractItem extends EntityDao<PurchaseContractItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**物品编号*/
	private String storageId;
	/**物品描述*/
	private String storageName;
	/**数量*/
	private Double num;
	/**单价*/
	private Double price;
	/**总价*/
	private Double fee;
	/**备注*/
	private String note;
	/**相关主表*/
	private PurchaseContract purchaseContract;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  物品编号
	 */
	@Column(name ="STORAGE_ID", length = 100)
	public String getStorageId(){
		return this.storageId;
	}
	/**
	 *方法: 设置String
	 *@param: String  物品编号
	 */
	public void setStorageId(String storageId){
		this.storageId = storageId;
	}
	/**
	 *方法: 取得String
	 *@return: String  物品描述
	 */
	@Column(name ="STORAGE_NAME", length = 50)
	public String getStorageName(){
		return this.storageName;
	}
	/**
	 *方法: 设置String
	 *@param: String  物品描述
	 */
	public void setStorageName(String storageName){
		this.storageName = storageName;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  数量
	 */
	@Column(name ="NUM", length = 50)
	public Double getNum(){
		return this.num;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  数量
	 */
	public void setNum(Double num){
		this.num = num;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 51)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得PurchaseContract
	 *@return: PurchaseContract  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PURCHASE_CONTRACT")
	@ForeignKey(name="null")
	public PurchaseContract getPurchaseContract(){
		return this.purchaseContract;
	}
	/**
	 *方法: 设置PurchaseContract
	 *@param: PurchaseContract  相关主表
	 */
	public void setPurchaseContract(PurchaseContract purchaseContract){
		this.purchaseContract = purchaseContract;
	}
	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	/**
	 * @return the fee
	 */
	public Double getFee() {
		return fee;
	}
	/**
	 * @param fee the fee to set
	 */
	public void setFee(Double fee) {
		this.fee = fee;
	}
	
}