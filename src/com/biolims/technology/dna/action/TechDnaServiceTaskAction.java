package com.biolims.technology.dna.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.user.service.UserService;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.common.constants.SystemCode;
import com.biolims.storage.model.StorageOut;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.technology.dna.service.TechDnaServiceTaskService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/technology/dna/techDnaServiceTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TechDnaServiceTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private TechDnaServiceTaskService techDnaServiceTaskService;
	private TechDnaServiceTask techDnaServiceTask = new TechDnaServiceTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private UserService userService;
	@Autowired
	private CommonDAO commonDAO;

	@Action(value = "showTechDnaServiceTaskList")
	public String showTechDnaServiceTaskList() throws Exception {
		rightsId = "280102";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/dna/techDnaServiceTask.jsp");
	}

	@Action(value = "showTechDnaServiceTaskListJson")
	public void showTechDnaServiceTaskListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = techDnaServiceTaskService.findTechDnaServiceTaskList(
					start, length, query, col, sort);
			List<TechDnaServiceTask> list = (List<TechDnaServiceTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");

			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Action(value = "techDnaServiceTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTechDnaServiceTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/dna/techDnaServiceTaskDialog.jsp");
	}

	@Action(value = "showDialogTechDnaServiceTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTechDnaServiceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techDnaServiceTaskService
				.findTechDnaServiceTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<TechDnaServiceTask> list = (List<TechDnaServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("orderType-id", "");
		map.put("orderType-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editTechDnaServiceTask")
	public String editTechDnaServiceTask() throws Exception {
		rightsId = "280101";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			techDnaServiceTask = techDnaServiceTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "techDnaServiceTask");
		} else {
			techDnaServiceTask.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			techDnaServiceTask.setCreateUser(user);
			techDnaServiceTask.setCreateDate(new Date());
			techDnaServiceTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			techDnaServiceTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(techDnaServiceTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/technology/dna/techDnaServiceTaskEdit.jsp");
	}

	@Action(value = "copyTechDnaServiceTask")
	public String copyTechDnaServiceTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		techDnaServiceTask = techDnaServiceTaskService.get(id);
		techDnaServiceTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/technology/dna/techDnaServiceTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = techDnaServiceTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "TechDnaServiceTask";
			String markCode = "KJTQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			techDnaServiceTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("techDnaServiceTaskItem",getParameterFromRequest("techDnaServiceTaskItemJson"));

		techDnaServiceTaskService.save(techDnaServiceTask, aMap);
		return redirect("/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action?id="
				+ techDnaServiceTask.getId());

	}

	@Action(value = "viewTechDnaServiceTask")
	public String toViewTechDnaServiceTask() throws Exception {
		String id = getParameterFromRequest("id");
		techDnaServiceTask = techDnaServiceTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/technology/dna/techDnaServiceTaskEdit.jsp");
	}

	@Action(value = "showTechDnaServiceTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechDnaServiceTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/technology/dna/techDnaServiceTaskItem.jsp");
	}

	@Action(value = "showTechDnaServiceTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechDnaServiceTaskItemListJson() throws Exception{
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = techDnaServiceTaskService
				.findTechDnaServiceTaskItemList(start, length, query, col,
						sort, id);
		List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("sampleOrder-id", "");
		map.put("techDnaServiceTask-name", "");
		map.put("techDnaServiceTask-id", "");
		map.put("orderType", "");
		map.put("classify", "");
		map.put("state", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("sampleNum", "");
		map.put("sampleInfoIn-id", "");
		map.put("sampleInfoIn-code", "");
		map.put("sampleInfoIn-location", "");
		map.put("sampleInfoIn-tempId", "");
		map.put("sampleInfoIn-concentration", "");
		map.put("sampleInfoIn-volume", "");
		map.put("sampleInfoIn-sumTotal", "");
		map.put("sampleInfoIn-customer-id", "");
		map.put("sampleInfoIn-customer-name", "");
		map.put("sampleInfoIn-customer-street", "");
		map.put("sampleInfoIn-customer-postcode", "");
		map.put("sampleInfoIn-sampleInfo-project-id", "");
		map.put("sampleInfoIn-sampleInfo-project-name", "");
		map.put("sampleInfoIn-sampleInfo-crmDoctor-id", "");
		map.put("sampleInfoIn-sampleInfo-crmDoctor-name", "");
		map.put("sampleInfoIn-sampleInfo-species", "");
		map.put("sampleInfoIn-sampleInfo-idCard", "");
		map.put("sampleInfoIn-sellPerson-id", "");
		map.put("sampleInfoIn-sellPerson-name", "");

		map.put("contractId", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("orderId", "");
		map.put("sampleReceiveDate", "yyyy-MM-dd");
		map.put("sonWkName", "");
		map.put("indexi7", "");
		map.put("indexi5", "");
		map.put("groupNo", "");
		map.put("note", "");
		map.put("barCode", "");
		map.put("dataTraffic", "");
		map.put("scopeId", "");
		map.put("scopeName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTechDnaServiceTaskItem")
	public void delTechDnaServiceTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			techDnaServiceTaskService.delTechDnaServiceTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	/**
	 * 选择样本编号
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "SampleInfoSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String SampleInfoSelect() throws Exception {
		String projectId = getRequest().getParameter("projectId");
		putObjToContext("id", projectId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/dna/sampleInfoDialog.jsp");
	}

	@Action(value = "showSampleInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String projectId = getRequest().getParameter("id");
			Map<String, Object> result = techDnaServiceTaskService
					.findSampleInfoListDialog(startNum, limitNum, dir, sort,
							projectId);
			Long total = (Long) result.get("total");
			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("sampleType2", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("receiveDate", "");
			map.put("reportDate", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showTechDnaServiceTaskDialog")
	public String showTechDnaServiceTaskDialog() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/dna/showTechDnaServiceTaskDialog.jsp");
	}

	@Action(value = "showTechDnaServiceTaskDialogJson")
	public void showTechDnaServiceTaskDialogJson() throws Exception {
		Map<String, Object> result = techDnaServiceTaskService
				.selectTechDnaServiceTaskListDialog();
		Long count = (Long) result.get("total");
		List<TechDnaServiceTask> list = (List<TechDnaServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("orderType-id", "");
		map.put("orderType-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("groupNo", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "selectSampleInfo")
	public void showSampleByTechTaskId() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> listMap = this.techDnaServiceTaskService
					.selectTechTaskItemByTaskId(id);
			result.put("success", true);
			result.put("data", listMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据主管显示认领的样本
	@Action(value = "showItemDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showItemDialogList() throws Exception {
		return dispatcher("/WEB-INF/page/technology/dna/showItemDialog.jsp");
	}

	@Action(value = "showItemDialogListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showItemDialogListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			// String scId = getRequest().getParameter("id");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			Map<String, Object> result = techDnaServiceTaskService
					.findTechDnaServiceTaskItemDialogList(map2Query,
							user.getId(), startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("techDnaServiceTask-name", "");
			map.put("techDnaServiceTask-id", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("sampleNum", "");

			map.put("sampleInfoIn-id", "");
			map.put("sampleInfoIn-code", "");
			map.put("sampleInfoIn-location", "");
			map.put("sampleInfoIn-tempId", "");
			map.put("sampleInfoIn-concentration", "");
			map.put("sampleInfoIn-volume", "");
			map.put("sampleInfoIn-sumTotal", "");
			map.put("sampleInfoIn-customer-id", "");
			map.put("sampleInfoIn-customer-name", "");
			map.put("sampleInfoIn-customer-street", "");
			map.put("sampleInfoIn-customer-postcode", "");
			map.put("sampleInfoIn-sampleInfo-productId", "");
			map.put("sampleInfoIn-sampleInfo-productName", "");
			map.put("sampleInfoIn-sampleInfo-project-id", "");
			map.put("sampleInfoIn-sampleInfo-project-name", "");
			map.put("sampleInfoIn-sampleInfo-crmDoctor-id", "");
			map.put("sampleInfoIn-sampleInfo-crmDoctor-name", "");
			map.put("sampleInfoIn-sampleInfo-species", "");
			map.put("sampleInfoIn-sellPerson-id", "");
			map.put("sampleInfoIn-sellPerson-name", "");
			map.put("sampleInfoIn-sampleInfo-patientName", "");
			map.put("sampleInfoIn-sampleInfo-idCard", "");
			map.put("project-id", "");
			map.put("project-name", "");
			map.put("orderId", "");
			map.put("groupNo", "");
			map.put("barCode", "");
			map.put("dataTraffic", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "showItemDialogNewListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showItemDialogNewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = techDnaServiceTaskService
				.findTechDnaServiceTaskItemList(start, length, query, col,
						sort);
		List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleOrder-id", "");
		map.put("sampleCode", "");
		map.put("techDnaServiceTask-name", "");
		map.put("techDnaServiceTask-id", "");
		map.put("orderType", "");
		map.put("classify", "");
		map.put("state", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("sampleNum", "");

		map.put("sampleInfoIn-id", "");
		map.put("sampleInfoIn-code", "");
		map.put("sampleInfoIn-location", "");
		map.put("sampleInfoIn-tempId", "");
		map.put("sampleInfoIn-concentration", "");
		map.put("sampleInfoIn-volume", "");
		map.put("sampleInfoIn-sumTotal", "");
		map.put("sampleInfoIn-customer-id", "");
		map.put("sampleInfoIn-customer-name", "");
		map.put("sampleInfoIn-customer-street", "");
		map.put("sampleInfoIn-customer-postcode", "");
		map.put("sampleInfoIn-sampleInfo-productId", "");
		map.put("sampleInfoIn-sampleInfo-productName", "");
		map.put("sampleInfoIn-sampleInfo-project-id", "");
		map.put("sampleInfoIn-sampleInfo-project-name", "");
		map.put("sampleInfoIn-sampleInfo-crmDoctor-id", "");
		map.put("sampleInfoIn-sampleInfo-crmDoctor-name", "");
		map.put("sampleInfoIn-sampleInfo-species", "");
		map.put("sampleInfoIn-sellPerson-id", "");
		map.put("sampleInfoIn-sellPerson-name", "");
		map.put("sampleInfoIn-sampleInfo-patientName", "");
		map.put("sampleInfoIn-sampleInfo-idCard", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("orderId", "");
		map.put("groupNo", "");
		map.put("barCode", "");
		map.put("dataTraffic", "");
		map.put("scopeId", "");
		map.put("scopeName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 
	 * @Title: showTechDnaServiceTaskListJson
	 * @Description: TODO(认领任务单主查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午6:02:49
	 * @throws
	 */
	@Action(value = "showtechnologyTechDnaServiceTaskListBySearchJson")
	public void showtechnologyTechDnaServiceTaskListBySearchJson() throws Exception {
		int startNum = 0;
		int limitNum = 23;
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String startcreateDate = getParameterFromRequest("startcreateDate");
		String endcreateDate = getParameterFromRequest("endcreateDate");
		String startconfirmDate = getParameterFromRequest("startconfirmDate");
		String endconfirmDate = getParameterFromRequest("endconfirmDate");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techDnaServiceTaskService
				.showtechnologyTechDnaServiceTaskListBySearchList(map2Query, startNum, limitNum, dir,
						sort,startconfirmDate,startcreateDate,endconfirmDate,endcreateDate);
		Long count = (Long) result.get("total");
		List<TechDnaServiceTask> list = (List<TechDnaServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("orderType-id", "");
		map.put("orderType-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TechDnaServiceTaskService getTechDnaServiceTaskService() {
		return techDnaServiceTaskService;
	}

	public void setTechDnaServiceTaskService(
			TechDnaServiceTaskService techDnaServiceTaskService) {
		this.techDnaServiceTaskService = techDnaServiceTaskService;
	}

	public TechDnaServiceTask getTechDnaServiceTask() {
		return techDnaServiceTask;
	}

	public void setTechDnaServiceTask(TechDnaServiceTask techDnaServiceTask) {
		this.techDnaServiceTask = techDnaServiceTask;
	}
	/**
	 * 认领任务单添加样本
	 * @throws Exception
	 */
	@Action(value = "addPurchaseApplyMakeUp")
	public void addPurchaseApplyMakeUp() throws Exception {
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String note = getParameterFromRequest("note");
		String createUser = getParameterFromRequest("createUser");
		String name = getParameterFromRequest("name");
		String state = getParameterFromRequest("state");
		String stateName = getParameterFromRequest("stateName");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String purchaseId = techDnaServiceTaskService.addTechDnaServiceTaskServiceItem(ids,note,id,createUser,createDate,name,state,stateName);
			result.put("success", true);
			result.put("data", purchaseId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	
	/**
	 * 
	 * @Title: saveStorageOutAndItem
	 * @Description: 大保存方法
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveStorageOutAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageOutAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");
		
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String note = getParameterFromRequest("note");
		String createUser = getParameterFromRequest("createUser");
		String name = getParameterFromRequest("name");
		String state = getParameterFromRequest("state");
		String stateName = getParameterFromRequest("stateName");
		String dataItemJson = getParameterFromRequest("ImteJson");
		
		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
				List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				TechDnaServiceTask so = new TechDnaServiceTask();
				Map aMap = new HashMap();
				so = (TechDnaServiceTask) commonDAO.Map2Bean(map1, so);
				so=commonDAO.get(TechDnaServiceTask.class, id);
				//保存主表信息
				so.setScopeId((String) ActionContext.getContext().getSession()
						.get("scopeId"));
				so.setScopeName((String) ActionContext.getContext().getSession()
						.get("scopeName"));
				aMap.put("ImteJson",dataItemJson);
				TechDnaServiceTask newSo = techDnaServiceTaskService.saveTechDnaServiceTaskById(so,id,note,name,
						changeLog,changeLogItem,aMap,createDate,createUser,state,stateName);
				map.put("id",newSo.getId());
			}
			map.put("success", true);
			
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
