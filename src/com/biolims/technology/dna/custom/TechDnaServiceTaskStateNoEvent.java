package com.biolims.technology.dna.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.technology.dna.service.TechDnaServiceTaskService;

public class TechDnaServiceTaskStateNoEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		TechDnaServiceTaskService mbService = (TechDnaServiceTaskService) ctx
				.getBean("techDnaServiceTaskService");
		mbService.stateNo(contentId);

		return "";
	}
}
