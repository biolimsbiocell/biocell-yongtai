package com.biolims.technology.dna.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.contract.model.Contract;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 科技服务-核酸任务单
 * @author lims-platform
 * @date 2016-03-08 09:52:36
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TECH_DNA_SERVICE_TASK")
@SuppressWarnings("serial")
public class TechDnaServiceTask extends EntityDao<TechDnaServiceTask> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 项目编号 */
	private Project projectId;
	/** 合同编号 */
	private Contract contractId;
	/** 任务单类型 */
	private DicType orderType;
	/** 项目负责人 */
	private User projectLeader;
	/** 实验负责人 */
	private User experimentLeader;
	/** 信息负责人 */
	private User analysisLeader;
	/** 备注 */
	private String note;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 审批人 */
	private User confirmUser;
	/** 审批时间 */
	private Date confirmDate;
	/** 状态 */
	private String state;
	/** 状态描述 */
	private String stateName;
	
	private String scopeId;
	private String scopeName;
	
	

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得Project
	 * 
	 * @return: Project 项目编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT_ID")
	public Project getProjectId() {
		return this.projectId;
	}

	/**
	 * 方法: 设置Project
	 * 
	 * @param: Project 项目编号
	 */
	public void setProjectId(Project projectId) {
		this.projectId = projectId;
	}

	/**
	 * 方法: 取得Contract
	 * 
	 * @return: Contract 合同编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONTRACT_ID")
	public Contract getContractId() {
		return this.contractId;
	}

	/**
	 * 方法: 设置Contract
	 * 
	 * @param: Contract 合同编号
	 */
	public void setContractId(Contract contractId) {
		this.contractId = contractId;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 任务单类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ORDER_TYPE")
	public DicType getOrderType() {
		return this.orderType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 任务单类型
	 */
	public void setOrderType(DicType orderType) {
		this.orderType = orderType;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 项目负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT_LEADER")
	public User getProjectLeader() {
		return this.projectLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 项目负责人
	 */
	public void setProjectLeader(User projectLeader) {
		this.projectLeader = projectLeader;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 实验负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPERIMENT_LEADER")
	public User getExperimentLeader() {
		return this.experimentLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 实验负责人
	 */
	public void setExperimentLeader(User experimentLeader) {
		this.experimentLeader = experimentLeader;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 信息负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ANALYSIS_LEADER")
	public User getAnalysisLeader() {
		return this.analysisLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 信息负责人
	 */
	public void setAnalysisLeader(User analysisLeader) {
		this.analysisLeader = analysisLeader;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 500)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审批人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审批人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审批时间
	 */
	@Column(name = "CONFIRM_DATE", length = 50)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审批时间
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态描述
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态描述
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}