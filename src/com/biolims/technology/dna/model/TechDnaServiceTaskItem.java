package com.biolims.technology.dna.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.storage.model.SampleInfoIn;

/**
 * @Title: Model
 * @Description: 科技服务-核酸任务单明细
 * @author lims-platform
 * @date 2016-03-08 09:52:31
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TECH_DNA_SERVICE_TASK_ITEM")
@SuppressWarnings("serial")
public class TechDnaServiceTaskItem extends EntityDao<TechDnaServiceTaskItem>
		implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String sampleCode;
	/** 数据通量 */
	private String dataTraffic;
	/** 相关主表 */
	private TechDnaServiceTask techDnaServiceTask;
	/** 任务单类型 */
	private String orderType;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private String classify;
	/** 状态 */
	private String state;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	// 样本类型
	private String sampleType;
	// 检测项目Id
	private String productId;
	// 检测项目
	private String productName;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	// 样本数量
	private String sampleNum;

	// 待出库样本信息
	private SampleInfoIn sampleInfoIn;
	// 合同号
	private String contractId;
	// 订单号
	private String orderId;
	// 样本接收时间
	private Date sampleReceiveDate;

	// 子文库名称
	private String sonWkName;
	// index i7
	private String indexi7;
	// index i5
	private String indexi5;
	// 科研项目(合同)
	private Project project;
	// 分组号
	private String groupNo;
	// 备注
	private String note;
	//条形码
	private String barCode;
	
	private String scopeId;
	private String scopeName;
	
	private SampleOrder sampleOrder;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
	
	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getSampleReceiveDate() {
		return sampleReceiveDate;
	}

	public void setSampleReceiveDate(Date sampleReceiveDate) {
		this.sampleReceiveDate = sampleReceiveDate;
	}

	public String getSonWkName() {
		return sonWkName;
	}

	public void setSonWkName(String sonWkName) {
		this.sonWkName = sonWkName;
	}

	public String getIndexi7() {
		return indexi7;
	}

	public void setIndexi7(String indexi7) {
		this.indexi7 = indexi7;
	}

	public String getIndexi5() {
		return indexi5;
	}

	public void setIndexi5(String indexi5) {
		this.indexi5 = indexi5;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO＿IN")
	public SampleInfoIn getSampleInfoIn() {
		return sampleInfoIn;
	}

	public void setSampleInfoIn(SampleInfoIn sampleInfoIn) {
		this.sampleInfoIn = sampleInfoIn;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得TechDnaServiceTask
	 * 
	 * @return: TechDnaServiceTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_DNA_SERVICE_TASK")
	public TechDnaServiceTask getTechDnaServiceTask() {
		return this.techDnaServiceTask;
	}

	/**
	 * 方法: 设置TechDnaServiceTask
	 * 
	 * @param: TechDnaServiceTask 相关主表
	 */
	public void setTechDnaServiceTask(TechDnaServiceTask techDnaServiceTask) {
		this.techDnaServiceTask = techDnaServiceTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务单类型
	 */
	@Column(name = "ORDER_TYPE", length = 50)
	public String getOrderType() {
		return this.orderType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务单类型
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 区分临床还是科技服务 0 临床 1 科技服务
	 */
	@Column(name = "CLASSIFY", length = 50)
	public String getClassify() {
		return this.classify;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 区分临床还是科技服务 0 临床 1 科技服务
	 */
	public void setClassify(String classify) {
		this.classify = classify;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}
	
}