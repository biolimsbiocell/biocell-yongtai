package com.biolims.technology.dna.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.project.model.Project;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.purchase.model.PurchaseApplyItem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.technology.dna.dao.TechDnaServiceTaskDao;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TechDnaServiceTaskService {
	@Resource
	private TechDnaServiceTaskDao techDnaServiceTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private CommonService commonService;
	@Resource
	private SystemCodeService systemCodeService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTechDnaServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return techDnaServiceTaskDao.selectTechDnaServiceTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}
	
	public Map<String, Object> findTechDnaServiceTaskList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return techDnaServiceTaskDao.selectTechDnaServiceTaskList(start, length, query, col, sort);
	}

	public Map<String, Object> selectTechDnaServiceTaskListDialog() {
		return techDnaServiceTaskDao.selectTechDnaServiceTaskListDialog();
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechDnaServiceTask i) throws Exception {

		techDnaServiceTaskDao.saveOrUpdate(i);

	}

	public TechDnaServiceTask get(String id) {
		TechDnaServiceTask techDnaServiceTask = commonDAO.get(
				TechDnaServiceTask.class, id);
		return techDnaServiceTask;
	}

	public Map<String, Object> findTechDnaServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = techDnaServiceTaskDao
				.selectTechDnaServiceTaskItemList(scId, startNum, limitNum,
						dir, sort);
		List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
				.get("list");
		return result;
	}
	
	public Map<String, Object> findTechDnaServiceTaskItemList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return techDnaServiceTaskDao.selectTechDnaServiceTaskItemList(start, length,
				query, col, sort, id);
	}
	
	public Map<String, Object> findTechDnaServiceTaskItemList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return techDnaServiceTaskDao.selectTechDnaServiceTaskItemList(start, length,
				query, col, sort);
	}

	// 主管认领的样本信息
	public Map<String, Object> findTechDnaServiceTaskItemDialogList(
			Map<String, String> map2Query, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = techDnaServiceTaskDao
				.selectTechDnaServiceTaskItemDialogList(map2Query, scId,
						startNum, limitNum, dir, sort);
		List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTechDnaServiceTaskItem(TechDnaServiceTask sc,
			String itemDataJson) throws Exception {
		List<TechDnaServiceTaskItem> saveItems = new ArrayList<TechDnaServiceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TechDnaServiceTaskItem scp = new TechDnaServiceTaskItem();
			// 将map信息读入实体类
			scp = (TechDnaServiceTaskItem) techDnaServiceTaskDao.Map2Bean(map,
					scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTechDnaServiceTask(sc);

			SampleInfoIn s = this.commonDAO.get(SampleInfoIn.class, scp
					.getSampleInfoIn().getId());
			s.setState("2");
			saveItems.add(scp);
		}
		techDnaServiceTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTechDnaServiceTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			TechDnaServiceTaskItem scp = techDnaServiceTaskDao.get(
					TechDnaServiceTaskItem.class, id);
			techDnaServiceTaskDao.delete(scp);
//			SampleInfoIn s = this.commonDAO.get(SampleInfoIn.class, scp
//					.getSampleInfoIn().getId());
//			s.setState("1");
		}
	}
	

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechDnaServiceTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			techDnaServiceTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("techDnaServiceTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTechDnaServiceTaskItem(sc, jsonStr);
			}
		}
	}

	public Map<String, Object> findSampleInfoListDialog(Integer startNum,
			Integer limitNum, String dir, String sort, String projectId)
			throws Exception {
		Map<String, Object> result = techDnaServiceTaskDao
				.selectSampleInfoListDialog(startNum, limitNum, dir, sort,
						projectId);
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");
		return result;
	}

	// 审核完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		TechDnaServiceTask sct = techDnaServiceTaskDao.get(
				TechDnaServiceTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		List<TechDnaServiceTaskItem> item = techDnaServiceTaskDao
				.setItemList(sct.getId());
		for (TechDnaServiceTaskItem t : item) {
			SampleInfoIn s = new SampleInfoIn();
			if (t.getSampleInfoIn() != null) {
				s = this.commonDAO.get(SampleInfoIn.class, t.getSampleInfoIn()
						.getId());
				s.setState("2");
			}
			if (t.getSampleCode() != null && !"".equals(t.getSampleCode())) {
				List<SampleInfo> cpatient = (List<SampleInfo>) commonService
						.get(SampleInfo.class, "code", t.getSampleCode());
				int su = cpatient.size();
				if (su > 0) {
					cpatient.get(0).setProject(t.getProject());
				} else {
				}
			}
		}

		// for (TechDnaServiceTaskItem t : item) {
		// String nextFlowId = t.getNextFlowId();
		// if (nextFlowId.equals("0009")) {// 样本入库
		// SampleInItemTemp st = new SampleInItemTemp();
		// st.setCode(t.getSampleCode());
		// st.setNum(Double.parseDouble(t.getSampleNum()));
		// st.setState("1");
		// techDnaServiceTaskDao.saveOrUpdate(st);
		// } else if (nextFlowId.equals("0016")) {// 样本处理
		// PlasmaTaskTemp p = new PlasmaTaskTemp();
		// t.setState("1");
		// sampleInputService.copy(p, t);
		// p.setOrderId(sct.getId());
		// techDnaServiceTaskDao.saveOrUpdate(p);
		// } else if (nextFlowId.equals("0017")) {// 核酸提取
		// DnaTaskTemp d = new DnaTaskTemp();
		// t.setState("1");
		// sampleInputService.copy(d, t);
		// d.setOrderId(sct.getId());
		// techDnaServiceTaskDao.saveOrUpdate(d);
		// } else if (nextFlowId.equals("0019")) {// 核酸提取
		// TechCheckServiceTemp d = new TechCheckServiceTemp();
		// t.setState("1");
		// sampleInputService.copy(d, t);
		// d.setOrderId(sct.getId());
		// techDnaServiceTaskDao.saveOrUpdate(d);
		// } else {
		// // 得到下一步流向的相关表单
		// List<NextFlow> list_nextFlow = nextFlowDao
		// .seletNextFlowById(nextFlowId);
		// for (NextFlow n : list_nextFlow) {
		// Object o = Class.forName(
		// n.getApplicationTypeTable().getClassPath())
		// .newInstance();
		// t.setState("1");
		// sampleInputService.copy(o, t);
		// }
		// }
		// }
	}

	// 作废
	public void stateNo(String id) throws Exception {
		TechDnaServiceTask sct = techDnaServiceTaskDao.get(
				TechDnaServiceTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID_NAME);

		List<TechDnaServiceTaskItem> item = techDnaServiceTaskDao
				.setItemList(sct.getId());
		for (TechDnaServiceTaskItem t : item) {
			SampleInfoIn s = new SampleInfoIn();
			if (t.getSampleInfoIn() != null) {
				s = this.commonDAO.get(SampleInfoIn.class, t.getSampleInfoIn()
						.getId());
				s.setState("1");
			}
		}

	}

	// 根据科技服务主表id 查询明细信息
	public List<Map<String, Object>> selectTechTaskItemByTaskId(String taskId) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = techDnaServiceTaskDao
				.selectTechTaskItemByTaskId(taskId);
		List<TechDnaServiceTaskItem> list = (List<TechDnaServiceTaskItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (TechDnaServiceTaskItem ti : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", ti.getId());
				map.put("sampleCode", ti.getSampleCode());
				map.put("productId", ti.getProductId());
				map.put("productName", ti.getProductName());
				map.put("acceptDate", ti.getAcceptDate());
				map.put("reportDate", ti.getReportDate());
				map.put("sampleNum", ti.getSampleNum());
				map.put("state", ti.getState());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 
	 * @Title: showtechnologyTechDnaServiceTaskListBySearchList
	 * @Description: TODO(认领任务单主查询)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param startconfirmDate
	 * @param @param startcreateDate
	 * @param @param endconfirmDate
	 * @param @param endcreateDate
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午6:07:08
	 * @throws
	 */
	public Map<String, Object> showtechnologyTechDnaServiceTaskListBySearchList(
			Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort, String startconfirmDate,
			String startcreateDate, String endconfirmDate, String endcreateDate) {

		return techDnaServiceTaskDao
				.showtechnologyTechDnaServiceTaskListBySearchList(map2Query,
						startNum, limitNum, dir, sort, startconfirmDate,
						startcreateDate, endconfirmDate, endcreateDate);

	}

	
	// 库存主数据内选择数据添加到认领任务单明细表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addTechDnaServiceTaskServiceItem(String[] ids, String note, 
			String id, String createUser, String createDate,String name,
			String state, String stateName) throws Exception {
		// 保存主表信息
		TechDnaServiceTask si = new TechDnaServiceTask();
		if (id == null || id.length() <= 0
				|| "NEW".equals(id)) {
			String code = systemCodeService.getCodeByPrefix("TechDnaServiceTask", "KJTQ"
					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
					0000, 4, null);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User u = commonDAO.get(User.class, createUser);
			si.setCreateUser(u);
			si.setCreateDate(sdf.parse(createDate));
			si.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			si.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			si.setNote(note);
			si.setName(name);
			si.setScopeId((String) ActionContext.getContext().getSession()
					.get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession()
					.get("scopeName"));
			id = si.getId();
			commonDAO.saveOrUpdate(si);
		}else{
			si = commonDAO.get(TechDnaServiceTask.class, id);
			si.setNote(note);
			si.setName(name);
			commonDAO.saveOrUpdate(si);
		}
		for (int i = 0; i < ids.length; i++) {
			String kuc = ids[i];
			// 通过id查询库存主数据
			SampleInfoIn s = commonDAO.get(SampleInfoIn.class, kuc);
			TechDnaServiceTaskItem sii = new TechDnaServiceTaskItem();
			sii.setTechDnaServiceTask(si);
			if(s!=null){
				String scopeId = s.getScopeId();
				String scopeName = s.getScopeName();
				if(scopeId!=null&&scopeName!=null) {
					sii.setScopeId(scopeId);
					sii.setScopeName(scopeName);
				}
				sii.setSampleInfoIn(s);
			}
			sii.setSampleCode(s.getSampleCode());
			sii.setSampleType(s.getSampleType());
			sii.setDataTraffic(s.getDataTraffic());
			sii.setName(s.getNote());
			sii.setState("1");
			if(s.getSampleInfo()!=null){
				if(s.getSampleInfo().getProject()!=null){
					sii.setProject(s.getSampleInfo().getProject());
				}
				sii.setSampleReceiveDate(s.getSampleInfo().getReceiveDate());
				sii.setGroupNo(s.getSampleInfo().getName());
			}
			s.setState("2");
			
			sii.setBarCode(s.getBarCode());
			commonDAO.saveOrUpdate(s);
			commonDAO.saveOrUpdate(sii);
		}
		return id;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public TechDnaServiceTask saveTechDnaServiceTaskById(TechDnaServiceTask so,
			String id, String note, String name, String logInfo,
			String changeLogItem, Map jsonMap,String createDate, String createUser,String state,String stateName) throws Exception {
		if("".equals(id)||id==null||"NEW".equals(id)){
			String code = systemCodeService.getCodeByPrefix("TechDnaServiceTask", "KJTQ"
					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
					0000, 4, null);
			so.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(!"".equals(createUser)&&createUser!=null){
				User u = commonDAO.get(User.class, createUser);
				if(u!=null){
					so.setCreateUser(u);
				}
			}
			
			if(!"".equals(createDate)&&createDate!=null){
				so.setCreateDate(sdf.parse(createDate));
			}else{
				so.setCreateDate(new Date());
			}
			so.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			so.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			so.setNote(note);
			so.setName(name);
			so.setScopeId((String) ActionContext.getContext().getSession()
					.get("scopeId"));
			so.setScopeName((String) ActionContext.getContext().getSession()
					.get("scopeName"));
			id = so.getId();
			commonDAO.saveOrUpdate(so);
		}else{
			so = commonDAO.get(TechDnaServiceTask.class, id);
			so.setNote(note);
			so.setName(name);
			commonDAO.saveOrUpdate(so);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(so.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		String jsonStr = "";
		jsonStr = (String) jsonMap.get("ImteJson");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveItem(so, jsonStr, changeLogItem);
		}
		return so;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveItem(TechDnaServiceTask newSo, String itemDataJson, String logInfo) throws Exception {
		List<TechDnaServiceTaskItem> saveItems = new ArrayList<TechDnaServiceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TechDnaServiceTaskItem scp = new TechDnaServiceTaskItem();
			// 将map信息读入实体类
			scp = (TechDnaServiceTaskItem) commonDAO.Map2Bean(map, scp);
			String orderId = scp.getOrderId();
			String note = scp.getNote();
			Project project = scp.getProject();
			SampleOrder sampleOrder = scp.getSampleOrder();
			if(scp.getId()!=null) {
				scp=commonDAO.get(TechDnaServiceTaskItem.class, scp.getId());
				scp.setOrderId(orderId);
				scp.setNote(note);
				scp.setProject(project);
				scp.setSampleOrder(sampleOrder);
			}
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setTechDnaServiceTask(newSo);

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSo.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

}
