package com.biolims.technology.dna.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cfdna.model.CfdnaTaskItem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.model.StorageOut;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class TechDnaServiceTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectTechDnaServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TechDnaServiceTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechDnaServiceTask> list = new ArrayList<TechDnaServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	public Map<String, Object> selectTechDnaServiceTaskList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechDnaServiceTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechDnaServiceTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechDnaServiceTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
	
	
	public Map<String, Object> selectTechDnaServiceTaskItemList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechDnaServiceTaskItem where 1=1 and techDnaServiceTask.id='"+id+"'";
		String key = "";
		
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechDnaServiceTaskItem where 1=1 and techDnaServiceTask.id='"+id+"' ";
//			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
//				col=col.replace("-", ".");
//				key+=" order by "+col+" "+sort;
//			}
			List<TechDnaServiceTaskItem> list = new ArrayList<TechDnaServiceTaskItem>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public Map<String, Object> selectTechDnaServiceTaskItemList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechDnaServiceTaskItem where 1=1 and state='1'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=key+map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechDnaServiceTaskItem where 1=1 and state='1' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechDnaServiceTaskItem> list = new ArrayList<TechDnaServiceTaskItem>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectTechDnaServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from TechDnaServiceTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and techDnaServiceTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechDnaServiceTaskItem> list = new ArrayList<TechDnaServiceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 主管认领的样本信息
	public Map<String, Object> selectTechDnaServiceTaskItemDialogList(
			Map<String, String> map2Query, String uId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from TechDnaServiceTaskItem where 1=1 and techDnaServiceTask.state='1' and state='1' ";
		String key = "";
		if (uId != null)
			key = key + " and techDnaServiceTask.createUser.id='" + uId + "'";
		if (map2Query != null)
			key = key + map2where(map2Query);
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechDnaServiceTaskItem> list = new ArrayList<TechDnaServiceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询样本主数据明细的全部内容
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSampleInfoListDialog(Integer startNum,
			Integer limitNum, String dir, String sort, String projectId)
			throws Exception {
		String hql = "from SampleInfo where 1=1 and project.id='" + projectId
				+ "'";
		String key = "";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<TechDnaServiceTaskItem> setItemList(String id) {
		String hql = "from TechDnaServiceTaskItem where 1=1 and techDnaServiceTask.id='"
				+ id + "'";
		List<TechDnaServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public Map<String, Object> selectTechDnaServiceTaskListDialog() {
		String key = " ";
		String hql = " from TechDnaServiceTask where 1=1 and state='1'";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechDnaServiceTask> list = new ArrayList<TechDnaServiceTask>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTechTaskItemByTaskId(String taskId) {
		String hql = "from TechDnaServiceTaskItem t where 1=1 and t.techDnaServiceTask.id='"
				+ taskId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<TechDnaServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 
	 * @Title: showtechnologyTechDnaServiceTaskListBySearchList
	 * @Description: TODO(认领任务单主查询)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param startconfirmDate
	 * @param @param startcreateDate
	 * @param @param endconfirmDate
	 * @param @param endcreateDate
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午6:09:13
	 * @throws
	 */
	public Map<String, Object> showtechnologyTechDnaServiceTaskListBySearchList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort, String startconfirmDate,
			String startcreateDate, String endconfirmDate, String endcreateDate) {

		String key = " ";
		String hql = " from TechDnaServiceTask where 1=1 ";
		if (null != startcreateDate && "" != startcreateDate
				&& null != endcreateDate && "" != endcreateDate) {
			key = key + " and createDate in ('" + startcreateDate + "','"
					+ endcreateDate + "')";
		}
		if (null != startconfirmDate && "" != startconfirmDate
				&& null != endconfirmDate && "" != endconfirmDate) {
			key = key + " and confirmDate in ('" + startconfirmDate + "','"
					+ endconfirmDate + "')";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechDnaServiceTask> list = new ArrayList<TechDnaServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
}