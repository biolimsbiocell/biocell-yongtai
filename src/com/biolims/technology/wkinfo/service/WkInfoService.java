package com.biolims.technology.wkinfo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.technology.wkinfo.dao.WkInfoDao;
import com.biolims.technology.wkinfo.model.WkInfo;
import com.biolims.technology.wkinfo.model.WkInfoItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WkInfoService {
	@Resource
	private WkInfoDao wkInfoDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWkInfoList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return wkInfoDao.selectWkInfoList(mapForQuery, startNum, limitNum, dir,
				sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkInfo i) throws Exception {

		wkInfoDao.saveOrUpdate(i);

	}

	public WkInfo get(String id) {
		WkInfo wkInfo = commonDAO.get(WkInfo.class, id);
		return wkInfo;
	}

	public Map<String, Object> findWkInfoItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkInfoDao.selectWkInfoItemList(scId,
				startNum, limitNum, dir, sort);
		List<WkInfoItem> list = (List<WkInfoItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkInfoItem(WkInfo sc, String itemDataJson) throws Exception {
		List<WkInfoItem> saveItems = new ArrayList<WkInfoItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkInfoItem scp = new WkInfoItem();
			// 将map信息读入实体类
			scp = (WkInfoItem) wkInfoDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkInfo(sc);

			saveItems.add(scp);
		}
		wkInfoDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkInfoItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkInfoItem scp = wkInfoDao.get(WkInfoItem.class, id);
			wkInfoDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkInfo sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wkInfoDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wkInfoItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkInfoItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 
	 * @Title: showtechnologyWkinfoListBySearchJson
	 * @Description: TODO(科技服务里的文库信息主查询)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param endcreateDate
	 * @param @param startcreateDate
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午5:27:01
	 * @throws
	 */
	public Map<String, Object> showtechnologyWkinfoListBySearchJson(
			Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort, String endcreateDate,
			String startcreateDate) {
		return wkInfoDao.showtechnologyWkinfoListBySearch(map2Query, startNum, limitNum, dir,
				sort,startcreateDate,endcreateDate);
	}
}
