package com.biolims.technology.wkinfo.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.technology.wkinfo.model.WkInfo;
import com.biolims.technology.wkinfo.model.WkInfoItem;
import com.biolims.technology.wkinfo.service.WkInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/technology/wkinfo/wkInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2806";
	@Autowired
	private WkInfoService wkInfoService;
	private WkInfo wkInfo = new WkInfo();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showWkInfoList")
	public String showWkInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wkinfo/wkInfo.jsp");
	}

	@Action(value = "showWkInfoListJson")
	public void showWkInfoListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkInfoService.findWkInfoList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkInfo> list = (List<WkInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");

		map.put("wkCode", "");
		map.put("outCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wkInfoSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWkInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wkinfo/wkInfoDialog.jsp");
	}

	@Action(value = "showDialogWkInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWkInfoListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkInfoService.findWkInfoList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkInfo> list = (List<WkInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWkInfo")
	public String editWkInfo() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wkInfo = wkInfoService.get(id);
			if (wkInfo == null) {
				wkInfo = new WkInfo();
				wkInfo.setId(id);
				User user = (User) this
						.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				wkInfo.setCreateUser(user);
				wkInfo.setCreateDate(new Date());
				wkInfo.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				wkInfo.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("handlemethod",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "",
						SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "wkInfo");
			}
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wkInfo.setCreateUser(user);
			wkInfo.setCreateDate(new Date());
			wkInfo.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wkInfo.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wkInfo.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/technology/wkinfo/wkInfoEdit.jsp");
	}

	@Action(value = "copyWkInfo")
	public String copyWkInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wkInfo = wkInfoService.get(id);
		wkInfo.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/technology/wkinfo/wkInfoEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wkInfo.getId();
		if (id != null && id.equals("")) {
			wkInfo.setId(null);
		}
		Map aMap = new HashMap();
		aMap.put("wkInfoItem", getParameterFromRequest("wkInfoItemJson"));

		wkInfoService.save(wkInfo, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/technology/wkinfo/wkInfo/editWkInfo.action?id="
				+ wkInfo.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewWkInfo")
	public String toViewWkInfo() throws Exception {
		String id = getParameterFromRequest("id");
		wkInfo = wkInfoService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/technology/wkinfo/wkInfoEdit.jsp");
	}

	@Action(value = "showWkInfoItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkInfoItemList() throws Exception {
		return dispatcher("/WEB-INF/page/technology/wkinfo/wkInfoItem.jsp");
	}

	@Action(value = "showWkInfoItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkInfoItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkInfoService.findWkInfoItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkInfoItem> list = (List<WkInfoItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("wkType", "");
			map.put("indexI7", "");
			map.put("indexI5", "");
			map.put("fwType", "");
			map.put("species", "");
			map.put("barcodeNo", "");
			map.put("barcode", "");
			map.put("wkSize", "");
			map.put("volume", "");
			map.put("concentration", "");
			map.put("hywkConcentration", "");
			map.put("sampleVolume", "");
			map.put("dataNum", "");
			map.put("note", "");
			map.put("wkInfo-name", "");
			map.put("wkInfo-id", "");
			map.put("wkCode", "");
			map.put("outCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkInfoItem")
	public void delWkInfoItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkInfoService.delWkInfoItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showtechnologyWkinfoListBySearchJson
	 * @Description: TODO(科技服务里文库信息主查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午5:25:17
	 * @throws
	 */
	@Action(value = "showtechnologyWkinfoListBySearchJson")
	public void showtechnologyWkinfoListBySearchJson() throws Exception {
		int startNum = 0;
		int limitNum = 23;
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String startcreateDate = getParameterFromRequest("startcreateDate");
		String endcreateDate = getParameterFromRequest("endcreateDate");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkInfoService.showtechnologyWkinfoListBySearchJson(map2Query,
				startNum, limitNum, dir, sort, endcreateDate, startcreateDate);
		Long count = (Long) result.get("total");
		List<WkInfo> list = (List<WkInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");

		map.put("wkCode", "");
		map.put("outCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WkInfoService getWkInfoService() {
		return wkInfoService;
	}

	public void setWkInfoService(WkInfoService wkInfoService) {
		this.wkInfoService = wkInfoService;
	}

	public WkInfo getWkInfo() {
		return wkInfo;
	}

	public void setWkInfo(WkInfo wkInfo) {
		this.wkInfo = wkInfo;
	}

}
