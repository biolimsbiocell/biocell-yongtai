package com.biolims.technology.wkinfo.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 子文库信息
 * @author lims-platform
 * @date 2016-11-28 16:19:07
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_INFO_ITEM")
@SuppressWarnings("serial")
public class WkInfoItem extends EntityDao<WkInfoItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 子文库名称 */
	private String name;
	/** 文库类型 */
	private String wkType;
	/** index i7 */
	private String indexI7;
	/** index i5 */
	private String indexI5;
	/** 服务类型 */
	private String fwType;
	/** 物种 */
	private String species;
	/** barcode no. */
	private String barcodeNo;
	/** barcode */
	private String barcode;
	/** 文库大小(bp,联川) */
	private String wkSize;
	/** 送样体积 */
	private Double volume;
	/** 送样浓度(nm,联川) */
	private Double concentration;
	/** 混样文库每个终浓度 */
	private Double hywkConcentration;
	/** 取样体积 */
	private Double sampleVolume;
	/** 所需数据量要求 */
	private String dataNum;
	/** 备注 */
	private String note;
	/** 文库信息 */
	private WkInfo wkInfo;
	
	
	
	//文库编号
	private String wkCode;
	//外部样本编号
	private String outCode;

	public String getWkCode() {
		return wkCode;
	}

	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}

	public String getOutCode() {
		return outCode;
	}

	public void setOutCode(String outCode) {
		this.outCode = outCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子文库名称
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 子文库名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库类型
	 */
	@Column(name = "WK_TYPE", length = 50)
	public String getWkType() {
		return this.wkType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库类型
	 */
	public void setWkType(String wkType) {
		this.wkType = wkType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index i7
	 */
	@Column(name = "INDEX_I7", length = 50)
	public String getIndexI7() {
		return this.indexI7;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index i7
	 */
	public void setIndexI7(String indexI7) {
		this.indexI7 = indexI7;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index i5
	 */
	@Column(name = "INDEX_I5", length = 50)
	public String getIndexI5() {
		return this.indexI5;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index i5
	 */
	public void setIndexI5(String indexI5) {
		this.indexI5 = indexI5;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 服务类型
	 */
	@Column(name = "FW_TYPE", length = 50)
	public String getFwType() {
		return this.fwType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 服务类型
	 */
	public void setFwType(String fwType) {
		this.fwType = fwType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 物种
	 */
	@Column(name = "SPECIES", length = 50)
	public String getSpecies() {
		return this.species;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 物种
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String barcode no.
	 */
	@Column(name = "BARCODE_NO", length = 50)
	public String getBarcodeNo() {
		return this.barcodeNo;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String barcode no.
	 */
	public void setBarcodeNo(String barcodeNo) {
		this.barcodeNo = barcodeNo;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String barcode
	 */
	@Column(name = "BARCODE", length = 50)
	public String getBarcode() {
		return this.barcode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String barcode
	 */
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库大小(bp,联川)
	 */
	@Column(name = "WK_SIZE", length = 50)
	public String getWkSize() {
		return this.wkSize;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库大小(bp,联川)
	 */
	public void setWkSize(String wkSize) {
		this.wkSize = wkSize;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 送样体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 送样体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 送样浓度(nm,联川)
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 送样浓度(nm,联川)
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 混样文库每个终浓度
	 */
	@Column(name = "HYWK_CONCENTRATION", length = 50)
	public Double getHywkConcentration() {
		return this.hywkConcentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 混样文库每个终浓度
	 */
	public void setHywkConcentration(Double hywkConcentration) {
		this.hywkConcentration = hywkConcentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 取样体积
	 */
	@Column(name = "SAMPLE_VOLUME", length = 50)
	public Double getSampleVolume() {
		return this.sampleVolume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 取样体积
	 */
	public void setSampleVolume(Double sampleVolume) {
		this.sampleVolume = sampleVolume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 所需数据量要求
	 */
	@Column(name = "DATA_NUM", length = 50)
	public String getDataNum() {
		return this.dataNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 所需数据量要求
	 */
	public void setDataNum(String dataNum) {
		this.dataNum = dataNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得WkInfo
	 * 
	 * @return: WkInfo 文库信息
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WK_INFO")
	public WkInfo getWkInfo() {
		return this.wkInfo;
	}

	/**
	 * 方法: 设置WkInfo
	 * 
	 * @param: WkInfo 文库信息
	 */
	public void setWkInfo(WkInfo wkInfo) {
		this.wkInfo = wkInfo;
	}
}