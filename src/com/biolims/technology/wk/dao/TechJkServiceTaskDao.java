package com.biolims.technology.wk.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.storage.model.StorageOut;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class TechJkServiceTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		String key = " ";
		String hql = " from TechJkServiceTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	
	public Map<String, Object> selectTechJkServiceTaskList(String[] split, Integer start,
			Integer length, String query, String col, String sort, String type) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechJkServiceTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=key +map2Where(query);
		}
		String ugId = "";
		if(split.length>0) {
			for (int i = 0; i < split.length; i++) {
				if(i==split.length-1) {
					ugId += "'" + split[i] + "'";
				}else {
					ugId += "'" + split[i] + "',";
				}
			}
			
			key += " and acceptUser.id in ("+ugId+")";
		}
		
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechJkServiceTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechJkServiceTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectTechJkServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from TechJkServiceTaskItem where 1=1 ";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		if (scId != null)
			key = key + " and techJkServiceTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectTechJkServiceTaskItemList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechJkServiceTaskItem where 1=1 ";
		String key = "";
		
		if(!"".equals(id)&&id!=null){
			key = key +" and techJkServiceTask.id='"+id+"' ";
		}else{
			key = key +" and 1=2 ";
		}

		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechJkServiceTaskItem where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 查询核酸提取结果表
	public Map<String, Object> selectDnaSampleInfoList(String projectId)
			throws Exception {
		String hql = "";
		if (projectId.equals("") || projectId == null) {
			hql = "from DnaTaskInfo where 1=1";
		} else {
			hql = "from DnaTaskInfo where 1=1 and sampleInfo.sampleReceive.project.id='"
					+ projectId + "'";
		}

		String key = "";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DnaTaskInfo> list = new ArrayList<DnaTaskInfo>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTechJkServiceTaskListDialog(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {

		String key = " ";
		String hql = " ";
		hql = " from TechJkServiceTask where 1=1 and type='" + type + "'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by createDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 
	 * @Title: selectTechJkServiceTaskListDialogForWk
	 * @Description: TODO(文库构建下的科技服务)
	 * @param @param mapForQuery
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param type
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-31 下午3:02:35
	 * @throws
	 */
	public Map<String, Object> selectTechJkServiceTaskListDialogForWk(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {

		String key = " ";
		String hql = " ";
		hql = " from TechJkServiceTask where 1=1 and type='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by createDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<TechJkServiceTaskItem> setItemList(String id) {
		String hql = "from TechJkServiceTaskItem where 1=1 and techJkServiceTask.id='"
				+ id + "'";
		List<TechJkServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据科研编号和样本号查询明细样本
	public TechJkServiceTaskItem setItemListByCode(String id, String code) {
		String hql = "from TechJkServiceTaskItem where 1=1 and techJkServiceTask.id='"
				+ id + "' and code='" + code + "'";
		TechJkServiceTaskItem item = (TechJkServiceTaskItem) this.getSession()
				.createQuery(hql).uniqueResult();
		return item;
	}

	// 查询实验左侧表的
	public List<Object> selModel(String model, String tjItem) {
		String hql = "from " + model + " where 1=1 and tjItem.id='" + tjItem
				+ "'";
		List<Object> o = this.getSession().createQuery(hql).list();
		return o;
	}

	/**
	 * 
	 * @Title: showtechnologyWkListBySearchJson
	 * @Description: TODO(认领任务单主查询)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param type
	 * @param @param startcreateDate
	 * @param @param endcreateDate
	 * @param @param startconfirmDate
	 * @param @param endconfirmDate
	 * @param @param startexperimentEndTime
	 * @param @param endexperimentEndTime
	 * @param @param startanalysisEndTime
	 * @param @param endanalysisEndTime
	 * @param @param startsequenceBillDate
	 * @param @param endsequenceBillDate
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午6:46:05
	 * @throws
	 */
	public Map<String, Object> showtechnologyWkListBySearchJson(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String startcreateDate,
			String endcreateDate, String startconfirmDate,
			String endconfirmDate, String startexperimentEndTime,
			String endexperimentEndTime, String startanalysisEndTime,
			String endanalysisEndTime, String startsequenceBillDate,
			String endsequenceBillDate) {

		String key = " ";
		String hql = " from TechJkServiceTask where 1=1";
		if (null != startcreateDate && "" != startcreateDate
				&& null != endcreateDate && "" != endcreateDate) {
			key = key + " and createDate >='" + startcreateDate
					+ "' and createDate <='" + endcreateDate + "' ";
		}
		if (null != startconfirmDate && "" != startconfirmDate 
				&& null != endconfirmDate && "" != endconfirmDate) {
			key = key + " and confirmDate >= '" + startconfirmDate
					+ "'and confirmDate <= '" + endconfirmDate + "'";
		}
		if (null != startexperimentEndTime && "" != startexperimentEndTime
				&& null != endexperimentEndTime && "" != endexperimentEndTime) {
			key = key + " and experimentEndTime >= '" + startexperimentEndTime
					+ "'and experimentEndTime <= '" + endexperimentEndTime + "' ";
		}
		if (null != startanalysisEndTime && "" != startanalysisEndTime
				&& null != endanalysisEndTime && "" != endanalysisEndTime) {
			key = key + " and analysisEndTime and >= '" + startanalysisEndTime
					+ "' and analysisEndTime <= '" + endanalysisEndTime + "' ";
		}
		if (null != startsequenceBillDate && "" != startsequenceBillDate
				&& null != endsequenceBillDate && "" != endsequenceBillDate) {
			key = key + " and endsequenceBillDate > = '"
					+ startsequenceBillDate + "'and endsequenceBillDate <= '" + endsequenceBillDate
					+ "')";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
}