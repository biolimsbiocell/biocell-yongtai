package com.biolims.technology.wk.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.sj.wkblend.model.WkBlendTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.technology.wk.service.TechJkServiceTaskService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/technology/wk/techJkServiceTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TechJkServiceTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private TechJkServiceTaskService techJkServiceTaskService;
	private TechJkServiceTask techJkServiceTask = new TechJkServiceTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private CommonService commonService;
	@Autowired
	private CommonDAO commonDAO;

	@Action(value = "showTechJkServiceTaskList")
	public String showTechJkServiceTaskList() throws Exception {
//		rightsId = "2803";
		rightsId = "280301";
//		String type = getParameterFromRequest("type");
//		putObjToContext("type", type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wk/techJkServiceTask.jsp");
	}

	@Action(value = "showTechJkServiceTaskListJson")
	public void showTechJkServiceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String type = getParameterFromRequest("type");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techJkServiceTaskService
				.findTechJkServiceTaskList(map2Query, startNum, limitNum, dir,
						sort, type);
		Long count = (Long) result.get("total");
		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("type", "");
		map.put("productId-id", "");
		map.put("productId-name", "");
		map.put("dataType", "");
		map.put("sequenceBillDate", "yyyy-MM-dd");
		map.put("sequenceBillName", "");
		map.put("sequenceType", "");
		map.put("sequenceLength", "");
		map.put("sequencePlatform", "");
		map.put("sampeNum", "");
		map.put("species", "");
		map.put("experimentPeriod", "");
		map.put("experimentEndTime", "yyyy-MM-dd");
		map.put("experimentRemarks", "");
		map.put("analysisCycle", "");
		map.put("analysisEndTime", "yyyy-MM-dd");
		map.put("anaysisRemarks", "");
		map.put("projectDifference", "");
		map.put("mixRatio", "");
		map.put("coefficient", "");
		map.put("readsOne", "");
		map.put("readsTwo", "");
		map.put("index1", "");
		map.put("index2", "");
		map.put("density", "");
		map.put("orders-id", "");
		map.put("orders-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("type", "");
		map.put("proId", "");
		map.put("proName", "");

		map.put("quoteId", "");
		map.put("sequencingType", "");
		map.put("projectDescription", "");
		map.put("notebookId", "");
		map.put("clientContactName", "");
		map.put("clientOrganization", "");
		map.put("wgcId", "");
		map.put("libraryType", "");
		map.put("methodology", "");
		map.put("lciIndex", "");
		map.put("insertSize", "");
		map.put("targetYield", "");
		map.put("lciGc", "");
		map.put("notes", "");
		map.put("siGc", "");
		map.put("siIndex", "");
		map.put("clientIndex", "");
		map.put("phix", "");
		map.put("exclusiveLane", "");
		map.put("exclusiveFlowcell", "");
		map.put("qcRequirements", "");
		map.put("deliveryMethod", "");
		map.put("dataAnalysis", "");
		map.put("analysisDetails", "");
		map.put("cxType", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	@Action(value = "showTechJkServiceTaskNewListJson")
	public void showTechJkServiceTaskNewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			String ugId = (String) this.getObjFromSession(SystemConstants.USER_SESSION_GROUPS);
			Map<String, Object> result = techJkServiceTaskService.findTechJkServiceTaskList(
					ugId, start, length, query, col, sort, type);
			List<TechJkServiceTask> list = (List<TechJkServiceTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("projectId-id", "");
			map.put("projectId-name", "");
			map.put("contractId-id", "");
			map.put("contractId-name", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("productId-id", "");
			map.put("productId-name", "");
			map.put("dataType", "");
			map.put("sequenceBillDate", "yyyy-MM-dd");
			map.put("sequenceBillName", "");
			map.put("sequenceType", "");
			map.put("sequenceLength", "");
			map.put("sequencePlatform", "");
			map.put("sampeNum", "");
			map.put("species", "");
			map.put("experimentPeriod", "");
			map.put("experimentEndTime", "yyyy-MM-dd");
			map.put("experimentRemarks", "");
			map.put("analysisCycle", "");
			map.put("analysisEndTime", "yyyy-MM-dd");
			map.put("anaysisRemarks", "");
			map.put("projectDifference", "");
			map.put("mixRatio", "");
			map.put("coefficient", "");
			map.put("readsOne", "");
			map.put("readsTwo", "");
			map.put("index1", "");
			map.put("index2", "");
			map.put("density", "");
			map.put("orders-id", "");
			map.put("orders-name", "");
			map.put("projectLeader-id", "");
			map.put("projectLeader-name", "");
			map.put("experimentLeader-id", "");
			map.put("experimentLeader-name", "");
			map.put("analysisLeader-id", "");
			map.put("analysisLeader-name", "");
//			map.put("techProjectTask-id", "");
//			map.put("techProjectTask-name", "");
			map.put("note", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmUser-id", "");
			map.put("confirmUser-name", "");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("type", "");
			map.put("proId", "");
			map.put("proName", "");

			map.put("quoteId", "");
			map.put("sequencingType", "");
			map.put("projectDescription", "");
			map.put("notebookId", "");
			map.put("clientContactName", "");
			map.put("clientOrganization", "");
			map.put("wgcId", "");
			map.put("libraryType", "");
			map.put("methodology", "");
			map.put("lciIndex", "");
			map.put("insertSize", "");
			map.put("targetYield", "");
			map.put("lciGc", "");
			map.put("notes", "");
			map.put("siGc", "");
			map.put("siIndex", "");
			map.put("clientIndex", "");
			map.put("phix", "");
			map.put("exclusiveLane", "");
			map.put("exclusiveFlowcell", "");
			map.put("qcRequirements", "");
			map.put("deliveryMethod", "");
			map.put("dataAnalysis", "");
			map.put("analysisDetails", "");
			map.put("cxType", "");
			map.put("testUserOneId", "");
			map.put("testUserOneName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "techJkServiceTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTechJkServiceTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wk/techJkServiceTaskDialog.jsp");
	}

	@Action(value = "showDialogTechJkServiceTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTechJkServiceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String type = getParameterFromRequest("type");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techJkServiceTaskService
				.findTechJkServiceTaskList(map2Query, startNum, limitNum, dir,
						sort, type);
		Long count = (Long) result.get("total");
		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("orderType-id", "");
		map.put("orderType-name", "");
		map.put("productId-id", "");
		map.put("productId-name", "");
		map.put("dataType", "");
		map.put("sequenceBillDate", "yyyy-MM-dd");
		map.put("sequenceBillName", "");
		map.put("sequenceType", "");
		map.put("sequenceLength", "");
		map.put("sequencePlatform", "");
		map.put("sampeNum", "");
		map.put("species", "");
		map.put("experimentPeriod", "");
		map.put("experimentEndTime", "yyyy-MM-dd");
		map.put("experimentRemarks", "");
		map.put("analysisCycle", "");
		map.put("analysisEndTime", "yyyy-MM-dd");
		map.put("anaysisRemarks", "");
		map.put("projectDifference", "");
		map.put("mixRatio", "");
		map.put("coefficient", "");
		map.put("readsOne", "");
		map.put("readsTwo", "");
		map.put("index1", "");
		map.put("index2", "");
		map.put("density", "");
		map.put("orders-id", "");
		map.put("orders-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("type", "");
		map.put("cxType", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editTechJkServiceTask")
	public String editTechJkServiceTask() throws Exception {
		rightsId = "280301";
		String id = getParameterFromRequest("id");
		String type = getParameterFromRequest("type");
		long num = 0;
		long num2 = 0;
		if (id != null && !id.equals("")) {
			techJkServiceTask = techJkServiceTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "techJkServiceTask","1");
			num2 = fileInfoService.findFileInfoCount(id, "techJkServiceTask","2");
		} else {
			techJkServiceTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			techJkServiceTask.setCreateUser(user);
			techJkServiceTask.setCreateDate(new Date());
			techJkServiceTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			techJkServiceTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			techJkServiceTask.setCxType("");
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(techJkServiceTask.getState());
		putObjToContext("fileNum", num);
		putObjToContext("fileNum2", num2);
		putObjToContext("type", type);
		// List<DicType> listReadLong = workOrderDao.getReadLongList();
		List<DicType> listPlatForm = workOrderDao.getPlatForm();
		List<DicType> listReadType = workOrderDao.getReadTypeList();
		List<DicType> listSequencingType = workOrderDao
				.getDicTypeList("sequencingtype");
		List<DicType> listLibraryType = workOrderDao
				.getDicTypeList("librarytype");
		List<DicType> listMethodology = workOrderDao
				.getDicTypeList("methodology");
		List<DicType> listLCIIndex = workOrderDao.getDicTypeList("lciindex");
		List<DicType> listIndex = workOrderDao.getDicTypeList("index");
		List<DicType> listSequencingCycle = workOrderDao
				.getDicTypeList("sequencingcycle");
		List<DicType> listClientIndex = workOrderDao
				.getDicTypeList("clientindex");
		// putObjToContext("listReadLong", listReadLong);
		putObjToContext("listPlatForm", listPlatForm);
		putObjToContext("listReadType", listReadType);
		putObjToContext("listSequencingType", listSequencingType);
		putObjToContext("listLibraryType", listLibraryType);
		putObjToContext("listMethodology", listMethodology);
		putObjToContext("listLCIIndex", listLCIIndex);
		putObjToContext("listIndex", listIndex);
		putObjToContext("listSequencingCycle", listSequencingCycle);
		putObjToContext("listClientIndex", listClientIndex);
		return dispatcher("/WEB-INF/page/technology/wk/techJkServiceTaskEdit.jsp");
	}

	@Action(value = "copyTechJkServiceTask")
	public String copyTechJkServiceTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		techJkServiceTask = techJkServiceTaskService.get(id);
		techJkServiceTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/technology/wk/techJkServiceTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = techJkServiceTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "TechJkServiceTask";
			String markCode = "TQ";
			/*if (techJkServiceTask.getType().equals("0")) {
				markCode = "TQ";
			}
			if (techJkServiceTask.getType().equals("1")) {
				markCode = "JK";
			}

			if (techJkServiceTask.getType().equals("2")) {
				markCode = "SJ";
			}
			if (techJkServiceTask.getType().equals("3")) {
				markCode = "SX";
			}*/
			String autoID = codingRuleService.genTransID3(modelName, markCode);
			techJkServiceTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("techJkServiceTaskItem",
				getParameterFromRequest("techJkServiceTaskItemJson"));

		techJkServiceTaskService.save(techJkServiceTask, aMap);
		return redirect("/technology/wk/techJkServiceTask/editTechJkServiceTask.action?id="
				+ techJkServiceTask.getId());

	}

	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String id = getParameterFromRequest("id");
			techJkServiceTask = commonService.get(TechJkServiceTask.class, id);

			Map aMap = new HashMap();
			aMap.put("techJkServiceTaskItem",
					getParameterFromRequest("techJkServiceTaskItemJson"));

			techJkServiceTaskService.save(techJkServiceTask, aMap);

			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewTechJkServiceTask")
	public String toViewTechJkServiceTask() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		String id = getParameterFromRequest("id");
		techJkServiceTask = techJkServiceTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/technology/wk/techJkServiceTaskEdit.jsp");
	}

	@Action(value = "showTechJkServiceTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechJkServiceTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/technology/wk/techJkServiceTaskItem.jsp");
	}

	@Action(value = "showTechJkServiceTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechJkServiceTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = techJkServiceTaskService
					.findTechJkServiceTaskItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleOrder-id", "");
			map.put("orderType", "");
			map.put("experimentUser-name", "");
			map.put("experimentUser-id", "");
			map.put("endDate", "yyyy-MM-dd");
			map.put("species", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-id", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlowId", "");
			map.put("sampleInfo-id", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("location", "");
			map.put("concentration", "");
			map.put("volume", "");

			map.put("wgcId", "");
			map.put("tissueId", "");
			map.put("sampeType", "");
			map.put("extraction", "");
			map.put("qcProtocol", "");
			map.put("sequencingApplication", "");
			map.put("sampleReceivedDate", "");
			map.put("specialRequirements", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			map.put("project-id", "");
			map.put("project-name", "");
			map.put("inwardCode", "");
			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");
			map.put("blendLane", "");
			map.put("blendFc", "");
			map.put("sampleInfoIn-id", "");
			map.put("sampleInfoIn-sampleInfo-patientName", "");
			map.put("khysConcentration", "");
			map.put("khysVolume", "");
			map.put("isStain", "");
			map.put("isNeedStain", "");
			map.put("probeCode-id", "");
			map.put("probeCode-name", "");
			map.put("gc", "");
			map.put("phix", "");
			map.put("q30", "");
			map.put("orderId", "");
			map.put("note", "");
			map.put("groupNo", "");
			map.put("barCode", "");
			map.put("orderCode", "");
			map.put("projectId", "");
			map.put("sampleID", "");
			map.put("sxProductName", "");
			map.put("runId", "");
			map.put("laneId", "");
			map.put("sxSampleCode", "");
			map.put("indexa", "");
			map.put("dataTraffic", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "showTechJkServiceTaskItemNewListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechJkServiceTaskItemNewListJson() throws Exception{
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = techJkServiceTaskService
				.findTechJkServiceTaskItemList(start, length, query, col,
						sort, id);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleOrder-id", "");
		map.put("sampleOrder-name", "");
		map.put("sampleOrder-gender", "");
		map.put("sampleOrder-age", "");
		map.put("sampleOrder-productName", "");
		map.put("sampleOrder-newTask", "");
		/*map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleOrder-id", "");
		map.put("orderType", "");
		map.put("experimentUser-name", "");
		map.put("experimentUser-id", "");
		map.put("endDate", "yyyy-MM-dd");
		map.put("species", "");
		map.put("techJkServiceTask-name", "");
		map.put("techJkServiceTask-id", "");
		map.put("classify", "");
		map.put("dataType", "");
		map.put("dataNum", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("nextFlowId", "");
		map.put("sampleInfo-id", "");
		map.put("nextFlow", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("sampleNum", "");
		map.put("tempId", "");
		map.put("location", "");
		map.put("concentration", "");
		map.put("volume", "");

		map.put("wgcId", "");
		map.put("tissueId", "");
		map.put("sampeType", "");
		map.put("extraction", "");
		map.put("qcProtocol", "");
		map.put("sequencingApplication", "");
		map.put("sampleReceivedDate", "");
		map.put("specialRequirements", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmDoctor-id", "");
		map.put("crmDoctor-name", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("inwardCode", "");
		map.put("sellPerson-id", "");
		map.put("sellPerson-name", "");
		map.put("blendLane", "");
		map.put("blendFc", "");
		map.put("sampleInfoIn-id", "");
		map.put("sampleInfoIn-sampleInfo-patientName", "");
		map.put("khysConcentration", "");
		map.put("khysVolume", "");
		map.put("isStain", "");
		map.put("isNeedStain", "");
		map.put("probeCode-id", "");
		map.put("probeCode-name", "");
		map.put("gc", "");
		map.put("phix", "");
		map.put("q30", "");
		map.put("orderId", "");
		map.put("note", "");
		map.put("groupNo", "");
		map.put("barCode", "");
		map.put("orderCode", "");
		map.put("projectId", "");
		map.put("sampleID", "");
		map.put("sxProductName", "");
		map.put("runId", "");
		map.put("laneId", "");
		map.put("sxSampleCode", "");
		map.put("indexa", "");
		map.put("dataTraffic", "");
		map.put("scopeId","");
		map.put("scopeName", "");*/
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTechJkServiceTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delTechJkServiceTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			techJkServiceTaskService.delTechJkServiceTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 选择样本编号
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "SampleDnaInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String SampleDnaInfo() throws Exception {
		String projectId = getRequest().getParameter("projectId");
		putObjToContext("id", projectId);
		return dispatcher("/WEB-INF/page/technology/wk/sampleInfoDialog.jsp");
	}

	@Action(value = "showSampleDnaInfoJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleDnaInfoJson() throws Exception {
		// // 开始记录数
		// int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// // limit
		// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// // 字段
		// String dir = getParameterFromRequest("dir");
		// // 排序方式
		// String sort = getParameterFromRequest("sort");
		try {
			String projectId = getRequest().getParameter("id");

			Map<String, Object> result = techJkServiceTaskService
					.selectDnaSampleInfoList(projectId);
			Long total = (Long) result.get("total");
			List<DnaTaskInfo> list = (List<DnaTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("dnaCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("reportDate", "");
			map.put("sampleNum", "");
			map.put("note", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showTechJkServiceTaskDialogForPoolingList
	 * @Description: TODO(富集下的科技服务)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-31 下午5:53:44
	 * @throws
	 */
	@Action(value = "showTechJkServiceTaskDialogForPoolingList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechJkServiceTaskDialogForPoolingList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wk/showTechJkServiceTaskDialogForPooling.jsp");
	}

	@Action(value = "showTechJkServiceTaskDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechJkServiceTaskDialogList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wk/showTechJkServiceTaskDialog.jsp");
	}

	/**
	 * 
	 * @Title: showTechJkServiceTaskDialogForWkList
	 * @Description: TODO(文库构建下的科技服务)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-31 下午2:38:59
	 * @throws
	 */
	@Action(value = "showTechJkServiceTaskDialogForWkList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechJkServiceTaskDialogForWkList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/wk/showTechJkServiceTaskDialogForWk.jsp");
	}

	/**
	 * 
	 * @Title: showTechJkServiceTaskDialogForWkListJson
	 * @Description: TODO(文库构建下的科技服务)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-31 下午2:48:44
	 * @throws
	 */
	@Action(value = "showTechJkServiceTaskDialogForWkListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechJkServiceTaskDialogForWkListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String type = getParameterFromRequest("type");// 0：质检 1：测序
		String state = getParameterFromRequest("state");// 1：完成 2：待出报告
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map2Query = new HashMap<String, String>();
		if (state.equals("1")) {
			map2Query.put("stateName", com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		} else if (state.equals("2")) {
			map2Query.put("stateName", "待出报告");
		}
		Map<String, Object> result = techJkServiceTaskService
				.selectTechJkServiceTaskListDialogForWk(map2Query, startNum,
						limitNum, dir, sort, type);
		Long count = (Long) result.get("total");
		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("proId", "");
		map.put("proName", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showTechJkServiceTaskDialogListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechJkServiceTaskDialogListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String type = getParameterFromRequest("type");// 0：质检 1：测序
		String state = getParameterFromRequest("state");// 1：完成 2：待出报告
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map2Query = new HashMap<String, String>();
		if (state.equals("1")) {
			map2Query.put("stateName", com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		} else if (state.equals("2")) {
			map2Query.put("stateName", "待出报告");
		}
		Map<String, Object> result = techJkServiceTaskService
				.selectTechJkServiceTaskListDialog(map2Query, startNum,
						limitNum, dir, sort, type);
		Long count = (Long) result.get("total");
		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("proId", "");
		map.put("proName", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 
	 * @Title: showTechJkServiceTaskDialogForPoolingListJson
	 * @Description: TODO(富集下的科技服务)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-31 下午5:54:45
	 * @throws
	 */
	@Action(value = "showTechJkServiceTaskDialogForPoolingListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechJkServiceTaskDialogForPoolingListJson()
			throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String type = getParameterFromRequest("type");// 0：质检 1：测序
		String state = getParameterFromRequest("state");// 1：完成 2：待出报告
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map2Query = new HashMap<String, String>();
		if (state.equals("1")) {
			map2Query.put("stateName", com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		} else if (state.equals("2")) {
			map2Query.put("stateName", "待出报告");
		}
		Map<String, Object> result = techJkServiceTaskService
				.selectTechJkServiceTaskListDialogForWk(map2Query, startNum,
						limitNum, dir, sort, type);
		Long count = (Long) result.get("total");
		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("proId", "");
		map.put("proName", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 根据科研编号查询明细样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "selTechJkserverTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selTechJkserverTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			List<TechJkServiceTaskItem> list = techJkServiceTaskService
					.selTechJkserverTaskItem(id);
			map.put("success", true);
			map.put("data", list);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据科研编号和样本号查询明细样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "selTechJkserverTaskItemBycode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selTechJkserverTaskItemBycode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String code = getParameterFromRequest("code");
			TechJkServiceTaskItem item = techJkServiceTaskService
					.selTechJkserverTaskItemBycode(id, code);
			map.put("success", true);
			map.put("data", item);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showTechJkServiceTaskListJson
	 * @Description: TODO(质检任务单主查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午6:35:51
	 * @throws
	 */
	@Action(value = "showtechnologyWkListBySearchJson")
	public void showtechnologyWkListBySearchJson() throws Exception {
		int startNum = 0;
		int limitNum = 23;
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String type = getParameterFromRequest("type");
		String data = getParameterFromRequest("data");
		String startcreateDate = getParameterFromRequest("startcreateDate");
		String endcreateDate = getParameterFromRequest("endcreateDate");
		String startconfirmDate = getParameterFromRequest("startconfirmDate");
		String endconfirmDate = getParameterFromRequest("endconfirmDate");
		String startexperimentEndTime = getParameterFromRequest("startexperimentEndTime");
		String endexperimentEndTime = getParameterFromRequest("endexperimentEndTime");
		String startanalysisEndTime = getParameterFromRequest("startanalysisEndTime");
		String endanalysisEndTime = getParameterFromRequest("endanalysisEndTime");
		String startsequenceBillDate = getParameterFromRequest("startsequenceBillDate");
		String endsequenceBillDate = getParameterFromRequest("endsequenceBillDate");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techJkServiceTaskService
				.showtechnologyWkListBySearchJson(map2Query, startNum, limitNum, dir,
						sort, type, startcreateDate, endcreateDate,
						startconfirmDate, endconfirmDate,
						startexperimentEndTime, endexperimentEndTime,
						startanalysisEndTime, endanalysisEndTime,
						startsequenceBillDate, endsequenceBillDate);
		Long count = (Long) result.get("total");
		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("type", "");
		map.put("productId-id", "");
		map.put("productId-name", "");
		map.put("dataType", "");
		map.put("sequenceBillDate", "yyyy-MM-dd");
		map.put("sequenceBillName", "");
		map.put("sequenceType", "");
		map.put("sequenceLength", "");
		map.put("sequencePlatform", "");
		map.put("sampeNum", "");
		map.put("species", "");
		map.put("experimentPeriod", "");
		map.put("experimentEndTime", "yyyy-MM-dd");
		map.put("experimentRemarks", "");
		map.put("analysisCycle", "");
		map.put("analysisEndTime", "yyyy-MM-dd");
		map.put("anaysisRemarks", "");
		map.put("projectDifference", "");
		map.put("mixRatio", "");
		map.put("coefficient", "");
		map.put("readsOne", "");
		map.put("readsTwo", "");
		map.put("index1", "");
		map.put("index2", "");
		map.put("density", "");
		map.put("orders-id", "");
		map.put("orders-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("type", "");
		map.put("proId", "");
		map.put("proName", "");

		map.put("quoteId", "");
		map.put("sequencingType", "");
		map.put("projectDescription", "");
		map.put("notebookId", "");
		map.put("clientContactName", "");
		map.put("clientOrganization", "");
		map.put("wgcId", "");
		map.put("libraryType", "");
		map.put("methodology", "");
		map.put("lciIndex", "");
		map.put("insertSize", "");
		map.put("targetYield", "");
		map.put("lciGc", "");
		map.put("notes", "");
		map.put("siGc", "");
		map.put("siIndex", "");
		map.put("clientIndex", "");
		map.put("phix", "");
		map.put("exclusiveLane", "");
		map.put("exclusiveFlowcell", "");
		map.put("qcRequirements", "");
		map.put("deliveryMethod", "");
		map.put("dataAnalysis", "");
		map.put("analysisDetails", "");
		map.put("cxType", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TechJkServiceTaskService getTechJkServiceTaskService() {
		return techJkServiceTaskService;
	}

	public void setTechJkServiceTaskService(
			TechJkServiceTaskService techJkServiceTaskService) {
		this.techJkServiceTaskService = techJkServiceTaskService;
	}

	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}
	
	
	
	/**
	 * 任务单添加样本
	 * @throws Exception
	 */
	@Action(value = "addPurchaseApplyMakeUp")
	public void addPurchaseApplyMakeUp() throws Exception {
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String note = getParameterFromRequest("note");
		String createUser = getParameterFromRequest("createUser");
		String name = getParameterFromRequest("name");
		String state = getParameterFromRequest("state");
		String stateName = getParameterFromRequest("stateName");
		String type = getParameterFromRequest("type");
		String[] ids = getRequest().getParameterValues("ids[]");
		String dataValue = getParameterFromRequest("dataValue");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String str = "[" + dataValue + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
					List.class);
			String purchaseId="New";
			for (Map<String, Object> map1 : list) {
				TechJkServiceTask so = new TechJkServiceTask();
				Map aMap = new HashMap();
				so = (TechJkServiceTask) commonDAO.Map2Bean(map1, so);
				//保存主表信息
				aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
				purchaseId = techJkServiceTaskService.addTechJkServiceTaskServiceItem(ids,note,id,createUser,createDate,name,state,stateName,type,so);
			}
			result.put("success", true);
			result.put("data", purchaseId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 
	 * @Title: saveStorageOutAndItem
	 * @Description: 大保存方法
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveStorageOutAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageOutAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");
		
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String note = getParameterFromRequest("note");
		String createUser = getParameterFromRequest("createUser");
		String name = getParameterFromRequest("name");
		String state = getParameterFromRequest("state");
		String stateName = getParameterFromRequest("stateName");
		
		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
				List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				TechJkServiceTask so = new TechJkServiceTask();
				Map aMap = new HashMap();
				so = (TechJkServiceTask) commonDAO.Map2Bean(map1, so);
				//保存主表信息
				aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
				TechJkServiceTask newSo = techJkServiceTaskService.saveTechJkServiceTaskById(so,id,note,name,
						changeLog,changeLogItem,aMap,createDate,createUser,state,stateName);
				map.put("id",newSo.getId());
			}
			map.put("success", true);
			
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
