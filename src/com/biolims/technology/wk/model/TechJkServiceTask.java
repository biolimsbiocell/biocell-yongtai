package com.biolims.technology.wk.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.model.user.User;
import com.biolims.contract.model.Contract;
import com.biolims.core.model.user.UserGroup;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.system.product.model.Product;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.util.DateUtil;
import com.opensymphony.xwork2.ActionContext;

/**
 * @Title: Model
 * @Description: 科技服务-建库任务单
 * @author lims-platform
 * @date 2016-03-07 09:38:13
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TECH_JK_SERVICE_TASK")
@SuppressWarnings("serial")
public class TechJkServiceTask extends EntityDao<TechJkServiceTask> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;

	/** 任务单类型 */
	private DicType orderType;
	/** 业务类型编号 */
	private Product productId;
	/** 数据类型 */
	private String dataType;
	/** 下单日期 （截止日期） */
	private Date sequenceBillDate;
	/** 任务单名称 */
	private String sequenceBillName;

	/** 样品数量 */
	private String sampeNum;

	/** 实验周期 */
	private String experimentPeriod;
	/** 实验截止日期 */
	private Date experimentEndTime;
	/** 实验要求 */
	private String experimentRemarks;
	/** 分析周期 */
	private String analysisCycle;
	/** 信息截止日期 */
	private Date analysisEndTime;
	/** 信息要求 */
	private String anaysisRemarks;
	/** 项目紧急识别 */
	private String projectDifference;
	/** 混合比例 */
	private Double mixRatio;
	/** 系数 */
	private Double coefficient;
	/** readsone */
	private String readsOne;
	/** readstwo */
	private String readsTwo;
	/** index1 */
	private String index1;
	/** index2 */
	private String index2;
	/** 预期密度 */
	private String density;
	/** 下单人 */
	private User orders;
	/** 项目负责人 */
	private User projectLeader;
	/** 实验负责人 */
	private User experimentLeader;
	/** 信息负责人 */
	private User analysisLeader;
	/** 备注 */
	private String note;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 审批人 */
	private User confirmUser;
	/** 审批时间 */
	private Date confirmDate;
	/** 状态 */
	private String state;
	/** 状态描述 */
	private String stateName;
	// 任务单类型
	private String type;
	// 检测项目Id
	private String proId;
	// 检测项目
	private String proName;
	// 内部项目号
	private String inwardCode;
	/** 实验组 */
	private UserGroup acceptUser;
	
	
	// 第一批实验员
	private String testUserOneId;
	private String testUserOneName;
	
	
	
	// 范围ID
	private String scopeId;
	
	// 范围
	private String scopeName;	
	
	
	public String getTestUserOneId() {
		return testUserOneId;
	}

	public void setTestUserOneId(String testUserOneId) {
		this.testUserOneId = testUserOneId;
	}

	public String getTestUserOneName() {
		return testUserOneName;
	}

	public void setTestUserOneName(String testUserOneName) {
		this.testUserOneName = testUserOneName;
	}

	/**
	 * 建库任务单
	 * 
	 * @return
	 */
	/*
	 * Project Information （项目信息）
	 */
	/** 合同编号 */
	private Contract contractId;
	// Quote ID
	private String quoteId;
	/** 项目编号 */
	private Project projectId;
	// Sequencing Type
	private String sequencingType;
	// 项目描述
	private String projectDescription;
	// 实验记录本
	private String notebookId;
	// 客户联系人姓名
	private String clientContactName;
	// 客户组织
	private String clientOrganization;
	/*
	 * Library Construction Information （图书馆信息化建设）
	 */
	// WGC ID
	private String wgcId;
	// Library Type
	private String libraryType;
	// Methodology
	private String methodology;
	// Index
	private String lciIndex;
	// Insert Size
	private String insertSize;
	// Target Yield
	private String targetYield;
	/** 物种类型 */
	private String species;
	// GC
	private String lciGc;
	// Notes
	private String notes;
	/*
	 * Sequencing Information (序列信息)
	 */
	/** 测序平台 */
	private String sequencePlatform;
	/** 测序方式 */
	private String sequenceType;
	/** 测序读长 */
	private String sequenceLength;
	// GC
	private String siGc;
	// Index
	private String siIndex;
	// Client Index
	private String clientIndex;
	// phix
	private String phix;
	// Exclusive Lane
	private String exclusiveLane;
	// Exclusive Flowcell
	private String exclusiveFlowcell;
	// QC Requirements
	private String qcRequirements;

	/*
	 * Data Delivery and Bioinformatics Analysis (数据传递和生物信息学分析)
	 */
	// Delivery Method
	private String deliveryMethod;
	// Data Analysis
	private String dataAnalysis;
	// Analysis Details 分析细节
	private String analysisDetails;

	// 芯片类型
	private String chipType;
	// 数据质量要求
	private String dataZlyq;
	// 样本制备方法
	private String sampleZbff;
	// 检测试剂盒
	private String testKit;
	// 基因
	private String gene;
	/* 设备 */
	private Instrument instrument;
	// 质控品
	private String zkp;
	// 标准品
	private String bzp;
	// 检测位点数
	private String snp;
	// 测序类型
	private String cxType;
	
	

	public String getCxType() {
		return cxType;
	}

	public void setCxType(String cxType) {
		this.cxType = cxType;
	}

	public String getChipType() {
		return chipType;
	}

	public void setChipType(String chipType) {
		this.chipType = chipType;
	}

	public String getDataZlyq() {
		return dataZlyq;
	}

	public void setDataZlyq(String dataZlyq) {
		this.dataZlyq = dataZlyq;
	}

	public String getSampleZbff() {
		return sampleZbff;
	}

	public void setSampleZbff(String sampleZbff) {
		this.sampleZbff = sampleZbff;
	}

	public String getTestKit() {
		return testKit;
	}

	public void setTestKit(String testKit) {
		this.testKit = testKit;
	}

	public String getGene() {
		return gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public String getZkp() {
		return zkp;
	}

	public void setZkp(String zkp) {
		this.zkp = zkp;
	}

	public String getBzp() {
		return bzp;
	}

	public void setBzp(String bzp) {
		this.bzp = bzp;
	}

	public String getSnp() {
		return snp;
	}

	public void setSnp(String snp) {
		this.snp = snp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_INSTRUMENT")
	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}

	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}

	public String getInwardCode() {
		return inwardCode;
	}

	public void setInwardCode(String inwardCode) {
		this.inwardCode = inwardCode;
	}

	public String getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}

	public String getSequencingType() {
		return sequencingType;
	}

	public void setSequencingType(String sequencingType) {
		this.sequencingType = sequencingType;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getNotebookId() {
		return notebookId;
	}

	public void setNotebookId(String notebookId) {
		this.notebookId = notebookId;
	}

	public String getClientContactName() {
		return clientContactName;
	}

	public void setClientContactName(String clientContactName) {
		this.clientContactName = clientContactName;
	}

	public String getClientOrganization() {
		return clientOrganization;
	}

	public void setClientOrganization(String clientOrganization) {
		this.clientOrganization = clientOrganization;
	}

	public String getWgcId() {
		return wgcId;
	}

	public void setWgcId(String wgcId) {
		this.wgcId = wgcId;
	}

	public String getLibraryType() {
		return libraryType;
	}

	public void setLibraryType(String libraryType) {
		this.libraryType = libraryType;
	}

	public String getMethodology() {
		return methodology;
	}

	public void setMethodology(String methodology) {
		this.methodology = methodology;
	}

	public String getLciIndex() {
		return lciIndex;
	}

	public void setLciIndex(String lciIndex) {
		this.lciIndex = lciIndex;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getTargetYield() {
		return targetYield;
	}

	public void setTargetYield(String targetYield) {
		this.targetYield = targetYield;
	}

	public String getLciGc() {
		return lciGc;
	}

	public void setLciGc(String lciGc) {
		this.lciGc = lciGc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getSiGc() {
		return siGc;
	}

	public void setSiGc(String siGc) {
		this.siGc = siGc;
	}

	public String getSiIndex() {
		return siIndex;
	}

	public void setSiIndex(String siIndex) {
		this.siIndex = siIndex;
	}

	public String getClientIndex() {
		return clientIndex;
	}

	public void setClientIndex(String clientIndex) {
		this.clientIndex = clientIndex;
	}

	public String getPhix() {
		return phix;
	}

	public void setPhix(String phix) {
		this.phix = phix;
	}

	public String getExclusiveLane() {
		return exclusiveLane;
	}

	public void setExclusiveLane(String exclusiveLane) {
		this.exclusiveLane = exclusiveLane;
	}

	public String getExclusiveFlowcell() {
		return exclusiveFlowcell;
	}

	public void setExclusiveFlowcell(String exclusiveFlowcell) {
		this.exclusiveFlowcell = exclusiveFlowcell;
	}

	public String getQcRequirements() {
		return qcRequirements;
	}

	public void setQcRequirements(String qcRequirements) {
		this.qcRequirements = qcRequirements;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getDataAnalysis() {
		return dataAnalysis;
	}

	public void setDataAnalysis(String dataAnalysis) {
		this.dataAnalysis = dataAnalysis;
	}

	public String getAnalysisDetails() {
		return analysisDetails;
	}

	public void setAnalysisDetails(String analysisDetails) {
		this.analysisDetails = analysisDetails;
	}

	public String getProId() {
		return proId;
	}

	public void setProId(String proId) {
		this.proId = proId;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得Project
	 * 
	 * @return: Project 项目编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT_ID")
	public Project getProjectId() {
		return this.projectId;
	}

	/**
	 * 方法: 设置Project
	 * 
	 * @param: Project 项目编号
	 */
	public void setProjectId(Project projectId) {
		this.projectId = projectId;
	}

	/**
	 * 方法: 取得Contract
	 * 
	 * @return: Contract 合同编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONTRACT_ID")
	public Contract getContractId() {
		return this.contractId;
	}

	/**
	 * 方法: 设置Contract
	 * 
	 * @param: Contract 合同编号
	 */
	public void setContractId(Contract contractId) {
		this.contractId = contractId;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 任务单类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ORDER_TYPE")
	public DicType getOrderType() {
		return this.orderType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 任务单类型
	 */
	public void setOrderType(DicType orderType) {
		this.orderType = orderType;
	}

	/**
	 * 方法: 取得Product
	 * 
	 * @return: Product 业务类型编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT_ID")
	public Product getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置Product
	 * 
	 * @param: Product 业务类型编号
	 */
	public void setProductId(Product productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 数据类型
	 */
	@Column(name = "DATA_TYPE", length = 50)
	public String getDataType() {
		return this.dataType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 数据类型
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下单日期
	 */
	@Column(name = "SEQUENCE_BILL_DATE", length = 50)
	public Date getSequenceBillDate() {
		return this.sequenceBillDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下单日期
	 */
	public void setSequenceBillDate(Date sequenceBillDate) {
		this.sequenceBillDate = sequenceBillDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务单名称
	 */
	@Column(name = "SEQUENCE_BILL_NAME", length = 50)
	public String getSequenceBillName() {
		return this.sequenceBillName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务单名称
	 */
	public void setSequenceBillName(String sequenceBillName) {
		this.sequenceBillName = sequenceBillName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序方式
	 */
	@Column(name = "SEQUENCE_TYPE", length = 50)
	public String getSequenceType() {
		return this.sequenceType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序方式
	 */
	public void setSequenceType(String sequenceType) {
		this.sequenceType = sequenceType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序读长
	 */
	@Column(name = "SEQUENCE_LENGTH", length = 50)
	public String getSequenceLength() {
		return this.sequenceLength;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序读长
	 */
	public void setSequenceLength(String sequenceLength) {
		this.sequenceLength = sequenceLength;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序平台
	 */
	@Column(name = "SEQUENCE_PLATFORM", length = 50)
	public String getSequencePlatform() {
		return this.sequencePlatform;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序平台
	 */
	public void setSequencePlatform(String sequencePlatform) {
		this.sequencePlatform = sequencePlatform;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样品数量
	 */
	@Column(name = "SAMPE_NUM", length = 50)
	public String getSampeNum() {
		return this.sampeNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样品数量
	 */
	public void setSampeNum(String sampeNum) {
		this.sampeNum = sampeNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 物种类型
	 */
	@Column(name = "SPECIES", length = 50)
	public String getSpecies() {
		return this.species;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 物种类型
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验周期
	 */
	@Column(name = "EXPERIMENT_PERIOD", length = 50)
	public String getExperimentPeriod() {
		return this.experimentPeriod;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验周期
	 */
	public void setExperimentPeriod(String experimentPeriod) {
		this.experimentPeriod = experimentPeriod;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 实验截止日期
	 */
	@Column(name = "EXPERIMENT_END_TIME", length = 50)
	public Date getExperimentEndTime() {
		return this.experimentEndTime;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 实验截止日期
	 */
	public void setExperimentEndTime(Date experimentEndTime) {
		this.experimentEndTime = experimentEndTime;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验要求
	 */
	@Column(name = "EXPERIMENT_REMARKS", length = 50)
	public String getExperimentRemarks() {
		return this.experimentRemarks;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验要求
	 */
	public void setExperimentRemarks(String experimentRemarks) {
		this.experimentRemarks = experimentRemarks;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 分析周期
	 */
	@Column(name = "ANALYSIS_CYCLE", length = 50)
	public String getAnalysisCycle() {
		return this.analysisCycle;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 分析周期
	 */
	public void setAnalysisCycle(String analysisCycle) {
		this.analysisCycle = analysisCycle;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 信息截止日期
	 */
	@Column(name = "ANALYSIS_END_TIME", length = 50)
	public Date getAnalysisEndTime() {
		return this.analysisEndTime;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 信息截止日期
	 */
	public void setAnalysisEndTime(Date analysisEndTime) {
		this.analysisEndTime = analysisEndTime;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 信息要求
	 */
	@Column(name = "ANAYSIS_REMARKS", length = 50)
	public String getAnaysisRemarks() {
		return this.anaysisRemarks;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 信息要求
	 */
	public void setAnaysisRemarks(String anaysisRemarks) {
		this.anaysisRemarks = anaysisRemarks;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 项目紧急识别
	 */
	@Column(name = "PROJECT_DIFFERENCE", length = 50)
	public String getProjectDifference() {
		return this.projectDifference;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 项目紧急识别
	 */
	public void setProjectDifference(String projectDifference) {
		this.projectDifference = projectDifference;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 混合比例
	 */
	@Column(name = "MIX_RATIO", length = 50)
	public Double getMixRatio() {
		return this.mixRatio;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 混合比例
	 */
	public void setMixRatio(Double mixRatio) {
		this.mixRatio = mixRatio;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 系数
	 */
	@Column(name = "COEFFICIENT", length = 50)
	public Double getCoefficient() {
		return this.coefficient;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 系数
	 */
	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String readsone
	 */
	@Column(name = "READS_ONE", length = 50)
	public String getReadsOne() {
		return this.readsOne;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String readsone
	 */
	public void setReadsOne(String readsOne) {
		this.readsOne = readsOne;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String readstwo
	 */
	@Column(name = "READS_TWO", length = 50)
	public String getReadsTwo() {
		return this.readsTwo;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String readstwo
	 */
	public void setReadsTwo(String readsTwo) {
		this.readsTwo = readsTwo;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index1
	 */
	@Column(name = "INDEX1", length = 50)
	public String getIndex1() {
		return this.index1;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index1
	 */
	public void setIndex1(String index1) {
		this.index1 = index1;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index2
	 */
	@Column(name = "INDEX2", length = 50)
	public String getIndex2() {
		return this.index2;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index2
	 */
	public void setIndex2(String index2) {
		this.index2 = index2;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 预期密度
	 */
	@Column(name = "DENSITY", length = 50)
	public String getDensity() {
		return this.density;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 预期密度
	 */
	public void setDensity(String density) {
		this.density = density;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下单人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ORDERS")
	public User getOrders() {
		return this.orders;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下单人
	 */
	public void setOrders(User orders) {
		this.orders = orders;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 项目负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT_LEADER")
	public User getProjectLeader() {
		return this.projectLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 项目负责人
	 */
	public void setProjectLeader(User projectLeader) {
		this.projectLeader = projectLeader;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 实验负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPERIMENT_LEADER")
	public User getExperimentLeader() {
		return this.experimentLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 实验负责人
	 */
	public void setExperimentLeader(User experimentLeader) {
		this.experimentLeader = experimentLeader;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 信息负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ANALYSIS_LEADER")
	public User getAnalysisLeader() {
		return this.analysisLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 信息负责人
	 */
	public void setAnalysisLeader(User analysisLeader) {
		this.analysisLeader = analysisLeader;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 500)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审批人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审批人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审批时间
	 */
	@Column(name = "CONFIRM_DATE", length = 50)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审批时间
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态描述
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态描述
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}