package com.biolims.technology.wk.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleState;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.model.Storage;

/**
 * @Title: Model
 * @Description: 科技服务-实验任务单明细
 * @author lims-platform
 * @date 2016-03-07 09:38:09
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TECH_JK_SERVICE_TASK_ITEM")
@SuppressWarnings("serial")
public class TechJkServiceTaskItem extends EntityDao<TechJkServiceTaskItem>
		implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 任务单类型 （质检类型） */
	private String orderType;
	/** 试验负责人 */
	private User experimentUser;
	/** 截止日期 */
	private Date endDate;
	/** 物种 */
	private String species;
	/** 相关主表 */
	private TechJkServiceTask techJkServiceTask;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private String classify;
	/** 数据类型 */
	private String dataType;
	/** 数据量 */
	private String dataNum;
	/** 状态 */
	private String state;
	/** 状态描述 */
	private String stateName;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	// 样本类型
	private String sampleType;
	// 检测项目Id
	private String productId;
	// 检测项目
	private String productName;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	// 样本数量
	private String sampleNum;
	// 中间表id
	private String tempId;
	// 储位
	private String location;
	// 浓度
	private Double concentration;
	// 体积
	private Double volume;
	// 科研项目
	private Project project;
	// 客户（客户单位）
	private CrmCustomer crmCustomer;
	// 医生(客户)
	private CrmDoctor crmDoctor;
	// 内部项目号
	private String inwardCode;
	// 患者姓名 出库时保存
	private String patientName;
	// 销售
	private User sellPerson;

	// 混Lane
	private String blendLane;
	// 混Flowcell
	private String blendFc;
	// 待出库样本信息
	private SampleInfoIn sampleInfoIn;
	/**
	 * 核酸任务单
	 * 
	 * @return
	 */
	// 混合分组
	private String wgcId;
	// Tissue ID
	private String tissueId;
	// Sample Type
	private String sampeType;
	// Extraction
	private String extraction;
	// QC Protocol
	private String qcProtocol;
	// Sequencing Application
	private String sequencingApplication;
	// Sample Received Date
	private String sampleReceivedDate;
	// Special Requirements
	private String specialRequirements;
	// 客户原始浓度
	private String khysConcentration;
	// 客户原始体积
	private String khysVolume;
	// 是否有染片
	private String isStain;
	// 是否需要染片
	private String isNeedStain;
	
	//条码号
	private String barCode;
	
	//订单编号
	private String orderCode;
	//项目编号
	private String projectId;
	//文库编号
	private String sampleID;
	//检测项目
	private String sxProductName;
	//RUN_ID
	private String runId;
	//lan编号
	private String laneId;
	//原始样本号
	private String sxSampleCode;
	
	
	//index	
	private String indexa;
	
	
	//数据通量
	private String dataTraffic;
	
	private String scopeId;
	private String scopeName;
	
	private SampleOrder sampleOrder;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getDataTraffic() {
		return dataTraffic;
	}

	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getSampleID() {
		return sampleID;
	}

	public void setSampleID(String sampleID) {
		this.sampleID = sampleID;
	}

	public String getSxProductName() {
		return sxProductName;
	}

	public void setSxProductName(String sxProductName) {
		this.sxProductName = sxProductName;
	}

	public String getRunId() {
		return runId;
	}

	public void setRunId(String runId) {
		this.runId = runId;
	}

	public String getLaneId() {
		return laneId;
	}

	public void setLaneId(String laneId) {
		this.laneId = laneId;
	}

	public String getSxSampleCode() {
		return sxSampleCode;
	}

	public void setSxSampleCode(String sxSampleCode) {
		this.sxSampleCode = sxSampleCode;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	/**
	 * 测序任务单 Illumina 平台
	 * 
	 * @return
	 */
	/** 探针 */
	private Storage probeCode;
	// GC%
	private String gc;
	// phix%
	private String phix;
	// Q30
	private String q30;

	// 订单号
	private String orderId;

	// 备注
	private String note;
	// 分组号 （收样时的分组号）
	private String groupNo;

	public String getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getKhysConcentration() {
		return khysConcentration;
	}

	public void setKhysConcentration(String khysConcentration) {
		this.khysConcentration = khysConcentration;
	}

	public String getKhysVolume() {
		return khysVolume;
	}

	public void setKhysVolume(String khysVolume) {
		this.khysVolume = khysVolume;
	}

	public String getIsStain() {
		return isStain;
	}

	public void setIsStain(String isStain) {
		this.isStain = isStain;
	}

	public String getIsNeedStain() {
		return isNeedStain;
	}

	public void setIsNeedStain(String isNeedStain) {
		this.isNeedStain = isNeedStain;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROBE_CODE")
	public Storage getProbeCode() {
		return probeCode;
	}

	public void setProbeCode(Storage probeCode) {
		this.probeCode = probeCode;
	}

	public String getGc() {
		return gc;
	}

	public void setGc(String gc) {
		this.gc = gc;
	}

	public String getPhix() {
		return phix;
	}

	public void setPhix(String phix) {
		this.phix = phix;
	}

	public String getQ30() {
		return q30;
	}

	public void setQ30(String q30) {
		this.q30 = q30;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO＿IN")
	public SampleInfoIn getSampleInfoIn() {
		return sampleInfoIn;
	}

	public void setSampleInfoIn(SampleInfoIn sampleInfoIn) {
		this.sampleInfoIn = sampleInfoIn;
	}

	public String getBlendLane() {
		return blendLane;
	}

	public void setBlendLane(String blendLane) {
		this.blendLane = blendLane;
	}

	public String getBlendFc() {
		return blendFc;
	}

	public void setBlendFc(String blendFc) {
		this.blendFc = blendFc;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getInwardCode() {
		return inwardCode;
	}

	public void setInwardCode(String inwardCode) {
		this.inwardCode = inwardCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	public User getSellPerson() {
		return sellPerson;
	}

	public void setSellPerson(User sellPerson) {
		this.sellPerson = sellPerson;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	/**
	 * 科研项目
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	private SampleInfo sampleInfo;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getWgcId() {
		return wgcId;
	}

	public void setWgcId(String wgcId) {
		this.wgcId = wgcId;
	}

	public String getTissueId() {
		return tissueId;
	}

	public void setTissueId(String tissueId) {
		this.tissueId = tissueId;
	}

	public String getSampeType() {
		return sampeType;
	}

	public void setSampeType(String sampeType) {
		this.sampeType = sampeType;
	}

	public String getExtraction() {
		return extraction;
	}

	public void setExtraction(String extraction) {
		this.extraction = extraction;
	}

	public String getQcProtocol() {
		return qcProtocol;
	}

	public void setQcProtocol(String qcProtocol) {
		this.qcProtocol = qcProtocol;
	}

	public String getSequencingApplication() {
		return sequencingApplication;
	}

	public void setSequencingApplication(String sequencingApplication) {
		this.sequencingApplication = sequencingApplication;
	}

	public String getSampleReceivedDate() {
		return sampleReceivedDate;
	}

	public void setSampleReceivedDate(String sampleReceivedDate) {
		this.sampleReceivedDate = sampleReceivedDate;
	}

	public String getSpecialRequirements() {
		return specialRequirements;
	}

	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务单类型
	 */
	@Column(name = "ORDER_TYPE", length = 50)
	public String getOrderType() {
		return this.orderType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务单类型
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 试验负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPERIMENT_USER")
	public User getExperimentUser() {
		return this.experimentUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 试验负责人
	 */
	public void setExperimentUser(User experimentUser) {
		this.experimentUser = experimentUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 截止日期
	 */
	@Column(name = "END_DATE", length = 50)
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 截止日期
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 物种
	 */
	@Column(name = "SPECIES", length = 50)
	public String getSpecies() {
		return this.species;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 物种
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

	/**
	 * 方法: 取得TechJkServiceTask
	 * 
	 * @return: TechJkServiceTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return this.techJkServiceTask;
	}

	/**
	 * 方法: 设置TechJkServiceTask
	 * 
	 * @param: TechJkServiceTask 相关主表
	 */
	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 区分临床还是科技服务 0 临床 1 科技服务
	 */
	@Column(name = "CLASSIFY", length = 50)
	public String getClassify() {
		return this.classify;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 区分临床还是科技服务 0 临床 1 科技服务
	 */
	public void setClassify(String classify) {
		this.classify = classify;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 数据类型
	 */
	@Column(name = "DATA_TYPE", length = 50)
	public String getDataType() {
		return this.dataType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 数据类型
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 数据量
	 */
	@Column(name = "DATA_NUM", length = 50)
	public String getDataNum() {
		return this.dataNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 数据量
	 */
	public void setDataNum(String dataNum) {
		this.dataNum = dataNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态描述
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态描述
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}