package com.biolims.technology.wk.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.technology.wk.service.TechJkServiceTaskService;

public class TechJkServiceTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		TechJkServiceTaskService mbService = (TechJkServiceTaskService) ctx
				.getBean("techJkServiceTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
