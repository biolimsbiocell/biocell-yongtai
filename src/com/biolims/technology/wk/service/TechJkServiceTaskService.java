package com.biolims.technology.wk.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.project.model.Project;
import com.biolims.experiment.check.model.TechCheckServiceTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.massarray.model.MassarrayTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.sanger.model.SangerTaskTemp;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.technology.wk.dao.TechJkServiceTaskDao;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TechJkServiceTaskService {
	@Resource
	private TechJkServiceTaskDao techJkServiceTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleStateService sampleStateService;
	
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		return techJkServiceTaskDao.selectTechJkServiceTaskList(mapForQuery,
				startNum, limitNum, dir, sort, type);
	}
	
	public Map<String, Object> findTechJkServiceTaskList(String ugId, Integer start,
			Integer length, String query, String col, String sort, String type) throws Exception {
		String[] split = ugId.split(",");
		return techJkServiceTaskDao.selectTechJkServiceTaskList(split, start, length, query, col, sort, type);
	}

	// 查询核酸提取结果表
	public Map<String, Object> selectDnaSampleInfoList(String projectId)
			throws Exception {
		return techJkServiceTaskDao.selectDnaSampleInfoList(projectId);
	}

	/**
	 * 
	 * @Title: selectTechJkServiceTaskListDialogForWk
	 * @Description: TODO(文库构建下的科技服务)
	 * @param @param mapForQuery
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param type
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-31 下午3:00:56
	 * @throws
	 */
	public Map<String, Object> selectTechJkServiceTaskListDialogForWk(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		return techJkServiceTaskDao.selectTechJkServiceTaskListDialogForWk(
				mapForQuery, startNum, limitNum, dir, sort, type);
	}

	public Map<String, Object> selectTechJkServiceTaskListDialog(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type) {
		return techJkServiceTaskDao.selectTechJkServiceTaskListDialog(
				mapForQuery, startNum, limitNum, dir, sort, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechJkServiceTask i) throws Exception {

		techJkServiceTaskDao.saveOrUpdate(i);

	}

	public TechJkServiceTask get(String id) {
		TechJkServiceTask techJkServiceTask = commonDAO.get(
				TechJkServiceTask.class, id);
		return techJkServiceTask;
	}

	public Map<String, Object> findTechJkServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = techJkServiceTaskDao
				.selectTechJkServiceTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		return result;
	}
	
	public Map<String, Object> findTechJkServiceTaskItemList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return techJkServiceTaskDao
				.selectTechJkServiceTaskItemList(start, length,
				query, col, sort, id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTechJkServiceTaskItem(TechJkServiceTask sc,
			String itemDataJson) throws Exception {
		List<TechJkServiceTaskItem> saveItems = new ArrayList<TechJkServiceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TechJkServiceTaskItem scp = new TechJkServiceTaskItem();
			// 将map信息读入实体类
			scp = (TechJkServiceTaskItem) techJkServiceTaskDao.Map2Bean(map,
					scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTechJkServiceTask(sc);

			TechDnaServiceTaskItem tDna = this.commonDAO.get(
					TechDnaServiceTaskItem.class, scp.getTempId());
			if (tDna != null) {
				tDna.setState("2");
			} else {// 测序任务单
				if (scp.getSampleInfoIn() != null
						&& !"".equals(scp.getSampleInfoIn())) {
					if(!"".equals(scp.getSampleInfoIn().getId())){
					SampleInfoIn s = this.commonDAO.get(SampleInfoIn.class, scp
							.getSampleInfoIn().getId());
					s.setState("2");
					}
				}

			}
			// if (scp.getInwardCode() == null ||
			// "".equals(scp.getInwardCode())) {
			// DateFormat format = new SimpleDateFormat("yyMMdd");
			// String markCode = "";
			// markCode = scp.getProject().getId();
			// Product p = this.commonDAO.get(Product.class,
			// scp.getProductId());
			// if (p != null) {
			// markCode += p.getMark();
			// }
			// // markCode += format.format(new Date());
			// // String inwardCode = codingRuleService.get(
			// // "TechJkServiceTaskItem", markCode, 00, 2, null,
			// // "inwardCode");
			// scp.setInwardCode(markCode);
			// }
			techJkServiceTaskDao.saveOrUpdate(scp);
			// saveItems.add(scp);
		}
		// techJkServiceTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTechJkServiceTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			TechJkServiceTaskItem scp = techJkServiceTaskDao.get(
					TechJkServiceTaskItem.class, id);
			techJkServiceTaskDao.delete(scp);
			SampleOrder s = commonDAO.get(SampleOrder.class, scp.getTempId());
			if(s!=null) {
				s.setState("1");
			}
			/*TechDnaServiceTaskItem tDna = this.commonDAO.get(
					TechDnaServiceTaskItem.class, scp.getTempId());*/
			/*if (tDna != null) {
				tDna.setState("1");
			} else {// 测序任务单
				if (scp.getSampleInfoIn() != null
						&& !"".equals(scp.getSampleInfoIn())) {
					SampleInfoIn s = this.commonDAO.get(SampleInfoIn.class, scp
							.getSampleInfoIn().getId());
					s.setState("1");
				}

			}*/
		}
	}

	/**
	 * 根据科研编号查询明细样本
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<TechJkServiceTaskItem> selTechJkserverTaskItem(String id)
			throws Exception {
		List<TechJkServiceTaskItem> item = techJkServiceTaskDao.setItemList(id);
		List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
		for (TechJkServiceTaskItem t : item) {
			String[] sampleCode = t.getSampleCode().split(",");
			for (int i = 0; i < sampleCode.length; i++) {
				SampleInfo s = this.sampleInfoMainDao
						.findSampleInfo(sampleCode[i]);
				if (s.getPatientName() != null
						&& !"".equals(s.getPatientName())) {
					t.setPatientName(s.getPatientName());
				}
			}
			list.add(t);
		}
		return list;
	}

	/**
	 * 根据科研编号和样本号查询明细样本
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public TechJkServiceTaskItem selTechJkserverTaskItemBycode(String id,
			String code) throws Exception {
		TechJkServiceTaskItem item = techJkServiceTaskDao.setItemListByCode(id,
				code);
		return item;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechJkServiceTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			
			
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			techJkServiceTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("techJkServiceTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTechJkServiceTaskItem(sc, jsonStr);
			}
		}
	}

	// 审核完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		TechJkServiceTask sct = techJkServiceTaskDao.get(
				TechJkServiceTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmDate(new Date());
		sct.setConfirmUser(user);

		List<TechJkServiceTaskItem> item = techJkServiceTaskDao.setItemList(sct
				.getId());
		for (TechJkServiceTaskItem t : item) {
			SampleOrder sampleOrder = t.getSampleOrder();
			if(sampleOrder!=null) {
				sampleOrder.setState("10");
				sampleOrder.setStateName("任务完成");
				sampleOrder.setNewTask(id);
				sampleOrder.setTechJkService(sct);
			}
			/*TechDnaServiceTaskItem tDna = this.commonDAO.get(
					TechDnaServiceTaskItem.class, t.getTempId());
			if (tDna != null) {
				tDna.setState("2");
			} else {// 测序任务单
				if (t.getSampleInfoIn() != null
						&& !"".equals(t.getSampleInfoIn())) {
					SampleInfoIn s = this.commonDAO.get(SampleInfoIn.class, t
							.getSampleInfoIn().getId());
					s.setState("2");
				}

			}
			
			if (t.getSampleCode() != null && !"".equals(t.getSampleCode())) {
				List<SampleInfo> cpatient = (List<SampleInfo>) commonService
						.get(SampleInfo.class, "code", t.getSampleCode());
				int su = cpatient.size();
				if (su > 0) {
					if("3".equals(sct.getType())){
						List<Project> pj = (List<Project>) commonService
								.get(Project.class,t.getProjectId());
						if(pj.size()>0){
							cpatient.get(0).setProject(pj.get(0));
						}
					}else{
						cpatient.get(0).setProject(t.getProject());
					}
				} else {
				}
			}*/
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sampleStateService.saveSampleState1(
					t.getSampleOrder(),
					null,
					null,
					null,
					null,
					"",
					sct.getCreateDate().toString(),
					format.format(new Date()),
					"TechJkServiceTask",
					"下达任务单",
					(User) ServletActionContext
							.getRequest()
							.getSession()
							.getAttribute(
									SystemConstants.USER_SESSION_KEY),
					id, null, null, null,
					null, null, null, null, null, null, null);

			/*String sampleCode = t.getSampleCode();
			String nextFlowId = t.getNextFlowId();
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if (nextFlowId != null && !"".equals(nextFlowId)) {
				if (nextFlowId.equals("0017")) {// 核酸提取
					DnaTaskTemp d = new DnaTaskTemp();
					t.setState("1");
					sampleInputService.copy(d, t);
					if (null != t.getSampleNum()
							&& !t.getSampleNum().equals(null)
							&& "" != t.getSampleNum()
							&& !t.getSampleNum().equals("")) {
					}
					d.setSampleInfo(sf);
					d.setTechJkServiceTask(sct);
					d.setTjItem(t);
//					d.setUserId(sct.getExperimentLeader().getId());
					techJkServiceTaskDao.saveOrUpdate(d);
				} else if (nextFlowId.equals("0016")) {// 样本处理
					PlasmaTaskTemp d = new PlasmaTaskTemp();
					t.setState("1");
					sampleInputService.copy(d, t);
					d.setSampleInfo(sf);
					d.setTechJkServiceTask(sct);
					d.setTjItem(t);
					techJkServiceTaskDao.saveOrUpdate(d);
				} else if (nextFlowId.equals("0018")) {// 文库构建
					WkTaskTemp w = new WkTaskTemp();
					t.setState("1");
					sampleInputService.copy(w, t);
//					if (t.getSampleNum() != null
//							&& !"".equals(t.getSampleNum())) {
//						w.setSampleNum(Double.parseDouble(t.getSampleNum()));
//					}
//					if (t.getConcentration() != null
//							&& !"".equals(t.getConcentration())) {
//						w.setConcentration(t.getConcentration().toString());
//					}
					if (t.getVolume() != null && !"".equals(t.getVolume())) {
						w.setVolume((t.getVolume()));
					}
					w.setSampleInfo(sf);
					w.setTechJkServiceTask(sct);
					w.setTjItem(t);
//					w.setSequenceBillDate(sct.getSequenceBillDate());
					techJkServiceTaskDao.saveOrUpdate(w);
				} else if (nextFlowId.equals("0019")) {// 纯化质检TechCheckServiceTask
					TechCheckServiceTaskTemp d = new TechCheckServiceTaskTemp();
					sct.setState("1");
					sampleInputService.copy(d, t);
					if (t.getProductId() != null) {
						Product p = commonDAO.get(Product.class,
								t.getProductId());
					}
					d.setTechJkServiceTask(sct);
					d.setTjItem(t);
					techJkServiceTaskDao.saveOrUpdate(d);
				} else if (nextFlowId.equals("0032")) {// 文库混合
//					WkBlendTaskTemp q = new WkBlendTaskTemp();
//					sampleInputService.copy(q, t);
//					if (t.getTechJkServiceTask() != null) {
//						TechJkServiceTask t1 = this.commonDAO.get(
//								TechJkServiceTask.class, t
//										.getTechJkServiceTask().getId());
//					} else {
//						Product p = this.commonDAO.get(Product.class,
//								t.getProductId());
//					}
//					q.setFjwk(t.getCode());
//					q.setSjfz(t.getSampleCode());
//					q.setState("1");
//					q.setSampleStyle("1");
//					q.setDataTraffic(t.getDataTraffic());
//					q.setTechJkServiceTask(sct);
//					q.setIndexa(t.getIndexa());
//					if (t.getConcentration() != null
//							&& !"".equals(t.getConcentration())) {
//						q.setConcentration(t.getConcentration().toString());
//					}
//					if (t.getVolume() != null && !"".equals(t.getVolume())) {
//						q.setVolume((t.getVolume().toString()));
//					}
//					q.setSampleType(t.getSampleType());
//					q.setSumTotal(t.getSampleNum());
//					q.setTjItem(t);
//					techJkServiceTaskDao.saveOrUpdate(q);
				} else if (nextFlowId.equals("0029")) {// 质谱实验
					MassarrayTaskTemp m = new MassarrayTaskTemp();
					t.setState("1");
					sampleInputService.copy(m, t);
					m.setOrderId(sct.getId());

					techJkServiceTaskDao.saveOrUpdate(m);
				} else if (nextFlowId.equals("0028")) {// sanger实验
					SangerTaskTemp s = new SangerTaskTemp();
					t.setState("1");
					sampleInputService.copy(s, t);
					s.setOrderId(sct.getId());
					techJkServiceTaskDao.saveOrUpdate(s);
				} else if (nextFlowId.equals("0030")) {// QPCR实验
					QtTaskTemp q = new QtTaskTemp();
					t.setState("1");
					sampleInputService.copy(q, t);
					q.setOrderId(sct.getId());
					techJkServiceTaskDao.saveOrUpdate(q);
				} else {
					// 得到下一步流向的相关表单
					List<NextFlow> list_nextFlow = nextFlowDao
							.seletNextFlowById(nextFlowId);
					for (NextFlow n : list_nextFlow) {
						Object o = Class.forName(
								n.getApplicationTypeTable().getClassPath())
								.newInstance();
						t.setState("1");
						sampleInputService.copy(o, t);

					}
				}
			}*/

		}
	}

	// 作废
	public void stateNo(String id) {
		TechJkServiceTask sct = techJkServiceTaskDao.get(
				TechJkServiceTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmDate(new Date());
		sct.setConfirmUser(user);

		List<TechJkServiceTaskItem> item = techJkServiceTaskDao.setItemList(sct
				.getId());
		for (TechJkServiceTaskItem t : item) {
			// TechDnaServiceTaskItem tDna = this.commonDAO.get(
			// TechDnaServiceTaskItem.class, t.getTempId());
			// tDna.setState("1");
			// 改变库存样本状态
			SampleInfoIn s = this.commonDAO.get(SampleInfoIn.class, t
					.getSampleInfoIn().getId());
			s.setState("1");

			String nextFlowId = t.getNextFlowId();
			if (nextFlowId.equals("0017")) {// 核酸提取
				List<Object> list = techJkServiceTaskDao.selModel(
						"DnaTaskTemp", t.getId());
				for (int i = 0; i < list.size(); i++) {
					techJkServiceTaskDao.delete(list.get(i));
				}
			} else if (nextFlowId.equals("0018")) {// 文库构建
				// WkTaskTemp w = new WkTaskTemp();
				List<Object> list = techJkServiceTaskDao.selModel("WkTaskTemp",
						t.getId());
				for (int i = 0; i < list.size(); i++) {
					techJkServiceTaskDao.delete(list.get(i));
				}
			}
		}

	}

	/**
	 * 
	 * @Title: showtechnologyWkListBySearchJson
	 * @Description: TODO(质检任务单主查询)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param type
	 * @param @param startcreateDate
	 * @param @param endcreateDate
	 * @param @param startconfirmDate
	 * @param @param endconfirmDate
	 * @param @param startexperimentEndTime
	 * @param @param endexperimentEndTime
	 * @param @param startanalysisEndTime
	 * @param @param endanalysisEndTime
	 * @param @param startsequenceBillDate
	 * @param @param endsequenceBillDate
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-10-30 下午6:41:57
	 * @throws
	 */
	public Map<String, Object> showtechnologyWkListBySearchJson(
			Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort, String type, String startcreateDate,
			String endcreateDate, String startconfirmDate,
			String endconfirmDate, String startexperimentEndTime,
			String endexperimentEndTime, String startanalysisEndTime,
			String endanalysisEndTime, String startsequenceBillDate,
			String endsequenceBillDate) {
		return techJkServiceTaskDao.showtechnologyWkListBySearchJson(map2Query,
				startNum, limitNum, dir, sort, type, startcreateDate,
				endcreateDate, startconfirmDate, endconfirmDate,
				startexperimentEndTime, endexperimentEndTime,
				startanalysisEndTime, endanalysisEndTime,
				startsequenceBillDate, endsequenceBillDate);
	}

	// 库存主数据内选择数据添加到认领任务单明细表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addTechJkServiceTaskServiceItem(String[] ids, String note, 
			String id, String createUser, String createDate,String name,
			String state, String stateName, String type, TechJkServiceTask so) throws Exception {
		// 保存主表信息
		TechJkServiceTask si = new TechJkServiceTask();
		if (id == null || id.length() <= 0
				|| "NEW".equals(id)) {
			String modelName = "TechJkServiceTask";
			String markCode = "TQ";
			/*if (type.equals("0")) {
				markCode = "TQ";
			}
			if (type.equals("1")) {
				markCode = "JK";
			}

			if (type.equals("2")) {
				markCode = "SJ";
			}
			if (type.equals("3")) {
				markCode = "SX";
			}*/
			String code = codingRuleService.genTransID3(modelName, markCode);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User u = commonDAO.get(User.class, createUser);
			si.setCreateUser(u);
			si.setCreateDate(sdf.parse(createDate));
			si.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			si.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			si.setNote(note);
			si.setName(name);
			si.setType(type);
			si.setScopeId((String) ActionContext.getContext().getSession()
					.get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession()
					.get("scopeName"));
			id = si.getId();
			commonDAO.saveOrUpdate(si);
		}else{
			si = commonDAO.get(TechJkServiceTask.class, id);
			si.setNote(note);
			si.setName(name);
			so.setId(id);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(!"".equals(createUser)&&createUser!=null){
				User u = commonDAO.get(User.class, createUser);
				if(u!=null){
					so.setCreateUser(u);
				}
			}
			if(!"".equals(createDate)&&createDate!=null){
				so.setCreateDate(sdf.parse(createDate));
			}else{
				so.setCreateDate(new Date());
			}
			so.setState(state);
			so.setStateName(stateName);
			commonDAO.merge(so);
			si=so;
		}
		for (int i = 0; i < ids.length; i++) {
			String kuc = ids[i];
			SampleOrder s = commonDAO.get(SampleOrder.class, kuc);
			if(s!=null) {
				TechJkServiceTaskItem sii = new TechJkServiceTaskItem();
				sii.setTempId(s.getId());
				sii.setState("1");
				sii.setSampleOrder(s);
				sii.setTechJkServiceTask(si);
				commonDAO.merge(sii);
				s.setState("2");
				commonDAO.saveOrUpdate(s);
			}
				// 通过id查询库存主数据
//				TechDnaServiceTaskItem s = commonDAO.get(TechDnaServiceTaskItem.class, kuc);
				/*if(s!=null){
					TechJkServiceTaskItem sii = new TechJkServiceTaskItem();
					if(s.getScopeId()!=null&&s.getScopeName()!=null) {
						sii.setScopeId(s.getScopeId());
						sii.setScopeName(s.getScopeName());
					}
					sii.setTechJkServiceTask(si);
					if(s.getSampleInfoIn()!=null){
						sii.setCode(s.getSampleInfoIn().getCode());
						if(s.getSampleInfoIn().getSampleInfo()!=null){
							sii.setName(s.getSampleInfoIn().getSampleInfo().getPatientName());
							sii.setProductId(s.getSampleInfoIn().getSampleInfo().getProductId());
							sii.setProductName(s.getSampleInfoIn().getSampleInfo().getProductName());
							if(s.getSampleInfoIn().getSampleInfo().getProject()!=null){
								sii.setProject(s.getSampleInfoIn().getSampleInfo().getProject());
							}
							sii.setCrmDoctor(s.getSampleInfoIn().getSampleInfo().getCrmDoctor());
							sii.setSpecies(s.getSampleInfoIn().getSampleInfo().getSpecies());
						}
						sii.setLocation(s.getSampleInfoIn().getLocation());
						sii.setConcentration(s.getSampleInfoIn().getConcentration());
						sii.setVolume(s.getSampleInfoIn().getVolume());
						if(!"".equals(s.getSampleInfoIn().getSumTotal())&&s.getSampleInfoIn().getSumTotal()!=null){
							sii.setSampleNum(String.valueOf(s.getSampleInfoIn().getSumTotal()));
						}
						sii.setSellPerson(s.getSampleInfoIn().getSampleInfo().getSellPerson());
						sii.setSampleInfoIn(s.getSampleInfoIn());
						sii.setCrmCustomer(s.getSampleInfoIn().getCustomer());
						
					}
					if(s.getSampleOrder()!=null) {
						sii.setSampleOrder(commonDAO.get(SampleOrder.class, s.getSampleOrder().getId()));
					}
					sii.setSampleCode(s.getSampleCode());
					sii.setSampleType(s.getSampleType());
					sii.setTempId(s.getId());
					sii.setState("1");
					sii.setProject(s.getProject());
					sii.setOrderId(s.getOrderId());
					sii.setGroupNo(s.getGroupNo());
					sii.setBarCode(s.getBarCode());
					sii.setDataTraffic(s.getDataTraffic());
					commonDAO.saveOrUpdate(sii);
					s.setState("2");
					commonDAO.saveOrUpdate(s);
				}*/
			}
		/*else if (type.equals("1")) {
			for (int i = 0; i < ids.length; i++) {
				String kuc = ids[i];
				// 通过id查询库存主数据
				SampleInfoIn s = commonDAO.get(SampleInfoIn.class, kuc);
				if(s!=null){
					TechJkServiceTaskItem sii = new TechJkServiceTaskItem();
					if(s.getScopeId()!=null&&s.getScopeName()!=null) {
						sii.setScopeId(s.getScopeId());
						sii.setScopeName(s.getScopeName());
					}
					sii.setTechJkServiceTask(si);
					sii.setCode(s.getCode());
					sii.setSampleCode(s.getSampleCode());
					sii.setSampleType(s.getSampleType());
					if(s.getSampleInfo()!=null){
						if(s.getSampleInfo().getProject()!=null){
							sii.setProject(s.getSampleInfo().getProject());
						}
//						sii.setSampleReceiveDate(s.getSampleInfo().getReceiveDate());
						sii.setGroupNo(s.getSampleInfo().getName());
						sii.setProductId(s.getSampleInfo().getProductId());
						sii.setProductName(s.getSampleInfo().getProductName());
						sii.setName(s.getSampleInfo().getPatientName());
						sii.setCrmDoctor(s.getSampleInfo().getCrmDoctor());
					}
					sii.setLocation(s.getLocation());
					sii.setConcentration(s.getConcentration());
					sii.setVolume(s.getVolume());
					if(!"".equals(s.getSumTotal())&&s.getSumTotal()!=null){
						sii.setSampleNum(String.valueOf(s.getSumTotal()));
					}
					sii.setTempId(s.getId());
					sii.setSampleInfoIn(s);
					sii.setCrmCustomer(s.getCustomer());
					sii.setState("1");
					if(s.getTjItem()!=null){
						sii.setInwardCode(s.getTjItem().getInwardCode());
						sii.setOrderId(s.getTjItem().getOrderId());
					}
					sii.setBarCode(s.getBarCode());
					sii.setDataTraffic(s.getDataTraffic());
					commonDAO.saveOrUpdate(sii);
				}
			}
		}else if (type.equals("2")) {
			for (int i = 0; i < ids.length; i++) {
				String kuc = ids[i];
				// 通过id查询库存主数据
				SampleInfoIn s = commonDAO.get(SampleInfoIn.class, kuc);
				if(s!=null){
					TechJkServiceTaskItem sii = new TechJkServiceTaskItem();
					if(s.getScopeId()!=null&&s.getScopeName()!=null) {
						sii.setScopeId(s.getScopeId());
						sii.setScopeName(s.getScopeName());
					}
					sii.setTechJkServiceTask(si);
					sii.setCode(s.getCode());
					sii.setSampleCode(s.getSampleCode());
					sii.setSampleType(s.getSampleType());
					if(s.getSampleInfo()!=null){
						sii.setProductId(s.getSampleInfo().getProductId());
						sii.setProductName(s.getSampleInfo().getProductName());
						if(s.getSampleInfo().getProject()!=null){
							sii.setProject(s.getSampleInfo().getProject());
						}
						sii.setName(s.getSampleInfo().getPatientName());
						sii.setCrmDoctor(s.getSampleInfo().getCrmDoctor());
						sii.setGroupNo(s.getSampleInfo().getName());
					}
					sii.setLocation(s.getLocation());
					sii.setConcentration(s.getConcentration());
					sii.setVolume(s.getVolume());
					if(!"".equals(s.getSumTotal())&&s.getSumTotal()!=null){
						sii.setSampleNum(String.valueOf(s.getSumTotal()));
					}
					sii.setTempId(s.getId());
					sii.setSampleInfoIn(s);
					sii.setCrmCustomer(s.getCustomer());
					sii.setState("1");
					if(s.getTjItem()!=null){
						sii.setInwardCode(s.getTjItem().getInwardCode());
						sii.setOrderId(s.getTjItem().getOrderId());
					}
					sii.setBarCode(s.getBarCode());
					sii.setIndexa(s.getIndexa());
					sii.setDataTraffic(s.getDataTraffic());
					commonDAO.saveOrUpdate(sii);
				}
			}
		}else if (type.equals("3")) {

			for (int i = 0; i < ids.length; i++) {
				String kuc = ids[i];
				// 通过id查询库存主数据
				DeSequencingTaskItem s = commonDAO.get(DeSequencingTaskItem.class, kuc);
				if(s!=null){
					TechJkServiceTaskItem sii = new TechJkServiceTaskItem();
					sii.setTechJkServiceTask(si);
					sii.setSampleCode(s.getSampleCode());
					sii.setOrderCode(s.getOrderCode());
					sii.setProjectId(s.getProjectId());
					sii.setSampleID(s.getSampleID());
					sii.setSxProductName(s.getProductName());
					sii.setRunId(s.getRunId());
					sii.setTempId(s.getId());
					sii.setState("1");
					commonDAO.saveOrUpdate(sii);
				}
			}
		}*/
		return id;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public TechJkServiceTask saveTechJkServiceTaskById(TechJkServiceTask so,
			String id, String note, String name, String changeLog,
			String changeLogItem, Map jsonMap, String createDate,
			String createUser, String state, String stateName) throws Exception {
		if("".equals(id)||id==null||"NEW".equals(id)){
			String modelName = "TechJkServiceTask";
			String markCode = "TQ";
			/*if (so.getType().equals("0")) {
				markCode = "TQ";
			}
			if (so.getType().equals("1")) {
				markCode = "JK";
			}

			if (so.getType().equals("2")) {
				markCode = "SJ";
			}
			if (so.getType().equals("3")) {
				markCode = "SX";
			}*/
			String code = codingRuleService.genTransID3(modelName, markCode);
			so.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(!"".equals(createUser)&&createUser!=null){
				User u = commonDAO.get(User.class, createUser);
				if(u!=null){
					so.setCreateUser(u);
				}
			}
			
			if(!"".equals(createDate)&&createDate!=null){
				so.setCreateDate(sdf.parse(createDate));
			}else{
				so.setCreateDate(new Date());
			}
			so.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			so.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			so.setNote(note);
			so.setName(name);
			so.setScopeId((String) ActionContext.getContext().getSession()
					.get("scopeId"));
			so.setScopeName((String) ActionContext.getContext().getSession()
					.get("scopeName"));
			id = so.getId();
			commonDAO.saveOrUpdate(so);
		}else{
//			so = commonDAO.get(TechJkServiceTask.class, id);
			so.setId(id);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if(!"".equals(createUser)&&createUser!=null){
				User u = commonDAO.get(User.class, createUser);
				if(u!=null){
					so.setCreateUser(u);
				}
			}
			if(!"".equals(createDate)&&createDate!=null){
				so.setCreateDate(sdf.parse(createDate));
			}else{
				so.setCreateDate(new Date());
			}
				so.setState(state);
				so.setStateName(stateName);
			commonDAO.saveOrUpdate(so);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(so.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		String jsonStr = "";
		jsonStr = (String) jsonMap.get("ImteJson");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveItem(so, jsonStr, changeLogItem);
		}
		return so;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveItem(TechJkServiceTask newSo, String itemDataJson, String logInfo) throws Exception {
		List<TechJkServiceTaskItem> saveItems = new ArrayList<TechJkServiceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TechJkServiceTaskItem scp = new TechJkServiceTaskItem();
			// 将map信息读入实体类
			scp = (TechJkServiceTaskItem) commonDAO.Map2Bean(map, scp);
			String wgcId = scp.getWgcId();
			String orderType = scp.getOrderType();
			String isStain = scp.getIsStain();
			String isNeedStain = scp.getIsNeedStain();
			String note = scp.getNote();
			SampleInfoIn sampleInfoIn = scp.getSampleInfoIn();
			String tempId = scp.getTempId();
			String projectId = scp.getProjectId();
			Project project = scp.getProject();
			String productId = scp.getProductId();
			String productName = scp.getProductName();
			String nextFlowId = scp.getNextFlowId();
			String nextFlow = scp.getNextFlow();
			if(scp.getId()!=null) {
				scp=commonDAO.get(TechJkServiceTaskItem.class, scp.getId());
				scp.setWgcId(wgcId);
				scp.setOrderType(orderType);
				scp.setIsStain(isStain);
				scp.setIsNeedStain(isNeedStain);
				scp.setNote(note);
				scp.setSampleInfoIn(sampleInfoIn);
				scp.setTempId(tempId);
				scp.setProjectId(projectId);
				scp.setProject(project);
				scp.setProductId(productId);
				scp.setProductName(productName);
				scp.setNextFlowId(nextFlowId);
				scp.setNextFlow(nextFlow);
				
			}
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setTechJkServiceTask(newSo);

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSo.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}
}
