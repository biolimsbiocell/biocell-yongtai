package com.biolims.technology.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.technology.dao.TechServiceTaskDao;
import com.biolims.technology.model.TechServiceTask;
import com.biolims.technology.model.TechServiceTaskItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TechServiceTaskService {
	@Resource
	private TechServiceTaskDao techServiceTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findTechServiceTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return techServiceTaskDao.selectTechServiceTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechServiceTask i) throws Exception {

		techServiceTaskDao.saveOrUpdate(i);

	}
	public TechServiceTask get(String id) {
		TechServiceTask techServiceTask = commonDAO.get(TechServiceTask.class, id);
		return techServiceTask;
	}
	public Map<String, Object> findTechServiceTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = techServiceTaskDao.selectTechServiceTaskItemList(scId, startNum, limitNum, dir, sort);
		List<TechServiceTaskItem> list = (List<TechServiceTaskItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTechServiceTaskItem(TechServiceTask sc, String itemDataJson) throws Exception {
		List<TechServiceTaskItem> saveItems = new ArrayList<TechServiceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TechServiceTaskItem scp = new TechServiceTaskItem();
			// 将map信息读入实体类
			scp = (TechServiceTaskItem) techServiceTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTechServiceTask(sc);

			saveItems.add(scp);
		}
		techServiceTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTechServiceTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			TechServiceTaskItem scp =  techServiceTaskDao.get(TechServiceTaskItem.class, id);
			 techServiceTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechServiceTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			techServiceTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("techServiceTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTechServiceTaskItem(sc, jsonStr);
			}
	}
   }
}
