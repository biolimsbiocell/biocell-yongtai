﻿package com.biolims.technology.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.technology.model.TechServiceTask;
import com.biolims.technology.model.TechServiceTaskItem;
import com.biolims.technology.service.TechServiceTaskService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/technology/techServiceTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TechServiceTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2801";
	@Autowired
	private TechServiceTaskService techServiceTaskService;
	private TechServiceTask techServiceTask = new TechServiceTask();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showTechServiceTaskList")
	public String showTechServiceTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/techServiceTask.jsp");
	}

	@Action(value = "showTechServiceTaskListJson")
	public void showTechServiceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techServiceTaskService
				.findTechServiceTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<TechServiceTask> list = (List<TechServiceTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "techServiceTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTechServiceTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/technology/techServiceTaskDialog.jsp");
	}

	@Action(value = "showDialogTechServiceTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTechServiceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techServiceTaskService
				.findTechServiceTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<TechServiceTask> list = (List<TechServiceTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editTechServiceTask")
	public String editTechServiceTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			techServiceTask = techServiceTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "techServiceTask");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// techServiceTask.setCreateUser(user);
			// techServiceTask.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/technology/techServiceTaskEdit.jsp");
	}

	@Action(value = "copyTechServiceTask")
	public String copyTechServiceTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		techServiceTask = techServiceTaskService.get(id);
		techServiceTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/technology/techServiceTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = techServiceTask.getId();
		if (id != null && id.equals("")) {
			techServiceTask.setId(null);
		}
		Map aMap = new HashMap();
		aMap.put("techServiceTaskItem",
				getParameterFromRequest("techServiceTaskItemJson"));

		techServiceTaskService.save(techServiceTask, aMap);
		return redirect("/technology/techServiceTask/editTechServiceTask.action?id="
				+ techServiceTask.getId());

	}

	@Action(value = "viewTechServiceTask")
	public String toViewTechServiceTask() throws Exception {
		String id = getParameterFromRequest("id");
		techServiceTask = techServiceTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/technology/techServiceTaskEdit.jsp");
	}

	@Action(value = "showTechServiceTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechServiceTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/technology/techServiceTaskItem.jsp");
	}

	@Action(value = "showTechServiceTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechServiceTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = techServiceTaskService
					.findTechServiceTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<TechServiceTaskItem> list = (List<TechServiceTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("state", "");
			map.put("techServiceTask-name", "");
			map.put("techServiceTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTechServiceTaskItem")
	public void delTechServiceTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			techServiceTaskService.delTechServiceTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TechServiceTaskService getTechServiceTaskService() {
		return techServiceTaskService;
	}

	public void setTechServiceTaskService(
			TechServiceTaskService techServiceTaskService) {
		this.techServiceTaskService = techServiceTaskService;
	}

	public TechServiceTask getTechServiceTask() {
		return techServiceTask;
	}

	public void setTechServiceTask(TechServiceTask techServiceTask) {
		this.techServiceTask = techServiceTask;
	}

}
