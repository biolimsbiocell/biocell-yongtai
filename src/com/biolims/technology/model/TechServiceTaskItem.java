package com.biolims.technology.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInputTechnology;

/**
 * @Title: Model
 * @Description: 科技服务明细
 * @author lims-platform
 * @date 2016-02-18 11:36:53
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TECH_SERVICE_TASK_ITEM")
@SuppressWarnings("serial")
public class TechServiceTaskItem extends EntityDao<TechServiceTaskItem>
		implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String sampleCode;

	private SampleInputTechnology sampleInput;
	/** 状态 */
	private String state;
	/** 相关主表 */
	private TechServiceTask techServiceTask;
	private Project project;
	// 客户
	private CrmCustomer crmCustomer;
	// 医生
	private CrmDoctor crmDoctor;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	/**
	 * 科研项目
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得TechServiceTask
	 * 
	 * @return: TechServiceTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_SERVICE_TASK")
	public TechServiceTask getTechServiceTask() {
		return this.techServiceTask;
	}

	/**
	 * 方法: 设置TechServiceTask
	 * 
	 * @param: TechServiceTask 相关主表
	 */
	public void setTechServiceTask(TechServiceTask techServiceTask) {
		this.techServiceTask = techServiceTask;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INPUT_TECHNOLOGY")
	public SampleInputTechnology getSampleInput() {
		return sampleInput;
	}

	public void setSampleInput(SampleInputTechnology sampleInput) {
		this.sampleInput = sampleInput;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

}