package com.biolims.util;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Title: 1将对象的kay动态赋值给map的kay
 * @Description:
 * @author :
 * @刘青松
 * @date 2019年3月6日下午12:33:14
 * @return
 */
public class ObjectToMapUtils {
	/*
	 * 将对象的属性名赋值给map的key
	 */
	public static Object getMapKey(Object obj) {
		Field[] fields = obj.getClass().getDeclaredFields();
		String[] fieldNames = new String[fields.length];
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < fields.length; i++) {
			map.put(fields[i].getName(), "");
		}
		return map;
	}

	/*
	 * 时间格式化    字符串
	 */
	public static String getTimeString() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String string = format.format(new Date());
		return string;
	}
	
	/*
	 * 时间格式化  日期加小时
	 */
	public static String getTimeInfosString() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String string = format.format(new Date());
		return string;
	}
	
	/*
	 * 两个对象字段名相同得值对copy
	 */
/*	public static Object getObjectToObject(Object obj,Object obj1) {
		Field[] fields = obj.getClass().getDeclaredFields();
		Field[] fields1 = obj1.getClass().getDeclaredFields();
		String[] fieldNames = new String[fields.length];
		String[] fieldNames1 = new String[fields1.length];
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < fields.length; i++) {
			for (int j = 0; j < fields1.length; j++) {
				if(fields[i].getName().equals(fields1[j])){
					
				}
				map.put(fields[i].getName(), "");
			}
		}
		return map;
	}*/
}
