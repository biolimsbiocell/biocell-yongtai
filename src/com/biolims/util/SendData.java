package com.biolims.util;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

public class SendData {

	private String formatBean(Map<String, String> propertiesMap, List<?> data) throws Exception {
		StringBuffer str = new StringBuffer();
		for (Iterator<?> i = data.iterator(); i.hasNext();) {
			StringBuffer sb = new StringBuffer("{");
			Object bean = i.next();
			for (String propertie : propertiesMap.keySet()) {
				String[] properties = propertie.split("[-]");

				if (sb.toString().equals("{"))
					sb.append("\'").append(propertie).append("\':\'");
				else
					sb.append(",\'").append(propertie).append("\':\'");

				Field field = null;
				Object temp = bean;
				if (temp != null) {
					for (String pro : properties) {
						if (temp != null) {
							Class<?> cc = temp.getClass();
							field = cc.getDeclaredField(pro);
							field.setAccessible(true);
							temp = field.get(temp);
						}
					}
					if (temp != null) {
						if (propertie.equals(propertiesMap.get(propertie))) {
							sb.append(temp.toString());
						} else {
							if (temp.getClass().getName().equals("java.lang.Double")) {
								sb.append(this.formatDouble((Double) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.util.Date")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.sql.Timestamp")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.lang.String")) {
								sb.append(this.formatStrForWeb(temp.toString()));
							} else {
								sb.append(temp.toString());
							}
						}
					}
					sb.append("\'");
				}
			}
			sb.append("}");

			str.append(sb.toString());
			//	str.append(sb.toString().replace(":\'", ":$$").replace("\'\'", "\',\'").replace(":$$", ":\'"));
		}
		return str.toString();
	}

	private String formatBeanFormatStr(Map<String, String> propertiesMap, List<?> data) throws Exception {
		StringBuffer str = new StringBuffer();
		for (Iterator<?> i = data.iterator(); i.hasNext();) {
			StringBuffer sb = new StringBuffer("{");
			Object bean = i.next();
			for (String propertie : propertiesMap.keySet()) {
				String[] properties = propertie.split("[-]");

				if (sb.toString().equals("{"))
					sb.append("\"").append(propertie).append("\":\"");
				else
					sb.append(",\"").append(propertie).append("\":\"");

				Field field = null;
				Object temp = bean;
				if (temp != null) {
					for (String pro : properties) {
						if (temp != null) {
							Class<?> cc = temp.getClass();
							field = cc.getDeclaredField(pro);
							field.setAccessible(true);
							temp = field.get(temp);
						}
					}
					if (temp != null) {
						if (propertie.equals(propertiesMap.get(propertie))) {
							sb.append(temp.toString());
						} else {
							if (temp.getClass().getName().equals("java.lang.Double")) {
								sb.append(this.formatDouble((Double) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.util.Date")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.sql.Timestamp")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.lang.String")) {
								sb.append(this.formatStr(temp.toString()));
							} else {
								sb.append(temp.toString());
							}
						}
					}
					sb.append("\"");
				}
			}
			sb.append("}");

			str.append(sb.toString());
			//	str.append(sb.toString().replace(":\'", ":$$").replace("\'\'", "\',\'").replace(":$$", ":\'"));
		}
		return str.toString();
	}
	
	private String formatBeanFormatStr1(Map<String, String> propertiesMap, List<?> data) throws Exception {
		StringBuffer str = new StringBuffer();
		for (Iterator<?> i = data.iterator(); i.hasNext();) {
			StringBuffer sb = new StringBuffer("{");
			Object bean = i.next();
			for (String propertie : propertiesMap.keySet()) {
				String[] properties = propertie.split("[-]");

				if (sb.toString().equals("{"))
					sb.append("\"").append(propertie).append("\":\"");
				else
					sb.append(",\"").append(propertie).append("\":\"");

				Field field = null;
				Object temp = bean;
				if (temp != null) {
					for (String pro : properties) {
						if (temp != null) {
							Class<?> cc = temp.getClass();
							field = cc.getDeclaredField(pro);
							field.setAccessible(true);
							temp = field.get(temp);
						}
					}
					if (temp != null) {
						if (propertie.equals(propertiesMap.get(propertie))) {
							sb.append(temp.toString());
						} else {
							if (temp.getClass().getName().equals("java.lang.Double")) {
								sb.append(this.formatDouble((Double) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.util.Date")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.sql.Timestamp")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.lang.String")) {
								sb.append(this.formatStr1(temp.toString()));
							} else {
								sb.append(temp.toString());
							}
						}
					}
					sb.append("\"");
				}
			}
			sb.append("}");

			str.append(sb.toString());
			//	str.append(sb.toString().replace(":\'", ":$$").replace("\'\'", "\',\'").replace(":$$", ":\'"));
		}
		return str.toString();
	}

	public void sendDateJson(Map<String, String> propertiesMap, List<?> data, long pageSize,
			HttpServletResponse response) {
		try {
			String dataStr = this.formatBean(propertiesMap, data);
			StringBuffer sb = new StringBuffer("{\"total\":").append(pageSize).append(",\"results\":[").append(dataStr)
					.append("]}");
			String str = sb.toString().replace("}{", "},{");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			//System.out.print(str);
			response.getWriter().write(str);
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendDataJsonWeb(Map<String, String> propertiesMap, List<?> data, long pageSize,
			HttpServletResponse response) {
		try {
			String dataStr = this.formatWebStr(propertiesMap, data);
			StringBuffer sb = new StringBuffer("{\"total\":").append(pageSize).append(",\"results\":[").append(dataStr)
					.append("]}");
			String str = sb.toString().replace("}{", "},{");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			//System.out.print(str);
			response.getWriter().write(str);
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String formatWebStr(Map<String, String> propertiesMap, List<?> data) throws Exception {
		StringBuffer str = new StringBuffer();
		for (Iterator<?> i = data.iterator(); i.hasNext();) {
			StringBuffer sb = new StringBuffer("{");
			Object bean = i.next();
			for (String propertie : propertiesMap.keySet()) {
				String[] properties = propertie.split("[-]");

				if (sb.toString().equals("{"))
					sb.append("\'").append(propertie).append("\':\'");
				else
					sb.append(",\'").append(propertie).append("\':\'");

				Field field = null;
				Object temp = bean;
				if (temp != null) {
					for (String pro : properties) {
						if (temp != null) {
							Class<?> cc = temp.getClass();
							field = cc.getDeclaredField(pro);
							field.setAccessible(true);
							temp = field.get(temp);
						}
					}
					if (temp != null) {
						if (propertie.equals(propertiesMap.get(propertie))) {
							sb.append(temp.toString());
						} else {
							if (temp.getClass().getName().equals("java.lang.Double")) {
								sb.append(this.formatDouble((Double) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.util.Date")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.sql.Timestamp")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.lang.String")) {
								sb.append(this.formatStrForHTML(temp.toString()));
							} else {
								sb.append(temp.toString());
							}
						}
					}
					sb.append("\'");
				}
			}
			sb.append("}");

			str.append(sb.toString());
			//	str.append(sb.toString().replace(":\'", ":$$").replace("\'\'", "\',\'").replace(":$$", ":\'"));
		}
		return str.toString();
	}

	private String formatHTMLStr(Map<String, String> propertiesMap, List<?> data) throws Exception {
		StringBuffer str = new StringBuffer();
		for (Iterator<?> i = data.iterator(); i.hasNext();) {
			StringBuffer sb = new StringBuffer("{");
			Object bean = i.next();
			for (String propertie : propertiesMap.keySet()) {

				String[] properties = propertie.split("[-]");

				if (sb.toString().equals("{"))
					sb.append("\'").append(propertie).append("\':\'");
				else
					sb.append(",\'").append(propertie).append("\':\'");
				Field field = null;
				Object temp = bean;
				if (temp != null) {
					for (String pro : properties) {
						if (temp != null) {
							Class<?> cc = temp.getClass();
							field = cc.getDeclaredField(pro);
							field.setAccessible(true);
							temp = field.get(temp);
						}
					}
					if (temp != null) {
						if (propertie.equals(propertiesMap.get(propertie))) {
							sb.append(temp.toString());
						} else {
							if (temp.getClass().getName().equals("java.lang.Double")) {
								sb.append(this.formatDouble((Double) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.util.Date")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.sql.Timestamp")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.lang.String")) {
								sb.append(this.formatStrForHTML(temp.toString()));
							} else {
								sb.append(temp.toString());
							}
						}
					}
					sb.append("\'");
				}
			}
			sb.append("}");
			//str.append(sb.toString().replace(":\'", ":$$").replace("\'\'", "\',\'").replace(":$$", ":\'"));
			str.append(sb.toString());

		}
		return str.toString();
	}

	public void sendDataJson(String jsonStr, HttpServletResponse response) {
		try {
			jsonStr = jsonStr.replace("\n", " ").replace("\r", " ").replace("\t", " ");
			//System.out.print(jsonStr);
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write("" + jsonStr + "");
			response.getWriter().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendDataJsonFormatStr(String jsonStr, HttpServletResponse response) {
		try {

			jsonStr = formatStr(jsonStr);
			jsonStr = jsonStr.replace("\n", " ").replace("\r", " ").replace("\t", " ");
			//System.out.print(jsonStr);
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().write("" + jsonStr + "");
			response.getWriter().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendDateJsonFormatStr(Map<String, String> propertiesMap, List<?> data, long pageSize,
			HttpServletResponse response) {
		try {
			String dataStr = this.formatBeanFormatStr(propertiesMap, data);

			StringBuffer sb = new StringBuffer("{\'total\':").append(pageSize).append(",\'results\':[").append(dataStr)
					.append("]}");
			String str = sb.toString().replace("}{", "},{");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			//System.out.print(str);
			response.getWriter().write(str);
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getDateJsonForDatatable(Map<String, String> propertiesMap, List<?> data) throws Exception{
		String str="";
		if(data!=null){
			String dataStr = this.formatBeanFormatStr(propertiesMap, data);
			StringBuffer sb = new StringBuffer("[").append(dataStr)
					.append("]");
			str = sb.toString().replace("}{", "},{");
		}else{
			str="[]";
		}
		return str;
}
	public String getDateJsonForDatatable1(Map<String, String> propertiesMap, List<?> data) throws Exception{
		String str="";
		if(data!=null){
			String dataStr = this.formatBeanFormatStr1(propertiesMap, data);
			StringBuffer sb = new StringBuffer("[").append(dataStr)
					.append("]");
			str = sb.toString().replace("}{", "},{");
		}else{
			str="[]";
		}
		return str;
	}
	
	private String formatDouble(Double dou, String str) {
		if (!str.equals("")) {
			DecimalFormat df = new DecimalFormat(str);
			return df.format(dou);
		} else {
			String douStr = String.valueOf(dou);
			if (douStr.endsWith(".0")) {

				int n = douStr.lastIndexOf(".");
				douStr = douStr.substring(0, n);

			}
			return douStr;

		}

	}

	private String formatDate(Date date, String str) {
		SimpleDateFormat df = new SimpleDateFormat(str);
		return df.format(date);
	}

	private String formatStr(String str) {
		char[] c = str.toCharArray();
		for (int i = 0; i < c.length; i++) {
			//'

			if (c[i] == 32) {
//				c[i] = (char) 12288;
				continue;
			}
			if (c[i] > 47 && c[i] < 58) {
				continue;
			}
			if (c[i] > 64 && c[i] < 91) {
				continue;
			}
			if (c[i] > 96 && c[i] < 123) {
				continue;
			}
			if (c[i] > 123 && c[i] < 127)//{ | } ~
				//c[i] = (char) (c[i] + 65248);
				continue;
		}
		return new String(c).toString().replace("\n", " ").replace("\r", " ").replace("\t", " ").replace("\"", "'");
	}
	
	private String formatStr1(String str) {
		char[] c = str.toCharArray();
		for (int i = 0; i < c.length; i++) {
			//'

			if (c[i] == 32) {
//				c[i] = (char) 12288;
				continue;
			}
			if (c[i] > 47 && c[i] < 58) {
				continue;
			}
			if (c[i] > 64 && c[i] < 91) {
				continue;
			}
			if (c[i] > 96 && c[i] < 123) {
				continue;
			}
			if (c[i] > 123 && c[i] < 127)//{ | } ~
				//c[i] = (char) (c[i] + 65248);
				continue;
		}
		return new String(c).toString().replace("\n", "\\n").replace("\r", "\\r").replace("\t", "\\t").replace("\"", "'");
	}

	private String formatStrForWeb(String s) {

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			switch (c) {
			case '\'':
				sb.append("\\\'");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '/':
				sb.append("\\/");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			case '"':
				sb.append("\"");
				break;	
			default:
				sb.append(c);
			}
		}
		return sb.toString();

	}

	private String formatStrForHTML(String s) {

		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			switch (c) {
			case '\'':
				sb.append("\\\'");
				break;
			case '\\':
				sb.append("\\\\");
				break;
			case '/':
				sb.append("\\/");
				break;
			case '\b':
				sb.append("\\b");
				break;
			case '\f':
				sb.append("\\f");
				break;
			case '\n':
				sb.append("\\n");
				break;
			case '\r':
				sb.append("\\r");
				break;
			case '\t':
				sb.append("\\t");
				break;
			default:
				sb.append(c);
			}
		}
		return sb.toString();

		//return str.replace("\n", "<BR>").replace("\r", "<BR>").replace("\t", " ");
	}

	private String formatBeanJson(Map<String, String> propertiesMap, List<?> data) throws Exception {
		StringBuffer str = new StringBuffer();
		for (Iterator<?> i = data.iterator(); i.hasNext();) {
			StringBuffer sb = new StringBuffer("{");
			Object bean = i.next();
			for (String propertie : propertiesMap.keySet()) {
				String[] properties = propertie.split("[-]");

				if (sb.toString().equals("{"))
					sb.append("\"").append(propertie).append("\":\"");
				else
					sb.append(",\"").append(propertie).append("\":\"");

				Field field = null;
				Object temp = bean;
				if (temp != null) {
					for (String pro : properties) {
						if (temp != null) {
							Class<?> cc = temp.getClass();
							field = cc.getDeclaredField(pro);
							field.setAccessible(true);
							temp = field.get(temp);
						}
					}
					if (temp != null) {
						if (propertie.equals(propertiesMap.get(propertie))) {
							sb.append(temp.toString());
						} else {
							if (temp.getClass().getName().equals("java.lang.Double")) {
								sb.append(this.formatDouble((Double) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.util.Date")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.sql.Timestamp")) {
								sb.append(this.formatDate((Date) temp, propertiesMap.get(propertie)));
							} else if (temp.getClass().getName().equals("java.lang.String")) {
								sb.append(this.formatStrForWeb(temp.toString()));
							} else {
								sb.append(temp.toString());
							}
						}
					}
					sb.append("\"");
				}
			}
			sb.append("}");

			str.append(sb.toString());
			//	str.append(sb.toString().replace(":\'", ":$$").replace("\'\'", "\',\'").replace(":$$", ":\'"));
		}
		return str.toString();
	}

	public void sendDataJson(Map<String, String> propertiesMap, List<?> data, long pageSize,
			HttpServletResponse response) {
		try {
			String dataStr = this.formatBeanJson(propertiesMap, data);
			StringBuffer sb = new StringBuffer("{\"total\":").append(pageSize).append(",\"results\":[").append(dataStr)
					.append("]}");
			String str = sb.toString().replace("}{", "},{");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			//System.out.print(str);
			response.getWriter().write(str);
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendDataJsonp(Map<String, String> propertiesMap, List<?> data, long pageSize,
			HttpServletResponse response, String jsonpcallback) {
		try {
			String dataStr = this.formatBeanJson(propertiesMap, data);
			StringBuffer sb = new StringBuffer();
			if (!jsonpcallback.equals(""))
				sb = new StringBuffer(jsonpcallback + "({\"total\":").append(pageSize).append(",\"results\":[").append(
						dataStr).append("]})");
			else
				sb = new StringBuffer("{\"total\":").append(pageSize).append(",\"results\":[").append(dataStr).append(
						"]}");
			String str = sb.toString().replace("}{", "},{");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
			//System.out.print(str);
			response.getWriter().write(str);
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
