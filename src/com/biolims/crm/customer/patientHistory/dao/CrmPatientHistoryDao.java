package com.biolims.crm.customer.patientHistory.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
import com.biolims.crm.customer.patient.model.CrmPatientTreat;
import com.biolims.crm.customer.patient.model.CrmPatientTumorInformation;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.crm.customer.patientHistory.model.CrmConsumerMarketHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientDiagnosisHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientGeneticTestingHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientItemHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientLaboratoryHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientLinkManHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientPathologyHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientPersonnelHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientRadiologyHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientSurgeriesHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientTreatHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientTumorInformationHistory;
import com.biolims.crm.customer.patientHistory.model.CrmTumourPositionHistory;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class CrmPatientHistoryDao extends BaseHibernateDao {
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> selectCrmPatientHistoryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type, String userId) {
		String key = " ";
		String hql = " from CrmPatientHistory where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		//		if (type != null && userId != null) {
		//			if (type.equals("false")) {
		//
		//				key += " and (crmCustomerId.dutyManId.id = '" + userId + "' or createUser.id = '" + userId + "')";
		//			}
		//		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatientHistory> list = new ArrayList<CrmPatientHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	public Map<String, Object> selectCrmPatientHistoryTwoList(Map<String, String> mapForQuery,String name, Integer startNum,
			Integer limitNum, String dir, String sort, String type, String userId) {
		String key = " ";
		String hql = " from CrmPatientHistory where 1=1 and name = '"+name+"'";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatientHistory> list = new ArrayList<CrmPatientHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmPatientHistoryList(String cId, Integer startNum, Integer limitNum, String dir,
			String sort, String type) {
		String key = " ";
		String hql = " from CrmPatientHistory where 1=1 and customer.id = '" + cId + "' or  customerDoctor.id ='" + cId + "'";

		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatientHistory> list = new ArrayList<CrmPatientHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	

	public Map<String, Object> selectCrmPatientDiagnosisHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientDiagnosisHistory where 1=1 and num = 1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientDiagnosisHistory> list = new ArrayList<CrmPatientDiagnosisHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by dateOfDiagnosis DESC " + sort + " " + dir;
			} else {
				key = key + " order by dateOfDiagnosis DESC ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientRestsHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientDiagnosisHistory where 1=1 and num = 2";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientDiagnosisHistory> list = new ArrayList<CrmPatientDiagnosisHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by dateOfDiagnosis DESC " + sort + " " + dir;
			} else {
				key = key + " order by dateOfDiagnosis DESC ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientTumorInformationsHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientTumorInformationHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientTumorInformationHistory> list = new ArrayList<CrmPatientTumorInformationHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientRadiologyHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientRadiologyHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientRadiologyHistory> list = new ArrayList<CrmPatientRadiologyHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  dateOfExamination DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientLaboratoryHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientLaboratoryHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientLaboratoryHistory> list = new ArrayList<CrmPatientLaboratoryHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  dateOfExamination DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientLinkManHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientLinkManHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientLinkManHistory> list = new ArrayList<CrmPatientLinkManHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 订单
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmPatientOrderList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleOrder where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and medicalNumber ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmSampleInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort,String number) throws Exception {
		String hql = "from SampleInfo t where 1=1 ";
		String key = "";
		if (scId != number)
			key = key + " and t.orderNum ='" + number + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 确认收费
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmCustormerList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort,String number) throws Exception {
		String hql = "from CrmConsumerMarket t where 1=1 ";
		String key = "";
		if (scId != number)
			key = key + " and t.sampleOrder.id ='" + number + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarket> list = new ArrayList<CrmConsumerMarket>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientPathologyHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientPathologyHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientPathologyHistory> list = new ArrayList<CrmPatientPathologyHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  dateOfExamination DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientTreatHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientTreatHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientTreatHistory> list = new ArrayList<CrmPatientTreatHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  begin DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientSurgeriesHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientSurgeriesHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientSurgeriesHistory> list = new ArrayList<CrmPatientSurgeriesHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by dateOfProcedureAndSurgery DESC" + sort + " " + dir;
			} else {
				key = key + " order by dateOfProcedureAndSurgery DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientGeneticTestingHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientGeneticTestingHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientGeneticTestingHistory> list = new ArrayList<CrmPatientGeneticTestingHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by dateOfTest DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> cusPositionHistory(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from CrmTumourPositionHistory where 1=1 ";
		key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmTumourPositionHistory> list = new ArrayList<CrmTumourPositionHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	@SuppressWarnings("unchecked")
	public void delModelId(String objName, String objProperty, String vals) throws Exception {
		String hql = " delete from " + objName + " where " + objProperty + " in (" + vals + ")";
		Query query = this.getSession().createQuery(hql);
		query.executeUpdate();
	}

	public Map<String, Object> selectConsumerMarketHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmConsumerMarketHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarketHistory> list = new ArrayList<CrmConsumerMarketHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by consumptionTime DESC";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<CrmConsumerMarketHistory> findCrmConsumerMarketHistory(String type) {
		List<CrmConsumerMarketHistory> list = commonDAO.find("from CrmConsumerMarketHistory  order by isFee asc");
		return list;

	}

	public List<CrmPatientPathologyHistory> findCrmPatientPathologyHistory(String type) {
		List<CrmPatientPathologyHistory> list = commonDAO
				.find("from CrmPatientPathologyHistory where crmPatientHistory.id = '" + type + "'");
		return list;

	}
	public Map<String, Object> selectConsumerMarketHistoryList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmConsumerMarketHistory where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarketHistory> list = new ArrayList<CrmConsumerMarketHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//
	//	public <table> List<table> findTableWhere(String table, String where, String type) {
	//		String hql = "";
	//		hql = "from " + table;
	//		if (where != null && type != null) {
	//			hql = hql + " where " + where + " = '" + type + "'";
	public <table> List<table> findTablehql(String table, String where) {
		List<table> list = commonDAO.find(where);
		return list;
	}

	public <table> List<table> findTableWhere(String table, String where, String type) {
		String hql = "";
		hql = "from " + table;
		if (where != null && type != null) {
			hql = hql + " where " + where + " = " + type + "";

		}
		hql = hql + " order by id asc ";
		List<table> list = commonDAO.find(hql);
		return list;
	}

	public <table> List<table> findTableWhere2(String table, String where, String type, String where2, String type2) {
		String hql = "";
		hql = "from " + table;
		if (where != null && type != null && where2 != null && type2 != null) {
			hql = hql + " where " + where + " = " + type + " and " + where2 + " = " + type2 + "";
		}
		hql = hql + " order by id asc ";
		List<table> list = commonDAO.find(hql);
		return list;
	}

	public Map<String, Object> selectCrmLinkManItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmLinkManItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmLinkManItem> list = new ArrayList<CrmLinkManItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by createDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//查询该记录是否存在
	public boolean queryByCount (String number){
		String hql = " from CrmPatient  where id = '"+number+"'";
		List<CrmPatient>list= this.getSession().createQuery(hql).list();
		if(list.size()>0){
			return true;
		}
		return false;
	}
	//添加明细
	public Map<String, Object> selectCrmPatientPersonnelHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientPersonnelHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientPersonnelHistory> list = new ArrayList<CrmPatientPersonnelHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectCrmPatientItemHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientItemHistory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatientHistory.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientItemHistory> list = new ArrayList<CrmPatientItemHistory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		//根据电子病历号查询订单编号
		public SampleOrder queryByOrderId(String id){
			String hql = " from SampleOrder  where medicalNumber = '"+id+"'";
			SampleOrder order=(SampleOrder) this.getSession().createQuery(hql).list().get(0);
			return order;
		}
	

}