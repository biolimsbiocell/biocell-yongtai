//package com.biolims.crm.customer.patientHistory.action;
//
//import java.net.URLDecoder;
//import java.net.URLEncoder;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
//import org.apache.struts2.ServletActionContext;
//import org.apache.struts2.convention.annotation.Action;
//import org.apache.struts2.convention.annotation.InterceptorRef;
//import org.apache.struts2.convention.annotation.Namespace;
//import org.apache.struts2.convention.annotation.ParentPackage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Controller;
//
//import com.biolims.common.action.BaseActionSupport;
//import com.biolims.common.constants.SystemConstants;
//import com.biolims.common.model.user.User;
//import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
//import com.biolims.crm.customer.linkman.service.CrmLinkManService;
//import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
//import com.biolims.crm.customer.patient.model.CrmPatient;
//import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
//import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
//import com.biolims.crm.customer.patient.model.CrmPatientItem;
//import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
//import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
//import com.biolims.crm.customer.patient.model.CrmPatientPathology;
//import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
//import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
//import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
//import com.biolims.crm.customer.patient.model.CrmPatientTreat;
//import com.biolims.crm.customer.patient.service.CrmPatientService;
//import com.biolims.file.service.FileInfoService;
//import com.biolims.sample.model.SampleInfo;
//import com.biolims.sample.model.SampleInput;
//import com.biolims.sample.model.SampleOrder;
//import com.biolims.sample.service.SampleOrderService;
//import com.biolims.system.sample.service.SampleMainService;
//import com.biolims.util.HttpUtils;
//import com.biolims.util.JsonUtils;
//import com.biolims.util.SendData;
//
//@Namespace("/crm/customer/patient")
//@Controller
//@Scope("prototype")
//@ParentPackage("default")
//@SuppressWarnings("unchecked")
//public final class CrmPatientHistoryAction extends BaseActionSupport {
//
//	private static final long serialVersionUID = 3488450258677393696L;
//	private String rightsId = "230102";
//	@Autowired
//	private CrmPatientService crmPatientService;
//
//	@Autowired
//	private CrmLinkManService crmLinkManService;
//
//	@Resource
//	private FileInfoService fileInfoService;
//
//	private CrmPatient crmPatient = new CrmPatient();
//	@Autowired
//	private SampleOrderService sampleOrderService;
//	private SampleOrder sampleOrder = new SampleOrder();
//	@Autowired
//	private SampleMainService sampleMainService;
//	private SampleInfo sampleInfo = new SampleInfo();
//	@Action(value = "showCrmPatientList")
//	public String showCrmPatientList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatient.jsp");
//	}
//
//	@Action(value = "showCrmPatientListJson")
//	public void showCrmPatientListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//
//		/*String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
//		if (isSaleManage.equals("false")) {
//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			map2Query.put("crmCustomerId.dutyManId.id", user.getId());
//		}*/
//		//if(map2Query==null)
//		//	map2Query.put("id", "not like##@@##'R%'");
//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		Map<String, Object> result = crmPatientService.findCrmPatientList(map2Query, startNum, limitNum, dir, sort,
//				isSaleManage, user.getId());
//		Long count = (Long) result.get("total");
//		List<CrmPatient> list = (List<CrmPatient>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("lastName", "");
//		map.put("firstName", "");
//		map.put("name", "");
//		map.put("gender", "");
//		map.put("dateOfBirth", "yyyy-MM-dd");
//		map.put("age", "");
//		map.put("placeOfBirth", "");
//		map.put("race", "");
//		map.put("jc", "");
//		map.put("adultTeenager", "");
//		map.put("maritalStatus", "");
//		map.put("isEnd", "");
//		map.put("occupation", "");
//		map.put("consentReceived", "");
//		map.put("patientStatus-id", "");
//		map.put("patientStatus-name", "");
//		map.put("address", "");
//		map.put("telphoneNumber1", "");
//		map.put("telphoneNumber2", "");
//		map.put("telphoneNumber3", "");
//		map.put("additionalRecipient1", "");
//		map.put("additionalRecipient2", "");
//		map.put("additionalRecipient3", "");
//		map.put("additionalTelphoneNumber1", "");
//		map.put("additionalTelphoneNumber2", "");
//		map.put("additionalTelphoneNumber3", "");
//		map.put("createUser-id", "");
//		map.put("createUser-name", "");
//		map.put("crmCustomerId-id", "");
//		map.put("crmCustomerId-name", "");
//		map.put("createDate", "yyyy-MM-dd");
//		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
//	}
//
//	@Action(value = "showCrmLinkManItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmLinkManItemList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmLinkManItem.jsp");
//	}
//
//	@Action(value = "showCrmLinkManItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmLinkManItemListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmLinkManItemList(scId, startNum, limitNum, dir, sort);
//			Long total = (Long) result.get("total");
//			List<CrmLinkManItem> list = (List<CrmLinkManItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("createDate", "yyyy-MM-dd");
//			//			map.put("dutyUser-name", "");
//			//			map.put("dutyUser-id", "");
//			map.put("content", "");
//			map.put("fee", "");
//			map.put("assign", "");
//			map.put("note", "");
//			//			map.put("crmLinkMan-name", "");
//			//			map.put("crmLinkMan-id", "");
//			map.put("link", "");
//			map.put("emailNote", "");
//			map.put("headLine", "");
//			map.put("ourPeople", "");
//			map.put("customerPeople", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "crmPatientSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showDialogCrmPatientList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientDialog.jsp");
//	}
//
//	@Action(value = "showDialogCrmPatientListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogCrmPatientListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
//		//		if (isSaleManage.equals("false")) {
//		//
//		//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		//			map2Query.put("crmCustomerId.dutyManId.id", user.getId());
//		//		}
//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		Map<String, Object> result = crmPatientService.findCrmPatientList(map2Query, startNum, limitNum, dir, sort,
//				isSaleManage, user.getId());
//		Long count = (Long) result.get("total");
//		List<CrmPatient> list = (List<CrmPatient>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("lastName", "");
//		map.put("firstName", "");
//		map.put("name", "");
//		map.put("gender", "");
//		map.put("dateOfBirth", "yyyy-MM-dd");
//		map.put("age", "");
//		map.put("placeOfBirth", "");
//		map.put("race", "");
//		map.put("adultTeenager", "");
//		map.put("maritalStatus", "");
//		map.put("occupation", "");
//		map.put("consentReceived", "");
//		map.put("patientStatus-id", "");
//		map.put("patientStatus-name", "");
//		map.put("address", "");
//		map.put("telphoneNumber1", "");
//		map.put("telphoneNumber2", "");
//		map.put("telphoneNumber3", "");
//		map.put("additionalRecipient1", "");
//		map.put("additionalRecipient2", "");
//		map.put("additionalRecipient3", "");
//		map.put("additionalTelphoneNumber1", "");
//		map.put("additionalTelphoneNumber2", "");
//		map.put("additionalTelphoneNumber3", "");
//		map.put("createUser-id", "");
//		map.put("createUser-name", "");
//		map.put("crmCustomerId-id", "");
//		map.put("crmCustomerId-name", "");
//		map.put("createDate", "yyyy-MM-dd");
//		map.put("checkedBoxTest", "");
//		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
//	}
//	
//	//查询电子病历编号
//	@Action(value = "crmPatientSelectTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String crmPatientSelectTwo() throws Exception {
//		String code =getRequest().getParameter("code");
//		String code1 =  URLDecoder.decode(code, "UTF-8");
//		putObjToContext("code",code1);
//		//System.out.println("====================================================="+code1);
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientDialogTwo.jsp");
//	}
//
//	@Action(value = "crmPatientSelectTwoJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void crmPatientSelectTwoJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		String name = getRequest().getParameter("id");	
//		String name2 = URLDecoder.decode(name, "UTF-8");
//		//System.out.println("====================================================="+name2);
//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		Map<String, Object> result = crmPatientService.findCrmPatientTwoList(map2Query, name2, startNum, limitNum, dir, sort,
//				isSaleManage, user.getId());
//		Long count = (Long) result.get("total");
//		List<CrmPatient> list = (List<CrmPatient>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("gender", "");
//		map.put("dateOfBirth", "yyyy-MM-dd");
//		map.put("age", "");
//		map.put("crmCustomerId-id", "");
//		map.put("crmCustomerId-name", "");
//	
//		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
//	}
//
//	@Action(value = "editCrmPatient")
//	public String editCrmPatient() throws Exception {
//		long num = 0;
//		long num1 = 0;
//		String id = getParameterFromRequest("id");
//
//		if (id != null && !id.equals("")) {
//			crmPatient = crmPatientService.get(id);
//			num = fileInfoService.findFileInfoCount(id, "crmPatient");
//			num1 = fileInfoService.findFileInfoCount(id, "patienteditxgbs");
//			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//		} else {
//
//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			crmPatient.setCreateUser(user);
//			crmPatient.setCreateDate(new Date());
//			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			putObjToContext("state", "3");
//		}
//		//List<DicCountTable> list = crmPatientService.findInstrumentListByType("");
//		//putObjToContext("DicCountTableList", list);
//		User filingUser = (User) getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
//		putObjToContext("isSaleManage", isSaleManage);
//		//putObjToContext("filingUser", filingUser.getId());
//		//putObjToContext("filingUseror", crmPatient.getCreateUser().getId());
//		putObjToContext("fileNum", num);
//		putObjToContext("fileNum1", num1);
//			
//		
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientEdit.jsp");
//	}
//
//	@Action(value = "copyCrmPatient")
//	public String copyCrmPatient() throws Exception {
//		String id = getParameterFromRequest("id");
//		String handlemethod = getParameterFromRequest("handlemethod");
//		crmPatient = crmPatientService.get(id);
//		crmPatient.setId("");
//		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
//		toToolBar(rightsId, "", "", handlemethod);
//		toSetStateCopy();
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientEdit.jsp");
//	}
//
//	@Action(value = "positionSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void positionSelect() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String id = getRequest().getParameter("data");
//		if (id != null) {
//			String city = this.crmPatientService.position(id);
//			map.put("message", city);
//			HttpUtils.write(JsonUtils.toJsonString(map));
//		}
//	}
//
//	
//	//明细保存
//	
//	@Action(value = "save")
//	public String save() throws Exception {
//		String id = crmPatient.getId();
//
//		String codeItem = "" + ((char) ((int) (Math.random() * 26) + 65)) + ((char) ((int) (Math.random() * 26) + 65))
//				+ ((char) ((int) (Math.random() * 26) + 65)) + ((int) (Math.random() * 10))
//				+ ((int) (Math.random() * 10)) + ((int) (Math.random() * 10));
//
//		if (crmPatient.getRandomCode() == null
//				|| (crmPatient.getRandomCode() != null && crmPatient.getRandomCode().equals(""))) {
//
//			crmPatient.setRandomCode(codeItem);
//		}
//
//		if (crmPatient.getSfz() != null && crmPatient.getSfz() != "" && crmPatient.getSfz().length() == 18) {
//			//			System.out.println(crmPatient.getSfz().substring(6, 14));
//			SimpleDateFormat ob = new SimpleDateFormat("yyyyMMdd");
//			crmPatient.setDateOfBirth(ob.parse(crmPatient.getSfz().substring(6, 14)));
//		}
//		crmPatient.setName(crmPatient.getLastName() + crmPatient.getFirstName());
//		if (crmPatient.getDateOfBirth() != null) {
//			crmPatient.setAge(Integer.toString(new Date().getYear() - crmPatient.getDateOfBirth().getYear()));
//		}
//		if (crmPatient.getAge() != null && !crmPatient.getAge().equals("")) {
//			if (Integer.parseInt(crmPatient.getAge()) < 10) {
//				crmPatient.setAdultTeenager("儿童");
//			} else {
//				if (Integer.parseInt(crmPatient.getAge()) >= 10 && Integer.parseInt(crmPatient.getAge()) < 18) {
//					crmPatient.setAdultTeenager("青年");
//				} else {
//					crmPatient.setAdultTeenager("成人");
//				}
//			}
//		}
//		if (id != null && id.equals("")) {
//			crmPatient.setId(null);
//		}
//		//		if (crmPatient.getPatientStatus().getId().equals("brzt2")) {
//		//			crmPatient.setDeathDate(new Date());
//		//		}
//		Map aMap = new HashMap();
//		aMap.put("crmConsumerMarket", getParameterFromRequest("crmConsumerMarketJson"));
//		aMap.put("crmLinkManItem", getParameterFromRequest("crmLinkManItemJson"));
//		aMap.put("crmPatientTumorInformation", getParameterFromRequest("crmPatientTumorInformationJson"));
//		aMap.put("crmPatientSurgeries", getParameterFromRequest("crmPatientSurgeriesJson"));
//		aMap.put("crmPatientDiagnosis", getParameterFromRequest("crmPatientDiagnosisJson"));
//		aMap.put("crmPatientRests", getParameterFromRequest("crmPatientRestsJson"));
//		aMap.put("crmPatientTreat", getParameterFromRequest("crmPatientTreatJson"));
//
//		aMap.put("crmPatientRadiology", getParameterFromRequest("crmPatientRadiologyJson"));
//
//		aMap.put("crmPatientLaboratory", getParameterFromRequest("crmPatientLaboratoryJson"));
//
//		aMap.put("crmPatientPathology", getParameterFromRequest("crmPatientPathologyJson"));
//
//		aMap.put("crmPatientGeneticTesting", getParameterFromRequest("crmPatientGeneticTestingJson"));
//		aMap.put("crmCustomerLinkMan", getParameterFromRequest("crmCustomerLinkManJson"));
//		//保存订单和样本
//		aMap.put("crmSampleOrder", getParameterFromRequest("crmSampleOrderJson"));
//		aMap.put("crmSampleMain", getParameterFromRequest("crmSampleMainJson"));
//		//保存收费确认
//		aMap.put("crmCustormer", getParameterFromRequest("crmCustormerJson"));
//		
//		//保存病史和用药信息
//		aMap.put("crmPatientPersonnel", getParameterFromRequest("crmPatientPersonnelJson"));
//		aMap.put("crmPatientItem", getParameterFromRequest("crmPatientItemJson"));
//		
//		crmPatientService.save(crmPatient, aMap);
//		return redirect("/crm/customer/patient/editCrmPatient.action?id=" + crmPatient.getId());
//
//	}
//
//	@Action(value = "viewCrmPatient")
//	public String toViewCrmPatient() throws Exception {
//		String id = getParameterFromRequest("id");
//		crmPatient = crmPatientService.get(id);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientEdit.jsp");
//	}
//
//	@Action(value = "showCrmPatientTreatList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientTreatList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientTreat.jsp");
//	}
//
//	@Action(value = "showCrmPatientTreatListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientTreatListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientTreatList(scId, startNum, limitNum, dir, sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientTreat> list = (List<CrmPatientTreat>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("scheme", "");
//			map.put("begin", "yyyy-MM-dd");
//			map.put("end", "yyyy-MM-dd");
//			map.put("crmPatient-id", "");
//			map.put("crmPatientDiagnosis-id", "");
//			map.put("crmPatientDiagnosis-diagnosis", "");
//			map.put("treat-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientRestsList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientRestsList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientRests.jsp");
//	}
//
//	@Action(value = "showCrmPatientRestsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientRestsListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientRestsList(scId, startNum, limitNum, dir, sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientDiagnosis> list = (List<CrmPatientDiagnosis>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("diagnosis", "");
//			map.put("dateOfDiagnosis", "yyyy-MM-dd");
//			map.put("hospitalName", "");
//			map.put("primaryPhysician", "");
//			map.put("patientAge", "");
//			map.put("diagnosisResult", "");
//			map.put("stageOfDisease", "");
//			map.put("recurrencesOrMetastases", "");
//			map.put("diagnosisDate", "yyyy-MM-dd");
//			map.put("locationOfTumor", "");
//			map.put("treatment", "");
//			map.put("radiotherapy", "");
//			map.put("chemotherapy", "");
//			map.put("targetedTherapy", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			map.put("tnm", "");
//			map.put("result", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientDiagnosisList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientDiagnosisList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientDiagnosis.jsp");
//	}
//
//	@Action(value = "showCrmPatientDiagnosisListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientDiagnosisListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientDiagnosisList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientDiagnosis> list = (List<CrmPatientDiagnosis>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("DiagnosisType-id", "");
//			map.put("diagnosis", "");
//			map.put("dateOfDiagnosis", "yyyy-MM-dd");
//			map.put("hospitalName", "");
//			map.put("primaryPhysician", "");
//			map.put("patientAge", "");
//			map.put("diagnosisResult", "");
//			map.put("stageOfDisease", "");
//			map.put("recurrencesOrMetastases", "");
//			map.put("diagnosisDate", "yyyy-MM-dd");
//			map.put("locationOfTumor", "");
//			map.put("treatment", "");
//			map.put("radiotherapy", "");
//			map.put("chemotherapy", "");
//			map.put("targetedTherapy", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			map.put("tnm", "");
//			map.put("result", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientSurgeriesList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientSurgeriesList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientSurgeries.jsp");
//	}
//
//	@Action(value = "showCrmPatientSurgeriesListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientSurgeriesListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientSurgeriesList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientSurgeries> list = (List<CrmPatientSurgeries>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("previousProceduresSurgeies", "");
//			map.put("nameOfProcedureOrSurgery", "");
//			map.put("dateOfProcedureAndSurgery", "yyyy-MM-dd");
//			map.put("hospital", "");
//			map.put("primarySurgeon", "");
//			map.put("resectionMargins", "");
//			map.put("priorCancerHistory", "");
//			map.put("familyHistoryOfCancer", "");
//			map.put("infectiousDiseases", "");
//			map.put("historyOfSmoking", "");
//			map.put("environmentalExposures", "");
//			map.put("otherDiseases", "");
//			map.put("crmPatientDiagnosis-id", "");
//			map.put("crmPatientDiagnosis-diagnosis", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientTumorInformationList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmTumorInformationList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientTumorInformation.jsp");
//	}
//
//	@Action(value = "showCrmPatientTumorInformationListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmTumorInformationListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientTumorInformationList(scId, startNum, limitNum,
//					dir, sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatient> list = (List<CrmPatient>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("tumorLocation-id", "");
//			map.put("tumorLocation-name", "");
//			map.put("site-id", "");
//			map.put("site-name", "");
//			map.put("pathologicalDiagnosis", "");
//			map.put("grade", "");
//			map.put("sequencingName", "");
//			map.put("differentiation", "");
//			map.put("degreeOfDifferentiation-id", "");
//			map.put("degreeOfDifferentiation-name", "");
//			map.put("biopsy", "");
//			map.put("dateOfBiopsy", "yyyy-MM-dd");
//			map.put("result", "");
//			map.put("primaryVsSecondary", "");
//			map.put("sampleType-id", "");
//			map.put("sampleType-name", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			map.put("sampleCode", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientRadiologyList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientRadiologyList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientRadiology.jsp");
//	}
//
//	@Action(value = "showCrmPatientRadiologyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientRadiologyListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientRadiologyList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientRadiology> list = (List<CrmPatientRadiology>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("examinationItem", "");
//			map.put("dateOfExamination", "yyyy-MM-dd");
//			map.put("hospital", "");
//			map.put("examination", "");
//			map.put("hospitalPatient", "");
//			map.put("keyResultsDescription", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientLaboratoryList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientLaboratoryList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientLaboratory.jsp");
//	}
//
//	@Action(value = "showCrmPatientLaboratoryListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientLaboratoryListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientLaboratoryList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientLaboratory> list = (List<CrmPatientLaboratory>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("examinationItem", "");
//			map.put("dateOfExamination", "yyyy-MM-dd");
//			map.put("hospital", "");
//			map.put("examination", "");
//			map.put("hospitalPatient", "");
//			map.put("keyResultsDescription", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientPathologyList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientPathologyList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientPathology.jsp");
//	}
//
//	@Action(value = "showCrmPatientPathologyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientPathologyListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientPathologyList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientPathology> list = (List<CrmPatientPathology>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("examinationItem", "");
//			map.put("dateOfExamination", "yyyy-MM-dd");
//			map.put("hospital", "");
//			map.put("examination", "");
//			map.put("hospitalPatient", "");
//			map.put("keyResultsDescription", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			map.put("tumorLocation-id", "");
//			map.put("site-id", "");
//			map.put("pathologicalDiagnosis", "");
//			map.put("grade", "");
//			map.put("differentiation", "");
//			map.put("degreeOfDifferentiation-id", "");
//			map.put("result", "");
//			map.put("primaryVsSecondary", "");
//			map.put("dateOfBiopsy", "");
//			map.put("biopsy", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientGeneticTestingList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientGeneticTestingList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientGeneticTesting.jsp");
//	}
//
//	@Action(value = "showCrmPatientGeneticTestingListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientGeneticTestingListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientGeneticTestingList(scId, startNum, limitNum,
//					dir, sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientGeneticTesting> list = (List<CrmPatientGeneticTesting>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("method", "");
//			map.put("dateOfTest", "yyyy-MM-dd");
//			map.put("hospitalOrCompany", "");
//			map.put("test", "");
//			map.put("biomarker", "");
//			map.put("result", "");
//			map.put("treatment", "");
//			map.put("locus", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmPatientLinkManList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmCustomerLinkManList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientLinkMan.jsp");
//	}
//
//	@Action(value = "showCrmPatientLinkManListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmCustomerLinkManListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientLinkManList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientLinkMan> list = (List<CrmPatientLinkMan>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("linkMan_mail", "");
//			map.put("linkMan_name", "");
//			map.put("linkMan_post", "");
//			map.put("linkMan_telNumber", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	/**
//	 * 订单和样本
//	 */
//	@Action(value = "showCrmOrderList")
//	public String showCrmOrderList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmSampleOrderOrder.jsp");
//	}
//
//	@Action(value = "showCrmOrderListJson")
//	public void showCrmOrderListJson() throws Exception {
// 		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientOrderList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("name", "");
//			map.put("gender", "");
//			map.put("birthDate", "yyyy-MM-dd");
//			map.put("diagnosisDate", "yyyy-MM-dd");
//			map.put("dicType", "");
//			map.put("sampleStage", "");
//			map.put("inspectionDepartment-id", "");
//			map.put("inspectionDepartment-name", "");
//			map.put("crmProduct-id", "");
//			map.put("crmProduct-name", "");
//			map.put("samplingDate", "yyyy-MM-dd");
//			map.put("samplingLocation-id", "");
//			map.put("samplingLocation-name", "");
//			map.put("samplingNumber", "");
//			map.put("pathologyConfirmed", "");
//			map.put("bloodSampleDate", "yyyy-MM-dd");
//			map.put("plasmapheresisDate", "yyyy-MM-dd");
//			map.put("commissioner-id", "");
//			map.put("commissioner-name", "");
//			map.put("receivedDate", "yyyy-MM-dd");
//			map.put("sampleTypeId", "");
//			map.put("sampleTypeName", "");
//			map.put("sampleCode", "");
//			map.put("medicalNumber", "");
//			map.put("family", "");
//			map.put("familyPhone", "");
//			map.put("familySite", "");
//			map.put("medicalInstitutions", "");
//			map.put("medicalInstitutionsPhone", "");
//			map.put("medicalInstitutionsSite", "");
//			map.put("attendingDoctor", "");
//			map.put("attendingDoctorPhone", "");
//			map.put("attendingDoctorSite", "");
//			map.put("note", "");
//			map.put("createUser-id", "");
//			map.put("createUser-name", "");
//			map.put("createDate", "yyyy-MM-dd");
//			map.put("confirmUser-id", "");
//			map.put("confirmUser-name", "");
//			map.put("confirmDate", "yyyy-MM-dd");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("sampleFlag", "");
//			map.put("successFlag", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	/**
//	 * 样本
//	 */
//	@Action(value = "showCrmSampleInfoList")
//	public String showCrmSampleInfoList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmSampleInfo.jsp");
//	}
//
//	@Action(value = "showCrmSampleInfoListJson")
//	public void showCrmSampleInfoListJson() throws Exception {
// 		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmSampleInfoList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("name", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("patientName", "");
//			map.put("note", "");
//			map.put("orderNum", "");
//			map.put("patientId", "");
//			map.put("sampleOrder", "");
//	
//			map.put("idCard", "");
//			map.put("businessType", "");
//			map.put("price", "");
//			map.put("type-id", "");
//			map.put("type-name", "");
//			map.put("upLoadAccessory-id", "");
//			map.put("upLoadAccessory-fileName", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	/**
//	 * 
//	 * 收费确认showCustoremerpageList
//	 * @throws Exception
//	 */
//	@Action(value = "showCustoremerpageList")
//	public String showCustoremerpageList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmShowCustormList.jsp");
//	}
//
//	@Action(value = "showCrmCustormerListJson")
//	public void showCrmCustormerListJson() throws Exception {
// 		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmCustormerList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "id");
//			map.put("isFee", "isFee");
//			map.put("fee", "");
//			map.put("consumptionDate", "yyyy-MM-dd");
//			map.put("feeWay", "");
//			map.put("monthFee", "");
//			map.put("note", "note");
//			map.put("bankNum", "bankNum");
//			map.put("receipNum", "");
//			map.put("posNum", "");
//			map.put("backFee", "");
//			map.put("backFeeDate", "yyyy-MM-dd");
//			map.put("invoiceCode", "");
//			map.put("invoiceDate", "yyyy-MM-dd");
//			map.put("invoiceNum", "");
//			map.put("alipayNum", "");
//			map.put("realFee", "realFee");
//			map.put("state", "");
//			map.put("financeUser-id", "");
//			map.put("financeUser-name", "");
//			map.put("sampleOrder-id", "");
//			map.put("sampleOrder-name", "");
//			map.put("sampleOrder-productName", "");
//			map.put("sampleOrder-commissioner-name", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	
//	@Action(value = "delModelId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void delModelId() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String objName = getParameterFromRequest("objName");
//			String objProperty = getParameterFromRequest("objProperty");
//			String vals = getParameterFromRequest("vals");
//			crmPatientService.delModelId(objName, objProperty, vals);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//			e.printStackTrace();
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	//个体销费记录管理
//	@Action(value = "showCrmConsumerMarketList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmConsumerMarketList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmConsumerMarketList.jsp");
//	}
//
//	@Action(value = "showCrmConsumerMarketListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmConsumerMarketListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findConsumerMarketList(scId, startNum, limitNum, dir, sort);
//			Long total = (Long) result.get("total");
//			List<CrmConsumerMarket> list = (List<CrmConsumerMarket>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("crmProduct-id", "");
//
//			map.put("consumptionTime", "yyyy-MM-dd");
//			map.put("contractTime", "yyyy-MM-dd");
//			map.put("skillReport", "yyyy-MM-dd");
//			map.put("clinicReport", "yyyy-MM-dd");
//			map.put("fSkillReport", "yyyy-MM-dd");
//			map.put("fReport", "yyyy-MM-dd");
//			map.put("paymentTime", "yyyy-MM-dd");
//			map.put("isFee", "isFee");
//			map.put("fee", "fee");
//			map.put("crmPatient-id", "");
//			map.put("code", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Action(value = "showCrmConsumerMarketJson")
//	public String showCrmConsumerMarketJson() throws Exception {
//		String type = super.getRequest().getParameter("type");
//		//		System.out.print(type);
//		String outStr = "{results:" + crmPatientService.getCrmConsumerMarketJson(type) + "}";
//
//		return renderText(outStr);
//	}
//
//	@Action(value = "patientButton", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void patientButton() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		int data = Integer.parseInt(getRequest().getParameter("data"));
//		String code = "";
//		for (int i = 1; i <= data; i++) {
//			String codeItem = "P" + ((char) ((int) (Math.random() * 26) + 65)) + ((int) (Math.random() * 10))
//					+ ((int) (Math.random() * 10)) + ((int) (Math.random() * 10));
//			Map<String, String> map2Query = new HashMap<String, String>();
//			map2Query.put("id", codeItem);
//			Map<String, Object> result = crmPatientService.findCrmPatientList(map2Query, null, null, null, null, null,
//					null);
//			List<CrmPatient> list = (List<CrmPatient>) result.get("list");
//			if (list.size() > 0) {
//				i = i - 1;
//			} else {
//				if (code.endsWith(">")) {
//					String[] as = code.split("<br/>");
//					int z = 0;
//					for (int j = 0; j < as.length; j++) {
//						if (as[j] == codeItem) {
//							z = 1;
//						}
//					}
//					if (z == 1) {
//						i = i - 1;
//					} else {
//						code = code + codeItem + "<br/>";
//					}
//				} else {
//					code = code + codeItem + "<br/>";
//				}
//			}
//		}
//		map.put("message", code);
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	@Action(value = "sfzVerify")
//	public void sfzVerify() throws Exception {
//		String data = super.getRequest().getParameter("data");
//		//		System.out.print(data);
//		//		String outStr = "{results:" + crmPatientService.getCrmConsumerMarketJson(type) + "}";
//		List<CrmPatient> CrmPatientList = crmPatientService.findTableWhere("CrmPatient", "sfz", "'" + data + "'");
//		Map<String, Object> map = new HashMap<String, Object>();
//		if (CrmPatientList.size() > 0) {
//			map.put("message", false);
//		} else {
//			map.put("message", true);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	@Action(value = "showCrmPatientPathologyJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientPathologyJson() throws Exception {
//		String id = super.getRequest().getParameter("id");
//		//		System.out.print(type);
//		String outStr = "{results:" + crmPatientService.getCrmPatientPathologyJson(id) + "}";
//		return renderText(outStr);
//	}
//	//添加病史和用药信息的明细
//	@Action(value = "showCrmPatientPersonnelList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientPersonnelList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientPersonnel.jsp");
//	}
//
//	@Action(value = "showCrmPatientPersonnelListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientPersonnelListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientPersonnelList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientPersonnel> list = (List<CrmPatientPersonnel>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("checkOutTheAge", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			map.put("tumorCategory-name", "");
//			map.put("tumorCategory-id", "");
//			map.put("crmFamilyPatientShip-id", "");
//			map.put("crmFamilyPatientShip-name", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delCrmPatientPersonnel")
//	public void delCrmPatientPersonnel() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			crmPatientService.delCrmPatientPersonnel(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	
//
//	@Action(value = "showCrmPatientItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmPatientItemList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/customer/patient/crmPatientItem.jsp");
//	}
//
//	@Action(value = "showCrmPatientItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmPatientItemListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = crmPatientService.findCrmPatientItemList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<CrmPatientItem> list = (List<CrmPatientItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("startDate", "yyyy-MM-dd");
//			map.put("stopDate", "yyyy-MM-dd");
//			map.put("useDrugName", "");
//			map.put("effectOfProgress", "");
//			map.put("effectOfProgressSpeed", "");
//			map.put("geneticTestHistory", "");
//			map.put("sampleDetectionName", "");
//			map.put("sampleExonRegion", "");
//			map.put("sampleDetectionResult", "");
//			map.put("crmPatient-name", "");
//			map.put("crmPatient-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delCrmPatientItem")
//	public void delCrmPatientItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			crmPatientService.delCrmPatientItem(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	
//	
//	
//	
//	
//	
///**
// * 
// * get和set方法
// * @return
// */
//	public String getRightsId() {
//		return rightsId;
//	}
//
//	public void setRightsId(String rightsId) {
//		this.rightsId = rightsId;
//	}
//
//	public CrmPatientService getCrmPatientService() {
//		return crmPatientService;
//	}
//
//	public void setCrmPatientService(CrmPatientService crmPatientService) {
//		this.crmPatientService = crmPatientService;
//	}
//
//	public CrmPatient getCrmPatient() {
//		return crmPatient;
//	}
//
//	public void setCrmPatient(CrmPatient crmPatient) {
//		this.crmPatient = crmPatient;
//	}
//
//	public SampleOrderService getSampleOrderService() {
//		return sampleOrderService;
//	}
//
//	public void setSampleOrderService(SampleOrderService sampleOrderService) {
//		this.sampleOrderService = sampleOrderService;
//	}
//
//	public SampleOrder getSampleOrder() {
//		return sampleOrder;
//	}
//
//	public void setSampleOrder(SampleOrder sampleOrder) {
//		this.sampleOrder = sampleOrder;
//	}
//
//	public SampleMainService getSampleMainService() {
//		return sampleMainService;
//	}
//
//	public void setSampleMainService(SampleMainService sampleMainService) {
//		this.sampleMainService = sampleMainService;
//	}
//
//	public SampleInfo getSampleInfo() {
//		return sampleInfo;
//	}
//
//	public void setSampleInfo(SampleInfo sampleInfo) {
//		this.sampleInfo = sampleInfo;
//	}
//
//	
//}
