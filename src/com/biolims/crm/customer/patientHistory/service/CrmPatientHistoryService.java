package com.biolims.crm.customer.patientHistory.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.patient.dao.CrmPatientDao;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
import com.biolims.crm.customer.patient.model.CrmPatientTreat;
import com.biolims.crm.customer.patient.model.CrmPatientTumorInformation;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.crm.customer.patientHistory.dao.CrmPatientHistoryDao;
import com.biolims.crm.customer.patientHistory.model.CrmConsumerMarketHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientDiagnosisHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientGeneticTestingHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientItemHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientLaboratoryHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientLinkManHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientPathologyHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientPersonnelHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientRadiologyHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientSurgeriesHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientTreatHistory;
import com.biolims.crm.customer.patientHistory.model.CrmPatientTumorInformationHistory;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmPatientHistoryService {
	@Resource
	private CrmPatientHistoryDao crmPatientHistoryDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	public <table> List<table> findTableWhere2(String table, String where, String type, String where2, String type2) {
		return crmPatientHistoryDao.findTableWhere2(table, where, type, where2, type2);
	}

	public <table> List<table> findTableWhere2(String table, String where) {
		return crmPatientHistoryDao.findTablehql(table, where);
	}

	public <table> List<table> findTableWhere(String table, String where, String type) {
		return crmPatientHistoryDao.findTableWhere(table, where, type);
	}

	public Map<String, Object> findCrmPatientHistoryList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String userId) {
		return crmPatientHistoryDao.selectCrmPatientHistoryList(mapForQuery, startNum, limitNum, dir, sort, type, userId);
	}
	public Map<String, Object> findCrmPatientTwoHistoryList(Map<String, String> mapForQuery,String  name, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String userId) {
		return crmPatientHistoryDao.selectCrmPatientHistoryTwoList(mapForQuery,name,startNum, limitNum, dir, sort, type, userId);
	}

	public Map<String, Object> findCrmPatientHistoryList(String cId, Integer startNum, Integer limitNum, String dir,
			String sort, String type) {
		return crmPatientHistoryDao.selectCrmPatientHistoryList(cId, startNum, limitNum, dir, sort, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmPatient i) throws Exception {

		crmPatientHistoryDao.saveOrUpdate(i);

	}

	public Map<String, Object> findCrmLinkManItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmLinkManItemList(scId, startNum, limitNum, dir, sort);
		//		List<CrmLinkManItem> list = (List<CrmLinkManItem>) result.get("list");
		return result;
	}

	public CrmPatient get(String id) {
		CrmPatient crmPatient = commonDAO.get(CrmPatient.class, id);
		return crmPatient;
	}

	public Map<String, Object> findCrmPatientDiagnosisHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientDiagnosisHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientDiagnosisHistory> list = (List<CrmPatientDiagnosisHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientRestsHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientRestsHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientDiagnosisHistory> list = (List<CrmPatientDiagnosisHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientTreatHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientTreatHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientTreatHistory> list = (List<CrmPatientTreatHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientSurgeriesHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientSurgeriesHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientSurgeriesHistory> list = (List<CrmPatientSurgeriesHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientTumorInformationHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientTumorInformationsHistoryList(scId, startNum, limitNum, dir,
				sort);
		List<CrmPatientTumorInformationHistory> list = (List<CrmPatientTumorInformationHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientRadiologyHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientRadiologyHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientRadiologyHistory> list = (List<CrmPatientRadiologyHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientLaboratoryHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientLaboratoryHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientLaboratoryHistory> list = (List<CrmPatientLaboratoryHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientLinkManHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientLinkManHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientLinkManHistory> list = (List<CrmPatientLinkManHistory>) result.get("list");
		return result;
	}

	//查询订单
	public Map<String, Object> findCrmPatientOrderList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientOrderList(scId, startNum, limitNum, dir, sort);
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");
		return result;
	}

	//查询样本
	public Map<String, Object> findCrmSampleInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		SampleOrder so = this.crmPatientHistoryDao.queryByOrderId(scId);
		//获取样本编号 
		String number = so.getId();
		Map<String, Object> result = crmPatientHistoryDao.selectCrmSampleInfoList(scId, startNum, limitNum, dir, sort, number);
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");
		return result;
	}
	//查询收费确认
		public Map<String, Object> findCrmCustormerList(String scId, Integer startNum, Integer limitNum, String dir,
				String sort) throws Exception {
			SampleOrder so = this.crmPatientHistoryDao.queryByOrderId(scId);
			//获取订单编号 
			String number = so.getId();
			Map<String, Object> result = crmPatientHistoryDao.selectCrmCustormerList(scId, startNum, limitNum, dir, sort, number);
			List<CrmConsumerMarket> list = (List<CrmConsumerMarket>) result.get("list");
			return result;
		}

	public Map<String, Object> findCrmPatientPathologyHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientPathologyHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientPathologyHistory> list = (List<CrmPatientPathologyHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientGeneticTestingHistoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientGeneticTestingHistoryList(scId, startNum, limitNum, dir,
				sort);
		List<CrmPatientGeneticTestingHistory> list = (List<CrmPatientGeneticTestingHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findConsumerMarketHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectConsumerMarketHistoryList(scId, startNum, limitNum, dir, sort);
		// List<CrmConsumerMarket> list = (List<CrmConsumerMarket>)
		// result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientDiagnosisHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientDiagnosisHistory> saveItems = new ArrayList<CrmPatientDiagnosisHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientDiagnosisHistory scp = new CrmPatientDiagnosisHistory();
			// 将map信息读入实体类
			scp = (CrmPatientDiagnosisHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);
			scp.setNum("1");
			if (scp.getDateOfDiagnosis() != null && scp.getCrmPatientHistory().getDateOfBirth() != null) {
				scp.setPatientAge(Integer.toString(scp.getDateOfDiagnosis().getYear()
						- scp.getCrmPatientHistory().getDateOfBirth().getYear()));
			}
			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientSurgeriesHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientSurgeriesHistory> saveItems = new ArrayList<CrmPatientSurgeriesHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientSurgeriesHistory scp = new CrmPatientSurgeriesHistory();
			// 将map信息读入实体类
			scp = (CrmPatientSurgeriesHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmConsumerMarketHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmConsumerMarketHistory> saveItems = new ArrayList<CrmConsumerMarketHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmConsumerMarketHistory scp = new CrmConsumerMarketHistory();
			// 将map信息读入实体类
			scp = (CrmConsumerMarketHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	//保存子表item和person信息
	public void saveCrmPatientItem2History(SampleOrderItem t, CrmPatientHistory r) throws Exception {
		CrmPatientItemHistory i = new CrmPatientItemHistory();
		//i.setId(t.getId());
		//i.setDrugDate(t.getDrugDate());
		i.setEffectOfProgress(t.getEffectOfProgress());
		i.setEffectOfProgressSpeed(t.getEffectOfProgressSpeed());
		i.setGeneticTestHistory(t.getGeneticTestHistory());
		i.setSampleDetectionName(t.getSampleDetectionName());
		i.setSampleDetectionResult(t.getSampleDetectionResult());
		i.setSampleExonRegion(t.getSampleExonRegion());
		i.setUseDrugName(t.getUseDrugName());
		i.setCrmPatientHistory(r);

		crmPatientHistoryDao.save(i);
	}

	public void saveCrmPatientPersonnel2History(SampleOrderPersonnel p, CrmPatientHistory r) throws Exception {
		CrmPatientPersonnelHistory c = new CrmPatientPersonnelHistory();
		c.setCheckOutTheAge(p.getCheckOutTheAge());
		//c.setFamilyRelation(p.getFamilyRelation());
		c.setTumorCategory(p.getTumorCategory());
		c.setCrmPatientHistory(r);
		crmPatientHistoryDao.save(c);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientTreatHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientTreatHistory> saveItems = new ArrayList<CrmPatientTreatHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientTreatHistory scp = new CrmPatientTreatHistory();
			// 将map信息读入实体类
			scp = (CrmPatientTreatHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);
			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientRestsHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientDiagnosisHistory> saveItems = new ArrayList<CrmPatientDiagnosisHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientDiagnosisHistory scp = new CrmPatientDiagnosisHistory();
			// 将map信息读入实体类
			scp = (CrmPatientDiagnosisHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);
			scp.setNum("2");
			if (scp.getDateOfDiagnosis() != null && scp.getCrmPatientHistory().getDateOfBirth() != null) {
				scp.setPatientAge(Integer.toString(scp.getDateOfDiagnosis().getYear()
						- scp.getCrmPatientHistory().getDateOfBirth().getYear()));
			}
			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	//
	//	@WriteOperLog
	//	@WriteExOperLog
	//	@Transactional(rollbackFor = Exception.class)
	//	public void saveCrmPatientTumorInformation(CrmPatient sc, String itemDataJson) throws Exception {
	//		List<SampleBloodInfo> saveItems = new ArrayList<SampleBloodInfo>();
	//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
	//		for (Map<String, Object> map : list) {
	//			SampleBloodInfo scp = new SampleBloodInfo();
	//			// 将map信息读入实体类
	//			scp = (SampleBloodInfo) crmPatientDao.Map2Bean(map, scp);
	//			//			if (scp.getId() != null && scp.getId().equals(""))
	//			//				scp.setId(null);
	//			//			scp.setCrmPatient(sc);
	//			scp.setCustomer(sc.getCrmCustomerId());
	//			saveItems.add(scp);
	//		}
	//		crmPatientDao.saveOrUpdateAll(saveItems);
	//	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientRadiologyHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientRadiologyHistory> saveItems = new ArrayList<CrmPatientRadiologyHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientRadiologyHistory scp = new CrmPatientRadiologyHistory();
			// 将map信息读入实体类
			scp = (CrmPatientRadiologyHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientLaboratoryHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientLaboratoryHistory> saveItems = new ArrayList<CrmPatientLaboratoryHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientLaboratoryHistory scp = new CrmPatientLaboratoryHistory();
			// 将map信息读入实体类
			scp = (CrmPatientLaboratoryHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientPathologyHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientPathologyHistory> saveItems = new ArrayList<CrmPatientPathologyHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientPathologyHistory scp = new CrmPatientPathologyHistory();
			// 将map信息读入实体类
			scp = (CrmPatientPathologyHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientGeneticTestingHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientGeneticTestingHistory> saveItems = new ArrayList<CrmPatientGeneticTestingHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientGeneticTestingHistory scp = new CrmPatientGeneticTestingHistory();
			// 将map信息读入实体类
			scp = (CrmPatientGeneticTestingHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmCustomerLinkManHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientLinkManHistory> saveItems = new ArrayList<CrmPatientLinkManHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientLinkManHistory scp = new CrmPatientLinkManHistory();
			// 将map信息读入实体类
			scp = (CrmPatientLinkManHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);
			crmPatientHistoryDao.saveOrUpdate(scp);
			// saveItems.add(scp);
		}

	}

	//该方法名由save修改为save1
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(CrmPatientHistory sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmPatientHistoryDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmLinkManItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				//saveCrmLinkManItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmConsumerMarket");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmConsumerMarketHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientTreat");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientTreatHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientRests");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientRestsHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientTumorInformation");
			//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			//				saveCrmPatientTumorInformation(sc, jsonStr);
			//			}
			jsonStr = (String) jsonMap.get("crmPatientSurgeries");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientSurgeriesHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientDiagnosis");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientDiagnosisHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientRadiology");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientRadiologyHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientLaboratory");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientLaboratoryHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientPathology");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientPathologyHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientGeneticTesting");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientGeneticTestingHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmCustomerLinkMan");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmCustomerLinkManHistory(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmLinkManItemHistoryHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmLinkManItem> saveItems = new ArrayList<CrmLinkManItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmLinkManItem scp = new CrmLinkManItem();
			// 将map信息读入实体类
			scp = (CrmLinkManItem) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			//scp.setCrmPatient()(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	public String position(String id) throws Exception {
		String city = "";
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("superior", id);
		Map<String, Object> map = crmPatientHistoryDao.cusPositionHistory(mapForQuery, null, null, null, null);
		List<CrmTumourPosition> list = (List<CrmTumourPosition>) map.get("list");
		for (int i = 0; i < list.size(); i++) {
			CrmTumourPosition sri = list.get(i);
			city = city + "," + sri.getId() + "," + sri.getName();
		}
		return city;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delModelId(String objName, String objProperty, String vals) throws Exception {
		crmPatientHistoryDao.delModelId(objName, objProperty, vals);
	}

	public String getCrmConsumerMarketHistoryJson(String type) throws Exception {
		List<CrmConsumerMarketHistory> list = crmPatientHistoryDao.findCrmConsumerMarketHistory(type);
		return JsonUtils.toJsonString(list);
	}

	public String getCrmPatientPathologyHistoryJson(String type) throws Exception {
		List<CrmPatientPathologyHistory> list = crmPatientHistoryDao.findCrmPatientPathologyHistory(type);
		return JsonUtils.toJsonString(list);
	}

	public boolean queryByCount(String number) {
		boolean flag = crmPatientHistoryDao.queryByCount(number);
		return flag;
	}

	//添加明细
	public Map<String, Object> findCrmPatientPersonnelHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientDiagnosisHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientPersonnelHistory> list = (List<CrmPatientPersonnelHistory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientItemHistoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectCrmPatientItemHistoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientItemHistory> list = (List<CrmPatientItemHistory>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientPersonnelHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientPersonnelHistory> saveItems = new ArrayList<CrmPatientPersonnelHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientPersonnelHistory scp = new CrmPatientPersonnelHistory();
			// 将map信息读入实体类
			scp = (CrmPatientPersonnelHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmPatientPersonnelHistory(String[] ids) throws Exception {
		for (String id : ids) {
			CrmPatientPersonnelHistory scp = crmPatientHistoryDao.get(CrmPatientPersonnelHistory.class, id);
			crmPatientHistoryDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientItemHistory(CrmPatientHistory sc, String itemDataJson) throws Exception {
		List<CrmPatientItemHistory> saveItems = new ArrayList<CrmPatientItemHistory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientItemHistory scp = new CrmPatientItemHistory();
			// 将map信息读入实体类
			scp = (CrmPatientItemHistory) crmPatientHistoryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatientHistory(sc);

			saveItems.add(scp);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmPatientItem(String[] ids) throws Exception {
		for (String id : ids) {
			CrmPatientItem scp = crmPatientHistoryDao.get(CrmPatientItem.class, id);
			crmPatientHistoryDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmPatientHistory sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmPatientHistoryDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmPatientPersonnel");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientPathologyHistory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientItemHistory(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCustomerPaymentHistory(String itemDataJson) throws Exception {
		List<CrmConsumerMarketHistory> saveItems = new ArrayList<CrmConsumerMarketHistory>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		int s1 = 0;
		String s2 = "";
		boolean blood = false;
		boolean dna = false;
		String s3 = "";
		for (Map map : list) {
			CrmConsumerMarketHistory sbi = new CrmConsumerMarketHistory();
			sbi = (CrmConsumerMarketHistory) crmPatientHistoryDao.Map2Bean(map, sbi);
			// sbi.setConsumptionTime(new Date());

			commonService.saveOrUpdate(sbi);

		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCustomerInfoHistory(String itemDataJson) throws Exception {
		List<CrmConsumerMarketHistory> saveItems = new ArrayList<CrmConsumerMarketHistory>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			CrmConsumerMarketHistory sbi = new CrmConsumerMarketHistory();
			sbi = (CrmConsumerMarketHistory) crmPatientHistoryDao.Map2Bean(map, sbi);
			saveItems.add(sbi);
		}
		crmPatientHistoryDao.saveOrUpdateAll(saveItems);

	}

	public Map<String, Object> findConsumerMarketListHistory(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientHistoryDao.selectConsumerMarketHistoryList(mapForQuery, startNum, limitNum, dir, sort);
		// List<CrmConsumerMarket> list = (List<CrmConsumerMarket>)
		// result.get("list");
		return result;
	}

}
