package com.biolims.crm.customer.patientHistory.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCity;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;

/**
 * @Title: Model
 * @Description: 电子病历历史表
 * @author lims-platform
 * @date 2014-07-30 17:11:31
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_PATIENT_HISTORY")
@SuppressWarnings("serial")
public class CrmPatientHistory extends EntityDao<CrmPatientHistory> implements
		java.io.Serializable {
	/** 病人id */
	private java.lang.String id;
	private String selectId;// 选择病人
	private java.lang.String lastName;
	/** 姓 */
	private java.lang.String firstName;

	private java.lang.String name;
	/** 性别 */
	private java.lang.String gender;
	/** 出生年月 */
	private java.util.Date dateOfBirth;
	/** 年龄 */
	private java.lang.String age;
	/** 籍贯 */
	private java.lang.String placeOfBirth;
	/** 民族 */
	private java.lang.String race;
	/** 成人/儿童 */
	private java.lang.String adultTeenager;
	/** 婚姻 */
	private java.lang.String maritalStatus;
	/** 职业 */
	private java.lang.String occupation;
	/** 同意接收 */
	private java.lang.String consentReceived;
	/** 病人状态 */
	private DicType patientStatus;
	/** 家庭住址 */
	private java.lang.String address;
	/** 鑱旂郴鐢佃瘽1 */
	private java.lang.String telphoneNumber1;
	/** 鑱旂郴鐢佃瘽2 */
	private java.lang.String telphoneNumber2;
	/** 鑱旂郴鐢佃瘽3 */
	private java.lang.String telphoneNumber3;
	/** 鍏朵粬鑱旂郴浜� */
	private java.lang.String additionalRecipient1;
	/** 鍏朵粬鑱旂郴浜� */
	private java.lang.String additionalRecipient2;
	/** 鍏朵粬鑱旂郴浜� */
	private java.lang.String additionalRecipient3;
	/** 鑱旂郴浜虹數璇� */
	private java.lang.String additionalTelphoneNumber1;
	/** 鑱旂郴浜虹數璇� */
	private java.lang.String additionalTelphoneNumber2;
	/** 鑱旂郴浜虹數璇� */
	private java.lang.String additionalTelphoneNumber3;
	/** 鑲跨槫鎵�湪鍣ㄥ畼 */
	private DicType tumorLocation;
	/** 鑲跨槫浣嶇疆 */
	private DicType site;
	/** 鐥呯悊璇婃柇 */
	private String pathologicalDiagnosis;
	/** 绛夌骇 */
	private String grade;
	/** 鏄惁鍒嗗寲 */
	private java.lang.String differentiation;
	/** 鍒嗗寲绋嬪害 */
	private DicType degreeOfDifferentiation;
	/** 鏄惁鍋氳繃娲绘 */
	private java.lang.String biopsy;
	/** 鏃堕棿 */
	private java.util.Date dateOfBiopsy;
	/** 缁撴灉 */
	private java.lang.String result;

	private java.lang.String primaryVsSecondary;
	/** 鏍锋湰绫诲瀷 */
	private DicType sampleType;

	private User createUser;
	/** 瀹㈡埛id */
	private CrmCustomer crmCustomerId;
	/** 鍒涘缓鏃ユ湡 */
	private java.util.Date createDate;

	private String jc;
	/** 身份证 */
	private String sfz;
	/** 自身癌症史 */
	private java.lang.String priorCancerHistory;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancer;
	/** 传染病 */
	private java.lang.String infectiousDiseases;
	/** 吸烟史 */
	private java.lang.String historyOfSmoking;
	/** 环境风险/毒物 */
	private java.lang.String environmentalExposures;
	/** 其他疾病史 */
	private java.lang.String otherDiseases;
	/** 自身癌症史 */
	private java.lang.String priorCancerHistorytext;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancergx;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancertext;
	/** 传染病 */
	private java.lang.String infectiousDiseasestext;
	/** 吸烟史 */
	private java.lang.String historyOfSmokingtext;
	/** 环境风险/毒物 */
	private java.lang.String environmentalExposurestext;
	/** 其他疾病史 */
	private java.lang.String otherDiseasestext;

	/** 随机码 */
	private java.lang.String randomCode;
	/** 饮酒史 */
	private java.lang.String ysj;
	/** 饮酒史 */
	private java.lang.String ysjtext;
	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鐥呬汉id
	 */
	/** 检测项目ID */
	private java.lang.String sequencingId;
	/** 检测项目 */
	private java.lang.String sequencingName;
	/** 检测类型 */
	private java.lang.String sequencingType;
	/** 待打印报告 */
	private java.lang.String waitReport;
	/** 已打印报告 */
	private java.lang.String report;
	/** 癌症种类 */
	private WeiChatCancerType cancerType;
	/** 子类一 */
	private WeiChatCancerTypeSeedOne cancerTypeSeedOne;
	/** 子类二 */
	private WeiChatCancerTypeSeedTwo cancerTypeSeedTwo;

	/** 相关表 */
	private CrmPatient crmPatient;
	/** 修改人 */
	private User modifier;
	/** 修改时间 */
	private Date changeDate;

	@Column(length = 200)
	private String isEnd;// 是否完结

	public String getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(String isEnd) {
		this.isEnd = isEnd;
	}

	@Column(name = "SEQUENCING_ID", length = 100)
	public java.lang.String getSequencingId() {
		return sequencingId;
	}

	public void setSequencingId(java.lang.String sequencingId) {
		this.sequencingId = sequencingId;
	}

	@Column(name = "SEQUENCING_NAME", length = 100)
	public java.lang.String getSequencingName() {
		return sequencingName;
	}

	public void setSequencingName(java.lang.String sequencingName) {
		this.sequencingName = sequencingName;
	}

	@Column(name = "SEQUENCING_TYPE", length = 100)
	public java.lang.String getSequencingType() {
		return sequencingType;
	}

	public void setSequencingType(java.lang.String sequencingType) {
		this.sequencingType = sequencingType;
	}

	@Column(name = "WAIT_REPORT", length = 100)
	public java.lang.String getWaitReport() {
		return waitReport;
	}

	public void setWaitReport(java.lang.String waitReport) {
		this.waitReport = waitReport;
	}

	@Column(name = "REPORT", length = 100)
	public java.lang.String getReport() {
		return report;
	}

	public void setReport(java.lang.String report) {
		this.report = report;
	}

	private String checkedBoxTest;

	private CrmCity selA;
	private CrmCity selB;
	private CrmCity selC;
	/** email */
	private java.lang.String email;
	// 死亡日期
	private java.util.Date deathDate;

	private CrmCustomer customer;// 客户

	private CrmDoctor customerDoctor;// 客户

	private DicType ks;// 城市

	@Column(name = "SELECT_NAME", length = 100)
	public String getSelectId() {
		return selectId;
	}

	public void setSelectId(String selectId) {
		this.selectId = selectId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELA")
	public CrmCity getSelA() {
		return selA;
	}

	public void setSelA(CrmCity selA) {
		this.selA = selA;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELB")
	public CrmCity getSelB() {
		return selB;
	}

	public void setSelB(CrmCity selB) {
		this.selB = selB;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELC")
	public CrmCity getSelC() {
		return selC;
	}

	public void setSelC(CrmCity selC) {
		this.selC = selC;
	}

	@Column(name = "CHECKEDBOXTEST", length = 100)
	public String getCheckedBoxTest() {
		return checkedBoxTest;
	}

	public void setCheckedBoxTest(String checkedBoxTest) {
		this.checkedBoxTest = checkedBoxTest;
	}

	@Id
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鐥呬汉id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 濮�
	 */
	@Column(name = "LAST_NAME", length = 20)
	public java.lang.String getLastName() {
		return this.lastName;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 濮�
	 */
	public void setLastName(java.lang.String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍚�
	 */
	@Column(name = "FIRST_NAME", length = 20)
	public java.lang.String getFirstName() {
		return this.firstName;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鍚�
	 */
	public void setFirstName(java.lang.String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 濮撳悕
	 */
	@Column(name = "NAME", length = 100)
	public java.lang.String getName() {
		return this.name;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 濮撳悕
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鎬у埆
	 */
	@Column(name = "GENDER", length = 100)
	public java.lang.String getGender() {
		return this.gender;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鎬у埆
	 */
	public void setGender(java.lang.String gender) {
		this.gender = gender;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鍑虹敓骞存湀
	 */
	@Column(name = "DATE_OF_BIRTH", length = 100)
	public java.util.Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date 鍑虹敓骞存湀
	 */
	public void setDateOfBirth(java.util.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 骞撮緞
	 */
	@Column(name = "AGE", length = 100)
	public java.lang.String getAge() {
		return this.age;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 骞撮緞
	 */
	public void setAge(java.lang.String age) {
		this.age = age;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 绫嶈疮
	 */
	@Column(name = "PLACE_OF_BIRTH", length = 200)
	public java.lang.String getPlaceOfBirth() {
		return this.placeOfBirth;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 绫嶈疮
	 */
	public void setPlaceOfBirth(java.lang.String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 姘戞棌
	 */
	@Column(name = "RACE", length = 100)
	public java.lang.String getRace() {
		return this.race;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 姘戞棌
	 */
	public void setRace(java.lang.String race) {
		this.race = race;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鎴愪汉/鍎跨
	 */
	@Column(name = "ADULT_TEENAGER", length = 100)
	public java.lang.String getAdultTeenager() {
		return this.adultTeenager;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鎴愪汉/鍎跨
	 */
	public void setAdultTeenager(java.lang.String adultTeenager) {
		this.adultTeenager = adultTeenager;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 濠氬Щ
	 */
	@Column(name = "MARITAL_STATUS", length = 100)
	public java.lang.String getMaritalStatus() {
		return this.maritalStatus;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 濠氬Щ
	 */
	public void setMaritalStatus(java.lang.String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱屼笟
	 */
	@Column(name = "OCCUPATION", length = 100)
	public java.lang.String getOccupation() {
		return this.occupation;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱屼笟
	 */
	public void setOccupation(java.lang.String occupation) {
		this.occupation = occupation;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍚屾剰鎺ユ敹
	 */
	@Column(name = "CONSENT_RECEIVED", length = 100)
	public java.lang.String getConsentReceived() {
		return this.consentReceived;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鍚屾剰鎺ユ敹
	 */
	public void setConsentReceived(java.lang.String consentReceived) {
		this.consentReceived = consentReceived;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鐥呬汉鐘舵�
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PATIENT_STATUS")
	public DicType getPatientStatus() {
		return this.patientStatus;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 鐥呬汉鐘舵�
	 */
	public void setPatientStatus(DicType patientStatus) {
		this.patientStatus = patientStatus;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 瀹跺涵浣忓潃
	 */
	@Column(name = "ADDRESS", length = 200)
	public java.lang.String getAddress() {
		return this.address;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 瀹跺涵浣忓潃
	 */
	public void setAddress(java.lang.String address) {
		this.address = address;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴鐢佃瘽1
	 */
	@Column(name = "TELPHONE_NUMBER1", length = 100)
	public java.lang.String getTelphoneNumber1() {
		return this.telphoneNumber1;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱旂郴鐢佃瘽1
	 */
	public void setTelphoneNumber1(java.lang.String telphoneNumber1) {
		this.telphoneNumber1 = telphoneNumber1;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴鐢佃瘽2
	 */
	@Column(name = "TELPHONE_NUMBER2", length = 100)
	public java.lang.String getTelphoneNumber2() {
		return this.telphoneNumber2;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱旂郴鐢佃瘽2
	 */
	public void setTelphoneNumber2(java.lang.String telphoneNumber2) {
		this.telphoneNumber2 = telphoneNumber2;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴鐢佃瘽3
	 */
	@Column(name = "TELPHONE_NUMBER3", length = 100)
	public java.lang.String getTelphoneNumber3() {
		return this.telphoneNumber3;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱旂郴鐢佃瘽3
	 */
	public void setTelphoneNumber3(java.lang.String telphoneNumber3) {
		this.telphoneNumber3 = telphoneNumber3;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	@Column(name = "ADDITIONAL_RECIPIENT1", length = 100)
	public java.lang.String getAdditionalRecipient1() {
		return this.additionalRecipient1;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	public void setAdditionalRecipient1(java.lang.String additionalRecipient1) {
		this.additionalRecipient1 = additionalRecipient1;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	@Column(name = "ADDITIONAL_RECIPIENT2", length = 100)
	public java.lang.String getAdditionalRecipient2() {
		return this.additionalRecipient2;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	public void setAdditionalRecipient2(java.lang.String additionalRecipient2) {
		this.additionalRecipient2 = additionalRecipient2;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	@Column(name = "ADDITIONAL_RECIPIENT3", length = 100)
	public java.lang.String getAdditionalRecipient3() {
		return this.additionalRecipient3;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	public void setAdditionalRecipient3(java.lang.String additionalRecipient3) {
		this.additionalRecipient3 = additionalRecipient3;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴浜虹數璇�
	 */
	@Column(name = "ADDITIONAL_TELPHONE_NUMBER1", length = 100)
	public java.lang.String getAdditionalTelphoneNumber1() {
		return this.additionalTelphoneNumber1;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱旂郴浜虹數璇�
	 */
	public void setAdditionalTelphoneNumber1(
			java.lang.String additionalTelphoneNumber1) {
		this.additionalTelphoneNumber1 = additionalTelphoneNumber1;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴浜虹數璇�
	 */
	@Column(name = "ADDITIONAL_TELPHONE_NUMBER2", length = 100)
	public java.lang.String getAdditionalTelphoneNumber2() {
		return this.additionalTelphoneNumber2;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱旂郴浜虹數璇�
	 */
	public void setAdditionalTelphoneNumber2(
			java.lang.String additionalTelphoneNumber2) {
		this.additionalTelphoneNumber2 = additionalTelphoneNumber2;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴浜虹數璇�
	 */
	@Column(name = "ADDITIONAL_TELPHONE_NUMBER3", length = 100)
	public java.lang.String getAdditionalTelphoneNumber3() {
		return this.additionalTelphoneNumber3;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鑱旂郴浜虹數璇�
	 */
	public void setAdditionalTelphoneNumber3(
			java.lang.String additionalTelphoneNumber3) {
		this.additionalTelphoneNumber3 = additionalTelphoneNumber3;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鑲跨槫鎵�湪鍣ㄥ畼
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TUMOR_LOCATION")
	public DicType getTumorLocation() {
		return this.tumorLocation;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 鑲跨槫鎵�湪鍣ㄥ畼
	 */
	public void setTumorLocation(DicType tumorLocation) {
		this.tumorLocation = tumorLocation;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鑲跨槫浣嶇疆
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SITE")
	public DicType getSite() {
		return this.site;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 鑲跨槫浣嶇疆
	 */
	public void setSite(DicType site) {
		this.site = site;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鐥呯悊璇婃柇
	 */
	@Column(name = "PATHOLOGICAL_DIAGNOSIS", length = 100)
	public String getPathologicalDiagnosis() {
		return this.pathologicalDiagnosis;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 鐥呯悊璇婃柇
	 */
	public void setPathologicalDiagnosis(String pathologicalDiagnosis) {
		this.pathologicalDiagnosis = pathologicalDiagnosis;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 绛夌骇
	 */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	@Column(name = "GRADE")
	public String getGrade() {
		return this.grade;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 绛夌骇
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鏄惁鍒嗗寲
	 */
	@Column(name = "DIFFERENTIATION", length = 100)
	public java.lang.String getDifferentiation() {
		return this.differentiation;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鏄惁鍒嗗寲
	 */
	public void setDifferentiation(java.lang.String differentiation) {
		this.differentiation = differentiation;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鍒嗗寲绋嬪害
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEGREE_OF_DIFFERENTIATION")
	public DicType getDegreeOfDifferentiation() {
		return this.degreeOfDifferentiation;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 鍒嗗寲绋嬪害
	 */
	public void setDegreeOfDifferentiation(DicType degreeOfDifferentiation) {
		this.degreeOfDifferentiation = degreeOfDifferentiation;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鏄惁鍋氳繃娲绘
	 */
	@Column(name = "BIOPSY", length = 100)
	public java.lang.String getBiopsy() {
		return this.biopsy;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鏄惁鍋氳繃娲绘
	 */
	public void setBiopsy(java.lang.String biopsy) {
		this.biopsy = biopsy;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鏃堕棿
	 */
	@Column(name = "DATE_OF_BIOPSY", length = 100)
	public java.util.Date getDateOfBiopsy() {
		return this.dateOfBiopsy;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date 鏃堕棿
	 */
	public void setDateOfBiopsy(java.util.Date dateOfBiopsy) {
		this.dateOfBiopsy = dateOfBiopsy;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 缁撴灉
	 */
	@Column(name = "RESULT", length = 100)
	public java.lang.String getResult() {
		return this.result;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 缁撴灉
	 */
	public void setResult(java.lang.String result) {
		this.result = result;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍘熷彂鎴栬浆绉�
	 */
	@Column(name = "PRIMARY_VS_SECONDARY", length = 100)
	public java.lang.String getPrimaryVsSecondary() {
		return this.primaryVsSecondary;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String 鍘熷彂鎴栬浆绉�
	 */
	public void setPrimaryVsSecondary(java.lang.String primaryVsSecondary) {
		this.primaryVsSecondary = primaryVsSecondary;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鏍锋湰绫诲瀷
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return this.sampleType;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType 鏍锋湰绫诲瀷
	 */
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 鏂规硶: 鍙栧緱User
	 * 
	 * @return: User 鍒涘缓浜�
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 鏂规硶: 璁剧疆User
	 * 
	 * @param: User 鍒涘缓浜�
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 鏂规硶: 鍙栧緱CrmCustomer
	 * 
	 * @return: CrmCustomer 瀹㈡埛id
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER_ID")
	public CrmCustomer getCrmCustomerId() {
		return this.crmCustomerId;
	}

	/**
	 * 鏂规硶: 璁剧疆CrmCustomer
	 * 
	 * @param: CrmCustomer 瀹㈡埛id
	 */
	public void setCrmCustomerId(CrmCustomer crmCustomerId) {
		this.crmCustomerId = crmCustomerId;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鍒涘缓鏃ユ湡
	 */
	@Column(name = "CREATE_DATE", length = 60)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date 鍒涘缓鏃ユ湡
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	public String getSfz() {
		return sfz;
	}

	public void setSfz(String sfz) {
		this.sfz = sfz;
	}

	public String getJc() {
		return jc;
	}

	public void setJc(String jc) {
		this.jc = jc;
	}

	public java.lang.String getPriorCancerHistory() {
		return priorCancerHistory;
	}

	public void setPriorCancerHistory(java.lang.String priorCancerHistory) {
		this.priorCancerHistory = priorCancerHistory;
	}

	public java.lang.String getFamilyHistoryOfCancer() {
		return familyHistoryOfCancer;
	}

	public void setFamilyHistoryOfCancer(java.lang.String familyHistoryOfCancer) {
		this.familyHistoryOfCancer = familyHistoryOfCancer;
	}

	public java.lang.String getInfectiousDiseases() {
		return infectiousDiseases;
	}

	public void setInfectiousDiseases(java.lang.String infectiousDiseases) {
		this.infectiousDiseases = infectiousDiseases;
	}

	public java.lang.String getHistoryOfSmoking() {
		return historyOfSmoking;
	}

	public void setHistoryOfSmoking(java.lang.String historyOfSmoking) {
		this.historyOfSmoking = historyOfSmoking;
	}

	public java.lang.String getEnvironmentalExposures() {
		return environmentalExposures;
	}

	public void setEnvironmentalExposures(
			java.lang.String environmentalExposures) {
		this.environmentalExposures = environmentalExposures;
	}

	public java.lang.String getOtherDiseases() {
		return otherDiseases;
	}

	public void setOtherDiseases(java.lang.String otherDiseases) {
		this.otherDiseases = otherDiseases;
	}

	public java.lang.String getPriorCancerHistorytext() {
		return priorCancerHistorytext;
	}

	public void setPriorCancerHistorytext(
			java.lang.String priorCancerHistorytext) {
		this.priorCancerHistorytext = priorCancerHistorytext;
	}

	public java.lang.String getFamilyHistoryOfCancergx() {
		return familyHistoryOfCancergx;
	}

	public void setFamilyHistoryOfCancergx(
			java.lang.String familyHistoryOfCancergx) {
		this.familyHistoryOfCancergx = familyHistoryOfCancergx;
	}

	public java.lang.String getFamilyHistoryOfCancertext() {
		return familyHistoryOfCancertext;
	}

	public void setFamilyHistoryOfCancertext(
			java.lang.String familyHistoryOfCancertext) {
		this.familyHistoryOfCancertext = familyHistoryOfCancertext;
	}

	public java.lang.String getInfectiousDiseasestext() {
		return infectiousDiseasestext;
	}

	public void setInfectiousDiseasestext(
			java.lang.String infectiousDiseasestext) {
		this.infectiousDiseasestext = infectiousDiseasestext;
	}

	public java.lang.String getHistoryOfSmokingtext() {
		return historyOfSmokingtext;
	}

	public void setHistoryOfSmokingtext(java.lang.String historyOfSmokingtext) {
		this.historyOfSmokingtext = historyOfSmokingtext;
	}

	public java.lang.String getEnvironmentalExposurestext() {
		return environmentalExposurestext;
	}

	public void setEnvironmentalExposurestext(
			java.lang.String environmentalExposurestext) {
		this.environmentalExposurestext = environmentalExposurestext;
	}

	public java.lang.String getOtherDiseasestext() {
		return otherDiseasestext;
	}

	public void setOtherDiseasestext(java.lang.String otherDiseasestext) {
		this.otherDiseasestext = otherDiseasestext;
	}

	@Column(name = "EMAIL", length = 50)
	public java.lang.String getEmail() {
		return email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.util.Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(java.util.Date deathDate) {
		this.deathDate = deathDate;
	}

	public java.lang.String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(java.lang.String randomCode) {
		this.randomCode = randomCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KS")
	public DicType getKs() {
		return ks;
	}

	public void setKs(DicType ks) {
		this.ks = ks;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CUSTOMER")
	public CrmCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CrmCustomer customer) {
		this.customer = customer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CUSTOMER_DOCTOR")
	public CrmDoctor getCustomerDoctor() {
		return customerDoctor;
	}

	public void setCustomerDoctor(CrmDoctor customerDoctor) {
		this.customerDoctor = customerDoctor;
	}

	@Column(name = "YSJ", length = 50)
	public java.lang.String getYsj() {
		return ysj;
	}

	public void setYsj(java.lang.String ysj) {
		this.ysj = ysj;
	}

	@Column(name = "YSJTEXT", length = 50)
	public java.lang.String getYsjtext() {
		return ysjtext;
	}

	public void setYsjtext(java.lang.String ysjtext) {
		this.ysjtext = ysjtext;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 癌症种类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE")
	public WeiChatCancerType getCancerType() {
		return cancerType;
	}

	public void setCancerType(WeiChatCancerType cancerType) {
		this.cancerType = cancerType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子类一
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_ONE")
	public WeiChatCancerTypeSeedOne getCancerTypeSeedOne() {
		return cancerTypeSeedOne;
	}

	public void setCancerTypeSeedOne(WeiChatCancerTypeSeedOne cancerTypeSeedOne) {
		this.cancerTypeSeedOne = cancerTypeSeedOne;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子类二
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_TWO")
	public WeiChatCancerTypeSeedTwo getCancerTypeSeedTwo() {
		return cancerTypeSeedTwo;
	}

	public void setCancerTypeSeedTwo(WeiChatCancerTypeSeedTwo cancerTypeSeedTwo) {
		this.cancerTypeSeedTwo = cancerTypeSeedTwo;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 相关电子病历主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return crmPatient;
	}

	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	public User getModifier() {
		return modifier;
	}

	public void setModifier(User modifier) {
		this.modifier = modifier;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
}