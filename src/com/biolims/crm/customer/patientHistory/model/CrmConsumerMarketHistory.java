package com.biolims.crm.customer.patientHistory.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.product.model.Product;

@Entity
@Table(name = "CRM_CONSUMER_M_HI")
public class CrmConsumerMarketHistory extends
		EntityDao<CrmConsumerMarketHistory> implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -407759837281114436L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//

	@Column(name = "CONSUMPTION_TIME", length = 36)
	private Date consumptionDate;// 消费时间

	@Column(name = "IS_FEE", length = 50)
	private String isFee;// 是否收费

	private Double fee;// 金额

	@Column(name = "FEE_WAY", length = 50)
	private String feeWay;// 收费方式

	@Column(name = "MONTH_FEE", length = 50)
	private String monthFee;// 月结

	@Column(name = "NOTE", length = 200)
	private String note;// 说明

	@Column(name = "BANKNUM", length = 50)
	private String bankNum;// 银行卡号

	@Column(name = "RECEIPTNUM", length = 50)
	private String receipNum;// 收据号

	@Column(name = "POSNUM", length = 50)
	private String posNum;// POS号

	@Column(name = "BACKFEE", length = 50)
	private Double backFee;// 退款金额

	@Column(name = "BACKFEE_DATE", length = 50)
	private Date backFeeDate;// 退款时间

	@Column(name = "INVOICECODE", length = 50)
	private String invoiceCode;// 发票编号

	@Column(name = "INVOICEDATE", length = 50)
	private Date invoiceDate;// 开票日期

	@Column(name = "INVOICENUM", length = 50)
	private String invoiceNum;// 发票抬头

	@Column(name = "ALIPAYNUM", length = 50)
	private String alipayNum;// 支付宝号

	@Column(name = "REALFEE", length = 50)
	private String realFee;// 到账金额

	@Column(name = "STATE", length = 50)
	private String state;// 状态
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FINANCE_USER_ID")
	private User financeUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	private SampleOrder sampleOrder;// 订单

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	// 订单

	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getIsFee() {
		return isFee;
	}

	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Date getConsumptionDate() {
		return consumptionDate;
	}

	public void setConsumptionDate(Date consumptionDate) {
		this.consumptionDate = consumptionDate;
	}

	public String getFeeWay() {
		return feeWay;
	}

	public void setFeeWay(String feeWay) {
		this.feeWay = feeWay;
	}

	public String getMonthFee() {
		return monthFee;
	}

	public void setMonthFee(String monthFee) {
		this.monthFee = monthFee;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getBankNum() {
		return bankNum;
	}

	public void setBankNum(String bankNum) {
		this.bankNum = bankNum;
	}

	public String getReceipNum() {
		return receipNum;
	}

	public void setReceipNum(String receipNum) {
		this.receipNum = receipNum;
	}

	public String getPosNum() {
		return posNum;
	}

	public void setPosNum(String posNum) {
		this.posNum = posNum;
	}

	public Double getBackFee() {
		return backFee;
	}

	public void setBackFee(Double backFee) {
		this.backFee = backFee;
	}

	public Date getBackFeeDate() {
		return backFeeDate;
	}

	public void setBackFeeDate(Date backFeeDate) {
		this.backFeeDate = backFeeDate;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}

	public String getAlipayNum() {
		return alipayNum;
	}

	public void setAlipayNum(String alipayNum) {
		this.alipayNum = alipayNum;
	}

	public String getRealFee() {
		return realFee;
	}

	public void setRealFee(String realFee) {
		this.realFee = realFee;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public User getFinanceUser() {
		return financeUser;
	}

	public void setFinanceUser(User financeUser) {
		this.financeUser = financeUser;
	}

}
