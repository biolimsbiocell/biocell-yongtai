package com.biolims.crm.customer.patientHistory.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 家属与患者关系表
 * @author lims-platform
 * @date 2016-04-29 11:39:34
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_FAMILY_P_S_T_HI")
@SuppressWarnings("serial")
public class CrmFamilyPatientShipHistory extends
		EntityDao<CrmFamilyPatientShipHistory> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 家属姓名 */
	private String name;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 家属姓名
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 家属姓名
	 */
	public void setName(String name) {
		this.name = name;
	}
}