package com.biolims.crm.customer.patientHistory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 手术明细
 * @author lims-platform
 * @date 2014-07-30 17:11:22
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_PATIENT_SU_HI")
@SuppressWarnings("serial")
public class CrmPatientSurgeriesHistory extends
		EntityDao<CrmPatientSurgeriesHistory> implements java.io.Serializable {
	/** 手术id */
	private java.lang.String id;
	/** 手术或操作 */
	private java.lang.String previousProceduresSurgeies;
	/** 手术或操作名称 */
	private java.lang.String nameOfProcedureOrSurgery;
	/** 手术或操作时间 */
	private java.util.Date dateOfProcedureAndSurgery;
	/** 医院名称 */
	private java.lang.String hospital;
	/** 主治医生 */
	private java.lang.String primarySurgeon;
	/** 手术切缘 */
	private java.lang.String resectionMargins;
	/** 自身癌症史 */
	private java.lang.String priorCancerHistory;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancer;
	/** 传染病 */
	private java.lang.String infectiousDiseases;
	/** 吸烟史 */
	private java.lang.String historyOfSmoking;
	/** 环境风险/毒物 */
	private java.lang.String environmentalExposures;
	/** 其他疾病史 */
	private java.lang.String otherDiseases;
	/** 病人id */
	private CrmPatientHistory crmPatientHistory;
	/** 病人id */
	private CrmPatientDiagnosisHistory crmPatientDiagnosisHistory;

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 手术id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 手术id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 手术或操作
	 */
	@Column(name = "PREVIOUS_PROCEDURES_SURGEIES", length = 100)
	public java.lang.String getPreviousProceduresSurgeies() {
		return this.previousProceduresSurgeies;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 手术或操作
	 */
	public void setPreviousProceduresSurgeies(
			java.lang.String previousProceduresSurgeies) {
		this.previousProceduresSurgeies = previousProceduresSurgeies;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 手术或操作名称
	 */
	@Column(name = "NAME_OF_PROCEDURE_OR_SURGERY", length = 100)
	public java.lang.String getNameOfProcedureOrSurgery() {
		return this.nameOfProcedureOrSurgery;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 手术或操作名称
	 */
	public void setNameOfProcedureOrSurgery(
			java.lang.String nameOfProcedureOrSurgery) {
		this.nameOfProcedureOrSurgery = nameOfProcedureOrSurgery;
	}

	/**
	 * 方法: 取得java.util.Date
	 * 
	 * @return: java.util.Date 手术或操作时间
	 */
	@Column(name = "DATE_OF_PROCEDURE_AND_SURGERY", length = 100)
	public java.util.Date getDateOfProcedureAndSurgery() {
		return this.dateOfProcedureAndSurgery;
	}

	/**
	 * 方法: 设置java.util.Date
	 * 
	 * @param: java.util.Date 手术或操作时间
	 */
	public void setDateOfProcedureAndSurgery(
			java.util.Date dateOfProcedureAndSurgery) {
		this.dateOfProcedureAndSurgery = dateOfProcedureAndSurgery;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 医院名称
	 */
	@Column(name = "HOSPITAL", length = 100)
	public java.lang.String getHospital() {
		return this.hospital;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 医院名称
	 */
	public void setHospital(java.lang.String hospital) {
		this.hospital = hospital;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 主治医生
	 */
	@Column(name = "PRIMARY_SURGEON", length = 100)
	public java.lang.String getPrimarySurgeon() {
		return this.primarySurgeon;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 主治医生
	 */
	public void setPrimarySurgeon(java.lang.String primarySurgeon) {
		this.primarySurgeon = primarySurgeon;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 手术切缘
	 */
	@Column(name = "RESECTION_MARGINS", length = 100)
	public java.lang.String getResectionMargins() {
		return this.resectionMargins;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 手术切缘
	 */
	public void setResectionMargins(java.lang.String resectionMargins) {
		this.resectionMargins = resectionMargins;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 自身癌症史
	 */
	@Column(name = "PRIOR_CANCER_HISTORY", length = 100)
	public java.lang.String getPriorCancerHistory() {
		return this.priorCancerHistory;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 自身癌症史
	 */
	public void setPriorCancerHistory(java.lang.String priorCancerHistory) {
		this.priorCancerHistory = priorCancerHistory;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 家族癌症史
	 */
	@Column(name = "FAMILY_HISTORY_OF_CANCER", length = 100)
	public java.lang.String getFamilyHistoryOfCancer() {
		return this.familyHistoryOfCancer;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 家族癌症史
	 */
	public void setFamilyHistoryOfCancer(java.lang.String familyHistoryOfCancer) {
		this.familyHistoryOfCancer = familyHistoryOfCancer;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 传染病
	 */
	@Column(name = "INFECTIOUS_DISEASES", length = 100)
	public java.lang.String getInfectiousDiseases() {
		return this.infectiousDiseases;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 传染病
	 */
	public void setInfectiousDiseases(java.lang.String infectiousDiseases) {
		this.infectiousDiseases = infectiousDiseases;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 吸烟史
	 */
	@Column(name = "HISTORY_OF_SMOKING", length = 100)
	public java.lang.String getHistoryOfSmoking() {
		return this.historyOfSmoking;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 吸烟史
	 */
	public void setHistoryOfSmoking(java.lang.String historyOfSmoking) {
		this.historyOfSmoking = historyOfSmoking;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 环境风险/毒物
	 */
	@Column(name = "ENVIRONMENTAL_EXPOSURES", length = 100)
	public java.lang.String getEnvironmentalExposures() {
		return this.environmentalExposures;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 环境风险/毒物
	 */
	public void setEnvironmentalExposures(
			java.lang.String environmentalExposures) {
		this.environmentalExposures = environmentalExposures;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 其他疾病史
	 */
	@Column(name = "OTHER_DISEASES", length = 100)
	public java.lang.String getOtherDiseases() {
		return this.otherDiseases;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String 其他疾病史
	 */
	public void setOtherDiseases(java.lang.String otherDiseases) {
		this.otherDiseases = otherDiseases;
	}

	/**
	 * 方法: 取得CrmPatient
	 * 
	 * @return: CrmPatient 病人id
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT_HISTORY")
	public CrmPatientHistory getCrmPatientHistory() {
		return crmPatientHistory;
	}

	/**
	 * 方法: 设置CrmPatient
	 * 
	 * @param: CrmPatient 病人id
	 */

	public void setCrmPatientHistory(CrmPatientHistory crmPatientHistory) {
		this.crmPatientHistory = crmPatientHistory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT_DIAGNOSIS_HISTORY")
	public CrmPatientDiagnosisHistory getCrmPatientDiagnosisHistory() {
		return crmPatientDiagnosisHistory;
	}

	public void setCrmPatientDiagnosisHistory(
			CrmPatientDiagnosisHistory crmPatientDiagnosisHistory) {
		this.crmPatientDiagnosisHistory = crmPatientDiagnosisHistory;
	}

}