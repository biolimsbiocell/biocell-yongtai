package com.biolims.crm.customer.linkman.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.mail.Part;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.linkman.dao.CrmLinkManDao;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.patient.dao.CrmPatientDao;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendEmailUtil;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmLinkManService {
	@Resource
	private CrmLinkManDao crmLinkManDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CrmPatientDao crmPatientDao;

	public Map<String, Object> findCrmLinkManList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String userId) {
		return crmLinkManDao.selectCrmLinkManList(mapForQuery, startNum, limitNum, dir, sort, type, userId);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmLinkMan i) throws Exception {

		crmLinkManDao.saveOrUpdate(i);

	}

	public CrmLinkMan get(String id) {
		CrmLinkMan crmLinkMan = commonDAO.get(CrmLinkMan.class, id);
		return crmLinkMan;
	}

	public Map<String, Object> findCrmLinkManList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmLinkManDao.selectCrmLinkManList(scId, startNum, limitNum, dir, sort);
		List<CrmLinkManItem> list = (List<CrmLinkManItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmLinkManItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmLinkManDao.selectCrmLinkManItemList(scId, startNum, limitNum, dir, sort);
		List<CrmLinkManItem> list = (List<CrmLinkManItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmLinkManItem(CrmLinkMan sc, String itemDataJson) throws Exception {
		List<CrmLinkManItem> saveItems = new ArrayList<CrmLinkManItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmLinkManItem scp = new CrmLinkManItem();
			// 将map信息读入实体类
			scp = (CrmLinkManItem) crmLinkManDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmLinkMan(sc);

			saveItems.add(scp);
		}
		crmLinkManDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmLinkMan sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmLinkManDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmLinkManItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmLinkManItem(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void operCrmLinkManEmail() throws Exception {
		List<User> userList = crmPatientDao.findTableWhere("User", null, null);
		for (User aUser : userList) {
			String userEmail = aUser.getEmail();

			if (aUser.getEmailPassword() != null && !aUser.getEmailPassword().equals("")) {
				List<SendEmailUtil> emailList = SendEmailUtil.getEmailList(userEmail, aUser.getEmailPassword());
				for (SendEmailUtil seu : emailList) {
					List clmList = crmLinkManDao.findByProperty(CrmLinkMan.class, "email", seu.getFrom().substring(
							seu.getFrom().indexOf("<") + 1, seu.getFrom().lastIndexOf(">")));
					if (clmList.size() > 0) {
						List<CrmLinkManItem> MessageId = crmPatientDao.findTableWhere("CrmLinkManItem", "messageId",
								"'" + seu.getMessageId() + "'");
						if (MessageId.size() == 0) {
							//							System.out.println(seu.getFrom().substring(seu.getFrom().indexOf("<") + 1,
							//									seu.getFrom().lastIndexOf(">")));
							CrmLinkManItem cmi = new CrmLinkManItem();
							cmi.setLink("emailin");
							cmi.setMessageId(seu.getMessageId());
							cmi.setContent("请查看附件");
							cmi.setHeadLine(seu.getSubject());
							cmi.setEmailNote(seu.getBodyText());
							cmi.setCreateDate(seu.getSentDateByDate());
							cmi.setDutyUser(aUser);
							CrmLinkMan clm = (CrmLinkMan) clmList.get(0);
							cmi.setCrmLinkMan(clm);
							crmLinkManDao.saveOrUpdate(cmi);
						}
					}
				}
			}
		}
		for (User aUser : userList) {
			String userEmail = aUser.getEmail();

			if (aUser.getEmailPassword() != null && !aUser.getEmailPassword().equals("")) {
				List<SendEmailUtil> emailList = SendEmailUtil.getEmailList(userEmail, aUser.getEmailPassword());
				for (SendEmailUtil seu : emailList) {
					List clmList = crmLinkManDao.findByProperty(CrmPatient.class, "email", seu.getFrom().substring(
							seu.getFrom().indexOf("<") + 1, seu.getFrom().lastIndexOf(">")));
					if (clmList.size() > 0) {
						List<CrmLinkManItem> MessageId = crmPatientDao.findTableWhere("CrmLinkManItem", "messageId",
								"'" + seu.getMessageId() + "'");
						if (MessageId.size() == 0) {
							//							System.out.println(seu.getFrom().substring(seu.getFrom().indexOf("<") + 1,
							//									seu.getFrom().lastIndexOf(">")));
							CrmLinkManItem cmi = new CrmLinkManItem();
							cmi.setLink("emailin");
							cmi.setMessageId(seu.getMessageId());
							cmi.setHeadLine(seu.getSubject());
							cmi.setEmailNote(seu.getBodyText());
							cmi.setContent("请查看附件");
							cmi.setCreateDate(seu.getSentDateByDate());
							cmi.setDutyUser(aUser);
							CrmPatient clm = (CrmPatient) clmList.get(0);
							cmi.setCrmPatient(clm);
							crmLinkManDao.saveOrUpdate(cmi);
						}
					}
				}
			}
		}
		List<SendEmailUtil> emailOutList = SendEmailUtil.getOutEmailList();
		for (SendEmailUtil seu : emailOutList) {
			try {
				List clmList = crmLinkManDao.findByProperty(CrmLinkMan.class, "email", seu.getMailAddress("to")
						.substring(seu.getMailAddress("to").indexOf("<") + 1, seu.getMailAddress("to").indexOf(">")));
				List clmList1 = crmLinkManDao.findByProperty(User.class, "email", seu.getFrom().substring(
						seu.getFrom().indexOf("<") + 1, seu.getFrom().indexOf(">")));
				if ((clmList.size() > 0) && (clmList1.size() > 0)) {
					List<CrmLinkManItem> MessageId = crmPatientDao.findTableWhere("CrmLinkManItem", "messageId", "'"
							+ seu.getMessageId() + "'");
					if (MessageId.size() == 0) {
						CrmLinkManItem cmi = new CrmLinkManItem();
						cmi.setMessageId(seu.getMessageId());
						cmi.setEmailNote(seu.getBodyText());
						cmi.setCreateDate(seu.getSentDateByDate());
						if (clmList.size() > 0) {
							CrmLinkMan clm = (CrmLinkMan) clmList.get(0);
							cmi.setCrmLinkMan(clm);
						}
						cmi.setLink("emailout");
						cmi.setContent("请查看附件");
						cmi.setHeadLine(seu.getSubject());
						if (clmList1.size() > 0) {
							User clm = (User) clmList1.get(0);
							cmi.setDutyUser(clm);
						}
						crmLinkManDao.saveOrUpdate(cmi);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (SendEmailUtil seu : emailOutList) {
			try {
				List clmList = crmLinkManDao.findByProperty(CrmPatient.class, "email", seu.getMailAddress("to")
						.substring(seu.getMailAddress("to").indexOf("<") + 1, seu.getMailAddress("to").indexOf(">")));
				List clmList1 = crmLinkManDao.findByProperty(User.class, "email", seu.getFrom().substring(
						seu.getFrom().indexOf("<") + 1, seu.getFrom().indexOf(">")));
				if ((clmList.size() > 0) && (clmList1.size() > 0)) {
					List<CrmLinkManItem> MessageId = crmPatientDao.findTableWhere("CrmLinkManItem", "messageId", "'"
							+ seu.getMessageId() + "'");
					if (MessageId.size() == 0) {
						CrmLinkManItem cmi = new CrmLinkManItem();
						cmi.setMessageId(seu.getMessageId());
						cmi.setEmailNote(seu.getBodyText());
						cmi.setCreateDate(seu.getSentDateByDate());
						if (clmList.size() > 0) {
							CrmPatient clm = (CrmPatient) clmList.get(0);
							cmi.setCrmPatient(clm);
						}
						cmi.setLink("emailout");
						cmi.setContent("请查看附件");
						cmi.setHeadLine(seu.getSubject());
						if (clmList1.size() > 0) {
							User clm = (User) clmList1.get(0);
							cmi.setDutyUser(clm);
						}
						crmLinkManDao.saveOrUpdate(cmi);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//		crmLinkManDao.saveOrUpdateAll(saveItems);
	}

}
