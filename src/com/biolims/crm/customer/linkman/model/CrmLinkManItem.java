package com.biolims.crm.customer.linkman.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleOrder;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;

/**   
 * @Title: Model
 * @Description: 活动明细
 * @author lims-platform
 * @date 2014-07-14 11:23:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_LINK_MAN_ITEM")
@SuppressWarnings("serial")
public class CrmLinkManItem extends EntityDao<CrmLinkManItem> implements java.io.Serializable {
	/**序号*/
	private java.lang.String id;
	/**日期*/
	private java.util.Date createDate;
	/**沟通方式*/
	private java.lang.String link;
	/**负责人*/
	private User dutyUser;
	/**内容*/
	private java.lang.String content;
	/**费用*/
	private java.lang.Double fee;
	/**后续安排*/
	private java.lang.String assign;
	/**备注*/
	private java.lang.String note;
	/**所属*/
	private CrmLinkMan crmLinkMan;
	/**所属*/
	private CrmPatient crmPatient;
	/**email内容*/
	private java.lang.String emailNote;
	/**email内容*/
	private java.lang.String messageId;
	/**email内容*/
	private java.lang.String headLine;
	/**
	 * 我方参加人
	 */
	private java.lang.String ourPeople;
	/**
	 * 客户方参加人
	 */
	private java.lang.String customerPeople;
	
	
	/** 订单实体  */
	private SampleOrder sampleOrder;
	
	
	
	/**
	 *方法: 取得CrmLinkMan
	 *@return: CrmLinkMan  所属
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return this.sampleOrder;
	}

	/**
	 *方法: 设置CrmLinkMan
	 *@param: CrmLinkMan  所属
	 */
	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	@Column(name = "OUR_PEOPLE", length = 200)
	public java.lang.String getOurPeople() {
		return ourPeople;
	}

	public void setOurPeople(java.lang.String ourPeople) {
		this.ourPeople = ourPeople;
	}

	@Column(name = "CUSTOMER_PEOPLE", length = 200)
	public java.lang.String getCustomerPeople() {
		return customerPeople;
	}

	public void setCustomerPeople(java.lang.String customerPeople) {
		this.customerPeople = customerPeople;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  序号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  序号
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  日期
	 */
	@Column(name = "CREATE_DATE", length = 20)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  日期
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	/**
	 *方法: 取得User
	 *@return: User  负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DUTY_USER")
	public User getDutyUser() {
		return this.dutyUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  负责人
	 */
	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  内容
	 */
	@Column(name = "CONTENT", length = 2000)
	public java.lang.String getContent() {
		return this.content;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  内容
	 */
	public void setContent(java.lang.String content) {
		this.content = content;
	}

	/**
	 *方法: 取得java.lang.Double
	 *@return: java.lang.Double  费用
	 */
	@Column(name = "FEE", length = 20)
	public java.lang.Double getFee() {
		return this.fee;
	}

	/**
	 *方法: 设置java.lang.Double
	 *@param: java.lang.Double  费用
	 */
	public void setFee(java.lang.Double fee) {
		this.fee = fee;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  后续安排
	 */
	@Column(name = "ASSIGN", length = 2000)
	public java.lang.String getAssign() {
		return this.assign;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  后续安排
	 */
	public void setAssign(java.lang.String assign) {
		this.assign = assign;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  备注
	 */
	@Column(name = "NOTE", length = 2000)
	public java.lang.String getNote() {
		return this.note;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  备注
	 */
	public void setNote(java.lang.String note) {
		this.note = note;
	}

	/**
	 *方法: 取得CrmLinkMan
	 *@return: CrmLinkMan  所属
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_LINK_MAN")
	public CrmLinkMan getCrmLinkMan() {
		return this.crmLinkMan;
	}

	/**
	 *方法: 设置CrmLinkMan
	 *@param: CrmLinkMan  所属
	 */
	public void setCrmLinkMan(CrmLinkMan crmLinkMan) {
		this.crmLinkMan = crmLinkMan;
	}

	@Column(name = "LINK", length = 80)
	public java.lang.String getLink() {
		return link;
	}

	public void setLink(java.lang.String link) {
		this.link = link;
	}

	@Column(name = "EMAIL_NOTE")
	public java.lang.String getEmailNote() {
		return emailNote;
	}

	public void setEmailNote(java.lang.String emailNote) {
		this.emailNote = emailNote;
	}

	@Column(name = "MESSAGE_ID", length = 200)
	public java.lang.String getMessageId() {
		return messageId;
	}

	public void setMessageId(java.lang.String messageId) {
		this.messageId = messageId;
	}

	@Column(name = "HEAD_LINE", length = 400)
	public java.lang.String getHeadLine() {
		return headLine;
	}

	public void setHeadLine(java.lang.String headLine) {
		this.headLine = headLine;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return crmPatient;
	}

	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

}