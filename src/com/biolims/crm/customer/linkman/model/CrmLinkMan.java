package com.biolims.crm.customer.linkman.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.Department;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 联系人
 * @author lims-platform
 * @date 2014-07-14 11:23:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_LINK_MAN")
@SuppressWarnings("serial")
public class CrmLinkMan extends EntityDao<CrmLinkMan> implements java.io.Serializable {
	/**编码*/
	private java.lang.String id;
	/**姓名*/
	private java.lang.String name;
	/**性别*/
	private java.lang.String sex;
	/**密码*/
	private java.lang.String pwd;
	/**线索来源*/
	//	private CrmTrail trailId;
	/**手机*/
	private java.lang.String mobile;
	/**email*/
	private java.lang.String email;
	/**客户来源*/
	private CrmCustomer customerId;
	/**电话*/
	private java.lang.String telephone;
	/**所属部门*/
	private java.lang.String deptName;
	/**市场活动来源*/
	//	private CrmMarketAct marketActId;
	/**传真*/
	private java.lang.String fax;
	/**职务*/
	private java.lang.String job;
	/**请勿致电*/
	private java.lang.String isTel;
	/**请勿传真*/
	private java.lang.String isFax;
	/**请勿邮件*/
	private java.lang.String isMail;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private java.util.Date createDate;
	/**是否私有*/
	private java.lang.String isPrivate;
	/**负责人*/
	private User dutyUser;
	/**负责部门*/
	private Department dutyDept;
	/**状态*/
	private DicState state;
	/**
	 * 是否是主要联系人
	 */
	@Column(name = "IS_ZHUYAO_LINK_MAN", length = 5)
	private java.lang.String isZhuYaoLinkMan;

	public java.lang.String getIsZhuYaoLinkMan() {
		return isZhuYaoLinkMan;
	}

	public void setIsZhuYaoLinkMan(java.lang.String isZhuYaoLinkMan) {
		this.isZhuYaoLinkMan = isZhuYaoLinkMan;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编码
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  姓名
	 */
	@Column(name = "NAME", length = 100)
	public java.lang.String getName() {
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  姓名
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  性别
	 */
	@Column(name = "SEX", length = 100)
	public String getSex() {
		return this.sex;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 *方法: 取得CrmTrail
	 *@return: CrmTrail  线索来源
	 */
	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@NotFound(action = NotFoundAction.IGNORE)
	//	@JoinColumn(name = "TRAIL_ID")
	//	public CrmTrail getTrailId() {
	//		return this.trailId;
	//	}
	//
	//	/**
	//	 *方法: 设置CrmTrail
	//	 *@param: CrmTrail  线索来源
	//	 */
	//	public void setTrailId(CrmTrail trailId) {
	//		this.trailId = trailId;
	//	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  手机
	 */
	@Column(name = "MOBILE", length = 20)
	public java.lang.String getMobile() {
		return this.mobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  手机
	 */
	public void setMobile(java.lang.String mobile) {
		this.mobile = mobile;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  email
	 */
	@Column(name = "EMAIL", length = 50)
	public java.lang.String getEmail() {
		return this.email;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  email
	 */
	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	/**
	 *方法: 取得CrmCustomer
	 *@return: CrmCustomer  客户来源
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CUSTOMER_ID")
	public CrmCustomer getCustomerId() {
		return this.customerId;
	}

	/**
	 *方法: 设置CrmCustomer
	 *@param: CrmCustomer  客户来源
	 */
	public void setCustomerId(CrmCustomer customerId) {
		this.customerId = customerId;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  电话
	 */
	@Column(name = "TELEPHONE", length = 20)
	public java.lang.String getTelephone() {
		return this.telephone;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  电话
	 */
	public void setTelephone(java.lang.String telephone) {
		this.telephone = telephone;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属部门
	 */
	@Column(name = "DEPT_NAME", length = 36)
	public java.lang.String getDeptName() {
		return this.deptName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属部门
	 */
	public void setDeptName(java.lang.String deptName) {
		this.deptName = deptName;
	}

	/**
	 *方法: 取得CrmMarketAct
	 *@return: CrmMarketAct  市场活动来源
	 */
	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@NotFound(action = NotFoundAction.IGNORE)
	//	@JoinColumn(name = "MARKET_ACT_ID")
	//	public CrmMarketAct getMarketActId() {
	//		return this.marketActId;
	//	}
	//
	//	/**
	//	 *方法: 设置CrmMarketAct
	//	 *@param: CrmMarketAct  市场活动来源
	//	 */
	//	public void setMarketActId(CrmMarketAct marketActId) {
	//		this.marketActId = marketActId;
	//	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  传真
	 */
	@Column(name = "FAX", length = 20)
	public java.lang.String getFax() {
		return this.fax;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  传真
	 */
	public void setFax(java.lang.String fax) {
		this.fax = fax;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  职务
	 */
	@Column(name = "JOB", length = 50)
	public java.lang.String getJob() {
		return this.job;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  职务
	 */
	public void setJob(java.lang.String job) {
		this.job = job;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  请勿致电
	 */
	@Column(name = "IS_TEL", length = 5)
	public java.lang.String getIsTel() {
		return this.isTel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  请勿致电
	 */
	public void setIsTel(java.lang.String isTel) {
		this.isTel = isTel;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  请勿传真
	 */
	@Column(name = "IS_FAX", length = 5)
	public java.lang.String getIsFax() {
		return this.isFax;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  请勿传真
	 */
	public void setIsFax(java.lang.String isFax) {
		this.isFax = isFax;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  请勿邮件
	 */
	@Column(name = "IS_MAIL", length = 5)
	public java.lang.String getIsMail() {
		return this.isMail;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  请勿邮件
	 */
	public void setIsMail(java.lang.String isMail) {
		this.isMail = isMail;
	}

	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建日期
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否私有
	 */
	@Column(name = "IS_PRIVATE", length = 5)
	public java.lang.String getIsPrivate() {
		return this.isPrivate;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否私有
	 */
	public void setIsPrivate(java.lang.String isPrivate) {
		this.isPrivate = isPrivate;
	}

	/**
	 *方法: 取得User
	 *@return: User  负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DUTY_USER")
	public User getDutyUser() {
		return this.dutyUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  负责人
	 */
	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	/**
	 *方法: 取得Department
	 *@return: Department  负责部门
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DUTY_DEPT")
	public Department getDutyDept() {
		return this.dutyDept;
	}

	/**
	 *方法: 设置Department
	 *@param: Department  负责部门
	 */
	public void setDutyDept(Department dutyDept) {
		this.dutyDept = dutyDept;
	}

	/**
	 *方法: 取得DicState
	 *@return: DicState  状态
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STATE")
	public DicState getState() {
		return this.state;
	}

	/**
	 *方法: 设置DicState
	 *@param: DicState  状态
	 */
	public void setState(DicState state) {
		this.state = state;
	}

	public java.lang.String getPwd() {
		return pwd;
	}

	public void setPwd(java.lang.String pwd) {
		this.pwd = pwd;
	}

}