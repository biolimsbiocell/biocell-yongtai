package com.biolims.crm.customer.linkman.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.linkman.service.CrmLinkManService;
import com.biolims.dic.model.DicType;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/crm/customer/linkman")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmLinkManAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "230103";
	@Autowired
	private CrmLinkManService crmLinkManService;
	@Autowired
	private CommonService commonService;
	@Autowired
	private SystemCodeService systemCodeService;
	private CrmLinkMan crmLinkMan = new CrmLinkMan();

	@Action(value = "showCrmLinkManList")
	public String showCrmLinkManList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/linkman/crmLinkMan.jsp");
	}

	@Action(value = "showCrmLinkManListJson")
	public void showCrmLinkManListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String custrmeId = getParameterFromRequest("id");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		if (custrmeId != null) {
			map2Query.put("customerId.id", custrmeId);

		}
		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		//		if (isSaleManage.equals("false")) {
		//
		//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		//			map2Query.put("dutyUser.id", user.getId());
		//		}

		Map<String, Object> result = crmLinkManService.findCrmLinkManList(map2Query, startNum, limitNum, dir, sort,
				isSaleManage, user.getId());
		Long count = (Long) result.get("total");
		List<CrmLinkMan> list = (List<CrmLinkMan>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sex", "");
		//		//				map.put("sex-name", "");
		//		map.put("trailId-id", "");
		//		map.put("trailId-name", "");
		map.put("mobile", "");
		map.put("email", "");
		map.put("customerId-id", "");
		map.put("customerId-name", "");
		map.put("telephone", "");
		map.put("deptName", "");
		//		map.put("marketActId-id", "");
		//		map.put("marketActId-name", "");
		map.put("fax", "");
		map.put("job", "");
		map.put("isTel", "");
		map.put("isFax", "");
		map.put("isMail", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("isPrivate", "");
		map.put("isZhuYaoLinkMan", "");
		map.put("dutyUser-id", "");
		map.put("dutyUser-name", "");
		map.put("dutyDept-id", "");
		map.put("dutyDept-name", "");
		map.put("state-id", "");
		map.put("state-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "crmLinkManSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmLinkManList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("custurmId", getParameterFromRequest("id"));
		return dispatcher("/WEB-INF/page/crm/customer/linkman/crmLinkManDialog.jsp");
	}

	@Action(value = "operCrmLinkManEmail", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void operCrmLinkManEmail() throws Exception {

		crmLinkManService.operCrmLinkManEmail();
	}

	@Action(value = "showDialogCrmLinkManListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmLinkManListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();

		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
		//		if (isSaleManage.equals("false")) {
		//
		//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		//			map2Query.put("dutyUser.id", user.getId());
		//		}

		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

		Map<String, Object> result = crmLinkManService.findCrmLinkManList(map2Query, startNum, limitNum, dir, sort,
				isSaleManage, user.getId());
		Long count = (Long) result.get("total");
		List<CrmLinkMan> list = (List<CrmLinkMan>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sex-id", "");
		map.put("sex-name", "");
		map.put("trailId-id", "");
		map.put("trailId-name", "");
		map.put("mobile", "");
		map.put("email", "");
		map.put("customerId-id", "");
		map.put("customerId-name", "");
		map.put("telephone", "");
		map.put("deptName", "");
		map.put("marketActId-id", "");
		map.put("marketActId-name", "");
		map.put("fax", "");
		map.put("job", "");
		map.put("isTel", "");
		map.put("isFax", "");
		map.put("isMail", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("isPrivate", "");
		map.put("dutyUser-id", "");
		map.put("dutyUser-name", "");
		map.put("dutyDept-id", "");
		map.put("dutyDept-name", "");
		map.put("state-id", "");
		map.put("state-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editCrmLinkMan")
	public String editCrmLinkMan() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			crmLinkMan = crmLinkManService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {

			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		//	crmLinkMan.setId(SystemCode.DEFAULT_SYSTEMCODE);
			crmLinkMan.setCreateUser(user);
			crmLinkMan.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/crm/customer/linkman/crmLinkManEdit.jsp");
	}

	@Action(value = "copyCrmLinkMan")
	public String copyCrmLinkMan() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmLinkMan = crmLinkManService.get(id);
		crmLinkMan.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/customer/linkman/crmLinkManEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = crmLinkMan.getId();

//		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
//			String code = systemCodeService.getCodeByPrefix(SystemCode.CRM_LINK_NAME, crmLinkMan.getCustomerId()
//					.getId(), SystemCode.CRM_LINK_CODE, 3, null);
//			crmLinkMan.setId(code);
//		}
		Map aMap = new HashMap();
		aMap.put("crmLinkManItem", getParameterFromRequest("crmLinkManItemJson"));

		crmLinkManService.save(crmLinkMan, aMap);
		return redirect("/crm/customer/linkman/editCrmLinkMan.action?id=" + crmLinkMan.getId());

	}

	@Action(value = "viewCrmLinkMan")
	public String toViewCrmLinkMan() throws Exception {
		String id = getParameterFromRequest("id");
		crmLinkMan = crmLinkManService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/customer/linkman/crmLinkManEdit.jsp");
	}

	@Action(value = "showCrmLinkManItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmLinkManItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/linkman/crmLinkManItem.jsp");
	}

	@Action(value = "showCrmLinkManItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmLinkManItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmLinkManService.findCrmLinkManItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmLinkManItem> list = (List<CrmLinkManItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("dutyUser-name", "");
			map.put("dutyUser-id", "");
			map.put("content", "");
			map.put("fee", "");
			map.put("assign", "");
			map.put("note", "");
			map.put("crmLinkMan-name", "");
			map.put("crmLinkMan-id", "");
			map.put("link", "");
			map.put("emailNote", "");
			map.put("headLine", "");
			map.put("ourPeople", "");
			map.put("customerPeople", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "toEmailNote", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toEmailNote() throws Exception {
		String id = getParameterFromRequest("id");
		try {
			CrmLinkManItem sri = commonService.get(CrmLinkManItem.class, id);
			putObjToContext("sri", sri.getEmailNote());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispatcher("/WEB-INF/page/crm/customer/linkman/editEmailNote.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmLinkManService getCrmLinkManService() {
		return crmLinkManService;
	}

	public void setCrmLinkManService(CrmLinkManService crmLinkManService) {
		this.crmLinkManService = crmLinkManService;
	}

	public CrmLinkMan getCrmLinkMan() {
		return crmLinkMan;
	}

	public void setCrmLinkMan(CrmLinkMan crmLinkMan) {
		this.crmLinkMan = crmLinkMan;
	}

}
