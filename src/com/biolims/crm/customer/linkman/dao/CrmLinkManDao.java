package com.biolims.crm.customer.linkman.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class CrmLinkManDao extends BaseHibernateDao {
	public Map<String, Object> selectCrmLinkManList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type, String userId) {
		String key = " ";
		String hql = " from CrmLinkMan where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);

		//		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
		if (type.equals("false")) {
			//
			//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			key += " and (dutyUser.id = '" + userId + "' or createUser.id = '" + userId
					+ "' or customerId.upCustomerId.dutyManId.id = '" + userId + "' )";
		}

		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmLinkMan> list = new ArrayList<CrmLinkMan>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmLinkManItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmLinkManItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmLinkMan.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmLinkManItem> list = new ArrayList<CrmLinkManItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by createDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmLinkManList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmLinkManItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmLinkMan.customerId.id='" + scId + "'";

		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmLinkManItem> list = new ArrayList<CrmLinkManItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by createDate DESC";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}