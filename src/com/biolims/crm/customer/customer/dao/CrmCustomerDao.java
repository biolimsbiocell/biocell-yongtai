package com.biolims.crm.customer.customer.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.crm.customer.customer.model.CrmCustomerLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CrmCustomerDao extends BaseHibernateDao {
	public Map<String, Object> selectCrmCustomerList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type, String userId) {
		String key = " ";
		String hql = " from CrmCustomer where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);

		// if (type.equals("false")) {
		//
		// key += " and (dutyManId.id = '" + userId + "' or createUser.id = '" + userId
		// + "')";
		// }
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmCustomer> list = new ArrayList<CrmCustomer>();

		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  id ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmCustomerLinkManList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmCustomerLinkMan where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmCustomer.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmCustomerLinkMan> list = new ArrayList<CrmCustomerLinkMan>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> city(Map<String, String> mapForQuery, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String key = "";
		String hql = "from CrmCity where 1=1 ";
		key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmTumourPosition> list = new ArrayList<CrmTumourPosition>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by " + castStrId("id") + " ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> showDialogCrmCustomerTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmCustomer where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmCustomer where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmCustomer> list = new ArrayList<CrmCustomer>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findCrmCustomerTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  CrmCustomer where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmCustomer where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmCustomer> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findProjectTable(Integer start, Integer length, String query, String col, String sort,
			String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Project where 1=1 and mainProject.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Project where 1=1 and mainProject.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Project> list = new ArrayList<Project>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findCrmPatientTable(Integer start, Integer length, String query, String col, String sort,
			String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmPatient where 1=1 and mainProject.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmPatient where 1=1 and mainProject.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmPatient> list = new ArrayList<CrmPatient>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	//科室
	public Map<String, Object> findCrmCustomerIteamTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmCustomerIteam where 1=1 and crmCustomer.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmCustomerIteam where 1=1 and crmCustomer.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmCustomerIteam> list = new ArrayList<CrmCustomerIteam>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	//科室根据科室iD
	public CrmCustomerIteam findCrmCustomerIteamTableById(String id) throws Exception {
		String hql = "from CrmCustomerIteam where 1=1 and id='"+id+"' ";
		CrmCustomerIteam crmCustomerIteam =(CrmCustomerIteam) getSession().createQuery(hql).uniqueResult();
		return crmCustomerIteam;
		}
	public CrmCustomer findCrmCustomerById(String id) throws Exception {
		String hql = "from CrmCustomer where 1=1 and id='"+id+"' ";
		CrmCustomer crmCustomer =(CrmCustomer) getSession().createQuery(hql).uniqueResult();
		return crmCustomer;
		}
	//科室根据科室iD
		public Map<String, Object> findKsByHpId(Integer start,
				Integer length, String query, String col,
				String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from CrmCustomerIteam where 1=1 and crmCustomer.id='" + id + "'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from CrmCustomerIteam where 1=1 and crmCustomer.id='"+id+"' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					key+=" order by "+col+" "+sort;
				}
				List<CrmCustomerIteam> list = new ArrayList<CrmCustomerIteam>();
				list=this.getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length)
						.list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;			
			}

}