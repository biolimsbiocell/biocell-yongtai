package com.biolims.crm.customer.customer.constants;

//系统业务数据的编码
public class SystemCode {

	public static final String CRMCUSTOMER_SYSTEMCODE = "NEW";

	public static final long CRMCUSTOMER_CODE = 100;

	public static final String CRMCUSTOMER_NAME = "CrmCustomer";
}
