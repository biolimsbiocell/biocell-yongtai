package com.biolims.crm.customer.customer.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.contract.common.service.ContractService;
import com.biolims.contract.model.Contract;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.crm.customer.customer.constants.SystemCode;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.crm.customer.customer.service.CrmCustomerService;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.linkman.service.CrmLinkManService;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.crm.project.model.Project;
import com.biolims.crm.project.service.ProjectService;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.file.service.OperFileService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/crm/customer/customer")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmCustomerAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "230101";
	@Autowired
	private CrmCustomerService crmCustomerService;
	private CrmCustomer crmCustomer = new CrmCustomer();
	private CrmCustomerIteam crmCustomerIteam = new CrmCustomerIteam();
	@Autowired
	private CommonService commonService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private OperFileService operFileService;
	@Resource
	private CrmLinkManService crmLinkManService;
	@Resource
	private ProjectService projectService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CrmPatientService crmPatientService;
	private FileInfo fileInfo;
	@Resource
	private ContractService collectionContractService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonDAO commonDAO;
	private String log = "";

	@Action(value = "showContractList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showContractList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/contractList.jsp");
	}

	@Action(value = "showContractListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showContractListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, String> map2Query = new HashMap<String, String>();
			map2Query.put("supplier.id", scId);
			Map<String, Object> result = collectionContractService.findContractList(map2Query, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<Contract> list = (List<Contract>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("mainContract-id", "");
			map.put("contractType-name", "");
			map.put("supplier-id", "");
			map.put("createDate", "");
			map.put("stateName", "");
			map.put("startDate", "yyyy-MM-dd");
			map.put("confirmUser-name", "");
			map.put("endDate", "yyyy-MM-dd");
			map.put("currencyType-name", "");
			map.put("supplier-name", "");
			map.put("signDate", "yyyy-MM-dd");
			map.put("collectionType-name", "");
			map.put("financePeriod", "####");
			map.put("financeUnit-name", "");
			map.put("currencyType-name", "");
			map.put("fee", "####.####");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmLinkManList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmLinkManItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmLinkManItem.jsp");
	}

	@Action(value = "showCrmLinkManListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmLinkManItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmLinkManService.findCrmLinkManList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmLinkManItem> list = (List<CrmLinkManItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("dutyUser-name", "");
			map.put("dutyUser-id", "");
			map.put("content", "");
			map.put("fee", "");
			map.put("assign", "");
			map.put("note", "");
			map.put("crmLinkMan-name", "");
			map.put("crmLinkMan-id", "");
			map.put("link", "");
			map.put("emailNote", "");
			map.put("headLine", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmCustomerList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmCustomerList() throws Exception {
		rightsId = "230101";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomer.jsp");
	}

	@Action(value = "showCrmCustomerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmCustomerListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

		Map<String, Object> result = crmCustomerService.findCrmCustomerList(map2Query, startNum, limitNum, dir, sort,
				isSaleManage, user.getId());
		Long count = (Long) result.get("total");
		List<CrmCustomer> list = (List<CrmCustomer>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("dutyManId-id", "");
		map.put("dutyManId-name", "");
		/*
		 * map.put("dutyDeptId-id", ""); map.put("dutyDeptId-name", "");
		 */
		map.put("name", "");
		map.put("upCustomerId-id", "");
		map.put("upCustomerId-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("capital", "");
		map.put("currencyTypeId-id", "");
		map.put("currencyTypeId-name", "");
		map.put("orgCode", "");
		map.put("industry", "");
		map.put("kind-id", "");
		map.put("kind-name", "");
		/*
		 * map.put("aptitude1", ""); map.put("aptitude2", ""); map.put("aptitude3-id",
		 * ""); map.put("aptitude3-name", "");
		 */
		map.put("bank", "");
		map.put("account", "");
		map.put("bankCode", "");
		map.put("selA-id", "");
		map.put("selA-name", "");
		map.put("selB-id", "");
		map.put("selB-name", "");
		map.put("selC-id", "");
		map.put("selC-name", "");
		map.put("postcode", "");
		map.put("cwsite", "");
		map.put("fhsite", "");
		map.put("street", "");
		map.put("district-id", "");
		map.put("district-name", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "showDialogCrmCustomerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmCustomerListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
		//
		// if (isSaleManage.equals("false")) {
		//
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		// map2Query.put("dutyManId.id", user.getId());
		// }

		Map<String, Object> result = crmCustomerService.findCrmCustomerList(map2Query, startNum, limitNum, dir, sort,
				isSaleManage, user.getId());
		Long count = (Long) result.get("total");
		List<CrmCustomer> list = (List<CrmCustomer>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("dutyManId-id", "");
		map.put("dutyManId-name", "");
		map.put("dutyDeptId-id", "");
		map.put("dutyDeptId-name", "");
		map.put("name", "");
		map.put("upCustomerId-id", "");
		map.put("upCustomerId-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("capital", "");
		map.put("currencyTypeId-id", "");
		map.put("currencyTypeId-name", "");
		map.put("orgCode", "");
		map.put("industry", "");
		map.put("kind-id", "");
		map.put("kind-name", "");
		map.put("aptitude1", "");
		map.put("aptitude2", "");
		map.put("aptitude3-id", "");
		map.put("aptitude3-name", "");
		map.put("bank", "");
		map.put("account", "");
		map.put("bankCode", "");
		map.put("street", "");
		map.put("ks", "");
		map.put("postcode", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "showDialogCrmCustomerTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmCustomerTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = crmCustomerService.showDialogCrmCustomerTableJson(start, length, query, col, sort);
		List<CrmCustomer> list = (List<CrmCustomer>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("crmPhone", "");
		map.put("name", "");
		map.put("dutyManId-name", "");
		map.put("street", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "editCrmCustomer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editCrmCustomer() throws Exception {
		long num = 0;
		long num1 = 0;
		String id = getParameterFromRequest("id");
		rightsId = "230101";
		if (id != null && !id.equals("")) {
			crmCustomer = crmCustomerService.get(id);
			num = fileInfoService.findFileInfoCount(id, "crmCustomer");
			num1 = fileInfoService.findFileInfoCount(id, "crmCustomerzz");
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			DicState d = this.commonService.get(DicState.class, "1cs");
			crmCustomer.setState(d);
			crmCustomer.setId(SystemCode.CRMCUSTOMER_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmCustomer.setCreateUser(user);
			crmCustomer.setCreateDate(new Date());
			crmCustomer.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			crmCustomer.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		putObjToContext("fileNum1", num1);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerEdit.jsp");
	}

	@Action(value = "copyCrmCustomer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String copyCrmCustomer() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmCustomer = crmCustomerService.get(id);
		crmCustomer.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerEdit.jsp");
	}

	@Action(value = "showCrmCustomerOrderList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmCustomerOrderList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerOrder.jsp");
	}

	@Action(value = "showCrmCustomerLinkManList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmCustomerLinkManList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerLinkMan.jsp");
	}

	@Action(value = "showCrmCustomerLinkManListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmCustomerLinkManListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, String> map2Query = new HashMap<String, String>();
			String scId = getParameterFromRequest("id");
			map2Query.put("customerId.id", scId);
			String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

			Map<String, Object> result = crmLinkManService.findCrmLinkManList(map2Query, startNum, limitNum, dir, sort,
					isSaleManage, user.getId());
			Long count = (Long) result.get("total");
			// System.out.println(count);
			List<CrmLinkMan> list = (List<CrmLinkMan>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("email", "");
			map.put("name", "");
			map.put("telephone", "");
			// map.put("sex-id", "");
			// map.put("sex-name", "");
			// map.put("trailId-id", "");
			// map.put("trailId-name", "");
			map.put("mobile", "");
			map.put("customerId-id", "");
			map.put("customerId-name", "");
			// map.put("linkMan_Post", "");

			map.put("deptName", "");
			// map.put("marketActId-id", "");
			// map.put("marketActId-name", "");
			map.put("fax", "");
			map.put("job", "");
			map.put("isTel", "");
			map.put("isFax", "");
			map.put("isMail", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("isPrivate", "");
			map.put("isZhuYaoLinkMan", "");
			map.put("dutyUser-id", "");
			map.put("dutyUser-name", "");
			map.put("dutyDept-id", "");
			map.put("dutyDept-name", "");
			map.put("state-id", "");
			map.put("state-name", "");
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showcrmProjectListItempage", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showcrmProjectListItempage() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmProjectItmList.jsp");
	}

	@Action(value = "showcrmProjectListItempageJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showcrmProjectListItempageJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, String> map2Query = new HashMap<String, String>();
			String scId = getParameterFromRequest("id");
			map2Query.put("mainProject.id", scId);
			// System.out.println(scId + "s" + startNum + "l" + limitNum + "d" +
			// dir + "s" + sort);
			/*
			 * Map<String, Object> result =
			 * crmCustomerService.findCrmCustomerLinkManList(scId, startNum, limitNum, dir,
			 * sort); Long total = (Long) result.get("total"); List<CrmCustomerLinkMan> list
			 * = (List<CrmCustomerLinkMan>) result.get("list"); Map<String, String> map =
			 * new HashMap<String, String>();
			 */
			Map<String, Object> result = projectService.findCrmProjectList(map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			// System.out.println(count);
			List<Project> list = (List<Project>) result.get("list");

			System.out.println("######################################");
			System.out.println("this is  test" + list.size());

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("type-name", "");
			map.put("applyDate", "yyyy-MM-dd");
			map.put("department-briefName", "");
			map.put("dutyUser-name", "");
			map.put("isRemind", "");
			// map.put("budget-id", "");
			map.put("contract-id", "");
			map.put("mainProject-id", "");
			map.put("state-name", "");
			map.put("applyUser-name", "");
			map.put("dqjz", "");
			map.put("question", "");
			map.put("confirmUser-name", "");
			map.put("planStartDate", "yyyy-MM-dd");
			map.put("planEndDate", "yyyy-MM-dd");
			map.put("factStartDate", "yyyy-MM-dd");
			map.put("factEndDate", "yyyy-MM-dd");
			map.put("fileUploadDate", "yyyy-MM-dd");
			map.put("contract-startDate", "yyyy-MM-dd");
			map.put("contract-startDate", "yyyy-MM-dd");
			map.put("htDate", "");
			map.put("lastDate", "");
			map.put("note", "");
			map.put("crmlinkMan-name", "");
			map.put("mainProject-name", "");

			map.put("inspectionDepartment-id", "");
			map.put("inspectionDepartment-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			// System.out.println("this is Map.test" + map.size());
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmPantienManpageList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmPantienManpageList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmPatientManItmList.jsp");
	}

	@Action(value = "showCrmPantienManpageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmPantienManpageListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, String> map2Query = new HashMap<String, String>();
			String scId = getParameterFromRequest("id");
			// map2Query.put("customerDoctor.id", scId);
			// System.out.println(scId + "s" + startNum + "l" + limitNum + "d" +
			// dir + "s" + sort);
			/*
			 * Map<String, Object> result =
			 * crmCustomerService.findCrmCustomerLinkManList(scId, startNum, limitNum, dir,
			 * sort); Long total = (Long) result.get("total"); List<CrmCustomerLinkMan> list
			 * = (List<CrmCustomerLinkMan>) result.get("list"); Map<String, String> map =
			 * new HashMap<String, String>();
			 */
			String isSaleManage = (String) this.getObjFromSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			Map<String, Object> result = crmPatientService.findCrmPatientList(scId, startNum, limitNum, dir, sort,
					isSaleManage);
			Long count = (Long) result.get("total");
			// System.out.println(count);
			List<CrmPatient> list = (List<CrmPatient>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("lastName", "");
			map.put("firstName", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("dateOfBirth", "yyyy-MM-dd");
			map.put("age", "");
			map.put("placeOfBirth", "");
			map.put("race", "");
			map.put("jc", "");
			map.put("adultTeenager", "");
			map.put("maritalStatus", "");
			map.put("occupation", "");
			map.put("consentReceived", "");
			map.put("patientStatus-id", "");
			map.put("patientStatus-name", "");
			map.put("address", "");
			map.put("telphoneNumber1", "");
			map.put("telphoneNumber2", "");
			map.put("telphoneNumber3", "");
			map.put("additionalRecipient1", "");
			map.put("additionalRecipient2", "");
			map.put("additionalRecipient3", "");
			map.put("additionalTelphoneNumber1", "");
			map.put("additionalTelphoneNumber2", "");
			map.put("additionalTelphoneNumber3", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("crmCustomerId-id", "");
			map.put("crmCustomerId-name", "");
			map.put("createDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmCustomerContractList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmCustomerContractList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerContract.jsp");
	}

	@Action(value = "citySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void citySelect() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getRequest().getParameter("data");
		if (id != null) {
			String city = this.crmCustomerService.city(id);
			map.put("message", city);
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
	}

	@Action(value = "toDateUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toDateUpload() {
		return dispatcher("/WEB-INF/page/crm/customer/customer/dataUpload.jsp");
	}

	@Action(value = "ajaxDateUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void ajaxDateUpload() throws Exception {
		String filepath = getRequest().getParameter("filepath");
		// List<String[]> list = new ArrayList<String[]>();
		// InputStream is = new FileInputStream(filepath);
		// HSSFWorkbook hwk = new HSSFWorkbook(is);
		// HSSFSheet sh = hwk.getSheetAt(0);
		// int rows = sh.getLastRowNum() + 1 - sh.getFirstRowNum();
		// for (int i = 0; i < rows; i++) {
		// HSSFRow row = sh.getRow(i);
		// int cols = row.getLastCellNum() + 1 - row.getFirstCellNum();
		// String[] str = new String[cols];
		// for (int j = 0; j < cols; j++) {
		// Object col = row.getCell((short) j);
		// str[j] = col.toString();
		// }
		// }
	}

	@Action(value = "showCrmCustomerEditList")
	public String showCrmCustomerEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerEditList.jsp");
	}

	@Action(value = "showCrmCustomerTableJson")
	public void showCrmCustomerTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = crmCustomerService.findCrmCustomerTable(start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<CrmCustomer> list = (List<CrmCustomer>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("ks", "");
			map.put("district-id", "");
			map.put("district-name", "");
			map.put("name", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("street", "");
			map.put("customerType", "");
			map.put("postcode", "");
			map.put("email", "");
			map.put("yjly", "");
			map.put("dutyManId-id", "");
			map.put("dutyManId-name", "");
			map.put("state-id", "");
			map.put("state-name", "");
			map.put("id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("CrmCustomer");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "crmCustomerSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String crmCustomerSelectTable() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerDialogTable.jsp");
	}

	@Action(value = "crmCustomerSelectTable1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmCustomerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerSelectTable.jsp");
	}
//	@Action(value = "save", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String save() throws Exception {
//		String id = crmCustomer.getId();
//		if (id != null && id.equals("")) {
//			crmCustomer.setId(null);
//		}
//		//
//		String codeItem = "" + ((char) ((int) (Math.random() * 26) + 65))
//				+ ((char) ((int) (Math.random() * 26) + 65))
//				+ ((char) ((int) (Math.random() * 26) + 65))
//				+ ((int) (Math.random() * 10)) + ((int) (Math.random() * 10))
//				+ ((int) (Math.random() * 10));
//
//		if (crmCustomer.getRadomPassword() == null
//				|| (crmCustomer.getRadomPassword() != null && crmCustomer
//						.getRadomPassword().equals(""))) {
//
//			crmCustomer.setRadomPassword(codeItem);
//		}
//		// DicType type = commonService.get(DicType.class, crmCustomer.getType()
//		// .getId());
//		DicType type = crmCustomer.getType();
//		if (id == null || id.length() <= 0
//				|| SystemCode.CRMCUSTOMER_SYSTEMCODE.equals(id)) {
//
//			String code = "";
//			if ("HOS".equals(type.getId())) {
//				code = systemCodeService.getCodeByPrefix("CrmCustomer", "HOS",
//						0000, 4, null);
//			} else if ("SRO".equals(type.getId())) {
//				code = systemCodeService.getCodeByPrefix("CrmCustomer", "SRO",
//						0000, 4, null);
//			} else if ("CCN".equals(type.getId())) {
//				code = systemCodeService.getCodeByPrefix("CrmCustomer", "CCN",
//						0000, 4, null);
//			} else if (type.getSysCode() != null
//					&& !type.getSysCode().equals("")) {
//
//				code = systemCodeService.getCodeByPrefix("CrmCustomer",
//						type.getSysCode(), 0000, 4, null);
//
//			} else {
//
//				code = systemCodeService.getCodeByPrefix("CrmCustomer", "C",
//						0000, 4, null);
//			}
//
//			// String code = systemCodeService.getCodeByPrefix(
//			// SystemCode.CRMCUSTOMER_NAME, type.getSysCode(),
//			// SystemCode.CRMCUSTOMER_CODE, 3, null);
//			// /*
//			// * supplier.getSupplierType().getSysCode() + a.getCode() +
//			// * b.getCode();
//			// */
//			// /*
//			// * + systemCodeService.getSampleCode(SystemCode.SUPPLIER_NAME,
//			// * SystemCode.SUPPLIER_CODE, null, null)
//			// */
//			crmCustomer.setId(code);
//		}
//		Map aMap = new HashMap();
//		aMap.put("crmCustomerLinkMan",
//				getParameterFromRequest("crmCustomerLinkManJson"));
//		crmCustomerService.save(crmCustomer, aMap);
//
//		String bpmTaskId = getParameterFromRequest("bpmTaskId");
//		String url = "/crm/customer/customer/editCrmCustomer.action?id="
//				+ crmCustomer.getId();
//		if (bpmTaskId != null && !bpmTaskId.equals("")
//				&& !bpmTaskId.equals("null"))
//			url += "&bpmTaskId=" + bpmTaskId;
//		return redirect(url);
//	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean success = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			// 科室
			String dataJson = getParameterFromRequest("dataJson");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);
			// 科室

			List<Map<String, Object>> ks = JsonUtils.toListByJsonArray(dataJson, List.class);

			for (Map<String, Object> map : list) {
				crmCustomer = (CrmCustomer) commonDAO.Map2Bean(map, crmCustomer);
			}

			String id = crmCustomer.getId();
			if (id != null && id.equals("")) {
				crmCustomer.setId(null);
			}
			DicType type = crmCustomer.getType();
			if (id == null || id.length() <= 0 || SystemCode.CRMCUSTOMER_SYSTEMCODE.equals(id)) {
				log = "123";
				String code = "";
				if ("HOS".equals(type.getId())) {
					code = systemCodeService.getCodeByPrefix("CrmCustomer", "HOS", 0000, 4, null);
				} else if ("SRO".equals(type.getId())) {
					code = systemCodeService.getCodeByPrefix("CrmCustomer", "SRO", 0000, 4, null);
				} else if ("CCN".equals(type.getId())) {
					code = systemCodeService.getCodeByPrefix("CrmCustomer", "CCN", 0000, 4, null);
				} else if (type.getSysCode() != null && !type.getSysCode().equals("")) {

					code = systemCodeService.getCodeByPrefix("CrmCustomer", type.getSysCode(), 0000, 4, null);

				} else {

					code = systemCodeService.getCodeByPrefix("CrmCustomer", "C", 0000, 4, null);
				}
				crmCustomer.setId(code);
			}

			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("project", getParameterFromRequest("projectJson"));

			crmCustomerService.save(crmCustomer, aMap, changeLog, lMap, changeLogItem,log);
			zId = crmCustomer.getId();
			CrmCustomer crm = crmCustomerService.findCrmCustomerById(zId);
			if (!"".equals(dataJson) && !"[{}]".equals(dataJson)) {

				if (!ks.isEmpty()) {
					// 向科室里添加数据
					for (Map<String, Object> maps : ks) {
						CrmCustomerIteam crmCustomerIteam = new CrmCustomerIteam();
						crmCustomerIteam = (CrmCustomerIteam) commonDAO.Map2Bean(maps, crmCustomerIteam);
						if (!"".equals(crmCustomerIteam.getId()) && crmCustomerIteam.getId() != null) {
							CrmCustomerIteam ct = crmCustomerService
									.findCrmCustomerIteamTableById(crmCustomerIteam.getId());
							if (!"".equals(crmCustomerIteam.getKsPhone()) && crmCustomerIteam.getKsPhone() != null) {
								ct.setKsPhone(crmCustomerIteam.getKsPhone());
							}
							if (!"".equals(crmCustomerIteam.getKsName()) && crmCustomerIteam.getKsName() != null) {
								ct.setKsName(crmCustomerIteam.getKsName());
								ct.setKsNum(crmCustomerIteam.getKsNum());

							}
							crmCustomerService.saveKs(ct);
						} else {
							String ksId = systemCodeService.getCodeByPrefix("CrmCustomerIteam", "KS", 0000, 4, null);
							crmCustomerIteam.setId(ksId);
							crmCustomerIteam.setCrmCustomer(crm);
							crmCustomerService.saveKs(crmCustomerIteam);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			success = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", success);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewCrmCustomer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toViewCrmCustomer() throws Exception {
		String id = getParameterFromRequest("id");
		crmCustomer = crmCustomerService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerEdit.jsp");
	}

	@Action(value = "showProjectTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProjectTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = crmCustomerService.findProjectTable(start, length, query, col, sort, id);
		List<Project> list = (List<Project>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type-name", "");
		map.put("type-id", "");
		map.put("crmDoctor-name", "");
		map.put("crmDoctor-id", "");
		map.put("inspectionDepartment-name", "");
		map.put("inspectionDepartment-id", "");
		map.put("state-name", "");
		map.put("state-id", "");
		map.put("dutyUser-name", "");
		map.put("dutyUser-id", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProject")
	public void delProject() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmCustomerService.delProject(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveProjectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveProjectTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		crmCustomer = commonService.get(CrmCustomer.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("project", getParameterFromRequest("dataJson"));
		lMap.put("project", getParameterFromRequest("changeLog"));
		try {
			crmCustomerService.save(crmCustomer, aMap, "", lMap, "",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showCrmPatientTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmPatientTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = crmCustomerService.findCrmPatientTable(start, length, query, col, sort, id);
		List<CrmPatient> list = (List<CrmPatient>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("lastName", "");
		map.put("firstName", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("placeOfBirth", "");
		map.put("dateOfBirth", "yyyy-MM-dd");
		map.put("age", "");
		map.put("adultTeenager", "");
		map.put("maritalStatus", "");
		map.put("jc", "");
		map.put("occupation", "");
		map.put("consentReceived", "");
		map.put("patientStatus-name", "");
		map.put("patientStatus-id", "");
		map.put("address", "");
		map.put("telphoneNumber1", "");
		map.put("telphoneNumber2", "");
		map.put("telphoneNumber3", "");
		map.put("createUser-name", "");
		map.put("createUser-id", "");
		map.put("crmCustomerId-name", "");
		map.put("crmCustomerId-id", "");
		map.put("createDate", "yyyy-MM-dd");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCrmPatient")
	public void delCrmPatient() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmCustomerService.delCrmPatient(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveCrmPatientTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCrmPatientTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		crmCustomer = commonService.get(CrmCustomer.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("crmPatient", getParameterFromRequest("dataJson"));
		lMap.put("crmPatient", getParameterFromRequest("changeLog"));
		try {
			crmCustomerService.save(crmCustomer, aMap, "", lMap, "",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveCrmCustomerTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCrmCustomerTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				CrmCustomer a = new CrmCustomer();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (CrmCustomer) commonDAO.Map2Bean(map1, a);
				a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				crmCustomerService.save(a, aMap, changeLog, lMap, changeLog,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 通过医院id获取医院信息
	@Action(value = "findCrmCustemrInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findCrmCustemrInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			CrmCustomer a = new CrmCustomer();
			a = crmCustomerService.get(id);
			map.put("data", a);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmCustomerService getCrmCustomerService() {
		return crmCustomerService;
	}

	public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
		this.crmCustomerService = crmCustomerService;
	}

	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public SystemCodeService getSystemCodeService() {
		return systemCodeService;
	}

	public void setSystemCodeService(SystemCodeService systemCodeService) {
		this.systemCodeService = systemCodeService;
	}

	public OperFileService getOperFileService() {
		return operFileService;
	}

	public void setOperFileService(OperFileService operFileService) {
		this.operFileService = operFileService;
	}

	public CrmLinkManService getCrmLinkManService() {
		return crmLinkManService;
	}

	public void setCrmLinkManService(CrmLinkManService crmLinkManService) {
		this.crmLinkManService = crmLinkManService;
	}

	public FileInfo getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	// 科室
	@Action(value = "showCrmCustomerIteamTableJson")
	public void showCrmCustomerIteamTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = crmCustomerService.findCrmCustomerIteamTable(start, length, query, col, sort, id);
		List<CrmCustomerIteam> list = (List<CrmCustomerIteam>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("ksNum", "");
		map.put("ksName", "");
		map.put("ksPhone", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	// 删除科室
	@Action(value = "delKs", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delKs() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmCustomerService.delKs(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showCrmCustomerIteamEditList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmCustomerIteamEditList() throws Exception {
		String id = super.getRequest().getParameter("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/crm/customer/customer/crmCustomerKsItem.jsp");
	}

	@Action(value = "findKsByHpId",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findKsByHpId() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = crmCustomerService.findKsByHpId(start, length, query, col, sort, id);
		List<CrmCustomerIteam> list = (List<CrmCustomerIteam>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("ksNum", "");
		map.put("ksName", "");
		map.put("ksPhone", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

}
