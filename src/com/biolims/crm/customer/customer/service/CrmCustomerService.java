package com.biolims.crm.customer.customer.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.dao.CrmCustomerDao;
import com.biolims.crm.customer.customer.model.CrmCity;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.crm.customer.customer.model.CrmCustomerLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.project.model.Project;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmCustomerService {
	@Resource
	private CrmCustomerDao crmCustomerDao;
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> findCrmCustomerList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String userId) {
		return crmCustomerDao.selectCrmCustomerList(mapForQuery, startNum, limitNum, dir, sort, type, userId);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmCustomer i) throws Exception {

		crmCustomerDao.saveOrUpdate(i);

	}

	public CrmCustomer get(String id) {
		CrmCustomer crmCustomer = commonDAO.get(CrmCustomer.class, id);
		return crmCustomer;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmCustomer sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmCustomerDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmCustomerLinkMan");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmCustomerLinkMan(sc, jsonStr);
			}
		}
	}

	public Map<String, Object> findCrmCustomerLinkManList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmCustomerDao.selectCrmCustomerLinkManList(scId, startNum, limitNum, dir, sort);
		List<CrmCustomerLinkMan> list = (List<CrmCustomerLinkMan>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmCustomerLinkMan(CrmCustomer sc, String itemDataJson) throws Exception {
		List<CrmCustomerLinkMan> saveItems = new ArrayList<CrmCustomerLinkMan>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmCustomerLinkMan scp = new CrmCustomerLinkMan();
			// 将map信息读入实体类
			scp = (CrmCustomerLinkMan) crmCustomerDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmCustomer(sc);

			saveItems.add(scp);
		}
		crmCustomerDao.saveOrUpdateAll(saveItems);
	}

	public String city(String id) throws Exception {
		String city = "";
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("superior", id);
		Map<String, Object> map = crmCustomerDao.city(mapForQuery, null, null, null, null);
		List<CrmCity> list = (List<CrmCity>) map.get("list");
		for (int i = 0; i < list.size(); i++) {
			CrmCity sri = list.get(i);
			city = city + "," + sri.getId() + "," + sri.getName();
		}
		return city;
	}

	public Map<String, Object> showDialogCrmCustomerTableJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return crmCustomerDao.showDialogCrmCustomerTableJson(start, length, query, col, sort);
	}

	public Map<String, Object> findCrmCustomerTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return crmCustomerDao.findCrmCustomerTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProject(CrmCustomer sc, String itemDataJson, String logInfo, String changeLogItem,String log)
			throws Exception {
		List<Project> saveItems = new ArrayList<Project>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			Project scp = new Project();
			// 将map信息读入实体类
			scp = (Project) crmCustomerDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMainProject(sc);

			saveItems.add(scp);
		}
		crmCustomerDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CrmCustomer");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatient(CrmCustomer sc, String itemDataJson, String logInfo) throws Exception {
		List<CrmPatient> saveItems = new ArrayList<CrmPatient>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatient scp = new CrmPatient();
			// 将map信息读入实体类
			scp = (CrmPatient) crmCustomerDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCustomer(sc);

			saveItems.add(scp);
		}
		crmCustomerDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CrmCustomer");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmCustomer sc, Map jsonMap, String logInfo, Map logMap, String changeLogItem,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			crmCustomerDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("CrmCustomer");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("project");
			jsonStr = (String) jsonMap.get("project");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProject(sc, jsonStr, logStr, changeLogItem,log);
			}
		}
	}

	public Map<String, Object> findProjectTable(Integer start, Integer length, String query, String col, String sort,
			String id) throws Exception {
		Map<String, Object> result = crmCustomerDao.findProjectTable(start, length, query, col, sort, id);
		List<Project> list = (List<Project>) result.get("list");
		return result;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProject(String[] ids) throws Exception {
		for (String id : ids) {
			Project scp = crmCustomerDao.get(Project.class, id);
			crmCustomerDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("CrmCustomer");
				li.setModifyContent("Project删除信息" + scp.getId());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	public Map<String, Object> findCrmPatientTable(Integer start, Integer length, String query, String col, String sort,
			String id) throws Exception {
		Map<String, Object> result = crmCustomerDao.findCrmPatientTable(start, length, query, col, sort, id);
		List<CrmPatient> list = (List<CrmPatient>) result.get("list");
		return result;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmPatient(String[] ids) throws Exception {
		for (String id : ids) {
			CrmPatient scp = crmCustomerDao.get(CrmPatient.class, id);
			crmCustomerDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getCrmCustomerId().getId());
				li.setClassName("CrmCustomer");
				li.setModifyContent("CrmPatient删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}
	//科室
	public Map<String, Object> findCrmCustomerIteamTable(Integer start, Integer length, String query, String col,
			String sort,String id) throws Exception {
		return crmCustomerDao.findCrmCustomerIteamTable(start, length, query, col, sort,id);
	}
	//科室保存
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKs(CrmCustomerIteam i) throws Exception {
		  crmCustomerDao.saveOrUpdate(i);

	}
	//删除科室
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKs(String[] ids) {
		for (String id : ids) {
			CrmCustomerIteam scp = crmCustomerDao.get(CrmCustomerIteam.class, id);
			crmCustomerDao.delete(scp);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getCrmCustomer().getId());
				li.setClassName("CrmCustomer");
				li.setModifyContent("医院管理:"+"科室编号:"+scp.getKsNum()+"科室名称:"+scp.getKsName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}	
	//科室
	public CrmCustomerIteam findCrmCustomerIteamTableById(String id) throws Exception {
		return crmCustomerDao.findCrmCustomerIteamTableById(id);
	}
	//根据医院id 查科室
	public Map<String, Object> findKsByHpId(Integer start,
			Integer length, String query, String col,
			String sort,String id) throws Exception{
		return crmCustomerDao.findKsByHpId(start,length,query,col,sort,id);
	}
	public CrmCustomer findCrmCustomerById(String id) throws Exception {
		return crmCustomerDao.findCrmCustomerById(id);
	}
	
}
