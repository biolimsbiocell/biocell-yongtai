package com.biolims.crm.customer.customer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.Department;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 客户主数据
 * @author lims-platform
 * @date 2014-07-14 11:23:08
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_CUSTOMER")
@SuppressWarnings("serial")
public class CrmCustomer extends EntityDao<CrmCustomer> implements java.io.Serializable {

	/** 医院负责人 */
	private String dutyManName;
	/** 负责人联系电话 */
	private String dutyManPhone;
	/** 客户编码 */
	private java.lang.String id;
	/** 负责人 */
	private User dutyManId;
	/** 负责部门 */
	private Department dutyDeptId;
	/** 名称(客户描述) */
	private java.lang.String name;
	/** 上级客户 */
	private CrmCustomer upCustomerId;
	/** 创建人 */
	private User createUser;
	/** 状态 */
	private DicState state;
	/** 分类 */
	private DicType type;
	/** 创建日期 */
	private java.util.Date createDate;
	/** 注册资本 */
	private java.lang.String capital;
	/** 币种 */
	private DicCurrencyType currencyTypeId;
	/** 机构代码 */
	private java.lang.String orgCode;
	/** 行业 */
	private java.lang.String industry;
	/** 企业性质 */
	private DicType kind;
	/** 资质1 */
	private java.lang.String aptitude1;
	/** 资质2 */
	private java.lang.String aptitude2;
	/** 资质3 */
	private DicType aptitude3;
	/** 开户行 */
	private java.lang.String bank;
	/** 账号 */
	private java.lang.String account;
	/** 银行代码 */
	private java.lang.String bankCode;
	/** 省 */
	private CrmCity selA;
	/** 市 */
	private CrmCity selB;
	/** 区 */
	private CrmCity selC;
	/** 街道 */
	private java.lang.String street;
	/** 邮编 */
	private java.lang.String postcode;
	/** 财务地址 */
	private java.lang.String cwsite;
	/** 发货地址 */
	private java.lang.String fhsite;
	// 是否提醒
	private java.lang.String isRemind;
	/** email */
	private java.lang.String email;
	/** email */
	private java.lang.String ks;
	/** email */
	private java.lang.String yjly;
	// 随机密码
	private java.lang.String radomPassword;
	/** 所属片区 */
	private DicType district;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;

	/** 客户类型 **/
	private String customerType;
	
	/** 机构电话 */
	private String crmPhone;
	public String getCrmPhone() {
		return crmPhone;
	}
	public void setCrmPhone(String crmPhone) {
		this.crmPhone = crmPhone;
	}
	public String getDutyManName() {
		return dutyManName;
	}

	public String getDutyManPhone() {
		return dutyManPhone;
	}

	public void setDutyManName(String dutyManName) {
		this.dutyManName = dutyManName;
	}

	public void setDutyManPhone(String dutyManPhone) {
		this.dutyManPhone = dutyManPhone;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DISTRICT")
	public DicType getDistrict() {
		return district;
	}

	public void setDistrict(DicType district) {
		this.district = district;
	}

	@Column(name = "IS_REMIND", length = 100)
	public java.lang.String getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(java.lang.String isRemind) {
		this.isRemind = isRemind;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 客户编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             客户编码
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DUTY_MAN_ID")
	public User getDutyManId() {
		return this.dutyManId;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             负责人
	 */
	public void setDutyManId(User dutyManId) {
		this.dutyManId = dutyManId;
	}

	/**
	 * 方法: 取得Department
	 * 
	 * @return: Department 负责部门
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DUTY_DEPT_ID")
	public Department getDutyDeptId() {
		return this.dutyDeptId;
	}

	/**
	 * 方法: 设置Department
	 * 
	 * @param: Department
	 *             负责部门
	 */
	public void setDutyDeptId(Department dutyDeptId) {
		this.dutyDeptId = dutyDeptId;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 名称(客户描述)
	 */
	@Column(name = "NAME", length = 100)
	public java.lang.String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             名称(客户描述)
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得CrmCustomer
	 * 
	 * @return: CrmCustomer 上级客户
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UP_CUSTOMER_ID")
	public CrmCustomer getUpCustomerId() {
		return this.upCustomerId;
	}

	/**
	 * 方法: 设置CrmCustomer
	 * 
	 * @param: CrmCustomer
	 *             上级客户
	 */
	public void setUpCustomerId(CrmCustomer upCustomerId) {
		this.upCustomerId = upCustomerId;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得DicState
	 * 
	 * @return: DicState 状态
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STATE")
	public DicState getState() {
		return this.state;
	}

	/**
	 * 方法: 设置DicState
	 * 
	 * @param: DicState
	 *             状态
	 */
	public void setState(DicState state) {
		this.state = state;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 分类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             分类
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 * 方法: 取得java.util.Date
	 * 
	 * @return: java.util.Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置java.util.Date
	 * 
	 * @param: java.util.Date
	 *             创建日期
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 注册资本
	 */
	@Column(name = "CAPITAL", length = 10)
	public java.lang.String getCapital() {
		return this.capital;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             注册资本
	 */
	public void setCapital(java.lang.String capital) {
		this.capital = capital;
	}

	/**
	 * 方法: 取得DicCurrencyType
	 * 
	 * @return: DicCurrencyType 币种
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CURRENCY_TYPE_ID")
	public DicCurrencyType getCurrencyTypeId() {
		return this.currencyTypeId;
	}

	/**
	 * 方法: 设置DicCurrencyType
	 * 
	 * @param: DicCurrencyType
	 *             币种
	 */
	public void setCurrencyTypeId(DicCurrencyType currencyTypeId) {
		this.currencyTypeId = currencyTypeId;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 机构代码
	 */
	@Column(name = "ORG_CODE", length = 40)
	public java.lang.String getOrgCode() {
		return this.orgCode;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             机构代码
	 */
	public void setOrgCode(java.lang.String orgCode) {
		this.orgCode = orgCode;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 行业
	 */
	@Column(name = "INDUSTRY", length = 100)
	public java.lang.String getIndustry() {
		return this.industry;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             行业
	 */
	public void setIndustry(java.lang.String industry) {
		this.industry = industry;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 企业性质
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KIND")
	public DicType getKind() {
		return this.kind;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             企业性质
	 */
	public void setKind(DicType kind) {
		this.kind = kind;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 资质1
	 */
	@Column(name = "APTITUDE1", length = 100)
	public java.lang.String getAptitude1() {
		return this.aptitude1;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             资质1
	 */
	public void setAptitude1(java.lang.String aptitude1) {
		this.aptitude1 = aptitude1;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 资质2
	 */
	@Column(name = "APTITUDE2", length = 100)
	public java.lang.String getAptitude2() {
		return this.aptitude2;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             资质2
	 */
	public void setAptitude2(java.lang.String aptitude2) {
		this.aptitude2 = aptitude2;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 资质3
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APTITUDE3")
	public DicType getAptitude3() {
		return this.aptitude3;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType
	 *             资质3
	 */
	public void setAptitude3(DicType aptitude3) {
		this.aptitude3 = aptitude3;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 开户行
	 */
	@Column(name = "BANK", length = 100)
	public java.lang.String getBank() {
		return this.bank;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             开户行
	 */
	public void setBank(java.lang.String bank) {
		this.bank = bank;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 账号
	 */
	@Column(name = "ACCOUNT", length = 40)
	public java.lang.String getAccount() {
		return this.account;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             账号
	 */
	public void setAccount(java.lang.String account) {
		this.account = account;
	}

	/**
	 * 方法: 取得java.lang.String
	 * 
	 * @return: java.lang.String 银行代码
	 */
	@Column(name = "BANK_CODE", length = 40)
	public java.lang.String getBankCode() {
		return this.bankCode;
	}

	/**
	 * 方法: 设置java.lang.String
	 * 
	 * @param: java.lang.String
	 *             银行代码
	 */
	public void setBankCode(java.lang.String bankCode) {
		this.bankCode = bankCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELA")
	public CrmCity getSelA() {
		return selA;
	}

	public void setSelA(CrmCity selA) {
		this.selA = selA;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELB")
	public CrmCity getSelB() {
		return selB;
	}

	public void setSelB(CrmCity selB) {
		this.selB = selB;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELC")
	public CrmCity getSelC() {
		return selC;
	}

	public void setSelC(CrmCity selC) {
		this.selC = selC;
	}

	@Column(name = "STREET", length = 100)
	public java.lang.String getStreet() {
		return street;
	}

	public void setStreet(java.lang.String street) {
		this.street = street;
	}

	@Column(name = "POSTCODE", length = 50)
	public java.lang.String getPostcode() {
		return postcode;
	}

	public void setPostcode(java.lang.String postcode) {
		this.postcode = postcode;
	}

	@Column(name = "CWSITE", length = 100)
	public java.lang.String getCwsite() {
		return cwsite;
	}

	public void setCwsite(java.lang.String cwsite) {
		this.cwsite = cwsite;
	}

	@Column(name = "FHSITE", length = 100)
	public java.lang.String getFhsite() {
		return fhsite;
	}

	public void setFhsite(java.lang.String fhsite) {
		this.fhsite = fhsite;
	}

	@Column(name = "STREET", length = 100)
	public java.lang.String street() {
		return this.street;
	}

	public java.lang.String getRadomPassword() {
		return radomPassword;
	}

	public void setRadomPassword(java.lang.String radomPassword) {
		this.radomPassword = radomPassword;
	}

	public java.lang.String getEmail() {
		return email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getKs() {
		return ks;
	}

	public void setKs(java.lang.String ks) {
		this.ks = ks;
	}

	public java.lang.String getYjly() {
		return yjly;
	}

	public void setYjly(java.lang.String yjly) {
		this.yjly = yjly;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}