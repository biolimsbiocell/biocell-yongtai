package com.biolims.crm.customer.customer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "CRM_CUSTOMER_ITEAM")
@SuppressWarnings("serial")
public class CrmCustomerIteam {
	
	/** ID */
	private String id;
	/** 科室编号*/
	private String ksNum;
	/** 科室姓名 */
	private String ksName;
	/** 科室电话 */
	private String ksPhone;
	
	public String getKsName() {
		return ksName;
	}

	@Id
	@Column(name = "id", length = 36)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setKsName(String ksName) {
		this.ksName = ksName;
	}
	public String getKsPhone() {
		return ksPhone;
	}
	public void setKsPhone(String ksPhone) {
		this.ksPhone = ksPhone;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}
	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	public String getKsNum() {
		return ksNum;
	}

	public void setKsNum(String ksNum) {
		this.ksNum = ksNum;
	}

	/**关联主表*/
	private CrmCustomer crmCustomer;
	
	

}
