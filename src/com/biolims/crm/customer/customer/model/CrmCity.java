package com.biolims.crm.customer.customer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "T_CRM_CITY")
public class CrmCity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7805863715704015606L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;
	//上级
	@Column(name = "SUPERIOR", length = 32)
	private String superior;
	@Column(name = "NAME", length = 110)
	private String name;
	@Column(name = "CODE", length = 110)
	private String code;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSuperior() {
		return superior;
	}

	public void setSuperior(String superior) {
		this.superior = superior;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
