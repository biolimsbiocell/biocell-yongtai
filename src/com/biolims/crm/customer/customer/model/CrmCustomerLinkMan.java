package com.biolims.crm.customer.customer.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 联系人
 * @author lims-platform
 * @date 2014-07-30 17:11:07
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_CUSTOMER_LINK_MAN")
@SuppressWarnings("serial")
public class CrmCustomerLinkMan extends EntityDao<CrmCustomerLinkMan> implements java.io.Serializable {
	/**编号*/
	private java.lang.String id;
	private java.lang.String linkMan_name;
	private java.lang.String linkMan_mail;
	private java.lang.String linkMan_telNumber;
	private java.lang.String linkMan_Post;
	/**所属客户*/
	private CrmCustomer crmCustomer;

	/**联系人编号*/
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编号
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	@Column(name = "LinkMan_name", length = 36)
	public java.lang.String getLinkMan_name() {
		return linkMan_name;
	}

	public void setLinkMan_name(java.lang.String linkMan_name) {
		this.linkMan_name = linkMan_name;
	}

	@Column(name = "LinkMan_mail", length = 36)
	public java.lang.String getLinkMan_mail() {
		return linkMan_mail;
	}

	public void setLinkMan_mail(java.lang.String linkMan_mail) {
		this.linkMan_mail = linkMan_mail;
	}

	@Column(name = "LinkMan_telNumber", length = 36)
	public java.lang.String getLinkMan_telNumber() {
		return linkMan_telNumber;
	}

	public void setLinkMan_telNumber(java.lang.String linkMan_telNumber) {
		this.linkMan_telNumber = linkMan_telNumber;
	}

	@Column(name = "LinkMan_Post", length = 50)
	public java.lang.String getLinkMan_Post() {
		return linkMan_Post;
	}

	public void setLinkMan_Post(java.lang.String linkMan_Post) {
		this.linkMan_Post = linkMan_Post;
	}

	/**
	 *方法: 取得CrmCustomer
	 *@return: CrmCustomer  所属客户
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return this.crmCustomer;
	}

	/**
	 *方法: 设置CrmCustomer
	 *@param: CrmCustomer  所属客户
	 */
	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

}