package com.biolims.crm.customer.patient.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
import com.biolims.crm.customer.patient.model.CrmPatientTreat;
import com.biolims.crm.customer.patient.model.CrmPatientTumorInformation;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.crm.customer.patient.model.ScientificSampleInfo;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.technology.dna.model.TechDnaServiceTask;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CrmPatientDao extends BaseHibernateDao {
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> selectCrmPatientList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String userId) {
		String key = " ";
		String hql = " from CrmPatient where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		// if (type != null && userId != null) {
		// if (type.equals("false")) {
		//
		// key += " and (crmCustomerId.dutyManId.id = '" + userId + "' or createUser.id
		// = '" + userId + "')";
		// }
		// }
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatient> list = new ArrayList<CrmPatient>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmPatientNewList(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  CrmPatient where 1=1 ";//and state='1'
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}

		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmPatient where 1=1 ";//and state='1'
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmPatient> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> selectCrmPatientTwoList(Map<String, String> mapForQuery, String name, Integer startNum,
			Integer limitNum, String dir, String sort, String type, String userId) {
		String key = " ";
		String hql = " from CrmPatient where 1=1 and name = '" + name + "'";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);

		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatient> list = new ArrayList<CrmPatient>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmPatientList(String cId, Integer startNum, Integer limitNum, String dir,
			String sort, String type) {
		String key = " ";
		String hql = " from CrmPatient where 1=1 and customer.id = '" + cId + "' or  customerDoctor.id ='" + cId + "'";

		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatient> list = new ArrayList<CrmPatient>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmPatientDiagnosisList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientDiagnosis where 1=1 and num = 1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientDiagnosis> list = new ArrayList<CrmPatientDiagnosis>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by dateOfDiagnosis DESC " + sort + " " + dir;
			} else {
				key = key + " order by dateOfDiagnosis DESC ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientRestsList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientDiagnosis where 1=1 and num = 2";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientDiagnosis> list = new ArrayList<CrmPatientDiagnosis>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by dateOfDiagnosis DESC " + sort + " " + dir;
			} else {
				key = key + " order by dateOfDiagnosis DESC ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientTumorInformationsList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientTumorInformation where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientTumorInformation> list = new ArrayList<CrmPatientTumorInformation>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientRadiologyList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientRadiology where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientRadiology> list = new ArrayList<CrmPatientRadiology>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  dateOfExamination DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientLaboratoryList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientLaboratory where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientLaboratory> list = new ArrayList<CrmPatientLaboratory>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  dateOfExamination DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientLinkManList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientLinkMan where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientLinkMan> list = new ArrayList<CrmPatientLinkMan>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 订单
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmPatientOrderList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SampleOrder where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and medicalNumber ='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientOrderNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleOrder where 1=1 ";
		String key = "";

		if ("".equals(id)) {
			key = key + " and 1=2 ";
		} else {
			key = key + " and medicalNumber='" + id + "' ";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleOrder where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleOrder> list = new ArrayList<SampleOrder>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	/**
	 * 临床样本
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmSampleInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String number) throws Exception {
		String hql = "from SampleInfo t where 1=1 ";
		String key = "";

		if (!number.equals(""))
			key = key + " and t.orderNum in " + number;
		else
			key = key + " and 1=2";

		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmSampleInfoNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleInfo where 1=1 ";
		String key = "";
		if (!id.equals("()"))
			key = key + " and orderNum in " + id;
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleInfo where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInfo> list = new ArrayList<SampleInfo>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	/**
	 * 科研样本
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmScientificSampleInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleInfo t where 1=1 ";
		String key = "";

		if (!scId.equals(""))
			key = key + " and t.patientId = '" + scId + "'";
		else
			key = key + " and 1=2";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> sampleInfo = new ArrayList<SampleInfo>();
		List<ScientificSampleInfo> scientificSampleInfos = new ArrayList<ScientificSampleInfo>();
		List<TechDnaServiceTaskItem> techDnaServiceTaskItem = new ArrayList<TechDnaServiceTaskItem>();
		List<TechDnaServiceTask> techDnaServiceTask = new ArrayList<TechDnaServiceTask>();
		List<Project> project = new ArrayList<Project>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				sampleInfo = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum)
						.list();
				for (int i = 0; i < sampleInfo.size(); i++) {
					int n = 0;
					ScientificSampleInfo scientificSampleInfo1 = new ScientificSampleInfo();
					scientificSampleInfo1 = change(sampleInfo.get(i), scientificSampleInfo1);
					String hql1 = "from TechDnaServiceTaskItem t where 1=1 and t.sampleCode = '"
							+ sampleInfo.get(i).getCode() + "'";
					techDnaServiceTaskItem = this.getSession().createQuery(hql1).list();
					for (int j = 0; j < techDnaServiceTaskItem.size(); j++) {
						ScientificSampleInfo scientificSampleInfo = new ScientificSampleInfo();
						scientificSampleInfo = change(sampleInfo.get(i), scientificSampleInfo);
						n = 1;
						String hql2 = "from TechDnaServiceTask t where 1=1 and t.id = '"
								+ techDnaServiceTaskItem.get(j).getTechDnaServiceTask().getId() + "'";
						techDnaServiceTask = this.getSession().createQuery(hql2).list();
						if (techDnaServiceTask.size() > 0) {
							scientificSampleInfo.setServiceId(techDnaServiceTask.get(0).getId());
							scientificSampleInfo.setServiceName(techDnaServiceTask.get(0).getName());
							String hql3 = "from Project t where 1=1 and t.id = '"
									+ techDnaServiceTask.get(0).getProjectId().getId() + "'";
							project = this.getSession().createQuery(hql3).list();
							if (project.size() > 0) {
								scientificSampleInfo.setProjectId(project.get(0).getId());
								scientificSampleInfo.setProjectName(project.get(0).getName());
							}
						}
						scientificSampleInfos.add(scientificSampleInfo);
					}
					if (n == 0) {
						scientificSampleInfos.add(scientificSampleInfo1);
					}
				}
			} else {
				sampleInfo = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum)
						.list();
				for (int i = 0; i < sampleInfo.size(); i++) {
					int n = 0;
					ScientificSampleInfo scientificSampleInfo1 = new ScientificSampleInfo();
					scientificSampleInfo1 = change(sampleInfo.get(i), scientificSampleInfo1);
					String hql1 = "from TechDnaServiceTaskItem t where 1=1 and t.sampleCode = '"
							+ sampleInfo.get(i).getCode() + "'";
					techDnaServiceTaskItem = this.getSession().createQuery(hql1).list();
					for (int j = 0; j < techDnaServiceTaskItem.size(); j++) {
						n = 1;
						ScientificSampleInfo scientificSampleInfo = new ScientificSampleInfo();
						scientificSampleInfo = change(sampleInfo.get(i), scientificSampleInfo);
						String hql2 = "from TechDnaServiceTask t where 1=1 and t.id = '"
								+ techDnaServiceTaskItem.get(j).getTechDnaServiceTask().getId() + "'";
						techDnaServiceTask = this.getSession().createQuery(hql2).list();
						if (techDnaServiceTask.size() > 0) {
							scientificSampleInfo.setServiceId(techDnaServiceTask.get(0).getId());
							scientificSampleInfo.setServiceName(techDnaServiceTask.get(0).getName());
							String hql3 = "from Project t where 1=1 and t.id = '"
									+ techDnaServiceTask.get(0).getProjectId().getId() + "'";
							project = this.getSession().createQuery(hql3).list();
							if (project.size() > 0) {
								scientificSampleInfo.setProjectId(project.get(0).getId());
								scientificSampleInfo.setProjectName(project.get(0).getName());
							}
						}
						scientificSampleInfos.add(scientificSampleInfo);
					}
					if (n == 0) {
						scientificSampleInfos.add(scientificSampleInfo1);
					}
				}
			}
		}
		Date d = new Date();
		Long time = d.getTime();
		for (int i = 0; i < scientificSampleInfos.size(); i++) {
			scientificSampleInfos.get(i).setId(time.toString());
			time = time + 1;
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", scientificSampleInfos);
		return result;
	}

	/**
	 * 确认收费
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectCrmCustormerList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String number) throws Exception {
		String hql = "from CrmConsumerMarket t where 1=1 ";
		String key = "";
		if (!number.equals("()"))
			key = key + " and t.sampleOrder.id in " + number;
		else
			key = key + " and 1=2";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarket> list = new ArrayList<CrmConsumerMarket>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmCustormerNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmConsumerMarket where 1=1 ";
		String key = "";
		if (!id.equals("()"))
			key = key + " and t.sampleOrder.id in " + id;
		else
			key = key + " and 1=2";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmConsumerMarket where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectCrmPatientPathologyList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientPathology where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientPathology> list = new ArrayList<CrmPatientPathology>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  dateOfExamination DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientTreatList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientTreat where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientTreat> list = new ArrayList<CrmPatientTreat>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by  begin DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientSurgeriesList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientSurgeries where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientSurgeries> list = new ArrayList<CrmPatientSurgeries>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by dateOfProcedureAndSurgery DESC" + sort + " " + dir;
			} else {
				key = key + " order by dateOfProcedureAndSurgery DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientGeneticTestingList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientGeneticTesting where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientGeneticTesting> list = new ArrayList<CrmPatientGeneticTesting>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by dateOfTest DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> cusPosition(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from CrmTumourPosition where 1=1 ";
		key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmTumourPosition> list = new ArrayList<CrmTumourPosition>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	@SuppressWarnings("unchecked")
	public void delModelId(String objName, String objProperty, String vals) throws Exception {
		String hql = " delete from " + objName + " where " + objProperty + " in (" + vals + ")";
		Query query = this.getSession().createQuery(hql);
		query.executeUpdate();
	}

	public Map<String, Object> selectConsumerMarketList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmConsumerMarket where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarket> list = new ArrayList<CrmConsumerMarket>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by consumptionTime DESC";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<CrmConsumerMarket> findCrmConsumerMarket(String type) {
		List<CrmConsumerMarket> list = commonDAO.find("from CrmConsumerMarket  order by isFee asc");
		return list;

	}

	public List<CrmPatientPathology> findCrmPatientPathology(String type) {
		List<CrmPatientPathology> list = commonDAO
				.find("from CrmPatientPathology where crmPatient.id = '" + type + "'");
		return list;

	}

	public Map<String, Object> selectConsumerMarketList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmConsumerMarket where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarket> list = new ArrayList<CrmConsumerMarket>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	//
	// public <table> List<table> findTableWhere(String table, String where, String
	// type) {
	// String hql = "";
	// hql = "from " + table;
	// if (where != null && type != null) {
	// hql = hql + " where " + where + " = '" + type + "'";
	public <table> List<table> findTablehql(String table, String where) {
		List<table> list = commonDAO.find(where);
		return list;
	}

	public <table> List<table> findTableWhere(String table, String where, String type) {
		String hql = "";
		hql = "from " + table;
		if (where != null && type != null) {
			hql = hql + " where " + where + " = " + type + "";

		}
		hql = hql + " order by id asc ";
		List<table> list = commonDAO.find(hql);
		return list;
	}

	public <table> List<table> findTableWhere2(String table, String where, String type, String where2, String type2) {
		String hql = "";
		hql = "from " + table;
		if (where != null && type != null && where2 != null && type2 != null) {
			hql = hql + " where " + where + " = " + type + " and " + where2 + " = " + type2 + "";
		}
		hql = hql + " order by id asc ";
		List<table> list = commonDAO.find(hql);
		return list;
	}

	public Map<String, Object> selectCrmLinkManItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmLinkManItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmLinkManItem> list = new ArrayList<CrmLinkManItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by createDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmLinkManItemNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmLinkManItem where 1=1 and crmPatient.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmLinkManItem where 1=1 and crmPatient.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmLinkManItem> list = new ArrayList<CrmLinkManItem>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 查询该记录是否存在
	public boolean queryByCount(String number) {
		String hql = " from CrmPatient  where id = '" + number + "'";
		List<CrmPatient> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			return true;
		}
		return false;
	}

	// 添加明细
	public Map<String, Object> selectCrmPatientPersonnelList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmPatientPersonnel where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientPersonnel> list = new ArrayList<CrmPatientPersonnel>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmPatientItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CrmPatientItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmPatient.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatientItem> list = new ArrayList<CrmPatientItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据电子病历号查询订单编号
	public String queryByOrderId(String id) {
		String hql = " from SampleOrder  where medicalNumber = '" + id + "'";
		String inStr = "";
		List<SampleOrder> l = this.getSession().createQuery(hql).list();
		SampleOrder order = null;
		for (SampleOrder so : l) {
			if (inStr.equals(""))
				inStr = "'" + so.getId() + "'";
			else
				inStr = inStr + ",'" + so.getId() + "'";
		}

		return "(" + inStr + ")";
	}

	public Map<String, Object> selectChargeSampleList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleOrder where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public static ScientificSampleInfo change(SampleInfo sampleInfo, ScientificSampleInfo scientificSampleInfo) {
		scientificSampleInfo.setId(sampleInfo.getId());
		scientificSampleInfo.setCode(sampleInfo.getCode());
		scientificSampleInfo.setName(sampleInfo.getName());
		scientificSampleInfo.setProductId(sampleInfo.getProductId());
		scientificSampleInfo.setProductName(sampleInfo.getProductName());
		scientificSampleInfo.setState(sampleInfo.getState());
		scientificSampleInfo.setStateName(sampleInfo.getStateName());
		scientificSampleInfo.setPatientName(sampleInfo.getPatientName());
		scientificSampleInfo.setNote(sampleInfo.getNote());
		scientificSampleInfo.setOrderNum(sampleInfo.getOrderNum());
		scientificSampleInfo.setPatientId(sampleInfo.getPatientId());
		scientificSampleInfo.setIdCard(sampleInfo.getIdCard());
		scientificSampleInfo.setBusinessType(sampleInfo.getBusinessType());
		scientificSampleInfo.setPrice(sampleInfo.getPrice());
		scientificSampleInfo.setType(sampleInfo.getType());
		scientificSampleInfo.setUpLoadAccessory(sampleInfo.getUpLoadAccessory());
		return scientificSampleInfo;
	}

	// 根据订单号查询数据
	public CrmConsumerMarket selCrmConsumerMarket(String id) {
		String hql = "from CrmConsumerMarket where 1=1 and sampleOrder.id='" + id + "'  ";//and state='1'
		CrmConsumerMarket p = (CrmConsumerMarket) this.getSession().createQuery(hql).uniqueResult();
		return p;
	}

	public Map<String, Object> crmPatientSelectTableJsonByName(Integer start, Integer length, String query, String col,
			String sort, String name) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmPatient where 1=1  ";//and state='1'
		if(name!=null
				&&!"".equals(name)){
			countHql  = countHql+" and name like '" + name + "'";
		}
		String key = "";
		if (query != null)
			key = map2Where(query);
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmPatient where 1=1  ";//and state='1'
			if(name!=null
					&&!"".equals(name)){
				hql  = hql+" and name like '" + name + "'";
			}
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmPatient> list = new ArrayList<CrmPatient>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public long selectFileInfoCount(String sId, String type, String userType) throws Exception {

		return this.getCount("from FileInfo where ownerModel = '" + type + "' and modelContentId = '" + sId
				+ "' and useType='" + userType + "' ");
	}

	public CrmPatient getPatient(String idCard) {
		String hql = " from CrmPatient where 1=1 and sfz = '" + idCard + "'";
		return (CrmPatient) this.getSession().createQuery(hql).uniqueResult();
	}

	public Map<String, Object> findCrmPatientDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmPatient where 1=1 ";//and state = '1'
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CrmPatient where 1=1 ";//and state = '1'
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CrmPatient> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}