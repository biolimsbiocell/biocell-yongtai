package com.biolims.crm.customer.patient.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;

@Repository
@SuppressWarnings("unchecked")
public class CrmTumourPositionDao extends BaseHibernateDao {
	@Resource
	private CommonDAO commonDAO;

	public List<CrmTumourPosition> findDicSamplType(String type) {
		List<CrmTumourPosition> list = commonDAO.find("from CrmTumourPosition where superior=0 order by id asc");
		return list;

	}

	public List<CrmTumourPosition> findDicSamplTypelist(String type) {
		List<CrmTumourPosition> list = commonDAO.find("from CrmTumourPosition order by id asc");
		return list;

	}

	public Map<String, Object> selectDicTypeList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from CrmTumourPosition";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<DicType> list = new ArrayList<DicType>();
		if (total > 0) {

			key = key + " order by id ASC";

			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

}
