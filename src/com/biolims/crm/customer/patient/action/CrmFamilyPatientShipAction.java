package com.biolims.crm.customer.patient.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmFamilyPatientShip;
import com.biolims.crm.customer.patient.service.CrmFamilyPatientShipService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/crm/customer/patient/crmFamilyPatientShip")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmFamilyPatientShipAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9022";
	@Autowired
	private CrmFamilyPatientShipService crmFamilyPatientShipService;
	private CrmFamilyPatientShip crmFamilyPatientShip = new CrmFamilyPatientShip();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showCrmFamilyPatientShipList")
	public String showCrmFamilyPatientShipList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/patient/crmFamilyPatientShip.jsp");
	}

	@Action(value = "showCrmFamilyPatientShipListJson")
	public void showCrmFamilyPatientShipListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmFamilyPatientShipService.findCrmFamilyPatientShipList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmFamilyPatientShip> list = (List<CrmFamilyPatientShip>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "crmFamilyPatientShipSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmFamilyPatientShipList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/patient/crmFamilyPatientShipDialog.jsp");
	}

	@Action(value = "showDialogCrmFamilyPatientShipListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmFamilyPatientShipListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmFamilyPatientShipService.findCrmFamilyPatientShipList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmFamilyPatientShip> list = (List<CrmFamilyPatientShip>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editCrmFamilyPatientShip")
	public String editCrmFamilyPatientShip() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			crmFamilyPatientShip = crmFamilyPatientShipService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmFamilyPatientShip");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			crmFamilyPatientShip.setCreateUser(user);
//			crmFamilyPatientShip.setCreateDate(new Date());
//			crmFamilyPatientShip.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//			crmFamilyPatientShip.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/customer/patient/crmFamilyPatientShipEdit.jsp");
	}

	@Action(value = "copyCrmFamilyPatientShip")
	public String copyCrmFamilyPatientShip() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmFamilyPatientShip = crmFamilyPatientShipService.get(id);
		crmFamilyPatientShip.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/customer/patient/crmFamilyPatientShipEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = crmFamilyPatientShip.getId();
		if(id!=null&&id.equals("")){
			crmFamilyPatientShip.setId(null);
		}
		Map aMap = new HashMap();
		crmFamilyPatientShipService.save(crmFamilyPatientShip,aMap);
		return redirect("/crm/customer/patient/crmFamilyPatientShip/editCrmFamilyPatientShip.action?id=" + crmFamilyPatientShip.getId());

	}

	@Action(value = "viewCrmFamilyPatientShip")
	public String toViewCrmFamilyPatientShip() throws Exception {
		String id = getParameterFromRequest("id");
		crmFamilyPatientShip = crmFamilyPatientShipService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/customer/patient/crmFamilyPatientShipEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmFamilyPatientShipService getCrmFamilyPatientShipService() {
		return crmFamilyPatientShipService;
	}

	public void setCrmFamilyPatientShipService(CrmFamilyPatientShipService crmFamilyPatientShipService) {
		this.crmFamilyPatientShipService = crmFamilyPatientShipService;
	}

	public CrmFamilyPatientShip getCrmFamilyPatientShip() {
		return crmFamilyPatientShip;
	}

	public void setCrmFamilyPatientShip(CrmFamilyPatientShip crmFamilyPatientShip) {
		this.crmFamilyPatientShip = crmFamilyPatientShip;
	}


}
