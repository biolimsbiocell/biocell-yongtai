package com.biolims.crm.customer.patient.action;

/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：字典管理
 * 创建人：倪毅
 * 创建时间：2011-8
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.crm.customer.patient.service.CrmTumourPositionService;
import com.biolims.dic.model.DicType;
import com.biolims.dic.service.DicTypeService;
import com.biolims.util.SendData;

@Namespace("/tumour/position")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked" })
public class CrmTumourPositionAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private DicTypeService dicService;
	@Autowired
	private CrmTumourPositionService crmTumourPositionService;
	//	用于页面上显示模块名称
	private String title = "字典管理";

	//该action权限id
	private String rightsId = "230104";

	/**
	 * 访问列表
	 */
	@Action(value = "showTumourPositionList")
	public String showTumourPositionList() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "120", "true", "true", "", "", editflag, "", "id" });
		//		map.put("code", new String[] { "", "string", "", "检索码", "32", "true", "true", "", "", editflag, "", "code" });
		map.put("name",
				new String[] { "", "string", "", "名称", "100", "true", "false", "", "", editflag, "", "dicName" });
		map.put("superior", new String[] { "", "string", "", "上级", "100", "true", "false", "", "", editflag,
				"Ext.util.Format.comboRenderer(cob)", "cob" });
		//		map.put("note", new String[] { "", "string", "", "说明", "200", "true", "false", "", "", editflag, "", "note" });
		//		map.put("sysCode", new String[] { "", "string", "", "系统标记", "200", "true", "true", "", "", editflag, "",
		//				"sysCode" });
		//
		//		map.put("type-id", new String[] { "", "string", "", "类型", "75", "true", "false", "", "", editflag,
		//				"Ext.util.Format.comboRenderer(cob)", "cob" });
		//		map.put("orderNumber", new String[] { "", "string", "", "排序号", "75", "true", "false", "", "", editflag, "",
		//				"orderNumber" });
		//		map.put("state", new String[] { "", "string", "", "状态", "75", "true", "false", "", "", editflag,
		//				"Ext.util.Format.comboRenderer(stateCob)", "stateCob" });

		//声明一个lisener
		//String statementLisener = "";
		//生成ext用type字符串
		exttype = generalexttype(map);
		//生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/tumour/position/showTumourPositionListJson.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/crm/customer/patient/crmTumourPosition.jsp");

	}

	@Action(value = "showTumourPositionListJson")
	public void showTumourPositionListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		List<CrmTumourPosition> list = null;
		long totalCount = 0;

		Map<String, Object> controlMap = crmTumourPositionService.findDicTypeList(null, startNum, limitNum, dir, sort);
		totalCount = Long.parseLong(controlMap.get("total").toString());
		list = (List<CrmTumourPosition>) controlMap.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("superior", "");
		//		map.put("orderNumber", "");
		//		map.put("sysCode", "");
		//		map.put("type-id", "");
		//		map.put("state", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 添加
	 */
	@Action(value = "saveDicType")
	public void saveDicType() throws Exception {
		String jsonString = getParameterFromRequest("data");
		crmTumourPositionService.saveDicType(jsonString);
	}

	/**
	 * 删除
	 */
	@Action(value = "delDicType")
	public void delDicType() throws Exception {
		String id = getParameterFromRequest("id");
		crmTumourPositionService.delDicType(id);

	}

	//	@Action(value = "showDicMainTypeListJson")
	//	public void showDicMainTypeListJson() throws Exception {
	//		String jsonStr = dicService.findDicMainTypeList();
	//		new SendData().sendDataJson(jsonStr, ServletActionContext.getResponse());
	//
	//	}

	@Action(value = "showDicMainTypeListJson")
	public String showDicMainTypeListJson() throws Exception {
		String type = super.getRequest().getParameter("type");
		//		System.out.print(type);
		String outStr = "{results:" + crmTumourPositionService.getDicSampleTypeJson(type) + "}";

		return renderText(outStr);
	}

	@Action(value = "showDicMainTypeList")
	public String showDicMainTypeList() throws Exception {
		String type = super.getRequest().getParameter("type");
		//		System.out.print(type);
		String outStr = "{results:" + crmTumourPositionService.getDicSampleType(type) + "}";

		return renderText(outStr);
	}

	/**
	 * 获取DIC字典列表数据
	 * @return
	 * @throws Exception
	 */
	@Action(value = "dicTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String getDicTypeListShow() throws Exception {
		String flag = super.getRequest().getParameter("flag");

		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/dic/type/dicTypeSelectJson.action?flag=" + flag;
		putObjToContext("flag", flag);
		String showUrl = "/WEB-INF/page/dic/dicTypeSelect.jsp";
		return dispatcher(commonShow(jsonUrl, showUrl));
	}

	@Action(value = "dicTypeFullRecSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeFullRecSelect() throws Exception {
		String flag = super.getRequest().getParameter("flag");

		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/dic/type/dicTypeSelectJson.action?flag=" + flag;
		putObjToContext("flag", flag);
		String showUrl = "/WEB-INF/page/dic/dicTypeFullRecSelect.jsp";
		return dispatcher(commonShow(jsonUrl, showUrl));
	}

	/**
	 * 访问字典表页面通用程序
	 */
	public String commonShow(String jsonUrl, String showUrl) throws Exception {

		String type = "{name: 'id'},{name: 'name'},{name: 'note'},{name: 'sysCode'}";
		String col = "{header:'id',sortable: true,hidden:true,dataIndex: 'id',width: 100},"
				+ "{header:'名称',sortable: true,dataIndex: 'name',width: 200},"
				+ "{id:'note',header:'说明',sortable: true,dataIndex: 'note',width: 240},{id:'sysCode',header:'系统码',sortable: true,hidden:true,dataIndex: 'sysCode',width: 100}";

		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("path", jsonUrl);
		return (showUrl);
	}

	@Action(value = "dicTypeSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void dicTypeSelectJson() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		map.put("sysCode", "");
		commonShowJsonByQueryDicType(map);
	}

	public <T> void commonShowJsonByQueryDicType(Map<String, String> map) throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String type = (String) super.getRequest().getParameter("flag");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		if (type == null || type.trim().length() <= 0) {
			type = "";
		}

		Map<String, Object> controlMap = dicService.findCommonDic(type, startNum, limitNum, dir, sort, null);
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<T> list = (List<T>) controlMap.get("list");

		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	@Action(value = "dicTypeCobJson")
	public String dicTypeCobJson() throws Exception {
		String type = super.getRequest().getParameter("type");

		String outStr = "{results:" + dicService.getDicTypeJson(type) + "}";

		return renderText(outStr);
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public DicTypeService getDicService() {
		return dicService;
	}

	public void setDicService(DicTypeService dicService) {
		this.dicService = dicService;
	}

	public CrmTumourPositionService getCrmTumourPositionService() {
		return crmTumourPositionService;
	}

	public void setCrmTumourPositionService(CrmTumourPositionService crmTumourPositionService) {
		this.crmTumourPositionService = crmTumourPositionService;
	}

}
