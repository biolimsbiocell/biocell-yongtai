package com.biolims.crm.customer.patient.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.linkman.service.CrmLinkManService;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
import com.biolims.crm.customer.patient.model.CrmPatientTreat;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.weichat.model.WeiChatCancerType;

@Namespace("/crm/customer/pay")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmPayAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2014";
	@Autowired
	private CrmPatientService crmPatientService;
	
	
	
	@Action(value = "showPayList")
	public String showPayList() throws Exception {
		putObjToContext("crmPatient.state", getParameterFromRequest("state_sam"));
		return dispatcher("/WEB-INF/page/crm/customer/patient/showPayList.jsp");
	}

	@Action(value = "showPayListJson")
	public void showCustomerInfoListJson() throws Exception {
		String _startNum = getParameterFromRequest("start");
		String _limitNum = "10000";

		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		// 开始记录数
		Integer startNum = _startNum != null && _startNum.length() > 0 ? Integer.valueOf(_startNum) : null;
		// limit
		Integer limitNum = _limitNum != null && _limitNum.length() > 0 ? Integer.valueOf(_limitNum) : null;

		try {

			List<CrmConsumerMarket> list = new ArrayList<CrmConsumerMarket>();
			Long total = 0l;

			Map<String, String> mapForQuery = new HashMap<String, String>();
			String queryData = getRequest().getParameter("data");
			Map<String, Object> result = new HashMap<String, Object>();
			if (queryData != null && queryData.length() > 0) {
				mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
				result = crmPatientService.findConsumerMarketList(mapForQuery, startNum, limitNum, dir, sort);
			} 
			else {
				mapForQuery.put("state", "1");
				result = crmPatientService.findConsumerMarketList(mapForQuery, startNum, limitNum, dir, sort);
			}

			total = (Long) result.get("total");
			list = (List<CrmConsumerMarket>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("isFee", "isFee");
			map.put("fee", "");
			map.put("consumptionDate", "yyyy-MM-dd");
			map.put("feeWay", "");
			map.put("monthFee", "");
			map.put("note", "note");
			map.put("bankNum", "bankNum");
			map.put("receipNum", "");
			map.put("posNum", "");
			map.put("backFee", "");
			map.put("backFeeDate", "yyyy-MM-dd");
			map.put("invoiceCode", "");
			map.put("invoiceDate", "yyyy-MM-dd");
			map.put("invoiceNum", "");
			map.put("alipayNum", "");
			map.put("realFee", "realFee");
			map.put("state", "");
			map.put("financeUser-id", "");
			map.put("financeUser-name", "");
			map.put("sampleOrder-id", "");
			map.put("sampleOrder-name", "");
			map.put("sampleOrder-productName", "");
			map.put("sampleOrder-commissioner-name", "");
//			map.put("sampleOrder-inspectionDepartment-name", "");
//			map.put("sampleOrder-medicalInstitutions", "");
//			map.put("sampleOrder-attendingDoctor", "");
//			map.put("sampleOrder-MedicalNumber", "");
			map.put("sampleId", "");
			map.put("source", "");
			
			
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "savePayInfo")
	public void saveCustomerInfo() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			crmPatientService.saveCustomerInfo(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//收费订单弹出框
	@Action(value = "showChargeSampleList")
	public String showWeiChatCancerTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/patient/showPayListDialog.jsp");
	}
	@Action(value = "showChargeSampleListJson")
	public void showChargeSampleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmPatientService.selectChargeSampleList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmPatientService getCrmPatientService() {
		return crmPatientService;
	}

	public void setCrmPatientService(CrmPatientService crmPatientService) {
		this.crmPatientService = crmPatientService;
	}

	// 提交样本
		@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void submitSample() throws Exception {
			String[] ids = getRequest().getParameterValues("ids[]");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				String str=this.crmPatientService.submitSample(ids);
				result.put("success", true);
				result.put("str", str);
			} catch (Exception e) {
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
}
