package com.biolims.crm.customer.patient.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 放射科检查
 * @author lims-platform
 * @date 2014-07-30 17:11:23
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_PATIENT_RADIOLOGY")
@SuppressWarnings("serial")
public class CrmPatientRadiology extends EntityDao<CrmPatientRadiology> implements java.io.Serializable {
	/**放射科检查id*/
	private java.lang.String id;
	/**检查项目*/
	private java.lang.String examinationItem;
	/**检查日期*/
	private java.util.Date dateOfExamination;
	/**医院名称*/
	private java.lang.String hospital;
	/**检查编号*/
	private java.lang.String examination;
	/**住院号*/
	private java.lang.String hospitalPatient;
	/**重点结果描述*/
	private java.lang.String keyResultsDescription;
	/**病人id */
	private CrmPatient crmPatient;

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  放射科检查id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  放射科检查id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检查项目
	 */
	@Column(name = "EXAMINATION_ITEM", length = 100)
	public java.lang.String getExaminationItem() {
		return this.examinationItem;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检查项目
	 */
	public void setExaminationItem(java.lang.String examinationItem) {
		this.examinationItem = examinationItem;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  检查日期
	 */
	@Column(name = "DATE_OF_EXAMINATION", length = 100)
	public java.util.Date getDateOfExamination() {
		return this.dateOfExamination;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  检查日期
	 */
	public void setDateOfExamination(java.util.Date dateOfExamination) {
		this.dateOfExamination = dateOfExamination;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  医院名称
	 */
	@Column(name = "HOSPITAL", length = 100)
	public java.lang.String getHospital() {
		return this.hospital;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  医院名称
	 */
	public void setHospital(java.lang.String hospital) {
		this.hospital = hospital;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检查编号
	 */
	@Column(name = "EXAMINATION", length = 100)
	public java.lang.String getExamination() {
		return this.examination;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检查编号
	 */
	public void setExamination(java.lang.String examination) {
		this.examination = examination;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  住院号
	 */
	@Column(name = "HOSPITAL_PATIENT", length = 100)
	public java.lang.String getHospitalPatient() {
		return this.hospitalPatient;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  住院号
	 */
	public void setHospitalPatient(java.lang.String hospitalPatient) {
		this.hospitalPatient = hospitalPatient;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  重点结果描述
	 */
	@Column(name = "KEY_RESULTS_DESCRIPTION", length = 200)
	public java.lang.String getKeyResultsDescription() {
		return this.keyResultsDescription;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  重点结果描述
	 */
	public void setKeyResultsDescription(java.lang.String keyResultsDescription) {
		this.keyResultsDescription = keyResultsDescription;
	}

	/**
	 *方法: 取得CrmPatient
	 *@return: CrmPatient  病人id 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	/**
	 *方法: 设置CrmPatient
	 *@param: CrmPatient  病人id 
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}
}