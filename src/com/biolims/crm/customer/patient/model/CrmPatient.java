package com.biolims.crm.customer.patient.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCity;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.family.model.Family;
import com.biolims.system.product.model.Product;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;

/**
 * @Title: Model
 * @Description: 鐥呬汉涓绘暟鎹�
 * @author lims-platform
 * @date 2014-07-30 17:11:31
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_PATIENT")
@SuppressWarnings("serial")
public class CrmPatient extends EntityDao<CrmPatient> implements java.io.Serializable {
	/** 病人id */
	private java.lang.String id;
	private String selectId;// 选择病人
	private java.lang.String lastName;
	/** 姓 */
	private java.lang.String firstName;

	private java.lang.String name;
	/** 性别 */
	private java.lang.String gender;
	/** 出生年月 */
	private java.util.Date dateOfBirth;
	/** 年龄 */
	private java.lang.String age;
	/** 籍贯 */
	private java.lang.String placeOfBirth;
	/** 民族 */
	private java.lang.String race;
	/** 成人/儿童 */
	private java.lang.String adultTeenager;
	/** 婚姻 */
	private java.lang.String maritalStatus;
	/** 职业 */
	private java.lang.String occupation;
	/** 同意接收 */
	private java.lang.String consentReceived;
	/** 病人状态 */
	private DicType patientStatus;
	/** 家庭住址 */
	private java.lang.String address;
	/** 鑱旂郴鐢佃瘽1 */
	private java.lang.String telphoneNumber1;
	/** 鑱旂郴鐢佃瘽2 */
	private java.lang.String telphoneNumber2;
	/** 鑱旂郴鐢佃瘽3 */
	private java.lang.String telphoneNumber3;
	/** 鍏朵粬鑱旂郴浜� */
	private java.lang.String additionalRecipient1;
	/** 鍏朵粬鑱旂郴浜� */
	private java.lang.String additionalRecipient2;
	/** 鍏朵粬鑱旂郴浜� */
	private java.lang.String additionalRecipient3;
	/** 鑱旂郴浜虹數璇� */
	private java.lang.String additionalTelphoneNumber1;
	/** 鑱旂郴浜虹數璇� */
	private java.lang.String additionalTelphoneNumber2;
	/** 鑱旂郴浜虹數璇� */
	private java.lang.String additionalTelphoneNumber3;
	/** 鑲跨槫鎵�湪鍣ㄥ畼 */
	private DicType tumorLocation;
	/** 鑲跨槫浣嶇疆 */
	private DicType site;
	/** 鐥呯悊璇婃柇 */
	private String pathologicalDiagnosis;
	/** 绛夌骇 */
	private String grade;
	/** 鏄惁鍒嗗寲 */
	private java.lang.String differentiation;
	/** 鍒嗗寲绋嬪害 */
	private DicType degreeOfDifferentiation;
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getApplicationNum() {
		return applicationNum;
	}

	public void setApplicationNum(String applicationNum) {
		this.applicationNum = applicationNum;
	}

	public String getRound() {
		return round;
	}

	public void setRound(String round) {
		this.round = round;
	}

	public String getHospitalPatientID() {
		return hospitalPatientID;
	}

	public void setHospitalPatientID(String hospitalPatientID) {
		this.hospitalPatientID = hospitalPatientID;
	}

	/** 鏄惁鍋氳繃娲绘 */
	private java.lang.String biopsy;
	/** 鏃堕棿 */
	private java.util.Date dateOfBiopsy;
	/** 缁撴灉 */
	private java.lang.String result;

	private java.lang.String primaryVsSecondary;
	/** 鏍锋湰绫诲瀷 */
	private DicType sampleType;

	private User createUser;
	/** 瀹㈡埛id */
	private CrmCustomer crmCustomerId;
	/** 鍒涘缓鏃ユ湡 */
	private java.util.Date createDate;

	private String jc;
	/** 身份证 */
	private String sfz;
	/** 自身癌症史 */
	private java.lang.String priorCancerHistory;
	/** 家族编号 */
	private String familyCode;
	/** 家族编号 */
	private Family familyId;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancer;
	/** 传染病 */
	private java.lang.String infectiousDiseases;
	/** 吸烟史 */
	private java.lang.String historyOfSmoking;
	/** 环境风险/毒物 */
	private java.lang.String environmentalExposures;
	/** 其他疾病史 */
	private java.lang.String otherDiseases;
	/** 自身癌症史 */
	private java.lang.String priorCancerHistorytext;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancergx;
	/** 家族癌症史 */
	private java.lang.String familyHistoryOfCancertext;
	/** 传染病 */
	private java.lang.String infectiousDiseasestext;
	/** 吸烟史 */
	private java.lang.String historyOfSmokingtext;
	/** 环境风险/毒物 */
	private java.lang.String environmentalExposurestext;
	/** 其他疾病史 */
	private java.lang.String otherDiseasestext;

	/** 随机码 */
	private java.lang.String randomCode;
	/** 饮酒史 */
	private java.lang.String ysj;
	/** 饮酒史 */
	private java.lang.String ysjtext;
	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鐥呬汉id
	 */
	/** 检测项目ID */
	private java.lang.String sequencingId;
	/** 检测项目 */
	private java.lang.String sequencingName;
	/** 检测类型 */
	private java.lang.String sequencingType;
	/** 待打印报告 */
	private java.lang.String waitReport;
	/** 已打印报告 */
	private java.lang.String report;
	/** 癌症种类 */
	private WeiChatCancerType cancerType;
	/** 子类一 */
	private WeiChatCancerTypeSeedOne cancerTypeSeedOne;
	/** 子类二 */
	private WeiChatCancerTypeSeedTwo cancerTypeSeedTwo;

	private String weichatId;
	// 客户姓名
	private String doctorName;
	// 客户部门
	private String doctorDept;
	// 医保证号
	private String yibao;
	/** 自定义字段值 */
	private String fieldContent;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/** 状态 */
	private String state;
	/**
	 * 以下为nci 新增字段
	 */
	/** dbGaP submitted subject ID */
	private String dbGaPID;
	/** dbGaP accession */
	private String dbGaPAccession;
	/** Diagnosis */
	private String diagnosis;
	/** Gene Expression Subgroup */
	private String geneExpressionSubgroup;
	/** Genetic Subtype */
	private String geneticSubtype;
	/** Treatment__ */
	private String treatment;
	/** Biopsy Type */
	private String biopsyType;
	/** Ann Arbor Stage */
	private String annArborStage;
	/** LDH Ratio */
	private String LDHRatio;
	/** ECOG Performance Status */
	private String ECOGPerformanceStatus;
	/** Number of Extranodal Sites */
	private String numberofExtranodalSites;
	/** IPI Group */
	private String IPIGroup;
	/** IPI Range */
	private String IPIRange;
	/** Status at Follow_up_ 0 Alive_ 1 Dead */
	private String statusatFollowup;
	/** Follow_up Time _yrs */
	private String followupTime;
	/** Progression_Free Survival _PFS_ Status_ 0 No Progressoin_ 1 Progression */
	private String PFSStatus;
	/** Progression_Free Survival _PFS_ Time _yrs */
	private String PFSTime;
	/** Included in Survival Analysis */
	private String survivalAnalysis;
	// 关联样本号
	private String sampleCode;
	/** 筛选号 */
	private String filtrateCode;
	/** CCOI */
	private String ccoi;
	/** 感染筛查 */
	private String infectionScreening;
	/** 肿瘤类别 */
	private DicType tumourType;
	
	
	/** 姓名缩写 */
	private String abbreviation;
	/**申请单编号*/
	private String applicationNum;
	/** 采血轮次 */
	private String round;	
	// 门诊号/住院号
	private String hospitalPatientID;
     //临床用药方案
	private String medicationPlan;
	/* 新增字段 */
	/** 乙肝HBV */
	@Column(name = "hepatitis_hbv", length = 50)
	private String hepatitisHbv;
	/** 丙肝HCV */
	@Column(name = "hepatitis_hcv", length = 50)
	private String hepatitisHcv;
	/** 艾滋HIV */
	@Column(name = "Hiv_virus", length = 50)
	private String HivVirus;
	/** 梅毒TPHA */
	@Column(name = "syphilis", length = 50)
	private String syphilis;
	// 肿瘤分期
	@Column(name = "tumorStaging", length = 50)
	private DicType tumorStaging;
	/** 肿瘤分期备注 */
	private String zhongliuNote;
	/** 肿瘤分期备注 */
	private String tumorStagingNote;
	
	/** 检测项目 */
	private Product crmProduct;
	/** 检测项目 */
	private String productId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public Product getCrmProduct() {
		return crmProduct;
	}

	public void setCrmProduct(Product crmProduct) {
		this.crmProduct = crmProduct;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/** 检测项目名称 */
	private String productName;
	
	public String getHepatitisHbv() {
		return hepatitisHbv;
	}

	public void setHepatitisHbv(String hepatitisHbv) {
		this.hepatitisHbv = hepatitisHbv;
	}

	public String getHepatitisHcv() {
		return hepatitisHcv;
	}

	public void setHepatitisHcv(String hepatitisHcv) {
		this.hepatitisHcv = hepatitisHcv;
	}

	public String getHivVirus() {
		return HivVirus;
	}

	public void setHivVirus(String hivVirus) {
		HivVirus = hivVirus;
	}

	public String getSyphilis() {
		return syphilis;
	}

	public void setSyphilis(String syphilis) {
		this.syphilis = syphilis;
	}

	public Double getWhiteBloodCellNum() {
		return whiteBloodCellNum;
	}

	public void setWhiteBloodCellNum(Double whiteBloodCellNum) {
		this.whiteBloodCellNum = whiteBloodCellNum;
	}

	public String getLymphoidCellSeries() {
		return lymphoidCellSeries;
	}

	public void setLymphoidCellSeries(String lymphoidCellSeries) {
		this.lymphoidCellSeries = lymphoidCellSeries;
	}

	public Double getPercentageOfLymphocytes() {
		return percentageOfLymphocytes;
	}

	public void setPercentageOfLymphocytes(Double percentageOfLymphocytes) {
		this.percentageOfLymphocytes = percentageOfLymphocytes;
	}

	public String getCounterDrawBlood() {
		return counterDrawBlood;
	}

	public void setCounterDrawBlood(String counterDrawBlood) {
		this.counterDrawBlood = counterDrawBlood;
	}

	public String getHeparinTube() {
		return heparinTube;
	}

	public void setHeparinTube(String heparinTube) {
		this.heparinTube = heparinTube;
	}

	/** 白细胞计数 */
	private Double whiteBloodCellNum;
	/** 淋巴细胞绝对值 */
	@Column(name = "lymphoid_cell_series", length = 50)
	private String lymphoidCellSeries;
	/** 淋巴细胞百分比 */
	private Double percentageOfLymphocytes;
	/** 采血计量 */
	@Column(name = "counter_draw_blood", length = 50)
	private String counterDrawBlood;
	/** 采血管 */
	@Column(name = "heparin_tube", length = 50)
	private String heparinTube;
	//科室电话
	private String crmPhone;
	
	//家庭
	/** 家属联系人 */
	private String family;
	/** 家属联系人电话 */
	private String familyPhone;
	/** 家属联系人地址 */
	private String familySite;
	/** 邮编 */
	private String zipCode;

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getFamilyPhone() {
		return familyPhone;
	}

	public void setFamilyPhone(String familyPhone) {
		this.familyPhone = familyPhone;
	}

	public String getFamilySite() {
		return familySite;
	}

	public void setFamilySite(String familySite) {
		this.familySite = familySite;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	public DicType getTumourType() {
		return tumourType;
	}

	public void setTumourType(DicType tumourType) {
		this.tumourType = tumourType;
	}

	public String getInfectionScreening() {
		return infectionScreening;
	}

	public void setInfectionScreening(String infectionScreening) {
		this.infectionScreening = infectionScreening;
	}

	public String getCcoi() {
		return ccoi;
	}

	public void setCcoi(String ccoi) {
		this.ccoi = ccoi;
	}

	public String getFiltrateCode() {
		return filtrateCode;
	}

	public void setFiltrateCode(String filtrateCode) {
		this.filtrateCode = filtrateCode;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getFieldContent() {
		return fieldContent;
	}

	public void setFieldContent(String fieldContent) {
		this.fieldContent = fieldContent;
	}

	public String getYibao() {
		return yibao;
	}

	public void setYibao(String yibao) {
		this.yibao = yibao;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorDept() {
		return doctorDept;
	}

	public void setDoctorDept(String doctorDept) {
		this.doctorDept = doctorDept;
	}

	@Column(length = 200)
	private String isEnd;// 是否完结

	public String getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(String isEnd) {
		this.isEnd = isEnd;
	}

	@Column(name = "SEQUENCING_ID", length = 100)
	public java.lang.String getSequencingId() {
		return sequencingId;
	}

	public void setSequencingId(java.lang.String sequencingId) {
		this.sequencingId = sequencingId;
	}

	@Column(name = "SEQUENCING_NAME", length = 100)
	public java.lang.String getSequencingName() {
		return sequencingName;
	}

	public void setSequencingName(java.lang.String sequencingName) {
		this.sequencingName = sequencingName;
	}

	@Column(name = "SEQUENCING_TYPE", length = 100)
	public java.lang.String getSequencingType() {
		return sequencingType;
	}

	public void setSequencingType(java.lang.String sequencingType) {
		this.sequencingType = sequencingType;
	}

	@Column(name = "WAIT_REPORT", length = 100)
	public java.lang.String getWaitReport() {
		return waitReport;
	}

	public void setWaitReport(java.lang.String waitReport) {
		this.waitReport = waitReport;
	}

	@Column(name = "REPORT", length = 100)
	public java.lang.String getReport() {
		return report;
	}

	public void setReport(java.lang.String report) {
		this.report = report;
	}

	private String checkedBoxTest;

	private CrmCity selA;
	private CrmCity selB;
	private CrmCity selC;
	/** email */
	private java.lang.String email;
	// 死亡日期
	private java.util.Date deathDate;

	private CrmCustomer customer;// 客户单位

	private CrmDoctor customerDoctor;// 客户

	private DicType ks;// 城市

	@Column(name = "SELECT_NAME", length = 100)
	public String getSelectId() {
		return selectId;
	}

	public void setSelectId(String selectId) {
		this.selectId = selectId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELA")
	public CrmCity getSelA() {
		return selA;
	}

	public void setSelA(CrmCity selA) {
		this.selA = selA;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELB")
	public CrmCity getSelB() {
		return selB;
	}

	public void setSelB(CrmCity selB) {
		this.selB = selB;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELC")
	public CrmCity getSelC() {
		return selC;
	}

	public void setSelC(CrmCity selC) {
		this.selC = selC;
	}

	@Column(name = "CHECKEDBOXTEST", length = 100)
	public String getCheckedBoxTest() {
		return checkedBoxTest;
	}

	public void setCheckedBoxTest(String checkedBoxTest) {
		this.checkedBoxTest = checkedBoxTest;
	}

	@Id
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鐥呬汉id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 濮�
	 */
	@Column(name = "LAST_NAME", length = 20)
	public java.lang.String getLastName() {
		return this.lastName;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             濮�
	 */
	public void setLastName(java.lang.String lastName) {
		this.lastName = lastName;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍚�
	 */
	@Column(name = "FIRST_NAME", length = 20)
	public java.lang.String getFirstName() {
		return this.firstName;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鍚�
	 */
	public void setFirstName(java.lang.String firstName) {
		this.firstName = firstName;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 濮撳悕
	 */
	@Column(name = "NAME", length = 100)
	public java.lang.String getName() {
		return this.name;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             濮撳悕
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鎬у埆
	 */
	@Column(name = "GENDER", length = 100)
	public java.lang.String getGender() {
		return this.gender;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鎬у埆
	 */
	public void setGender(java.lang.String gender) {
		this.gender = gender;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鍑虹敓骞存湀
	 */
	@Column(name = "DATE_OF_BIRTH", length = 100)
	public java.util.Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date
	 *             鍑虹敓骞存湀
	 */
	public void setDateOfBirth(java.util.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 骞撮緞
	 */
	@Column(name = "AGE", length = 100)
	public java.lang.String getAge() {
		return this.age;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             骞撮緞
	 */
	public void setAge(java.lang.String age) {
		this.age = age;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 绫嶈疮
	 */
	@Column(name = "PLACE_OF_BIRTH", length = 200)
	public java.lang.String getPlaceOfBirth() {
		return this.placeOfBirth;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             绫嶈疮
	 */
	public void setPlaceOfBirth(java.lang.String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 姘戞棌
	 */
	@Column(name = "RACE", length = 100)
	public java.lang.String getRace() {
		return this.race;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             姘戞棌
	 */
	public void setRace(java.lang.String race) {
		this.race = race;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鎴愪汉/鍎跨
	 */
	@Column(name = "ADULT_TEENAGER", length = 100)
	public java.lang.String getAdultTeenager() {
		return this.adultTeenager;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鎴愪汉/鍎跨
	 */
	public void setAdultTeenager(java.lang.String adultTeenager) {
		this.adultTeenager = adultTeenager;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 濠氬Щ
	 */
	@Column(name = "MARITAL_STATUS", length = 100)
	public java.lang.String getMaritalStatus() {
		return this.maritalStatus;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             濠氬Щ
	 */
	public void setMaritalStatus(java.lang.String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱屼笟
	 */
	@Column(name = "OCCUPATION", length = 100)
	public java.lang.String getOccupation() {
		return this.occupation;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱屼笟
	 */
	public void setOccupation(java.lang.String occupation) {
		this.occupation = occupation;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍚屾剰鎺ユ敹
	 */
	@Column(name = "CONSENT_RECEIVED", length = 100)
	public java.lang.String getConsentReceived() {
		return this.consentReceived;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鍚屾剰鎺ユ敹
	 */
	public void setConsentReceived(java.lang.String consentReceived) {
		this.consentReceived = consentReceived;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鐥呬汉鐘舵�
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PATIENT_STATUS")
	public DicType getPatientStatus() {
		return this.patientStatus;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             鐥呬汉鐘舵�
	 */
	public void setPatientStatus(DicType patientStatus) {
		this.patientStatus = patientStatus;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 瀹跺涵浣忓潃
	 */
	@Column(name = "ADDRESS", length = 200)
	public java.lang.String getAddress() {
		return this.address;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             瀹跺涵浣忓潃
	 */
	public void setAddress(java.lang.String address) {
		this.address = address;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴鐢佃瘽1
	 */
	@Column(name = "TELPHONE_NUMBER1", length = 100)
	public java.lang.String getTelphoneNumber1() {
		return this.telphoneNumber1;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱旂郴鐢佃瘽1
	 */
	public void setTelphoneNumber1(java.lang.String telphoneNumber1) {
		this.telphoneNumber1 = telphoneNumber1;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴鐢佃瘽2
	 */
	@Column(name = "TELPHONE_NUMBER2", length = 100)
	public java.lang.String getTelphoneNumber2() {
		return this.telphoneNumber2;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱旂郴鐢佃瘽2
	 */
	public void setTelphoneNumber2(java.lang.String telphoneNumber2) {
		this.telphoneNumber2 = telphoneNumber2;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴鐢佃瘽3
	 */
	@Column(name = "TELPHONE_NUMBER3", length = 100)
	public java.lang.String getTelphoneNumber3() {
		return this.telphoneNumber3;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱旂郴鐢佃瘽3
	 */
	public void setTelphoneNumber3(java.lang.String telphoneNumber3) {
		this.telphoneNumber3 = telphoneNumber3;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	@Column(name = "ADDITIONAL_RECIPIENT1", length = 100)
	public java.lang.String getAdditionalRecipient1() {
		return this.additionalRecipient1;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鍏朵粬鑱旂郴浜�
	 */
	public void setAdditionalRecipient1(java.lang.String additionalRecipient1) {
		this.additionalRecipient1 = additionalRecipient1;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	@Column(name = "ADDITIONAL_RECIPIENT2", length = 100)
	public java.lang.String getAdditionalRecipient2() {
		return this.additionalRecipient2;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鍏朵粬鑱旂郴浜�
	 */
	public void setAdditionalRecipient2(java.lang.String additionalRecipient2) {
		this.additionalRecipient2 = additionalRecipient2;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍏朵粬鑱旂郴浜�
	 */
	@Column(name = "ADDITIONAL_RECIPIENT3", length = 100)
	public java.lang.String getAdditionalRecipient3() {
		return this.additionalRecipient3;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鍏朵粬鑱旂郴浜�
	 */
	public void setAdditionalRecipient3(java.lang.String additionalRecipient3) {
		this.additionalRecipient3 = additionalRecipient3;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴浜虹數璇�
	 */
	@Column(name = "ADDITIONAL_TELPHONE_NUMBER1", length = 100)
	public java.lang.String getAdditionalTelphoneNumber1() {
		return this.additionalTelphoneNumber1;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱旂郴浜虹數璇�
	 */
	public void setAdditionalTelphoneNumber1(java.lang.String additionalTelphoneNumber1) {
		this.additionalTelphoneNumber1 = additionalTelphoneNumber1;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴浜虹數璇�
	 */
	@Column(name = "ADDITIONAL_TELPHONE_NUMBER2", length = 100)
	public java.lang.String getAdditionalTelphoneNumber2() {
		return this.additionalTelphoneNumber2;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱旂郴浜虹數璇�
	 */
	public void setAdditionalTelphoneNumber2(java.lang.String additionalTelphoneNumber2) {
		this.additionalTelphoneNumber2 = additionalTelphoneNumber2;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鑱旂郴浜虹數璇�
	 */
	@Column(name = "ADDITIONAL_TELPHONE_NUMBER3", length = 100)
	public java.lang.String getAdditionalTelphoneNumber3() {
		return this.additionalTelphoneNumber3;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鑱旂郴浜虹數璇�
	 */
	public void setAdditionalTelphoneNumber3(java.lang.String additionalTelphoneNumber3) {
		this.additionalTelphoneNumber3 = additionalTelphoneNumber3;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鑲跨槫鎵�湪鍣ㄥ畼
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TUMOR_LOCATION")
	public DicType getTumorLocation() {
		return this.tumorLocation;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             鑲跨槫鎵�湪鍣ㄥ畼
	 */
	public void setTumorLocation(DicType tumorLocation) {
		this.tumorLocation = tumorLocation;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鑲跨槫浣嶇疆
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SITE")
	public DicType getSite() {
		return this.site;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             鑲跨槫浣嶇疆
	 */
	public void setSite(DicType site) {
		this.site = site;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鐥呯悊璇婃柇
	 */
	@Column(name = "PATHOLOGICAL_DIAGNOSIS", length = 100)
	public String getPathologicalDiagnosis() {
		return this.pathologicalDiagnosis;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             鐥呯悊璇婃柇
	 */
	public void setPathologicalDiagnosis(String pathologicalDiagnosis) {
		this.pathologicalDiagnosis = pathologicalDiagnosis;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 绛夌骇
	 */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	@Column(name = "GRADE")
	public String getGrade() {
		return this.grade;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             绛夌骇
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鏄惁鍒嗗寲
	 */
	@Column(name = "DIFFERENTIATION", length = 100)
	public java.lang.String getDifferentiation() {
		return this.differentiation;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鏄惁鍒嗗寲
	 */
	public void setDifferentiation(java.lang.String differentiation) {
		this.differentiation = differentiation;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鍒嗗寲绋嬪害
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEGREE_OF_DIFFERENTIATION")
	public DicType getDegreeOfDifferentiation() {
		return this.degreeOfDifferentiation;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             鍒嗗寲绋嬪害
	 */
	public void setDegreeOfDifferentiation(DicType degreeOfDifferentiation) {
		this.degreeOfDifferentiation = degreeOfDifferentiation;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鏄惁鍋氳繃娲绘
	 */
	@Column(name = "BIOPSY", length = 100)
	public java.lang.String getBiopsy() {
		return this.biopsy;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鏄惁鍋氳繃娲绘
	 */
	public void setBiopsy(java.lang.String biopsy) {
		this.biopsy = biopsy;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鏃堕棿
	 */
	@Column(name = "DATE_OF_BIOPSY", length = 100)
	public java.util.Date getDateOfBiopsy() {
		return this.dateOfBiopsy;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date
	 *             鏃堕棿
	 */
	public void setDateOfBiopsy(java.util.Date dateOfBiopsy) {
		this.dateOfBiopsy = dateOfBiopsy;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 缁撴灉
	 */
	@Column(name = "RESULT", length = 100)
	public java.lang.String getResult() {
		return this.result;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             缁撴灉
	 */
	public void setResult(java.lang.String result) {
		this.result = result;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.lang.String
	 * 
	 * @return: java.lang.String 鍘熷彂鎴栬浆绉�
	 */
	@Column(name = "PRIMARY_VS_SECONDARY", length = 100)
	public java.lang.String getPrimaryVsSecondary() {
		return this.primaryVsSecondary;
	}

	/**
	 * 鏂规硶: 璁剧疆java.lang.String
	 * 
	 * @param: java.lang.String
	 *             鍘熷彂鎴栬浆绉�
	 */
	public void setPrimaryVsSecondary(java.lang.String primaryVsSecondary) {
		this.primaryVsSecondary = primaryVsSecondary;
	}

	/**
	 * 鏂规硶: 鍙栧緱DicType
	 * 
	 * @return: DicType 鏍锋湰绫诲瀷
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return this.sampleType;
	}

	/**
	 * 鏂规硶: 璁剧疆DicType
	 * 
	 * @param: DicType
	 *             鏍锋湰绫诲瀷
	 */
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 鏂规硶: 鍙栧緱User
	 * 
	 * @return: User 鍒涘缓浜�
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 鏂规硶: 璁剧疆User
	 * 
	 * @param: User
	 *             鍒涘缓浜�
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 鏂规硶: 鍙栧緱CrmCustomer
	 * 
	 * @return: CrmCustomer 瀹㈡埛id
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER_ID")
	public CrmCustomer getCrmCustomerId() {
		return this.crmCustomerId;
	}

	/**
	 * 鏂规硶: 璁剧疆CrmCustomer
	 * 
	 * @param: CrmCustomer
	 *             瀹㈡埛id
	 */
	public void setCrmCustomerId(CrmCustomer crmCustomerId) {
		this.crmCustomerId = crmCustomerId;
	}

	/**
	 * 鏂规硶: 鍙栧緱java.util.Date
	 * 
	 * @return: java.util.Date 鍒涘缓鏃ユ湡
	 */
	@Column(name = "CREATE_DATE", length = 60)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 鏂规硶: 璁剧疆java.util.Date
	 * 
	 * @param: java.util.Date
	 *             鍒涘缓鏃ユ湡
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	public String getSfz() {
		return sfz;
	}

	public void setSfz(String sfz) {
		this.sfz = sfz;
	}

	public String getJc() {
		return jc;
	}

	public void setJc(String jc) {
		this.jc = jc;
	}

	public java.lang.String getPriorCancerHistory() {
		return priorCancerHistory;
	}

	public void setPriorCancerHistory(java.lang.String priorCancerHistory) {
		this.priorCancerHistory = priorCancerHistory;
	}

	public java.lang.String getFamilyHistoryOfCancer() {
		return familyHistoryOfCancer;
	}

	public void setFamilyHistoryOfCancer(java.lang.String familyHistoryOfCancer) {
		this.familyHistoryOfCancer = familyHistoryOfCancer;
	}

	public java.lang.String getInfectiousDiseases() {
		return infectiousDiseases;
	}

	public void setInfectiousDiseases(java.lang.String infectiousDiseases) {
		this.infectiousDiseases = infectiousDiseases;
	}

	public java.lang.String getHistoryOfSmoking() {
		return historyOfSmoking;
	}

	public void setHistoryOfSmoking(java.lang.String historyOfSmoking) {
		this.historyOfSmoking = historyOfSmoking;
	}

	public java.lang.String getEnvironmentalExposures() {
		return environmentalExposures;
	}

	public void setEnvironmentalExposures(java.lang.String environmentalExposures) {
		this.environmentalExposures = environmentalExposures;
	}

	public java.lang.String getOtherDiseases() {
		return otherDiseases;
	}

	public void setOtherDiseases(java.lang.String otherDiseases) {
		this.otherDiseases = otherDiseases;
	}

	public java.lang.String getPriorCancerHistorytext() {
		return priorCancerHistorytext;
	}

	public void setPriorCancerHistorytext(java.lang.String priorCancerHistorytext) {
		this.priorCancerHistorytext = priorCancerHistorytext;
	}

	public java.lang.String getFamilyHistoryOfCancergx() {
		return familyHistoryOfCancergx;
	}

	public void setFamilyHistoryOfCancergx(java.lang.String familyHistoryOfCancergx) {
		this.familyHistoryOfCancergx = familyHistoryOfCancergx;
	}

	public java.lang.String getFamilyHistoryOfCancertext() {
		return familyHistoryOfCancertext;
	}

	public void setFamilyHistoryOfCancertext(java.lang.String familyHistoryOfCancertext) {
		this.familyHistoryOfCancertext = familyHistoryOfCancertext;
	}

	public java.lang.String getInfectiousDiseasestext() {
		return infectiousDiseasestext;
	}

	public void setInfectiousDiseasestext(java.lang.String infectiousDiseasestext) {
		this.infectiousDiseasestext = infectiousDiseasestext;
	}

	public java.lang.String getHistoryOfSmokingtext() {
		return historyOfSmokingtext;
	}

	public void setHistoryOfSmokingtext(java.lang.String historyOfSmokingtext) {
		this.historyOfSmokingtext = historyOfSmokingtext;
	}

	public java.lang.String getEnvironmentalExposurestext() {
		return environmentalExposurestext;
	}

	public void setEnvironmentalExposurestext(java.lang.String environmentalExposurestext) {
		this.environmentalExposurestext = environmentalExposurestext;
	}

	public java.lang.String getOtherDiseasestext() {
		return otherDiseasestext;
	}

	public void setOtherDiseasestext(java.lang.String otherDiseasestext) {
		this.otherDiseasestext = otherDiseasestext;
	}

	@Column(name = "EMAIL", length = 50)
	public java.lang.String getEmail() {
		return email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.util.Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(java.util.Date deathDate) {
		this.deathDate = deathDate;
	}

	public java.lang.String getRandomCode() {
		return randomCode;
	}

	public void setRandomCode(java.lang.String randomCode) {
		this.randomCode = randomCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KS")
	public DicType getKs() {
		return ks;
	}

	public void setKs(DicType ks) {
		this.ks = ks;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "CUSTOMER")
	public CrmCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(CrmCustomer customer) {
		this.customer = customer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CUSTOMER_DOCTOR")
	public CrmDoctor getCustomerDoctor() {
		return customerDoctor;
	}

	public void setCustomerDoctor(CrmDoctor customerDoctor) {
		this.customerDoctor = customerDoctor;
	}

	@Column(name = "YSJ", length = 50)
	public java.lang.String getYsj() {
		return ysj;
	}

	public void setYsj(java.lang.String ysj) {
		this.ysj = ysj;
	}

	@Column(name = "YSJTEXT", length = 50)
	public java.lang.String getYsjtext() {
		return ysjtext;
	}

	public void setYsjtext(java.lang.String ysjtext) {
		this.ysjtext = ysjtext;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 癌症种类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE")
	public WeiChatCancerType getCancerType() {
		return cancerType;
	}

	public void setCancerType(WeiChatCancerType cancerType) {
		this.cancerType = cancerType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子类一
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_ONE")
	public WeiChatCancerTypeSeedOne getCancerTypeSeedOne() {
		return cancerTypeSeedOne;
	}

	public void setCancerTypeSeedOne(WeiChatCancerTypeSeedOne cancerTypeSeedOne) {
		this.cancerTypeSeedOne = cancerTypeSeedOne;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 子类二
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEICHAT_CANCER_TYPE_SEED_TWO")
	public WeiChatCancerTypeSeedTwo getCancerTypeSeedTwo() {
		return cancerTypeSeedTwo;
	}

	public void setCancerTypeSeedTwo(WeiChatCancerTypeSeedTwo cancerTypeSeedTwo) {
		this.cancerTypeSeedTwo = cancerTypeSeedTwo;
	}

	@Column(name = "WEI_CHAT_ID")
	public String getWeichatId() {
		return weichatId;
	}

	public void setWeichatId(String weichatId) {
		this.weichatId = weichatId;
	}

	/**
	 * @return the dbGaPID
	 */
	public String getDbGaPID() {
		return dbGaPID;
	}

	/**
	 * @param dbGaPID
	 *            the dbGaPID to set
	 */
	public void setDbGaPID(String dbGaPID) {
		this.dbGaPID = dbGaPID;
	}

	/**
	 * @return the dbGaPAccession
	 */
	public String getDbGaPAccession() {
		return dbGaPAccession;
	}

	/**
	 * @param dbGaPAccession
	 *            the dbGaPAccession to set
	 */
	public void setDbGaPAccession(String dbGaPAccession) {
		this.dbGaPAccession = dbGaPAccession;
	}

	/**
	 * @return the diagnosis
	 */
	public String getDiagnosis() {
		return diagnosis;
	}

	/**
	 * @param diagnosis
	 *            the diagnosis to set
	 */
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	/**
	 * @return the geneExpressionSubgroup
	 */
	public String getGeneExpressionSubgroup() {
		return geneExpressionSubgroup;
	}

	/**
	 * @param geneExpressionSubgroup
	 *            the geneExpressionSubgroup to set
	 */
	public void setGeneExpressionSubgroup(String geneExpressionSubgroup) {
		this.geneExpressionSubgroup = geneExpressionSubgroup;
	}

	/**
	 * @return the geneticSubtype
	 */
	public String getGeneticSubtype() {
		return geneticSubtype;
	}

	/**
	 * @param geneticSubtype
	 *            the geneticSubtype to set
	 */
	public void setGeneticSubtype(String geneticSubtype) {
		this.geneticSubtype = geneticSubtype;
	}

	/**
	 * @return the treatment
	 */
	public String getTreatment() {
		return treatment;
	}

	/**
	 * @param treatment
	 *            the treatment to set
	 */
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	/**
	 * @return the biopsyType
	 */
	public String getBiopsyType() {
		return biopsyType;
	}

	/**
	 * @param biopsyType
	 *            the biopsyType to set
	 */
	public void setBiopsyType(String biopsyType) {
		this.biopsyType = biopsyType;
	}

	/**
	 * @return the annArborStage
	 */
	public String getAnnArborStage() {
		return annArborStage;
	}

	/**
	 * @param annArborStage
	 *            the annArborStage to set
	 */
	public void setAnnArborStage(String annArborStage) {
		this.annArborStage = annArborStage;
	}

	/**
	 * @return the lDHRatio
	 */
	public String getLDHRatio() {
		return LDHRatio;
	}

	/**
	 * @param lDHRatio
	 *            the lDHRatio to set
	 */
	public void setLDHRatio(String lDHRatio) {
		LDHRatio = lDHRatio;
	}

	/**
	 * @return the eCOGPerformanceStatus
	 */
	public String getECOGPerformanceStatus() {
		return ECOGPerformanceStatus;
	}

	/**
	 * @param eCOGPerformanceStatus
	 *            the eCOGPerformanceStatus to set
	 */
	public void setECOGPerformanceStatus(String eCOGPerformanceStatus) {
		ECOGPerformanceStatus = eCOGPerformanceStatus;
	}

	/**
	 * @return the numberofExtranodalSites
	 */
	public String getNumberofExtranodalSites() {
		return numberofExtranodalSites;
	}

	/**
	 * @param numberofExtranodalSites
	 *            the numberofExtranodalSites to set
	 */
	public void setNumberofExtranodalSites(String numberofExtranodalSites) {
		this.numberofExtranodalSites = numberofExtranodalSites;
	}

	/**
	 * @return the iPIGroup
	 */
	public String getIPIGroup() {
		return IPIGroup;
	}

	/**
	 * @param iPIGroup
	 *            the iPIGroup to set
	 */
	public void setIPIGroup(String iPIGroup) {
		IPIGroup = iPIGroup;
	}

	/**
	 * @return the iPIRange
	 */
	public String getIPIRange() {
		return IPIRange;
	}

	/**
	 * @param iPIRange
	 *            the iPIRange to set
	 */
	public void setIPIRange(String iPIRange) {
		IPIRange = iPIRange;
	}

	/**
	 * @return the statusatFollowup
	 */
	public String getStatusatFollowup() {
		return statusatFollowup;
	}

	/**
	 * @param statusatFollowup
	 *            the statusatFollowup to set
	 */
	public void setStatusatFollowup(String statusatFollowup) {
		this.statusatFollowup = statusatFollowup;
	}

	/**
	 * @return the followupTime
	 */
	public String getFollowupTime() {
		return followupTime;
	}

	/**
	 * @param followupTime
	 *            the followupTime to set
	 */
	public void setFollowupTime(String followupTime) {
		this.followupTime = followupTime;
	}

	/**
	 * @return the pFSStatus
	 */
	public String getPFSStatus() {
		return PFSStatus;
	}

	/**
	 * @param pFSStatus
	 *            the pFSStatus to set
	 */
	public void setPFSStatus(String pFSStatus) {
		PFSStatus = pFSStatus;
	}

	/**
	 * @return the pFSTime
	 */
	public String getPFSTime() {
		return PFSTime;
	}

	/**
	 * @param pFSTime
	 *            the pFSTime to set
	 */
	public void setPFSTime(String pFSTime) {
		PFSTime = pFSTime;
	}

	/**
	 * @return the survivalAnalysis
	 */
	public String getSurvivalAnalysis() {
		return survivalAnalysis;
	}

	/**
	 * @param survivalAnalysis
	 *            the survivalAnalysis to set
	 */
	public void setSurvivalAnalysis(String survivalAnalysis) {
		this.survivalAnalysis = survivalAnalysis;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the familyCode
	 */
	public String getFamilyCode() {
		return familyCode;
	}

	/**
	 * @param familyCode
	 *            the familyCode to set
	 */
	public void setFamilyCode(String familyCode) {
		this.familyCode = familyCode;
	}

	/**
	 * @return the familyId
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public Family getFamilyId() {
		return familyId;
	}

	/**
	 * @param familyId
	 *            the familyId to set
	 */
	public void setFamilyId(Family familyId) {
		this.familyId = familyId;
	}

	public String getMedicationPlan() {
		return medicationPlan;
	}

	public void setMedicationPlan(String medicationPlan) {
		this.medicationPlan = medicationPlan;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public DicType getTumorStaging() {
		return tumorStaging;
	}

	public void setTumorStaging(DicType tumorStaging) {
		this.tumorStaging = tumorStaging;
	}

	public String getZhongliuNote() {
		return zhongliuNote;
	}

	public void setZhongliuNote(String zhongliuNote) {
		this.zhongliuNote = zhongliuNote;
	}

	public String getTumorStagingNote() {
		return tumorStagingNote;
	}

	public void setTumorStagingNote(String tumorStagingNote) {
		this.tumorStagingNote = tumorStagingNote;
	}

	public String getCrmPhone() {
		return crmPhone;
	}

	public void setCrmPhone(String crmPhone) {
		this.crmPhone = crmPhone;
	}

}