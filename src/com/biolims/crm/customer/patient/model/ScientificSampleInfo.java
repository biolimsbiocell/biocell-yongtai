package com.biolims.crm.customer.patient.model;

import java.util.Date;

import com.biolims.common.model.user.User;
import com.biolims.crm.project.model.Project;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleChromosome;
import com.biolims.sample.model.SampleFolicAcid;
import com.biolims.sample.model.SampleGene;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleTumor;
import com.biolims.sample.model.SampleVisit;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 
 * @author lims-platform
 * @date 2015-11-03 16:21:36
 * @version V1.0
 * 
 */
public class ScientificSampleInfo implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 样本名称 */
	private String name;
	/** 病人姓名 */
	private String patientName;
	/** 项目编号 */
	private String productId;
	/** 项目名称 */
	private String productName;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 备注 */
	private String note;
	/** 订单实体 */
	private SampleOrder sampleOrder;
	/** 订单号 */
	private String orderNum;
	/** 病历号 */
	private String patientId;

	/** 分期 */
	private String sampleStage;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private Project project;
	/** 样本关系 */
	private CrmFamilyPatientShip personShip;

	private SampleReceive sampleReceive;
	// 样本数量
	private String sampleNum;
	// 样本类型
	private String sampleType2;
	/** 采样日期 */
	private Date samplingDate;
	/** 采样部位 */
	private DicType dicType;
	// 单位
	private String unit;
	// 样本说明
	private String sampleNote;
	/** 采样部位 */
	private String dicTypeName;
	

	/** 外部样本号 */
	private String idCard;
	/** 手机号 */
	private String phone;
	/** 样本类型 */
	private DicSampleType sampleType;
	/** 检测方法 */
	private WorkType businessType;
	/** 检测方法 */
	private String sequenceFun;
	/** 送检医院 */
	private String hospital;
	/** 送检医院 */
	private String sampleUnit;
	/** 送检医生 */
	private String doctor;
	// 推荐人
	private String referUser;
	/** 价格 */
	private Double price;
	// 金额
	private Double fee;
	/** 价格 */
	private Double projectFee;
	/** 付款方式 */
	private DicType type;
	/** 付款方式 */
	private DicType feeWay;
	// /**项目*/
	// private Product product;

	/** 附件数量 */
	private String fileNum;

	// 取样时间
	private String sampleTime;
	// 送检日期
	private String inspectDate;
	// 接收日期
	private Date receiveDate;
	// 应出报告日期
	private String reportDate;
	// 姓名拼音
	private String spellName;
	// 性别
	private String gender;
	// 年龄
	private String age;
	// 籍贯
	private String birthAddress;
	// 民族
	private String nation;
	// 身高
	private Double hight;
	// 体重
	private Double weight;
	// 婚姻情况
	private String marriage;
	// 证件类型
	private String cardType;
	// 证件号码
	private String cardNumber;
	// 联系方式
	private String tele;
	// 邮箱
	private String email;
	// 家庭住址
	private String familyAddress;
	// 是否收费
	private String isFee;
	// 优惠类型
	private String privilege;
	// 发票号
	private String billNumber;
	// 发票号
	private String invoiceCode;
	// 发票抬头
	private String billTitle;
	// 发票抬头
	private String invoiceNum;
	// 录入人
	private User createUser;
	// 录入人1
	private String createUser1;
	// 录入人2
	private String createUser2;
	// 录入人
	private String inputUser;
	// 审核人1
	private String confirmUser1;
	// 审核人2
	private String confirmUser2;
	// 业务员
	private User salesman;
	// 上传图片
	private FileInfo upLoadAccessory;
	// 地区
	private String area;
	// 病历号
	private String medicalCard;
	// 任务单
	private WorkOrder orderId;
	// 图片上传时间
	private Date upImgTime;
	// 标示查询SampleInfo
	private String next;
	// 产前
	private SampleInput si;
	// 染色体
	private SampleChromosome sc;

	// 叶酸
	private SampleFolicAcid sfa;
	// 基因
	private SampleGene sg;
	// 肿瘤
	private SampleTumor st;
	// 临检所
	private SampleVisit sv;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 关联主样本
	private String sampleRelevanceMain;
	
	/** 科研 */
	private String serviceId;
	/** 科研名称 */
	private String serviceName;
	/** 科研项目 */
	private String projectId;
	/** 科研项目名称 */
	private String projectName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public WorkType getBusinessType() {
		return businessType;
	}
	public void setBusinessType(WorkType businessType) {
		this.businessType = businessType;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public DicType getType() {
		return type;
	}
	public void setType(DicType type) {
		this.type = type;
	}
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}
	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	public String getSampleStage() {
		return sampleStage;
	}
	public void setSampleStage(String sampleStage) {
		this.sampleStage = sampleStage;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public CrmFamilyPatientShip getPersonShip() {
		return personShip;
	}
	public void setPersonShip(CrmFamilyPatientShip personShip) {
		this.personShip = personShip;
	}
	public SampleReceive getSampleReceive() {
		return sampleReceive;
	}
	public void setSampleReceive(SampleReceive sampleReceive) {
		this.sampleReceive = sampleReceive;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getSampleType2() {
		return sampleType2;
	}
	public void setSampleType2(String sampleType2) {
		this.sampleType2 = sampleType2;
	}
	public Date getSamplingDate() {
		return samplingDate;
	}
	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}
	public DicType getDicType() {
		return dicType;
	}
	public void setDicType(DicType dicType) {
		this.dicType = dicType;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSampleNote() {
		return sampleNote;
	}
	public void setSampleNote(String sampleNote) {
		this.sampleNote = sampleNote;
	}
	public String getDicTypeName() {
		return dicTypeName;
	}
	public void setDicTypeName(String dicTypeName) {
		this.dicTypeName = dicTypeName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public DicSampleType getSampleType() {
		return sampleType;
	}
	public void setSampleType(DicSampleType sampleType) {
		this.sampleType = sampleType;
	}
	public String getSequenceFun() {
		return sequenceFun;
	}
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getSampleUnit() {
		return sampleUnit;
	}
	public void setSampleUnit(String sampleUnit) {
		this.sampleUnit = sampleUnit;
	}
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}
	public String getReferUser() {
		return referUser;
	}
	public void setReferUser(String referUser) {
		this.referUser = referUser;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public Double getProjectFee() {
		return projectFee;
	}
	public void setProjectFee(Double projectFee) {
		this.projectFee = projectFee;
	}
	public DicType getFeeWay() {
		return feeWay;
	}
	public void setFeeWay(DicType feeWay) {
		this.feeWay = feeWay;
	}
	public String getFileNum() {
		return fileNum;
	}
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
	public String getSampleTime() {
		return sampleTime;
	}
	public void setSampleTime(String sampleTime) {
		this.sampleTime = sampleTime;
	}
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	public Date getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getSpellName() {
		return spellName;
	}
	public void setSpellName(String spellName) {
		this.spellName = spellName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBirthAddress() {
		return birthAddress;
	}
	public void setBirthAddress(String birthAddress) {
		this.birthAddress = birthAddress;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public Double getHight() {
		return hight;
	}
	public void setHight(Double hight) {
		this.hight = hight;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getMarriage() {
		return marriage;
	}
	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getTele() {
		return tele;
	}
	public void setTele(String tele) {
		this.tele = tele;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFamilyAddress() {
		return familyAddress;
	}
	public void setFamilyAddress(String familyAddress) {
		this.familyAddress = familyAddress;
	}
	public String getIsFee() {
		return isFee;
	}
	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}
	public String getPrivilege() {
		return privilege;
	}
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	public String getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	public String getBillTitle() {
		return billTitle;
	}
	public void setBillTitle(String billTitle) {
		this.billTitle = billTitle;
	}
	public String getInvoiceNum() {
		return invoiceNum;
	}
	public void setInvoiceNum(String invoiceNum) {
		this.invoiceNum = invoiceNum;
	}
	public User getCreateUser() {
		return createUser;
	}
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	public String getCreateUser1() {
		return createUser1;
	}
	public void setCreateUser1(String createUser1) {
		this.createUser1 = createUser1;
	}
	public String getCreateUser2() {
		return createUser2;
	}
	public void setCreateUser2(String createUser2) {
		this.createUser2 = createUser2;
	}
	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}
	public String getConfirmUser1() {
		return confirmUser1;
	}
	public void setConfirmUser1(String confirmUser1) {
		this.confirmUser1 = confirmUser1;
	}
	public String getConfirmUser2() {
		return confirmUser2;
	}
	public void setConfirmUser2(String confirmUser2) {
		this.confirmUser2 = confirmUser2;
	}
	public User getSalesman() {
		return salesman;
	}
	public void setSalesman(User salesman) {
		this.salesman = salesman;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getMedicalCard() {
		return medicalCard;
	}
	public void setMedicalCard(String medicalCard) {
		this.medicalCard = medicalCard;
	}
	public WorkOrder getOrderId() {
		return orderId;
	}
	public void setOrderId(WorkOrder orderId) {
		this.orderId = orderId;
	}
	public Date getUpImgTime() {
		return upImgTime;
	}
	public void setUpImgTime(Date upImgTime) {
		this.upImgTime = upImgTime;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public SampleInput getSi() {
		return si;
	}
	public void setSi(SampleInput si) {
		this.si = si;
	}
	public SampleChromosome getSc() {
		return sc;
	}
	public void setSc(SampleChromosome sc) {
		this.sc = sc;
	}
	
	public SampleFolicAcid getSfa() {
		return sfa;
	}
	public void setSfa(SampleFolicAcid sfa) {
		this.sfa = sfa;
	}
	public SampleGene getSg() {
		return sg;
	}
	public void setSg(SampleGene sg) {
		this.sg = sg;
	}
	public SampleTumor getSt() {
		return st;
	}
	public void setSt(SampleTumor st) {
		this.st = st;
	}
	public SampleVisit getSv() {
		return sv;
	}
	public void setSv(SampleVisit sv) {
		this.sv = sv;
	}
	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
	public String getSampleRelevanceMain() {
		return sampleRelevanceMain;
	}
	public void setSampleRelevanceMain(String sampleRelevanceMain) {
		this.sampleRelevanceMain = sampleRelevanceMain;
	}
	
}