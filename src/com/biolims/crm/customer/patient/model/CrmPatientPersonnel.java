package com.biolims.crm.customer.patient.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
/**   
 * @Title: Model
 * @Description: 电子病历家庭病史表
 * @author lims-platform
 * @date 2016-03-11 17:35:34
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_PATIENT_PERSONNEL")
@SuppressWarnings("serial")
public class CrmPatientPersonnel extends EntityDao<CrmPatientPersonnel> implements java.io.Serializable {
	/**id*/
	private String id;
	/**检出年龄*/
	private String checkOutTheAge;
	/**相关主表*/
	private CrmPatient crmPatient; 
	/**肿瘤类别*/
	private DicType tumorCategory;
	/**家属与患者关系*/
	private CrmFamilyPatientShip crmFamilyPatientShip;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  检出年龄
	 */
	@Column(name ="CHECK_OUT_THE_AGE", length = 50)
	public String getCheckOutTheAge(){
		return this.checkOutTheAge;
	}
	/**
	 *方法: 设置String
	 *@param: String  检出年龄
	 */
	public void setCheckOutTheAge(String checkOutTheAge){
		this.checkOutTheAge = checkOutTheAge;
	}
	/**
	 *方法: 取得CrmPatient
	 *@return: CrmPatient  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return crmPatient;
	}
	
	/**
	 *方法: 设置CrmPatient
	 *@param: CrmPatient  相关主表
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  肿瘤类别
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TUMOR_CATEGORY")
	public DicType getTumorCategory(){
		return this.tumorCategory;
	}
	
	/**
	 *方法: 设置DicType
	 *@param: DicType  肿瘤类别
	 */
	public void setTumorCategory(DicType tumorCategory){
		this.tumorCategory = tumorCategory;
	}
	/**
	 *方法: 取得String
	 *@return: String  家属与患者关系
	 */
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_FAMILY_PATIENT_SHIP_TYPE")
	public CrmFamilyPatientShip getCrmFamilyPatientShip() {
		return crmFamilyPatientShip;
	}
	/**
	 *方法: 设置String
	 *@param: String  家属与患者关系
	 */
	public void setCrmFamilyPatientShip(CrmFamilyPatientShip crmFamilyPatientShip) {
		this.crmFamilyPatientShip = crmFamilyPatientShip;
	}
	
}