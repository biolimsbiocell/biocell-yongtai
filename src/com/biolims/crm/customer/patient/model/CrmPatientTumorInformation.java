package com.biolims.crm.customer.patient.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

@Entity
@Table(name = "Crm_Tumor_Information")
@SuppressWarnings("serial")
public class CrmPatientTumorInformation extends EntityDao<CrmPatientTumorInformation> implements java.io.Serializable {
	/**肿瘤id*/
	private java.lang.String id;

	/**样本类型*/
	private DicType sampleType;
	/**病人id */
	private CrmPatient crmPatient;
	/**样本编号*/
	private java.lang.String sampleCode;
	/**病理 */
	private CrmPatientPathology patientPathology;

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  肿瘤id
	 */

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PATIENT_PATHOLOGY")
	public CrmPatientPathology getPatientPathology() {
		return this.patientPathology;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  肿瘤id
	 */
	public void setPatientPathology(CrmPatientPathology patientPathology) {
		this.patientPathology = patientPathology;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  肿瘤id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  病理诊断
	 */

	/**
	 *方法: 取得DicType
	 *@return: DicType  等级
	 */
	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@NotFound(action = NotFoundAction.IGNORE)

	/**
	 *方法: 取得DicType
	 *@return: DicType  样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return this.sampleType;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  样本类型
	 */
	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 *方法: 取得CrmPatient
	 *@return: CrmPatient  病人id 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	/**
	 *方法: 设置CrmPatient
	 *@param: CrmPatient  病人id 
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public java.lang.String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(java.lang.String sampleCode) {
		this.sampleCode = sampleCode;
	}

}
