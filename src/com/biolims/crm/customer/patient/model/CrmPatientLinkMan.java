package com.biolims.crm.customer.patient.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * 
 * @author
 * @Description:联系人
 *
 */
@Entity
@Table(name = "CRM_PATIENT_LINK_MAN")
@SuppressWarnings("serial")
public class CrmPatientLinkMan extends EntityDao<CrmPatientLinkMan> implements java.io.Serializable {

	private java.lang.String id;

	private java.lang.String linkMan_name;

	private java.lang.String linkMan_mail;

	private java.lang.String linkMan_telNumber;

	private java.lang.String linkMan_post;

	private CrmPatient crmPatient;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	@Column(name = "LinkMan_name", length = 36)
	public java.lang.String getLinkMan_name() {
		return linkMan_name;
	}

	public void setLinkMan_name(java.lang.String linkMan_name) {
		this.linkMan_name = linkMan_name;
	}

	@Column(name = "LinkMan_mail", length = 36)
	public java.lang.String getLinkMan_mail() {
		return linkMan_mail;
	}

	public void setLinkMan_mail(java.lang.String linkMan_mail) {
		this.linkMan_mail = linkMan_mail;
	}

	@Column(name = "LinkMan_telNumber", length = 36)
	public java.lang.String getLinkMan_telNumber() {
		return linkMan_telNumber;
	}

	public void setLinkMan_telNumber(java.lang.String linkMan_telNumber) {
		this.linkMan_telNumber = linkMan_telNumber;
	}

	@Column(name = "LinkMan_Post", length = 50)
	public java.lang.String getLinkMan_post() {
		return linkMan_post;
	}

	public void setLinkMan_post(java.lang.String linkMan_post) {
		this.linkMan_post = linkMan_post;
	}

}
