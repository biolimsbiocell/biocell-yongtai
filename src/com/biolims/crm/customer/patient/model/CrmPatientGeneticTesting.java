package com.biolims.crm.customer.patient.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 基因检测
 * @author lims-platform
 * @date 2014-07-30 17:11:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_PATIENT_GENETIC_TESTING")
@SuppressWarnings("serial")
public class CrmPatientGeneticTesting extends EntityDao<CrmPatientGeneticTesting> implements java.io.Serializable {
	/**基因检测id*/
	private java.lang.String id;
	/**检测方法  */
	private java.lang.String method;
	/**检测日期*/
	private java.util.Date dateOfTest;
	/**医院/公司*/
	private java.lang.String hospitalOrCompany;
	/**测试编号*/
	private java.lang.String test;
	/**生物标志物*/
	private java.lang.String biomarker;
	/**检测结果*/
	private java.lang.String result;
	/**是否治疗*/
	private java.lang.String treatment;
	/**病人id */
	private CrmPatient crmPatient;
	/**位点*/
	private java.lang.String locus;
	/**受检基因*/
	private java.lang.String subjectGene;
	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  基因检测id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  基因检测id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检测方法  
	 */
	@Column(name = "METHOD", length = 100)
	public java.lang.String getMethod() {
		return this.method;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检测方法  
	 */
	public void setMethod(java.lang.String method) {
		this.method = method;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  检测日期
	 */
	@Column(name = "DATE_OF_TEST", length = 100)
	public java.util.Date getDateOfTest() {
		return this.dateOfTest;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  检测日期
	 */
	public void setDateOfTest(java.util.Date dateOfTest) {
		this.dateOfTest = dateOfTest;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  医院/公司
	 */
	@Column(name = "HOSPITAL_OR_COMPANY", length = 100)
	public java.lang.String getHospitalOrCompany() {
		return this.hospitalOrCompany;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  医院/公司
	 */
	public void setHospitalOrCompany(java.lang.String hospitalOrCompany) {
		this.hospitalOrCompany = hospitalOrCompany;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  测试编号
	 */
	@Column(name = "TEST", length = 100)
	public java.lang.String getTest() {
		return this.test;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  测试编号
	 */
	public void setTest(java.lang.String test) {
		this.test = test;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  生物标志物
	 */
	@Column(name = "BIOMARKER", length = 100)
	public java.lang.String getBiomarker() {
		return this.biomarker;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  生物标志物
	 */
	public void setBiomarker(java.lang.String biomarker) {
		this.biomarker = biomarker;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检测结果
	 */
	@Column(name = "RESULT", length = 100)
	public java.lang.String getResult() {
		return this.result;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检测结果
	 */
	public void setResult(java.lang.String result) {
		this.result = result;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否治疗
	 */
	@Column(name = "TREATMENT", length = 100)
	public java.lang.String getTreatment() {
		return this.treatment;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否治疗
	 */
	public void setTreatment(java.lang.String treatment) {
		this.treatment = treatment;
	}

	/**
	 *方法: 取得CrmPatient
	 *@return: CrmPatient  病人id 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	/**
	 *方法: 设置CrmPatient
	 *@param: CrmPatient  病人id 
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	@Column(name = "LOCUS", length = 100)
	public java.lang.String getLocus() {
		return locus;
	}

	public void setLocus(java.lang.String locus) {
		this.locus = locus;
	}
	/**受检基因*/
	@Column(name = "SUBJECTGENE", length = 100)
	public java.lang.String getSubjectGene() {
		return subjectGene;
	}

	public void setSubjectGene(java.lang.String subjectGene) {
		this.subjectGene = subjectGene;
	}
	

}