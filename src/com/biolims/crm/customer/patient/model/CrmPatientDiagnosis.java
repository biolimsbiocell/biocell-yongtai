package com.biolims.crm.customer.patient.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 璇婃柇鏄庣粏
 * @author lims-platform
 * @date 2014-07-30 17:11:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_PATIENT_DIAGNOSIS")
@SuppressWarnings("serial")
public class CrmPatientDiagnosis extends EntityDao<CrmPatientDiagnosis> implements java.io.Serializable {
	/**璇婃柇id*/
	private java.lang.String id;
	/**璇婃柇*/
	private java.lang.String diagnosis;
	/**璇婃柇鏃ユ湡*/
	private java.util.Date dateOfDiagnosis;
	/**鍖婚櫌鍚嶇О*/
	private java.lang.String hospitalName;
	/**涓绘不鍖荤敓*/
	private java.lang.String primaryPhysician;
	/**鎮ｈ�骞撮緞*/
	private java.lang.String patientAge;
	/**璇婃柇缁撴灉*/
	private java.lang.String diagnosisResult;
	/**鐤剧梾绛夌骇*/
	private java.lang.String stageOfDisease;

	private java.lang.String recurrencesOrMetastases;
	/**鏃堕棿*/
	private java.util.Date diagnosisDate;
	/**鑲跨槫閮ㄤ綅*/
	private java.lang.String locationOfTumor;
	/**鏄惁娌荤枟*/
	private java.lang.String treatment;
	/**鏀剧枟*/
	private java.lang.String radiotherapy;
	/**鍖栫枟*/
	private java.lang.String chemotherapy;
	/**闈跺悜娌荤枟*/
	private java.lang.String targetedTherapy;
	/**闈跺悜娌荤枟*/
	private java.lang.String tnm;
	/**鐥呬汉id */
	private CrmPatient crmPatient;
	/**鐥呬汉id */
	private java.lang.String num;
	/**鐥呬汉id */
	private DicType DiagnosisType;

	private java.lang.String result;

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  璇婃柇id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  璇婃柇id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  璇婃柇
	 */
	@Column(name = "DIAGNOSIS", length = 100)
	public java.lang.String getDiagnosis() {
		return this.diagnosis;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  璇婃柇
	 */
	public void setDiagnosis(java.lang.String diagnosis) {
		this.diagnosis = diagnosis;
	}

	/**
	 *鏂规硶: 鍙栧緱java.util.Date
	 *@return: java.util.Date  璇婃柇鏃ユ湡
	 */
	@Column(name = "DATE_OF_DIAGNOSIS", length = 100)
	public java.util.Date getDateOfDiagnosis() {
		return this.dateOfDiagnosis;
	}

	/**
	 *鏂规硶: 璁剧疆java.util.Date
	 *@param: java.util.Date  璇婃柇鏃ユ湡
	 */
	public void setDateOfDiagnosis(java.util.Date dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鍖婚櫌鍚嶇О
	 */
	@Column(name = "HOSPITAL_NAME", length = 100)
	public java.lang.String getHospitalName() {
		return this.hospitalName;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鍖婚櫌鍚嶇О
	 */
	public void setHospitalName(java.lang.String hospitalName) {
		this.hospitalName = hospitalName;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  涓绘不鍖荤敓
	 */
	@Column(name = "PRIMARY_PHYSICIAN", length = 100)
	public java.lang.String getPrimaryPhysician() {
		return this.primaryPhysician;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  涓绘不鍖荤敓
	 */
	public void setPrimaryPhysician(java.lang.String primaryPhysician) {
		this.primaryPhysician = primaryPhysician;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鎮ｈ�骞撮緞
	 */
	@Column(name = "PATIENT_AGE", length = 100)
	public java.lang.String getPatientAge() {
		return this.patientAge;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鎮ｈ�骞撮緞
	 */
	public void setPatientAge(java.lang.String patientAge) {
		this.patientAge = patientAge;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  璇婃柇缁撴灉
	 */
	@Column(name = "DIAGNOSIS_RESULT", length = 100)
	public java.lang.String getDiagnosisResult() {
		return this.diagnosisResult;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  璇婃柇缁撴灉
	 */
	public void setDiagnosisResult(java.lang.String diagnosisResult) {
		this.diagnosisResult = diagnosisResult;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鐤剧梾绛夌骇
	 */
	@Column(name = "STAGE_OF_DISEASE", length = 100)
	public java.lang.String getStageOfDisease() {
		return this.stageOfDisease;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鐤剧梾绛夌骇
	 */
	public void setStageOfDisease(java.lang.String stageOfDisease) {
		this.stageOfDisease = stageOfDisease;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  澶嶅彂鎴栬浆绉�
	 */
	@Column(name = "RECURRENCES_OR_METASTASES", length = 100)
	public java.lang.String getRecurrencesOrMetastases() {
		return this.recurrencesOrMetastases;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  澶嶅彂鎴栬浆绉�
	 */
	public void setRecurrencesOrMetastases(java.lang.String recurrencesOrMetastases) {
		this.recurrencesOrMetastases = recurrencesOrMetastases;
	}

	/**
	 *鏂规硶: 鍙栧緱java.util.Date
	 *@return: java.util.Date  鏃堕棿
	 */
	@Column(name = "DIAGNOSIS_DATE", length = 100)
	public java.util.Date getDiagnosisDate() {
		return this.diagnosisDate;
	}

	/**
	 *鏂规硶: 璁剧疆java.util.Date
	 *@param: java.util.Date  鏃堕棿
	 */
	public void setDiagnosisDate(java.util.Date diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鑲跨槫閮ㄤ綅
	 */
	@Column(name = "LOCATION_OF_TUMOR", length = 100)
	public java.lang.String getLocationOfTumor() {
		return this.locationOfTumor;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鑲跨槫閮ㄤ綅
	 */
	public void setLocationOfTumor(java.lang.String locationOfTumor) {
		this.locationOfTumor = locationOfTumor;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鏄惁娌荤枟
	 */
	@Column(name = "TREATMENT", length = 100)
	public java.lang.String getTreatment() {
		return this.treatment;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鏄惁娌荤枟
	 */
	public void setTreatment(java.lang.String treatment) {
		this.treatment = treatment;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鏀剧枟
	 */
	@Column(name = "RADIOTHERAPY", length = 100)
	public java.lang.String getRadiotherapy() {
		return this.radiotherapy;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鏀剧枟
	 */
	public void setRadiotherapy(java.lang.String radiotherapy) {
		this.radiotherapy = radiotherapy;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  鍖栫枟
	 */
	@Column(name = "CHEMOTHERAPY", length = 100)
	public java.lang.String getChemotherapy() {
		return this.chemotherapy;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  鍖栫枟
	 */
	public void setChemotherapy(java.lang.String chemotherapy) {
		this.chemotherapy = chemotherapy;
	}

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  闈跺悜娌荤枟
	 */
	@Column(name = "TARGETED_THERAPY", length = 100)
	public java.lang.String getTargetedTherapy() {
		return this.targetedTherapy;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  闈跺悜娌荤枟
	 */
	public void setTargetedTherapy(java.lang.String targetedTherapy) {
		this.targetedTherapy = targetedTherapy;
	}

	/**
	 *鏂规硶: 鍙栧緱CrmPatient
	 *@return: CrmPatient  鐥呬汉id 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	/**
	 *鏂规硶: 璁剧疆CrmPatient
	 *@param: CrmPatient  鐥呬汉id 
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	@Column(name = "TNM", length = 100)
	public java.lang.String getTnm() {
		return tnm;
	}

	public void setTnm(java.lang.String tnm) {
		this.tnm = tnm;
	}

	public java.lang.String getNum() {
		return num;
	}

	public void setNum(java.lang.String num) {
		this.num = num;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIAGNOSIS_TYPE")
	public DicType getDiagnosisType() {
		return DiagnosisType;
	}

	public void setDiagnosisType(DicType diagnosisType) {
		DiagnosisType = diagnosisType;
	}

	@Column(name = "RESULT", length = 100)
	public java.lang.String getResult() {
		return result;
	}

	public void setResult(java.lang.String result) {
		this.result = result;
	}

}