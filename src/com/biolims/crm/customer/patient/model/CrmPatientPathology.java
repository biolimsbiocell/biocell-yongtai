package com.biolims.crm.customer.patient.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 病理检查
 * @author lims-platform
 * @date 2014-07-30 17:11:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_PATIENT_PATHOLOGY")
@SuppressWarnings("serial")
public class CrmPatientPathology extends EntityDao<CrmPatientPathology> implements java.io.Serializable {
	/**病理检查id*/
	private java.lang.String id;
	/**肿瘤所在器官*/
	private CrmTumourPosition tumorLocation;
	/**肿瘤位置*/
	private CrmTumourPosition site;
	/**病理诊断*/
	private java.lang.String pathologicalDiagnosis;
	/**等级*/
	private java.lang.String grade;
	/**是否分化*/
	private java.lang.String differentiation;
	/**分化程度*/
	private DicType degreeOfDifferentiation;
	/**是否做过活检*/
	private java.lang.String biopsy;
	/**时间*/
	private java.util.Date dateOfBiopsy;
	/**结果*/
	private java.lang.String result;
	/**原发或转移*/
	private java.lang.String primaryVsSecondary;
	/**检查项目*/
	private java.lang.String examinationItem;
	/**检查日期*/
	private java.util.Date dateOfExamination;
	/**医院名称*/
	private java.lang.String hospital;
	/**检查编号*/
	private java.lang.String examination;
	/**住院号*/
	private java.lang.String hospitalPatient;
	/**重点结果描述*/
	private java.lang.String keyResultsDescription;
	/**病人id */
	private CrmPatient crmPatient;
	
	
	
	/**肿瘤所在器官*/
	private java.lang.String tumorLocationString;
	/**肿瘤位置*/
	private java.lang.String siteString;
	
	

	public java.lang.String getTumorLocationString() {
		return tumorLocationString;
	}

	public void setTumorLocationString(java.lang.String tumorLocationString) {
		this.tumorLocationString = tumorLocationString;
	}

	public java.lang.String getSiteString() {
		return siteString;
	}

	public void setSiteString(java.lang.String siteString) {
		this.siteString = siteString;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  病理检查id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  病理检查id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  肿瘤所在器官
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TUMOR_LOCATION")
	public CrmTumourPosition getTumorLocation() {
		return this.tumorLocation;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  肿瘤位置
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SITE")
	public CrmTumourPosition getSite() {
		return this.site;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  肿瘤位置
	 */
	public void setSite(CrmTumourPosition site) {
		this.site = site;
	}

	@Column(name = "PATHOLOGICAL_DIAGNOSIS", length = 100)
	public java.lang.String getPathologicalDiagnosis() {
		return pathologicalDiagnosis;
	}

	public void setPathologicalDiagnosis(java.lang.String pathologicalDiagnosis) {
		this.pathologicalDiagnosis = pathologicalDiagnosis;
	}

	@Column(name = "GRADE")
	public String getGrade() {
		return this.grade;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否分化
	 */
	@Column(name = "DIFFERENTIATION", length = 100)
	public java.lang.String getDifferentiation() {
		return this.differentiation;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  分化程度
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEGREE_OF_DIFFERENTIATION")
	public DicType getDegreeOfDifferentiation() {
		return this.degreeOfDifferentiation;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否做过活检
	 */
	public void setBiopsy(java.lang.String biopsy) {
		this.biopsy = biopsy;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  时间
	 */
	public void setDateOfBiopsy(java.util.Date dateOfBiopsy) {
		this.dateOfBiopsy = dateOfBiopsy;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  结果
	 */
	@Column(name = "RESULT", length = 100)
	public java.lang.String getResult() {
		return this.result;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  结果
	 */
	public void setResult(java.lang.String result) {
		this.result = result;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  原发或转移
	 */
	@Column(name = "PRIMARY_VS_SECONDARY", length = 100)
	public java.lang.String getPrimaryVsSecondary() {
		return this.primaryVsSecondary;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否做过活检
	 */
	@Column(name = "BIOPSY", length = 100)
	public java.lang.String getBiopsy() {
		return this.biopsy;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  原发或转移
	 */
	public void setPrimaryVsSecondary(java.lang.String primaryVsSecondary) {
		this.primaryVsSecondary = primaryVsSecondary;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  时间
	 */
	@Column(name = "DATE_OF_BIOPSY", length = 100)
	public java.util.Date getDateOfBiopsy() {
		return this.dateOfBiopsy;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  分化程度
	 */
	public void setDegreeOfDifferentiation(DicType degreeOfDifferentiation) {
		this.degreeOfDifferentiation = degreeOfDifferentiation;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否分化
	 */
	public void setDifferentiation(java.lang.String differentiation) {
		this.differentiation = differentiation;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  等级
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  肿瘤所在器官
	 */
	public void setTumorLocation(CrmTumourPosition tumorLocation) {
		this.tumorLocation = tumorLocation;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检查项目
	 */
	@Column(name = "EXAMINATION_ITEM", length = 100)
	public java.lang.String getExaminationItem() {
		return this.examinationItem;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检查项目
	 */
	public void setExaminationItem(java.lang.String examinationItem) {
		this.examinationItem = examinationItem;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  检查日期
	 */
	@Column(name = "DATE_OF_EXAMINATION", length = 100)
	public java.util.Date getDateOfExamination() {
		return this.dateOfExamination;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  检查日期
	 */
	public void setDateOfExamination(java.util.Date dateOfExamination) {
		this.dateOfExamination = dateOfExamination;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  医院名称
	 */
	@Column(name = "HOSPITAL", length = 100)
	public java.lang.String getHospital() {
		return this.hospital;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  医院名称
	 */
	public void setHospital(java.lang.String hospital) {
		this.hospital = hospital;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  检查编号
	 */
	@Column(name = "EXAMINATION", length = 100)
	public java.lang.String getExamination() {
		return this.examination;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  检查编号
	 */
	public void setExamination(java.lang.String examination) {
		this.examination = examination;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  住院号
	 */
	@Column(name = "HOSPITAL_PATIENT", length = 100)
	public java.lang.String getHospitalPatient() {
		return this.hospitalPatient;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  住院号
	 */
	public void setHospitalPatient(java.lang.String hospitalPatient) {
		this.hospitalPatient = hospitalPatient;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  重点结果描述
	 */
	@Column(name = "KEY_RESULTS_DESCRIPTION", length = 200)
	public java.lang.String getKeyResultsDescription() {
		return this.keyResultsDescription;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  重点结果描述
	 */
	public void setKeyResultsDescription(java.lang.String keyResultsDescription) {
		this.keyResultsDescription = keyResultsDescription;
	}

	/**
	 *方法: 取得CrmPatient
	 *@return: CrmPatient  病人id 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return this.crmPatient;
	}

	/**
	 *方法: 设置CrmPatient
	 *@param: CrmPatient  病人id 
	 */
	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}
}