package com.biolims.crm.customer.patient.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 璇婃柇鏄庣粏
 * @author lims-platform
 * @date 2014-07-30 17:11:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_PATIENT_TREAT")
@SuppressWarnings("serial")
public class CrmPatientTreat extends EntityDao<CrmPatientTreat> implements java.io.Serializable {
	/**璇婃柇id*/
	private java.lang.String id;
	/**璇婃柇*/
	private java.lang.String scheme;
	/**璇婃柇鏃ユ湡*/
	private java.util.Date begin;
	/**鏃堕棿*/
	private java.util.Date end;
	/**肿瘤所在器官*/
	private DicType treat;
	/**病人id */
	private CrmPatient crmPatient;
	/**病人id */
	private CrmPatientDiagnosis crmPatientDiagnosis;

	/**
	 *鏂规硶: 鍙栧緱java.lang.String
	 *@return: java.lang.String  璇婃柇id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *鏂规硶: 璁剧疆java.lang.String
	 *@param: java.lang.String  璇婃柇id
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getScheme() {
		return scheme;
	}

	public void setScheme(java.lang.String scheme) {
		this.scheme = scheme;
	}

	public java.util.Date getBegin() {
		return begin;
	}

	public void setBegin(java.util.Date begin) {
		this.begin = begin;
	}

	public java.util.Date getEnd() {
		return end;
	}

	public void setEnd(java.util.Date end) {
		this.end = end;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TREAT")
	public DicType getTreat() {
		return treat;
	}

	public void setTreat(DicType treat) {
		this.treat = treat;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT")
	public CrmPatient getCrmPatient() {
		return crmPatient;
	}

	public void setCrmPatient(CrmPatient crmPatient) {
		this.crmPatient = crmPatient;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PATIENT_DIAGNOSIS")
	public CrmPatientDiagnosis getCrmPatientDiagnosis() {
		return crmPatientDiagnosis;
	}

	public void setCrmPatientDiagnosis(CrmPatientDiagnosis crmPatientDiagnosis) {
		this.crmPatientDiagnosis = crmPatientDiagnosis;
	}

}