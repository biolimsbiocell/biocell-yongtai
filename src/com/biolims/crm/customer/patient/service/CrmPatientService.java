package com.biolims.crm.customer.patient.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.agent.primary.dao.PrimaryTaskDao;
import com.biolims.crm.agent.primary.model.PrimaryAgreementTaskItem;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.patient.dao.CrmPatientDao;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
import com.biolims.crm.customer.patient.model.CrmPatientItem;
import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.crm.customer.patient.model.CrmPatientPersonnel;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
import com.biolims.crm.customer.patient.model.CrmPatientTreat;
import com.biolims.crm.customer.patient.model.CrmPatientTumorInformation;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.experiment.dna.dao.DnaTaskDao;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.customfields.model.FieldTemplateItem;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmPatientService {
	@Resource
	private CrmPatientDao crmPatientDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;

	@Resource
	private SampleInputService sampleInputService;

	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private FieldService fieldService;

	@Resource
	private SampleInfoMainDao sampleInfoMainDao;

	@Resource
	private PrimaryTaskDao primaryTaskDao;
	@Resource
	private DnaTaskDao dnaTaskDao;

	public <table> List<table> findTableWhere2(String table, String where, String type, String where2, String type2) {
		return crmPatientDao.findTableWhere2(table, where, type, where2, type2);
	}

	public <table> List<table> findTableWhere2(String table, String where) {
		return crmPatientDao.findTablehql(table, where);
	}

	public <table> List<table> findTableWhere(String table, String where, String type) {
		return crmPatientDao.findTableWhere(table, where, type);
	}

	public Map<String, Object> findCrmPatientList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String type, String userId) {
		return crmPatientDao.selectCrmPatientList(mapForQuery, startNum, limitNum, dir, sort, type, userId);
	}

	public Map<String, Object> findCrmPatientNewList(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		return crmPatientDao.selectCrmPatientNewList(start, length, query, col, sort, type);
	}

	public Map<String, Object> findCrmPatientTwoList(Map<String, String> mapForQuery, String name, Integer startNum,
			Integer limitNum, String dir, String sort, String type, String userId) {
		return crmPatientDao.selectCrmPatientTwoList(mapForQuery, name, startNum, limitNum, dir, sort, type, userId);
	}

	public Map<String, Object> findCrmPatientList(String cId, Integer startNum, Integer limitNum, String dir,
			String sort, String type) {
		return crmPatientDao.selectCrmPatientList(cId, startNum, limitNum, dir, sort, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmPatient i) throws Exception {

		crmPatientDao.saveOrUpdate(i);

	}

	public Map<String, Object> findCrmLinkManItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmLinkManItemList(scId, startNum, limitNum, dir, sort);
		// List<CrmLinkManItem> list = (List<CrmLinkManItem>)
		// result.get("list");
		return result;
	}

	public Map<String, Object> findCrmLinkManItemNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return crmPatientDao.selectCrmLinkManItemNewList(start, length, query, col, sort, id);
	}

	public CrmPatient get(String id) {
		CrmPatient crmPatient = commonDAO.get(CrmPatient.class, id);
		return crmPatient;
	}

	public Map<String, Object> findCrmPatientDiagnosisList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientDiagnosisList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientDiagnosis> list = (List<CrmPatientDiagnosis>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientRestsList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientRestsList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientDiagnosis> list = (List<CrmPatientDiagnosis>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientTreatList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientTreatList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientTreat> list = (List<CrmPatientTreat>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientSurgeriesList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientSurgeriesList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientSurgeries> list = (List<CrmPatientSurgeries>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientTumorInformationList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientTumorInformationsList(scId, startNum, limitNum, dir,
				sort);
		List<CrmPatientTumorInformation> list = (List<CrmPatientTumorInformation>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientRadiologyList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientRadiologyList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientRadiology> list = (List<CrmPatientRadiology>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientLaboratoryList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientLaboratoryList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientLaboratory> list = (List<CrmPatientLaboratory>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientLinkManList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientLinkManList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientLinkMan> list = (List<CrmPatientLinkMan>) result.get("list");
		return result;
	}

	// 查询订单
	public Map<String, Object> findCrmPatientOrderList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientOrderList(scId, startNum, limitNum, dir, sort);
		return result;
	}

	public Map<String, Object> findCrmPatientOrderNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return crmPatientDao.selectCrmPatientOrderNewList(start, length, query, col, sort, id);
	}

	// 查询临床样本Scientific
	public Map<String, Object> findCrmSampleInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String number = this.crmPatientDao.queryByOrderId(scId);
		// 获取样本编号
		Map<String, Object> result = crmPatientDao.selectCrmSampleInfoList(scId, startNum, limitNum, dir, sort, number);
		return result;
	}

	public Map<String, Object> findCrmSampleInfoNewList(Integer start, Integer length, String query, String col,
			String sort, String id) {
		String number = this.crmPatientDao.queryByOrderId(id);
		return crmPatientDao.selectCrmSampleInfoNewList(start, length, query, col, sort, number);
	}

	// 查询科研样本
	public Map<String, Object> findCrmScientificSampleInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		// String number = this.crmPatientDao.queryByOrderId(scId);
		// 获取样本编号
		Map<String, Object> result = crmPatientDao.selectCrmScientificSampleInfoList(scId, startNum, limitNum, dir,
				sort);
		return result;
	}

	// 查询收费确认
	public Map<String, Object> findCrmCustormerList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String number = this.crmPatientDao.queryByOrderId(scId);
		Map<String, Object> result = crmPatientDao.selectCrmCustormerList(scId, startNum, limitNum, dir, sort, number);
		return result;

	}

	// 查询收费确认
	public Map<String, Object> findCrmCustormerNewList(Integer start, Integer length, String query, String col,
			String sort, String scId) throws Exception {
		String number = this.crmPatientDao.queryByOrderId(scId);
		return crmPatientDao.selectCrmCustormerNewList(start, length, query, col, sort, number);

	}

	public Map<String, Object> findCrmPatientPathologyList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientPathologyList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientPathology> list = (List<CrmPatientPathology>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientGeneticTestingList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientGeneticTestingList(scId, startNum, limitNum, dir,
				sort);
		List<CrmPatientGeneticTesting> list = (List<CrmPatientGeneticTesting>) result.get("list");
		return result;
	}

	public Map<String, Object> findConsumerMarketList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectConsumerMarketList(scId, startNum, limitNum, dir, sort);
		// List<CrmConsumerMarket> list = (List<CrmConsumerMarket>)
		// result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientDiagnosis(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientDiagnosis> saveItems = new ArrayList<CrmPatientDiagnosis>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientDiagnosis scp = new CrmPatientDiagnosis();
			// 将map信息读入实体类
			scp = (CrmPatientDiagnosis) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			scp.setNum("1");
			if (scp.getDateOfDiagnosis() != null && scp.getCrmPatient().getDateOfBirth() != null) {
				scp.setPatientAge(Integer
						.toString(scp.getDateOfDiagnosis().getYear() - scp.getCrmPatient().getDateOfBirth().getYear()));
			}
			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientSurgeries(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientSurgeries> saveItems = new ArrayList<CrmPatientSurgeries>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientSurgeries scp = new CrmPatientSurgeries();
			// 将map信息读入实体类
			scp = (CrmPatientSurgeries) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmConsumerMarket(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmConsumerMarket> saveItems = new ArrayList<CrmConsumerMarket>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmConsumerMarket scp = new CrmConsumerMarket();
			// 将map信息读入实体类
			scp = (CrmConsumerMarket) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	// 保存子表item和person信息
	public void saveCrmPatientItem2(SampleOrderItem t, CrmPatient r) throws Exception {
		CrmPatientItem i = new CrmPatientItem();
		// i.setId(t.getId());
		// i.setDrugDate(t.getDrugDate());
		i.setEffectOfProgress(t.getEffectOfProgress());
		i.setEffectOfProgressSpeed(t.getEffectOfProgressSpeed());
		i.setGeneticTestHistory(t.getGeneticTestHistory());
		i.setSampleDetectionName(t.getSampleDetectionName());
		i.setSampleDetectionResult(t.getSampleDetectionResult());
		i.setSampleExonRegion(t.getSampleExonRegion());
		i.setUseDrugName(t.getUseDrugName());
		i.setCrmPatient(r);

		crmPatientDao.save(i);
	}

	public void saveCrmPatientPersonnel2(SampleOrderPersonnel p, CrmPatient r) throws Exception {
		CrmPatientPersonnel c = new CrmPatientPersonnel();
		c.setCheckOutTheAge(p.getCheckOutTheAge());
		// c.setFamilyRelation(p.getFamilyRelation());
		c.setTumorCategory(p.getTumorCategory());
		c.setCrmPatient(r);
		crmPatientDao.save(c);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientTreat(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientTreat> saveItems = new ArrayList<CrmPatientTreat>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientTreat scp = new CrmPatientTreat();
			// 将map信息读入实体类
			scp = (CrmPatientTreat) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientRests(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientDiagnosis> saveItems = new ArrayList<CrmPatientDiagnosis>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientDiagnosis scp = new CrmPatientDiagnosis();
			// 将map信息读入实体类
			scp = (CrmPatientDiagnosis) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			scp.setNum("2");
			if (scp.getDateOfDiagnosis() != null && scp.getCrmPatient().getDateOfBirth() != null) {
				scp.setPatientAge(Integer
						.toString(scp.getDateOfDiagnosis().getYear() - scp.getCrmPatient().getDateOfBirth().getYear()));
			}
			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveCrmPatientTumorInformation(CrmPatient sc, String
	// itemDataJson) throws Exception {
	// List<SampleBloodInfo> saveItems = new ArrayList<SampleBloodInfo>();
	// List<Map<String, Object>> list =
	// JsonUtils.toListByJsonArray(itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// SampleBloodInfo scp = new SampleBloodInfo();
	// // 将map信息读入实体类
	// scp = (SampleBloodInfo) crmPatientDao.Map2Bean(map, scp);
	// // if (scp.getId() != null && scp.getId().equals(""))
	// // scp.setId(null);
	// // scp.setCrmPatient(sc);
	// scp.setCustomer(sc.getCrmCustomerId());
	// saveItems.add(scp);
	// }
	// crmPatientDao.saveOrUpdateAll(saveItems);
	// }

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientRadiology(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientRadiology> saveItems = new ArrayList<CrmPatientRadiology>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientRadiology scp = new CrmPatientRadiology();
			// 将map信息读入实体类
			scp = (CrmPatientRadiology) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientLaboratory(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientLaboratory> saveItems = new ArrayList<CrmPatientLaboratory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientLaboratory scp = new CrmPatientLaboratory();
			// 将map信息读入实体类
			scp = (CrmPatientLaboratory) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientPathology(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientPathology> saveItems = new ArrayList<CrmPatientPathology>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientPathology scp = new CrmPatientPathology();
			// 将map信息读入实体类
			scp = (CrmPatientPathology) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientGeneticTesting(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientGeneticTesting> saveItems = new ArrayList<CrmPatientGeneticTesting>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientGeneticTesting scp = new CrmPatientGeneticTesting();
			// 将map信息读入实体类
			scp = (CrmPatientGeneticTesting) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmCustomerLinkMan(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientLinkMan> saveItems = new ArrayList<CrmPatientLinkMan>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientLinkMan scp = new CrmPatientLinkMan();
			// 将map信息读入实体类
			scp = (CrmPatientLinkMan) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			crmPatientDao.saveOrUpdate(scp);
			// saveItems.add(scp);
		}

	}

	// 该方法名由save修改为save1
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(CrmPatient sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmPatientDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmLinkManItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmLinkManItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmConsumerMarket");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmConsumerMarket(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientTreat");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientTreat(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientRests");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientRests(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientTumorInformation");
			// if (jsonStr != null && !jsonStr.equals("{}") &&
			// !jsonStr.equals("")) {
			// saveCrmPatientTumorInformation(sc, jsonStr);
			// }
			jsonStr = (String) jsonMap.get("crmPatientSurgeries");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientSurgeries(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientDiagnosis");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientDiagnosis(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientRadiology");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientRadiology(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientLaboratory");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientLaboratory(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientPathology");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientPathology(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmPatientGeneticTesting");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmPatientGeneticTesting(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("crmCustomerLinkMan");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmCustomerLinkMan(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmLinkManItem(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmLinkManItem> saveItems = new ArrayList<CrmLinkManItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmLinkManItem scp = new CrmLinkManItem();
			// 将map信息读入实体类
			scp = (CrmLinkManItem) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	public String position(String id) throws Exception {
		String city = "";
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("superior", id);
		Map<String, Object> map = crmPatientDao.cusPosition(mapForQuery, null, null, null, null);
		List<CrmTumourPosition> list = (List<CrmTumourPosition>) map.get("list");
		for (int i = 0; i < list.size(); i++) {
			CrmTumourPosition sri = list.get(i);
			city = city + "," + sri.getId() + "," + sri.getName();
		}
		return city;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delModelId(String objName, String objProperty, String vals) throws Exception {
		crmPatientDao.delModelId(objName, objProperty, vals);
	}

	public String getCrmConsumerMarketJson(String type) throws Exception {
		List<CrmConsumerMarket> list = crmPatientDao.findCrmConsumerMarket(type);
		return JsonUtils.toJsonString(list);
	}

	public String getCrmPatientPathologyJson(String type) throws Exception {
		List<CrmPatientPathology> list = crmPatientDao.findCrmPatientPathology(type);
		return JsonUtils.toJsonString(list);
	}

	public boolean queryByCount(String number) {
		boolean flag = crmPatientDao.queryByCount(number);
		return flag;
	}

	// 添加明细
	public Map<String, Object> findCrmPatientPersonnelList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientPersonnelList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientPersonnel> list = (List<CrmPatientPersonnel>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmPatientItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectCrmPatientItemList(scId, startNum, limitNum, dir, sort);
		List<CrmPatientItem> list = (List<CrmPatientItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientPersonnel(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientPersonnel> saveItems = new ArrayList<CrmPatientPersonnel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientPersonnel scp = new CrmPatientPersonnel();
			// 将map信息读入实体类
			scp = (CrmPatientPersonnel) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmPatientPersonnel(String[] ids) throws Exception {
		for (String id : ids) {
			CrmPatientPersonnel scp = crmPatientDao.get(CrmPatientPersonnel.class, id);
			crmPatientDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientItem(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientItem> saveItems = new ArrayList<CrmPatientItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientItem scp = new CrmPatientItem();
			// 将map信息读入实体类
			scp = (CrmPatientItem) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmPatientItem(String[] ids) throws Exception {
		for (String id : ids) {
			CrmPatientItem scp = crmPatientDao.get(CrmPatientItem.class, id);
			crmPatientDao.delete(scp);
		}
	}

	// @WriteOperLogTable
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(CrmPatient sc, Map jsonMap) throws Exception {
	//
	// if (sc != null) {
	// crmPatientDao.saveOrUpdate(sc);
	//
	// String jsonStr = "";
	// jsonStr = (String) jsonMap.get("crmPatientPersonnel");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientPersonnel(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientItem");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientItem(sc, jsonStr);
	// }
	//
	// jsonStr = (String) jsonMap.get("crmLinkManItem");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmLinkManItem(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmConsumerMarket");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmConsumerMarket(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientTreat");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientTreat(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientRests");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientRests(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientSurgeries");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientSurgeries(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientDiagnosis");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientDiagnosis(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientRadiology");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientRadiology(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientLaboratory");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientLaboratory(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientPathology");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientPathology(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmPatientGeneticTesting");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmPatientGeneticTesting(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("crmCustomerLinkMan");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveCrmCustomerLinkMan(sc, jsonStr);
	// }
	// }
	//
	// }

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmPatient sc, Map jsonMap) throws Exception {

		if (sc != null) {
			crmPatientDao.saveOrUpdate(sc);

			String jsonStr = "";

			jsonStr = (String) jsonMap.get("crmLinkManItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmLinkManItem(sc, jsonStr);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmPatient sc, Map jsonMap, String logInfo) throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		if (sc != null) {
			sc.setCreateUser(user);
			sc.setCreateDate(new Date());
			crmPatientDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("CrmPatient");
				li.setModifyContent(logInfo);
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}
			
			String jsonStr = "";

			jsonStr = (String) jsonMap.get("crmLinkManItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmLinkManItem(sc, jsonStr);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCustomerPayment(String itemDataJson) throws Exception {
		List<CrmConsumerMarket> saveItems = new ArrayList<CrmConsumerMarket>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			CrmConsumerMarket sbi = new CrmConsumerMarket();
			sbi = (CrmConsumerMarket) crmPatientDao.Map2Bean(map, sbi);
			sbi.setState("1");
			crmPatientDao.saveOrUpdate(sbi);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCustomerInfo(String itemDataJson) throws Exception {
		List<CrmConsumerMarket> saveItems = new ArrayList<CrmConsumerMarket>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			CrmConsumerMarket sbi = new CrmConsumerMarket();
			sbi = (CrmConsumerMarket) crmPatientDao.Map2Bean(map, sbi);
			saveItems.add(sbi);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);

	}

	public Map<String, Object> findConsumerMarketList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmPatientDao.selectConsumerMarketList(mapForQuery, startNum, limitNum, dir, sort);
		// List<CrmConsumerMarket> list = (List<CrmConsumerMarket>)
		// result.get("list");
		return result;
	}

	public Map<String, Object> selectChargeSampleList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return crmPatientDao.selectChargeSampleList(mapForQuery, startNum, limitNum, dir, sort);
	}

	// 提交样本
	public String submitSample(String[] ids) throws Exception {
		String str = "";
		for (String id : ids) {
			boolean flag = false;
			CrmConsumerMarket ccm = crmPatientDao.get(CrmConsumerMarket.class, id);

			if (ccm.getIsFee().equals("1")) {// 代理商已付费
				// 获得相应产品定价直和
				PrimaryTask pt = this.sampleReceiveDao.get(PrimaryTask.class,
						ccm.getSampleOrder().getPrimary().getId());
				String[] pid = ccm.getSampleOrder().getProductId().split(",");
				Double z = 0.0;
				Double a = 0.0;
				for (String s : pid) {
					PrimaryAgreementTaskItem p = this.primaryTaskDao.selPrimaryAgreementTaskItem(pt.getId(), s);
					z += Double.parseDouble(p.getPrice());
				}

				if (Double.parseDouble(pt.getMoney()) - z >= 0) {// 余额充足
					// 计算代理商剩余余额
					a = Double.parseDouble(pt.getMoney()) - z;
					pt.setMoney(a.toString());

					// 走正常流程
					ccm.setState("0");
					flag = true;

				} else {// 余额不足
					str = "代理商所剩余额不足";
				}
			} else {// 其他情况
				ccm.setState("0");
				flag = true;
			}

			if (flag == true) {
				String[] sampleId = ccm.getSampleId().split(",");
				for (int i = 0; i < sampleId.length; i++) {
					SampleReceiveItem sri = crmPatientDao.get(SampleReceiveItem.class, sampleId[i]);
					DnaTaskInfo scp = crmPatientDao.get(DnaTaskInfo.class, sampleId[i]);
					if (ccm.getSource().equals("SampleReceiveItem")) {
						if (sri.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(sri.getSampleCode());
							st.setSampleCode(sri.getSampleCode());
							if (sri.getSampleNum() != null) {
								st.setNum(Double.parseDouble(sri.getSampleNum()));
							}
							DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
							st.setSampleType(t.getName());
							st.setState("1");
							st.setPatientName(sri.getPatientName());
							st.setSampleTypeId(sri.getDicSampleType().getId());
							st.setInfoFrom("SampleInfo");
							sampleReceiveDao.saveOrUpdate(st);
						} else if (sri.getNextFlowId().equals("0017")) {// 核酸提取
							DnaTaskTemp d = new DnaTaskTemp();
							sri.setState("1");
							sampleInputService.copy(d, sri);
							d.setCode(sri.getSampleCode());
							d.setSampleCode(sri.getSampleCode());
							DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
							d.setSampleType(t.getName());
							// d.setReportDate(DateUtil.format(so.getReportDate()));
							sampleReceiveDao.saveOrUpdate(d);
						} else if (sri.getNextFlowId().equals("0018")) {// 文库构建
							WkTaskTemp d = new WkTaskTemp();
							sri.setState("1");
							sampleInputService.copy(d, sri);
							d.setCode(sri.getSampleCode());
							d.setSampleCode(sri.getSampleCode());
							DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
							d.setSampleType(t.getName());
							// d.setReportDate(so.getReportDate());
							sampleReceiveDao.saveOrUpdate(d);
						} else if (sri.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							sri.setState("4");
							sampleInputService.copy(wka, sri);
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(sri.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
								sri.setState("1");

								DicSampleType t = commonDAO.get(DicSampleType.class, sri.getDicSampleType().getId());
								sri.setSampleType(t.getName());
								// sri.setReportDate(so.getReportDate());
								sampleInputService.copy(o, sri);
							}
						}
					} else {
						String nextFlowId = scp.getNextFlowId();
						NextFlow nf = commonService.get(NextFlow.class, nextFlowId);
						nextFlowId = nf.getSysCode();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setSampleType(scp.getSampleType());
							st.setSampleTypeId(scp.getDicSampleType().getId());
							st.setInfoFrom("SampleDnaInfo");
							// st.setNote("od1:" + scp.getOd260() + " od2:"
							// + scp.getOd280());
							st.setState("1");
							SampleInfo s = this.sampleInfoMainDao.findSampleInfo(scp.getSampleCode());
							st.setPatientName(s.getPatientName());
							dnaTaskDao.saveOrUpdate(st);
						} else if (nextFlowId.equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							sampleInputService.copy(eda, scp);

						} else if (nextFlowId.equals("0011")) {// 重建库
							WkTaskAbnormal wka = new WkTaskAbnormal();
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0012")) {// 暂停
						} else if (nextFlowId.equals("0013")) {// 终止
						} else if (nextFlowId.equals("0018")) {// 文库构建
							WkTaskTemp d = new WkTaskTemp();
							sampleInputService.copy(d, scp);
							dnaTaskDao.saveOrUpdate(d);
						} else if (nextFlowId.equals("0020")) {// 2100质控
							Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
							sampleInputService.copy(qc2100, scp);
							qc2100.setWkType("2");
							dnaTaskDao.saveOrUpdate(qc2100);
						} else if (nextFlowId.equals("0021")) {// QPCR质控
							QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
							sampleInputService.copy(qpcr, scp);
							qpcr.setWkType("1");
							dnaTaskDao.saveOrUpdate(qpcr);
						} else if (nextFlowId.equals("0022")) {// 超声破碎
							UfTaskTemp uf = new UfTaskTemp();
							sampleInputService.copy(uf, scp);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
								try {
									BeanUtils.getDeclaredField(o, "code");
									BeanUtils.setFieldValue(o, "code", sri.getSampleCode());

								} catch (NoSuchFieldException e) {
								}
								sampleInputService.copy(o, scp);
							}
						}
					}
				}
			}

		}
		return str;
	}

	public Map<String, Object> crmPatientSelectTableJsonByName(Integer start, Integer length, String query, String col,
			String sort, String name) throws Exception {
		return crmPatientDao.crmPatientSelectTableJsonByName(start, length, query, col, sort, name);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmLinkManItem(CrmPatient sc, String itemDataJson, String logInfo) throws Exception {
		List<CrmLinkManItem> saveItems = new ArrayList<CrmLinkManItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmLinkManItem scp = new CrmLinkManItem();
			// 将map信息读入实体类
			scp = (CrmLinkManItem) crmPatientDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmPatientDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CrmPatient");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	public long findFileInfoCount(String sId, String type, String userType) throws Exception {
		return crmPatientDao.selectFileInfoCount(sId, type, userType);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> getCsvContent(String fileId) throws Exception {
		FileInfo fileInfo = crmPatientDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		String prompt = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if (a.isFile()) {
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				Map<String, Object> mapField = fieldService.findFieldByModuleValue("CrmPatient");
				List<FieldTemplateItem> list = (List<FieldTemplateItem>) mapField.get("result");
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				CrmPatient cp = null;
				while (reader.readRecord()) {
					String error = "";
					StringBuffer sb = new StringBuffer();
					if (reader.get("Patient ID*") != null && !"".equals(reader.get("Patient ID*"))) {
						cp = commonDAO.get(CrmPatient.class, reader.get("Patient ID*"));
						if (cp == null) {
							cp = new CrmPatient();
							if (reader.get("Date of Birth") != null && !"".equals(reader.get("Date of Birth"))) {
								String pattern = "^((0?[1-9]|1[0-2])/(0?[1-9]|1[0-9]|2[0-8])|(0?[1-9]|1[0-2])/(29|30)|(0?[13578]|1[02])/31)/(?!0000)[0-9]{4}$";
								boolean isMatch = Pattern.matches(pattern, reader.get("Date of Birth"));
								if (isMatch) {
									cp.setDateOfBirth(sdf.parse(reader.get("Date of Birth")));
								} else {
									error += "Date of Birth is incorrect,Date format:MM/dd/yyyy,";
								}
							}
							if (reader.get("Gender") != null && !"".equals(reader.get("Gender"))) {
								if (reader.get("Gender").equals("男") || reader.get("Gender").equals("Male")) {
									cp.setGender("1");
								} else if (reader.get("Gender").equals("女") || reader.get("Gender").equals("Female")) {
									cp.setGender("0");
								} else if (reader.get("Gender").equals("未知") || reader.get("Gender").equals("Unknow")) {
									cp.setGender("3");
								}
							} else {
								error += " Gender must be entered Male or Female or Unknow,";
							}
							cp.setDbGaPID(reader.get("dbGaP submitted subject ID"));
							cp.setDbGaPAccession(reader.get("dbGaP accession"));
							cp.setDiagnosis(reader.get("Diagnosis"));
							cp.setGeneExpressionSubgroup(reader.get("Gene Expression Subgroup"));
							cp.setGeneticSubtype(reader.get("Genetic Subtype"));
							cp.setTreatment(reader.get("Treatment"));
							cp.setBiopsyType(reader.get("Biopsy Type"));
							cp.setAnnArborStage(reader.get("Ann Arbor Stage"));
							cp.setLDHRatio(reader.get("LDH Ratio"));
							cp.setECOGPerformanceStatus(reader.get("ECOG Performance Status"));
							cp.setNumberofExtranodalSites(reader.get("Number of Extranodal Sites"));
							cp.setIPIGroup(reader.get("IPI Group"));
							cp.setIPIRange(reader.get("IPI Range"));
							if (reader.get("Status at Follow up") != null
									&& !"".equals(reader.get("Status at Follow up"))) {
								if (reader.get("Status at Follow up").equals("Alive")) {
									cp.setStatusatFollowup("1");
								} else if (reader.get("Status at Follow up").equals("Dead")) {
									cp.setStatusatFollowup("0");
								} else {
									error += " Status at Follow up must be entered Alive or Dead,";
								}
							}
							if (reader.get("Marriage Status") != null && !"".equals(reader.get("Marriage Status"))) {
								if (reader.get("Marriage Status").equals("已婚")
										|| reader.get("Marriage Status").equals("Married")) {
									cp.setMaritalStatus("1");
								} else if (reader.get("Marriage Status").equals("未婚")
										|| reader.get("Marriage Status").equals("Unmarried")) {
									cp.setMaritalStatus("0");
								} else if (reader.get("Marriage Status").equals("未知")
										|| reader.get("Marriage Status").equals("Unknow")) {
									cp.setMaritalStatus("3");
								} else {
									error += " Marriage Status must be entered Married or Unmarried or Unknow,";
								}
							}
							cp.setFollowupTime(reader.get("Follow up Time"));
							if (reader.get("PFS Status") != null && !"".equals(reader.get("PFS Status"))) {
								if (reader.get("PFS Status").equals("Progressoin")) {
									cp.setPFSStatus("1");
								} else if (reader.get("PFS Status").equals("No Progressoin")) {
									cp.setPFSStatus("0");
								} else {
									error += " PFS Status must be entered Progressoin or No Progressoin,";
								}
							}
							cp.setState("1");
							cp.setPFSTime(reader.get("PFS Time"));
							cp.setSurvivalAnalysis(reader.get("Included in Survival Analysis"));
							cp.setSfz(reader.get("IdCard"));
							cp.setAddress(reader.get("Address"));
							cp.setPlaceOfBirth(reader.get("Native Place"));
							cp.setOccupation(reader.get("Occupation"));
							cp.setEmail(reader.get("Email"));
							cp.setTelphoneNumber1(reader.get("TelphoneNumber1"));
							cp.setTelphoneNumber2(reader.get("TelphoneNumber2"));
							cp.setWeichatId(reader.get("Social-media Account"));
							cp.setName(reader.get("Name"));
							cp.setAge(reader.get("Age"));
							cp.setCreateDate(new Date());
							cp.setId(reader.get("Patient ID*"));
							User u = (User) ServletActionContext.getRequest().getSession()
									.getAttribute(SystemConstants.USER_SESSION_KEY);
							cp.setCreateUser(u);
							cp.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
							cp.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
							sb.append("{");
							for (int b = 0; b < list.size(); b++) {
								if (b != list.size() - 1) {
									if (list.get(b).getFieldType().equals("text")
											|| list.get(b).getFieldType().equals("number")
											|| list.get(b).getFieldType().equals("date")) {
										sb.append("\"" + list.get(b).getFieldName() + "\":\""
												+ reader.get(list.get(b).getLabel()) + "\",");
									} else if (list.get(b).getFieldType().equals("radio")) {
										String[] singleOptsId = list.get(b).getSingleOptionId().split("/");
										String[] singleOptsName = list.get(b).getSingleOptionName().split("/");
										for (int i = 0; i < singleOptsName.length; i++) {
											if (singleOptsName[i].equals(reader.get(list.get(b).getLabel()))) {
												sb.append("\"" + list.get(b).getFieldName() + "\":\"" + singleOptsId[i]
														+ "\",");
											}
										}
									} else if (list.get(b).getFieldType().equals("checkbox")) {
										sb.append("\"" + list.get(b).getFieldName() + "\":[");
										String[] singleOptsIds = list.get(b).getSingleOptionId().split("/");
										String[] singleOptsNames = list.get(b).getSingleOptionName().split("/");
										String[] singleOptsName = reader.get(list.get(b).getLabel()).split("/");
										for (int j = 0; j < singleOptsName.length; j++) {
											for (int i = 0; i < singleOptsNames.length; i++) {
												if (singleOptsNames[i].equals(singleOptsName[j])
														&& j != singleOptsName.length - 1) {
													sb.append("\"" + singleOptsIds[i] + "\",");
												} else if (singleOptsNames[i].equals(singleOptsName[j])) {
													sb.append("\"" + singleOptsIds[i] + "\"");
												}
											}
										}
										sb.append("],");
									}
								} else {

									if (list.get(b).getFieldType().equals("text")
											|| list.get(b).getFieldType().equals("number")
											|| list.get(b).getFieldType().equals("date")) {
										sb.append("\"" + list.get(b).getFieldName() + "\":\""
												+ reader.get(list.get(b).getLabel()) + "\"");
									} else if (list.get(b).getFieldType().equals("radio")) {
										String[] singleOptsId = list.get(b).getSingleOptionId().split("/");
										String[] singleOptsName = list.get(b).getSingleOptionName().split("/");
										for (int i = 0; i < singleOptsName.length; i++) {
											if (singleOptsName[i].equals(reader.get(list.get(b).getLabel()))) {
												sb.append("\"" + list.get(b).getFieldName() + "\":\"" + singleOptsId[i]
														+ "\"");
											}
										}
									} else if (list.get(b).getFieldType().equals("checkbox")) {
										sb.append("\"" + list.get(b).getFieldName() + "\":[");
										String[] singleOptsIds = list.get(b).getSingleOptionId().split("/");
										String[] singleOptsNames = list.get(b).getSingleOptionName().split("/");
										String[] singleOptsName = reader.get(list.get(b).getLabel()).split("/");
										for (int j = 0; j < singleOptsName.length; j++) {
											for (int i = 0; i < singleOptsNames.length; i++) {
												if (singleOptsNames[i].equals(singleOptsName[j])
														&& j != singleOptsName.length - 1) {
													sb.append("\"" + singleOptsIds[i] + "\",");
												} else if (singleOptsNames[i].equals(singleOptsName[j])) {
													sb.append("\"" + singleOptsIds[i] + "\"");
												}
											}
										}
										sb.append("]");
									}

								}
							}
							sb.append("}");
							cp.setFieldContent(sb.toString());
							if (error.equals("")) {
								crmPatientDao.saveOrUpdate(cp);
							}
						} else {
							error += "Patient ID already exists,";
						}
					} else {
						error += "Patient ID required,";
					}
					if (!error.equals("")) {
						prompt += reader.get("Patient ID*") + ":" + error + ";";
					}
				}
				map.put("prompt", prompt);
			}
		}
		return map;
	}

	public CrmPatient getPatient(String idCard) {
		return crmPatientDao.getPatient(idCard);
	}

	public Map<String, Object> findCrmPatientDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return crmPatientDao.findCrmPatientDialogList(start, length, query, col, sort);
	}

}
