package com.biolims.crm.customer.patient.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.dao.CrmTumourPositionDao;
import com.biolims.crm.customer.patient.model.CrmTumourPosition;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmTumourPositionService extends CommonService {
	@Resource
	private CrmTumourPositionDao crmTumourPositionDao;
	@Resource
	private CommonDAO commonDAO;

	public String getDicSampleTypeJson(String type) throws Exception {
		List<CrmTumourPosition> list = crmTumourPositionDao.findDicSamplType(type);
		return JsonUtils.toJsonString(list);
	}

	public String getDicSampleType(String type) throws Exception {
		List<CrmTumourPosition> list = crmTumourPositionDao.findDicSamplTypelist(type);
		return JsonUtils.toJsonString(list);
	}

	public Map<String, Object> findDicTypeList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		return crmTumourPositionDao.selectDicTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicType(String jsonString) throws Exception {

		List<Map> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map map : list) {
			CrmTumourPosition uct = new CrmTumourPosition();
			uct = (CrmTumourPosition) commonDAO.Map2Bean(map, uct);
			if (uct.getId() != null && uct.getId().equals("")) {
				uct.setId(null);

			}
			//			uct.setState("1");
			commonDAO.saveOrUpdate(uct);

		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicType(String id) {
		CrmTumourPosition uct = commonDAO.get(CrmTumourPosition.class, id);
		commonDAO.delete(uct);
	}
}
