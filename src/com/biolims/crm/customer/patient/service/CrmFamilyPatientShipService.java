package com.biolims.crm.customer.patient.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.patient.dao.CrmFamilyPatientShipDao;
import com.biolims.crm.customer.patient.model.CrmFamilyPatientShip;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmFamilyPatientShipService {
	@Resource
	private CrmFamilyPatientShipDao crmFamilyPatientShipDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findCrmFamilyPatientShipList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return crmFamilyPatientShipDao.selectCrmFamilyPatientShipList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmFamilyPatientShip i) throws Exception {

		crmFamilyPatientShipDao.saveOrUpdate(i);

	}
	public CrmFamilyPatientShip get(String id) {
		CrmFamilyPatientShip crmFamilyPatientShip = commonDAO.get(CrmFamilyPatientShip.class, id);
		return crmFamilyPatientShip;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmFamilyPatientShip sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmFamilyPatientShipDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
