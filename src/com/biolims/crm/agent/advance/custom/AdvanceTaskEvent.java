package com.biolims.crm.agent.advance.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.crm.agent.advance.service.AdvanceTaskService;

public class AdvanceTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		AdvanceTaskService mbService = (AdvanceTaskService) ctx.getBean("advanceTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}