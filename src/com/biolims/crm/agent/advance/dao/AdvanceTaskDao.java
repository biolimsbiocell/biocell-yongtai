package com.biolims.crm.agent.advance.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.agent.advance.model.AdvanceTask;
import com.biolims.crm.agent.advance.model.AdvanceTaskItem;
import com.biolims.crm.agent.primary.model.PrimaryAdvanceTaskItem;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class AdvanceTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectAdvanceTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AdvanceTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AdvanceTask> list = new ArrayList<AdvanceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectAdvanceTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AdvanceTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and advanceTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AdvanceTaskItem> list = new ArrayList<AdvanceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<AdvanceTaskItem> setItemList(String id) {
			String hql = "from AdvanceTaskItem where 1=1 and advanceTask.id='" + id + "'";
			List<AdvanceTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
		public List<PrimaryAdvanceTaskItem> setList(String id) {
			String hql = "from PrimaryAdvanceTaskItem where 1=1 and primaryTask.id='" + id + "'";
			List<PrimaryAdvanceTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
}