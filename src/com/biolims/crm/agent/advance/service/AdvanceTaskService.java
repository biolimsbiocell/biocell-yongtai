package com.biolims.crm.agent.advance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.advance.dao.AdvanceTaskDao;
import com.biolims.crm.agent.advance.model.AdvanceTask;
import com.biolims.crm.agent.advance.model.AdvanceTaskItem;
import com.biolims.crm.agent.primary.model.PrimaryAdvanceTaskItem;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AdvanceTaskService {
	@Resource
	private AdvanceTaskDao advanceTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAdvanceTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return advanceTaskDao.selectAdvanceTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AdvanceTask i) throws Exception {

		advanceTaskDao.saveOrUpdate(i);

	}
	public AdvanceTask get(String id) {
		AdvanceTask advanceTask = commonDAO.get(AdvanceTask.class, id);
		return advanceTask;
	}
	public Map<String, Object> findAdvanceTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = advanceTaskDao.selectAdvanceTaskItemList(scId, startNum, limitNum, dir, sort);
		List<AdvanceTaskItem> list = (List<AdvanceTaskItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAdvanceTaskItem(AdvanceTask sc, String itemDataJson) throws Exception {
		List<AdvanceTaskItem> saveItems = new ArrayList<AdvanceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AdvanceTaskItem scp = new AdvanceTaskItem();
			// 将map信息读入实体类
			scp = (AdvanceTaskItem) advanceTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAdvanceTask(sc);

			saveItems.add(scp);
		}
		advanceTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAdvanceTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			AdvanceTaskItem scp =  advanceTaskDao.get(AdvanceTaskItem.class, id);
			 advanceTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AdvanceTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			advanceTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("advanceTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAdvanceTaskItem(sc, jsonStr);
			}
	}
   }
	
	// 改变状态
		public void changeState(String applicationTypeActionId, String id)
				throws Exception {
			AdvanceTask sct = advanceTaskDao.get(AdvanceTask.class, id);
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			advanceTaskDao.update(sct);
			List<AdvanceTaskItem> list = this.advanceTaskDao.setItemList(id);
//			List<PrimaryAdvanceTaskItem> list2 = this.advanceTaskDao.setList(id);
			Double a = 0.0;
			for (AdvanceTaskItem scp : list) {
				AdvanceTask at=this.advanceTaskDao.get(AdvanceTask.class, scp.getAdvanceTask().getId());
				PrimaryTask pt = this.advanceTaskDao.get(PrimaryTask.class, scp.getAdvance().getId());
//				PrimaryAdvanceTaskItem  pat = this.advanceTaskDao.get(PrimaryAdvanceTaskItem.class, scp.getAdvanceTask().getId());
//				for(PrimaryTask pt : list2){
//					if(pt.getId()==scp.getAdvance()){
						if (scp != null ) {
								PrimaryAdvanceTaskItem p = new PrimaryAdvanceTaskItem();
								p.setAdvanceDate(at.getCreateDate());
								p.setAdvanceMoney(scp.getMoney());
								p.setNote(scp.getNote());
								p.setState("1");
								p.setPrimaryTask(scp.getAdvance());
//								Double b = 0.0;
//								if(pat.getActualMoney()==null && "".equals(pat.getActualMoney())){
//								}
//								b = Double.parseDouble(pat.getAdvanceMoney())  -  Double.parseDouble(pat.getActualMoney());
//								pat.setSurplusMoney(b.toString());
//								Double a =0.0;
//								for(PrimaryAdvanceTaskItem w : list2){
//									   a += Double.parseDouble(w.getSurplusMoney());
//									}
//								pt.setMoney(a.toString());
								advanceTaskDao.saveOrUpdate(p);
								if(pt.getMoney()==null || "".equals(pt.getMoney())){
									a = Double.parseDouble(scp.getMoney());
								}else{
									a = Double.parseDouble(scp.getMoney()) + Double.parseDouble(pt.getMoney());
								}
								pt.setMoney(a.toString());
//						}
//					}
				}
			}
		}

	
}
