package com.biolims.crm.agent.advance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 预付款申请明细
 * @author lims-platform
 * @date 2016-08-24 09:56:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ADVANCE_TASK_ITEM")
@SuppressWarnings("serial")
public class AdvanceTaskItem extends EntityDao<AdvanceTaskItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**临时表id*/
	private String tempId;
	/**相关主表*/
	private AdvanceTask advanceTask;
	/**代理商*/
	private PrimaryTask advance;
	/**申请金额*/
	private String money;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	
	
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId(){
		return this.id;
	}
	
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  临时表id
	 */
	@Column(name ="TEMP_ID", length = 100)
	public String getTempId(){
		return this.tempId;
	}
	/**
	 *方法: 设置String
	 *@param: String  临时表id
	 */
	public void setTempId(String tempId){
		this.tempId = tempId;
	}
	/**
	 *方法: 取得AdvanceTask
	 *@return: AdvanceTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ADVANCE_TASK")
	public AdvanceTask getAdvanceTask(){
		return this.advanceTask;
	}
	/**
	 *方法: 设置AdvanceTask
	 *@param: AdvanceTask  相关主表
	 */
	public void setAdvanceTask(AdvanceTask advanceTask){
		this.advanceTask = advanceTask;
	}
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ADVANCE")
	public PrimaryTask getAdvance() {
		return advance;
	}

	public void setAdvance(PrimaryTask advance) {
		this.advance = advance;
	}

	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}