package com.biolims.crm.agent.advance.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.advance.model.AdvanceTask;
import com.biolims.crm.agent.advance.model.AdvanceTaskItem;
import com.biolims.crm.agent.advance.service.AdvanceTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/crm/agent/advance/advanceTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AdvanceTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2308";
	@Autowired
	private AdvanceTaskService advanceTaskService;
	private AdvanceTask advanceTask = new AdvanceTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showAdvanceTaskList")
	public String showAdvanceTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/advance/advanceTask.jsp");
	}

	@Action(value = "showAdvanceTaskListJson")
	public void showAdvanceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = advanceTaskService.findAdvanceTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AdvanceTask> list = (List<AdvanceTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "advanceTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAdvanceTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/advance/advanceTaskDialog.jsp");
	}

	@Action(value = "showDialogAdvanceTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAdvanceTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = advanceTaskService.findAdvanceTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AdvanceTask> list = (List<AdvanceTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editAdvanceTask")
	public String editAdvanceTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			advanceTask = advanceTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "advanceTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			advanceTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			advanceTask.setCreateUser(user);
			advanceTask.setCreateDate(new Date());
			advanceTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			advanceTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(advanceTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/agent/advance/advanceTaskEdit.jsp");
	}

	@Action(value = "copyAdvanceTask")
	public String copyAdvanceTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		advanceTask = advanceTaskService.get(id);
		advanceTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/agent/advance/advanceTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = advanceTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "AdvanceTask";
			String markCode = "YFK";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			advanceTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("advanceTaskItem",
				getParameterFromRequest("advanceTaskItemJson"));

		advanceTaskService.save(advanceTask, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/crm/agent/advance/advanceTask/editAdvanceTask.action?id="
				+ advanceTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}

	@Action(value = "viewAdvanceTask")
	public String toViewAdvanceTask() throws Exception {
		String id = getParameterFromRequest("id");
		advanceTask = advanceTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/agent/advance/advanceTaskEdit.jsp");
	}

	@Action(value = "showAdvanceTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAdvanceTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/agent/advance/advanceTaskItem.jsp");
	}

	@Action(value = "showAdvanceTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAdvanceTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = advanceTaskService
					.findAdvanceTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<AdvanceTaskItem> list = (List<AdvanceTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("tempId", "");
			map.put("advanceTask-name", "");
			map.put("advanceTask-id", "");
			map.put("advance-id", "");
			map.put("advance-name", "");
			map.put("money", "");
			map.put("note", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAdvanceTaskItem")
	public void delAdvanceTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			advanceTaskService.delAdvanceTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AdvanceTaskService getAdvanceTaskService() {
		return advanceTaskService;
	}

	public void setAdvanceTaskService(AdvanceTaskService advanceTaskService) {
		this.advanceTaskService = advanceTaskService;
	}

	public AdvanceTask getAdvanceTask() {
		return advanceTask;
	}

	public void setAdvanceTask(AdvanceTask advanceTask) {
		this.advanceTask = advanceTask;
	}

}
