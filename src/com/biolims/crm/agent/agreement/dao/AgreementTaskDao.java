package com.biolims.crm.agent.agreement.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.agent.agreement.model.AgreementTask;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.file.model.FileInfo;

@Repository
@SuppressWarnings("unchecked")
public class AgreementTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectAgreementTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AgreementTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AgreementTask> list = new ArrayList<AgreementTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectAgreementTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgreementTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and agreementTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgreementTaskItem> list = new ArrayList<AgreementTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		
		
		/**
		 * 根据实体明和id查询图片
		 * 
		 * @param id
		 * @return
		 */
		public List<FileInfo> findFileName(String id, String model) {
			String hql = "from FileInfo t where t.modelContentId='" + id
					+ "' and t.ownerModel='" + model + "'";
			List<FileInfo> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
//		public List<AgreementTaskItem> selectItem(String id) {
//			String hql = "from AgreementTaskItem where 1=1 and agreementTask.id='"
//					+ id + "'";
//			List<AgreementTaskItem> list = this.getSession().createQuery(hql)
//					.list();
//
//			return list;
//		}
		public List<AgreementTaskItem> setItemList(String code) {
			String hql = "from AgreementTaskItem where 1=1 and agreementTask.id='"
					+ code + "'";
			List<AgreementTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
	
}