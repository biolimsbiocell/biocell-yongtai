package com.biolims.crm.agent.agreement.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 协议明细
 * @author lims-platform
 * @date 2016-08-24 10:16:35
 * @version V1.0   
 *
 */
@Entity
@Table(name = "AGREEMENT_TASK_ITEM")
@SuppressWarnings("serial")
public class AgreementTaskItem extends EntityDao<AgreementTaskItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**相关主表*/
	private AgreementTask agreementTask;
	/**代理商*/
	private PrimaryTask advance;
//	/**产品*/
//	private String product;
	// 产品ID
	private String productId;
	// 产品名称
	private String productName;
	/**报价*/
	private String money;
	/**扣率*/
	private String discount;
	/**定价*/
	private String price;
	/**开始日期*/
	private Date startDate;
	/**结束日期*/
	private Date endDate;
	/**附件*/
	private String accessory;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId(){
		return this.id;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得AgreementTask
	 *@return: AgreementTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AGREEMENT_TASK")
	public AgreementTask getAgreementTask(){
		return this.agreementTask;
	}
	/**
	 *方法: 设置AgreementTask
	 *@param: AgreementTask  相关主表
	 */
	public void setAgreementTask(AgreementTask agreementTask){
		this.agreementTask = agreementTask;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ADVANCE")
	public PrimaryTask getAdvance() {
		return advance;
	}

	public void setAdvance(PrimaryTask advance) {
		this.advance = advance;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  扣率
	 */
	@Column(name ="DISCOUNT", length = 50)
	public String getDiscount(){
		return this.discount;
	}
	/**
	 *方法: 设置String
	 *@param: String  扣率
	 */
	public void setDiscount(String discount){
		this.discount = discount;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始日期
	 */
	@Column(name ="START_DATE", length = 50)
	public Date getStartDate(){
		return this.startDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始日期
	 */
	public void setStartDate(Date startDate){
		this.startDate = startDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束日期
	 */
	@Column(name ="END_DATE", length = 50)
	public Date getEndDate(){
		return this.endDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束日期
	 */
	public void setEndDate(Date endDate){
		this.endDate = endDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  附件
	 */
	@Column(name ="ACCESSORY", length = 50)
	public String getAccessory(){
		return this.accessory;
	}
	/**
	 *方法: 设置String
	 *@param: String  附件
	 */
	public void setAccessory(String accessory){
		this.accessory = accessory;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}