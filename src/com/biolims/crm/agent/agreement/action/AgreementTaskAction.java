package com.biolims.crm.agent.agreement.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.ehcache.util.ProductInfo;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.agreement.dao.AgreementTaskDao;
import com.biolims.crm.agent.agreement.model.AgreementTask;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.crm.agent.agreement.service.AgreementTaskService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.product.model.Product;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
@Namespace("/crm/agent/agreement/agreementTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AgreementTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2309";
	@Autowired
	private AgreementTaskService agreementTaskService;
	private AgreementTask agreementTask = new AgreementTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private AgreementTaskDao agreementTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Action(value = "showAgreementTaskList")
	public String showAgreementTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/agreement/agreementTask.jsp");
	}

	@Action(value = "showAgreementTaskListJson")
	public void showAgreementTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = agreementTaskService.findAgreementTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AgreementTask> list = (List<AgreementTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "agreementTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAgreementTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/agreement/agreementTaskDialog.jsp");
	}

	@Action(value = "showDialogAgreementTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAgreementTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = agreementTaskService.findAgreementTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AgreementTask> list = (List<AgreementTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editAgreementTask")
	public String editAgreementTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
			if (id != null && !id.equals("")) {
				agreementTask = agreementTaskService.get(id);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "agreementTask");
				String tName = workflowProcessInstanceService
						.findToDoTaskNameByFormId(id);
				putObjToContext("taskName", tName);
		} else {
			agreementTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			agreementTask.setCreateUser(user);
			agreementTask.setCreateDate(new Date());
			agreementTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			agreementTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(agreementTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/agent/agreement/agreementTaskEdit.jsp");
	}

	@Action(value = "copyAgreementTask")
	public String copyAgreementTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		agreementTask = agreementTaskService.get(id);
		agreementTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/agent/agreement/agreementTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = agreementTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "AgreementTask";
			String markCode = "XY";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			agreementTask.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("agreementTaskItem",getParameterFromRequest("agreementTaskItemJson"));
		agreementTaskService.save(agreementTask,aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/crm/agent/agreement/agreementTask/editAgreementTask.action?id=" 
				+ agreementTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	
	}

	@Action(value = "viewAgreementTask")
	public String toViewAgreementTask() throws Exception {
		String id = getParameterFromRequest("id");
		agreementTask = agreementTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/agent/agreement/agreementTaskEdit.jsp");
	}
	

	@Action(value = "showAgreementTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAgreementTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/agent/agreement/agreementTaskItem.jsp");
	}

	@Action(value = "showAgreementTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAgreementTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = agreementTaskService.findAgreementTaskItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<AgreementTaskItem> list = (List<AgreementTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("agreementTask-name", "");
			map.put("agreementTask-id", "");
			map.put("advance-id", "");
			map.put("advance-name", "");
			map.put("money", "");
			map.put("discount", "");
			map.put("price", "");
			map.put("startDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			map.put("accessory", "");
			map.put("note", "");
			map.put("state", "");
			map.put("productId", "");
			map.put("productName", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delAgreementTaskItem")
	public void delAgreementTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			agreementTaskService.delAgreementTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AgreementTaskService getAgreementTaskService() {
		return agreementTaskService;
	}

	public void setAgreementTaskService(AgreementTaskService agreementTaskService) {
		this.agreementTaskService = agreementTaskService;
	}

	public AgreementTask getAgreementTask() {
		return agreementTask;
	}

	public void setAgreementTask(AgreementTask agreementTask) {
		this.agreementTask = agreementTask;
	}
	


	/**
	 * 加载图片
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "loadPic", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String loadPic() throws Exception {
		String id = getRequest().getParameter("id");
		String model = getRequest().getParameter("model");
		List<FileInfo> flist = agreementTaskDao.findFileName(id, model);
		putObjToContext("flist", flist);
		return dispatcher("/WEB-INF/page/crm/agent/agreement/showTechAnalysisPic.jsp");
	}


}
