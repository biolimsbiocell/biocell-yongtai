package com.biolims.crm.agent.agreement.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.agreement.dao.AgreementTaskDao;
import com.biolims.crm.agent.agreement.model.AgreementTask;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.crm.agent.primary.model.PrimaryAgreementTaskItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AgreementTaskService {
	@Resource
	private AgreementTaskDao agreementTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAgreementTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return agreementTaskDao.selectAgreementTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AgreementTask i) throws Exception {

		agreementTaskDao.saveOrUpdate(i);

	}
	public AgreementTask get(String id) {
		AgreementTask agreementTask = commonDAO.get(AgreementTask.class, id);
		return agreementTask;
	}
	public Map<String, Object> findAgreementTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = agreementTaskDao.selectAgreementTaskItemList(scId, startNum, limitNum, dir, sort);
		List<AgreementTaskItem> list = (List<AgreementTaskItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAgreementTaskItem(AgreementTask sc, String itemDataJson) throws Exception {
		List<AgreementTaskItem> saveItems = new ArrayList<AgreementTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AgreementTaskItem scp = new AgreementTaskItem();
			// 将map信息读入实体类
			scp = (AgreementTaskItem) agreementTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAgreementTask(sc);

			saveItems.add(scp);
		}
		agreementTaskDao.saveOrUpdateAll(saveItems);
		// 设置保留三维小数
				DecimalFormat df = new DecimalFormat("######0.00");

				List<AgreementTaskItem> list2 = this.agreementTaskDao.setItemList(sc
						.getId());
				for (AgreementTaskItem q : list2) {
					if("".equals(q.getPrice()) || q.getPrice()==null){
						if( q.getMoney()!=null && !"".equals(q.getMoney()) 
								&& q.getDiscount()!=null && !"".equals(q.getDiscount())){
							Double b = Double.parseDouble(q.getMoney()) * Double.parseDouble(q.getDiscount()) / 100;
							q.setPrice(df.format(b).toString());
						}
						
					}
				}
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgreementTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			AgreementTaskItem scp =  agreementTaskDao.get(AgreementTaskItem.class, id);
			 agreementTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AgreementTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			agreementTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("agreementTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAgreementTaskItem(sc, jsonStr);
			}
	}
   }
	// 改变状态
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		AgreementTask sct = agreementTaskDao.get(AgreementTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		agreementTaskDao.update(sct);
		List<AgreementTaskItem> list = this.agreementTaskDao.setItemList(id);
//		List<PrimaryTask> list2 = this.advanceTaskDao.setList(id);
		for (AgreementTaskItem scp : list) {
//			PrimaryTask pt=this.advanceTaskDao.get(PrimaryTask.class, scp.getAdvance().getId());
//			for(PrimaryTask pt : list2){
//				if(pt.getId()==scp.getAdvance()){
					if (scp != null ) {
							PrimaryAgreementTaskItem p = new PrimaryAgreementTaskItem();
							p.setStartDate(scp.getStartDate());
							p.setEndDate(scp.getEndDate());
							p.setProductName(scp.getProductName());
							p.setMoney(scp.getMoney());
							p.setDiscount(scp.getDiscount());
							p.setPrice(scp.getPrice());
							p.setProductId(scp.getProductId());
							p.setPrimaryTask(scp.getAdvance());
							p.setNote(scp.getNote());
							p.setState("1");
							agreementTaskDao.saveOrUpdate(p);
//					}
//				}
			}
		}
	}


}
