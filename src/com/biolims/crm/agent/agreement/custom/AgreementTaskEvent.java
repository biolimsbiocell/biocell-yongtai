package com.biolims.crm.agent.agreement.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.crm.agent.agreement.service.AgreementTaskService;

public class AgreementTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		AgreementTaskService mbService = (AgreementTaskService) ctx.getBean("agreementTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}