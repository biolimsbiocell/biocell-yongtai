package com.biolims.crm.agent.primarynew.service;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.dao.PrimaryTaskNewDao;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PrimaryTaskNewService {
	@Resource
	private PrimaryTaskNewDao primaryTaskNewDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findPrimaryTaskNewTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return primaryTaskNewDao.findPrimaryTaskNewTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PrimaryTask i) throws Exception {

		primaryTaskNewDao.saveOrUpdate(i);

	}

	public PrimaryTask get(String id) {
		PrimaryTask primaryTaskNew = commonDAO.get(PrimaryTask.class, id);
		return primaryTaskNew;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PrimaryTask sc, Map jsonMap, String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			primaryTaskNewDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("PrimaryTask");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
		}
	}
}
