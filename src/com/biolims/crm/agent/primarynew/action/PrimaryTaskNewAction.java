﻿
package com.biolims.crm.agent.primarynew.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.agent.primarynew.service.PrimaryTaskNewService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/crm/agent/primarynew/primaryTaskNew")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PrimaryTaskNewAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "23101";
	@Autowired
	private PrimaryTaskNewService primaryTaskNewService;
	private PrimaryTask primaryTaskNew = new PrimaryTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	private String log = "";

	@Action(value = "showPrimaryTaskNewList")
	public String showPrimaryTaskNewList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/primarynew/primaryTaskNew.jsp");
	}

	@Action(value = "showPrimaryTaskNewEditList")
	public String showPrimaryTaskNewEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/primarynew/primaryTaskNewEditList.jsp");
	}

	@Action(value = "showPrimaryTaskNewTableJson")
	public void showPrimaryTaskNewTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = primaryTaskNewService.findPrimaryTaskNewTable(start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("address", "");
			map.put("principal", "");
			map.put("phone", "");
			map.put("marketer", "");
			map.put("money", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state-name", "");
			map.put("note", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("PrimaryTaskNew");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "primaryTaskNewSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPrimaryTaskNewList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/primarynew/primaryTaskNewSelectTable.jsp");
	}

	@Action(value = "editPrimaryTaskNew")
	public String editPrimaryTaskNew() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			primaryTaskNew = primaryTaskNewService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "primaryTaskNew");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			primaryTaskNew.setId("NEW");
			primaryTaskNew.setCreateUser(user);
			primaryTaskNew.setCreateDate(new Date());
			// primaryTaskNew.setState(SystemConstants.DIC_STATE_NEW);
			// primaryTaskNew.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			primaryTaskNew.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			primaryTaskNew.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/agent/primarynew/primaryTaskNewEdit.jsp");
	}

	@Action(value = "copyPrimaryTaskNew")
	public String copyPrimaryTaskNew() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		primaryTaskNew = primaryTaskNewService.get(id);
		primaryTaskNew.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/agent/primarynew/primaryTaskNewEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				primaryTaskNew = (PrimaryTask) commonDAO.Map2Bean(map, primaryTaskNew);
			}
			String id = primaryTaskNew.getId();
			if ((id != null && id.equals("")) || "NEW".equals(id)) {
				log = "123";
				String modelName = "PrimaryTask";
				String markCode = "DLS";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				primaryTaskNew.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();

			primaryTaskNewService.save(primaryTaskNew, aMap, changeLog, lMap,log);

			zId = primaryTaskNew.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewPrimaryTaskNew")
	public String toViewPrimaryTaskNew() throws Exception {
		String id = getParameterFromRequest("id");
		primaryTaskNew = primaryTaskNewService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/agent/primarynew/primaryTaskNewEdit.jsp");
	}

	@Action(value = "savePrimaryTaskNewTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePrimaryTaskNewTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				PrimaryTask a = new PrimaryTask();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (PrimaryTask) commonDAO.Map2Bean(map1, a);
				a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				primaryTaskNewService.save(a, aMap, changeLog, lMap,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PrimaryTaskNewService getPrimaryTaskNewService() {
		return primaryTaskNewService;
	}

	public void setPrimaryTaskNewService(PrimaryTaskNewService primaryTaskNewService) {
		this.primaryTaskNewService = primaryTaskNewService;
	}

	public PrimaryTask getPrimaryTaskNew() {
		return primaryTaskNew;
	}

	public void setPrimaryTaskNew(PrimaryTask primaryTaskNew) {
		this.primaryTaskNew = primaryTaskNew;
	}

}
