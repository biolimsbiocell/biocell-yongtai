package com.biolims.crm.agent.primarynew.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 代理商主数据
 * @author lims-platform
 * @date 2016-08-24 11:07:50
 * @version V1.0
 *
 */
@Entity
@Table(name = "PRIMARY_TASK")
@SuppressWarnings("serial")
public class PrimaryTask extends EntityDao<PrimaryTask> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	private String name;
	/** 地址 */
	private String address;
	/** 负责人 */
	private String principal;
	/** 电话 */
	private String phone;
	/** 管理销售 */
	private User marketer;
	/** 预付余额 */
	private String money;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 评级 */
	private DicType rate;
	/** 状态 */
	private DicState state;
	/** 备注 */
	private String note;
	private String scopeId;
	private String scopeName;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	public String getScopeId() {
		return scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 地址
	 */
	@Column(name = "ADDRESS", length = 50)
	public String getAddress() {
		return this.address;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 负责人
	 */
	@Column(name = "PRINCIPAL", length = 50)
	public String getPrincipal() {
		return this.principal;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             负责人
	 */
	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 电话
	 */
	@Column(name = "PHONE", length = 50)
	public String getPhone() {
		return this.phone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MARKETER")
	public User getMarketer() {
		return marketer;
	}

	public void setMarketer(User marketer) {
		this.marketer = marketer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RATE")
	public DicType getRate() {
		return rate;
	}

	public void setRate(DicType rate) {
		this.rate = rate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得DicState
	 * 
	 * @return: DicState 状态
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STATE")
	public DicState getState() {
		return this.state;
	}

	/**
	 * 方法: 设置DicState
	 * 
	 * @param: DicState
	 *             状态
	 */
	public void setState(DicState state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 100)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
}