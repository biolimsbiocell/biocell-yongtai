package com.biolims.crm.agent.primary.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primary.model.PrimaryAdvanceTaskItem;
import com.biolims.crm.agent.primary.model.PrimaryAgreementTaskItem;
import com.biolims.crm.agent.primary.service.PrimaryTaskService;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/crm/agent/primary/primaryTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PrimaryTaskAction extends BaseActionSupport {

	// private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2310";
	@Autowired
	private PrimaryTaskService primaryTaskService;
	private PrimaryTask primaryTask = new PrimaryTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showPrimaryTaskList")
	public String showPrimaryTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryTask.jsp");
	}

	@Action(value = "showPrimaryTaskListJson")
	public void showPrimaryTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = primaryTaskService.findPrimaryTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("address", "");
		map.put("principal", "");
		map.put("phone", "");
		map.put("marketer-id", "");
		map.put("marketer-name", "");
		map.put("money", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("note", "");
		map.put("rate-id", "");
		map.put("rate-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	// @Action(value = "showPrimaryTaskSelectTree", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showPrimaryTaskSelectTree() throws Exception {
	//
	// putObjToContext(
	// "path",
	// ServletActionContext.getRequest().getContextPath()
	// + "/crm/agent/primary/primaryTask/showPrimaryTaskSelectTreeJson.action");
	//
	// return
	// dispatcher("/WEB-INF/page/com/biolims/system/product/showProductSelectTree.jsp");
	// }
	//
	// @Action(value = "showPrimaryTaskSelectTreeJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showPrimaryTaskSelectTreeJson() throws Exception {
	// String upId = getParameterFromRequest("treegrid_id");
	// List<PrimaryTask> aList = null;
	// if (upId.equals("0") || upId.equals("")) {
	//
	// aList = primaryTaskService.findPrimaryTaskList();
	// } else {
	//
	// }
	// // aList = productService.findProductList(upId);
	// // List<Product> aList = productService.findProductList();
	//
	// String a = primaryTaskService.getTreeJson(aList);
	//
	// new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// }

	@Action(value = "primaryTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPrimaryTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryTaskDialog.jsp");
	}

	@Action(value = "showDialogPrimaryTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPrimaryTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = primaryTaskService.findPrimaryTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("address", "");
		map.put("principal", "");
		map.put("phone", "");
		map.put("marketer-id", "");
		map.put("marketer-name", "");
		map.put("money", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editPrimaryTask")
	public String editPrimaryTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			primaryTask = primaryTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "primaryTask");
			String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			primaryTask.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			primaryTask.setCreateUser(user);
			primaryTask.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryTaskEdit.jsp");
	}

	@Action(value = "copyPrimaryTask")
	public String copyPrimaryTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		primaryTask = primaryTaskService.get(id);
		primaryTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = primaryTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "PrimaryTask";
			String markCode = "DLS";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			primaryTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("primaryAdvanceTaskItem", getParameterFromRequest("primaryAdvanceTaskItemJson"));
		aMap.put("primaryAgreementTaskItem", getParameterFromRequest("primaryAgreementTaskItemJson"));
		primaryTaskService.save(primaryTask, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/crm/agent/primary/primaryTask/editPrimaryTask.action?id=" + primaryTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}

	@Action(value = "viewPrimaryTask")
	public String toViewPrimaryTask() throws Exception {
		String id = getParameterFromRequest("id");
		primaryTask = primaryTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryTaskEdit.jsp");
	}

	// 查询
	@Action(value = "showPrimaryAdvanceTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPrimaryAdvanceTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryAdvanceTaskItem.jsp");
	}

	@Action(value = "showPrimaryAdvanceTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPrimaryAdvanceTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = primaryTaskService.findPrimaryAdvanceTaskItemList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<PrimaryAdvanceTaskItem> list = (List<PrimaryAdvanceTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("primaryTask-name", "");
			map.put("primaryTask-id", "");
			map.put("advanceMoney", "");
			map.put("advanceDate", "yyyy-MM-dd");
			map.put("actualMoney", "");
			map.put("surplusMoney", "");
			map.put("note", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPrimaryAdvanceTaskItem")
	public void delPrimaryAdvanceTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			primaryTaskService.delPrimaryAdvanceTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPrimaryAgreementTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPrimaryAgreementTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryAgreementTaskItem.jsp");
	}

	@Action(value = "showPrimaryAgreementTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPrimaryAgreementTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = primaryTaskService.findPrimaryAgreementTaskItemList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<PrimaryAgreementTaskItem> list = (List<PrimaryAgreementTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("primaryTask-name", "");
			map.put("primaryTask-id", "");
			map.put("startDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			map.put("productId", "");
			map.put("productName", "");
			map.put("money", "");
			map.put("discount", "");
			map.put("price", "");
			map.put("deduct", "");
			map.put("isvalid", "");
			map.put("note", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPrimaryAgreementTaskItem")
	public void delPrimaryAgreementTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			primaryTaskService.delPrimaryAgreementTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPrimaryOrderTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPrimaryOrderTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/agent/primary/primaryOrderTaskItem.jsp");
	}

	@Action(value = "showPrimaryOrderTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPrimaryOrderTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = primaryTaskService.findPrimaryOrderTaskItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("primary-name", "");
			map.put("primary-id", "");
			map.put("name", "");
			map.put("receivedDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	// @Action(value = "delPrimaryOrderTaskItem")
	// public void delPrimaryOrderTaskItem() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// String[] ids = getRequest().getParameterValues("ids[]");
	// primaryTaskService.delPrimaryOrderTaskItem(ids);
	// map.put("success", true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// map.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//

	@Action(value = "showPrimarySampleTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPrimarySampleTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/agent/primary/primarySampleTaskItem.jsp");
	}

	@Action(value = "showPrimarySampleTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPrimarySampleTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = primaryTaskService.findPrimarySampleTaskItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();

			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleNum", "");
			map.put("productName", "");

			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PrimaryTaskService getPrimaryTaskService() {
		return primaryTaskService;
	}

	public void setPrimaryTaskService(PrimaryTaskService primaryTaskService) {
		this.primaryTaskService = primaryTaskService;
	}

	public PrimaryTask getPrimaryTask() {
		return primaryTask;
	}

	public void setPrimaryTask(PrimaryTask primaryTask) {
		this.primaryTask = primaryTask;
	}

}
