package com.biolims.crm.agent.primary.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 预付款信息明细
 * @author lims-platform
 * @date 2016-08-24 11:07:41
 * @version V1.0   
 *
 */
@Entity
@Table(name = "PRIMARY_ADVANCE_TASK_ITEM")
@SuppressWarnings("serial")
public class PrimaryAdvanceTaskItem extends EntityDao<PrimaryAdvanceTaskItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**相关主表*/
	private PrimaryTask primaryTask;
	/**预付金额*/
	private String advanceMoney;
	/**预付日期*/
	private Date advanceDate;
	/**销售金额*/
	private String actualMoney;
	/**余额*/
	private String surplusMoney;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	
	
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId(){
		return this.id;
	}
	public String getAdvanceMoney() {
		return advanceMoney;
	}
	public void setAdvanceMoney(String advanceMoney) {
		this.advanceMoney = advanceMoney;
	}
	public String getActualMoney() {
		return actualMoney;
	}
	public void setActualMoney(String actualMoney) {
		this.actualMoney = actualMoney;
	}
	public String getSurplusMoney() {
		return surplusMoney;
	}
	public void setSurplusMoney(String surplusMoney) {
		this.surplusMoney = surplusMoney;
	}
	public Date getAdvanceDate() {
		return advanceDate;
	}
	public void setAdvanceDate(Date advanceDate) {
		this.advanceDate = advanceDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得PrimaryTask
	 *@return: PrimaryTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRIMARY_TASK")
	public PrimaryTask getPrimaryTask(){
		return this.primaryTask;
	}
	/**
	 *方法: 设置PrimaryTask
	 *@param: PrimaryTask  相关主表
	 */
	public void setPrimaryTask(PrimaryTask primaryTask){
		this.primaryTask = primaryTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}