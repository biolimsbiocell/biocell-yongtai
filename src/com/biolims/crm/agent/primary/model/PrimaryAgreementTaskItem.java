package com.biolims.crm.agent.primary.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 协议信息明细
 * @author lims-platform
 * @date 2016-08-24 11:07:43
 * @version V1.0   
 *
 */
@Entity
@Table(name = "PRIMARY_AGREEMENT_TASK_ITEM")
@SuppressWarnings("serial")
public class PrimaryAgreementTaskItem extends EntityDao<PrimaryAgreementTaskItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**相关主表*/
	private PrimaryTask primaryTask;
	/**开始执行日期*/
	private Date startDate;
	/**结束执行日期*/
	private Date endDate;
	// 产品ID
	private String productId;
	// 产品名称
	private String productName;
	/**报价*/
	private String money;
	/**扣率*/
	private String discount;
	/**定价*/
	private String price;
	/**预付扣除*/
	private String deduct;
	/**是否有效*/
	private String isvalid;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	
	
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId(){
		return this.id;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDeduct() {
		return deduct;
	}
	public void setDeduct(String deduct) {
		this.deduct = deduct;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得PrimaryTask
	 *@return: PrimaryTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRIMARY_TASK")
	public PrimaryTask getPrimaryTask(){
		return this.primaryTask;
	}
	/**
	 *方法: 设置PrimaryTask
	 *@param: PrimaryTask  相关主表
	 */
	public void setPrimaryTask(PrimaryTask primaryTask){
		this.primaryTask = primaryTask;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始执行日期
	 */
	@Column(name ="START_DATE", length = 50)
	public Date getStartDate(){
		return this.startDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始执行日期
	 */
	public void setStartDate(Date startDate){
		this.startDate = startDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束执行日期
	 */
	@Column(name ="END_DATE", length = 50)
	public Date getEndDate(){
		return this.endDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束执行日期
	 */
	public void setEndDate(Date endDate){
		this.endDate = endDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  扣率
	 */
	@Column(name ="DISCOUNT", length = 50)
	public String getDiscount(){
		return this.discount;
	}
	/**
	 *方法: 设置String
	 *@param: String  扣率
	 */
	public void setDiscount(String discount){
		this.discount = discount;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否有效
	 */
	@Column(name ="ISVALID", length = 50)
	public String getIsvalid(){
		return this.isvalid;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否有效
	 */
	public void setIsvalid(String isvalid){
		this.isvalid = isvalid;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
}