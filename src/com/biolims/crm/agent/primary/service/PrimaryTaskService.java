package com.biolims.crm.agent.primary.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.agent.primary.dao.PrimaryTaskDao;
import com.biolims.crm.agent.primary.model.PrimaryAdvanceTaskItem;
import com.biolims.crm.agent.primary.model.PrimaryAgreementTaskItem;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PrimaryTaskService {
	@Resource
	private PrimaryTaskDao primaryTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findPrimaryTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return primaryTaskDao.selectPrimaryTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PrimaryTask i) throws Exception {

		primaryTaskDao.saveOrUpdate(i);

	}
	public PrimaryTask get(String id) {
		PrimaryTask primaryTask = commonDAO.get(PrimaryTask.class, id);
		return primaryTask;
	}
	public Map<String, Object> findPrimaryAdvanceTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = primaryTaskDao.selectPrimaryAdvanceTaskItemList(scId, startNum, limitNum, dir, sort);
		List<PrimaryAdvanceTaskItem> list = (List<PrimaryAdvanceTaskItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findPrimaryAgreementTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = primaryTaskDao.selectPrimaryAgreementTaskItemList(scId, startNum, limitNum, dir, sort);
		List<PrimaryAgreementTaskItem> list = (List<PrimaryAgreementTaskItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePrimaryAdvanceTaskItem(PrimaryTask sc, String itemDataJson) throws Exception {
		List<PrimaryAdvanceTaskItem> saveItems = new ArrayList<PrimaryAdvanceTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PrimaryAdvanceTaskItem scp = new PrimaryAdvanceTaskItem();
			// 将map信息读入实体类
			scp = (PrimaryAdvanceTaskItem) primaryTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPrimaryTask(sc);

			saveItems.add(scp);
		}
		primaryTaskDao.saveOrUpdateAll(saveItems);
//		// 设置保留三维小数
//		DecimalFormat df = new DecimalFormat("######0.00");
//
//		List<PrimaryAdvanceTaskItem> list2 = this.primaryTaskDao.setAdvaceItemList(sc
//				.getId());
////		PrimaryTask pt = this.primaryTaskDao.setAdvaceItemList(code)
//		PrimaryTask pt = this.primaryTaskDao.get(PrimaryTask.class, sc.getId());
//		Double a = 0.0;
//		for (PrimaryAdvanceTaskItem q : list2) {
//			
//			if(q.getAdvanceMoney()!=null && !"".equals(q.getActualMoney())
//					&& q.getAdvanceMoney()!=null && !"".equals(q.getActualMoney())){
//				Double  b = Double.parseDouble(q.getAdvanceMoney()) - Double.parseDouble(q.getActualMoney());
//				q.setSurplusMoney(df.format(b).toString());
//			}
//				 a += Double.parseDouble(q.getSurplusMoney());
//				
//		}
//		 pt.setMoney(df.format(a).toString());
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPrimaryAdvanceTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			PrimaryAdvanceTaskItem scp =  primaryTaskDao.get(PrimaryAdvanceTaskItem.class, id);
			 primaryTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePrimaryAgreementTaskItem(PrimaryTask sc, String itemDataJson) throws Exception {
		List<PrimaryAgreementTaskItem> saveItems = new ArrayList<PrimaryAgreementTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PrimaryAgreementTaskItem scp = new PrimaryAgreementTaskItem();
			// 将map信息读入实体类
			scp = (PrimaryAgreementTaskItem) primaryTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPrimaryTask(sc);

			saveItems.add(scp);
		}
		primaryTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPrimaryAgreementTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			PrimaryAgreementTaskItem scp =  primaryTaskDao.get(PrimaryAgreementTaskItem.class, id);
			 primaryTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PrimaryTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			primaryTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("primaryAdvanceTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePrimaryAdvanceTaskItem(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("primaryAgreementTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePrimaryAgreementTaskItem(sc, jsonStr);
			}
			
	}
   }
	public List<PrimaryTask> findPrimaryTaskList() throws Exception {
		List<PrimaryTask> list = commonDAO.find("from PrimaryTask order by id asc");
		return list;
	}
	
//	订单明细子表
	public Map<String, Object> findPrimaryOrderTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = primaryTaskDao.selectPrimaryOrderTaskItemList(scId, startNum, limitNum, dir, sort);
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");
		return result;
	}
//	
//	/**
//	 * 删除明细
//	 * @param ids
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delPrimaryOrderTaskItem(String[] ids) throws Exception {
//		for (String id : ids) {
//			SampleOrder scp =  primaryTaskDao.get(SampleOrder.class, id);
//			 primaryTaskDao.delete(scp);
//		}
//	}
//	
	

//	样本明细子表
	public Map<String, Object> findPrimarySampleTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = primaryTaskDao.selectPrimarySampleTaskItemList(scId, startNum, limitNum, dir, sort);
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");
		return result;
	}
}
