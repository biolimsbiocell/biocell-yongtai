package com.biolims.crm.agent.primary.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.agent.primary.model.PrimaryAdvanceTaskItem;
import com.biolims.crm.agent.primary.model.PrimaryAgreementTaskItem;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class PrimaryTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectPrimaryTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from PrimaryTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<PrimaryTask> list = new ArrayList<PrimaryTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectPrimaryAdvanceTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from PrimaryAdvanceTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and primaryTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<PrimaryAdvanceTaskItem> list = new ArrayList<PrimaryAdvanceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectPrimaryAgreementTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from PrimaryAgreementTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and primaryTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<PrimaryAgreementTaskItem> list = new ArrayList<PrimaryAgreementTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectPrimaryOrderTaskItemList(String scId, Integer startNum, Integer limitNum,
				String dir, String sort) throws Exception {
			String hql = "from SampleOrder where 1=1 ";
			String key = "";
			if (scId != null)
				key = key + " and primary.id='" + scId + "'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<SampleOrder> list = new ArrayList<SampleOrder>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		
		public Map<String, Object> selectPrimarySampleTaskItemList(String scId, Integer startNum, Integer limitNum,
				String dir, String sort) throws Exception {
			String hql = "from SampleInfo s where 1=1 ";
			String key = "";
			if (scId != null)
				key = key + " and s.sampleOrder.primary.id='" + scId + "'";
			
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<SampleInfo> list = new ArrayList<SampleInfo>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		
		
		
	
		
		
		
		public List<PrimaryAdvanceTaskItem> setAdvaceItemList(String code) {
			String hql = "from PrimaryAdvanceTaskItem where 1=1 and primaryTask.id='"
					+ code + "'";
			List<PrimaryAdvanceTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		//根据主表id和检测项目id查询
		public PrimaryAgreementTaskItem selPrimaryAgreementTaskItem(String id,String pid){
			String hql = "from PrimaryAgreementTaskItem where 1=1 and primaryTask.id='"
					+ id + "' and productId='"+pid+"'";
			PrimaryAgreementTaskItem p=(PrimaryAgreementTaskItem)this.getSession().createQuery(hql).uniqueResult();
			return p;
		}
	
		
}