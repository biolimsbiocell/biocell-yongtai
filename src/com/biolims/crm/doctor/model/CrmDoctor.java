package com.biolims.crm.doctor.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 鐥呬汉涓绘暟鎹�
 * @author lims-platform
 * @date 2014-07-30 17:11:31
 * @version V1.0
 */
@Entity
@Table(name = "CRM_DOCTOR")
@SuppressWarnings("serial")
public class CrmDoctor extends EntityDao<CrmDoctor> implements
		java.io.Serializable {

	private java.lang.String id;// ID

	private java.lang.String name;// 姓名

	// private java.lang.String gender;//性别

	private User createUser;// 创建人
	//
	private java.util.Date createDate;// 创建日期

	private String isMonthFee;// 是否月结

	private java.lang.String mail;// 邮箱

	private java.lang.String mobile;// 手机号

	private CrmCustomer crmCustomer;// 所属医院

	private DicType ks;// 所属科室

	private String state;// 状态

	private String jobTitle;// 职称

	private String payTreasure;// 支付宝

	private String doctorIntroduction;// 医生简介

	private String goodAtDisease;// 擅长疾病

	private String clinicCharacteristics;// 就诊特点

	private String contactPerson;// 联络人

	private String HeadOfSales;// 销售负责人

	private String weichatId;// 微信号

	/** 分类 */
	private DicType type;
	// 其他信息
	private String otherInformation;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	public String getOtherInformation() {
		return otherInformation;
	}

	public void setOtherInformation(String otherInformation) {
		this.otherInformation = otherInformation;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 分类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 分类
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	@Id
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	@Column(name = "MOBILE", length = 20)
	public java.lang.String getMobile() {
		return mobile;
	}

	public void setMobile(java.lang.String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "PAY_TREASURE", length = 40)
	public String getPayTreasure() {
		return payTreasure;
	}

	public void setPayTreasure(String payTreasure) {
		this.payTreasure = payTreasure;
	}

	@Column(name = "CONTACT_PERSON", length = 30)
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Column(name = "HEAD_OF_SALES", length = 10)
	public String getHeadOfSales() {
		return HeadOfSales;
	}

	public void setHeadOfSales(String headOfSales) {
		this.HeadOfSales = headOfSales;
	}

	@Column(name = "JOB_TITLE", length = 40)
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Column(name = "DOCTOR_INTRODUCTION", length = 200)
	public String getDoctorIntroduction() {
		return doctorIntroduction;
	}

	public void setDoctorIntroduction(String doctorIntroduction) {
		this.doctorIntroduction = doctorIntroduction;
	}

	@Column(name = "GOOD_AT_DISEASE", length = 200)
	public String getGoodAtDisease() {
		return goodAtDisease;
	}

	public void setGoodAtDisease(String goodAtDisease) {
		this.goodAtDisease = goodAtDisease;
	}

	@Column(name = "CLINIC_CHARACTERISTICS", length = 200)
	public String getClinicCharacteristics() {
		return clinicCharacteristics;
	}

	public void setClinicCharacteristics(String clinicCharacteristics) {
		this.clinicCharacteristics = clinicCharacteristics;
	}

	@Column(name = "NAME", length = 20)
	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	@Column(name = "IS_MONTH_FEE", length = 100)
	public String getIsMonthFee() {
		return isMonthFee;
	}

	public void setIsMonthFee(String isMonthFee) {
		this.isMonthFee = isMonthFee;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	@Column(name = "CREATE_DATE", length = 20)
	public java.util.Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "MAIL", length = 30)
	public java.lang.String getMail() {
		return mail;
	}

	public void setMail(java.lang.String mail) {
		this.mail = mail;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER_ID")
	public CrmCustomer getCrmCustomer() {
		return crmCustomer;
	}

	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KS")
	public DicType getKs() {
		return ks;
	}

	public void setKs(DicType ks) {
		this.ks = ks;
	}

	/*
	 * 微信号
	 */
	@Column(name = "WEI_CHAT_ID")
	public String getWeichatId() {
		return weichatId;
	}

	public void setWeichatId(String weichatId) {
		this.weichatId = weichatId;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}