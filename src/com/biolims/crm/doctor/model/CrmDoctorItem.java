package com.biolims.crm.doctor.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 协议信息明细
 * @author lims-platform
 * @date 2016-08-24 11:07:43
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_DOCTOR_ITEM")
@SuppressWarnings("serial")
public class CrmDoctorItem extends EntityDao<CrmDoctorItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**相关主表*/
	private CrmDoctor crmDoctor;
	// 产品ID
		private String productId;
		// 产品名称
		private String productName;
	/**报价*/
	private String money;
	/**扣率*/
	private String discount;
	/**定价*/
	private String price;
//	结款方式
	private String way;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	
	//相关主表
		@ManyToOne(fetch = FetchType.LAZY)
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name = "CRM_DOCTOR")
	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}
	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}
	//产品ID
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	//结款方式
	public String getWay() {
		return way;
	}
	public void setWay(String way) {
		this.way = way;
	}
	//产品
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	//报价
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	//扣率
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	//定价
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	//备注
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	//状态
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
}