package com.biolims.crm.doctor.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.doctor.model.CrmDoctorItem;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CrmDoctorDao extends BaseHibernateDao {
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> selectCrmPatientList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from CrmDoctor where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmPatient> list = new ArrayList<CrmPatient>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	@SuppressWarnings("unchecked")
	public void delModelId(String objName, String objProperty, String vals) throws Exception {
		String hql = " delete from " + objName + " where " + objProperty + " in (" + vals + ")";
		Query query = this.getSession().createQuery(hql);
		query.executeUpdate();
	}

	public Map<String, Object> selectConsumerMarketList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmConsumerMarket where 1=1 ";
		String key = "";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmConsumerMarket> list = new ArrayList<CrmConsumerMarket>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<CrmConsumerMarket> findCrmConsumerMarket(String type) {
		List<CrmConsumerMarket> list = commonDAO.find("from CrmConsumerMarket  order by id asc");
		return list;

	}

	public <table> List<table> findTableWhere(String table, String where, String type) {
		String hql = "";
		hql = "from " + table;
		if (where != null && type != null) {
			hql = hql + " where " + where + " = " + type + "";
		}
		hql = hql + " order by id asc ";
		List<table> list = commonDAO.find(hql);
		return list;
	}

	public <table> List<table> findTableWhere2(String table, String where, String type, String where2, String type2) {
		String hql = "";
		hql = "from " + table;
		if (where != null && type != null && where2 != null && type2 != null) {
			hql = hql + " where " + where + " = " + type + " and " + where2 + " = " + type2 + "";
		}
		hql = hql + " order by id asc ";
		List<table> list = commonDAO.find(hql);
		return list;
	}

	public <table> List<table> findTablehql(String table, String where) {
		List<table> list = commonDAO.find(where);
		return list;
	}
	
	public Map<String, Object> selectCrmDoctorItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmDoctorItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmDoctor.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmDoctorItem> list = new ArrayList<CrmDoctorItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public List<CrmDoctorItem> setItemList(String code) {
		String hql = "from CrmDoctorItem where 1=1 and crmDoctor.id='"
				+ code + "'";
		List<CrmDoctorItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showDialogCrmPatientTableJson(Integer start,
			Integer length, String query, String col, String sort) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmDoctor where 1=1 ";
		String key = "";
//		if(queryMap!=null){
//			key=map2Where(queryMap);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CrmDoctor where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CrmDoctor> list = new ArrayList<CrmDoctor>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}

	public Map<String, Object> findCrmDoctorTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  CrmDoctor where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CrmDoctor where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CrmDoctor> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findCrmDoctorItemTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CrmDoctorItem where 1=1 and crmDoctor.id='"+id+"'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CrmDoctorItem where 1=1 and crmDoctor.id='"+id+"' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CrmDoctorItem> list = new ArrayList<CrmDoctorItem>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
}