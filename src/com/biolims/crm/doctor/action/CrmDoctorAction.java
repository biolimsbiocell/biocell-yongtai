package com.biolims.crm.doctor.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.doctor.model.CrmDoctorItem;
import com.biolims.crm.doctor.service.CrmDoctorService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 医生的Action
 */
@Namespace("/crm/doctor")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmDoctorAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2307";
	@Autowired
	private CrmDoctorService crmDoctorService;
	private CrmDoctor crmDoctor = new CrmDoctor();
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private FieldService fieldService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private CommonDAO commonDAO;
	private String log = "";
	
/*NEW*/
	@Action(value = "showCrmDoctorList")
	public String showCrmDoctorList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		rightsId = "2307";
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctor.jsp");
	}

	@Action(value = "showCrmDoctorEditList")
	public String showCrmDoctorEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		rightsId = "2307";
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctorEditList.jsp");
	}
	@Action(value = "showCrmDoctorTableJson")
	public void showCrmDoctorTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = crmDoctorService.findCrmDoctorTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<CrmDoctor> list = (List<CrmDoctor>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mobile", "");
			map.put("weichatId", "");
			map.put("isMonthFee", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("state", "");
			map.put("name", "");
			map.put("crmCustomer-id", "");
			map.put("crmCustomer-name", "");
			map.put("ks-id", "");
			map.put("ks-name", "");
			map.put("clinicCharacteristics", "");
			map.put("goodAtDisease", "");
			map.put("doctorIntroduction", "");
			map.put("mail", "");
			map.put("payTreasure", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("CrmDoctor");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "editCrmDoctor")
	public String editCrmDoctor() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		rightsId = "2307";
		if (id != null && !id.equals("")) {
			crmDoctor = crmDoctorService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmDoctor");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmDoctor.setCreateUser(user);
			crmDoctor.setCreateDate(new Date());
			crmDoctor.setState(SystemConstants.DIC_STATE_NEW);
			crmDoctor.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			crmDoctor.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctorEdit.jsp");
	}

	@Action(value = "crmDoctorSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmDoctorList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctorSelectTable.jsp");
	}

	@Action(value = "copyCrmDoctor")
	public String copyCrmDoctor() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmDoctor = crmDoctorService.get(id);
		crmDoctor.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctorEdit.jsp");
	}


	@Action(value = "save")
	public void save() throws Exception {
		
		
		
		String msg = "";
		String zId = "";
		boolean bool = true;	
		String code = "";
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				crmDoctor = (CrmDoctor)commonDAO.Map2Bean(map, crmDoctor);
			}
			
			String id = crmDoctor.getId();
			if(id!=null&&id.equals("")){
				crmDoctor.setId(null);
			}
			if (id == null || id.length() <= 0 || "NEW".equals(id)) {
				log = "123";
				code = systemCodeService.getCodeByPrefix("CrmDoctor", "Doc", 0000, 4,
						null);
				crmDoctor.setId(code);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("crmDoctorItem",getParameterFromRequest("crmDoctorItemJson"));
			
			
			crmDoctorService.save(crmDoctor,aMap,changeLog,lMap,changeLogItem,log);
			
			zId = crmDoctor.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "showCrmDoctorItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmDoctorItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = crmDoctorService
				.findCrmDoctorItemTable(start, length, query, col, sort,
						id);
		List<CrmDoctorItem> list=(List<CrmDoctorItem>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
			map.put("id", "");
			map.put("crmDoctor-name", "");
			map.put("crmDoctor-id", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("money", "");
			map.put("discount", "");
			map.put("price", "");
			map.put("way", "");
			map.put("note", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	@Action(value = "viewCrmDoctor")
	public String toViewCrmDoctor() throws Exception {
		String id = getParameterFromRequest("id");
		crmDoctor = crmDoctorService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctorEdit.jsp");
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delCrmDoctorItem")
	public void delCrmDoctorItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmDoctorService.delCrmDoctorItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "saveCrmDoctorItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCrmDoctorItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		crmDoctor=commonService.get(CrmDoctor.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("crmDoctorItem",
				getParameterFromRequest("dataJson"));
		lMap.put("crmDoctorItem",
				getParameterFromRequest("changeLog"));
		try {
			crmDoctorService.save(crmDoctor, aMap,"",lMap,"",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "saveCrmDoctorTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCrmDoctorTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		
		String str = "["+dataValue+"]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
		Map<String, Object> map=new HashMap<String, Object>();
		try {
		for (Map<String, Object> map1 : list) {
			CrmDoctor a = new CrmDoctor();
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (CrmDoctor)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			crmDoctorService.save(a,aMap,changeLog,lMap,changeLog,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

/*old*/
	@Action(value = "showCrmPatientList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmPatientList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/doctor/crmPatient.jsp");
	}
	@Action(value = "showCrmPatientListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmPatientListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmDoctorService.findCrmPatientList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmDoctor> list = (List<CrmDoctor>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("isMonthFee", "");
		map.put("crmCustomer-name", "");
		map.put("mail", "");
		map.put("mobile", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("ks-id", "");
		map.put("ks-name", "");
		map.put("jobTitle", "");
		map.put("payTreasure", "");
		map.put("doctorIntroduction", "");
		map.put("goodAtDisease", "");
		map.put("clinicCharacteristics", "");
		map.put("contactPerson", "");
		map.put("HeadOfSales", "");
		map.put("weichatId", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("otherInformation", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "crmPatientSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmPatientList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/doctor/crmPatientDialog.jsp");
	}

	@Action(value = "showDialogCrmPatientListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmPatientListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String crmCustomerId = getParameterFromRequest("crmCustomerId");
		Map<String, String> map2Query = new HashMap<String, String>();
		if(crmCustomerId!=null && !"".equals(crmCustomerId) &&!"null".equals(crmCustomerId)){
			map2Query.put("crmCustomer.id", crmCustomerId);
		}
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmDoctorService.findCrmPatientList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmDoctor> list = (List<CrmDoctor>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("mail", "");
		map.put("mobile", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("isMonthFee", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("ks-id", "");
		map.put("ks-name", "");
		map.put("jobTitle", "");
		map.put("payTreasure", "");
		map.put("doctorIntroduction", "");
		map.put("goodAtDisease", "");
		map.put("clinicCharacteristics", "");

		map.put("contactPerson", "");
		map.put("HeadOfSales", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("otherInformation", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	@Action(value="showDialogCrmPatientTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmPatientTable() throws Exception{
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/doctor/crmPatientDialogTable.jsp");
	}
	
	@Action(value = "showDialogCrmPatientTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmPatientTableJson() throws Exception {
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=crmDoctorService.showDialogCrmPatientTableJson(start,length,query,col,sort);
		List<CrmDoctor> list = (List<CrmDoctor>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("mobile", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	@Action(value = "editCrmPatient", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editCrmPatient() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			crmDoctor = crmDoctorService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			crmDoctor.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmDoctor.setCreateUser(user);
			crmDoctor.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/crm/doctor/crmPatientEdit.jsp");
	}

	@Action(value = "copyCrmPatient", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String copyCrmPatient() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmDoctor = crmDoctorService.get(id);
		crmDoctor.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/doctor/crmPatientEdit.jsp");
	}

/*	@Action(value = "save", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String save() throws Exception {
		String id = crmDoctor.getId();
		DicType type = crmDoctor.getType();
		String code = "";

		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
			// if ("DOCTOR".equals(type.getId())) {
			// code = systemCodeService.getCodeByPrefix("CrmDoctor", "DOCTOR",
			// 0000, 4, null);
			// } else if ("RSCHER".equals(type.getId())) {
			// code = systemCodeService.getCodeByPrefix("CrmDoctor", "RSCHER",
			// 0000, 4, null);
			// } else if ("CCNPOR".equals(type.getId())) {
			// code = systemCodeService.getCodeByPrefix("CrmDoctor", "CCNPOR",
			// 0000, 4, null);
			// }

			code = systemCodeService.getCodeByPrefix("CrmDoctor", "C", 0000, 4,
					null);

			crmDoctor.setId(code);
		}

		Map aMap = new HashMap();
		aMap.put("crmDoctorItem", getParameterFromRequest("crmDoctorItemJson"));
		crmDoctorService.save(crmDoctor, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/crm/doctor/editCrmPatient.action?id="
				+ crmDoctor.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}*/

	@Action(value = "viewCrmPatient", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toViewCrmPatient() throws Exception {
		String id = getParameterFromRequest("id");
		crmDoctor = crmDoctorService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/doctor/crmPatientEdit.jsp");
	}

	@Action(value = "delModelId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delModelId() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String objName = getParameterFromRequest("objName");
			String objProperty = getParameterFromRequest("objProperty");
			String vals = getParameterFromRequest("vals");
			crmDoctorService.delModelId(objName, objProperty, vals);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 个体销费记录管理
	@Action(value = "showCrmConsumerMarketList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmConsumerMarketList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/doctor/crmConsumerMarketList.jsp");
	}

	@Action(value = "showCrmConsumerMarketJson")
	public String showCrmConsumerMarketJson() throws Exception {
		String type = super.getRequest().getParameter("type");
		String outStr = "{results:"
				+ crmDoctorService.getCrmConsumerMarketJson(type) + "}";

		return renderText(outStr);
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmDoctorService getCrmDoctorService() {
		return crmDoctorService;
	}

	public void setCrmDoctorService(CrmDoctorService crmDoctorService) {
		this.crmDoctorService = crmDoctorService;
	}

	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

	@Action(value = "showCrmDoctorItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmDoctorItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/doctor/crmDoctorItem.jsp");
	}

	@Action(value = "showCrmDoctorItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmDoctorItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmDoctorService
					.findCrmDoctorItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmDoctorItem> list = (List<CrmDoctorItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("crmDoctor-name", "");
			map.put("crmDoctor-id", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("money", "");
			map.put("discount", "");
			map.put("price", "");
			map.put("note", "");
			map.put("state", "");
			map.put("way", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
/*	@Action(value = "delCrmDoctorItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delCrmDoctorItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmDoctorService.delCrmDoctorItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}*/

}
