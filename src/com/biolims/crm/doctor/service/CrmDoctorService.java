package com.biolims.crm.doctor.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.agent.advance.model.AdvanceTask;
import com.biolims.crm.agent.advance.model.AdvanceTaskItem;
import com.biolims.crm.agent.agreement.model.AgreementTaskItem;
import com.biolims.crm.customer.patient.model.CrmConsumerMarket;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.model.CrmPatientDiagnosis;
import com.biolims.crm.customer.patient.model.CrmPatientGeneticTesting;
import com.biolims.crm.customer.patient.model.CrmPatientLaboratory;
import com.biolims.crm.customer.patient.model.CrmPatientLinkMan;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.crm.customer.patient.model.CrmPatientSurgeries;
import com.biolims.crm.customer.patient.model.CrmPatientTreat;
import com.biolims.crm.customer.patient.model.CrmPatientTumorInformation;
import com.biolims.crm.doctor.dao.CrmDoctorDao;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.doctor.model.CrmDoctorItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmDoctorService {
	@Resource
	private CrmDoctorDao crmDoctorDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	public Map<String, Object> findCrmPatientList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		return crmDoctorDao.selectCrmPatientList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmPatient i) throws Exception {

		crmDoctorDao.saveOrUpdate(i);

	}

	public CrmDoctor get(String id) {
		CrmDoctor crmDoctor = commonDAO.get(CrmDoctor.class, id);
		return crmDoctor;
	}

	public Map<String, Object> findConsumerMarketList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmDoctorDao.selectConsumerMarketList(mapForQuery, startNum, limitNum, dir, sort);
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientDiagnosis(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientDiagnosis> saveItems = new ArrayList<CrmPatientDiagnosis>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientDiagnosis scp = new CrmPatientDiagnosis();
			// 将map信息读入实体类
			scp = (CrmPatientDiagnosis) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			scp.setNum("1");
			if (scp.getDateOfDiagnosis() != null && scp.getCrmPatient().getDateOfBirth() != null) {
				scp.setPatientAge(Integer.toString(scp.getDateOfDiagnosis().getYear()
						- scp.getCrmPatient().getDateOfBirth().getYear()));
			}
			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientSurgeries(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientSurgeries> saveItems = new ArrayList<CrmPatientSurgeries>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientSurgeries scp = new CrmPatientSurgeries();
			// 将map信息读入实体类
			scp = (CrmPatientSurgeries) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientTreat(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientTreat> saveItems = new ArrayList<CrmPatientTreat>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientTreat scp = new CrmPatientTreat();
			// 将map信息读入实体类
			scp = (CrmPatientTreat) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientRests(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientDiagnosis> saveItems = new ArrayList<CrmPatientDiagnosis>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientDiagnosis scp = new CrmPatientDiagnosis();
			// 将map信息读入实体类
			scp = (CrmPatientDiagnosis) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			scp.setNum("2");
			if (scp.getDateOfDiagnosis() != null && scp.getCrmPatient().getDateOfBirth() != null) {
				scp.setPatientAge(Integer.toString(scp.getDateOfDiagnosis().getYear()
						- scp.getCrmPatient().getDateOfBirth().getYear()));
			}
			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientTumorInformation(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientTumorInformation> saveItems = new ArrayList<CrmPatientTumorInformation>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientTumorInformation scp = new CrmPatientTumorInformation();
			// 将map信息读入实体类
			scp = (CrmPatientTumorInformation) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientRadiology(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientRadiology> saveItems = new ArrayList<CrmPatientRadiology>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientRadiology scp = new CrmPatientRadiology();
			// 将map信息读入实体类
			scp = (CrmPatientRadiology) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientLaboratory(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientLaboratory> saveItems = new ArrayList<CrmPatientLaboratory>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientLaboratory scp = new CrmPatientLaboratory();
			// 将map信息读入实体类
			scp = (CrmPatientLaboratory) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientPathology(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientPathology> saveItems = new ArrayList<CrmPatientPathology>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientPathology scp = new CrmPatientPathology();
			// 将map信息读入实体类
			scp = (CrmPatientPathology) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmPatientGeneticTesting(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientGeneticTesting> saveItems = new ArrayList<CrmPatientGeneticTesting>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientGeneticTesting scp = new CrmPatientGeneticTesting();
			// 将map信息读入实体类
			scp = (CrmPatientGeneticTesting) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmCustomerLinkMan(CrmPatient sc, String itemDataJson) throws Exception {
		List<CrmPatientLinkMan> saveItems = new ArrayList<CrmPatientLinkMan>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmPatientLinkMan scp = new CrmPatientLinkMan();
			// 将map信息读入实体类
			scp = (CrmPatientLinkMan) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmPatient(sc);
			crmDoctorDao.saveOrUpdate(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmDoctor sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmDoctorDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("crmDoctorItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmDoctorItem(sc, jsonStr);
			}
	}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmDoctorItem(CrmDoctor sc, String itemDataJson) throws Exception {
		List<CrmDoctorItem> saveItems = new ArrayList<CrmDoctorItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmDoctorItem scp = new CrmDoctorItem();
			// 将map信息读入实体类
			scp = (CrmDoctorItem)crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmDoctor(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
		// 设置保留三维小数
		DecimalFormat df = new DecimalFormat("######.00");

		List<CrmDoctorItem> list2 = this.crmDoctorDao.setItemList(sc
				.getId());
		for (CrmDoctorItem q : list2) {
			if("".equals(q.getPrice()) ){
				if( q.getMoney()!=null && !"".equals(q.getMoney()) 
						&& q.getDiscount()!=null && !"".equals(q.getDiscount())){
					Double b = Double.parseDouble(q.getMoney()) * Double.parseDouble(q.getDiscount()) / 100;
					q.setPrice(df.format(b).toString());
				}
				
			}
		}
}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delModelId(String objName, String objProperty, String vals) throws Exception {
		crmDoctorDao.delModelId(objName, objProperty, vals);
	}

	public String getCrmConsumerMarketJson(String type) throws Exception {
		List<CrmConsumerMarket> list = crmDoctorDao.findCrmConsumerMarket(type);
		return JsonUtils.toJsonString(list);
	}
	
	
	public Map<String, Object> findCrmDoctorItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmDoctorDao.selectCrmDoctorItemList(scId, startNum, limitNum, dir, sort);
		List<CrmDoctorItem> list = (List<CrmDoctorItem>) result.get("list");
		return result;
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmDoctorItem(String[] ids) throws Exception {
		for (String id : ids) {
			CrmDoctorItem scp =  crmDoctorDao.get(CrmDoctorItem.class, id);
			commonDAO.delete(scp);
		}
	}

	public Map<String, Object> showDialogCrmPatientTableJson(Integer start,
			Integer length, String query, String col, String sort) {
		return crmDoctorDao.showDialogCrmPatientTableJson(start,length,query,col,sort);
	}


	public Map<String, Object> findCrmDoctorTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return crmDoctorDao.findCrmDoctorTable(start, length, query, col, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmDoctorItem(CrmDoctor sc, String itemDataJson,String logInfo,String changeLogItem,String log) throws Exception {
		List<CrmDoctorItem> saveItems = new ArrayList<CrmDoctorItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmDoctorItem scp = new CrmDoctorItem();
			// 将map信息读入实体类
			scp = (CrmDoctorItem) crmDoctorDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmDoctor(sc);

			saveItems.add(scp);
		}
		crmDoctorDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CrmDoctor");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmDoctor sc, Map jsonMap,String logInfo, Map logMap,String changeLogItem,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			crmDoctorDao.saveOrUpdate(sc);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("CrmDoctor");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			String logStr = "";
           	logStr =  (String)logMap.get("crmDoctorItem");
			jsonStr = (String)jsonMap.get("crmDoctorItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmDoctorItem(sc, jsonStr,logStr,changeLogItem,log);
			}
	}
   }

	public Map<String, Object> findCrmDoctorItemTable(Integer start,
			Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = crmDoctorDao.findCrmDoctorItemTable(start, length, query,
				col, sort, id);
		List<CrmDoctorItem> list = (List<CrmDoctorItem>) result.get("list");
		return result;
	}
	
	
}
