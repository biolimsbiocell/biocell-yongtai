package com.biolims.crm.sale.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.crm.sale.service.CrmSaleBillService;

public class CrmSaleBillEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CrmSaleBillService csbService = (CrmSaleBillService) ctx.getBean("crmSaleBillService");
		csbService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
