package com.biolims.crm.sale.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.crm.sale.service.CrmSaleChanceService;

public class CrmSaleChanceEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CrmSaleChanceService cscService = (CrmSaleChanceService) ctx.getBean("crmSaleChanceService");
		cscService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
