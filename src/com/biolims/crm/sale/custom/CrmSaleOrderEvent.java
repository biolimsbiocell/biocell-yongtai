package com.biolims.crm.sale.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.crm.sale.service.CrmSaleOrderService;

public class CrmSaleOrderEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CrmSaleOrderService csoService = (CrmSaleOrderService) ctx.getBean("crmSaleOrderService");
		csoService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
