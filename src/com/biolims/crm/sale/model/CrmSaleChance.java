package com.biolims.crm.sale.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 项目评估申请
 * @author lims-platform
 * @date 2015-08-03 11:27:11
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_SALE_CHANCE")
@SuppressWarnings("serial")
public class CrmSaleChance extends EntityDao<CrmSaleChance> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 描述 */
	private String name;
	/** 项目评估负责人 */
	private User changeUser;
	/** 打靶方案 */
	private DicType dbMethod;
	/** 基因名称 */
	private String geneName;
	/** 基因ID */
	private String geneId;
	/** 项目联系人 */
	private CrmLinkMan linkMan;
	/** 项目来源 */
	private DicType chanceSource;
	/** 分类 */
	private DicType type;
	/** 地区 */
	private DicType area;
	/** 邮件 */
	private String email;
	/** 电话 */
	private String phone;
	/** 客户经理 */
	private User manager;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 期望结束日期 */
	private Date endDate;
	/** 工作流状态 */
	private String state;
	/** 工作流状态名称 */
	private String stateName;
	/** 商机内容 */
	private String content;
	/** 是否已创建 */
	private String changeStateName;
	/** 评估类型 */
	private DicType evalustionType;
	/** 业务类型 */
	private DicType bussinessType;
	/** 产品类型 */
	private DicType productType;
	/** 种属 */
	private DicType genus;
	/** 品系 */
	private DicType strain;
	/** 技术类型 */
	private DicType technologyType;
	/** 预计评估完成时间 */
	private Date preFinishDate;
	/** 评估负责人 */
	private User evalustionUser;
	/** 评估成单可能性 */
	private String successChance;
	/** 跟踪人员 */
	private User followUser;
	/** 跟踪日期 */
	private Date followDate;
	/** 跟踪结果 */
	private DicType followResult;
	/** 结果说明 */
	private String followExplain;
	/** 商机跟踪成单可能性 */
	private String orderChance;
//	/** 项目 */
//	private Project project;
	/** content1 */
	private String content1;
	/** content2 */
	private String content2;
	/** content3 */
	private String content3;
	/** content4 */
	private String content4;
	/** content5 */
	private Double content5;
	/** content6 */
	private Double content6;
	/** content7 */
	private Double content7;
	/** content8 */
	private Double content8;
	/** content9 */
	private Date content9;
	/** content10 */
	private Date content10;
	/** content11 */
	private Date content11;
	/** content12 */
	private Date content12;

	@Column(name = "GENENAME", length = 120)
	public String getGeneName() {
		return geneName;
	}

	public void setGeneName(String geneName) {
		this.geneName = geneName;
	}

	@Column(name = "GENEID", length = 120)
	public String getGeneId() {
		return geneId;
	}

	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}

	/**
	 * 项目评估负责人
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHANGE_USER")
	public User getChangeUser() {
		return changeUser;
	}

	/**
	 * 项目评估负责人
	 * 
	 * @return
	 */
	public void setChangeUser(User changeUser) {
		this.changeUser = changeUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "linkMan")
	public CrmLinkMan getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(CrmLinkMan linkMan) {
		this.linkMan = linkMan;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DBMETHOD")
	public DicType getDbMethod() {
		return dbMethod;
	}

	public void setDbMethod(DicType dbMethod) {
		this.dbMethod = dbMethod;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 是否已生成
	 * 
	 * @return
	 */
	@Column(name = "CHANGE_STATE_NAME", length = 36)
	public String getChangeStateName() {
		return changeStateName;
	}

	/**
	 * 是否已生成
	 * 
	 * @return
	 */
	public void setChangeStateName(String changeStateName) {
		this.changeStateName = changeStateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHANCE_SOURCE")
	/**
	 * @return the chanceSource
	 */
	public DicType getChanceSource() {
		return this.chanceSource;
	}

	/**
	 * @param chanceSource
	 *            the chanceSource to set
	 */
	public void setChanceSource(DicType chanceSource) {
		this.chanceSource = chanceSource;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 分类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 分类
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 地区
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AREA")
	public DicType getArea() {
		return this.area;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 地区
	 */
	public void setArea(DicType area) {
		this.area = area;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 邮件
	 */
	@Column(name = "EMAIL", length = 60)
	public String getEmail() {
		return this.email;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 邮件
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 电话
	 */
	@Column(name = "PHONE", length = 40)
	public String getPhone() {
		return this.phone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 客户经理
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MANAGER")
	public User getManager() {
		return this.manager;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 客户经理
	 */
	public void setManager(User manager) {
		this.manager = manager;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 期望结束日期
	 * 
	 * @return
	 */
	@Column(name = "END_DATE", length = 50)
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * 期望结束日期
	 * 
	 * @return
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态名称
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 商机内容
	 */
	@Column(name = "CONTENT", length = 400)
	public String getContent() {
		return this.content;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 商机内容
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 跟踪人员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FOLLOW_USER")
	public User getFollowUser() {
		return this.followUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 跟踪人员
	 */
	public void setFollowUser(User followUser) {
		this.followUser = followUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 跟踪日期
	 */
	@Column(name = "FOLLOW_DATE", length = 255)
	public Date getFollowDate() {
		return this.followDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 跟踪日期
	 */
	public void setFollowDate(Date followDate) {
		this.followDate = followDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 跟踪结果
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FOLLOW_RESULT")
	public DicType getFollowResult() {
		return this.followResult;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 跟踪结果
	 */
	public void setFollowResult(DicType followResult) {
		this.followResult = followResult;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果说明
	 */
	@Column(name = "FOLLOW_EXPLAIN", length = 500)
	public String getFollowExplain() {
		return this.followExplain;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果说明
	 */
	public void setFollowExplain(String followExplain) {
		this.followExplain = followExplain;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 成单可能性
	 */
	@Column(name = "ORDER_CHANCE", length = 10)
	public String getOrderChance() {
		return this.orderChance;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 成单可能性
	 */
	public void setOrderChance(String orderChance) {
		this.orderChance = orderChance;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content1
	 */
	@Column(name = "CONTENT1", length = 50)
	public String getContent1() {
		return this.content1;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content1
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content2
	 */
	@Column(name = "CONTENT2", length = 50)
	public String getContent2() {
		return this.content2;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content2
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content3
	 */
	@Column(name = "CONTENT3", length = 50)
	public String getContent3() {
		return this.content3;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content4
	 */
	@Column(name = "CONTENT4", length = 50)
	public String getContent4() {
		return this.content4;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content5
	 */
	@Column(name = "CONTENT5", length = 50)
	public Double getContent5() {
		return this.content5;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content5
	 */
	public void setContent5(Double content5) {
		this.content5 = content5;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content6
	 */
	@Column(name = "CONTENT6", length = 50)
	public Double getContent6() {
		return this.content6;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content6
	 */
	public void setContent6(Double content6) {
		this.content6 = content6;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content7
	 */
	@Column(name = "CONTENT7", length = 50)
	public Double getContent7() {
		return this.content7;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content7
	 */
	public void setContent7(Double content7) {
		this.content7 = content7;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content8
	 */
	@Column(name = "CONTENT8", length = 50)
	public Double getContent8() {
		return this.content8;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content8
	 */
	public void setContent8(Double content8) {
		this.content8 = content8;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content9
	 */
	@Column(name = "CONTENT9", length = 255)
	public Date getContent9() {
		return this.content9;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content9
	 */
	public void setContent9(Date content9) {
		this.content9 = content9;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content10
	 */
	@Column(name = "CONTENT10", length = 255)
	public Date getContent10() {
		return this.content10;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content10
	 */
	public void setContent10(Date content10) {
		this.content10 = content10;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content11
	 */
	@Column(name = "CONTENT11", length = 255)
	public Date getContent11() {
		return this.content11;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content11
	 */
	public void setContent11(Date content11) {
		this.content11 = content11;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content12
	 */
	@Column(name = "CONTENT12", length = 255)
	public Date getContent12() {
		return this.content12;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content12
	 */
	public void setContent12(Date content12) {
		this.content12 = content12;
	}

	/**
	 * 方法: 取得Dictype
	 * 
	 * @return: Dictype 评估类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EVALUSTION_TYPE")
	public DicType getEvalustionType() {
		return this.evalustionType;
	}

	/**
	 * 方法: 设置Dictype
	 * 
	 * @param: Dictype 评估类型
	 */
	public void setEvalustionType(DicType evalustionType) {
		this.evalustionType = evalustionType;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 业务类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSSINESS_TYPE")
	public DicType getBussinessType() {
		return this.bussinessType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 业务类型
	 */
	public void setBussinessType(DicType bussinessType) {
		this.bussinessType = bussinessType;
	}

	/**
	 * 品系
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STRAIN")
	public DicType getStrain() {
		return strain;
	}

	/**
	 * 品系
	 * 
	 * @return
	 */
	public void setStrain(DicType strain) {
		this.strain = strain;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 产品类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT_TYPE")
	public DicType getProductType() {
		return this.productType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 产品类型
	 */
	public void setProductType(DicType productType) {
		this.productType = productType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 种属
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENUS")
	public DicType getGenus() {
		return genus;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 种属
	 */
	public void setGenus(DicType genus) {
		this.genus = genus;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 技术类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECHNOLOGY_TYPE")
	public DicType getTechnologyType() {
		return this.technologyType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 技术类型
	 */
	public void setTechnologyType(DicType technologyType) {
		this.technologyType = technologyType;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 预计评估完成时间
	 */
	@Column(name = "PRE_FINISH_DATE", length = 255)
	public Date getPreFinishDate() {
		return this.preFinishDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 预计评估完成时间
	 */
	public void setPreFinishDate(Date preFinishDate) {
		this.preFinishDate = preFinishDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 客户经理
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EVALUSTION_USER")
	public User getEvalustionUser() {
		return this.evalustionUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 客户经理
	 */
	public void setEvalustionUser(User evalustionUser) {
		this.evalustionUser = evalustionUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 成单可能性
	 */
	@Column(name = "SUCCESS_CHANCE", length = 10)
	public String getSuccessChance() {
		return this.successChance;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 成单可能性
	 */
	public void setSuccessChance(String successChance) {
		this.successChance = successChance;
	}

//	/**
//	 * 方法: 取得Project
//	 * 
//	 * @return: Project 项目
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT")
//	public Project getProject() {
//		return project;
//	}
//
//	/**
//	 * 方法: 设置Project
//	 * 
//	 * @param: Project 项目
//	 */
//	public void setProject(Project project) {
//		this.project = project;
//	}

}