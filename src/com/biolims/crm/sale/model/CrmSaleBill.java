package com.biolims.crm.sale.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.contract.model.CrmContract;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 发票管理
 * @author lims-platform
 * @date 2015-08-03 11:27:20
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_SALE_BILL")
@SuppressWarnings("serial")
public class CrmSaleBill extends EntityDao<CrmSaleBill> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**所属合同*/
	private CrmContract crmContract;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**审核人*/
	private User confirmUser;
	/**审核日期*/
	private Date confirmDate;
	/**工作流状态*/
	private String state;
	/**工作流状态名称*/
	private String stateName;
	/**content1*/
	private String content1;
	/**content2*/
	private String content2;
	/**content3*/
	private String content3;
	/**content4*/
	private String content4;
	/**content5*/
	private Double content5;
	/**content6*/
	private Double content6;
	/**content7*/
	private Double content7;
	/**content8*/
	private Double content8;
	/**content9*/
	private Date content9;
	/**content10*/
	private Date content10;
	/**content11*/
	private Date content11;
	/**content12*/
	private Date content12;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *方法: 取得CrmContract
	 *@return: CrmContract  所属合同
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 *方法: 设置CrmContract
	 *@param: CrmContract  所属合同
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  审核日期
	 */
	@Column(name = "CONFIRM_DATE", length = 255)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  审核日期
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 *方法: 取得String
	 *@return: String  工作流状态名称
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 *方法: 设置String
	 *@param: String  工作流状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 *方法: 取得String
	 *@return: String  content1
	 */
	@Column(name = "CONTENT1", length = 50)
	public String getContent1() {
		return this.content1;
	}

	/**
	 *方法: 设置String
	 *@param: String  content1
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 *方法: 取得String
	 *@return: String  content2
	 */
	@Column(name = "CONTENT2", length = 50)
	public String getContent2() {
		return this.content2;
	}

	/**
	 *方法: 设置String
	 *@param: String  content2
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 *方法: 取得String
	 *@return: String  content3
	 */
	@Column(name = "CONTENT3", length = 50)
	public String getContent3() {
		return this.content3;
	}

	/**
	 *方法: 设置String
	 *@param: String  content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 *方法: 取得String
	 *@return: String  content4
	 */
	@Column(name = "CONTENT4", length = 50)
	public String getContent4() {
		return this.content4;
	}

	/**
	 *方法: 设置String
	 *@param: String  content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content5
	 */
	@Column(name = "CONTENT5", length = 50)
	public Double getContent5() {
		return this.content5;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content5
	 */
	public void setContent5(Double content5) {
		this.content5 = content5;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content6
	 */
	@Column(name = "CONTENT6", length = 50)
	public Double getContent6() {
		return this.content6;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content6
	 */
	public void setContent6(Double content6) {
		this.content6 = content6;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content7
	 */
	@Column(name = "CONTENT7", length = 50)
	public Double getContent7() {
		return this.content7;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content7
	 */
	public void setContent7(Double content7) {
		this.content7 = content7;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content8
	 */
	@Column(name = "CONTENT8", length = 50)
	public Double getContent8() {
		return this.content8;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content8
	 */
	public void setContent8(Double content8) {
		this.content8 = content8;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content9
	 */
	@Column(name = "CONTENT9", length = 255)
	public Date getContent9() {
		return this.content9;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content9
	 */
	public void setContent9(Date content9) {
		this.content9 = content9;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content10
	 */
	@Column(name = "CONTENT10", length = 255)
	public Date getContent10() {
		return this.content10;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content10
	 */
	public void setContent10(Date content10) {
		this.content10 = content10;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content11
	 */
	@Column(name = "CONTENT11", length = 255)
	public Date getContent11() {
		return this.content11;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content11
	 */
	public void setContent11(Date content11) {
		this.content11 = content11;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content12
	 */
	@Column(name = "CONTENT12", length = 255)
	public Date getContent12() {
		return this.content12;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content12
	 */
	public void setContent12(Date content12) {
		this.content12 = content12;
	}
}