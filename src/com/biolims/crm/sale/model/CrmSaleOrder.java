package com.biolims.crm.sale.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.contract.model.CrmContract;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 订单管理
 * @author lims-platform
 * @date 2015-08-06 13:59:03
 * @version V1.0   
 *
 */
/**
 * @author Administrator
 *
 */
@Entity
@Table(name = "CRM_SALE_ORDER")
@SuppressWarnings("serial")
public class CrmSaleOrder extends EntityDao<CrmSaleOrder> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**是否创建后续工作*/
	private String orderStateName;
	/**是否去Neo*/
	private String neoResult;
	/**Southern blot鉴定*/
	private DicType checkup;
	/**终产品数量*/
	private String productNo;
	/**来源*/
	private DicType orderSource;
	/**委托单位*/
	private String client;
//	/**促销计划*/
//	private CrmMarcketSale crmMarcketSale;
//	/**市场计划*/
//	private CrmMarcketActivity crmMarcketActivity;
//	/**初步计划*/
//	private ProjectStart projectStart;
	/**客户要求*/
	private String customerRequirements;
	/**技术类型*/
	private DicType technologyType;
	/**委托人*/
	private CrmCustomer crmCustomer;
	/**金额*/
	private Double money;
	/**产品类型*/
	private DicType type;
	/**业务类型*/
	private DicType workType;
	/**种属*/
	private DicType genus;
	/**品系*/
	private DicType strain;
	/**基因打靶类型*/
	private DicType geneTargetingType;
	/**基因名称*/
	private String geneName;
	/**基因ID(NCBI)*/
	private String geneId;
	/**基因别名*/
	private String geneByName;
	/**项目负责人*/
	private User boss;
	/**终产品*/
	private DicType product;
	/**商机*/
	private CrmSaleChance crmSaleChance;
	/**客户经理*/
	private User manager;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**合同开始日期 */
	@Transient
	private Date startDate;
	/**合同结束日期*/
	@Transient
	private Date endDate;
	/**父级订单*/
	private CrmSaleOrder parent;
	/**销售合同*/
	private CrmContract crmContract;
	/**审核人*/
	private User confirmUser;
	/**审核日期*/
	private Date confirmDate;
	/**工作流状态*/
	private String state;
	/**工作流状态名称*/
	private String stateName;
	/**课题要求*/
	private String projectAsk;
//	/**项目*/
//	private Project project;
	/**下单日期*/
	private Date genProjectDate;
	/**备注*/
	private String note;
	/**委托单位*/
	private String content1;
	/**content2*/
	private String content2;
	/**content3*/
	private String content3;
	/**content4*/
	private String content4;
	/**content5*/
	private Double content5;
	/**content6*/
	private Double content6;
	/**content7*/
	private Double content7;
	/**content8*/
	private Double content8;
	/**content9*/
	private Date content9;
	/**content10*/
	private Date content10;
	/**content11*/
	private Date content11;
	/**代养目标*/
	private String generationTarget ;
	/**
	 *方法: 取得String 
	 *@return: String   编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String 
	 *@param: String   编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String 
	 *@return: String   描述
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 *方法: 设置String 
	 *@param: String   描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "CLIENT", length = 120)
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	/**
	 * 基因名称
	 * @return
	 */
	@Column(name = "GENE_NAME", length = 120)
	public String getGeneName() {
		return geneName;
	}

	/**
	 * 基因名称
	 * @return
	 */
	public void setGeneName(String geneName) {
		this.geneName = geneName;
	}

	/**
	 * 基因ID 
	 * @return
	 */
	@Column(name = "GENE_ID", length = 120)
	public String getGeneId() {
		return geneId;
	}

	/**
	 * 基因ID 
	 * @return
	 */
	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}

	/**
	 * 基因别名
	 * @return
	 */
	@Column(name = "GENE_BY_NAME", length = 120)
	public String getGeneByName() {
		return geneByName;
	}

	/**
	 * 基因别名
	 * @return
	 */
	public void setGeneByName(String geneByName) {
		this.geneByName = geneByName;
	}

	/**
	 *方法: 取得String 
	 *@return: String   是否创建后续工作
	 */
	@Column(name = "ORDER_STATE_NAME", length = 10)
	public String getOrderStateName() {
		return orderStateName;
	}

	/**
	 *方法: 取得String 
	 *@return: String   是否创建后续工作
	 */
	public void setOrderStateName(String orderStateName) {
		this.orderStateName = orderStateName;
	}

	/**
	 * 是否去neo
	 * @return
	 */
	@Column(name = "NEO_RESULT", length = 10)
	public String getNeoResult() {
		return neoResult;
	}

	/**
	 * 是否去neo
	 * @return
	 */
	public void setNeoResult(String neoResult) {
		this.neoResult = neoResult;
	}

	/**
	 * southrn blot鉴定
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHECK_UP")
	public DicType getCheckup() {
		return checkup;
	}

	/**
	 * southrn blot鉴定
	 * @return
	 */
	public void setCheckup(DicType checkup) {
		this.checkup = checkup;
	}

	/**
	 * 终产品数量
	 * @return
	 */
	@Column(name = "PRODUCT_NO", length = 100)
	public String getProductNo() {
		return productNo;
	}

	/**
	 * 终产品数量
	 * @return
	 */
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ORDER_SOURCE")
	public DicType getOrderSource() {
		return this.orderSource;
	}

	/**
	 * @param orderSource the orderSource to set
	 */
	public void setOrderSource(DicType orderSource) {
		this.orderSource = orderSource;
	}

//	/**
//	 *方法: 取得CrmMarcketSale
//	 *@return: CrmMarcketSale  促销计划
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CRM_MARCKET_SALE")
//	public CrmMarcketSale getCrmMarcketSale() {
//		return this.crmMarcketSale;
//	}
//
//	/**
//	 *方法: 设置CrmMarcketSale
//	 *@param: CrmMarcketSale  促销计划
//	 */
//	public void setCrmMarcketSale(CrmMarcketSale crmMarcketSale) {
//		this.crmMarcketSale = crmMarcketSale;
//	}

	/**
	 *方法: 取得CrmCustomer
	 *@return: CrmCustomer  客户
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return this.crmCustomer;
	}

	/**
	 *方法: 设置CrmCustomer
	 *@param: CrmCustomer  客户
	 */
	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  金额
	 */
	@Column(name = "MONEY", length = 20)
	public Double getMoney() {
		return this.money;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  金额
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  产品类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  产品类型
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 *方法: 取得CrmSaleChance
	 *@return: CrmSaleChance  商机
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_SALE_CHANCE")
	public CrmSaleChance getCrmSaleChance() {
		return this.crmSaleChance;
	}

	/**
	 *方法: 设置CrmSaleChance
	 *@param: CrmSaleChance  商机
	 */
	public void setCrmSaleChance(CrmSaleChance crmSaleChance) {
		this.crmSaleChance = crmSaleChance;
	}

	/**
	 *方法: 取得User
	 *@return: User  客户经理
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MANAGER")
	public User getManager() {
		return this.manager;
	}

	/**
	 *方法: 设置User
	 *@param: User  客户经理
	 */
	public void setManager(User manager) {
		this.manager = manager;
	}

	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 合同开始日期
	 * @return
	 */
	@Column(name = "START_DATE", length = 50)
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * 合同开始日期
	 * @return
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * 合同结束日期
	 * @return
	 */
	@Column(name = "END_DATE", length = 50)
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * 合同结束日期
	 * @return
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 *方法: 取得CrmSaleOrder
	 *@return: CrmSaleOrder  父级订单
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PARENT")
	public CrmSaleOrder getParent() {
		return this.parent;
	}

	/**
	 *方法: 设置CrmSaleOrder
	 *@param: CrmSaleOrder  父级订单
	 */
	public void setParent(CrmSaleOrder parent) {
		this.parent = parent;
	}

	/**
	 *方法: 取得CrmContract
	 *@return: CrmContract  销售合同
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 *方法: 设置CrmContract
	 *@param: CrmContract  销售合同
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  审核日期
	 */
	@Column(name = "CONFIRM_DATE", length = 255)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  审核日期
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT_START")
//	public ProjectStart getProjectStart() {
//		return projectStart;
//	}
//
//	public void setProjectStart(ProjectStart projectStart) {
//		this.projectStart = projectStart;
//	}

	@Column(name = "CUSTOMER_REQUIREMENTS", length = 500)
	public String getCustomerRequirements() {
		return customerRequirements;
	}

	public void setCustomerRequirements(String customerRequirements) {
		this.customerRequirements = customerRequirements;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECHNOLOGY_TYPE")
	public DicType getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(DicType technologyType) {
		this.technologyType = technologyType;
	}

	/**
	 *方法: 取得String 
	 *@return: String   工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 *方法: 设置String 
	 *@param: String   工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 *方法: 取得String 
	 *@return: String   工作流状态名称
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 *方法: 设置String 
	 *@param: String   工作流状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 *方法: 取得String 
	 *@return: String   课题要求
	 */
	@Column(name = "PROJECT_ASK", length = 400)
	public String getProjectAsk() {
		return this.projectAsk;
	}

	/**
	 *方法: 设置String 
	 *@param: String   课题要求
	 */
	public void setProjectAsk(String projectAsk) {
		this.projectAsk = projectAsk;
	}

	/**
	 *方法: 取得String 
	 *@return: String   content1
	 */
	@Column(name = "CONTENT1", length = 50)
	public String getContent1() {
		return this.content1;
	}

	/**
	 *方法: 设置String 
	 *@param: String   content1
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 *方法: 取得String 
	 *@return: String   content2
	 */
	@Column(name = "CONTENT2", length = 50)
	public String getContent2() {
		return this.content2;
	}

	/**
	 *方法: 设置String 
	 *@param: String   content2
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 *方法: 取得String 
	 *@return: String   content3
	 */
	@Column(name = "CONTENT3", length = 50)
	public String getContent3() {
		return this.content3;
	}

	/**
	 *方法: 设置String 
	 *@param: String   content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 *方法: 取得String 
	 *@return: String   content4
	 */
	@Column(name = "CONTENT4", length = 50)
	public String getContent4() {
		return this.content4;
	}

	/**
	 *方法: 设置String 
	 *@param: String   content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content5
	 */
	@Column(name = "CONTENT5", length = 50)
	public Double getContent5() {
		return this.content5;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content5
	 */
	public void setContent5(Double content5) {
		this.content5 = content5;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content6
	 */
	@Column(name = "CONTENT6", length = 50)
	public Double getContent6() {
		return this.content6;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content6
	 */
	public void setContent6(Double content6) {
		this.content6 = content6;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content7
	 */
	@Column(name = "CONTENT7", length = 50)
	public Double getContent7() {
		return this.content7;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content7
	 */
	public void setContent7(Double content7) {
		this.content7 = content7;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content8
	 */
	@Column(name = "CONTENT8", length = 50)
	public Double getContent8() {
		return this.content8;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content8
	 */
	public void setContent8(Double content8) {
		this.content8 = content8;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content9
	 */
	@Column(name = "CONTENT9", length = 255)
	public Date getContent9() {
		return this.content9;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content9
	 */
	public void setContent9(Date content9) {
		this.content9 = content9;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content10
	 */
	@Column(name = "CONTENT10", length = 255)
	public Date getContent10() {
		return this.content10;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content10
	 */
	public void setContent10(Date content10) {
		this.content10 = content10;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content11
	 */
	@Column(name = "CONTENT11", length = 255)
	public Date getContent11() {
		return this.content11;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content11
	 */
	public void setContent11(Date content11) {
		this.content11 = content11;
	}

	/**
	 *方法: 取得下单日期
	 *@return: Date  genProjectDate
	 */
	@Column(name = "GEN_PROJECT_DATE", length = 255)
	public Date getGenProjectDate() {
		return this.genProjectDate;
	}

	/**
	 *方法: 设置下单日期
	 *@param: Date  genProjectDate
	 */
	public void setGenProjectDate(Date genProjectDate) {
		this.genProjectDate = genProjectDate;
	}

//	/**
//	 *方法: 取得Project
//	 *@return: Project  项目
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT")
//	public Project getProject() {
//		return project;
//	}
//
//	/**
//	 *方法: 设置Project
//	 *@param: Project  项目
//	 */
//	public void setProject(Project project) {
//		this.project = project;
//	}

	/**
	 *方法: 取得种属
	 *@return: DicType  种属
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENUS")
	public DicType getGenus() {
		return genus;
	}

	/**
	 *方法: 设置种属
	 *@param: DicType  种属
	 */
	public void setGenus(DicType genus) {
		this.genus = genus;
	}

	/**
	 * 品系
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STRAIN")
	public DicType getStrain() {
		return strain;
	}

	/**
	 * 品系
	 * @return
	 */
	public void setStrain(DicType strain) {
		this.strain = strain;
	}

	/**
	 *方法: 取得基因打靶类型
	 *@return: DicType  基因打靶类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENE_TARGETING_TYPE")
	public DicType getGeneTargetingType() {
		return geneTargetingType;
	}

	/**
	 *方法: 设置基因打靶类型
	 *@param: DicType  基因打靶类型
	 */
	public void setGeneTargetingType(DicType geneTargetingType) {
		this.geneTargetingType = geneTargetingType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_TYPE")
	public DicType getWorkType() {
		return workType;
	}

	public void setWorkType(DicType workType) {
		this.workType = workType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BOSS")
	public User getBoss() {
		return boss;
	}

	public void setBoss(User boss) {
		this.boss = boss;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public DicType getProduct() {
		return product;
	}

	public void setProduct(DicType product) {
		this.product = product;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CRM_MARCKET_ACTIVITY")
//	public CrmMarcketActivity getCrmMarcketActivity() {
//		return crmMarcketActivity;
//	}
//
//	public void setCrmMarcketActivity(CrmMarcketActivity crmMarcketActivity) {
//		this.crmMarcketActivity = crmMarcketActivity;
//	}

	/**
	 *方法: 取得备注
	 *@return: String 备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return note;
	}

	/**
	 *方法: 设置备注
	 *@param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  代养目标
	 */
	@Column(name = "GENERATION_TARGET", length = 200)
	public String getGenerationTarget() {
		return generationTarget;
	}
	
	/**
	 *方法: 设置String
	 *@return: String  代养目标
	 */
	public void setGenerationTarget(String generationTarget) {
		this.generationTarget = generationTarget;
	}
	
}