package com.biolims.crm.sale.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicState;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;

/**   
 * @Title: Model
 * @Description: 商机管理
 * @author lims-platform
 * @date 2016-02-02 14:53:29
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_BUSINESS_MANAGE")
@SuppressWarnings("serial")
public class CrmBusinessManage extends EntityDao<CrmBusinessManage> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**商机来源*/
	private String name;
	/**客户经理*/
	private DicType type;
	/**状态*/
	private DicState state;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**备注*/
	private String note;
	/**岗位类型*/
	private DicType postType;
	/**联系人姓名*/
	private CrmLinkMan linkmanNname;
	/**联系人手机*/
	private String linkmanMobile;
	/**固定电话*/
	private String linkmanPhone;
	/**email*/
	private String email;
	/**单位名称*/
	private String unitName;
	/**单位地址*/
	private String unitAddress;
	/**所属委托人*/
	private User belongConsignor;
	/**研究方向*/
	private String resDirection;
	/**部门/学科*/
	private String dept;
	/**行业类型*/
	private DicType industryType;
	/**国家*/
	private String country;
	/**省*/
	private String privince;
	/**市*/
	private String city;
	/**content1*/
	private String content1;
	/**content2*/
	private String content2;
	/**content3*/
	private String content3;
	/**content4*/
	private String content4;
	/**content5*/
	private Date content5;
	/**content6*/
	private Date content6;
	/**content7*/
	private Date content7;
	/**content8*/
	private Date content8;
	/**content9*/
	private Double content9;
	/**content10*/
	private Double content10;
	/**content11*/
	private Double content11;
	/**content12*/
	private Double content12;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  商机来源
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 *方法: 设置String
	 *@param: String  商机来源
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  客户经理
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  客户经理
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 *方法: 取得DicState
	 *@return: DicState  状态
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STATE")
	public DicState getState() {
		return this.state;
	}

	/**
	 *方法: 设置DicState
	 *@param: DicState  状态
	 */
	public void setState(DicState state) {
		this.state = state;
	}

	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return this.note;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  岗位类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "POST_TYPE")
	public DicType getPostType() {
		return this.postType;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  岗位类型
	 */
	public void setPostType(DicType postType) {
		this.postType = postType;
	}

	/**
	 *方法: 取得User
	 *@return: User  联系人姓名
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "LINKMAN_NNAME")
	public CrmLinkMan getLinkmanNname() {
		return this.linkmanNname;
	}

	/**
	 *方法: 设置User
	 *@param: User  联系人姓名
	 */
	public void setLinkmanNname(CrmLinkMan linkmanNname) {
		this.linkmanNname = linkmanNname;
	}

	/**
	 *方法: 取得String
	 *@return: String  联系人手机
	 */
	@Column(name = "LINKMAN_MOBILE", length = 20)
	public String getLinkmanMobile() {
		return this.linkmanMobile;
	}

	/**
	 *方法: 设置String
	 *@param: String  联系人手机
	 */
	public void setLinkmanMobile(String linkmanMobile) {
		this.linkmanMobile = linkmanMobile;
	}

	/**
	 *方法: 取得String
	 *@return: String  固定电话
	 */
	@Column(name = "LINKMAN_PHONE", length = 20)
	public String getLinkmanPhone() {
		return this.linkmanPhone;
	}

	/**
	 *方法: 设置String
	 *@param: String  固定电话
	 */
	public void setLinkmanPhone(String linkmanPhone) {
		this.linkmanPhone = linkmanPhone;
	}

	/**
	 *方法: 取得String
	 *@return: String  email
	 */
	@Column(name = "EMAIL", length = 120)
	public String getEmail() {
		return this.email;
	}

	/**
	 *方法: 设置String
	 *@param: String  email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 *方法: 取得String
	 *@return: String  单位名称
	 */
	@Column(name = "UNIT_NAME", length = 100)
	public String getUnitName() {
		return this.unitName;
	}

	/**
	 *方法: 设置String
	 *@param: String  单位名称
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	/**
	 *方法: 取得String
	 *@return: String  单位地址
	 */
	@Column(name = "UNIT_ADDRESS", length = 120)
	public String getUnitAddress() {
		return this.unitAddress;
	}

	/**
	 *方法: 设置String
	 *@param: String  单位地址
	 */
	public void setUnitAddress(String unitAddress) {
		this.unitAddress = unitAddress;
	}

	/**
	 *方法: 取得User
	 *@return: User  所属委托人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BELONG_CONSIGNOR")
	public User getBelongConsignor() {
		return this.belongConsignor;
	}

	/**
	 *方法: 设置User
	 *@param: User  所属委托人
	 */
	public void setBelongConsignor(User belongConsignor) {
		this.belongConsignor = belongConsignor;
	}

	/**
	 *方法: 取得String
	 *@return: String  研究方向
	 */
	@Column(name = "RES_DIRECTION", length = 50)
	public String getResDirection() {
		return this.resDirection;
	}

	/**
	 *方法: 设置String
	 *@param: String  研究方向
	 */
	public void setResDirection(String resDirection) {
		this.resDirection = resDirection;
	}

	/**
	 *方法: 取得String
	 *@return: String  部门/学科
	 */
	@Column(name = "DEPT", length = 50)
	public String getDept() {
		return this.dept;
	}

	/**
	 *方法: 设置String
	 *@param: String  部门/学科
	 */
	public void setDept(String dept) {
		this.dept = dept;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  行业类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INDUSTRY_TYPE")
	public DicType getIndustryType() {
		return this.industryType;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  行业类型
	 */
	public void setIndustryType(DicType industryType) {
		this.industryType = industryType;
	}

	/**
	 *方法: 取得String
	 *@return: String  国家
	 */
	@Column(name = "COUNTRY", length = 50)
	public String getCountry() {
		return this.country;
	}

	/**
	 *方法: 设置String
	 *@param: String  国家
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 *方法: 取得String
	 *@return: String  省
	 */
	@Column(name = "PRIVINCE", length = 120)
	public String getPrivince() {
		return this.privince;
	}

	/**
	 *方法: 设置String
	 *@param: String  省
	 */
	public void setPrivince(String privince) {
		this.privince = privince;
	}

	/**
	 *方法: 取得String
	 *@return: String  市
	 */
	@Column(name = "CITY", length = 30)
	public String getCity() {
		return this.city;
	}

	/**
	 *方法: 设置String
	 *@param: String  市
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 *方法: 取得String
	 *@return: String  content1
	 */
	@Column(name = "CONTENT1", length = 100)
	public String getContent1() {
		return this.content1;
	}

	/**
	 *方法: 设置String
	 *@param: String  content1
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 *方法: 取得String
	 *@return: String  content2
	 */
	@Column(name = "CONTENT2", length = 100)
	public String getContent2() {
		return this.content2;
	}

	/**
	 *方法: 设置String
	 *@param: String  content2
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 *方法: 取得String
	 *@return: String  content3
	 */
	@Column(name = "CONTENT3", length = 100)
	public String getContent3() {
		return this.content3;
	}

	/**
	 *方法: 设置String
	 *@param: String  content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 *方法: 取得String
	 *@return: String  content4
	 */
	@Column(name = "CONTENT4", length = 100)
	public String getContent4() {
		return this.content4;
	}

	/**
	 *方法: 设置String
	 *@param: String  content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content5
	 */
	@Column(name = "CONTENT5", length = 255)
	public Date getContent5() {
		return this.content5;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content5
	 */
	public void setContent5(Date content5) {
		this.content5 = content5;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content6
	 */
	@Column(name = "CONTENT6", length = 255)
	public Date getContent6() {
		return this.content6;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content6
	 */
	public void setContent6(Date content6) {
		this.content6 = content6;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content7
	 */
	@Column(name = "CONTENT7", length = 255)
	public Date getContent7() {
		return this.content7;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content7
	 */
	public void setContent7(Date content7) {
		this.content7 = content7;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  content8
	 */
	@Column(name = "CONTENT8", length = 255)
	public Date getContent8() {
		return this.content8;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  content8
	 */
	public void setContent8(Date content8) {
		this.content8 = content8;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content9
	 */
	@Column(name = "CONTENT9", length = 20)
	public Double getContent9() {
		return this.content9;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content9
	 */
	public void setContent9(Double content9) {
		this.content9 = content9;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content10
	 */
	@Column(name = "CONTENT10", length = 20)
	public Double getContent10() {
		return this.content10;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content10
	 */
	public void setContent10(Double content10) {
		this.content10 = content10;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content11
	 */
	@Column(name = "CONTENT11", length = 20)
	public Double getContent11() {
		return this.content11;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content11
	 */
	public void setContent11(Double content11) {
		this.content11 = content11;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  content12
	 */
	@Column(name = "CONTENT12", length = 20)
	public Double getContent12() {
		return this.content12;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  content12
	 */
	public void setContent12(Double content12) {
		this.content12 = content12;
	}
}