package com.biolims.crm.sale.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 发票开票信息
 * @author lims-platform
 * @date 2015-08-03 11:27:15
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_SALE_BILL_ITEM")
@SuppressWarnings("serial")
public class CrmSaleBillItem extends EntityDao<CrmSaleBillItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**发票抬头*/
	private String billHead;
	/**明细*/
	private String billDet;
	/**期数*/
	private Integer num;
	/**金额*/
	private Double money;
	/**发票号*/
	private String billCode;
	/**开票日期*/
	private Date billDate;
	/**备注*/
	private String note;
	/**相关主表*/
	private CrmSaleBill crmSaleBill;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  发票抬头
	 */
	@Column(name = "BILL_HEAD", length = 20)
	public String getBillHead() {
		return this.billHead;
	}

	/**
	 *方法: 设置String
	 *@param: String  发票抬头
	 */
	public void setBillHead(String billHead) {
		this.billHead = billHead;
	}

	/**
	 *方法: 取得String
	 *@return: String  明细
	 */
	@Column(name = "BILL_DET", length = 20)
	public String getBillDet() {
		return this.billDet;
	}

	/**
	 *方法: 设置String
	 *@param: String  明细
	 */
	public void setBillDet(String billDet) {
		this.billDet = billDet;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  期数
	 */
	@Column(name = "NUM", length = 10)
	public Integer getNum() {
		return this.num;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  期数
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  金额
	 */
	@Column(name = "MONEY", length = 10)
	public Double getMoney() {
		return this.money;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  金额
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

	/**
	 *方法: 取得String
	 *@return: String  发票号
	 */
	@Column(name = "BILL_CODE", length = 40)
	public String getBillCode() {
		return this.billCode;
	}

	/**
	 *方法: 设置String
	 *@param: String  发票号
	 */
	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  开票日期
	 */
	@Column(name = "BILL_DATE", length = 255)
	public Date getBillDate() {
		return this.billDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  开票日期
	 */
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 40)
	public String getNote() {
		return this.note;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *方法: 取得CrmSaleBill
	 *@return: CrmSaleBill  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_SALE_BILL")
	public CrmSaleBill getCrmSaleBill() {
		return this.crmSaleBill;
	}

	/**
	 *方法: 设置CrmSaleBill
	 *@param: CrmSaleBill  相关主表
	 */
	public void setCrmSaleBill(CrmSaleBill crmSaleBill) {
		this.crmSaleBill = crmSaleBill;
	}
}