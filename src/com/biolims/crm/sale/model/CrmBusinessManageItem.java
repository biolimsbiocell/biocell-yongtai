package com.biolims.crm.sale.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;

/**   
 * @Title: Model
 * @Description: 商机管理明细
 * @author lims-platform
 * @date 2016-02-02 14:53:24
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_BUSINESS_MANAGE_ITEM")
@SuppressWarnings("serial")
public class CrmBusinessManageItem extends EntityDao<CrmBusinessManageItem> implements java.io.Serializable {
	/**序号*/
	private String id;
	/**跟踪日期*/
	private String code;
	/**跟踪方式*/
	private String progress;
	/**跟踪内容*/
	private String type;
	/**项目来源*/
	private String days;
	/**备注*/
	private String note;
	/**业务类型*/
	private DicType saleType;
	/**产品类型*/
	private DicType productType;
	/**技术类型*/
	private DicType technologyType;
	/**成单可能性*/
	private String chengdanProbability;
	/**后续工作*/
	private String followupWork;
	/**相关主表*/
	private CrmBusinessManage crmBusinessManage;

	/**
	 *方法: 取得String
	 *@return: String  序号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  跟踪日期
	 */
	@Column(name = "CODE", length = 36)
	public String getCode() {
		return this.code;
	}

	/**
	 *方法: 设置String
	 *@param: String  跟踪日期
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 *方法: 取得String
	 *@return: String  跟踪方式
	 */
	@Column(name = "PROGRESS", length = 60)
	public String getProgress() {
		return this.progress;
	}

	/**
	 *方法: 设置String
	 *@param: String  跟踪方式
	 */
	public void setProgress(String progress) {
		this.progress = progress;
	}

	/**
	 *方法: 取得String
	 *@return: String  跟踪内容
	 */
	@Column(name = "TYPE", length = 400)
	public String getType() {
		return this.type;
	}

	/**
	 *方法: 设置String
	 *@param: String  跟踪内容
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 *方法: 取得String
	 *@return: String  项目来源
	 */
	@Column(name = "DAYS", length = 60)
	public String getDays() {
		return this.days;
	}

	/**
	 *方法: 设置String
	 *@param: String  项目来源
	 */
	public void setDays(String days) {
		this.days = days;
	}

	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return this.note;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  业务类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SALE_TYPE")
	public DicType getSaleType() {
		return this.saleType;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  业务类型
	 */
	public void setSaleType(DicType saleType) {
		this.saleType = saleType;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  产品类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT_TYPE")
	public DicType getProductType() {
		return this.productType;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  产品类型
	 */
	public void setProductType(DicType productType) {
		this.productType = productType;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  技术类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECHNOLOGY_TYPE")
	public DicType getTechnologyType() {
		return this.technologyType;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  技术类型
	 */
	public void setTechnologyType(DicType technologyType) {
		this.technologyType = technologyType;
	}

	/**
	 *方法: 取得String
	 *@return: String  成单可能性
	 */
	@Column(name = "CHENGDAN_PROBABILITY", length = 20)
	public String getChengdanProbability() {
		return this.chengdanProbability;
	}

	/**
	 *方法: 设置String
	 *@param: String  成单可能性
	 */
	public void setChengdanProbability(String chengdanProbability) {
		this.chengdanProbability = chengdanProbability;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  后续工作
	 */
	@Column(name = "FOLLOWUP_WORK", length = 200)
	public String getFollowupWork() {
		return this.followupWork;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  后续工作
	 */
	public void setFollowupWork(String followupWork) {
		this.followupWork = followupWork;
	}

	/**
	 *方法: 取得CrmBusinessManage
	 *@return: CrmBusinessManage  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSINESS_MANAGE")
	public CrmBusinessManage getCrmBusinessManage() {
		return this.crmBusinessManage;
	}

	/**
	 *方法: 设置CrmBusinessManage
	 *@param: CrmBusinessManage  相关主表
	 */
	public void setCrmBusinessManage(CrmBusinessManage crmBusinessManage) {
		this.crmBusinessManage = crmBusinessManage;
	}
}