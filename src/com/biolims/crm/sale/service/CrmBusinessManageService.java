package com.biolims.crm.sale.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.sale.dao.CrmBusinessManageDao;
import com.biolims.crm.sale.model.CrmBusinessManage;
import com.biolims.crm.sale.model.CrmBusinessManageItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmBusinessManageService {
	@Resource
	private CrmBusinessManageDao crmBusinessManageDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findCrmBusinessManageList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return crmBusinessManageDao.selectCrmBusinessManageList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmBusinessManage i) throws Exception {

		crmBusinessManageDao.saveOrUpdate(i);

	}

	public CrmBusinessManage get(String id) {
		CrmBusinessManage crmBusinessManage = commonDAO.get(CrmBusinessManage.class, id);
		return crmBusinessManage;
	}

	public Map<String, Object> findCrmBusinessManageItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = crmBusinessManageDao.selectCrmBusinessManageItemList(scId, startNum, limitNum,
				dir, sort);
		List<CrmBusinessManageItem> list = (List<CrmBusinessManageItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmBusinessManageItem(CrmBusinessManage sc, String itemDataJson) throws Exception {
		List<CrmBusinessManageItem> saveItems = new ArrayList<CrmBusinessManageItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmBusinessManageItem scp = new CrmBusinessManageItem();
			// 将map信息读入实体类
			scp = (CrmBusinessManageItem) crmBusinessManageDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmBusinessManage(sc);

			saveItems.add(scp);
		}
		crmBusinessManageDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmBusinessManageItem(String[] ids) throws Exception {
		for (String id : ids) {
			CrmBusinessManageItem scp = crmBusinessManageDao.get(CrmBusinessManageItem.class, id);
			crmBusinessManageDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmBusinessManage sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmBusinessManageDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmBusinessManageItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmBusinessManageItem(sc, jsonStr);
			}
		}
	}
}
