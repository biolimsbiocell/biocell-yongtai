package com.biolims.crm.sale.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.sale.dao.CrmSaleChanceDao;
import com.biolims.crm.sale.dao.CrmSaleOrderDao;
import com.biolims.crm.sale.model.CrmSaleChance;
import com.biolims.crm.sale.model.CrmSaleChanceLinkman;
import com.biolims.crm.sale.model.CrmSaleOrder;
import com.biolims.crm.sale.model.CrmSaleOrderLinkman;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmSaleChanceService {
	@Resource
	private CrmSaleChanceDao crmSaleChanceDao;
	@Resource
	private CrmSaleOrderDao crmSaleOrderDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findCrmSaleChanceList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return crmSaleChanceDao.selectCrmSaleChanceList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmSaleChance i) throws Exception {

		crmSaleChanceDao.saveOrUpdate(i);

	}

	public CrmSaleChance get(String id) {
		CrmSaleChance crmSaleChance = commonDAO.get(CrmSaleChance.class, id);
		return crmSaleChance;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmSaleChance sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmSaleChanceDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmSaleChanceLinkman");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmSaleChanceLinkman(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmSaleChanceLinkman(CrmSaleChance sc, String itemDataJson) throws Exception {
		List<CrmSaleChanceLinkman> saveItems = new ArrayList<CrmSaleChanceLinkman>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmSaleChanceLinkman scp = new CrmSaleChanceLinkman();
			// 将map信息读入实体类
			scp = (CrmSaleChanceLinkman) crmSaleChanceDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmSaleChance(sc);

			saveItems.add(scp);
		}
		crmSaleChanceDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToOrder(String id) throws Exception {

		List<CrmSaleChance> saveItems = crmSaleChanceDao.setOrderList(id);
		for (CrmSaleChance cmpa : saveItems) {

			CrmSaleOrder cma = new CrmSaleOrder();
			//			cma.setId("CrmSaleChance" + cmpa.getId());
			//			cma.setName("商机" + cmpa.getId() + "生成的订单");
			//			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
			//					SystemConstants.USER_SESSION_KEY);
			String modelName = "CrmSaleOrder";
			String autoID = systemCodeService.getCodeByPrefix("CrmSaleOrder", "DD",
					00000, 5, null);//projectService.findAutoID(modelName, 4);
			cma.setId(autoID);
			//			cma.setName("商机" + cmpa.getId() + "生成的订单");
			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);

			cma.setCreateUser(user);
			cma.setCreateDate(new Date());
			cma.setType(cmpa.getType());
			cma.setManager(cmpa.getManager());
			cma.setCrmSaleChance(cmpa);
			crmSaleOrderDao.saveOrUpdate(cma);
		}

	}

	//审批完成
	public void chengeState(String applicationTypeActionId, String id) {
		CrmSaleChance sct = crmSaleChanceDao.get(CrmSaleChance.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		crmSaleChanceDao.update(sct);
		//给相关负责人发送消息
//		if (sct.getEvalustionUser() != null && !sct.getEvalustionUser().equals("")) {
//			String title = "项目评估通知";
//			String content = "您有一个需要评估的项目，请尽快去评估。";
//			projectService.createSampleSysRemind(title + ":" + content, sct.getEvalustionUser(), "sysRemind", content);
//		}
	}

	//项目评估申请子表
	public Map<String, Object> findCrmSaleChanceLinkManList(String scId, int startNum, int limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmSaleChanceDao.selectCrmChanceLinkManList(scId, startNum, limitNum, dir, sort);
		List<CrmSaleChanceLinkman> list = (List<CrmSaleChanceLinkman>) result.get("list");

		return result;
	}

}
