package com.biolims.crm.sale.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.sale.dao.CrmSaleBillDao;
import com.biolims.crm.sale.model.CrmSaleBill;
import com.biolims.crm.sale.model.CrmSaleBillItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmSaleBillService {
	@Resource
	private CrmSaleBillDao crmSaleBillDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findCrmSaleBillList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		return crmSaleBillDao.selectCrmSaleBillList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmSaleBill i) throws Exception {

		crmSaleBillDao.saveOrUpdate(i);

	}

	public CrmSaleBill get(String id) {
		CrmSaleBill crmSaleBill = commonDAO.get(CrmSaleBill.class, id);
		return crmSaleBill;
	}

	public Map<String, Object> findCrmSaleBillItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmSaleBillDao.selectCrmSaleBillItemList(scId, startNum, limitNum, dir, sort);
		List<CrmSaleBillItem> list = (List<CrmSaleBillItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmSaleBillItem(CrmSaleBill sc, String itemDataJson) throws Exception {
		List<CrmSaleBillItem> saveItems = new ArrayList<CrmSaleBillItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmSaleBillItem scp = new CrmSaleBillItem();
			// 将map信息读入实体类
			scp = (CrmSaleBillItem) crmSaleBillDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmSaleBill(sc);

			saveItems.add(scp);
		}
		crmSaleBillDao.saveOrUpdateAll(saveItems);
	}

	//审批完成
	public void chengeState(String applicationTypeActionId, String id) {
		CrmSaleBill sct = crmSaleBillDao.get(CrmSaleBill.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmUser(user);
		sct.setConfirmDate(new Date());

		crmSaleBillDao.update(sct);
	}

	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmSaleBillItem(String[] ids) throws Exception {
		for (String id : ids) {
			CrmSaleBillItem scp = crmSaleBillDao.get(CrmSaleBillItem.class, id);
			crmSaleBillDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmSaleBill sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmSaleBillDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmSaleBillItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmSaleBillItem(sc, jsonStr);
			}
		}
	}
}
