package com.biolims.crm.sale.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.crm.contract.model.CrmContract;
import com.biolims.crm.customer.customer.dao.CrmCustomerDao;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.sale.dao.CrmSaleOrderDao;
import com.biolims.crm.sale.model.CrmSaleChance;
import com.biolims.crm.sale.model.CrmSaleOrder;
import com.biolims.crm.sale.model.CrmSaleOrderLinkman;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.tra.transport.model.TransportApplyAnimal;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmSaleOrderService {
	@Resource
	private CrmSaleOrderDao crmSaleOrderDao;
	@Resource
	private CommonDAO commonDAO;
	private CrmCustomer crmCustomer;
	private CrmSaleOrder crmSaleOrder;
	@Resource
	private CrmCustomerDao crmCustomerDao;
//	@Resource
//	private ProjectDetailDao projectDetailDao;
	@Resource
	private UserGroupService userGroupService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findCrmSaleOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		/*if (!user.getId().equals("admin")) {
			return crmSaleOrderDao.selectCrmSaleOrderList(mapForQuery, startNum, limitNum, dir, sort, user.getId());
		} else {*/
		return crmSaleOrderDao.selectCrmSaleOrderList(mapForQuery, startNum, limitNum, dir, sort);
		//		}
	}

	public Map<String, Object> findCrmSaleOrderListById(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String code) {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		return crmSaleOrderDao.selectCrmSaleOrderListById(mapForQuery, startNum, limitNum, dir, sort, code, user
				.getId());
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmSaleOrder i) throws Exception {

		crmSaleOrderDao.saveOrUpdate(i);

	}

	public CrmSaleOrder get(String id) {
		CrmSaleOrder crmSaleOrder = commonDAO.get(CrmSaleOrder.class, id);
		return crmSaleOrder;
	}

	public Map<String, Object> findCrmSaleOrderLinkmanList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = crmSaleOrderDao.selectCrmSaleOrderLinkmanList(scId, startNum, limitNum, dir, sort);
		List<CrmSaleOrderLinkman> list = (List<CrmSaleOrderLinkman>) result.get("list");
		return result;
	}

	/*//查询相关联系人
	public void findLinkManList(CrmSaleOrder sc, String orderId) {
		CrmSaleOrderLinkman cs = new CrmSaleOrderLinkman();
		List<CrmLinkMan> list = crmSaleOrderDao.selectLinkManByManagerId(orderId);
		for (CrmLinkMan cl : list) {

			cs.setCode(cl.getId());
			cs.setEmail(cl.getEmail());
			cs.setName(cl.getName());
			cs.setTel(cl.getTelephone());
			cs.setTrans(cl.getIsFax());
			cs.setCrmSaleOrder(sc);
			crmSaleOrderDao.saveOrUpdate(cs);
		}

	}
	*/
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmSaleOrderLinkman(CrmSaleOrder sc, String itemDataJson) throws Exception {
		List<CrmSaleOrderLinkman> saveItems = new ArrayList<CrmSaleOrderLinkman>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmSaleOrderLinkman scp = new CrmSaleOrderLinkman();
			// 将map信息读入实体类
			scp = (CrmSaleOrderLinkman) crmSaleOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmSaleOrder(sc);

			saveItems.add(scp);
		}
		crmSaleOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmSaleOrderLinkman(String[] ids) throws Exception {
		for (String id : ids) {
			CrmSaleOrderLinkman scp = crmSaleOrderDao.get(CrmSaleOrderLinkman.class, id);
			crmSaleOrderDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmSaleOrder sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmSaleOrderDao.saveOrUpdate(sc);

			//如果该订单归属于父级订单，则把该订单的客户要求补充到父级订单中
			if (sc.getParent().getId() != null && !sc.getParent().getId().equals("")) {
				CrmSaleOrder cc = crmSaleOrderDao.get(CrmSaleOrder.class, sc.getParent().getId());
				cc.setCustomerRequirements(cc.getCustomerRequirements() + sc.getCustomerRequirements());
				crmSaleOrderDao.saveOrUpdate(cc);
			}

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("crmSaleOrderLinkman");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmSaleOrderLinkman(sc, jsonStr);
			}
			//String id =;//crmSaleOrder.getCrmCustomer().getId();//getParameterFromRequest("crmCustomer-id");
			//findLinkManList(sc, code);
		}
	}

	/**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<CrmSaleOrder> getChildList(List<CrmSaleOrder> list, CrmSaleOrder node) throws Exception { //得到子节点列表   
		List<CrmSaleOrder> li = new ArrayList<CrmSaleOrder>();
		Iterator<CrmSaleOrder> it = list.iterator();
		while (it.hasNext()) {
			CrmSaleOrder n = (CrmSaleOrder) it.next();
			if (n.getParent() != null && n.getParent().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	//审批完成
	public void chengeState(String applicationTypeActionId, String id) {
		CrmSaleOrder sct = crmSaleOrderDao.get(CrmSaleOrder.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmUser(user);
		sct.setConfirmDate(new Date());
		System.out.println("***********************************************************" + sct.getConfirmDate());
		crmSaleOrderDao.update(sct);
		CrmCustomer cc = sct.getCrmCustomer();
		if (cc != null) {
//			cc.setIsOrder("是");
			crmCustomerDao.saveOrUpdate(cc);
		}
	}

	/**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<CrmSaleOrder> list, CrmSaleOrder node) throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public boolean hasChildCount(CrmSaleOrder node) throws Exception {

		Long c = commonDAO.getCount("from CrmSaleOrder where parent.id='" + node.getId() + "'");
		return c > 0 ? true : false;
	}

	public String getJson(List<CrmSaleOrder> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<CrmSaleOrder> nodeList0 = new ArrayList<CrmSaleOrder>();
		Iterator<CrmSaleOrder> it1 = list.iterator();
		while (it1.hasNext()) {
			CrmSaleOrder node = (CrmSaleOrder) it1.next();
			//if (node.getLevel() == 0) {
			nodeList0.add(node);
			//}
		}
		Iterator<CrmSaleOrder> it = nodeList0.iterator();
		while (it.hasNext()) {
			CrmSaleOrder node = (CrmSaleOrder) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<CrmSaleOrder> list, CrmSaleOrder treeNode) throws Exception {
		json.append("{");
		json.append("\"id\":'");
		json.append(treeNode.getId() == null ? "" : treeNode.getId());
		json.append("',");
		json.append("\"name\":'");
		json.append(treeNode.getName() == null ? "" : treeNode.getName());
		json.append("',");
		json.append("\"crmMarcketSale\":'");
//		json.append(JsonUtils.formatStr(treeNode.getCrmMarcketSale() == null ? "" : treeNode.getCrmMarcketSale()
//				.getName())
//				+ "");
		json.append("',");
		json.append("\"crmCustomer\":'");
		json.append(JsonUtils.formatStr(treeNode.getCrmCustomer() == null ? "" : treeNode.getCrmCustomer().getName())
				+ "");
		json.append("',");
		json.append("\"money\":'");
		json.append(treeNode.getMoney() == null ? "" : treeNode.getMoney());
		json.append("',");
		json.append("\"type\":'");
		json.append(JsonUtils.formatStr(treeNode.getType() == null ? "" : treeNode.getType().getName()) + "");
		json.append("',");
		json.append("\"crmSaleChance\":'");
		json.append(JsonUtils.formatStr(treeNode.getCrmSaleChance() == null ? "" : treeNode.getCrmSaleChance()
				.getName())
				+ "");
		json.append("',");
		json.append("\"manager\":'");
		json.append(JsonUtils.formatStr(treeNode.getManager() == null ? "" : treeNode.getManager().getName()) + "");
		json.append("',");
		json.append("\"createUser\":'");
		json.append(JsonUtils.formatStr(treeNode.getCreateUser() == null ? "" : treeNode.getCreateUser().getName())
				+ "");
		json.append("',");
		json.append("\"createDate\":'");
		json.append(treeNode.getCreateDate() == null ? "" : treeNode.getCreateDate());
		json.append("',");
		json.append("\"parent\":'");
		json.append(JsonUtils.formatStr(treeNode.getParent() == null ? "" : treeNode.getParent().getName()) + "");
		json.append("',");
		json.append("\"crmContract\":'");
		json.append(JsonUtils.formatStr(treeNode.getCrmContract() == null ? "" : treeNode.getCrmContract().getName())
				+ "");
		json.append("',");
		json.append("\"confirmUser\":'");
		json.append(JsonUtils.formatStr(treeNode.getConfirmUser() == null ? "" : treeNode.getConfirmUser().getName())
				+ "");
		json.append("',");
		json.append("\"confirmDate\":'");
		json.append(treeNode.getConfirmDate() == null ? "" : treeNode.getConfirmDate());
		json.append("',");
		json.append("\"state\":'");
		json.append(treeNode.getState() == null ? "" : treeNode.getState());
		json.append("',");
		json.append("\"stateName\":'");
		json.append(treeNode.getStateName() == null ? "" : treeNode.getStateName());
		json.append("',");
		json.append("\"projectAsk\":'");
		json.append(treeNode.getProjectAsk() == null ? "" : treeNode.getProjectAsk());
		json.append("',");
		json.append("\"content1\":'");
		json.append(treeNode.getContent1() == null ? "" : treeNode.getContent1());
		json.append("',");
		json.append("\"content2\":'");
		json.append(treeNode.getContent2() == null ? "" : treeNode.getContent2());
		json.append("',");
		json.append("\"content3\":'");
		json.append(treeNode.getContent3() == null ? "" : treeNode.getContent3());
		json.append("',");
		json.append("\"content4\":'");
		json.append(treeNode.getContent4() == null ? "" : treeNode.getContent4());
		json.append("',");
		json.append("\"content5\":'");
		json.append(treeNode.getContent5() == null ? "" : treeNode.getContent5());
		json.append("',");
		json.append("\"content6\":'");
		json.append(treeNode.getContent6() == null ? "" : treeNode.getContent6());
		json.append("',");
		json.append("\"content7\":'");
		json.append(treeNode.getContent7() == null ? "" : treeNode.getContent7());
		json.append("',");
		json.append("\"content8\":'");
		json.append(treeNode.getContent8() == null ? "" : treeNode.getContent8());
		json.append("',");
		json.append("\"content9\":'");
		json.append(treeNode.getContent9() == null ? "" : treeNode.getContent9());
		json.append("',");
		json.append("\"content10\":'");
		json.append(treeNode.getContent10() == null ? "" : treeNode.getContent10());
		json.append("',");
		json.append("\"content11\":'");
		json.append(treeNode.getContent11() == null ? "" : treeNode.getContent11());
		json.append("',");
		json.append("\"genProjectDate\":'");
		json.append(treeNode.getGenProjectDate() == null ? "" : treeNode.getGenProjectDate());
		json.append("',");
		if (hasChildCount(treeNode)) {
			json.append("\"leaf\":false");
		} else {
			json.append("\"leaf\":true");
		}
		json.append(",\"upId\":'");
		json.append((treeNode.getParent() == null ? "" : treeNode.getParent().getId()) + "");
		json.append("'}");

	}

	public List<CrmSaleOrder> findCrmSaleOrderList(String upId) throws Exception {
		List<CrmSaleOrder> list = null;
		if (upId.equals("0")) {
			list = commonDAO.find("from CrmSaleOrder where  parent.id is null order by id asc");
		} else {
			list = commonDAO.find("from CrmSaleOrder where parent.id='" + upId + "' order by id asc");
		}
		return list;
	}

	public List<CrmSaleOrder> findCrmSaleOrderList() throws Exception {
		List<CrmSaleOrder> list = commonDAO.find("from CrmSaleOrder where  parent.id is null order by id asc");
		return list;
	}

	//	//根据客户带出联系人
	//	public List<Map<String, String>> showLinkManList(String code) throws Exception {
	//		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	//		Map<String, Object> result = crmSaleOrderDao.setLinkManList(code);
	//		List<CrmLinkMan> list = (List<CrmLinkMan>) result.get("list");
	//
	//		if (list != null && list.size() > 0) {
	//			for (CrmLinkMan srai : list) {
	//				Map<String, String> map = new HashMap<String, String>();
	//				map.put("id", srai.getId());
	//				map.put("name", srai.getName());
	//				map.put("email", srai.getEmail());
	//				map.put("telephone", srai.getTelephone());
	//				//map.put("sex", srai.getSex());
	//				map.put("mobile", srai.getMobile());
	//				mapList.add(map);
	//			}
	//		}
	//		return mapList;
	//	}

	//由订单生成合同
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public boolean saveToContract(String id) throws Exception {

		List<CrmSaleOrder> saveItems = crmSaleOrderDao.setOrderList(id);
		for (CrmSaleOrder cmpa : saveItems) {
			CrmContract cma = new CrmContract();
			String autoId = systemCodeService.getCodeByPrefix("CrmContract", "HT",
					00000, 5, null);//projectService.findAutoID("CrmContract", 4);
			cma.setId(autoId);
			cma.setName("订单" + cmpa.getId() + "生成的合同");
			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			cma.setCreateUser(user);
			cma.setCreateDate(new Date());
			//cma.setType(cmpa.getType());
			//			cma.setManager(cmpa.getManager());
//			cma.setCrmSaleOrder(cmpa);
			cma.setCrmCustomer(cmpa.getCrmCustomer());
			crmSaleOrderDao.saveOrUpdate(cma);
		}
		return true;
	}

//	//由订单生成详细设计
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public boolean saveToProjectDetail(String id) throws Exception {
//
//		List<CrmSaleOrder> saveItems = crmSaleOrderDao.setOrderList(id);
//		ProjectDetail cma = new ProjectDetail();
//		for (CrmSaleOrder cmpa : saveItems) {
//
//			String autoId = projectService.findAutoID("ProjectDetail", 5);
//			cma.setId(autoId);
//			//			cma.setName("订单" + cmpa.getId() + "生成的详细设计");
//			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
//					SystemConstants.USER_SESSION_KEY);
//			cma.setCreateUser(user);
//			cma.setProjectStart(cmpa.getProjectStart());
//			/*cma.setName(cmpa.getProjectStart().getName());*/
//			cma.setCreateDate(new Date());
//			cma.setConStartDate(cmpa.getStartDate());
//			cma.setConEndDate(cmpa.getEndDate());
//			cma.setCheckup(cmpa.getCheckup());
//			if (cmpa.getProjectStart() != null) {
//				cma.setName(cmpa.getProjectStart().getName());
//			} else {
//				cma.setName("");
//			}
//			cma.setManager(cmpa.getManager());
//			cma.setCrmCustomer(cmpa.getCrmCustomer());
//			cma.setNeoResult(cmpa.getNeoResult());
//			cma.setConEndDate(cmpa.getEndDate());
//			cma.setConStartDate(cmpa.getStartDate());
//			cma.setCustomerRequirements(cmpa.getCustomerRequirements());
//			cma.setProduct(cmpa.getProduct());
//			cma.setProductNo(cmpa.getProductNo());
//			cma.setType(cmpa.getType());
//			cma.setTechnologyType(cmpa.getTechnologyType());
//			cma.setGeneTargetingType(cmpa.getGeneTargetingType());
//			cma.setWorkType(cmpa.getWorkType());
//			cma.setGenus(cmpa.getGenus());
//			cmpa.setOrderStateName("详细设计");
//			//cma.setType(cmpa.getType());
//			//cma.se(cmpa.getManager());
//			cma.setCrmSaleOrder(cmpa);
//			//cma.setCrmCustomer(cmpa.getCrmCustomer());
//			projectDetailDao.saveOrUpdate(cma);
//		}
//		return true;
//
//	}

//	//由订单生成项目管理
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public boolean saveToProject(String id) throws Exception {
//
//		List<CrmSaleOrder> saveItems = crmSaleOrderDao.setOrderList(id);
//		for (CrmSaleOrder cmpa : saveItems) {
//			/*cmpa.setGenProjectDate(new Date());*///下单日期
//			Project cma = new Project();
//			String modelName = "Project";
//			String autoId = projectService.findAutoID("Project", 5);
//			cma.setId(autoId);
//			if (cmpa.getProjectStart() != null) {
//				cma.setName(cmpa.getProjectStart().getName());
//			} else {
//				cma.setName("");
//			}
//			cma.setName(cmpa.getContent2());
//			cma.setCheckup(cmpa.getCheckup());
//			cma.setCrmCustomer(cmpa.getCrmCustomer());
//			cma.setNeoResult(cmpa.getNeoResult());
//			cma.setProductNo(cmpa.getProductNo());
//			//cma.setName("订单" + cmpa.getId() + "生成的课题管理");
//			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
//					SystemConstants.USER_SESSION_KEY);
//			cma.setCreateUser(user);
//			cma.setWorkType(cmpa.getWorkType());
//			cma.setGenus(cmpa.getGenus());
//			cma.setStrain(cmpa.getStrain());
//			cma.setManager(cmpa.getManager());
//			cma.setTechnologyType(cmpa.getTechnologyType());
//			cma.setGeneTargetingType(cmpa.getGeneTargetingType());
//			cma.setOrderSource(cmpa.getOrderSource());
//			cma.setType(cmpa.getType());
//			cma.setStartDate(cmpa.getStartDate());
//			cma.setEndDate(cmpa.getEndDate());
//			cma.setProjectStart(cmpa.getProjectStart());
//			cma.setCreateDate(new Date());
//			cma.setCustomerRequirements(cmpa.getCustomerRequirements());
//			cma.setProduct(cmpa.getProduct());
//			cma.setProjectStart(cmpa.getProjectStart());
//			cma.setGenProjectDate(cmpa.getGenProjectDate());
//			//cma.setType(cmpa.getType());
//			cma.setCrmSaleOrder(cmpa);
//			//cma.setCrmCustomer(cmpa.getCrmCustomer());
//			crmSaleOrderDao.saveOrUpdate(cma);
//			cmpa.setState(SystemConstants.WORK_FLOW_COMPLETE);
//			cmpa.setStateName(SystemConstants.WORK_FLOW_COMPLETE_NAME);
//			cmpa.setConfirmUser(user);
//			cmpa.setOrderStateName("项目管理");
//			//消息提醒
//			List<String> l = new ArrayList<String>();
//			l.add("manageGroup");
//			List<UserGroupUser> l1 = userGroupService.findUserGroupUserListByList(l);
//			User zg = null;
//			if (l1.size() > 0) {
//				for (UserGroupUser ugu : l1) {
//					zg = ugu.getUser();
//					projectService.createSampleSysRemind("订单创建项目管理消息提醒", zg, modelName, autoId, new Date());
//				}
//			}
//		}
//		return true;
//
//	}
}
