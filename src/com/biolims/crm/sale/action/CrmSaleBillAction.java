package com.biolims.crm.sale.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.sale.model.CrmSaleBill;
import com.biolims.crm.sale.model.CrmSaleBillItem;
import com.biolims.crm.sale.service.CrmSaleBillService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/crm/sale/crmSaleBill")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmSaleBillAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2217";
	@Autowired
	private CrmSaleBillService crmSaleBillService;
	private CrmSaleBill crmSaleBill = new CrmSaleBill();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;

	@Action(value = "showCrmSaleBillList")
	public String showCrmSaleBillList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleBill.jsp");
	}

	@Action(value = "showCrmSaleBillListJson")
	public void showCrmSaleBillListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmSaleBillService.findCrmSaleBillList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmSaleBill> list = (List<CrmSaleBill>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("crmContract-id", "");
		map.put("crmContract-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "crmSaleBillSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmSaleBillList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleBillDialog.jsp");
	}

	@Action(value = "showDialogCrmSaleBillListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmSaleBillListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmSaleBillService.findCrmSaleBillList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmSaleBill> list = (List<CrmSaleBill>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("crmContract-id", "");
		map.put("crmContract-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editCrmSaleBill")
	public String editCrmSaleBill() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			crmSaleBill = crmSaleBillService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmSaleBill");
		} else {
			crmSaleBill.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmSaleBill.setCreateUser(user);
			crmSaleBill.setCreateDate(new Date());
			crmSaleBill.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
			crmSaleBill.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(crmSaleBill.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleBillEdit.jsp");
	}

	@Action(value = "copyCrmSaleBill")
	public String copyCrmSaleBill() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmSaleBill = crmSaleBillService.get(id);
		crmSaleBill.setId("NEW");
		crmSaleBill.setState("3");
		crmSaleBill.setStateName("新建");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleBillEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = crmSaleBill.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "CrmSaleBill";
			String autoID = systemCodeService.getCodeByPrefix("CrmSaleBill", "FP",
					00000, 5, null);//projectService.findAutoID(modelName, 4);
			crmSaleBill.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("crmSaleBillItem", getParameterFromRequest("crmSaleBillItemJson"));

		crmSaleBillService.save(crmSaleBill, aMap);
		return redirect("/crm/sale/crmSaleBill/editCrmSaleBill.action?id=" + crmSaleBill.getId());

	}

	@Action(value = "viewCrmSaleBill")
	public String toViewCrmSaleBill() throws Exception {
		String id = getParameterFromRequest("id");
		crmSaleBill = crmSaleBillService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleBillEdit.jsp");
	}

	@Action(value = "showCrmSaleBillItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmSaleBillItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleBillItem.jsp");
	}

	@Action(value = "showCrmSaleBillItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmSaleBillItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmSaleBillService
					.findCrmSaleBillItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmSaleBillItem> list = (List<CrmSaleBillItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("billHead", "");
			map.put("billDet", "");
			map.put("num", "");
			map.put("money", "");
			map.put("billCode", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("crmSaleBill-name", "");
			map.put("crmSaleBill-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	* 删除明细信息
	* @throws Exception
	*/
	@Action(value = "delCrmSaleBillItem")
	public void delCrmSaleBillItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmSaleBillService.delCrmSaleBillItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmSaleBillService getCrmSaleBillService() {
		return crmSaleBillService;
	}

	public void setCrmSaleBillService(CrmSaleBillService crmSaleBillService) {
		this.crmSaleBillService = crmSaleBillService;
	}

	public CrmSaleBill getCrmSaleBill() {
		return crmSaleBill;
	}

	public void setCrmSaleBill(CrmSaleBill crmSaleBill) {
		this.crmSaleBill = crmSaleBill;
	}

}
