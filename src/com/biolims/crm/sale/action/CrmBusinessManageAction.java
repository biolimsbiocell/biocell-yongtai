package com.biolims.crm.sale.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.sale.model.CrmBusinessManage;
import com.biolims.crm.sale.model.CrmBusinessManageItem;
import com.biolims.crm.sale.service.CrmBusinessManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/crm/sale/crmBusinessManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmBusinessManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "222222222";
	@Autowired
	private CrmBusinessManageService crmBusinessManageService;
	private CrmBusinessManage crmBusinessManage = new CrmBusinessManage();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;

	@Action(value = "showCrmBusinessManageList")
	public String showCrmBusinessManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmBusinessManage.jsp");
	}

	@Action(value = "showCrmBusinessManageListJson")
	public void showCrmBusinessManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmBusinessManageService.findCrmBusinessManageList(map2Query, startNum, limitNum,
				dir, sort);
		Long count = (Long) result.get("total");
		List<CrmBusinessManage> list = (List<CrmBusinessManage>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("note", "");
		map.put("postType-id", "");
		map.put("postType-name", "");
		map.put("linkmanNname-id", "");
		map.put("linkmanNname-name", "");
		map.put("linkmanMobile", "");
		map.put("linkmanPhone", "");
		map.put("email", "");
		map.put("unitName", "");
		map.put("unitAddress", "");
		map.put("belongConsignor-id", "");
		map.put("belongConsignor-name", "");
		map.put("resDirection", "");
		map.put("dept", "");
		map.put("industryType-id", "");
		map.put("industryType-name", "");
		map.put("country", "");
		map.put("privince", "");
		map.put("city", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "yyyy-MM-dd");
		map.put("content6", "yyyy-MM-dd");
		map.put("content7", "yyyy-MM-dd");
		map.put("content8", "yyyy-MM-dd");
		map.put("content9", "");
		map.put("content10", "");
		map.put("content11", "");
		map.put("content12", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "crmBusinessManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmBusinessManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmBusinessManageDialog.jsp");
	}

	@Action(value = "showDialogCrmBusinessManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmBusinessManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmBusinessManageService.findCrmBusinessManageList(map2Query, startNum, limitNum,
				dir, sort);
		Long count = (Long) result.get("total");
		List<CrmBusinessManage> list = (List<CrmBusinessManage>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("note", "");
		map.put("postType-id", "");
		map.put("postType-name", "");
		map.put("linkmanNname-id", "");
		map.put("linkmanNname-name", "");
		map.put("linkmanMobile", "");
		map.put("linkmanPhone", "");
		map.put("businessManage-id", "");
		map.put("businessManage-name", "");
		map.put("email", "");
		map.put("unitName", "");
		map.put("unitAddress", "");
		map.put("belongConsignor-id", "");
		map.put("belongConsignor-name", "");
		map.put("resDirection", "");
		map.put("dept", "");
		map.put("industryType-id", "");
		map.put("industryType-name", "");
		map.put("country", "");
		map.put("privince", "");
		map.put("city", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "yyyy-MM-dd");
		map.put("content6", "yyyy-MM-dd");
		map.put("content7", "yyyy-MM-dd");
		map.put("content8", "yyyy-MM-dd");
		map.put("content9", "");
		map.put("content10", "");
		map.put("content11", "");
		map.put("content12", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editCrmBusinessManage")
	public String editCrmBusinessManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			crmBusinessManage = crmBusinessManageService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmBusinessManage");
		} else {
			crmBusinessManage.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmBusinessManage.setCreateUser(user);
			crmBusinessManage.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/sale/crmBusinessManageEdit.jsp");
	}

	@Action(value = "copyCrmBusinessManage")
	public String copyCrmBusinessManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmBusinessManage = crmBusinessManageService.get(id);
		crmBusinessManage.setId("NEW");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/sale/crmBusinessManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = crmBusinessManage.getId();
		if (id != null && id.equals("") || id.equals("NEW")) {
			String modelName = "CrmBusinessManage";
			String autoID = systemCodeService.getCodeByPrefix("CrmBusinessManage", "SJ",
					00000, 5, null);//projectService.findAutoID(modelName, 5);
			crmBusinessManage.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("crmBusinessManageItem", getParameterFromRequest("crmBusinessManageItemJson"));

		crmBusinessManageService.save(crmBusinessManage, aMap);
		return redirect("/crm/sale/crmBusinessManage/editCrmBusinessManage.action?id=" + crmBusinessManage.getId());

	}

	@Action(value = "viewCrmBusinessManage")
	public String toViewCrmBusinessManage() throws Exception {
		String id = getParameterFromRequest("id");
		crmBusinessManage = crmBusinessManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/sale/crmBusinessManageEdit.jsp");
	}

	@Action(value = "showCrmBusinessManageItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmBusinessManageItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/sale/crmBusinessManageItem.jsp");
	}

	@Action(value = "showCrmBusinessManageItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmBusinessManageItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmBusinessManageService.findCrmBusinessManageItemList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmBusinessManageItem> list = (List<CrmBusinessManageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("progress", "");
			map.put("type", "");
			map.put("days", "");
			map.put("note", "");
			map.put("saleType-name", "");
			map.put("saleType-id", "");
			map.put("productType-name", "");
			map.put("productType-id", "");
			map.put("technologyType-name", "");
			map.put("technologyType-id", "");
			map.put("chengdanProbability", "");
			map.put("followupWork", "");
			map.put("crmBusinessManage-name", "");
			map.put("crmBusinessManage-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	* 删除明细信息
	* @throws Exception
	*/
	@Action(value = "delCrmBusinessManageItem")
	public void delCrmBusinessManageItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmBusinessManageService.delCrmBusinessManageItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmBusinessManageService getCrmBusinessManageService() {
		return crmBusinessManageService;
	}

	public void setCrmBusinessManageService(CrmBusinessManageService crmBusinessManageService) {
		this.crmBusinessManageService = crmBusinessManageService;
	}

	public CrmBusinessManage getCrmBusinessManage() {
		return crmBusinessManage;
	}

	public void setCrmBusinessManage(CrmBusinessManage crmBusinessManage) {
		this.crmBusinessManage = crmBusinessManage;
	}

}
