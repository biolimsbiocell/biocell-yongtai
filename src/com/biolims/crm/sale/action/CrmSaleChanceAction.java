package com.biolims.crm.sale.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;

import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.sale.model.CrmSaleChance;
import com.biolims.crm.sale.model.CrmSaleChanceLinkman;
import com.biolims.crm.sale.service.CrmSaleChanceService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;

@Namespace("/crm/sale/crmSaleChance")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmSaleChanceAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2220";
	@Autowired
	private CrmSaleChanceService crmSaleChanceService;
	private CrmSaleChance crmSaleChance = new CrmSaleChance();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;

	@Action(value = "showCrmSaleChanceList")
	public String showCrmSaleChanceList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleChance.jsp");
	}

	@Action(value = "showCrmSaleChanceListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmSaleChanceListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data").toLowerCase();
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmSaleChanceService
				.findCrmSaleChanceList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmSaleChance> list = (List<CrmSaleChance>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("changeUser-id", "");
		map.put("changeUser-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("geneName", "");
		map.put("geneId", "");
		map.put("changeStateName", "");
		map.put("chanceSource-id", "");
		map.put("chanceSource-name", "");
		map.put("dbMethod-id", "");
		map.put("dbMethod-name", "");
		map.put("linkMan-id", "");
		map.put("linkMan-name", "");
		map.put("linkMan-mobile", "");
		map.put("linkMan-email", "");
		// map.put("linkMan-customerId-department-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("area-id", "");
		map.put("area-name", "");
		// map.put("email", "");
		// map.put("phone", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("evalustionType-id", "");
		map.put("evalustionType-name", "");
		map.put("bussinessType-id", "");
		map.put("bussinessType-name", "");
		map.put("productType-id", "");
		map.put("productType-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("preFinishDate", "yyyy-MM-dd");
		map.put("evalustionUser-id", "");
		map.put("evalustionUser-name", "");
		map.put("successChance", "");
		map.put("followUser-id", "");
		map.put("followUser-name", "");
		map.put("followDate", "yyyy-MM-dd");
		map.put("followResult-id", "");
		map.put("followResult-name", "");
		map.put("followExplain", "");
		map.put("orderChance", "");
		map.put("content", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "crmSaleChanceSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmSaleChanceList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleChanceDialog.jsp");
	}

	@Action(value = "showDialogCrmSaleChanceListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmSaleChanceListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmSaleChanceService
				.findCrmSaleChanceList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmSaleChance> list = (List<CrmSaleChance>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("changeUser-id", "");
		map.put("changeUser-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("geneId", "");
		map.put("geneName", "");
		map.put("changeStateName", "");
		map.put("chanceSource-id", "");
		map.put("chanceSource-name", "");
		map.put("dbMethod-id", "");
		map.put("dbMethod-name", "");
		map.put("linkMan-id", "");
		map.put("linkMan-name", "");
		map.put("linkMan-mobile", "");
		map.put("linkMan-email", "");
		map.put("linkMan-customerId-department-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("area-id", "");
		map.put("area-name", "");
		map.put("email", "");
		map.put("phone", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("endDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("evalustionType-id", "");
		map.put("evalustionType-name", "");
		map.put("bussinessType-id", "");
		map.put("bussinessType-name", "");
		map.put("productType-id", "");
		map.put("productType-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("preFinishDate", "yyyy-MM-dd");
		map.put("evalustionUser-id", "");
		map.put("evalustionUser-name", "");
		map.put("successChance", "");
		map.put("followUser-id", "");
		map.put("followUser-name", "");
		map.put("followDate", "yyyy-MM-dd");
		map.put("followResult-id", "");
		map.put("followResult-name", "");
		map.put("followExplain", "");
		map.put("orderChance", "");
		map.put("content", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editCrmSaleChance")
	public String editCrmSaleChance() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		FileInfo fileInfo = new FileInfo();
		if (id != null && !id.equals("")) {
			crmSaleChance = crmSaleChanceService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmSaleChance");
			// fileInfo = fileInfoService.getFileInfoById(id);
		} else {
			crmSaleChance.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmSaleChance.setCreateUser(user);
			crmSaleChance.setCreateDate(new Date());
			crmSaleChance
					.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
			crmSaleChance
					.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(crmSaleChance.getState());
		putObjToContext("fileNum", num);
		// putObjToContext("fileName", fileInfo.getPicPath());
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleChanceEdit.jsp");
	}

	@Action(value = "copyCrmSaleChance")
	public String copyCrmSaleChance() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmSaleChance = crmSaleChanceService.get(id);
		crmSaleChance.setId("NEW");
		crmSaleChance.setState("3");
		crmSaleChance.setStateName("新建");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleChanceEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = crmSaleChance.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "CrmSaleChance";
			String autoID = systemCodeService.getCodeByPrefix("CrmSaleChance", "XMPG",
					00000, 5, null);//projectService.findAutoID(modelName, 4);
			crmSaleChance.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("crmSaleChanceLinkman",
				getParameterFromRequest("crmSaleChanceLinkmanJson"));
		crmSaleChanceService.save(crmSaleChance, aMap);
		return redirect("/crm/sale/crmSaleChance/editCrmSaleChance.action?id="
				+ crmSaleChance.getId());

	}

	// 项目评估申请子表
	@Action(value = "showCrmSaleChanceLinkmanList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmSaleOrderLinkmanList() throws Exception {
		String wid = getParameterFromRequest("id");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleChanceLinkman.jsp");
	}

	@Action(value = "showCrmSaleChanceLinkmanListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmSaleOrderLinkmanListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getParameterFromRequest("id");
			Map<String, Object> result = crmSaleChanceService
					.findCrmSaleChanceLinkManList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<CrmSaleChanceLinkman> list = (List<CrmSaleChanceLinkman>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("email", "");
			map.put("trans", "");
			map.put("deptName", "");
			map.put("ktEmail", "");
			map.put("phone", "");
			map.put("tel", "");
			map.put("crmlinkMan-id", "");
			map.put("crmlinkMan-name", "");
			map.put("crmSaleChance-id", "");
			map.put("crmSaleChance-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "viewCrmSaleChance", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toViewCrmSaleChance() throws Exception {
		String id = getParameterFromRequest("id");
		crmSaleChance = crmSaleChanceService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleChanceEdit.jsp");
	}

	// 由商机生成订单
	@Action(value = "editOrderList")
	public String editOrderList() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");

		crmSaleChanceService.saveToOrder(id);
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		// toSetStateCopy();
		return redirect("/crm/sale/crmSaleChance/editCrmSaleChance.action?id="
				+ id);
	}

	// 获取项目管理信息，并赋值给项目评估申请
	@Action(value = "getProjectMessageAndSet")
	public String getProjectMessageAndSet() throws Exception {
		long num = 0;
		String id = getParameterFromRequest("id");// 主表的ID
		String projectId = getParameterFromRequest("projectId");
		crmSaleChance = crmSaleChanceService.get(id);
//		Project p = projectService.get(projectId);
//		crmSaleChance.setProject(p);
//		crmSaleChance.setDbMethod(p.getGeneTargetingType());// 基因打靶类型
//		crmSaleChance.setTechnologyType(p.getTechnologyType());// 技术类型
//		crmSaleChance.setGenus(p.getGenus());// 种属
//		crmSaleChance.setStrain(p.getStrain());// 品系
//		crmSaleChance.setChanceSource(p.getOrderSource());// 项目来源
//		if (p.getProjectStart() != null) {
//			crmSaleChance.setGeneName(p.getProjectStart().getGeneName());// 基因正式名称
//		}
		toState(crmSaleChance.getState());
		num = fileInfoService.findFileInfoCount(id, "crmSaleChance");
		putObjToContext("fileNum", num);
		// 保存表
		Map aMap = new HashMap();
		aMap.put("crmSaleChanceLinkman",
				getParameterFromRequest("crmSaleChanceLinkmanJson"));
		crmSaleChanceService.save(crmSaleChance, aMap);
		return redirect("/crm/sale/crmSaleChance/editCrmSaleChance.action?id="
				+ crmSaleChance.getId());
	}

//	// 获取项目管理信息
//	@Action(value = "getProjectMessage")
//	public void getProjectMessage() throws Exception {
//		String projectId = getRequest().getParameter("projectId");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Project> dataList = new ArrayList<Project>();
//			dataList.add(projectService.get(projectId));
//			result.put("success", true);
//			result.put("data", dataList);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmSaleChanceService getCrmSaleChanceService() {
		return crmSaleChanceService;
	}

	public void setCrmSaleChanceService(
			CrmSaleChanceService crmSaleChanceService) {
		this.crmSaleChanceService = crmSaleChanceService;
	}

	public CrmSaleChance getCrmSaleChance() {
		return crmSaleChance;
	}

	public void setCrmSaleChance(CrmSaleChance crmSaleChance) {
		this.crmSaleChance = crmSaleChance;
	}

}
