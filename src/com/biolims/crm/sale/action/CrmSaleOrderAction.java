package com.biolims.crm.sale.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.service.CrmLinkManService;
import com.biolims.crm.sale.model.CrmSaleOrder;
import com.biolims.crm.sale.model.CrmSaleOrderLinkman;
import com.biolims.crm.sale.service.CrmSaleOrderService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/crm/sale/crmSaleOrder")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmSaleOrderAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2221";
	@Autowired
	private CrmSaleOrderService crmSaleOrderService;
	private CrmSaleOrder crmSaleOrder = new CrmSaleOrder();
	@Resource
	private CrmLinkManService crmLinkManService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;

	@Action(value = "showCrmSaleOrderList")
	public String showCrmSaleOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrder.jsp");
	}

	@Action(value = "showCrmSaleOrderListJson")
	public void showCrmSaleOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmSaleOrderService.findCrmSaleOrderList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmSaleOrder> list = (List<CrmSaleOrder>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("orderStateName", "");
		//map.put("crmMarcketSale-id", "");
		//map.put("crmMarcketSale-name", "");
		map.put("orderSource-id", "");
		map.put("orderSource-name", "");
		map.put("crmMarcketActivity-id", "");
		map.put("crmMarcketActivity-name", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmCustomer-department-name", "");
		map.put("money", "");
		map.put("productNo", "");
		map.put("client", "");
		map.put("checkup-id", "");
		map.put("checkup-name", "");
		map.put("neoResult", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("boss-id", "");
		map.put("boss-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("projectStart-id", "");
		map.put("projectStart-name", "");
		map.put("customerRequirements", "");
		map.put("crmSaleChance-id", "");
		map.put("crmSaleChance-name", "");
		map.put("projectStart-id", "");
		map.put("projectStart-name", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("parent-id", "");
		map.put("parent-name", "");
//		map.put("crmContract-id", "");
		map.put("crmContract-identifiers", "");
		map.put("crmContract-name", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("projectAsk", "");
		map.put("geneTargetingType-id", "");
		map.put("geneTargetingType-name", "");
		map.put("geneByName", "");
		map.put("geneName", "");
		map.put("geneId", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("note", "");
		map.put("startDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("genProjectDate", "yyyy-MM-dd"); //下单日期，就是生成项目管理时候的日期
		
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "crmSaleOrderSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmSaleOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrderDialog.jsp");
	}

	@Action(value = "showDialogCrmSaleOrderListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmSaleOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmSaleOrderService.findCrmSaleOrderList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmSaleOrder> list = (List<CrmSaleOrder>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("orderStateName", "");
		//map.put("crmMarcketSale-id", "");
		//map.put("crmMarcketSale-name", "");
		map.put("orderSource-id", "");
		map.put("orderSource-name", "");
		map.put("crmMarcketActivity-id", "");
		map.put("crmMarcketActivity-name", "");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("client", "");
		map.put("geneByName", "");
		map.put("geneName", "");
		map.put("geneId", "");
		map.put("money", "");
		map.put("productNo", "");
		map.put("checkup-id", "");
		map.put("checkup-name", "");
		map.put("neoResult", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("projectStart-id", "");
		map.put("projectStart-name", "");
		map.put("customerRequirements", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("boss-id", "");
		map.put("boss-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("crmSaleChance-id", "");
		map.put("crmSaleChance-name", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("parent-id", "");
		map.put("parent-name", "");
//		map.put("crmContract-id", "");
		map.put("crmContract-identifier", "");
		map.put("crmContract-name", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("projectAsk", "");
		map.put("startDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("genProjectDate", "yyyy-MM-dd"); //下单日期，就是生成项目管理时候的日期
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editCrmSaleOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editCrmSaleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			crmSaleOrder = crmSaleOrderService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmSaleOrder");
		} else {
			crmSaleOrder.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmSaleOrder.setCreateUser(user);
			crmSaleOrder.setCreateDate(new Date());
			crmSaleOrder.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
			crmSaleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(crmSaleOrder.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrderEdit.jsp");
	}

	@Action(value = "copyCrmSaleOrder")
	public String copyCrmSaleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmSaleOrder = crmSaleOrderService.get(id);
		crmSaleOrder.setId("NEW");
		crmSaleOrder.setState("3");
		crmSaleOrder.setStateName("新建");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrderEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = crmSaleOrder.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "CrmSaleOrder";
			String autoID = systemCodeService.getCodeByPrefix("CrmSaleOrder", "DD",
					00000, 5, null);//projectService.findAutoID(modelName, 4);
			crmSaleOrder.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("crmSaleOrderLinkman", getParameterFromRequest("crmSaleOrderLinkmanJson"));
		crmSaleOrderService.save(crmSaleOrder, aMap);
		return redirect("/crm/sale/crmSaleOrder/editCrmSaleOrder.action?id=" + crmSaleOrder.getId());
	}

	//查看订单
	@Action(value = "viewCrmSaleOrder")
	public String toViewCrmSaleOrder() throws Exception {
		String id = getParameterFromRequest("id");
		crmSaleOrder = crmSaleOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrderEdit.jsp");
	}

	/**
	 * 访问 任务树
	 */
	@Action(value = "showCrmSaleOrderTree")
	public String showCrmSaleOrderTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/crm/sale/crmSaleOrder/showCrmSaleOrderTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrderTree.jsp");
	}

	@Action(value = "showCrmSaleOrderTreeJson")
	public void showCrmSaleOrderTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<CrmSaleOrder> aList = null;
		if (upId.equals("")) {

			aList = crmSaleOrderService.findCrmSaleOrderList();
		} else {

			aList = crmSaleOrderService.findCrmSaleOrderList(upId);
		}

		String a = crmSaleOrderService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showCrmSaleOrderChildTreeJson")
	public void showCrmSaleOrderChildTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<CrmSaleOrder> aList = crmSaleOrderService.findCrmSaleOrderList(upId);
		String a = crmSaleOrderService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showCrmSaleOrderLinkmanList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmSaleOrderLinkmanList() throws Exception {
		String wid = getParameterFromRequest("wid");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/crm/sale/crmSaleOrderLinkman.jsp");
	}

	@Action(value = "showCrmSaleOrderLinkmanListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmSaleOrderLinkmanListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getParameterFromRequest("wid");
			Map<String, Object> result = crmSaleOrderService.findCrmSaleOrderLinkmanList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<CrmSaleOrderLinkman> list = (List<CrmSaleOrderLinkman>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("email", "");
			map.put("trans", "");
			map.put("deptName", "");
			map.put("ktEmail", "");
			map.put("phone", "");
			map.put("tel", "");
			map.put("crmlinkMan-id", "");
			map.put("crmlinkMan-name", "");
			map.put("crmSaleOrder-id", "");
			map.put("crmSaleOrder-name", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	* 删除明细信息
	* @throws Exception
	*/
	@Action(value = "delCrmSaleOrderLinkman")
	public void delCrmSaleOrderLinkman() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmSaleOrderService.delCrmSaleOrderLinkman(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmSaleOrderService getCrmSaleOrderService() {
		return crmSaleOrderService;
	}

	public void setCrmSaleOrderService(CrmSaleOrderService crmSaleOrderService) {
		this.crmSaleOrderService = crmSaleOrderService;
	}

	public CrmSaleOrder getCrmSaleOrder() {
		return crmSaleOrder;
	}

	public void setCrmSaleOrder(CrmSaleOrder crmSaleOrder) {
		this.crmSaleOrder = crmSaleOrder;
	}

//	//由客户带出联系人
//	@Action(value = "showLinkManList")
//	public void showLinkManList() throws Exception {
//		String code = getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.crmLinkManService.showLinkManList(code);
//			result.put("success", true);
//			result.put("data", dataListMap);
//
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	//由订单生成合同
	@Action(value = "editContractList")
	public void editContractList() throws Exception {
		String id = getParameterFromRequest("id");
		//		String handlemethod = getParameterFromRequest("handlemethod");

		//		crmSaleOrderService.saveToContract(id);
		//		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		//		toToolBar(rightsId, "", "", handlemethod);
		//		toSetStateCopy();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			boolean dataListMap = this.crmSaleOrderService.saveToContract(id);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

//	//由订单生成详细设计
//	@Action(value = "editProjectDetail")
//	public void editProjrctDetail() throws Exception {
//		String id = getParameterFromRequest("id");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			boolean dataListMap = this.crmSaleOrderService.saveToProjectDetail(id);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

//	//由订单生成项目管理
//	@Action(value = "editProject")
//	public void editProject() throws Exception {
//		String id = getParameterFromRequest("id");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			boolean dataListMap = this.crmSaleOrderService.saveToProject(id);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
