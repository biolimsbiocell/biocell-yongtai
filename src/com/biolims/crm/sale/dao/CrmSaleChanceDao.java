package com.biolims.crm.sale.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.crm.sale.model.CrmSaleChance;
import com.biolims.crm.sale.model.CrmSaleChanceLinkman;
import com.biolims.crm.sale.model.CrmSaleOrderLinkman;

@Repository
@SuppressWarnings("unchecked")
public class CrmSaleChanceDao extends BaseHibernateDao {
	public Map<String, Object> selectCrmSaleChanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from CrmSaleChance where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmSaleChance> list = new ArrayList<CrmSaleChance>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 商机生成订单
	public List<CrmSaleChance> setOrderList(String orderId) {
		String hql = "from CrmSaleChance where id='" + orderId + "'";
		List<CrmSaleChance> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 子表
	public Map<String, Object> selectCrmChanceLinkManList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from CrmSaleChanceLinkman where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmSaleChance='" + scId + "'";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmSaleChanceLinkman> list = new ArrayList<CrmSaleChanceLinkman>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public String map2where(Map<String, String> mapForQuery) {
		String whereStr = "";
		if (mapForQuery != null) {
			Set key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {
				String sOld = (String) it.next();
				String s = sOld;
				if ((mapForQuery.get(sOld) != null)
						&& (!(((String) mapForQuery.get(sOld)).equals("")))) {
					if (sOld.contains("##@@##")) {
						String[] sN = sOld.split("##@@##");
						s = sN[0];
					}

					String ss = (String) mapForQuery.get(sOld);

					if (ss.contains("##@@##")) {
						String[] s1 = ss.split("##@@##");

						if ((s.endsWith("Date")) || (s.endsWith("Time"))) {
							String[] a = ss.split(":");
							if (a.length == 0)
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " " + s1[1];

							if (a.length == 1)
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " to_date('" + s1[1]
										+ "','yyyy-MM-dd')";

							if (a.length == 2) {
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " to_date('" + s1[1]
										+ "','yyyy-MM-dd HH24:mi')";
							}

							if (a.length == 3)
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " to_date('" + s1[1]
										+ "','yyyy-MM-dd HH24:mi:ss')";

						} else {
							whereStr = whereStr + " and " + s + " " + s1[0]
									+ " " + s1[1];
						}
					} else {
						String whereS = s;

						if ((!(((String) mapForQuery.get(s)).startsWith("%")))
								&& (!(((String) mapForQuery.get(s))
										.endsWith("%"))))
							whereS = whereS + " = '"
									+ ((String) mapForQuery.get(s)) + "'";
						else {
							whereS = "lower(" + whereS + ") like '"
									+ ((String) mapForQuery.get(s)) + "'";
						}

						whereStr = whereStr + " and " + whereS;
					}
				}
			}

		}
		return whereStr;
	}
}