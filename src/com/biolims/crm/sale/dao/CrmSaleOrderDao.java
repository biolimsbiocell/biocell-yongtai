package com.biolims.crm.sale.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.sale.model.CrmSaleChance;
import com.biolims.crm.sale.model.CrmSaleOrder;
import com.biolims.crm.sale.model.CrmSaleOrderLinkman;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.tra.transport.model.TransportApplyAnimal;

@Repository
@SuppressWarnings("unchecked")
public class CrmSaleOrderDao extends BaseHibernateDao {
	public Map<String, Object> selectCrmSaleOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String userid) {
		String key = " ";
		String hql = " from CrmSaleOrder where 1=1  and createUser.id='" + userid + "' ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmSaleOrder> list = new ArrayList<CrmSaleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	//如果是管理员可以查询全部创建人的订单
	public Map<String, Object> selectCrmSaleOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = " ";
		String hql = " from CrmSaleOrder where 1=1 ";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmSaleOrder> list = new ArrayList<CrmSaleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	//查询该合同下的订单
	public Map<String, Object> selectCrmSaleOrderListById(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String code, String userid) {
		String key = " ";
		String hql = " from CrmSaleOrder where 1=1 and crmContract.id='" + code + "' and createUser.id='" + userid
				+ "' ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmSaleOrder> list = new ArrayList<CrmSaleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	//	//查询相关联系人
	//
	//	public List<CrmLinkMan> selectLinkManByManagerId(String orderId) {
	//		String hql = "from CrmLinkMan where customerId='" + orderId + "'";
	//		List<CrmLinkMan> list = this.getSession().createQuery(hql).list();
	//		return list;
	//	}

	public Map<String, Object> selectCrmSaleOrderLinkmanList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from CrmSaleOrderLinkman where 1=1 ";
		String key = "";
		if (scId != null) {
			key = key + " and crmSaleOrder='" + scId + "'";
			//key = key + " and crmSaleOrder.crmCustomer.id='" + orderId + "'";
		}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmSaleOrderLinkman> list = new ArrayList<CrmSaleOrderLinkman>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	//	//ajax要选择的联系人
	//	public Map<String, Object> setLinkManList(String code) throws Exception {
	//		String hql = "from CrmLinkMan t where 1=1 ";
	//		String key = "";
	//		if (code != null)
	//			key = key + " and  t.customerId='" + code + "'";
	//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
	//		List<CrmLinkMan> list = this.getSession().createQuery(hql + key).list();
	//		Map<String, Object> result = new HashMap<String, Object>();
	//		result.put("total", total);
	//		result.put("list", list);
	//		return result;
	//	}

	//订单生成合同
	//订单生成详细设计	
	//订单生成课题管理
	public List<CrmSaleOrder> setOrderList(String orderId) {
		String hql = "from CrmSaleOrder where id='" + orderId + "'";
		List<CrmSaleOrder> list = this.getSession().createQuery(hql).list();
		return list;
	}

}