/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：项目管理service
 * 创建人：倪毅
 * 创建时间：2011-10
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.crm.project.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.applicationType.model.ApplicationTypeActionLog;
import com.biolims.applicationType.service.ApplicationTypeActionService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.Department;
import com.biolims.crm.project.dao.ProjectDao;
import com.biolims.crm.project.model.Project;
import com.biolims.crm.project.model.ProjectObjectives;
import com.biolims.file.service.OperFileService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ProjectService {
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	CommonService commonService;
	@Resource
	private OperFileService operFileService;
	@Resource
	ApplicationTypeActionService applicationTypeActionService;

	@Resource
	private ProjectDao projectDao;

	StringBuffer json = new StringBuffer();

	/**
	 * 
	 * 检索预算,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	// @SuppressWarnings("unchecked")
	// public Map<String, Object> findProjectList(int startNum, int limitNum,
	// String dir, String sort, String contextPath, String data,
	// String rightsId, String curUserId) throws Exception {
	//
	// // 检索条件map,每个字段检索值
	// Map<String, Object> mapForQuery = new HashMap<String, Object>();
	// // 检索方式map,每个字段的检索方式
	// Map<String, String> mapForCondition = new HashMap<String, String>();
	// String startDate = "";
	// String endDate = "";
	//
	// if (data != null && !data.equals("")) {
	// BeanUtils.data2Map(data, mapForQuery, mapForCondition);
	// mapForQuery.remove("queryStartDate");
	// mapForCondition.remove("queryStartDate");
	// mapForQuery.remove("queryEndDate");
	// mapForCondition.remove("queryEndDate");
	// }
	//
	// // 设置组织机构管理范围Map
	// Map<String, String> mapForManageScope = new HashMap<String, String>();
	//
	// // 生成管理范围map
	// mapForManageScope = BeanUtils.configueDepartmentMap("department",
	// "applyUser.id", rightsId, curUserId);
	//
	// mapForQuery.put("mapForManageScope", mapForManageScope);
	//
	// // 将map值传入，获取列表
	//
	// Map<String, Object> criteriaMap = commonDAO.findObjectCriteria(
	// startNum, limitNum, dir, sort, Project.class, mapForQuery,
	// mapForCondition);
	// Criteria cr = (Criteria) criteriaMap.get("criteria");
	// Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
	// Map map = JsonUtils.toObjectByJson(data, Map.class);
	// if (data != null && !data.equals("")) {
	// if (map.get("queryStartDate") != null
	// && !map.get("queryStartDate").equals("")) {
	//
	// startDate = (String) map.get("queryStartDate");
	// cr.add(Restrictions.ge("factStartDate",
	// DateUtil.parse(startDate)));
	// cc.add(Restrictions.ge("factStartDate",
	// DateUtil.parse(startDate)));
	// mapForQuery.remove("queryStartDate");
	// mapForCondition.remove("queryStartDate");
	//
	// }
	// if (map.get("queryEndDate") != null
	// && !map.get("queryEndDate").equals("")) {
	//
	// endDate = (String) map.get("queryEndDate");
	// cr.add(Restrictions.le("factStartDate", DateUtil.parse(endDate)));
	// cc.add(Restrictions.le("factStartDate", DateUtil.parse(endDate)));
	// mapForQuery.remove("queryEndDate");
	// mapForCondition.remove("queryEndDate");
	// }
	//
	// }
	// Map<String, Object> controlMap = commonDAO.findObjectList(startNum,
	// limitNum, dir, sort, Project.class, mapForQuery,
	// mapForCondition, cc, cr);
	// // Map<String, Object> map2=new HashMap<String, Object>();
	// List<Project> list = (List<Project>) controlMap.get("list");
	// // List<Project> list2 = new ArrayList<Project>();
	// for(int i=0;i<list.size();i++){
	// String id = list.get(i).getId();
	// String num= projectDao.selectProjectNum(id);
	// list.get(i).setSampleCodeNum(num);
	// }
	// // List<Project> list5 = new ArrayList<Project>();
	// // // 当前登录用户
	// // User user = (User) ServletActionContext.getRequest().getSession()
	// // .getAttribute(SystemConstants.USER_SESSION_KEY);
	// // // 获取当前登录人id
	// // String userid = user.getId();
	// // // 获取部门ID
	// // Department department = user.getDepartment();
	// // String departmentid = department.getId();
	// //
	// // // 通过部门id查询到组织机构
	// // List<Department> list1 = projectDao.selectlevelID(departmentid);
	// // for (int i = 0; i < list1.size(); i++) {
	// // // 通过获得的组织机构的levelid查询下属levelid
	// // String id1 = list1.get(i).getLevelId();
	// // List<Department> list2 = projectDao.selectdownlevelID(id1);
	// // for (int j = 0; j < list2.size(); j++) {
	// // // 通过下属的levelId查询该组的部门id
	// // String id2 = list2.get(j).getLevelId();
	// // List<Department> list3 = projectDao.selectdowndepartmentId(id2);
	// // for (int k = 0; k < list3.size(); k++) {
	// // // 通过该组的部门id查询到该组的用户
	// // String id3 = list3.get(k).getId();
	// // List<User> list4 = projectDao.selectUser(id3);
	// // for (int m = 0; m < list4.size(); m++) {
	// // String id4 = list4.get(m).getId();
	// // List<Project> list6 = new ArrayList<Project>();
	// // list6 = projectDao.selectProject(id4);
	// // list5.addAll(list6);
	// // }
	// // }
	// //
	// // }
	// // }
	//
	// controlMap.put("list", list);
	// return controlMap;
	// }

	public Map<String, Object> findProjectList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return projectDao.selectProjectList(mapForQuery, startNum, limitNum,
				dir, sort);
	}
	
	public Map<String, Object> findProjectList(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return projectDao.selectProjectList(start, length, query, col, sort);
	}

	public Project getProject(String id) {
		Project project = commonDAO.get(Project.class, id);
		return project;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(Project pr, String jsonStr, String jsonStr2)
			throws Exception {
		commonDAO.saveOrUpdate(pr);
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveApplicationTypeActionLog(jsonStr);
		}
		if (jsonStr2 != null && !jsonStr2.equals("{}") && !jsonStr2.equals("")) {
			saveProjectObjectives(pr, jsonStr2);
		}

	}
	
	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(Project pr,String logInfo)
			throws Exception {
		commonDAO.saveOrUpdate(pr);
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(pr.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public Map<String, Object> findCrmProjectList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return projectDao.selectCrmProjectList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveApplicationTypeActionLog(String itemDataJson)
			throws Exception {
		List<ApplicationTypeActionLog> saveItems = new ArrayList<ApplicationTypeActionLog>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ApplicationTypeActionLog scp = new ApplicationTypeActionLog();
			// 将map信息读入实体类
			scp = (ApplicationTypeActionLog) projectDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setCrmOverseasMedical(sc);

			saveItems.add(scp);
		}
		projectDao.saveOrUpdateAll(saveItems);
	}

	public Map<String, Object> findApplicationTypeActionLogList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = projectDao
				.selectApplicationTypeActionLogList(scId, startNum, limitNum,
						dir, sort);
		List<ApplicationTypeActionLog> list = (List<ApplicationTypeActionLog>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addApplicationTypeActionLog(String applicationTypeActionId,
			String formId) throws Exception {
		Date n = new Date();
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		ApplicationTypeAction ata = commonDAO.get(ApplicationTypeAction.class,
				applicationTypeActionId);
		applicationTypeActionService.saveApplicationTypeActionLog(
				ata.getActionName(), "crmOverseasMedical", "", formId, "",
				user, n);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delApplicationTypeActionLog(String[] ids) throws Exception {
		for (String id : ids) {
			ApplicationTypeActionLog scp = projectDao.get(
					ApplicationTypeActionLog.class, id);
			projectDao.delete(scp);
		}
	}

	public Map<String, Object> findProjectObjectivesList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = projectDao.selectProjectObjectivesList(
				scId, startNum, limitNum, dir, sort);
		List<ProjectObjectives> list = (List<ProjectObjectives>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProjectObjectives(Project sc, String itemDataJson)
			throws Exception {
		List<ProjectObjectives> saveItems = new ArrayList<ProjectObjectives>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProjectObjectives scp = new ProjectObjectives();
			// 将map信息读入实体类
			scp = (ProjectObjectives) projectDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTProject(sc);

			saveItems.add(scp);
		}
		projectDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProjectObjectives(String[] ids) throws Exception {
		for (String id : ids) {
			ProjectObjectives scp = projectDao.get(ProjectObjectives.class, id);
			projectDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Project sc, Map jsonMap) throws Exception {
		if (sc != null) {
			projectDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("projectObjectives");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProjectObjectives(sc, jsonStr);
			}
		}

	}

	/**
	 * 
	 * @Title: findZjListForOrderId
	 * @Description: TODO(通过订单号查找任务单对应的进度信息)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-7 上午11:50:08
	 * @throws
	 */

	public Map<String, Object> findZjListForOrderId(String scId, int startNum,
			int limitNum, String dir, String sort) {
		Map<String, Object> result = projectDao.findZjListForOrderId(scId,
				startNum, limitNum, dir, sort);
		return result;
	}

	/**
	 * 
	 * @Title:
	 * @Description: TODO(通过订单号查找任务单对应的进度信息)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-7 上午11:50:08
	 * @throws
	 */

	public Map<String, Object> findJKListForOrderId(String scId, int startNum,
			int limitNum, String dir, String sort) {
		Map<String, Object> result = projectDao.findJKListForOrderId(scId,
				startNum, limitNum, dir, sort);
		return result;
	}

	/**
	 * 
	 * @Title: findSJListForOrderId
	 * @Description: TODO(上机任务单进度查询)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-9 上午9:26:12
	 * @throws
	 */
	public Map<String, Object> findSJListForOrderId(String scId, int startNum,
			int limitNum, String dir, String sort) {
		Map<String, Object> result = projectDao.findSJListForOrderId(scId,
				startNum, limitNum, dir, sort);
		return result;
	}

	/**
	 * 
	 * @Title: findSXListForOrderId
	 * @Description: TODO(生信任务单进度查询)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-9 下午3:41:08
	 * @throws
	 */
	public Map<String, Object> findSXListForOrderId(String scId, int startNum,
			int limitNum, String dir, String sort) {
		Map<String, Object> result = projectDao.findSXListForOrderId(scId,
				startNum, limitNum, dir, sort);
		return result;
	}

	public Map<String, Object> showProjectJson(Integer start, Integer length,
			String query, String col, String sort) {
		return projectDao.showProjectJson(start, length, query, col, sort);
	}
}
