package com.biolims.crm.project.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.contract.model.Contract;
import com.biolims.core.model.user.Department;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;

@Entity
@Table(name = "T_PROJECT")
public class Project extends EntityDao<Project> {

	private static final long serialVersionUID = -7915457065164332028L;
	// 编号
	@Id
	@Column(length = 32)
	private String id;
	// 名称
	@Column(length = 110)
	private String name;

	/** 选择部门 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSPECTION_DEPARTMENT")
	private DicType inspectionDepartment;
	/** 选择客户 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_DOCTOR")
	private CrmDoctor crmDoctor;
	// 类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	private DicType type;
	// 客户
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	private CrmCustomer mainProject;
	// 申报人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User applyUser;
	// 项目联系人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private CrmLinkMan crmlinkMan;
	@Transient
	private Long sampleNum;// 样本数量
	// 项目提成
	@Column(length = 110)
	private String tc;
	
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/** 自定义字段值*/
	private String fieldContent;
	


	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getFieldContent() {
		return fieldContent;
	}

	public void setFieldContent(String fieldContent) {
		this.fieldContent = fieldContent;
	}

	public String getTc() {
		return tc;
	}

	public void setTc(String tc) {
		this.tc = tc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User createUser;

	private Date createDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User dutyUser; // 项目
	// 合同编号
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Contract contract;
	// 批准人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User confirmUser;

	private Date confirmDate;
	// 部门
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Department department;
	// 申报日期
	private Date applyDate;
	// 合同开始日期
	private Date planStartDate;
	// 计划结束日期
	private Date planEndDate;
	// 实际开始日期
	private Date factStartDate;
	// 实际结束日期
	private Date factEndDate;
	/** 状态 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicState state;

	@Column(length = 2000)
	private String note;
	// 文件上传时间
	private Date fileUploadDate;
	@Column(length = 200)
	private String htDate;
	@Column(length = 200)
	private String lastDate;// 最后交付

	public String getHtDate() {
		return htDate;
	}

	public void setHtDate(String htDate) {
		this.htDate = htDate;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	// 问题
	@Column(length = 200)
	private String question;
	// 已计算进度
	@Transient
	private String yjsjd;
	// 持续天数
	@Transient
	private String cxts;
	// 延期
	@Transient
	private String yq;
	// 延期天数
	@Transient
	private String yqts;
	// 剩余天数
	@Transient
	private String syts;

	// 当前进展
	@Transient
	private String dqjz;
	// 下一步计划
	@Transient
	private String nextPlan;

	public String getNextPlan() {
		return nextPlan;
	}

	public void setNextPlan(String nextPlan) {
		this.nextPlan = nextPlan;
	}

	@Column(length = 200)
	private String isRemind;// 是否提醒

	@Column(length = 200)
	private String isEnd;// 是否完结

	// 项目销售
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	private User person;
	// 项目金额
	private String money;
	// 产品类型id
	private String productId;
	// 产品类型
	private String productName;
	// 总回款比例
	private String sumScale;
	
	//项目关联样本数量
	private String sampleCodeNum;

	public String getSampleCodeNum() {
		return sampleCodeNum;
	}

	public void setSampleCodeNum(String sampleCodeNum) {
		this.sampleCodeNum = sampleCodeNum;
	}

	public User getPerson() {
		return person;
	}

	public void setPerson(User person) {
		this.person = person;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSumScale() {
		return sumScale;
	}

	public void setSumScale(String sumScale) {
		this.sumScale = sumScale;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getIsRemind() {
		return isRemind;
	}

	public void setIsRemind(String isRemind) {
		this.isRemind = isRemind;
	}

	public Date getFileUploadDate() {
		return fileUploadDate;
	}

	public void setFileUploadDate(Date fileUploadDate) {
		this.fileUploadDate = fileUploadDate;
	}

	public String getSyts() {
		return syts;
	}

	public void setSyts(String syts) {
		this.syts = syts;
	}

	public String getCxts() {
		return cxts;
	}

	public void setCxts(String cxts) {
		this.cxts = cxts;
	}

	public String getYq() {
		return yq;
	}

	public void setYq(String yq) {
		this.yq = yq;
	}

	public String getYqts() {
		return yqts;
	}

	public void setYqts(String yqts) {
		this.yqts = yqts;
	}

	public String getYjsjd() {
		return yjsjd;
	}

	public void setYjsjd(String yjsjd) {
		this.yjsjd = yjsjd;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getFactStartDate() {
		return factStartDate;
	}

	public void setFactStartDate(Date factStartDate) {
		this.factStartDate = factStartDate;
	}

	public Date getFactEndDate() {
		return factEndDate;
	}

	public void setFactEndDate(Date factEndDate) {
		this.factEndDate = factEndDate;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public User getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(User applyUser) {
		this.applyUser = applyUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CrmCustomer getMainProject() {
		return mainProject;
	}

	public void setMainProject(CrmCustomer mainProject) {
		this.mainProject = mainProject;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public User getDutyUser() {
		return dutyUser;
	}

	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDqjz() {
		return dqjz;
	}

	public void setDqjz(String dqjz) {
		this.dqjz = dqjz;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public CrmLinkMan getCrmlinkMan() {
		return crmlinkMan;
	}

	public void setCrmlinkMan(CrmLinkMan crmlinkMan) {
		this.crmlinkMan = crmlinkMan;
	}

	public Long getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Long sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(String isEnd) {
		this.isEnd = isEnd;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 送检科室
	 */

	public DicType getInspectionDepartment() {
		return this.inspectionDepartment;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 送检科室
	 */
	public void setInspectionDepartment(DicType inspectionDepartment) {
		this.inspectionDepartment = inspectionDepartment;
	}

	public CrmDoctor getCrmDoctor() {
		return crmDoctor;
	}

	public void setCrmDoctor(CrmDoctor crmDoctor) {
		this.crmDoctor = crmDoctor;
	}

}
