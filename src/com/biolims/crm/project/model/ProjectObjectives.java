package com.biolims.crm.project.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 项目目标表
 * @author lims-platform
 * @date 2016-04-29 18:52:19
 * @version V1.0
 * 
 */
@Entity
@Table(name = "T_PROJECT_OBJECTIVES")
@SuppressWarnings("serial")
public class ProjectObjectives extends EntityDao<ProjectObjectives> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 名称 */
	private String name;
	/** 相关主表 */
	private Project tProject;

	// 回款金额
	private String returnedMoney;
	// 回款日期
	private Date returnedDate;
	// 回款销售
	private User returnedPerson;
	// 回款比例
	private String returnedScale;
	// 订单编号
	private String orderCode;

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getReturnedMoney() {
		return returnedMoney;
	}

	public void setReturnedMoney(String returnedMoney) {
		this.returnedMoney = returnedMoney;
	}

	public Date getReturnedDate() {
		return returnedDate;
	}

	public void setReturnedDate(Date returnedDate) {
		this.returnedDate = returnedDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER")
	public User getReturnedPerson() {
		return returnedPerson;
	}

	public void setReturnedPerson(User returnedPerson) {
		this.returnedPerson = returnedPerson;
	}

	public String getReturnedScale() {
		return returnedScale;
	}

	public void setReturnedScale(String returnedScale) {
		this.returnedScale = returnedScale;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 2000)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得Project
	 * 
	 * @return: Project 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_PROJECT")
	public Project getTProject() {
		return this.tProject;
	}

	/**
	 * 方法: 设置Project
	 * 
	 * @param: Project 相关主表
	 */
	public void setTProject(Project tProject) {
		this.tProject = tProject;
	}
}