package com.biolims.crm.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 任务进度
 * @author lims-platform
 * @date 2016-04-29 18:52:19
 * @version V1.0
 * 
 */
@Entity
@Table(name = "T_TASk_ACVANCE")
public class task extends EntityDao<task> implements java.io.Serializable {
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	* @author  zhiqiang.yang@biolims.cn  
	* @date 2017-11-8 上午11:36:28 
	*/ 
	private static final long serialVersionUID = 1L;
	/** 任务单号 */
	@Id
	@Column(length = 32)
	private String taskid;
	/** 名称 */
	private String name;
	/** 名称 */
	private String advance;
	/** 完成数量 */
	private String accomplispNumber;
	/** 未完成数量 */
	private String unaccomplispNumber;
	/** 异常数量 */
	private String abnormalNumber;
	/** 报告主键 */
	private String reportId;
	/** 报告名称 */
	private String reportName;

	/**
	 * @return taskid
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getTaskid() {
		return taskid;
	}

	/**
	 * @param taskid
	 *            the taskid to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}

	/**
	 * @return name
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return advance
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getAdvance() {
		return advance;
	}

	/**
	 * @param advance
	 *            the advance to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setAdvance(String advance) {
		this.advance = advance;
	}

	/**
	 * @return accomplispNumber
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getAccomplispNumber() {
		return accomplispNumber;
	}

	/**
	 * @param accomplispNumber
	 *            the accomplispNumber to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setAccomplispNumber(String accomplispNumber) {
		this.accomplispNumber = accomplispNumber;
	}

	/**
	 * @return unaccomplispNumber
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getUnaccomplispNumber() {
		return unaccomplispNumber;
	}

	/**
	 * @param unaccomplispNumber
	 *            the unaccomplispNumber to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setUnaccomplispNumber(String unaccomplispNumber) {
		this.unaccomplispNumber = unaccomplispNumber;
	}

	/** 
	 * @return abnormalNumber
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getAbnormalNumber() {
		return abnormalNumber;
	}

	/**
	 * @param abnormalNumber the abnormalNumber to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setAbnormalNumber(String abnormalNumber) {
		this.abnormalNumber = abnormalNumber;
	}

	/** 
	 * @return reportId
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getReportId() {
		return reportId;
	}

	/**
	 * @param reportId the reportId to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	/** 
	 * @return reportName
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName the reportName to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

}