/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：ProjectAction
 * 类描述：项目管理
 * 创建人：倪毅
 * 创建时间：2011-10
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.crm.project.action;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.model.ApplicationTypeActionLog;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.project.model.Project;
import com.biolims.crm.project.model.ProjectObjectives;
import com.biolims.crm.project.model.task;
import com.biolims.crm.project.service.ProjectService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleReceive;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.product.model.Product;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;
import com.sun.org.apache.bcel.internal.generic.NEW;

@Namespace("/crm/project")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class ProjectAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;
	private Project project = new Project();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Autowired
	private ProjectService projectService;

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	@Autowired
	private CommonService commonService;
	// 用于页面上显示模块名称
	private String title = "项目管理";

	// 该action权限id
	private String rightsId = "230201";

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	@Action(value = "selProjectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selProjectList() throws Exception {
		String a = getParameterFromRequest("a");
		putObjToContext("a", a);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/project/projectDialog.jsp");
	}

	@Action(value = "selProjectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selProjectListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		// User user = (User) this
		// .getObjFromSession(SystemConstants.USER_SESSION_KEY);
		//
		// Map<String, Object> controlMap = projectService.findProjectList(
		// startNum, limitNum, dir, sort, getContextPath(), data,
		// rightsId, user.getId());
		// long totalCount = Long.parseLong(controlMap.get("totalCount")
		// .toString());
		//
		// List<Project> list = (List<Project>) controlMap.get("list");

		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = projectService.findProjectList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Project> list = (List<Project>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();

		map.put("id", "");
		map.put("name", "");
		map.put("type-name", "");
		map.put("applyDate", "yyyy-MM-dd");
		map.put("department-briefName", "");
		map.put("dutyUser-name", "");
		map.put("isEnd", "");
		map.put("contract-id", "");
		map.put("state-name", "");
		map.put("applyUser-name", "");
		map.put("dqjz", "");
		map.put("nextPlan", "");
		map.put("question", "");
		map.put("mainProject-id", "");
		map.put("mainProject-name", "");
		map.put("crmDoctor-id", "");
		map.put("crmDoctor-name", "");
		map.put("confirmUser-name", "");
		map.put("planStartDate", "yyyy-MM-dd");
		map.put("planEndDate", "yyyy-MM-dd");
		map.put("factStartDate", "yyyy-MM-dd");
		map.put("factEndDate", "yyyy-MM-dd");
		map.put("fileUploadDate", "yyyy-MM-dd");
		map.put("contract-startDate", "yyyy-MM-dd");
		map.put("contract-startDate", "yyyy-MM-dd");
		map.put("htDate", "");
		map.put("lastDate", "");
		map.put("note", "");

		map.put("person-id", "");
		map.put("person-name", "");
		map.put("money", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sumScale", "");
		map.put("sampleCodeNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());

	}

	/**
	 * 访问列表
	 */
	@Action(value = "showProjectSelectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectSelectList() throws Exception {

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "项目编号", "120", "true",
				"", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "项目名称", "150", "true",
				"", "", "", "", "", "" });
		map.put("mainProject-id", new String[] { "", "string", "", "客户编码",
				"120", "true", "", "", "", "", "", "" });
		map.put("mainProject-name", new String[] { "", "string", "", "客户名称",
				"150", "true", "", "", "", "", "", "" });
		map.put("crmDoctor-id", new String[] { "", "string", "", "医生编号", "120",
				"true", "", "", "", "", "", "" });
		map.put("crmDoctor-name", new String[] { "", "string", "", "医生名称",
				"150", "true", "", "", "", "", "", "" });
		map.put("crmlinkMan-id", new String[] { "", "string", "", "客户联系人ID",
				"120", "true", "", "", "", "", "", "" });
		map.put("crmlinkMan-name", new String[] { "", "string", "", "客户联系人姓名",
				"150", "true", "", "", "", "", "", "" });
		// map.put("sampleCodeNum", new String[] { "", "string", "", "关联样本数量",
		// "120", "true",
		// "", "", "", "", "", "" });
		String type = generalexttype(map);

		String col = generalextcol(map);
		// 用于ext显示
		String flag = getParameterFromRequest("flag");
		putObjToContext("flag", flag);
		putObjToContext("type", type);
		putObjToContext("col", col);
		String crmCus = getParameterFromRequest("crmCus");
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/crm/project/showProjectListJson.action?crmCus=" + crmCus);
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/crm/project/showProjectListJson.jsp");
	}
	
	/**
	 * 访问列表
	 * @return 
	 */
	@Action(value = "showProjectSelectNewList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectSelectNewList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/crm/project/projectDialog.jsp");
	}

	@Action(value = "showProjectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectList() throws Exception {

		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 生成toolbar

		// 用于调用json时的action链接
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath() + "/crm/project/showProjectListJson.action");
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/crm/project/showProjectList.jsp");
	}

//	@Action(value = "showProjectListJson")
//	public void showProjectListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		String crmCus = getParameterFromRequest("crmCus");
//		// // 当前登录用户
//		// User user = (User) this
//		// .getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		//
//		// Map<String, Object> controlMap = projectService.findProjectList(
//		// startNum, limitNum, dir, sort, getContextPath(), data,
//		// rightsId, user.getId());
//		// long totalCount = Long.parseLong(controlMap.get("totalCount")
//		// .toString());
//		//
//		// List<Project> list = (List<Project>) controlMap.get("list");
//
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (crmCus != null && !"".equals(crmCus) && !"null".equals(crmCus)) {
//			map2Query.put("mainProject.id", crmCus);
//		}
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = projectService.findProjectList(map2Query,
//				startNum, limitNum, dir, sort);
//		Long count = (Long) result.get("total");
//		List<Project> list = (List<Project>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//
//		map.put("id", "");
//		map.put("name", "");
//		map.put("type-name", "");
//		map.put("applyDate", "yyyy-MM-dd");
//		map.put("department-briefName", "");
//		map.put("dutyUser-name", "");
//		map.put("isEnd", "");
//		map.put("contract-id", "");
//		map.put("state-name", "");
//		map.put("applyUser-name", "");
//		map.put("dqjz", "");
//		map.put("nextPlan", "");
//		map.put("question", "");
//		map.put("mainProject-id", "");
//		map.put("mainProject-name", "");
//		map.put("crmDoctor-id", "");
//		map.put("crmDoctor-name", "");
//		map.put("confirmUser-name", "");
//		map.put("planStartDate", "yyyy-MM-dd");
//		map.put("planEndDate", "yyyy-MM-dd");
//		map.put("factStartDate", "yyyy-MM-dd");
//		map.put("factEndDate", "yyyy-MM-dd");
//		map.put("fileUploadDate", "yyyy-MM-dd");
//		map.put("contract-startDate", "yyyy-MM-dd");
//		map.put("contract-startDate", "yyyy-MM-dd");
//		map.put("htDate", "");
//		map.put("lastDate", "");
//		map.put("note", "");
//
//		map.put("person-id", "");
//		map.put("person-name", "");
//		map.put("money", "");
//		map.put("productId", "");
//		map.put("productName", "");
//		map.put("sumScale", "");
//		map.put("sampleCodeNum", "");
//
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//
//	}
	
	@Action(value = "showProjectListNewJson")
	public void showProjectListNewJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = projectService.findProjectList(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<Project> list = (List<Project>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("mainProject-id", "");
			map.put("mainProject-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			map.put("crmlinkMan-id", "");
			map.put("crmlinkMan-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "showProjectListJson")
	public void showProjectListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = projectService.findProjectList(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<CrmCustomer> list = (List<CrmCustomer>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("type-name", "");
			map.put("applyDate", "yyyy-MM-dd");
			map.put("department-briefName", "");
			map.put("dutyUser-name", "");
			map.put("isEnd", "");
			map.put("contract-id", "");
			map.put("state-name", "");
			map.put("applyUser-name", "");
			map.put("dqjz", "");
			map.put("nextPlan", "");
			map.put("question", "");
			map.put("mainProject-id", "");
			map.put("mainProject-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			map.put("confirmUser-name", "");
			map.put("planStartDate", "yyyy-MM-dd");
			map.put("planEndDate", "yyyy-MM-dd");
			map.put("factStartDate", "yyyy-MM-dd");
			map.put("factEndDate", "yyyy-MM-dd");
			map.put("fileUploadDate", "yyyy-MM-dd");
			map.put("contract-startDate", "yyyy-MM-dd");
			map.put("contract-startDate", "yyyy-MM-dd");
			map.put("htDate", "");
			map.put("lastDate", "");
			map.put("note", "");

			map.put("person-id", "");
			map.put("person-name", "");
			map.put("money", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sumScale", "");
			map.put("sampleCodeNum", "");
			map.put("fieldContent", "");			
			
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("Project");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 访问添加页面
	 */

	@Action(value = "toEditProject")
	public String toEditProject() throws Exception {
		long num = 0;
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			num = fileInfoService.findFileInfoCount(id, "project");
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			System.out.println(project.getState());
			project = projectService.getProject(id);
			handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);

		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);

			project.setApplyUser(user);
			project.setCreateUser(user);
			project.setCreateDate(new Date());
			String workflowState = SystemConstants.DIC_STATE_NO;
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/project/editProject.jsp");
	}

	@Action(value = "toViewProject")
	public String toViewProject() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			project = projectService.getProject(id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);

		}

		return dispatcher("/WEB-INF/page/crm/project/editProject.jsp");
	}

	/**
	 * 添加
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = this.project.getId();
		if (id == null || id.length() <= 0
				|| SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			/*
			 * String code =
			 * systemCodeService.getSystemCode(SystemCode.PROJECT_PROJECT_NAME,
			 * SystemCode.PROJECT_PROJECT_CODE, null, null);
			 * this.project.setId(code);
			 */
		}
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
		
//			String applicationTypeActionLogJson = getParameterFromRequest("applicationTypeActionLogJson");
//			String projectObjectivesJson = getParameterFromRequest("projectObjectivesJson");

//			projectService.save(project, applicationTypeActionLogJson,
//					projectObjectivesJson);
			project.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			project.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			projectService.save(project, changeLog);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		// 具体操作，如删除，填加动作，应用redirect
		return redirect("/crm/project/toEditProject.action?id="
				+ project.getId());
	}

	/**
	 * 访问 任务树
	 */
	@Action(value = "showProjectTaskTree")
	public String showProjectTaskTree() throws Exception {
		String projectId = getParameterFromRequest("projectId");
		String tf = getParameterFromRequest("tf");
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/project/project/showProjectTaskTreeJson.action?projectId="
				+ projectId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("projectId", projectId);
		if (tf != null && !tf.equals(""))
			putObjToContext("tf", tf);
		return dispatcher("/WEB-INF/lims/project/project/showProjectTaskTree.jsp");
	}

	@Action(value = "hasId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String hasId() throws Exception {
		String id = getParameterFromRequest("id");
		return this.renderText(commonService.hasId(id, Project.class));

	}

	@Action(value = "showApplicationTypeActionLogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showApplicationTypeActionLogList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/applicationTypeActionLog.jsp");
	}

	/**
	 * 
	 * @Title: showApplicationTypeActionLogZJList
	 * @Description: TODO(项目主数据里的质检任务单信息查询页面)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-7 上午10:54:40
	 * @throws
	 */
	@Action(value = "showApplicationTypeActionLogZJList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showApplicationTypeActionLogZJList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/applicationTypeActionZJLog.jsp");
	}

	/**
	 * 
	 * @Title: showApplicationTypeActionLogListZJJson
	 * @Description: TODO(质检任务单 数据查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-7 上午11:00:06
	 * @throws
	 */
	@Action(value = "showApplicationTypeActionLogListZJJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showApplicationTypeActionLogListZJJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			if (!"".equals(scId)) {
				Map<String, Object> result = projectService
						.findZjListForOrderId(scId, startNum, limitNum, dir,
								sort);
				List<task> list = (List<task>) result.get("list");
				Map<String, String> map = new HashMap<String, String>();
				map.put("taskid", "");
				map.put("name", "");
				map.put("accomplispNumber", "");
				map.put("unaccomplispNumber", "");
				map.put("abnormalNumber", "");
				map.put("advance", "");
				map.put("reportId", "");
				map.put("reportName", "");
				new SendData().sendDateJson(map, list, list.size(),
						ServletActionContext.getResponse());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showApplicationTypeActionLogListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showApplicationTypeActionLogListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = projectService
					.findApplicationTypeActionLogList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<ApplicationTypeActionLog> list = (List<ApplicationTypeActionLog>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("applicationTypeTableId", "");
			map.put("contentId", "");
			map.put("oldActionName", "");
			map.put("actionName", "");
			map.put("note", "");
			map.put("nextNote", "");
			map.put("operUser-name", "");
			map.put("operUser-id", "");
			map.put("operDate", "yyyy-MM-dd");
			map.put("startDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showProjectObjectivesDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectObjectivesDialogList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/crm/project/projectObjectivesDialog.jsp");
	}

	/**
	 * 项目目标（订单信息）
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showProjectObjectivesList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectObjectivesList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/applicationTypeActionLog.jsp");
	}

	/**
	 * 
	 * @Title: showProjectObjectivesList
	 * @Description: TODO(建库任务单查询页面)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-8 下午4:27:30
	 * @throws
	 */
	@Action(value = "showProjectObjectivesJKList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectObjectivesJKList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/applicationTypeActionJKLog.jsp");
	}

	/**
	 * 
	 * @Title: showProjectObjectivesSJList
	 * @Description: TODO(上机任务单)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-8 下午6:02:41
	 * @throws
	 */
	@Action(value = "showProjectObjectivesSJList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectObjectivesSJList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/applicationTypeActionSJLog.jsp");
	}

	/**
	 * 
	 * @Title: showProjectObjectivesSXList
	 * @Description: TODO(生信任务单)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-8 下午6:03:00
	 * @throws
	 */
	@Action(value = "showProjectObjectivesSXList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectObjectivesSXList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/applicationTypeActionSXLog.jsp");
	}

	/**
	 * 
	 * @Title: showApplicationTypeActionLogListJKJson
	 * @Description: TODO(建库任务单)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-8 下午5:00:37
	 * @throws
	 */
	@Action(value = "showApplicationTypeActionLogListJKJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showApplicationTypeActionLogListJKJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			if (!"".equals(scId)) {
				Map<String, Object> result = projectService
						.findJKListForOrderId(scId, startNum, limitNum, dir,
								sort);
				List<task> list = (List<task>) result.get("list");
				Map<String, String> map = new HashMap<String, String>();
				map.put("taskid", "");
				map.put("name", "");
				map.put("accomplispNumber", "");
				map.put("unaccomplispNumber", "");
				map.put("abnormalNumber", "");
				map.put("advance", "");
				map.put("reportId", "");
				map.put("reportName", "");
				new SendData().sendDateJson(map, list, list.size(),
						ServletActionContext.getResponse());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showApplicationTypeActionLogListSJJson
	 * @Description: TODO(上机任务单查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-8 下午6:04:17
	 * @throws
	 */
	@Action(value = "showApplicationTypeActionLogListSJJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showApplicationTypeActionLogListSJJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			if (!"".equals(scId)) {

				Map<String, Object> result = projectService
						.findSJListForOrderId(scId, startNum, limitNum, dir,
								sort);
				List<task> list = (List<task>) result.get("list");
				Map<String, String> map = new HashMap<String, String>();
				map.put("taskid", "");
				map.put("name", "");
				map.put("accomplispNumber", "");
				map.put("unaccomplispNumber", "");
				map.put("advance", "");
				new SendData().sendDateJson(map, list, list.size(),
						ServletActionContext.getResponse());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showApplicationTypeActionLogListSXJson
	 * @Description: TODO(生信任务单查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-8 下午6:05:21
	 * @throws
	 */
	@Action(value = "showApplicationTypeActionLogListSXJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showApplicationTypeActionLogListSXJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = projectService.findSXListForOrderId(
					scId, startNum, limitNum, dir, sort);
			List<task> list = (List<task>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("taskid", "");
			map.put("name", "");
			map.put("accomplispNumber", "");
			map.put("unaccomplispNumber", "");
			map.put("advance", "");
			new SendData().sendDateJson(map, list, list.size(),
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 项目目标（订单信息）
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showProjectObjectivesZJList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProjectObjectivesZJList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/project/projectObjectives.jsp");
	}

	@Action(value = "showProjectObjectivesListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProjectObjectivesListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = projectService
					.findProjectObjectivesList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<ProjectObjectives> list = (List<ProjectObjectives>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("tProject-name", "");
			map.put("tProject-id", "");
			map.put("returnedMoney", "");
			map.put("returnedDate", "yyyy-MM-dd");
			map.put("returnedPerson-name", "");
			map.put("returnedPerson-id", "");
			map.put("returnedScale", "");
			map.put("orderCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProjectObjectives")
	public void delProjectObjectives() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			projectService.delProjectObjectives(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
/**
 * 
 * @Title: showProject  
 * @Description: 选择合同  
 * @author : shengwei.wang
 * @date 2018年2月2日上午9:56:18
 * @return
 * String
 * @throws
 */
	@Action(value = "showProject")
	public String showProject() {
		return dispatcher("/WEB-INF/page/crm/project/showProjectSelTable.jsp");
	}
	@Action(value="showProjectJson")
	public void showProjectJson(){
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = projectService
					.showProjectJson(start, length, query, col, sort);
			List<Project> list = (List<Project>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("type-name", "");
			map.put("applyDate", "yyyy-MM-dd");
			map.put("department-briefName", "");
			map.put("dutyUser-name", "");
			map.put("isEnd", "");
			map.put("contract-id", "");
			map.put("state-name", "");
			map.put("applyUser-name", "");
			map.put("dqjz", "");
			map.put("nextPlan", "");
			map.put("question", "");
			map.put("mainProject-id", "");
			map.put("mainProject-name", "");
			map.put("crmDoctor-id", "");
			map.put("crmDoctor-name", "");
			map.put("confirmUser-name", "");
			map.put("planStartDate", "yyyy-MM-dd");
			map.put("planEndDate", "yyyy-MM-dd");
			map.put("factStartDate", "yyyy-MM-dd");
			map.put("factEndDate", "yyyy-MM-dd");
			map.put("fileUploadDate", "yyyy-MM-dd");
			map.put("contract-startDate", "yyyy-MM-dd");
			map.put("contract-startDate", "yyyy-MM-dd");
			map.put("htDate", "");
			map.put("lastDate", "");
			map.put("note", "");
			map.put("person-id", "");
			map.put("person-name", "");
			map.put("money", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sumScale", "");
			map.put("sampleCodeNum", "");
			String data=new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result,data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
