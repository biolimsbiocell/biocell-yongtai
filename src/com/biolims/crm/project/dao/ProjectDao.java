package com.biolims.crm.project.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.applicationType.model.ApplicationTypeActionLog;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.Department;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.project.model.Project;
import com.biolims.crm.project.model.ProjectObjectives;
import com.biolims.crm.project.model.task;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.system.product.model.Product;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class ProjectDao extends BaseHibernateDao {
	public Map<String, Object> selectCrmProjectList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from Project where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Project> list = new ArrayList<Project>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by mainProject.id,id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectApplicationTypeActionLogList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ApplicationTypeActionLog where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and   contentId ='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ApplicationTypeActionLog> list = new ArrayList<ApplicationTypeActionLog>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by startDate DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectProjectObjectivesList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProjectObjectives where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and T_PROJECT ='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProjectObjectives> list = new ArrayList<ProjectObjectives>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * //通过部门id查询组织机构获取到 该部门下级的部门id
	 */
	public List<Department> selectDepartment(String departmentid) {
		String hql = "from Department where 1=1 and upDepartment='"
				+ departmentid + "'";
		List<Department> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * //通过部门id查该部门下的用户
	 */
	public List<User> selectUserid(String downUserid) {
		String hql = "from User where 1=1 and department='" + downUserid + "'";
		List<User> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * //通过用户查询到项目主数据
	 */
	public List<Project> selectProject(String id) {
		String hql = "from Project where 1=1 and dutyUser='" + id + "'";
		List<Project> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 通过部门id查询到组织机构levelID
	 */
	public List<Department> selectlevelID(String departmentid) {
		String hql = "from Department where 1=1 and id='" + departmentid + "'";
		List<Department> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * //通过获得的组织机构的levelid查询下属levelid
	 */
	public List<Department> selectdownlevelID(String departmentid) {
		String hql = "from Department where 1=1 and levelId like'"
				+ departmentid + "%'";
		List<Department> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * /通过下属的levelId查询该组的部门id
	 */
	public List<Department> selectdowndepartmentId(String departmentid) {
		String hql = "from Department where 1=1 and levelId='" + departmentid
				+ "'";
		List<Department> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * /通过下属的levelId查询该组的部门id
	 */
	public List<User> selectUser(String departmentid) {
		String hql = "from User where 1=1 and department='" + departmentid
				+ "'";
		List<User> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * //通过用户查询到样本主数据
	 */
	public List<SampleInfo> selectSampleInfo(String id) {
		String hql = "from SampleInfo where 1=1 and sellPerson='" + id + "'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * //用户为空的时候的情况询到样本主数据
	 */
	public List<SampleInfo> selectSampleInfoNull() {
		String hql = "from SampleInfo where 1=1 and sellPerson is null";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * //查询项目关联样本条数
	 */
	public String selectProjectNum(String id) {
		String hql = "from SampleInfo t where 1=1 and t.project='" + id + "'";
		String num = this.getSession().createQuery("select count(*) " + hql)
				.uniqueResult().toString();
		return num;

	}

	public Map<String, Object> selectProjectList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from Project where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		// else {
		// key = " and state ='1'";
		// }
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Project> list = new ArrayList<Project>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}

		// for(int i=0;i<list.size();i++){
		// String id = list.get(i).getId();
		// String num = selSampleCodeNum(id);
		// list.get(i).setSampleCodeNum(num);
		// }
		List<Project> list2 = new ArrayList<Project>();
		for (Project p : list) {
			String id = p.getId();
			String num = selSampleCodeNum(id);
			p.setSampleCodeNum(num);
			list2.add(p);
		}
		saveOrUpdateAll(list2);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list2);
		return result;

	}
	
	public Map<String, Object> selectProjectList(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  Project where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Project where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<Project> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			List<Project> list2 = new ArrayList<Project>();
			for (Project p : list) {
				String id = p.getId();
				String num = selSampleCodeNum(id);
				p.setSampleCodeNum(num);
				list2.add(p);
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list2);
		}
		return map;
	}

	// 查询项目关联样本数量
	public String selSampleCodeNum(String id) {
		String hql = "from SampleInfo t where 1=1 and t.project='" + id + "'";
		String num = this.getSession().createQuery("select count(*) " + hql)
				.uniqueResult().toString();
		return num;
	}

	/**
	 * 
	 * @Title: findZjListForOrderId
	 * @Description: TODO(通过订单号查找任务单对应的进度信息)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-7 上午11:56:06
	 * @throws
	 */

	public Map<String, Object> findZjListForOrderId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "SELECT DISTINCT\n"
				+ "  jk.tech_jk_service_task AS id,\n"
				+ "  (SELECT\n"
				+ "     yy.name\n"
				+ "   FROM tech_jk_service_task yy\n"
				+ "   WHERE yy.id = jk.tech_jk_service_task) AS NAME,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM tech_jk_service_task_item itm\n"
				+ "   WHERE itm.tech_jk_service_task = jk.tech_jk_service_task) AS countNumber,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM SAMPLE_DNA_INFO dna\n"
				+ "   WHERE dna.type = '1'\n"
				+ "       AND dna.submit = '1'\n"
				+ "       AND dna.tech_jk_service_task = jk.tech_jk_service_task) AS accomplispNumber,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM SAMPLE_DNA_INFO yc\n"
				+ "   WHERE yc.result = '0'\n"
				+ "       AND yc.submit = '1'\n"
				+ "       AND yc.tech_jk_service_task = jk.tech_jk_service_task) AS abnormalNumber\n"
				+ "FROM tech_jk_service_task_item jk\n"
				+ "WHERE jk.t_project = '" + scId + "'\n"
				+ "    AND jk.tech_jk_service_task LIKE 'TQ%'";

		String key = "";
		int total = this.getSession().createSQLQuery(hql + key).list().size();
		List<Object[]> list = new ArrayList<Object[]>();
		List<String> lista = new ArrayList<String>();
		List<task> list2 = new ArrayList<task>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createSQLQuery(hql + key).list();
			} else {
				list = this.getSession().createSQLQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}

			task p = null;
			for (int i = 0; i < list.size(); i++) {

				Object[] object = list.get(i);
				String id = (String) object[0];
				String name = object[1].toString();
				String countNumber = object[2].toString();
				String accomplispnumber = object[3].toString();
				String abnormalNumber = object[4].toString();
				String dnaTask = "";
				String dt = "SELECT DISTINCT dna_task FROM SAMPLE_DNA_INFO WHERE submit='1' AND tech_jk_service_task='"
						+ id + "'";
				lista = this.getSession().createSQLQuery(dt).list();
				for (int j = 0; j < lista.size(); j++) {
//					Object[] ss = lista.get(j);
					
					if (j<=0) {
						dnaTask =lista.get(j);
						
					} else {
						dnaTask += ","+lista.get(j);
					}
				}
				// if (object[5] != null && object[5] != "") {
				//
				// dnaTask = object[5].toString();
				// } else {
				// dnaTask="";
				// }
				Integer unaccomplispnumber = (Integer) (Integer
						.parseInt(countNumber) - Integer
						.parseInt(accomplispnumber));
				p = new task();
				p.setTaskid(id);
				p.setName(name);
				p.setReportId(dnaTask);
				p.setAccomplispNumber(accomplispnumber);
				p.setUnaccomplispNumber(Integer.toString(unaccomplispnumber));
				p.setAbnormalNumber(abnormalNumber);
				if (unaccomplispnumber == 0) {
					p.setAdvance(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}
				if (unaccomplispnumber != 0) {
					p.setAdvance("未完成");
				}
				list2.add(p);
			}
			// saveOrUpdateAll(list2);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list2);
		return result;
	}

	/**
	 * 
	 * @Title: findZjListForOrderId
	 * @Description: TODO(通过订单号查找建库任务单对应的进度信息)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-7 上午11:56:06
	 * @throws
	 */

	public Map<String, Object> findJKListForOrderId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "SELECT DISTINCT\n"
				+ "  jk.tech_jk_service_task AS id,\n"
				+ "  (SELECT\n"
				+ "     yy.name\n"
				+ "   FROM tech_jk_service_task yy\n"
				+ "   WHERE yy.id = jk.tech_jk_service_task AND yy.type = '1') AS NAME,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM tech_jk_service_task_item itm\n"
				+ "   WHERE itm.tech_jk_service_task = jk.tech_jk_service_task) AS countNumber,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM SAMPLE_WK_INFO dna\n"
				+ "   WHERE dna.submit = '1'\n"
				+ "       AND dna.tech_jk_service_task = jk.tech_jk_service_task) AS accomplispNumber\n"
				+ "FROM tech_jk_service_task_item jk\n"
				+ "WHERE jk.t_project = '" + scId
				+ "' AND jk.tech_jk_service_task LIKE '%JK%'";
		String key = "";
		int total = this.getSession().createSQLQuery(hql + key).list().size();
		List<Object[]> list = new ArrayList<Object[]>();
		List<task> list2 = new ArrayList<task>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createSQLQuery(hql + key).list();
			} else {
				list = this.getSession().createSQLQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}

			task p = null;
			for (int i = 0; i < list.size(); i++) {

				Object[] object = list.get(i);
				String id = (String) object[0];
				String name = object[1].toString();
				String countNumber = object[2].toString();
				String accomplispnumber = object[3].toString();
				Integer unaccomplispnumber = (Integer) (Integer
						.parseInt(countNumber) - Integer
						.parseInt(accomplispnumber));
				p = new task();
				p.setTaskid(id);
				p.setName(name);
				p.setAccomplispNumber(accomplispnumber);
				p.setUnaccomplispNumber(Integer.toString(unaccomplispnumber));
				if (unaccomplispnumber == 0) {
					p.setAdvance(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}
				if (unaccomplispnumber != 0) {
					p.setAdvance("未完成");
				}
				list2.add(p);
			}
			// saveOrUpdateAll(list2);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list2);
		return result;
	}

	/**
	 * 
	 * @Title: findSJListForOrderId
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-9 上午9:35:31
	 * @throws
	 */
	public Map<String, Object> findSJListForOrderId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "SELECT DISTINCT\n"
				+ "  (SELECT\n"
				+ "     sj.id\n"
				+ "   FROM tech_jk_service_task sj\n"
				+ "   WHERE sj.id = item.tech_jk_service_task\n"
				+ "       AND sj.type = '2') AS taskID,\n"
				+ "  (SELECT\n"
				+ "     yy.name\n"
				+ "   FROM tech_jk_service_task yy\n"
				+ "   WHERE yy.id = item.tech_jk_service_task\n"
				+ "       AND yy.type = '2') AS NAME,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM tech_jk_service_task_item itm\n"
				+ "   WHERE itm.tech_jk_service_task = taskId) AS countNumber\n"
				+ "FROM tech_jk_service_task_item item\n"
				+ "WHERE item.t_project = '" + scId + "'";
		String key = "";
		int total = this.getSession().createSQLQuery(hql + key).list().size();
		List<Object[]> list = new ArrayList<Object[]>();
		List<String> list1 = new ArrayList<String>();
		List<String> list11 = new ArrayList<String>();
		List<String> list3 = new ArrayList<String>();
		List<Object[]> list4 = new ArrayList<Object[]>();
		List<task> list2 = new ArrayList<task>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createSQLQuery(hql + key).list();
			} else {
				list = this.getSession().createSQLQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}

			task p = null;
			String accomplispNumber = null;
			for (int i = 0; i < list.size(); i++) {
				Object[] object = list.get(i);
				String id = (String) object[0];
				if (id != null) {

					String name = object[1].toString();
					String countNumber = object[2].toString();
					// String accomplispnumber = object[3].toString();

					// 查询样本编号

					String sql = "SELECT pp.code FROM tech_jk_service_task_item pp WHERE pp.tech_jk_service_task= '"
							+ id + "'";
					list1 = this.getSession().createSQLQuery(sql).list();
					for (int j = 0; j < list1.size(); j++) {
						// Object[] object1 = list1.get(j);
						String code = list1.get(j);

						String sql11 = "SELECT dti.desequencing_task  AS id FROM DESEQUENCING_TASK_ITEM dti WHERE dti.sampleid = '"
								+ code + "'  ";
						list11 = this.getSession().createSQLQuery(sql11).list();

						for (int kk = 0; kk < list11.size(); kk++) {
							String wkCode = list11.get(kk);
							String sql1 = "SELECT id FROM DESEQUENCING_TASK WHERE id='"
									+ wkCode + "' AND state='40'";
							list3 = this.getSession().createSQLQuery(sql1)
									.list();
							for (int k = 0; k < list3.size(); k++) {
								// Object[] object2 = list3.get(k);
								String desid = list3.get(k);
								String sql2 = "SELECT COUNT(*) FROM DESEQUENCING_TASK_ITEM dti WHERE dti.desequencing_task='"
										+ desid + "'";
								list4 = this.getSession().createSQLQuery(sql2)
										.list();
								for (int l = 0; l < list4.size(); l++) {
									accomplispNumber = String.valueOf(list4
											.get(l));
								}
							}
						}
					}
					//
					Integer unaccomplispnumber = (Integer) (Integer
							.parseInt(countNumber) - Integer
							.parseInt(accomplispNumber));
					p = new task();
					p.setTaskid(id);
					p.setName(name);
					p.setAccomplispNumber(accomplispNumber.toString());
					p.setUnaccomplispNumber(Integer
							.toString(unaccomplispnumber));
					if (unaccomplispnumber == 0) {
						p.setAdvance(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
					}
					if (unaccomplispnumber != 0) {
						p.setAdvance("未完成");
					}
					list2.add(p);
				}
			}
			// saveOrUpdateAll(list2);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list2);
		return result;
	}

	/**
	 * 
	 * @Title: findSXListForOrderId
	 * @Description: TODO(生信任务单进度查询)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-9 下午3:42:16
	 * @throws
	 */
	public Map<String, Object> findSXListForOrderId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "SELECT DISTINCT\n"
				+ "  jk.tech_jk_service_task AS id,\n"
				+ "  (SELECT\n"
				+ "     yy.name\n"
				+ "   FROM tech_jk_service_task yy\n"
				+ "   WHERE yy.id = jk.tech_jk_service_task AND yy.type = '1') AS NAME,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM tech_jk_service_task_item itm\n"
				+ "   WHERE itm.tech_jk_service_task = jk.tech_jk_service_task) AS countNumber,\n"
				+ "  (SELECT\n"
				+ "     COUNT(*)\n"
				+ "   FROM SAMPLE_WK_INFO dna\n"
				+ "   WHERE dna.submit = '1'\n"
				+ "       AND dna.tech_jk_service_task = jk.tech_jk_service_task) AS accomplispNumber,\n"
				+ " (SELECT  DISTINCT dna_task "
				+ "FROM SAMPLE_DNA_INFO "
				+ "WHERE tech_jk_service_task=jk.tech_jk_service_task AND submit='1';) as dnaTask\n"
				+ "FROM tech_jk_service_task_item jk\n"
				+ "WHERE jk.t_project = '" + scId
				+ "' AND jk.tech_jk_service_task LIKE '%JK%'";
		String key = "";
		int total = this.getSession().createSQLQuery(hql + key).list().size();
		List<Object[]> list = new ArrayList<Object[]>();
		List<task> list2 = new ArrayList<task>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createSQLQuery(hql + key).list();
			} else {
				list = this.getSession().createSQLQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}

			task p = null;
			for (int i = 0; i < list.size(); i++) {

				Object[] object = list.get(i);
				String id = (String) object[0];
				String name = object[1].toString();
				String countNumber = object[2].toString();
				String accomplispnumber = object[3].toString();
				String dnaTask = object[4].toString();
				Integer unaccomplispnumber = (Integer) (Integer
						.parseInt(countNumber) - Integer
						.parseInt(accomplispnumber));
				p = new task();
				p.setTaskid(id);
				p.setReportId(dnaTask);
				p.setName(name);
				p.setAccomplispNumber(accomplispnumber);
				p.setUnaccomplispNumber(Integer.toString(unaccomplispnumber));
				if (unaccomplispnumber == 0) {
					p.setAdvance(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}
				if (unaccomplispnumber != 0) {
					p.setAdvance("未完成");
				}
				list2.add(p);
			}
			// saveOrUpdateAll(list2);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list2);
		return result;
	}

	public Map<String, Object> showProjectJson(Integer start, Integer length,
			String query, String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Project where 1=1 ";
		String key = "";
//		if(query!=null&&!"".equals(query)){
//			key=map2Where(query);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Project where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<Project> list = new ArrayList<Project>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}
