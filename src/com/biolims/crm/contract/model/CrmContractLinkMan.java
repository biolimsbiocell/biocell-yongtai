package com.biolims.crm.contract.model;

import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 合同管理联系人
 * @author lims-platform
 * @date 2015-08-18 14:41:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_CONTRACT_LINKMAN")
@SuppressWarnings("serial")
public class CrmContractLinkMan extends EntityDao<CrmContractLinkMan> implements java.io.Serializable {
	/**编码*/
	private String id;
	/** 联系人编号 */
	private CrmLinkMan crmlinkMan;
	private String code;
	/** 联系人姓名 */
	private String name;
	/** 接收项目汇报邮箱*/
	private String ktEmail;
	/** 邮箱 */
	private String email;
	/**单位名称*/
	private String deptName;
	/** 手机 */
	private String phone;
	/** 固定电话 */
	private String tel;
	/** 传真 */
	private String trans;
	/**相关主表*/
	private CrmContract crmContract;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_LINK_MAN")
	public CrmLinkMan getCrmlinkMan() {
		return crmlinkMan;
	}

	public void setCrmlinkMan(CrmLinkMan crmlinkMan) {
		this.crmlinkMan = crmlinkMan;
	}

	/**
	 * 联系人姓名
	 * 
	 * @return
	 */
	@Column(name = "NAME", length = 60)
	public String getName() {
		return name;
	}

	/**
	 * 联系人姓名
	 * 
	 * @return
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 部门名称
	 * @return
	 */
	@Column(name = "DEPT_NAME", length = 100)
	public String getDeptName() {
		return deptName;
	}

	/**
	 * 部门名称
	 * @return
	 */
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	/**
	 * 联系人接收项目汇报邮箱
	 * 
	 * @return
	 */
	@Column(name = "KT_EMAIL", length = 100)
	public String getKtEmail() {
		return ktEmail;
	}

	/**
	 * 联系人接收项目汇报邮箱
	 * 
	 * @return
	 */
	public void setKtEmail(String ktEmail) {
		this.ktEmail = ktEmail;
	}

	/**
	 * 联系人邮箱
	 * 
	 * @return
	 */
	@Column(name = "EMAIL", length = 100)
	public String getEmail() {
		return email;
	}

	/**
	 * 联系人邮箱
	 * 
	 * @return
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 联系人手机
	 * 
	 * @return
	 */
	@Column(name = "PHONE", length = 60)
	public String getPhone() {
		return phone;
	}

	/**
	 * 联系人手机
	 * 
	 * @return
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 联系人固定电话
	 * 
	 * @return
	 */
	@Column(name = "TEL", length = 60)
	public String getTel() {
		return tel;
	}

	/**
	 * 联系人固定电话
	 * 
	 * @return
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * 传真
	 * 
	 * @return
	 */
	@Column(name = "TRANS", length = 60)
	public String getTrans() {
		return trans;
	}

	/**
	 * 传真
	 * 
	 * @return
	 */
	public void setTrans(String trans) {
		this.trans = trans;
	}

	/**
	 *方法: 取得CrmContract
	 *@return: CrmContract  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 *方法: 设置CrmContract
	 *@param: CrmContract  相关主表
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}
}