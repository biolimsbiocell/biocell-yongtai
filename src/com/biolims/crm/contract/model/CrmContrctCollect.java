package com.biolims.crm.contract.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 合同回款明细
 * @author lims-platform
 * @date 2015-08-03 11:27:24
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_CONTRACT_COLLECT")
@SuppressWarnings("serial")
public class CrmContrctCollect extends EntityDao<CrmContrctCollect> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 序号 */
	private Integer collectNum;
	/** 回款金额 */
	private Double money;
	/** 开票日期 */
	private Date billDate;
	/** 开票金额 */
	private Double billMoney;
	/** 回款单位 */
	private String collectDept;
	/** 回款单位 */
	private String collectType;
	/** 发票开具状态 */
	private String billState;
	/** 发票号 */
	private String billId;
	/** 开票要求 */
	private String request;
//	/** 项目节点 */
//	private ProjectProgress projectNode;
	/** 收款比例 */
	private String remind;
	/** 到达节点提醒 */
	private String scaling;
	/** 开票数量 */
	private Integer num;
	/** 回款日期 */
	private Date receivedDate;
	/** 计划回款日期 */
	private Date planReceivedDate;
	/** 备注（前） */
	private String note;
	/** 备注（后） */
	private String note2;
	/** 状态 */
	private String state;

	/** 经手人 */

	private String brokerage;

	public String getBrokerage() {
		return brokerage;
	}

	public void setBrokerage(String brokerage) {
		this.brokerage = brokerage;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/** 相关主表 */
	private CrmContract crmContract;
	/** 对应付款计划 */
	private CrmContrctPay crmContrctPay;

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT_NODE")
//	public ProjectProgress getProjectNode() {
//		return projectNode;
//	}
//
//	public void setProjectNode(ProjectProgress projectNode) {
//		this.projectNode = projectNode;
//	}

	public String getRemind() {
		return remind;
	}

	public void setRemind(String remind) {
		this.remind = remind;
	}

	public String getScaling() {
		return scaling;
	}

	public void setScaling(String scaling) {
		this.scaling = scaling;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 金额
	 */
	@Column(name = "MONEY", length = 20)
	public Double getMoney() {
		return this.money;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 金额
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 开票日期
	 */
	@Column(name = "BILL_DATE", length = 255)
	public Date getBillDate() {
		return this.billDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 开票日期
	 */
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 开票金额
	 */
	@Column(name = "BILL_MONEY", length = 20)
	public Double getBillMoney() {
		return this.billMoney;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 开票金额
	 */
	public void setBillMoney(Double billMoney) {
		this.billMoney = billMoney;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 发票号
	 */
	@Column(name = "BILL_ID", length = 40)
	public String getBillId() {
		return this.billId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 发票号
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 回款日期
	 */
	@Column(name = "RECEIVED_DATE", length = 255)
	public Date getReceivedDate() {
		return this.receivedDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 回款日期
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 计划回款日期
	 */
	@Column(name = "PLAN_RECEIVED_DATE", length = 255)
	public Date getPlanReceivedDate() {
		return this.planReceivedDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 计划回款日期
	 */
	public void setPlanReceivedDate(Date planReceivedDate) {
		this.planReceivedDate = planReceivedDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得CrmContract
	 * 
	 * @return: CrmContract 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 * 方法: 设置CrmContract
	 * 
	 * @param: CrmContract 相关主表
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

	/**
	 * 方法: 取得CrmContrctPay
	 * 
	 * @return: CrmContrctPay 对应回款计划
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRCT_PAY")
	public CrmContrctPay getCrmContrctPay() {
		return crmContrctPay;
	}

	public void setCrmContrctPay(CrmContrctPay crmContrctPay) {
		this.crmContrctPay = crmContrctPay;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 回款单位
	 */
	@Column(name = "COLLECTDEPT", length = 60)
	public String getCollectDept() {
		return collectDept;
	}

	/**
	 * 方法: 设置回款单位
	 * 
	 * @param: String 回款单位
	 */
	public void setCollectDept(String collectDept) {
		this.collectDept = collectDept;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 回款类型
	 */
	@Column(name = "COLLECTTYPE", length = 32)
	public String getCollectType() {
		return collectType;
	}

	/**
	 * 方法: 设置回款类型
	 * 
	 * @param: String 回款类型
	 */
	public void setCollectType(String collectType) {
		this.collectType = collectType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 发票类型
	 */
	@Column(name = "BILLSTATE", length = 32)
	public String getBillState() {
		return billState;
	}

	/**
	 * 方法: 设置发票类型
	 * 
	 * @param: String 发票类型
	 */
	public void setBillState(String billState) {
		this.billState = billState;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注（后）
	 */
	@Column(name = "NOTE2", length = 150)
	public String getNote2() {
		return note2;
	}

	/**
	 * 方法: 设置备注（后）
	 * 
	 * @param: String 备注（后）
	 */
	public void setNote2(String note2) {
		this.note2 = note2;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 序号
	 */
	@Column(name = "COLLECT_NUM", length = 150)
	public Integer getCollectNum() {
		return collectNum;
	}

	/**
	 * 方法: 设置备注（后）
	 * 
	 * @param: Integer 备注（后）
	 */
	public void setCollectNum(Integer collectNum) {
		this.collectNum = collectNum;
	}

}