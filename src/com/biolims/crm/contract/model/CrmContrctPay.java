package com.biolims.crm.contract.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 合同价款信息
 * @author lims-platform
 * @date 2015-08-03 11:27:24
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_CONTRACT_PAY")
@SuppressWarnings("serial")
public class CrmContrctPay extends EntityDao<CrmContrctPay> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 描述 */
	private String name;
	/** 期数 */
	private Integer periodsNum;
	/** 金额 */
	private Double money;
	/** 合同总金额 */
	private Double totalMoney;
	/** 币种 */
	private String currency;
	/** 开票日期 */
	private Date billDate;
	/** 开票金额 */
	private Double billMoney;
	/** 发票号 */
	private String billId;
	/** 开票要求 */
	private String request;
//	/** 项目节点 */
//	private ProjectProgress projectNode;
	/** 收款比例 */
	private String remind;
	/** 到达节点提醒 */
	private String scaling;
	/** 开票数量 */
	private Integer num;
	/** 回款日期 */
	private Date receivedDate;
	/** 计划回款日期 */
	private Date planReceivedDate;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private CrmContract crmContract;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "NAME", length = 120)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT_NODE")
//	public ProjectProgress getProjectNode() {
//		return projectNode;
//	}
//
//	public void setProjectNode(ProjectProgress projectNode) {
//		this.projectNode = projectNode;
//	}

	public String getRemind() {
		return remind;
	}

	public void setRemind(String remind) {
		this.remind = remind;
	}

	public String getScaling() {
		return scaling;
	}

	public void setScaling(String scaling) {
		this.scaling = scaling;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 金额
	 */
	@Column(name = "MONEY", length = 20)
	public Double getMoney() {
		return this.money;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 金额
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 开票日期
	 */
	@Column(name = "BILL_DATE", length = 255)
	public Date getBillDate() {
		return this.billDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 开票日期
	 */
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 开票金额
	 */
	@Column(name = "BILL_MONEY", length = 20)
	public Double getBillMoney() {
		return this.billMoney;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 开票金额
	 */
	public void setBillMoney(Double billMoney) {
		this.billMoney = billMoney;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 发票号
	 */
	@Column(name = "BILL_ID", length = 40)
	public String getBillId() {
		return this.billId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 发票号
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 回款日期
	 */
	@Column(name = "RECEIVED_DATE", length = 255)
	public Date getReceivedDate() {
		return this.receivedDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 回款日期
	 */
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 计划回款日期
	 */
	@Column(name = "PLAN_RECEIVED_DATE", length = 255)
	public Date getPlanReceivedDate() {
		return this.planReceivedDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 计划回款日期
	 */
	public void setPlanReceivedDate(Date planReceivedDate) {
		this.planReceivedDate = planReceivedDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得CrmContract
	 * 
	 * @return: CrmContract 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 * 方法: 设置CrmContract
	 * 
	 * @param: CrmContract 相关主表
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

	/**
	 * 方法: 取得合同总金额
	 * 
	 * @return: Double 合同总金额
	 */
	@Column(name = "TOTALMONEY", length = 50)
	public Double getTotalMoney() {
		return totalMoney;
	}

	/**
	 * 方法: 设置合同总金额
	 * 
	 * @param: Double 合同总金额
	 */
	public void setTotalMoney(Double totalMoney) {
		this.totalMoney = totalMoney;
	}

	/**
	 * 方法: 取得币种
	 * 
	 * @return: String 币种
	 */
	@Column(name = "CURRENCY", length = 50)
	public String getCurrency() {
		return currency;
	}

	/**
	 * 方法: 设置币种
	 * 
	 * @param: String 币种
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * 方法: 取得收款期数
	 * 
	 * @return: Integer 收款期数
	 */
	@Column(name = "PERIODSNUM", length = 50)
	public Integer getPeriodsNum() {
		return periodsNum;
	}

	/**
	 * 方法: 设置收款期数
	 * 
	 * @param: Integer 收款期数
	 */
	public void setPeriodsNum(Integer periodsNum) {
		this.periodsNum = periodsNum;
	}
}