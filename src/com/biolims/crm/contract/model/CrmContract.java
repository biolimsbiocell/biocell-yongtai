package com.biolims.crm.contract.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.finalProduct.model.FinalProduct;

/**
 * @Title: Model
 * @Description: 合同管理
 * @author lims-platform
 * @date 2015-08-03 11:27:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_CONTRACT")
@SuppressWarnings("serial")
public class CrmContract extends EntityDao<CrmContract> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 合同名称 */
	private String name;
	/** 合同编号 */
	private String identifiers;
	/** 客户性质 */
	private String customerProperty;
	/** 委托人 */
	private CrmCustomer crmCustomer;
	/** 法人 */
	private CrmCustomer legalPerson;
//	/** 委托人单位 */
//	private CrmCustomerDept crmCustomerDept;
	/** 签订日期 */
	private Date writeDate;
//	/** 项目编号 */
//	private Project project;
	/** 客户要求 */
	private String customerRequirements;
	/** 签订的组织机构 */
	private String organization;
	/** 终产品id */
	private String product;
	/** 终产品name */
	private String productName;
	/** 终产品 实体 */
	private FinalProduct finalProduct;
	/** 合同实施状态 */
	private DicType conState;
	/** 合同款使用状态 */
	private DicType conMoneyState;
	/** 课题类型 */
	private DicType projectType;
	/** 合同种类 */
	private DicType kind;
	/** 合同邮寄日期 */
	private Date toDate;
	/** 合同返回日期 */
	private Date fromDate;
	/** 开始日期 */
	private Date startDate;
	/** 结束日期 */
	private Date endDate;
	/** 实际合同开始日期 */
	private Date factStartDate;
	/** 实际合同结束日期 */
	private Date factEndDate;
	/** 合同类型 */
	private DicType type;
	/** 合同总金额 */
	private Double money;
//	/** 订单 */
//	private CrmSaleOrder crmSaleOrder;
//	/** 订单 */
//	private ProjectStart projectStart;
	/** 父级合同 */
	private CrmContract parent;
	/** 付款计划 */
	private CrmContrctPay crmContractPay;
	/** 客户经理 */
	private User manager;
	/** 是否备案 */
	private String isRecord;
	/** 创建人 */
	private User createUser;
	/** 实验员 */
	private User labAssistant;
	/** 创建日期 */
	private Date createDate;
	/** 审核人 */
	private User confirmUser;
	/** 审核日期 */
	private Date confirmDate;
	/** 工作流状态 */
	private String state;
	/** 工作流状态名称 */
	private String stateName;
	/** 订单数量 */
	private Integer orderNum;
	/** 订单创建 */
	private String createOrder;
	/** 委托单位 */
	private String client;
	/** 币种 */
	private String currency;
	/** 合同状态 */
	private String conStateName;
	/** 收款期数 */
	private Integer periodsNum;
	/** 产品类型 */
	private DicType productType;
	/** 业务类型 */
	private DicType workType;
	/** 种属 */
	private DicType genus;
	/** 项目来源 */
	private DicType chanceSource;
	/** 品系 */
	private DicType strain;
	/** 基因打靶类型 */
	private DicType geneTargetingType;
	/** 技术类型 */
	private DicType technologyType;
	/** 终产品数量 */
	private String productNo;
	/** 基因名称 */
	private String geneName;
	/** 基因ID(NCBI) */
	private String geneId;
	/** 基因别名 */
	private String geneByName;
	/** 是否去Neo */
	private String neoResult;
	/** Southern blot鉴定 */
	private DicType checkup;
	/** 备注 */
	private String content1;
	/** 订单接收人 */
	private String orderRecipient;
	/** content2 */
	private String content2;
	/** content3 */
	private String content3;
	/** content4 */
	private String content4;
	/** content5 */
	private Double content5;
	/** content6 */
	private Double content6;
	/** content7 */
	private Double content7;
	/** content8 */
	private Double content8;
	/** content9 */
	private Date content9;
	/** content10 */
	private Date content10;
	/** content11 */
	private Date content11;
	/** content12 */
	private Date content12;
	/** 代养目标 */
	private String generationTarget;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FINAL_PRODUCT")
	public FinalProduct getFinalProduct() {
		return finalProduct;
	}

	public void setFinalProduct(FinalProduct finalProduct) {
		this.finalProduct = finalProduct;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 项目来源
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHANGE_SOURCE")
	public DicType getChanceSource() {
		return chanceSource;
	}

	/**
	 * 项目来源
	 * 
	 * @return
	 */
	public void setChanceSource(DicType chanceSource) {
		this.chanceSource = chanceSource;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT")
//	public Project getProject() {
//		return project;
//	}
//
//	public void setProject(Project project) {
//		this.project = project;
//	}

	/**
	 * 基因ID
	 * 
	 * @return
	 */
	@Column(name = "GENE_ID", length = 100)
	public String getGeneId() {
		return geneId;
	}

	/**
	 * 基因ID
	 * 
	 * @return
	 */
	public void setGeneId(String geneId) {
		this.geneId = geneId;
	}

	/**
	 * 基因别名
	 * 
	 * @return
	 */
	@Column(name = "GENE_BY_NAME", length = 100)
	public String getGeneByName() {
		return geneByName;
	}

	/**
	 * 基因别名
	 * 
	 * @return
	 */
	public void setGeneByName(String geneByName) {
		this.geneByName = geneByName;
	}

	/**
	 * 基因名称
	 * 
	 * @return
	 */
	@Column(name = "GENE_NAME", length = 100)
	public String getGeneName() {
		return geneName;
	}

	/**
	 * 基因名称
	 * 
	 * @return
	 */
	public void setGeneName(String geneName) {
		this.geneName = geneName;
	}

//	/**
//	 * 委托人单位
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CRM_CUSTOMER_DEPT")
//	public CrmCustomerDept getCrmCustomerDept() {
//		return crmCustomerDept;
//	}

//	/**
//	 * 委托人单位
//	 */
//	public void setCrmCustomerDept(CrmCustomerDept crmCustomerDept) {
//		this.crmCustomerDept = crmCustomerDept;
//	}

	/**
	 * 客户要求
	 * 
	 * @return
	 */
	@Column(name = "CUSTOMER_REQUIREMENTS", length = 120)
	public String getCustomerRequirements() {
		return customerRequirements;
	}

	/**
	 * 客户要求
	 * 
	 * @return
	 */
	public void setCustomerRequirements(String customerRequirements) {
		this.customerRequirements = customerRequirements;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 合同名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 合同名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 合同状态
	 * 
	 * @return
	 */
	@Column(name = "CON_STATE_NAME", length = 120)
	public String getConStateName() {
		return conStateName;
	}

	/**
	 * 合同状态
	 * 
	 * @return
	 */
	public void setConStateName(String conStateName) {
		this.conStateName = conStateName;
	}

	/**
	 * 方法: 取得CrmCustomer
	 * 
	 * @return: CrmCustomer 委托人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CUSTOMER")
	public CrmCustomer getCrmCustomer() {
		return this.crmCustomer;
	}

	/**
	 * 方法: 设置CrmCustomer
	 * 
	 * @param: CrmCustomer 委托人
	 */
	public void setCrmCustomer(CrmCustomer crmCustomer) {
		this.crmCustomer = crmCustomer;
	}

	/**
	 * 方法: 取得CrmCustomer
	 * 
	 * @return:LegalPerson 法人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "LEGAL_PERSON")
	public CrmCustomer getLegalPerson() {
		return legalPerson;
	}

	public void setLegalPerson(CrmCustomer legalPerson) {
		this.legalPerson = legalPerson;
	}

	/**
	 * 签订的组织机构
	 * 
	 * @return
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * 签订的组织机构
	 * 
	 * @param organization
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * 课题类型
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECTTYPE")
	public DicType getProjectType() {
		return projectType;
	}

	/**
	 * 课题类型
	 * 
	 * @return
	 */
	public void setProjectType(DicType projectType) {
		this.projectType = projectType;
	}

	/**
	 * 合同实施状态
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONSTATE")
	public DicType getConState() {
		return conState;
	}

	/**
	 * 合同实施状态
	 * 
	 * @return
	 */
	public void setConState(DicType conState) {
		this.conState = conState;
	}

//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PRODUCT")
//	public DicType getProduct() {
//		return product;
//	}
//
//	public void setProduct(DicType product) {
//		this.product = product;
//	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 签订日期
	 */
	@Column(name = "WRITE_DATE", length = 255)
	public Date getWriteDate() {
		return this.writeDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 签订日期
	 */
	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	/**
	 * 订单创建
	 * 
	 * @return
	 */
	@Column(name = "CREATE_ORDER", length = 255)
	public String getCreateOrder() {
		return createOrder;
	}

	/**
	 * 订单创建
	 * 
	 * @return
	 */
	public void setCreateOrder(String createOrder) {
		this.createOrder = createOrder;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 开始日期
	 */
	@Column(name = "START_DATE", length = 255)
	public Date getStartDate() {
		return this.startDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 开始日期
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * 合同邮寄日期
	 * 
	 * @return
	 */
	@Column(name = "TO_DATE", length = 255)
	public Date getToDate() {
		return toDate;
	}

	/**
	 * 合同返回日期
	 * 
	 * @return
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * 合同返回日期
	 * 
	 * @return
	 */
	@Column(name = "FROM_DATE", length = 255)
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * 合同邮寄日期
	 * 
	 * @return
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 结束日期
	 */
	@Column(name = "END_DATE", length = 255)
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 结束日期
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * 实际合同开始日期
	 * 
	 * @return
	 */
	@Column(name = "FACT_START_DATE", length = 255)
	public Date getFactStartDate() {
		return factStartDate;
	}

	/**
	 * 实际合同开始日期
	 * 
	 * @return
	 */
	public void setFactStartDate(Date factStartDate) {
		this.factStartDate = factStartDate;
	}

	/**
	 * 实际合同开始日期
	 * 
	 * @return
	 */
	@Column(name = "FACT_END_DATE", length = 255)
	public Date getFactEndDate() {
		return factEndDate;
	}

	/**
	 * 实际合同开始日期
	 * 
	 * @return
	 */
	public void setFactEndDate(Date factEndDate) {
		this.factEndDate = factEndDate;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 合同类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 合同类型
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 合同种类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KIND")
	public DicType getKind() {
		return kind;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 合同种类
	 */
	public void setKind(DicType kind) {
		this.kind = kind;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 合同金额
	 */
	@Column(name = "MONEY", length = 20)
	public Double getMoney() {
		return this.money;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 合同金额
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

//	/**
//	 * 方法: 取得CrmSaleOrder
//	 * 
//	 * @return: CrmSaleOrder 订单
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CRM_SALE_ORDER")
//	public CrmSaleOrder getCrmSaleOrder() {
//		return this.crmSaleOrder;
//	}
//
//	/**
//	 * 方法: 设置CrmSaleOrder
//	 * 
//	 * @param: CrmSaleOrder 订单
//	 */
//	public void setCrmSaleOrder(CrmSaleOrder crmSaleOrder) {
//		this.crmSaleOrder = crmSaleOrder;
//	}
//
//	/**
//	 * 初步设计
//	 * 
//	 * @return
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT_START")
//	public ProjectStart getProjectStart() {
//		return projectStart;
//	}
//
//	/**
//	 * 初步设计
//	 * 
//	 * @return
//	 */
//	public void setProjectStart(ProjectStart projectStart) {
//		this.projectStart = projectStart;
//	}

	/**
	 * 方法: 取得CrmContract
	 * 
	 * @return: CrmContract 父级合同
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PARENT")
	public CrmContract getParent() {
		return this.parent;
	}

	/**
	 * 方法: 设置CrmContract
	 * 
	 * @param: CrmContract 父级合同
	 */
	public void setParent(CrmContract parent) {
		this.parent = parent;
	}

	/**
	 * 方法: 取得CrmContrctPay
	 * 
	 * @return: CrmContrctPay 付款计划
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT_PAY")
	public CrmContrctPay getCrmContractPay() {
		return this.crmContractPay;
	}

	/**
	 * 方法: 设置CrmContrctPay
	 * 
	 * @param: CrmContrctPay 付款计划
	 */
	public void setCrmContractPay(CrmContrctPay crmContractPay) {
		this.crmContractPay = crmContractPay;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 客户经理
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MANAGER")
	public User getManager() {
		return this.manager;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 客户经理
	 */
	public void setManager(User manager) {
		this.manager = manager;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否备案
	 */
	@Column(name = "IS_RECORD", length = 10)
	public String getIsRecord() {
		return this.isRecord;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否备案
	 */
	public void setIsRecord(String isRecord) {
		this.isRecord = isRecord;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审核人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审核日期
	 */
	@Column(name = "CONFIRM_DATE", length = 255)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审核日期
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态名称
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content1
	 */
	@Column(name = "CONTENT1", length = 50)
	public String getContent1() {
		return this.content1;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content1
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 * 合同状态
	 */
	@Column(name = "CONTENT2", length = 50)
	public String getContent2() {
		return this.content2;
	}

	/**
	 * 合同状态
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content3
	 */
	@Column(name = "CONTENT3", length = 50)
	public String getContent3() {
		return this.content3;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content4
	 */
	@Column(name = "CONTENT4", length = 50)
	public String getContent4() {
		return this.content4;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	@Column(name = "CUSTOMERPROPERTY", length = 50)
	public String getCustomerProperty() {
		return customerProperty;
	}

	public void setCustomerProperty(String customerProperty) {
		this.customerProperty = customerProperty;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content5
	 */
	@Column(name = "CONTENT5", length = 50)
	public Double getContent5() {
		return this.content5;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content5
	 */
	public void setContent5(Double content5) {
		this.content5 = content5;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content6
	 */
	@Column(name = "CONTENT6", length = 50)
	public Double getContent6() {
		return this.content6;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content6
	 */
	public void setContent6(Double content6) {
		this.content6 = content6;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content7
	 */
	@Column(name = "CONTENT7", length = 50)
	public Double getContent7() {
		return this.content7;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content7
	 */
	public void setContent7(Double content7) {
		this.content7 = content7;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content8
	 */
	@Column(name = "CONTENT8", length = 50)
	public Double getContent8() {
		return this.content8;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content8
	 */
	public void setContent8(Double content8) {
		this.content8 = content8;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content9
	 */
	@Column(name = "CONTENT9", length = 255)
	public Date getContent9() {
		return this.content9;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content9
	 */
	public void setContent9(Date content9) {
		this.content9 = content9;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content10
	 */
	@Column(name = "CONTENT10", length = 255)
	public Date getContent10() {
		return this.content10;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content10
	 */
	public void setContent10(Date content10) {
		this.content10 = content10;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content11
	 */
	@Column(name = "CONTENT11", length = 255)
	public Date getContent11() {
		return this.content11;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content11
	 */
	public void setContent11(Date content11) {
		this.content11 = content11;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content12
	 */
	@Column(name = "CONTENT12", length = 255)
	public Date getContent12() {
		return this.content12;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content12
	 */
	public void setContent12(Date content12) {
		this.content12 = content12;
	}

	/**
	 * 方法: 取得订单数量
	 * 
	 * @return: Integer 订单数量
	 */
	public Integer getOrderNum() {
		return orderNum;
	}

	/**
	 * 方法: 设置订单数量
	 * 
	 * @param: Interer 订单数量
	 */
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	/**
	 * 方法: 取得委托单位
	 * 
	 * @return: String 委托单位
	 */
	public String getClient() {
		return client;
	}

	/**
	 * 方法:
	 * 
	 * @param: String 订单数量
	 */
	public void setClient(String client) {
		this.client = client;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONMONEYSTATE")
	/**
	 * 取得合同款使用状态
	 * @return DicType 合同款使用状态
	 */
	public DicType getConMoneyState() {
		return conMoneyState;
	}

	/**
	 * 设置合同款使用状态
	 * 
	 * @param conMoneyState
	 */
	public void setConMoneyState(DicType conMoneyState) {
		this.conMoneyState = conMoneyState;
	}

	/**
	 * 方法: 取得币种
	 * 
	 * @return: String 币种
	 */
	@Column(name = "CURRENCY", length = 50)
	public String getCurrency() {
		return currency;
	}

	/**
	 * 方法: 设置币种
	 * 
	 * @param: String 币种
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * 方法: 取得种属
	 * 
	 * @return: DicType 种属
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENUS")
	public DicType getGenus() {
		return genus;
	}

	/**
	 * 方法: 设置种属
	 * 
	 * @param: DicType 种属
	 */
	public void setGenus(DicType genus) {
		this.genus = genus;
	}

	/**
	 * 品系
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STRAIN")
	public DicType getStrain() {
		return strain;
	}

	/**
	 * 品系
	 * 
	 * @return
	 */
	public void setStrain(DicType strain) {
		this.strain = strain;
	}

	/**
	 * 方法: 取得基因打靶类型
	 * 
	 * @return: DicType 基因打靶类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENE_TARGETING_TYPE")
	public DicType getGeneTargetingType() {
		return geneTargetingType;
	}

	/**
	 * 方法: 设置基因打靶类型
	 * 
	 * @param: DicType 基因打靶类型
	 */
	public void setGeneTargetingType(DicType geneTargetingType) {
		this.geneTargetingType = geneTargetingType;
	}

	/**
	 * 技术类型
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECHNOLOGY_TYPE")
	public DicType getTechnologyType() {
		return technologyType;
	}

	/**
	 * 技术类型
	 * 
	 * @return
	 */
	public void setTechnologyType(DicType technologyType) {
		this.technologyType = technologyType;
	}

	/**
	 * 终产品数量
	 * 
	 * @return
	 */
	@Column(name = "PRODUCT_NO", length = 100)
	public String getProductNo() {
		return productNo;
	}

	/**
	 * 终产品数量
	 * 
	 * @return
	 */
	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	/**
	 * 是否去neo
	 * 
	 * @return
	 */
	@Column(name = "NEO_RESULT", length = 10)
	public String getNeoResult() {
		return neoResult;
	}

	/**
	 * 是否去neo
	 * 
	 * @return
	 */
	public void setNeoResult(String neoResult) {
		this.neoResult = neoResult;
	}

	/**
	 * southrn blot鉴定
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHECK_UP")
	public DicType getCheckup() {
		return checkup;
	}

	/**
	 * southrn blot鉴定
	 * 
	 * @return
	 */
	public void setCheckup(DicType checkup) {
		this.checkup = checkup;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 产品类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT_TYPE")
	public DicType getProductType() {
		return this.productType;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 产品类型
	 */
	public void setProductType(DicType productType) {
		this.productType = productType;
	}

	/**
	 * 业务类型
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_TYPE")
	public DicType getWorkType() {
		return workType;
	}

	/**
	 * 业务类型
	 * 
	 * @return
	 */
	public void setWorkType(DicType workType) {
		this.workType = workType;
	}

	/**
	 * 方法: 取得收款期数
	 * 
	 * @return: Integer 收款期数
	 */
	@Column(name = "PERIODSNUM", length = 50)
	public Integer getPeriodsNum() {
		return periodsNum;
	}

	/**
	 * 方法: 设置收款期数
	 * 
	 * @param: Integer 收款期数
	 */
	public void setPeriodsNum(Integer periodsNum) {
		this.periodsNum = periodsNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 订单接收人
	 */
	@Column(name = "ORDER_RECIPIENT", length = 60)
	public String getOrderRecipient() {
		return orderRecipient;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 订单接收人
	 */
	public void setOrderRecipient(String orderRecipient) {
		this.orderRecipient = orderRecipient;
	}

	/**
	 * 合同编号
	 */
	@Column(name = "IDENTIFIER", length = 100)
	public String getIdentifiers() {
		return identifiers;
	}

	@Column(name = "IDENTIFIER", length = 100)
	public void setIdentifiers(String identifiers) {
		this.identifiers = identifiers;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 代养目标
	 */
	@Column(name = "GENERATION_TARGET", length = 200)
	public String getGenerationTarget() {
		return generationTarget;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @return: String 代养目标
	 */
	public void setGenerationTarget(String generationTarget) {
		this.generationTarget = generationTarget;
	}

}