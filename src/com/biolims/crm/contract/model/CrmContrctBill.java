package com.biolims.crm.contract.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 合同发票管理
 * @author lims-platform
 * @date 2015-08-03 11:27:24
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CRM_CONTRACT_BILL")
@SuppressWarnings("serial")
public class CrmContrctBill extends EntityDao<CrmContrctBill> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 描述 */
	private String name;
	/** 税点(%) */
	private String taxPoint;
	/** 发票抬头 */
	private String billCode;
	/** 明细 */
	private String details;
	/** 期数 */
	private Integer num;
	/** 账期 */
	private Integer accountPeriod;
	/** 开票日期 */
	private Date billDate;
	/** 希望开票日期 */
	private Date hopeBillDate;
	/** 申请开票日期 */
	private Date applyBillDate;
	/** 开票金额 */
	private Double billMoney;
	/** 发票号 */
	private String billId;
	/** 币种 */
	private String currency;
	/** 开票要求 */
	private String required;
	/** 发票类型 */
	private String billType;
	/** 技术性收入核定 */
	private String incomeCheck;
	/** 备注（前） */
	private String note;
	/** 备注（后） */
	private String note2;
	/** 注意事项 */
	private String attention;
	/** 是否开票 */
	private String isBill;
	/** 已收金额 */
	private Double receivedMoney;
	
	@Column(name = "REQUIRED", length = 36)
	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	public Double getReceivedMoney() {
		return receivedMoney;
	}

	public void setReceivedMoney(Double receivedMoney) {
		this.receivedMoney = receivedMoney;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBillCode() {
		return billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	/**
	 * 申请开票日期
	 * 
	 * @return
	 */
	@Column(name = "APPLY_BILL_DATE", length = 36)
	public Date getApplyBillDate() {
		return applyBillDate;
	}

	/**
	 * 申请开票日期
	 * 
	 * @param applyBillDate
	 */
	public void setApplyBillDate(Date applyBillDate) {
		this.applyBillDate = applyBillDate;
	}

	/**
	 * 希望开票日期
	 * 
	 * @param hopeBillDate
	 */
	@Column(name = "HOPE_BILL_DATE", length = 36)
	public Date getHopeBillDate() {
		return hopeBillDate;
	}

	/**
	 * 希望开票日期
	 * 
	 * @param hopeBillDate
	 */
	public void setHopeBillDate(Date hopeBillDate) {
		this.hopeBillDate = hopeBillDate;
	}

	public Double getBillMoney() {
		return billMoney;
	}

	public void setBillMoney(Double billMoney) {
		this.billMoney = billMoney;
	}

	/**
	 * 发票号
	 * 
	 * @return
	 */
	public String getBillId() {
		return billId;
	}

	/**
	 * 发票号
	 * 
	 * @return
	 */
	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/** 相关主表 */
	private CrmContract crmContract;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得CrmContract
	 * 
	 * @return: CrmContract 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 * 方法: 设置CrmContract
	 * 
	 * @param: CrmContract 相关主表
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 币种
	 */
	@Column(name = "CURRENCY", length = 36)
	public String getCurrency() {
		return currency;
	}

	/**
	 * 方法: 设置currency
	 * 
	 * @param: currency 币种
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * 税点(%)
	 * 
	 * @return
	 */
	@Column(name = "TAX_POINT", length = 10)
	public String getTaxPoint() {
		return taxPoint;
	}

	/**
	 * 税点(%)
	 * 
	 * @return
	 */
	public void setTaxPoint(String taxPoint) {
		this.taxPoint = taxPoint;
	}

//	/**
//	 * 方法: 取得String
//	 * 
//	 * @return: String 开票要求
//	 */
//	@Column(name = "REQUIRE", length = 36)
//	public String getRequire() {
//		return require;
//	}
//
//	/**
//	 * 方法: 设置require
//	 * 
//	 * @param: require 开票要求
//	 */
//	public void setRequire(String require) {
//		this.require = require;
//	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 发票类型
	 */
	@Column(name = "BILLTYPE", length = 36)
	public String getBillType() {
		return billType;
	}

	/**
	 * 方法: 设置billType
	 * 
	 * @param: billType 发票类型
	 */
	public void setBillType(String billType) {
		this.billType = billType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 技术收入核定
	 */
	@Column(name = "INCOMECHECK", length = 36)
	public String getIncomeCheck() {
		return incomeCheck;
	}

	/**
	 * 方法: 设置incomeCheck
	 * 
	 * @param: incomeCheck 技术收入核定
	 */
	public void setIncomeCheck(String incomeCheck) {
		this.incomeCheck = incomeCheck;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 账期
	 */
	@Column(name = "ACCOUNT_PERIOD", length = 100)
	public Integer getAccountPeriod() {
		return accountPeriod;
	}

	/**
	 * 方法: 设置accountPeriod
	 * 
	 * @param: accountPeriod 账期
	 */
	public void setAccountPeriod(Integer accountPeriod) {
		this.accountPeriod = accountPeriod;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 账期
	 */
	@Column(name = "NOTE2", length = 200)
	public String getNote2() {
		return note2;
	}

	public String getIsBill() {
		return isBill;
	}

	public void setIsBill(String isBill) {
		this.isBill = isBill;
	}

	/**
	 * 方法: 设置note2
	 * 
	 * @param: note2 备注（后）
	 */
	public void setNote2(String note2) {
		this.note2 = note2;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 注意事项
	 */
	@Column(name = "ATTENTION", length = 250)
	public String getAttention() {
		return attention;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @return: String 注意事项
	 */
	public void setAttention(String attention) {
		this.attention = attention;
	}

}