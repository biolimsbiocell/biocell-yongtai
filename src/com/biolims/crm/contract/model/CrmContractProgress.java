package com.biolims.crm.contract.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 合同课题进展
 * @author lims-platform
 * @date 2015-08-18 14:41:15
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CRM_CONTRACT_PROGRESS")
@SuppressWarnings("serial")
public class CrmContractProgress extends EntityDao<CrmContractProgress> implements java.io.Serializable {
	/**序号*/
	private String id;
	/**编号*/
	private String code;
	/**进展*/
	private String progress;
	/**完成时间*/
	private Date endDate;
	/**计划完成时间*/
	private Date planEndDate;
	/**负责人*/
	private String confirmUser;
	/**备注*/
	private String note;
	/**相关主表*/
	private CrmContract crmContract;

	/**
	 *方法: 取得String
	 *@return: String  序号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Column(name = "CODE", length = 36)
	public String getCode() {
		return this.code;
	}

	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 *方法: 取得String
	 *@return: String  进展
	 */
	@Column(name = "PROGRESS", length = 36)
	public String getProgress() {
		return this.progress;
	}

	/**
	 *方法: 设置String
	 *@param: String  进展
	 */
	public void setProgress(String progress) {
		this.progress = progress;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  完成时间
	 */
	@Column(name = "END_DATE", length = 60)
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  完成时间
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  计划完成时间
	 */
	@Column(name = "PLAN_END_DATE", length = 60)
	public Date getPlanEndDate() {
		return this.planEndDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  计划完成时间
	 */
	public void setPlanEndDate(Date planEndDate) {
		this.planEndDate = planEndDate;
	}

	/**
	 *方法: 取得String
	 *@return: String  负责人
	 */
	@Column(name = "CONFIRM_USER", length = 36)
	public String getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 *方法: 设置String
	 *@param: String  负责人
	 */
	public void setConfirmUser(String confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 *方法: 取得CrmContract
	 *@return: CrmContract  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_CONTRACT")
	public CrmContract getCrmContract() {
		return this.crmContract;
	}

	/**
	 *方法: 设置CrmContract
	 *@param: CrmContract  相关主表
	 */
	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}
}