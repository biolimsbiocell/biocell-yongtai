package com.biolims.crm.contract.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.biolims.crm.contract.model.CrmContract;
import com.biolims.crm.contract.model.CrmContractProgress;
import com.biolims.crm.contract.model.CrmContrctBill;
import com.biolims.crm.contract.model.CrmContrctCollect;
import com.biolims.crm.contract.model.CrmContrctPay;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class CrmContractDao extends BaseHibernateDao {
	public Map<String, Object> selectCrmContractList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from CrmContract where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmContract> list = new ArrayList<CrmContract>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + "order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectCrmContrctPayList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from CrmContrctPay where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmContract.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctPay> list = new ArrayList<CrmContrctPay>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 合同子表联系人
	public Map<String, Object> selectCrmContrctLinkManList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from CrmContractLinkMan where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmContract='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctPay> list = new ArrayList<CrmContrctPay>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmContrctCollectList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmContrctCollect where 1=1  ";
		String key = "";
		if (scId != null) {
			key = key + " and crmContract.id='" + scId + "'";
		} else {
			key = key + " and state = '0' ";
		}

		if (mapForQuery != null && mapForQuery.size() > 0)
			key = map2where(mapForQuery) + " and state = '0' ";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctCollect> list = new ArrayList<CrmContrctCollect>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + "order by id asc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public CrmContrctCollect findCrmContrctCollectById(String cccId)
			throws Exception {
		String hql = "from CrmContrctCollect where 1=1 ";
		String key = "";
		if (cccId != null)
			key = key + " and id='" + cccId + "'";

		CrmContrctCollect crmContrctCollect = (CrmContrctCollect) this
				.getSession().createQuery(hql + key).uniqueResult();

		return crmContrctCollect;
	}

	public Map<String, Object> selectCrmContrctBillList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmContrctBill where 1=1 and billId is not null";
		String key = "";
		if (scId != null)
			key = key + " and crmContract.id='" + scId + "'";
		if (mapForQuery != null && mapForQuery.size() > 0)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctBill> list = new ArrayList<CrmContrctBill>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by crmContract.id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public CrmContrctBill selectCrmContrctBillById(String scId)
			throws Exception {
		String hql = "from CrmContrctBill where 1=1 and billId is not null";
		String key = "";
		if (scId != null)
			key = key + " and id='" + scId + "'";

		CrmContrctBill crmContrctBill = (CrmContrctBill) this.getSession()
				.createQuery(hql + key).uniqueResult();

		return crmContrctBill;
	}

	public Map<String, Object> selectCrmContrctBillListByHostTable(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from CrmContrctBill where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmContract.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctBill> list = new ArrayList<CrmContrctBill>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmContrctNoBillList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmContrctBill where 1=1 and billId is null";
		String key = "";
		if (scId != null)
			key = key + " and crmContract.id='" + scId + "'";
		if (mapForQuery != null && mapForQuery.size() > 0)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctBill> list = new ArrayList<CrmContrctBill>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by crmContract.id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectCrmContrctBillList1(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from CrmContrctBill where 1=1 and billMoney > receivedMoney ";
		String key = "";
		if (mapForQuery != null)
			key = super.map2where(mapForQuery);
		if (scId != null)
			key = key + " and crmContract.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContrctBill> list = new ArrayList<CrmContrctBill>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 子表联系人查询
	public Map<String, Object> selectCusContractList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String userId) {
		String key = " ";
		String hql = " from CrmContract where 1=1 ";
		// if (mapForQuery != null)
		// key = map2where4Split(mapForQuery);
		if (userId != "NEW") {
			key += " and crmCustomer.id = '" + userId + "'";
		}

		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<CrmContract> list = new ArrayList<CrmContract>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 子表委托人单位查询
	// public Map<String, Object> selectCrmContractCrmCustomerList(
	// Map<String, String> mapForQuery, Integer startNum,
	// Integer limitNum, String dir, String sort, String customer_id) {
	// String key = "  " + "";
	// String hql = " from CrmContract where 1=1 ";
	// if (customer_id != null && !customer_id.equals("")) {
	// key += " and crmCustomer.id = " + customer_id;
	// }
	// Long total = (Long) this.getSession()
	// .createQuery(" select count(*) " + hql + key).uniqueResult();
	// List<CrmContract> list = new ArrayList<CrmContract>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }

	public Map<String, Object> selectCrmContractProgressList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from CrmContractProgress where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and crmContract.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContractProgress> list = new ArrayList<CrmContractProgress>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据委托人查询合同
	public Map<String, Object> selectCrmContractListByCrmCustomer(
			Integer startNum, Integer limitNum, String dir, String sort,
			String crmCustomer_Id) {
		String key = "  ";
		String hql = " from CrmContract c,CrmCustomer m where 1=1 ";
		if (crmCustomer_Id != null) {
			key = key + " and c.crmCustomer.id=m.id  and m.id='"
					+ crmCustomer_Id + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();

		List<CrmContract> list = new ArrayList<CrmContract>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery("select c " + hql + key)
						.list();
			} else {
				list = this.getSession().createQuery("select c " + hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 合同生成发票
	public List<CrmContract> setOrderList(String orderId) {
		String hql = "from CrmContract where id='" + orderId + "'";
		List<CrmContract> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 合同生成订单
	public Map<String, Object> generateOrder(String code) throws Exception {
		String hql = "from CrmContract t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and t.id='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmContract> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据合同名称查询合同
	public List selectCrmContractListByCrmContractName(String crmContractName)
			throws Exception {
		String hql = "from CrmContract a where a.name ='" + crmContractName
				+ "'";
		List list = new ArrayList();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public String map2where(Map<String, String> mapForQuery) {
		String whereStr = "";
		if (mapForQuery != null) {
			Set key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {
				String sOld = (String) it.next();
				String s = sOld;
				if ((mapForQuery.get(sOld) != null)
						&& (!(((String) mapForQuery.get(sOld)).equals("")))) {
					if (sOld.contains("##@@##")) {
						String[] sN = sOld.split("##@@##");
						s = sN[0];
					}

					String ss = (String) mapForQuery.get(sOld);

					if (ss.contains("##@@##")) {
						String[] s1 = ss.split("##@@##");

						if ((s.endsWith("Date")) || (s.endsWith("Time"))) {
							String[] a = ss.split(":");
							if (a.length == 0)
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " " + s1[1];

							if (a.length == 1)
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " to_date('" + s1[1]
										+ "','yyyy-MM-dd')";

							if (a.length == 2) {
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " to_date('" + s1[1]
										+ "','yyyy-MM-dd HH24:mi')";
							}

							if (a.length == 3)
								whereStr = whereStr + " and " + s + " " + s1[0]
										+ " to_date('" + s1[1]
										+ "','yyyy-MM-dd HH24:mi:ss')";

						} else {
							whereStr = whereStr + " and " + s + " " + s1[0]
									+ " " + s1[1];
						}
					} else {
						String whereS = s;

						if ((!(((String) mapForQuery.get(s)).startsWith("%")))
								&& (!(((String) mapForQuery.get(s))
										.endsWith("%"))))
							whereS = whereS + " = '"
									+ ((String) mapForQuery.get(s)) + "'";
						else {
							whereS = "lower("
									+ whereS
									+ ") like '"
									+ ((String) mapForQuery.get(s))
											.toLowerCase() + "'";
						}

						whereStr = whereStr + " and " + whereS;
					}
				}
			}

		}
		return whereStr;
	}

}