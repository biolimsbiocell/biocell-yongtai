package com.biolims.crm.contract.action;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.contract.dao.CrmContractDao;
import com.biolims.crm.contract.model.CrmContract;
import com.biolims.crm.contract.model.CrmContractLinkMan;
import com.biolims.crm.contract.model.CrmContrctBill;
import com.biolims.crm.contract.model.CrmContrctCollect;
import com.biolims.crm.contract.model.CrmContrctPay;
import com.biolims.crm.contract.service.CrmContractService;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.service.CrmLinkManService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/crm/contract/crmContract")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmContractAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "5";
	@Autowired
	private CrmContractService crmContractService;
	@Resource
	private CrmLinkManService crmLinkManService;
	private CrmContract crmContract = new CrmContract();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private CrmSaleOrderService crmSaleOrderService;
//	@Resource
//	private ProjectService projectService;

	@Action(value = "showCrmContractList")
	public String showCrmContractList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/contract/crmContract.jsp");
	}

	// 选择合同
	@Action(value = "selectCrmContract")
	public String selectCrmContract() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/contract/crmSelectContract.jsp");
	}

	@Action(value = "showCrmContractListJson")
	public void showCrmContractListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmContractService.findCrmContractList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmContract> list = (List<CrmContract>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("identifiers", "");
		map.put("geneByName", "");
		map.put("geneName", "");
		map.put("geneId", "");
		map.put("createOrder", "");
		map.put("conStateName", "");
//		map.put("project-id", "");
//		map.put("project-name", "");
//		map.put("project-projectId", "");
//		map.put("projectType-id", "");
//		map.put("projectType-name", "");
		map.put("conState-id", "");
		map.put("conState-name", "");
		map.put("conMoneyState-id", "");
		map.put("conMoneyState-name", "");
		map.put("customerProperty", "");
		map.put("customerRequirements", "");
		map.put("chanceSource-id", "");
		map.put("chanceSource-name", "");
		map.put("productType-id", "");
		map.put("productType-name", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("geneTargetingType-id", "");
		map.put("geneTargetingType-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("productNo", "");
		map.put("neoResult", "");
		map.put("checkup-id", "");
		map.put("checkup-name", "");
		map.put("fromDate", "yyyy-MM-dd");
		map.put("toDate", "yyyy-MM-dd");

		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmCustomer-selA", "");
//		map.put("crmCustomerDept-id", "");
//		map.put("crmCustomerDept-name", "");
		map.put("writeDate", "yyyy-MM-dd");
		map.put("startDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("factStartDate", "yyyy-MM-dd");
		map.put("factEndDate", "yyyy-MM-dd");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("money", "");
		map.put("organization", "");
		
//		map.put("product-id", "");
//		map.put("product-name", "");
		map.put("product", "");
		map.put("productName", "");
		
//		map.put("crmSaleOrder-id", "");
//		map.put("crmSaleOrder-name", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("crmContractPay-id", "");
		map.put("crmContractPay-name", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("isRecord", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		map.put("orderNum", "");
		map.put("client", "");
		map.put("currency", "");
		map.put("periodsNum", "");
		map.put("orderRecipient", "");
		map.put("kind-id", "");
		map.put("kind-name", "");
		
		map.put("finalProduct-id", "");
		map.put("finalProduct-name", "");
		map.put("productNo", "");
		map.put("finalProduct-dicSampleType-id", "");
		map.put("finalProduct-dicSampleType-name", "");
		map.put("finalProduct-pronoun", "");
		
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "crmContractSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmContractList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/contract/crmContractDialog.jsp");
	}

	@Action(value = "showDialogCrmContractListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmContractListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data").toLowerCase();
		String customer = getRequest().getParameter("customId");
		putObjToContext("crmCustomer.id", customer);
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = crmContractService.findCrmContractList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<CrmContract> list = (List<CrmContract>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("identifiers", "");
		map.put("geneByName", "");
		map.put("geneName", "");
		map.put("geneId", "");
		map.put("createOrder", "");
		map.put("conStateName", "");
//		map.put("project-id", "");
//		map.put("project-name", "");
//		map.put("project-projectId", "");
//		map.put("projectType-id", "");
//		map.put("projectType-name", "");
		map.put("conState-id", "");
		map.put("conState-name", "");
		map.put("conMoneyState-id", "");
		map.put("conMoneyState-name", "");
		map.put("customerProperty", "");
		map.put("customerRequirements", "");
		map.put("chanceSource-id", "");
		map.put("chanceSource-name", "");
		map.put("productType-id", "");
		map.put("productType-name", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("geneTargetingType-id", "");
		map.put("geneTargetingType-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("productNo", "");
		map.put("neoResult", "");
		map.put("checkup-id", "");
		map.put("checkup-name", "");
		map.put("fromDate", "yyyy-MM-dd");
		map.put("toDate", "yyyy-MM-dd");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmCustomer-selA", "");
//		map.put("crmCustomerDept-id", "");
//		map.put("crmCustomerDept-name", "");
		map.put("writeDate", "yyyy-MM-dd");
		map.put("startDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("factStartDate", "yyyy-MM-dd");
		map.put("factEndDate", "yyyy-MM-dd");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("money", "");
		map.put("organization", "");
		
//		map.put("product-id", "");
//		map.put("product-name", "");
		map.put("product", "");
		map.put("productName", "");
		
//		map.put("crmSaleOrder-id", "");
//		map.put("crmSaleOrder-name", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("crmContractPay-id", "");
		map.put("crmContractPay-name", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("isRecord", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		map.put("orderNum", "");
		map.put("client", "");
		map.put("currency", "");
		map.put("periodsNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "crmContractByCustomerSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogCrmContractByCustomerList() throws Exception {
		String customer = getParameterFromRequest("customId");
		putObjToContext("customId", customer);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/crm/customer/customerdept/crmContractByCustomerDialog.jsp");
	}

	@Action(value = "showCrmContractByCustomerJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContractJsonByCustomer() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		// 委托人id
		String customer = getParameterFromRequest("customId");
		Map<String, String> mapQuery = new HashMap<String, String>();
		Map<String, Object> result = crmContractService
				.findCrmContrctCustomerList(customer, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<CrmContract> list = (List<CrmContract>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("identifiers", "");
		map.put("geneByName", "");
		map.put("geneName", "");
		map.put("geneId", "");
		map.put("createOrder", "");
		map.put("conStateName", "");
//		map.put("project-id", "");
//		map.put("project-name", "");
//		map.put("project-projectId", "");
//		map.put("projectType-id", "");
//		map.put("projectType-name", "");
		map.put("conState-id", "");
		map.put("conState-name", "");
		map.put("conMoneyState-id", "");
		map.put("conMoneyState-name", "");
		map.put("customerProperty", "");
		map.put("customerRequirements", "");
		map.put("chanceSource-id", "");
		map.put("chanceSource-name", "");
		map.put("productType-id", "");
		map.put("productType-name", "");
		map.put("workType-id", "");
		map.put("workType-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("geneTargetingType-id", "");
		map.put("geneTargetingType-name", "");
		map.put("technologyType-id", "");
		map.put("technologyType-name", "");
		map.put("productNo", "");
		map.put("neoResult", "");
		map.put("checkup-id", "");
		map.put("checkup-name", "");
		map.put("fromDate", "yyyy-MM-dd");
		map.put("toDate", "yyyy-MM-dd");
		map.put("crmCustomer-id", "");
		map.put("crmCustomer-name", "");
		map.put("crmCustomer-selA", "");
//		map.put("crmCustomerDept-id", "");
//		map.put("crmCustomerDept-name", "");
		map.put("writeDate", "yyyy-MM-dd");
		map.put("startDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("factStartDate", "yyyy-MM-dd");
		map.put("factEndDate", "yyyy-MM-dd");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("money", "");
		map.put("organization", "");
		
//		map.put("product-id", "");
//		map.put("product-name", "");
		map.put("product", "");
		map.put("productName", "");
		
//		map.put("crmSaleOrder-id", "");
//		map.put("crmSaleOrder-name", "");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("crmContractPay-id", "");
		map.put("crmContractPay-name", "");
		map.put("manager-id", "");
		map.put("manager-name", "");
		map.put("isRecord", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		map.put("orderNum", "");
		map.put("client", "");
		map.put("currency", "");
		map.put("periodsNum", "");
		map.put("kind-id", "");
		map.put("kind-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editCrmContract")
	public String editCrmContract() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			crmContract = crmContractService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "crmContract");
		} else {
			crmContract.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			crmContract.setCreateUser(user);
			crmContract.setCreateDate(new Date());
			crmContract
					.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
			crmContract
					.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(crmContract.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/crm/contract/crmContractEdit.jsp");
	}

	@Action(value = "copyCrmContract")
	public String copyCrmContract() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		crmContract = crmContractService.get(id);
		crmContract.setId("NEW");
		crmContract.setState("3");
		crmContract.setStateName("新建");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/crm/contract/crmContractEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = crmContract.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "CrmContract";
			String autoID = systemCodeService.getCodeByPrefix("CrmContract", "SKHT",
					00000, 5, null);//projectService.findAutoID(modelName, 5);
			crmContract.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("crmContrctPay", getParameterFromRequest("crmContrctPayJson"));
		aMap.put("crmContrctBill",
				getParameterFromRequest("crmContrctBillJson"));
		aMap.put("crmContractLinkMan",
				getParameterFromRequest("crmContractLinkManJson"));
		aMap.put("crmContrctCollect",
				getParameterFromRequest("crmContrctCollectJson"));
		crmContractService.save(crmContract, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/crm/contract/crmContract/editCrmContract.action?id="
				+ crmContract.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewCrmContract")
	public String toViewCrmContract() throws Exception {
		String id = getParameterFromRequest("id");
		crmContract = crmContractService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/crm/contract/crmContractEdit.jsp");
	}

	/**
	 * 访问 任务树
	 */
	@Action(value = "showCrmContractTree")
	public String showCrmContractTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/crm/contract/crmContract/showCrmContractTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/crm/contract/crmContractTree.jsp");
	}

	@Action(value = "showCrmContractTreeJson")
	public void showCrmContractTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		List<CrmContract> aList = null;
		if (upId.equals("")) {

			aList = crmContractService.findCrmContractList();
		} else {

			aList = crmContractService.findCrmContractList(upId);
		}

		String a = crmContractService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showCrmContractChildTreeJson")
	public void showCrmContractChildTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<CrmContract> aList = crmContractService.findCrmContractList(upId);
		String a = crmContractService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showCrmContrctPayList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctPayList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContrctPay.jsp");
	}

	@Action(value = "showCrmContrctPayDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctPayDialogList() throws Exception {
		String id = getRequest().getParameter("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/crm/contract/crmContrctPayDialog.jsp");
	}

	// 查询发票对话框
	@Action(value = "showCrmContrctBillDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctBillDialogList() throws Exception {
		// String id = getRequest().getParameter("id");
		// putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/crm/contract/crmContrctBillDialog.jsp");
	}

	@Action(value = "showDialogCrmContrctBillListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogCrmContrctBillListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctBillList1(map2Query, scId, startNum,
							limitNum, dir, sort);
			// Map<String, Object> result =
			// crmContractService.findCrmContrctBillList(scId, startNum,
			// limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctBill> list = (List<CrmContrctBill>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("applyBillDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("receivedMoney", "");
			map.put("billCode", "");
			map.put("billId", "");
			map.put("details", "");
			map.put("num", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("currency", "");
			map.put("required", "");
			map.put("billType", "");
			map.put("incomeCheck", "");
			map.put("accountPeriod", "");

			map.put("crmContract-identifiers", "");
			map.put("crmContract-type-id", "");
			map.put("crmContract-type-name", "");
			map.put("crmContract-writeDate", "yyyy-MM-dd");
			map.put("crmContract-crmCustomer", "");
			map.put("crmContract-crmCustomer-name", "");
			map.put("crmContract-crmCustomer-id", "");
			map.put("crmContract-periodsNum", "");
			map.put("crmContract-manager-name", "");
			map.put("crmContract-content1", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmContrctPayListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContrctPayListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctPayList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctPay> list = (List<CrmContrctPay>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("projectNode-id", "");
			map.put("projectNode-progress", "");
			map.put("money", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("billId", "");
			map.put("num", "");
			map.put("remind", "");
			map.put("request", "");
			map.put("scaling", "");
			map.put("receivedDate", "yyyy-MM-dd");
			map.put("planReceivedDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("totalMoney", "");
			map.put("currency", "");
			map.put("periodsNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmContrctCollectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctCollectList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContrctCollect.jsp");
	}

	// 查询回款明细
	@Action(value = "showCrmContractCollectItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctCollectItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContrctCollectItem.jsp");
	}

	@Action(value = "showCrmContrctCollectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContrctCollectListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data").toLowerCase();
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctCollectList(map2Query, scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctCollect> list = (List<CrmContrctCollect>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("money", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("billId", "");
			map.put("num", "");
			map.put("remind", "");
			map.put("request", "");
			map.put("scaling", "");
			map.put("receivedDate", "yyyy-MM-dd");
			map.put("planReceivedDate", "yyyy-MM-dd");
			map.put("note", "");

			map.put("crmContrctPay-name", "");
			map.put("crmContrctPay-id", "");
			map.put("crmContrctPay-projectNode-id", "");
			map.put("crmContrctPay-projectNode-progress", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("collectNum", "");
			map.put("crmContract-periodsNum", "");
			map.put("crmContract-manager-id", "");
			map.put("crmContract-manager-name", "");
			// map.put("crmContract-type-id", "");
			// map.put("crmContract-type-name", "");
			// map.put("crmContract-writeDate", "yyyy-MM-dd");
			// map.put("crmContract-crmCustomer-id", "");
			// map.put("crmContract-crmCustomer-name", "");
			// map.put("crmContract-content1", "");
			map.put("note2", "");
			map.put("billState", "");
			map.put("collectType", "");
			map.put("collectDept", "");
			map.put("state", "");
			map.put("brokerage", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmContrctBillList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctBillList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContrctBill.jsp");
	}

	// 发票管理
	@Action(value = "showCrmContractBillItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctBillItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContractBillItem.jsp");
	}

	@Action(value = "showCrmContrctBillListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContrctBillListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data").toLowerCase();
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctBillList(map2Query, scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctBill> list = (List<CrmContrctBill>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("applyBillDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("receivedMoney", "");
			map.put("billCode", "");
			map.put("billId", "");
			map.put("details", "");
			map.put("num", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("currency", "");
			map.put("required", "");
			map.put("billType", "");
			map.put("incomeCheck", "");
			map.put("accountPeriod", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showCrmContrctBillListByHostTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContrctBillListByHostTableJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctBillListByHostTable(scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctBill> list = (List<CrmContrctBill>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("applyBillDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("receivedMoney", "");
			map.put("billCode", "");
			map.put("billId", "");
			map.put("details", "");
			map.put("num", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("currency", "");
			map.put("required", "");
			map.put("billType", "");
			map.put("incomeCheck", "");
			map.put("accountPeriod", "");
			map.put("isBill", "");
			map.put("attention", "");// 注意事项
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Action(value = "showCrmContractOrderList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmContractOrderList() throws Exception {
//		return dispatcher("/WEB-INF/page/crm/contract/crmContractOrder.jsp");
//	}
//
//	@Action(value = "showCrmContractOrderListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmContractOrderListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, String> mapForQuery = new HashMap<String, String>();
//			Map<String, Object> result = crmSaleOrderService
//					.findCrmSaleOrderListById(mapForQuery, startNum, limitNum,
//							dir, sort, scId);
//			Long total = (Long) result.get("total");
//			List<CrmSaleOrder> list = (List<CrmSaleOrder>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("name", "");
//			map.put("createUser-id", "");
//			map.put("createUser-name", "");
//			map.put("createDate", "yyyy-MM-dd");
//			// map.put("planEndDate", "yyyy-MM-dd");
//			map.put("note", "");
//			map.put("crmContract-name", "");
//			map.put("crmContract-id", "");
//			new SendData().sendDateJson(map, list, total,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCrmContractProgress")
	public void delCrmContractProgress() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmContractService.delCrmContractProgress(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 检索合同名称
	@Action(value = "checkNameDuplicate")
	public void checkNameDuplicate() throws Exception {
		String crmContractName = getParameterFromRequest("crmContractName");
		Map aMap = new HashMap();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			boolean flag = crmContractService
					.checkNameDuplicate(crmContractName);
			if (flag) {
				result.put("success", false);
			} else {
				result.put("success", true);
			}
		} catch (Exception e) {
			// TODO: handle exception
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

//	// 带出联系人
//	@Action(value = "showCrmContractLinkManList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showCrmContractLinkManList() throws Exception {
//		String wid = getParameterFromRequest("wid");
//		putObjToContext("wid", wid);
//		return dispatcher("/WEB-INF/page/crm/contract/crmContractLinkMan.jsp");
//	}
//
//	@Action(value = "showCrmContractLinkManListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showCrmContractLinkManListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String code = getParameterFromRequest("id");
//			Map<String, Object> result = crmLinkManService
//					.findCrmLinkManListByCId(code, startNum, limitNum, dir,
//							sort);//
//			Long count = (Long) result.get("total");
//			List<CrmContractLinkMan> list = (List<CrmContractLinkMan>) result
//					.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("name", "");
//			map.put("code", "");
//			map.put("email", "");
//			map.put("trans", "");
//			map.put("deptName", "");
//			map.put("ktEmail", "");
//			map.put("phone", "");
//			map.put("tel", "");
//			map.put("crmlinkMan-id", "");
//			map.put("crmlinkMan-name", "");
//			map.put("crmContract-id", "");
//			map.put("crmContract-name", "");
//			new SendData().sendDateJson(map, list, count,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCrmContrctPay")
	public void delCrmContrctPay() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmContractService.delCrmContrctPay(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCrmContrctCollect")
	public void delCrmContrctCollect() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmContractService.delCrmContrctCollect(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCrmContrctBill")
	public void delCrmContrctBill() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmContractService.delCrmContrctBill(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmContractService getCrmContractService() {
		return crmContractService;
	}

	public void setCrmContractService(CrmContractService crmContractService) {
		this.crmContractService = crmContractService;
	}

	public CrmContract getCrmContract() {
		return crmContract;
	}

	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

//	// 由客户带出联系人
//	@Action(value = "showLinkManList")
//	public void showLinkManList() throws Exception {
//		String code = getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.crmLinkManService
//					.showLinkManList(code);
//			result.put("success", true);
//			result.put("data", dataListMap);
//
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

//	// 由合同生成订单
//	@Action(value = "generateOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void generateOrder() throws Exception {
//		String esId = getRequest().getParameter("Id");
//		String orderNum = getRequest().getParameter("orderNum");
//		Integer order = Integer.valueOf(orderNum);
//		int od = order.intValue();
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			boolean dataListMap = this.crmContractService.generateOrder(esId,
//					od);// mapForQuery,
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	// 由合同生成发票
	@Action(value = "editBillList")
	public String editBillList() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");

		crmContractService.saveToBill(id);
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		// toSetStateCopy();
		return redirect("/crm/contract/crmContract/editCrmContract.action?id="
				+ id);
	}

	// 保存回款明细信息
	@Action(value = "saveContrctCollectItem")
	public String saveContrctCollectItem() throws Exception {
		Map aMap = new HashMap();
		aMap.put("crmContrctCollectItem",
				getParameterFromRequest("crmContrctCollectItemJson"));
		crmContractService.save(null, aMap);
		return redirect("/crm/contract/crmContract/showCrmContractCollectItemList.action");
	}

	// 保存发票管理信息
	@Action(value = "saveContrctBillItem")
	public String saveContrctBillItem() throws Exception {
		Map aMap = new HashMap();
		aMap.put("crmContrctBillItem",
				getParameterFromRequest("crmContrctBillItemJson"));
		crmContractService.save(null, aMap);
		return redirect("/crm/contract/crmContract/showCrmContractBillItemList.action");
	}

	// 保存大子表发票管理信息
	@Action(value = "saveContrctBillItemOutside")
	public void saveContrctBillItemOutside() throws Exception {
		Map aMap = new HashMap();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			aMap.put("crmContrctBillItem",
					getParameterFromRequest("crmContrctBillItemJson"));
			crmContractService.save(null, aMap);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据发票数分割回款
	@Action(value = "saveContrctCollectItemCut")
	public void saveContrctCollectItemCut() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String ids = getParameterFromRequest("ids");
		String cccId = getParameterFromRequest("crmContrctCollect");
		String[] idsArr = ids.split(",");

		try {
			CrmContrctCollect ccc = crmContractService
					.findCrmContrctCollectById(cccId);
			for (String id : idsArr) {
				CrmContrctBill crmContrctBill = crmContractService
						.findCrmContrctBillById(id);
				crmContrctBill.setReceivedMoney(crmContrctBill.getBillMoney());
				crmContractService.saveCrmContrctBill(crmContrctBill);
				CrmContrctCollect crmContrctCollect = new CrmContrctCollect();

				crmContrctCollect.setCrmContract(crmContrctBill
						.getCrmContract());
				crmContrctCollect.setMoney(crmContrctBill.getBillMoney());
				crmContrctCollect.setBillId(crmContrctBill.getBillId());
				crmContrctCollect.setBillMoney(crmContrctBill.getBillMoney());
				crmContrctCollect.setReceivedDate(ccc.getReceivedDate());
				crmContrctCollect.setCollectDept(ccc.getCollectDept());
				crmContrctCollect.setCollectType(ccc.getCollectType());
				crmContrctCollect.setNote("总金额:" + ccc.getMoney());
				crmContrctCollect.setState("1");
				crmContractService.saveCrmContrctCollect(crmContrctCollect);
			}
			ccc.setNote2("");
			ccc.setState("0");
			crmContractService.saveCrmContrctCollect(ccc);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据发票金额判断一个回款多几个发票
	@Action(value = "saveContrctCollectItemOne")
	public void saveContrctCollectItemOne() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String ids = getParameterFromRequest("ids");
		String cccId = getParameterFromRequest("crmContrctCollect");

		try {
			CrmContrctCollect ccc = crmContractService
					.findCrmContrctCollectById(cccId);
			CrmContrctBill crmContrctBill = crmContractService
					.findCrmContrctBillById(ids);
			if (ccc.getMoney() <= crmContrctBill.getBillMoney()) {
				crmContrctBill.setReceivedMoney(crmContrctBill
						.getReceivedMoney() + ccc.getMoney());
				crmContractService.saveCrmContrctBill(crmContrctBill);
			} else if (ccc.getMoney() == crmContrctBill.getBillMoney()) {
				crmContrctBill.setReceivedMoney(ccc.getMoney());
				crmContractService.saveCrmContrctBill(crmContrctBill);
			}

			ccc.setNote2("");
			ccc.setState("0");
			crmContractService.saveCrmContrctCollect(ccc);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 保存大子表回款管理信息
	@Action(value = "saveContrctCollectItemOutside")
	public void saveContrctCollectItemOutside() throws Exception {
		Map aMap = new HashMap();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			aMap.put("crmContrctCollectItem",
					getParameterFromRequest("crmContrctCollectItemJson"));
			crmContractService.save(null, aMap);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
