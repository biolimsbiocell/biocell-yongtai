package com.biolims.crm.contract.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.contract.model.CrmContract;
import com.biolims.crm.contract.model.CrmContrctBill;
import com.biolims.crm.contract.model.CrmContrctCollect;
import com.biolims.crm.contract.model.CrmContrctPay;
import com.biolims.crm.contract.service.CrmContractService;
import com.biolims.crm.customer.linkman.model.CrmLinkMan;
import com.biolims.crm.customer.linkman.service.CrmLinkManService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/crm/contract/crmContractBill")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CrmContractBillAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "5001";
	@Autowired
	private CrmContractService crmContractService;
	@Resource
	private CrmLinkManService crmLinkManService;
	private CrmContract crmContract = new CrmContract();
	@Resource
	private FileInfoService fileInfoService;
//	@Resource
//	private CrmSaleOrderService crmSaleOrderService;
//	@Resource
//	private ProjectService projectService;

	// //发票管理
	// @Action(value = "showCrmContractBillItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showCrmContrctBillItemList() throws Exception {
	// return dispatcher("/WEB-INF/page/crm/contract/crmContractBillItem.jsp");
	// }

	// 发票管理
	@Action(value = "showCrmContractBillItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContrctBillItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContractBillItem.jsp");
	}

	@Action(value = "showCrmContrctBillListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContrctBillListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctBillList(map2Query, scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctBill> list = (List<CrmContrctBill>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("applyBillDate", "yyyy-MM-dd");
			map.put("hopeBillDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("receivedMoney", "");
			map.put("billCode", "");
			map.put("billId", "");
			map.put("taxPoint", "");
			map.put("details", "");
			map.put("num", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("crmContract-type-id", "");
			map.put("crmContract-manager-id", "");
			map.put("crmContract-manager-name", "");
			map.put("crmContract-crmCustomer-id", "");
			map.put("crmContract-crmCustomer-name", "");
			map.put("crmContract-writeDate", "");
			map.put("crmContract-type-name", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("currency", "");
			map.put("required", "");
			map.put("billType", "");
			map.put("incomeCheck", "");
			map.put("accountPeriod", "");
			map.put("isBill", "");
			map.put("attention", "");// 注意事项
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 发票管理
	@Action(value = "showCrmContractNoBillItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCrmContractNoBillItemList() throws Exception {
		return dispatcher("/WEB-INF/page/crm/contract/crmContractNoBillItem.jsp");
	}

	@Action(value = "showCrmContrctNoBillListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCrmContrctNoBillListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = crmContractService
					.findCrmContrctNoBillList(map2Query, scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<CrmContrctBill> list = (List<CrmContrctBill>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("billDate", "yyyy-MM-dd");
			map.put("applyBillDate", "yyyy-MM-dd");
			map.put("hopeBillDate", "yyyy-MM-dd");
			map.put("billMoney", "");
			map.put("receivedMoney", "");
			map.put("billCode", "");
			map.put("billId", "");
			map.put("taxPoint", "");
			map.put("details", "");
			map.put("num", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("crmContract-type-id", "");
			map.put("crmContract-type-name", "");
			map.put("crmContract-name", "");
			map.put("crmContract-id", "");
			map.put("currency", "");
			map.put("required", "");
			map.put("billType", "");
			map.put("incomeCheck", "");
			map.put("accountPeriod", "");
			map.put("isBill", "");
			map.put("attention", "");// 注意事项
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCrmContrctBill")
	public void delCrmContrctBill() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			crmContractService.delCrmContrctBill(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CrmContractService getCrmContractService() {
		return crmContractService;
	}

	public void setCrmContractService(CrmContractService crmContractService) {
		this.crmContractService = crmContractService;
	}

	public CrmContract getCrmContract() {
		return crmContract;
	}

	public void setCrmContract(CrmContract crmContract) {
		this.crmContract = crmContract;
	}

}
