package com.biolims.crm.contract.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.crm.contract.dao.CrmContractDao;
import com.biolims.crm.contract.model.CrmContract;
import com.biolims.crm.contract.model.CrmContractLinkMan;
import com.biolims.crm.contract.model.CrmContractProgress;
import com.biolims.crm.contract.model.CrmContrctBill;
import com.biolims.crm.contract.model.CrmContrctCollect;
import com.biolims.crm.contract.model.CrmContrctPay;
import com.biolims.crm.sale.model.CrmSaleBill;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CrmContractService {
	@Resource
	private CrmContractDao crmContractDao;
	@Resource
	private CommonDAO commonDAO;
//	@Resource
//	private CrmSaleOrderDao crmSaleOrderDao;
	@Resource
	private UserGroupService userGroupService;
	@Resource
	private SystemCodeService systemCodeService;
//	@Resource
//	private ProjectService projectService;
	StringBuffer json = new StringBuffer();

	// 子表联系人查询
	public Map<String, Object> findCusContractList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String userId) {
		return crmContractDao.selectCusContractList(mapForQuery, startNum,
				limitNum, dir, sort, userId);
	}

	// 子表委托人单位查询
	// public Map<String, Object> findCrmContractCrmCustomerList(
	// Map<String, String> mapForQuery, Integer startNum,
	// Integer limitNum, String dir, String sort, String customer_id) {
	// return crmContractDao.selectCrmContractCrmCustomerList(mapForQuery,
	// startNum, limitNum, dir, sort, customer_id);
	// }

	public Map<String, Object> findCrmContractList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return crmContractDao.selectCrmContractList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findCrmContractProgressList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = crmContractDao
				.selectCrmContractProgressList(scId, startNum, limitNum, dir,
						sort);
		List<CrmContractProgress> list = (List<CrmContractProgress>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmContractProgress(CrmContract sc, String itemDataJson)
			throws Exception {
		List<CrmContractProgress> saveItems = new ArrayList<CrmContractProgress>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContractProgress scp = new CrmContractProgress();
			// 将map信息读入实体类
			scp = (CrmContractProgress) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmContract(sc);

			saveItems.add(scp);
		}
		crmContractDao.saveOrUpdateAll(saveItems);
	}

	// 审批完成
	public void chengeState(String applicationTypeActionId, String id) {
		CrmContract sct = crmContractDao.get(CrmContract.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmUser(user);
		sct.setConfirmDate(new Date());

		crmContractDao.update(sct);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmContractProgress(String[] ids) throws Exception {
		for (String id : ids) {
			CrmContractProgress scp = crmContractDao.get(
					CrmContractProgress.class, id);
			crmContractDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmContract i) throws Exception {

		crmContractDao.saveOrUpdate(i);

	}

	public CrmContract get(String id) {
		CrmContract crmContract = commonDAO.get(CrmContract.class, id);
		return crmContract;
	}

	public Map<String, Object> findCrmContrctPayList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = crmContractDao.selectCrmContrctPayList(
				scId, startNum, limitNum, dir, sort);
		List<CrmContrctPay> list = (List<CrmContrctPay>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmContrctCollectList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmContractDao
				.selectCrmContrctCollectList(mapForQuery, scId, startNum,
						limitNum, dir, sort);
		List<CrmContrctCollect> list = (List<CrmContrctCollect>) result
				.get("list");
		return result;
	}

	public CrmContrctCollect findCrmContrctCollectById(String cccId)
			throws Exception {
		CrmContrctCollect crmContrctCollect = crmContractDao
				.findCrmContrctCollectById(cccId);

		return crmContrctCollect;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmContrctPay(CrmContract sc, String itemDataJson)
			throws Exception {
		List<CrmContrctPay> saveItems = new ArrayList<CrmContrctPay>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContrctPay scp = new CrmContrctPay();
			// 将map信息读入实体类
			scp = (CrmContrctPay) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmContract(sc);

			saveItems.add(scp);
		}
		crmContractDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmContrctPay(String[] ids) throws Exception {
		for (String id : ids) {
			CrmContrctPay scp = crmContractDao.get(CrmContrctPay.class, id);
			crmContractDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmContrctCollect(CrmContract sc, String itemDataJson)
			throws Exception {
		List<CrmContrctCollect> saveItems = new ArrayList<CrmContrctCollect>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContrctCollect scp = new CrmContrctCollect();
			// 将map信息读入实体类
			scp = (CrmContrctCollect) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmContract(sc);

			saveItems.add(scp);
		}
		crmContractDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmContrctCollect(CrmContrctCollect crmContrctCollect)
			throws Exception {

		crmContractDao.saveOrUpdate(crmContrctCollect);
	}

	// 保存合同回款信息
	public void saveCrmContrctCollectItem(CrmContract sc, String itemDataJson)
			throws Exception {
		List<CrmContrctCollect> saveItems = new ArrayList<CrmContrctCollect>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContrctCollect scp = new CrmContrctCollect();
			// 将map信息读入实体类
			scp = (CrmContrctCollect) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (sc != null)
				scp.setCrmContract(sc);
			crmContractDao.saveOrUpdate(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmContrctCollect(String[] ids) throws Exception {
		for (String id : ids) {
			CrmContrctCollect scp = crmContractDao.get(CrmContrctCollect.class,
					id);
			crmContractDao.delete(scp);
		}
	}

	public Map<String, Object> findCrmContrctBillList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmContractDao.selectCrmContrctBillList(
				mapForQuery, scId, startNum, limitNum, dir, sort);
		List<CrmContrctBill> list = (List<CrmContrctBill>) result.get("list");
		return result;
	}

	public CrmContrctBill findCrmContrctBillById(String scId) throws Exception {
		CrmContrctBill result = crmContractDao.selectCrmContrctBillById(scId);
		return result;
	}

	public Map<String, Object> findCrmContrctBillListByHostTable(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = crmContractDao
				.selectCrmContrctBillListByHostTable(scId, startNum, limitNum,
						dir, sort);
		List<CrmContrctBill> list = (List<CrmContrctBill>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmContrctNoBillList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmContractDao.selectCrmContrctNoBillList(
				mapForQuery, scId, startNum, limitNum, dir, sort);
		List<CrmContrctBill> list = (List<CrmContrctBill>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmContrctBillList1(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = crmContractDao.selectCrmContrctBillList1(
				mapForQuery, scId, startNum, limitNum, dir, sort);
		List<CrmContrctBill> list = (List<CrmContrctBill>) result.get("list");
		return result;
	}

	public Map<String, Object> findCrmContrctCustomerList(String customer_Id,
			Integer startNum, Integer limitNum, String dir, String sort) {
		Map<String, Object> result = crmContractDao
				.selectCrmContractListByCrmCustomer(startNum, limitNum, dir,
						sort, customer_Id);
		return result;
	}

	// 保存合同付款计划
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmContrctBill(CrmContract sc, String itemDataJson)
			throws Exception {
		List<CrmContrctBill> saveItems = new ArrayList<CrmContrctBill>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContrctBill scp = new CrmContrctBill();
			// 将map信息读入实体类
			scp = (CrmContrctBill) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmContract(sc);

			saveItems.add(scp);
		}
		crmContractDao.saveOrUpdateAll(saveItems);
	}

	// 保存合同付款计划
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCrmContrctBill(CrmContrctBill crmContrctBill)
			throws Exception {

		crmContractDao.saveOrUpdate(crmContrctBill);
	}

	// 保存合同发票管理
	public void saveCrmContrctBillItem(CrmContract sc, String itemDataJson)
			throws Exception {
		// List<CrmContrctBill> saveItems = new ArrayList<CrmContrctBill>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContrctBill scp = new CrmContrctBill();
			// 将map信息读入实体类
			scp = (CrmContrctBill) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (sc != null)
				scp.setCrmContract(sc);
			if (scp.getBillId() != null && !scp.getBillId().equals("")) {
				scp.setApplyBillDate(new Date());
			}
			crmContractDao.saveOrUpdate(scp);
		}
	}

	// 保存合同联系人
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savecrmContractLinkMan(CrmContract sc, String itemDataJson)
			throws Exception {
		List<CrmContractLinkMan> saveItems = new ArrayList<CrmContractLinkMan>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CrmContractLinkMan scp = new CrmContractLinkMan();
			// 将map信息读入实体类
			scp = (CrmContractLinkMan) crmContractDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCrmContract(sc);

			saveItems.add(scp);
		}
		crmContractDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCrmContrctBill(String[] ids) throws Exception {
		for (String id : ids) {
			CrmContrctBill scp = crmContractDao.get(CrmContrctBill.class, id);
			crmContractDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CrmContract sc, Map jsonMap) throws Exception {
		if (sc != null) {
			crmContractDao.saveOrUpdate(sc);

			// 如果该合同归属于父级订单，则把该合同的客户要求补充到父级订单中
			if(sc.getParent()!=null) {
				if (sc.getParent().getId() != null
						&& !sc.getParent().getId().equals("")) {
					CrmContract cc = crmContractDao.get(CrmContract.class, sc
							.getParent().getId());
					cc.setCustomerRequirements(cc.getCustomerRequirements()
							+ " 补充：" + sc.getCustomerRequirements());
					crmContractDao.saveOrUpdate(cc);
				}
			}
		}

		String jsonStr = "";
		if (jsonMap.containsKey("crmContrctCollectItem")) {
			jsonStr = (String) jsonMap.get("crmContrctCollectItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmContrctCollectItem(sc, jsonStr);
			}
		}
		if (jsonMap.containsKey("crmContrctBillItem")) {
			jsonStr = (String) jsonMap.get("crmContrctBillItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmContrctBillItem(sc, jsonStr);
			}
		}
		if (jsonMap.containsKey("crmContrctPay")) {
			jsonStr = (String) jsonMap.get("crmContrctPay");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmContrctPay(sc, jsonStr);
			}
		}
		if (jsonMap.containsKey("crmContrctBill")) {
			jsonStr = (String) jsonMap.get("crmContrctBill");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmContrctBill(sc, jsonStr);
			}
		}
		/*
		 * jsonStr = (String) jsonMap.get("crmContractProgress"); if (jsonStr !=
		 * null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
		 * saveCrmContractProgress(sc, jsonStr); }
		 */
		if (jsonMap.containsKey("crmContrctCollect")) {
			jsonStr = (String) jsonMap.get("crmContrctCollect");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCrmContrctCollect(sc, jsonStr);
			}
		}

		/*
		 * jsonStr = (String) jsonMap.get("crmContractLinkMan"); if (jsonStr !=
		 * null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
		 * savecrmContractLinkMan(sc, jsonStr); }
		 */

	}

	/**
	 * 获得子节点列表信息
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public List<CrmContract> getChildList(List<CrmContract> list,
			CrmContract node) throws Exception { // 得到子节点列表
		List<CrmContract> li = new ArrayList<CrmContract>();
		Iterator<CrmContract> it = list.iterator();
		while (it.hasNext()) {
			CrmContract n = (CrmContract) it.next();
			if (n.getParent() != null
					&& n.getParent().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<CrmContract> list, CrmContract node)
			throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public boolean hasChildCount(CrmContract node) throws Exception {

		Long c = commonDAO.getCount("from CrmContract where parent.id='"
				+ node.getId() + "'");
		return c > 0 ? true : false;
	}

	public String getJson(List<CrmContract> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<CrmContract> nodeList0 = new ArrayList<CrmContract>();
		Iterator<CrmContract> it1 = list.iterator();
		while (it1.hasNext()) {
			CrmContract node = (CrmContract) it1.next();
			// if (node.getLevel() == 0) {
			nodeList0.add(node);
			// }
		}
		Iterator<CrmContract> it = nodeList0.iterator();
		while (it.hasNext()) {
			CrmContract node = (CrmContract) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	/**
	 * 构建Json文件
	 * 
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<CrmContract> list, CrmContract treeNode)
			throws Exception {
		json.append("{");
		json.append("\"id\":'");
		json.append(JsonUtils.formatStr(treeNode.getId() == null ? ""
				: treeNode.getId()) + "");
		json.append("',");
		json.append("\"name\":'");
		json.append(JsonUtils.formatStr(treeNode.getName() == null ? ""
				: treeNode.getName()) + "");
		json.append("',");
		json.append("\"crmCustomer\":'");
		json.append(JsonUtils.formatStr(treeNode.getCrmCustomer() == null ? ""
				: treeNode.getCrmCustomer().getName()) + "");
		json.append("',");
		json.append("\"writeDate\":'");
		json.append(treeNode.getWriteDate() == null ? "" : treeNode
				.getWriteDate());
		json.append("',");
		json.append("\"startDate\":'");
		json.append(treeNode.getStartDate() == null ? "" : treeNode
				.getStartDate());
		json.append("',");
		json.append("\"endDate\":'");
		json.append(treeNode.getEndDate() == null ? "" : treeNode.getEndDate());
		json.append("',");
		json.append("\"type\":'");
		json.append(JsonUtils.formatStr(treeNode.getType() == null ? ""
				: treeNode.getType().getName()) + "");
		json.append("',");
		json.append("\"money\":'");
		json.append(treeNode.getMoney() == null ? "" : treeNode.getMoney());
		json.append("',");
		json.append("\"crmSaleOrder\":'");
//		json.append(JsonUtils.formatStr(treeNode.getCrmSaleOrder() == null ? ""
//				: treeNode.getCrmSaleOrder().getName()) + "");
		json.append("',");
		json.append("\"parent\":'");
		json.append(JsonUtils.formatStr(treeNode.getParent() == null ? ""
				: treeNode.getParent().getName()) + "");
		json.append("',");
		// json.append("\"crmContractPay\":'");
		// json.append(JsonUtils.formatStr(treeNode.getCrmContractPay() == null
		// ? "" : treeNode.getCrmContractPay().getName()) + "");
		// json.append("',");
		json.append("\"manager\":'");
		json.append(JsonUtils.formatStr(treeNode.getManager() == null ? ""
				: treeNode.getManager().getName()) + "");
		json.append("',");
		json.append("\"isRecord\":'");
		json.append(JsonUtils.formatStr(treeNode.getIsRecord() == null ? ""
				: treeNode.getIsRecord()) + "");
		json.append("',");
		json.append("\"createUser\":'");
		json.append(JsonUtils.formatStr(treeNode.getCreateUser() == null ? ""
				: treeNode.getCreateUser().getName()) + "");
		json.append("',");
		json.append("\"createDate\":'");
		json.append(treeNode.getCreateDate() == null ? "" : treeNode
				.getCreateDate());
		json.append("',");
		json.append("\"confirmUser\":'");
		json.append(JsonUtils.formatStr(treeNode.getConfirmUser() == null ? ""
				: treeNode.getConfirmUser().getName()) + "");
		json.append("',");
		json.append("\"confirmDate\":'");
		json.append(treeNode.getConfirmDate() == null ? "" : treeNode
				.getConfirmDate());
		json.append("',");
		json.append("\"state\":'");
		json.append(JsonUtils.formatStr(treeNode.getState() == null ? ""
				: treeNode.getState()) + "");
		json.append("',");
		json.append("\"stateName\":'");
		json.append(JsonUtils.formatStr(treeNode.getStateName() == null ? ""
				: treeNode.getStateName()) + "");
		json.append("',");
		json.append("\"content1\":'");
		json.append(JsonUtils.formatStr(treeNode.getContent1() == null ? ""
				: treeNode.getContent1()) + "");
		json.append("',");
		json.append("\"content2\":'");
		json.append(JsonUtils.formatStr(treeNode.getContent2() == null ? ""
				: treeNode.getContent2()) + "");
		json.append("',");
		json.append("\"content3\":'");
		json.append(JsonUtils.formatStr(treeNode.getContent3() == null ? ""
				: treeNode.getContent3()) + "");
		json.append("',");
		json.append("\"content4\":'");
		json.append(JsonUtils.formatStr(treeNode.getContent4() == null ? ""
				: treeNode.getContent4()) + "");
		json.append("',");
		json.append("\"content5\":'");
		json.append(treeNode.getContent5() == null ? "" : treeNode
				.getContent5());
		json.append("',");
		json.append("\"content6\":'");
		json.append(treeNode.getContent6() == null ? "" : treeNode
				.getContent6());
		json.append("',");
		json.append("\"content7\":'");
		json.append(treeNode.getContent7() == null ? "" : treeNode
				.getContent7());
		json.append("',");
		json.append("\"content8\":'");
		json.append(treeNode.getContent8() == null ? "" : treeNode
				.getContent8());
		json.append("',");
		json.append("\"content9\":'");
		json.append(treeNode.getContent9() == null ? "" : treeNode
				.getContent9());
		json.append("',");
		json.append("\"content10\":'");
		json.append(treeNode.getContent10() == null ? "" : treeNode
				.getContent10());
		json.append("',");
		json.append("\"content11\":'");
		json.append(treeNode.getContent11() == null ? "" : treeNode
				.getContent11());
		json.append("',");
		json.append("\"content12\":'");
		json.append(treeNode.getContent12() == null ? "" : treeNode
				.getContent12());
		json.append("',");
		if (hasChildCount(treeNode)) {
			json.append("\"leaf\":false");
		} else {
			json.append("\"leaf\":true");
		}
		json.append(",\"upId\":'");
		json.append((treeNode.getParent() == null ? "" : treeNode.getParent()
				.getId()) + "");
		json.append("'},");

	}

	public List<CrmContract> findCrmContractList(String upId) throws Exception {
		List<CrmContract> list = null;
		if (upId.equals("0")) {
			list = commonDAO
					.find("from CrmContract where  parent.id is null order by id asc");
		} else {

			list = commonDAO.find("from CrmContract where parent.id='" + upId
					+ "' order by id asc");
		}
		return list;
	}

	public List<CrmContract> findCrmContractList() throws Exception {
		List<CrmContract> list = commonDAO
				.find("from CrmContract where  parent.id is null order by id asc");
		return list;
	}

	// 由合同生成订单
//	public boolean generateOrder(String code, int orderNum) throws Exception {
//		Map<String, Object> result = crmContractDao.generateOrder(code);
//		List<CrmContract> list = (List<CrmContract>) result.get("list");
//		if (list != null && list.size() > 0) {
//			if (orderNum > 0) {
//				for (int i = 0; i < orderNum; i++) {
//					for (CrmContract srai : list) {
//						CrmSaleOrder cus = new CrmSaleOrder();
//						String modelName = "CrmSaleOrder";
//						String autoID = projectService.findAutoID(modelName, 4);
//						cus.setId(autoID);
//						cus.setContent2(srai.getContent3());
//						cus.setName(srai.getName());
//						cus.setMoney(srai.getMoney());
//						cus.setManager(srai.getManager());
//						cus.setGenProjectDate(new Date());
//						cus.setCreateDate(new Date());
//						System.out.println("aaa=" + srai.getProjectStart());
//						cus.setProjectStart(srai.getProjectStart());
//						cus.setCustomerRequirements(srai
//								.getCustomerRequirements());
//						cus.setCrmContract(srai);
//						cus.setClient(srai.getClient());// 从合同管理获取委托单位，赋值到订单管理
//						cus.setOrderSource(srai.getChanceSource());// 获得合同管理中的合同来源
//																	// 赋值给订单管理中的订单来源
//						cus.setTechnologyType(srai.getTechnologyType());
//						cus.setType(srai.getProductType());
//						cus.setGenus(srai.getGenus());
//						cus.setStrain(srai.getStrain());
//						cus.setGeneTargetingType(srai.getGeneTargetingType());
//						cus.setWorkType(srai.getWorkType());
//						cus.setCheckup(srai.getCheckup());
//						cus.setNeoResult(srai.getNeoResult());
//						cus.setProductNo(srai.getProductNo());
//						cus.setProduct(srai.getProduct());
//						cus.setCustomerRequirements(srai
//								.getCustomerRequirements());
//						cus.setGeneByName(srai.getGeneByName());
//						cus.setGeneName(srai.getGeneName());
//						cus.setGeneId(srai.getGeneId());
//						cus.setStartDate(srai.getFactStartDate());
//						cus.setEndDate(srai.getFactEndDate());
//						cus.setCrmCustomer(srai.getCrmCustomer());
//						cus.setManager(srai.getManager());
//						User user = (User) ServletActionContext.getRequest()
//								.getSession()
//								.getAttribute(SystemConstants.USER_SESSION_KEY);
//						cus.setCreateUser(user);
//						// cus.setConfirmUser(srai.getConfirmUser());
//						// cus.setConfirmDate(srai.getConfirmDate());
//						commonDAO.saveOrUpdate(cus);
//						srai.setConStateName("已生成");
//						srai.setState(SystemConstants.WORK_FLOW_NEW);
//						srai.setStateName("开始");
//						// 获取子表信息
//						Map<String, Object> result1 = crmContractDao
//								.selectCrmContrctLinkManList(code, null, null,
//										null, null);
//						List<CrmContractLinkMan> list1 = (List<CrmContractLinkMan>) result1
//								.get("list");
//						for (CrmContractLinkMan scip : list1) {
//							CrmSaleOrderLinkman csi = new CrmSaleOrderLinkman();
//							if (scip.getName() != null) {
//								csi.setName(scip.getName());
//								csi.setCode(scip.getCode());
//								csi.setCrmlinkMan(scip.getCrmlinkMan());
//								csi.setDeptName(scip.getDeptName());
//								csi.setEmail(scip.getEmail());
//								csi.setKtEmail(scip.getKtEmail());
//								csi.setPhone(scip.getPhone());
//								csi.setTel(scip.getTel());
//								csi.setTrans(scip.getTrans());
//								csi.setCrmSaleOrder(cus);
//								crmSaleOrderDao.saveOrUpdate(csi);
//							}
//						}
//						// 消息提醒
//						List<String> l = new ArrayList<String>();
//						l.add("xmglz");
//						List<UserGroupUser> l1 = userGroupService
//								.findUserGroupUserListByList(l);
//						User zg = null;
//						if (l1.size() > 0) {
//							for (UserGroupUser ugu : l1) {
//								zg = ugu.getUser();
//								projectService.createSampleSysRemind("订单管理",
//										zg, modelName, autoID, new Date());
//							}
//						}
//					}
//				}
//			}
//		}
//		return true;
//	}

	// 由合同生成发票
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToBill(String id) throws Exception {

		List<CrmContract> saveItems = crmContractDao.setOrderList(id);
		for (CrmContract cmpa : saveItems) {

			CrmSaleBill cma = new CrmSaleBill();
			// <<<<<<< .mine
			// String cid = "FP" + (int) (Math.random() * 100000);
			// //cma.setId("CrmContract" + cmpa.getId());
			// cma.setId(cid);
			// cma.setName("合同" + cmpa.getId() + "生成的发票");
			// User user = (User)
			// ServletActionContext.getRequest().getSession().getAttribute(
			// SystemConstants.USER_SESSION_KEY);
			// =======
			String modelName = "CrmSaleBill";
			String autoID = systemCodeService.getCodeByPrefix("CrmSaleBill", "FP",
					00000, 5, null);//projectService.findAutoID(modelName, 5);
			cma.setId(autoID);
			// cma.setName("合同" + cmpa.getId() + "生成的发票");
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);

			cma.setCreateUser(user);
			cma.setCreateDate(new Date());
			// cma.setType(cmpa.getType());
			// cma.(cmpa.getManager());
			// cma.setCrmSaleOrder(cmpa);
			cma.setCrmContract(cmpa);
			crmContractDao.saveOrUpdate(cma);
		}
	}

	public Boolean checkNameDuplicate(String crmContractName) throws Exception {
		List list = new ArrayList();
		list = crmContractDao
				.selectCrmContractListByCrmContractName(crmContractName);
		if (list != null && list.size() > 0) {
			return true;// 已占用
		} else {
			return false;// 未占用
		}
	}

}
