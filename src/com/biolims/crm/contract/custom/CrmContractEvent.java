package com.biolims.crm.contract.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.crm.contract.service.CrmContractService;

public class CrmContractEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CrmContractService ccService = (CrmContractService) ctx.getBean("crmContractService");
		ccService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
