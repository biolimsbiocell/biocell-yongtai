package com.biolims;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;

public class LetCxfGoFilter extends StrutsPrepareAndExecuteFilter {
	@Override
	public void doFilter(ServletRequest arg0,ServletResponse arg1,FilterChain arg2) throws IOException, ServletException {     
	HttpServletRequest request = (HttpServletRequest) arg0;
		//如果路径包含cxf则放行。
		if(request.getRequestURI().contains("sshServer")){   
			arg2.doFilter(arg0, arg1);   
		}else{
			super.doFilter(arg0,arg1, arg2);
			}
		}
}
