package com.biolims.dic.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.dic.dao.DicCurrencyTypeDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
public class DicCurrencyTypeService {
	@Resource
	private DicCurrencyTypeDao dicCurrencyTypeDao;

	public Map<String, Object> queryDicJob(int startNum, int limitNum, String dir, String sort) {
		return dicCurrencyTypeDao.selectDicJob(startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addOrUpdate(String jsonString) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			DicCurrencyType dct = new DicCurrencyType();
			dct = (DicCurrencyType) dicCurrencyTypeDao.Map2Bean(map, dct);
			dicCurrencyTypeDao.saveOrUpdate(dct);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicCurrencyType(String id) {
		DicCurrencyType uct = dicCurrencyTypeDao.get(DicCurrencyType.class, id);
		dicCurrencyTypeDao.delete(uct);
	}

}
