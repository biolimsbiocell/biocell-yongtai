package com.biolims.dic.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.dao.DicUnitDao;
import com.biolims.dic.model.DicUnit;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
public class DicUnitService {
	@Resource
	private DicUnitDao dicUnitDao;
	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> queryDicUnit(String classify, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return dicUnitDao.selectDicUnit(classify, start, length, query, col, sort);
	}

	/**
	 * @Title: saveDicUnit @Description: 保存结果 @author : dwb @date 2018-04-11
	 * 16:55:46 @param id @param dataJson @throws Exception @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicUnit(String id, String dataJson, String logInfo, String confirmUser) throws Exception {
		List<DicUnit> saveItems = new ArrayList<DicUnit>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		DicUnit pt = commonDAO.get(DicUnit.class, id);
		for (Map<String, Object> map : list) {
			DicUnit uct = new DicUnit();
			// 将map信息读入实体类
			uct = (DicUnit) commonDAO.Map2Bean(map, uct);
			if (uct.getId() != null && uct.getId().equals("")) {
				uct.setId(null);
			}
			if (uct.getState() == null)
				uct.setState("1");
			commonDAO.saveOrUpdate(uct);
			saveItems.add(uct);
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(uct.getName());
				li.setClassName("DicUnit");
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setState("1");
				li.setState("数据新增");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		
		dicUnitDao.saveOrUpdateAll(saveItems);
	}

	/*
	 * public Map<String, Object> queryDicUnit(int startNum, int limitNum, String
	 * dir, String sort) { return dicUnitDao.selectDicUnit(startNum, limitNum, dir,
	 * sort); }
	 */
	public List<DicUnit> queryDicUnit(String type) {
		return dicUnitDao.selectDicUnit(type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addOrUpdate(String jsonString) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			DicUnit du = new DicUnit();
			du = (DicUnit) dicUnitDao.Map2Bean(map, du);
			dicUnitDao.saveOrUpdate(du);
		}
	}

	public List<DicUnit> findDicUnit() {
		List<DicUnit> list = dicUnitDao.find("from DicUnit  order by orderNumber asc");
		return list;

	}

	public String getDicUnitTypeJson(String type) throws Exception {
		List<DicUnit> list = findWeightDicUnit(type);
		return JsonUtils.toJsonString(list);
	}

	public List<DicUnit> findTimeDicUnit() {
		List<DicUnit> list = dicUnitDao.find("from DicUnit  where type='time' order by orderNumber asc");
		return list;

	}

	public List<DicUnit> findWeightDicUnit(String type) {
		List<DicUnit> list = dicUnitDao.find("from DicUnit  where type='" + type + "' order by orderNumber asc");
		return list;

	}

	/**
	 * 删除字典表
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicUnit(String[] ids) throws Exception {
		for (String id : ids) {
			DicUnit uct = commonDAO.get(DicUnit.class, id);
			commonDAO.delete(uct);

			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(uct.getName());
				li.setClassName("DicUnit");
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setModifyContent("单位字典管理:" + "名称为:" + uct.getName() + " 的记录已删除");
				li.setState("2");
				li.setState("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}
	/*
	 * @WriteOperLog
	 * 
	 * @WriteExOperLog
	 * 
	 * @Transactional(rollbackFor = Exception.class) public void delDicUnit(String
	 * id) { DicUnit uct = dicUnitDao.get(DicUnit.class, id);
	 * dicUnitDao.delete(uct); }
	 */

	public <T> Map<String, Object> commonShowJsonByQuery(Class<T> persistentClass, Map<String, String> map, String type,
			int start, int limit, String dir, String sort) throws Exception {
		Map<String, Object> controlMap = null;
		if (type != null && type.contains("(") && type.contains(")")) {
			controlMap = findCommonDic(persistentClass.getName(), " type in " + type + "", start, limit, dir, sort, "");
		} else {
			controlMap = findCommonDic(persistentClass.getName(), " 1=1", start, limit, dir, sort, "");
		}
		return controlMap;

	}

	public <T> Map<String, Object> findCommonDic(String className, String condition, int startNum, int limitNum,
			String dir, String sort, String contextPath) throws Exception {
		String where = "where 1=1 ";
		if (condition != null && !condition.equals("")) {
			where = where + " and " + condition;
		}
		String hql = "from " + className + " ";
		Map<String, Object> controlMap = dicUnitDao.findObject(hql + where + " order by orderNumber ASC", startNum,
				limitNum, dir, sort);
		List<T> list = (List<T>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> showUnitSelectTableJson(Integer start, Integer length, String query, String col,
			String sort) {
		return dicUnitDao.showUnitSelectTableJson(start, length, query, col, sort);
	}
}
