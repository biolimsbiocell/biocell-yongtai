package com.biolims.dic.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.service.CommonService;
import com.biolims.dic.dao.DicMainTypeDao;
import com.biolims.dic.model.DicMainType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class DicMainTypeService extends CommonService {
	@Resource
	private DicMainTypeDao dicMainTypeDao;

	/**
	 * 字典主类的列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDicMainTypeList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		return dicMainTypeDao.selectDicMainTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 表单类型的保存和修改
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicMainTypeList(String itemDataJson) throws Exception {
		List<DicMainType> saveItems = new ArrayList<DicMainType>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			String orderNumber = (String) map.get("orderNumber");
			if (orderNumber != null && orderNumber.length() > 0) {
				map.put("orderNumber", Integer.valueOf(orderNumber));
			} else {
				map.put("orderNumber", null);
			}
			DicMainType dmt = new DicMainType();
			// 将map信息读入实体类
			dmt = (DicMainType) dicMainTypeDao.Map2Bean(map, dmt);
			saveItems.add(dmt);
		}
		dicMainTypeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 表单类型的删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicMainType(String[] ids) throws Exception {
		for (String id : ids) {
			DicMainType at = dicMainTypeDao.get(DicMainType.class, id);
			/*at.setState("0");*/
			dicMainTypeDao.saveOrUpdate(at);
		}
	}
}
