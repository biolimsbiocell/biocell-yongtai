/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：字典管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.dic.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.dao.DicMainTypeDao;
import com.biolims.dic.dao.DicTypeDao;
import com.biolims.dic.model.DicMainType;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings({ "unchecked", "rawtypes" })
public class DicTypeService extends CommonService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private DicTypeDao dicTypeDao;

	public Map<String, Object> findDicTypeList(int startNum, int limitNum,
			String dir, String sort, String contextPath, String data)
			throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();

		if (data != null && !data.equals("")) {
			DicType dt = new DicType();
			mapForCondition.put("type", "=");
			mapForCondition.put("id", "like");
			Map map = JsonUtils.toObjectByJson(data, Map.class);
			dt = (DicType) commonDAO.Map2Bean(map, dt);
			mapForQuery = BeanUtils.po2MapNotNull(mapForCondition, dt);
		}
		if (dir.equals(""))
			dir = "ASC";
		if (sort.equals(""))
			sort = "type.orderNumber";
		Map<String, Object> controlMap = commonDAO.findObjectCondition(
				startNum, limitNum, dir, sort, DicType.class, mapForQuery,
				mapForCondition);

		return controlMap;
	}

	/**
	 * 字典主类的列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDicTypeList(String classify, Integer start,
			Integer length, String query, String col,String sort) throws Exception {
		return dicTypeDao.selectDicTypeList(classify,start, length, query, col, sort);
	}
	/*public Map<String, Object> findDicTypeList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		return dicTypeDao.selectDicTypeList(mapForQuery, startNum, limitNum,
				dir, sort);
	}*/

	public Map<String, Object> selectDicTypeListInWhere(String in,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		return dicTypeDao.selectDicTypeListInWhere(in, startNum, limitNum, dir,
				sort);
	}

	
	
	/**
	 * @param confirmUser 
	 * @Title: saveDicType
	 * @Description: 保存结果
	 * @author : dwb
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicType(String id, String dataJson, String logInfo, String confirmUser) throws Exception {
		List<DicType> saveItems = new ArrayList<DicType>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		DicType pt = commonDAO.get(DicType.class, id);
		for (Map<String, Object> map : list) {
			DicType uct = new DicType();
			// 将map信息读入实体类
			uct = (DicType) commonDAO.Map2Bean(map, uct);
			if (uct.getId() != null && uct.getId().equals("")) {
				uct.setId(null);
			}
			if (uct.getState() == null)
				uct.setState("1");
			commonDAO.saveOrUpdate(uct);
			saveItems.add(uct);
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(uct.getId());
				li.setClassName("DicType");
				li.setModifyContent(logInfo);
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}
		}
		
		dicTypeDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除字典表
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicType(String[] ids) throws Exception {
		for (String id : ids) {
			DicType uct = commonDAO.get(DicType.class, id);
			commonDAO.delete(uct);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
						li.setUserId(u.getId());
				li.setFileId(uct.getId());
				li.setClassName("DicType");
				li.setModifyContent("字典管理:"+"类型为:"+uct.getType().getName()+" 的记录已删除");
				li.setState("2");
				li.setState("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}
/*	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicType(String jsonString) throws Exception {
		List<Map> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map map : list) {
			DicType uct = new DicType();
			uct = (DicType) commonDAO.Map2Bean(map, uct);
			if (uct.getId() != null && uct.getId().equals("")) {
				uct.setId(null);
			}
			if (uct.getState() == null)
				uct.setState("1");
			commonDAO.saveOrUpdate(uct);
		}
	}*/

	/*@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicType(String id) {
		DicType uct = commonDAO.get(DicType.class, id);
		commonDAO.delete(uct);
	}*/
	
	public Map<String, String> getDicMainTypes() {
		Map<String, String> map = new HashMap<String, String>();
		List<DicMainType> list = commonDAO.find("from DicMainType where state ='"
						+ SystemConstants.DIC_STATE_YES_ID
						+ "' order by name ASC");
		StringBuffer id = new StringBuffer();
		StringBuffer name = new StringBuffer();
		if(list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				DicMainType remind = list.get(i);
				id.append(remind.getId()+"|");
				name.append(remind.getName()+"|");
			}
			id.toString().substring(0,id.toString().length()-1);
			name.toString().substring(0,name.toString().length()-1);
		}
		map.put(id.toString(), name.toString());
		return map;
	}

	public String findDicMainTypeList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<DicMainType> list = commonDAO
				.find("from DicMainType where state ='"
						+ SystemConstants.DIC_STATE_YES_ID
						+ "' order by name ASC");
		StringBuffer str = new StringBuffer();

		for (int i = 0; i < list.size(); i++) {
			DicMainType remind = list.get(i);
			str.append("{");

			str.append("'id':'").append(remind.getId()).append("',");
			str.append("'name':'")
					.append(remind.getName() == null ? "" : remind.getName())
					.append("'");

			str.append("}");
		}
		map.put("list", list);
		map.put("str", str.toString());
		StringBuffer sb = new StringBuffer("{'results':[").append(str).append(
				"]}");
		String a = sb.toString().replace("}{", "},{");

		return a;
	}

	/**
	 * 
	 * 检索通用字典
	 * 
	 * @return list
	 */
	public List<DicType> findDicType(String type) {

		List<DicType> list = commonDAO.find("from DicType where  type = '"
				+ type + "' order by orderNumber asc");
		return list;

	}

	/**
	 * 
	 * 检索某类信息,用于ext显示
	 * 
	 * @param className
	 *            类名称
	 * @return list
	 */
	public <T> Map<String, Object> findCommonDic(String type, int startNum,
			int limitNum, String dir, String sort,
			Map<String, String> mapForQuery) throws Exception {

		Map<String, Object> controlMap = dicTypeDao.findCommonDic(type,
				startNum, limitNum, dir, sort, mapForQuery);

		return controlMap;
	}

	public String getDicTypeJson(String type) throws Exception {
		List<DicType> list = findDicType(type);
		return JsonUtils.toJsonString(list);
	}

	/**
	 * 
	 * 根据ID检索
	 * 
	 * @return list
	 * @throws Exception
	 */
	public List<DicType> findDicTypeById(String id) throws Exception {

		List<DicType> list = (List<DicType>) dicTypeDao.selectDicTypeById(id)
				.get("list");
		return list;

	}

	public Map<String, Object> dicTypeSelectTableJson(Integer start,
			Integer length, String query, String col,
			String sort,String flag) throws Exception{
		return dicTypeDao.dicTypeSelectTableJson(start,length,query,col,sort,flag);
	}

	public List<DicType> findDicTypeSelectList(String flag) {
		return dicTypeDao.findDicTypeSelectList(flag);
	}

}
