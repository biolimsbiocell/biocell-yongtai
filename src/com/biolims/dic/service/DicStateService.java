/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：字典管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.dic.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dic.dao.DicStateDao;
import com.biolims.dic.model.DicState;

@Service
public class DicStateService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private DicStateDao dicStateDao;

	public Map<String, Object> showStateList(int startNum, int limitNum, String dir, String sort, String data)
			throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		mapForCondition.put("type", "=");
		mapForQuery.put("type", data);
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, DicState.class,
				mapForQuery, mapForCondition);

		return controlMap;
	}

	public Map<String, Object> dicStateSelectTableJson(Integer start,
			Integer length, String query, String col, String sort, String flag) {
		return dicStateDao.dicStateSelectTableJson(start, length, query, col, sort, flag);
	}
}
