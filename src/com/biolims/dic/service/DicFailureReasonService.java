package com.biolims.dic.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.dic.dao.DicFailureReasonDao;

@Service
public class DicFailureReasonService {

	@Resource
	private DicFailureReasonDao dicFailureReasonDao;

	/**
	 * 查询 不合格原因字典列表
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDicFailureReasonList(Map<String, String> mapForQuery, int startNum, int limitNum,
			String dir, String sort) throws Exception {
		return dicFailureReasonDao.selectDicFailureReasonList(mapForQuery, startNum, limitNum, dir, sort);
	}
}
