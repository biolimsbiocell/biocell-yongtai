/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：字典管理
 * 创建人：倪毅
 * 创建时间：2011-8
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.dic.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dic.model.DicType;
import com.biolims.dic.service.DicTypeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/dic/type")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings({ "unchecked" })
public class DicTypeAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private DicTypeService dicService;
	
	
	/**
		 *	根据type,展示select下拉选
	     * @Title: findDicTypeSelectList  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2019年4月17日
	     * @throws
	 */
	@Action(value = "findDicTypeSelectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findDicTypeSelectList() throws Exception {
		String flag = getParameterFromRequest("flag");
		List<DicType> list = dicService.findDicTypeSelectList(flag);
		HttpUtils.write(JsonUtils.toJsonString(list));
	}
	
	@Action(value = "showDicTypeList")
	public String showDicTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		String scId = getRequest().getParameter("id");  
		putObjToContext("id", scId);
		return dispatcher("/WEB-INF/page/dic/showDicTypeList.jsp");
	}
	
	@Action(value = "showDicTypeListJson")
	public void showDicTypeListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String classify=getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = dicService.findDicTypeList(classify,start, length, query, col, sort);
			List<DicType> list = (List<DicType>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("sysCode", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("orderNumber", "");
			map.put("state", "");
//			map.put("dicPhone", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: saveDicType
	 * @Description: 保存字典管理
	 * @author : 
	 * @date
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "saveDicType")
	public void saveDicType() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		String confirmUser=getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			dicService.saveDicType(id, dataJson, logInfo,confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDicType")
	public void delDicType() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dicService.delDicType(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "selectDicMainTypes", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectDicMainTypes() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Map<String, String> dataListMap =this.dicService.getDicMainTypes();
			if(dataListMap.size()>0){
				map.put("success", true);
				map.put("data", dataListMap);
			}else{
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 用于页面上显示模块名称
	private String title = "字典管理";

	// 该action权限id
	private String rightsId = "1105";
	
	/**
	 * 访问列表
	 *//*
	@Action(value = "showDicTypeList")
	public String showDicTypeList() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "120", "true",
				"false", "", "", editflag, "", "id" });
		map.put("code", new String[] { "", "string", "", "功能码", "32", "true",
				"false", "", "", editflag, "", "code" });
		map.put("name", new String[] { "", "string", "", "名称", "110", "true",
				"false", "", "", editflag, "", "dicName" });
		map.put("note", new String[] { "", "string", "", "说明", "200", "true",
				"false", "", "", editflag, "", "note" });
		map.put("sysCode", new String[] { "", "string", "", "系统标记", "200",
				"true", "false", "", "", editflag, "", "sysCode" });

		map.put("type-id", new String[] { "", "string", "", "类型", "75", "true",
				"false", "", "", editflag,
				"Ext.util.Format.comboRenderer(cob)", "cob" });
		map.put("orderNumber", new String[] { "", "string", "", "排序号", "75",
				"true", "false", "", "", editflag, "", "orderNumber" });
		map.put("state", new String[] { "", "string", "", "状态", "75", "true",
				"false", "", "", editflag,
				"Ext.util.Format.comboRenderer(stateCob)", "stateCob" });

		// 声明一个lisener
		// String statementLisener = "";
		// 生成ext用type字符串
		exttype = generalexttype(map);
		// 生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath() + "/dic/type/showDicTypeListJson.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/dic/showDicTypeList.jsp");
	}
*/
	/*@Action(value = "showDicTypeListJson")
	public void showDicTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		List<DicType> list = null;
		long totalCount = 0;

		Map<String, Object> controlMap = dicService.findDicTypeList(null,
				startNum, limitNum, dir, sort);
		totalCount = Long.parseLong(controlMap.get("total").toString());
		list = (List<DicType>) controlMap.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		map.put("code", "");
		map.put("orderNumber", "");
		map.put("sysCode", "");
		map.put("type-id", "");
		map.put("state", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount,
					ServletActionContext.getResponse());
	}*/
//
//	/**
//	 * 添加
//	 */
//	@Action(value = "saveDicType")
//	public void saveDicType() throws Exception {
//		String jsonString = getParameterFromRequest("data");
//		dicService.saveDicType(jsonString);
//	}

	/**
	 * 删除
	 *//*
	@Action(value = "delDicType")
	public void delDicType() throws Exception {
		String id = getParameterFromRequest("id");
		dicService.delDicType(id);

	}*/

	@Action(value = "showDicMainTypeListJson")
	public void showDicMainTypeListJson() throws Exception {
		String jsonStr = dicService.findDicMainTypeList();
		new SendData()
				.sendDataJson(jsonStr, ServletActionContext.getResponse());
	}

	/**
	 * 获取DIC字典列表数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "dicTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String getDicTypeListShow() throws Exception {
		String flag = super.getRequest().getParameter("flag");

		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/dic/type/dicTypeSelectJson.action?flag=" + flag;
		putObjToContext("flag", flag);
		String showUrl = "/WEB-INF/page/dic/dicTypeSelect.jsp";
		return dispatcher(commonShow(jsonUrl, showUrl));
	}
	/**
	 * 
	 * @Title: dicTypeSelectTables  
	 * @Description: 多选
	 * @author : shengwei.wang
	 * @date 2018年4月18日上午11:36:59
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "dicTypeSelectTables", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeSelectTables() throws Exception {
		String flag = super.getRequest().getParameter("flag");
		putObjToContext("flag", flag);
		return dispatcher("/WEB-INF/page/dic/dicStateSelectCheboxTable.jsp");
	}
	@Action(value="dicTypeSelectTablesJson")
	public void dicTypeSelectTablesJson()throws Exception{
		String flag=getParameterFromRequest("flag");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=dicService.dicTypeSelectTableJson(start,length,query,col,sort,flag);
		List<DicType> list = (List<DicType>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sysCode", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	/**
	 * 
	 * @Title: dicTypeSelectTable  
	 * @Description: 单选 
	 * @author : shengwei.wang
	 * @date 2018年4月18日上午11:36:43
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "dicTypeSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeSelectTable() throws Exception {
		String flag = super.getRequest().getParameter("flag");
		putObjToContext("flag", flag);
		return dispatcher("/WEB-INF/page/dic/dicTypeSelectTable.jsp");
	}
	@Action(value="dicTypeSelectTableJson")
	public void dicTypeSelectTableJson()throws Exception{
		String flag=getParameterFromRequest("flag");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=dicService.dicTypeSelectTableJson(start,length,query,col,sort,flag);
		List<DicType> list = (List<DicType>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sysCode", "");
//		map.put("dicPhone", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	@Action(value = "dicTypeJquerySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeJquerySelect() throws Exception {
		String flag = super.getRequest().getParameter("flag");

		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/dic/type/dicTypeSelectJson.action?flag=" + flag;
		putObjToContext("flag", flag);
		String showUrl = "/WEB-INF/page/dic/dicTypeJquerySelect.jsp";
		return dispatcher(commonShow(jsonUrl, showUrl));
	}

	@Action(value = "dicTypeFullRecSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeFullRecSelect() throws Exception {
		String flag = super.getRequest().getParameter("flag");

		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/dic/type/dicTypeSelectJson.action?flag=" + flag;
		putObjToContext("flag", flag);
		String showUrl = "/WEB-INF/page/dic/dicTypeFullRecSelect.jsp";
		return dispatcher(commonShow(jsonUrl, showUrl));
	}

	/**
	 * 访问字典表页面通用程序
	 */
	public String commonShow(String jsonUrl, String showUrl) throws Exception {

		String type = "{name: 'id'},{name: 'name'},{name: 'note'},{name: 'sysCode'}";
		String col = "{header:'id',sortable: true,hidden:true,dataIndex: 'id',width: 100},"
				+ "{header:'名称',sortable: true,dataIndex: 'name',width: 200},"
				+ "{id:'note',header:'说明',sortable: true,dataIndex: 'note',width: 240},{id:'sysCode',header:'系统码',sortable: true,hidden:true,dataIndex: 'sysCode',width: 100}";

		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("path", jsonUrl);
		return (showUrl);
	}

	@Action(value = "dicTypeSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void dicTypeSelectJson() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		map.put("sysCode", "");
		commonShowJsonByQueryDicType(map);
	}

	public <T> void commonShowJsonByQueryDicType(Map<String, String> map)
			throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String type = (String) super.getRequest().getParameter("flag");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		String queryData = getRequest().getParameter("data");
		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		if (type == null || type.trim().length() <= 0) {
			type = "";
		}

		Map<String, Object> controlMap = dicService.findCommonDic(type,
				startNum, limitNum, dir, sort, mapForQuery);
		long totalCount = Long.parseLong(controlMap.get("totalCount")
				.toString());
		List<T> list = (List<T>) controlMap.get("list");

		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}

	@Action(value = "dicTypeCobJson")
	public String dicTypeCobJson() throws Exception {
		String type = super.getRequest().getParameter("type");

		String outStr = "{results:" + dicService.getDicTypeJson(type) + "}";

		return renderText(outStr);
	}

	/**
	 * 根据ID检索字典
	 * 
	 * @throws Exception
	 */
	@Action(value = "selectDicTypeSysCodeById")
	public void selectDicTypeSysCodeById() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<DicType> list = dicService.findDicTypeById(id);
			result.put("success", true);
			result.put("data", list.get(0).getSysCode());
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public DicTypeService getDicService() {
		return dicService;
	}

	public void setDicService(DicTypeService dicService) {
		this.dicService = dicService;
	}
}
