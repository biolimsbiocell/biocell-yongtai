package com.biolims.dic.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.dic.service.DicUnitService;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

@Namespace("/dic/unit")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked" })
public class DicUnitAction extends BaseActionSupport {

	private static final long serialVersionUID = -8841346883208605554L;

	@Autowired
	private DicUnitService dicUnitService;

	/**
	 * @Title: showDicUnitList
	 * @Description: 单位管理
	 * @author : dwb
	 * @date  2018-04-11 16:53:56
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "showDicUnitList")
	public String showDicUnitList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		String scId = getRequest().getParameter("id");  
		putObjToContext("id", scId);
		return dispatcher("/WEB-INF/page/dic/showDicUnitList.jsp");
	}
	
	@Action(value = "showDicUnitListJson")
	public void showDicUnitListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String classify=getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = dicUnitService.queryDicUnit(classify,start, length, query, col, sort);
			List<DicUnit> list = (List<DicUnit>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("type", "");
			map.put("note", "");
			map.put("orderNumber", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: saveDicType
	 * @Description: 保存单位管理
	 * @author : dwb
	 * @date  2018-04-11 16:53:37
	 * @throws Exception
	 * @throws
	 */
	@Action(value = "saveDicUnit")
	public void saveDicUnit() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		String confirmUser=getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			dicUnitService.saveDicUnit(id, dataJson, logInfo,confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDicUnit")
	public void delDicUnit() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dicUnitService.delDicUnit(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//用于页面上显示模块名称
		private String title = "单位管理";

		//该action权限id
		private String rightsId = "1113";
	
	/**
	 * 访问列表
	 *//*
	@Action(value = "showDicUnitList")
	public String showDicUnitList() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "ID", "32", "true", "true", "", "", "", "", "" });
		map.put("name",
				new String[] { "", "string", "", "名称", "110", "true", "false", "", "", editflag, "", "typeName" });
		map.put("type", new String[] { "", "string", "", "类型", "120", "true", "false", "", "", editflag,
				"Ext.util.Format.comboRenderer(type)", "type" });
		map.put("note", new String[] { "", "string", "", "备注", "80", "true", "false", "", "", editflag, "", "note" });
		map.put("orderNumber", new String[] { "", "string", "", "排序号", "110", "true", "false", "", "", editflag, "",
				"orderNumber" });

		//生成ext用type字符串
		exttype = generalexttype(map);
		//生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/dic/unit/showDicUnitListJson.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/dic/showDicUnitList.jsp");
	}*/

	/*@Action(value = "showDicUnitListJson")
	public void showDicUnitListJson() throws Exception {
		int start = Integer.parseInt(getParameterFromRequest("start"));
		int limit = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		long total = 0;

		Map<String, Object> result = this.dicUnitService.queryDicUnit(start, limit, dir, sort);
		total = (Long) result.get("total");
		List<DicUnit> list = (List<DicUnit>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type", "");
		map.put("note", "");
		map.put("orderNumber", "");
		if (list != null)
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}*/

	/**
	 * 添加
	 */
	@Action(value = "editDicUnit")
	public void editDicUnit() throws Exception {
		String jsonString = getParameterFromRequest("data");
		dicUnitService.addOrUpdate(jsonString);
	}

	/**
	 * 删除
	 *//*
	@Action(value = "delDicUnit")
	public void delDicUnit() throws Exception {
		String id = getParameterFromRequest("id");
		dicUnitService.delDicUnit(id);

	}*/

	@Action(value = "dicUnitSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicUnitSelect() throws Exception {
		String unitType = getParameterFromRequest("flag");
		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/dic/unit/dicUnitSelectJson.action?unitType=" + unitType;
		String showUrl = "/WEB-INF/page/dic/dicUnitSelect.jsp";
		return dispatcher(commonShowNotNote(jsonUrl, showUrl));
	}

	@Action(value = "dicUnitSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void dicUnitSelectJson() throws Exception {
		String unitType = getParameterFromRequest("unitType");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		Map<String, Object> controlMap = dicUnitService.commonShowJsonByQuery(DicUnit.class, map, unitType, startNum,
				limitNum, dir, sort);

		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<DicUnit> list = (List<DicUnit>) controlMap.get("list");

		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	public String commonShowNotNote(String jsonUrl, String showUrl) throws Exception {

		String type = "{name: 'id'},{name: 'name'},{name: 'note'}";
		String col = "{header:'ID',sortable: true,hidden:true,dataIndex: 'id',width: 100},"
				+ "{header:'名称',sortable: true,dataIndex: 'name',width: 140},{id:'note',header:'说明',sortable: true,dataIndex: 'note',width: 200}";

		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("path", jsonUrl);
		return (showUrl);

	}

	@Action(value = "dicUnitTypeCobJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeCobJson() throws Exception {
		String type = super.getRequest().getParameter("type");

		String outStr = "{results:" + dicUnitService.getDicUnitTypeJson(type) + "}";

		return renderText(outStr);
	}
	/**
	 * 
	 * @Title: showUnitSelectTable  
	 * @Description: 选择单位  
	 * @author : shengwei.wang
	 * @date 2018年2月1日上午10:32:40
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="showUnitSelectTable")
	public String showUnitSelectTable(){
		return dispatcher("/WEB-INF/page/dic/dicUnitSelectTable.jsp");
	}
	@Action(value="showUnitSelectTableJson")
	public void showUnitSelectTableJson(){
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result= dicUnitService.showUnitSelectTableJson(start, length, query, col, sort);
			List<DicUnit> list = (List<DicUnit>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
		}
	}
	public DicUnitService getDicUnitService() {
		return dicUnitService;
	}

	public void setDicUnitService(DicUnitService dicUnitService) {
		this.dicUnitService = dicUnitService;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
