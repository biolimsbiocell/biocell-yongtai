package com.biolims.dic.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dic.model.DicMainType;
import com.biolims.dic.service.DicMainTypeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

;

@Namespace("/dicMainType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class DicMainTypeAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 184141464911926413L;

	private String rightsId = "1121";

	private DicMainTypeAction dmt;

	@Resource
	private DicMainTypeService dicmainTypeService;

	/**
	 * 字典主类别列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDicMainTypeList")
	public String showDicMainTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dic/showDicMainTypeList.jsp");
	}

	/**
	 * 字典主类别列表
	 * @return
	 * @throws Exception 
	 */
	@Action(value = "showDicMainTypeListJson")
	public void showDicMainTypeListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		try {
			Map<String, Object> result = this.dicmainTypeService.findDicMainTypeList(mapForQuery, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<DicMainType> list = (List<DicMainType>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "name");
			map.put("orderNumber", "orderNumber");
			map.put("state", "state");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存字典主类别
	 * @throws Exception
	 */
	@Action(value = "saveDicMainTypeList")
	public void saveDicMainTypeList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			dicmainTypeService.saveDicMainTypeList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 *删除表单类型
	 * @throws Exception
	 */
	@Action(value = "delDicMainType")
	public void delDicMainType() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dicmainTypeService.delDicMainType(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DicMainTypeAction getDmt() {
		return dmt;
	}

	public void setDmt(DicMainTypeAction dmt) {
		this.dmt = dmt;
	}

	public DicMainTypeService getDicmainTypeService() {
		return dicmainTypeService;
	}

	public void setDicmainTypeService(DicMainTypeService dicmainTypeService) {
		this.dicmainTypeService = dicmainTypeService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
