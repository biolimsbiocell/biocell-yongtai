package com.biolims.dic.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.dic.model.DicFailureReason;
import com.biolims.dic.service.DicFailureReasonService;
import com.biolims.util.SendData;

/**
 *不合格原因
 * @author cong
 *
 */
@Namespace("/dic/failureReason")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked" })
public class DicFailureReasonAction extends BaseActionSupport {

	private static final long serialVersionUID = -6532348431981855224L;
	@Resource
	private DicFailureReasonService dicFailureReasonService;

	/**
	 * 不合格原因选择列表
	 * @return
	 */
	@Action(value = "showFailureReasonList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFailureReasonList() {
		return dispatcher("/WEB-INF/page/dic/showFailureReasonList.jsp");
	}

	@Action(value = "showFailureReasonListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFailureReasonListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		//取出检索用的对象
		//String data = getParameterFromRequest("data");

		Map<String, Object> result = dicFailureReasonService.findDicFailureReasonList(null, startNum, limitNum, dir,
				sort);
		Long total = (Long) result.get("total");
		List<DicFailureReason> list = (List<DicFailureReason>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "");
		map.put("id", "id");

		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}
}
