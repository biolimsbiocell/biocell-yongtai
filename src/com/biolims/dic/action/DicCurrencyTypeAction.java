package com.biolims.dic.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.service.DicCurrencyTypeService;
import com.biolims.util.SendData;

@Namespace("/dic/currency")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked" })
public class DicCurrencyTypeAction extends BaseActionSupport {

	private static final long serialVersionUID = -8841346883208605554L;

	@Autowired
	private DicCurrencyTypeService dicCurrencyTypeService;

	//用于页面上显示模块名称
	private String title = "币种管理";

	//该action权限id
	private String rightsId = "1114";

	/**
	 * 访问列表
	 */
	@Action(value = "showCurrencyTypeList")
	public String showCurrencyTypeList() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "ID", "32", "true", "true", "", "", "", "", "" });
		map.put("name",
				new String[] { "", "string", "", "名称", "110", "true", "false", "", "", editflag, "", "typeName" });
		map.put("note", new String[] { "", "string", "", "备注", "80", "true", "false", "", "", editflag, "", "note" });

		//生成ext用type字符串
		exttype = generalexttype(map);
		//生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/dic/currency/showCurrencyTypeListJson.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/dic/showCurrencyTypeList.jsp");
	}

	@Action(value = "showCurrencyTypeListJson")
	public void showDicjobJson() throws Exception {
		int start = Integer.parseInt(getParameterFromRequest("start"));
		int limit = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		long total = 0;

		Map<String, Object> result = this.dicCurrencyTypeService.queryDicJob(start, limit, dir, sort);
		total = (Long) result.get("total");
		List<DicCurrencyType> list = (List<DicCurrencyType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		if (list != null)
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	/**
	 * 访问列表
	 */
	@Action(value = "currencyTypeSelect")
	public String currencyTypeSelect() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "32", "true", "true", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "名称", "110", "true", "false", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "备注", "80", "true", "false", "", "", "", "", "" });

		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/dic/currency/currencyTypeSelectJson.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/dic/dicCurrencyTypeSelect.jsp");
	}

	@Action(value = "currencyTypeSelectJson")
	public void currencyTypeSelectJson() throws Exception {
		int start = Integer.parseInt(getParameterFromRequest("start"));
		int limit = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		long total = 0;

		Map<String, Object> result = this.dicCurrencyTypeService.queryDicJob(start, limit, dir, sort);
		total = (Long) result.get("total");
		List<DicCurrencyType> list = (List<DicCurrencyType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	/**
	 * 添加
	 */
	@Action(value = "editDicCurrencyType")
	public void editDicCurrencyType() throws Exception {
		String jsonString = getParameterFromRequest("data");
		dicCurrencyTypeService.addOrUpdate(jsonString);
	}

	/**
	 * 删除
	 */
	@Action(value = "delDicCurrencyType")
	public void delDicCurrencyType() throws Exception {
		String id = getParameterFromRequest("id");
		dicCurrencyTypeService.delDicCurrencyType(id);

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
