/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：字典管理
 * 创建人：倪毅
 * 创建时间：2011-8
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.dic.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.dic.model.DicState;
import com.biolims.dic.service.DicStateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.SendData;

@Namespace("/dic/state")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked" })
public class DicStateAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private DicStateService stateService;

	//用于页面上显示模块名称
	private String title = "字典管理";

	//该action权限id
	private String rightsId = "";

	@Action(value = "stateSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String stateSelect() throws Exception {

		String stateType = getParameterFromRequest("type");

		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "编码", "150", "true", "true", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "名称", "150", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "250", "true", "", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/dic/state/showStateListJson.action?type=" + stateType);
		//导向jsp页面显示
		return dispatcher("/WEB-INF/page/dic/dicStateSelect.jsp");
	}

	@Action(value = "showStateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStateListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		String data = getParameterFromRequest("type");
		//取出session中检索用的对象

		Map<String, Object> controlMap = stateService.showStateList(startNum, limitNum, dir, sort, data);

		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<DicState> list = (List<DicState>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}
	
	@Action(value = "dicStateSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicStateSelectTable() throws Exception {
		String flag = super.getRequest().getParameter("flag");
		putObjToContext("flag", flag);
		return dispatcher("/WEB-INF/page/dic/dicStateSelectTable.jsp");
	}
	
	@Action(value="dicStateSelectTableJson")
	public void dicStateSelectTableJson()throws Exception{
		String flag=getParameterFromRequest("flag");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=stateService.dicStateSelectTableJson(start,length,query,col,sort,flag);
		List<DicState> list = (List<DicState>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
