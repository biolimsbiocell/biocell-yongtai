package com.biolims.dic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 币种
 * @author godcong
 *
 */

@Entity
@Table(name = "T_DIC_CURRENCY_TYPE")
public class DicCurrencyType implements Serializable {

	private static final long serialVersionUID = 3447497183569042970L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;
	@Column(name = "NAME", length = 40)
	private String name;
	@Column(name = "NOTE", length = 110)
	private String note;
	@Column(name = "STATE", length = 32)
	private String state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
