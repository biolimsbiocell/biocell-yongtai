package com.biolims.dic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "T_DIC_TYPE")
public class DicType implements Serializable {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	private String id;

	@Column(name = "NAME", length = 100)
	private String name; //名称

	@Column(name = "ORDER_NUMBER")
	private Integer orderNumber; //排序号

	@Column(name = "NOTE", length = 200)
	private String note;//说明
	
//	private String dicPhone;//科室电话

	@Column(name = "CODE", length = 32)
	private String code;
	@Column(name = "SYS_CODE", length = 32)
	private String sysCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicMainType type;//字典类型

	@Column(name = "STATE", length = 2)
	private String state;//状态 
	
	
	
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public DicMainType getType() {
		return type;
	}

	public void setType(DicMainType type) {
		this.type = type;
	}

//	public String getDicPhone() {
//		return dicPhone;
//	}
//
//	public void setDicPhone(String dicPhone) {
//		this.dicPhone = dicPhone;
//	}

	

}
