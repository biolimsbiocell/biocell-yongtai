package com.biolims.dic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 不合格原因
 * @author cong
 *
 */
@Entity
@Table(name = "T_DIC_FAILURE_REASON")
public final class DicFailureReason implements Serializable {

	private static final long serialVersionUID = 1788191555271631765L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;
	@Column(name = "NAME", length = 50)
	private String name;//名称
	@Column(name = "STATE", length = 2)
	private String state;//是否生效

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
