package com.biolims.dic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_DIC_MAIN_TYPE")
public class DicMainType implements Serializable {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;
	//名称
	@Column(name = "NAME", length = 100)
	private String name;
	//排序号
	@Column(name = "ORDER_NUMBER")
	private Integer orderNumber;

	@Column(name = "STATE", length = 2)
	private String state;//状态 

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
