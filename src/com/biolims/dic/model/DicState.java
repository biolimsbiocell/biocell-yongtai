package com.biolims.dic.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;

/**
 * 状态
 * @author godcong
 *
 */
@Entity
@Table(name = "T_DIC_STATE")
public class DicState extends EntityDao<DicState> implements Serializable {
	private static final long serialVersionUID = 1266645733315175911L;

	@Id
	@Column(name = "ID", length = 30)
	private String id;

	@Column(name = "NAME", length = 40)
	private String name;//名称

	@Column(name = "STATE", length = 50)
	private String state;//状态

	@Column(name = "TYPE", length = 32)
	private String type;//类型

	@Column(name = "NOTE", length = 200)
	private String note;//说明

	@Column(name = "ORDER_NUM")
	private int orderNum;//排序号

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
