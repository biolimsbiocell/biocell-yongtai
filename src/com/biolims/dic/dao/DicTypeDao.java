package com.biolims.dic.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.splitsample.model.SplitSampleItem;
import com.biolims.sample.model.SampleOrderItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DicTypeDao extends BaseHibernateDao {
	
	public Map<String, Object> selectDicTypeList(String classify, Integer start,
			Integer length, String query, String col,String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicType where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		//String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		//if(!"all".equals(scopeId)){
		//	key+=" and scopeId='"+scopeId+"'";
		//}
		/*if("1".equals(classify)){
			key+= " and state='2' and techJkServiceTask is null";
		}else{
			key+= " and state='2'";
		}*/
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from DicType where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<DicType> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
	
	/*public Map<String, Object> selectDicTypeList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from DicType";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<DicType> list = new ArrayList<DicType>();
		if (total > 0) {

			key = key + " order by type.orderNumber,orderNumber ASC";

			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}*/

	public Map<String, Object> selectDicTypeListInWhere(String in,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String key = "";
		String hql = "from DicType where 1=1 ";
		if (in != null && !in.equals("")) {
			String a[] = in.split(",");
			for (int i = 0; i < a.length; i++) {
				key = key + "'" + a[i] + "',";
			}
			key = key.substring(0, key.length() - 1);

			key = "and type.id in (" + key + ")";
		}

		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<DicType> list = new ArrayList<DicType>();
		if (total > 0) {

			key = key + " order by type.orderNumber,orderNumber ASC";

			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public <T> Map<String, Object> findCommonDic(String type, int startNum,
			int limitNum, String dir, String sort,
			Map<String, String> mapForQuery) throws Exception {
		String where = "where 1=1 ";
		String key = "";
		String key1 = "";
		String hql = "from DicType ";
		if (mapForQuery != null) {
			key1 = map2where(mapForQuery);
		}
		if (type.equals("instrument")) {
			where += " and type='device' and  state='1'";
		}else if (type.equals("gzlx")) {
			where += " and type='instrument' and  state='1'";
		}else{

		if (type != null && !type.equals("")) {

			if (type.contains(",")) {

				String a[] = type.split(",");
				for (int i = 0; i < a.length; i++) {
					key = key + "'" + a[i] + "',";
				}
				key = key.substring(0, key.length() - 1);

				key = "and type.id in (" + key + ") and state='"
						+ SystemConstants.DIC_STATE_YES_ID + "'";

				where = where + key + key1;
			} else {
				where = where + " and type.id = '" + type + "' and state='"
						+ SystemConstants.DIC_STATE_YES_ID + "'" + key1;

			}
		}

		}
		if (dir.equals(""))
			dir = "ASC";
		if (sort.equals(""))
			sort = "orderNumber";

		Map<String, Object> controlMap = findObject(hql + where + "order by "
				+ sort + " " + dir, startNum, limitNum, dir, sort);
		List<T> list = (List<T>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> selectDicTypeById(String id) throws Exception {
		String key = "";
		String hql = "from DicType d where 1=1";
		if (id != null && !id.equals("")) {
			key = " and d.id='" + id + "' ";
		}
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<DicType> list = new ArrayList<DicType>();
		if (total > 0) {

			key = key + " order by d.type.orderNumber,d.orderNumber ASC";

			list = (List<DicType>) this.getSession().createQuery(hql + key)
					.list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> dicTypeSelectTableJson(Integer start,
			Integer length, String query, String col,
			String sort, String flag) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicType where 1=1 and type='"+flag+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from DicType where 1=1 and type='"+flag+"' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				key+=" order by "+col+" "+sort;
			}
			List<DicType> list = new ArrayList<DicType>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}

	public List<DicType> findDicTypeSelectList(String flag) {
		String hql = "from DicType where 1=1 and type = '" +flag+ "'";
		List<DicType> list = this.getSession().createQuery(hql).list();
		return list;
	}
}
