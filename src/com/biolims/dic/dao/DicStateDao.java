package com.biolims.dic.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicState;

@Repository
public class DicStateDao extends BaseHibernateDao {
	
	public Map<String, Object> dicStateSelectTableJson(Integer start,
			Integer length, String query, String col, String sort, String flag) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicState where 1=1 and type='"
				+ flag + "'";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DicState where 1=1 and type='" + flag + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				key += " order by " + col + " " + sort;
			}
			List<DicState> list = new ArrayList<DicState>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}
}
