package com.biolims.dic.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicFailureReason;

@Repository
@SuppressWarnings( { "rawtypes", "unchecked" })
public class DicFailureReasonDao extends BaseHibernateDao {

	public Map<String, Object> selectDicFailureReasonList(Map mapForQuery, int startNum, int limitNum, String dir,
			String sort) throws Exception {
		String key = "";
		String hql = "from DicFailureReason where 1=1 ";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}

		hql = hql + key;
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<DicFailureReason> list = new ArrayList<DicFailureReason>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
				key = key + " order by " + sort + " " + dir;

			list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}
