package com.biolims.dic.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DicUnitDao extends CommonDAO {

	public Map<String, Object> selectDicUnit(String classify, Integer start,
			Integer length, String query, String col,String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicUnit where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		//String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		//if(!"all".equals(scopeId)){
		//	key+=" and scopeId='"+scopeId+"'";
		//}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from DicUnit where 1=1 ";
			/*if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}*/
			List<DicUnit> list = getSession().createQuery(hql+key+"order by orderNumber ASC").setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
/*	public Map<String, Object> selectDicUnit(int startNum, int limitNum, String dir, String sort) {
		String hql = "from DicUnit";
		Map<String, Object> map = new HashMap<String, Object>();
		Long total = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();
		List<DicUnit> list = null;
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
				hql = hql + " order by " + sort + " " + dir;
			else
				hql = hql + " order by orderNumber ASC";
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();

		}
		map.put("total", total);
		map.put("list", list);
		return map;
	}*/

	public List<DicUnit> selectDicUnit(String type) {
		String hql = "from DicUnit where type = '" + type + "' order by orderNumber ASC";
		return this.getSession().createQuery(hql).list();
	}

	public Map<String, Object> showUnitSelectTableJson(Integer start,
			Integer length, String query, String col, String sort) {


		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicUnit where 1=1 ";
		String key = "";
//		if(query!=null&&!"".equals(query)){
//			key=map2Where(query);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from DicUnit  where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<DicUnit> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
}
