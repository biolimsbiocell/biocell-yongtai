/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：数据库操作基础类
 * 创建人：
 * 创建时间：
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;

import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@SuppressWarnings( { "unchecked", "rawtypes" })
public abstract class BaseHibernateDao {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public <T> List<T> queryList(final String hql, final Integer start, final Integer limit) {
		Query query = getSession().createQuery(hql);
		List<T> list = null;
		if (start == null || limit == null) {
			list = query.list();
		} else {
			list = query.setFirstResult(start).setMaxResults(limit).list();
		}
		return list;
	}

	public <T> T queryUniqueResult(final String hql) {
		return (T) getSession().createQuery(hql).uniqueResult();
	}

	public <T> List<T> queryListByIds(final String objName, final String objProperty, final String ids) {
		Query query = getSession().createQuery("from " + objName + " where " + objProperty + " in (" + ids + ")");
		return query.list();
	}

	/**
	 * 
	 * 用主键检索记录,通用DAO
	 * 
	 * @param persistentClass
	 *            实体类
	 * @param id
	 *            检索主键字段值
	 * @return T，实体类数据
	 */
	public <T> T get(Class<T> persistentClass, Serializable id) {
		T t = (T) getSession().get(persistentClass, id);
		//getSession().clear();
		return t;
	}

	/**
	 * 
	 * 检索Unique LIST记录,通用DAO,采用Criteria
	 * 
	 * @param persistentClass
	 *            实体类
	 * @param propertyName
	 *            检索字段
	 * @param value
	 *            检索字段值
	 * @return T，实体类数据
	 */
	public <T> T findUniqueByProperty(Class<T> persistentClass, String propertyName, Object value) {
		Criteria criteria = getSession().createCriteria(persistentClass);
		criteria.add(Restrictions.eq(propertyName, value));
		T t = (T) criteria.uniqueResult();
		// ////getSession().clear();
		return t;
	}

	/**
	 * 
	 * 检索Object通用DAO,采用Criteria
	 * 
	 * @param startNum
	 *            开始记录数
	 * @param limitNum
	 *            每页记录数
	 * @param dir
	 *            DESC ASC
	 * @param sort
	 *            排序字段
	 * @param persistentClass
	 *            实体类 检索字段map 检索字段方式
	 * @param mapForQuery
	 *            检索字段map
	 * @param mapForCondition
	 *            检索字段方式map
	 * @return Map，数据List和数据记录数
	 */
	public <T> void saveFlush(T bean) {
		save(bean);
		getSession().flush();
	}

	/**
	 * 
	 * 检索LIST记录,通用DAO,采用Criteria
	 * 
	 * @param persistentClass
	 *            实体类
	 * @param propertyName
	 *            检索字段
	 * @param value
	 *            检索字段值
	 * @return T，实体类数据
	 */
	public <T> List<T> findByProperty(Class<T> persistentClass, String propertyName, Object value) {
		Assert.hasText(propertyName);
		Criteria criteria = getSession().createCriteria(persistentClass);
		criteria.add(Restrictions.eq(propertyName, value));
		List<T> list = criteria.list();
		return list;
	}

	/**
	 * 
	 * 检索Object一条记录,通用DAO,采用Criteria
	 * 
	 * @param persistentClass
	 *            实体类
	 * @param propertyName
	 *            检索字段
	 * @param value
	 *            检索字段值
	 * @return T，实体类数据
	 */
	public <T> T findOneByProperty(Class<T> persistentClass, String propertyName, Object value) {
		Criteria criteria = getSession().createCriteria(persistentClass);
		criteria.add(Restrictions.eq(propertyName, value));
		List<T> list = criteria.list();
		// getSession().clear();
		if (list.size() > 0) {
			return (T) list.get(0);
		} else {
			return null;
		}
	}

	/**
	 * 
	 * 检索LIST记录,通用DAO,采用HSQL方式,无检索条件数组,不分页
	 * 
	 * @param hql
	 *            检索HSQL
	 * @return List，实体类数据列表
	 */
	public <T> List<T> find(String hql) {
		Query q = getSession().createQuery(hql);
		return q.list();
	}

	/**
	 * 
	 * 检索LIST记录,通用DAO,采用HSQL方式,有检索条件数组,不分页
	 * 
	 * @param hql
	 *            检索HSQL
	 * @param values
	 *            检索条件数组
	 * @return List，实体类数据列表
	 */

	// hql->select * from sometable where a=:id and b=:somevalue
	// values->{1,'string'}
	// sql---select * from sometable where a=1 and b='string'
	public <T> List<T> find(String hql, Object... values) throws Exception {
		Query q = getSession().createQuery(hql);
		if (values != null && values.length > 0) {
			for (int i = 0; i < values.length; i++) {
				q.setParameter(i, values[i]);
			}
		}
		return q.list();
	}

	public <T> void deleteAll(List<T> list) {
		if (list != null && list.size() > 0) {
			for (T bean : list) {
				delete(bean);
			}
		}
	}

	public <T> void delete(T bean) {
		getSession().delete(bean);
	}

	public <T> void save(T bean) {
		getSession().save(bean);
	}

	public <T> void saveOrUpdate(T bean) {
		Field[] inputFields = bean.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			try {
				Object olds = BeanUtils.getFieldValue(bean, fd.getName());
				String data = JsonUtils.toJsonString(olds);
				if (data.contains("{")) {
					String[] oo = data.split("\"");
					data = oo[3];
					if ("".equals(data) || data == null || "\"\"".equals(data)) {
						BeanUtils.setFieldValue(bean, fd.getName(), null);
					} 
				} 

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		getSession().saveOrUpdate(bean);
	}

	public <T> void merge(T bean) {
		getSession().merge(bean);
	}

	public <T> void update(T bean) {
		getSession().update(bean);
	}

	/**
	 * 
	 * 检索LIST记录,通用DAO,采用HSQL方式,返回Map,封装成对象
	 * 
	 * @param hql
	 *            检索HSQL,用于单独检索字段,如:select new Map(p.id as id, p.name as
	 *            name,c.name as name,d as dicEduLevel) from User p left join
	 *            p.dicJob as c left join p.dicEduLevel as d
	 * @param o
	 *            封装的对象
	 * @return List，实体类数据列表
	 */
	public List findMap(String hql, Object o) throws Exception {

		List lists = new ArrayList();
		// List <Map>list = this.createQuery(hql).list();
		List<Map> list = this.getSession().createQuery(hql).list();
		Class c = o.getClass();
		for (Map<String, Object> aMap : list) {
			Object ob = c.newInstance();
			for (String aMapKey : aMap.keySet()) {

				ob = modifAttribute(aMapKey, aMap.get(aMapKey), ob);
			}
			lists.add(ob);

		}
		return lists;
	}

	private Object modifAttribute(String attribute, Object value, Object o) throws Exception {
		Field field = o.getClass().getDeclaredField(attribute);
		field.setAccessible(true);
		field.set(o, value);

		return o;

	}

	/**
	 * 
	 * 检索LIST记录,通用DAO,采用HSQL方式,返回MapLIST
	 * 
	 * @param hql
	 *            检索HSQL,用于单独检索字段,如:select new Map(p.id as id, p.name as
	 *            name,c.name as name,d as dicEduLevel) from User p left join
	 *            p.dicJob as c left join p.dicEduLevel as d
	 * @param o
	 *            封装的对象
	 * @return List，实体类数据列表
	 */
	public List findMap(String hql) throws Exception {
		// List <Map>list = this.createQuery(hql).list();
		List<Map> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 
	 * 检索Object通用DAO,采用HSQL方式,分页
	 * 
	 * @param shql
	 *            HSQL
	 * @param startNum
	 *            开始记录数
	 * @param limitNum
	 *            每页记录数
	 * @param dir
	 *            DESC ASC
	 * @param sort
	 *            排序字段
	 * @return Map，数据List和数据记录数
	 */
	public Map<String, Object> findObject(String shql, int startNum, int limitNum, String dir, String sort)
			throws Exception {
		StringBuffer hql = new StringBuffer(shql);
		if (dir != null && sort != null && !"".equals(dir) && !"".equals(sort)) {
			hql.append(" order by ").append(sort).append(" ").append(dir);
		}
		return pagedQuery(startNum, limitNum, hql.toString());
	}

	public Query createQuery(String queryString, Object... values) {
		Query queryObject = getSession().createQuery(queryString);
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				queryObject.setParameter(i, values[i]);
			}
		}
		return queryObject;
	}

	public void executeUpdate(String queryString, Object... values) {
		Query q = getSession().createQuery(queryString);
		if (values != null && values.length > 0) {
			for (int i = 0; i < values.length; i++) {
				q.setParameter(i, values[i]);
			}
		}
		q.executeUpdate();
	}

	public long getCount(String hql, Object... values) throws Exception {
		String countQueryString = " select count (*) " + removeSelect(removeOrders(hql));
		List<?> countlist = find(countQueryString, values);
		// getSession().clear();
		long totalCount = 0;
		if (countlist.size() != 0)
			return (Long) countlist.get(0);
		return totalCount;
	}

	private String removeSelect(String hql) {
		int beginPos = hql.toLowerCase().indexOf("from");
		return hql.substring(beginPos);
	}

	private String removeOrders(String hql) {
		Pattern p = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(hql);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(sb, "");
		}
		m.appendTail(sb);
		return sb.toString();
	}

	public <T> Map<String, Object> pagedQuery(int startNum, int limitNum, String hql, Object... values)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		long totalCount = this.getCount(hql, values);
		map.put("totalCount", totalCount);
		// if (totalCount < 1) {
		// map.put("list", new ArrayList<T>());
		// } else {
		Query query = createQuery(hql, values);
		List<T> list = query.setFirstResult(startNum).setMaxResults(limitNum).list();
		map.put("list", list);
		// }
		// //getSession().clear();
		return map;
	}

	/**
	 * 
	 * 存储字段以及值的map转化成指定的实体bean
	 * 
	 * @param propertiesMap
	 * @param bean
	 *            检索字段
	 * @return T，实体类数据
	 */
	public Object Map2Bean(Map<String, Object> propertiesMap, Object bean) {
		try {
			Map<String, Object> temp = propertiesMap;
			Map<String, Object> tempM = new HashMap<String, Object>();

			tempM.putAll(temp);

			for (String propertie1 : tempM.keySet()) {

				String[] properties1 = propertie1.split("[-]");
				if (properties1.length > 1) {
					Field field = null;

					Object tempO = bean;
					Class<?> temp1;
					Class<?> cc = tempO.getClass();
					field = cc.getDeclaredField(properties1[0]);
					temp1 = field.getType();

					Object value1 = temp.get(propertie1);
					if (properties1[1].equals("id")) {
						Object a = temp1.newInstance();
						BeanUtils.setFieldValue(a, properties1[1], value1);
						// Object a = findOneByProperty(temp1, properties1[1],
						// value1);
						temp.put(properties1[0], a);
					}
				}
			}

			for (String propertie : temp.keySet()) {

				Field[] fields = bean.getClass().getDeclaredFields();
				for (Field afield : fields) {

					if (afield.getName().equals(propertie)) {
						Object value = temp.get(propertie);
						String type = afield.getType().getName();
						// if(getFieldValue(bean,name)!=null){
						if (value != null) {
							if (type.equals("java.lang.Integer")) {
								String C = String.valueOf(value);
								if (!C.equals("")) {
									BeanUtils.setFieldValue(bean, afield.getName(), Integer.valueOf(C));
								}
							} else if (type.equals("int")) {
								String C = String.valueOf(value);
								if (!C.equals("")) {
									BeanUtils.setFieldValue(bean, afield.getName(), Integer.valueOf(C).intValue());
								}
							}else if (type.equals("java.lang.Double")) {
								String C = String.valueOf(value);
								if (!C.equals("")) {
									BeanUtils.setFieldValue(bean, afield.getName(), Double.valueOf(C));
								}
							} else if (type.equals("java.lang.Float")) {
								String C = String.valueOf(value);
								if (!C.equals("")) {
									BeanUtils.setFieldValue(bean, afield.getName(), Float.valueOf(C));
								}
							}

							else if (type.equals("java.util.Date")) {
								String C = String.valueOf(value);
								if (!C.equals("")){
									if(C.indexOf(":")!=-1){
										BeanUtils.setFieldValue(bean, afield.getName(), DateUtil.parse(String
												.valueOf(value),"yyyy-MM-dd HH:mm"));
									}else{
										BeanUtils.setFieldValue(bean, afield.getName(), DateUtil.parse(String
												.valueOf(value),"yyyy-MM-dd"));
									}
									
								}
							} else if (type.equals("java.sql.Timestamp")) {
								BeanUtils.setFieldValue(bean, afield.getName(), (Date) value);
							} else if (type.equals("java.lang.String")) {
								BeanUtils.setFieldValue(bean, afield.getName(), String.valueOf(value));
							} else {
								BeanUtils.setFieldValue(bean, afield.getName(), value);
							}
						}

					}
				}
			}
			return bean;

		} catch (Exception e) {
			e.printStackTrace();

		}

		return null;

	}
	
	public void saveOrUpdateAll(Collection all) {
		if (all != null && all.size() > 0) {
			for (Object o : all) {
				saveOrUpdate(o);
			}
		}
	}

	public String map2where4Split(Map<String, String> mapForQuery) {

		String whereStr = "";
		if (mapForQuery != null) {
			Set<String> key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {

				String sOld = (String) it.next();
				String s = sOld;
				boolean isDate = false;
				if (mapForQuery.get(sOld) != null && !mapForQuery.get(sOld).equals("")) {

					String[] sN;
					if (sOld.contains("##@@##")) {

						sN = sOld.split("##@@##");
						s = sN[0];

						if (sN.length > 2) {

							if (sN[2].equals("date")) {
								isDate = true;
							}
						}

						String ss = mapForQuery.get(sOld);

						if (isDate == true) {

							String[] a = ss.split(":");

							  if (a.length == 1) {
								  /* 565 */                 whereStr = whereStr + " or " + s + " " + sN[1] + " str_to_date('" + ss + "','%Y-%m-%d')";
								  /*     */               }
								  /* 567 */               if (a.length == 2) {
								  /* 568 */                 whereStr = whereStr + " or " + s + " " + sN[1] + " str_to_date('" + ss + "','%Y-%m-%d HH24:mi')";
								  /*     */               }
								  /*     */ 
								  /* 572 */               if (a.length == 3) {
								  /* 573 */                 whereStr = whereStr + " or " + s + " " + sN[1] + " str_to_date('" + ss + "','%Y-%m-%d H:i:s')";
								  /*     */               }

						} else
							whereStr += " and " + s + " " + sN[1] + " " + ss;

					} else {

						if (sOld.contains("##**##")) {

							sN = sOld.split("##**##");
							s = sN[0];

							if (sN.length > 2) {

								if (sN[2].equals("date")) {
									isDate = true;
								}
							}

							String ss = mapForQuery.get(sOld);

							if (isDate == true) {

								String[] a = ss.split(":");

								  if (a.length == 1) {
									  /* 565 */                 whereStr = whereStr + " or " + s + " " + sN[1] + " str_to_date('" + ss + "','%Y-%m-%d')";
									  /*     */               }
									  /* 567 */               if (a.length == 2) {
									  /* 568 */                 whereStr = whereStr + " or " + s + " " + sN[1] + " str_to_date('" + ss + "','%Y-%m-%d HH24:mi')";
									  /*     */               }
									  /*     */ 
									  /* 572 */               if (a.length == 3) {
									  /* 573 */                 whereStr = whereStr + " or " + s + " " + sN[1] + " str_to_date('" + ss + "','%Y-%m-%d H:i:s')";
									  /*     */               }

							} else
								whereStr += " or " + s + " " + sN[1] + " " + ss;

						} else {

							String whereS = s;

							if (!mapForQuery.get(s).startsWith("%") && !mapForQuery.get(s).endsWith("%"))
								whereS = whereS + " = '" + mapForQuery.get(s) + "'";
							else {
								whereS = whereS + " like '" + mapForQuery.get(s) + "'";

							}

							whereStr += " and " + whereS;

						}

					}

				}

			}
		}
		return whereStr;
	}

	public String map2where(Map<String, String> mapForQuery) {

		String whereStr = "";
		if (mapForQuery != null) {
			Set<String> key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {

				String sOld = (String) it.next();
				String s = sOld;
				if (mapForQuery.get(sOld) != null && !mapForQuery.get(sOld).equals("")) {

					String[] sN;
					if (sOld.contains("##@@##")) {

						sN = sOld.split("##@@##");
						s = sN[0];
					}

					String ss = mapForQuery.get(sOld);

					if (ss.contains("##@@##")) {

						String[] s1 = ss.split("##@@##");

						if (s.endsWith("Date") || s.endsWith("Time")) {

							String[] a = ss.split(":");
							if (a.length == 0) {
								whereStr += " and " + s + " " + s1[0] + " " + s1[1];
							}
							if (a.length == 1) {
								whereStr += " and " + s + " " + s1[0] + " str_to_date('" + s1[1] + "','%Y-%m-%d')";
								
								
							}
							if (a.length == 2) {
								whereStr += " and " + s + " " + s1[0] + " str_to_date('" + s1[1] + "','%Y-%m-%d HH24:mi')";

							}
							if (a.length == 3) {
								whereStr += " and " + s + " " + s1[0] + " str_to_date('" + s1[1] + "','%Y-%m-%d H:i:s')";
							}

						} else
							whereStr += " and " + s + " " + s1[0] + " " + s1[1];

					} else {
						String whereS = s;

						if (!mapForQuery.get(s).startsWith("%") && !mapForQuery.get(s).endsWith("%"))
							whereS = whereS + " = '" + mapForQuery.get(s) + "'";
						else {
							whereS = whereS + " like '" + mapForQuery.get(s) + "'";

						}

						whereStr += " and " + whereS;
					}

				}

			}
		}
		return whereStr;
	}
	public String map2Where(String query) throws Exception {
		Map<String, String> mapForQuery = (Map<String, String>)JsonUtils.toObjectByJson(query, Map.class);  
		String whereStr = "";
		if (mapForQuery != null) {
			Set<String> key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {

				String sOld = (String) it.next();
				String s = sOld;
				if (mapForQuery.get(sOld) != null && !mapForQuery.get(sOld).equals("")) {

					String[] sN;
					if (sOld.contains("##@@##")) {

						sN = sOld.split("##@@##");
						s = sN[0];
					}

					String ss = mapForQuery.get(sOld);

					if (ss.contains("##@@##")) {

						String[] s1 = ss.split("##@@##");
						String q="";
						if(s1[0].equals("s")){
							q=">=";
						}else if(s1[0].equals("e")){
							q="<=";
						}else{
							q="=";
						}

						if (s.endsWith("Date") || s.endsWith("Time")||s.endsWith("Birth")||s.endsWith("Start")||s.endsWith("End")) {

							String[] a = ss.split(":");
							if(s1.length>1){
							if (a.length == 0) {
								whereStr += " and " + s + " " + q + " " + s1[1];
							}
							if (a.length == 1) {
								whereStr += " and " + s + " " + q + " str_to_date('" + s1[1] + "','%Y-%m-%d')";
								
								
							}
							if (a.length == 2) {
								whereStr += " and " + s + " " + q + " str_to_date('" + s1[1] + "','%Y-%m-%d HH24:mi')";

							}
							if (a.length == 3) {
								whereStr += " and " + s + " " + q + " str_to_date('" + s1[1] + "','%Y-%m-%d H:i:s')";
							}
							}
						} else{
							if(s1.length>1){
								whereStr += " and " + s + " " + q + " " + s1[1];
							}
						}
							

					} else {
						String whereS = s.replace("-", ".");

						if (!mapForQuery.get(s).startsWith("%") && !mapForQuery.get(s).endsWith("%"))
							whereS = whereS + " = '" + mapForQuery.get(s) + "'";
						else {
							whereS = whereS + " like '" + mapForQuery.get(s) + "'";

						}

						whereStr += " and " + whereS;
					}

				}

			}
		}
		return whereStr;
	}
	public String map2whereNoDate(Map<String, String> mapForQuery) {

		String whereStr = "";
		if (mapForQuery != null) {
			Set<String> key = mapForQuery.keySet();
			for (Iterator it = key.iterator(); it.hasNext();) {

				String sOld = (String) it.next();
				String s = sOld;
				if (mapForQuery.get(sOld) != null && !mapForQuery.get(sOld).equals("")) {

					String[] sN;
					if (sOld.contains("##@@##")) {

						sN = sOld.split("##@@##");
						s = sN[0];
					}

					String ss = mapForQuery.get(sOld);

					if (ss.contains("##@@##")) {

						String[] s1 = ss.split("##@@##");

						whereStr += " and " + s + " " + s1[0] + " " + s1[1];

					} else {
						String whereS = s;

						if (!mapForQuery.get(s).startsWith("%") && !mapForQuery.get(s).endsWith("%"))
							whereS = whereS + " = '" + mapForQuery.get(s) + "'";
						else {
							whereS = whereS + " like '" + mapForQuery.get(s) + "'";

						}

						whereStr += " and " + whereS;
					}

				}

			}
		}
		return whereStr;
	}

	public String castStrId(String id){
		
		return " cast("+id+" as int) ";
	}
	 
	/**
	 * 使用指定的检索标准检索数据并分页返回数据For JDBC
	 */
	public Long getCountForJdbc(String sql) {
		return this.getJdbcTemplate().queryForLong(sql);
	}

	/**
	 * 使用指定的检索标准检索数据并分页返回数据For JDBC-采用预处理方式
	 * 
	 */
	public Long getCountForJdbcParam(String sql, Object[] objs) {
		return this.getJdbcTemplate().queryForLong(sql, objs);
	}

	public List<Map<String, Object>> findForJdbc(String sql, Object... objs) {
		return this.jdbcTemplate.queryForList(sql, objs);
	}

	public Integer executeSql(String sql, List<Object> param) {
		return this.jdbcTemplate.update(sql, param);
	}

	public Integer executeSql(String sql, Object... param) {
		return this.jdbcTemplate.update(sql, param);
	}

	public Integer executeSql(String sql) {
		return this.jdbcTemplate.update(sql);
	}

	public Integer countByJdbc(String sql, Object... param) {
		return this.jdbcTemplate.queryForInt(sql, param);
	}


}