package com.biolims.experiment.sanger.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import java.util.Date;
import com.biolims.dic.model.DicType;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: Sanger实验结果
 * @author lims-platform
 * @date 
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SANGER_TASK_INFO")
@SuppressWarnings("serial")
public class SangerTaskInfo extends EntityDao<SangerTaskInfo> implements
		java.io.Serializable {
	
	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 染色体位置 */
	private String chromosomalLocation;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** sanger结果*/
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private SangerTask sangerTask;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 是否提交*/
	private String submit;
	/** 患者姓名 */
	private String patientName;
	/** 浓度 */
	private Double concentration;
	/** 任务单Id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 临床 1 科研 2*/
	private String classify;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 样本类型 */
	private String sampleType;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/** 应出报告日期 */
	private Date reportDate;
	/** 单位组 */
	private DicType unitGroup;
	/** dye*/
	private String dye;
	/** alleleOne*/
	private String alleleOne;
	/** alleleTwo*/
	private String alleleTwo;
	/** sizeOne*/
	private String sizeOne;
	/** sizeTwo*/
	private String sizeTwo;
	/** heightOne*/
	private String heightOne;
	/** heightTwo*/
	private String heightTwo;
	/** 测序批次数据文档*/
	private String sequencingBatchDataFile;
	/**状态*/
	private String state;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血浆编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血浆编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 36)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得DicUnit
	 * 
	 * @return: DicUnit 单位
	 */
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	/**
	 * 方法: 取得SangerTask
	 * 
	 * @return: SangerTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SANGER_TASK")
	public SangerTask getSangerTask() {
		return this.sangerTask;
	}

	/**
	 * 方法: 设置SangerTask
	 * 
	 * @param: SangerTask 相关主表
	 */
	public void setSangerTask(SangerTask sangerTask) {
		this.sangerTask = sangerTask;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * @return the dye
	 */
	public String getDye() {
		return dye;
	}

	/**
	 * @param dye the dye to set
	 */
	public void setDye(String dye) {
		this.dye = dye;
	}

	/**
	 * @return the alleleOne
	 */
	public String getAlleleOne() {
		return alleleOne;
	}

	/**
	 * @param alleleOne the alleleOne to set
	 */
	public void setAlleleOne(String alleleOne) {
		this.alleleOne = alleleOne;
	}

	/**
	 * @return the alleleTwo
	 */
	public String getAlleleTwo() {
		return alleleTwo;
	}

	/**
	 * @param alleleTwo the alleleTwo to set
	 */
	public void setAlleleTwo(String alleleTwo) {
		this.alleleTwo = alleleTwo;
	}

	/**
	 * @return the sizeOne
	 */
	public String getSizeOne() {
		return sizeOne;
	}

	/**
	 * @param sizeOne the sizeOne to set
	 */
	public void setSizeOne(String sizeOne) {
		this.sizeOne = sizeOne;
	}

	/**
	 * @return the sizeTwo
	 */
	public String getSizeTwo() {
		return sizeTwo;
	}

	/**
	 * @param sizeTwo the sizeTwo to set
	 */
	public void setSizeTwo(String sizeTwo) {
		this.sizeTwo = sizeTwo;
	}

	/**
	 * @return the heightOne
	 */
	public String getHeightOne() {
		return heightOne;
	}

	/**
	 * @param heightOne the heightOne to set
	 */
	public void setHeightOne(String heightOne) {
		this.heightOne = heightOne;
	}

	/**
	 * @return the heightTwo
	 */
	public String getHeightTwo() {
		return heightTwo;
	}

	/**
	 * @param heightTwo the heightTwo to set
	 */
	public void setHeightTwo(String heightTwo) {
		this.heightTwo = heightTwo;
	}

	/**
	 * @return the sequencingBatchDataFile
	 */
	public String getSequencingBatchDataFile() {
		return sequencingBatchDataFile;
	}

	/**
	 * @param sequencingBatchDataFile the sequencingBatchDataFile to set
	 */
	public void setSequencingBatchDataFile(String sequencingBatchDataFile) {
		this.sequencingBatchDataFile = sequencingBatchDataFile;
	}

	/**
	 * @return the chromosomalLocation
	 */
	public String getChromosomalLocation() {
		return chromosomalLocation;
	}

	/**
	 * @param chromosomalLocation the chromosomalLocation to set
	 */
	public void setChromosomalLocation(String chromosomalLocation) {
		this.chromosomalLocation = chromosomalLocation;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}