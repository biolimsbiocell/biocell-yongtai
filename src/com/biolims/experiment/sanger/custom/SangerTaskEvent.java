package com.biolims.experiment.sanger.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.sanger.service.SangerTaskService;

public class SangerTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SangerTaskService mbService = (SangerTaskService) ctx
				.getBean("sangerTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
