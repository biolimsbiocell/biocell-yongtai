package com.biolims.experiment.sanger.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sanger.model.SangerTask;
import com.biolims.experiment.sanger.model.SangerTaskCos;
import com.biolims.experiment.sanger.model.SangerTaskItem;
import com.biolims.experiment.sanger.model.SangerTaskReagent;
import com.biolims.experiment.sanger.model.SangerTaskTemp;
import com.biolims.experiment.sanger.model.SangerTaskTemplate;
import com.biolims.experiment.sanger.model.SangerTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class SangerTaskDao extends BaseHibernateDao {

	public Map<String, Object> findSangerTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SangerTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SangerTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SangerTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectSangerTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from SangerTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<SangerTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from SangerTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findSangerTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SangerTaskItem where 1=1 and state='1' and sangerTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SangerTaskItem  where 1=1 and state='1' and sangerTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SangerTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<SangerTaskItem> showWellPlate(String id) {
		String hql="from SangerTaskItem  where 1=1 and state='2' and  sangerTask.id='"+id+"'";
		List<SangerTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findSangerTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SangerTaskItem where 1=1 and state='2' and sangerTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SangerTaskItem  where 1=1 and state='2' and sangerTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SangerTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from SangerTaskItem where 1=1 and state='2' and sangerTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from SangerTaskItem where 1=1 and state='2' and sangerTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<SangerTaskTemplate> showSangerTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from SangerTaskTemplate where 1=1 and sangerTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<SangerTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from SangerTaskTemplate where 1=1 and sangerTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<SangerTaskReagent> showSangerTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from SangerTaskReagent where 1=1 and sangerTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<SangerTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from SangerTaskReagent where 1=1 and sangerTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<SangerTaskCos> showSangerTaskCosJson(String id,String code) {
		String countHql = "select count(*) from SangerTaskCos where 1=1 and sangerTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<SangerTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from SangerTaskCos where 1=1 and sangerTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<SangerTaskTemplate> delTemplateItem(String id) {
		List<SangerTaskTemplate> list=new ArrayList<SangerTaskTemplate>();
		String hql = "from SangerTaskTemplate where 1=1 and sangerTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<SangerTaskReagent> delReagentItem(String id) {
		List<SangerTaskReagent> list=new ArrayList<SangerTaskReagent>();
		String hql = "from SangerTaskReagent where 1=1 and sangerTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<SangerTaskCos> delCosItem(String id) {
		List<SangerTaskCos> list=new ArrayList<SangerTaskCos>();
		String hql = "from SangerTaskCos where 1=1 and sangerTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showSangerTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SangerTaskInfo where 1=1 and sangerTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SangerTaskInfo  where 1=1 and sangerTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SangerTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from SangerTaskItem where 1=1 and sangerTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<SangerTaskItem> plateSample(String id, String counts) {
		String hql="from SangerTaskItem where 1=1 and sangerTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<SangerTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SangerTaskItem where 1=1 and sangerTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SangerTaskItem  where 1=1 and sangerTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SangerTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<SangerTaskInfo> findSangerTaskInfoByCode(String code) {
		String hql="from SangerTaskInfo where 1=1 and code='"+code+"'";
		List<SangerTaskInfo> list=new ArrayList<SangerTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SangerTaskInfo> selectAllResultListById(String code) {
		String hql = "from SangerTaskInfo  where (submit is null or submit='') and sangerTask.id='"
				+ code + "'";
		List<SangerTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SangerTaskInfo> selectResultListById(String id) {
		String hql = "from SangerTaskInfo  where sangerTask.id='"
				+ id + "'";
		List<SangerTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SangerTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from SangerTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SangerTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<SangerTaskItem> selectSangerTaskItemList(String scId)
			throws Exception {
		String hql = "from SangerTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sangerTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SangerTaskItem> list = new ArrayList<SangerTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from SangerTaskItem where 1=1 and sangerTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public SangerTaskInfo getResultByCode(String code) {
			String hql=" from SangerTaskInfo where 1=1 and code='"+code+"'";
			SangerTaskInfo spi=(SangerTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}