package com.biolims.experiment.sanger.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.sanger.dao.SangerTaskDao;
import com.biolims.experiment.sanger.model.SangerTask;
import com.biolims.experiment.sanger.model.SangerTaskAbnormal;
import com.biolims.experiment.sanger.model.SangerTaskCos;
import com.biolims.experiment.sanger.model.SangerTaskInfo;
import com.biolims.experiment.sanger.model.SangerTaskItem;
import com.biolims.experiment.sanger.model.SangerTaskReagent;
import com.biolims.experiment.sanger.model.SangerTaskTemp;
import com.biolims.experiment.sanger.model.SangerTaskTemplate;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;
@Service
@Transactional
public class SangerTaskService {
	
	@Resource
	private SangerTaskDao sangerTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : 
	 * @date 
	 * @param id
	 * @return SangerTask
	 * @throws
	 */
	public SangerTask get(String id) {
		SangerTask sangerTask = commonDAO.get(SangerTask.class, id);
		return sangerTask;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSangerTaskItem(String delStr, String[] ids, User user, String sangerTask_id) throws Exception {
		String delId="";
		for (String id : ids) {
			SangerTaskItem scp = sangerTaskDao.get(SangerTaskItem.class, id);
			if (scp.getId() != null) {
				SangerTask pt = sangerTaskDao.get(SangerTask.class, scp
						.getSangerTask().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				SangerTaskTemp sangerTaskTemp = this.commonDAO.get(
						SangerTaskTemp.class, scp.getTempId());
				if (sangerTaskTemp != null) {
					sangerTaskTemp.setState("1");
					sangerTaskDao.update(sangerTaskTemp);
				}
				sangerTaskDao.update(pt);
				sangerTaskDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(sangerTask_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delSangerTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSangerTaskItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			SangerTaskItem scp = sangerTaskDao.get(SangerTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				sangerTaskDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSangerTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			SangerTaskInfo scp = sangerTaskDao
					.get(SangerTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				sangerTaskDao.delete(scp);
		}
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSangerTaskReagent(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			SangerTaskReagent scp = sangerTaskDao.get(SangerTaskReagent.class,
					id);
			sangerTaskDao.delete(scp);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSangerTaskCos(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			SangerTaskCos scp = sangerTaskDao.get(SangerTaskCos.class, id);
			sangerTaskDao.delete(scp);
		}
	}

	/**
	 * 
	 * @Title: saveSangerTaskTemplate
	 * @Description: 保存模板
	 * @author : 
	 * @date 
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveSangerTaskTemplate(SangerTask sc) {
		List<SangerTaskTemplate> tlist2 = sangerTaskDao.delTemplateItem(sc
				.getId());
		List<SangerTaskReagent> rlist2 = sangerTaskDao.delReagentItem(sc
				.getId());
		List<SangerTaskCos> clist2 = sangerTaskDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		for (TemplateItem t : tlist) {
			SangerTaskTemplate ptt = new SangerTaskTemplate();
			ptt.setSangerTask(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			sangerTaskDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			SangerTaskReagent ptr = new SangerTaskReagent();
			ptr.setSangerTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			sangerTaskDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			SangerTaskCos ptc = new SangerTaskCos();
			ptc.setSangerTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			sangerTaskDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : 
	 * @date 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SangerTask sct = sangerTaskDao.get(SangerTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		sangerTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		SangerTask sc = this.sangerTaskDao.get(SangerTask.class, id);
		// 获取结果表样本信息
		List<SangerTaskInfo> list;
		if (ids == null)
			list = this.sangerTaskDao.selectAllResultListById(id);
		else
			list = this.sangerTaskDao.selectAllResultListByIds(ids);

		for (SangerTaskInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("SangerTaskInfo");
						sangerTaskDao.saveOrUpdate(st);
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0017")) {// 核酸提取
						DnaTaskTemp d = new DnaTaskTemp();
						sampleInputService.copy(d, scp);
						
						DicSampleType t = commonDAO.get(DicSampleType.class,
								scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						if (scp.getProductId() != null) {
							Product p = commonDAO.get(Product.class,
									scp.getProductId());
						}
						d.setState("1");
						sangerTaskDao.saveOrUpdate(d);
					} else if (next.equals("0018")) {// 文库构建
						WkTaskTemp d = new WkTaskTemp();
						sampleInputService.copy(d, scp);
						DicSampleType t = commonDAO.get(DicSampleType.class,
								scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						if (scp.getProductId() != null) {
							Product p = commonDAO.get(Product.class,
									scp.getProductId());
						}
						d.setState("1");
						sangerTaskDao.saveOrUpdate(d);
					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					SangerTaskAbnormal pa = new SangerTaskAbnormal();// 样本异常
					sampleInputService.copy(pa, scp);
					pa.setOrderId(sc.getId());
					pa.setState("1");
					pa.setScopeId(sc.getScopeId());
					pa.setScopeName(sc.getScopeName());
					commonDAO.saveOrUpdate(pa);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"SangerTask",
								"Sanger实验",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				
				scp.setSubmit("1");
				sangerTaskDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findSangerTaskTable
	 * @Description: 展示主表
	 * @author :
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findSangerTaskTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return sangerTaskDao.findSangerTaskTable(start, length, query, col,
				sort);
	}

	/**
	 * 
	 * @Title: selectSangerTaskTempTable
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectSangerTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return sangerTaskDao.selectSangerTaskTempTable(codes,start, length, query,
				col, sort);
	}
	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : 
	 * @date 
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			SangerTask pt = new SangerTask();
			pt = (SangerTask) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getName();
			Integer sampleNum=pt.getSampleNum();
			Integer maxNum=pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "SangerTask";
				String markCode = "Sanger";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				
			} else {
				id = pt.getId();
				pt=commonDAO.get(SangerTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			t.setId(templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			pt.setTestUserOneName(userName);
			sangerTaskDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveSangerTaskTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							SangerTaskTemp ptt = sangerTaskDao.get(
									SangerTaskTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									SangerTaskItem pti = new SangerTaskItem();
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									pti.setTempId(temp);
									pti.setSangerTask(pt);
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									sangerTaskDao.saveOrUpdate(pti);
								}
							} else {
								SangerTaskItem pti = new SangerTaskItem();
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								if (t.getStorageContainer() != null) {
									pti.setState("1");
								}else{
									pti.setState("2");
								}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setSangerTask(pt);
								sangerTaskDao.saveOrUpdate(pti);
							}
							sangerTaskDao.saveOrUpdate(ptt);
						}
				}

			
			}
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findSangerTaskItemTable
	 * @Description:展示未排版样本
	 * @author : 
	 * @date 
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findSangerTaskItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sangerTaskDao.findSangerTaskItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : 
	 * @date 
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<SangerTaskItem> saveItems = new ArrayList<SangerTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		SangerTask pt = commonDAO.get(SangerTask.class, id);
		SangerTaskItem scp = new SangerTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (SangerTaskItem) sangerTaskDao.Map2Bean(map, scp);
			SangerTaskItem pti = commonDAO.get(SangerTaskItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());    
			scp.setSangerTask(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		sangerTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : 
	 * @date 
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<SangerTaskItem>
	 * @throws
	 */
	public List<SangerTaskItem> showWellPlate(String id) throws Exception {
		List<SangerTaskItem> list = sangerTaskDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findSangerTaskItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date 
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findSangerTaskItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sangerTaskDao.findSangerTaskItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			SangerTaskItem pti = commonDAO.get(SangerTaskItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			sangerTaskDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showSangerTaskStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date 
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showSangerTaskStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<SangerTaskTemplate> pttList = sangerTaskDao
				.showSangerTaskStepsJson(id, code);
		List<SangerTaskReagent> ptrList = sangerTaskDao
				.showSangerTaskReagentJson(id, code);
		List<SangerTaskCos> ptcList = sangerTaskDao.showSangerTaskCosJson(id,
				code);
		List<Object> plate = sangerTaskDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showSangerTaskResultTableJson
	 * @Description: 展示结果
	 * @author : 
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showSangerTaskResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sangerTaskDao.showSangerTaskResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		SangerTask pt = commonDAO.get(SangerTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SangerTaskItem> list = sangerTaskDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
		row = pt.getTemplate().getStorageContainer().getRowNum();
		col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = sangerTaskDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			SangerTask pt = sangerTaskDao.get(SangerTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<SangerTaskInfo> listPTI = sangerTaskDao
								.findSangerTaskInfoByCode(code);
						if (listPTI.size() > 0) {
							SangerTaskInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader
									.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							spi.setDye(reader.get(6));
							spi.setAlleleOne(reader.get(7));
							spi.setAlleleTwo(reader.get(8));
							spi.setSizeOne(reader.get(9));
							spi.setSizeTwo(reader.get(10));
							spi.setHeightOne(reader.get(11));
							spi.setHeightTwo(reader.get(12));
							spi.setSequencingBatchDataFile(reader.get(13));
							spi.setChromosomalLocation(reader.get(14));
							sangerTaskDao.saveOrUpdate(spi);
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : 
	 * @date 
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}	
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date 
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			SangerTaskTemplate ptt = sangerTaskDao.get(
					SangerTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			sangerTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : 
	 * @date 
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
	
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			SangerTaskReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(SangerTaskReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new SangerTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setSangerTask(commonDAO.get(SangerTask.class, id));
				}

			}
			sangerTaskDao.saveOrUpdate(ptr);
		}
	
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
	
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			SangerTaskCos ptc=null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(SangerTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new SangerTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setSangerTask(commonDAO.get(SangerTask.class, id));
				}
			}
			sangerTaskDao.saveOrUpdate(ptc);
		}
	
	
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : 
	 * @date 
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sangerTaskDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
	
	
		List<SangerTaskItem> list = sangerTaskDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SangerTaskInfo> spiList = sangerTaskDao.selectResultListById(id);
		for (SangerTaskItem pti : list) {
			boolean b = true;
			for (SangerTaskInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							SangerTaskInfo scp = new SangerTaskInfo();
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setSangerTask(pti.getSangerTask());
							scp.setResult("1");
							String markCode = "";
							if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = str;
									} else {
										markCode = str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = scp.getSampleCode();
									} else {
										markCode = scp.getSampleCode()
												+ d.getCode();
									}
								}

							}
							String code = codingRuleService.getCode(
									"SangerTaskInfo", markCode, 00, 2, null);
							scp.setCode(code);
							sangerTaskDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								SangerTaskInfo scp = new SangerTaskInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setSangerTask(pti.getSangerTask());
								scp.setResult("1");
								String markCode = "";
								if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}
								String code = codingRuleService.getCode(
										"SangerTaskInfo", markCode, 00, 2,
										null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								sangerTaskDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						SangerTaskInfo spi = sangerTaskDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						sangerTaskDao.saveOrUpdate(spi);
					}
				}
			}
		}
	
	
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser) throws Exception {

		List<SangerTaskInfo> saveItems = new ArrayList<SangerTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		SangerTask pt = commonDAO.get(SangerTask.class, id);		
		for (Map<String, Object> map : list) {
		SangerTaskInfo scp = new SangerTaskInfo();
			// 将map信息读入实体类
			scp = (SangerTaskInfo) sangerTaskDao.Map2Bean(map, scp);
			scp.setSangerTask(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			sangerTaskDao.saveOrUpdate(pt);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		sangerTaskDao.saveOrUpdateAll(saveItems);

	}

	public List<SangerTaskItem> findSangerTaskItemList(String scId)
			throws Exception {
		List<SangerTaskItem> list = sangerTaskDao.selectSangerTaskItemList(scId);
		return list;
	}
	public Integer generateBlendCode(String id) {
		return sangerTaskDao.generateBlendCode(id);
	}

	public SangerTaskInfo getInfoById(String id) {
		// TODO Auto-generated method stub
		return sangerTaskDao.get(SangerTaskInfo.class, id);
	}
}

