﻿package com.biolims.experiment.sanger.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.sanger.dao.SangerTaskDao;
import com.biolims.experiment.sanger.model.SangerTask;
import com.biolims.experiment.sanger.model.SangerTaskInfo;
import com.biolims.experiment.sanger.model.SangerTaskItem;
import com.biolims.experiment.sanger.model.SangerTaskTemp;
import com.biolims.experiment.sanger.service.SangerTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
@Namespace("/experiment/sanger/sangerTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SangerTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private SangerTaskService sangerTaskService;
	private SangerTask sangerTask = new SangerTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SangerTaskDao sangerTaskDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	/**
	 * 
	 * @Title: showSangerTaskList
	 * @Description:展示主表
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSangerTaskTable")
	public String showSangerTaskTable() throws Exception {
		rightsId="2416";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTask.jsp");
	}

	@Action(value = "showSangerTaskTableJson")
	public void showSangerTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sangerTaskService.findSangerTaskTable(
					start, length, query, col, sort);
			List<SangerTask> list = (List<SangerTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: editSangerTask
	 * @Description: 新建实验单
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editSangerTask")
	public String editSangerTask() throws Exception {
		rightsId="2415";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			sangerTask = sangerTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
			if (sangerTask.getMaxNum() == null) {
				sangerTask.setMaxNum(0);
			}
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			sangerTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sangerTask.setCreateUser(user);
			sangerTask.setMaxNum(0);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			sangerTask.setCreateDate(stime);
			sangerTask
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sangerTask
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<Template> templateList = templateService
				.showDialogTemplateTableJson("SangerTask",null);
		List<Template> selTemplate = new ArrayList<Template>();
		List<UserGroupUser> userList = new ArrayList<UserGroupUser>();
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int j = 0; j < templateList.size(); j++) {
			if(templateList.get(j).getAcceptUser()!=null){
				List<UserGroupUser> userTempList = (List<UserGroupUser>) userGroupUserService.getUserGroupUserBygroupId(templateList.get(j).getAcceptUser().getId())
						.get("list");
				for(UserGroupUser ugu:userTempList){
					if(!userList.contains(ugu)){
						userList.add(ugu);
					}
				}
			}
			if (sangerTask.getTemplate() != null) {
				if (sangerTask.getTemplate().getId()
						.equals(templateList.get(j).getId())) {
					selTemplate.add(templateList.get(j));
					templateList.remove(j);
					j--;
				}
			}
		}
		for (int i = 0; i < userList.size(); i++) {
			if (sangerTask.getTestUserOneId() != null) {
				String [] userOne=sangerTask.getTestUserOneId().split(",");
				one:for(String u:userOne){
					if(u.equals(userList.get(i).getUser().getId())){
						selUser.add(userList.get(i));
						userList.remove(i);
						i--;
						break one;
					}
				}
			}
		}
		putObjToContext("template", templateList);
		putObjToContext("selTemplate", selTemplate);
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		toState(sangerTask.getState());
		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskAllot.jsp");
	}

	/**
	 * 
	 * @Title: showSangerTaskItemTable
	 * @Description: 展示待排板列表
	 * @author :
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSangerTaskItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSangerTaskItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		sangerTask = sangerTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskMakeUp.jsp");
	}

	@Action(value = "showSangerTaskItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSangerTaskItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sangerTaskService
					.findSangerTaskItemTable(scId, start, length, query, col,
							sort);
			List<SangerTaskItem> list = (List<SangerTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("chromosomalLocation", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showSangerTaskItemAfTableJson
	 * @Description: 排板后样本展示
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */

	@Action(value = "showSangerTaskItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSangerTaskItemAfTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sangerTaskService
					.findSangerTaskItemAfTable(scId, start, length, query, col,
							sort);
			List<SangerTaskItem> list = (List<SangerTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("sangerTask-name", "");
			map.put("sangerTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("chromosomalLocation", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("color", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delSangerTaskItem
	 * @Description: 删除待排板样本
	 * @author
	 * @date
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delSangerTaskItem")
	public void delSangerTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr=getParameterFromRequest("del");
		String id=getParameterFromRequest("id");
		User user =(User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sangerTaskService.delSangerTaskItem(delStr,ids,user,id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: delSangerTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delSangerTaskItemAf")
	public void delSangerTaskItemAf() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sangerTaskService.delSangerTaskItemAf(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBSangerTaskResultTable
	 * @Description
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSangerTaskResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSangerTaskResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		sangerTask = sangerTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskResult.jsp");
	}

	@Action(value = "showSangerTaskResultTableJson")
	public void showSangerTaskResultTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sangerTaskService
					.showSangerTaskResultTableJson(id, start, length, query,
							col, sort);
			List<SangerTaskInfo> list = (List<SangerTaskInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("note", "");
			map.put("dye", "");
			map.put("chromosomalLocation", "");
			map.put("alleleOne", "");
			map.put("alleleTwo", "");
			map.put("sizeOne", "");
			map.put("sizeTwo", "");
			map.put("heightOne", "");
			map.put("heightTwo", "");
			map.put("sequencingBatchDataFile", "");
			map.put("sangerTask-id", "");
			map.put("sangerTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: delSangerTaskResult
	 * @Description: 删除结果明细
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delSangerTaskResult")
	public void delSangerTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sangerTaskService.delSangerTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showSangerTaskSteps
	 * @Description: 实验步骤
	 * @author : 
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSangerTaskSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSangerTaskSteps() throws Exception {
		String id = getParameterFromRequest("id");
		sangerTask = sangerTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskSteps.jsp");
	}

	@Action(value = "showSangerTaskStepsJson")
	public void showSangerTaskStepsJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = sangerTaskService.showSangerTaskStepsJson(id, orderNum);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSangerTaskReagent")
	public void delSangerTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			sangerTaskService.delSangerTaskReagent(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSangerTaskCos")
	public void delSangerTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			sangerTaskService.delSangerTaskCos(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: makeCode
	 * @Description: 打印条码
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String id = getParameterFromRequest("id");
		String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
		CodeMain codeMain = null;
		codeMain = codeMainService.get(id);
		if (codeMain != null) {
			String printStr = "";
			String context="";
			for(int a=0;a<sampleCode.length;a++){
			String codeFull = sampleCode[a];
			String name= "";
			//sampleReceiveService.getNameBySampleCode(codeFull);
			printStr = codeMain.getCode();
			String code1 = sampleCode[a].substring(0, 9);
			String code2 = sampleCode[a].substring(9);
			printStr = printStr.replaceAll("@@code1@@", code1);
			printStr = printStr.replaceAll("@@code2@@", code2);
			printStr = printStr.replaceAll("@@code@@", codeFull);
			printStr = printStr.replaceAll("@@name@@",name );
			context+=printStr;
			}
			String ip = codeMain.getIp();
			Socket socket = null;
			OutputStream os;
			try {
				System.out.println(context);
				socket = new Socket();
				SocketAddress sa = new InetSocketAddress(ip, 9100);
				socket.connect(sa);
				os = socket.getOutputStream();
				os.write(context.getBytes("UTF-8"));
				os.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @Title: showSangerTaskFromReceiveList
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSangerTaskTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSangerTaskFromReceiveList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskTemp.jsp");
	}

	@Action(value = "showSangerTaskTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSangerTaskTempTableJson() throws Exception {
				
		String [] codes=getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sangerTaskService
					.selectSangerTaskTempTable(codes,start, length, query, col, sort);
			List<SangerTaskTemp> list = (List<SangerTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("counts", "");
			map.put("productName", "");
			map.put("sampleType", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-project-id", "");
			map.put("sampleInfo-changeType", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配页面
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "saveAllot")
	public void saveAllot() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = sangerTaskService.saveAllot(main, tempId, userId,
					templateId,logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排版界面
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveMakeUp")
	public void saveMakeUp() throws Exception {
		String blood_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sangerTaskService.saveMakeUp(blood_id, item,logInfo);
			result.put("success", true);
			result.put("id", blood_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveSteps")
	public void saveSteps() throws Exception {
		String id = getParameterFromRequest("id");
		String templateJson = getParameterFromRequest("templateJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String logInfo=getParameterFromRequest("logInfo");
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sangerTaskService.saveSteps(id, templateJson, reagentJson, cosJson,logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveResult")
	public void saveResult() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		String confirmUser=getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sangerTaskService.saveResult(id, dataJson,logInfo,confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示排版
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<SangerTaskItem> json = sangerTaskService.showWellPlate(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			sangerTaskService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateSample")
	public void plateSample() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = sangerTaskService.plateSample(id, counts);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description:展示孔板样本列表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateSampleTable")
	public void plateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = sangerTaskService.plateSampleTable(id, counts, start,
					length, query, col, sort);
			List<SangerTaskItem> list = (List<SangerTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("sangerTask-name", "");
			map.put("sangerTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: uploadCsvFile
	 * @Description: 上传结果
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			sangerTaskService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "bringResult")
	public void bringResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			sangerTaskService.bringResult(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: generateBlendCode
	 * @Description: 生成混合号
	 * @author : shengwei.wang
	 * @date 2018年3月6日上午11:02:26 void
	 * @throws
	 */
	@Action(value = "generateBlendCode")
	public void generateBlendCode() {
		String id = getParameterFromRequest("id");
		try {
			Integer blendCode = sangerTaskService.generateBlendCode(id);
			HttpUtils.write(String.valueOf(blendCode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: submitSample  
	 * @Description: 提交样本  
	 * @author : shengwei.wang
	 * @date 2018年3月22日下午5:39:40
	 * void
	 * @throws
	 */
	@Action(value="submitSample")
	public void submitSample() throws Exception{
		String id=getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			sangerTaskService.submitSample(id, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: downLoadTemp @Description: TODO(核算提取模板下载) @param @param
	 * ids @param @throws Exception    设定文件 @return void    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-8-22 上午11:46:55 @throws
	 */
	@Action(value = "downLoadTemp")
	public void downLoadTemp() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids");
		String[] codes = getRequest().getParameterValues("codes");
		String id = ids[0];
		String code = codes[0];
		String[] str1 = code.split(",");
		String co = str1[0];
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = co + sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		properties.load(is);

		String filePath = properties.getProperty("result.template.file") + "\\";// 写入csv路径
		String fileName = filePath + a + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "样本编号", "原始样本编号", "产物类型", "检测项目", "浓度", 
				  "体积","dye","alleleOne","alleleTwo","sizeOne","sizeTwo","heightOne","heightTwo","测序批次数据文档","位点" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		String[] sid = id.split(",");
		for (int j = 0; j < sid.length; j++) {
			String idc = sid[j];
			for (int i = 0; i < ids.length; i++) {
				SangerTaskInfo sr=sangerTaskService.getInfoById(idc);
				StringBuffer sb = new StringBuffer();
				setMolecularMarkersData(sr, sb);
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
				if (sr.equals("")) {
					result.put("success", false);
				} else {
					result.put("success", true);

				}
			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3(a, filePath);
	}

	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData @Description: TODO(将对应的值添加到模板里) @param @param
	 * sr @param @param sb @param @throws Exception    设定文件 @return void   
	 * 返回类型 @author zhiqiang.yang@biolims.cn @date 2017-8-22 下午1:21:58 @throws
	 */
	public void setMolecularMarkersData(SangerTaskInfo sr, StringBuffer sb) throws Exception {
		if (null!=sr.getCode()) {
			sb.append("\"").append(sr.getCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null!=sr.getSampleCode()) {
			sb.append("\"").append(sr.getSampleCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null!=sr.getSampleType()) {
			sb.append("\"").append(sr.getSampleType()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null!=sr.getDicSampleType().getName()) {
			sb.append("\"").append(sr.getDicSampleType().getName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		
		if (null!=sr.getProductName()) {
			sb.append("\"").append(sr.getProductName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
	}

	/**
	 * 
	 * @Title: toUtf8String @Description: TODO(解决乱码) @param @param
	 * s @param @return    设定文件 @return String    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-8-23 下午4:40:07 @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SangerTaskService getSangerTaskService() {
		return sangerTaskService;
	}

	public void setSangerTaskService(SangerTaskService sangerTaskService) {
		this.sangerTaskService = sangerTaskService;
	}

	public SangerTask getSangerTask() {
		return sangerTask;
	}

	public void setSangerTask(SangerTask sangerTask) {
		this.sangerTask = sangerTask;
	}

	public SangerTaskDao getSangerTaskDao() {
		return sangerTaskDao;
	}

	public void setSangerTaskDao(SangerTaskDao sangerTaskDao) {
		this.sangerTaskDao = sangerTaskDao;
	}

}
