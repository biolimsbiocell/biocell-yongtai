package com.biolims.experiment.bobs.sample.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: BoBs样本处理结果
 * @author lims-platform
 * @date 2016-06-03 15:33:09
 * @version V1.0
 * 
 */
@Entity
@Table(name = "BOBS_SAMPLE_RESULT")
@SuppressWarnings("serial")
public class BobsSampleResult extends EntityDao<BobsSampleResult> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	
	/** 异常原因 */
	private String ycReasonId;
	/** 异常原因 */
	private String ycReasonName;
	
	
	/** 样本编号 */
	private String code;
	/** 实验编号 */
	private String orderNumber;
	/** 核对样本编号 */
	private String checkCode;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本类型 */
	private String sampleType;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 纯化后浓度 */
	private String concentration;
	/** 260/280 */
	private String od280;
	/** 260/230 */
	private String od230;
	/** 浓度调解后总体积 */
	private String volume;
	/** 浓度调解后稀释液 */
	private String diluent;
	/** 下一步流向id */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 是否合格 */
	private String result;
	/** 是否提交 */
	private String submit;
	/** 失败原因 */
	private String reason;
	/** 状态 */
	private String state;
	/** 关联任务单 */
	private String orderId;
	/** 相关主表 */
	private BobsSample bobsSample;
	/** 备注 */
	private String note;
	// 临时表Id
	private String tempId;

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验编号
	 */
	@Column(name = "ORDER_NUMBER", length = 50)
	public String getOrderNumber() {
		return this.orderNumber;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验编号
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对样本编号
	 */
	@Column(name = "CHECK_CODE", length = 50)
	public String getCheckCode() {
		return this.checkCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对样本编号
	 */
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 50)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 纯化后浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public String getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 纯化后浓度
	 */
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 260/280
	 */
	@Column(name = "OD280", length = 50)
	public String getOd280() {
		return this.od280;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 260/280
	 */
	public void setOd280(String od280) {
		this.od280 = od280;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 260/230
	 */
	@Column(name = "OD230", length = 50)
	public String getOd230() {
		return this.od230;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 260/230
	 */
	public void setOd230(String od230) {
		this.od230 = od230;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 浓度调解后总体积
	 */
	@Column(name = "VOLUME", length = 50)
	public String getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 浓度调解后总体积
	 */
	public void setVolume(String volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 浓度调解后稀释液
	 */
	@Column(name = "DILUENT", length = 50)
	public String getDiluent() {
		return this.diluent;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 浓度调解后稀释液
	 */
	public void setDiluent(String diluent) {
		this.diluent = diluent;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向id
	 */
	@Column(name = "NEXT_FLOW_ID", length = 50)
	public String getNextFlowId() {
		return this.nextFlowId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向id
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否合格
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否合格
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否提交
	 */
	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return this.submit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否提交
	 */
	public void setSubmit(String submit) {
		this.submit = submit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得BobsSample
	 * 
	 * @return: BobsSample 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BOBS_SAMPLE")
	public BobsSample getBobsSample() {
		return this.bobsSample;
	}

	/**
	 * 方法: 设置BobsSample
	 * 
	 * @param: BobsSample 相关主表
	 */
	public void setBobsSample(BobsSample bobsSample) {
		this.bobsSample = bobsSample;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "YC_REASON_ID", length = 4000)
	public String getYcReasonId() {
		return ycReasonId;
	}

	public void setYcReasonId(String ycReasonId) {
		this.ycReasonId = ycReasonId;
	}

	@Column(name = "YC_REASON_NAME", length = 4000)
	public String getYcReasonName() {
		return ycReasonName;
	}

	public void setYcReasonName(String ycReasonName) {
		this.ycReasonName = ycReasonName;
	}
	
//	/**
//	 * 方法: 取得异常原因
//	 * 
//	 * @return: ycReason 异常原因
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "YC_REASON")
//	public DicType getYcReason() {
//		return ycReason;
//	}
//	
//	public void setYcReason(DicType ycReason) {
//		this.ycReason = ycReason;
//	}

	
}