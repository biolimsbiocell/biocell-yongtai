package com.biolims.experiment.bobs.sample.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: BoBs样本处理明细
 * @author lims-platform
 * @date 2016-06-03 15:33:04
 * @version V1.0
 * 
 */
@Entity
@Table(name = "BOBS_SAMPLE_ITEM")
@SuppressWarnings("serial")
public class BobsSampleItem extends EntityDao<BobsSampleItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 实验编号 */
	private String orderNumber;
	/** 核对样本编号 */
	private String checkCode;
	/** 样本类型 */
	private String sampleType;
	/** 原始样本编号 */
	private String sampleCode;
	/** 核酸浓度 */
	private Double concentration;
	/** 样本体积 */
	private Double volume;
	/** 稀释液体积 */
	private Double xsyVolume;
	/** 标记前浓度 */
	private Double bjqConcentration;
	/** 标记前调解体积 */
	private Double bjqVolume;
	/** 标记前调解稀释液 */
	private Double bjqDiluent;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 步骤编号 */
	private String stepNum;
	/** 关联任务单 */
	private String orderId;
	/** 状态 */
	private String state;
	/** 相关主表 */
	private BobsSample bobsSample;
	/** 中间产物数量 */
	private String productNum;
	/** 备注 */
	private String note;
    /** 临时表Id */
	private String tempId;
	/** 是否提交 */
	private String submit;

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验编号
	 */
	@Column(name = "ORDER_NUMBER", length = 50)
	public String getOrderNumber() {
		return this.orderNumber;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验编号
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对样本编号
	 */
	@Column(name = "CHECK_CODE", length = 50)
	public String getCheckCode() {
		return this.checkCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对样本编号
	 */
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 核酸浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 核酸浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 样本体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 样本体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 稀释液体积
	 */
	@Column(name = "XSY_VOLUME", length = 50)
	public Double getXsyVolume() {
		return this.xsyVolume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 稀释液体积
	 */
	public void setXsyVolume(Double xsyVolume) {
		this.xsyVolume = xsyVolume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 标记前浓度
	 */
	@Column(name = "BJQ_CONCENTRATION", length = 50)
	public Double getBjqConcentration() {
		return this.bjqConcentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 标记前浓度
	 */
	public void setBjqConcentration(Double bjqConcentration) {
		this.bjqConcentration = bjqConcentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 标记前调解体积
	 */
	@Column(name = "BJQ_VOLUME", length = 50)
	public Double getBjqVolume() {
		return this.bjqVolume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 标记前调解体积
	 */
	public void setBjqVolume(Double bjqVolume) {
		this.bjqVolume = bjqVolume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 标记前调解稀释液
	 */
	@Column(name = "BJQ_DILUENT", length = 50)
	public Double getBjqDiluent() {
		return this.bjqDiluent;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 标记前调解稀释液
	 */
	public void setBjqDiluent(Double bjqDiluent) {
		this.bjqDiluent = bjqDiluent;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 50)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 步骤编号
	 */
	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return this.stepNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 步骤编号
	 */
	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得BobsSample
	 * 
	 * @return: BobsSample 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BOBS_SAMPLE")
	public BobsSample getBobsSample() {
		return this.bobsSample;
	}

	/**
	 * 方法: 设置BobsSample
	 * 
	 * @param: BobsSample 相关主表
	 */
	public void setBobsSample(BobsSample bobsSample) {
		this.bobsSample = bobsSample;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 中间产物数量
	 */
	@Column(name = "PRODUCT_NUM", length = 50)
	public String getProductNum() {
		return this.productNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 中间产物数量
	 */
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}
}