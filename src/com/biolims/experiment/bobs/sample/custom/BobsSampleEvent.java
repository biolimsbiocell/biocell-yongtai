package com.biolims.experiment.bobs.sample.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.bobs.sample.service.BobsSampleService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class BobsSampleEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		BobsSampleService mbService = (BobsSampleService) ctx.getBean("bobsSampleService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
