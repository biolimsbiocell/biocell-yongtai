package com.biolims.experiment.bobs.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.bobs.sample.model.BobsSampleCos;
import com.biolims.experiment.bobs.sample.model.BobsSampleItem;
import com.biolims.experiment.bobs.sample.model.BobsSampleReagent;
import com.biolims.experiment.bobs.sample.model.BobsSampleResult;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemplate;
import com.biolims.experiment.bobs.sample.service.BobsSampleService;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/bobs/sample/bobsSample")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BobsSampleAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249201";
	@Autowired
	private BobsSampleService bobsSampleService;
	private BobsSample bobsSample = new BobsSample();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showBobsSampleList")
	public String showBobsSampleList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSample.jsp");
	}

	@Action(value = "showBobsSampleListJson")
	public void showBobsSampleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsSampleService.findBobsSampleList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsSample> list = (List<BobsSample>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
   
	@Action(value = "bobsSampleSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogBobsSampleList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleDialog.jsp");
	}
	@Action(value = "showDialogBobsSampleListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogBobsSampleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsSampleService
				.findBobsSampleListByState(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<BobsSample> list = (List<BobsSample>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editBobsSample")
	public String editBobsSample() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			bobsSample = bobsSampleService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "bobsSample");
		} else {
			bobsSample.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			bobsSample.setCreateUser(user);
			bobsSample.setCreateDate(new Date());
			bobsSample.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			bobsSample.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doBobsSample");
//			Template t=tlist.get(0);
//			bobsSample.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				bobsSample.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(bobsSample.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleEdit.jsp");
	}

	@Action(value = "copyBobsSample")
	public String copyBobsSample() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		bobsSample = bobsSampleService.get(id);
		bobsSample.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = bobsSample.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "BobsSample";
			String markCode = "BOBSCL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			bobsSample.setId(autoID);
			bobsSample.setName("Bobs样本预处理-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("bobsSampleItem",
				getParameterFromRequest("bobsSampleItemJson"));

		aMap.put("bobsSampleTemplate",
				getParameterFromRequest("bobsSampleTemplateJson"));

		aMap.put("bobsSampleReagent",
				getParameterFromRequest("bobsSampleReagentJson"));

		aMap.put("bobsSampleCos", getParameterFromRequest("bobsSampleCosJson"));

		aMap.put("bobsSampleResult",
				getParameterFromRequest("bobsSampleResultJson"));

		bobsSampleService.save(bobsSample, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/bobs/sample/bobsSample/editBobsSample.action?id="
				+ bobsSample.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/bobs/sample/bobsSample/editBobsSample.action?id="
		// + bobsSample.getId());

	}

	@Action(value = "viewBobsSample")
	public String toViewBobsSample() throws Exception {
		String id = getParameterFromRequest("id");
		bobsSample = bobsSampleService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleEdit.jsp");
	}

	@Action(value = "showBobsSampleItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSampleItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleItem.jsp");
	}

	@Action(value = "showBobsSampleItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSampleItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSampleService
					.findBobsSampleItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSampleItem> list = (List<BobsSampleItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleType", "");
			map.put("sampleCode", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("xsyVolume", "");
			map.put("bjqConcentration", "");
			map.put("bjqVolume", "");
			map.put("bjqDiluent", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("bobsSample-name", "");
			map.put("bobsSample-id", "");
			map.put("productNum", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("submit", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSampleItem")
	public void delBobsSampleItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSampleService.delBobsSampleItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSampleTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSampleTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleTemplate.jsp");
	}

	@Action(value = "showBobsSampleTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSampleTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSampleService
					.findBobsSampleTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsSampleTemplate> list = (List<BobsSampleTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("bobsSample-name", "");
			map.put("bobsSample-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSampleTemplate", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delBobsSampleTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSampleService.delBobsSampleTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSampleReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSampleReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleReagent.jsp");
	}

	@Action(value = "showBobsSampleReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSampleReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSampleService
					.findBobsSampleReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsSampleReagent> list = (List<BobsSampleReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("bobsSample-name", "");
			map.put("bobsSample-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSampleReagent", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delBobsSampleReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSampleService.delBobsSampleReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSampleCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSampleCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleCos.jsp");
	}

	@Action(value = "showBobsSampleCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSampleCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSampleService
					.findBobsSampleCosList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSampleCos> list = (List<BobsSampleCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("bobsSample-name", "");
			map.put("bobsSample-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSampleCos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delBobsSampleCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSampleService.delBobsSampleCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			String model = getParameterFromRequest("model");
			bobsSampleService.delOne(ids, model);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSampleResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSampleResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleResult.jsp");
	}

	@Action(value = "showBobsSampleResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSampleResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSampleService
					.findBobsSampleResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsSampleResult> list = (List<BobsSampleResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("concentration", "");
			map.put("od280", "");
			map.put("od230", "");
			map.put("volume", "");
			map.put("diluent", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("reason", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("bobsSample-name", "");
			map.put("bobsSample-id", "");
			map.put("note", "");
			map.put("tempId", "");

			map.put("ycReasonId", "");
			map.put("ycReasonName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSampleResult")
	public void delBobsSampleResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSampleService.delBobsSampleResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSampleTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSampleTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sample/bobsSampleTemp.jsp");
	}

	@Action(value = "showBobsSampleTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSampleTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsSampleService.findBobsSampleTempList(
				map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<BobsSampleTemp> list = (List<BobsSampleTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("orderId", "");
		map.put("state", "");
		map.put("note", "");
		map.put("chargeNote", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSampleTemp")
	public void delBobsSampleTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSampleService.delBobsSampleTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BobsSampleService getBobsSampleService() {
		return bobsSampleService;
	}

	public void setBobsSampleService(BobsSampleService bobsSampleService) {
		this.bobsSampleService = bobsSampleService;
	}

	public BobsSample getBobsSample() {
		return bobsSample;
	}

	public void setBobsSample(BobsSample bobsSample) {
		this.bobsSample = bobsSample;
	}

	/**
	 * 根据主表id加载结果子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "setResultToCross", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setResultToCross() throws Exception {
		String id = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.bobsSampleService
					.getBobsSampleItemById(id);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id =getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.bobsSampleService.submitSample(id,ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 生成Excel
	 * 
	 * @throws Exception
	 */
	
	 @Action(value = "createExcel") 
	 public void createExcel() throws Exception{ 
		 String[] ids = getRequest().getParameterValues("ids[]");
		 Map<String,Boolean> map = new HashMap<String, Boolean>(); 
		 try { 
			 bobsSampleService.createBobsSampleItemCSV(ids); 
			 map.put("success", true);
		 } catch (Exception e) { 
			 e.printStackTrace(); 
		 	 map.put("success", false);
		 }
		 HttpUtils.write(JsonUtils.toJsonString(map));
	 }
		/**
		 * 判断状态，是否已提交
		 * 
		 * @throws Exception
		 */
		 @Action(value = "remindSubmit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		 public void remindSubmit() throws Exception {
		 		Map<String, String> map = new HashMap<String, String >();
		 		try {
		 			String id = getParameterFromRequest("code");
		 			String state = bobsSampleService.selectRemindSubmit(id);
		 			map.put("state", state);
		 		} catch (Exception e) {
		 			e.printStackTrace();
		 		}
		 		HttpUtils.write(JsonUtils.toJsonString(map));
		 }
		 
		 /**
			 * 保存杂交洗脱样本明细
			 * @throws Exception
			 */
			@Action(value = "saveBobsSampleItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
			public void saveBobsSampleItem() throws Exception {
				String id = getRequest().getParameter("id");
				String itemDataJson = getRequest().getParameter("itemDataJson");
				Map<String, Object> result = new HashMap<String, Object>();
				try {
					BobsSample sc=commonDAO.get(BobsSample.class, id);
					Map aMap = new HashMap();
					aMap.put("bobsSampleItem",
							itemDataJson);
					if(sc!=null){
						bobsSampleService.save(sc,aMap);
					}
					result.put("success", true);
				} catch (Exception e) {
					e.printStackTrace();
					result.put("success", false);
				}
				HttpUtils.write(JsonUtils.toJsonString(result));
			}
			
			 /**
			 * 保存杂交洗脱结果
			 * @throws Exception
			 */
			@Action(value = "saveBobsSampleResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
			public void saveBobsSampleResult() throws Exception {
				String id = getRequest().getParameter("id");
				String itemDataJson = getRequest().getParameter("itemDataJson");
				Map<String, Object> result = new HashMap<String, Object>();
				try {
					BobsSample sc=commonDAO.get(BobsSample.class, id);
					Map aMap = new HashMap();
					aMap.put("bobsSampleResult",
							itemDataJson);
					if(sc!=null){
						bobsSampleService.save(sc,aMap);
					}
					result.put("success", true);
				} catch (Exception e) {
					e.printStackTrace();
					result.put("success", false);
				}
				HttpUtils.write(JsonUtils.toJsonString(result));
			}
	
}
