package com.biolims.experiment.bobs.sample.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.abnormal.model.BobsAbnormal;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemp;
import com.biolims.experiment.bobs.sample.dao.BobsSampleDao;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.bobs.sample.model.BobsSampleCos;
import com.biolims.experiment.bobs.sample.model.BobsSampleItem;
import com.biolims.experiment.bobs.sample.model.BobsSampleReagent;
import com.biolims.experiment.bobs.sample.model.BobsSampleResult;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemplate;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BobsSampleService {
	@Resource
	private BobsSampleDao bobsSampleDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleStateService sampleStateService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findBobsSampleList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsSampleDao.selectBobsSampleList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findBobsSampleListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsSampleDao.selectBobsSampleListByState(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsSample i) throws Exception {

		bobsSampleDao.saveOrUpdate(i);

	}

	public BobsSample get(String id) {
		BobsSample bobsSample = commonDAO.get(BobsSample.class, id);
		return bobsSample;
	}

	public Map<String, Object> findBobsSampleItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSampleDao.selectBobsSampleItemList(
				scId, startNum, limitNum, dir, sort);
		List<BobsSampleItem> list = (List<BobsSampleItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSampleTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSampleDao
				.selectBobsSampleTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<BobsSampleTemplate> list = (List<BobsSampleTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBobsSampleReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSampleDao.selectBobsSampleReagentList(
				scId, startNum, limitNum, dir, sort);
		List<BobsSampleReagent> list = (List<BobsSampleReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBobsSampleCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSampleDao.selectBobsSampleCosList(
				scId, startNum, limitNum, dir, sort);
		List<BobsSampleCos> list = (List<BobsSampleCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSampleResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSampleDao.selectBobsSampleResultList(
				scId, startNum, limitNum, dir, sort);
		List<BobsSampleResult> list = (List<BobsSampleResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBobsSampleTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = bobsSampleDao.selectBobsSampleTempList(
				mapForQuery, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsSampleTemp> list = (List<BobsSampleTemp>) result.get("list");

		Map<String, Object> result2 = new HashMap<String, Object>();
		List<BobsSampleTemp> list2 = new ArrayList<BobsSampleTemp>();
		for (BobsSampleTemp t : list) {
			SampleInfo s = this.sampleInfoMainDao.findSampleInfo(t
					.getSampleCode());
			t.setChargeNote(s.getSampleOrder().getChargeNote());
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list);
		return result2;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSampleItem(BobsSample sc, String itemDataJson)
			throws Exception {
		List<BobsSampleItem> saveItems = new ArrayList<BobsSampleItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSampleItem scp = new BobsSampleItem();
			// 将map信息读入实体类
			scp = (BobsSampleItem) bobsSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSample(sc);

			saveItems.add(scp);
		}
		bobsSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSampleItem(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSampleItem scp = bobsSampleDao.get(BobsSampleItem.class, id);
			bobsSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSampleTemplate(BobsSample sc, String itemDataJson)
			throws Exception {
		List<BobsSampleTemplate> saveItems = new ArrayList<BobsSampleTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSampleTemplate scp = new BobsSampleTemplate();
			// 将map信息读入实体类
			scp = (BobsSampleTemplate) bobsSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSample(sc);

			saveItems.add(scp);
		}
		bobsSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSampleTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSampleTemplate scp = bobsSampleDao.get(
					BobsSampleTemplate.class, id);
			bobsSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSampleReagent(BobsSample sc, String itemDataJson)
			throws Exception {
		List<BobsSampleReagent> saveItems = new ArrayList<BobsSampleReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSampleReagent scp = new BobsSampleReagent();
			// 将map信息读入实体类
			scp = (BobsSampleReagent) bobsSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSample(sc);

			saveItems.add(scp);
		}
		bobsSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSampleReagent(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSampleReagent scp = bobsSampleDao.get(BobsSampleReagent.class,
					id);
			bobsSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSampleCos(BobsSample sc, String itemDataJson)
			throws Exception {
		List<BobsSampleCos> saveItems = new ArrayList<BobsSampleCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSampleCos scp = new BobsSampleCos();
			// 将map信息读入实体类
			scp = (BobsSampleCos) bobsSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSample(sc);

			saveItems.add(scp);
		}
		bobsSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSampleCos(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSampleCos scp = bobsSampleDao.get(BobsSampleCos.class, id);
			bobsSampleDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOne(String ids, String model) throws Exception {
		if (model.equals("1")) {
			BobsSampleTemplate scp = bobsSampleDao.get(
					BobsSampleTemplate.class, ids);
			if (scp != null) {
				bobsSampleDao.delete(scp);
			}
		}
		if (model.equals("2")) {
			BobsSampleReagent scp2 = bobsSampleDao.get(BobsSampleReagent.class,
					ids);
			if (scp2 != null) {
				bobsSampleDao.delete(scp2);
			}
		}

		if (model.equals("3")) {
			BobsSampleCos scp3 = bobsSampleDao.get(BobsSampleCos.class, ids);
			if (scp3 != null) {
				bobsSampleDao.delete(scp3);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSampleResult(BobsSample sc, String itemDataJson)
			throws Exception {
		List<BobsSampleResult> saveItems = new ArrayList<BobsSampleResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSampleResult scp = new BobsSampleResult();
			// 将map信息读入实体类
			scp = (BobsSampleResult) bobsSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSample(sc);

			saveItems.add(scp);
			/* if (scp != null){
			 if (scp.getResult() != null && scp.getSubmit() != null &&
			 !scp.getResult().equals("") && !scp.getSubmit().equals("") ){
			 if(scp.getSubmit().equals("1")){
			 if(scp.getResult().equals("0")){
			 BobsAbnormal ka=new BobsAbnormal();
			 ka.setCode(scp.getCode());
			 ka.setSampleCode(scp.getSampleCode());
			 ka.setProductId(scp.getProductId());
			 ka.setProductName(scp.getProductName());
			 ka.setSampleType(scp.getSampleType());
			 ka.setTaskId(scp.getBobsSample().getId());
			 ka.setNote(scp.getNote());
			 ka.setTaskName("BoBs样本处理");
			 ka.setState("1");
			
			 commonDAO.saveOrUpdate(ka);
			 }
			 }
			 }
			 }*/
			/*
			 * commonDAO.saveOrUpdate(scp); DateFormat format = new
			 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); if (scp != null) { if
			 * (scp.getResult()!= null && scp.getSubmit()!= null &&
			 * scp.getNextFlowId()!=null && !scp.getResult().equals("") &&
			 * !scp.getSubmit().equals("") && !scp.getNextFlowId().equals("")){
			 * if(scp.getSubmit().equals("1")){ if(scp.getResult().equals("1")){
			 * if (scp.getNextFlowId().equals("0009")) {// 样本入库 SampleInItemTemp
			 * st = new SampleInItemTemp(); st.setCode(scp.getCode());
			 * st.setSampleCode(scp.getSampleCode()); st.setState("1");
			 * commonDAO.saveOrUpdate(st); // 入库，改变SampleInfo中原始样本的状态为“待入库”
			 * SampleInfo sf = sampleInfoMainDao .findSampleInfo(scp.getCode());
			 * if (sf != null) { sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
			 * sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME); } } else if
			 * (scp.getNextFlowId().equals("0012")) {// 暂停 //
			 * 暂停，改变SampleInfo中原始样本的状态为“实验暂停” SampleInfo sf = sampleInfoMainDao
			 * .findSampleInfo(scp.getCode()); if (sf != null) {
			 * sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
			 * sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME); } } else
			 * if (scp.getNextFlowId().equals("0013")) {// 终止 //
			 * 终止，改变SampleInfo中原始样本的状态为“实验终止” SampleInfo sf = sampleInfoMainDao
			 * .findSampleInfo(scp.getCode()); if (sf != null) {
			 * sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
			 * sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME); } }else {
			 * // 得到下一步流向的相关表单 List<NextFlow> list_nextFlow = nextFlowDao
			 * .seletNextFlowById(scp.getNextFlowId()); for (NextFlow n :
			 * list_nextFlow) { Object o = Class.forName(
			 * n.getApplicationTypeTable() .getClassPath()) .newInstance();
			 * scp.setState("1"); sampleInputService.copy(o, scp); } } }
			 * sampleStateService .saveSampleState( scp.getCode(),
			 * scp.getSampleCode(), scp.getProductId(), scp.getProductName(),
			 * "", format.format(sc.getCreateDate()), format.format(new Date()),
			 * "BobsSample", "BoBs样本处理", (User) ServletActionContext
			 * .getRequest() .getSession() .getAttribute(
			 * SystemConstants.USER_SESSION_KEY), sc.getId(), scp.getNextFlow(),
			 * scp.getResult(), null, null, null, null, null, null, null, null);
			 * } } }
			 */
		}
		bobsSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSampleResult(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSampleResult scp = bobsSampleDao
					.get(BobsSampleResult.class, id);
			bobsSampleDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSampleTemp(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSampleTemp scp = bobsSampleDao.get(BobsSampleTemp.class, id);
			bobsSampleDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsSample sc, Map jsonMap) throws Exception {
		if (sc != null) {
			bobsSampleDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("bobsSampleItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSampleItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSampleTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSampleTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSampleReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSampleReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSampleCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSampleCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSampleResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSampleResult(sc, jsonStr);
			}
			List<BobsSampleItem> snlist = bobsSampleDao.getItem(sc.getId());
			List<BobsSampleReagent> rglist = bobsSampleDao.getReagent(sc.getId());
			 DecimalFormat df = new DecimalFormat("#.00");
			 Double nums = 0.00;
			if (snlist.size() > 0) {
				if (rglist.size() > 0) {
					for (BobsSampleReagent sr : rglist) {
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if (sr.getOneNum() != null && sr.getSampleNum() != null) {
							nums =Double.valueOf(df.format(sr.getOneNum() * snlist.size()));
							sr.setNum(nums);
						}
					}
				}
			}
		}
	}

	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		BobsSample kt = bobsSampleDao.get(BobsSample.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		bobsSampleDao.update(kt);
		// 完成后改变左侧表的状态
		List<BobsSampleResult> list = bobsSampleDao.getBobsSampleResultList(kt
				.getId());
		if (list.size() > 0) {
			for (BobsSampleResult scp : list) {
				BobsSampleTemp tt = commonDAO.get(BobsSampleTemp.class,
						scp.getTempId());
				if (tt != null) {
					tt.setState("2");
				}
				if (scp != null) {
					if (scp.getResult() != null) {
						String isOk = scp.getResult();
						if (isOk.equals("1")) {
							BobsCrossTemp k = new BobsCrossTemp();
							k.setCode(scp.getCode());
							k.setSampleCode(scp.getSampleCode());
							k.setProductId(scp.getProductId());
							k.setProductName(scp.getProductName());
							k.setSampleType(scp.getSampleType());
							k.setState("1");
							bobsSampleDao.saveOrUpdate(k);
							DateFormat format = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							sampleStateService
									.saveSampleState(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											DateUtil.format(kt.getCreateDate()),
											format.format(new Date()),
											"BobsSample",
											"BoBs样本处理",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											kt.getId(), "BoBs杂交",
											scp.getResult(), null, null, null,
											null, null, null, null, null);
							scp.setSubmit("1");
							bobsSampleDao.saveOrUpdate(scp);
						} else {

							BobsAbnormal ka = new BobsAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getBobsSample().getId());
							ka.setNote(scp.getNote());
							ka.setNextFlow("BoBs样本处理");
							ka.setNextFlowId("0032");
							ka.setTaskName("BoBs样本处理");
							ka.setState("1");
							bobsSampleDao.saveOrUpdate(ka);
						}
					}
				}
			}
		}
	}

	/**
	 * 根据主表ID获取结果表信息
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> getBobsSampleItemById(String id)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = bobsSampleDao.getBobsSampleResultById(id);
		List<BobsSampleResult> list = (List<BobsSampleResult>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (BobsSampleResult pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("checkCode", pt.getCheckCode());
				map.put("orderNumber", pt.getOrderNumber());
				map.put("sampleType", pt.getSampleType());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("note", pt.getNote());
				map.put("result", pt.getResult());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// 提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		BobsSample sc = this.bobsSampleDao.get(BobsSample.class, id);
		// 获取结果表样本信息

		List<BobsSampleResult> list;
		if (ids == null)
			list = this.bobsSampleDao.setResultById(id);
		else
			list = this.bobsSampleDao.setResultByIds(ids);
		for (BobsSampleResult scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && scp.getSubmit() == null) {
					String isOk = scp.getResult();
					//BoBs杂交
					if (isOk.equals("1")) {
						BobsCrossTemp k = new BobsCrossTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
						k.setState("1");
						bobsSampleDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(sc.getCreateDate()),
										format.format(new Date()),
										"BobsSample",
										"BoBs样本处理",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "BoBs杂交", scp.getResult(),
										null, null, null, null, null, null,
										null, null);
						scp.setSubmit("1");
						bobsSampleDao.saveOrUpdate(scp);
					} else {
						//终止报告
						if(scp.getNextFlowId().equals("0038")){
						EndReport cr = new EndReport();
						cr.setCode(scp.getCode());
						cr.setSampleCode(scp.getSampleCode());
						cr.setProductId(scp.getProductId());
						cr.setProductName(scp.getProductName());
						cr.setAdvice(scp.getNote());
						cr.setSampleType(scp.getSampleType());
						cr.setTaskId(scp.getId());
						cr.setNote(scp.getNote());
					    cr.setTaskResultId(scp.getBobsSample().getId());
						cr.setTaskType("BoBs样本处理");
						cr.setWaitDate(new Date());
						commonDAO.saveOrUpdate(cr);
						}else{
						 //样本处理异常
							BobsAbnormal ka = new BobsAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getBobsSample().getId());
							ka.setNote(scp.getNote());
							ka.setNextFlow("BoBs样本处理");
							ka.setNextFlowId("0032");
							ka.setTaskName("BoBs杂交");
							ka.setState("1");
							bobsSampleDao.saveOrUpdate(ka);
						}
						scp.setSubmit("1");
					}
				}
			}
		}
	}
	
	/**
	 * 生成Excel文件
	 * 
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createBobsSampleItemCSV(String[] ids) throws Exception {
		String a = "/BoBs实验导出列表/BoBs样本处理明细";
		String filePath = SystemConstants.WRITE_QC_PATH ;// 写入csv路径,以fc号为准
		// 往qcdata中写入两个csv文件
		String markCode = DateUtil.dateFormatterByPattern(
					new Date(), "yyMMddHHmmss");
		String fileName = filePath + a +"/"+markCode+"BoBs样本处理明细.csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = {"code","sampleCode","checkCode",
		"orderNumber","sampleType","concentration","volume","xsyVolume",
		"bjqConcentration","bjqVolume","bjqDiluent","productName","note"};
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",")
					.toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		for(int i=0;i<ids.length;i++){
			BobsSampleItem sr=bobsSampleDao.geBobsSampleItemByIds(ids[i]);
			StringBuffer sb = new StringBuffer();
			setBobsSampleItemData(sr,sb);
			String rowStr = sb.toString();
			csvWtriter.write(rowStr);
			csvWtriter.newLine();
			sr.setState("1");
			bobsSampleDao.saveOrUpdate(sr);
		}
		csvWtriter.flush();
		csvWtriter.close();
		
	}
	/**
	 * 生成BoBs样本处理明细CSV文件
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setBobsSampleItemData(BobsSampleItem s,StringBuffer sb) throws Exception {
		sb.append("\"").append(s.getCode()).append("\",");
		sb.append("\"").append(s.getSampleCode()).append("\",");
		sb.append("\"").append(s.getCheckCode()).append("\",");
		sb.append("\"").append(s.getOrderNumber()).append("\",");
		sb.append("\"").append(s.getSampleType()).append("\",");
		sb.append("\"").append(s.getConcentration()).append("\",");
		sb.append("\"").append(s.getBjqVolume()).append("\",");
		sb.append("\"").append(s.getBjqVolume()).append("\",");
		sb.append("\"").append(s.getBjqConcentration()).append("\",");
		sb.append("\"").append(s.getBjqVolume()).append("\",");
		sb.append("\"").append(s.getBjqDiluent()).append("\",");
		sb.append("\"").append(s.getProductName()).append("\",");
		sb.append("\"").append(s.getNote()).append("\",");
	}

	/**
	 * 判断状态，是否已提交
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String selectRemindSubmit(String id) throws Exception {
		BobsSample bs = commonDAO.get(BobsSample.class,
				id);
		if(bs != null &&( bs.getState() != null || !"".equals(bs.getState()))){
			String state = bs.getState();
			return state;
		}
		return "-1";
	}
	
	
}
