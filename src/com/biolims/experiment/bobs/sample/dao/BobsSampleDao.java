package com.biolims.experiment.bobs.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.cross.model.BobsCrossItem;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.bobs.sample.model.BobsSampleCos;
import com.biolims.experiment.bobs.sample.model.BobsSampleItem;
import com.biolims.experiment.bobs.sample.model.BobsSampleReagent;
import com.biolims.experiment.bobs.sample.model.BobsSampleResult;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemplate;
import com.biolims.experiment.bobs.sj.model.BobsSjResult;
import com.biolims.experiment.fish.model.FishCrossTaskResult;
import com.biolims.experiment.fish.model.FishProTaskItem;
import com.biolims.experiment.fish.model.FishProTaskReagent;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;

@Repository
@SuppressWarnings("unchecked")
public class BobsSampleDao extends BaseHibernateDao {
	public Map<String, Object> selectBobsSampleList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsSample where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsSample> list = new ArrayList<BobsSample>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public Map<String, Object> selectBobsSampleListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsSample where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsSample> list = new ArrayList<BobsSample>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public Map<String, Object> selectBobsSampleItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSampleItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSampleItem> list = new ArrayList<BobsSampleItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSampleTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSampleTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSampleTemplate> list = new ArrayList<BobsSampleTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSampleReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSampleReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSampleReagent> list = new ArrayList<BobsSampleReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSampleCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSampleCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSampleCos> list = new ArrayList<BobsSampleCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSampleResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSampleResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSampleResult> list = new ArrayList<BobsSampleResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSampleTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsSampleTemp where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key =" and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsSampleTemp> list = new ArrayList<BobsSampleTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * 根据主表ID查询结果表
	 */
	public List<BobsSampleResult> getBobsSampleResultList(String id){
		String hql = " from BobsSampleResult where 1=1  and bobsSample='"+id+"'";
		List<BobsSampleResult> list=this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * ajax根据主表id查询子表信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	
	
	//String code=id.substring(id.length()-1);
	public Map<String,Object> getBobsSampleResultById(String id) throws Exception{
		String hql="from BobsSampleResult t where 1=1";
		String key = "";
		if (id != null)
			key = key + " and  t.bobsSample in (" + id + ")";
		List<BobsSampleResult> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}
	
	
//	public Map<String,Object> getBobsSampleResultById(String id) throws Exception{
//		String hql="from BobsSampleResult t where 1=1";
//		String key = "";
//		if (id != null)
//			key = key + " and  t.bobsSample='" + id + "'";
//		List<BobsSampleResult> list = this.getSession().createQuery(hql + key).list();
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("list", list);
//		return result;
//	}
	
	
	// 根据样本编号查询
		public List<BobsSampleResult> setResultById(String code) {
			String hql = "from BobsSampleResult t where (submit is null or submit='') and bobsSample.id='"
					+ code + "'";
			List<BobsSampleResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

		// 根据样本编号查询
		public List<BobsSampleResult> setResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}

			String hql = "from BobsSampleResult t where (submit is null or submit='') and id in ("
					+ insql + ")";
			List<BobsSampleResult> list = this.getSession().createQuery(hql).list();
			return list;
		}
	/**
	 * 根据样本编号查询临时表样本信息
	 * @param code
	 * @return
	 */
	public List<BobsSampleTemp> getTempByCode(String code) {
		String hql = "from BobsSampleTemp t where 1=1  and state='2' and code='"+ code + "'";
		List<BobsSampleTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<BobsSampleItem> getItem(String id) {
		String hql = "from BobsSampleItem t where 1=1  and bobsSample='"+ id + "'";
		List<BobsSampleItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public BobsSampleItem geBobsSampleItemByIds(String id) {
		String hql = "from BobsSampleItem where 1=1 and id='"
				+ id + "'";
		BobsSampleItem s = (BobsSampleItem) this.getSession()
				.createQuery(hql).uniqueResult();
		return s;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<BobsSampleReagent> getReagent(String id) {
		String hql = "from BobsSampleReagent t where 1=1  and bobsSample='"+ id + "'";
		List<BobsSampleReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
}