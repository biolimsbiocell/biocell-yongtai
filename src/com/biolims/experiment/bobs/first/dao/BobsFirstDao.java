package com.biolims.experiment.bobs.first.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.analysis.model.BobsAgainInstance;
import com.biolims.experiment.bobs.first.model.BobsFirstInstance;

@Repository
@SuppressWarnings("unchecked")
public class BobsFirstDao extends BaseHibernateDao {
	public Map<String, Object> selectBobsFirstInstanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsFirstInstance where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsFirstInstance> list = new ArrayList<BobsFirstInstance>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<BobsFirstInstance> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from BobsFirstInstance t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<BobsFirstInstance> list = this.getSession().createQuery(hql).list();
		return list;
	}
}