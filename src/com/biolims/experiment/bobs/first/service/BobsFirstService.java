package com.biolims.experiment.bobs.first.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.analysis.model.BobsAgainInstance;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysis;
import com.biolims.experiment.bobs.first.dao.BobsFirstDao;
import com.biolims.experiment.bobs.first.model.BobsFirstInstance;
import com.biolims.experiment.bobs.second.model.BobsSecondInstance;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BobsFirstService {
	@Resource
	private BobsFirstDao bobsFirstDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private ComSearchService comSearchService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findBobsFirstInstanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsFirstDao.selectBobsFirstInstanceList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsFirstInstance i) throws Exception {

		bobsFirstDao.saveOrUpdate(i);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsFirstInstance(String itemDataJson) throws Exception {
		List<BobsFirstInstance> saveItems = new ArrayList<BobsFirstInstance>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsFirstInstance scp = new BobsFirstInstance();
			// 将map信息读入实体类
			scp = (BobsFirstInstance) bobsFirstDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			bobsFirstDao.saveOrUpdate(scp);
			// 获取相关图片信息
			List<FileInfo> picList = comSearchDao.findPicture(
					scp.getTaskResultId(), "bobsAnalysisResult");
			// 根据检测项目查询报告模板
			if (picList.size() > 0) {
				List<ReportTemplateInfo> rlist = comSearchDao
						.getReportTemplateInfo1(scp.getProductId());
				ReportTemplateInfo rt = rlist.get(0);
				scp.setReportInfo(rt);
				scp.setTemplate(rt.getAttach());
			} else {
				scp.setReportInfo(null);
				scp.setTemplate(null);
			}
			if (scp != null) {
				if (scp.getSubmit() != null
				// && scp.getResult() != null
				// && !scp.getResult().equals("")
						&& !scp.getSubmit().equals("")) {
					if (scp.getSubmit().equals("1")) {
						// if (scp.getResult().equals("1")) {
						// 提交合格的到二审
						BobsSecondInstance sf = new BobsSecondInstance();
						sf.setCode(scp.getCode());
						sf.setSampleCode(scp.getSampleCode());
						sf.setChipNum(scp.getChipNum());
						sf.setJg(scp.getJg());
						sf.setJgjs(scp.getJgjs());
						sf.setLcjy(scp.getLcjy());
						sf.setProductId(scp.getProductId());
						sf.setProductName(scp.getProductName());
						sf.setSampleType(scp.getSampleType());
						if (scp.getTemplate() != null) {
							sf.setTemplate(scp.getTemplate());
						}
						if (scp.getReportInfo() != null) {
							sf.setReportInfo(scp.getReportInfo());
						}
						sf.setSampleType(scp.getSampleType());
						sf.setTaskId(scp.getTaskId());
						sf.setTaskResultId(scp.getTaskResultId());
						sf.setTaskType(scp.getTaskType());
						sf.setState("1");
						sf.setFormerDate(new Date());
						commonDAO.saveOrUpdate(sf);

						// 完成后改变一审的状态
						scp.setState("2");

						sampleStateService
								.saveSampleState1(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										format.format(new Date()),
										format.format(new Date()),
										"BobsFirstInstance",
										"BoBs数据审核核",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										scp.getId(), "BoBs报告审核", "1", null,
										null, null, null, null, null, null,
										null, scp.getJg(), scp.getJgjs(),
										scp.getLcjy(), scp.getYcbg());
					}
				}
			}
		}
		bobsFirstDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsFirstInstance(String[] ids) throws Exception {
		for (String id : ids) {
			BobsFirstInstance scp = bobsFirstDao.get(BobsFirstInstance.class,
					id);
			bobsFirstDao.delete(scp);
		}
	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFilePDF(String[] ids) throws Exception {
		// @Resource
		// private ComSearchDao comSearchDao;
		// 临时地址
		String tempFile = ConfigFileUtil.getValueByKey("file.report.temp.path");
		// 正式地址
		String formFile = ConfigFileUtil.getValueByKey("file.report.form.path");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		for (String id : ids) {
			BobsFirstInstance sri = commonDAO.get(BobsFirstInstance.class, id);
			// 获取相关图片信息
			List<FileInfo> picList = comSearchDao.findPicture(
					sri.getTaskResultId(), "bobsAnalysisResult");

			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
					.getSampleCode());
			/*
			 * SampleOrder so = new SampleOrder(); if (sf != null) { if
			 * (sf.getSampleOrder() != null) { so = sf.getSampleOrder(); } }
			 */
			SampleOrder so = commonDAO.get(SampleOrder.class,
					sri.getSampleCode());
			FileInfo fileInfo = sri.getTemplate();
			if (fileInfo != null) {
				FileInputStream in = new FileInputStream(fileInfo.getFilePath());
				PdfReader reader = new PdfReader(in);
				String root = ConfigFileUtil.getRootPath() + File.separator
						+ DateUtil.format(new Date(), "yyyyMMdd");
				if (!new File(root).exists()) {
					new File(root).mkdirs();
				}
				File deskFile = new File(tempFile, "PDF" + sri.getCode()
						+ ".pdf");
				PdfStamper ps = new PdfStamper(reader, new FileOutputStream(
						deskFile)); // 生成的输出流

				// 获取订单信息
				AcroFields s = ps.getAcroFields();
				if (so.getName() != null) {
					String names = so.getName();
					s.setField("names", names);
				} else {
					s.setField("names", "");
				}
				System.out.println("姓名" + so.getName());
				if (so.getGender() != null) {
					String m = "男";
					String w = "女";
					if (so.getGender().equals("1")) {
						s.setField("genders", m);
					} else {
						s.setField("genders", w);
					}
				} else {
					s.setField("genders", "");
				}
				System.out.println("性别" + so.getGender());
				if (so.getAge() != null) {
					s.setField("age", so.getAge().toString());
				} else {
					s.setField("age", "");
				}
				if (so.getBirthDate() != null) {
					s.setField("birthDay", format.format(so.getBirthDate()));
				} else {
					s.setField("birthDay", "");
				}
				if (sri.getSampleCode() != null) {
					s.setField("slideCode", sri.getSampleCode());
				} else {
					s.setField("slideCode", "");
				}
				s.setField("medicalNum", so.getMedicalNumber());
				if (sri.getSampleType() != null) {
					String t = sri.getSampleType();
					s.setField("sampleTypes", t);
				} else {
					s.setField("sampleTypes", "");
				}

				System.out.println("样本类型" + sri.getSampleType());
				if (so.getCrmCustomer() != null) {
					s.setField("crmCustomer", so.getCrmCustomer().getName());
				} else {
					s.setField("crmCustomer", "");
				}
				if (so.getSamplingDate() != null) {
					s.setField("samplingDate", format.format(so.getSamplingDate()));
				} else {
					s.setField("samplingDate", "");
				}
				String jg = sri.getJg();
				jg = jg.replace("\r", "");
				jg = jg.replace("\n", "");
				int num0 = jg.getBytes("UTF-8").length; // 字节长度
				String jgjs = sri.getJgjs(); // 结果解释
				jgjs = jgjs.replace("\r", "");
				jgjs = jgjs.replace("\n", "");
				int num1 = jgjs.getBytes("UTF-8").length; // 字节长度
				int n0 = 0;
				int n1 = 0;
				String jg1 = "";
				String jg2 = "";
				String jg3 = "";

				String jgjs1 = "";
				String jgjs2 = "";
				String jgjs3 = "";
				// 结果
				if (num0 > 0) {
					if (num0 <= 88) {
						jg1 = jg;
					} else {
						for (int i = 1; i <= (num0 / 88); i++) {
							jg2 = jg.replace(jg3, "");
							n0 = jg2.getBytes("UTF-8").length;
							if (n0 >= 88) {
								jg1 += comSearchService.bSubstring(jg2, 88)
										+ "\r";
								jg3 += comSearchService.bSubstring(jg2, 88);
							} else if (n0 < 90 && n0 > 0) {
								jg1 += comSearchService.bSubstring(jg2, 88)
										+ "\r";
								jg3 += comSearchService.bSubstring(jg2, 88);
							}
						}
					}
				}
				// jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
				System.out.println(jg1 + jg.replace(jg3, ""));
				if (!jg.replace(jg3, "").equals("")) {
					if (sri.getJg() != null) {
						s.setField("result", jg1 + jg.replace(jg3, ""));
					} else {
						s.setField("result", "");
					}
				} else {
					if (sri.getJg() != null) {
						s.setField("result", jg1);
					} else {
						s.setField("result", "");
					}
				}

				// 结果解释
				if (num1 > 0) {
					if (num1 <= 84) {
						jgjs1 = jgjs;
					} else {
						for (int i = 0; i <= (num1 / 84); i++) {
							jgjs2 = jgjs.replace(jgjs3, "");
							n1 = jgjs2.getBytes("UTF-8").length;
							if (n1 >= 84) {
								jgjs1 += comSearchService.bSubstring(jgjs2, 84)
										+ "\r";
								jgjs3 += comSearchService.bSubstring(jgjs2, 84);
							} else if (n1 < 84 && n1 > 0) {
								jgjs1 += comSearchService.bSubstring(jgjs2, n1)
										+ "\r";
								jgjs3 += comSearchService.bSubstring(jgjs2, n1);
							}
						}
					}
				}
				System.out.println(jgjs1 + jgjs.replace(jgjs3, ""));
				if (!jgjs.replace(jgjs3, "").equals("")) {
					// jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
					if (sri.getJgjs() != null) {
						s.setField("resultDescription",
								jgjs1 + jgjs.replace(jgjs3, ""));
					} else {
						s.setField("resultDescription", "");
					}
				} else {
					if (sri.getJgjs() != null) {
						s.setField("resultDescription", jgjs1);
					} else {
						s.setField("resultDescription", "");
					}
				}
				if (sri.getLcjy() != null) {
					if (sri.getLcjy().equals("1")) {
						s.setField("advice", "门诊随访");
					} else {
						s.setField("advice", "遗传门诊随访");
					}
				} else {
					s.setField("advice", "");
				}

				// 临床症状描述
				if (so.getSubmitReasonName() != null) {
					s.setField("lczz", so.getSubmitReasonName());
				} else {
					s.setField("lczz", "");
				}
				s.setField("reportDate", format.format(new Date()));

				/*
				 * BobsAnalysis bs=commonDAO.get(BobsAnalysis.class,
				 * sri.getTaskId()); s.setField("checkUser", bs
				 * .getAcceptUser().getName()); s.setField("confirmUser", bs
				 * .getCreateUser().getName());
				 */
				if (picList.size() > 0) {
					// if(sri.getProductId().equals("A0009")){
					// insertImage1(ps,s,picList.get(0).getFilePath(),"Text1");
					// }else{
					insertImage(ps, s, picList.get(0).getFilePath(), "Text1");
					// }
				}

				ps.setFormFlattening(true);// 这句不能少
				ps.close();
				reader.close();
				in.close();
				commonDAO.update(sri);
			}
		}
	}

	/**
	 * 向PDF插入图片
	 * 
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	public static void insertImage(PdfStamper ps, AcroFields s, String path,
			String text) {

		try {
			// Image image = Image.getInstance(path);
			// List<AcroFields.FieldPosition> list = s.getFieldPositions(text);
			// Rectangle signRect = list.get(0).position;
			// PdfContentByte under = ps.getOverContent(1);
			// float x = signRect.getLeft();
			// float y = signRect.getBottom();
			// System.out.println(x);
			// System.out.println(y);
			// image.setAbsolutePosition(x, y);
			// image.scaleToFit(290, 412);
			//
			// under.addImage(image);
			int pageNo = s.getFieldPositions(text).get(0).page;
			Rectangle signRect = s.getFieldPositions(text).get(0).position;
			float x = signRect.getLeft();
			float y = signRect.getBottom();

			// 读图片
			Image image = Image.getInstance(path);
			// 获取操作的页面
			PdfContentByte under = ps.getOverContent(pageNo);
			// 根据域的大小缩放图片
			image.scaleToFit(signRect.getWidth(), signRect.getHeight());
			System.out.println(x);
			System.out.println(y);
			// 添加图片
			image.setAbsolutePosition(x, y);
			under.addImage(image);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 向PDF插入图片
	 * 
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	public static void insertImage1(PdfStamper ps, AcroFields s, String path,
			String text) {

		try {
			Image image = Image.getInstance(path);
			List<AcroFields.FieldPosition> list = s.getFieldPositions(text);
			Rectangle signRect = list.get(0).position;
			PdfContentByte under = ps.getOverContent(1);
			float x = signRect.getLeft();
			float y = signRect.getBottom();
			System.out.println(x);
			System.out.println(y);
			image.setAbsolutePosition(x, y);
			image.scaleToFit(380, 600);

			under.addImage(image);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String[] ids) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<BobsFirstInstance> list = this.bobsFirstDao.setResultByIds(ids);
		for (BobsFirstInstance scp : list) {
			if (scp != null) {
				BobsSecondInstance sf = new BobsSecondInstance();
				sf.setCode(scp.getCode());
				sf.setSampleCode(scp.getSampleCode());
				sf.setChipNum(scp.getChipNum());
				sf.setJg(scp.getJg());
				sf.setJgjs(scp.getJgjs());
				sf.setLcjy(scp.getLcjy());
				sf.setProductId(scp.getProductId());
				sf.setProductName(scp.getProductName());
				sf.setSampleType(scp.getSampleType());
				if (scp.getTemplate() != null) {
					sf.setTemplate(scp.getTemplate());
				}
				if (scp.getReportInfo() != null) {
					sf.setReportInfo(scp.getReportInfo());
				}
				sf.setSampleType(scp.getSampleType());
				sf.setTaskId(scp.getTaskId());
				sf.setTaskResultId(scp.getTaskResultId());
				sf.setTaskType(scp.getTaskType());
				sf.setState("1");
				sf.setFormerDate(new Date());
				commonDAO.saveOrUpdate(sf);
			}
			// 完成后改变一审的状态
			scp.setState("2");
			scp.setSubmit("1");
			sampleStateService.saveSampleState1(scp.getCode(),
					scp.getSampleCode(), scp.getProductId(),
					scp.getProductName(), "", format.format(new Date()),
					format.format(new Date()), "BobsFirstInstance",
					"BoBs数据审核核",
					(User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY),
					scp.getId(), "BoBs报告审核", "1", null, null, null, null, null,
					null, null, null, scp.getJg(), scp.getJgjs(),
					scp.getLcjy(), scp.getYcbg());
		}

	}
}
