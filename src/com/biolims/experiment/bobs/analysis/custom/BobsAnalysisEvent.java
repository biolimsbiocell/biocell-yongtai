package com.biolims.experiment.bobs.analysis.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.bobs.analysis.service.BobsAnalysisService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class BobsAnalysisEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		BobsAnalysisService mbService = (BobsAnalysisService) ctx.getBean("bobsAnalysisService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
