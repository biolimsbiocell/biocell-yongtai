package com.biolims.experiment.bobs.analysis.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysis;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysisResult;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysisTemp;

@Repository
@SuppressWarnings("unchecked")
public class BobsAnalysisDao extends BaseHibernateDao {
	public Map<String, Object> selectBobsAnalysisList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsAnalysis where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsAnalysis> list = new ArrayList<BobsAnalysis>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectBobsAnalysisResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsAnalysisResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsAnalysis.id='" + scId + "' and submit is null";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsAnalysisResult> list = new ArrayList<BobsAnalysisResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsAnalysisTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
//		String hql = "from BobsAnalysisTemp where 1=1 and state='1'";
//		String key = "";
		String key = " ";
		String hql = " from BobsAnalysisTemp where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsAnalysisTemp> list = new ArrayList<BobsAnalysisTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<BobsAnalysisResult> getResult(String id) {
		String hql = "from BobsAnalysisResult where 1=1 and bobsAnalysis.id='"
				+ id + "'";
		List<BobsAnalysisResult> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}
	/**
	 * 根据主表ID查询结果表
	 */
	public List<BobsAnalysisResult> getBobsAnalysisResultList(String id){
		String hql = " from BobsAnalysisResult where 1=1  and bobsAnalysis='"+id+"'";
		List<BobsAnalysisResult> list=this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<BobsAnalysisResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from BobsAnalysisResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<BobsAnalysisResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	// 根据样本编号查询
		public List<BobsAnalysisResult> setResultById(String code) {
			String hql = "from BobsAnalysisResult t where (submit is null or submit='') and bobsAnalysis.id='"
					+ code + "'";
			List<BobsAnalysisResult> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
}