package com.biolims.experiment.bobs.analysis.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;

/**
 * @Title: Model
 * @Description: BoBss分析结果
 * @author lims-platform
 * @date 2016-06-03 19:20:23
 * @version V1.0
 * 
 */
@Entity
@Table(name = "BOBS_ANALYSIS_RESULT")
@SuppressWarnings("serial")
public class BobsAnalysisResult extends EntityDao<BobsAnalysisResult> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本类型 */
	private String sampleType;
	/** 临床建议 */
	private String lcjy;
	/** 是否异常报告 */
	private String ycbg;
	/** 结果 */
	private String jg;
	/** 结果解释 */
	private String jgjs;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 下一步流向id */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 是否合格 */
	private String result;
	/** 是否提交 */
	private String submit;
	/** 状态 */
	private String state;
	/** 相关主表 */
	private BobsAnalysis bobsAnalysis;
	/** 备注 */
	private String note;
	/**模板*/
	private ReportTemplateInfo reportInfo;
	/**附件*/
	private FileInfo template;
	/**附件数量*/
	private String fileNum;
	/**附件状态*/
	private String fileState;
	/**上传时间*/
	private String upTime;
	
	public String getFileState() {
		return fileState;
	}

	public void setFileState(String fileState) {
		this.fileState = fileState;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 临床建议
	 */
	@Column(name = "LCJY", length = 50)
	public String getLcjy() {
		return this.lcjy;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 临床建议
	 */
	public void setLcjy(String lcjy) {
		this.lcjy = lcjy;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否异常报告
	 */
	@Column(name = "YCBG", length = 50)
	public String getYcbg() {
		return this.ycbg;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否异常报告
	 */
	public void setYcbg(String ycbg) {
		this.ycbg = ycbg;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果
	 */
	@Column(name = "JG", length = 500)
	public String getJg() {
		return this.jg;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果
	 */
	public void setJg(String jg) {
		this.jg = jg;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果解释
	 */
	@Column(name = "JGJS", length = 4000)
	public String getJgjs() {
		return this.jgjs;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果解释
	 */
	public void setJgjs(String jgjs) {
		this.jgjs = jgjs;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 50)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向id
	 */
	@Column(name = "NEXT_FLOW_ID", length = 50)
	public String getNextFlowId() {
		return this.nextFlowId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向id
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否合格
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否合格
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否提交
	 */
	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return this.submit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否提交
	 */
	public void setSubmit(String submit) {
		this.submit = submit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得BobsAnalysis
	 * 
	 * @return: BobsAnalysis 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BOBS_ANALYSIS")
	public BobsAnalysis getBobsAnalysis() {
		return this.bobsAnalysis;
	}

	/**
	 * 方法: 设置BobsAnalysis
	 * 
	 * @param: BobsAnalysis 相关主表
	 */
	public void setBobsAnalysis(BobsAnalysis bobsAnalysis) {
		this.bobsAnalysis = bobsAnalysis;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public FileInfo getTemplate() {
		return template;
	}
	public void setTemplate(FileInfo template) {
		this.template = template;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_INFO")
	public ReportTemplateInfo getReportInfo() {
		return reportInfo;
	}
	public void setReportInfo(ReportTemplateInfo reportInfo) {
		this.reportInfo = reportInfo;
	}

	public String getUpTime() {
		return upTime;
	}

	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
}