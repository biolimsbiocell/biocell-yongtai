package com.biolims.experiment.bobs.analysis.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 核型分析临时表
 * @author lims-platform
 * @date 2016-05-30 17:12:04
 * @version V1.0   
 *
 */
@Entity
@Table(name = "BOBS_ANALYSIS_TEMP")
@SuppressWarnings("serial")
public class BobsAnalysisTemp extends EntityDao<BobsAnalysisTemp> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**备注*/
	private String note ;
	/**原始样本编号*/
	private String sampleCode;
	/**状态*/
	private String state;
	/**样本编号*/
	private String code;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	// 样本类型
	private String sampleType;
	/** 玻片编号 */
	private String slideCode;
	/**缴费状态*/
	private String chargeNote;
	
	public String getChargeNote() {
		return chargeNote;
	}
	public void setChargeNote(String chargeNote) {
		this.chargeNote = chargeNote;
	}
	public String getSlideCode() {
		return slideCode;
	}
	public void setSlideCode(String slideCode) {
		this.slideCode = slideCode;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote (){
		return this.note ;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote (String note ){
		this.note  = note ;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
}