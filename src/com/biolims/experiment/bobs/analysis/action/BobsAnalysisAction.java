package com.biolims.experiment.bobs.analysis.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysis;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysisResult;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysisTemp;
import com.biolims.experiment.bobs.analysis.service.BobsAnalysisService;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/bobs/analysis/bobsAnalysis")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BobsAnalysisAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249205";
	@Autowired
	private BobsAnalysisService bobsAnalysisService;
	private BobsAnalysis bobsAnalysis = new BobsAnalysis();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showBobsAnalysisList")
	public String showBobsAnalysisList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysis.jsp");
	}

	@Action(value = "showBobsAnalysisListJson")
	public void showBobsAnalysisListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsAnalysisService.findBobsAnalysisList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsAnalysis> list = (List<BobsAnalysis>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sjDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("bobsSj-id", "");
		map.put("bobsSj-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "bobsAnalysisSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogBobsAnalysisList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysisDialog.jsp");
	}

	@Action(value = "showDialogBobsAnalysisListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogBobsAnalysisListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsAnalysisService.findBobsAnalysisList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsAnalysis> list = (List<BobsAnalysis>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sjDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("bobsSj-id", "");
		map.put("bobsSj-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editBobsAnalysis")
	public String editBobsAnalysis() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			bobsAnalysis = bobsAnalysisService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "bobsAnalysis");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			bobsAnalysis.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			bobsAnalysis.setCreateUser(user);
			bobsAnalysis.setCreateDate(new Date());
			bobsAnalysis.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			bobsAnalysis.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			UserGroup u=comSearchDao.get(UserGroup.class,"BoBsFG001");
			//if(bobsAnalysis.getAcceptUser()!=null){
				bobsAnalysis.setAcceptUser(u);
			//}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(bobsAnalysis.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysisEdit.jsp");
	}

	@Action(value = "copyBobsAnalysis")
	public String copyBobsAnalysis() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		bobsAnalysis = bobsAnalysisService.get(id);
		bobsAnalysis.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysisEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = bobsAnalysis.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "BobsAnalysis";
			String markCode = "BOBSFX";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			bobsAnalysis.setId(autoID);
			bobsAnalysis.setName("Bobs数据分析-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("bobsAnalysisResult",
				getParameterFromRequest("bobsAnalysisResultJson"));

		bobsAnalysisService.save(bobsAnalysis, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/bobs/analysis/bobsAnalysis/editBobsAnalysis.action?id="
				+ bobsAnalysis.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/bobs/analysis/bobsAnalysis/editBobsAnalysis.action?id="
		// + bobsAnalysis.getId());

	}

	@Action(value = "viewBobsAnalysis")
	public String toViewBobsAnalysis() throws Exception {
		String id = getParameterFromRequest("id");
		bobsAnalysis = bobsAnalysisService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysisEdit.jsp");
	}

	@Action(value = "showBobsAnalysisResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsAnalysisResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysisResult.jsp");
	}

	@Action(value = "showBobsAnalysisResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsAnalysisResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsAnalysisService
					.findBobsAnalysisResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsAnalysisResult> list = (List<BobsAnalysisResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("lcjy", "");
			map.put("ycbg", "");
			map.put("jg", "");
			map.put("jgjs", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("state", "");
			map.put("bobsAnalysis-name", "");
			map.put("bobsAnalysis-id", "");
			map.put("note", "");
			map.put("template-id", "");
			map.put("template-fileName", "");
			map.put("reportInfo-id", "");
			map.put("reportInfo-name", "");
			map.put("fileNum", "");
			map.put("upTime", "");
			map.put("fileState", "");
			
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsAnalysisResult")
	public void delBobsAnalysisResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsAnalysisService.delBobsAnalysisResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsAnalysisTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsAnalysisTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAnalysisTemp.jsp");
	}

	@Action(value = "showBobsAnalysisTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsAnalysisTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsAnalysisService
				.findBobsAnalysisTempList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<BobsAnalysisTemp> list = (List<BobsAnalysisTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		map.put("sampleCode", "");
		map.put("state", "");
		map.put("code", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("slideCode", "");
		map.put("chargeNote", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsAnalysisTemp")
	public void delBobsAnalysisTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsAnalysisService.delBobsAnalysisTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BobsAnalysisService getBobsAnalysisService() {
		return bobsAnalysisService;
	}

	public void setBobsAnalysisService(BobsAnalysisService bobsAnalysisService) {
		this.bobsAnalysisService = bobsAnalysisService;
	}

	public BobsAnalysis getBobsAnalysis() {
		return bobsAnalysis;
	}

	public void setBobsAnalysis(BobsAnalysis bobsAnalysis) {
		this.bobsAnalysis = bobsAnalysis;
	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsAnalysisService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id =getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.bobsAnalysisService.submitSample(id,ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	    /**
		 * 判断状态，是否已提交
		 * 
		 * @throws Exception
		 */
		 @Action(value = "remindSubmit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		 public void remindSubmit() throws Exception {
		 		Map<String, String> map = new HashMap<String, String >();
		 		try {
		 			String id = getParameterFromRequest("code");
		 			String state = bobsAnalysisService.selectRemindSubmit(id);
		 			map.put("state", state);
		 		} catch (Exception e) {
		 			e.printStackTrace();
		 		}
		 		HttpUtils.write(JsonUtils.toJsonString(map));
		 }
		 
		 /**
			 * 保存杂交洗脱样本明细
			 * @throws Exception
			 */
			@Action(value = "saveBobsAnalysisResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
			public void saveBobsAnalysisResult() throws Exception {
				String id = getRequest().getParameter("id");
				String itemDataJson = getRequest().getParameter("itemDataJson");
				Map<String, Object> result = new HashMap<String, Object>();
				try {
					BobsAnalysis sc=commonDAO.get(BobsAnalysis.class, id);
					Map aMap = new HashMap();
					aMap.put("bobsAnalysisResult",
							itemDataJson);
					if(sc!=null){
						bobsAnalysisService.save(sc,aMap);
					}
					result.put("success", true);
				} catch (Exception e) {
					e.printStackTrace();
					result.put("success", false);
				}
				HttpUtils.write(JsonUtils.toJsonString(result));
			}
}
