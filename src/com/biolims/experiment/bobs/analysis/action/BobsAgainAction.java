package com.biolims.experiment.bobs.analysis.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.bobs.analysis.model.BobsAgainInstance;
import com.biolims.experiment.bobs.analysis.service.BobsAgainService;
import com.biolims.experiment.bobs.first.model.BobsFirstInstance;
import com.biolims.experiment.bobs.first.service.BobsFirstService;
import com.biolims.experiment.snpjc.first.model.SnpFirstInstance;
import com.biolims.experiment.snpjc.first.service.SnpFirstService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/bobs/again")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BobsAgainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249206";
	@Autowired
	private BobsAgainService bobsAgainService;
	private BobsAgainInstance bobsAgainInstance = new BobsAgainInstance();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showBobsAgainInstanceList")
	public String showBobsAgainInstanceList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/analysis/bobsAgainInstance.jsp");
	}

	@Action(value = "showBobsAgainInstanceListJson")
	public void showBobsAgainInstanceListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsAgainService.findBobsAgainInstanceList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsAgainInstance> list = (List<BobsAgainInstance>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("chipNum", "");
		map.put("lcjy", "");
		map.put("ycbg", "");
		map.put("jg", "");
		map.put("jgjs", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("state", "");
		map.put("note", "");
		map.put("template-id", "");
		map.put("template-fileName", "");
		map.put("reportInfo-id", "");
		map.put("reportInfo-name", "");

		map.put("fileNum", "");
		map.put("taskType", "");
		map.put("taskResultId", "");
		map.put("taskId", "");
		
		map.put("formerDate", "yyyy-MM-dd");
		map.put("upTime", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsAgainInstance")
	public void delBobsAgainInstance() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsAgainService.delBobsAgainInstance(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BobsAgainService getBobsAgainService() {
		return bobsAgainService;
	}

	public void setBobsAgainService(BobsAgainService bobsAgainService) {
		this.bobsAgainService = bobsAgainService;
	}

	public BobsAgainInstance getBobsAgainInstance() {
		return bobsAgainInstance;
	}

	public void setBobsAgainInstance(BobsAgainInstance bobsAgainInstance) {
		this.bobsAgainInstance = bobsAgainInstance;
	}
	
	/**
	 * 保存一审结果
	 */
	@Action(value = "saveBobsAgainInstance",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveBobsAgainInstance() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			bobsAgainService.saveBobsAgainInstance(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsAgainService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
	
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.bobsAgainService.submitSample(ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
