package com.biolims.experiment.bobs.sj.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.bobs.sj.service.BobsSjService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class BobsSjEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		BobsSjService mbService = (BobsSjService) ctx.getBean("bobsSjService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
