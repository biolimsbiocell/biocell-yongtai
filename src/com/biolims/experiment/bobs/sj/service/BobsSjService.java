package com.biolims.experiment.bobs.sj.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.abnormal.model.BobsAbnormal;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysisTemp;
import com.biolims.experiment.bobs.cross.model.BobsCross;
import com.biolims.experiment.bobs.sj.dao.BobsSjDao;
import com.biolims.experiment.bobs.sj.model.BobsSj;
import com.biolims.experiment.bobs.sj.model.BobsSjCos;
import com.biolims.experiment.bobs.sj.model.BobsSjItem;
import com.biolims.experiment.bobs.sj.model.BobsSjReagent;
import com.biolims.experiment.bobs.sj.model.BobsSjResult;
import com.biolims.experiment.bobs.sj.model.BobsSjTemp;
import com.biolims.experiment.bobs.sj.model.BobsSjTemplate;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BobsSjService {
	@Resource
	private BobsSjDao bobsSjDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findBobsSjList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return bobsSjDao.selectBobsSjList(mapForQuery, startNum, limitNum, dir,
				sort);
	}

	public Map<String, Object> findBobsSjListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsSjDao.selectBobsSjListByState(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsSj i) throws Exception {

		bobsSjDao.saveOrUpdate(i);

	}

	public BobsSj get(String id) {
		BobsSj bobsSj = commonDAO.get(BobsSj.class, id);
		return bobsSj;
	}

	public Map<String, Object> findBobsSjItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSjDao.selectBobsSjItemList(scId,
				startNum, limitNum, dir, sort);
		List<BobsSjItem> list = (List<BobsSjItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSjTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSjDao.selectBobsSjTemplateList(scId,
				startNum, limitNum, dir, sort);
		List<BobsSjTemplate> list = (List<BobsSjTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSjReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSjDao.selectBobsSjReagentList(scId,
				startNum, limitNum, dir, sort);
		List<BobsSjReagent> list = (List<BobsSjReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSjCosList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = bobsSjDao.selectBobsSjCosList(scId,
				startNum, limitNum, dir, sort);
		List<BobsSjCos> list = (List<BobsSjCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSjResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSjDao.selectBobsSjResultList(scId,
				startNum, limitNum, dir, sort);
		List<BobsSjResult> list = (List<BobsSjResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsSjTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsSjDao.selectBobsSjTempList(scId,
				startNum, limitNum, dir, sort);
		List<BobsSjTemp> list = (List<BobsSjTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSjItem(BobsSj sc, String itemDataJson) throws Exception {
		List<BobsSjItem> saveItems = new ArrayList<BobsSjItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSjItem scp = new BobsSjItem();
			// 将map信息读入实体类
			scp = (BobsSjItem) bobsSjDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSj(sc);

			saveItems.add(scp);
		}
		bobsSjDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjItem(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSjItem scp = bobsSjDao.get(BobsSjItem.class, id);
			bobsSjDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjItemOne(String ids) throws Exception {
		BobsSjItem scp = bobsSjDao.get(BobsSjItem.class, ids);
		bobsSjDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSjTemplate(BobsSj sc, String itemDataJson)
			throws Exception {
		List<BobsSjTemplate> saveItems = new ArrayList<BobsSjTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSjTemplate scp = new BobsSjTemplate();
			// 将map信息读入实体类
			scp = (BobsSjTemplate) bobsSjDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSj(sc);

			saveItems.add(scp);
		}
		bobsSjDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSjTemplate scp = bobsSjDao.get(BobsSjTemplate.class, id);
			bobsSjDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSjReagent(BobsSj sc, String itemDataJson)
			throws Exception {
		List<BobsSjReagent> saveItems = new ArrayList<BobsSjReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSjReagent scp = new BobsSjReagent();
			// 将map信息读入实体类
			scp = (BobsSjReagent) bobsSjDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSj(sc);

			saveItems.add(scp);
		}
		bobsSjDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjReagent(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSjReagent scp = bobsSjDao.get(BobsSjReagent.class, id);
			bobsSjDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSjCos(BobsSj sc, String itemDataJson) throws Exception {
		List<BobsSjCos> saveItems = new ArrayList<BobsSjCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSjCos scp = new BobsSjCos();
			// 将map信息读入实体类
			scp = (BobsSjCos) bobsSjDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSj(sc);

			saveItems.add(scp);
		}
		bobsSjDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjCos(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSjCos scp = bobsSjDao.get(BobsSjCos.class, id);
			bobsSjDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOne(String ids, String model) throws Exception {
		if (model.equals("1")) {
			BobsSjTemplate scp = bobsSjDao.get(BobsSjTemplate.class, ids);
			if (scp != null) {
				bobsSjDao.delete(scp);
			}
		}
		if (model.equals("2")) {
			BobsSjReagent scp2 = bobsSjDao.get(BobsSjReagent.class, ids);
			if (scp2 != null) {
				bobsSjDao.delete(scp2);
			}
		}

		if (model.equals("3")) {
			BobsSjCos scp3 = bobsSjDao.get(BobsSjCos.class, ids);
			if (scp3 != null) {
				bobsSjDao.delete(scp3);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsSjResult(BobsSj sc, String itemDataJson)
			throws Exception {
		List<BobsSjResult> saveItems = new ArrayList<BobsSjResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsSjResult scp = new BobsSjResult();
			// 将map信息读入实体类
			scp = (BobsSjResult) bobsSjDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsSj(sc);

			saveItems.add(scp);
		/*	 if (scp != null) {
			 if (scp.getResult() != null && scp.getSubmit() != null
			 && !scp.getResult().equals("")
			 && !scp.getSubmit().equals("")) {
			 if (scp.getSubmit().equals("1")) {
			 if (scp.getResult().equals("0")) {
			 BobsAbnormal ka = new BobsAbnormal();
			 ka.setCode(scp.getCode());
			 ka.setSampleCode(scp.getSampleCode());
			 ka.setProductId(scp.getProductId());
			 ka.setProductName(scp.getProductName());
			 ka.setSampleType(scp.getSampleType());
			 ka.setTaskId(scp.getBobsSj().getId());
			 ka.setNote(scp.getNote());
			 ka.setTaskName("BoBs上机");
			 ka.setState("1");
			
			 commonDAO.saveOrUpdate(ka);
			 }
			 }
			 }
			 }*/
		}
		bobsSjDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjResult(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSjResult scp = bobsSjDao.get(BobsSjResult.class, id);
			bobsSjDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsSjTemp(String[] ids) throws Exception {
		for (String id : ids) {
			BobsSjTemp scp = bobsSjDao.get(BobsSjTemp.class, id);
			bobsSjDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsSj sc, Map jsonMap) throws Exception {
		if (sc != null) {
			bobsSjDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("bobsSjItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSjItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSjTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSjTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSjReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSjReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSjCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSjCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsSjResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsSjResult(sc, jsonStr);
			}
			List<BobsSjItem> snlist=bobsSjDao.getItem(sc.getId());
			List<BobsSjReagent> rglist=bobsSjDao.getReagent(sc.getId());
			DecimalFormat df = new DecimalFormat("######0.00"); 
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(BobsSjReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && 
								sr.getSampleNum()!=null){
							String n=df.format(sr.getOneNum()*snlist.size());
							sr.setNum(n);
						}
					}
				}
			}
		}
	}

	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		BobsSj kt = bobsSjDao.get(BobsSj.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		bobsSjDao.update(kt);

		// 完成后改变所选杂交实验的状态
		if (kt.getBobsCross() != null) {
			BobsCross bc = commonDAO.get(BobsCross.class, kt.getBobsCross()
					.getId());
			if (bc != null) {
				bc.setState("2");
			}
		}
		submitSample(id, null);
	}

	/**
	 * 根据主表ID获取结果表信息
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> getBobsSjResultById(String id)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = bobsSjDao.getBobsSjResultById(id);
		List<BobsSjResult> list = (List<BobsSjResult>) result.get("list");
		if (list != null && list.size() > 0) {
			for (BobsSjResult pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("note", pt.getNote());
				map.put("sampleCode", pt.getSampleCode());
				// map.put("checkCode", pt.getCheckCode());
				// map.put("orderNumber", pt.getOrderNumber());
				map.put("sampleType", pt.getSampleType());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// 提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		BobsSj sc = this.bobsSjDao.get(BobsSj.class, id);
		// 获取结果表样本信息

		List<BobsSjResult> list;
		if (ids == null)
			list = this.bobsSjDao.setResultById(id);
		else
			list = this.bobsSjDao.setResultByIds(ids);
		for (BobsSjResult scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && scp.getSubmit() == null) {
					String isOk = scp.getResult();
					if (isOk.equals("1")) {
						BobsAnalysisTemp k = new BobsAnalysisTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
						k.setState("1");
						bobsSjDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(sc.getCreateDate()),
										format.format(new Date()),
										"BobsSj",
										"BoBs上机",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "BoBs分析", scp.getResult(),
										null, null, null, null, null, null,
										null, null);
						scp.setSubmit("1");
						bobsSjDao.saveOrUpdate(scp);
					} else {
						BobsAbnormal ka = new BobsAbnormal();
						ka.setCode(scp.getCode());
						ka.setSampleCode(scp.getSampleCode());
						ka.setProductId(scp.getProductId());
						ka.setProductName(scp.getProductName());
						ka.setSampleType(scp.getSampleType());
						ka.setTaskId(scp.getBobsSj().getId());
						ka.setNote(scp.getNote());
						ka.setNextFlow("BoBs样本处理");
						ka.setNextFlowId("0032");
						ka.setTaskName("BoBs上机");
						ka.setState("1");
						bobsSjDao.saveOrUpdate(ka);
					}
				}
			}
		}
	}
	
	/**
	 * 生成Excel文件
	 * 
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createBobsSjItemCSV(String[] ids) throws Exception {
		String a = "/BoBs实验导出列表/BoBs上机明细";
		String filePath = SystemConstants.WRITE_QC_PATH ;// 写入csv路径,以fc号为准
		// 往qcdata中写入两个csv文件
		String markCode = DateUtil.dateFormatterByPattern(
					new Date(), "yyMMddHHmmss");
		String fileName = filePath + a +"/"+markCode+"BoBs上机明细.csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = {"code","sampleCode","checkCode",
		"orderNumber","sampleType","productName","note"};
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",")
					.toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		for(int i=0;i<ids.length;i++){
			BobsSjItem sr=bobsSjDao.getBobsSjItemByIds(ids[i]);
			StringBuffer sb = new StringBuffer();
			setBobsSjItemData(sr,sb);
			String rowStr = sb.toString();
			csvWtriter.write(rowStr);
			csvWtriter.newLine();
			sr.setState("1");
			bobsSjDao.saveOrUpdate(sr);
		}
		csvWtriter.flush();
		csvWtriter.close();
	}
	/**
	 * 生成BoBs上机明细CSV文件
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setBobsSjItemData(BobsSjItem s,StringBuffer sb) throws Exception {
		sb.append("\"").append(s.getCode()).append("\",");
		sb.append("\"").append(s.getSampleCode()).append("\",");
		sb.append("\"").append(s.getCheckCode()).append("\",");
		sb.append("\"").append(s.getOrderNumber()).append("\",");
		sb.append("\"").append(s.getSampleType()).append("\",");
		sb.append("\"").append(s.getProductName()).append("\",");
		sb.append("\"").append(s.getNote()).append("\",");
	}
	
	/**
	 * 生成Excel文件
	 * 
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createBobsSjResultCSV(String[] ids) throws Exception {
		String a = "/BoBs实验导出列表/BoBs上机结果";
		String filePath = SystemConstants.WRITE_QC_PATH ;
		String markCode = DateUtil.dateFormatterByPattern(
					new Date(), "yyMMddHHmmss");
		String fileName = filePath + a +"/"+markCode+"BoBs上机结果.csv";
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		csvFile.createNewFile();
		csvWtriter = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(csvFile), "GBK"), 1024);
		Object[] head = {"code","sampleCode","sampleType","productName",
				"result","submit","note"};
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",")
					.toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		for(int i=0;i<ids.length;i++){
			BobsSjResult sr=bobsSjDao.getBobsSjResultByIds(ids[i]);
			StringBuffer sb = new StringBuffer();
			setBobsSjResultData(sr,sb);
			String rowStr = sb.toString();
			csvWtriter.write(rowStr);
			csvWtriter.newLine();
			sr.setState("1");
			bobsSjDao.saveOrUpdate(sr);
		}
		csvWtriter.flush();
		csvWtriter.close();
		
	}
	/**
	 * 生成BoBs上机结果CSV文件
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setBobsSjResultData(BobsSjResult s,StringBuffer sb) throws Exception {
		sb.append("\"").append(s.getCode()).append("\",");
		sb.append("\"").append(s.getSampleCode()).append("\",");
		sb.append("\"").append(s.getSampleType()).append("\",");
		sb.append("\"").append(s.getProductName()).append("\",");
		sb.append("\"").append(s.getResult()).append("\",");
		sb.append("\"").append(s.getSubmit()).append("\",");
		sb.append("\"").append(s.getNote()).append("\",");
	}
	/**
	 * 判断状态，是否已提交
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String selectRemindSubmit(String id) throws Exception {
		BobsSj bs = commonDAO.get(BobsSj.class,
				id);
		if(bs != null &&( bs.getState() != null || !"".equals(bs.getState()))){
			String state = bs.getState();
			return state;
		}
		return "-1";
	}
}
