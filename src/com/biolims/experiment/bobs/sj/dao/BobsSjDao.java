package com.biolims.experiment.bobs.sj.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.cross.model.BobsCrossResult;
import com.biolims.experiment.bobs.sample.model.BobsSampleItem;
import com.biolims.experiment.bobs.sample.model.BobsSampleReagent;
import com.biolims.experiment.bobs.sj.model.BobsSj;
import com.biolims.experiment.bobs.sj.model.BobsSjCos;
import com.biolims.experiment.bobs.sj.model.BobsSjItem;
import com.biolims.experiment.bobs.sj.model.BobsSjReagent;
import com.biolims.experiment.bobs.sj.model.BobsSjResult;
import com.biolims.experiment.bobs.sj.model.BobsSjTemp;
import com.biolims.experiment.bobs.sj.model.BobsSjTemplate;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;
@Repository
@SuppressWarnings("unchecked")
public class BobsSjDao extends BaseHibernateDao {
	public Map<String, Object> selectBobsSjList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsSj where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsSj> list = new ArrayList<BobsSj>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsSj where 1=1 ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsSj> list = new ArrayList<BobsSj>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSjItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSjItem> list = new ArrayList<BobsSjItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSjTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSjTemplate> list = new ArrayList<BobsSjTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSjReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSjReagent> list = new ArrayList<BobsSjReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSjCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSjCos> list = new ArrayList<BobsSjCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSjResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSjResult> list = new ArrayList<BobsSjResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsSjTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsSjTemp where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsSjTemp> list = new ArrayList<BobsSjTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * ajax根据主表id查询子表信息
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getBobsSjResultById(String id) throws Exception {
		String hql = "from BobsSjResult t where 1=1";
		String key = "";
		if (id != null)
			key = key + " and  t.bobsSj='" + id + "'";
		List<BobsSjResult> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}

	// 根据样本编号查询
	public List<BobsSjResult> setResultById(String code) {
		String hql = "from BobsSjResult t where (submit is null or submit='') and bobsSj.id='"
				+ code + "'";
		List<BobsSjResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<BobsSjResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from BobsSjResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<BobsSjResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<BobsSjItem> getItem(String id) {
		String hql = "from BobsSjItem t where 1=1  and bobsSj='"+ id + "'";
		List<BobsSjItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public BobsSjItem getBobsSjItemByIds(String id) {
		String hql = "from BobsSjItem where 1=1 and id='"
				+ id + "'";
		BobsSjItem s = (BobsSjItem) this.getSession()
				.createQuery(hql).uniqueResult();
		return s;
	}
	/**
	 * 根据主表编号Result信息
	 * @param code
	 * @return
	 */
	public BobsSjResult getBobsSjResultByIds(String id) {
		String hql = "from BobsSjResult where 1=1 and id='"
				+ id + "'";
		BobsSjResult s = (BobsSjResult) this.getSession()
				.createQuery(hql).uniqueResult();
		return s;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<BobsSjReagent> getReagent(String id) {
		String hql = "from BobsSjReagent t where 1=1  and bobsSj='"+ id + "'";
		List<BobsSjReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
}