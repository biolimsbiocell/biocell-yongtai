package com.biolims.experiment.bobs.sj.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.sj.model.BobsSj;
import com.biolims.experiment.bobs.sj.model.BobsSjCos;
import com.biolims.experiment.bobs.sj.model.BobsSjItem;
import com.biolims.experiment.bobs.sj.model.BobsSjReagent;
import com.biolims.experiment.bobs.sj.model.BobsSjResult;
import com.biolims.experiment.bobs.sj.model.BobsSjTemp;
import com.biolims.experiment.bobs.sj.model.BobsSjTemplate;
import com.biolims.experiment.bobs.sj.service.BobsSjService;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/bobs/sj/bobsSj")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BobsSjAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249204";
	@Autowired
	private BobsSjService bobsSjService;
	private BobsSj bobsSj = new BobsSj();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showBobsSjList")
	public String showBobsSjList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSj.jsp");
	}

	@Action(value = "showBobsSjListJson")
	public void showBobsSjListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsSjService.findBobsSjList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsSj> list = (List<BobsSj>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sjDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("bobsCross-id", "");
		map.put("bobsCross-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "bobsSjSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogBobsSjList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjDialog.jsp");
	}

	@Action(value = "showDialogBobsSjListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogBobsSjListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsSjService.findBobsSjListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsSj> list = (List<BobsSj>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sjDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("bobsCross-id", "");
		map.put("bobsCross-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editBobsSj")
	public String editBobsSj() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			bobsSj = bobsSjService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "bobsSj");
		} else {
			bobsSj.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			bobsSj.setCreateUser(user);
			bobsSj.setCreateDate(new Date());
			bobsSj.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			bobsSj.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			
//			List<Template> tlist=comSearchDao.getTemplateByType("doBobsSj");
//			Template t=tlist.get(0);
//			bobsSj.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				bobsSj.setAcceptUser(t.getAcceptUser());
//			}
			
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(bobsSj.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjEdit.jsp");
	}

	@Action(value = "copyBobsSj")
	public String copyBobsSj() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		bobsSj = bobsSjService.get(id);
		bobsSj.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = bobsSj.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "BobsSj";
			String markCode = "BOBSSJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			bobsSj.setId(autoID);
			bobsSj.setName("Bobs上机-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("bobsSjItem", getParameterFromRequest("bobsSjItemJson"));

		aMap.put("bobsSjTemplate",
				getParameterFromRequest("bobsSjTemplateJson"));

		aMap.put("bobsSjReagent", getParameterFromRequest("bobsSjReagentJson"));

		aMap.put("bobsSjCos", getParameterFromRequest("bobsSjCosJson"));

		aMap.put("bobsSjResult", getParameterFromRequest("bobsSjResultJson"));

		bobsSjService.save(bobsSj, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/bobs/sj/bobsSj/editBobsSj.action?id="
				+ bobsSj.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return redirect("/experiment/bobs/sj/bobsSj/editBobsSj.action?id="
		// + bobsSj.getId());

	}

	@Action(value = "viewBobsSj")
	public String toViewBobsSj() throws Exception {
		String id = getParameterFromRequest("id");
		bobsSj = bobsSjService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjEdit.jsp");
	}

	@Action(value = "showBobsSjItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSjItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjItem.jsp");
	}

	@Action(value = "showBobsSjItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSjItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSjService.findBobsSjItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSjItem> list = (List<BobsSjItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleType", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("bobsSj-name", "");
			map.put("bobsSj-id", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjItem")
	public void delBobsSjItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSjService.delBobsSjItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjItemOne")
	public void delBobsSjItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			bobsSjService.delBobsSjItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSjTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSjTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjTemplate.jsp");
	}

	@Action(value = "showBobsSjTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSjTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSjService.findBobsSjTemplateList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSjTemplate> list = (List<BobsSjTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("bobsSj-name", "");
			map.put("bobsSj-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjTemplate")
	public void delBobsSjTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSjService.delBobsSjTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSjReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSjReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjReagent.jsp");
	}

	@Action(value = "showBobsSjReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSjReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSjService.findBobsSjReagentList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSjReagent> list = (List<BobsSjReagent>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("bobsSj-name", "");
			map.put("bobsSj-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjReagent")
	public void delBobsSjReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSjService.delBobsSjReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSjCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSjCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjCos.jsp");
	}

	@Action(value = "showBobsSjCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSjCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSjService.findBobsSjCosList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSjCos> list = (List<BobsSjCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("bobsSj-name", "");
			map.put("bobsSj-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjCos")
	public void delBobsSjCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSjService.delBobsSjCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			String model = getParameterFromRequest("model");
			bobsSjService.delOne(ids, model);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSjResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSjResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjResult.jsp");
	}

	@Action(value = "showBobsSjResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSjResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSjService.findBobsSjResultList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSjResult> list = (List<BobsSjResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("state", "");
			map.put("bobsSj-name", "");
			map.put("bobsSj-id", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjResult")
	public void delBobsSjResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSjService.delBobsSjResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsSjTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsSjTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/sj/bobsSjTemp.jsp");
	}

	@Action(value = "showBobsSjTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsSjTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsSjService.findBobsSjTempList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsSjTemp> list = (List<BobsSjTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsSjTemp")
	public void delBobsSjTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsSjService.delBobsSjTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据主表id加载结果子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "setResultToAnalysis", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setResultToAnalysis() throws Exception {
		String id = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.bobsSjService
					.getBobsSjResultById(id);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.bobsSjService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 生成Excel
	 * 
	 * @throws Exception
	 */
	 @Action(value = "createExcel") 
	 public void createExcel() throws Exception{ 
		 String[] ids = getRequest().getParameterValues("ids[]");
		 Map<String,Boolean> map = new HashMap<String, Boolean>(); 
		 try { 
			 bobsSjService.createBobsSjItemCSV(ids); 
			 map.put("success", true);
		 } catch (Exception e) { 
			 e.printStackTrace(); 
		 	 map.put("success", false);
		 }
		 HttpUtils.write(JsonUtils.toJsonString(map));
	 }
	/**
	 * 生成Excel
	 * 
	 * @throws Exception
	 */
	 @Action(value = "createExcelResult") 
	 public void createExcelResult() throws Exception{ 
		 String[] ids = getRequest().getParameterValues("ids[]");
		 Map<String,Boolean> map = new HashMap<String, Boolean>(); 
		 try { 
			 bobsSjService.createBobsSjResultCSV(ids); 
			 map.put("success", true);
		 } catch (Exception e) { 
			 e.printStackTrace(); 
		 	 map.put("success", false);
		 }
		 HttpUtils.write(JsonUtils.toJsonString(map));
	 }
	 
    /**
	 * 判断状态，是否已提交
	 * 
	 * @throws Exception
	 */
	 @Action(value = "remindSubmit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	 public void remindSubmit() throws Exception {
	 		Map<String, String> map = new HashMap<String, String >();
	 		try {
	 			String id = getParameterFromRequest("code");
	 			String state = bobsSjService.selectRemindSubmit(id);
	 			map.put("state", state);
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	 		HttpUtils.write(JsonUtils.toJsonString(map));
	 }
	 
	 /**
		 * 保存BoBs上机明细
		 * @throws Exception
		 */
		@Action(value = "saveBobsSjItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveBobsSjItem() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				BobsSj sc=commonDAO.get(BobsSj.class, id);
				Map aMap = new HashMap();
				aMap.put("bobsSjItem",
						itemDataJson);
				if(sc!=null){
					bobsSjService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
		 /**
		 * 保存BoBs上机结果
		 * @throws Exception
		 */
		@Action(value = "saveBobsSjResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveBobsSjResult() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				BobsSj sc=commonDAO.get(BobsSj.class, id);
				Map aMap = new HashMap();
				aMap.put("bobsSjResult",
						itemDataJson);
				if(sc!=null){
					bobsSjService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BobsSjService getBobsSjService() {
		return bobsSjService;
	}

	public void setBobsSjService(BobsSjService bobsSjService) {
		this.bobsSjService = bobsSjService;
	}

	public BobsSj getBobsSj() {
		return bobsSj;
	}

	public void setBobsSj(BobsSj bobsSj) {
		this.bobsSj = bobsSj;
	}

}
