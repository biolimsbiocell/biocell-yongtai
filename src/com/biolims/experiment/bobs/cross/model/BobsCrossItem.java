package com.biolims.experiment.bobs.cross.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: BoBs杂交明细
 * @author lims-platform
 * @date 2016-06-03 16:52:04
 * @version V1.0
 * 
 */
@Entity
@Table(name = "BOBS_CROSS_ITEM")
@SuppressWarnings("serial")
public class BobsCrossItem extends EntityDao<BobsCrossItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 实验编号 */
	private String orderNumber;
	/** 核对样本编号 */
	private String checkCode;
	/** 样本类型 */
	private String sampleType;
	/** 原始样本编号 */
	private String sampleCode;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 步骤编号 */
	private String stepNum;
	/** 关联任务单 */
	private String orderId;
	/** 状态 */
	private String state;
	/** 相关主表 */
	private BobsCross bobsCross;
	/** 备注 */
	private String note;
	/** 是否合格 */
	private String result;
	/** 是否提交 */
	private String submit;
	// 临时表Id
	private String tempId;

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验编号
	 */
	@Column(name = "ORDER_NUMBER", length = 50)
	public String getOrderNumber() {
		return this.orderNumber;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验编号
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对样本编号
	 */
	@Column(name = "CHECK_CODE", length = 50)
	public String getCheckCode() {
		return this.checkCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对样本编号
	 */
	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 50)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 步骤编号
	 */
	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return this.stepNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 步骤编号
	 */
	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得BobsCross
	 * 
	 * @return: BobsCross 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BOBS_CROSS")
	public BobsCross getBobsCross() {
		return this.bobsCross;
	}

	/**
	 * 方法: 设置BobsCross
	 * 
	 * @param: BobsCross 相关主表
	 */
	public void setBobsCross(BobsCross bobsCross) {
		this.bobsCross = bobsCross;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}
}