package com.biolims.experiment.bobs.cross.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.system.template.model.Template;

/**
 * @Title: Model
 * @Description: BoBs杂交
 * @author lims-platform
 * @date 2016-06-03 16:52:18
 * @version V1.0
 * 
 */
@Entity
@Table(name = "BOBS_CROSS")
@SuppressWarnings("serial")
public class BobsCross extends EntityDao<BobsCross> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 洗脱时间 */
	private Date washDate;
	/** 实验模板 */
	private Template template;
	/** 实验组 */
	private UserGroup acceptUser;
	/** 样本处理编号 */
	private BobsSample bobsSample;
	
	/** 样本处理编号 ID */
	private String bobsSampleId;
	/** 样本处理编号 NAME */
	private String bobsSampleName;
	
	public String getBobsSampleId() {
		return bobsSampleId;
	}

	public void setBobsSampleId(String bobsSampleId) {
		this.bobsSampleId = bobsSampleId;
	}

	public String getBobsSampleName() {
		return bobsSampleName;
	}

	public void setBobsSampleName(String bobsSampleName) {
		this.bobsSampleName = bobsSampleName;
	}

	/** 备注 */
	private String note;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 洗脱时间
	 */
	@Column(name = "WASH_DATE", length = 50)
	public Date getWashDate() {
		return this.washDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 洗脱时间
	 */
	public void setWashDate(Date washDate) {
		this.washDate = washDate;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 实验模板
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}

	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 实验模板
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * 方法: 取得UserGroup
	 * 
	 * @return: UserGroup 实验组
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public UserGroup getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置UserGroup
	 * 
	 * @param: UserGroup 实验组
	 */
	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得BobsSample
	 * 
	 * @return: BobsSample 样本处理编号
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BOBS_SAMPLE")
	public BobsSample getBobsSample() {
		return this.bobsSample;
	}

	/**
	 * 方法: 设置BobsSample
	 * 
	 * @param: BobsSample 样本处理编号
	 */
	public void setBobsSample(BobsSample bobsSample) {
		this.bobsSample = bobsSample;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 100)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态名称
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}