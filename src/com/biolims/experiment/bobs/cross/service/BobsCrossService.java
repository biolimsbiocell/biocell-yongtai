package com.biolims.experiment.bobs.cross.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.abnormal.model.BobsAbnormal;
import com.biolims.experiment.bobs.analysis.model.BobsAnalysisTemp;
import com.biolims.experiment.bobs.cross.dao.BobsCrossDao;
import com.biolims.experiment.bobs.cross.model.BobsCross;
import com.biolims.experiment.bobs.cross.model.BobsCrossCos;
import com.biolims.experiment.bobs.cross.model.BobsCrossItem;
import com.biolims.experiment.bobs.cross.model.BobsCrossReagent;
import com.biolims.experiment.bobs.cross.model.BobsCrossResult;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemp;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemplate;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.bobs.sj.model.BobsSj;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BobsCrossService {
	@Resource
	private BobsCrossDao bobsCrossDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	
	StringBuffer json = new StringBuffer();
	
	public Map<String, Object> findBobsSampleList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsCrossDao.selectBobsSampleList(mapForQuery, startNum,
				limitNum, dir, sort);
	}
	
	public Map<String, Object> findBobsCrossList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsCrossDao.selectBobsCrossList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findBobsCrossListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return bobsCrossDao.selectBobsCrossListByState(mapForQuery, startNum,
				limitNum, dir, sort);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsCross i) throws Exception {

		bobsCrossDao.saveOrUpdate(i);

	}

	public BobsCross get(String id) {
		BobsCross bobsCross = commonDAO.get(BobsCross.class, id);
		return bobsCross;
	}

	public Map<String, Object> findBobsCrossItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsCrossDao.selectBobsCrossItemList(scId,
				startNum, limitNum, dir, sort);
		List<BobsCrossItem> list = (List<BobsCrossItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsCrossTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsCrossDao.selectBobsCrossTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<BobsCrossTemplate> list = (List<BobsCrossTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBobsCrossReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsCrossDao.selectBobsCrossReagentList(
				scId, startNum, limitNum, dir, sort);
		List<BobsCrossReagent> list = (List<BobsCrossReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBobsCrossCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsCrossDao.selectBobsCrossCosList(scId,
				startNum, limitNum, dir, sort);
		List<BobsCrossCos> list = (List<BobsCrossCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsCrossResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsCrossDao.selectBobsCrossResultList(
				scId, startNum, limitNum, dir, sort);
		List<BobsCrossResult> list = (List<BobsCrossResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findBobsCrossTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = bobsCrossDao.selectBobsCrossTempList(mapForQuery, startNum, limitNum, dir, sort);
		List<BobsCrossTemp> list = (List<BobsCrossTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsCrossItem(BobsCross sc, String itemDataJson)
			throws Exception {
		List<BobsCrossItem> saveItems = new ArrayList<BobsCrossItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsCrossItem scp = new BobsCrossItem();
			// 将map信息读入实体类
			scp = (BobsCrossItem) bobsCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsCross(sc);

			saveItems.add(scp);
		}
		bobsCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossItem(String[] ids) throws Exception {
		for (String id : ids) {
			BobsCrossItem scp = bobsCrossDao.get(BobsCrossItem.class, id);
			bobsCrossDao.delete(scp);
		}
	}
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossItemOne(String ids) throws Exception {
		BobsCrossItem scp = bobsCrossDao.get(BobsCrossItem.class, ids);
		bobsCrossDao.delete(scp);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsCrossTemplate(BobsCross sc, String itemDataJson)
			throws Exception {
		List<BobsCrossTemplate> saveItems = new ArrayList<BobsCrossTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsCrossTemplate scp = new BobsCrossTemplate();
			// 将map信息读入实体类
			scp = (BobsCrossTemplate) bobsCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsCross(sc);

			saveItems.add(scp);
		}
		bobsCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			BobsCrossTemplate scp = bobsCrossDao.get(BobsCrossTemplate.class,
					id);
			bobsCrossDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsCrossReagent(BobsCross sc, String itemDataJson)
			throws Exception {
		List<BobsCrossReagent> saveItems = new ArrayList<BobsCrossReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsCrossReagent scp = new BobsCrossReagent();
			// 将map信息读入实体类
			scp = (BobsCrossReagent) bobsCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsCross(sc);

			saveItems.add(scp);
		}
		bobsCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossReagent(String[] ids) throws Exception {
		for (String id : ids) {
			BobsCrossReagent scp = bobsCrossDao.get(BobsCrossReagent.class, id);
			bobsCrossDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsCrossCos(BobsCross sc, String itemDataJson)
			throws Exception {
		List<BobsCrossCos> saveItems = new ArrayList<BobsCrossCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsCrossCos scp = new BobsCrossCos();
			// 将map信息读入实体类
			scp = (BobsCrossCos) bobsCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsCross(sc);

			saveItems.add(scp);
		}
		bobsCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossCos(String[] ids) throws Exception {
		for (String id : ids) {
			BobsCrossCos scp = bobsCrossDao.get(BobsCrossCos.class, id);
			bobsCrossDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOne(String ids, String model) throws Exception {
		if (model.equals("1")) {
			BobsCrossTemplate scp = bobsCrossDao.get(BobsCrossTemplate.class,
					ids);
			if (scp != null) {
				bobsCrossDao.delete(scp);
			}
		}
		if (model.equals("2")) {
			BobsCrossReagent scp2 = bobsCrossDao.get(BobsCrossReagent.class,
					ids);
			if (scp2 != null) {
				bobsCrossDao.delete(scp2);
			}
		}

		if (model.equals("3")) {
			BobsCrossCos scp3 = bobsCrossDao.get(BobsCrossCos.class, ids);
			if (scp3 != null) {
				bobsCrossDao.delete(scp3);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsCrossResult(BobsCross sc, String itemDataJson)
			throws Exception {
		List<BobsCrossResult> saveItems = new ArrayList<BobsCrossResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BobsCrossResult scp = new BobsCrossResult();
			// 将map信息读入实体类
			scp = (BobsCrossResult) bobsCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBobsCross(sc);

			saveItems.add(scp);
//			if (scp != null){
//				if (scp.getResult() != null && scp.getSubmit() != null &&
//						!scp.getResult().equals("") && !scp.getSubmit().equals("") ){
//					if(scp.getSubmit().equals("1")){
//						if(scp.getResult().equals("0")){
//							BobsAbnormal ka=new BobsAbnormal();
//							ka.setCode(scp.getCode());
//							ka.setSampleCode(scp.getSampleCode());
//							ka.setProductId(scp.getProductId());
//							ka.setProductName(scp.getProductName());
//							ka.setSampleType(scp.getSampleType());
//							ka.setTaskId(scp.getBobsCross().getId());
//							ka.setNote(scp.getNote());
//							ka.setTaskName("BoBs杂交");
//							ka.setState("1");
//							
//							commonDAO.saveOrUpdate(ka);
//						}
//					}
//				}
//			}
			/*commonDAO.saveOrUpdate(scp);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (scp != null) {
				if (scp.getResult()!= null && scp.getSubmit()!= null &&
					scp.getNextFlowId()!=null && !scp.getResult().equals("") 
					&& !scp.getSubmit().equals("") && !scp.getNextFlowId().equals("")){
					if(scp.getSubmit().equals("1")){
						if(scp.getResult().equals("1")){
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setState("1");
								commonDAO.saveOrUpdate(st);
								// 入库，改变SampleInfo中原始样本的状态为“待入库”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
								}
							} else if (scp.getNextFlowId().equals("0012")) {// 暂停
								// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
								}
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
								// 终止，改变SampleInfo中原始样本的状态为“实验终止”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
								}
							}else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}
						}
						sampleStateService
						.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"BobsCross",
							"BoBs杂交",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(),
							scp.getResult(), null, null, null,
							null, null, null, null, null);
					}
				}
			}*/
		}
		bobsCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossResult(String[] ids) throws Exception {
		for (String id : ids) {
			BobsCrossResult scp = bobsCrossDao.get(BobsCrossResult.class, id);
			bobsCrossDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsCrossTemp(String[] ids) throws Exception {
		for (String id : ids) {
			BobsCrossTemp scp = bobsCrossDao.get(BobsCrossTemp.class, id);
			bobsCrossDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BobsCross sc, Map jsonMap) throws Exception {
		if (sc != null) {
			bobsCrossDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("bobsCrossItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsCrossItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsCrossTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsCrossTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsCrossReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsCrossReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsCrossCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsCrossCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("bobsCrossResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBobsCrossResult(sc, jsonStr);
			}

		}
	}
	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		BobsCross kt = bobsCrossDao.get(BobsCross.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		bobsCrossDao.update(kt);
		
		//List<BobsCrossResult> list=bobsCrossDao
		//		.getBobsCrossResultList(kt.getId());
		List<BobsCrossItem> list=bobsCrossDao
				.getBobsCrossItemList(kt.getId());
		if(list.size()>0){
			for(BobsCrossItem scp:list){
				if (scp != null) {
					if (scp.getResult() != null
						&& !scp.getResult().equals("")) {
						String isOk = scp.getResult();
						if (isOk.equals("0")) {
						//	BobsAnalysisTemp k = new BobsAnalysisTemp();
						//	k.setCode(scp.getCode());
						//	k.setSampleCode(scp.getSampleCode());
						//	k.setProductId(scp.getProductId());
						//	k.setProductName(scp.getProductName());
						//	k.setSampleType(scp.getSampleType());
						//	k.setState("1");
						//	bobsCrossDao.saveOrUpdate(k);
						//	
						//} else {
							BobsAbnormal ka = new BobsAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(id);
							ka.setNote(scp.getNote());
							ka.setNextFlow("BoBs样本处理");
							ka.setNextFlowId("0032");
							ka.setTaskName("BoBs杂交");
							ka.setState("1");
							bobsCrossDao.saveOrUpdate(ka);
						}
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(kt.getCreateDate()),
										format.format(new Date()),
										"BobsCross",
										"BoBs杂交",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										kt.getId(), "BoBs上机", scp.getResult(),
										null, null, null, null, null, null,
										null, null);
						scp.setSubmit("1");
						bobsCrossDao.saveOrUpdate(scp);
					}
				}
			}
		}
	/*	//完成后改变所选样本处理的状态
		String ids=kt.getBobsSampleId();
		String [] ids1=ids.split(",");
		for(int i=0;i<ids1.length;i++){
			BobsSample bs=commonDAO.get(BobsSample.class,ids1[i]);
			if(bs!=null){
				bs.setState("2");
			}
		}*/
		//完成后改变所选样本的处理状态拿多个id
		String ids=kt.getBobsSampleId();
		String [] ids1=ids.split(",");
		for(int i=0;i<ids1.length;i++){
			BobsSample bs=commonDAO.get(BobsSample.class, ids1[i]);
			if(bs!=null){
				bs.setState("2");	
			}
		}
		
		
		
		
//		if(kt.getBobsSample()!=null){
//			BobsSample bs=commonDAO.get(BobsSample.class,
//					kt.getBobsSample().getId());
//			if(bs!=null){
//				bs.setState("2");
//			}
//			
//		}
		
	}
//	}
//	
	/**
	 * 根据主表ID获取明细表信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> getBobsCrossItemById(String id) throws Exception{
		List<Map<String, String>> mapList=new ArrayList<Map<String,String>>();
		Map<String,Object> result=bobsCrossDao.getBobsCrossItemById(id);
		List<BobsCrossItem> list=(List<BobsCrossItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (BobsCrossItem pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("result", pt.getResult());
				map.put("orderNumber", pt.getOrderNumber());
				map.put("sampleType", pt.getSampleType());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("note", pt.getNote());
				mapList.add(map);
			}
		}
		return mapList;
	}
	/**
	 * 根据主表ID获取结果表信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> getBobsCrossResultById(String id) throws Exception{
		List<Map<String, String>> mapList=new ArrayList<Map<String,String>>();
		Map<String,Object> result=bobsCrossDao.getBobsCrossResultById(id);
		List<BobsCrossItem> list=(List<BobsCrossItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (BobsCrossItem pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("result", pt.getResult());
				map.put("orderNumber", pt.getOrderNumber());
				map.put("sampleType", pt.getSampleType());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("note", pt.getNote());
				mapList.add(map);
			}
		}
		return mapList;
	}
	// 提交样本
		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void submitSample(String id, String[] ids) throws Exception {
			// 获取主表信息
			BobsSj sc = this.bobsCrossDao.get(BobsSj.class, id);
			// 获取结果表样本信息

			List<BobsCrossResult> list;
			if (ids == null)
				list = this.bobsCrossDao.setResultById(id);
			else
				list = this.bobsCrossDao.setResultByIds(ids);
			for (BobsCrossResult scp : list) {
				if (scp != null) {
					if (scp.getResult() != null && scp.getSubmit() == null) {
						String isOk = scp.getResult();
						if (isOk.equals("1")) {
							BobsAnalysisTemp k = new BobsAnalysisTemp();
							k.setCode(scp.getCode());
							k.setSampleCode(scp.getSampleCode());
							k.setProductId(scp.getProductId());
							k.setProductName(scp.getProductName());
							k.setSampleType(scp.getSampleType());
							k.setState("1");
							bobsCrossDao.saveOrUpdate(k);
							DateFormat format = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							sampleStateService
									.saveSampleState(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											DateUtil.format(sc.getCreateDate()),
											format.format(new Date()),
											"BobsCross",
											"BoBs杂交",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											sc.getId(), "BoBs上机", scp.getResult(),
											null, null, null, null, null, null,
											null, null);
							scp.setSubmit("1");
							bobsCrossDao.saveOrUpdate(scp);
						} else {
							BobsAbnormal ka = new BobsAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getBobsCross().getId());
							ka.setNote(scp.getNote());
							ka.setTaskName("BoBs杂交");
							ka.setState("1");
							bobsCrossDao.saveOrUpdate(ka);
						}
					}
				}
			}
		}
		
	/**
	 * 生成Excel文件
	 * 
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createBobsCrossItemCSV(String[] ids) throws Exception {
		// 时间转换
		SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		String a = "/BoBs实验导出列表/BoBs杂交明细";
		//String b = "/in/\\";
		String filePath = SystemConstants.WRITE_QC_PATH ;// 写入csv路径,以fc号为准
		// 往qcdata中写入两个csv文件
		String markCode = DateUtil.dateFormatterByPattern(
					new Date(), "yyMMddHHmmss");
		String fileName = filePath + a +"/"+markCode+"BoBs杂交明细.csv";// 文件名称
		/*String fileNames = filePath + "/in";
		File file = new File(fileNames);
		file.mkdirs();*/
		// String fName = filePath + b + "";// 生成in 文件夹
		File csvFile = null;
		//File csvFile1 = null;
		BufferedWriter csvWtriter = null;
	//	BufferedWriter csvWtriter1 = null;
		csvFile = new File(fileName);
		// csvFile1 = new File(fName);
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = {"code","sampleCode","checkCode",
		"orderNumber","sampleType","productName","result","note"};
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",")
					.toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		for(int i=0;i<ids.length;i++){
			BobsCrossItem sr=bobsCrossDao.getBobsCrossItemByIds(ids[i]);
			StringBuffer sb = new StringBuffer();
			setBobsCrossItemData(sr,sb);
			String rowStr = sb.toString();
			csvWtriter.write(rowStr);
			csvWtriter.newLine();
			sr.setState("1");
			bobsCrossDao.saveOrUpdate(sr);
		}
		csvWtriter.flush();
		csvWtriter.close();
	}
	/**
	 * 生成BoBs杂交明细CSV文件
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setBobsCrossItemData(BobsCrossItem s,StringBuffer sb) throws Exception {
		sb.append("\"").append(s.getCode()).append("\",");
		sb.append("\"").append(s.getSampleCode()).append("\",");
		sb.append("\"").append(s.getCheckCode()).append("\",");
		sb.append("\"").append(s.getOrderNumber()).append("\",");
		sb.append("\"").append(s.getProductName()).append("\",");
		sb.append("\"").append(s.getResult()).append("\",");
		sb.append("\"").append(s.getNote()).append("\",");
	}
	
	/**
	 * 判断状态，是否已提交
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String selectRemindSubmit(String id) throws Exception {
		BobsCross bs = commonDAO.get(BobsCross.class,
				id);
		if(bs != null &&( bs.getState() != null || !"".equals(bs.getState()))){
			String state = bs.getState();
			return state;
		}
		return "-1";
	}
		
}

