package com.biolims.experiment.bobs.cross.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.bobs.cross.model.BobsCross;
import com.biolims.experiment.bobs.cross.model.BobsCrossCos;
import com.biolims.experiment.bobs.cross.model.BobsCrossItem;
import com.biolims.experiment.bobs.cross.model.BobsCrossReagent;
import com.biolims.experiment.bobs.cross.model.BobsCrossResult;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemp;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemplate;
import com.biolims.experiment.bobs.cross.service.BobsCrossService;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/bobs/cross/bobsCross")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BobsCrossAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249203";
	@Autowired
	private BobsCrossService bobsCrossService;
	private BobsCross bobsCross = new BobsCross();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;
	
	//我的方法
		@Action(value = "bobsSampleList1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public String showSubmitReason() throws Exception {
			String type = getRequest().getParameter("type");
			putObjToContext("type", type);
			return dispatcher("/WEB-INF/page/experiment/bobs/cross/showCrossReason.jsp");
		}

		@Action(value = "showBobsSampleList1Json", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void showBobsSampleListJson() throws Exception {
			int startNum = Integer.parseInt(getParameterFromRequest("start"));
			int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
			String dir = getParameterFromRequest("dir");
			String sort = getParameterFromRequest("sort");
			String data = getParameterFromRequest("data");
			Map<String, String> map2Query = new HashMap<String, String>();
			if (data != null && data.length() > 0)
				map2Query = JsonUtils.toObjectByJson(data, Map.class);
			Map<String, Object> result = bobsCrossService.findBobsSampleList(
					map2Query, startNum, limitNum, dir, sort);
			Long count = (Long) result.get("total");
			List<BobsSample> list = (List<BobsSample>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("template-id", "");
			map.put("template-name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("note", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		}
		
	@Action(value = "showBobsCrossList")
	public String showBobsCrossList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCross.jsp");
	}

	@Action(value = "showBobsCrossListJson")
	public void showBobsCrossListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsCrossService.findBobsCrossList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsCross> list = (List<BobsCross>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("washDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("bobsSample-id", "");
		map.put("bobsSample-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		
		map.put("bobsSampleId", "");
		map.put("bobsSampleName", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "bobsCrossSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogBobsCrossList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossDialog.jsp");
	}

	@Action(value = "showDialogBobsCrossListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogBobsCrossListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsCrossService.findBobsCrossListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BobsCross> list = (List<BobsCross>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("washDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("bobsSample-id", "");
		map.put("bobsSample-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editBobsCross")
	public String editBobsCross() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			bobsCross = bobsCrossService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "bobsCross");
		} else {
			bobsCross.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			bobsCross.setCreateUser(user);
			bobsCross.setCreateDate(new Date());
			bobsCross.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			bobsCross.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			
//			List<Template> tlist=comSearchDao.getTemplateByType("doBobsCross");
//			Template t=tlist.get(0);
//			bobsCross.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				bobsCross.setAcceptUser(t.getAcceptUser());
//			}
			
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(bobsCross.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossEdit.jsp");
	}

	@Action(value = "copyBobsCross")
	public String copyBobsCross() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		bobsCross = bobsCrossService.get(id);
		bobsCross.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = bobsCross.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "BobsCross";
			String markCode = "BOBSZJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			bobsCross.setId(autoID);
			bobsCross.setName("Bobs杂交-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("bobsCrossItem", getParameterFromRequest("bobsCrossItemJson"));

		aMap.put("bobsCrossTemplate",
				getParameterFromRequest("bobsCrossTemplateJson"));

		aMap.put("bobsCrossReagent",
				getParameterFromRequest("bobsCrossReagentJson"));

		aMap.put("bobsCrossCos", getParameterFromRequest("bobsCrossCosJson"));

		aMap.put("bobsCrossResult",
				getParameterFromRequest("bobsCrossResultJson"));

		bobsCrossService.save(bobsCross, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/bobs/cross/bobsCross/editBobsCross.action?id="
				+ bobsCross.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/bobs/cross/bobsCross/editBobsCross.action?id="
		// + bobsCross.getId());

	}

	@Action(value = "viewBobsCross")
	public String toViewBobsCross() throws Exception {
		String id = getParameterFromRequest("id");
		bobsCross = bobsCrossService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossEdit.jsp");
	}

	@Action(value = "showBobsCrossItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsCrossItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossItem.jsp");
	}

	@Action(value = "showBobsCrossItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsCrossItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsCrossService
					.findBobsCrossItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsCrossItem> list = (List<BobsCrossItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleType", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("bobsCross-name", "");
			map.put("bobsCross-id", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("result", "");
			map.put("submit", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossItem")
	public void delBobsCrossItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsCrossService.delBobsCrossItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossItemOne")
	public void delBobsCrossItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			bobsCrossService.delBobsCrossItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsCrossTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsCrossTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossTemplate.jsp");
	}

	@Action(value = "showBobsCrossTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsCrossTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsCrossService
					.findBobsCrossTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsCrossTemplate> list = (List<BobsCrossTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("bobsCross-name", "");
			map.put("bobsCross-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossTemplate")
	public void delBobsCrossTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsCrossService.delBobsCrossTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsCrossReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsCrossReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossReagent.jsp");
	}

	@Action(value = "showBobsCrossReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsCrossReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsCrossService
					.findBobsCrossReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsCrossReagent> list = (List<BobsCrossReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("bobsCross-name", "");
			map.put("bobsCross-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossReagent")
	public void delBobsCrossReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsCrossService.delBobsCrossReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsCrossCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsCrossCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossCos.jsp");
	}

	@Action(value = "showBobsCrossCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsCrossCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsCrossService.findBobsCrossCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BobsCrossCos> list = (List<BobsCrossCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("bobsCross-name", "");
			map.put("bobsCross-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossCos")
	public void delBobsCrossCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsCrossService.delBobsCrossCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			String model = getParameterFromRequest("model");
			bobsCrossService.delOne(ids, model);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsCrossResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsCrossResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossResult.jsp");
	}

	@Action(value = "showBobsCrossResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsCrossResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bobsCrossService
					.findBobsCrossResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BobsCrossResult> list = (List<BobsCrossResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("state", "");
			map.put("bobsCross-name", "");
			map.put("bobsCross-id", "");
			map.put("note", "");
			map.put("orderNumber", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossResult")
	public void delBobsCrossResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsCrossService.delBobsCrossResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBobsCrossTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBobsCrossTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/bobs/cross/bobsCrossTemp.jsp");
	}

	@Action(value = "showBobsCrossTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBobsCrossTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = bobsCrossService.findBobsCrossTempList(
				map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<BobsCrossTemp> list = (List<BobsCrossTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("state", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBobsCrossTemp")
	public void delBobsCrossTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bobsCrossService.delBobsCrossTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 根据主表id加载子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "setItemToSj", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setItemToSj() throws Exception {
		String id = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.bobsCrossService
					.getBobsCrossItemById(id);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 根据主表id加载结果子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "setResultToSj", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setResultToSj() throws Exception {
		String id = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.bobsCrossService
					.getBobsCrossResultById(id);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 生成Excel
	 * 
	 * @throws Exception
	 */
	
	 @Action(value = "createExcel") 
	 public void createExcel() throws Exception{ 
		 String[] ids = getRequest().getParameterValues("ids[]");
		 Map<String,Boolean> map = new HashMap<String, Boolean>(); 
		 try { 
			 bobsCrossService.createBobsCrossItemCSV(ids); 
			 map.put("success", true);
		 } catch (Exception e) { 
			 e.printStackTrace(); 
		 	 map.put("success", false);
		 }
		 HttpUtils.write(JsonUtils.toJsonString(map));
	 }
	 
 /**
	 * 判断状态，是否已提交
	 * 
	 * @throws Exception
	 */
	 @Action(value = "remindSubmit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	 public void remindSubmit() throws Exception {
	 		Map<String, String> map = new HashMap<String, String >();
	 		try {
	 			String id = getParameterFromRequest("code");
	 			String state = bobsCrossService.selectRemindSubmit(id);
	 			map.put("state", state);
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	 		HttpUtils.write(JsonUtils.toJsonString(map));
	 }

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BobsCrossService getBobsCrossService() {
		return bobsCrossService;
	}

	public void setBobsCrossService(BobsCrossService bobsCrossService) {
		this.bobsCrossService = bobsCrossService;
	}

	public BobsCross getBobsCross() {
		return bobsCross;
	}

	public void setBobsCross(BobsCross bobsCross) {
		this.bobsCross = bobsCross;
	}
//	// 提交样本
//		@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//		public void submitSample() throws Exception {
//			String id = getParameterFromRequest("id");
//			String[] ids = getRequest().getParameterValues("ids[]");
//			Map<String, Object> result = new HashMap<String, Object>();
//			try {
//				this.bobsCrossService.submitSample(id, ids);
//
//				result.put("success", true);
//
//			} catch (Exception e) {
//				result.put("success", false);
//			}
//			HttpUtils.write(JsonUtils.toJsonString(result));
//		}
	
	 /**
	 * 保存BoBs杂交明细
	 * @throws Exception
	 */
	@Action(value = "saveBobsCrossItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveBobsCrossItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			BobsCross sc=commonDAO.get(BobsCross.class, id);
			Map aMap = new HashMap();
			aMap.put("bobsCrossItem",
					itemDataJson);
			if(sc!=null){
				bobsCrossService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
