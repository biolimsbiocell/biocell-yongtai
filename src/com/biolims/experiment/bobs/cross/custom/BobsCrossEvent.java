package com.biolims.experiment.bobs.cross.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.bobs.cross.service.BobsCrossService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class BobsCrossEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		BobsCrossService mbService = (BobsCrossService) ctx.getBean("bobsCrossService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
