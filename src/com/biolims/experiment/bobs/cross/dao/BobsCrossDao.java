package com.biolims.experiment.bobs.cross.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.cross.model.BobsCross;
import com.biolims.experiment.bobs.cross.model.BobsCrossCos;
import com.biolims.experiment.bobs.cross.model.BobsCrossItem;
import com.biolims.experiment.bobs.cross.model.BobsCrossReagent;
import com.biolims.experiment.bobs.cross.model.BobsCrossResult;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemp;
import com.biolims.experiment.bobs.cross.model.BobsCrossTemplate;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.bobs.sample.model.BobsSampleResult;
import com.biolims.experiment.bobs.sj.model.BobsSjResult;
import com.biolims.experiment.fish.model.FishCrossTask;

@Repository
@SuppressWarnings("unchecked")
public class BobsCrossDao extends BaseHibernateDao {
	public Map<String, Object> selectBobsCrossList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsCross where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsCross> list = new ArrayList<BobsCross>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsCrossListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsCross where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsCross> list = new ArrayList<BobsCross>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public Map<String, Object> selectBobsCrossItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsCrossItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsCrossItem> list = new ArrayList<BobsCrossItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsCrossTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsCrossTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsCrossTemplate> list = new ArrayList<BobsCrossTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsCrossReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsCrossReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsCrossReagent> list = new ArrayList<BobsCrossReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsCrossCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsCrossCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsCrossCos> list = new ArrayList<BobsCrossCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBobsCrossResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from BobsCrossResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bobsCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<BobsCrossResult> list = new ArrayList<BobsCrossResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectBobsSampleList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsSample where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
//		if (mapForQuery != null)
//			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsSample> list = new ArrayList<BobsSample>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectBobsCrossTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from BobsCrossTemp where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<BobsCrossTemp> list = new ArrayList<BobsCrossTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * 根据主表ID查询明细表
	 */
	public List<BobsCrossItem> getBobsCrossItemList(String id){
		String hql = " from BobsCrossItem where 1=1  and bobsCross.id='"+id+"'";
		List<BobsCrossItem> list=this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表ID查询结果表
	 */
	public List<BobsCrossResult> getBobsCrossResultList(String id){
		String hql = " from BobsCrossResult where 1=1  and bobsCross.id='"+id+"'";
		List<BobsCrossResult> list=this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * ajax根据主表id查询Result信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> getBobsCrossResultById(String id) throws Exception{
		String hql="from BobsCrossItem t where 1=1";
		String key = "";
		if (id != null)
			key = key + " and  t.bobsCross='" + id + "'";
		List<BobsCrossItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}
	/**
	 * ajax根据主表id查询Item信息
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> getBobsCrossItemById(String id) throws Exception{
		String hql="from BobsCrossItem t where 1=1";
		String key = "";
		if (id != null)
			key = key + " and  t.bobsCross='" + id + "'";
		List<BobsCrossItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", list);
		return result;
	}
	public BobsCrossItem getBobsCrossItemByIds(String id) {
		String hql = "from BobsCrossItem where 1=1 and id='"
				+ id + "'";
		BobsCrossItem s = (BobsCrossItem) this.getSession()
				.createQuery(hql).uniqueResult();
		return s;
	}

	// 根据样本编号查询
		public List<BobsCrossResult> setResultById(String code) {
			String hql = "from BobsCrossResult t where (submit is null or submit='')  and bobsCross.id='"
					+ code + "'";
			List<BobsCrossResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

		// 根据样本编号查询
		public List<BobsCrossResult> setResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}
			String hql = "from BobsCrossResult t where (submit is null or submit='') and id in ("
					+ insql + ")";
			List<BobsCrossResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

	}
	
	