package com.biolims.experiment.bobs.abnormal.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.bobs.abnormal.dao.BobsAbnormalDao;
import com.biolims.experiment.bobs.abnormal.model.BobsAbnormal;
import com.biolims.experiment.bobs.sample.dao.BobsSampleDao;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BobsAbnormalService {
	@Resource
	private BobsAbnormalDao bobsAbnormalDao;
	@Resource
	private BobsSampleDao bobsSampleDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findBobsAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = bobsAbnormalDao.selectBobsAbnormalList(mapForQuery,startNum,
				limitNum, dir,sort);
		List<BobsAbnormal> list = (List<BobsAbnormal>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBobsAbnormal(String itemDataJson) throws Exception {
		List<BobsAbnormal> saveItems = new ArrayList<BobsAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> map : list) {
			BobsAbnormal scp = new BobsAbnormal();
			// 将map信息读入实体类
			scp = (BobsAbnormal) bobsAbnormalDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);

			saveItems.add(scp);
			bobsAbnormalDao.saveOrUpdate(scp);
			/*if (scp != null) {
				if (scp.getIsSubmit() != null &&
					//scp.getResult() != null && 
					scp.getNextFlowId()!=null && 
					!scp.getIsSubmit().equals("") 
					//&& !scp.getResult().equals("")
					&& !scp.getNextFlowId().equals("")){
					if(scp.getIsSubmit().equals("1")){
						//if(scp.getResult().equals("1")){
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setState("1");
								commonDAO.saveOrUpdate(st);
								// 入库，改变SampleInfo中原始样本的状态为“待入库”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
								}
								scp.setState("2");
							} else if (scp.getNextFlowId().equals("0032")) {//BoBs样本处理
								List<BobsSampleTemp> bsList=bobsSampleDao.getTempByCode(scp.getCode());
								if(bsList.size()>0){
									BobsSampleTemp bs=bsList.get(0);
									bs.setState("1");
								}
							}else if (scp.getNextFlowId().equals("0012")) {// 暂停
								// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
								}
								
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
								// 终止，改变SampleInfo中原始样本的状态为“实验终止”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
								}
							}else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							//}
							scp.setState("2");
						}
					}
				}
			}*/
		}
		bobsAbnormalDao.saveOrUpdateAll(saveItems);
	}
	
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBobsAbnormal(String[] ids) throws Exception {
		for (String id : ids) {
			BobsAbnormal scp = bobsAbnormalDao.get(BobsAbnormal.class, id);
			bobsAbnormalDao.delete(scp);
		}
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String[] ids) throws Exception {
		List<BobsAbnormal> list = this.bobsAbnormalDao.setResultByIds(ids);
		for (BobsAbnormal scp : list) {
				if (scp != null) {
					if (scp.getIsSubmit() != null &&
						//scp.getResult() != null && 
						scp.getNextFlowId()!=null && 
						!scp.getIsSubmit().equals("") 
						//&& !scp.getResult().equals("")
						&& !scp.getNextFlowId().equals("")){
						if(scp.getIsSubmit().equals("1")){
								if (scp.getNextFlowId().equals("0009")) {// 样本入库
									SampleInItemTemp st = new SampleInItemTemp();
									st.setCode(scp.getCode());
									st.setSampleCode(scp.getSampleCode());
									st.setState("1");
									commonDAO.saveOrUpdate(st);
									// 入库，改变SampleInfo中原始样本的状态为“待入库”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
									}
									scp.setState("2");
								} else if (scp.getNextFlowId().equals("0032")) {
									//BoBs样本处理
									List<BobsSampleTemp> bsList=bobsSampleDao.getTempByCode(scp.getCode());
									if(bsList.size()>0){
										BobsSampleTemp bs=bsList.get(0);
										bs.setState("1");
									}
								}else if (scp.getNextFlowId().equals("0012")) {// 暂停
									// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
									}
									
								} else if (scp.getNextFlowId().equals("0013")) {// 终止
									// 终止，改变SampleInfo中原始样本的状态为“实验终止”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
									}
								}else {
									// 得到下一步流向的相关表单
									List<NextFlow> list_nextFlow = nextFlowDao
											.seletNextFlowById(scp.getNextFlowId());
									for (NextFlow n : list_nextFlow) {
										Object o = Class.forName(
												n.getApplicationTypeTable()
														.getClassPath())
												.newInstance();
										scp.setState("1");
										sampleInputService.copy(o, scp);
									}
								//}
								scp.setState("2");
							}
						}
					}
				}
			scp.setIsSubmit("1");
		}
	}
}
