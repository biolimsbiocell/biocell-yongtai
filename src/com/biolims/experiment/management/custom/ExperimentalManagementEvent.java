package com.biolims.experiment.management.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.management.service.ExperimentalManagementService;

public class ExperimentalManagementEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		ExperimentalManagementService mbService = (ExperimentalManagementService) ctx
				.getBean("experimentalManagementService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
