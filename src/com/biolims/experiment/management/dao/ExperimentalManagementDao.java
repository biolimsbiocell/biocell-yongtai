package com.biolims.experiment.management.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.management.model.ExperimentalManagement;
import com.biolims.experiment.management.model.ExperimentalManagementCos;
import com.biolims.experiment.management.model.ExperimentalManagementItem;
import com.biolims.experiment.management.model.ExperimentalManagementReagent;
import com.biolims.experiment.management.model.ExperimentalManagementTemp;
import com.biolims.experiment.management.model.ExperimentalManagementTemplate;
import com.biolims.experiment.management.model.ExperimentalManagementInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class ExperimentalManagementDao extends BaseHibernateDao {

	public Map<String, Object> findExperimentalManagementTable(Integer start, String type,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ExperimentalManagement where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(type!=null) {
			key += " and moduleType = '"+type+"'";
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ExperimentalManagement where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ExperimentalManagement> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectExperimentalManagementTempTable(String type, String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from ExperimentalManagementTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(type!=null&&!"".equals(type)) {
				key += " and moduleType = '"+type+"'";
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<ExperimentalManagementTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from ExperimentalManagementTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findExperimentalManagementItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ExperimentalManagementItem where 1=1 and state='1' and experimentalManagement.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ExperimentalManagementItem  where 1=1 and state='1' and experimentalManagement.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ExperimentalManagementItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<ExperimentalManagementItem> showWellPlate(String id) {
		String hql="from ExperimentalManagementItem  where 1=1 and state='2' and  experimentalManagement.id='"+id+"'";
		List<ExperimentalManagementItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findExperimentalManagementItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ExperimentalManagementItem where 1=1 and state='2' and experimentalManagement.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ExperimentalManagementItem  where 1=1 and state='2' and experimentalManagement.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ExperimentalManagementItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from ExperimentalManagementItem where 1=1 and state='2' and experimentalManagement.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from ExperimentalManagementItem where 1=1 and state='2' and experimentalManagement.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<ExperimentalManagementTemplate> showExperimentalManagementStepsJson(String id,String code) {

		String countHql = "select count(*) from ExperimentalManagementTemplate where 1=1 and experimentalManagement.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<ExperimentalManagementTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ExperimentalManagementTemplate where 1=1 and experimentalManagement.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ExperimentalManagementReagent> showExperimentalManagementReagentJson(String id,String code) {
		String countHql = "select count(*) from ExperimentalManagementReagent where 1=1 and experimentalManagement.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<ExperimentalManagementReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ExperimentalManagementReagent where 1=1 and experimentalManagement.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ExperimentalManagementCos> showExperimentalManagementCosJson(String id,String code) {
		String countHql = "select count(*) from ExperimentalManagementCos where 1=1 and experimentalManagement.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<ExperimentalManagementCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ExperimentalManagementCos where 1=1 and experimentalManagement.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ExperimentalManagementTemplate> delTemplateItem(String id) {
		List<ExperimentalManagementTemplate> list=new ArrayList<ExperimentalManagementTemplate>();
		String hql = "from ExperimentalManagementTemplate where 1=1 and experimentalManagement.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<ExperimentalManagementReagent> delReagentItem(String id) {
		List<ExperimentalManagementReagent> list=new ArrayList<ExperimentalManagementReagent>();
		String hql = "from ExperimentalManagementReagent where 1=1 and experimentalManagement.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<ExperimentalManagementCos> delCosItem(String id) {
		List<ExperimentalManagementCos> list=new ArrayList<ExperimentalManagementCos>();
		String hql = "from ExperimentalManagementCos where 1=1 and experimentalManagement.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showExperimentalManagementResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ExperimentalManagementInfo where 1=1 and experimentalManagement.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ExperimentalManagementInfo  where 1=1 and experimentalManagement.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ExperimentalManagementInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from ExperimentalManagementItem where 1=1 and experimentalManagement.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<ExperimentalManagementItem> plateSample(String id, String counts) {
		String hql="from ExperimentalManagementItem where 1=1 and experimentalManagement.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<ExperimentalManagementItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ExperimentalManagementItem where 1=1 and experimentalManagement.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ExperimentalManagementItem  where 1=1 and experimentalManagement.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ExperimentalManagementItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<ExperimentalManagementInfo> findExperimentalManagementInfoByCode(String code) {
		String hql="from ExperimentalManagementInfo where 1=1 and code='"+code+"'";
		List<ExperimentalManagementInfo> list=new ArrayList<ExperimentalManagementInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<ExperimentalManagementInfo> selectAllResultListById(String code) {
		String hql = "from ExperimentalManagementInfo  where (submit is null or submit='') and experimentalManagement.id='"
				+ code + "'";
		List<ExperimentalManagementInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<ExperimentalManagementInfo> selectResultListById(String id) {
		String hql = "from ExperimentalManagementInfo  where experimentalManagement.id='"
				+ id + "'";
		List<ExperimentalManagementInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ExperimentalManagementInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from ExperimentalManagementInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<ExperimentalManagementInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<ExperimentalManagementItem> selectExperimentalManagementItemList(String scId)
			throws Exception {
		String hql = "from ExperimentalManagementItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and experimentalManagement.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ExperimentalManagementItem> list = new ArrayList<ExperimentalManagementItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from ExperimentalManagementItem where 1=1 and experimentalManagement.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public ExperimentalManagementInfo getResultByCode(String code) {
			String hql=" from ExperimentalManagementInfo where 1=1 and code='"+code+"'";
			ExperimentalManagementInfo spi=(ExperimentalManagementInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}