package com.biolims.experiment.out.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;

/**
 * @Title: Model
 * @Description: 外包实验结果
 * @author lims-platform
 * @date 2015-11-22 17:06:03
 * @version V1.0
 * 
 */
@Entity
@Table(name = "OUT_TASK_INFO")
@SuppressWarnings("serial")
public class OutTaskInfo extends EntityDao<OutTaskInfo> implements
		java.io.Serializable {
	/** 编码 */
	private String id;

	/** 样本编号 */
	private String code;
	/** 原始样本编号*/
	private String sampleCode;
	/** 病人姓名 */
	private String patientName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 检测方法 */
	private String sequenceFun;
	/** 取样时间 */
	private String inspectDate;
	/** 身份证号 */
	private String idCard;
	/** 应出报告日期 */
	private String reportDate;
	/** 手机号 */
	private String phone;
	/** 任务单*/
	private String orderId;
	/** experiment.out储位 */
	private String location;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 结果(合格/不合格) */
	private String result;
	/** 处理意见 */
	private String method;
	/** 是否执行 */
	private String isExecute;
	/** 是否反馈到项目 */
	private String isToProject;
	/** 是否提交*/
	private String submit;
	/** 下一步流向ID*/
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 说明 */
	private String note;
	/** 状态 */
	private String state;
	/** 相关主表 */
	private OutTask outTask;
	/** 样本名称*/
	private String sampleName;
	/** 样本数量*/
	private Double sampleNum;
	/** 样本体积*/
	private Double sampleVolume;
	/** 补充体积*/
	private Double addVolume;
	/** 总量*/
	private Double sumVolume;
	/** indexs*/
	private String indexs;
	/** 临时表ID*/
	private String tempId;
	/** RIN*/
	private String rin;
	/** Nanodrop浓度*/
	private Double contraction;
	/** 处理意见*/
	private String reason;
	/** od260/230*/
	private Double od260;
	/** 0d260/280*/
	private Double od280;
	/** 项目编号 */
	private String projectId;
	/** 合同编号 */
	private String contractId;
	/** 任务单类型 */
	private String orderType;
	/** 科技服务任务单 */
	private String taskId;
	/** 区分临床还是科技服务 0 临床 1 科技服务*/
	private String classify;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
    /** 样本类型*/
	private String sampleType;
	/** 种系/变异*/
	private String dataBits;
	/** Qubit浓度*/
	private Double qbcontraction;
	/** 实验室样本号*/
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;

	/**
	 * 方法: 取得Info
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置Info
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getQbcontraction() {
		return qbcontraction;
	}

	public void setQbcontraction(Double qbcontraction) {
		this.qbcontraction = qbcontraction;
	}

	public String getDataBits() {
		return dataBits;
	}

	public void setDataBits(String dataBits) {
		this.dataBits = dataBits;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getSampleName() {
		return sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getSampleVolume() {
		return sampleVolume;
	}

	public void setSampleVolume(Double sampleVolume) {
		this.sampleVolume = sampleVolume;
	}

	public Double getAddVolume() {
		return addVolume;
	}

	public void setAddVolume(Double addVolume) {
		this.addVolume = addVolume;
	}

	public Double getSumVolume() {
		return sumVolume;
	}

	public void setSumVolume(Double sumVolume) {
		this.sumVolume = sumVolume;
	}

	public String getIndexs() {
		return indexs;
	}

	public void setIndexs(String indexs) {
		this.indexs = indexs;
	}

	public String getRin() {
		return rin;
	}

	public void setRin(String rin) {
		this.rin = rin;
	}

	public Double getContraction() {
		return contraction;
	}

	public void setContraction(Double contraction) {
		this.contraction = contraction;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getOd260() {
		return od260;
	}

	public void setOd260(Double od260) {
		this.od260 = od260;
	}

	public Double getOd280() {
		return od280;
	}

	public void setOd280(Double od280) {
		this.od280 = od280;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 接收样本号
	 */
	@Column(name = "SAMPLE_CODE", length = 36)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 接收样本号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String experiment.out储位
	 */
	@Column(name = "LOCATION", length = 60)
	public String getLocation() {
		return this.location;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String experiment.out储位
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 255)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 单位
	 */
	@Column(name = "UNIT", length = 60)
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 单位
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 说明
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 说明
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得OutTask
	 * 
	 * @return: OutTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OUT_TASK")
	public OutTask getOutTask() {
		return this.outTask;
	}

	/**
	 * 方法: 设置OutTask
	 * 
	 * @param: OutTask 相关主表
	 */
	public void setOutTask(OutTask outTask) {
		this.outTask = outTask;
	}

	@Column(name = "NEXT_FLOW", length = 20)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}


	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getIsToProject() {
		return isToProject;
	}

	public void setIsToProject(String isToProject) {
		this.isToProject = isToProject;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}
}