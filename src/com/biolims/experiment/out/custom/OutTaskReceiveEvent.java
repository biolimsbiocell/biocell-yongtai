package com.biolims.experiment.out.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.out.service.OutTaskReceiveService;

public class OutTaskReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		OutTaskReceiveService mbService = (OutTaskReceiveService) ctx
				.getBean("outTaskReceiveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
