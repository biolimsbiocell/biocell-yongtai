package com.biolims.experiment.out.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.out.service.OutTaskService;

public class OutTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		OutTaskService mbService = (OutTaskService) ctx
				.getBean("outTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
