package com.biolims.experiment.out.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.out.dao.OutTaskManageDao;
import com.biolims.experiment.out.model.OutTaskAbnormal;
import com.biolims.experiment.out.model.OutTaskItem;
import com.biolims.experiment.out.model.OutTaskTemp;
import com.biolims.experiment.out.model.OutTaskInfo;
import com.biolims.experiment.out.model.OutTaskWaitManage;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class OutTaskManageService {
	@Resource
	private OutTaskManageDao outTaskManageDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findOutTaskManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return outTaskManageDao.selectOutTaskManageList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findOutTaskItemList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return outTaskManageDao.selectOutTaskItemList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OutTaskWaitManage i) throws Exception {
		outTaskManageDao.saveOrUpdate(i);
	}

	public OutTaskWaitManage get(String id) {
		OutTaskWaitManage outTaskManage = commonDAO.get(
				OutTaskWaitManage.class, id);
		return outTaskManage;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OutTaskWaitManage sc, Map jsonMap) throws Exception {
		if (sc != null) {
			outTaskManageDao.saveOrUpdate(sc);
			String jsonStr = "";
		}
	}

	/**
	 * 保存中间产物管理
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOutTaskInfoManager(String itemDataJson)
			throws Exception {
		List<OutTaskInfo> saveItems = new ArrayList<OutTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OutTaskInfo sbi = new OutTaskInfo();
			sbi = (OutTaskInfo) outTaskManageDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
			if (sbi.getNextFlow() != null && sbi.getSubmit() != null) {
				if (sbi.getSubmit().equals("1")) {
					if (sbi.getNextFlow().equals("0")) {
						// 到文库构建
						WkTaskTemp wa = new WkTaskTemp();
						wa.setCode(sbi.getCode());
						wa.setCode(sbi.getCode());
//						wa.setSequenceFun(sbi.getSequenceFun());
						wa.setPatientName(sbi.getPatientName());
//						wa.setPhone(sbi.getPhone());
						wa.setProductId(sbi.getProductId());
						wa.setProductName(sbi.getProductName());
//						wa.setInspectDate(sbi.getInspectDate());			
						wa.setOrderId(sbi.getOrderId());
//						wa.setIdCard(sbi.getIdCard());
						wa.setState("1");
						wa.setVolume(sbi.getVolume());
						wa.setClassify(sbi.getClassify());
						outTaskManageDao.saveOrUpdate(wa);
						// 样本到建库，改变Info中原始样本的状态为“完成外包实验”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getCode());
						if (sf != null) {
							sf.setState("1");
							sf.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
						}
						sbi.setState("3");
					} else if (sbi.getNextFlow().equals("1")) {
						// 重新外包实验
						OutTaskAbnormal da = new OutTaskAbnormal();
						da.setClassify(sbi.getClassify());
						da.setCode(sbi.getCode());
						da.setFeedbackTime(DateUtil.dateFormatter(new Date()));
						da.setIdCard(sbi.getIdCard());
						da.setInspectDate(sbi.getInspectDate());
						da.setNextFlow(sbi.getNextFlow());
						da.setNote(sbi.getNote());
						da.setOrderId(sbi.getOrderId());
						da.setPatientName(sbi.getPatientName());
						da.setPhone(sbi.getPhone());
						da.setProductId(sbi.getProductId());
						da.setProductName(sbi.getProductName());
						da.setReportDate(sbi.getReportDate());
						da.setResult(sbi.getResult());
						da.setCode(sbi.getCode());
						da.setSequenceFun(sbi.getSequenceFun());
						this.outTaskManageDao.saveOrUpdate(da);
						sbi.setState("3");
					} else if (sbi.getNextFlow().equals("2")) { // 入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setCode(sbi.getCode());
						st.setNum(sbi.getVolume());
						st.setState("1");
						outTaskManageDao.saveOrUpdate(st);
						// 入库，改变Info中原始样本的状态为“待入库”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getCode());
						if (sf != null) {
							sf.setState("3");
							sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
						}
						sbi.setState("3");
					} else if (sbi.getNextFlow().equals("3")) {
						// 反馈到项目组
						
					} else if (sbi.getNextFlow().equals("4")) {
						// 终止，改变Info中原始样本的状态为“实验终止”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					} else {
						// 暂停，改变Info中原始样本的状态为“实验暂停”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
						}
					}
				}
			}
		}
		outTaskManageDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 保存样本管理(OutTaskItem)
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOutTaskItemManager(String itemDataJson) throws Exception {
		List<OutTaskItem> saveItems = new ArrayList<OutTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OutTaskItem sbi = new OutTaskItem();
			sbi = (OutTaskItem) outTaskManageDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
			if (sbi.getNextFlow() != null && sbi.getResult() != null) {
				if (sbi.getResult().equals("1")) {
					if (sbi.getNextFlow().equals("0")) {
						SampleInItemTemp st = new SampleInItemTemp();
						if (sbi.getCode() == null) {
							st.setCode(sbi.getCode());
						} else {
							st.setCode(sbi.getCode());
						}
						st.setCode(sbi.getCode());
						if (sbi.getSampleNum() != null
						&& sbi.getSampleConsume() != null) {
						st.setNum(sbi.getSampleNum() - sbi.getSampleConsume());
						st.setState("1");
						outTaskManageDao.saveOrUpdate(st);
						// 入库，改变Info中原始样本的状态为“待入库”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getCode());
						if (sf != null) {
							sf.setState("3");
							sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
						}
						sbi.setState("2");
					} else {
						// 终止，改变Info中原始样本的状态为“实验终止”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					}
				}
			}
		}
		outTaskManageDao.saveOrUpdateAll(saveItems);
	}
}
	/**
	 * 外包实验样本管理明细入库
	 * @param ids
	 */
	public void OutTaskManageItemRuku(String ids) {
		String[] id1 = ids.split(",");
		for (String id : id1) {
			OutTaskItem scp = outTaskManageDao.get(OutTaskItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				st.setCode(scp.getCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				outTaskManageDao.saveOrUpdate(st);
			}
		}
	}

	/**
	 * 外包实验样本管理明细入库
	 * @param id
	 */
	public void OutTaskManageItemTiqu(String id) {
		OutTaskItem scp = outTaskManageDao.get(OutTaskItem.class, id);
		if (scp != null) {
			scp.setState("2");
			OutTaskTemp st = new OutTaskTemp();
			st.setSampleCode(scp.getSampleCode());
			st.setCode(scp.getCode());
			st.setLabCode(scp.getLabCode());
			st.setProductId(scp.getProductId());
			st.setProductName(scp.getProductName());
			st.setSampleType(scp.getSampleType());
			if (scp.getSampleNum() != null && scp.getSampleConsume() != null) {
				st.setSampleNum(scp.getSampleNum() - scp.getSampleConsume());
			}
			st.setState("1");
			outTaskManageDao.saveOrUpdate(st);
		}
	}
}
