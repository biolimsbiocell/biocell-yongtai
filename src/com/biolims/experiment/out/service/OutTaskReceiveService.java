package com.biolims.experiment.out.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.out.dao.OutTaskReceiveDao;
import com.biolims.experiment.out.model.OutTaskReceiveTemp;
import com.biolims.experiment.out.model.OutTaskTemp;
import com.biolims.experiment.out.model.OutTaskReceive;
import com.biolims.experiment.out.model.OutTaskReceiveItem;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class OutTaskReceiveService {
	@Resource
	private OutTaskReceiveDao outTaskReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findOutTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return outTaskReceiveDao.selectOutTaskReceiveList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OutTaskReceive i) throws Exception {
		outTaskReceiveDao.saveOrUpdate(i);
	}

	public OutTaskReceive get(String id) {
		OutTaskReceive outTaskReceive = commonDAO.get(OutTaskReceive.class, id);
		return outTaskReceive;
	}

	public Map<String, Object> findOutTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = outTaskReceiveDao
				.selectOutTaskReceiveItemList(scId, startNum, limitNum,
						dir, sort);
		List<OutTaskReceiveItem> list = (List<OutTaskReceiveItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOutTaskReceiveItem(OutTaskReceive sc, String itemDataJson)
			throws Exception {
		List<OutTaskReceiveItem> saveItems = new ArrayList<OutTaskReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OutTaskReceiveItem scp = new OutTaskReceiveItem();
			// 将map信息读入实体类
			scp = (OutTaskReceiveItem) outTaskReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOutTaskReceive(sc);
			saveItems.add(scp);
		}
		outTaskReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOutTaskReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			OutTaskReceiveItem scp = outTaskReceiveDao.get(
					OutTaskReceiveItem.class, id);
			outTaskReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OutTaskReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			outTaskReceiveDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("outTaskReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOutTaskReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		OutTaskReceive sct = outTaskReceiveDao.get(OutTaskReceive.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		this.outTaskReceiveDao.update(sct);
		try {
			List<OutTaskReceiveItem> edri = this.outTaskReceiveDao
					.selectReceiveByIdList(sct.getId());
			for (OutTaskReceiveItem ed : edri) {
				if (ed.getMethod().equals("1")) {
					OutTaskTemp dst = new OutTaskTemp();
					dst.setConcentration(ed.getConcentration());
					dst.setLocation(ed.getLocation());
					dst.setCode(ed.getCode());
					dst.setId(ed.getId());
					dst.setSequenceFun(ed.getSequenceFun());
					dst.setPatientName(ed.getPatientName());
					dst.setProductName(ed.getProductName());
					dst.setProductId(ed.getProductId());
					dst.setIdCard(ed.getIdCard());
					dst.setInspectDate(ed.getInspectDate());
					dst.setPhone(ed.getPhone());
					dst.setOrderId(ed.getOrderId());
					dst.setReportDate(ed.getReportDate());
					dst.setState("1");
					dst.setClassify(ed.getClassify());
					dst.setCode(ed.getCode());
					dst.setSampleType(ed.getSampleType());
					dst.setSampleNum(ed.getSampleNum());
					dst.setLabCode(ed.getLabCode());
					outTaskReceiveDao.saveOrUpdate(dst);
					OutTaskReceiveTemp temp = commonDAO.get(OutTaskReceiveTemp.class,
							ed.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}
				// 审批完成改变Info中的样本状态为“待外包实验外包实验”
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(ed
						.getCode());
				if (sf != null) {
					sf.setState("3");
					sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
				}
				ed.setState("1");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 审批完成之后，将明细表数据保存到OutTask外包实验中间表
	 * @param code
	 */
	public void saveToOutTaskTemp(String code) {
		List<OutTaskReceiveItem> list = outTaskReceiveDao
				.selectReceiveItemListById(code);
		if (list.size() > 0) {
			for (OutTaskReceiveItem ed : list) {
				OutTaskTemp dst = new OutTaskTemp();
				dst.setConcentration(ed.getConcentration());
				dst.setLocation(ed.getLocation());
				dst.setCode(ed.getCode());
				dst.setId(ed.getId());
				dst.setSequenceFun(ed.getSequenceFun());
				dst.setPatientName(ed.getPatientName());
				dst.setProductName(ed.getProductName());
				dst.setProductId(ed.getProductId());
				dst.setIdCard(ed.getIdCard());
				dst.setInspectDate(ed.getInspectDate());
				dst.setPhone(ed.getPhone());
				dst.setOrderId(ed.getOrderId());
				dst.setReportDate(ed.getReportDate());
				dst.setState("1");
				dst.setClassify(ed.getClassify());
				dst.setSampleCode(ed.getSampleCode());
				dst.setSampleType(ed.getSampleType());
				dst.setSampleNum(ed.getSampleNum());
				outTaskReceiveDao.saveOrUpdate(dst);
			}
		}
	}
		public Map<String, Object> selectOutTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = outTaskReceiveDao.selectOutTaskReceiveTempList(
				startNum, limitNum, dir, sort);
		List<OutTaskReceiveTemp> list = (List<OutTaskReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
	
}
