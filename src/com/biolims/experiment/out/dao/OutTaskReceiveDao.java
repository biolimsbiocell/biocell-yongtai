package com.biolims.experiment.out.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.out.model.OutTaskReceiveTemp;
import com.biolims.experiment.out.model.OutTaskReceive;
import com.biolims.experiment.out.model.OutTaskReceiveItem;

@Repository
@SuppressWarnings("unchecked")
public class OutTaskReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectOutTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from OutTaskReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<OutTaskReceive> list = new ArrayList<OutTaskReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOutTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskReceiveItem> list = new ArrayList<OutTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOutTaskReceiveItemByTypeList(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from OutTaskReceiveItem where 1=1 and outTaskReceive.Type='OutTask'";
		String key = "";
		if (scId != null)
			key = key + " and outTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskReceiveItem> list = new ArrayList<OutTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询从开向检验完成的样本
	 * @return
	 */
	public List<OutTaskReceiveTemp> selectOutTaskReceiveTempList() {
		String hql = "from OutTaskReceiveTemp where 1=1 and state='1' order by reportDate desc";
		List<OutTaskReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 修改状态
	 * @param id
	 * @return
	 */
	public List<OutTaskReceiveItem> selectReceiveByIdList(String id) {
		String hql = "from OutTaskReceiveItem where 1=1 and state='1' and method='1' and outTaskReceive.id='"
				+ id + "'";
		List<OutTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 明细数据
	 * @param code
	 * @return
	 */
	public List<OutTaskReceiveItem> selectReceiveItemListById(String code) {
		String hql = "from OutTaskReceiveItem where 1=1 and outTaskReceive.id='"
				+ code + "'";
		List<OutTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public Map<String, Object> selectOutTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from OutTaskReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskReceiveTemp> list = new ArrayList<OutTaskReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
}