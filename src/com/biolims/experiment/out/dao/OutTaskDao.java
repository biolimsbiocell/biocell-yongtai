package com.biolims.experiment.out.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.out.model.OutTaskReceiveItem;
import com.biolims.experiment.out.model.OutTask;
import com.biolims.experiment.out.model.OutTaskCos;
import com.biolims.experiment.out.model.OutTaskItem;
import com.biolims.experiment.out.model.OutTaskReagent;
import com.biolims.experiment.out.model.OutTaskTemp;
import com.biolims.experiment.out.model.OutTaskTemplate;
import com.biolims.experiment.out.model.OutTaskInfo;

@Repository
@SuppressWarnings("unchecked")
public class OutTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectOutTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from OutTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<OutTask> list = new ArrayList<OutTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOutTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskItem> list = new ArrayList<OutTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOutTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskInfo> list = new ArrayList<OutTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 模板对应的子表
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectOutTaskTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskTemplate> list = new ArrayList<OutTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOutTaskTemplateReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskReagent> list = new ArrayList<OutTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据ItemId查询试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectOutTaskTemplateReagentListByItemId(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from OutTaskReagent t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskReagent> list = new ArrayList<OutTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from OutTaskReagent t where 1=1 and t.outTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<OutTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from OutTaskCos t where 1=1 and t.outTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<OutTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectOutTaskTemplateCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskCos> list = new ArrayList<OutTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectOutTaskTemplateCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from OutTaskCos t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskCos> list = new ArrayList<OutTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据OutTask外包实验主表ID查询试剂明细
	 */
	public List<OutTaskReagent> setReagentList(String code) {
		String hql = "from OutTaskReagent where 1=1 and outTask.id='" + code
				+ "'";
		List<OutTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据OutTask外包实验主表ID查询外包实验明细
	 */
	public List<OutTaskItem> setOutTaskItemList(String code) {
		String hql = "from OutTaskItem where 1=1 and outTask.id='" + code + "'";
		List<OutTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据状态查询OutTask
	 * @return
	 */
	public List<OutTask> selectOutTaskList() {
		// String hql = "from OutTask t where 1=1 and t.state=1";
		String hql = "from OutTask t where 1=1 and t.state='1'";
		List<OutTask> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<OutTaskInfo> selectOutTaskResultList(String id) {
		String hql = "from OutTaskInfo t where 1=1 and outTask.state='1' and outTask='"
				+ id + "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<OutTaskInfo> setOutTaskResultByCode(String code) {
		String hql = "from OutTaskInfo t where 1=1 and outTask.id='" + code
				+ "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<OutTaskInfo> setOutTaskResultById(String code) {
		String hql = "from OutTaskInfo t where submit is null and outTask.id='"
				+ code + "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<OutTaskInfo> setOutTaskResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from OutTaskInfo t where submit is null and id in ("
				+ insql + ")";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询外包实验样本去到QPCR或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectExperimentOutTaskReslutList(
			Integer startNum, Integer limitNum, String dir, String sort,
			String state) throws Exception {
		String hql = "from OutTaskInfo where 1=1 and experiment.outInfo.state='完成' and outTask.stateName='"
				+ state + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskInfo> list = new ArrayList<OutTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectOutTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskReceiveItem where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskReceiveItem> list = new ArrayList<OutTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询异常外包实验
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectOutTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from OutTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskInfo> list = new ArrayList<OutTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询左侧中间表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectOutTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from OutTaskTemp where 1=1 and state='1'";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<OutTaskTemp> list = new ArrayList<OutTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询外包实验异常样本
	 * @param code
	 * @return
	 */
	public List<OutTaskInfo> selectOutTaskResultAbnormalList(String code) {
		String hql = "from OutTaskInfo where 1=1 and  outTask='" + code + "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<OutTaskInfo> selectOutTaskAllList(String code) {
		String hql = "from OutTaskInfo  where 1=1  and outTask='" + code
				+ "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id查询外包实验生成结果 用于更新是否合格字段
	 * @param id
	 * @return
	 */
	public List<OutTaskInfo> findOutTaskResultById(String id) {
		String hql = "from OutTaskInfo  where 1=1 and code='" + id + "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 状态完成下一步流向
	 * @param code
	 * @return
	 */
	public List<OutTaskInfo> setNextFlowList(String code) {
		String hql = "from OutTaskInfo where 1=1  and outTask.state='1' and outTask.id='"
				+ code + "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从上一步样本接收完成的外包实验样本
	 * @return
	 */
	public List<OutTaskReceiveItem> selectReceiveOutTaskList() {
		String hql = "from OutTaskReceiveItem where 1=1  and state='1' ";
		List<OutTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from OutTaskItem t where 1=1 and t.outTask.id='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据id查询外包实验结果
	 * @param id
	 * @return
	 */
	public List<OutTaskInfo> selectOutTaskResultListById(String id) {
		String hql = "from OutTaskInfo where 1=1  and outTask.id='" + id
				+ "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的外包实验次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from OutTaskItem t where 1=1 and t.sampleCode='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.Code)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 查询从样本接收完成的的样本
	 * @param id
	 * @return
	 */
	public List<OutTaskItem> findOutTaskItemList(String id) {
		String hql = "from OutTaskItem  where 1=1 and state='1' and outTask='"
				+ id + "'";
		List<OutTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据Code查询OutTaskInfo
	 * @param code
	 * @return
	 */
	public String findOutTaskInfo(String code) {
		String hql = "from OutTaskInfo  where 1=1 and tempId='" + code + "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 根据样本编号查出OutTaskTemp对象
	 * @param code
	 * @return
	 */
	public OutTaskTemp findOutTaskTemp(String code) {
		String hql = "from OutTaskTemp t where 1=1 and t.sampleCode = '" + code
				+ "'";
		OutTaskTemp OutTaskTemp = (OutTaskTemp) this.getSession()
				.createQuery(hql).uniqueResult();
		return OutTaskTemp;
	}


	/**
	 * 根据样本查询主表
	 * @param sid
	 * @return
	 */
	public String selectTask(String sid) {
		String hql = "from TechCheckServiceOrder t where t.id = '" + sid + "'";
		String str = (String) this.getSession()
				.createQuery(" select t.id " + hql).uniqueResult();
		return str;
	}

	public List<OutTaskItem> selectOutTaskItemList(String scId) throws Exception {
		String hql = "from OutTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and outTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<OutTaskItem> list = new ArrayList<OutTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * 根据样本编号查询检测临时表
	 * @param id
	 * @return
	 */
	public List<OutTaskInfo> setOutTaskInfo(String id) {
		String hql = "from OutTaskInfo where 1=1 and outTask.id='" + id
				+ "' and nextFlowId='0009'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据入库的原始样本号查询该结果表信息
	 * @param id
	 * @param Code
	 * @return
	 */
	public List<OutTaskInfo> setOutTaskInfo2(String id, String code) {
		String hql = "from OutTaskInfo t where 1=1 and outTask.id='" + id
				+ "' and sampleCode='" + code + "'";
		List<OutTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}