﻿package com.biolims.experiment.out.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.out.model.OutTaskAbnormal;
import com.biolims.experiment.out.service.OutTaskRepeatService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/out")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class OutTaskRepeatAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240207";
	@Autowired
	private OutTaskRepeatService outTaskRepeatService;

	@Action(value = "showRepeatOutTaskList")
	public String showRepeatOutTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/out/outTaskRepeat.jsp");
	}

	@Action(value = "showRepeatOutTaskListJson")
	public void showRepeatOutTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = outTaskRepeatService
				.findRepeatOutTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OutTaskAbnormal> list = (List<OutTaskAbnormal>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("inspectDate", "");
		map.put("idCard", "");
		map.put("reportDate", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sampleType", "");
		map.put("sampleCondition", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("state", "");
		map.put("name", "");
		map.put("classify", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 保存外包实验异常样本
	 * @throws Exception
	 */
	@Action(value = "saveOutTaskRepeat")
	public void saveOutTaskRepeat() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			outTaskRepeatService.saveOutTaskRepeatList(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据条件检索重外包实验
	 * @throws Exception
	 */
	@Action(value = "selectRepeat")
	public void selectRepeat() throws Exception {
		String code1 = getParameterFromRequest("code");
		String code2 = getParameterFromRequest("Code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.outTaskRepeatService
					.selectOutTaskRepeat(code1, code2);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public OutTaskRepeatService getOutTaskRepeatService() {
		return outTaskRepeatService;
	}

	public void setOutTaskRepeatService(
			OutTaskRepeatService outTaskRepeatService) {
		this.outTaskRepeatService = outTaskRepeatService;
	}
}
