﻿package com.biolims.experiment.out.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.out.dao.OutTaskReceiveDao;
import com.biolims.experiment.out.model.OutTaskReceive;
import com.biolims.experiment.out.model.OutTaskReceiveTemp;
import com.biolims.experiment.out.model.OutTaskReceiveItem;
import com.biolims.experiment.out.service.OutTaskReceiveService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/experiment/out/outTaskReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class OutTaskReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240206";
	@Autowired
	private OutTaskReceiveService outTaskReceiveService;
	private OutTaskReceive outTaskReceive = new OutTaskReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showOutTaskReceiveList")
	public String showOutTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceive.jsp");
	}

	@Action(value = "showOutTaskReceiveListJson")
	public void showOutTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = outTaskReceiveService
				.findOutTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<OutTaskReceive> list = (List<OutTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "outTaskReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogOutTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceiveDialog.jsp");
	}

	@Action(value = "showDialogOutTaskReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogOutTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = outTaskReceiveService
				.findOutTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<OutTaskReceive> list = (List<OutTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("experiment.outCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editOutTaskReceive")
	public String editOutTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			outTaskReceive = outTaskReceiveService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "outTaskReceive");
		} else {
			outTaskReceive.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			outTaskReceive.setReceiveUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			outTaskReceive.setReceiverDate(stime);
			outTaskReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			outTaskReceive
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceiveEdit.jsp");
	}

	@Action(value = "copyOutTaskReceive")
	public String copyOutTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		outTaskReceive = outTaskReceiveService.get(id);
		outTaskReceive.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceiveEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = outTaskReceive.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "OutTaskReceive";
			String markCode = "HSJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			outTaskReceive.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("outTaskReceiveItem",
				getParameterFromRequest("outTaskReceiveItemJson"));
		outTaskReceiveService.save(outTaskReceive, aMap);
		return redirect("/experiment/out/outTaskReceive/editOutTaskReceive.action?id="
				+ outTaskReceive.getId());

	}

	@Action(value = "viewOutTaskReceive")
	public String toViewOutTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		outTaskReceive = outTaskReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceiveEdit.jsp");
	}

	@Action(value = "showOutTaskReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOutTaskReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceiveItem.jsp");
	}

	@Action(value = "showOutTaskReceiveItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOutTaskReceiveItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = outTaskReceiveService
					.findOutTaskReceiveItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<OutTaskReceiveItem> list = (List<OutTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleId", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("outTaskReceive-id", "");
			map.put("outTaskReceive-name", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOutTaskReceiveItem")
	public void delOutTaskReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			outTaskReceiveService.delOutTaskReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
 	/**
	* 去待接收的样本
	* @throws Exception
	 */
	@Action(value = "showOutTaskReceiveItemListTo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOutTaskReceiveItemListTo() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/out/outTaskReceiveItemTo.jsp");
	}

	@Action(value = "showOutTaskReceiveItemListToJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOutTaskReceiveItemListToJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, Object> result = outTaskReceiveService
					.selectOutTaskReceiveTempList(startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<OutTaskReceiveTemp> list = (List<OutTaskReceiveTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("inspectDate", "");
			map.put("sampleType", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("reportDate", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("classify", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public OutTaskReceiveService getOutTaskReceiveService() {
		return outTaskReceiveService;
	}

	public void setOutTaskReceiveService(
			OutTaskReceiveService outTaskReceiveService) {
		this.outTaskReceiveService = outTaskReceiveService;
	}

	public OutTaskReceive getOutTaskReceive() {
		return outTaskReceive;
	}

	public void setOutTaskReceive(OutTaskReceive outTaskReceive) {
		this.outTaskReceive = outTaskReceive;
	}
}
