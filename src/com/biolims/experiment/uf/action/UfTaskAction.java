﻿package com.biolims.experiment.uf.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.uf.dao.UfTaskDao;
import com.biolims.experiment.uf.model.UfTask;
import com.biolims.experiment.uf.model.UfTaskCos;
import com.biolims.experiment.uf.model.UfTaskItem;
import com.biolims.experiment.uf.model.UfTaskReagent;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.uf.model.UfTaskTemplate;
import com.biolims.experiment.uf.model.UfTaskInfo;
import com.biolims.experiment.uf.service.UfTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
import com.biolims.sample.service.SampleReceiveService;
@Namespace("/experiment/uf/ufTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class UfTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240307";
	@Autowired
	private UfTaskService ufTaskService;
	private UfTask ufTask = new UfTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private UfTaskDao ufTaskDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	/**
	 * 
	 * @Title: showUfTaskList
	 * @Description:展示主表
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUfTaskTable")
	public String showUfTaskTable() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/uf/ufTask.jsp");
	}

	@Action(value = "showUfTaskTableJson")
	public void showUfTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = ufTaskService.findUfTaskTable(
					start, length, query, col, sort);
			List<UfTask> list = (List<UfTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: editUfTask
	 * @Description: 新建实验单
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editUfTask")
	public String editUfTask() throws Exception {
		
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			ufTask = ufTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
			if (ufTask.getMaxNum() == null) {
				ufTask.setMaxNum(0);
			}
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			ufTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			ufTask.setCreateUser(user);
			ufTask.setMaxNum(0);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			ufTask.setCreateDate(stime);
			ufTask
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			ufTask
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<Template> templateList = templateService
				.showDialogTemplateTableJson("doBlood",null);
		List<Template> selTemplate = new ArrayList<Template>();
		List<UserGroupUser> userList = (List<UserGroupUser>) userGroupUserService
				.getUserGroupUserBygroupId("admin").get("list");
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int j = 0; j < templateList.size(); j++) {
			if (ufTask.getTemplate() != null) {
				if (ufTask.getTemplate().getId()
						.equals(templateList.get(j).getId())) {
					selTemplate.add(templateList.get(j));
					templateList.remove(j);
					j--;
				}
			}
		}
		for (int i = 0; i < userList.size(); i++) {
			if (ufTask.getTestUserOneId() != null) {
				if (ufTask.getTestUserOneId().indexOf(
						userList.get(i).getUser().getId()) > -1) {
					selUser.add(userList.get(i));
					userList.remove(i);
					i--;
				}
			}
		}
		putObjToContext("template", templateList);
		putObjToContext("selTemplate", selTemplate);
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		toState(ufTask.getState());
		return dispatcher("/WEB-INF/page/experiment/uf/ufTaskAllot.jsp");
	}

	/**
	 * 
	 * @Title: showUfTaskItemTable
	 * @Description: 展示待排板列表
	 * @author :
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUfTaskItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showUfTaskItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		ufTask = ufTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/uf/ufTaskMakeUp.jsp");
	}

	@Action(value = "showUfTaskItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showUfTaskItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = ufTaskService
					.findUfTaskItemTable(scId, start, length, query, col,
							sort);
			List<UfTaskItem> list = (List<UfTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("chromosomalLocation", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showUfTaskItemAfTableJson
	 * @Description: 排板后样本展示
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */

	@Action(value = "showUfTaskItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showUfTaskItemAfTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = ufTaskService
					.findUfTaskItemAfTable(scId, start, length, query, col,
							sort);
			List<UfTaskItem> list = (List<UfTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("ufTask-name", "");
			map.put("ufTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("chromosomalLocation", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("color", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delUfTaskItem
	 * @Description: 删除待排板样本
	 * @author
	 * @date
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delUfTaskItem")
	public void delUfTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr=getParameterFromRequest("del");
		String id=getParameterFromRequest("id");
		User user =(User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			ufTaskService.delUfTaskItem(delStr,ids,user,id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: delUfTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delUfTaskItemAf")
	public void delUfTaskItemAf() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			ufTaskService.delUfTaskItemAf(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBUfTaskResultTable
	 * @Description
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUfTaskResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showUfTaskResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		ufTask = ufTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/uf/ufTaskResult.jsp");
	}

	@Action(value = "showUfTaskResultTableJson")
	public void showUfTaskResultTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = ufTaskService
					.showUfTaskResultTableJson(id, start, length, query,
							col, sort);
			List<UfTaskInfo> list = (List<UfTaskInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("note", "");
			map.put("ufTask-id", "");
			map.put("ufTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: delUfTaskResult
	 * @Description: 删除结果明细
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delUfTaskResult")
	public void delUfTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			ufTaskService.delUfTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showUfTaskSteps
	 * @Description: 实验步骤
	 * @author : 
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUfTaskSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showUfTaskSteps() throws Exception {
		String id = getParameterFromRequest("id");
		ufTask = ufTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/uf/ufTaskSteps.jsp");
	}

	@Action(value = "showUfTaskStepsJson")
	public void showUfTaskStepsJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = ufTaskService.showUfTaskStepsJson(id, orderNum);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delUfTaskReagent")
	public void delUfTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			ufTaskService.delUfTaskReagent(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delUfTaskCos")
	public void delUfTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			ufTaskService.delUfTaskCos(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: makeCode
	 * @Description: 打印条码
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String id = getParameterFromRequest("id");
		String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
		CodeMain codeMain = null;
		codeMain = codeMainService.get(id);
		if (codeMain != null) {
			String printStr = "";
			String context="";
			for(int a=0;a<sampleCode.length;a++){
			String codeFull = sampleCode[a];
			String name= "";
			//sampleReceiveService.getNameBySampleCode(codeFull);
			printStr = codeMain.getCode();
			String code1 = sampleCode[a].substring(0, 9);
			String code2 = sampleCode[a].substring(9);
			printStr = printStr.replaceAll("@@code1@@", code1);
			printStr = printStr.replaceAll("@@code2@@", code2);
			printStr = printStr.replaceAll("@@code@@", codeFull);
			printStr = printStr.replaceAll("@@name@@",name );
			context+=printStr;
			}
			String ip = codeMain.getIp();
			Socket socket = null;
			OutputStream os;
			try {
				System.out.println(context);
				socket = new Socket();
				SocketAddress sa = new InetSocketAddress(ip, 9100);
				socket.connect(sa);
				os = socket.getOutputStream();
				os.write(context.getBytes("UTF-8"));
				os.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @Title: showUfTaskFromReceiveList
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUfTaskTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showUfTaskFromReceiveList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/uf/ufTaskTemp.jsp");
	}

	@Action(value = "showUfTaskTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showUfTaskTempTableJson() throws Exception {
				
		String [] codes=getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = ufTaskService
					.selectUfTaskTempTable(codes,start, length, query, col, sort);
			List<UfTaskTemp> list = (List<UfTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配页面
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "saveAllot")
	public void saveAllot() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = ufTaskService.saveAllot(main, tempId, userId,
					templateId,logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排版界面
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveMakeUp")
	public void saveMakeUp() throws Exception {
		String blood_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ufTaskService.saveMakeUp(blood_id, item,logInfo);
			result.put("success", true);
			result.put("id", blood_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveSteps")
	public void saveSteps() throws Exception {
		String id = getParameterFromRequest("id");
		String templateJson = getParameterFromRequest("templateJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String logInfo=getParameterFromRequest("logInfo");
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			ufTaskService.saveSteps(id, templateJson, reagentJson, cosJson,logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveResult")
	public void saveResult() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			ufTaskService.saveResult(id, dataJson,logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示排版
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<UfTaskItem> json = ufTaskService.showWellPlate(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ufTaskService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateSample")
	public void plateSample() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = ufTaskService.plateSample(id, counts);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description:展示孔板样本列表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateSampleTable")
	public void plateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = ufTaskService.plateSampleTable(id, counts, start,
					length, query, col, sort);
			List<UfTaskItem> list = (List<UfTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("ufTask-name", "");
			map.put("ufTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: uploadCsvFile
	 * @Description: 上传结果
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			ufTaskService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "bringResult")
	public void bringResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			ufTaskService.bringResult(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}

	}
	
	/**
	 * 
	 * @Title: generateBlendCode
	 * @Description: 生成混合号
	 * @author : shengwei.wang
	 * @date 2018年3月6日上午11:02:26 void
	 * @throws
	 */
	@Action(value = "generateBlendCode")
	public void generateBlendCode() {
		String id = getParameterFromRequest("id");
		try {
			Integer blendCode = ufTaskService.generateBlendCode(id);
			HttpUtils.write(String.valueOf(blendCode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public UfTaskService getUfTaskService() {
		return ufTaskService;
	}

	public void setUfTaskService(UfTaskService ufTaskService) {
		this.ufTaskService = ufTaskService;
	}

	public UfTask getUfTask() {
		return ufTask;
	}

	public void setUfTask(UfTask ufTask) {
		this.ufTask = ufTask;
	}

	public UfTaskDao getUfTaskDao() {
		return ufTaskDao;
	}

	public void setUfTaskDao(UfTaskDao ufTaskDao) {
		this.ufTaskDao = ufTaskDao;
	}

}
