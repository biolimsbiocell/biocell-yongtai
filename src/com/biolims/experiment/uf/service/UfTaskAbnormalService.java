package com.biolims.experiment.uf.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.uf.dao.UfTaskAbnormalDao;
import com.biolims.experiment.uf.model.UfTaskAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;
import com.biolims.sample.service.SampleInputService;

@Service
@Transactional
public class UfTaskAbnormalService {
	@Resource
	private UfTaskAbnormalDao ufTaskAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;

	/**
	 * 
	 * @Title: showUfTaskAbnormalTableJson
	 * @Description: 展示异常样本列表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showUfTaskAbnormalTableJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskAbnormalDao.showUfTaskAbnormalTableJson(start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description: 保存并执行
	 * @author : 
	 * @date 
	 * @param ids
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson) throws Exception {
		List<UfTaskAbnormal> saveItems1 = new ArrayList<UfTaskAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		UfTaskAbnormal scp = new UfTaskAbnormal();
		for (Map<String, Object> map : list) {
			scp = (UfTaskAbnormal) ufTaskAbnormalDao.Map2Bean(map, scp);
			UfTaskAbnormal sbi = commonDAO.get(UfTaskAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}

		ufTaskAbnormalDao.saveOrUpdateAll(saveItems1);
		List<UfTaskAbnormal> saveItems = new ArrayList<UfTaskAbnormal>();
		for (String id : ids) {
			UfTaskAbnormal sbi = commonDAO.get(UfTaskAbnormal.class, id);
			sbi.setIsExecute("1");
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi.getCode());
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {// 确认执行
					String next = sbi.getNextFlowId();
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setCode(sbi.getCode());
						st.setSampleCode(sbi.getSampleCode());
						st.setNum(sbi.getSampleNum());
						st.setState("1");
						ufTaskAbnormalDao.saveOrUpdate(st);
						// 入库，改变SampleInfo中原始样本的状态为“待入库”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
						}
					} else if (next.equals("0012")) {// 暂停
						// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
						}
					} else if (next.equals("0013")) {// 终止
						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					} else if (next.equals("0014")) {// 反馈项目组
					} else {
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sbi.setState("1");
							sampleInputService.copy(o, sbi);
						}
					}
					// 改变状态不显示在异常样本中
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		ufTaskAbnormalDao.saveOrUpdateAll(saveItems);
	}

}
