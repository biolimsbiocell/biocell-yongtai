package com.biolims.experiment.uf.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.uf.dao.UfTaskDao;
import com.biolims.experiment.uf.model.UfTask;
import com.biolims.experiment.uf.model.UfTaskAbnormal;
import com.biolims.experiment.uf.model.UfTaskCos;
import com.biolims.experiment.uf.model.UfTaskInfo;
import com.biolims.experiment.uf.model.UfTaskItem;
import com.biolims.experiment.uf.model.UfTaskReagent;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.uf.model.UfTaskTemplate;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
@Service
@Transactional
public class UfTaskService {
	
	@Resource
	private UfTaskDao ufTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : 
	 * @date 
	 * @param id
	 * @return UfTask
	 * @throws
	 */
	public UfTask get(String id) {
		UfTask ufTask = commonDAO.get(UfTask.class, id);
		return ufTask;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUfTaskItem(String delStr, String[] ids, User user, String ufTask_id) throws Exception {
		String delId="";
		for (String id : ids) {
			UfTaskItem scp = ufTaskDao.get(UfTaskItem.class, id);
			if (scp.getId() != null) {
				UfTask pt = ufTaskDao.get(UfTask.class, scp
						.getUfTask().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				UfTaskTemp ufTaskTemp = this.commonDAO.get(
						UfTaskTemp.class, scp.getTempId());
				if (ufTaskTemp != null) {
					ufTaskTemp.setState("1");
					ufTaskDao.update(ufTaskTemp);
				}
				ufTaskDao.update(pt);
				ufTaskDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(ufTask_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delUfTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUfTaskItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			UfTaskItem scp = ufTaskDao.get(UfTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				ufTaskDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUfTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			UfTaskInfo scp = ufTaskDao
					.get(UfTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				ufTaskDao.delete(scp);
		}
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUfTaskReagent(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			UfTaskReagent scp = ufTaskDao.get(UfTaskReagent.class,
					id);
			ufTaskDao.delete(scp);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUfTaskCos(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			UfTaskCos scp = ufTaskDao.get(UfTaskCos.class, id);
			ufTaskDao.delete(scp);
		}
	}

	/**
	 * 
	 * @Title: saveUfTaskTemplate
	 * @Description: 保存模板
	 * @author : 
	 * @date 
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveUfTaskTemplate(UfTask sc) {
		List<UfTaskTemplate> tlist2 = ufTaskDao.delTemplateItem(sc
				.getId());
		List<UfTaskReagent> rlist2 = ufTaskDao.delReagentItem(sc
				.getId());
		List<UfTaskCos> clist2 = ufTaskDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		for (TemplateItem t : tlist) {
			UfTaskTemplate ptt = new UfTaskTemplate();
			ptt.setUfTask(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			ufTaskDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			UfTaskReagent ptr = new UfTaskReagent();
			ptr.setUfTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			ufTaskDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			UfTaskCos ptc = new UfTaskCos();
			ptc.setUfTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			ufTaskDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : 
	 * @date 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		UfTask sct = ufTaskDao.get(UfTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		ufTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		UfTask sc = this.ufTaskDao.get(UfTask.class, id);
		// 获取结果表样本信息
		List<UfTaskInfo> list;
		if (ids == null)
			list = this.ufTaskDao.selectAllResultListById(id);
		else
			list = this.ufTaskDao.selectAllResultListByIds(ids);

		for (UfTaskInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						sampleInputService.copy(st, scp);

						st.setSampleType(scp.getSampleType());
						if (scp.getDicSampleType() != null) {
							st.setDicSampleType(scp.getDicSampleType());
						}
						// st.setConcentration(scp.getConcentration());
						// st.setVolume(scp.getVolume());
						// st.setSumTotal(scp.getSampleNum());
						st.setInfoFrom("UfTaskInfo");
						st.setState("1");
						// String[] sampleCode = scp.getSampleCode().split(",");
						// for (int i = 0; i < sampleCode.length; i++) {
						// SampleInfo s = this.sampleInfoMainDao
						// .findSampleInfo(sampleCode[i]);
						// if (s != null) {
						// st.setPatientName(s.getPatientName());
						// }
						// }
						ufTaskDao.saveOrUpdate(st);
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0017")) {// 核酸提取
						DnaTaskTemp d = new DnaTaskTemp();
						sampleInputService.copy(d, scp);
						DicSampleType t = commonDAO.get(DicSampleType.class,
								scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						if (scp.getProductId() != null) {
							Product p = commonDAO.get(Product.class,
									scp.getProductId());
						}
						ufTaskDao.saveOrUpdate(d);
					} else if (next.equals("0018")) {// 文库构建
						WkTaskTemp d = new WkTaskTemp();
						sampleInputService.copy(d, scp);
						DicSampleType t = commonDAO.get(DicSampleType.class,
								scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						if (scp.getProductId() != null) {
							Product p = commonDAO.get(Product.class,
									scp.getProductId());
						}
						ufTaskDao.saveOrUpdate(d);
						// } else if (next.equals("0010")) {// 重提取
						// DnaAbnormal eda = new DnaAbnormal();
						// scp.setState("4");// 状态为4 在重提取中显示
						// sampleInputService.copy(eda, scp);
						//
						// } else if (next.equals("0011")) {// 重建库
						// WkAbnormalBack wka = new WkAbnormalBack();
						// scp.setState("4");
						// sampleInputService.copy(wka, scp);
						// } else if (next.equals("0024")) {// 重抽血
						// UfTaskAbnormal wka = new UfTaskAbnormal();
						// scp.setState("4");
						// sampleInputService.copy(wka, scp);
					} else {
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					UfTaskAbnormal pa = new UfTaskAbnormal();// 样本异常
					sampleInputService.copy(pa, scp);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"UfTask",
								"样本处理",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				// 设备状态记录
//				Instrument ins = null;
//				String code = scp.getUfTask().getId();
//				String cosString = "PLASMA_TASK_COS";
//				List<String> result = experimentDnaGetDao.findInstrumentId(
//						code, cosString);
//				if (!result.equals(null)) {
//					for (int i = 0; i < result.size(); i++) {
//						String codes = result.get(i);
//
//						if (!codes.equals(null)) {
//							ins = experimentDnaGetDao.get(Instrument.class,
//									codes);
//
//							instrumentStateService.saveInstrumentState(
//									ins.getId(), scp.getSampleCode(),
//									ins.getName(), ins.getState().toString(),
//									scp.getProductId(), scp.getProductName(),
//									"", sc.getCreateDate(),
//									format.format(new Date()), "UfTask",
//									"样本处理", sc.getCreateUser().getName()
//											.toString(), sc.getId(),
//									scp.getNextFlow(), scp.getResult(), "",
//									sc.getTemplate().getName().toString(),
//									sc.getTestUserOneName(), null, null, null,
//									null, null, null, null, null);
//						}
//					}
//				}
				scp.setSubmit("1");
				ufTaskDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findUfTaskTable
	 * @Description: 展示主表
	 * @author :
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findUfTaskTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskDao.findUfTaskTable(start, length, query, col,
				sort);
	}

	/**
	 * 
	 * @Title: selectUfTaskTempTable
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectUfTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskDao.selectUfTaskTempTable(codes,start, length, query,
				col, sort);
	}
	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : 
	 * @date 
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			UfTask pt = new UfTask();
			pt = (UfTask) commonDAO.Map2Bean(list.get(0), pt);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "UfTask";
				String markCode = "YBCL";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
			} else {
				id = pt.getId();
			}
			Template t = new Template();
			t.setId(templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			ufTaskDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveUfTaskTemplate(pt);
			}
			if (tempId != null) {
				for (String temp : tempId) {
					UfTaskTemp ptt = ufTaskDao.get(
							UfTaskTemp.class, temp);
					UfTaskItem pti = new UfTaskItem();
					pti.setSampleCode(ptt.getSampleCode());
					pti.setCode(ptt.getCode());
					pti.setState("1");
					ptt.setState("2");
					pti.setTempId(temp);
					pti.setUfTask(pt);
					ufTaskDao.saveOrUpdate(ptt);
					ufTaskDao.saveOrUpdate(pti);
				}
			}
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				li.setUserId(pt.getCreateUser().getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findUfTaskItemTable
	 * @Description:展示未排版样本
	 * @author : 
	 * @date 
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findUfTaskItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskDao.findUfTaskItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : 
	 * @date 
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<UfTaskItem> saveItems = new ArrayList<UfTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		UfTask pt = commonDAO.get(UfTask.class, id);
		UfTaskItem scp = new UfTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (UfTaskItem) ufTaskDao.Map2Bean(map, scp);
			UfTaskItem pti = commonDAO.get(UfTaskItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			scp.setUfTask(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(pt.getCreateUser().getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		ufTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : 
	 * @date 
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<UfTaskItem>
	 * @throws
	 */
	public List<UfTaskItem> showWellPlate(String id) throws Exception {
		List<UfTaskItem> list = ufTaskDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findUfTaskItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date 
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findUfTaskItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskDao.findUfTaskItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			UfTaskItem pti = commonDAO.get(UfTaskItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			ufTaskDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showUfTaskStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date 
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showUfTaskStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<UfTaskTemplate> pttList = ufTaskDao
				.showUfTaskStepsJson(id, code);
		List<UfTaskReagent> ptrList = ufTaskDao
				.showUfTaskReagentJson(id, code);
		List<UfTaskCos> ptcList = ufTaskDao.showUfTaskCosJson(id,
				code);
		List<Object> plate = ufTaskDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showUfTaskResultTableJson
	 * @Description: 展示结果
	 * @author : 
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showUfTaskResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskDao.showUfTaskResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		UfTask pt = commonDAO.get(UfTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<UfTaskItem> list = ufTaskDao.plateSample(id, counts);
		Integer row = pt.getTemplate().getStorageContainer().getRowNum();
		Integer col = pt.getTemplate().getStorageContainer().getColNum();
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = ufTaskDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			UfTask pt = ufTaskDao.get(UfTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<UfTaskItem> listPTI = ufTaskDao
								.findUfTaskItemByCode(code);
						if (listPTI.size() > 0) {
							UfTaskInfo spi = new UfTaskInfo();
							spi.setSampleCode(listPTI.get(0).getSampleCode());
							spi.setCode(reader.get(0));
							spi.setConcentration(Double.parseDouble(reader
									.get(1)));
							spi.setVolume(Double.parseDouble(reader.get(2)));
							spi.setResult(reader.get(3).equals("合格") ? "1"
									: "0");
							spi.setUfTask(pt);
							spi.setProductId(listPTI.get(0).getProductId());
							spi.setProductName(listPTI.get(0).getPatientName());
							ufTaskDao.saveOrUpdate(spi);
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : 
	 * @date 
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		UfTask pt=commonDAO.get(UfTask.class, id);
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(pt.getCreateUser().getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}	
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date 
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			UfTaskTemplate ptt = ufTaskDao.get(
					UfTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			ufTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : 
	 * @date 
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			if (reagentId != null && !"".equals(reagentId)) {
				UfTaskReagent ptr = commonDAO.get(UfTaskReagent.class,
						reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}
				ufTaskDao.saveOrUpdate(ptr);
			} else {
				UfTaskReagent ptr = new UfTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setBatch(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemid");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
				}
				ufTaskDao.saveOrUpdate(ptr);
			}
		
		}
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
	
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			UfTaskCos ptc=null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(UfTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new UfTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setUfTask(commonDAO.get(UfTask.class, id));
				}
			}
			ufTaskDao.saveOrUpdate(ptc);
		}
	
	
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : 
	 * @date 
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return ufTaskDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
	
	
		List<UfTaskItem> list = ufTaskDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<UfTaskInfo> spiList = ufTaskDao.selectResultListById(id);
		for (UfTaskItem pti : list) {
			boolean b = true;
			for (UfTaskInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							UfTaskInfo scp = new UfTaskInfo();
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setUfTask(pti.getUfTask());
							scp.setResult("1");
							String markCode = "";
							if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = str;
									} else {
										markCode = str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = scp.getSampleCode();
									} else {
										markCode = scp.getSampleCode()
												+ d.getCode();
									}
								}

							}
							String code = codingRuleService.getCode(
									"UfTaskInfo", markCode, 00, 2, null);
							scp.setCode(code);
							ufTaskDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								UfTaskInfo scp = new UfTaskInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setUfTask(pti.getUfTask());
								scp.setResult("1");
								String markCode = "";
								if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}
								String code = codingRuleService.getCode(
										"UfTaskInfo", markCode, 00, 2,
										null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								ufTaskDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						UfTaskInfo spi = ufTaskDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						ufTaskDao.saveOrUpdate(spi);
					}
				}
			}
		}
	
	
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo) throws Exception {

		List<UfTaskInfo> saveItems = new ArrayList<UfTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		UfTask pt = commonDAO.get(UfTask.class, id);
		UfTaskInfo scp = new UfTaskInfo();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (UfTaskInfo) ufTaskDao.Map2Bean(map, scp);
			UfTaskInfo pti = commonDAO.get(UfTaskInfo.class,
					scp.getId());
			pti.setNextFlowId(scp.getNextFlowId());
			pti.setNextFlow(scp.getNextFlow());
			pti.setConcentration(scp.getConcentration());
			pti.setVolume(scp.getVolume());
			pti.setResult(scp.getResult());
			pti.setNote(scp.getNote());
			pti.setUfTask(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(pt.getCreateUser().getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		ufTaskDao.saveOrUpdateAll(saveItems);

	}

	public List<UfTaskItem> findUfTaskItemList(String scId)
			throws Exception {
		List<UfTaskItem> list = ufTaskDao.selectUfTaskItemList(scId);
		return list;
	}
	public Integer generateBlendCode(String id) {
		return ufTaskDao.generateBlendCode(id);
	}
}

