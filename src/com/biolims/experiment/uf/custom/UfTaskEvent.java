package com.biolims.experiment.uf.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.uf.service.UfTaskService;

public class UfTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		UfTaskService mbService = (UfTaskService) ctx
				.getBean("ufTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
