package com.biolims.experiment.pcr.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cfdna.model.CfdnaTaskItem;
import com.biolims.experiment.cfdna.model.CfdnaTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.other.model.OtherTaskTemp;
import com.biolims.experiment.pcr.dao.PcrTaskDao;
import com.biolims.experiment.pcr.model.PcrTask;
import com.biolims.experiment.pcr.model.PcrTaskCos;
import com.biolims.experiment.pcr.model.PcrTaskItem;
import com.biolims.experiment.pcr.model.PcrTaskReagent;
import com.biolims.experiment.pcr.model.PcrTaskResult;
import com.biolims.experiment.pcr.model.PcrTaskTemp;
import com.biolims.experiment.pcr.model.PcrTaskTemplate;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.uf.model.UfTask;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class PcrTaskService {
	@Resource
	private PcrTaskDao pcrTaskDao;
	@Resource
	private CommonService commonService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findPcrTaskList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return pcrTaskDao.selectPcrTaskList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PcrTask i) throws Exception {

		pcrTaskDao.saveOrUpdate(i);

	}

	public PcrTask get(String id) {
		PcrTask pcrTask = commonDAO.get(PcrTask.class, id);
		return pcrTask;
	}

	public Map<String, Object> findPcrTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = pcrTaskDao.selectPcrTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<PcrTaskItem> list = (List<PcrTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findPcrTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = pcrTaskDao.selectPcrTaskTemplateList(scId,
				startNum, limitNum, dir, sort);
		List<PcrTaskTemplate> list = (List<PcrTaskTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findPcrTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = pcrTaskDao.selectPcrTaskReagentList(scId,
				startNum, limitNum, dir, sort);
		List<PcrTaskReagent> list = (List<PcrTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findPcrTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = pcrTaskDao.selectPcrTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<PcrTaskCos> list = (List<PcrTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findPcrTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = pcrTaskDao.selectPcrTaskResultList(scId,
				startNum, limitNum, dir, sort);
		List<PcrTaskResult> list = (List<PcrTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findPcrTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = pcrTaskDao.selectPcrTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<PcrTaskTemp> list = (List<PcrTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePcrTaskItem(PcrTask sc, String itemDataJson)
			throws Exception {
		List<PcrTaskItem> saveItems = new ArrayList<PcrTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PcrTaskItem scp = new PcrTaskItem();
			// 将map信息读入实体类
			scp = (PcrTaskItem) pcrTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPcrTask(sc);

			saveItems.add(scp);
		}
		pcrTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			PcrTaskItem scp = pcrTaskDao.get(PcrTaskItem.class, id);
			pcrTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePcrTaskTemplate(PcrTask sc, String itemDataJson)
			throws Exception {
		List<PcrTaskTemplate> saveItems = new ArrayList<PcrTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PcrTaskTemplate scp = new PcrTaskTemplate();
			// 将map信息读入实体类
			scp = (PcrTaskTemplate) pcrTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPcrTask(sc);

			saveItems.add(scp);
		}
		pcrTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			PcrTaskTemplate scp = pcrTaskDao.get(PcrTaskTemplate.class, id);
			pcrTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskTemplateOne(String ids) throws Exception {
		PcrTaskTemplate scp = pcrTaskDao.get(PcrTaskTemplate.class, ids);
		pcrTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePcrTaskReagent(PcrTask sc, String itemDataJson)
			throws Exception {
		List<PcrTaskReagent> saveItems = new ArrayList<PcrTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PcrTaskReagent scp = new PcrTaskReagent();
			// 将map信息读入实体类
			scp = (PcrTaskReagent) pcrTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPcrTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		pcrTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			PcrTaskReagent scp = pcrTaskDao.get(PcrTaskReagent.class, id);
			pcrTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskReagentOne(String ids) throws Exception {
		PcrTaskReagent scp = pcrTaskDao.get(PcrTaskReagent.class, ids);
		pcrTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePcrTaskCos(PcrTask sc, String itemDataJson)
			throws Exception {
		List<PcrTaskCos> saveItems = new ArrayList<PcrTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PcrTaskCos scp = new PcrTaskCos();
			// 将map信息读入实体类
			scp = (PcrTaskCos) pcrTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPcrTask(sc);

			saveItems.add(scp);
		}
		pcrTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			PcrTaskCos scp = pcrTaskDao.get(PcrTaskCos.class, id);
			pcrTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskCosOne(String ids) throws Exception {
		PcrTaskCos scp = pcrTaskDao.get(PcrTaskCos.class, ids);
		pcrTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePcrTaskResult(PcrTask sc, String itemDataJson)
			throws Exception {
		List<PcrTaskResult> saveItems = new ArrayList<PcrTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PcrTaskResult scp = new PcrTaskResult();
			// 将map信息读入实体类
			scp = (PcrTaskResult) pcrTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPcrTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = scp.getSampleCode() + "PRC";
				String code = codingRuleService.getCode("PcrTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			pcrTaskDao.saveOrUpdate(scp);

			if (scp.getResult() != null && scp.getSubmit() != null) {
				if (scp.getSubmit().equals("1")) {
					if (scp.getResult().equals("1")) {
						String nextFlowId = scp.getNextFlowId();NextFlow nf = commonService.get(NextFlow.class, nextFlowId);nextFlowId = nf.getSysCode();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getVolume());
							st.setSampleType(scp.getSampleType());
							st.setState("1");
							pcrTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkTaskAbnormal wka = new WkTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0012")) {// 暂停

						} else if (nextFlowId.equals("0013")) {// 终止

						} else if (nextFlowId.equals("0020")) {// 2100质控
							Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(qc2100, scp);
							qc2100.setWkType("2");
							pcrTaskDao.saveOrUpdate(qc2100);
						} else if (nextFlowId.equals("0021")) {// QPCR质控
							QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(qpcr, scp);
							qpcr.setWkType("1");
							pcrTaskDao.saveOrUpdate(qpcr);
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"CfdnaTask",
							"cfDNA质量评估",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			PcrTaskResult scp = pcrTaskDao.get(PcrTaskResult.class, id);
			pcrTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPcrTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			PcrTaskTemp scp = pcrTaskDao.get(PcrTaskTemp.class, id);
			pcrTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PcrTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			pcrTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("pcrTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePcrTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("pcrTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePcrTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("pcrTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePcrTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("pcrTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePcrTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("pcrTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePcrTaskResult(sc, jsonStr);
			}

		}
	}

	// 审核完成
	public void changeState(String applicationTypeActionId, String id) {
		PcrTask sct = pcrTaskDao.get(PcrTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		List<PcrTaskItem> item = pcrTaskDao.setItemList(sct.getId());
		for (PcrTaskItem c : item) {
			PcrTaskTemp temp = commonDAO.get(PcrTaskTemp.class, c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
	}
}
