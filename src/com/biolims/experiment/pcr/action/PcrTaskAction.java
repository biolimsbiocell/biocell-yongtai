package com.biolims.experiment.pcr.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.pcr.model.PcrTask;
import com.biolims.experiment.pcr.model.PcrTaskCos;
import com.biolims.experiment.pcr.model.PcrTaskItem;
import com.biolims.experiment.pcr.model.PcrTaskReagent;
import com.biolims.experiment.pcr.model.PcrTaskResult;
import com.biolims.experiment.pcr.model.PcrTaskTemp;
import com.biolims.experiment.pcr.model.PcrTaskTemplate;
import com.biolims.experiment.pcr.service.PcrTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/pcr/pcrTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PcrTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2409";
	@Autowired
	private PcrTaskService pcrTaskService;
	private PcrTask pcrTask = new PcrTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showPcrTaskList")
	public String showPcrTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTask.jsp");
	}

	@Action(value = "showPcrTaskListJson")
	public void showPcrTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = pcrTaskService.findPcrTaskList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PcrTask> list = (List<PcrTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "pcrTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogPcrTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskDialog.jsp");
	}

	@Action(value = "showDialogPcrTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogPcrTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = pcrTaskService.findPcrTaskList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<PcrTask> list = (List<PcrTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editPcrTask")
	public String editPcrTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			pcrTask = pcrTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "pcrTask");
		} else {
			pcrTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			pcrTask.setCreateUser(user);
			pcrTask.setCreateDate(new Date());
			pcrTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			pcrTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskEdit.jsp");
	}

	@Action(value = "copyPcrTask")
	public String copyPcrTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		pcrTask = pcrTaskService.get(id);
		pcrTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = pcrTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "PcrTask";
			String markCode = "PCR";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			pcrTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("pcrTaskItem", getParameterFromRequest("pcrTaskItemJson"));

		aMap.put("pcrTaskTemplate",
				getParameterFromRequest("pcrTaskTemplateJson"));

		aMap.put("pcrTaskReagent",
				getParameterFromRequest("pcrTaskReagentJson"));

		aMap.put("pcrTaskCos", getParameterFromRequest("pcrTaskCosJson"));

		aMap.put("pcrTaskResult", getParameterFromRequest("pcrTaskResultJson"));

		pcrTaskService.save(pcrTask, aMap);
		return redirect("/experiment/pcr/pcrTask/editPcrTask.action?id="
				+ pcrTask.getId());

	}

	@Action(value = "viewPcrTask")
	public String toViewPcrTask() throws Exception {
		String id = getParameterFromRequest("id");
		pcrTask = pcrTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskEdit.jsp");
	}

	@Action(value = "showPcrTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPcrTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskItem.jsp");
	}

	@Action(value = "showPcrTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPcrTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pcrTaskService.findPcrTaskItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<PcrTaskItem> list = (List<PcrTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("orderNumber", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("pcrTask-name", "");
			map.put("pcrTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("counts", "");
			map.put("unit", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("productNum", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskItem")
	public void delPcrTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			pcrTaskService.delPcrTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPcrTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPcrTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskTemplate.jsp");
	}

	@Action(value = "showPcrTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPcrTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pcrTaskService
					.findPcrTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<PcrTaskTemplate> list = (List<PcrTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("pcrTask-name", "");
			map.put("pcrTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskTemplate")
	public void delPcrTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			pcrTaskService.delPcrTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskTemplateOne")
	public void delPcrTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			pcrTaskService.delPcrTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPcrTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPcrTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskReagent.jsp");
	}

	@Action(value = "showPcrTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPcrTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pcrTaskService.findPcrTaskReagentList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<PcrTaskReagent> list = (List<PcrTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("pcrTask-name", "");
			map.put("pcrTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskReagent")
	public void delPcrTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			pcrTaskService.delPcrTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskReagentOne")
	public void delPcrTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			pcrTaskService.delPcrTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPcrTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPcrTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskCos.jsp");
	}

	@Action(value = "showPcrTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPcrTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pcrTaskService.findPcrTaskCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<PcrTaskCos> list = (List<PcrTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("pcrTask-name", "");
			map.put("pcrTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskCos")
	public void delPcrTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			pcrTaskService.delPcrTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskCosOne")
	public void delPcrTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			pcrTaskService.delPcrTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPcrTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPcrTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskResult.jsp");
	}

	@Action(value = "showPcrTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPcrTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pcrTaskService.findPcrTaskResultList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<PcrTaskResult> list = (List<PcrTaskResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("pcrTask-name", "");
			map.put("pcrTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskResult")
	public void delPcrTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			pcrTaskService.delPcrTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPcrTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPcrTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/pcr/pcrTaskTemp.jsp");
	}

	@Action(value = "showPcrTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPcrTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pcrTaskService.findPcrTaskTempList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<PcrTaskTemp> list = (List<PcrTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("sampleType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPcrTaskTemp")
	public void delPcrTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			pcrTaskService.delPcrTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PcrTaskService getPcrTaskService() {
		return pcrTaskService;
	}

	public void setPcrTaskService(PcrTaskService pcrTaskService) {
		this.pcrTaskService = pcrTaskService;
	}

	public PcrTask getPcrTask() {
		return pcrTask;
	}

	public void setPcrTask(PcrTask pcrTask) {
		this.pcrTask = pcrTask;
	}

}
