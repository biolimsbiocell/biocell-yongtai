package com.biolims.experiment.pcr.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: PCR扩增临时表
 * @author lims-platform
 * @date 2016-03-25 10:16:51
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PCR_TASK_TEMP")
@SuppressWarnings("serial")
public class PcrTaskTemp extends EntityDao<PcrTaskTemp> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 患者姓名 */
	private String patientName;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 取样日期 */
	private Date inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 身份证 */
	private String idCard;
	/** 检测方法 */
	private String sequenceFun;
	/** 应出报告日期 */
	private Date reportDate;
	/** 手机号 */
	private String phone;
	/** 任务单 */
	private String orderId;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 区分临床还是科技服务 0临床 1科技服务 */
	private String classify;
	// 样本类型
	private String sampleType;

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 患者姓名
	 */
	@Column(name = "PATIENT_NAME", length = 50)
	public String getPatientName() {
		return this.patientName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 患者姓名
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 取样日期
	 */
	@Column(name = "INSPECT_DATE", length = 50)
	public Date getInspectDate() {
		return this.inspectDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 取样日期
	 */
	public void setInspectDate(Date inspectDate) {
		this.inspectDate = inspectDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 接收日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 接收日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 单位
	 */
	@Column(name = "UNIT", length = 50)
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 单位
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 身份证
	 */
	@Column(name = "ID_CARD", length = 50)
	public String getIdCard() {
		return this.idCard;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 身份证
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测方法
	 */
	@Column(name = "SEQUENCE_FUN", length = 50)
	public String getSequenceFun() {
		return this.sequenceFun;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测方法
	 */
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 应出报告日期
	 */
	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return this.reportDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 应出报告日期
	 */
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 手机号
	 */
	@Column(name = "PHONE", length = 50)
	public String getPhone() {
		return this.phone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 手机号
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 区分临床还是科技服务 0临床 1科技服务
	 */
	@Column(name = "CLASSIFY", length = 50)
	public String getClassify() {
		return this.classify;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 区分临床还是科技服务 0临床 1科技服务
	 */
	public void setClassify(String classify) {
		this.classify = classify;
	}
}