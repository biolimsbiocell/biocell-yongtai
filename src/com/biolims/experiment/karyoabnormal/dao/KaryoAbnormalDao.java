package com.biolims.experiment.karyoabnormal.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.karyo.model.KaryoTypeTaskHarvest;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoget.model.KaryoGetTaskResult;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;

@Repository
@SuppressWarnings("unchecked")
public class KaryoAbnormalDao extends BaseHibernateDao {
	public Map<String, Object> selectKaryoAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoAbnormal where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and isSubmit is null";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoAbnormal> list = new ArrayList<KaryoAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	// 根据样本编号查询
	public List<KaryoAbnormal> setResultById(String code) {
		String hql = "from KaryoAbnormal t where (isSubmit is null or isSubmit='') '"
				+ code + "'";
		List<KaryoAbnormal> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<KaryoAbnormal> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from KaryoAbnormal t where (isSubmit is null or isSubmit='') and id in ("
				+ insql + ")";
		List<KaryoAbnormal> list = this.getSession().createQuery(hql).list();
		return list;
	}
	

}