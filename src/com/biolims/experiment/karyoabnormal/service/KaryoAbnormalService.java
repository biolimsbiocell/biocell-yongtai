package com.biolims.experiment.karyoabnormal.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.karyo.model.KaryoTypeTask;
import com.biolims.experiment.karyo.model.KaryoTypeTaskHarvest;
import com.biolims.experiment.karyoabnormal.dao.KaryoAbnormalDao;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoget.model.KaryoGetTask;
import com.biolims.experiment.karyoget.model.KaryoGetTaskResult;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.karyotyping.model.KaryotypingAgain;
import com.biolims.experiment.karyotyping.model.KaryotypingFirst;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskItem;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.experiment.snpjc.analysis.model.SnpAgainInstance;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemp;
import com.biolims.experiment.snpjc.first.model.SnpFirstInstance;
import com.biolims.experiment.snpjc.sample.model.SnpSample;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.model.Template;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KaryoAbnormalService {
	@Resource
	private KaryoAbnormalDao karyoAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findKaryoAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = karyoAbnormalDao.selectKaryoAbnormalList(mapForQuery,startNum,
				limitNum, dir,sort);
		List<KaryoAbnormal> list = (List<KaryoAbnormal>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoAbnormal(String itemDataJson) throws Exception {
		List<KaryoAbnormal> saveItems = new ArrayList<KaryoAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> map : list) {
			KaryoAbnormal scp = new KaryoAbnormal();
			// 将map信息读入实体类
			scp = (KaryoAbnormal) karyoAbnormalDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);

			saveItems.add(scp);
			karyoAbnormalDao.saveOrUpdate(scp);
			if (scp != null) {
				if (scp.getResult() != null && scp.getIsSubmit() != null &&
					scp.getNextFlowId()!=null && !scp.getResult().equals("") 
					&& !scp.getIsSubmit().equals("") && !scp.getNextFlowId().equals("")){
					if(scp.getIsSubmit().equals("1")){
						if(scp.getResult().equals("1")){
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setState("1");
								commonDAO.saveOrUpdate(st);
								// 入库，改变SampleInfo中原始样本的状态为“待入库”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
								}
								scp.setState("2");
							} else if (scp.getNextFlowId().equals("0012")) {// 暂停
								// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
								}
								
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
								// 终止，改变SampleInfo中原始样本的状态为“实验终止”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
								}
							}else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}
							scp.setState("2");
						}
					}
				}
			}
		}
		karyoAbnormalDao.saveOrUpdateAll(saveItems);
	}
	
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoAbnormal(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoAbnormal scp =  karyoAbnormalDao.get(KaryoAbnormal.class, id);
			karyoAbnormalDao.delete(scp);
		}
	}
	
	/**
	 * 提交样本
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String[] ids) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 获取结果表样本信息
		List<KaryoAbnormal> list = this.karyoAbnormalDao.setResultByIds(ids);
		for (KaryoAbnormal scp : list) {
		if (scp != null) {
			
			KaryoGetTaskTemp k = new KaryoGetTaskTemp();
			k.setCode(scp.getCode());
			k.setSampleCode(scp.getSampleCode());
			k.setProductId(scp.getProductId());
			k.setProductName(scp.getProductName());
			k.setSampleType(scp.getSampleType());
			k.setState("1");
			k.setAcceptDate(scp.getAcceptDate());
			k.setInoculateDate(scp.getInoculateDate());
			k.setPreReapDate(scp.getPreReapDate());
			karyoAbnormalDao.saveOrUpdate(k);
			
	scp.setIsSubmit("1");
	karyoAbnormalDao.saveOrUpdate(scp);
		  }scp.setIsSubmit("1");
		
		}
	}

}
