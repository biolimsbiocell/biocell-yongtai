package com.biolims.experiment.pooling.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.abnormal.model.AbnormalItem;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.pooling.dao.PoolingAbnormalDao;
import com.biolims.experiment.pooling.model.PoolingAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;
import com.biolims.sample.service.SampleInputService;

@Service
@Transactional
public class PoolingAbnormalService {
	@Resource
	private PoolingAbnormalDao poolingAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;

	/**
	 * 
	 * @Title: showPoolingAbnormalTableJson
	 * @Description: 展示异常样本列表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showPoolingAbnormalTableJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return poolingAbnormalDao.showPoolingAbnormalTableJson(start, length,
				query, col, sort);
	}

	/**
	 * @param changeLog 
	 * 
	 * @Title: executeAbnormal
	 * @Description: 保存并执行
	 * @author : 
	 * @date 
	 * @param ids
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson, String changeLog) throws Exception {
		List<PoolingAbnormal> saveItems1 = new ArrayList<PoolingAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		PoolingAbnormal scp = new PoolingAbnormal();
		for (Map<String, Object> map : list) {
			scp = (PoolingAbnormal) poolingAbnormalDao.Map2Bean(map, scp);
			PoolingAbnormal sbi = commonDAO.get(PoolingAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		poolingAbnormalDao.saveOrUpdateAll(saveItems1);
		List<PoolingAbnormal> saveItems = new ArrayList<PoolingAbnormal>();
		for (String id : ids) {
			PoolingAbnormal sbi = commonDAO.get(PoolingAbnormal.class, id);
			sbi.setIsExecute("1");
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi.getCode());
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {// 确认执行
					String next = sbi.getNextFlowId();
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setCode(sbi.getCode());
						st.setSampleCode(sbi.getSampleCode());
						st.setNum(sbi.getSampleNum());
						st.setState("1");
						poolingAbnormalDao.saveOrUpdate(st);
						// 入库，改变SampleInfo中原始样本的状态为“待入库”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
						}
					} else if (next.equals("0012")) {// 暂停
						// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
						}
					} else if (next.equals("0013")) {// 终止
						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					} else if (next.equals("0014")) {// 反馈项目组
					} else {
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sbi.setState("1");
							sampleInputService.copy(o, sbi);
						}
					}
					// 改变状态不显示在异常样本中
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		poolingAbnormalDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String dataJson, String changeLog) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		PoolingAbnormal scp = new PoolingAbnormal();
		for (Map<String, Object> map : list) {
			scp = (PoolingAbnormal) poolingAbnormalDao.Map2Bean(map, scp);
			PoolingAbnormal sbi = commonDAO.get(PoolingAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setResult(scp.getResult());
			sbi.setNote(scp.getNote());
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void feedbackAbnormal(String[] ids) {
		if(ids!=null&&ids.length>0){
			for(String id:ids){
				PoolingAbnormal sa=commonDAO.get(PoolingAbnormal.class, id);
				sa.setState("2");
				AbnormalItem ai=new AbnormalItem();
				ai.setDicSampleType(sa.getDicSampleType());
				ai.setMethod(sa.getMethod());
				ai.setNextFlow(sa.getNextFlow());
				ai.setNextFlowId(sa.getNextFlowId());
				ai.setNote(sa.getNote());
				ai.setOrderId(sa.getOrderId());
				ai.setProductId(sa.getProductId());
				ai.setProductName(sa.getProductName());
				ai.setProject(sa.getProject());
				ai.setSampleCode(sa.getSampleCode());
				ai.setState("1");
				commonDAO.saveOrUpdate(sa);
				commonDAO.saveOrUpdate(ai);
			}
		}
		
	}

}
