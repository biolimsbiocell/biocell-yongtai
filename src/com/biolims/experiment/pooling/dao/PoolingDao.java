package com.biolims.experiment.pooling.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.pooling.model.Pooling;
import com.biolims.experiment.pooling.model.PoolingCos;
import com.biolims.experiment.pooling.model.PoolingInfo;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.pooling.model.PoolingReagent;
import com.biolims.experiment.pooling.model.PoolingTemp;
import com.biolims.experiment.pooling.model.PoolingTemplate;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class PoolingDao extends BaseHibernateDao {

	public Map<String, Object> findPoolingTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Pooling where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Pooling where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<Pooling> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectPoolingTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from PoolingTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<PoolingTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from PoolingTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findPoolingItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PoolingItem where 1=1 and state='1' and pooling.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PoolingItem  where 1=1 and state='1' and pooling.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PoolingItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<PoolingItem> showWellPlate(String id) {
		String hql="from PoolingItem  where 1=1 and state='2' and  pooling.id='"+id+"'";
		List<PoolingItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findPoolingItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PoolingItem where 1=1 and state='2' and pooling.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PoolingItem  where 1=1 and state='2' and pooling.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PoolingItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from PoolingItem where 1=1 and state='2' and pooling.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from PoolingItem where 1=1 and state='2' and pooling.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<PoolingTemplate> showPoolingStepsJson(String id,String code) {

		String countHql = "select count(*) from PoolingTemplate where 1=1 and pooling.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<PoolingTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PoolingTemplate where 1=1 and pooling.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PoolingReagent> showPoolingReagentJson(String id,String code) {
		String countHql = "select count(*) from PoolingReagent where 1=1 and pooling.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<PoolingReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PoolingReagent where 1=1 and pooling.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PoolingCos> showPoolingCosJson(String id,String code) {
		String countHql = "select count(*) from PoolingCos where 1=1 and pooling.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<PoolingCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PoolingCos where 1=1 and pooling.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PoolingTemplate> delTemplateItem(String id) {
		List<PoolingTemplate> list=new ArrayList<PoolingTemplate>();
		String hql = "from PoolingTemplate where 1=1 and pooling.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<PoolingReagent> delReagentItem(String id) {
		List<PoolingReagent> list=new ArrayList<PoolingReagent>();
		String hql = "from PoolingReagent where 1=1 and pooling.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<PoolingCos> delCosItem(String id) {
		List<PoolingCos> list=new ArrayList<PoolingCos>();
		String hql = "from PoolingCos where 1=1 and pooling.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showPoolingResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PoolingInfo where 1=1 and pooling.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PoolingInfo  where 1=1 and pooling.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PoolingInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from PoolingItem where 1=1 and pooling.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<PoolingItem> plateSample(String id, String counts) {
		String hql="from PoolingItem where 1=1 and pooling.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<PoolingItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PoolingItem where 1=1 and pooling.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PoolingItem  where 1=1 and pooling.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PoolingItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<PoolingInfo> findPoolingInfoByCode(String code) {
		String hql="from PoolingInfo where 1=1 and code='"+code+"'";
		List<PoolingInfo> list=new ArrayList<PoolingInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<PoolingInfo> selectAllResultListById(String code) {
		String hql = "from PoolingInfo  where (submit is null or submit='') and pooling.id='"
				+ code + "'";
		List<PoolingInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<PoolingInfo> selectResultListById(String id) {
		String hql = "from PoolingInfo  where pooling.id='"
				+ id + "'";
		List<PoolingInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<PoolingInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from PoolingInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<PoolingInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<PoolingItem> selectPoolingItemList(String scId)
			throws Exception {
		String hql = "from PoolingItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and pooling.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<PoolingItem> list = new ArrayList<PoolingItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from PoolingItem where 1=1 and pooling.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public PoolingInfo getResultByCode(String code) {
			String hql=" from PoolingInfo where 1=1 and code='"+code+"'";
			PoolingInfo spi=(PoolingInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}