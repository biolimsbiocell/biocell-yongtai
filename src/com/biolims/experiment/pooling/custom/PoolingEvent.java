package com.biolims.experiment.pooling.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.pooling.service.PoolingService;

public class PoolingEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		PoolingService mbService = (PoolingService) ctx
				.getBean("poolingService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
