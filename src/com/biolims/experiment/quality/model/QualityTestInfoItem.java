package com.biolims.experiment.quality.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 质量检测结果
 * @author lims-platform
 * @date
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QUALITY_TEST_INFO_Item")
@SuppressWarnings("serial")
public class QualityTestInfoItem extends EntityDao<QualityTestInfoItem> implements java.io.Serializable {

	/** 编码 */
	private String id;
	/** 检测结果名称 */
	private String resultName;
	/** 检测结果参考值 */
	private String resultReferenceValue;
	/** 检测结果 */
	private String result;
	/** 关联子表id */
	private String infoId;
	/** 检测标准 */
	private String testingCriteria;
	
	
	/** 修改人 */
	private User modifier;
	/** 修改时间 */
	private Date modificationTime;
	
	
	/** 关联子表实体 */
	private QualityTestInfo qualityTestInfo;
	
	/** 检测结果(原始值) */
	private String resultOriginal;
	
	/** 修改意见 */
	private String modificationNote;
	
	
	
	
	public String getModificationNote() {
		return modificationNote;
	}

	public void setModificationNote(String modificationNote) {
		this.modificationNote = modificationNote;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MODIFIER")
	public User getModifier() {
		return modifier;
	}

	public void setModifier(User modifier) {
		this.modifier = modifier;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

	public String getResultOriginal() {
		return resultOriginal;
	}

	public void setResultOriginal(String resultOriginal) {
		this.resultOriginal = resultOriginal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QUALITY_TEST_IN_FO")
	public QualityTestInfo getQualityTestInfo() {
		return qualityTestInfo;
	}

	public void setQualityTestInfo(QualityTestInfo qualityTestInfo) {
		this.qualityTestInfo = qualityTestInfo;
	}

	public String getTestingCriteria() {
		return testingCriteria;
	}

	public void setTestingCriteria(String testingCriteria) {
		this.testingCriteria = testingCriteria;
	}

	public String getResultName() {
		return resultName;
	}

	public void setResultName(String resultName) {
		this.resultName = resultName;
	}

	public String getResultReferenceValue() {
		return resultReferenceValue;
	}

	public void setResultReferenceValue(String resultReferenceValue) {
		this.resultReferenceValue = resultReferenceValue;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getInfoId() {
		return infoId;
	}

	public void setInfoId(String infoId) {
		this.infoId = infoId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

}