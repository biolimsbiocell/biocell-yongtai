package com.biolims.experiment.quality.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.Storage;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 待质量检测样本接收明细
 * @author lims-platform
 * @date 2015-11-27 15:41:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QUALITY_TEST_TEMP")
@SuppressWarnings("serial")
public class QualityTestTemp extends EntityDao<QualityTestTemp> implements
		java.io.Serializable,Cloneable {
	
	/** 编码 */
	private String id;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本编号（批号） */
	private String code;
	/** 患者姓名 */
	private String patientName;
	/** 备注 */
	private String note;
	/** 状态ID */
	private String state;
	/** 状态 */
	private String stateName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 浓度 */
	private Double concentration;
	/** 体积 */
	private Double volume;
	/** 任务单id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 临床 1 科研 2 */
	private String classify;
	/** 样本数量 */
	private String sampleNum;
	/** 样本类型 */
	private String sampleType;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;

	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	
	private String parentId;
	/**关联订单*/
	private SampleOrder sampleOrder;
	/**实验步骤*/
	private String experimentalSteps;
	/**实验步骤名称*/
	private String experimentalStepsName;
	/**步骤质检项id*/
	private String zjId;
	/**模块标志*/
	private String mark;
	/**步骤质检名称*/
	private String zjName;
	/**检测项*/
	private SampleDeteyion sampleDeteyion;
	/**质检类型  自主1:过程干细胞		2:产品干细胞		3:产品免疫细胞（改成试剂质检）		4:产品细胞因子
	 * 第三方5:过程干细胞		6:产品干细胞		7:产品免疫细胞（改成试剂质检）		8:产品细胞因子*/
	private String cellType;
	/**检测项*/
	private String testItem;
	
	/**批次id(试剂)*/
	private String serial;
	/**试剂主数据*/
	private Storage storage;
	
	/**批次（细胞）*/
	private String batch;
	/** 细胞生产，样本信息子表id  */
	private String cellSampleTaleId;
	
	/**质检提交时间*/
	private Date qualitySubmitTime;
	
	/**单位*/
	private String sampleNumUnit;
	
	/**样本编号*/
	private String SampleNumber;
	//条码确认 1 确认
	private String confirm;
	
	
	
	
	public String getSampleNumber() {
		return SampleNumber;
	}

	public void setSampleNumber(String sampleNumber) {
		SampleNumber = sampleNumber;
	}

	public String getSampleNumUnit() {
		return sampleNumUnit;
	}

	public void setSampleNumUnit(String sampleNumUnit) {
		this.sampleNumUnit = sampleNumUnit;
	}

	public String getExperimentalStepsName() {
		return experimentalStepsName;
	}

	public void setExperimentalStepsName(String experimentalStepsName) {
		this.experimentalStepsName = experimentalStepsName;
	}

	public Date getQualitySubmitTime() {
		return qualitySubmitTime;
	}

	public void setQualitySubmitTime(Date qualitySubmitTime) {
		this.qualitySubmitTime = qualitySubmitTime;
	}

	public String getCellSampleTaleId() {
		return cellSampleTaleId;
	}

	public void setCellSampleTaleId(String cellSampleTaleId) {
		this.cellSampleTaleId = cellSampleTaleId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "t_STORAGE")
	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	
	public String getZjName() {
		return zjName;
	}

	public void setZjName(String zjName) {
		this.zjName = zjName;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getZjId() {
		return zjId;
	}

	public void setZjId(String zjId) {
		this.zjId = zjId;
	}

	
	



	public String getTestItem() {
		return testItem;
	}

	public void setTestItem(String testItem) {
		this.testItem = testItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETECYION")
	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}

	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}


	public String getCellType() {
		return cellType;
	}

	public void setCellType(String cellType) {
		this.cellType = cellType;
	}
	

	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/** 
	 * @return orderCode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getOrderCode() {
		return orderCode;
	}

	/**
	 * @param orderCode the orderCode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getExperimentalSteps() {
		return experimentalSteps;
	}

	public void setExperimentalSteps(String experimentalSteps) {
		this.experimentalSteps = experimentalSteps;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	
}