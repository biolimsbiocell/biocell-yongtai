package com.biolims.experiment.quality.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 质控分析
 * @author lims-platform
 * @date 2015-11-24 11:57:10
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QUALITY_ANALYZE")
@SuppressWarnings("serial")
public class QualityAnalyze extends EntityDao<QualityAnalyze> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**审核人*/
	private User receiver;
	/**审核时间*/
	private Date receiverDate;
	/**结果判定*/
	private String resultDecide;
	/**分析原因*/
	private String analyzeReason;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECEIVER")
	public User getReceiver(){
		return this.receiver;
	}
	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setReceiver(User receiver){
		this.receiver = receiver;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审核时间
	 */
	@Column(name ="RECEIVER_DATE", length = 50)
	public Date getReceiverDate(){
		return this.receiverDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审核时间
	 */
	public void setReceiverDate(Date receiverDate){
		this.receiverDate = receiverDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="RESULT_DECIDE", length = 50)
	public String getResultDecide(){
		return this.resultDecide;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setResultDecide(String resultDecide){
		this.resultDecide = resultDecide;
	}
	/**
	 *方法: 取得String
	 *@return: String  分析原因
	 */
	@Column(name ="ANALYZE_REASON", length = 50)
	public String getAnalyzeReason(){
		return this.analyzeReason;
	}
	/**
	 *方法: 设置String
	 *@param: String  分析原因
	 */
	public void setAnalyzeReason(String analyzeReason){
		this.analyzeReason = analyzeReason;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 150)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
}