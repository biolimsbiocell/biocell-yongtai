package com.biolims.experiment.quality.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.Storage;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 质量检测结果
 * @author lims-platform
 * @date 
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QUALITY_TEST_INFO")
@SuppressWarnings("serial")
public class QualityTestInfo extends EntityDao<QualityTestInfo> implements
		java.io.Serializable {
	
	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private QualityTest qualityTest;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 是否提交*/
	private String submit;
	/** 患者姓名 */
	private String patientName;
	/** 浓度 */
	private Double concentration;
	/** 任务单Id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 临床 1 科研 2*/
	private String classify;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 样本类型 */
	private String sampleType;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/** 应出报告日期 */
	private Date reportDate;
	/** 单位组 */
	private DicType unitGroup;
    /**状态*/
	private String state;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/**关联订单*/
	private SampleOrder sampleOrder;
	/**PBMC分离: A（Q01A01）表型检查*/
	private String qab;
	/**PBMC分离: A（Q01A02）无菌1*/
	private String qaw1;
	/** PBMC分离:B（Q01B01）无菌2*/
	private String qbw2;
	/** PBMC分离:B Q01B02）支原体1*/
	private String qbzy1;
	/**PBMC分离:B（Q01B03）支原体2*/
	private String qbzy2;
	/**PBMC分离:C（Q01C01）无菌1*/
	private String qcw1;
	/**PBMC分离:C（Q01C02）数量*/
	private String qcNum;
	/**PBMC分离:C（Q01C03）活率*/
	private String qchl;
	/**装袋培养:（Q0201）细胞密度*/
	private String qmd;
	/**装袋培养:（Q0202）无菌1*/
	private String qw1;
	/**分袋培养:（Q0301）无菌1 */
	private String qw3;
	/**分袋培养1（Q0401）无菌1*/
	private String qw4;
	/**分袋培养2（Q0401）无菌1*/
	private String qw42;
	/**（Q0502）表型检测  */
	private String q5;
	/**（Q0601）细胞数量*/
	private String q6Num;
	/**（Q0602）活率*/
	private String q6hl;
	/**（（Q0603）无菌2*/
	private String q6w;
	/**（Q0604）支原体1*/
	private String qzy1;
	/**（Q0605）支原体2*/
	private String qzy2;
	/**（Q0606）细菌内毒素  */
	private String qds  ;
	/**检测项*/
	private SampleDeteyion sampleDeteyion;
	/**明细检测项对应结果的字段值*/
	private String testItemData;
	/**来源模块标识*/
	private String mark;
	/**质检名称*/
	private String zjName;
	
	/**内参 */
	private String internalReference;
	/**库存主数据*/
	private Storage storagea;
	/**批次id(试剂)*/
	private String serial;
	/**批次(细胞)*/
	private String batch;
	/**细胞生产，样本信息子表id*/
	private String cellSampleTableId;
	/**质检提交时间*/
	private Date qualitySubmitTime;
	/**质检完成时间*/
	private Date qualityFinishTime;
	/**步骤名称*/
	private String experimentalStepsName;
	/**检测单位*/
	private String sampleNumUnit;
	
	/**样本编号*/
	private String SampleNumber;
	
	
	
	
	
	public String getSampleNumber() {
		return SampleNumber;
	}

	public void setSampleNumber(String sampleNumber) {
		SampleNumber = sampleNumber;
	}

	public String getSampleNumUnit() {
		return sampleNumUnit;
	}

	public void setSampleNumUnit(String sampleNumUnit) {
		this.sampleNumUnit = sampleNumUnit;
	}

	public String getExperimentalStepsName() {
		return experimentalStepsName;
	}

	public void setExperimentalStepsName(String experimentalStepsName) {
		this.experimentalStepsName = experimentalStepsName;
	}

	public Date getQualitySubmitTime() {
		return qualitySubmitTime;
	}

	public void setQualitySubmitTime(Date qualitySubmitTime) {
		this.qualitySubmitTime = qualitySubmitTime;
	}

	public Date getQualityFinishTime() {
		return qualityFinishTime;
	}

	public void setQualityFinishTime(Date qualityFinishTime) {
		this.qualityFinishTime = qualityFinishTime;
	}

	public String getCellSampleTableId() {
		return cellSampleTableId;
	}

	public void setCellSampleTableId(String cellSampleTableId) {
		this.cellSampleTableId = cellSampleTableId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_STORAGE")
	public Storage getStoragea() {
		return storagea;
	}

	public void setStoragea(Storage storagea) {
		this.storagea = storagea;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getZjName() {
		return zjName;
	}

	public void setZjName(String zjName) {
		this.zjName = zjName;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getTestItemData() {
		return testItemData;
	}

	public void setTestItemData(String testItemData) {
		this.testItemData = testItemData;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETECYION")
	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}

	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}
	/**实验步骤*/
	private String experimentalSteps;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血浆编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血浆编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 36)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得DicUnit
	 * 
	 * @return: DicUnit 单位
	 */
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	/**
	 * 方法: 取得QualityTest
	 * 
	 * @return: QualityTest 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QUALITY_TEST")
	public QualityTest getQualityTest() {
		return this.qualityTest;
	}

	/**
	 * 方法: 设置QualityTest
	 * 
	 * @param: QualityTest 相关主表
	 */
	public void setQualityTest(QualityTest qualityTest) {
		this.qualityTest = qualityTest;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getState() {
		return state;
	}

	public String getQab() {
		return qab;
	}

	public String getQaw1() {
		return qaw1;
	}

	public String getQbw2() {
		return qbw2;
	}

	public String getQbzy1() {
		return qbzy1;
	}

	public String getQbzy2() {
		return qbzy2;
	}

	public String getQcw1() {
		return qcw1;
	}

	public String getQcNum() {
		return qcNum;
	}

	public String getQchl() {
		return qchl;
	}

	public String getQmd() {
		return qmd;
	}

	public String getQw1() {
		return qw1;
	}

	public String getQw3() {
		return qw3;
	}

	public String getQw4() {
		return qw4;
	}

	public String getQ5() {
		return q5;
	}

	public String getQ6Num() {
		return q6Num;
	}

	public String getQ6hl() {
		return q6hl;
	}

	public String getQ6w() {
		return q6w;
	}

	public String getQzy1() {
		return qzy1;
	}

	public String getQzy2() {
		return qzy2;
	}

	public String getQds() {
		return qds;
	}

	public void setQab(String qab) {
		this.qab = qab;
	}

	public void setQaw1(String qaw1) {
		this.qaw1 = qaw1;
	}

	public void setQbw2(String qbw2) {
		this.qbw2 = qbw2;
	}

	public void setQbzy1(String qbzy1) {
		this.qbzy1 = qbzy1;
	}

	public void setQbzy2(String qbzy2) {
		this.qbzy2 = qbzy2;
	}

	public void setQcw1(String qcw1) {
		this.qcw1 = qcw1;
	}

	public void setQcNum(String qcNum) {
		this.qcNum = qcNum;
	}

	public void setQchl(String qchl) {
		this.qchl = qchl;
	}

	public void setQmd(String qmd) {
		this.qmd = qmd;
	}

	public void setQw1(String qw1) {
		this.qw1 = qw1;
	}

	public void setQw3(String qw3) {
		this.qw3 = qw3;
	}

	public void setQw4(String qw4) {
		this.qw4 = qw4;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	public void setQ6Num(String q6Num) {
		this.q6Num = q6Num;
	}

	public void setQ6hl(String q6hl) {
		this.q6hl = q6hl;
	}

	public void setQ6w(String q6w) {
		this.q6w = q6w;
	}

	public void setQzy1(String qzy1) {
		this.qzy1 = qzy1;
	}

	public void setQzy2(String qzy2) {
		this.qzy2 = qzy2;
	}

	public void setQds(String qds) {
		this.qds = qds;
	}

	public String getQw42() {
		return qw42;
	}

	public void setQw42(String qw42) {
		this.qw42 = qw42;
	}

	public String getExperimentalSteps() {
		return experimentalSteps;
	}

	public void setExperimentalSteps(String experimentalSteps) {
		this.experimentalSteps = experimentalSteps;
	}

	/**
	 * @return the internalReference
	 */
	public String getInternalReference() {
		return internalReference;
	}

	/**
	 * @param internalReference the internalReference to set
	 */
	public void setInternalReference(String internalReference) {
		this.internalReference = internalReference;
	}

}