package com.biolims.experiment.quality.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 质控计划
 * @author lims-platform
 * @date 2015-11-24 11:57:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QUALITY_PLAN")
@SuppressWarnings("serial")
public class QualityPlan extends EntityDao<QualityPlan> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**计划日期*/
	private Date planDate;
	/**工作流状态*/
	private String workState;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  计划日期
	 */
	@Column(name ="PLAN_DATE", length = 50)
	public Date getPlanDate(){
		return this.planDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  计划日期
	 */
	public void setPlanDate(Date planDate){
		this.planDate = planDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="WORK_STATE", length = 50)
	public String getWorkState(){
		return this.workState;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setWorkState(String workState){
		this.workState = workState;
	}
}