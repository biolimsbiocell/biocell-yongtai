package com.biolims.experiment.quality.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleOrder;

/**
 * @Title: Model
 * @Description: 生产质检结果管理
 * @author lims-platform
 * @date 2019-11-11 11:30
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QUALITY_TEST_RESULT_MANAGE")
@SuppressWarnings("serial")
public class QualityTestResultManage extends EntityDao<QualityTestResultManage> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	
	
	/** 提交人 */
	private User createUser;
	/** 提交时间 */
	private Date createDate;
	/** 审核人 */
	private User confirmUser;
	/** 完成时间 */
	private Date confirmDate;
	/** 工作流状态 */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 类型 */
	private String type;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/** 订单编号 */
	private SampleOrder sampleOrder;
	
	/** 订单状态 */
	private String sampleOrderState;

	/** 第二审核人 */
	private User confirmUserTwo;
	/** 第二完成时间 */
	private Date confirmDateTwo;
	
	/** 第二提交人 */
	private User createUserTwo;
	/** 第二提交时间 */
	private Date createDateTwo;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER_TWO")
	public User getCreateUserTwo() {
		return createUserTwo;
	}

	public void setCreateUserTwo(User createUserTwo) {
		this.createUserTwo = createUserTwo;
	}

	public Date getCreateDateTwo() {
		return createDateTwo;
	}

	public void setCreateDateTwo(Date createDateTwo) {
		this.createDateTwo = createDateTwo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER_TWO")
	public User getConfirmUserTwo() {
		return confirmUserTwo;
	}

	public void setConfirmUserTwo(User confirmUserTwo) {
		this.confirmUserTwo = confirmUserTwo;
	}

	public Date getConfirmDateTwo() {
		return confirmDateTwo;
	}

	public void setConfirmDateTwo(Date confirmDateTwo) {
		this.confirmDateTwo = confirmDateTwo;
	}

	public String getSampleOrderState() {
		return sampleOrderState;
	}

	public void setSampleOrderState(String sampleOrderState) {
		this.sampleOrderState = sampleOrderState;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

//	/**
//	 * 方法: 取得Date
//	 * 
//	 * @return: Date 下达时间
//	 */
//	@Column(name = "CREATE_DATE", length = 255)
//	public String getCreateDate() {
//		return this.createDate;
//	}
//
//	/**
//	 * 方法: 设置Date
//	 * 
//	 * @param: Date
//	 *             下达时间
//	 */
//	public void setCreateDate(String createDate) {
//		this.createDate = createDate;
//	}

	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId
	 *            the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName
	 *            the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
}