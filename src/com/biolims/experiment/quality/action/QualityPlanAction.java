﻿
package com.biolims.experiment.quality.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.quality.model.QualityPlan;
import com.biolims.experiment.quality.service.QualityPlanService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/experiment/quality/qualityPlan")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityPlanAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private QualityPlanService qualityPlanService;
	private QualityPlan qualityPlan = new QualityPlan();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showQualityPlanList")
	public String showQualityPlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityPlan.jsp");
	}

	@Action(value = "showQualityPlanListJson")
	public void showQualityPlanListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityPlanService.findQualityPlanList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QualityPlan> list = (List<QualityPlan>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("planDate", "yyyy-MM-dd");
		map.put("workState", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "qualityPlanSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQualityPlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityPlanDialog.jsp");
	}

	@Action(value = "showDialogQualityPlanListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQualityPlanListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityPlanService.findQualityPlanList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QualityPlan> list = (List<QualityPlan>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("planDate", "yyyy-MM-dd");
		map.put("workState", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editQualityPlan")
	public String editQualityPlan() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qualityPlan = qualityPlanService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qualityPlan");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			qualityPlan.setCreateUser(user);
//			qualityPlan.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityPlanEdit.jsp");
	}

	@Action(value = "copyQualityPlan")
	public String copyQualityPlan() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qualityPlan = qualityPlanService.get(id);
		qualityPlan.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/quality/qualityPlanEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = qualityPlan.getId();
		if(id!=null&&id.equals("")){
			qualityPlan.setId(null);
		}
		Map aMap = new HashMap();
		qualityPlanService.save(qualityPlan,aMap);
		return redirect("/experiment/quality/qualityPlan/editQualityPlan.action?id=" + qualityPlan.getId());

	}

	@Action(value = "viewQualityPlan")
	public String toViewQualityPlan() throws Exception {
		String id = getParameterFromRequest("id");
		qualityPlan = qualityPlanService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityPlanEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityPlanService getQualityPlanService() {
		return qualityPlanService;
	}

	public void setQualityPlanService(QualityPlanService qualityPlanService) {
		this.qualityPlanService = qualityPlanService;
	}

	public QualityPlan getQualityPlan() {
		return qualityPlan;
	}

	public void setQualityPlan(QualityPlan qualityPlan) {
		this.qualityPlan = qualityPlan;
	}


}
