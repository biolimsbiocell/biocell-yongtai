﻿package com.biolims.experiment.quality.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.quality.model.QualityTestAbnormal;
import com.biolims.experiment.quality.service.QualityTestAbnormalService;
import com.biolims.experiment.quality.service.QualityTestService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.common.PushData;
@Namespace("/experiment/quality/qualityTestAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityTestAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";// 240102
	@Autowired
	private QualityTestAbnormalService qualityTestAbnormalService;
	@Resource
	private FileInfoService fileInfoService;
	@Autowired
	private QualityTestService qualityTestService;
	
	/**
	 * 
	 * @Title: showQualityTestAbnormalTable
	 * @Description: 展示异常样本列表
	 * @author 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showQualityTestAbnormalTable")
	public String showQualityTestAbnormalTable() throws Exception {
		String cellType = getParameterFromRequest("cellType");
		if(cellType.equals("1")) {//产品干细胞
			rightsId = "211010103";
		}else if(cellType.equals("2")) {
			rightsId = "211010203";
		}else if(cellType.equals("3")) {
			rightsId = "211010303";
		}else if(cellType.equals("4")) {
			rightsId = "211010403";
		}else if(cellType.equals("5")) {//产品干细胞
			rightsId = "211020103";
		}else if(cellType.equals("6")) {
			rightsId = "211020203";
		}else if(cellType.equals("7")) {
			rightsId = "211020303";
		}else if(cellType.equals("8")) {
			rightsId = "211020403";
		}
		putObjToContext("cellType", cellType);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestAbnormal.jsp");
	}

	@Action(value = "showQualityTestAbnormalTableJson")
	public void showQualityTestAbnormalTableJson() throws Exception {
		String cellType = getParameterFromRequest("cellType");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = qualityTestAbnormalService
				.showQualityTestAbnormalTableJson(cellType, start, length, query, col, sort);
		List<QualityTestAbnormal> list = (List<QualityTestAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("code", "");
		map.put("sampleOrder-ccoi", "");
		map.put("sampleOrder-filtrateCode", "");
		map.put("sampleOrder-barcode", "");
		map.put("sampleType", "");
		map.put("experimentalStepsName", "");
		map.put("unit", "");
		map.put("sampleNum", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("isExecute", "");
		map.put("note", "");
		map.put("method", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("result", "");
//		map.put("volume", "");
		map.put("patient", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("reason", "");
//		map.put("concentration", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("orderId", "");
		map.put("classify", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
     /**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年5月3日下午2:13:02
	 * @throws Exception
	 *             void
	 * 修改纪录：dwb 增加日志功能 
	 * 2018-05-11 17:56:35
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String dataJson = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			qualityTestAbnormalService.save(dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: feedbackAbnormal  
	 * @Description: 反馈至项目组  
	 * @author : shengwei.wang
	 * @date 2018年5月4日上午10:01:32
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="feedbackAbnormal")
	public void feedbackAbnormal() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			qualityTestAbnormalService.feedbackAbnormal(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));	
	}
	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description:执行异常列表
	 * @author 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "executeAbnormal")
	public void executeAbnormal() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String dataJson = getParameterFromRequest("dataJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			qualityTestAbnormalService.executeAbnormal(ids, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityTestAbnormalService getQualityTestAbnormalService() {
		return qualityTestAbnormalService;
	}

	public void setQualityTestAbnormalService(
			QualityTestAbnormalService qualityTestAbnormalService) {
		this.qualityTestAbnormalService =qualityTestAbnormalService;
	}

}
