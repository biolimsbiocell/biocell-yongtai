﻿
package com.biolims.experiment.quality.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.quality.model.QualityAnalyze;
import com.biolims.experiment.quality.service.QualityAnalyzeService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/experiment/quality/qualityAnalyze")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityAnalyzeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private QualityAnalyzeService qualityAnalyzeService;
	private QualityAnalyze qualityAnalyze = new QualityAnalyze();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showQualityAnalyzeList")
	public String showQualityAnalyzeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityAnalyze.jsp");
	}

	@Action(value = "showQualityAnalyzeListJson")
	public void showQualityAnalyzeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityAnalyzeService.findQualityAnalyzeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QualityAnalyze> list = (List<QualityAnalyze>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("receiver-id", "");
		map.put("receiver-name", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("resultDecide", "");
		map.put("analyzeReason", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "qualityAnalyzeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQualityAnalyzeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityAnalyzeDialog.jsp");
	}

	@Action(value = "showDialogQualityAnalyzeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQualityAnalyzeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityAnalyzeService.findQualityAnalyzeList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QualityAnalyze> list = (List<QualityAnalyze>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("receiver-id", "");
		map.put("receiver-name", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("resultDecide", "");
		map.put("analyzeReason", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editQualityAnalyze")
	public String editQualityAnalyze() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qualityAnalyze = qualityAnalyzeService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qualityAnalyze");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qualityAnalyze.setReceiver(user);
			qualityAnalyze.setReceiverDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityAnalyzeEdit.jsp");
	}

	@Action(value = "copyQualityAnalyze")
	public String copyQualityAnalyze() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qualityAnalyze = qualityAnalyzeService.get(id);
		qualityAnalyze.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/quality/qualityAnalyzeEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qualityAnalyze.getId();
		if(id!=null&&id.equals("")){
			qualityAnalyze.setId(null);
		}
		Map aMap = new HashMap();
		qualityAnalyzeService.save(qualityAnalyze,aMap);
		return redirect("/experiment/quality/qualityAnalyze/editQualityAnalyze.action?id=" + qualityAnalyze.getId());

	}

	@Action(value = "viewQualityAnalyze")
	public String toViewQualityAnalyze() throws Exception {
		String id = getParameterFromRequest("id");
		qualityAnalyze = qualityAnalyzeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityAnalyzeEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityAnalyzeService getQualityAnalyzeService() {
		return qualityAnalyzeService;
	}

	public void setQualityAnalyzeService(QualityAnalyzeService qualityAnalyzeService) {
		this.qualityAnalyzeService = qualityAnalyzeService;
	}

	public QualityAnalyze getQualityAnalyze() {
		return qualityAnalyze;
	}

	public void setQualityAnalyze(QualityAnalyze qualityAnalyze) {
		this.qualityAnalyze = qualityAnalyze;
	}


}
