package com.biolims.experiment.quality.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.service.ApplicationTypeActionService;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.quality.dao.QualityTestDao;
import com.biolims.experiment.quality.dao.QualityTestResultManageDao;
import com.biolims.experiment.quality.model.QualityTest;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.model.QualityTestInfoItem;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.quality.model.QualityTestResultManage;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.quality.service.QualityTestResultManageService;
import com.biolims.experiment.quality.service.QualityTestService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.detecyion.model.SampleDeteyionItem;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/quality/qualityTestResultManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityTestResultManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2110901";
	private QualityTestResultManage qualityTestResultManage = new QualityTestResultManage();
	@Autowired
	private QualityTestResultManageService qualityTestResultManageService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private QualityTestResultManageDao qualityTestResultManageDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	
	
	/**
	 * 展示订单的基本信息，及生产到哪一步
	 * @throws Exception 
	 */
	@Action(value = "showQualityTestResulManagetList")
	public String showQualityTestResultManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestResultManage.jsp");
	}

	@Action(value = "showQualityTestResultManageListJson")
	public void showQualityTestResultManageListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");

		try {
			Map<String, Object> result = qualityTestResultManageService.findResultManage(start, length, query,
					col, sort);
			List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("sampleOrder-barcode", "");
			map.put("sampleOrder-batchStateName", "");
			map.put("sampleOrderState", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmUser-name", "");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("stateName", "");
			
			map.put("createUserTwo-name", "");
			map.put("createDateTwo", "yyyy-MM-dd");
			map.put("confirmUserTwo-name", "");
			map.put("confirmDateTwo", "yyyy-MM-dd");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 * @Description: 编辑查看质检结果
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editQualityTestResultManage")
	public String editDustParticle() throws Exception {
		rightsId = "2110901";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			qualityTestResultManage = commonService.get(QualityTestResultManage.class, id);
			toState(qualityTestResultManage.getState());
		}else{
			
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/quality/editQualityTestResultManage.jsp");
	}
	@Action(value = "viewQualityTestResultManage")
	public String viewQualityTestResultManage() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			qualityTestResultManage = commonService.get(QualityTestResultManage.class, id);
			toState(qualityTestResultManage.getState());
		}else{
			
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/quality/editQualityTestResultManage.jsp");
	}
	
	// 浮游菌子表 列表数据
	@Action(value = "showQualityTestResultManageJsonList")
	public void showQualityTestResultManageJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = qualityTestResultManageService.showQualityTestResultItemTableJson(id, start, length,
					query, col, sort);
			List<QualityTestInfoItem> list = (List<QualityTestInfoItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("resultName", "");
			map.put("resultReferenceValue", "");
			map.put("result", "");
			map.put("infoId", "");
			map.put("testingCriteria", "");
			
			map.put("resultOriginal", "");
			map.put("modifier-name", "");
			map.put("modificationTime", "yyyy-MM-dd HH:mm:ss");
			
			
			map.put("qualityTestInfo-sampleOrder-id", "");
			map.put("qualityTestInfo-batch", "");
			map.put("qualityTestInfo-sampleOrder-ccoi", "");
			map.put("qualityTestInfo-sampleOrder-name", "");
			map.put("qualityTestInfo-sampleOrder-filtrateCode", "");
			map.put("qualityTestInfo-SampleNumber", "");
			map.put("qualityTestInfo-sampleCode", "");
			map.put("qualityTestInfo-concentration", "");
			map.put("qualityTestInfo-internalReference", "");
			map.put("qualityTestInfo-code", "");
			map.put("qualityTestInfo-cellSampleTableId", "");
			map.put("qualityTestInfo-sampleCode", "");
			map.put("qualityTestInfo-concentration", "");
			map.put("qualityTestInfo-productId", "");
			map.put("qualityTestInfo-productName", "");
			map.put("qualityTestInfo-sampleType", "");
			map.put("qualityTestInfo-experimentalSteps", "");
			map.put("qualityTestInfo-experimentalStepsName", "");
			map.put("qualityTestInfo-sampleDeteyion-id", "");
			map.put("qualityTestInfo-sampleDeteyion-name", "");
			map.put("qualityTestInfo-sampleNumUnit", "");
			map.put("qualityTestInfo-orderId", "");
			map.put("qualityTestInfo-result", "");
			map.put("qualityTestInfo-qualitySubmitTime", "");
			map.put("qualityTestInfo-qualityFinishTime", "");
			map.put("qualityTestInfo-reportDate", "");
			map.put("qualityTestInfo-method", "");
			map.put("qualityTestInfo-note", "");
			map.put("qualityTestInfo-mark", "");
			
			map.put("modificationNote", "");
			
//			CleanAreaBacteriaItem cleanAreaMicrobe = new CleanAreaBacteriaItem();
//			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMicrobe);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveItem")
	public String saveItem() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String dataJson = getParameterFromRequest("documentInfoItemJson");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String id = qualityTestResultManage.getId();
		qualityTestResultManageService.saveQualityTestResultManageItem(qualityTestResultManage, dataJson, changeLog, changeLogItem);
		return redirect("/experiment/quality/qualityTestResultManage/editQualityTestResultManage.action?id=" + qualityTestResultManage.getId()+"&bpmTaskId="+bpmTaskId);
	}
	
	@Action(value = "submitTask")
	public void submitTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String zt = getParameterFromRequest("zt");
		String text = "";
		try {
			qualityTestResultManageService.submitTask(id,zt);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		map.put("text", text);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "tongguoTask")
	public void tongguoTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String zt = getParameterFromRequest("zt");
		String text = "";
		try {
			qualityTestResultManageService.tongguoTask(id,zt);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		map.put("text", text);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "tuihuiTask")
	public void tuihuiTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String zt = getParameterFromRequest("zt");
		String text = "";
		try {
			qualityTestResultManageService.tuihuiTask(id,zt);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		map.put("text", text);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
//
//	/**
//	 * @throws UnsupportedEncodingException
//	 *             展示对应质检项的结果 @Title: showQualityTestResultListForzj @Description:
//	 *             TODO @param @return @return String @author 孙灵达 @date
//	 *             2018年9月13日 @throws
//	 */
//	@Action(value = "showQualityTestResultListAllForzj")
//	public String showQualityTestResultListAllForzj() throws UnsupportedEncodingException {
//		String testId = getParameterFromRequest("testId");
//		putObjToContext("testId", testId);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestResultAllForzj.jsp");
//	}
//
//	@Action(value = "showQualityTestResultListAllJsonForzj")
//	public void showQualityTestResultListAllJsonForzj() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//
//		String testId = getParameterFromRequest("testId");
//		try {
//			Map<String, Object> result = qualityTestService.findResultAllForzj(testId, start, length, query, col, sort);
//			List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("sampleOrder-id", "");
//			map.put("sampleCode", "");
//			map.put("code", "");
//			map.put("productName", "");
//			map.put("sampleType", "");
//			map.put("dicSampleType-id", "");
//			map.put("dicSampleType-name", "");
//			map.put("result", "");
//			map.put("qab", "");
//			map.put("qaw1", "");
//			map.put("qbw2", "");
//			map.put("qbzy1", "");
//			map.put("qbzy2", "");
//			map.put("qcw1", "");
//			map.put("qcNum", "");
//			map.put("qchl", "");
//			map.put("qmd", "");
//			map.put("qw1", "");
//			map.put("qw3", "");
//			map.put("qw4", "");
//			map.put("qw42", "");
//			map.put("q5", "");
//			map.put("q6Num", "");
//			map.put("q6hl", "");
//			map.put("q6w", "");
//			map.put("qzy1", "");
//			map.put("qzy2", "");
//			map.put("qds", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	@Action(value = "findQualityTestItemTestItemText")
//	public void findQualityTestItemTestItemText() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String id = getParameterFromRequest("id");
//		String text = "";
//		try {
//			List<QualityTestItem> list = qualityTestService.findQualityTestItemTestItemText(id);
//			if (list.size() > 0) {
//				text = list.get(0).getTestItem();
//			}
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		map.put("text", text);
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: showQualityTestList @Description:展示主表 @author @date @return @throws
//	 *         Exception String @throws
//	 */
//	@Action(value = "showQualityTestTable")
//	public String showQualityTestTable() throws Exception {
//		String cellType = getParameterFromRequest("cellType");
//		if (cellType.equals("1")) {// 产品干细胞
//			rightsId = "211010102";
//		} else if (cellType.equals("2")) {
//			rightsId = "211010202";
//		} else if (cellType.equals("3")) {
//			rightsId = "211010302";
//		} else if (cellType.equals("4")) {
//			rightsId = "211010402";
//		} else if (cellType.equals("5")) {// 产品干细胞
//			rightsId = "211020102";
//		} else if (cellType.equals("6")) {
//			rightsId = "211020202";
//		} else if (cellType.equals("7")) {
//			rightsId = "211020302";
//		} else if (cellType.equals("8")) {
//			rightsId = "211020402";
//		}
//		putObjToContext("cellType", cellType);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTest.jsp");
//	}
//
//	@Action(value = "showQualityTestTableJson")
//	public void showQualityTestTableJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		String cellType = getParameterFromRequest("cellType");
//		try {
//			Map<String, Object> result = qualityTestService.findQualityTestTable(cellType, start, length, query, col,
//					sort);
//			List<QualityTest> list = (List<QualityTest>) result.get("list");
//			if(list.size()>0) {
//				for(QualityTest qt:list) {
//					List<QualityTestItem> qtis = commonService.get(QualityTestItem.class, "qualityTest.id", qt.getId());
//					if(qtis.size()>0) {
//						qt.setSampleNum(qtis.size());
//					}else {
//						qt.setSampleNum(0);
//					}
//				}
//			}
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("name", "");
//			map.put("sampleNum", "");
//			map.put("createUser-name", "");
//			map.put("createDate", "yyyy-MM-dd");
//			map.put("confirmDate", "yyyy-MM-dd");
//			map.put("template-name", "");
//			map.put("testUserOneName", "");
//			map.put("stateName", "");
//			map.put("scopeName", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 
//	 * @Title: editQualityTest @Description: 新建实验单 @author @date @return @throws
//	 *         Exception String @throws
//	 */
//	@Action(value = "editQualityTest")
//	public String editQualityTest() throws Exception {
//		String cellType = getParameterFromRequest("cellType");
//		if (cellType.equals("1")) {// 自主检测
//			rightsId = "211010101";
//		} else if (cellType.equals("2")) {
//			rightsId = "211010201";
//		} else if (cellType.equals("3")) {
//			rightsId = "211010301";
//		} else if (cellType.equals("4")) {
//			rightsId = "211010401";
//		} else if (cellType.equals("5")) {// 第三方检测
//			rightsId = "211020101";
//		} else if (cellType.equals("6")) {
//			rightsId = "211020201";
//		} else if (cellType.equals("7")) {
//			rightsId = "211020301";
//		} else if (cellType.equals("8")) {
//			rightsId = "211020401";
//		}
//		String id = getParameterFromRequest("id");
//		if (id != null && !id.equals("")) {
//			qualityTest = qualityTestService.get(id);
//			if(qualityTest!=null) {
//				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//				String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
//				putObjToContext("taskName", tName);
//				if (qualityTest.getMaxNum() == null) {
//					qualityTest.setMaxNum(0);
//				}
//				String bpmTaskId = getParameterFromRequest("bpmTaskId");
//				putObjToContext("bpmTaskId", bpmTaskId);
//			}else {
//				qualityTest = new QualityTest();
//				qualityTest.setId("NEW");
//				qualityTest.setCellType(cellType);
//				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//				qualityTest.setCreateUser(user);
//				if (qualityTest.getMaxNum() == null) {
//					qualityTest.setMaxNum(0);
//				}
//				Date date = new Date();
//				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//				String stime = format.format(date);
//				qualityTest.setCreateDate(stime);
//				qualityTest.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//				qualityTest.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//				String bpmTaskId = getParameterFromRequest("bpmTaskId");
//				putObjToContext("bpmTaskId", bpmTaskId);
//			}
//		} else {
//			qualityTest.setId("NEW");
//			qualityTest.setCellType(cellType);
//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			qualityTest.setCreateUser(user);
//			if (qualityTest.getMaxNum() == null) {
//				qualityTest.setMaxNum(0);
//			}
//			Date date = new Date();
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			String stime = format.format(date);
//			qualityTest.setCreateDate(stime);
//			qualityTest.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//			qualityTest.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			String bpmTaskId = getParameterFromRequest("bpmTaskId");
//			putObjToContext("bpmTaskId", bpmTaskId);
//		}
//		List<Template> templateList = templateService.showDialogTemplateTableJson("QualityTest", cellType);
//		List<Template> selTemplate = new ArrayList<Template>();
//		List<UserGroupUser> userList = new ArrayList<UserGroupUser>();
//		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
//		for (int j = 0; j < templateList.size(); j++) {
//			if (templateList.get(j).getAcceptUser() != null) {
//				List<UserGroupUser> userTempList = (List<UserGroupUser>) userGroupUserService
//						.getUserGroupUserBygroupId(templateList.get(j).getAcceptUser().getId()).get("list");
//				for (UserGroupUser ugu : userTempList) {
//					if (!userList.contains(ugu)) {
//						userList.add(ugu);
//					}
//				}
//			}
//			if (qualityTest.getTemplate() != null) {
//				if (qualityTest.getTemplate().getId().equals(templateList.get(j).getId())) {
//					selTemplate.add(templateList.get(j));
//					templateList.remove(j);
//					j--;
//				}
//			}
//		}
//		for (int i = 0; i < userList.size(); i++) {
//			if (qualityTest.getTestUserOneId() != null) {
//				String[] userOne = qualityTest.getTestUserOneId().split(",");
//				one: for (String u : userOne) {
//					if (u.equals(userList.get(i).getUser().getId())) {
//						selUser.add(userList.get(i));
//						userList.remove(i);
//						i--;
//						break one;
//					}
//				}
//
//			}
//		}
//		putObjToContext("cellType", cellType);
//		putObjToContext("template", templateList);
//		putObjToContext("selTemplate", selTemplate);
//		putObjToContext("user", userList);
//		putObjToContext("selUser", selUser);
//		toState(qualityTest.getState());
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestAllot.jsp");
//	}
//
//	/**
//	 * 
//	 * @Title: showQualityTestItemTable @Description: 展示待排板列表 @author
//	 *         : @date @return @throws Exception String @throws
//	 */
//	@Action(value = "showQualityTestItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showQualityTestItemTable() throws Exception {
//		String id = getParameterFromRequest("id");
//		qualityTest = qualityTestService.get(id);
//		String bpmTaskId = getParameterFromRequest("bpmTaskId");
//		String cellType = getParameterFromRequest("cellType");
//		putObjToContext("bpmTaskId", bpmTaskId);
//		putObjToContext("cellType", cellType);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestMakeUp.jsp");
//	}
//
//	@Action(value = "showQualityTestItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showQualityTestItemTableJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = qualityTestService.findQualityTestItemTable(scId, start, length, query, col,
//					sort);
//			List<QualityTestItem> list = (List<QualityTestItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("sampleCode", "");
//			map.put("chromosomalLocation", "");
//			map.put("experimentalSteps", "");
//			map.put("experimentalStepsName", "");
//			map.put("sampleDeteyion-id", "");
//			map.put("sampleDeteyion-name", "");
//			map.put("orderId", "");
//			map.put("testItem", "");
//			map.put("mark", "");
//			map.put("zjName", "");
//			map.put("storagea-id", "");
//			map.put("storagea-name", "");
//			map.put("storagea-barCode", "");
//			map.put("serial", "");
//			
//			map.put("batch", "");
//			
//			
//			map.put("productName", "");
//			map.put("orderCode", "");
//			map.put("sampleOrder-filtrateCode", "");
//			map.put("sampleOrder-name", "");
//			map.put("sampleNum", "");
//			map.put("note", "");
//			
//			
//			map.put("sampleType", "");
//			map.put("SampleNumber", "");
//			
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: showQualityTestItemAfTableJson @Description: 排板后样本展示 @author
//	 *         : @date @throws Exception void @throws
//	 */
//
//	@Action(value = "showQualityTestItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showQualityTestItemAfTableJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = qualityTestService.findQualityTestItemAfTable(scId, start, length, query, col,
//					sort);
//			List<QualityTestItem> list = (List<QualityTestItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("sampleOrder-id", "");
//			map.put("code", "");
//			map.put("tempId", "");
//			map.put("sampleCode", "");
//			map.put("patientName", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("state", "");
//			map.put("note", "");
//			map.put("qualityTest-name", "");
//			map.put("qualityTest-id", "");
//			map.put("volume", "");
//			map.put("reportDate", "yyyy-MM-dd");
//			map.put("orderId", "");
//			map.put("chromosomalLocation", "");
//			map.put("orderNumber", "");
//			map.put("posId", "");
//			map.put("counts", "");
//			map.put("classify", "");
//			map.put("productNum", "");
//			map.put("dicSampleTypeId", "");
//			map.put("dicSampleTypeName", "");
//			map.put("sampleNum", "");
//			map.put("sampleConsume", "");
//			map.put("sampleType", "");
//			map.put("sampleInfo-id", "");
//			map.put("sampleInfo-idCard", "");
//			map.put("sampleInfo-note", "");
//			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
//			map.put("sampleInfo-reportDate", "");
//			map.put("techJkServiceTask-id", "");
//			map.put("techJkServiceTask-name", "");
//			map.put("blendCode", "");
//			map.put("color", "");
//			map.put("tjItem-id", "");
//			map.put("tjItem-inwardCode", "");
//			map.put("tjItem-wgcId", "");
//			map.put("isOut", "");
//			map.put("experimentalSteps", "");
//			map.put("sampleDeteyion-id", "");
//			map.put("sampleDeteyion-name", "");
//			map.put("orderId", "");
//			map.put("testItem", "");
//			map.put("mark", "");
//			map.put("zjName", "");
//			map.put("concentration", "");
//			map.put("internalReference", "");
//			map.put("storagea-id", "");
//			map.put("storagea-name", "");
//			map.put("storagea-barCode", "");
//			map.put("serial", "");
//			
//			map.put("batch", "");
//			
//			map.put("cellSampleTableId", "");
//			
//			map.put("sampleOrder-filtrateCode", "");
//			map.put("sampleOrder-name", "");
//			
//			map.put("qualitySubmitTime", "yyyy-MM-dd HH:mm:ss");
//			map.put("experimentalStepsName", "");
//			map.put("sampleOrder-ccoi", "");
//			map.put("sampleNumUnit", "");
//			//样本编号
//			map.put("SampleNumber", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: delQualityTestItem @Description: 删除待排板样本 @author @date @throws
//	 *         Exception void @throws
//	 */
//	@Action(value = "delQualityTestItem")
//	public void delQualityTestItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String delStr = getParameterFromRequest("del");
//		String id = getParameterFromRequest("id");
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			qualityTestService.delQualityTestItem(delStr, ids, user, id);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/** 保存审核人 */
//	@Action(value = "saveUser")
//	public void saveUser() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String type = getParameterFromRequest("type");
//		String zid = getParameterFromRequest("zid");
//		String uid = getParameterFromRequest("uid");
//		try {
//			qualityTestService.saveUser(type, zid, uid);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: delQualityTestItemAf @Description: 重新排板 @author : @date @throws
//	 *         Exception void @throws
//	 */
//	@Action(value = "delQualityTestItemAf")
//	public void delQualityTestItemAf() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			qualityTestService.delQualityTestItemAf(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: showBQualityTestResultTable @Description @author @date @return @throws
//	 *         Exception String @throws
//	 */
//	@Action(value = "showQualityTestResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showQualityTestResultTable() throws Exception {
//		String id = getParameterFromRequest("id");
//		String falg = getParameterFromRequest("falg");
//		String sczj = getParameterFromRequest("sczj");
//		qualityTest = qualityTestService.get(id);
//		String bpmTaskId = getParameterFromRequest("bpmTaskId");
//		putObjToContext("buttonShow", falg);
//		putObjToContext("bpmTaskId", bpmTaskId);
//		putObjToContext("sczj", sczj);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestResult.jsp");
//	}
//
//	@Action(value = "showQualityTestResultTableJson")
//	public void showQualityTestResultTableJson() throws Exception {
//		String id = getParameterFromRequest("id");
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			Map<String, Object> result = qualityTestService.showQualityTestResultTableJson(id, start, length, query,
//					col, sort);
//			List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("sampleOrder-id", "");
//			map.put("code", "");
//			map.put("sampleCode", "");
//			map.put("testItemData", "");
//			// map.put("volume", "");
//			map.put("unit", "");
//			map.put("result", "");
//			map.put("nextFlowId", "");
//			map.put("nextFlow", "");
//			map.put("method", "");
//			map.put("note", "");
//			map.put("qualityTest-id", "");
//			map.put("qualityTest-name", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("submit", "");
//			map.put("patientName", "");
//			map.put("reportDate", "yyyy-MM-dd");
//			// map.put("concentration", "");
//			map.put("orderId", "");
//			map.put("classify", "");
//			map.put("dicSampleType-id", "");
//			map.put("dicSampleType-name", "");
//			map.put("sampleType", "");
//			map.put("sampleNum", "");
//			map.put("sampleInfo-id", "");
//			map.put("sampleInfo-idCard", "");
//			map.put("sampleInfo-note", "");
//			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
//			map.put("sampleInfo-reportDate", "");
//			map.put("techJkServiceTask-id", "");
//			map.put("techJkServiceTask-name", "");
//			map.put("tjItem-id", "");
//			map.put("tjItem-inwardCode", "");
//			map.put("qab", "");
//			map.put("qaw1", "");
//			map.put("qbw2", "");
//			map.put("qbzy1", "");
//			map.put("qbzy2", "");
//			map.put("qcw1", "");
//			map.put("qcNum", "");
//			map.put("qchl", "");
//			map.put("qmd", "");
//			map.put("qw1", "");
//			map.put("qw3", "");
//			map.put("qw4", "");
//			map.put("qw42", "");
//			map.put("q5", "");
//			map.put("q6Num", "");
//			map.put("q6hl", "");
//			map.put("q6w", "");
//			map.put("qzy1", "");
//			map.put("qzy2", "");
//			map.put("qds", "");
//			map.put("experimentalSteps", "");
//			map.put("experimentalStepsName", "");
//			map.put("sampleNumUnit", "");
//			map.put("sampleDeteyion-id", "");
//			map.put("sampleDeteyion-name", "");
//			map.put("orderId", "");
//			map.put("zjName", "");
//			map.put("mark", "");
//			map.put("concentration", "");
//			map.put("internalReference", "");
//			map.put("storagea-id", "");
//			map.put("storagea-name", "");
//			map.put("storagea-barCode", "");
//			map.put("serial", "");
//			
//			map.put("batch", "");
//			map.put("cellSampleTableId", "");
//			map.put("internalReference", "");
//			map.put("sampleOrder-name", "");
//			map.put("sampleOrder-filtrateCode", "");
//			
//			map.put("sampleOrder-ccoi", "");
//			
//			/** 质检提交时间 */
//			map.put("qualitySubmitTime", "yyyy-MM-dd HH:mm:ss");
//			/** 质检完成时间 */
//			map.put("qualityFinishTime", "yyyy-MM-dd HH:mm:ss");
//			//y样本编号
//			map.put("SampleNumber","");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 
//	 * @Title: showBQualityTestResultTable @Description @author @date @return @throws
//	 *         Exception String @throws
//	 */
//	@Action(value = "showQualityTestResultItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showQualityTestResultItemTable() throws Exception {
//		String itemid = getParameterFromRequest("itemid");
//		String jiance = getParameterFromRequest("jiance");
//		String sczj = getParameterFromRequest("sczj");
//		String ly = getParameterFromRequest("ly");
//		putObjToContext("jiance", jiance);
//		putObjToContext("itemid", itemid);
//		putObjToContext("ly", ly);
//		putObjToContext("sczj", sczj);
//		qualityTestService.showQualityTestResultItemTableJson(itemid, jiance);
//
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestResultItem.jsp");
//	}
//
//	@Action(value = "showQualityTestResultItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showQualityTestResultItemTableJson() throws Exception {
//		String id = getParameterFromRequest("id");
//		String itemid = getParameterFromRequest("itemid");
//		String jiance = getParameterFromRequest("jiance");
//
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			Map<String, Object> result = qualityTestService.showQualityTestResultItemTableJson(id, start, length, query,
//					col, sort, itemid, jiance);
//			List<QualityTestInfoItem> list = (List<QualityTestInfoItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("resultName", "");
//			map.put("resultReferenceValue", "");
//			map.put("result", "");
//			map.put("infoId", "");
//			
//			map.put("testingCriteria", "");
//			
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 
//	 * @Title: 弹框展示质检结果 @Description: TODO @author : nan.jiang @date
//	 *         2018-8-31下午2:30:00 @return @throws Exception String @throws
//	 */
//	@Action(value = "showSampledetecyionDialog", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showSampledetecyionDialog() throws Exception {
//		String id = getParameterFromRequest("id");// 本是的主表Id
//		// String experimentalSteps =
//		// getParameterFromRequest("experimentalSteps");//本次实验的步骤
//		String experimentalSteps = URLDecoder.decode(getParameterFromRequest("experimentalSteps"), "utf-8");
//		String sampleDeteyion = URLDecoder.decode(getParameterFromRequest("sampleDeteyion"), "utf-8");
//		;// 本次实验的检测项
//		String sampleDeteyionName = URLDecoder.decode(getParameterFromRequest("sampleDeteyionName"), "utf-8");// 本次实验的检测项
//		// qualityTest = qualityTestService.get(id);
//		String bpmTaskId = getParameterFromRequest("bpmTaskId");
//		putObjToContext("bpmTaskId", bpmTaskId);
//		putObjToContext("orderId", id);
//		putObjToContext("experimentalSteps", experimentalSteps);
//		putObjToContext("sampleDeteyion", sampleDeteyion);
//		putObjToContext("sampleDeteyionName", sampleDeteyionName);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestResultDialog.jsp");
//	}
//
//	@Action(value = "showSampledetecyionDialogJson")
//	public void showSampledetecyionDialogJson() throws Exception {
//		String id = getParameterFromRequest("id");
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		String orderId = getParameterFromRequest("orderId");// 任务单号
//		String experimentalSteps = URLDecoder.decode(getParameterFromRequest("experimentalSteps"), "utf-8");
//		String sampleDeteyion = URLDecoder.decode(getParameterFromRequest("sampleDeteyion"), "utf-8");
//		;// 本次实验的检测项
//		try {
//			Map<String, Object> result = qualityTestService.showQualityTestResultTableDialogJson(id, start, length,
//					query, col, sort, orderId, experimentalSteps, sampleDeteyion);
//			List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("sampleOrder-id", "");
//			map.put("code", "");
//			map.put("sampleCode", "");
//			map.put("volume", "");
//			map.put("unit", "");
//			map.put("result", "");
//			map.put("nextFlowId", "");
//			map.put("nextFlow", "");
//			map.put("method", "");
//			map.put("note", "");
//			map.put("qualityTest-id", "");
//			map.put("qualityTest-name", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("submit", "");
//			map.put("patientName", "");
//			map.put("reportDate", "yyyy-MM-dd");
//			map.put("concentration", "");
//			map.put("orderId", "");
//			map.put("classify", "");
//			map.put("dicSampleType-id", "");
//			map.put("dicSampleType-name", "");
//			map.put("sampleType", "");
//			map.put("sampleNum", "");
//			map.put("sampleInfo-id", "");
//			map.put("sampleInfo-idCard", "");
//			map.put("sampleInfo-note", "");
//			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
//			map.put("sampleInfo-reportDate", "");
//			map.put("techJkServiceTask-id", "");
//			map.put("techJkServiceTask-name", "");
//			map.put("tjItem-id", "");
//			map.put("tjItem-inwardCode", "");
//			map.put("qab", "");
//			map.put("qaw1", "");
//			map.put("qbw2", "");
//			map.put("qbzy1", "");
//			map.put("qbzy2", "");
//			map.put("qcw1", "");
//			map.put("qcNum", "");
//			map.put("qchl", "");
//			map.put("qmd", "");
//			map.put("qw1", "");
//			map.put("qw3", "");
//			map.put("qw4", "");
//			map.put("qw42", "");
//			map.put("q5", "");
//			map.put("q6Num", "");
//			map.put("q6hl", "");
//			map.put("q6w", "");
//			map.put("qzy1", "");
//			map.put("qzy2", "");
//			map.put("qds", "");
//			map.put("experimentalSteps", "");
//			map.put("orderId", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 
//	 * @Title: delQualityTestResult @Description: 删除结果明细 @author : @date @throws
//	 *         Exception void @throws
//	 */
//	@Action(value = "delQualityTestResult")
//	public void delQualityTestResult() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String delStr = getParameterFromRequest("del");
//		String mainId = getParameterFromRequest("id");
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			qualityTestService.delQualityTestResult(ids, delStr, mainId, user);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: showQualityTestSteps @Description: 实验步骤 @author
//	 *         : @date @return @throws Exception String @throws
//	 */
//	@Action(value = "showQualityTestSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showQualityTestSteps() throws Exception {
//		String id = getParameterFromRequest("id");
//		qualityTest = qualityTestService.get(id);
//		String bpmTaskId = getParameterFromRequest("bpmTaskId");
//		putObjToContext("bpmTaskId", bpmTaskId);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestSteps.jsp");
//	}
//
//	@Action(value = "showQualityTestStepsJson")
//	public void showQualityTestStepsJson() throws Exception {
//		String id = getParameterFromRequest("id");
//		String orderNum = getParameterFromRequest("orderNum");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			map = qualityTestService.showQualityTestStepsJson(id, orderNum);
//		} catch (Exception e) {
//			map.put("sueccess", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 删除试剂明细信息
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "delQualityTestReagent")
//	public void delQualityTestReagent() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String delStr = getParameterFromRequest("del");
//		String mainId = getParameterFromRequest("main_id");
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		try {
//			String id = getParameterFromRequest("id");
//			qualityTestService.delQualityTestReagent(id, delStr, mainId, user);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 删除设备明细信息
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "delQualityTestCos")
//	public void delQualityTestCos() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String delStr = getParameterFromRequest("del");
//		String mainId = getParameterFromRequest("main_id");
//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//		try {
//			String id = getParameterFromRequest("id");
//			qualityTestService.delQualityTestCos(id, delStr, mainId, user);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: makeCode @Description: 打印条码 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void makeCode() throws Exception {
//
//		String id = getParameterFromRequest("id");
//		String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
//		CodeMain codeMain = null;
//		codeMain = codeMainService.get(id);
//		if (codeMain != null) {
//			String printStr = "";
//			String context = "";
//			for (int a = 0; a < sampleCode.length; a++) {
//				String codeFull = sampleCode[a];
//				String name = "";
//				// sampleReceiveService.getNameBySampleCode(codeFull);
//				printStr = codeMain.getCode();
//				String code1 = sampleCode[a].substring(0, 9);
//				String code2 = sampleCode[a].substring(9);
//				printStr = printStr.replaceAll("@@code1@@", code1);
//				printStr = printStr.replaceAll("@@code2@@", code2);
//				printStr = printStr.replaceAll("@@code@@", codeFull);
//				printStr = printStr.replaceAll("@@name@@", name);
//				context += printStr;
//			}
//			String ip = codeMain.getIp();
//			Socket socket = null;
//			OutputStream os;
//			try {
//				System.out.println(context);
//				socket = new Socket();
//				SocketAddress sa = new InetSocketAddress(ip, 9100);
//				socket.connect(sa);
//				os = socket.getOutputStream();
//				os.write(context.getBytes("UTF-8"));
//				os.flush();
//			} catch (UnknownHostException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			} finally {
//				if (socket != null) {
//					try {
//						socket.close();
//					} catch (IOException e) {
//					}
//				}
//			}
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: showQualityTestFromReceiveList @Description: 展示临时表 @author
//	 *         : @date @return @throws Exception String @throws
//	 */
//	@Action(value = "showQualityTestTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showQualityTestFromReceiveList() throws Exception {
//		String id = getParameterFromRequest("id");
//		putObjToContext("id", id);
//		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestTemp.jsp");
//	}
//
//	@Action(value = "showQualityTestTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showQualityTestTempTableJson() throws Exception {
//
//		String[] codes = getRequest().getParameterValues("codes[]");
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		String cellType = getParameterFromRequest("cellType");
//		try {
//			Map<String, Object> result = qualityTestService.selectQualityTestTempTable(cellType, codes, start, length,
//					query, col, sort);
//			List<QualityTestTemp> list = (List<QualityTestTemp>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("sampleCode", "");
//			map.put("productName", "");
//			map.put("testItem", "");
//			// map.put("concentration", "");
//			// map.put("volume", "");
//			map.put("orderCode", "");
//			map.put("sampleType", "");
//			map.put("sampleInfo-id", "");
//			map.put("sampleInfo-project-id", "");
//			map.put("orderId", "");
//			map.put("classify", "");
//			map.put("scopeId", "");
//			map.put("scopeName", "");
//			map.put("experimentalSteps", "");
//			//步骤名称
//			map.put("experimentalStepsName", "");
//			map.put("sampleDeteyion-id", "");
//			map.put("sampleDeteyion-name", "");
//			map.put("scopeName", "");
//
//			map.put("storage-id", "");
//			map.put("storage-name", "");
//			map.put("storage-barCode", "");
//			map.put("serial", "");
//			
//			map.put("batch", "");
//			
//			map.put("note", "");
//			map.put("sampleNum", "");
//			//单位
//			map.put("sampleNumUnit","");
//			map.put("sampleOrder-name", "");
//			map.put("sampleOrder-filtrateCode", "");
//			map.put("sampleOrder-ccoi", "");
//			
//			map.put("qualitySubmitTime", "yyyy-MM-dd HH:mm:ss");
//			//样本编号
//			map.put("SampleNumber","");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 
//	 * @Title: saveAllot @Description: 保存任务分配页面 @author : @date @throws
//	 */
//	@Action(value = "saveAllot")
//	public void saveAllot() throws Exception {
//		String main = getRequest().getParameter("main");
//		String userId = getParameterFromRequest("user");
//		String[] tempId = getRequest().getParameterValues("temp[]");
//		String templateId = getParameterFromRequest("template");
//		String logInfo = getParameterFromRequest("logInfo");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String id = qualityTestService.saveAllot(main, tempId, userId, templateId, logInfo);
//			result.put("success", true);
//			result.put("id", id);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 
//	 * @Title: saveMakeUp @Description: 保存排版界面 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "saveMakeUp")
//	public void saveMakeUp() throws Exception {
//		String blood_id = getParameterFromRequest("id");
//		String item = getParameterFromRequest("dataJson");
//		String logInfo = getParameterFromRequest("logInfo");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			qualityTestService.saveMakeUp(blood_id, item, logInfo);
//			result.put("success", true);
//			result.put("id", blood_id);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	@Action(value = "saveLeftQuality")
//	public void saveLeftQuality() throws Exception {
//		String id = getParameterFromRequest("id");
//		String[] ids = getRequest().getParameterValues("ids[]");
//		String logInfo = getParameterFromRequest("logInfo");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			QualityTest qualityTest = qualityTestService.get(id);
//			qualityTestService.saveLeftQuality(qualityTest, ids, logInfo);
//			result.put("success", true);
//			result.put("id", id);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	@Action(value = "saveRightQuality")
//	public void saveRightQuality() throws Exception {
//		String id = getParameterFromRequest("id");
//		String[] ids = getRequest().getParameterValues("ids[]");
//		String logInfo = getParameterFromRequest("logInfo");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			QualityTest qualityTest = qualityTestService.get(id);
//			qualityTestService.saveRightQuality(qualityTest, ids, logInfo);
//			result.put("success", true);
//			result.put("id", id);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 
//	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "saveSteps")
//	public void saveSteps() throws Exception {
//		String id = getParameterFromRequest("id");
//		String templateJson = getParameterFromRequest("templateJson");
//		String reagentJson = getParameterFromRequest("reagentJson");
//		String cosJson = getParameterFromRequest("cosJson");
//		String logInfo = getParameterFromRequest("logInfo");
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			qualityTestService.saveSteps(id, templateJson, reagentJson, cosJson, logInfo);
//			map.put("success", true);
//		} catch (Exception e) {
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: saveResult @Description: 保存结果表 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "saveResult")
//	public void saveResult() throws Exception {
//		String id = getParameterFromRequest("id");
//		String dataJson = getParameterFromRequest("dataJson");
//		String logInfo = getParameterFromRequest("logInfo");
//		String confirmUser = getParameterFromRequest("confirmUser");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			qualityTestService.saveResult(id, dataJson, logInfo, confirmUser);
//			map.put("success", true);
//		} catch (Exception e) {
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: saveResult @Description: 保存结果表 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "saveResultItem")
//	public void saveResultItem() throws Exception {
//		String dataJson = getParameterFromRequest("dataJson");
//		String logInfo = getParameterFromRequest("logInfo");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			qualityTestService.saveResultItem(dataJson, logInfo);
//			map.put("success", true);
//		} catch (Exception e) {
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * @throws Exception
//	 * 
//	 * @Title: showWellPlate @Description: 展示排版 @author : @date @throws
//	 */
//	@Action(value = "showWellPlate")
//	public void showWellPlate() throws Exception {
//		String id = getParameterFromRequest("id");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			List<QualityTestItem> json = qualityTestService.showWellPlate(id);
//			map.put("data", json);
//		} catch (Exception e) {
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: plateLayout @Description: 排板 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "plateLayout")
//	public void plateLayout() throws Exception {
//		String[] data = getRequest().getParameterValues("data[]");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			qualityTestService.plateLayout(data);
//			result.put("success", true);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: plateSample @Description: 孔板样本 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "plateSample")
//	public void plateSample() throws Exception {
//		String id = getParameterFromRequest("id");
//		String counts = getParameterFromRequest("counts");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			result = qualityTestService.plateSample(id, counts);
//			result.put("success", true);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 
//	 * @Title: plateSampleTable @Description:展示孔板样本列表 @author : @date @throws
//	 *         Exception void @throws
//	 */
//	@Action(value = "plateSampleTable")
//	public void plateSampleTable() throws Exception {
//		String id = getParameterFromRequest("id");
//		String counts = getParameterFromRequest("counts");
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			Map<String, Object> result = new HashMap<String, Object>();
//			result = qualityTestService.plateSampleTable(id, counts, start, length, query, col, sort);
//			List<QualityTestItem> list = (List<QualityTestItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("tempId", "");
//			map.put("sampleCode", "");
//			map.put("patientName", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("note", "");
//			map.put("color", "");
//			map.put("chromosomalLocation", "");
//			map.put("concentration", "");
//			map.put("qualityTest-name", "");
//			map.put("qualityTest-id", "");
//			map.put("volume", "");
//			map.put("reportDate", "yyyy-MM-dd");
//			map.put("orderId", "");
//			map.put("orderNumber", "");
//			map.put("posId", "");
//			map.put("counts", "");
//			map.put("classify", "");
//			map.put("productNum", "");
//			map.put("dicSampleTypeId", "");
//			map.put("dicSampleTypeName", "");
//			map.put("sampleNum", "");
//			map.put("sampleConsume", "");
//			map.put("sampleType", "");
//			map.put("sampleInfo-id", "");
//			map.put("sampleInfo-idCard", "");
//			map.put("sampleInfo-note", "");
//			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
//			map.put("sampleInfo-reportDate", "");
//			map.put("techJkServiceTask-id", "");
//			map.put("techJkServiceTask-name", "");
//			map.put("blendCode", "");
//			map.put("tjItem-id", "");
//			map.put("tjItem-inwardCode", "");
//			map.put("tjItem-wgcId", "");
//			map.put("isOut", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 
//	 * @Title: uploadCsvFile @Description: 上传结果 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void uploadCsvFile() throws Exception {
//		String id = getParameterFromRequest("id");
//		String fileId = getParameterFromRequest("fileId");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			qualityTestService.getCsvContent(id, fileId);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: bringResult @Description: 生成结果 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "bringResult")
//	public void bringResult() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String id = getParameterFromRequest("id");
//		try {
//			qualityTestService.bringResult(id);
//			map.put("success", true);
//		} catch (Exception e) {
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: generateBlendCode @Description: 生成混合号 @author : shengwei.wang @date
//	 *         2018年3月6日上午11:02:26 void @throws
//	 */
//	@Action(value = "generateBlendCode")
//	public void generateBlendCode() {
//		String id = getParameterFromRequest("id");
//		try {
//			Integer blendCode = qualityTestService.generateBlendCode(id);
//			HttpUtils.write(String.valueOf(blendCode));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * @throws Exception
//	 * 
//	 * @Title: submitSample @Description: 提交样本 @author : shengwei.wang @date
//	 *         2018年3月22日下午5:39:40 void @throws
//	 */
//	@Action(value = "submitSample")
//	public void submitSample() throws Exception {
//		String id = getParameterFromRequest("id");
//		String[] ids = getRequest().getParameterValues("ids[]");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			qualityTestService.submitSample(id, ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			map.put("success", false);
//			e.printStackTrace();
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: findQualityTestResultbyId @Description: TODO @author :
//	 *         nan.jiang @date 2018-8-29下午1:41:49 @throws Exception void @throws
//	 */
//	@Action(value = "findQualityTestResultbyId")
//	public void findQualityTestResultbyId() throws Exception {
//		String id = getParameterFromRequest("id");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			List<SampleDeteyionItem> sdList = qualityTestService.findQualityTestResultbyId(id);
//			map.put("list", sdList);
//			map.put("success", true);
//
//		} catch (Exception e) {
//			map.put("success", false);
//			e.printStackTrace();
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: downLoadTemp @Description: TODO(核算提取模板下载) @param @param
//	 *         ids @param @throws Exception    设定文件 @return void    返回类型 @author
//	 *         zhiqiang.yang@biolims.cn @date 2017-8-22 上午11:46:55 @throws
//	 */
//	@Action(value = "downLoadTemp")
//	public void downLoadTemp() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		String[] ids = getRequest().getParameterValues("ids");
//		String[] codes = getRequest().getParameterValues("codes");
//		String id = ids[0];
//		String code = codes[0];
//		String[] str1 = code.split(",");
//		String co = str1[0];
//		Date date = new Date();
//		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
//		String a = co + sdf.format(date);
//		Properties properties = new Properties();
//		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
//		properties.load(is);
//
//		String filePath = properties.getProperty("result.template.file") + "\\";// 写入csv路径
//		String fileName = filePath + a + ".csv";// 文件名称
//		File csvFile = null;
//		BufferedWriter csvWtriter = null;
//		csvFile = new File(fileName);
//		File parent = csvFile.getParentFile();
//		if (!parent.exists()) {
//			parent.mkdirs();
//		} else {
//			parent.delete();
//			parent.mkdirs();
//		}
//		csvFile.createNewFile();
//		// GB2312使正确读取分隔符","
//		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
//		// 写入文件头部
//		Object[] head = { "样本编号", "原始样本编号", "产物类型", "检测项目", "浓度", "体积" };
//		List<Object> headList = Arrays.asList(head);
//		for (Object data : headList) {
//			StringBuffer sb = new StringBuffer();
//			String rowStr = sb.append("\"").append(data).append("\",").toString();
//			csvWtriter.write(rowStr);
//		}
//		csvWtriter.newLine();
//		String[] sid = id.split(",");
//		for (int j = 0; j < sid.length; j++) {
//			String idc = sid[j];
//			for (int i = 0; i < ids.length; i++) {
//				QualityTestInfo sr = qualityTestService.getInfoById(idc);
//				StringBuffer sb = new StringBuffer();
//				setMolecularMarkersData(sr, sb);
//				String rowStr = sb.toString();
//				csvWtriter.write(rowStr);
//				csvWtriter.newLine();
//				if (sr.equals("")) {
//					result.put("success", false);
//				} else {
//					result.put("success", true);
//
//				}
//			}
//		}
//		csvWtriter.flush();
//		csvWtriter.close();
//		// HttpUtils.write(JsonUtils.toJsonString(result));
//		downLoadTemp3(a, filePath);
//	}
//
//	@Action(value = "downLoadTemp3")
//	public void downLoadTemp3(String a, String filePath2) throws Exception {
//		String filePath = filePath2;// 保存窗口中显示的文件名
//		String fileName = a + ".csv";// 保存窗口中显示的文件名
//		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");
//
//		/*
//		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
//		 */
//		ServletOutputStream out = null;
//		// PrintWriter out = null;
//		InputStream inStream = null;
//		try {
//			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//
//
//			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
//			// inline
//			out = super.getResponse().getOutputStream();
//
//			inStream = new FileInputStream(filePath + toUtf8String(fileName));
//
//			// 循环取出流中的数据
//			byte[] b = new byte[1024];
//			int len;
//			while ((len = inStream.read(b)) > 0)
//				out.write(b, 0, len);
//			super.getResponse().setStatus(super.getResponse().SC_OK);
//			super.getResponse().flushBuffer();
//
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (RuntimeException e) {
//			e.printStackTrace();
//		} finally {
//			if (out != null)
//				out.close();
//			inStream.close();
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: setMolecularMarkersData @Description: TODO(将对应的值添加到模板里) @param @param
//	 *         sr @param @param sb @param @throws Exception    设定文件 @return void   
//	 *         返回类型 @author zhiqiang.yang@biolims.cn @date 2017-8-22
//	 *         下午1:21:58 @throws
//	 */
//	public void setMolecularMarkersData(QualityTestInfo sr, StringBuffer sb) throws Exception {
//		if (null != sr.getCode()) {
//			sb.append("\"").append(sr.getCode()).append("\",");
//		} else {
//			sb.append("\"").append("").append("\",");
//		}
//		if (null != sr.getSampleCode()) {
//			sb.append("\"").append(sr.getSampleCode()).append("\",");
//		} else {
//			sb.append("\"").append("").append("\",");
//		}
//
//		if (null != sr.getDicSampleType().getName()) {
//			sb.append("\"").append(sr.getDicSampleType().getName()).append("\",");
//		} else {
//			sb.append("\"").append("").append("\",");
//		}
//
//		if (null != sr.getProductName()) {
//			sb.append("\"").append(sr.getProductName()).append("\",");
//		} else {
//			sb.append("\"").append("").append("\",");
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: toUtf8String @Description: TODO(解决乱码) @param @param
//	 *         s @param @return    设定文件 @return String    返回类型 @author
//	 *         zhiqiang.yang@biolims.cn @date 2017-8-23 下午4:40:07 @throws
//	 */
//	public static String toUtf8String(String s) {
//		StringBuffer sb = new StringBuffer();
//		for (int i = 0; i < s.length(); i++) {
//			char c = s.charAt(i);
//			if (c >= 0 && c <= 255) {
//				sb.append(c);
//			} else {
//				byte[] b;
//				try {
//					b = Character.toString(c).getBytes("utf-8");
//				} catch (Exception ex) {
//					System.out.println(ex);
//					b = new byte[0];
//				}
//				for (int j = 0; j < b.length; j++) {
//					int k = b[j];
//					if (k < 0)
//						k += 256;
//					sb.append("%" + Integer.toHexString(k).toUpperCase());
//				}
//			}
//		}
//		return sb.toString();
//	}
//	/**
//	 * 查询匹配模板
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "getTemplateId")
//	public void getTemplateId() throws Exception {
//		String proId = getParameterFromRequest("proId");
//		String id = getParameterFromRequest("id");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String tpId = qualityTestService.getTemplate(proId, id);
//			map.put("tpId", tpId);
//			map.put("success", true);
//		} catch (Exception e) {
//			map.put("success", false);
//			e.printStackTrace();
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	
//	
//	
//	/**
//	 * 执行改变状态的方法
//	 */
//	@Action(value = "exeFun", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String exeFun() throws Exception {
//		Map<String, String> messageMap = new HashMap<String, String>();
//		try {
//
//			String applicationTypeActionId = ServletActionContext.getRequest().getParameter("applicationTypeActionId");
//			String contentId = ServletActionContext.getRequest().getParameter("formId");
//			String r = "";
//
//			r = qualityTestService.exeEvent(applicationTypeActionId, contentId);
//			messageMap.put("message", r);
//		} catch (Exception e) {
//			e.printStackTrace();
//			messageMap.put("message", "请稍后再试，或者联系管理员!");
//
//		}
//		return this.renderText(JsonUtils.toJsonString(messageMap));
//
//	}
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityTestResultManageService getQualityTestResultManageService() {
		return qualityTestResultManageService;
	}

	public void setQualityTestResultManageService(QualityTestResultManageService qualityTestResultManageService) {
		this.qualityTestResultManageService = qualityTestResultManageService;
	}

	public QualityTestResultManage getQualityTestResultManage() {
		return qualityTestResultManage;
	}

	public void setQualityTestResultManage(QualityTestResultManage qualityTestResultManage) {
		this.qualityTestResultManage = qualityTestResultManage;
	}

	public QualityTestResultManageDao getQualityTestResultManageDao() {
		return qualityTestResultManageDao;
	}

	public void setQualityTestResultManageDao(QualityTestResultManageDao qualityTestDao) {
		this.qualityTestResultManageDao = qualityTestResultManageDao;
	}

}
