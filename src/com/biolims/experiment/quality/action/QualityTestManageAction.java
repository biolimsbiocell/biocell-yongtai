﻿package com.biolims.experiment.quality.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.service.QualityTestManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/quality/qualityTestManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityTestManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private QualityTestManageService qualityTestManageService;
	
	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 
	 * @Title: qualityTestManageItemRuku
	 * @Description: 样本管理入库
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "qualityTestManageItemRuku")
	public void qualityTestManageItemRuku() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qualityTestManageService.qualityTestManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: showQualityTestManage
	 * @Description: 样本管理
	 * @author : 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showQualityTestManage")
	public String showQualityTestManage() throws Exception {
		String cellType = getParameterFromRequest("cellType");
		if(cellType.equals("1")) {//产品干细胞
			rightsId = "211010104";
		}else if(cellType.equals("2")) {
			rightsId = "211010204";
		}else if(cellType.equals("3")) {
			rightsId = "211010304";
		}else if(cellType.equals("4")) {
			rightsId = "211010404";
		}else if(cellType.equals("5")) {//产品干细胞
			rightsId = "211020104";
		}else if(cellType.equals("6")) {
			rightsId = "211020204";
		}else if(cellType.equals("7")) {
			rightsId = "211020304";
		}else if(cellType.equals("8")) {
			rightsId = "211020404";
		}
		putObjToContext("cellType", cellType);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/quality/qualityTestManage.jsp");
	}

	@Action(value = "showQualityTestManageJson")
	public void showQualityTestManageJson() throws Exception {
		String cellType = getParameterFromRequest("cellType");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = qualityTestManageService
					.showQualityTestManageJson(cellType, start, length, query, col, sort);
			List<QualityTestItem> list = (List<QualityTestItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("qualityTest-name", "");
			map.put("qualityTest-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	
}

