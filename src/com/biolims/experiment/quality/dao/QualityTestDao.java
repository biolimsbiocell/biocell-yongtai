package com.biolims.experiment.quality.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.quality.model.QualityTest;
import com.biolims.experiment.quality.model.QualityTestCos;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.model.QualityTestInfoItem;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.quality.model.QualityTestReagent;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.quality.model.QualityTestTemplate;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.file.model.FileInfo;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.template.model.Template;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class QualityTestDao extends BaseHibernateDao {

	public Map<String, Object> findQualityTestTable(String cellType, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTest where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (cellType != null && !"".equals(cellType)) {
			key += " and cellType = '" + cellType + "'";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTest where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTest> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectQualityTestTempTable(String cellType, String[] codes, Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestTemp where 1=1 and state='1'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (cellType != null && !"".equals(cellType)) {
			key += " and cellType = '" + cellType + "'";
		}
		if (codes != null) {
			key += " and code in (:alist)";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = null;
		if (codes != null) {
			sumCount = (Long) getSession().createQuery(countHql + key).setParameterList("alist", codes).uniqueResult();
		} else {
			sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		}
		if (0l != sumCount) {
			List<QualityTestTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				if("SampleNumber".equals(col)) {
					key += " order by id asc ";
				}else {
					key += " order by " + col + " " + sort;
				}
			}
			if (codes != null) {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
						.setParameterList("alist", codes).list();
			} else {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findQualityTestItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestItem where 1=1 and state='1' and qualityTest.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestItem  where 1=1 and state='1' and qualityTest.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<QualityTestItem> showWellPlate(String id) {
		String hql = "from QualityTestItem  where 1=1 and state='2' and  qualityTest.id='" + id + "'";
		List<QualityTestItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findQualityTestItemAfTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestItem where 1=1 and state='2' and qualityTest.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestItem  where 1=1 and state='2' and qualityTest.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from QualityTestItem where 1=1 and state='2' and qualityTest.id='" + id
				+ "'";
		String key = "";
		List<String> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "select counts from QualityTestItem where 1=1 and state='2' and qualityTest.id='" + id
					+ "' group by counts";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<QualityTestTemplate> showQualityTestStepsJson(String id, String code) {

		String countHql = "select count(*) from QualityTestTemplate where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and orderNum ='" + code + "'";
		} else {
		}
		List<QualityTestTemplate> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from QualityTestTemplate where 1=1 and qualityTest.id='" + id + "'";
			key += " order by orderNum asc";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<QualityTestReagent> showQualityTestReagentJson(String id, String code) {
		String countHql = "select count(*) from QualityTestReagent where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<QualityTestReagent> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from QualityTestReagent where 1=1 and qualityTest.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<QualityTestCos> showQualityTestCosJson(String id, String code) {
		String countHql = "select count(*) from QualityTestCos where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<QualityTestCos> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from QualityTestCos where 1=1 and qualityTest.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<QualityTestTemplate> delTemplateItem(String id) {
		List<QualityTestTemplate> list = new ArrayList<QualityTestTemplate>();
		String hql = "from QualityTestTemplate where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<QualityTestReagent> delReagentItem(String id) {
		List<QualityTestReagent> list = new ArrayList<QualityTestReagent>();
		String hql = "from QualityTestReagent where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<QualityTestCos> delCosItem(String id) {
		List<QualityTestCos> list = new ArrayList<QualityTestCos>();
		String hql = "from QualityTestCos where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> showQualityTestResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1 and qualityTest.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> showQualityTestResultItemTableJson(String id, Integer start, Integer length,
			String query, String col, String sort, String itemid, String jiance) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfoItem where 1=1 and infoId='" + itemid + "' ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfoItem where 1=1 and infoId='" + itemid + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestInfoItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Object> showWellList(String id) {

		String hql = "select counts,count(*) from QualityTestItem where 1=1 and qualityTest.id='" + id
				+ "' group by counts";
		List<Object> list = getSession().createQuery(hql).list();

		return list;
	}

	public List<QualityTestItem> plateSample(String id, String counts) {
		String hql = "from QualityTestItem where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		if (counts == null || counts.equals("")) {
			key = " and counts is null";
		} else {
			key = "and counts='" + counts + "'";
		}
		List<QualityTestItem> list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestItem where 1=1 and qualityTest.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (counts == null || counts.equals("")) {
			key += " and counts is null";
		} else {
			key += "and counts='" + counts + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestItem  where 1=1 and qualityTest.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<QualityTestInfo> findQualityTestInfoByCode(String code) {
		String hql = "from QualityTestInfo where 1=1 and code='" + code + "'";
		List<QualityTestInfo> list = new ArrayList<QualityTestInfo>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<QualityTestInfo> selectAllResultListById(String code) {
		String hql = "from QualityTestInfo  where (submit is null or submit='') and qualityTest.id='" + code + "'";
		List<QualityTestInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QualityTestInfo> selectResultListById(String id) {
		String hql = "from QualityTestInfo  where qualityTest.id='" + id + "'";
		List<QualityTestInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<QualityTestTemp> findQualityTestTemp(String code) {
		String hql = "from QualityTestTemp  where SampleNumber='" + code + "'";
		List<QualityTestTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QualityTestInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from QualityTestInfo t where (submit is null or submit='') and id in (" + insql + ")";
		List<QualityTestInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QualityTestItem> selectQualityTestItemList(String scId) throws Exception {
		String hql = "from QualityTestItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qualityTest.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<QualityTestItem> list = new ArrayList<QualityTestItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Integer generateBlendCode(String id) {
		String hql = "select max(blendCode) from QualityTestItem where 1=1 and qualityTest.id='" + id + "'";
		Integer blendCode = (Integer) this.getSession().createQuery(hql).uniqueResult();
		return blendCode;
	}

	public QualityTestInfo getResultByCode(String code) {
		String hql = " from QualityTestInfo where 1=1 and code='" + code + "'";
		QualityTestInfo spi = (QualityTestInfo) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}

	/**
	 * @Title: findQualityTestResultbyId @Description: TODO @author :
	 *         nan.jiang @date 2018-8-29下午1:45:34 @param id @return
	 *         List<QualityTestItem> @throws
	 */
	public List<QualityTestItem> findQualityTestResultbyId(String id) {
		String hql = "from QualityTestItem where 1=1 ";
		String key = "";
		if (id != null)
			key = key + " and qualityTest.id='" + id + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<QualityTestItem> list = new ArrayList<QualityTestItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * @throws Exception
	 * @Title: showQualityTestResultTableDialogJson @Description: TODO @author :
	 *         nan.jiang @date 2018-8-31下午2:58:54 @param id @param start @param
	 *         length @param query @param col @param sort @param orderId @param
	 *         experimentalSteps @param sampleDeteyion @return
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> showQualityTestResultTableDialogJson(String id, Integer start, Integer length,
			String query, String col, String sort, String orderId, String experimentalSteps, String sampleDeteyion)
			throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1 ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (orderId != null && !orderId.equals("")) {
			key += " and orderId='" + orderId + "'";
		}
		if (experimentalSteps != null && !experimentalSteps.equals("")) {
			key += " and experimentalSteps like '%" + experimentalSteps.replace(" ", "") + "%'";
		}
		if (sampleDeteyion != null && !sampleDeteyion.equals("")) {
			key += " and sampleDeteyion='" + sampleDeteyion + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1 ";

			List<QualityTestInfo> list = getSession().createQuery(hql + key).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<QualityTestItem> findQualityTestItemTestItemText(String id) {
		List<QualityTestItem> list = new ArrayList<QualityTestItem>();
		String hql = "from QualityTestItem where 1=1 and qualityTest.id = '" + id + "'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findResultForzj(String stepNum, String mark, String testId, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (stepNum != null && !"".equals(stepNum)) {
			key += " and experimentalSteps = '" + stepNum + "'";
		}
		if (mark != null && !"".equals(mark)) {
			key += " and mark = '" + mark + "'";
		}
		if (testId != null && !"".equals(testId)) {
			key += " and sampleDeteyion.id = '" + testId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findResultAllForzj(String testId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (testId != null && !"".equals(testId)) {
			key += " and code like '%" + testId + "%'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<QualityTestInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public CellProductionRecord getCellProductionRecord(String id,
			String stepNum) {
		String hql = " from CellProductionRecord where 1=1 and cellPassage.id='" + id + "' and orderNum='"+stepNum+"' ";
		CellProductionRecord spi = (CellProductionRecord) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}
	public ReagentTaskRecord getReagentTaskRecord(String id,
			String stepNum) {
		String hql = " from ReagentTaskRecord where 1=1 and reagentTask.id='" + id + "' and orderNum='"+stepNum+"' ";
		ReagentTaskRecord spi = (ReagentTaskRecord) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}

	public List<FileInfo> getFileInfos(String id) {
		List<FileInfo> list = new ArrayList<FileInfo>();
		String hql = "from FileInfo where 1=1 and modelContentId = '" + id + "' and  ownerModel='Template' ";
		list = this.getSession().createQuery(hql).list();
		return list;
	}


	public Template findSampeDeteyion(String id) {
		String hql = " from Template where 1=1 and id='" + id+"'";
		Template spi = (Template) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}

	
	/**  
	* @Title: findStorageReagentBuySerialByTaskIdOrPc  
	* @Description: TODO(这里用一句话描述这个方法的作用)  
	* @param @param id
	* @param @param batch
	* @param @return   
	* @return List<StorageReagentBuySerial>    
	* @user nan.jiang
	* @date 2020年1月10日 
	* @throws  
	*/ 
	
	public List<StorageReagentBuySerial> findStorageReagentBuySerialByTaskIdOrPc(String id, String batch) {
		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
		String hql = "from StorageReagentBuySerial where 1=1 and storage = '" + id + "' and  serial='"+batch+"'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}
	
}