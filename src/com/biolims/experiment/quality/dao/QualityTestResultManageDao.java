package com.biolims.experiment.quality.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Repository;

import com.biolims.common.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.quality.model.QualityTestResultManage;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.model.QualityTestInfoItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class QualityTestResultManageDao extends BaseHibernateDao {
	
	public Map<String, Object> findResultManage(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestResultManage where 1=1  ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		
		if("admin".equals(u.getId())) {
			
		}else {
			key = key + " and (stateName='新建' or stateName='完成' or stateName='第二次待提交' "
					+ "or (stateName='第一次审核' and confirmUser.id='"+u.getId()+"' ) or (stateName='第一次退回' and createUser.id='"+u.getId()+"' ) "
					+ "or (stateName='第二次审核' and confirmUserTwo.id='"+u.getId()+"' ) or (stateName='第二次退回' and createUserTwo.id='"+u.getId()+"' ) ) ";
		}
		
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from QualityTestResultManage  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}else {
				key += " order by state_name desc ";
			}
			List<QualityTestResultManage> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showQualityTestResultItemTableJson(String id, Integer start, Integer length,
			String query, String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfoItem where 1=1 and qualityTestInfo.batch='" + id + "' and qualityTestInfo.qualityTest.state='1' ";
		String key = "";
//		if (query != null) {
//			key = map2Where(query);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfoItem where 1=1 and qualityTestInfo.batch='" + id + "' and qualityTestInfo.qualityTest.state='1' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}else {
				key += " order by qualityTestInfo.id";
			}
			List<QualityTestInfoItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}