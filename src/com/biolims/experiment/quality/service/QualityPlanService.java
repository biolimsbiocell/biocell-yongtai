package com.biolims.experiment.quality.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.experiment.quality.dao.QualityPlanDao;
import com.biolims.experiment.quality.model.QualityPlan;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QualityPlanService {
	@Resource
	private QualityPlanDao qualityPlanDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findQualityPlanList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityPlanDao.selectQualityPlanList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QualityPlan i) throws Exception {

		qualityPlanDao.saveOrUpdate(i);

	}
	public QualityPlan get(String id) {
		QualityPlan qualityPlan = commonDAO.get(QualityPlan.class, id);
		return qualityPlan;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QualityPlan sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qualityPlanDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
