package com.biolims.experiment.quality.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItemNegative;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.dao.QualityTestManageDao;
import com.biolims.experiment.quality.dao.QualityTestResultManageDao;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.model.QualityTestInfoItem;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.quality.model.QualityTestResultManage;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class QualityTestResultManageService {
	@Resource
	private QualityTestResultManageDao qualityTestResultManageDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	/**
	 * 
	 * @Title: findResultManage
	 * @throws
	 */
	public Map<String, Object> findResultManage(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return qualityTestResultManageDao.findResultManage(start, length, query,
				col, sort);
	}

	/**
	 * 获取主表实体方法
	 * @param id
	 * @return
	 */
	public QualityTestResultManage get(String id) {
		QualityTestResultManage qualityTestResultManage = commonDAO.get(QualityTestResultManage.class, id);
		return qualityTestResultManage;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> showQualityTestResultItemTableJson(String id, Integer start, Integer length,
			String query, String col, String sort) {
		List<QualityTestInfo> qtis = commonService.get(QualityTestInfo.class, "batch", id);
		for(QualityTestInfo qti:qtis) {
			List<QualityTestInfoItem> qtiis = commonService.get(QualityTestInfoItem.class, "infoId", qti.getId());
			for(QualityTestInfoItem qtii:qtiis) {
				if(qtii.getQualityTestInfo()!=null) {
					
				}else {
					qtii.setResultOriginal(qtii.getResult());
				}
				qtii.setQualityTestInfo(qti);
				commonDAO.saveOrUpdate(qtii);
			}
		}
		return qualityTestResultManageDao.showQualityTestResultItemTableJson(id, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQualityTestResultManageItem(QualityTestResultManage sc, String dataJson,
			String logInfo, String changeLogItem) throws Exception {
		
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		
		qualityTestResultManageDao.saveOrUpdate(sc);
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("QualityTestResultManage");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			qualityTestResultManageDao.saveOrUpdate(li);
		}
		
		
		List<QualityTestInfoItem> saveItems = new ArrayList<QualityTestInfoItem>();
		//子表
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				dataJson, List.class);
		for (Map<String, Object> map : list) {
			QualityTestInfoItem scpNew = new QualityTestInfoItem();
			// 将map信息读入实体类
			scpNew = (QualityTestInfoItem) qualityTestResultManageDao.Map2Bean(map, scpNew);
			
			QualityTestInfoItem scp = commonDAO.get(QualityTestInfoItem.class, scpNew.getId());
			
			scp.setModificationNote(scpNew.getModificationNote());
			
			if(scp.getResult().equals(scpNew.getResult())) {
				
			}else {
				scp.setModifier(u);
				scp.setModificationTime(new Date());
				scp.setResult(scpNew.getResult());
			}
					
			saveItems.add(scp);
		}
		qualityTestResultManageDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("QualityTestInfoItem");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(changeLogItem);
			qualityTestResultManageDao.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCreateUserAndTime(String businessKey) {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		QualityTestResultManage scp = commonDAO.get(QualityTestResultManage.class, businessKey);
		if(scp!=null) {
			scp.setCreateUser(u);
			scp.setCreateDate(new Date());
			scp.setState("20");
			scp.setStateName("审核中");
			commonDAO.saveOrUpdate(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCreateUserAndTimeSubmit(String businessKey) {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		QualityTestResultManage scp = commonDAO.get(QualityTestResultManage.class, businessKey);
		if(scp!=null) {
			scp.setCreateUser(u);
			scp.setCreateDate(new Date());
			scp.setState("20");
			scp.setStateName("提交人修改");
			commonDAO.saveOrUpdate(scp);
		}
	}

	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) {
		
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		QualityTestResultManage sct = qualityTestResultManageDao.get(QualityTestResultManage.class, id);
		
		if(sct!=null) {
			sct.setConfirmDate(new Date());
			sct.setState("1");
			sct.setStateName("完成");
			commonDAO.saveOrUpdate(sct);
//			List<PlasmaReceiveItem> pris = commonService.get(PlasmaReceiveItem.class, "sampleCode", sct.getSampleOrder().getBarcode());
//			if(pris.size()>0) {
//				for(PlasmaReceiveItem pri:pris) {
//					pri.setQualityTestPass("1");
//					commonDAO.saveOrUpdate(pri);
//				}
//			}
		}
	}

	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitTask(String id, String zt) throws ParseException {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		QualityTestResultManage sct = qualityTestResultManageDao.get(QualityTestResultManage.class, id);
		
		if(sct!=null) {
			if("新建".equals(zt)) {
				sct.setCreateUser(u);
				sct.setCreateDate(format.parse(format.format(new Date())));
				sct.setStateName("第一次审核");
			}else if("第一次退回".equals(zt)){
				sct.setCreateUser(u);
				sct.setCreateDate(format.parse(format.format(new Date())));
				sct.setStateName("第一次审核");
			}else if("第二次待提交".equals(zt)){
				sct.setCreateUserTwo(u);
				sct.setCreateDateTwo(format.parse(format.format(new Date())));
				sct.setStateName("第二次审核");
			}else if("第二次退回".equals(zt)){
				sct.setCreateUserTwo(u);
				sct.setCreateDateTwo(format.parse(format.format(new Date())));
				sct.setStateName("第二次审核");
			}
//			sct.setConfirmDate(new Date());
//			sct.setState("1");
//			sct.setStateName("完成");
			commonDAO.saveOrUpdate(sct);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void tongguoTask(String id, String zt) throws ParseException {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		QualityTestResultManage sct = qualityTestResultManageDao.get(QualityTestResultManage.class, id);
		
		if(sct!=null) {
			if("第一次审核".equals(zt)) {
				sct.setConfirmUser(u);
				sct.setConfirmDate(format.parse(format.format(new Date())));
				
				List<PlasmaReceiveItem> pris = commonService.get(PlasmaReceiveItem.class, "batchNumber", sct.getSampleOrder().getBarcode());
				if(pris.size()>0) {
					for(PlasmaReceiveItem pri:pris) {
						pri.setQualityTestPass("1");
						commonDAO.saveOrUpdate(pri);
					}
				}
				
				sct.setStateName("第二次待提交");
			}else if("第二次审核".equals(zt)){
				sct.setConfirmUserTwo(u);
				sct.setConfirmDateTwo(format.parse(format.format(new Date())));
				sct.setState("1");
				sct.setStateName("完成");
			}
			commonDAO.saveOrUpdate(sct);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void tuihuiTask(String id, String zt) {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		QualityTestResultManage sct = qualityTestResultManageDao.get(QualityTestResultManage.class, id);
		
		if(sct!=null) {
			if("第一次审核".equals(zt)) {
				sct.setConfirmUser(u);
				sct.setConfirmDate(new Date());
				sct.setStateName("第一次退回");
			}else if("第二次审核".equals(zt)){
				sct.setConfirmUserTwo(u);
				sct.setConfirmDateTwo(new Date());
				sct.setStateName("第二次退回");
			}
			commonDAO.saveOrUpdate(sct);
		}
	}
	
	
}

