package com.biolims.experiment.quality.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.quality.dao.QualityTestManageDao;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class QualityTestManageService {
	@Resource
	private QualityTestManageDao qualityTestManageDao;
	@Resource
	private CommonDAO commonDAO;



	/**
	 * 
	 * @Title: qualityTestManageItemRuku
	 * @Description: 入库
	 * @author : 
	 * @date 
	 * @param ids
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void qualityTestManageItemRuku(String[] ids) {
		for (String id : ids) {
			QualityTestItem scp = qualityTestManageDao.get(QualityTestItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				if (scp.getCode() == null) {
					st.setCode(scp.getSampleCode());
				} else {
					st.setCode(scp.getCode());
				}
				st.setSampleCode(scp.getSampleCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				qualityTestManageDao.saveOrUpdate(st);
			}

		}
	}

	/**
	 * 
	 * @Title: showQualityTestManageJson
	 * @Description: 展示样本管理明细
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showQualityTestManageJson(String cellType, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return qualityTestManageDao.showQualityTestManageJson(cellType, start, length, query,
				col, sort);
	}
}

