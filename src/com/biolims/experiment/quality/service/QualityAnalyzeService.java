package com.biolims.experiment.quality.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.experiment.quality.dao.QualityAnalyzeDao;
import com.biolims.experiment.quality.model.QualityAnalyze;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QualityAnalyzeService {
	@Resource
	private QualityAnalyzeDao qualityAnalyzeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findQualityAnalyzeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityAnalyzeDao.selectQualityAnalyzeList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QualityAnalyze i) throws Exception {

		qualityAnalyzeDao.saveOrUpdate(i);

	}
	public QualityAnalyze get(String id) {
		QualityAnalyze qualityAnalyze = commonDAO.get(QualityAnalyze.class, id);
		return qualityAnalyze;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QualityAnalyze sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qualityAnalyzeDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
