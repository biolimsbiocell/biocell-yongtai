﻿package com.biolims.experiment.quality.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.dao.ApplicationTypeActionDao;
import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.quality.dao.QualityTestDao;
import com.biolims.experiment.quality.model.QualityTest;
import com.biolims.experiment.quality.model.QualityTestAbnormal;
import com.biolims.experiment.quality.model.QualityTestCos;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.model.QualityTestInfoItem;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.quality.model.QualityTestReagent;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.quality.model.QualityTestTemplate;
import com.biolims.experiment.reagent.model.ReagentTaskInfoQuality;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.detecyion.dao.SampleDeteyionDao;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.detecyion.model.SampleDeteyionItem;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional
public class QualityTestService {

	@Resource
	private QualityTestDao qualityTestDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleDeteyionDao sampleDeteyionDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonService commonService;
	@Resource
	private ApplicationTypeActionDao applicationTypeActionDao;

	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         QualityTest @throws
	 */
	public QualityTest get(String id) {
		QualityTest qualityTest = commonDAO.get(QualityTest.class, id);
		return qualityTest;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestItem(String delStr, String[] ids, User user, String qualityTest_id) throws Exception {
		String delId = "";
		for (String id : ids) {
			QualityTestItem scp = qualityTestDao.get(QualityTestItem.class, id);
			if (scp.getId() != null) {
				QualityTest pt = qualityTestDao.get(QualityTest.class, scp.getQualityTest().getId());
				// 改变左侧样本状态
				QualityTestTemp qualityTestTemp = this.commonDAO.get(QualityTestTemp.class, scp.getTempId());
				if (qualityTestTemp != null) {
					pt.setSampleNum(pt.getSampleNum() - 1);
					qualityTestTemp.setState("1");
					qualityTestDao.update(qualityTestTemp);
				}
				qualityTestDao.update(pt);
				qualityTestDao.delete(scp);
			}
			delId += scp.getCode() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("QualityTest");
			li.setFileId(qualityTest_id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(delStr + delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delQualityTestItemAf @Description: 重新排板 @author : @date @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			QualityTestItem scp = qualityTestDao.get(QualityTestItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				qualityTestDao.saveOrUpdate(scp);
			}

			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setUserId(u.getId());
				li.setClassName("QualityTest");
				li.setFileId(scp.getQualityTest().getId());
				li.setModifyContent("质量检测删除已排版样本：" + scp.getSampleCode());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr = "";
		for (String id : ids) {
			QualityTestInfo scp = qualityTestDao.get(QualityTestInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				qualityTestDao.delete(scp);

			if (!"3".equals(scp.getQualityTest().getCellType()) || !"7".equals(scp.getQualityTest().getCellType())) {
				idStr += "产品批号:" + scp.getSampleCode() + "，样本编号:" + scp.getSampleNumber() + "，样本类型:"
						+ scp.getSampleType() + "步骤" + scp.getExperimentalSteps() + "步骤名称"
						+ scp.getExperimentalStepsName() + "，质检项:" + scp.getZjName();
			} else {
				idStr += "原辅料编号:" + scp.getStoragea().getId() + ",名称:" + scp.getStoragea().getName();
			}

		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setUserId(u.getId());
		li.setClassName("QualityTest");
		li.setFileId(mainId);
		li.setState("3");
		li.setStateName("数据修改");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			QualityTestReagent scp = qualityTestDao.get(QualityTestReagent.class, id);
			qualityTestDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setState("3");
			li.setStateName("数据修改");
			li.setClassName("QualityTest");
			li.setModifyContent(delStr + scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			QualityTestCos scp = qualityTestDao.get(QualityTestCos.class, id);
			qualityTestDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("QualityTest");
			li.setFileId(mainId);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(delStr + scp.getName());
		}
	}

	/**
	 * 
	 * @Title: saveQualityTestTemplate @Description: 保存模板 @author : @date @param
	 *         sc void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveQualityTestTemplate(QualityTest sc) {
		List<QualityTestTemplate> tlist2 = qualityTestDao.delTemplateItem(sc.getId());
		List<QualityTestReagent> rlist2 = qualityTestDao.delReagentItem(sc.getId());
		List<QualityTestCos> clist2 = qualityTestDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate().getId(), null);
		for (TemplateItem t : tlist) {
			QualityTestTemplate ptt = new QualityTestTemplate();
			ptt.setQualityTest(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			qualityTestDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			QualityTestReagent ptr = new QualityTestReagent();
			ptr.setQualityTest(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());

			ptr.setOneNum(t.getNum());
			qualityTestDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			QualityTestCos ptc = new QualityTestCos();
			ptc.setQualityTest(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			qualityTestDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);

		QualityTest sct = qualityTestDao.get(QualityTest.class, id);

		if ("1".equals(sct.getState())) {

		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date d = sdf.parse(sdf.format(new Date()));
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			sct.setConfirmDate(d);

			qualityTestDao.update(sct);
			// if (sct.getTemplate() != null) {
			// String iid = sct.getTemplate().getId();
			// if (iid != null) {
			// templateService.setCosIsUsedOver(iid);
			// }
			// }
			if ("3".equals(sct.getCellType()) || "7".equals(sct.getCellType())) {
				List<QualityTestInfo> list = this.qualityTestDao.selectAllResultListById(id);
				for (QualityTestInfo qti : list) {
					if (qti.getCellSampleTableId() != null && !"".equals(qti.getCellSampleTableId())) {
						ReagentTaskInfoQuality cpqi = commonDAO.get(ReagentTaskInfoQuality.class,
								qti.getCellSampleTableId());
						if (cpqi != null) {
							if (qti.getResult() != null && "1".equals(qti.getResult())) {
								cpqi.setQualified("2");
								cpqi.setQualityFinishTime(d);
							} else if (qti.getResult() != null && "0".equals(qti.getResult())) {
								cpqi.setQualified("1");
								cpqi.setQualityFinishTime(d);
							} else {
								cpqi.setQualityFinishTime(d);
							}
							cpqi.setQualityInfoId(qti.getId());
							qualityTestDao.saveOrUpdate(cpqi);

							if (cpqi.getReagentTask() != null && cpqi.getStepNum() != null) {
								ReagentTaskRecord cpr = qualityTestDao
										.getReagentTaskRecord(cpqi.getReagentTask().getId(), cpqi.getStepNum());
								if (cpr != null) {
									SysRemind srz = new SysRemind();
									srz.setId(null);
									// 提醒类型
									srz.setType(null);
									// 事件标题
									srz.setTitle("质检结果提醒");
									// 发起人（系统为SYSTEM）
									srz.setRemindUser(u.getName());
									// 发起时间
									srz.setStartDate(new Date());
									// 查阅提醒时间
									srz.setReadDate(new Date());
									// 提醒内容
									srz.setContent("质检单号" + cpqi.getReagentTask().getId() + "第" + cpqi.getStepNum()
											+ "步质检" + cpqi.getSampleDeteyion().getName() + "的结果已出");
									// 状态 0.草稿 1.已阅
									srz.setState("0");
									// 提醒的用户
									srz.setHandleUser(cpqi.getTempleOperator());
									// 表单id
									srz.setContentId(cpqi.getReagentTask().getId() + "&orderNum=" + cpqi.getStepNum());
									srz.setTableId("ReagentTask");
									commonDAO.saveOrUpdate(srz);
								}
							}
						}

					}
					List<StorageReagentBuySerial> s = commonService.get(StorageReagentBuySerial.class, "serial",
							qti.getCode());
					if (s.size() > 0) {
						for (StorageReagentBuySerial buySerial : s) {
							if ("1".equals(buySerial.getQcState())) {
								if ("1".equals(qti.getResult())) {
									if (qti.getSerial() != null && !"".equals(qti.getSerial())) {
										StorageReagentBuySerial srb = commonDAO.get(StorageReagentBuySerial.class,
												qti.getSerial());
										if (srb != null) {
											srb.setQcState("1");
											srb.setQcPassDate(d);
											qualityTestDao.saveOrUpdate(srb);
										}
									} else {
										List<StorageReagentBuySerial> srbs = commonService
												.get(StorageReagentBuySerial.class, "serial", qti.getCode());
										if (srbs.size() > 0) {
											for (StorageReagentBuySerial srb : srbs) {
												srb.setQcState("1");
												srb.setQcPassDate(d);
												qualityTestDao.saveOrUpdate(srb);
											}
										}
									}
								} else {
									if (qti.getSerial() != null && !"".equals(qti.getSerial())) {
										StorageReagentBuySerial srb = commonDAO.get(StorageReagentBuySerial.class,
												qti.getSerial());
										if (srb != null) {
											srb.setQcState("0");
											srb.setQcPassDate(d);
											qualityTestDao.saveOrUpdate(srb);
										}
									} else {
										List<StorageReagentBuySerial> srbs = commonService
												.get(StorageReagentBuySerial.class, "serial", qti.getCode());
										if (srbs.size() > 0) {
											for (StorageReagentBuySerial srb : srbs) {
												srb.setQcState("0");
												srb.setQcPassDate(d);
												qualityTestDao.saveOrUpdate(srb);
											}
										}
									}

								}

							} else {
								if ("1".equals(qti.getResult())) {
									if (qti.getSerial() != null && !"".equals(qti.getSerial())) {
										StorageReagentBuySerial srb = commonDAO.get(StorageReagentBuySerial.class,
												qti.getSerial());
										if (srb != null) {
											srb.setQcState("0");
											srb.setQcPassDate(d);
											qualityTestDao.saveOrUpdate(srb);
										}
									} else {
										List<StorageReagentBuySerial> srbs = commonService
												.get(StorageReagentBuySerial.class, "serial", qti.getCode());
										if (srbs.size() > 0) {
											for (StorageReagentBuySerial srb : srbs) {
												srb.setQcState("0");
												srb.setQcPassDate(d);
												qualityTestDao.saveOrUpdate(srb);
											}
										}
									}
								}
								if ("0".equals(qti.getResult())) {
									if (qti.getSerial() != null && !"".equals(qti.getSerial())) {
										StorageReagentBuySerial srb = commonDAO.get(StorageReagentBuySerial.class,
												qti.getSerial());
										if (srb != null) {
											srb.setQcState("0");
											srb.setQcPassDate(d);
											qualityTestDao.saveOrUpdate(srb);
										}
									} else {
										List<StorageReagentBuySerial> srbs = commonService
												.get(StorageReagentBuySerial.class, "serial", qti.getCode());
										if (srbs.size() > 0) {
											for (StorageReagentBuySerial srb : srbs) {
												srb.setQcState("0");
												srb.setQcPassDate(d);
												qualityTestDao.saveOrUpdate(srb);
											}
										}
									}

								}

							}

						}

					}
					// if ("1".equals(qti.getResult())) {
					// if (qti.getSerial() != null &&
					// !"".equals(qti.getSerial())) {
					// StorageReagentBuySerial srb =
					// commonDAO.get(StorageReagentBuySerial.class,
					// qti.getSerial());
					// if (srb != null) {
					// srb.setQcState("1");
					// srb.setQcPassDate(d);
					// qualityTestDao.saveOrUpdate(srb);
					// }
					// } else {
					// List<StorageReagentBuySerial> srbs =
					// commonService.get(StorageReagentBuySerial.class,
					// "serial", qti.getCode());
					// if (srbs.size() > 0) {
					// for (StorageReagentBuySerial srb : srbs) {
					// srb.setQcState("1");
					// srb.setQcPassDate(d);
					// qualityTestDao.saveOrUpdate(srb);
					// }
					// }
					// }
					// }
					// if ("0".equals(qti.getResult())) {
					// if (qti.getSerial() != null &&
					// !"".equals(qti.getSerial())) {
					// StorageReagentBuySerial srb =
					// commonDAO.get(StorageReagentBuySerial.class,
					// qti.getSerial());
					// if (srb != null) {
					// srb.setQcState("0");
					// srb.setQcPassDate(d);
					// qualityTestDao.saveOrUpdate(srb);
					// }
					// } else {
					// List<StorageReagentBuySerial> srbs =
					// commonService.get(StorageReagentBuySerial.class,
					// "serial", qti.getCode());
					// if (srbs.size() > 0) {
					// for (StorageReagentBuySerial srb : srbs) {
					// srb.setQcState("0");
					// srb.setQcPassDate(d);
					// qualityTestDao.saveOrUpdate(srb);
					// }
					// }
					// }
					//
					// }
					qti.setQualityFinishTime(d);
					qti.setSubmit("1");
					qualityTestDao.saveOrUpdate(qti);
				}
			} else {

				List<QualityTestInfo> list = this.qualityTestDao.selectAllResultListById(id);
				for (QualityTestInfo qti : list) {
					if (qti.getCellSampleTableId() != null && !"".equals(qti.getCellSampleTableId())) {

						CellPassageQualityItem cpqi = commonDAO.get(CellPassageQualityItem.class,
								qti.getCellSampleTableId());
						if (cpqi != null) {
							if("1".equals(qti.getSampleDeteyion().getYesOrNo())) {
								if (qti.getResult() != null && "1".equals(qti.getResult())) {
									cpqi.setQualified("2");
									cpqi.setQualityFinishTime(d);
								} else if (qti.getResult() != null && "0".equals(qti.getResult())) {
									cpqi.setQualified("1");
									cpqi.setQualityFinishTime(d);
									QualityTestAbnormal qualityTestAbnormal = new QualityTestAbnormal();
//									//样本编号	
//									
									qualityTestAbnormal.setCode(qti.getSampleNumber());
//									//CCOI//产品批号//产品//筛选好
									qualityTestAbnormal.setSampleOrder(qti.getSampleOrder());
//								
//									//检测项
									if(qti.getSampleDeteyion()!=null) {
										qualityTestAbnormal.setProductName(qti.getSampleDeteyion().getName());
										//监测项ID
										qualityTestAbnormal.setProductId(qti.getSampleDeteyion().getId());

									}
//									//样本类型
									qualityTestAbnormal.setSampleType(qti.getSampleType());
//									//步骤号
//									//步骤名称
									qualityTestAbnormal.setExperimentalStepsName(qti.getExperimentalStepsName());
////									//检测量
//									if(!"".equals(qti.getSampleNum()!=null) {
//										Double valueOf = Double.valueOf(qti.getSampleNum());
//										qualityTestAbnormal.setSampleNum(valueOf);
//
//									}
//									//监测单位
									qualityTestAbnormal.setUnit(qti.getSampleNumUnit());
									qualityTestAbnormal.setCellType("1");
									qualityTestAbnormal.setState("1");
									qualityTestDao.saveOrUpdate(qualityTestAbnormal);
//									
								} else {
									cpqi.setQualityFinishTime(d);
								}
								cpqi.setQualityInfoId(qti.getId());
								qualityTestDao.saveOrUpdate(cpqi);

							if (cpqi.getCellPassage() != null && cpqi.getStepNum() != null) {
								CellProductionRecord cpr = qualityTestDao
										.getCellProductionRecord(cpqi.getCellPassage().getId(), cpqi.getStepNum());
								if (cpr != null) {
									SysRemind srz = new SysRemind();
									srz.setId(null);
									// 提醒类型
									srz.setType(null);
									// 事件标题
									srz.setTitle("质检结果提醒");
									// 发起人（系统为SYSTEM）
									srz.setRemindUser(u.getName());
									// 发起时间
									srz.setStartDate(new Date());
									// 查阅提醒时间
									srz.setReadDate(new Date());
									// 提醒内容
									srz.setContent("批次" + cpqi.getCellPassage().getBatch() + "第" + cpqi.getStepNum()
											+ "步质检" + cpqi.getSampleDeteyion().getName() + "的结果已出");
									// 状态 0.草稿 1.已阅
									srz.setState("0");
									// 提醒的用户
									srz.setHandleUser(cpr.getTempleOperator());
									// 表单id

									srz.setContentId(cpqi.getCellPassage().getId() + "&bpmTaskId=");
									srz.setTableId("CellPassageQualityItem");
									commonDAO.saveOrUpdate(srz);
								}
							}
						}else {
							if (qti.getResult() != null && "0".equals(qti.getResult())) {
								QualityTestAbnormal qualityTestAbnormal = new QualityTestAbnormal();
//								//样本编号	
//								
								qualityTestAbnormal.setCode(qti.getSampleNumber());
//								//CCOI//产品批号//产品//筛选好
								qualityTestAbnormal.setSampleOrder(qti.getSampleOrder());
//							
//								//检测项
								if(qti.getSampleDeteyion()!=null) {
									qualityTestAbnormal.setProductName(qti.getSampleDeteyion().getName());
									//监测项ID
									qualityTestAbnormal.setProductId(qti.getSampleDeteyion().getId());

								}
//								//样本类型
								qualityTestAbnormal.setSampleType(qti.getSampleType());
//								//步骤号
//								//步骤名称
								qualityTestAbnormal.setExperimentalStepsName(qti.getExperimentalStepsName());
////								//检测量
//								if(!"".equals(qti.getSampleNum()!=null) {
//									Double valueOf = Double.valueOf(qti.getSampleNum());
//									qualityTestAbnormal.setSampleNum(valueOf);
//
//								}
//								//监测单位
								qualityTestAbnormal.setUnit(qti.getSampleNumUnit());
								qualityTestAbnormal.setCellType("1");
								qualityTestAbnormal.setState("1");
								qualityTestDao.saveOrUpdate(qualityTestAbnormal);
							}
							
							
						}

						}

					}
					
					qti.setQualityFinishTime(d);
					qti.setSubmit("1");
					qualityTestDao.saveOrUpdate(qti);

				}

			}
			// submitSample(id, null);
		}

	}

	/**
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : @date @param id @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = sdf.parse(sdf.format(new Date()));

		// 获取主表信息
		QualityTest sc = this.qualityTestDao.get(QualityTest.class, id);
		// 获取结果表样本信息
		List<QualityTestInfo> list;
		if (ids == null)
			list = this.qualityTestDao.selectAllResultListById(id);
		else
			list = this.qualityTestDao.selectAllResultListByIds(ids);

		for (QualityTestInfo scp : list) {
			if (scp != null && scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
				if ("3".equals(sc.getCellType()) || "7".equals(sc.getCellType())) {
					if ("1".equals(scp.getResult())) {
						if (scp.getSerial() != null && !"".equals(scp.getSerial())) {
							StorageReagentBuySerial srb = commonDAO.get(StorageReagentBuySerial.class, scp.getSerial());
							if (srb != null) {
								srb.setQcState("1");
								srb.setQcPassDate(d);
								qualityTestDao.saveOrUpdate(srb);
							}
						}
					}
				} else {

					if (scp.getCellSampleTableId() != null && !"".equals(scp.getCellSampleTableId())) {
						CellPassageQualityItem cpqi = commonDAO.get(CellPassageQualityItem.class,
								scp.getCellSampleTableId());
						if (cpqi != null) {
							if (scp.getResult() != null && "1".equals(scp.getResult())) {
								cpqi.setQualified("2");
								cpqi.setQualityFinishTime(d);
							} else if (scp.getResult() != null && "0".equals(scp.getResult())) {
								cpqi.setQualified("1");
								cpqi.setQualityFinishTime(d);
							} else {
								cpqi.setQualityFinishTime(d);
							}
							cpqi.setQualityInfoId(scp.getId());
							qualityTestDao.saveOrUpdate(cpqi);

							if (cpqi.getCellPassage() != null && cpqi.getStepNum() != null) {
								CellProductionRecord cpr = qualityTestDao
										.getCellProductionRecord(cpqi.getCellPassage().getId(), cpqi.getStepNum());
								if (cpr != null) {
									SysRemind srz = new SysRemind();
									srz.setId(null);
									// 提醒类型
									srz.setType(null);
									// 事件标题
									srz.setTitle("质检结果提醒");
									// 发起人（系统为SYSTEM）
									srz.setRemindUser(u.getName());
									// 发起时间
									srz.setStartDate(new Date());
									// 查阅提醒时间
									srz.setReadDate(new Date());
									// 提醒内容
									srz.setContent("批次" + cpqi.getCellPassage().getBatch() + "第" + cpqi.getStepNum()
											+ "步质检" + cpqi.getSampleDeteyion().getName() + "的结果已出");
									// 状态 0.草稿 1.已阅
									srz.setState("0");
									// 提醒的用户
									srz.setHandleUser(cpr.getTempleOperator());
									// 表单id
									srz.setContentId(cpqi.getCellPassage().getId() + "&orderNum=" + cpqi.getStepNum());
									srz.setTableId("CellPassageQualityItem");
									commonDAO.saveOrUpdate(srz);
								}
							}
						}
					}
				}
				// if (scp.getResult().equals("1")) {// 合格
				// String next = scp.getNextFlowId();
				//
				// if (next.equals("0009")) {// 样本入库
				// // 样本入库
				// SampleInItemTemp st = new SampleInItemTemp();
				// st.setSampleState("1");
				// st.setScopeId(sc.getScopeId());
				// st.setScopeName(sc.getScopeName());
				// st.setProductId(scp.getProductId());
				// st.setProductName(scp.getProductName());
				// st.setCode(scp.getCode());
				// st.setSampleCode(scp.getSampleCode());
				// st.setConcentration(scp.getConcentration());
				// st.setVolume(scp.getVolume());
				// DicSampleType t = commonDAO.get(DicSampleType.class,
				// scp.getDicSampleType().getId());
				// st.setSampleType(t.getName());
				// st.setDicSampleType(scp.getDicSampleType());
				// st.setState("1");
				// st.setPatientName(scp.getPatientName());
				// st.setSampleTypeId(scp.getDicSampleType().getId());
				// st.setInfoFrom("QualityTestInfo");
				// if (scp.getSampleOrder() != null) {
				// st.setSampleOrder(commonDAO.get(SampleOrder.class,
				// scp.getSampleOrder().getId()));
				// }
				// qualityTestDao.saveOrUpdate(st);
				//
				// } else if (next.equals("0012")) {// 暂停
				// } else if (next.equals("0013")) {// 终止
				// } else if (next.equals("0017")) {// 核酸提取
				// // 核酸提取
				// DnaTaskTemp d = new DnaTaskTemp();
				// scp.setState("1");
				// d.setState("1");
				// d.setConcentration(scp.getConcentration());
				// d.setVolume(scp.getVolume());
				// d.setOrderId(sc.getId());
				// d.setScopeId(sc.getScopeId());
				// d.setScopeName(sc.getScopeName());
				// d.setProductId(scp.getProductId());
				// d.setProductName(scp.getProductName());
				// d.setSampleCode(scp.getSampleCode());
				// d.setCode(scp.getCode());
				// DicSampleType t = commonDAO.get(DicSampleType.class,
				// scp.getDicSampleType().getId());
				// d.setSampleType(t.getName());
				// d.setSampleInfo(scp.getSampleInfo());
				// if (scp.getSampleOrder() != null) {
				// d.setSampleOrder(commonDAO.get(SampleOrder.class,
				// scp.getSampleOrder().getId()));
				// }
				// qualityTestDao.saveOrUpdate(d);
				// } else if (next.equals("0018")) {// 文库构建
				// // 文库构建
				// WkTaskTemp d = new WkTaskTemp();
				// scp.setState("1");
				// d.setState("1");
				// d.setScopeId(sc.getScopeId());
				// d.setScopeName(sc.getScopeName());
				// d.setProductId(scp.getProductId());
				// d.setProductName(scp.getProductName());
				// d.setConcentration(scp.getConcentration());
				// d.setVolume(scp.getVolume());
				// d.setOrderId(sc.getId());
				// d.setSampleCode(scp.getSampleCode());
				// d.setCode(scp.getCode());
				// DicSampleType t = commonDAO.get(DicSampleType.class,
				// scp.getDicSampleType().getId());
				// d.setSampleType(t.getName());
				// d.setSampleInfo(scp.getSampleInfo());
				// if (scp.getSampleOrder() != null) {
				// d.setSampleOrder(commonDAO.get(SampleOrder.class,
				// scp.getSampleOrder().getId()));
				// }
				// qualityTestDao.saveOrUpdate(d);
				//
				// } else {
				// scp.setState("1");
				// scp.setScopeId(sc.getScopeId());
				// scp.setScopeName(sc.getScopeName());
				// // 得到下一步流向的相关表单
				// List<NextFlow> list_nextFlow =
				// nextFlowDao.seletNextFlowById(next);
				// for (NextFlow n : list_nextFlow) {
				// Object o =
				// Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
				// sampleInputService.copy(o, scp);
				// }
				// }
				// } else {// 不合格
				// QualityTestAbnormal pa = new QualityTestAbnormal();// 样本异常
				// pa.setOrderId(sc.getId());
				// pa.setState("1");
				// sampleInputService.copy(pa, scp);
				// }
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService.saveSampleState1(scp.getSampleOrder(), scp.getCode(), scp.getSampleCode(),
						scp.getProductId(), scp.getProductName(), "", sc.getCreateDate(), format.format(new Date()),
						"QualityTest", "质量检测",
						(User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY),
						id, scp.getNextFlow(), scp.getResult(), null, null, null, null, null, null, null, null);
				scp.setQualityFinishTime(d);
				scp.setSubmit("1");
				qualityTestDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findQualityTestTable @Description: 展示主表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findQualityTestTable(String cellType, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return qualityTestDao.findQualityTestTable(cellType, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: selectQualityTestTempTable @Description: 展示临时表 @author
	 *         : @date @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectQualityTestTempTable(String cellType, String[] codes, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return qualityTestDao.selectQualityTestTempTable(cellType, codes, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId, String templateId, String logInfo)
			throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			QualityTest pt = new QualityTest();
			pt = (QualityTest) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String log = "";
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				log = "123";
				String modelName = "QualityTest";
				String markCode = "QT";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			} else {
				id = pt.getId();
				pt = commonDAO.get(QualityTest.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String[] users = userId.split(",");
			String userName = "";
			for (int i = 0; i < users.length; i++) {
				if (i == users.length - 1) {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName();
				} else {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName() + ",";
				}
			}
			pt.setTestUserOneName(userName);
			qualityTestDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveQualityTestTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
					for (String temp : tempId) {
						QualityTestTemp ptt = qualityTestDao.get(QualityTestTemp.class, temp);
						if (t.getIsSeparate().equals("1")) {
							List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
									.getMappingPrimersLibraryItem(ptt.getProductId());
							for (MappingPrimersLibraryItem mpi : listMPI) {
								QualityTestItem pti = new QualityTestItem();
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
										? t.getDicSampleType().getId() : t.getDicSampleTypeId());
								pti.setDicSampleTypeName(
										t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
												? t.getDicSampleType().getName() : t.getDicSampleTypeName());
								pti.setProductName(ptt.getProductName());
								pti.setProductNum(t.getProductNum() != null && !"".equals(t.getProductNum())
										? t.getProductNum() : t.getProductNum1());
								pti.setCode(ptt.getCode());
								pti.setSampleNumber(ptt.getSampleNumber());
								if (t.getStorageContainer() != null) {
									pti.setState("1");
								} else {
									pti.setState("2");
								}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setExperimentalSteps(ptt.getExperimentalSteps());
								pti.setSampleDeteyion(ptt.getSampleDeteyion());
								pti.setOrderId(ptt.getOrderId());
								pti.setQualityTest(pt);
								pti.setChromosomalLocation(mpi.getChromosomalLocation());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (ptt.getSampleOrder() != null) {
									pti.setSampleOrder(ptt.getSampleOrder());
								}
								pti.setCellType(pt.getCellType());
								pti.setTestItem(ptt.getTestItem());
								pti.setMark(ptt.getMark());
								pti.setZjName(ptt.getZjName());

								pti.setSerial(ptt.getSerial());
								pti.setStoragea(ptt.getStorage());

								pti.setBatch(ptt.getBatch());
								pti.setExperimentalStepsName(ptt.getExperimentalStepsName());
								pti.setSampleNumUnit(ptt.getSampleNumUnit());
								if (ptt.getSampleOrder() == null) {
									if (ptt.getOrderCode() != null && !"".equals(ptt.getOrderCode())) {
										SampleOrder soo = commonDAO.get(SampleOrder.class, ptt.getOrderCode());
										if (soo != null) {
											pti.setSampleOrder(soo);
										}
									}
								}

								pti.setCellSampleTableId(ptt.getCellSampleTaleId());
								pti.setOrderCode(ptt.getOrderCode());

								if (ptt.getSampleNum() != null && !"".equals(ptt.getSampleNum())) {
									pti.setSampleNum(Double.valueOf(ptt.getSampleNum()));
								}

								pti.setNote(ptt.getNote());

								pti.setSampleType(ptt.getSampleType());
								pti.setProductNum("1");

								if ("3".equals(pt.getCellType()) || "7".equals(pt.getCellType())) {
									ReagentTaskInfoQuality infoQuality = commonDAO.get(ReagentTaskInfoQuality.class,
											pti.getCellSampleTableId());
									if (infoQuality != null) {
										infoQuality.setQualityReceiveTime(sdf.parse(pt.getCreateDate()));
										qualityTestDao.saveOrUpdate(infoQuality);
									}
								} else {

									CellPassageQualityItem cpqi = commonDAO.get(CellPassageQualityItem.class,
											pti.getCellSampleTableId());
									if (cpqi != null) {
										cpqi.setQualityReceiveTime(sdf.parse(pt.getCreateDate()));
										qualityTestDao.saveOrUpdate(cpqi);
									}

								}

								pti.setQualitySubmitTime(ptt.getQualitySubmitTime());

								qualityTestDao.saveOrUpdate(pti);
							}
						} else {
							QualityTestItem pti = new QualityTestItem();
							pti.setSampleCode(ptt.getSampleCode());
							pti.setProductId(ptt.getProductId());
							pti.setProductName(ptt.getProductName());
							pti.setDicSampleTypeId(t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
									? t.getDicSampleType().getId() : t.getDicSampleTypeId());
							pti.setProductNum(t.getProductNum() != null && !"".equals(t.getProductNum())
									? t.getProductNum() : t.getProductNum1());
							pti.setDicSampleTypeName(t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
									? t.getDicSampleType().getName() : t.getDicSampleTypeName());
							pti.setCode(ptt.getCode());
							pti.setSampleNumber(ptt.getSampleNumber());
							pti.setExperimentalSteps(ptt.getExperimentalSteps());
							pti.setSampleDeteyion(ptt.getSampleDeteyion());
							pti.setOrderId(ptt.getOrderId());
							pti.setSampleInfo(ptt.getSampleInfo());
							if (t.getStorageContainer() != null) {
								pti.setState("1");
							} else {
								pti.setState("2");
							}
							ptt.setState("2");
							pti.setTempId(temp);
							pti.setQualityTest(pt);
							if (ptt.getSampleOrder() != null) {
								pti.setSampleOrder(commonDAO.get(SampleOrder.class, ptt.getSampleOrder().getId()));
							}
							pti.setCellType(pt.getCellType());
							pti.setTestItem(ptt.getTestItem());
							pti.setMark(ptt.getMark());
							pti.setZjName(ptt.getZjName());

							pti.setSerial(ptt.getSerial());
							pti.setStoragea(ptt.getStorage());

							pti.setBatch(ptt.getBatch());
							pti.setExperimentalStepsName(ptt.getExperimentalStepsName());
							pti.setSampleNumUnit(ptt.getSampleNumUnit());

							if (ptt.getSampleOrder() == null) {
								if (ptt.getOrderCode() != null && !"".equals(ptt.getOrderCode())) {
									SampleOrder soo = commonDAO.get(SampleOrder.class, ptt.getOrderCode());
									if (soo != null) {
										pti.setSampleOrder(soo);
									}
								}
							}

							pti.setCellSampleTableId(ptt.getCellSampleTaleId());
							pti.setOrderCode(ptt.getOrderCode());

							if (ptt.getSampleNum() != null && !"".equals(ptt.getSampleNum())) {
								pti.setSampleNum(Double.valueOf(ptt.getSampleNum()));
							}

							pti.setNote(ptt.getNote());

							pti.setSampleType(ptt.getSampleType());
							pti.setProductNum("1");

							if ("3".equals(pt.getCellType()) || "7".equals(pt.getCellType())) {
								ReagentTaskInfoQuality infoQuality = commonDAO.get(ReagentTaskInfoQuality.class,
										pti.getCellSampleTableId());
								if (infoQuality != null) {
									infoQuality.setQualityReceiveTime(sdf.parse(pt.getCreateDate()));
									qualityTestDao.saveOrUpdate(infoQuality);
								}
							} else {

								CellPassageQualityItem cpqi = commonDAO.get(CellPassageQualityItem.class,
										pti.getCellSampleTableId());
								if (cpqi != null) {
									cpqi.setQualityReceiveTime(sdf.parse(pt.getCreateDate()));
									qualityTestDao.saveOrUpdate(cpqi);
								}

							}

							pti.setQualitySubmitTime(ptt.getQualitySubmitTime());

							qualityTestDao.saveOrUpdate(pti);
						}
						qualityTestDao.saveOrUpdate(ptt);
					}
				}

			}

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setClassName("QualityTest");
				li.setModifyContent(logInfo);
				if ("123".equals(log) && !"".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findQualityTestItemTable @Description:展示未排版样本 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> findQualityTestItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return qualityTestDao.findQualityTestItemTable(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排板数据 @author : @date @param id @param
	 *         item @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<QualityTestItem> saveItems = new ArrayList<QualityTestItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		QualityTest pt = commonDAO.get(QualityTest.class, id);
		QualityTestItem scp = new QualityTestItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (QualityTestItem) qualityTestDao.Map2Bean(map, scp);
			QualityTestItem pti = commonDAO.get(QualityTestItem.class, scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setConcentration(scp.getConcentration());
			pti.setInternalReference(scp.getInternalReference());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());

			pti.setCellSampleTableId(scp.getCellSampleTableId());
			pti.setBatch(scp.getBatch());

			scp.setQualityTest(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("QualityTest");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
		qualityTestDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate @Description: 展示孔板 @author : @date @param
	 *         id @return @throws Exception List<QualityTestItem> @throws
	 */
	public List<QualityTestItem> showWellPlate(String id) throws Exception {
		List<QualityTestItem> list = qualityTestDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findQualityTestItemAfTable @Description: 展示排板后 @author
	 *         : @date @param scId @param start @param length @param
	 *         query @param col @param sort @return @throws Exception
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> findQualityTestItemAfTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return qualityTestDao.findQualityTestItemAfTable(scId, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			QualityTestItem pti = commonDAO.get(QualityTestItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			qualityTestDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showQualityTestStepsJson @Description: 展示实验步骤 @author
	 *         : @date @param id @param code @return Map<String,Object> @throws
	 */
	public Map<String, Object> showQualityTestStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<QualityTestTemplate> pttList = qualityTestDao.showQualityTestStepsJson(id, code);
		List<QualityTestReagent> ptrList = qualityTestDao.showQualityTestReagentJson(id, code);
		List<QualityTestCos> ptcList = qualityTestDao.showQualityTestCosJson(id, code);
		List<Object> plate = qualityTestDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showQualityTestResultTableJson @Description: 展示结果 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> showQualityTestResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return qualityTestDao.showQualityTestResultTableJson(id, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> showQualityTestResultItemTableJson(String id, Integer start, Integer length,
			String query, String col, String sort, String itemid, String jiance) throws Exception {
		// List<QualityTestInfoItem> qtiis =
		// commonService.get(QualityTestInfoItem.class, "infoId", itemid);
		// if(qtiis.size()>0){
		//
		// }else{
		// List<SampleDeteyion> sds = commonService.get(SampleDeteyion.class,
		// "name",
		// jiance);
		// if(sds.size()>0){
		// List<SampleDeteyionItem> sdis =
		// commonService.get(SampleDeteyionItem.class,
		// "sampleDeteyion.id", sds.get(0).getId());
		// if(sdis.size()>0){
		// for(SampleDeteyionItem sdi:sdis){
		// QualityTestInfoItem qdi = new QualityTestInfoItem();
		// qdi.setInfoId(itemid);
		// qdi.setResultName(sdi.getName());
		// qdi.setResultReferenceValue(sdi.getNote());
		// commonDAO.saveOrUpdate(qdi);
		// }
		// }
		// }
		// }
		return qualityTestDao.showQualityTestResultItemTableJson(id, start, length, query, col, sort, itemid, jiance);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void showQualityTestResultItemTableJson(String itemid, String jiance) throws Exception {
		List<QualityTestInfoItem> qtiis = commonService.get(QualityTestInfoItem.class, "infoId", itemid);
		if (qtiis.size() > 0) {

		} else {
			List<SampleDeteyion> sds = commonService.get(SampleDeteyion.class, "id", jiance);
			if (sds.size() > 0) {
				List<SampleDeteyionItem> sdis = commonService.get(SampleDeteyionItem.class, "sampleDeteyion.id",
						sds.get(0).getId());
				if (sdis.size() > 0) {
					for (SampleDeteyionItem sdi : sdis) {
						QualityTestInfoItem qdi = new QualityTestInfoItem();
						qdi.setInfoId(itemid);
						qdi.setResultName(sdi.getName());
						qdi.setResultReferenceValue(sdi.getNote());

						qdi.setTestingCriteria(sdi.getTestingCriteria());
						commonDAO.saveOrUpdate(qdi);
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @param id @param
	 *         counts @return Map<String,Object> @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		QualityTest pt = commonDAO.get(QualityTest.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<QualityTestItem> list = qualityTestDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent @Description: 上传结果 @author : @date @param id @param
	 *         fileId @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = qualityTestDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			QualityTest pt = qualityTestDao.get(QualityTest.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<QualityTestInfo> listPTI = qualityTestDao.findQualityTestInfoByCode(code);
						if (listPTI.size() > 0) {
							QualityTestInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							qualityTestDao.saveOrUpdate(spi);
						}

					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @param id @param
	 *         templateJson @param reagentJson @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson, String cosJson, String logInfo) {
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("QualityTest");
			li.setFileId(id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}") && !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}") && !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate @Description: 保存模板明细 @author : @date @param
	 *         id @param templateJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			QualityTestTemplate ptt = qualityTestDao.get(QualityTestTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			qualityTestDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent @Description: 保存试剂 @author : @date @param id @param
	 *         reagentJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {

		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			QualityTestReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(QualityTestReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

				String oneNum = (String) object.get("oneNum");
				if (oneNum != null && !"".equals(oneNum)) {
					ptr.setOneNum(Double.valueOf(oneNum));
				} else {
					ptr.setOneNum(null);
				}
				String num = (String) object.get("num");
				if (num != null && !"".equals(num)) {
					ptr.setNum(Double.valueOf(num));
				} else {
					ptr.setOneNum(null);
				}
			} else {
				ptr = new QualityTestReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					String oneNum = (String) object.get("oneNum");
					if (oneNum != null && !"".equals(oneNum)) {
						ptr.setOneNum(Double.valueOf(oneNum));
					} else {
						ptr.setOneNum(null);
					}
					String num = (String) object.get("num");
					if (num != null && !"".equals(num)) {
						ptr.setNum(Double.valueOf(num));
					} else {
						ptr.setOneNum(null);
					}
					ptr.setQualityTest(commonDAO.get(QualityTest.class, id));
				}

			}
			qualityTestDao.saveOrUpdate(ptr);
		}

	}

	/**
	 * 
	 * @Title: saveCos @Description: 保存设备 @author : @date @param id @param
	 *         cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {

		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			QualityTestCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(QualityTestCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new QualityTestCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setQualityTest(commonDAO.get(QualityTest.class, id));
				}
			}
			qualityTestDao.saveOrUpdate(ptc);
		}

	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 孔板样本列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return qualityTestDao.plateSampleTable(id, counts, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @param id @throws
	 *         Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {

		List<QualityTestItem> list = qualityTestDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<QualityTestInfo> spiList = qualityTestDao.selectResultListById(id);
		for (QualityTestItem pti : list) {
			boolean b = true;
			for (QualityTestInfo spi : spiList) {
				if (pti.getQuality() != null && "1".equals(pti.getQuality())) {

				} else {
					if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
						b = false;
					}
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null && !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							QualityTestInfo scp = new QualityTestInfo();

							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setSampleInfo(pti.getSampleInfo());
							scp.setQualityTest(pti.getQualityTest());
							scp.setExperimentalSteps(pti.getExperimentalSteps());
							scp.setSampleDeteyion(pti.getSampleDeteyion());
							scp.setOrderId(pti.getOrderId());
							scp.setResult("1");
							scp.setConcentration(pti.getConcentration());
							scp.setInternalReference(pti.getInternalReference());
							scp.setExperimentalStepsName(pti.getExperimentalStepsName());
							scp.setSampleNumUnit(pti.getSampleNumUnit());

							if ("3".equals(pti.getQualityTest().getCellType())
									|| "7".equals(pti.getQualityTest().getCellType())) {
								scp.setCode(pti.getCode());
								scp.setSampleNumber(pti.getSampleNumber());
							} else {
								// DicSampleType ds =
								// commonDAO.get(DicSampleType.class,
								// pti.getDicSampleTypeId());
								// if (ds != null) {
								// scp.setDicSampleType(ds);
								// }
								// String markCode = "";
								// if (scp.getDicSampleType() != null) {
								// DicSampleType d =
								// commonDAO.get(DicSampleType.class,
								// scp.getDicSampleType().getId());
								// String[] sampleCode =
								// scp.getSampleCode().split(",");
								// if (sampleCode.length > 1) {
								// String str = sampleCode[0].substring(0,
								// sampleCode[0].length() - 4);
								// for (int j = 0; j < sampleCode.length; j++) {
								// str +=
								// sampleCode[j].substring(sampleCode[j].length()
								// - 4,
								// sampleCode[j].length());
								// }
								// if (d == null) {
								// markCode = str;
								// } else {
								// markCode = str + d.getCode();
								// }
								// } else {
								// if (d == null) {
								// markCode = scp.getSampleCode();
								// } else {
								// markCode = scp.getSampleCode() + d.getCode();
								// }
								// }
								//
								// }
								// String code =
								// codingRuleService.getCode("QualityTestInfo",
								// markCode, 00, 2, null);
								scp.setCode(pti.getCode());
								scp.setSampleNumber(pti.getSampleNumber());
							}

							if (pti.getSampleOrder() != null) {
								scp.setSampleOrder(commonDAO.get(SampleOrder.class, pti.getSampleOrder().getId()));
							}
							scp.setMark(pti.getMark());
							scp.setZjName(pti.getZjName());

							scp.setStoragea(pti.getStoragea());
							scp.setSerial(pti.getSerial());

							scp.setBatch(pti.getBatch());

							scp.setCellSampleTableId(pti.getCellSampleTableId());

							scp.setNote(pti.getNote());

							scp.setSampleType(pti.getSampleType());

							scp.setQualitySubmitTime(pti.getQualitySubmitTime());

							qualityTestDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null && !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								QualityTestInfo scp = new QualityTestInfo();

								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setSampleInfo(pti.getSampleInfo());
								scp.setExperimentalSteps(pti.getExperimentalSteps());
								scp.setExperimentalStepsName(pti.getExperimentalStepsName());
								scp.setSampleNumUnit(pti.getSampleNumUnit());
								scp.setSampleDeteyion(pti.getSampleDeteyion());
								scp.setOrderId(pti.getOrderId());
								scp.setQualityTest(pti.getQualityTest());
								scp.setConcentration(pti.getConcentration());
								scp.setInternalReference(pti.getInternalReference());
								scp.setResult("1");

								if ("3".equals(pti.getQualityTest().getCellType())
										|| "7".equals(pti.getQualityTest().getCellType())) {
									scp.setCode(pti.getCode());
									scp.setSampleNumber(pti.getSampleNumber());
									map.put(String.valueOf(pti.getBlendCode()), pti.getCode());
								} else {
									// DicSampleType ds =
									// commonDAO.get(DicSampleType.class,
									// pti.getDicSampleTypeId());
									// if (ds != null) {
									// scp.setDicSampleType(ds);
									// }
									// String markCode = "";
									// if (scp.getDicSampleType() != null) {
									// DicSampleType d =
									// commonDAO.get(DicSampleType.class,
									// scp.getDicSampleType().getId());
									// String[] sampleCode =
									// scp.getSampleCode().split(",");
									// if (sampleCode.length > 1) {
									// String str = sampleCode[0].substring(0,
									// sampleCode[0].length() - 4);
									// for (int j = 0; j < sampleCode.length;
									// j++) {
									// str +=
									// sampleCode[j].substring(sampleCode[j].length()
									// - 4,
									// sampleCode[j].length());
									// }
									// if (d == null) {
									// markCode = str;
									// } else {
									// markCode = str + d.getCode();
									// }
									// } else {
									// if (d == null) {
									// markCode = scp.getSampleCode();
									// } else {
									// markCode = scp.getSampleCode() +
									// d.getCode();
									// }
									// }
									//
									// }
									// String code =
									// codingRuleService.getCode("QualityTestInfo",
									// markCode, 00, 2, null);
									scp.setCode(pti.getCode());
									scp.setSampleNumber(pti.getSampleNumber());
									map.put(String.valueOf(pti.getBlendCode()), pti.getCode());
								}

								if (pti.getSampleOrder() != null) {
									scp.setSampleOrder(commonDAO.get(SampleOrder.class, pti.getSampleOrder().getId()));
								}
								scp.setMark(pti.getMark());
								scp.setZjName(pti.getZjName());

								scp.setStoragea(pti.getStoragea());
								scp.setSerial(pti.getSerial());

								scp.setBatch(pti.getBatch());

								scp.setCellSampleTableId(pti.getCellSampleTableId());

								scp.setNote(pti.getNote());

								scp.setSampleType(pti.getSampleType());

								scp.setQualitySubmitTime(pti.getQualitySubmitTime());
								qualityTestDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti.getBlendCode()));
						QualityTestInfo spi = qualityTestDao.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + "," + pti.getSampleCode());
						qualityTestDao.saveOrUpdate(spi);
					}
				}
			}
			if ("3".equals(pti.getQualityTest().getCellType()) || "7".equals(pti.getQualityTest().getCellType())) {
				String hsbg = "质检结果：" + "原辅料编号" + pti.getStoragea().getId() + "名称" + pti.getStoragea().getName()
						+ "的结果已生成";
				if (hsbg != null && !"".equals(hsbg)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					User u = (User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					li.setUserId(u.getId());
					li.setClassName("QualityTest");
					li.setFileId(pti.getQualityTest().getId());
					li.setModifyContent(hsbg);
					li.setState("3");
					li.setStateName("数据修改");
					commonDAO.saveOrUpdate(li);
				}
			} else {

				String hsbg = "质检结果：" + "产品批号:" + pti.getSampleCode() + ",样本类型:" + pti.getSampleType() + ",步骤"
						+ pti.getExperimentalSteps() + ",步骤名称" + pti.getExperimentalStepsName() + ",质检项:"
						+ pti.getZjName() + "的结果已生成";
				if (hsbg != null && !"".equals(hsbg)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					User u = (User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					li.setUserId(u.getId());
					li.setClassName("QualityTest");
					li.setFileId(pti.getQualityTest().getId());
					li.setModifyContent(hsbg);
					li.setState("3");
					li.setStateName("数据修改");
					commonDAO.saveOrUpdate(li);
				}
			}
		}

	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果 @author : @date @param id @param
	 *         dataJson @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo, String confirmUser) throws Exception {

		List<QualityTestInfo> saveItems = new ArrayList<QualityTestInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		QualityTest pt = commonDAO.get(QualityTest.class, id);
		for (Map<String, Object> map : list) {
			QualityTestInfo scp = new QualityTestInfo();
			// 将map信息读入实体类
			scp = (QualityTestInfo) qualityTestDao.Map2Bean(map, scp);
			scp.setQualityTest(pt);
			saveItems.add(scp);
		}
		if (confirmUser != null && !"".equals(confirmUser)) {
			User confirm = commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			qualityTestDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("QualityTest");
			li.setFileId(id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		qualityTestDao.saveOrUpdateAll(saveItems);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResultItem(String dataJson, String logInfo) throws Exception {

		List<QualityTestInfoItem> saveItems = new ArrayList<QualityTestInfoItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		for (Map<String, Object> map : list) {
			QualityTestInfoItem scp = new QualityTestInfoItem();
			// 将map信息读入实体类
			scp = (QualityTestInfoItem) qualityTestDao.Map2Bean(map, scp);
			saveItems.add(scp);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("QualityTest");
				li.setModifyContent(logInfo);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}

		qualityTestDao.saveOrUpdateAll(saveItems);

	}

	public List<QualityTestItem> findQualityTestItemList(String scId) throws Exception {
		List<QualityTestItem> list = qualityTestDao.selectQualityTestItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return qualityTestDao.generateBlendCode(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(QualityTest pt, String[] ids, String logInfo) throws Exception {
		List<QualityTestItem> qtiList = qualityTestDao.selectQualityTestItemList(pt.getId());
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				QualityTestItem pti = new QualityTestItem();
				pti.setQualityTest(pt);
				pti.setCode(ids[i]);
				pti.setSampleNumber(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setSampleDeteyion(qtiList.get(0).getSampleDeteyion());
				pti.setTestItem(qtiList.get(0).getTestItem());
				pti.setZjName(qtiList.get(0).getZjName());
				pti.setQuality("1");
				pti.setProductNum("1");
				pti.setState("1");
				qualityTestDao.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setClassName("QualityTest");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRightQuality(QualityTest pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			List<QualityTestItem> qtiList = qualityTestDao.selectQualityTestItemList(pt.getId());
			for (int i = 0; i < ids.length; i++) {
				QualityTestItem pti = new QualityTestItem();
				pti.setQualityTest(pt);
				pti.setCode(ids[i]);
				pti.setSampleNumber(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setSampleDeteyion(qtiList.get(0).getSampleDeteyion());
				pti.setTestItem(qtiList.get(0).getTestItem());
				pti.setZjName(qtiList.get(0).getZjName());
				pti.setQuality("1");
				pti.setProductNum("1");
				pti.setState("1");
				commonDAO.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setClassName("QualityTest");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public QualityTestInfo getInfoById(String idc) {
		return qualityTestDao.get(QualityTestInfo.class, idc);

	}

	public List<QualityTestTemp> findQualityTestTemp(String code) {
		return qualityTestDao.findQualityTestTemp(code);

	}

	/**
	 * @Title: findQualityTestResultbyId @Description:根据主表id查询明细栏的实验步骤
	 *         返回给结果栏来显示隐藏字段 @author : nan.jiang @date 2018-8-29下午1:43:58 @param
	 *         id @return List<QualityTestItem> @throws
	 */
	public List<SampleDeteyionItem> findQualityTestResultbyId(String id) {
		List<SampleDeteyion> list = new ArrayList<SampleDeteyion>();
		List<SampleDeteyionItem> sdItems = new ArrayList<SampleDeteyionItem>();
		List<QualityTestItem> itemList = qualityTestDao.findQualityTestResultbyId(id);
		for (QualityTestItem item : itemList) {
			SampleDeteyion sampleDeteyion = item.getSampleDeteyion();
			if (sampleDeteyion != null) {
				list.add(sampleDeteyion);
			}
		}
		for (SampleDeteyion sd : list) {
			if (sd != null) {
				sdItems = sampleDeteyionDao.fingItemListBySampleDeteyion(sd.getId());
			}
		}
		return sdItems;
	}

	/**
	 * @throws Exception
	 * @Title: showQualityTestResultTableDialogJson @Description: TODO @author :
	 *         nan.jiang @date 2018-8-31下午2:56:51 @param id @param start @param
	 *         length @param query @param col @param sort @param orderId @param
	 *         experimentalSteps @param sampleDeteyion @return
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> showQualityTestResultTableDialogJson(String id, Integer start, Integer length,
			String query, String col, String sort, String orderId, String experimentalSteps, String sampleDeteyion)
			throws Exception {
		// TODO Auto-generated method stub
		return qualityTestDao.showQualityTestResultTableDialogJson(id, start, length, query, col, sort, orderId,
				experimentalSteps, sampleDeteyion);
	}

	public List<QualityTestItem> findQualityTestItemTestItemText(String id) {
		return qualityTestDao.findQualityTestItemTestItemText(id);
	}

	public Map<String, Object> findResultForzj(String stepNum, String mark, String testId, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return qualityTestDao.findResultForzj(stepNum, mark, testId, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveUser(String type, String zid, String uid) {
		if ("1".equals(type)) {
			User user = new User();
			user = commonDAO.get(User.class, uid);
			QualityTest qt = new QualityTest();
			qt = commonDAO.get(QualityTest.class, zid);
			qt.setConfirmUserOne(user);
			commonDAO.saveOrUpdate(qt);
		}
		if ("2".equals(type)) {
			User user = new User();
			user = commonDAO.get(User.class, uid);
			QualityTest qt = new QualityTest();
			qt = commonDAO.get(QualityTest.class, zid);
			qt.setConfirmUser(user);
			commonDAO.saveOrUpdate(qt);
		}
	}

	public Map<String, Object> findResultAllForzj(String testId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		if (testId != null && !"".equals(testId)) {
			List<CellPassageItem> cpis = commonService.get(CellPassageItem.class, "cellPassage.id", testId);
			if (cpis.size() > 0) {
				testId = cpis.get(0).getOrderCode();
			} else {
				testId = "XXXXXXXX";
			}
		} else {
			testId = "XXXXXXXX";
		}
		return qualityTestDao.findResultAllForzj(testId, start, length, query, col, sort);
	}

	public String getTemplate(String proId, String id) {
		String productId = proId;
		String[] pro = productId.split(",");
		SampleDeteyion sd = qualityTestDao.get(SampleDeteyion.class, pro[0]);
		if (id.equals("NEW")) {
			if (sd != null) {
				if (sd.getTemplate() != null) {
					return sd.getTemplate().getId();
				}
			}
		}
		return productId;
	}

	public String exeEvent(String applicationTypeActionId, String formId) throws Exception {
		ObjectEvent objectEvent = null;
		ApplicationTypeAction ata = applicationTypeActionDao.get(ApplicationTypeAction.class, applicationTypeActionId);
		String r = "";
		if (ata != null) {
			if (ata.getClassPath() != null && !ata.getClassPath().equals(""))
				objectEvent = (ObjectEvent) Class.forName(ata.getClassPath()).newInstance();
			if (objectEvent != null) {

				r = objectEvent.operation(ata.getId(), formId);
			}
		}

		String hsbg = "质检结果:状态由新建改为完成";
		if (hsbg != null && !"".equals(hsbg)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setClassName("QualityTest");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(hsbg);
			commonDAO.saveOrUpdate(li);
		}
		return r;
	}

	public void saveCpqi(CellPassageQualityItem cpqi) {
		commonDAO.saveOrUpdate(cpqi);
	}

	public void saveInfoQuality(ReagentTaskInfoQuality infoQuality) {
		commonDAO.saveOrUpdate(infoQuality);
	}

	public void savePti(QualityTestItem pti) {
		commonDAO.saveOrUpdate(pti);
	}

	public void savePt(QualityTest pt) {
		commonDAO.saveOrUpdate(pt);
	}

	/**
	 * 
	 * @Title: dnaTaskManageItemRuku @Description: 入库 @author : @date @param ids
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void qualityTestRuku(String[] ids) {
		for (String id : ids) {
			QualityTestTemp scp = qualityTestDao.get(QualityTestTemp.class, id);
			if (scp != null) {
				scp.setState("2");

				SampleInItemTemp st = new SampleInItemTemp();
				if (scp.getSampleNumber() == null) {
					st.setCode(scp.getSampleNumber());
					st.setSampleCode(scp.getSampleNumber());
				} else {
					st.setCode(scp.getSampleNumber());
					st.setSampleCode(scp.getSampleNumber());
				}

				st.setProductName(scp.getProductName());
				st.setProductId(scp.getProductId());
				st.setSampleOrder(scp.getSampleOrder());
				// batch
				st.setSampleType(scp.getSampleType());
				// experimentalSteps
				// experimentalStepsName
				if (scp.getSampleNum() != null && !"".equals(scp.getSampleNum())) {
					st.setNum(Double.valueOf(scp.getSampleNum()));
					st.setSumTotal(Double.valueOf(scp.getSampleNum()));
				}
				// sampleNumUnit
				if (scp.getSampleDeteyion() != null) {
					st.setSampleDeteyionId(scp.getSampleDeteyion().getId());
					st.setSampleDeteyionName(scp.getSampleDeteyion().getName());
				}
				st.setInfoFrom("QualityTestTemp");
				st.setInfoFromId(scp.getId());

				st.setState("1");

				qualityTestDao.saveOrUpdate(st);
			}

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFileDOC(String id) throws Exception {
		StringBuffer sb = null;
		String reportStrInfo = null;
		String fileId = "";
		String name = "";
		Map<String, String> map = new HashMap<String, String>();
		// 格式化日期
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {
			QualityTest sri = commonDAO.get(QualityTest.class, id);
			// 获取供试品List
			List<QualityTestItem> qualityTestItem = qualityTestDao.findQualityTestItemTestItemText(id);
			// 获取试剂List
			List<QualityTestReagent> qualityTestReagent = qualityTestDao.delReagentItem(id);
			// 获取仪器设备List
			List<QualityTestCos> qualityTestCos = qualityTestDao.delCosItem(id);
			// 遍历获取供试品List
			if (qualityTestItem != null && qualityTestItem.size() > 0) {
				for (int i = 0; i < 100; i++) {
					if (i < qualityTestItem.size()) {
						// 获取供试品名称供试品名称
						map.put("${sampleType" + i + "}", qualityTestItem.get(i).getSampleType() != null
								? qualityTestItem.get(i).getSampleType() : "");// 样本类型
						// 供试品批号
						map.put("${batch" + i + "}",
								qualityTestItem.get(i).getBatch() != null ? qualityTestItem.get(i).getBatch() : "");// 产品批号
						// 供试品编号
						map.put("${sampleNumber" + i + "}", qualityTestItem.get(i).getSampleNumber() != null
								? qualityTestItem.get(i).getSampleNumber() : "");// 样本编号
					} else {
						map.put("${sampleType" + i + "}", "");// 样本类型
						map.put("${batch" + i + "}", "");// 产品批号
						map.put("${sampleNumber" + i + "}", "");// 样本编号
					}
				}
			}

			// 遍历获取试剂List

			// 默认100行，没有到话将占位符替换成空
			for (int i = 0; i < 100; i++) {
				if (qualityTestReagent != null && qualityTestReagent.size() > 0) {
					Storage storage = null;
					if (i < qualityTestReagent.size()) {
						if (qualityTestReagent.get(i).getCode() != null
								&& !qualityTestReagent.get(i).getCode().equals("")) {
							storage = commonDAO.get(Storage.class, qualityTestReagent.get(i).getCode());
							// 获取试剂名称
							if (storage != null) {
								map.put("${sjname" + i + "}", qualityTestReagent.get(i).getName() != null
										? qualityTestReagent.get(i).getName() : "");// 试剂名称
								map.put("${sjpc" + i + "}", qualityTestReagent.get(i).getBatch() != null
										? qualityTestReagent.get(i).getBatch() : "");// 试剂批次
								if (storage.getProducer() != null) {
									// 获取试剂厂家
									Supplier supplier = commonDAO.get(Supplier.class, storage.getProducer().getId());
									map.put("${sjcj" + i + "}", supplier.getName() != null ? supplier.getName() : "");// 试剂厂家

								}
								map.put("${sjgg" + i + "}", storage.getSpec() != null ? storage.getSpec() : "");// 试剂规格

								// 根据试剂主表id和批次号去查询失效日期
								List<StorageReagentBuySerial> list = qualityTestDao
										.findStorageReagentBuySerialByTaskIdOrPc(storage.getId(),
												qualityTestReagent.get(i).getBatch());
								if (list != null && list.size() > 0) {
									if (list.get(0).getExpireDate() != null) {
										String sjsxq = formatter1.format(list.get(0).getExpireDate());
										map.put("${sjsxq" + i + "}", sjsxq);// 试剂失效期
									}else{
										map.put("${sjsxq" + i + "}", "");// 试剂失效期
									}

								}else{
									map.put("${sjsxq" + i + "}", "");// 试剂失效期
								}
							}
						}
					} else {
						map.put("${sjname" + i + "}", "");// 试剂名称
						map.put("${sjpc" + i + "}", "");// 试剂批次
						map.put("${sjcj" + i + "}", "");// 试剂厂家
						map.put("${sjgg" + i + "}", "");// 试剂规格
						map.put("${sjsxq" + i + "}", "");
					}
				}else{
					map.put("${sjname" + i + "}", "");// 试剂名称
					map.put("${sjpc" + i + "}", "");// 试剂批次
					map.put("${sjcj" + i + "}", "");// 试剂厂家
					map.put("${sjgg" + i + "}", "");// 试剂规格
					map.put("${sjsxq" + i + "}", "");
				}
			}
			// 遍历获取仪器设备List
		
				for (int i = 0; i < 100; i++) {
					if (qualityTestCos != null && qualityTestCos.size() > 0) {
					if (i < qualityTestCos.size()) {
						// 获取仪器设备名称
						map.put("${yqname" + i + "}",
								qualityTestCos.get(i).getName() != null ? qualityTestCos.get(i).getName() : "");// 仪器设备

						// 获取仪器设备编号
						map.put("${yqcode" + i + "}",
								qualityTestCos.get(i).getCode() != null ? qualityTestCos.get(i).getCode() : "");// 仪器编号
					} else {
						map.put("${yqname" + i + "}", "");// 仪器设备
						map.put("${yqcode" + i + "}", "");// 仪器编号

					}
				}else{
					map.put("${yqcode" + i + "}","");// 仪器编号
					map.put("${yqname" + i + "}", "");// 仪器设备
					map.put("${yqname" + i + "}", "");// 仪器设备
					map.put("${yqcode" + i + "}", "");// 仪器编号
				}
			}

			// 读取word源文件
			List<FileInfo> fis = qualityTestDao.getFileInfos(sri.getTemplate().getId());
			String filePath = "";
			if (fis.size() > 0) {
				filePath = fis.get(0).getFilePath();
			}
			FileInputStream fileInputStream = new FileInputStream(filePath);
			XWPFDocument document = new XWPFDocument(fileInputStream);
			document = searchAndReplace(document, map);
			String formFile = ConfigFileUtil.getValueByKey("write_qc_word_path");
			fileInputStream.close();
			File deskFile = new File(formFile, id + ".docx");
			// 写到目标文件
			OutputStream output = new FileOutputStream(deskFile);
			document.write(output);
			output.close();
			try {
				HttpUtils.write("successed");
			} catch (Exception e) {
				e.printStackTrace();
				HttpUtils.write("error");
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	public static XWPFDocument searchAndReplace(XWPFDocument document, Map<String, String> map) {
		try {

			Iterator<XWPFParagraph> itPara = document.getParagraphsIterator();
			while (itPara.hasNext()) {
				XWPFParagraph paragraph = (XWPFParagraph) itPara.next();
				// String s = paragraph.getParagraphText();
				Set<String> set = map.keySet();
				Iterator<String> iterator = set.iterator();
				while (iterator.hasNext()) {
					String key = iterator.next();
					List<XWPFRun> run = paragraph.getRuns();
					for (int i = 0; i < run.size(); i++) {
						if (run.get(i).getText(run.get(i).getTextPosition()) != null
								&& run.get(i).getText(run.get(i).getTextPosition()).equals(key)) {
							/**
							 * 参数0表示生成的文字是要从哪一个地方开始放置,设置文字从位置0开始 就可以把原来的文字全部替换掉了
							 */
							run.get(i).setText(map.get(key), 0);
						}
					}
				}
			}

			// 替换表格中的指定文字
			Iterator<XWPFTable> itTable = document.getTablesIterator();
			while (itTable.hasNext()) {
				XWPFTable table = (XWPFTable) itTable.next();
				int rcount = table.getNumberOfRows();
				for (int i = 0; i < rcount; i++) {
					XWPFTableRow row = table.getRow(i);
					List<XWPFTableCell> cells = row.getTableCells();
					for (XWPFTableCell cell : cells) {
						for (Entry<String, String> e : map.entrySet()) {
							if (cell.getText().contains(e.getKey())) {
								String b = cell.getText();
								System.out.println("b is" + b);

								String a = "";
								if (e.getValue() != null)
									a = b.replace(e.getKey(), e.getValue());
								cell.removeParagraph(0);
								cell.setText(a);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return document;

	}


	public Template findSampeDeteyion(String id) {
		return qualityTestDao.findSampeDeteyion(id);
		
	}

}
