package com.biolims.experiment.quality.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.quality.service.QualityTestResultManageService;
import com.biolims.experiment.quality.service.QualityTestService;

public class QualityTestResultManageEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QualityTestResultManageService mbService = (QualityTestResultManageService) ctx
				.getBean("qualityTestResultManageService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
