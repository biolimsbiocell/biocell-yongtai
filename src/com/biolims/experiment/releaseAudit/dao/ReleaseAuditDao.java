package com.biolims.experiment.releaseAudit.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.releaseAudit.model.ReleaseAudit;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.model.SampleOutApplyItem;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本出库申请
 * 
 * @author admin
 * 
 */
@Repository
@SuppressWarnings("unchecked")
public class ReleaseAuditDao extends BaseHibernateDao {
	public Map<String, Object> selectReleaseAuditList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReleaseAudit where 1=1  ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReleaseAudit where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key += " order by " + col + " " + sort;
			}
			List<ReleaseAudit> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public ReleaseAudit selectDate(String id) {
		String hql = "from  ReleaseAudit where 1=1 and id = '"+ id + "' ";
		ReleaseAudit releaseAudit = (ReleaseAudit) getSession().createQuery(hql).uniqueResult();
		return releaseAudit;
	}
	
	
}