package com.biolims.experiment.releaseAudit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 放行审核
 * @author lims-platform
 * @date 2015-11-03 16:20:05
 * @version V1.0
 * 
 */
@Entity
@Table(name = "RELEASE_AUDIT")
@SuppressWarnings("serial")
public class ReleaseAudit extends EntityDao<ReleaseAudit> implements
		java.io.Serializable {
	/** 放行审核编码 */
	private String id;
	/** 患者 */
	private String patientName;
	/** 样本编号 */
	private String code;
	/** 产品批号 */
	private String productBatchNumber;
	/** 规格 */
	private String specifications;
	/** 数量 */
	private String num;
	/** 医院 */
	private String hospital;
	/** 科室 */
	private String department;
	/** 病历号/门诊号 */
	private String patientId;
	/** 生产指令是否符合工艺规程需求 */
	private String result1;
	/** 是否已按照指令和工艺规程完成所有工序的生产 */
	private String result2;
	/** 所用物料是否已完成所有检查，检验，并经保证部批准放行 */
	private String result3;
	/** 物料平衡是否在规定范围内 */
	private String result4;
	/** 批生产/包装记录是否内容齐全、书写正确、数据完整，已由生产和质量制指定人员完成审核 */
	private String result5;
	/** 所用标签是否正确，批号打印及有效期是否正确 */
	private String result6;
	/** 关键工艺参数设置及控制是否符合工艺要求 */
	private String result7;
	/** 所有与该批产品有关的偏差是否已完成调查和适当处理 */
	private String result8;
	/** 所有与该产品有关的变更是否已按照相关规定处理完毕 */
	private String result9;
	/** 生产条件是否符合工艺要求，生产人员卫生是否符合规定 */
	private String result10;
	/** 中间控制项目是否符合工艺要求，检查结果是否符合标准要求 */
	private String result11;
	/** 环境监控是否符合规定，日常及定期监测结果是否符合标准要求 */
	private String result12;
	/** 中间产品、成品是否已按照质量标准完成检验，检验结果是否符合放行质量标准要求 */
	private String result13;
	/** 批检验记录是否内容齐全、书写正确、数据完整，已由QC和QA指定人员完成审核并签名 */
	private String result14;
	/** 是否依据检验记录出具成品检验报告单，并由检验负责人签名 */
	private String result15;
	/** 是否有检验偏差，如有应执行00S/00T调查程序，并完成相应处理、评估及批准 */
	private String result16;
	/** 审核结论结果 */
	private String result;
	/** 质量保证部负责人 */
	private String chargePerson;
	/** 质量保证时间 */
	private Date qualityAssuranceTime;
	/** 放行结论 */
	private String releaseConclusion;
	/** 质量授权人 */
	private String qualityAuthorizer;
	/** 质量授权日期 */
	private Date qualityAuthorizationDate;
	
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 审核人 */
	private User acceptUser;
	/** 审核日期 */
	private Date acceptDate;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	private String scopeId;
	private String scopeName;
	
	/** CCOI */
	private String ccoi;
	
	
	

	public String getCcoi() {
		return ccoi;
	}

	public void setCcoi(String ccoi) {
		this.ccoi = ccoi;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductBatchNumber() {
		return productBatchNumber;
	}

	public void setProductBatchNumber(String productBatchNumber) {
		this.productBatchNumber = productBatchNumber;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getResult1() {
		return result1;
	}

	public void setResult1(String result1) {
		this.result1 = result1;
	}

	public String getResult2() {
		return result2;
	}

	public void setResult2(String result2) {
		this.result2 = result2;
	}

	public String getResult3() {
		return result3;
	}

	public void setResult3(String result3) {
		this.result3 = result3;
	}

	public String getResult4() {
		return result4;
	}

	public void setResult4(String result4) {
		this.result4 = result4;
	}

	public String getResult5() {
		return result5;
	}

	public void setResult5(String result5) {
		this.result5 = result5;
	}

	public String getResult6() {
		return result6;
	}

	public void setResult6(String result6) {
		this.result6 = result6;
	}

	public String getResult7() {
		return result7;
	}

	public void setResult7(String result7) {
		this.result7 = result7;
	}

	public String getResult8() {
		return result8;
	}

	public void setResult8(String result8) {
		this.result8 = result8;
	}

	public String getResult9() {
		return result9;
	}

	public void setResult9(String result9) {
		this.result9 = result9;
	}

	public String getResult10() {
		return result10;
	}

	public void setResult10(String result10) {
		this.result10 = result10;
	}

	public String getResult11() {
		return result11;
	}

	public void setResult11(String result11) {
		this.result11 = result11;
	}

	public String getResult12() {
		return result12;
	}

	public void setResult12(String result12) {
		this.result12 = result12;
	}

	public String getResult13() {
		return result13;
	}

	public void setResult13(String result13) {
		this.result13 = result13;
	}

	public String getResult14() {
		return result14;
	}

	public void setResult14(String result14) {
		this.result14 = result14;
	}

	public String getResult15() {
		return result15;
	}

	public void setResult15(String result15) {
		this.result15 = result15;
	}

	public String getResult16() {
		return result16;
	}

	public void setResult16(String result16) {
		this.result16 = result16;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getChargePerson() {
		return chargePerson;
	}

	public void setChargePerson(String chargePerson) {
		this.chargePerson = chargePerson;
	}

	public Date getQualityAssuranceTime() {
		return qualityAssuranceTime;
	}

	public void setQualityAssuranceTime(Date qualityAssuranceTime) {
		this.qualityAssuranceTime = qualityAssuranceTime;
	}

	public String getReleaseConclusion() {
		return releaseConclusion;
	}

	public void setReleaseConclusion(String releaseConclusion) {
		this.releaseConclusion = releaseConclusion;
	}

	public String getQualityAuthorizer() {
		return qualityAuthorizer;
	}

	public void setQualityAuthorizer(String qualityAuthorizer) {
		this.qualityAuthorizer = qualityAuthorizer;
	}

	public Date getQualityAuthorizationDate() {
		return qualityAuthorizationDate;
	}

	public void setQualityAuthorizationDate(Date qualityAuthorizationDate) {
		this.qualityAuthorizationDate = qualityAuthorizationDate;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本出库申请id
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本出库申请id
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 100)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审核人
	 */
	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审核日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审核日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}