package com.biolims.experiment.releaseAudit.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.releaseAudit.dao.ReleaseAuditDao;
import com.biolims.experiment.releaseAudit.model.ReleaseAudit;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleOutApplyDao;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.model.SampleOutApplyItem;
import com.biolims.sample.storage.model.SampleOutTaskId;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本出库申请
 * 
 * @author admin
 * 
 */
@Service
@SuppressWarnings("unchecked")
@Transactional
public class ReleaseAuditService {
	@Resource
	private ReleaseAuditDao releaseAuditDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private SystemCodeService systemCodeService;
	
	
	
	
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findReleaseAuditList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return releaseAuditDao.selectReleaseAuditList(start, length, query, col, sort);
	}
	
	public ReleaseAudit findReleaseAuditById(String id) {
		return releaseAuditDao.get(ReleaseAudit.class, id);
	}
	
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReleaseAudit saveSampleOutApplyById(ReleaseAudit soa, String id, 
			String ReleaseAuditDate,String note, String logInfo) throws Exception {
		ReleaseAudit newSoa = commonDAO.get(ReleaseAudit.class, id);
		if (id == null || id.length() <= 0
				|| "NEW".equals(id)) {
			String code = systemCodeService.getCodeByPrefix("ReleaseAudit", "RP"
					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
					000, 3, null);
			newSoa.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			newSoa.setCreateUser(user);
			newSoa.setCreateDate(new Date());
			newSoa.setState("3");
			newSoa.setStateName("新建");
			newSoa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			newSoa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			id = soa.getId();
			commonDAO.saveOrUpdate(soa);
		}else{
			newSoa = commonDAO.get(ReleaseAudit.class, id);
		}
		
		
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSoa.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		commonDAO.saveOrUpdate(newSoa);
		return newSoa;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReleaseAuditAndItem(ReleaseAudit newSoa, Map jsonMap,
			String logInfo, String logInfoItem) throws Exception {
		if (newSoa != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(newSoa.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveReleaseAudit(String jsonItem) throws Exception {
		String id = "";
		String mainJson = "[" + jsonItem + "]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				mainJson, List.class);
		ReleaseAudit pt = new ReleaseAudit();
		pt = (ReleaseAudit) commonDAO.Map2Bean(list.get(0), pt);
		if(pt.getId()!=null
				&&!"".equals(pt.getId())){
			id = pt.getId();
			releaseAuditDao.saveOrUpdate(pt);
		}
		return pt.getId();
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReleaseAudit selectDate(String id) throws Exception {
		return releaseAuditDao.selectDate(id);
	}
	
}
