package com.biolims.experiment.cfdna.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.cfdna.service.CfdnaTaskService;

public class CfdnaTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CfdnaTaskService mbService = (CfdnaTaskService) ctx
				.getBean("cfdnaTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
