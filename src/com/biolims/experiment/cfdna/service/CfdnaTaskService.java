package com.biolims.experiment.cfdna.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cfdna.dao.CfdnaTaskDao;
import com.biolims.experiment.cfdna.model.CfdnaTask;
import com.biolims.experiment.cfdna.model.CfdnaTaskCos;
import com.biolims.experiment.cfdna.model.CfdnaTaskItem;
import com.biolims.experiment.cfdna.model.CfdnaTaskReagent;
import com.biolims.experiment.cfdna.model.CfdnaTaskResult;
import com.biolims.experiment.cfdna.model.CfdnaTaskTemp;
import com.biolims.experiment.cfdna.model.CfdnaTaskTemplate;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CfdnaTaskService {
	@Resource
	private CfdnaTaskDao cfdnaTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findCfdnaTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return cfdnaTaskDao.selectCfdnaTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CfdnaTask i) throws Exception {

		cfdnaTaskDao.saveOrUpdate(i);

	}

	public CfdnaTask get(String id) {
		CfdnaTask cfdnaTask = commonDAO.get(CfdnaTask.class, id);
		return cfdnaTask;
	}

	public Map<String, Object> findCfdnaTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = cfdnaTaskDao.selectCfdnaTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<CfdnaTaskItem> list = (List<CfdnaTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findCfdnaTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = cfdnaTaskDao.selectCfdnaTaskTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<CfdnaTaskTemplate> list = (List<CfdnaTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findCfdnaTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = cfdnaTaskDao.selectCfdnaTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<CfdnaTaskReagent> list = (List<CfdnaTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findCfdnaTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = cfdnaTaskDao.selectCfdnaTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<CfdnaTaskCos> list = (List<CfdnaTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCfdnaTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = cfdnaTaskDao.selectCfdnaTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<CfdnaTaskResult> list = (List<CfdnaTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findCfdnaTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = cfdnaTaskDao.selectCfdnaTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<CfdnaTaskTemp> list = (List<CfdnaTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCfdnaTaskItem(CfdnaTask sc, String itemDataJson)
			throws Exception {
		List<CfdnaTaskItem> saveItems = new ArrayList<CfdnaTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CfdnaTaskItem scp = new CfdnaTaskItem();
			// 将map信息读入实体类
			scp = (CfdnaTaskItem) cfdnaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCfdnaTask(sc);

			saveItems.add(scp);
		}
		cfdnaTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			CfdnaTaskItem scp = cfdnaTaskDao.get(CfdnaTaskItem.class, id);
			cfdnaTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCfdnaTaskTemplate(CfdnaTask sc, String itemDataJson)
			throws Exception {
		List<CfdnaTaskTemplate> saveItems = new ArrayList<CfdnaTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CfdnaTaskTemplate scp = new CfdnaTaskTemplate();
			// 将map信息读入实体类
			scp = (CfdnaTaskTemplate) cfdnaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCfdnaTask(sc);

			saveItems.add(scp);
		}
		cfdnaTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			CfdnaTaskTemplate scp = cfdnaTaskDao.get(CfdnaTaskTemplate.class,
					id);
			cfdnaTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskTemplateOne(String ids) throws Exception {

		CfdnaTaskTemplate scp = cfdnaTaskDao.get(CfdnaTaskTemplate.class, ids);
		cfdnaTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCfdnaTaskReagent(CfdnaTask sc, String itemDataJson)
			throws Exception {
		List<CfdnaTaskReagent> saveItems = new ArrayList<CfdnaTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CfdnaTaskReagent scp = new CfdnaTaskReagent();
			// 将map信息读入实体类
			scp = (CfdnaTaskReagent) cfdnaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCfdnaTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		cfdnaTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			CfdnaTaskReagent scp = cfdnaTaskDao.get(CfdnaTaskReagent.class, id);
			cfdnaTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskReagentOne(String ids) throws Exception {

		CfdnaTaskReagent scp = cfdnaTaskDao.get(CfdnaTaskReagent.class, ids);
		cfdnaTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCfdnaTaskCos(CfdnaTask sc, String itemDataJson)
			throws Exception {
		List<CfdnaTaskCos> saveItems = new ArrayList<CfdnaTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CfdnaTaskCos scp = new CfdnaTaskCos();
			// 将map信息读入实体类
			scp = (CfdnaTaskCos) cfdnaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCfdnaTask(sc);

			saveItems.add(scp);
		}
		cfdnaTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			CfdnaTaskCos scp = cfdnaTaskDao.get(CfdnaTaskCos.class, id);
			cfdnaTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskCosOne(String ids) throws Exception {
		CfdnaTaskCos scp = cfdnaTaskDao.get(CfdnaTaskCos.class, ids);
		cfdnaTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCfdnaTaskResult(CfdnaTask sc, String itemDataJson)
			throws Exception {
		List<CfdnaTaskResult> saveItems = new ArrayList<CfdnaTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CfdnaTaskResult scp = new CfdnaTaskResult();
			// 将map信息读入实体类
			scp = (CfdnaTaskResult) cfdnaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setCfdnaTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = scp.getSampleCode() + "CF";
				String code = codingRuleService.getCode("CfdnaTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			cfdnaTaskDao.saveOrUpdate(scp);

			if (scp.getResult() != null && scp.getSubmit() != null) {
				if (scp.getSubmit().equals("1")) {// 提交
					if (scp.getResult().equals("1")) {// 合格
						String nextFlowId = scp.getNextFlowId();NextFlow nf = commonService.get(NextFlow.class, nextFlowId);nextFlowId = nf.getSysCode();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getVolume());
							st.setSampleType(scp.getSampleType());
							st.setState("1");
							cfdnaTaskDao.saveOrUpdate(st);
						} else if (nextFlowId.equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (nextFlowId.equals("0011")) {// 重建库
							WkTaskAbnormal wka = new WkTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0012")) {// 暂停

						} else if (nextFlowId.equals("0013")) {// 终止

						} else if (nextFlowId.equals("0020")) {// 2100质控
							Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(qc2100, scp);
							qc2100.setWkType("2");
							cfdnaTaskDao.saveOrUpdate(qc2100);
						} else if (nextFlowId.equals("0021")) {// QPCR质控
							QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(qpcr, scp);
							qpcr.setWkType("1");
							cfdnaTaskDao.saveOrUpdate(qpcr);
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"CfdnaTask",
							"cfDNA质量评估",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			CfdnaTaskResult scp = cfdnaTaskDao.get(CfdnaTaskResult.class, id);
			cfdnaTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCfdnaTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			CfdnaTaskTemp scp = cfdnaTaskDao.get(CfdnaTaskTemp.class, id);
			cfdnaTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CfdnaTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			cfdnaTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("cfdnaTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCfdnaTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("cfdnaTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCfdnaTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("cfdnaTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCfdnaTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("cfdnaTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCfdnaTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("cfdnaTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCfdnaTaskResult(sc, jsonStr);
			}

		}
	}

	// 审核完成
	public void changeState(String applicationTypeActionId, String id) {
		CfdnaTask sct = cfdnaTaskDao.get(CfdnaTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		List<CfdnaTaskItem> item = cfdnaTaskDao.setItemList(sct.getId());
		for (CfdnaTaskItem c : item) {
			CfdnaTaskTemp temp = commonDAO.get(CfdnaTaskTemp.class,
					c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
	}
}
