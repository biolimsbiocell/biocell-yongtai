package com.biolims.experiment.enmonitor.volume.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.volume.service.CleanAreaVolumeService;

public class CleanAreaVolumeEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CleanAreaVolumeService mbService = (CleanAreaVolumeService) ctx
				.getBean("cleanAreaVolumeService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
