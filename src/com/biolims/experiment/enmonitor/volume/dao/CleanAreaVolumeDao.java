package com.biolims.experiment.enmonitor.volume.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.enmonitor.dust.model.DustParticleCos;
import com.biolims.experiment.enmonitor.dust.model.DustParticleItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleReagent;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemp;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemplate;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeItem;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleInfo;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CleanAreaVolumeDao extends BaseHibernateDao {

	public Map<String, Object> findDustParticleTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DustParticle where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from DustParticle where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DustParticle> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectDustParticleTempTable(String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DustParticleTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (codes != null && !codes.equals("")) {
			key += " and code in (:alist)";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			List<DustParticleTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from DustParticleTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			if (codes != null && !codes.equals("")) {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
						.setParameterList("alist", codes).list();
			} else {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findDustParticleItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DustParticleItem where 1=1 and state='1' and dustParticle.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from DustParticleItem  where 1=1 and state='1' and dustParticle.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DustParticleItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<DustParticleItem> showWellPlate(String id) {
		String hql = "from DustParticleItem  where 1=1 and  dustParticle.id='" + id + "'";
		List<DustParticleItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findDustParticleItemAfTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DustParticleItem where 1=1 and state='2' and dustParticle.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from DustParticleItem  where 1=1 and state='2' and dustParticle.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DustParticleItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from DustParticleItem where 1=1 and state='2' and dustParticle.id='" + id
				+ "'";
		String key = "";
		List<String> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "select counts from DustParticleItem where 1=1 and state='2' and dustParticle.id='" + id
					+ "' group by counts";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<DustParticleTemplate> showDustParticleStepsJson(String id, String code) {

		String countHql = "select count(*) from DustParticleTemplate where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and orderNum ='" + code + "'";
		} else {
		}
		List<DustParticleTemplate> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from DustParticleTemplate where 1=1 and dustParticle.id='" + id + "'";
			key += " order by orderNum asc";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<DustParticleReagent> showDustParticleReagentJson(String id, String code) {
		String countHql = "select count(*) from DustParticleReagent where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<DustParticleReagent> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from DustParticleReagent where 1=1 and dustParticle.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<DustParticleCos> showDustParticleCosJson(String id, String code) {
		String countHql = "select count(*) from DustParticleCos where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<DustParticleCos> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from DustParticleCos where 1=1 and dustParticle.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<DustParticleTemplate> delTemplateItem(String id) {
		List<DustParticleTemplate> list = new ArrayList<DustParticleTemplate>();
		String hql = "from DustParticleTemplate where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DustParticleReagent> delReagentItem(String id) {
		List<DustParticleReagent> list = new ArrayList<DustParticleReagent>();
		String hql = "from DustParticleReagent where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DustParticleCos> delCosItem(String id) {
		List<DustParticleCos> list = new ArrayList<DustParticleCos>();
		String hql = "from DustParticleCos where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> showDustParticleResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DustParticleInfo where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from DustParticleInfo  where 1=1 and dustParticle.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DustParticleInfo> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Object> showWellList(String id) {

		String hql = "select counts,count(*) from DustParticleItem where 1=1 and dustParticle.id='" + id
				+ "' group by counts";
		List<Object> list = getSession().createQuery(hql).list();

		return list;
	}

	public List<DustParticleItem> plateSample(String id, String counts) {
		String hql = "from DustParticleItem where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		if (counts == null || counts.equals("")) {
			key = " and counts is null";
		} else {
			key = "and counts='" + counts + "'";
		}
		List<DustParticleItem> list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DustParticleItem where 1=1 and dustParticle.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (counts == null || counts.equals("")) {
			key += " and counts is null";
		} else {
			key += "and counts='" + counts + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from DustParticleItem  where 1=1 and dustParticle.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DustParticleItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<DustParticleInfo> findDustParticleInfoByCode(String code) {
		String hql = "from DustParticleInfo where 1=1 and code='" + code + "'";
		List<DustParticleInfo> list = new ArrayList<DustParticleInfo>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<DustParticleInfo> selectAllResultListById(String code) {
		String hql = "from DustParticleInfo  where (submit is null or submit='') and dustParticle.id='" + code + "'";
		List<DustParticleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<DustParticleInfo> selectResultListById(String id) {
		String hql = "from DustParticleInfo  where dustParticle.id='" + id + "'";
		List<DustParticleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<DustParticleInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from DustParticleInfo t where (submit is null or submit='') and id in (" + insql + ")";
		List<DustParticleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<DustParticleItem> selectDustParticleItemList(String scId) throws Exception {
		String hql = "from DustParticleItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dustParticle.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<DustParticleItem> list = new ArrayList<DustParticleItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Integer generateBlendCode(String id) {
		String hql = "select max(blendCode) from DustParticleItem where 1=1 and dustParticle.id='" + id + "'";
		Integer blendCode = (Integer) this.getSession().createQuery(hql).uniqueResult();
		return blendCode;
	}

	public DustParticleInfo getResultByCode(String code) {
		String hql = " from DustParticleInfo where 1=1 and code='" + code + "'";
		DustParticleInfo spi = (DustParticleInfo) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}
	

	// 尘埃粒子主表列表
	public Map<String, Object> findCleanAreaMicrodeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaVolume where 1=1";
		String key = "";
		/*
		 * if(counts==null||counts.equals("")){ key+=" and counts is null";
		 * }else{ key+="and counts='"+counts+"'"; }
		 */
		if (query != null) {
			key = key+map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaVolume  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 尘埃粒子子表列表
	public Map<String, Object> findCleanAreaMicrodeItemTable(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaVolumeItem where 1=1 and cleanAreaVolume.id='" + id + "'";
		String key = "";
		if(map2Query != null) {
			key = map2where(map2Query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaVolumeItem where 1=1 and cleanAreaVolume.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CleanAreaBacteriaItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	public List<CleanAreaVolumeItem> findCleanAreaVolumeItemTable(String id){
		List<CleanAreaVolumeItem> list = new ArrayList<CleanAreaVolumeItem>();
		if (null != id && !"".equals(id)) {
			String hql = "from CleanAreaVolumeItem where 1=1 and cleanAreaVolume.id='" + id + "'";
			list = getSession().createQuery(hql).list();
		}
		return list;
	}
}