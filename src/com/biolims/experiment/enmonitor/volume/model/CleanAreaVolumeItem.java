package com.biolims.experiment.enmonitor.volume.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 风量 子表
 * @author lims-platform
 * @date 2019-3-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_volume_item")
@SuppressWarnings("serial")
public class CleanAreaVolumeItem extends EntityDao<CleanAreaVolumeItem> implements
		java.io.Serializable {
	
	/** 编号 */
	private String id;
	/** 房间名称 */
	@Column(name="room_name")
	private String roomName;
	/**房间编号*/
	@Column(name="room_num")
	private String roomNum;
	/**送风口1 */
	@Column(name="air_outlet_one")
	private String airOutletOne;
	/** 送风口2 */
	@Column(name="air_outlet_tow")
	private String airOutletTow;
	/** 送风口3 */
	@Column(name="air_outlet_three")
	private String airOutletThree;
	/** 送风口4 */
	@Column(name="air_outlet_four")
	private String airOutletFour;
	/** 备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**送风口 */
	@SuppressWarnings("unused")
	@Column(name="air_outlet")
	private String airOutlet;
	/**送风口情况 */
	@Column(name="air_outlet_situation")
	private String airOutletSituation;
	/**监测点 */
	@Column(name="MONITORING_POINT")
	private String monitoringPoint;
	/**编号 */
	@Column(name="SERIAL_NUMBER")
	private String serialNumber;
	/**风速 */
	@Column(name="WIND_SPEED")
	private String windSpeed;
	/**合格标准 */
	@Column(name="ELIGIBILITY_CRITERIA")
	private String eligibilityCriteria;
	/**是否合格标准 */
	@Column(name="DIELIGIBILITY_CRITERIA")
	private String dieligibilityCriteria;
	/**备注 */
	@Column(name="REMARK")
	private String remark;
	/**风量*/
	private String volumeMeter;
	/**总送风量*/
	private String qvent;
	/**房间面积*/
	private String centiare;
	/**房间体积*/
	private String stere;

	
	

	public String getCentiare() {
		return centiare;
	}

	public void setCentiare(String centiare) {
		this.centiare = centiare;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

	/**换气次数*/
	private String ventilationRate;
	
	
	
	public String getVolumeMeter() {
		return volumeMeter;
	}

	public void setVolumeMeter(String volumeMeter) {
		this.volumeMeter = volumeMeter;
	}

	public String getQvent() {
		return qvent;
	}

	public void setQvent(String qvent) {
		this.qvent = qvent;
	}

	

	public String getStere() {
		return stere;
	}

	public void setStere(String stere) {
		this.stere = stere;
	}

	public String getVentilationRate() {
		return ventilationRate;
	}

	public void setVentilationRate(String ventilationRate) {
		this.ventilationRate = ventilationRate;
	}

	public String getMonitoringPoint() {
		return monitoringPoint;
	}

	public void setMonitoringPoint(String monitoringPoint) {
		this.monitoringPoint = monitoringPoint;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(String windSpeed) {
		this.windSpeed = windSpeed;
	}

	public String getEligibilityCriteria() {
		return eligibilityCriteria;
	}

	public void setEligibilityCriteria(String eligibilityCriteria) {
		this.eligibilityCriteria = eligibilityCriteria;
	}

	public String getDieligibilityCriteria() {
		return dieligibilityCriteria;
	}

	public void setDieligibilityCriteria(String dieligibilityCriteria) {
		this.dieligibilityCriteria = dieligibilityCriteria;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**关联主表 */
	private CleanAreaVolume cleanAreaVolume;
	
	public String getAirOutlet() {
		return airOutlet;
	}

	public void setAirOutlet(String airOutlet) {
		this.airOutlet = airOutlet;
	}

	public String getAirOutletSituation() {
		return airOutletSituation;
	}

	public void setAirOutletSituation(String airOutletSituation) {
		this.airOutletSituation = airOutletSituation;
	}

	/**
	 * 方法: 关联的主表 
	 * 
	 * @return: 关联的主表 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "clean_area_volume")
	public CleanAreaVolume getCleanAreaVolume() {
		return cleanAreaVolume;
	}

	public void setCleanAreaVolume(CleanAreaVolume cleanAreaVolume) {
		this.cleanAreaVolume = cleanAreaVolume;
	}
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	public String getAirOutletOne() {
		return airOutletOne;
	}

	public void setAirOutletOne(String airOutletOne) {
		this.airOutletOne = airOutletOne;
	}

	public String getAirOutletTow() {
		return airOutletTow;
	}

	public void setAirOutletTow(String airOutletTow) {
		this.airOutletTow = airOutletTow;
	}

	public String getAirOutletThree() {
		return airOutletThree;
	}

	public void setAirOutletThree(String airOutletThree) {
		this.airOutletThree = airOutletThree;
	}

	public String getAirOutletFour() {
		return airOutletFour;
	}

	public void setAirOutletFour(String airOutletFour) {
		this.airOutletFour = airOutletFour;
	}

	
}