package com.biolims.experiment.enmonitor.volume.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;

/**
 * @Title: Model
 * @Description: 风量  主表
 * @author lims-platform
 * @date 2019-03-22 17:06:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_volume")
@SuppressWarnings("serial")
public class CleanAreaVolume extends EntityDao<CleanAreaVolume> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	@Column(name="name")
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	@Column(name="create_date")
	private String createDate;
	/** 车间部门 */
	@Column(name="workshop_department")
	private String workshopDepartment;
	/** 区域 */
	@Column(name="region")
	private String region;
	/** 区域iD */
	@Column(name="regionId")
	private String regionId;
	/** 测量设备及编号*/
	@Column(name="measuring_instrument_code")
	private String measuringInstrumentCode;
	/** 测试人 */
	@Column(name="test_user")
	private String testUser;
	/** 洁净区状态   0动态   1静态*/
	@Column(name="clean_zone_status")
	private String cleanZoneStatus;
	/** 洁净区换气次数房间等级 */
	@Column(name="clean_zone_grade")
	private String cleanZoneGrade;
	/** 洁净区换气数房间为 */
	@Column(name="clean_zone_roomrating")
	private String cleanZoneRoomrating;
	/**不符合的房间为*/
	@Column(name="not_room_rating")
	private String notRoomRating;
	/**备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**类型*/
	@Column(name="type")
	private String type;
	/** 审核人 */
	private User confirmUser;
	/**状态名称 */
	@Column(name="STATE_NAME")
	private String stateName;
	/** 测试时间 */
	@Column(name="TEST_DATE")
	private String testDate;
	/** 复核时间*/
	private String confirmDate;
	/** 通知人id */
	private String notifierId;
	/** 通知人姓名 */
	private String notifierName;
	/**洁净区级别*/
	private String cleanlinessClass;
	/** 警戒线 */
	private String cordon;
	/** 纠偏线 */
	private String deviationCorrectionLine;
	
	//批准人
	private User approver;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPROVER")
	public User getApprover() {
		return approver;
	}

	public void setApprover(User approver) {
		this.approver = approver;
	}
	
	public String getCordon() {
		return cordon;
	}

	public void setCordon(String cordon) {
		this.cordon = cordon;
	}

	public String getDeviationCorrectionLine() {
		return deviationCorrectionLine;
	}

	public void setDeviationCorrectionLine(String deviationCorrectionLine) {
		this.deviationCorrectionLine = deviationCorrectionLine;
	}

	public String getCleanlinessClass() {
		return cleanlinessClass;
	}

	public void setCleanlinessClass(String cleanlinessClass) {
		this.cleanlinessClass = cleanlinessClass;
	}

	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getNotifierId() {
		return notifierId;
	}

	public void setNotifierId(String notifierId) {
		this.notifierId = notifierId;
	}

	public String getNotifierName() {
		return notifierName;
	}

	public void setNotifierName(String notifierName) {
		this.notifierName = notifierName;
	}

	public String getTestDate() {
		return testDate;
	}

	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	public String getWorkshopDepartment() {
		return workshopDepartment;
	}

	public void setWorkshopDepartment(String workshopDepartment) {
		this.workshopDepartment = workshopDepartment;
	}
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	public String getNotRoomRating() {
		return notRoomRating;
	}

	public void setNotRoomRating(String notRoomRating) {
		this.notRoomRating = notRoomRating;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	public String getMeasuringInstrumentCode() {
		return measuringInstrumentCode;
	}

	public void setMeasuringInstrumentCode(String measuringInstrumentCode) {
		this.measuringInstrumentCode = measuringInstrumentCode;
	}

	public String getCleanZoneStatus() {
		return cleanZoneStatus;
	}

	public void setCleanZoneStatus(String cleanZoneStatus) {
		this.cleanZoneStatus = cleanZoneStatus;
	}

	public String getCleanZoneGrade() {
		return cleanZoneGrade;
	}

	public void setCleanZoneGrade(String cleanZoneGrade) {
		this.cleanZoneGrade = cleanZoneGrade;
	}

	public String getCleanZoneRoomrating() {
		return cleanZoneRoomrating;
	}

	public void setCleanZoneRoomrating(String cleanZoneRoomrating) {
		this.cleanZoneRoomrating = cleanZoneRoomrating;
	}

	public String getTestUser() {
		return testUser;
	}

	public void setTestUser(String testUser) {
		this.testUser = testUser;
	}

	

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	
}