﻿package com.biolims.experiment.enmonitor.template.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicState;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.template.dao.RoomTemplateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplate;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class RoomTemplateService {

	@Resource
	private RoomTemplateDao roomTemplateDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;

	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	@Resource
	private CommonService commonService;

	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         DustParticle @throws
	 */
	public RoomTemplate get(String id) {
		RoomTemplate roomTemplate = commonDAO.get(RoomTemplate.class, id);
		return roomTemplate;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delRoomTemplateItem(String delStr, String[] ids, User user, String roomTemplate_id) throws Exception {
		//String delId = "";
		for (String id : ids) {
			RoomTemplateItem rti = roomTemplateDao.get(RoomTemplateItem.class, id);
			if (rti.getId() != null) {
				RoomTemplate pt = roomTemplateDao.get(RoomTemplate.class, rti.getRoomTemplate().getId());
				// 改变左侧样本状态
				
				/*
				 * DustParticleTemp dustParticleTemp =
				 * this.commonDAO.get(DustParticleTemp.class, scp.getTempId()); if
				 * (dustParticleTemp != null) { pt.setSampleNum(pt.getSampleNum() - 1);
				 * dustParticleTemp.setState("1"); dustParticleDao.update(dustParticleTemp); }
				 * dustParticleDao.update(pt); dustParticleDao.delete(scp);
				 */
			}
				/*delId += scp.getSampleCode() + ",";*/
				
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("RoomTemplate");
			li.setFileId(roomTemplate_id);
			//li.setModifyContent(delStr + delId);
			commonDAO.saveOrUpdate(li);
		}
	}


	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDustParticleCos(String id, String delStr, String mainId, User user) throws Exception {
//		if (id != null && !"".equals(id)) {
//			DustParticleCos scp = dustParticleDao.get(DustParticleCos.class, id);
//			dustParticleDao.delete(scp);
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			User u = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			li.setUserId(u.getId());
//			li.setUserId(u.getId());
//			li.setFileId(mainId);
//			li.setClassName("CleanAreaDust");
//			li.setModifyContent(delStr + scp.getName());
//		}
	}
	

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {

		RoomTemplate sct = roomTemplateDao.get(RoomTemplate.class, id);
		if(applicationTypeActionId.equals("1996")) {
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName("作废");	
		}else {
			
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		}
		
		sct.setFinishDate(new Date());
		List<RoomTemplateItem> cads = commonService.get(RoomTemplateItem.class, "roomTemplate.id", sct.getId());
		for (RoomTemplateItem cad : cads) {
			cad.setState("1");
			commonDAO.saveOrUpdate(cad);
		}

		roomTemplateDao.update(sct);
	}


	/**
	 * 
	 * 
	 */
	public Map<String, Object> findRoomTemplateTable(String id, Integer start, Integer length, String query,
			String col, String sort,String region) throws Exception {
		return roomTemplateDao.findRoomTemplateTable(start, length, query, col, sort,region);
	}
	public Map<String, Object> findRoomTemplateTableList(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return roomTemplateDao.findRoomTemplateTableList(start, length, query, col, sort);
	}
	public void selAllRoom(String id,String quId) throws Exception {
		List<RoomManagement> selAllRoom = roomTemplateDao.selAllRoom(quId);
		RoomTemplate roomTemplate = commonDAO.get(RoomTemplate.class, id);
		if(selAllRoom.size()>0) {
			for (RoomManagement roomManagement : selAllRoom) {
				RoomTemplateItem roomTemplateItem = new RoomTemplateItem();
				roomTemplateItem.setCode(roomManagement.getId());
				roomTemplateItem.setName(roomManagement.getRoomName());
				roomTemplateItem.setRoomTemplate(roomTemplate);
				commonDAO.saveOrUpdate(roomTemplateItem);
				
			}
		}
	}

	/**
	 * 
	 * 
	 */
	public Map<String, Object> findRoomTemplateItemTable(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		return roomTemplateDao.findRoomTemplateItemTable(id, start, length, map2Query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delRoom(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			RoomTemplateItem rti = roomTemplateDao.get(RoomTemplateItem.class, id);
			roomTemplateDao.delete(rti);
			idStr += rti.getName() + "的记录已删除";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaDust");
		li.setState("2");
		li.setStateName("数据删除");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	public Map<String, Object> ShowUsersTableList(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return roomTemplateDao.ShowUsersTableList(start, length, query, col, sort);
	}
	
	// 保存子表信息
		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void saveRoomTemplateItem(RoomTemplate sc, String itemDataJson, String logInfo,String changeLogItem,String log) throws Exception {
			//List<RoomTemplateItem> saveItems = new ArrayList<RoomTemplateItem>();
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				RoomTemplateItem scp = new RoomTemplateItem();
				RoomManagement rm = new RoomManagement();
				Instrument im = new Instrument();
				// 将map信息读入实体类
				scp = (RoomTemplateItem) roomTemplateDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals("")) {
					scp.setId(null);
				}
//				if("0".equals(sc.getType())) {
//					rm=commonDAO.get(RoomManagement.class, scp.getRoomManagement().getId());
//					
//					commonDAO.saveOrUpdate(rm);
//				}else if("1".equals(sc.getType())){
//					im=commonDAO.get(Instrument.class, scp.getRoomManagement().getId());
//					DicState ds = commonDAO.get(DicState.class, "0r");
//					if(ds!=null) {
//						im.setState(ds);
//					}
//				}
				scp.setRoomTemplate(sc);
				roomTemplateDao.saveOrUpdate(scp);
			}
			//roomTemplateDao.saveOrUpdateAll(saveItems);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("RoomTemplate");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				roomTemplateDao.saveOrUpdate(li);
			}
			
			if (changeLogItem != null && !"".equals(changeLogItem)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("RoomTemplate");
				li.setModifyContent(changeLogItem);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				roomTemplateDao.saveOrUpdate(li);
			}
		}
}
