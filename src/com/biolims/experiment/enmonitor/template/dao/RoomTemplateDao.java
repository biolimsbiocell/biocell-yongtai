package com.biolims.experiment.enmonitor.template.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplate;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;


@Repository
@SuppressWarnings("unchecked")
public class RoomTemplateDao extends BaseHibernateDao {

	// 房间模板主表列表
	public Map<String, Object> findRoomTemplateTable(Integer start, Integer length, String query,
			String col, String sort,String region) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from RoomTemplate where 1=1 and regionId='"+region+"' ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from RoomTemplate  where 1=1 and regionId='"+region+"' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<RoomTemplate> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	public Map<String, Object> findRoomTemplateTableList(Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from RoomTemplate where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from RoomTemplate  where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<RoomTemplate> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 子表列表
	public Map<String, Object> findRoomTemplateItemTable(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from RoomTemplateItem where 1=1 and roomTemplate.id='" + id + "'";
		String key = "";
		if (map2Query != null) {
			key = map2where(map2Query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from RoomTemplateItem where 1=1 and roomTemplate.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<RoomTemplateItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> ShowUsersTableList(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from User where 1=1 and state.id='1z'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from User where 1=1  and state.id='1z'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {

				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<User> list = new ArrayList<User>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	//子表列表
	public List<RoomTemplateItem> findRoomTemplateItemTable(String id) throws Exception {
		List<RoomTemplateItem> list = new ArrayList<RoomTemplateItem>();
		if (null != id && !"".equals(id)) {
			String hql = "from RoomTemplateItem where 1=1 and roomTemplate.id='" + id + "'";
			list = getSession().createQuery(hql).list();
		}
		return list;
	}
	//子表列表
	public List<RoomManagement> selAllRoom(String quId) throws Exception {
		List<RoomManagement> list = new ArrayList<RoomManagement>();
		String hql = "from RoomManagement where 1=1 and regionalManagement.id='"+quId+"'  ";

		list = getSession().createQuery(hql).list();
		return list;
	}
}