package com.biolims.experiment.enmonitor.template.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.template.service.RoomTemplateService;

public class RoomTemplateEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		RoomTemplateService mbService = (RoomTemplateService) ctx
				.getBean("roomTemplateService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
