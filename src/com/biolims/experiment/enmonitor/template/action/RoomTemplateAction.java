package com.biolims.experiment.enmonitor.template.action;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.enmonitor.template.dao.RoomTemplateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplate;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.enmonitor.template.service.RoomTemplateService;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;


@Namespace("/experiment/enmonitor/template/roomTemplate")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class RoomTemplateAction extends BaseActionSupport {

	private static final long serialVersionUID = 8032203124491612573L;
	private String rightsId = "21103056";
	@Autowired
	private RoomTemplateService roomTemplateService;
	private RoomTemplate rt = new RoomTemplate();

	@Resource
	private RoomTemplateDao roomTemplateDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;



	/**
	 * 
	 * @Title: showDustParticleList @Description:展示主表 @author 洁净区
	 * 
	 */
	@Action(value = "showRoomTemplateEdit")
	public String showRoomTemplateTable() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			rt = commonService.get(RoomTemplate.class, id);
			toState(rt.getState());
		}else{
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			rt.setState("3");
			rt.setStateName("新建");
			rt.setCreateUser(user);
			rt.setCreateDate(new Date());
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/template/roomTemplateEdit.jsp");
	}
	@Action(value = "showRoomTemplateView")
	public String showRoomTemplateTableView() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			rt = commonService.get(RoomTemplate.class, id);
			toState(rt.getState());
		}
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/template/roomTemplateEdit.jsp");
	}
	@Action(value = "showRoomTemplateTableJson")
	public String showRoomTemplateTableJson() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/template/roomTemplate.jsp");
	}
	// 主表 列表数据
	@Action(value = "showRoomTemplateTableJsonList")
	public void showRoomTemplateTableJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String region = getParameterFromRequest("region");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = roomTemplateService.findRoomTemplateTable(id, start, length, query, col,
					sort,region);
			List<RoomTemplate> list = (List<RoomTemplate>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			RoomTemplate newRoomTemplate = new RoomTemplate();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(newRoomTemplate);
			map.put("createUser-name", "");
			map.put("confirmUser-name", "");
			map.put("region", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("finishDate", "yyyy-MM-dd");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showRoomTemplateTableJsonListsNew")
	public void showRoomTemplateTableJsonListsNew() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = roomTemplateService.findRoomTemplateTableList(id, start, length, query, col,
					sort);
			List<RoomTemplate> list = (List<RoomTemplate>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			RoomTemplate newRoomTemplate = new RoomTemplate();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(newRoomTemplate);
			map.put("createUser-name", "");
			map.put("confirmUser-name", "");
			map.put("region", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("finishDate", "yyyy-MM-dd");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 子表 列表数据
	@Action(value = "showRoomTemplateItemTableJsonList")
	public void showRoomTemplateItemTableJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (query != null && !"".equals(query)) {
			map2Query = JsonUtils.toObjectByJson(query, Map.class);
			id=(String) map2Query.get("id");
			map2Query.remove("id");
		}
		try {
			Map<String, Object> result = roomTemplateService.findRoomTemplateItemTable(id, start, length, map2Query,
					col, sort);
			List<RoomTemplateItem> list = (List<RoomTemplateItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			/*
			 * RoomTemplateItem cleanAreaMicrobe = new RoomTemplateItem(); map =
			 * (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMicrobe);
			 */
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("monitoringPoint", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveItem")
	public String saveItem() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("documentInfoItemJson");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String changeLogItem = getParameterFromRequest("changeLogItem"); 
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String id = rt.getId();
		String log="";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log="123";
			String modelName = "RoomTemplate";
			String markCode = "RT";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
			rt.setId(autoID);
			rt.setCreateUser(user);		
		}
		commonService.saveOrUpdate(rt);
		roomTemplateService.saveRoomTemplateItem(rt, dataJson, changeLog,changeLogItem,log);
		return redirect("/experiment/enmonitor/template/roomTemplate/showRoomTemplateEdit.action?id=" + rt.getId()+"&bpmTaskId="+bpmTaskId);
	}
	
	/**
	 * 选择房间
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selectRoomTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectRoomTable() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);

		return dispatcher("/WEB-INF/page/experiment/roomManagement/selectRoomTableAll.jsp");
	}
	@Action(value = "delRoom", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delRoom() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			roomTemplateService.delRoom(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
	
	@Action(value = "selUsersTableList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selUsersTableList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/enmonitor/template/selUsersTableList.jsp");
	}

	@Action(value = "selUsersTableListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selUsersTableListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = roomTemplateService
				.ShowUsersTableList(start, length, query, col, sort);
		List<User> list = (List<User>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	public RoomTemplate getRt() {
		return rt;
	}

	public void setRt(RoomTemplate rt) {
		this.rt = rt;
	}
	@Action(value = "selAllRoom", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selAllRoom() throws Exception {
		String id = getParameterFromRequest("id");
		String quId = getParameterFromRequest("quId");
		roomTemplateService.selAllRoom(id,quId);
	}
	
	@Action(value = "selRoomTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selRoomTemplateList() throws Exception {
		String region = getParameterFromRequest("region");
		putObjToContext("region", region);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/template/roomTemplateDialog.jsp");
	}
}
