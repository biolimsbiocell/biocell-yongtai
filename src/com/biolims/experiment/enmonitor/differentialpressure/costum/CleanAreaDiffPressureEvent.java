package com.biolims.experiment.enmonitor.differentialpressure.costum;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.enmonitor.bacteria.service.CleanAreaBacteriaService;
import com.biolims.experiment.enmonitor.differentialpressure.service.CleanAreaDiffPressureService;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microorganism.service.CleanAreaMicroorganismService;
import com.biolims.experiment.enmonitor.volume.service.CleanAreaVolumeService;

public class CleanAreaDiffPressureEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CleanAreaDiffPressureService mbService = (CleanAreaDiffPressureService) ctx
				.getBean("cleanAreaDiffPressureService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
