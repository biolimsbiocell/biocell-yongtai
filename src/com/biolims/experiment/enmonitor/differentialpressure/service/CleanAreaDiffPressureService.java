﻿package com.biolims.experiment.enmonitor.differentialpressure.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.dao.CleanAreaDiffPressureDao;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressure;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.dao.DustParticleDao;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.enmonitor.dust.model.DustParticleAbnormal;
import com.biolims.experiment.enmonitor.dust.model.DustParticleCos;
import com.biolims.experiment.enmonitor.dust.model.DustParticleInfo;
import com.biolims.experiment.enmonitor.dust.model.DustParticleItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleReagent;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemp;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemplate;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.template.dao.RoomTemplateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.enmonitor.volume.dao.CleanAreaVolumeDao;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional
public class CleanAreaDiffPressureService {

	@Resource
	private CleanAreaDiffPressureDao diffPressureDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private DustParticleDao dustParticleDao;
	@Resource
	private RoomTemplateDao roomTemplateDao;
	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 * DustParticle @throws
	 */
	public CleanAreaDiffPressure gets(String id) {
		CleanAreaDiffPressure cleanAreaDiffPressure = commonDAO.get(CleanAreaDiffPressure.class, id);
		return cleanAreaDiffPressure;
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description:压差主表列表 @author : @date @param
	 * id @param counts @param start @param length @param query @param
	 * col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> showDifferentialpressureTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return diffPressureDao.showDifferentialpressureTableJson(id, start, length, query, col, sort);
	}
	
	//保存
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(CleanAreaDiffPressure sc, String itemDataJson,
			String logInfo,String changeLogItem,String log) throws Exception {
		List<CleanAreaDiffPressureItem> saveItems = new ArrayList<CleanAreaDiffPressureItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CleanAreaDiffPressureItem scp = new CleanAreaDiffPressureItem();
			RoomManagement rm = new RoomManagement();
			Instrument im = new Instrument();
			// 将map信息读入实体类
			scp = (CleanAreaDiffPressureItem) diffPressureDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")){
				scp.setId(null);
			}
			if("0".equals(sc.getType())) {
				rm=commonDAO.get(RoomManagement.class, scp.getRoomNum());
//				if (scp.getRelativePa() != "" && sc.getCordon() != "" && sc.getDeviationCorrectionLine() != "") {
//					// 0.5最大允许数
//					Integer num1 = Integer.valueOf(scp.getRelativePa());
//					// 警戒线
//					Integer num2 = Integer.valueOf(sc.getCordon());
//					// 纠偏线
//					Integer num3 = Integer.valueOf(sc.getDeviationCorrectionLine());
//
//					if (num1 <= num2) {
//						// 合格
//						scp.setResult("是");
//						rm.setRoomState("1");
//
//					} else if (num1 > num2 && num1 < num3) {
//						// 合格
//						scp.setResult("是");
//						rm.setRoomState("1");
//
//					} else {
//						// 不合格
//						scp.setResult("否");
//						rm.setRoomState("0");
//
//					}
//				} else {
//
//				}
				if (scp.getRelativePa() != "" && scp.getAql() != "") {

					Double num1 = Double.valueOf(scp.getRelativePa());
					Double num2 = Double.valueOf(scp.getAql());
					if (num1 >= num2) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");

					} else if (num1 < num2) {
						// 不合格
						scp.setResult("否");
						rm.setRoomState("0");

					} else {}
				}
				commonDAO.saveOrUpdate(rm);
			}else if("1".equals(sc.getType())) {
				im=commonDAO.get(Instrument.class, scp.getRoomNum());
//				if (scp.getRelativePa() != "" && sc.getCordon() != "" && sc.getDeviationCorrectionLine() != "") {
//					// 0.5最大允许数
//					Integer num1 = Integer.valueOf(scp.getRelativePa());
//					// 警戒线
//					Integer num2 = Integer.valueOf(sc.getCordon());
//					// 纠偏线
//					Integer num3 = Integer.valueOf(sc.getDeviationCorrectionLine());
//
//					if (num1 <= num2) {
//						// 合格
//						scp.setResult("是");
//						DicState ds = commonDAO.get(DicState.class, "1r");
//						if(ds!=null) {
//							im.setState(ds);
//						}
//
//					} else if (num1 > num2 && num1 < num3) {
//						// 合格
//						scp.setResult("是");
//						DicState ds = commonDAO.get(DicState.class, "1r");
//						if(ds!=null) {
//							im.setState(ds);
//						}
//
//					} else {
//						// 不合格
//						scp.setResult("否");
//						DicState ds = commonDAO.get(DicState.class, "0r");
//						if(ds!=null) {
//							im.setState(ds);
//						}
//
//					}
//				} else {
//
//				}
				if (scp.getRelativePa() != "" && scp.getAql() != "") {

					Double num1 = Double.valueOf(scp.getRelativePa());
					Double num2 = Double.valueOf(scp.getAql());
					if (num1 >= num2) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 < num2) {
						// 不合格
						scp.setResult("否");
						DicState ds = commonDAO.get(DicState.class, "0r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else {}
				}
				commonDAO.saveOrUpdate(im);
			}

			
			scp.setCleanAreaDiffPressure(sc);
			saveItems.add(scp);
		}
		diffPressureDao.saveOrUpdateAll(saveItems);
		
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaDiffPressure");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			diffPressureDao.saveOrUpdate(li);
		}
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaDiffPressure");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			diffPressureDao.saveOrUpdate(li);
		}
	}
	
	/**
	 * 
	 * @Title: plateSampleTable @Description: 浮游菌子表列表 @author : @date @param
	 * id @param counts @param start @param length @param query @param
	 * col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> showDiffPressureItemTableJsonList(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		return diffPressureDao.showDiffPressureItemTableJsonList(id, start, length, map2Query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void uploadCsvFile(String id, String fileId) throws Exception {
		CleanAreaDiffPressureItem cv = diffPressureDao.get(CleanAreaDiffPressureItem.class, id);
		
		FileInfo fileInfo = diffPressureDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		
		File file = new File(filepath);
		InputStream is = new FileInputStream(filepath);
		List<CleanAreaDiffPressureItem> itemList=diffPressureDao.findCleanAreaDiffPressureItemTable(id);
		if (file.isFile()) {
			CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
			reader.readHeaders();// 跳过表头
			if(itemList.size()>0) {
				while (reader.readRecord()) {
					for(CleanAreaDiffPressureItem cadi:itemList) {
						if(cadi.getRoomName().equals(reader.get(0))&&cadi.getRoomNum().equals(reader.get(1))) {
								cadi.setRoomName(reader.get(0));
								cadi.setRoomNum(reader.get(1));
								cadi.setRelativeRoom(reader.get(2));
								cadi.setRelativePa(reader.get(3));
								cadi.setAql(reader.get(4));
								cadi.setResult(reader.get(5));
								cadi.setNote(reader.get(6));
								diffPressureDao.saveOrUpdate(cadi);
						}
					}
				}
			}
		}
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delRoom(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			CleanAreaDiffPressureItem scp = diffPressureDao.get(CleanAreaDiffPressureItem.class, id);
			diffPressureDao.delete(scp);
			idStr += scp.getRoomName() + "的记录已删除";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaDiffPressure");
		li.setModifyContent(delStr + idStr);
		li.setState("2");
		li.setStateName("数据删除");
		commonDAO.saveOrUpdate(li);
	}
	
	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 * applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		CleanAreaDiffPressure sct = diffPressureDao.get(CleanAreaDiffPressure.class, id);
		if(applicationTypeActionId.equals("2001")) {
			sct.setState("1");
			sct.setStateName("作废");
		}else {
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		}
		
		sct.setConfirmUser(u);
		sct.setConfirmDate(format.format(new Date()));
		
		if(sct.getNotifierId()!=null
				&&!"".equals(sct.getNotifierId())){
			String userids[] = sct.getNotifierId().split(",");
			for(String userid:userids){
				if(userid!=null
						&&!"".equals(userid)){
					User uu = commonDAO.get(User.class, userid);
					SysRemind srz = new SysRemind();
					srz.setId(null);
					// 提醒类型
					srz.setType(null);
					// 事件标题
					srz.setTitle("压差记录提醒");
					// 发起人（系统为SYSTEM）
					srz.setRemindUser(u.getName());
					// 发起时间
					srz.setStartDate(new Date());
					// 查阅提醒时间
					srz.setReadDate(new Date());
					// 提醒内容
					srz.setContent(sct.getTestTime()+"压差记录提醒");
					// 状态 0.草稿  1.已阅 
					srz.setState("0");
					//提醒的用户
					srz.setHandleUser(uu);
					// 表单id
					srz.setContentId(sct.getId());
					srz.setTableId("CleanAreaDiffPressure");
					commonDAO.saveOrUpdate(srz);
				}
			}
		}
		
		
		commonDAO.saveOrUpdate(sct);
	}

	public void useTemplateAddItem(String id,String cleanAreaDiffPressure_id) throws Exception {
		CleanAreaDiffPressure cleanAreaDiffPressure= diffPressureDao.get(CleanAreaDiffPressure.class, cleanAreaDiffPressure_id);
		List<RoomTemplateItem> useTemplateAddItem = roomTemplateDao.findRoomTemplateItemTable(id);
		for (RoomTemplateItem roomTemplateItem : useTemplateAddItem) {
			
/*			if(!"".equals(roomTemplateItem.getMonitoringPoint())&& null!=roomTemplateItem.getMonitoringPoint()) {
				int num = Integer.valueOf(roomTemplateItem.getMonitoringPoint());
				for(int i=1;i<=num;i++) {
					CleanAreaBacteriaItem cleanAreaBacteriaItem = new CleanAreaBacteriaItem();
					cleanAreaBacteriaItem.setRoomNum(roomTemplateItem.getCode());
					cleanAreaBacteriaItem.setRoomName(roomTemplateItem.getName());
					cleanAreaBacteriaItem.setMonitoringPoint(String.valueOf(i));
					cleanAreaBacteriaItem.setCleanAreaBacteria(cleanAreaBacteria);
					cleanAreaBacteriaDao.saveOrUpdate(cleanAreaBacteriaItem);
				}
			}else {*/
			CleanAreaDiffPressureItem cleanAreaDiffPressureItem = new CleanAreaDiffPressureItem();
			cleanAreaDiffPressureItem.setRoomNum(roomTemplateItem.getCode());
			cleanAreaDiffPressureItem.setRoomName(roomTemplateItem.getName());
		//	cleanAreaDiffPressureItem.setMonitoringPoint("1");
			cleanAreaDiffPressureItem.setCleanAreaDiffPressure(cleanAreaDiffPressure);
			diffPressureDao.saveOrUpdate(cleanAreaDiffPressureItem);
//			}
			
		}
	}
	public List<CleanAreaDiffPressureItem> findCleanAreaDiffPressureItemTable(String id) throws Exception{
		return diffPressureDao.findCleanAreaDiffPressureItemTable(id);
	}
}
