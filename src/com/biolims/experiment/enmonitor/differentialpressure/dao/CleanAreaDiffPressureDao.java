package com.biolims.experiment.enmonitor.differentialpressure.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.enmonitor.dust.model.DustParticleCos;
import com.biolims.experiment.enmonitor.dust.model.DustParticleItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleReagent;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemp;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemplate;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleInfo;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CleanAreaDiffPressureDao extends BaseHibernateDao {

	public Map<String, Object> showDifferentialpressureTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaDiffPressure where 1=1";
		String key = "";
		/*
		 * if(counts==null||counts.equals("")){ key+=" and counts is null";
		 * }else{ key+="and counts='"+counts+"'"; }
		 */
		if (query != null) {
			key = key+map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaDiffPressure  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showDiffPressureItemTableJsonList(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaDiffPressureItem where 1=1 and cleanAreaDiffPressure.id='" + id + "'";
		String key = "";
		if (map2Query != null) {
			key = map2where(map2Query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaDiffPressureItem where 1=1 and cleanAreaDiffPressure.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CleanAreaDiffPressureItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CleanAreaDiffPressureItem> findCleanAreaDiffPressureItemTable(String id) throws Exception {
		List<CleanAreaDiffPressureItem> list = new ArrayList<CleanAreaDiffPressureItem>();
		if (null != id && !"".equals(id)) {
			String hql = "from CleanAreaDiffPressureItem where 1=1 and cleanAreaDiffPressure.id='" + id + "'";
			list = getSession().createQuery(hql).list();
		}
		return list;
	}
}