package com.biolims.experiment.enmonitor.differentialpressure.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 风量 子表
 * @author lims-platform
 * @date 2019-3-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_d_p_item")
@SuppressWarnings("serial")
public class CleanAreaDiffPressureItem extends EntityDao<CleanAreaDiffPressureItem> implements
		java.io.Serializable {
	
	/** 编号 */
	private String id;
	/** 房间名称 */
	@Column(name="room_name")
	private String roomName;
	/**房间编号*/
	@Column(name="room_num")
	private String roomNum;
	/**相对房间*/
	@Column(name="relative_room")
	private String relativeRoom;
	/** 相对压差 */
	@Column(name="relative_pa")
	private String relativePa;
	/** 合格标准 */
	@Column(name="aql")
	private String aql;
	/** 是否符合标准 */
	@Column(name="result")
	private String result;
	/** 备注 */
	@Column(name="note")
	private String note;
	/**房间面积*/
	private String centiare;
	/**房间体积*/
	private String stere;

	

	public String getCentiare() {
		return centiare;
	}

	public void setCentiare(String centiare) {
		this.centiare = centiare;
	}

	public String getStere() {
		return stere;
	}

	public void setStere(String stere) {
		this.stere = stere;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

	/**关联主表 */
	private CleanAreaDiffPressure cleanAreaDiffPressure;

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 关联的主表 
	 * 
	 * @return: 关联的主表 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "clean_area_diff")
	public CleanAreaDiffPressure getCleanAreaDiffPressure() {
		return cleanAreaDiffPressure;
	}

	/**
	 * @param cleanAreaDiffPressure the cleanAreaDiffPressure to set
	 */
	public void setCleanAreaDiffPressure(CleanAreaDiffPressure cleanAreaDiffPressure) {
		this.cleanAreaDiffPressure = cleanAreaDiffPressure;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return the relativeRoom
	 */
	public String getRelativeRoom() {
		return relativeRoom;
	}

	/**
	 * @param relativeRoom the relativeRoom to set
	 */
	public void setRelativeRoom(String relativeRoom) {
		this.relativeRoom = relativeRoom;
	}

	/**
	 * @return the relativePa
	 */
	public String getRelativePa() {
		return relativePa;
	}

	/**
	 * @param relativePa the relativePa to set
	 */
	public void setRelativePa(String relativePa) {
		this.relativePa = relativePa;
	}

	/**
	 * @return the aql
	 */
	public String getAql() {
		return aql;
	}

	/**
	 * @param aql the aql to set
	 */
	public void setAql(String aql) {
		this.aql = aql;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}



	
}