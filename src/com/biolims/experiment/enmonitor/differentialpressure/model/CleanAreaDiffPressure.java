package com.biolims.experiment.enmonitor.differentialpressure.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;

/**
 * @Title: Model
 * @Description: 风量  主表
 * @author lims-platform
 * @date 2019-03-22 17:06:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_d_p")
@SuppressWarnings("serial")
public class CleanAreaDiffPressure extends EntityDao<CleanAreaDiffPressure> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	@Column(name="name")
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	@Column(name="create_date")
	private String createDate;
	/** 测量设备及编号*/
	@Column(name="measuring_instrument_code")
	private String measuringInstrumentCode;
	/** 测试人 */
	@Column(name="test_user")
	private String testUser;
	/** 测试时间 */
	@Column(name="test_time")
	private String testTime;
	/** 洁净级别 */
	@Column(name="cleanliness_class")
	private String cleanlinessClass;
	/**复核人*/
	@Column(name="confirm_user")
	private User confirmUser;
	/**备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**状态名称 */
	@Column(name="STATE_NAME")
	private String stateName;
	/** 复核时间*/
	@Column(name = "confrim_date")
	private String confirmDate;
	/** 通知人id */
	private String notifierId;
	/** 通知人姓名 */
	private String notifierName;
	/**区域*/
	private String region;
	/**区域ID*/
	private String regionId;
	/**类型*/
	@Column(name="type")
	private String type;
	/** 警戒线 */
	private String cordon;
	/** 纠偏线 */
	private String deviationCorrectionLine;
	
	//批准人
	private User approver;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPROVER")
	public User getApprover() {
		return approver;
	}

	public void setApprover(User approver) {
		this.approver = approver;
	}
	
	public String getCordon() {
		return cordon;
	}

	public void setCordon(String cordon) {
		this.cordon = cordon;
	}

	public String getDeviationCorrectionLine() {
		return deviationCorrectionLine;
	}

	public void setDeviationCorrectionLine(String deviationCorrectionLine) {
		this.deviationCorrectionLine = deviationCorrectionLine;
	}
	
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	
	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getNotifierId() {
		return notifierId;
	}

	public void setNotifierId(String notifierId) {
		this.notifierId = notifierId;
	}

	public String getNotifierName() {
		return notifierName;
	}

	public void setNotifierName(String notifierName) {
		this.notifierName = notifierName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getMeasuringInstrumentCode() {
		return measuringInstrumentCode;
	}

	public void setMeasuringInstrumentCode(String measuringInstrumentCode) {
		this.measuringInstrumentCode = measuringInstrumentCode;
	}

	public String getTestUser() {
		return testUser;
	}

	public void setTestUser(String testUser) {
		this.testUser = testUser;
	}

	

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the testTime
	 */
	public String getTestTime() {
		return testTime;
	}

	/**
	 * @param testTime the testTime to set
	 */
	public void setTestTime(String testTime) {
		this.testTime = testTime;
	}

	/**
	 * @return the cleanlinessClass
	 */
	public String getCleanlinessClass() {
		return cleanlinessClass;
	}

	/**
	 * @param cleanlinessClass the cleanlinessClass to set
	 */
	public void setCleanlinessClass(String cleanlinessClass) {
		this.cleanlinessClass = cleanlinessClass;
	}

	/**
	 * @return the confirmUser
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	/**
	 * @param confirmUser the confirmUser to set
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

}