﻿package com.biolims.experiment.enmonitor.differentialpressure.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressure;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.differentialpressure.service.CleanAreaDiffPressureService;
import com.biolims.experiment.enmonitor.dust.dao.DustParticleDao;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.enmonitor.volume.service.CleanAreaVolumeService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/enmonitor/differentialpressure")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CleanAreaDiffPressureAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "211030602";
	@Autowired
	private CleanAreaDiffPressureService diffPressureService;
	private CleanAreaDiffPressure cad = new CleanAreaDiffPressure();
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;
	
	/**
	 *	明细上传CSV文件
     * @Title: uploadCsvFile  
     * @Description: TODO  
     * @param @throws Exception    
     * @return void  
	 * @author 孙灵达  
     * @date 2019年4月19日
     * @throws
	 */
	@Action(value = "uploadCsvFile")
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			diffPressureService.uploadCsvFile(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 *	明细下载CSV模板文件
     * @Title: downloadCsvFile  
     * 
     * @throws
	 */
	@Action(value = "downloadCsvFile")
	public void downloadCsvFile() throws Exception {
		String mainTable_id=getParameterFromRequest("id");
				
		Map<String, Object> result = new HashMap<String, Object>();
		Date date= new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("system.properties");

		properties.load(is);
				
		String filePath = properties.getProperty("sample.path")+"\\";// 写入csv路径
		String fileName = filePath + "cleanAreaDiffPressureItem" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
				parent.mkdirs();
		} else {
				parent.delete();
				parent.mkdirs();
		}
			csvFile.createNewFile();
				// GB2312使正确读取分隔符","
				csvWtriter = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(csvFile), "GBK"), 1024);
				// 写入文件头部
				Object[] head = { "房间名称", "房间编号","相对房间", "相对压差Pa","合格标准Pa", "结果是否合格","备注" };
				List<Object> headList = Arrays.asList(head);
				for (Object data : headList) {
					StringBuffer sb = new StringBuffer();
					String rowStr = sb.append("\"").append(data).append("\",")
							.toString();
					csvWtriter.write(rowStr);
				}
				csvWtriter.newLine();
				List<CleanAreaDiffPressureItem> list = diffPressureService.findCleanAreaDiffPressureItemTable(mainTable_id);
				for (int i = 0; i < list.size(); i++) {
					StringBuffer sb = new StringBuffer();
					setMolecularMarkersData(list.get(i), sb);
					String rowStr = sb.toString();
					csvWtriter.write(rowStr);
					csvWtriter.newLine();
					result.put("success", true);

			}
				csvWtriter.flush(); 
				csvWtriter.close();
				//HttpUtils.write(JsonUtils.toJsonString(result));
				downLoadTemp3("cleanAreaDiffPressureItem", filePath);
	}
			
			/**
			 * 
			 * @Title: setMolecularMarkersData  
			 * @Description: 数据添加到csv内
			 * @author qi.yan
			 * @date 2018-12-24上午11:53:44
			 * 
			 */
			public void setMolecularMarkersData(CleanAreaDiffPressureItem sr, StringBuffer sb)
					throws Exception {
				if (null!=sr.getRoomName() && !"".equals(sr.getRoomName())) {
					sb.append("\"").append(sr.getRoomName()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				} 
				//编号
				if (null!=sr.getRoomNum() && !"".equals(sr.getRoomNum())) {
					sb.append("\"").append(sr.getRoomNum()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				}
				
			}
			/**
			 * 
			 * @Title: downLoadTemp3  
			 * @Description: 下载文件
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:26
			 * @param a
			 * @param filePath2
			 * @throws Exception
			 * void
			 * @throws
			 */
			@Action(value = "downLoadTemp3")
			public void downLoadTemp3(String a, String filePath2) throws Exception {
				String filePath = filePath2;// 保存窗口中显示的文件名
				String fileName = a + ".csv";// 保存窗口中显示的文件名
				super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

				/*
				 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
				 */
				ServletOutputStream out = null;
				// PrintWriter out = null;
				InputStream inStream = null;
				try {
					fileName = super.getResponse().encodeURL(
							new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

					super.getResponse().setHeader("Content-Disposition",
							"attachment; filename=\"" + fileName + "\"");
					// inline
					out = super.getResponse().getOutputStream();

					inStream = new FileInputStream(filePath + toUtf8String(fileName));

					// 循环取出流中的数据
					byte[] b = new byte[1024];
					int len;
					while ((len = inStream.read(b)) > 0)
						out.write(b, 0, len);
					super.getResponse().setStatus(super.getResponse().SC_OK);
					super.getResponse().flushBuffer();

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (RuntimeException e) {
					e.printStackTrace();
				} finally {
					if (out != null)
						out.close();
					inStream.close();
				}
			}
			/**
			 * 
			 * @Title: toUtf8String  
			 * @Description: 解决乱码问题
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:11
			 * @param s
			 * @return
			 * String
			 * @throws
			 */
			public static String toUtf8String(String s) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < s.length(); i++) {
					char c = s.charAt(i);
					if (c >= 0 && c <= 255) {
						sb.append(c);
					} else {
						byte[] b;
						try {
							b = Character.toString(c).getBytes("utf-8");
						} catch (Exception ex) {
							System.out.println(ex);
							b = new byte[0];
						}
						for (int j = 0; j < b.length; j++) {
							int k = b[j];
							if (k < 0)
								k += 256;
							sb.append("%" + Integer.toHexString(k).toUpperCase());
						}
					}
				}
				return sb.toString();
			}
	/**
	 * 
	 * @Title: showDustParticleList @Description:展示主表 @author 洁净区
	 * 风量模块 @date @return @throws Exception String @throws
	 */
	@Action(value = "showDifferentialpressureEdit")
	public String showDifferentialpressureEdit() throws Exception {
		rightsId = "211030601";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			cad = commonService.get(CleanAreaDiffPressure.class, id);
			toState(cad.getState());
		}else{
			cad.setId("NEW");
			cad.setState("3");
			cad.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/differentialpresure/cleanAreaDiffpressureEdit.jsp");
	}

	@Action(value = "showDifferentialpressureTable")
	public String showDifferentialpressureTable() throws Exception {
		rightsId = "211030602";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/differentialpresure/cleanAreaDiffpressure.jsp");
	}

	// 压差主表 列表数据
	@Action(value = "showDifferentialpressureTableJson")
	public void showDifferentialpressureTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = diffPressureService.showDifferentialpressureTableJson(id, start, length, query, col,
					sort);
			List<CleanAreaDiffPressure> list = (List<CleanAreaDiffPressure>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaDiffPressure cleanAreaVolume = new CleanAreaDiffPressure();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaVolume);
			map.put("createUser-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 压差子表 列表数据
	@Action(value = "showDiffPressureItemTableJsonList")
	public void showDiffPressureItemTableJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (query != null && !"".equals(query)) {
			map2Query = JsonUtils.toObjectByJson(query, Map.class);
			id=(String) map2Query.get("id");
			map2Query.remove("id");
		}
		try {
			Map<String, Object> result = diffPressureService.showDiffPressureItemTableJsonList(id, start, length, map2Query,
					col, sort);
			List<CleanAreaDiffPressureItem> list = (List<CleanAreaDiffPressureItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaDiffPressureItem cleanAreaVolumeItem = new CleanAreaDiffPressureItem();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaVolumeItem);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String dataJson = getParameterFromRequest("documentInfoItemJson");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String id = cad.getId();
		String log="";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log="123";
			String modelName = "CleanAreaDiffPressure";
			String markCode = "GU";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
			cad.setId(autoID);
			cad.setCreateUser(user);
			cad.setCreateDate(ObjectToMapUtils.getTimeString());
		}
		commonService.saveOrUpdate(cad);
		diffPressureService.save(cad, dataJson, changeLog,changeLogItem,log);
		return redirect("/experiment/enmonitor/differentialpressure/showDifferentialpressureEdit.action?id=" + cad.getId()+"&bpmTaskId="+bpmTaskId);
	}
	@Action(value = "delRoom", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delRoom() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			diffPressureService.delRoom(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	@Action(value = "useTemplateAddItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void useTemplateAddItem() throws Exception {
		String id = getParameterFromRequest("id");
		String diffPressureId = getParameterFromRequest("diffPressureId");
		diffPressureService.useTemplateAddItem(id,diffPressureId);
	}
	/**
	 * 选择房间
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selectRoomTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectRoomTable() throws Exception {
//		return dispatcher("/WEB-INF/page/experiment/roomManagement/selectRoomTable.jsp");
		return dispatcher("/WEB-INF/page/experiment/roomManagement/selectRoomTableAll.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	/**
	 * @return the diffPressureService
	 */
	public CleanAreaDiffPressureService getDiffPressureService() {
		return diffPressureService;
	}

	/**
	 * @param diffPressureService the diffPressureService to set
	 */
	public void setDiffPressureService(CleanAreaDiffPressureService diffPressureService) {
		this.diffPressureService = diffPressureService;
	}

	/**
	 * @return the cad
	 */
	public CleanAreaDiffPressure getCad() {
		return cad;
	}

	/**
	 * @param cad the cad to set
	 */
	public void setCad(CleanAreaDiffPressure cad) {
		this.cad = cad;
	}

	/**
	 * @return the codingRuleService
	 */
	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	/**
	 * @param codingRuleService the codingRuleService to set
	 */
	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}

	/**
	 * @return the commonService
	 */
	public CommonService getCommonService() {
		return commonService;
	}

	/**
	 * @param commonService the commonService to set
	 */
	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}


}
