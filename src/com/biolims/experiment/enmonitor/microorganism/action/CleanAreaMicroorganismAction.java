﻿package com.biolims.experiment.enmonitor.microorganism.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicoorganismItemNegative;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.microorganism.service.CleanAreaMicroorganismService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/enmonitor/microorganism")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CleanAreaMicroorganismAction extends BaseActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1933202753308540430L;
	private String rightsId = "211030502";
	@Autowired
	private CleanAreaMicroorganismService microorganismService;
	private CleanAreaMicroorganism cam = new CleanAreaMicroorganism();
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;

	/**
	 * 明细上传CSV文件 @Title: uploadCsvFile @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2019年4月19日 @throws
	 */
	@Action(value = "uploadCsvFile")
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			microorganismService.uploadCsvFile(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 *	明细下载CSV模板文件
     * @Title: downloadCsvFile  
     * 
     * @throws
	 */
	@Action(value = "downloadCsvFile")
	public void downloadCsvFile() throws Exception {
		String mainTable_id=getParameterFromRequest("id");
				
		Map<String, Object> result = new HashMap<String, Object>();
		Date date= new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("system.properties");

		properties.load(is);
				
		String filePath = properties.getProperty("sample.path")+"\\";// 写入csv路径
		String fileName = filePath + "cleanAreaMicroorganismItem" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
				parent.mkdirs();
		} else {
				parent.delete();
				parent.mkdirs();
		}
			csvFile.createNewFile();
				// GB2312使正确读取分隔符","
				csvWtriter = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(csvFile), "GBK"), 1024);
				// 写入文件头部
				Object[] head = { "房间名称", "房间编号","监测点","洁净级别", "合格标准","菌落数cfu(碟/手套)","结果是否合格", "备注" };
				List<Object> headList = Arrays.asList(head);
				for (Object data : headList) {
					StringBuffer sb = new StringBuffer();
					String rowStr = sb.append("\"").append(data).append("\",")
							.toString();
					csvWtriter.write(rowStr);
				}
				csvWtriter.newLine();
				List<CleanAreaMicroorganismItem> list = microorganismService.findCleanAreaMicroorganismItemTable(mainTable_id);
				for (int i = 0; i < list.size(); i++) {
					StringBuffer sb = new StringBuffer();
					setMolecularMarkersData(list.get(i), sb);
					String rowStr = sb.toString();
					csvWtriter.write(rowStr);
					csvWtriter.newLine();
					result.put("success", true);

			}
				csvWtriter.flush(); 
				csvWtriter.close();
				//HttpUtils.write(JsonUtils.toJsonString(result));
				downLoadTemp3("cleanAreaMicroorganismItem", filePath);
	}
			
			/**
			 * 
			 * @Title: setMolecularMarkersData  
			 * @Description: 数据添加到csv内
			 * @author qi.yan
			 * @date 2018-12-24上午11:53:44
			 * 
			 */
			public void setMolecularMarkersData(CleanAreaMicroorganismItem sr, StringBuffer sb)
					throws Exception {
				if (null!=sr.getRoomName() && !"".equals(sr.getRoomName())) {
					sb.append("\"").append(sr.getRoomName()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				} 
				//编号
				if (null!=sr.getRoomNum() && !"".equals(sr.getRoomNum())) {
					sb.append("\"").append(sr.getRoomNum()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				}
				if (null!=sr.getInspectionPoint()&& !"".equals(sr.getInspectionPoint())) {
					sb.append("\"").append(sr.getInspectionPoint()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				}
				
			}
			/**
			 * 
			 * @Title: downLoadTemp3  
			 * @Description: 下载文件
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:26
			 * @param a
			 * @param filePath2
			 * @throws Exception
			 * void
			 * @throws
			 */
			@Action(value = "downLoadTemp3")
			public void downLoadTemp3(String a, String filePath2) throws Exception {
				String filePath = filePath2;// 保存窗口中显示的文件名
				String fileName = a + ".csv";// 保存窗口中显示的文件名
				super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

				/*
				 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
				 */
				ServletOutputStream out = null;
				// PrintWriter out = null;
				InputStream inStream = null;
				try {
					fileName = super.getResponse().encodeURL(
							new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

					super.getResponse().setHeader("Content-Disposition",
							"attachment; filename=\"" + fileName + "\"");
					// inline
					out = super.getResponse().getOutputStream();

					inStream = new FileInputStream(filePath + toUtf8String(fileName));

					// 循环取出流中的数据
					byte[] b = new byte[1024];
					int len;
					while ((len = inStream.read(b)) > 0)
						out.write(b, 0, len);
					super.getResponse().setStatus(super.getResponse().SC_OK);
					super.getResponse().flushBuffer();

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (RuntimeException e) {
					e.printStackTrace();
				} finally {
					if (out != null)
						out.close();
					inStream.close();
				}
			}
			/**
			 * 
			 * @Title: toUtf8String  
			 * @Description: 解决乱码问题
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:11
			 * @param s
			 * @return
			 * String
			 * @throws
			 */
			public static String toUtf8String(String s) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < s.length(); i++) {
					char c = s.charAt(i);
					if (c >= 0 && c <= 255) {
						sb.append(c);
					} else {
						byte[] b;
						try {
							b = Character.toString(c).getBytes("utf-8");
						} catch (Exception ex) {
							System.out.println(ex);
							b = new byte[0];
						}
						for (int j = 0; j < b.length; j++) {
							int k = b[j];
							if (k < 0)
								k += 256;
							sb.append("%" + Integer.toHexString(k).toUpperCase());
						}
					}
				}
				return sb.toString();
			}
	/**
	 * 表面微生物模块
	 * 
	 * @Title: showDustParticleList @Description:展示主表 @author 洁净区
	 *         风量模块 @date @return @throws Exception String @throws
	 */
	@Action(value = "showMicroorganismEdit")
	public String showMicroorganismEdit() throws Exception {
		rightsId = "211030501";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			cam = commonService.get(CleanAreaMicroorganism.class, id);
			toState(cam.getState());
		}else{
			cam.setId("NEW");
			cam.setState("3");
			cam.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/microorganism/cleanAreaMicroorganismEdit.jsp");
	}

	@Action(value = "showMicroorganismTable")
	public String showMicroorganismTable() throws Exception {
		rightsId = "211030502";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/microorganism/cleanAreaMicroorganism.jsp");
	}

	// 沉降菌主表 列表数据
	@Action(value = "showMicroorganismTableJson")
	public void showMicroorganismTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			                                             
			Map<String, Object> result = microorganismService.showMicroorganismTableJson(id, start, length, query, col,
					sort);
			List<CleanAreaMicroorganism> list = (List<CleanAreaMicroorganism>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaMicroorganism cleanAreaM = new CleanAreaMicroorganism();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaM);
			map.put("createUser-name", "");
			map.put("confirmUser-name", "");
			
			map.put("observeDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 微生物子表 列表数据
	@Action(value = "showCleanAreaMicItemTableJson")
	public void showCleanAreaMicItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (query != null && !"".equals(query)) {
			map2Query = JsonUtils.toObjectByJson(query, Map.class);
			id=(String) map2Query.get("id");
			map2Query.remove("id");
		}
		try {
			Map<String, Object> result = microorganismService.showCleanAreaMicItemTableJson(id, start, length, map2Query,
					col, sort);
			List<CleanAreaMicroorganismItem> list = (List<CleanAreaMicroorganismItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaMicroorganismItem cleanAreaMItem = new CleanAreaMicroorganismItem();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMItem);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//阴性对照  子表 列表数据
		@Action(value = "showMicroorganismNegativeTableJsonList")
		public void showMicroorganismNegativeTableJsonList() throws Exception {
			String query = getParameterFromRequest("query");
			String id = getParameterFromRequest("id");
			String colNum = getParameterFromRequest("order[0][column]");
			String col = getParameterFromRequest("columns[" + colNum + "][data]");
			String sort = getParameterFromRequest("order[0][dir]");
			Integer start = Integer.valueOf(getParameterFromRequest("start"));
			Integer length = Integer.valueOf(getParameterFromRequest("length"));
			String draw = getParameterFromRequest("draw");
			try {
				Map<String, Object> result = microorganismService.findMicrodeNegativeTable(id, start, length,
						query, col, sort);
				List<CleanAreaMicoorganismItemNegative> list = (List<CleanAreaMicoorganismItemNegative>) result.get("list");
				Map<String, String> map = new HashMap<String, String>();
				CleanAreaMicoorganismItemNegative cleanAreaMicrobe = new CleanAreaMicoorganismItemNegative();
				map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMicrobe);
				String data = new SendData().getDateJsonForDatatable(map, list);
				HttpUtils.write(PushData.pushData(draw, result, data));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String changeLogItem2 = getParameterFromRequest("changeLogItem2");
		String dataJson = getParameterFromRequest("documentInfoItemJson");
		String dataJsontow = getParameterFromRequest("documentInfoItemJsontow");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String id = cam.getId();
		String log="";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log="123";
			String modelName = "CleanAreaMicroorganism";
			String markCode = "CAM";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
			cam.setId(autoID);
			cam.setCreateUser(user);
			cam.setCreateDate(ObjectToMapUtils.getTimeString());
		}
		commonService.saveOrUpdate(cam);
		microorganismService.saveMicroorganism(cam, dataJson,dataJsontow,changeLog,changeLogItem,changeLogItem2,log);
		return redirect("/experiment/enmonitor/microorganism/showMicroorganismEdit.action?id=" + cam.getId()+"&bpmTaskId="+bpmTaskId);
	}

	@Action(value = "del", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void del() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			microorganismService.delItem(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "delNegative", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delNegative() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			microorganismService.delNegative(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "useTemplateAddItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void useTemplateAddItem() throws Exception {
		String id = getParameterFromRequest("id");
		String microorganismId = getParameterFromRequest("microorganismId");
		microorganismService.useTemplateAddItem(id,microorganismId);
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	/**
	 * @return the microorganismService
	 */
	public CleanAreaMicroorganismService getMicroorganismService() {
		return microorganismService;
	}

	/**
	 * @param microorganismService
	 *            the microorganismService to set
	 */
	public void setMicroorganismService(CleanAreaMicroorganismService microorganismService) {
		this.microorganismService = microorganismService;
	}

	/**
	 * @return the cam
	 */
	public CleanAreaMicroorganism getCam() {
		return cam;
	}

	/**
	 * @param cam
	 *            the cam to set
	 */
	public void setCam(CleanAreaMicroorganism cam) {
		this.cam = cam;
	}

}
