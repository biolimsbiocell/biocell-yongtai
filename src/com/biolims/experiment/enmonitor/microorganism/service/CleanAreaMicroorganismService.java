﻿package com.biolims.experiment.enmonitor.microorganism.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.main.service.InstrumentStateService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItemNegative;
import com.biolims.experiment.enmonitor.dust.dao.DustParticleDao;
import com.biolims.experiment.enmonitor.dust.model.DustParticleAbnormal;
import com.biolims.log.model.LogInfo;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.enmonitor.dust.model.DustParticleCos;
import com.biolims.experiment.enmonitor.dust.model.DustParticleItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleReagent;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemp;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemplate;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItemNegative;
import com.biolims.experiment.enmonitor.microorganism.dao.CleanAreaMicroorganismDao;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicoorganismItemNegative;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.template.dao.RoomTemplateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.enmonitor.volume.dao.CleanAreaVolumeDao;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.reagent.model.ReagentTaskItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.enmonitor.dust.model.DustParticleInfo;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.biolims.sample.service.SampleReceiveService;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional
public class CleanAreaMicroorganismService {

	@Resource
	private CleanAreaMicroorganismDao cleanAreaMicroorganismDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private DustParticleDao dustParticleDao;
	@Resource
	private RoomTemplateDao roomTemplateDao;
	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         DustParticle @throws
	 */
	public CleanAreaMicroorganism gets(String id) {
		CleanAreaMicroorganism cleanAreaMicroorganism = commonDAO.get(CleanAreaMicroorganism.class, id);
		return cleanAreaMicroorganism;
	}

	public Map<String, Object> showMicroorganismTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return cleanAreaMicroorganismDao.showMicroorganismTableJson(id, start, length, query, col, sort);
	}

	public Map<String, Object> showCleanAreaMicItemTableJson(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) {
		return cleanAreaMicroorganismDao.showCleanAreaMicItemTableJson(id, start, length, map2Query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 阴性对照子表列表 @author : @date @param
	 * id @param counts @param start @param length @param query @param
	 * col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findMicrodeNegativeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return cleanAreaMicroorganismDao.findMicrodeNegativeTable(id, start, length, query, col, sort);
	}

	// 保存
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMicroorganism(CleanAreaMicroorganism sc, String itemDataJson,String itemDatetow, String logInfo,String changeLogItem,String changeLogItem2,String log) throws Exception {
		List<CleanAreaMicroorganismItem> saveItems = new ArrayList<CleanAreaMicroorganismItem>();
		List<CleanAreaMicoorganismItemNegative> saveItemstow = new ArrayList<CleanAreaMicoorganismItemNegative>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CleanAreaMicroorganismItem scp = new CleanAreaMicroorganismItem();
			RoomManagement rm = new RoomManagement();
			Instrument im = new Instrument();
			// 将map信息读入实体类
			scp = (CleanAreaMicroorganismItem) cleanAreaMicroorganismDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			if("0".equals(sc.getType())) {
				rm = commonDAO.get(RoomManagement.class, scp.getRoomNum());
				/*
				if (scp.getClumpCount() != "" 
						&& sc.getCordon() != ""
						&& sc.getDeviationCorrectionLine() != ""
						&& scp.getAql() !="") {
					// 换气次数
					Double num1 = Double.valueOf(scp.getClumpCount());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine());
					// 合格标准
					Double num4 = Double.valueOf(scp.getAql());
					
					if (num1 <= num2) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else {
						// 不合格
						scp.setResult("否");
						rm.setRoomState("0");
					}
				}else {
					
				}*/
				if (scp.getClumpCount() != ""
						&& scp.getAql()!="") {
					Double num1 = Double.valueOf(scp.getClumpCount());
					// 合格标准
					Double num2 = Double.valueOf(scp.getAql());
						
					if (num1 <= num2) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else if (num1 > num2) {
						// 不合格
						scp.setResult("否");
						rm.setRoomState("0");
					} else {}
				}
				commonDAO.saveOrUpdate(rm);
			}else if("1".equals(sc.getType())){
				im = commonDAO.get(Instrument.class, scp.getRoomNum());
				/*
				if (scp.getClumpCount() != "" 
						&& sc.getCordon() != ""
						&& sc.getDeviationCorrectionLine() != ""
						&& scp.getAql()!="") {
					// 换气次数
					Double num1 = Double.valueOf(scp.getClumpCount());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine());
					// 合格标准
					Double num4 = Double.valueOf(scp.getAql());
						
					if (num1 <= num2) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else {
						// 不合格
						scp.setResult("否");
						DicState ds = commonDAO.get(DicState.class, "0r");
						if(ds!=null) {
							im.setState(ds);
						}
					}
				}else {
					
				}*/
				if (scp.getClumpCount() != ""
						&& scp.getAql()!="") {
					Double num1 = Double.valueOf(scp.getClumpCount());
					// 合格标准
					Double num2 = Double.valueOf(scp.getAql());
						
					if (num1 <= num2) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 > num2) {
						// 不合格
						scp.setResult("否");
						DicState ds = commonDAO.get(DicState.class, "0r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else {}
				}
				commonDAO.saveOrUpdate(im);
			}
			
			scp.setCleanAreaMicroorganism(sc);
			saveItems.add(scp);
		}
		//阴性对照
	    List<Map<String, Object>> listtow = JsonUtils.toListByJsonArray(itemDatetow, List.class);
		for (Map<String, Object> maps : listtow) {
			CleanAreaMicoorganismItemNegative scp = new CleanAreaMicoorganismItemNegative();
			// 将map信息读入实体类
			scp = (CleanAreaMicoorganismItemNegative) cleanAreaMicroorganismDao.Map2Bean(maps, scp);
			if (scp.getId() != null && scp.getId().equals("")){
				scp.setId(null);
				scp.setState("0");
			}
			scp.setCleanAreaMicroorganism(sc);;
			saveItemstow.add(scp);
		}
		cleanAreaMicroorganismDao.saveOrUpdateAll(saveItems);
		cleanAreaMicroorganismDao.saveOrUpdateAll(saveItemstow);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaMicroorganism");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			cleanAreaMicroorganismDao.saveOrUpdate(li);
		}
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaMicroorganism");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			cleanAreaMicroorganismDao.saveOrUpdate(li);
		}
		if (changeLogItem2 != null && !"".equals(changeLogItem2)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaMicroorganism");
			li.setModifyContent(changeLogItem2);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			cleanAreaMicroorganismDao.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void uploadCsvFile(String id, String fileId) throws Exception {
		CleanAreaMicroorganism cam = cleanAreaMicroorganismDao.get(CleanAreaMicroorganism.class, id);

		FileInfo fileInfo = cleanAreaMicroorganismDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();

		File file = new File(filepath);
		InputStream is = new FileInputStream(filepath);
		List<CleanAreaMicroorganismItem> itemList = cleanAreaMicroorganismDao.findCleanAreaMicroorganismItemTable(id);
		if (file.isFile()) {
			CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
			reader.readHeaders();// 跳过表头
			if(itemList.size()>0) {
				while (reader.readRecord()) {
					for(CleanAreaMicroorganismItem cami:itemList) {			
						if(cami.getRoomName().equals(reader.get(0))&&cami.getRoomNum().equals(reader.get(1))) {
							if(cami.getInspectionPoint().equals(reader.get(2))) {
								cami.setCleanAreaMicroorganism(cam);
								cami.setRoomName(reader.get(0));
								cami.setRoomNum(reader.get(1));
								cami.setInspectionPoint(reader.get(2));
								cami.setCleanlinessClass(reader.get(3));
								cami.setAql(reader.get(4));
								cami.setClumpCount(reader.get(5));
								cami.setResult(reader.get(6));
								cami.setNote(reader.get(7));
								cleanAreaMicroorganismDao.saveOrUpdate(cami);
							}
						}
					}
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delItem(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			CleanAreaMicroorganismItem scp = cleanAreaMicroorganismDao.get(CleanAreaMicroorganismItem.class, id);
			cleanAreaMicroorganismDao.delete(scp);
			idStr += scp.getRoomName()+ "的记录已删除";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaMicroorganism");
		li.setModifyContent(delStr + idStr);
		li.setState("2");
		li.setStateName("数据删除");
		commonDAO.saveOrUpdate(li);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delNegative(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			CleanAreaMicoorganismItemNegative scp = cleanAreaMicroorganismDao.get(CleanAreaMicoorganismItemNegative.class, id);
			cleanAreaMicroorganismDao.delete(scp);
			idStr += scp.getNegativeControl() + ",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaMicroorganism");
		li.setModifyContent(delStr + idStr);
		li.setState("2");
		li.setStateName("数据删除");
		commonDAO.saveOrUpdate(li);
	}
	
	

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		CleanAreaMicroorganism sct = dustParticleDao.get(CleanAreaMicroorganism.class, id);
		if(applicationTypeActionId.equals("2000")) {
			sct.setState("1");
			sct.setStateName("作废");
		}else {
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		}
		sct.setConfirmUser(u);
		sct.setConfirmDate(new Date());

		if (sct.getNotifierId() != null && !"".equals(sct.getNotifierId())) {
			String userids[] = sct.getNotifierId().split(",");
			for (String userid : userids) {
				if (userid != null && !"".equals(userid)) {
					User uu = commonDAO.get(User.class, userid);
					SysRemind srz = new SysRemind();
					srz.setId(null);
					// 提醒类型
					srz.setType(null);
					// 事件标题
					srz.setTitle("表面微生物监测记录提醒");
					// 发起人（系统为SYSTEM）
					srz.setRemindUser(u.getName());
					// 发起时间
					srz.setStartDate(new Date());
					// 查阅提醒时间
					srz.setReadDate(new Date());
					// 提醒内容
					srz.setContent("区域：" + sct.getRegion() + "车间/部门：" + sct.getWorkshopDepartment() + "表面微生物监测记录提醒");
					// 状态 0.草稿 1.已阅
					srz.setState("0");
					// 提醒的用户
					srz.setHandleUser(uu);
					// 表单id
					srz.setContentId(sct.getId());
					srz.setTableId("CleanAreaMicroorganism");
					commonDAO.saveOrUpdate(srz);
				}
			}
		}

		commonDAO.saveOrUpdate(sct);
	}
	
	public void useTemplateAddItem(String id,String cleanAreaMicroorganism_id) throws Exception {
		CleanAreaMicroorganism cleanAreaMicroorganism = commonDAO.get(CleanAreaMicroorganism.class, cleanAreaMicroorganism_id);
		List<RoomTemplateItem> useTemplateAddItem = roomTemplateDao.findRoomTemplateItemTable(id);
		for (RoomTemplateItem roomTemplateItem : useTemplateAddItem) {
			
			if(!"".equals(roomTemplateItem.getMonitoringPoint())&& null!=roomTemplateItem.getMonitoringPoint()) {
				int num = Integer.valueOf(roomTemplateItem.getMonitoringPoint());
				for(int i=1;i<=num;i++) {
					CleanAreaMicroorganismItem cleanAreaMicroorganismItem = new CleanAreaMicroorganismItem();
					cleanAreaMicroorganismItem.setRoomNum(roomTemplateItem.getCode());
					cleanAreaMicroorganismItem.setRoomName(roomTemplateItem.getName());
					cleanAreaMicroorganismItem.setInspectionPoint(String.valueOf(i));
					cleanAreaMicroorganismItem.setCleanAreaMicroorganism(cleanAreaMicroorganism);
					cleanAreaMicroorganismDao.saveOrUpdate(cleanAreaMicroorganismItem);
				}
			}else {
				CleanAreaMicroorganismItem cleanAreaMicroorganismItem = new CleanAreaMicroorganismItem();
				cleanAreaMicroorganismItem.setRoomNum(roomTemplateItem.getCode());
				cleanAreaMicroorganismItem.setRoomName(roomTemplateItem.getName());
				cleanAreaMicroorganismItem.setInspectionPoint("1");
				cleanAreaMicroorganismItem.setCleanAreaMicroorganism(cleanAreaMicroorganism);
				cleanAreaMicroorganismDao.saveOrUpdate(cleanAreaMicroorganismItem);
			}
			
		}
	}
	public List<CleanAreaMicroorganismItem> findCleanAreaMicroorganismItemTable(String id) throws Exception {
		return cleanAreaMicroorganismDao.findCleanAreaMicroorganismItemTable(id);
	}

}
