package com.biolims.experiment.enmonitor.microorganism.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;

/**
 * @Title: Model
 * @Description: 微生物 主表
 * @author lims-platform
 * @date 2019-03-22 17:06:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_microoganism")
@SuppressWarnings("serial")
public class CleanAreaMicroorganism extends EntityDao<CleanAreaMicroorganism> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	@Column(name="name")
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	@Column(name="create_date")
	private String createDate;
	/** 车间部门 */
	@Column(name="workshop_department")
	private String workshopDepartment;
	/** 区域 */
	@Column(name="region")
	private String region;
	/** 区域 */
	@Column(name="regionId")
	private String regionId;
	/** 测量设备及编号*/
	@Column(name="measuring_instrument_code")
	private String measuringInstrumentCode;
	/** 测试人 */
	@Column(name="test_user")
	private String testUser;
	/** 测试时间 */
	@Column(name="test_time")
	private String testTime;
	/**培养基名称及规格*/
	@Column(name="medium")
	private String medium;
	/**培养基批号*/
	@Column(name="medium_batch")
	private String mediumBatch;
	/**培养地点或设备名称*/
	@Column(name="cultivate_addr")
	private String cultivateAddr;
	/**设备编号*/
	@Column(name="equipment_no")
	private String equipmentNo;
	/**培养开始时间*/
	@Column(name="cultivate_start")
	private String cultivateStart;
	/**培养结束时间*/
	@Column(name="cultivate_end")
	private String cultivateEnd;
	/**观察人*/
	@Column(name="observe_user")
	private String observeUser;
	/**观察日期*/
	@Column(name="observe_date")
	private Date observeDate;
	/**复核人*/
	@Column(name="confirm_user")
	private User confirmUser;
	/**复核日期*/
	@Column(name="confirm_date")
	private Date confirmDate;
	/**备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**类型*/
	@Column(name="type")
	private String type;
	/**状态名称 */
	@Column(name="STATE_NAME")
	private String stateName;
	/** 通知人id */
	private String notifierId;
	/** 通知人姓名 */
	private String notifierName;
	
	/**警戒线*/
	@Column(name="cordon")
	private String cordon;
	/**纠偏线*/
	@Column(name="deviationCorrectionLine")
	private String deviationCorrectionLine;
	
	
	//批准人
	private User approver;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPROVER")
	public User getApprover() {
		return approver;
	}

	public void setApprover(User approver) {
		this.approver = approver;
	}
	
	

	public String getCordon() {
		return cordon;
	}

	public void setCordon(String cordon) {
		this.cordon = cordon;
	}

	

	public String getDeviationCorrectionLine() {
		return deviationCorrectionLine;
	}

	public void setDeviationCorrectionLine(String deviationCorrectionLine) {
		this.deviationCorrectionLine = deviationCorrectionLine;
	}

	public String getNotifierId() {
		return notifierId;
	}

	public void setNotifierId(String notifierId) {
		this.notifierId = notifierId;
	}

	public String getNotifierName() {
		return notifierName;
	}

	public void setNotifierName(String notifierName) {
		this.notifierName = notifierName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	public String getWorkshopDepartment() {
		return workshopDepartment;
	}

	public void setWorkshopDepartment(String workshopDepartment) {
		this.workshopDepartment = workshopDepartment;
	}
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	public String getMeasuringInstrumentCode() {
		return measuringInstrumentCode;
	}

	public void setMeasuringInstrumentCode(String measuringInstrumentCode) {
		this.measuringInstrumentCode = measuringInstrumentCode;
	}


	public String getTestUser() {
		return testUser;
	}

	public void setTestUser(String testUser) {
		this.testUser = testUser;
	}

	

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the testTime
	 */
	public String getTestTime() {
		return testTime;
	}

	/**
	 * @param testTime the testTime to set
	 */
	public void setTestTime(String testTime) {
		this.testTime = testTime;
	}

	/**
	 * @return the medium
	 */
	public String getMedium() {
		return medium;
	}

	/**
	 * @param medium the medium to set
	 */
	public void setMedium(String medium) {
		this.medium = medium;
	}

	/**
	 * @return the mediumBatch
	 */
	public String getMediumBatch() {
		return mediumBatch;
	}

	/**
	 * @param mediumBatch the mediumBatch to set
	 */
	public void setMediumBatch(String mediumBatch) {
		this.mediumBatch = mediumBatch;
	}

	/**
	 * @return the cultivateAddr
	 */
	public String getCultivateAddr() {
		return cultivateAddr;
	}

	/**
	 * @param cultivateAddr the cultivateAddr to set
	 */
	public void setCultivateAddr(String cultivateAddr) {
		this.cultivateAddr = cultivateAddr;
	}

	/**
	 * @return the equipmentNo
	 */
	public String getEquipmentNo() {
		return equipmentNo;
	}

	/**
	 * @param equipmentNo the equipmentNo to set
	 */
	public void setEquipmentNo(String equipmentNo) {
		this.equipmentNo = equipmentNo;
	}


	/**
	 * @return the cultivateStart
	 */
	public String getCultivateStart() {
		return cultivateStart;
	}

	/**
	 * @param cultivateStart the cultivateStart to set
	 */
	public void setCultivateStart(String cultivateStart) {
		this.cultivateStart = cultivateStart;
	}

	/**
	 * @return the cultivateEnd
	 */
	public String getCultivateEnd() {
		return cultivateEnd;
	}

	/**
	 * @param cultivateEnd the cultivateEnd to set
	 */
	public void setCultivateEnd(String cultivateEnd) {
		this.cultivateEnd = cultivateEnd;
	}

	/**
	 * @return the observeUser
	 */
	public String getObserveUser() {
		return observeUser;
	}

	/**
	 * @param observeUser the observeUser to set
	 */
	public void setObserveUser(String observeUser) {
		this.observeUser = observeUser;
	}

	/**
	 * @return the observeDate
	 */
	public Date getObserveDate() {
		return observeDate;
	}

	/**
	 * @param observeDate the observeDate to set
	 */
	public void setObserveDate(Date observeDate) {
		this.observeDate = observeDate;
	}



	/**
	 * @return the confirmUser
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	/**
	 * @param confirmUser the confirmUser to set
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * @return the confirmDate
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

}