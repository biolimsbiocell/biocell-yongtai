package com.biolims.experiment.enmonitor.microorganism.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**
 * @Title: Model
 * @Description:  表面微生物子表
 * @author lims-platform
 * @date 2019-3-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_m_item")
@SuppressWarnings("serial")
public class CleanAreaMicroorganismItem extends EntityDao<CleanAreaMicroorganismItem> implements
		java.io.Serializable {
	
	/** 编号 */
	private String id;
	/** 序号 */
	@Column(name="serial")
	private String serial;
	/**计划检测点 */
	@Column(name="inspection_Point")
	private String inspectionPoint;
	/** 洁净级别 */
	@Column(name="cleanliness_class")
	private String cleanlinessClass;
	/** 合格标准*/
	@Column(name="aql")
	private String aql;
	/** 菌落数 */
	@Column(name="clump_count")
	private String clumpCount;
	/** 结果是否合格 */
	@Column(name="result")
	private String result;
	/** 备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**房间名称*/
	@Column(name="room_name")
	private String roomName;
	/**房间编号*/
	@Column(name="room_num")
	private String roomNum;
	
	/**房间面积*/
	private String centiare;
	/**房间体积*/
	private String stere;
	


	public String getCentiare() {
		return centiare;
	}

	public void setCentiare(String centiare) {
		this.centiare = centiare;
	}

	public String getStere() {
		return stere;
	}

	public void setStere(String stere) {
		this.stere = stere;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

	/**关联主表 */
	private CleanAreaMicroorganism cleanAreaMicroorganism;
	



	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * @param serial the serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * @return the inspectionPoint
	 */
	public String getInspectionPoint() {
		return inspectionPoint;
	}

	/**
	 * @param inspectionPoint the inspectionPoint to set
	 */
	public void setInspectionPoint(String inspectionPoint) {
		this.inspectionPoint = inspectionPoint;
	}

	/**
	 * @return the cleanlinessClass
	 */
	public String getCleanlinessClass() {
		return cleanlinessClass;
	}

	/**
	 * @param cleanlinessClass the cleanlinessClass to set
	 */
	public void setCleanlinessClass(String cleanlinessClass) {
		this.cleanlinessClass = cleanlinessClass;
	}

	/**
	 * @return the aql
	 */
	public String getAql() {
		return aql;
	}

	/**
	 * @param aql the aql to set
	 */
	public void setAql(String aql) {
		this.aql = aql;
	}

	/**
	 * @return the clumpCount
	 */
	public String getClumpCount() {
		return clumpCount;
	}

	/**
	 * @param clumpCount the clumpCount to set
	 */
	public void setClumpCount(String clumpCount) {
		this.clumpCount = clumpCount;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 关联的主表 
	 * 
	 * @return: 关联的主表 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "clean_area_mic")
	public CleanAreaMicroorganism getCleanAreaMicroorganism() {
		return cleanAreaMicroorganism;
	}

	/**
	 * @param cleanAreaMicroorganism the cleanAreaMicroorganism to set
	 */
	public void setCleanAreaMicroorganism(CleanAreaMicroorganism cleanAreaMicroorganism) {
		this.cleanAreaMicroorganism = cleanAreaMicroorganism;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	
}