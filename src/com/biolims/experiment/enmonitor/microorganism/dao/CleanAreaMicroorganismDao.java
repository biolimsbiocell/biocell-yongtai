package com.biolims.experiment.enmonitor.microorganism.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItemNegative;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicoorganismItemNegative;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;

@Repository
@SuppressWarnings("unchecked")
public class CleanAreaMicroorganismDao extends BaseHibernateDao {

	public Map<String, Object> showMicroorganismTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaMicroorganism where 1=1";
		String key = "";
		if (query != null) {
			key = key+map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaMicroorganism  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showCleanAreaMicItemTableJson(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.id='" + id + "'";
		String key = "";
		if (map2Query != null) {
			key = map2where(map2Query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CleanAreaBacteriaItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	// 阴性对照子表列表
		public Map<String, Object> findMicrodeNegativeTable(String id, Integer start, Integer length, String query,
				String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from CleanAreaMicoorganismItemNegative where 1=1 and cleanAreaMicroorganism.id='" + id + "'";
			String key = "";
			Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
				String hql = "from CleanAreaMicoorganismItemNegative where 1=1 and cleanAreaMicroorganism.id='" + id + "'";
				if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<CleanAreaMicoorganismItemNegative> list = getSession().createQuery(hql + key).setFirstResult(start)
						.setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		public List<CleanAreaMicroorganismItem> findCleanAreaMicroorganismItemTable(String id) throws Exception {
			List<CleanAreaMicroorganismItem> list = new ArrayList<CleanAreaMicroorganismItem>();
			if (null != id && !"".equals(id)) {
				String hql = "from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.id='" + id + "'";
				list = getSession().createQuery(hql).list();
			}
			return list;
		}
}