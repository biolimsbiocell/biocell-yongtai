package com.biolims.experiment.enmonitor.microbe.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobe;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeCos;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeItem;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeReagent;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeTemp;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeTemplate;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeInfo;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SettlingMicrobeDao extends BaseHibernateDao {

	public Map<String, Object> findSettlingMicrobeTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SettlingMicrobe where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SettlingMicrobe where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobe> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectSettlingMicrobeTempTable(String[] codes, Integer start, Integer length,
			String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SettlingMicrobeTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (codes != null && !codes.equals("")) {
			key += " and code in (:alist)";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			List<SettlingMicrobeTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from SettlingMicrobeTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			if (codes != null && !codes.equals("")) {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
						.setParameterList("alist", codes).list();
			} else {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findSettlingMicrobeItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SettlingMicrobeItem where 1=1 and state='1' and settlingMicrobe.id='"
				+ id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SettlingMicrobeItem  where 1=1 and state='1' and settlingMicrobe.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SettlingMicrobeItem> showWellPlate(String id) {
		String hql = "from SettlingMicrobeItem  where 1=1 and  settlingMicrobe.id='" + id + "'";
		List<SettlingMicrobeItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findSettlingMicrobeItemAfTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SettlingMicrobeItem where 1=1 and state='2' and settlingMicrobe.id='"
				+ id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SettlingMicrobeItem  where 1=1 and state='2' and settlingMicrobe.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from SettlingMicrobeItem where 1=1 and state='2' and settlingMicrobe.id='"
				+ id + "'";
		String key = "";
		List<String> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "select counts from SettlingMicrobeItem where 1=1 and state='2' and settlingMicrobe.id='" + id
					+ "' group by counts";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<SettlingMicrobeTemplate> showSettlingMicrobeStepsJson(String id, String code) {

		String countHql = "select count(*) from SettlingMicrobeTemplate where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and orderNum ='" + code + "'";
		} else {
		}
		List<SettlingMicrobeTemplate> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from SettlingMicrobeTemplate where 1=1 and settlingMicrobe.id='" + id + "'";
			key += " order by orderNum asc";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<SettlingMicrobeReagent> showSettlingMicrobeReagentJson(String id, String code) {
		String countHql = "select count(*) from SettlingMicrobeReagent where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<SettlingMicrobeReagent> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from SettlingMicrobeReagent where 1=1 and settlingMicrobe.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<SettlingMicrobeCos> showSettlingMicrobeCosJson(String id, String code) {
		String countHql = "select count(*) from SettlingMicrobeCos where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<SettlingMicrobeCos> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from SettlingMicrobeCos where 1=1 and settlingMicrobe.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<SettlingMicrobeTemplate> delTemplateItem(String id) {
		List<SettlingMicrobeTemplate> list = new ArrayList<SettlingMicrobeTemplate>();
		String hql = "from SettlingMicrobeTemplate where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<SettlingMicrobeReagent> delReagentItem(String id) {
		List<SettlingMicrobeReagent> list = new ArrayList<SettlingMicrobeReagent>();
		String hql = "from SettlingMicrobeReagent where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<SettlingMicrobeCos> delCosItem(String id) {
		List<SettlingMicrobeCos> list = new ArrayList<SettlingMicrobeCos>();
		String hql = "from SettlingMicrobeCos where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> showSettlingMicrobeResultTableJson(String id, Integer start, Integer length,
			String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SettlingMicrobeInfo where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SettlingMicrobeInfo  where 1=1 and settlingMicrobe.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeInfo> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Object> showWellList(String id) {

		String hql = "select counts,count(*) from SettlingMicrobeItem where 1=1 and settlingMicrobe.id='" + id
				+ "' group by counts";
		List<Object> list = getSession().createQuery(hql).list();

		return list;
	}

	public List<SettlingMicrobeItem> plateSample(String id, String counts) {
		String hql = "from SettlingMicrobeItem where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		if (counts == null || counts.equals("")) {
			key = " and counts is null";
		} else {
			key = "and counts='" + counts + "'";
		}
		List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SettlingMicrobeItem where 1=1 and settlingMicrobe.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (counts == null || counts.equals("")) {
			key += " and counts is null";
		} else {
			key += "and counts='" + counts + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SettlingMicrobeItem  where 1=1 and settlingMicrobe.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<SettlingMicrobeInfo> findSettlingMicrobeInfoByCode(String code) {
		String hql = "from SettlingMicrobeInfo where 1=1 and code='" + code + "'";
		List<SettlingMicrobeInfo> list = new ArrayList<SettlingMicrobeInfo>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<SettlingMicrobeInfo> selectAllResultListById(String code) {
		String hql = "from SettlingMicrobeInfo  where (submit is null or submit='') and settlingMicrobe.id='" + code
				+ "'";
		List<SettlingMicrobeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SettlingMicrobeInfo> selectResultListById(String id) {
		String hql = "from SettlingMicrobeInfo  where settlingMicrobe.id='" + id + "'";
		List<SettlingMicrobeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SettlingMicrobeInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from SettlingMicrobeInfo t where (submit is null or submit='') and id in (" + insql + ")";
		List<SettlingMicrobeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SettlingMicrobeItem> selectSettlingMicrobeItemList(String scId) throws Exception {
		String hql = "from SettlingMicrobeItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and settlingMicrobe.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SettlingMicrobeItem> list = new ArrayList<SettlingMicrobeItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Integer generateBlendCode(String id) {
		String hql = "select max(blendCode) from SettlingMicrobeItem where 1=1 and settlingMicrobe.id='" + id + "'";
		Integer blendCode = (Integer) this.getSession().createQuery(hql).uniqueResult();
		return blendCode;
	}

	public SettlingMicrobeInfo getResultByCode(String code) {
		String hql = " from SettlingMicrobeInfo where 1=1 and code='" + code + "'";
		SettlingMicrobeInfo spi = (SettlingMicrobeInfo) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}

	// 浮游菌主表列表
	public Map<String, Object> findCleanAreaMicrodeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaMicrobe where 1=1";
		String key = "";
		/*
		 * if(counts==null||counts.equals("")){ key+=" and counts is null";
		 * }else{ key+="and counts='"+counts+"'"; }
		 */
		if (query != null) {
			key = key + map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaMicrobe  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 浮游菌子表列表
	public Map<String, Object> findCleanAreaMicrodeItemTable(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.id='" + id + "'";
		String key = "";
		if(map2Query != null) {
			key = map2where(map2Query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 阴性对照子表列表
	public Map<String, Object> findCleanAreaNegativeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CleanAreaMicrobeItemNegative where 1=1 and cleanAreaMicrobe.id='" + id + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CleanAreaMicrobeItemNegative where 1=1 and cleanAreaMicrobe.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SettlingMicrobeItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public List<CleanAreaMicrobeItem> findCleanAreaMicrobeItemTable(String id) throws Exception {
		List<CleanAreaMicrobeItem> list = new ArrayList<CleanAreaMicrobeItem>();
		if (null != id && !"".equals(id)) {
			String hql = "from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.id='" + id + "'";
			list = getSession().createQuery(hql).list();
		}
		return list;
	}
}