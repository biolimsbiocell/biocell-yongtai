package com.biolims.experiment.enmonitor.microbe.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.enmonitor.microbe.service.SettlingMicrobeService;

public class SettlingMicrobeEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SettlingMicrobeService mbService = (SettlingMicrobeService) ctx
				.getBean("settlingMicrobeService");
		mbService.changeStates(applicationTypeActionId, contentId);

		return "";
	}
}
