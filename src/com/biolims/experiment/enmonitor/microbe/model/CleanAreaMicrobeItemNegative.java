package com.biolims.experiment.enmonitor.microbe.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 沉降菌    阴性对照第二张子表
 * @author lims-platform
 * @date 2019-3-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_microbe_negative")
@SuppressWarnings("serial")
public class CleanAreaMicrobeItemNegative extends EntityDao<CleanAreaMicrobeItemNegative> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 阴性对照 */
	@Column(name="negative_control")
	private String negativeControl;
	/** 观察日期 */
	@Column(name="observation_date")
	private String observationDate;
	/** 观察人 */
	@Column(name="observer")
	private String observer;
	/** 备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**结果是否合格*/
	@Column(name="result")
	private String result;
	/**监测点*/
	private String monitoringPoint;
	/**洁净区级别*/
	private String cleanZoneGrade;
	/**采样量L*/
	private String samplingQuantity;
	/**合格标准cfu/m³*/
	private String eligibilityCriteria;
	/**监测结果cfu/皿*/
	private String monitoringResults;
	/**计算结果cfu/m³*/
	private String computationResults;



	
	/**关联的主表 */
	private CleanAreaMicrobe cleanAreaMicrobe;
	
	
	/**
	 * 方法: 关联的主表 
	 * 
	 * @return: 关联的主表 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "clean_area_microbe")
	public CleanAreaMicrobe getCleanAreaMicrobe() {
		return cleanAreaMicrobe;
	}

	public void setCleanAreaMicrobe(CleanAreaMicrobe cleanAreaMicrobe) {
		this.cleanAreaMicrobe = cleanAreaMicrobe;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	
	
	
	public void setId(String id) {
		this.id = id;
	}
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMonitoringPoint() {
		return monitoringPoint;
	}

	public void setMonitoringPoint(String monitoringPoint) {
		this.monitoringPoint = monitoringPoint;
	}

	public String getCleanZoneGrade() {
		return cleanZoneGrade;
	}

	public void setCleanZoneGrade(String cleanZoneGrade) {
		this.cleanZoneGrade = cleanZoneGrade;
	}

	public String getSamplingQuantity() {
		return samplingQuantity;
	}

	public void setSamplingQuantity(String samplingQuantity) {
		this.samplingQuantity = samplingQuantity;
	}

	public String getEligibilityCriteria() {
		return eligibilityCriteria;
	}

	public void setEligibilityCriteria(String eligibilityCriteria) {
		this.eligibilityCriteria = eligibilityCriteria;
	}

	public String getMonitoringResults() {
		return monitoringResults;
	}

	public void setMonitoringResults(String monitoringResults) {
		this.monitoringResults = monitoringResults;
	}

	public String getComputationResults() {
		return computationResults;
	}

	public void setComputationResults(String computationResults) {
		this.computationResults = computationResults;
	}

	public String getObservationDate() {
		return observationDate;
	}

	public void setObservationDate(String observationDate) {
		this.observationDate = observationDate;
	}

	public String getObserver() {
		return observer;
	}

	public void setObserver(String observer) {
		this.observer = observer;
	}
	public String getNegativeControl() {
		return negativeControl;
	}

	public void setNegativeControl(String negativeControl) {
		this.negativeControl = negativeControl;
	}
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}