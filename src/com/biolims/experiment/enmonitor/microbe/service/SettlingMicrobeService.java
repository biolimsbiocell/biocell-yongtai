﻿package com.biolims.experiment.enmonitor.microbe.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.document.model.DocumentInfo;
import com.biolims.document.model.DocumentInfoItem;
import com.biolims.equipment.main.service.InstrumentStateService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleInfo;
import com.biolims.experiment.enmonitor.dust.model.DustParticleItem;
import com.biolims.experiment.enmonitor.microbe.dao.SettlingMicrobeDao;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeAbnormal;
import com.biolims.log.model.LogInfo;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobe;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItemNegative;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobe;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeCos;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeItem;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeReagent;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeTemp;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeTemplate;
import com.biolims.experiment.enmonitor.template.dao.RoomTemplateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeInfo;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.biolims.sample.service.SampleReceiveService;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional
public class SettlingMicrobeService {

	@Resource
	private SettlingMicrobeDao settlingMicrobeDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private RoomTemplateDao roomTemplateDao;
	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         SettlingMicrobe @throws
	 */
	public SettlingMicrobe get(String id) {
		SettlingMicrobe settlingMicrobe = commonDAO.get(SettlingMicrobe.class, id);
		return settlingMicrobe;
	}
	
	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         SettlingMicrobe @throws
	 */
	public CleanAreaMicrobe gets(String id) {
		CleanAreaMicrobe cleanAreaMicrobe = commonDAO.get(CleanAreaMicrobe.class, id);
		return cleanAreaMicrobe;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSettlingMicrobeItem(String delStr, String[] ids, User user, String settlingMicrobe_id)
			throws Exception {
		String delId = "";
		for (String id : ids) {
			SettlingMicrobeItem scp = settlingMicrobeDao.get(SettlingMicrobeItem.class, id);
			if (scp.getId() != null) {
				SettlingMicrobe pt = settlingMicrobeDao.get(SettlingMicrobe.class, scp.getSettlingMicrobe().getId());
				// 改变左侧样本状态
				SettlingMicrobeTemp settlingMicrobeTemp = this.commonDAO.get(SettlingMicrobeTemp.class,
						scp.getTempId());
				if (settlingMicrobeTemp != null) {
					pt.setSampleNum(pt.getSampleNum() - 1);
					settlingMicrobeTemp.setState("1");
					settlingMicrobeDao.update(settlingMicrobeTemp);
				}
				settlingMicrobeDao.update(pt);
				settlingMicrobeDao.delete(scp);
			}
			delId += scp.getSampleCode() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("CleanAreaMicrobe");
			li.setFileId(settlingMicrobe_id);
			li.setModifyContent(delStr + delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delSettlingMicrobeItemAf @Description: 重新排板 @author : @date @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSettlingMicrobeItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			SettlingMicrobeItem scp = settlingMicrobeDao.get(SettlingMicrobeItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				settlingMicrobeDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSettlingMicrobeResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr = "";
		for (String id : ids) {
			SettlingMicrobeInfo scp = settlingMicrobeDao.get(SettlingMicrobeInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				settlingMicrobeDao.delete(scp);
			idStr += scp.getSampleCode() + ",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setUserId(u.getId());
		li.setClassName("CleanAreaMicrobe");
		li.setFileId(mainId);
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSettlingMicrobeReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			SettlingMicrobeReagent scp = settlingMicrobeDao.get(SettlingMicrobeReagent.class, id);
			settlingMicrobeDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(delStr + scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSettlingMicrobeCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			SettlingMicrobeCos scp = settlingMicrobeDao.get(SettlingMicrobeCos.class, id);
			settlingMicrobeDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(delStr + scp.getName());
		}
	}

	/**
	 * 
	 * @Title: saveSettlingMicrobeTemplate @Description: 保存模板 @author : @date @param
	 *         sc void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveSettlingMicrobeTemplate(SettlingMicrobe sc) {
		List<SettlingMicrobeTemplate> tlist2 = settlingMicrobeDao.delTemplateItem(sc.getId());
		List<SettlingMicrobeReagent> rlist2 = settlingMicrobeDao.delReagentItem(sc.getId());
		List<SettlingMicrobeCos> clist2 = settlingMicrobeDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate().getId(), null);
		for (TemplateItem t : tlist) {
			SettlingMicrobeTemplate ptt = new SettlingMicrobeTemplate();
			ptt.setSettlingMicrobe(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			settlingMicrobeDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			SettlingMicrobeReagent ptr = new SettlingMicrobeReagent();
			ptr.setSettlingMicrobe(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			settlingMicrobeDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			SettlingMicrobeCos ptc = new SettlingMicrobeCos();
			ptc.setSettlingMicrobe(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			settlingMicrobeDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		SettlingMicrobe sct = settlingMicrobeDao.get(SettlingMicrobe.class, id);
		
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		settlingMicrobeDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		// submitSample(id, null);

	}
	
	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeStates(String applicationTypeActionId, String id) throws Exception {
		
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
		CleanAreaMicrobe sct = settlingMicrobeDao.get(CleanAreaMicrobe.class, id);
		if(applicationTypeActionId.equals("1997")) {
			sct.setState("1");
			sct.setStateName("作废");
		}else {
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		}
		sct.setConfirmUser(u);
		sct.setConfirmDate(format.format(new Date()));
		
		if(sct.getNotifierId()!=null
				&&!"".equals(sct.getNotifierId())){
			String userids[] = sct.getNotifierId().split(",");
			for(String userid:userids){
				if(userid!=null
						&&!"".equals(userid)){
					User uu = commonDAO.get(User.class, userid);
					SysRemind srz = new SysRemind();
					srz.setId(null);
					// 提醒类型
					srz.setType(null);
					// 事件标题
					srz.setTitle("洁净区浮游菌测试记录提醒");
					// 发起人（系统为SYSTEM）
					srz.setRemindUser(u.getName());
					// 发起时间
					srz.setStartDate(new Date());
					// 查阅提醒时间
					srz.setReadDate(new Date());
					// 提醒内容
					srz.setContent("区域："+sct.getRegion()+"车间/部门："+sct.getWorkshopDepartment()+"洁净区浮游菌测试记录提醒");
					// 状态 0.草稿  1.已阅 
					srz.setState("0");
					//提醒的用户
					srz.setHandleUser(uu);
					// 表单id
					srz.setContentId(sct.getId());
					srz.setTableId("CleanAreaMicrobe");
					commonDAO.saveOrUpdate(srz);
				}
			}
		}

		settlingMicrobeDao.update(sct);
	}

	/**
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : @date @param id @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		SettlingMicrobe sc = this.settlingMicrobeDao.get(SettlingMicrobe.class, id);
		// 获取结果表样本信息
		List<SettlingMicrobeInfo> list;
		if (ids == null)
			list = this.settlingMicrobeDao.selectAllResultListById(id);
		else
			list = this.settlingMicrobeDao.selectAllResultListByIds(ids);

		for (SettlingMicrobeInfo scp : list) {
			if (scp != null && scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("SettlingMicrobeInfo");
						settlingMicrobeDao.saveOrUpdate(st);

					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0017")) {// 核酸提取
						// 核酸提取
						DnaTaskTemp d = new DnaTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						settlingMicrobeDao.saveOrUpdate(d);
					} else if (next.equals("0018")) {// 文库构建
						// 文库构建
						WkTaskTemp d = new WkTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						settlingMicrobeDao.saveOrUpdate(d);

					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					SettlingMicrobeAbnormal pa = new SettlingMicrobeAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService.saveSampleState(scp.getCode(), scp.getSampleCode(), scp.getProductId(),
						scp.getProductName(), "", sc.getCreateDate(), format.format(new Date()), "SettlingMicrobe",
						"沉降菌检测",
						(User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY),
						id, scp.getNextFlow(), scp.getResult(), null, null, null, null, null, null, null, null);

				scp.setSubmit("1");
				settlingMicrobeDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findSettlingMicrobeTable @Description: 展示主表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findSettlingMicrobeTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return settlingMicrobeDao.findSettlingMicrobeTable(start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: selectSettlingMicrobeTempTable @Description: 展示临时表 @author
	 *         : @date @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectSettlingMicrobeTempTable(String[] codes, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return settlingMicrobeDao.selectSettlingMicrobeTempTable(codes, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String itemJson, String[] tempId, String userId, String templateId,
			String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			SettlingMicrobe pt = new SettlingMicrobe();
			pt = (SettlingMicrobe) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				String modelName = "SettlingMicrobe";
				String markCode = "SM";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			} else {
				id = pt.getId();
				pt = commonDAO.get(SettlingMicrobe.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String[] users = userId.split(",");
			String userName = "";
			for (int i = 0; i < users.length; i++) {
				if (i == users.length - 1) {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName();
				} else {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName() + ",";
				}
			}
			pt.setTestUserOneName(userName);
			settlingMicrobeDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveSettlingMicrobeTemplate(pt);
			}
			if (itemJson != null) {
				List<Map<String, Object>> listJsonData = JsonUtils.toListByJsonArray(itemJson, List.class);
				SettlingMicrobeItem item = new SettlingMicrobeItem();
				item = (SettlingMicrobeItem) commonDAO.Map2Bean(listJsonData.get(0), item);
				if ((item.getId() != null && "".equals(item.getId())) || item.getId().equals("NEW")) {
					item.setId(null);
					item.setState("1");
					item.setSettlingMicrobe(pt);
					commonDAO.saveOrUpdate(item);
				} else {
					commonDAO.saveOrUpdate(item);
				}
			}
			/*
			 * if (tempId != null) { if (pt.getTemplate() != null) { for (String temp :
			 * tempId) { SettlingMicrobeTemp ptt = settlingMicrobeDao.get(
			 * SettlingMicrobeTemp.class, temp); if (t.getIsSeparate().equals("1")) {
			 * List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
			 * .getMappingPrimersLibraryItem(ptt .getProductId()); for
			 * (MappingPrimersLibraryItem mpi : listMPI) { SettlingMicrobeItem pti = new
			 * SettlingMicrobeItem(); pti.setSampleCode(ptt.getSampleCode());
			 * pti.setProductId(ptt.getProductId());
			 * pti.setProductName(ptt.getProductName());
			 * pti.setDicSampleTypeId(t.getDicSampleType() != null &&
			 * !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getId() : t
			 * .getDicSampleTypeId()); pti.setDicSampleTypeName(t .getDicSampleType() !=
			 * null && !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getName() :
			 * t .getDicSampleTypeName()); pti.setProductName(ptt.getProductName());
			 * pti.setProductNum(t.getProductNum() != null && !"".equals(t.getProductNum())
			 * ? t .getProductNum() : t .getProductNum1()); pti.setCode(ptt.getCode()); if
			 * (t.getStorageContainer() != null) { pti.setState("1"); }else{
			 * pti.setState("2"); } ptt.setState("2"); pti.setTempId(temp);
			 * pti.setSettlingMicrobe(pt); pti.setChromosomalLocation(mpi
			 * .getChromosomalLocation()); pti.setSampleInfo(ptt.getSampleInfo());
			 * settlingMicrobeDao.saveOrUpdate(pti); } } else { SettlingMicrobeItem pti =
			 * new SettlingMicrobeItem(); pti.setSampleCode(ptt.getSampleCode());
			 * pti.setProductId(ptt.getProductId());
			 * pti.setProductName(ptt.getProductName());
			 * pti.setDicSampleTypeId(t.getDicSampleType() != null &&
			 * !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getId() : t
			 * .getDicSampleTypeId()); pti.setProductNum(t.getProductNum() != null &&
			 * !"".equals(t.getProductNum()) ? t .getProductNum() : t.getProductNum1());
			 * pti.setDicSampleTypeName(t.getDicSampleType() != null &&
			 * !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getName() : t
			 * .getDicSampleTypeName()); pti.setCode(ptt.getCode());
			 * pti.setSampleInfo(ptt.getSampleInfo()); if (t.getStorageContainer() != null)
			 * { pti.setState("1"); }else{ pti.setState("2"); } ptt.setState("2");
			 * pti.setTempId(temp); pti.setSettlingMicrobe(pt);
			 * settlingMicrobeDao.saveOrUpdate(pti); } settlingMicrobeDao.saveOrUpdate(ptt);
			 * } }
			 * 
			 * 
			 * }
			 */

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setClassName("CleanAreaMicrobe");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findSettlingMicrobeItemTable @Description:展示未排版样本 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findSettlingMicrobeItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return settlingMicrobeDao.findSettlingMicrobeItemTable(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排板数据 @author : @date @param id @param
	 *         item @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<SettlingMicrobeItem> saveItems = new ArrayList<SettlingMicrobeItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		SettlingMicrobe pt = commonDAO.get(SettlingMicrobe.class, id);
		SettlingMicrobeItem scp = new SettlingMicrobeItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (SettlingMicrobeItem) settlingMicrobeDao.Map2Bean(map, scp);
			SettlingMicrobeItem pti = commonDAO.get(SettlingMicrobeItem.class, scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());
			scp.setSettlingMicrobe(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		settlingMicrobeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate @Description: 展示孔板 @author : @date @param
	 *         id @return @throws Exception List<SettlingMicrobeItem> @throws
	 */
	public List<SettlingMicrobeItem> showWellPlate(String id) throws Exception {
		List<SettlingMicrobeItem> list = settlingMicrobeDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findSettlingMicrobeItemAfTable @Description: 展示排板后 @author
	 *         : @date @param scId @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findSettlingMicrobeItemAfTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return settlingMicrobeDao.findSettlingMicrobeItemAfTable(scId, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			SettlingMicrobeItem pti = commonDAO.get(SettlingMicrobeItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			settlingMicrobeDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showSettlingMicrobeStepsJson @Description: 展示实验步骤 @author
	 *         : @date @param id @param code @return Map<String,Object> @throws
	 */
	public Map<String, Object> showSettlingMicrobeStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<SettlingMicrobeTemplate> pttList = settlingMicrobeDao.showSettlingMicrobeStepsJson(id, code);
		List<SettlingMicrobeReagent> ptrList = settlingMicrobeDao.showSettlingMicrobeReagentJson(id, code);
		List<SettlingMicrobeCos> ptcList = settlingMicrobeDao.showSettlingMicrobeCosJson(id, code);
		List<Object> plate = settlingMicrobeDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showSettlingMicrobeResultTableJson @Description: 展示结果 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> showSettlingMicrobeResultTableJson(String id, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return settlingMicrobeDao.showSettlingMicrobeResultTableJson(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @param id @param
	 *         counts @return Map<String,Object> @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		SettlingMicrobe pt = commonDAO.get(SettlingMicrobe.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SettlingMicrobeItem> list = settlingMicrobeDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent @Description: 上传结果 @author : @date @param id @param
	 *         fileId @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = settlingMicrobeDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			SettlingMicrobe pt = settlingMicrobeDao.get(SettlingMicrobe.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<SettlingMicrobeInfo> listPTI = settlingMicrobeDao.findSettlingMicrobeInfoByCode(code);
						if (listPTI.size() > 0) {
							SettlingMicrobeInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							settlingMicrobeDao.saveOrUpdate(spi);
						}

					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @param id @param
	 *         templateJson @param reagentJson @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson, String cosJson, String logInfo) {
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}") && !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}") && !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate @Description: 保存模板明细 @author : @date @param id @param
	 *         templateJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			SettlingMicrobeTemplate ptt = settlingMicrobeDao.get(SettlingMicrobeTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			settlingMicrobeDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent @Description: 保存试剂 @author : @date @param id @param
	 *         reagentJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {

		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			SettlingMicrobeReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(SettlingMicrobeReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new SettlingMicrobeReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setSettlingMicrobe(commonDAO.get(SettlingMicrobe.class, id));
				}

			}
			settlingMicrobeDao.saveOrUpdate(ptr);
		}

	}

	/**
	 * 
	 * @Title: saveCos @Description: 保存设备 @author : @date @param id @param cosJson
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {

		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			SettlingMicrobeCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(SettlingMicrobeCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new SettlingMicrobeCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setSettlingMicrobe(commonDAO.get(SettlingMicrobe.class, id));
				}
			}
			settlingMicrobeDao.saveOrUpdate(ptc);
		}

	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 孔板样本列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return settlingMicrobeDao.plateSampleTable(id, counts, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @param id @throws
	 *         Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {

		List<SettlingMicrobeItem> list = settlingMicrobeDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SettlingMicrobeInfo> spiList = settlingMicrobeDao.selectResultListById(id);
		SettlingMicrobe settlingMicrobe = commonDAO.get(SettlingMicrobe.class, id);
		for (SettlingMicrobeItem pti : list) {
			SettlingMicrobeInfo scp = new SettlingMicrobeInfo();
			scp.setId(pti.getId());
			scp.setRoomNumber(pti.getRoomNumber());
			scp.setSamplingNumber(pti.getSamplingNumber());
			scp.setCultureDishNumber(pti.getCultureDishNumber());
			scp.setClumpCount(pti.getClumpCount());
			scp.setResult("1");
			scp.setSettlingMicrobe(settlingMicrobe);
			pti.setState("2");
			commonDAO.saveOrUpdate(pti);
			commonDAO.saveOrUpdate(scp);
			/*
			 * boolean b = true; for (SettlingMicrobeInfo spi : spiList) { if
			 * (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) { b = false; } } if
			 * (b) { if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) { if
			 * (pti.getProductNum() != null && !"".equals(pti.getProductNum())) { Integer pn
			 * = Integer.parseInt(pti.getProductNum()); for (int i = 0; i < pn; i++) {
			 * SettlingMicrobeInfo scp = new SettlingMicrobeInfo(); DicSampleType ds =
			 * commonDAO.get( DicSampleType.class, pti.getDicSampleTypeId());
			 * scp.setDicSampleType(ds); scp.setSampleCode(pti.getSampleCode());
			 * scp.setProductId(pti.getProductId());
			 * scp.setProductName(pti.getProductName());
			 * scp.setSampleInfo(pti.getSampleInfo());
			 * scp.setSettlingMicrobe(pti.getSettlingMicrobe()); scp.setResult("1"); String
			 * markCode = ""; if (scp.getDicSampleType() != null) { DicSampleType d =
			 * commonDAO.get( DicSampleType.class, scp .getDicSampleType().getId());
			 * String[] sampleCode = scp.getSampleCode() .split(","); if (sampleCode.length
			 * > 1) { String str = sampleCode[0].substring(0, sampleCode[0].length() - 4);
			 * for (int j = 0; j < sampleCode.length; j++) { str += sampleCode[j].substring(
			 * sampleCode[j].length() - 4, sampleCode[j].length()); } if (d == null) {
			 * markCode = str; } else { markCode = str + d.getCode(); } } else { if (d ==
			 * null) { markCode = scp.getSampleCode(); } else { markCode =
			 * scp.getSampleCode() + d.getCode(); } }
			 * 
			 * } String code = codingRuleService.getCode( "SettlingMicrobeInfo", markCode,
			 * 00, 2, null); scp.setCode(code); settlingMicrobeDao.saveOrUpdate(scp); } } }
			 * else { if (!map.containsKey(String.valueOf(pti.getBlendCode()))) { if
			 * (pti.getProductNum() != null && !"".equals(pti.getProductNum())) { Integer pn
			 * = Integer.parseInt(pti.getProductNum()); for (int i = 0; i < pn; i++) {
			 * SettlingMicrobeInfo scp = new SettlingMicrobeInfo(); DicSampleType ds =
			 * commonDAO.get( DicSampleType.class, pti.getDicSampleTypeId());
			 * scp.setDicSampleType(ds); scp.setSampleCode(pti.getSampleCode());
			 * scp.setProductId(pti.getProductId());
			 * scp.setProductName(pti.getProductName());
			 * scp.setSampleInfo(pti.getSampleInfo());
			 * scp.setSettlingMicrobe(pti.getSettlingMicrobe()); scp.setResult("1"); String
			 * markCode = ""; if (scp.getDicSampleType() != null) { DicSampleType d =
			 * commonDAO .get(DicSampleType.class, scp .getDicSampleType().getId());
			 * String[] sampleCode = scp.getSampleCode() .split(","); if (sampleCode.length
			 * > 1) { String str = sampleCode[0].substring(0, sampleCode[0].length() - 4);
			 * for (int j = 0; j < sampleCode.length; j++) { str += sampleCode[j].substring(
			 * sampleCode[j].length() - 4, sampleCode[j].length()); } if (d == null) {
			 * markCode = str; } else { markCode = str + d.getCode(); } } else { if (d ==
			 * null) { markCode = scp.getSampleCode(); } else { markCode =
			 * scp.getSampleCode() + d.getCode(); } }
			 * 
			 * } String code = codingRuleService.getCode( "SettlingMicrobeInfo", markCode,
			 * 00, 2, null); scp.setCode(code); map.put(String.valueOf(pti.getBlendCode()),
			 * code); settlingMicrobeDao.saveOrUpdate(scp); } } } else { String code =
			 * (String) map.get(String.valueOf(pti .getBlendCode())); SettlingMicrobeInfo
			 * spi = settlingMicrobeDao .getResultByCode(code);
			 * spi.setSampleCode(spi.getSampleCode() + "," + pti.getSampleCode());
			 * settlingMicrobeDao.saveOrUpdate(spi); } } }
			 */
		}

	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果 @author : @date @param id @param
	 *         dataJson @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo, String confirmUser) throws Exception {

		List<SettlingMicrobeInfo> saveItems = new ArrayList<SettlingMicrobeInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		SettlingMicrobe pt = commonDAO.get(SettlingMicrobe.class, id);
		for (Map<String, Object> map : list) {
			SettlingMicrobeInfo scp = new SettlingMicrobeInfo();
			// 将map信息读入实体类
			scp = (SettlingMicrobeInfo) settlingMicrobeDao.Map2Bean(map, scp);
			scp.setSettlingMicrobe(pt);
			saveItems.add(scp);
		}
		if (confirmUser != null && !"".equals(confirmUser)) {
			User confirm = commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			settlingMicrobeDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		settlingMicrobeDao.saveOrUpdateAll(saveItems);

	}

	public List<SettlingMicrobeItem> findSettlingMicrobeItemList(String scId) throws Exception {
		List<SettlingMicrobeItem> list = settlingMicrobeDao.selectSettlingMicrobeItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return settlingMicrobeDao.generateBlendCode(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(SettlingMicrobe pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				SettlingMicrobeItem pti = new SettlingMicrobeItem();
				pti.setSettlingMicrobe(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				settlingMicrobeDao.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRightQuality(SettlingMicrobe pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				SettlingMicrobeItem pti = new SettlingMicrobeItem();
				pti.setSettlingMicrobe(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public SettlingMicrobeInfo getInfoById(String idc) {
		return settlingMicrobeDao.get(SettlingMicrobeInfo.class, idc);

	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 浮游菌主表列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCleanAreaMicrodeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return settlingMicrobeDao.findCleanAreaMicrodeTable(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 浮游菌子表列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCleanAreaMicrodeItemTable(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		return settlingMicrobeDao.findCleanAreaMicrodeItemTable(id, start, length, map2Query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 阴性对照子表列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCleanAreaNegativeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return settlingMicrobeDao.findCleanAreaNegativeTable(id, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMicrobeInfoItem(CleanAreaMicrobe sc, String itemDataJson, String itemDataTow, String logInfo,String changeLogItem,String changeLogItem2,String log)
			throws Exception {
		List<CleanAreaMicrobeItem> saveItems = new ArrayList<CleanAreaMicrobeItem>();
		List<CleanAreaMicrobeItemNegative> saveItemstow = new ArrayList<CleanAreaMicrobeItemNegative>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		List<Map<String, Object>> listtow = JsonUtils.toListByJsonArray(itemDataTow, List.class);
		// 浮游菌子表
		for (Map<String, Object> map : list) {
			CleanAreaMicrobeItem scp = new CleanAreaMicrobeItem();
			RoomManagement rm = new RoomManagement();
			Instrument im = new Instrument();
			// 将map信息读入实体类
			scp = (CleanAreaMicrobeItem) settlingMicrobeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			if("0".equals(sc.getType())) {
				rm=commonDAO.get(RoomManagement.class, scp.getRoomNum());
				if (scp.getMonitoringResults() != "" 
						&& scp.getSamplingQuantity() != "") {
					// 监测结果
					Double num1 = Double.valueOf(scp.getMonitoringResults());
					//
					Double num2 = Double.valueOf(scp.getSamplingQuantity());
					Double num3 = (num1*1000)/num2;
					scp.setComputationResults(String.valueOf(num3));
					if(scp.getEligibilityCriteria() !="") {
						Double num4 = Double.valueOf(scp.getEligibilityCriteria());
						if (num3 > num4) {
							// 合格
							scp.setResult("否");
							rm.setRoomState("0");
						} else if (num3 <= num4) {
							// 不合格
							scp.setResult("是");
							rm.setRoomState("1");
						} else {}
					}
				}/*
				if (scp.getMonitoringResults() != "" 
						&& sc.getCordon() != ""
						&& sc.getDeviationCorrectionLine() != ""
						&& scp.getEligibilityCriteria()!="") {
					// 监测结果
					Double num1 = Double.valueOf(scp.getMonitoringResults());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine());
					// 合格标准cfu/m³
					Double num4 = Double.valueOf(scp.getEligibilityCriteria());
					
						
					if (num1 <= num2) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setResult("是");
						rm.setRoomState("1");
					} else {
						// 不合格
						scp.setResult("否");
						rm.setRoomState("0");
					}
				}else {
					
				}*/
				commonDAO.saveOrUpdate(rm);
			}else if("1".equals(sc.getType())) {
				im=commonDAO.get(Instrument.class, scp.getRoomNum());
				if (scp.getMonitoringResults() != "" 
						&& scp.getSamplingQuantity() != "") {
					// 监测结果
					Double num1 = Double.valueOf(scp.getMonitoringResults());
					//
					Double num2 = Double.valueOf(scp.getSamplingQuantity());
					Double num3 = (num1*1000)/num2;
					scp.setComputationResults(String.valueOf(num3));
					if(scp.getEligibilityCriteria() !="") {
						Double num4 = Double.valueOf(scp.getEligibilityCriteria());
						if (num3 > num4) {
							// 合格
							scp.setResult("否");
							DicState ds = commonDAO.get(DicState.class, "0r");
							if(ds!=null) {
								im.setState(ds);
							}
						} else if (num3 <= num4) {
							// 不合格
							scp.setResult("是");
							DicState ds = commonDAO.get(DicState.class, "1r");
							if(ds!=null) {
								im.setState(ds);
							}
						} else {}
					}
				}
				/*
				if (scp.getMonitoringResults() != "" 
						&& sc.getCordon() != ""
						&& sc.getDeviationCorrectionLine() != ""
						&& scp.getEligibilityCriteria()!="") {
					// 监测结果
					Double num1 = Double.valueOf(scp.getMonitoringResults());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine());
					// 合格标准cfu/m³
					Double num4 = Double.valueOf(scp.getEligibilityCriteria());
						
					if (num1 <= num2) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setResult("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else {
						// 不合格
						scp.setResult("否");
						DicState ds = commonDAO.get(DicState.class, "0r");
						if(ds!=null) {
							im.setState(ds);
						}
					}
				}else {
					
				}*/
				commonDAO.saveOrUpdate(im);
			}
			
			scp.setCleanAreaMicrobe(sc);
			saveItems.add(scp);
			
		}
		// 阴性对照
		for (Map<String, Object> maps : listtow) {
			CleanAreaMicrobeItemNegative scp2 = new CleanAreaMicrobeItemNegative();
			// 将map信息读入实体类
			scp2 = (CleanAreaMicrobeItemNegative) settlingMicrobeDao.Map2Bean(maps, scp2);
			if (scp2.getId() != null && scp2.getId().equals("")) {
				scp2.setId(null);
				scp2.setState("0");
			}
			scp2.setCleanAreaMicrobe(sc);
			saveItemstow.add(scp2);
		}
		settlingMicrobeDao.saveOrUpdateAll(saveItems);
		settlingMicrobeDao.saveOrUpdateAll(saveItemstow);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			settlingMicrobeDao.saveOrUpdate(li);
		}
		
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			settlingMicrobeDao.saveOrUpdate(li);
		}
		if (changeLogItem2 != null && !"".equals(changeLogItem2)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaMicrobe");
			li.setModifyContent(changeLogItem2);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			settlingMicrobeDao.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void uploadCsvFile(String id, String flag, String fileId) throws Exception {
		CleanAreaMicrobe cm = settlingMicrobeDao.get(CleanAreaMicrobe.class, id);
		FileInfo fileInfo = settlingMicrobeDao.get(FileInfo.class, fileId);
		String filePath = fileInfo.getFilePath();
		File file = new File(filePath);
		InputStream is = new FileInputStream(file);
		List<CleanAreaMicrobeItem> itemList = settlingMicrobeDao.findCleanAreaMicrobeItemTable(id);
		if (file.isFile()) {
			CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
			reader.readHeaders();// 跳过表头
			if(itemList.size()>0) {
				while (reader.readRecord()) {
					for(CleanAreaMicrobeItem cam:itemList) {
						if (flag != null && flag.equals("1")) {
							if(cam.getRoomName().equals(reader.get(0))&&cam.getRoomNum().equals(reader.get(1))) {
								if(cam.getMonitoringPoint().equals(reader.get(2))) {
									cam.setCleanAreaMicrobe(cm);
									cam.setRoomName(reader.get(0));
									cam.setRoomNum(reader.get(1));
									cam.setMonitoringPoint(reader.get(2));
									cam.setCleanZoneGrade(reader.get(3));
									cam.setSamplingQuantity(reader.get(4));
									cam.setEligibilityCriteria(reader.get(5));
									cam.setMonitoringResults(reader.get(6));
									cam.setComputationResults(reader.get(7));
									cam.setResult(reader.get(8));
									cam.setNote(reader.get(9));
									settlingMicrobeDao.saveOrUpdate(cam);
								}
							}
						}
						if (flag != null && flag.equals("2")) {
							CleanAreaMicrobeItemNegative item = new CleanAreaMicrobeItemNegative();
							item.setId(null);
							item.setCleanAreaMicrobe(cm);
							item.setNegativeControl(reader.get(0));
							item.setObserver(reader.get(1));
							item.setObservationDate(reader.get(2));
							item.setNote(reader.get(3));
							settlingMicrobeDao.saveOrUpdate(item);
						}
					}
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delRoom(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			CleanAreaMicrobeItem scp = settlingMicrobeDao.get(CleanAreaMicrobeItem.class, id);
			settlingMicrobeDao.delete(scp);
			idStr += scp.getRoomName() + "的记录已删除";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setState("2");
		li.setStateName("数据删除");
		li.setClassName("CleanAreaMicrobe");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delNegative(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			CleanAreaMicrobeItemNegative scp = settlingMicrobeDao.get(CleanAreaMicrobeItemNegative.class, id);
			settlingMicrobeDao.delete(scp);
			idStr += "阴性对照"+scp.getNegativeControl() + "的记录已删除";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaMicrobe");
		li.setState("2");
		li.setStateName("数据删除");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}
	public void useTemplateAddItem(String id,String cleanAreaMicrobe_id) throws Exception {
		CleanAreaMicrobe cleanAreaMicrobe= settlingMicrobeDao.get(CleanAreaMicrobe.class, cleanAreaMicrobe_id);
		List<RoomTemplateItem> useTemplateAddItem = roomTemplateDao.findRoomTemplateItemTable(id);
		for (RoomTemplateItem roomTemplateItem : useTemplateAddItem) {
			
			if(!"".equals(roomTemplateItem.getMonitoringPoint())&& null!=roomTemplateItem.getMonitoringPoint()) {
				int num = Integer.valueOf(roomTemplateItem.getMonitoringPoint());
				for(int i=1;i<=num;i++) {
					CleanAreaMicrobeItem cleanAreaMicrobeItem = new CleanAreaMicrobeItem();
					cleanAreaMicrobeItem.setRoomNum(roomTemplateItem.getCode());
					cleanAreaMicrobeItem.setRoomName(roomTemplateItem.getName());
					cleanAreaMicrobeItem.setMonitoringPoint(String.valueOf(i));
					cleanAreaMicrobeItem.setCleanAreaMicrobe(cleanAreaMicrobe);
					settlingMicrobeDao.saveOrUpdate(cleanAreaMicrobeItem);
				}
			}else {
				CleanAreaMicrobeItem cleanAreaMicrobeItem = new CleanAreaMicrobeItem();
				cleanAreaMicrobeItem.setRoomNum(roomTemplateItem.getCode());
				cleanAreaMicrobeItem.setRoomName(roomTemplateItem.getName());
				cleanAreaMicrobeItem.setMonitoringPoint("1");
				cleanAreaMicrobeItem.setCleanAreaMicrobe(cleanAreaMicrobe);
				settlingMicrobeDao.saveOrUpdate(cleanAreaMicrobeItem);
			}
			
		}
	}
	public List<CleanAreaMicrobeItem> findCleanAreaMicrobeItemTable(String id) throws Exception {
		return settlingMicrobeDao.findCleanAreaMicrobeItemTable(id);
	}
}
