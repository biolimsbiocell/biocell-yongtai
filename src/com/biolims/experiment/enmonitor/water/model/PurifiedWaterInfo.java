package com.biolims.experiment.enmonitor.water.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import java.util.Date;
import com.biolims.dic.model.DicType;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 纯化水检测结果
 * @author lims-platform
 * @date 
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PURIFIED_WATER_INFO")
@SuppressWarnings("serial")
public class PurifiedWaterInfo extends EntityDao<PurifiedWaterInfo> implements
		java.io.Serializable {
	
	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private PurifiedWater purifiedWater;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 是否提交*/
	private String submit;
	/** 患者姓名 */
	private String patientName;
	/** 浓度 */
	private Double concentration;
	/** 任务单Id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 临床 1 科研 2*/
	private String classify;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 样本类型 */
	private String sampleType;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/** 应出报告日期 */
	private Date reportDate;
	/** 单位组 */
	private DicType unitGroup;
    /**状态*/
	private String state;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	
	
	/**房间号*/
	private String roomNumber;
	/**性状*/
	private String characters;
	/**酸碱度*/
	private String phValue;
	/**硝酸盐*/
	private String nitrate;
	/**亚硝酸盐*/
	private String nitrite;
	/**氨*/
	private String ammonia;
	/**电导率*/
	private String conductivity;
	/**TOC*/
	private String toc;
	/**重金属*/
	private String heavyMetal;
	/**微生物限度*/
	private String microbialLimit;
	
	
	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getCharacters() {
		return characters;
	}

	public void setCharacters(String characters) {
		this.characters = characters;
	}

	public String getPhValue() {
		return phValue;
	}

	public void setPhValue(String phValue) {
		this.phValue = phValue;
	}

	public String getNitrate() {
		return nitrate;
	}

	public void setNitrate(String nitrate) {
		this.nitrate = nitrate;
	}

	public String getNitrite() {
		return nitrite;
	}

	public void setNitrite(String nitrite) {
		this.nitrite = nitrite;
	}

	public String getAmmonia() {
		return ammonia;
	}

	public void setAmmonia(String ammonia) {
		this.ammonia = ammonia;
	}

	public String getConductivity() {
		return conductivity;
	}

	public void setConductivity(String conductivity) {
		this.conductivity = conductivity;
	}

	public String getToc() {
		return toc;
	}

	public void setToc(String toc) {
		this.toc = toc;
	}

	public String getHeavyMetal() {
		return heavyMetal;
	}

	public void setHeavyMetal(String heavyMetal) {
		this.heavyMetal = heavyMetal;
	}

	public String getMicrobialLimit() {
		return microbialLimit;
	}

	public void setMicrobialLimit(String microbialLimit) {
		this.microbialLimit = microbialLimit;
	}

	public String getState() {
		return state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	/*@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")*/
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血浆编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血浆编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 36)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得DicUnit
	 * 
	 * @return: DicUnit 单位
	 */
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	/**
	 * 方法: 取得PurifiedWater
	 * 
	 * @return: PurifiedWater 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PURIFIED_WATER")
	public PurifiedWater getPurifiedWater() {
		return this.purifiedWater;
	}

	/**
	 * 方法: 设置PurifiedWater
	 * 
	 * @param: PurifiedWater 相关主表
	 */
	public void setPurifiedWater(PurifiedWater purifiedWater) {
		this.purifiedWater = purifiedWater;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}