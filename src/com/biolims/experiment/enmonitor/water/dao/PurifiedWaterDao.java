package com.biolims.experiment.enmonitor.water.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.enmonitor.water.model.PurifiedWater;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterCos;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterItem;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterReagent;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterTemp;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterTemplate;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class PurifiedWaterDao extends BaseHibernateDao {

	public Map<String, Object> findPurifiedWaterTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurifiedWater where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PurifiedWater where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PurifiedWater> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectPurifiedWaterTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from PurifiedWaterTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<PurifiedWaterTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from PurifiedWaterTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findPurifiedWaterItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurifiedWaterItem where 1=1 and state='1' and purifiedWater.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PurifiedWaterItem  where 1=1 and state='1' and purifiedWater.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PurifiedWaterItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<PurifiedWaterItem> showWellPlate(String id) {
		String hql="from PurifiedWaterItem  where 1=1 and  purifiedWater.id='"+id+"'";
		List<PurifiedWaterItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findPurifiedWaterItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurifiedWaterItem where 1=1 and state='2' and purifiedWater.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PurifiedWaterItem  where 1=1 and state='2' and purifiedWater.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PurifiedWaterItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from PurifiedWaterItem where 1=1 and state='2' and purifiedWater.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from PurifiedWaterItem where 1=1 and state='2' and purifiedWater.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<PurifiedWaterTemplate> showPurifiedWaterStepsJson(String id,String code) {

		String countHql = "select count(*) from PurifiedWaterTemplate where 1=1 and purifiedWater.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<PurifiedWaterTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PurifiedWaterTemplate where 1=1 and purifiedWater.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PurifiedWaterReagent> showPurifiedWaterReagentJson(String id,String code) {
		String countHql = "select count(*) from PurifiedWaterReagent where 1=1 and purifiedWater.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<PurifiedWaterReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PurifiedWaterReagent where 1=1 and purifiedWater.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PurifiedWaterCos> showPurifiedWaterCosJson(String id,String code) {
		String countHql = "select count(*) from PurifiedWaterCos where 1=1 and purifiedWater.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<PurifiedWaterCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PurifiedWaterCos where 1=1 and purifiedWater.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PurifiedWaterTemplate> delTemplateItem(String id) {
		List<PurifiedWaterTemplate> list=new ArrayList<PurifiedWaterTemplate>();
		String hql = "from PurifiedWaterTemplate where 1=1 and purifiedWater.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<PurifiedWaterReagent> delReagentItem(String id) {
		List<PurifiedWaterReagent> list=new ArrayList<PurifiedWaterReagent>();
		String hql = "from PurifiedWaterReagent where 1=1 and purifiedWater.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<PurifiedWaterCos> delCosItem(String id) {
		List<PurifiedWaterCos> list=new ArrayList<PurifiedWaterCos>();
		String hql = "from PurifiedWaterCos where 1=1 and purifiedWater.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showPurifiedWaterResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurifiedWaterInfo where 1=1 and purifiedWater.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PurifiedWaterInfo  where 1=1 and purifiedWater.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PurifiedWaterInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from PurifiedWaterItem where 1=1 and purifiedWater.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<PurifiedWaterItem> plateSample(String id, String counts) {
		String hql="from PurifiedWaterItem where 1=1 and purifiedWater.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<PurifiedWaterItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PurifiedWaterItem where 1=1 and purifiedWater.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PurifiedWaterItem  where 1=1 and purifiedWater.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PurifiedWaterItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<PurifiedWaterInfo> findPurifiedWaterInfoByCode(String code) {
		String hql="from PurifiedWaterInfo where 1=1 and code='"+code+"'";
		List<PurifiedWaterInfo> list=new ArrayList<PurifiedWaterInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<PurifiedWaterInfo> selectAllResultListById(String code) {
		String hql = "from PurifiedWaterInfo  where (submit is null or submit='') and purifiedWater.id='"
				+ code + "'";
		List<PurifiedWaterInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<PurifiedWaterInfo> selectResultListById(String id) {
		String hql = "from PurifiedWaterInfo  where purifiedWater.id='"
				+ id + "'";
		List<PurifiedWaterInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<PurifiedWaterInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from PurifiedWaterInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<PurifiedWaterInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<PurifiedWaterItem> selectPurifiedWaterItemList(String scId)
			throws Exception {
		String hql = "from PurifiedWaterItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and purifiedWater.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<PurifiedWaterItem> list = new ArrayList<PurifiedWaterItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from PurifiedWaterItem where 1=1 and purifiedWater.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public PurifiedWaterInfo getResultByCode(String code) {
			String hql=" from PurifiedWaterInfo where 1=1 and code='"+code+"'";
			PurifiedWaterInfo spi=(PurifiedWaterInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}