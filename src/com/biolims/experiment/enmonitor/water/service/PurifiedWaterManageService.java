package com.biolims.experiment.enmonitor.water.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.enmonitor.water.dao.PurifiedWaterManageDao;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class PurifiedWaterManageService {
	@Resource
	private PurifiedWaterManageDao purifiedWaterManageDao;
	@Resource
	private CommonDAO commonDAO;



	/**
	 * 
	 * @Title: purifiedWaterManageItemRuku
	 * @Description: 入库
	 * @author : 
	 * @date 
	 * @param ids
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void purifiedWaterManageItemRuku(String[] ids) {
		for (String id : ids) {
			PurifiedWaterItem scp = purifiedWaterManageDao.get(PurifiedWaterItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				if (scp.getCode() == null) {
					st.setCode(scp.getSampleCode());
				} else {
					st.setCode(scp.getCode());
				}
				st.setSampleCode(scp.getSampleCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				purifiedWaterManageDao.saveOrUpdate(st);
			}

		}
	}

	/**
	 * 
	 * @Title: showPurifiedWaterManageJson
	 * @Description: 展示样本管理明细
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showPurifiedWaterManageJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterManageDao.showPurifiedWaterManageJson(start, length, query,
				col, sort);
	}
}

