﻿package com.biolims.experiment.enmonitor.water.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.main.service.InstrumentStateService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeInfo;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobeItem;
import com.biolims.experiment.enmonitor.water.dao.PurifiedWaterDao;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterAbnormal;
import com.biolims.log.model.LogInfo;
import com.biolims.experiment.enmonitor.water.model.PurifiedWater;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterCos;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterItem;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterReagent;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterTemp;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterTemplate;
import com.biolims.experiment.enmonitor.water.model.PurifiedWaterInfo;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.biolims.sample.service.SampleReceiveService;
import com.opensymphony.xwork2.ActionContext;
@Service
@Transactional
public class PurifiedWaterService {
	
	@Resource
	private PurifiedWaterDao purifiedWaterDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : 
	 * @date 
	 * @param id
	 * @return PurifiedWater
	 * @throws
	 */
	public PurifiedWater get(String id) {
		PurifiedWater purifiedWater = commonDAO.get(PurifiedWater.class, id);
		return purifiedWater;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurifiedWaterItem(String delStr, String[] ids, User user, String purifiedWater_id) throws Exception {
		String delId="";
		for (String id : ids) {
			PurifiedWaterItem scp = purifiedWaterDao.get(PurifiedWaterItem.class, id);
			if (scp.getId() != null) {
				PurifiedWater pt = purifiedWaterDao.get(PurifiedWater.class, scp
						.getPurifiedWater().getId());
				// 改变左侧样本状态
				PurifiedWaterTemp purifiedWaterTemp = this.commonDAO.get(
						PurifiedWaterTemp.class, scp.getTempId());
				if (purifiedWaterTemp != null) {
				pt.setSampleNum(pt.getSampleNum() - 1);
					purifiedWaterTemp.setState("1");
					purifiedWaterDao.update(purifiedWaterTemp);
				}
				purifiedWaterDao.update(pt);
				purifiedWaterDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(purifiedWater_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delPurifiedWaterItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurifiedWaterItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			PurifiedWaterItem scp = purifiedWaterDao.get(PurifiedWaterItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				purifiedWaterDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurifiedWaterResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr="";
		for (String id : ids) {
			PurifiedWaterInfo scp = purifiedWaterDao
					.get(PurifiedWaterInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				purifiedWaterDao.delete(scp);
			idStr+=scp.getSampleCode()+",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setModifyContent(delStr+idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurifiedWaterReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			PurifiedWaterReagent scp = purifiedWaterDao.get(PurifiedWaterReagent.class,
					id);
			purifiedWaterDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr+scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPurifiedWaterCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			PurifiedWaterCos scp = purifiedWaterDao.get(PurifiedWaterCos.class, id);
			purifiedWaterDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr+scp.getName());
		}
	}

	/**
	 * 
	 * @Title: savePurifiedWaterTemplate
	 * @Description: 保存模板
	 * @author : 
	 * @date 
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void savePurifiedWaterTemplate(PurifiedWater sc) {
		List<PurifiedWaterTemplate> tlist2 = purifiedWaterDao.delTemplateItem(sc
				.getId());
		List<PurifiedWaterReagent> rlist2 = purifiedWaterDao.delReagentItem(sc
				.getId());
		List<PurifiedWaterCos> clist2 = purifiedWaterDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		for (TemplateItem t : tlist) {
			PurifiedWaterTemplate ptt = new PurifiedWaterTemplate();
			ptt.setPurifiedWater(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			purifiedWaterDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			PurifiedWaterReagent ptr = new PurifiedWaterReagent();
			ptr.setPurifiedWater(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			purifiedWaterDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			PurifiedWaterCos ptc = new PurifiedWaterCos();
			ptc.setPurifiedWater(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			purifiedWaterDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : 
	 * @date 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		PurifiedWater sct = purifiedWaterDao.get(PurifiedWater.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		purifiedWaterDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

//		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		PurifiedWater sc = this.purifiedWaterDao.get(PurifiedWater.class, id);
		// 获取结果表样本信息
		List<PurifiedWaterInfo> list;
		if (ids == null)
			list = this.purifiedWaterDao.selectAllResultListById(id);
		else
			list = this.purifiedWaterDao.selectAllResultListByIds(ids);

		for (PurifiedWaterInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("PurifiedWaterInfo");
						purifiedWaterDao.saveOrUpdate(st);
					
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0017")) {// 核酸提取
						// 核酸提取
						DnaTaskTemp d = new DnaTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						purifiedWaterDao.saveOrUpdate(d);
					} else if (next.equals("0018")) {// 文库构建
						// 文库构建
						WkTaskTemp d = new WkTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						purifiedWaterDao.saveOrUpdate(d);
					
					} else {
					    scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					PurifiedWaterAbnormal pa = new PurifiedWaterAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"PurifiedWater",
								"纯化水检测",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				
				scp.setSubmit("1");
				purifiedWaterDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findPurifiedWaterTable
	 * @Description: 展示主表
	 * @author :
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findPurifiedWaterTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterDao.findPurifiedWaterTable(start, length, query, col,
				sort);
	}

	/**
	 * 
	 * @Title: selectPurifiedWaterTempTable
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectPurifiedWaterTempTable(String[] codes, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterDao.selectPurifiedWaterTempTable(codes,start, length, query,
				col, sort);
	}
	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : 
	 * @date 
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String itemJson, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			PurifiedWater pt = new PurifiedWater();
			pt = (PurifiedWater) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getName();
			Integer sampleNum=pt.getSampleNum();
			Integer maxNum=pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "PurifiedWater";
				String markCode = "PW";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				
			} else {
				id = pt.getId();
				pt=commonDAO.get(PurifiedWater.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			pt.setTestUserOneName(userName);
			purifiedWaterDao.saveOrUpdate(pt);			
            if (pt.getTemplate() != null) {
				savePurifiedWaterTemplate(pt);
			}
            if(itemJson!=null) {
				List<Map<String, Object>> listJsonData = JsonUtils.toListByJsonArray(itemJson, List.class);
				PurifiedWaterItem item = new PurifiedWaterItem();
				item = (PurifiedWaterItem) commonDAO.Map2Bean(listJsonData.get(0), item);
				if((item.getId()!=null&&"".equals(item.getId()))||item.getId().equals("NEW")){
					item.setId(null);
					item.setState("1");
					item.setPurifiedWater(pt);
					commonDAO.saveOrUpdate(item);
				}else {
					commonDAO.saveOrUpdate(item);
				}
			}
			/*if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							PurifiedWaterTemp ptt = purifiedWaterDao.get(
									PurifiedWaterTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									PurifiedWaterItem pti = new PurifiedWaterItem();
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									pti.setTempId(temp);
									pti.setPurifiedWater(pt);
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									pti.setSampleInfo(ptt.getSampleInfo());
									purifiedWaterDao.saveOrUpdate(pti);
								}
							} else {
								PurifiedWaterItem pti = new PurifiedWaterItem();
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setPurifiedWater(pt);
								purifiedWaterDao.saveOrUpdate(pti);
							}
							purifiedWaterDao.saveOrUpdate(ptt);
						}
				}

			
			}*/
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findPurifiedWaterItemTable
	 * @Description:展示未排版样本
	 * @author : 
	 * @date 
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findPurifiedWaterItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterDao.findPurifiedWaterItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : 
	 * @date 
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<PurifiedWaterItem> saveItems = new ArrayList<PurifiedWaterItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		PurifiedWater pt = commonDAO.get(PurifiedWater.class, id);
		PurifiedWaterItem scp = new PurifiedWaterItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (PurifiedWaterItem) purifiedWaterDao.Map2Bean(map, scp);
			PurifiedWaterItem pti = commonDAO.get(PurifiedWaterItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());    
			scp.setPurifiedWater(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		purifiedWaterDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : 
	 * @date 
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<PurifiedWaterItem>
	 * @throws
	 */
	public List<PurifiedWaterItem> showWellPlate(String id) throws Exception {
		List<PurifiedWaterItem> list = purifiedWaterDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findPurifiedWaterItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date 
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findPurifiedWaterItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterDao.findPurifiedWaterItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			PurifiedWaterItem pti = commonDAO.get(PurifiedWaterItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			purifiedWaterDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showPurifiedWaterStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date 
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showPurifiedWaterStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<PurifiedWaterTemplate> pttList = purifiedWaterDao
				.showPurifiedWaterStepsJson(id, code);
		List<PurifiedWaterReagent> ptrList = purifiedWaterDao
				.showPurifiedWaterReagentJson(id, code);
		List<PurifiedWaterCos> ptcList = purifiedWaterDao.showPurifiedWaterCosJson(id,
				code);
		List<Object> plate = purifiedWaterDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showPurifiedWaterResultTableJson
	 * @Description: 展示结果
	 * @author : 
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showPurifiedWaterResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterDao.showPurifiedWaterResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		PurifiedWater pt = commonDAO.get(PurifiedWater.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<PurifiedWaterItem> list = purifiedWaterDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
		row = pt.getTemplate().getStorageContainer().getRowNum();
		col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = purifiedWaterDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			PurifiedWater pt = purifiedWaterDao.get(PurifiedWater.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<PurifiedWaterInfo> listPTI = purifiedWaterDao
								.findPurifiedWaterInfoByCode(code);
						if (listPTI.size() > 0) {
							PurifiedWaterInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader
									.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							purifiedWaterDao.saveOrUpdate(spi);
						}
					
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : 
	 * @date 
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}	
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date 
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			PurifiedWaterTemplate ptt = purifiedWaterDao.get(
					PurifiedWaterTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			purifiedWaterDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : 
	 * @date 
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
	
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			PurifiedWaterReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(PurifiedWaterReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new PurifiedWaterReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setPurifiedWater(commonDAO.get(PurifiedWater.class, id));
				}

			}
			purifiedWaterDao.saveOrUpdate(ptr);
		}
	
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
	
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			PurifiedWaterCos ptc=null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(PurifiedWaterCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new PurifiedWaterCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setPurifiedWater(commonDAO.get(PurifiedWater.class, id));
				}
			}
			purifiedWaterDao.saveOrUpdate(ptc);
		}
	
	
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : 
	 * @date 
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return purifiedWaterDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
	
	
		List<PurifiedWaterItem> list = purifiedWaterDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<PurifiedWaterInfo> spiList = purifiedWaterDao.selectResultListById(id);
		PurifiedWater purifiedWater = commonDAO.get(PurifiedWater.class, id);
		for (PurifiedWaterItem pti : list) {
			PurifiedWaterInfo scp = new PurifiedWaterInfo();
			scp.setId(pti.getId());
			scp.setCharacters(pti.getCharacters());
			scp.setPhValue(pti.getPhValue());
			scp.setNitrate(pti.getNitrate());
			scp.setNitrite(pti.getNitrite());
			scp.setAmmonia(pti.getAmmonia());
			scp.setConductivity(pti.getConductivity());
			scp.setToc(pti.getToc());
			scp.setHeavyMetal(pti.getHeavyMetal());
			scp.setMicrobialLimit(pti.getMicrobialLimit());
			scp.setResult("1");
			scp.setPurifiedWater(purifiedWater);
			pti.setState("2");
			commonDAO.saveOrUpdate(pti);
			commonDAO.saveOrUpdate(scp);
		}
		/*boolean b = true;
			for (PurifiedWaterInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							PurifiedWaterInfo scp = new PurifiedWaterInfo();
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setSampleInfo(pti.getSampleInfo());
							scp.setPurifiedWater(pti.getPurifiedWater());
							scp.setResult("1");
							String markCode = "";
							if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = str;
									} else {
										markCode = str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = scp.getSampleCode();
									} else {
										markCode = scp.getSampleCode()
												+ d.getCode();
									}
								}

							}
							String code = codingRuleService.getCode(
									"PurifiedWaterInfo", markCode, 00, 2, null);
							scp.setCode(code);
							purifiedWaterDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								PurifiedWaterInfo scp = new PurifiedWaterInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setSampleInfo(pti.getSampleInfo());
								scp.setPurifiedWater(pti.getPurifiedWater());
								scp.setResult("1");
								String markCode = "";
								if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}
								String code = codingRuleService.getCode(
										"PurifiedWaterInfo", markCode, 00, 2,
										null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								purifiedWaterDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						PurifiedWaterInfo spi = purifiedWaterDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						purifiedWaterDao.saveOrUpdate(spi);
					}
				}
			}
		}*/
	
	
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser) throws Exception {

		List<PurifiedWaterInfo> saveItems = new ArrayList<PurifiedWaterInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		PurifiedWater pt = commonDAO.get(PurifiedWater.class, id);		
		for (Map<String, Object> map : list) {
		PurifiedWaterInfo scp = new PurifiedWaterInfo();
			// 将map信息读入实体类
			scp = (PurifiedWaterInfo) purifiedWaterDao.Map2Bean(map, scp);
			scp.setPurifiedWater(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			purifiedWaterDao.saveOrUpdate(pt);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		purifiedWaterDao.saveOrUpdateAll(saveItems);

	}

	public List<PurifiedWaterItem> findPurifiedWaterItemList(String scId)
			throws Exception {
		List<PurifiedWaterItem> list = purifiedWaterDao.selectPurifiedWaterItemList(scId);
		return list;
	}
	public Integer generateBlendCode(String id) {
		return purifiedWaterDao.generateBlendCode(id);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(PurifiedWater pt,String []ids,String logInfo) throws Exception {
		if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				PurifiedWaterItem pti = new PurifiedWaterItem();
				pti.setPurifiedWater(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				purifiedWaterDao.save(pti);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		
	}

@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void saveRightQuality(PurifiedWater pt,String []ids,String logInfo) throws Exception {
	if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				PurifiedWaterItem pti = new PurifiedWaterItem();
				pti.setPurifiedWater(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	
}
public PurifiedWaterInfo getInfoById(String idc) {
	return purifiedWaterDao.get(PurifiedWaterInfo.class,idc);
	
}
}

