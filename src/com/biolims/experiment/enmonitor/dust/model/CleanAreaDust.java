package com.biolims.experiment.enmonitor.dust.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;

/**
 * @Title: Model
 * @Description: 尘埃  主表
 * @author lims-platform
 * @date 2019-03-22 17:06:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_dust")
@SuppressWarnings("serial")
public class CleanAreaDust extends EntityDao<CleanAreaDust> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 创建人*/
	private User createUser;
	/** 创建时间*/
	//@Column(name="create_date")
	private String createDate;
	/** 车间部门 */
	//@Column(name="workshop_department")
	private String workshopDepartment;
	/** 区域*/
	//@Column(name="region")
	private String region;
	/** 区域*/
	//@Column(name="region")
	private String regionId;
	/** 测试时间 */
	//@Column(name="test_date")
	private String testDate;
	/** 测试人*/
	//@Column(name="test_user")
	private String testUser;
	/** 测量设备 */
	//@Column(name="measuring_instrument")
	private String measuringInstrument;
	/** 洁净区状态 */
	//@Column(name="clean_zone_status")
	private String cleanZoneStatus;
	/** 备注*/
	//@Column(name="note")
	private String note;
	/** 洁净区尘埃最大允许数等级*/
	//@Column(name="maximum_allowable_dust")
	private String maximumAllowableDust;
	/** 洁净区尘埃最大允许数/m3 大于等于0.5um*/
	//@Column(name="dust_zeropoint_five")
	private String dustZeropointFive;
	/**洁净区尘埃最大允许数/m3 大于等于5um */
	//@Column(name="max_fivedust")
	private String maxFivedust;	
	/**状态 */
	//@Column(name="state")
	private String state;
	/** 状态名称  */
	private String stateName;
	/**类型*/
	@Column(name="type")
	private String type;
	
	/** 审核人 */
	private User confirmUser;
	/** 复核时间*/
	private String confirmDate;
	/** 通知人id */
	private String notifierId;
	/** 通知人姓名 */
	private String notifierName;
	
	/** 合格标准线 */
	private String qualifiedStandardLine;
	/** 0.5um警戒线 */
	private String cordon;
	/** 0.5um纠偏线 */
	private String deviationCorrectionLine;
	
	/** 5um警戒线 */
	private String cordon2;
	/** 5um纠偏线 */
	private String deviationCorrectionLine2;
	
	//批准人
	private User approver;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPROVER")
	public User getApprover() {
		return approver;
	}

	public void setApprover(User approver) {
		this.approver = approver;
	}

	public String getCordon2() {
		return cordon2;
	}

	public void setCordon2(String cordon2) {
		this.cordon2 = cordon2;
	}

	public String getDeviationCorrectionLine2() {
		return deviationCorrectionLine2;
	}

	public void setDeviationCorrectionLine2(String deviationCorrectionLine2) {
		this.deviationCorrectionLine2 = deviationCorrectionLine2;
	}

	public String getQualifiedStandardLine() {
		return qualifiedStandardLine;
	}

	public void setQualifiedStandardLine(String qualifiedStandardLine) {
		this.qualifiedStandardLine = qualifiedStandardLine;
	}

	public String getCordon() {
		return cordon;
	}

	public void setCordon(String cordon) {
		this.cordon = cordon;
	}

	public String getDeviationCorrectionLine() {
		return deviationCorrectionLine;
	}

	public void setDeviationCorrectionLine(String deviationCorrectionLine) {
		this.deviationCorrectionLine = deviationCorrectionLine;
	}

	public String getNotifierId() {
		return notifierId;
	}

	public void setNotifierId(String notifierId) {
		this.notifierId = notifierId;
	}

	public String getNotifierName() {
		return notifierName;
	}

	public void setNotifierName(String notifierName) {
		this.notifierName = notifierName;
	}

	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public String getWorkshopDepartment() {
		return workshopDepartment;
	}

	public void setWorkshopDepartment(String workshopDepartment) {
		this.workshopDepartment = workshopDepartment;
	}

	public String getTestDate() {
		return testDate;
	}

	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}

	public String getTestUser() {
		return testUser;
	}

	public void setTestUser(String testUser) {
		this.testUser = testUser;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getMeasuringInstrument() {
		return measuringInstrument;
	}

	public void setMeasuringInstrument(String measuringInstrument) {
		this.measuringInstrument = measuringInstrument;
	}

	public String getCleanZoneStatus() {
		return cleanZoneStatus;
	}

	public void setCleanZoneStatus(String cleanZoneStatus) {
		this.cleanZoneStatus = cleanZoneStatus;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDustZeropointFive() {
		return dustZeropointFive;
	}

	public void setDustZeropointFive(String dustZeropointFive) {
		this.dustZeropointFive = dustZeropointFive;
	}

	public String getMaximumAllowableDust() {
		return maximumAllowableDust;
	}

	public void setMaximumAllowableDust(String maximumAllowableDust) {
		this.maximumAllowableDust = maximumAllowableDust;
	}

	public String getMaxFivedust() {
		return maxFivedust;
	}

	public void setMaxFivedust(String maxFivedust) {
		this.maxFivedust = maxFivedust;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

}