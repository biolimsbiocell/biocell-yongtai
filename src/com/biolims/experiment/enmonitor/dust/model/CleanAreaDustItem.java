package com.biolims.experiment.enmonitor.dust.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description:尘埃  子表
 * @author lims-platform
 * @date 2019-3-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_dust_item")
@SuppressWarnings("serial")
public class CleanAreaDustItem extends EntityDao<CleanAreaDustItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 房间名称 */
	@Column(name="room_name")
	private String roomName;
	/**房间编号*/
	@Column(name = "room_num")
	private String roomNum;
	/** 平均数大于等于0.5um */
	@Column(name="average_zeropoint_five")
	private String averageZeropointFive;
	/** 平均数大于等于5um*/
	@Column(name="average_max_five")
	private String averageMaxFive;
	/** 置信上限（UCL）0.5um */
	@Column(name="ucl_maxpoint_five")
	private String uclMaxpointFive;
	/** 置信上限（UCL）5um */
	@Column(name="ucl_max_five")
	private String uclMaxFive;
	/**洁净区等级*/
	@Column(name="clean_zone_grade")
	private String cleanZoneGrade;
	/**洁净区要求*/
	@Column(name="clean_zone_requirements")
	private String cleanZoneRequirements;
	/** 是否符合洁净区   0符合   1不符合*/
	@Column(name="note_clean_zone")
	private String noteCleanZone;
	/** 备注 */
	@Column(name="note")
	private String note;
	/**状态 */
	@Column(name="state")
	private String state;
	/**监测点 */
	@Column(name="MONITORING_POINT")
	private String monitoringPoint;
	/**备注 */
	@Column(name="REMARK")
	private String remark;
	
	/**房间面积*/
	private String centiare;
	/**房间体积*/
	private String stere;
	


	public String getCentiare() {
		return centiare;
	}

	public void setCentiare(String centiare) {
		this.centiare = centiare;
	}

	public String getStere() {
		return stere;
	}

	public void setStere(String stere) {
		this.stere = stere;
	}

	public String getRoomNum() {
		return roomNum;
	}

	public void setRoomNum(String roomNum) {
		this.roomNum = roomNum;
	}

	public String getMonitoringPoint() {
		return monitoringPoint;
	}

	public void setMonitoringPoint(String monitoringPoint) {
		this.monitoringPoint = monitoringPoint;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**关联主表*/
	private CleanAreaDust cleanAreaDust;
	
	
	/**
	 * 方法: 关联的主表 
	 * 
	 * @return: 关联的主表 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "clean_area_dust")
	public CleanAreaDust getCleanAreaDust() {
		return cleanAreaDust;
	}

	public void setCleanAreaDust(CleanAreaDust cleanAreaDust) {
		this.cleanAreaDust = cleanAreaDust;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	public String getAverageZeropointFive() {
		return averageZeropointFive;
	}

	public void setAverageZeropointFive(String averageZeropointFive) {
		this.averageZeropointFive = averageZeropointFive;
	}

	public String getAverageMaxFive() {
		return averageMaxFive;
	}

	public void setAverageMaxFive(String averageMaxFive) {
		this.averageMaxFive = averageMaxFive;
	}

	public String getUclMaxpointFive() {
		return uclMaxpointFive;
	}

	public void setUclMaxpointFive(String uclMaxpointFive) {
		this.uclMaxpointFive = uclMaxpointFive;
	}

	public String getUclMaxFive() {
		return uclMaxFive;
	}

	public void setUclMaxFive(String uclMaxFive) {
		this.uclMaxFive = uclMaxFive;
	}

	public String getCleanZoneGrade() {
		return cleanZoneGrade;
	}

	public void setCleanZoneGrade(String cleanZoneGrade) {
		this.cleanZoneGrade = cleanZoneGrade;
	}

	public String getCleanZoneRequirements() {
		return cleanZoneRequirements;
	}

	public void setCleanZoneRequirements(String cleanZoneRequirements) {
		this.cleanZoneRequirements = cleanZoneRequirements;
	}

	public String getNoteCleanZone() {
		return noteCleanZone;
	}

	public void setNoteCleanZone(String noteCleanZone) {
		this.noteCleanZone = noteCleanZone;
	}
	
}