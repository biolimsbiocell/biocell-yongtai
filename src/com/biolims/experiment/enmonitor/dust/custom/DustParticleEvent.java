package com.biolims.experiment.enmonitor.dust.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;

public class DustParticleEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		DustParticleService mbService = (DustParticleService) ctx
				.getBean("dustParticleService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
