﻿package com.biolims.experiment.enmonitor.dust.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.poi.ss.formula.ptg.ScalarConstantPtg;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.dao.DustParticleDao;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.enmonitor.dust.model.DustParticleAbnormal;
import com.biolims.experiment.enmonitor.dust.model.DustParticleCos;
import com.biolims.experiment.enmonitor.dust.model.DustParticleInfo;
import com.biolims.experiment.enmonitor.dust.model.DustParticleItem;
import com.biolims.experiment.enmonitor.dust.model.DustParticleReagent;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemp;
import com.biolims.experiment.enmonitor.dust.model.DustParticleTemplate;
import com.biolims.experiment.enmonitor.template.dao.RoomTemplateDao;
import com.biolims.experiment.enmonitor.template.model.RoomTemplateItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional
public class DustParticleService {

	@Resource
	private DustParticleDao dustParticleDao;
	@Resource
	private RoomTemplateDao roomTemplateDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	@Resource
	private CommonService commonService;

	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         DustParticle @throws
	 */
	public DustParticle get(String id) {
		DustParticle dustParticle = commonDAO.get(DustParticle.class, id);
		return dustParticle;
	}

	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         DustParticle @throws
	 */
	public CleanAreaDust gets(String id) {
		CleanAreaDust cleanAreaDust = commonDAO.get(CleanAreaDust.class, id);
		return cleanAreaDust;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDustParticleItem(String delStr, String[] ids, User user, String dustParticle_id) throws Exception {
		String delId = "";
		for (String id : ids) {
			DustParticleItem scp = dustParticleDao.get(DustParticleItem.class, id);
			if (scp.getId() != null) {
				DustParticle pt = dustParticleDao.get(DustParticle.class, scp.getDustParticle().getId());
				// 改变左侧样本状态
				DustParticleTemp dustParticleTemp = this.commonDAO.get(DustParticleTemp.class, scp.getTempId());
				if (dustParticleTemp != null) {
					pt.setSampleNum(pt.getSampleNum() - 1);
					dustParticleTemp.setState("1");
					dustParticleDao.update(dustParticleTemp);
				}
				dustParticleDao.update(pt);
				dustParticleDao.delete(scp);
			}
			delId += scp.getSampleCode() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setClassName("CleanAreaDust");
			li.setFileId(dustParticle_id);
			li.setModifyContent(delStr + delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delDustParticleItemAf @Description: 重新排板 @author : @date @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDustParticleItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			DustParticleItem scp = dustParticleDao.get(DustParticleItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				dustParticleDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDustParticleResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr = "";
		for (String id : ids) {
			DustParticleInfo scp = dustParticleDao.get(DustParticleInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				dustParticleDao.delete(scp);
			idStr += scp.getSampleCode() + ",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaDust");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDustParticleReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			DustParticleReagent scp = dustParticleDao.get(DustParticleReagent.class, id);
			dustParticleDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setClassName("CleanAreaDust");
			li.setModifyContent(delStr + scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDustParticleCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			DustParticleCos scp = dustParticleDao.get(DustParticleCos.class, id);
			dustParticleDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setClassName("CleanAreaDust");
			li.setModifyContent(delStr + scp.getName());
		}
	}

	/**
	 * 
	 * @Title: saveDustParticleTemplate @Description: 保存模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveDustParticleTemplate(DustParticle sc) {
		List<DustParticleTemplate> tlist2 = dustParticleDao.delTemplateItem(sc.getId());
		List<DustParticleReagent> rlist2 = dustParticleDao.delReagentItem(sc.getId());
		List<DustParticleCos> clist2 = dustParticleDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate().getId(), null);
		for (TemplateItem t : tlist) {
			DustParticleTemplate ptt = new DustParticleTemplate();
			ptt.setDustParticle(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			dustParticleDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			DustParticleReagent ptr = new DustParticleReagent();
			ptr.setDustParticle(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			dustParticleDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			DustParticleCos ptc = new DustParticleCos();
			ptc.setDustParticle(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			dustParticleDao.saveOrUpdate(ptc);
		}
	}

//	/**
//	 * 
//	 * @Title: changeState @Description:改变状态 @author : @date @param
//	 * applicationTypeActionId @param id @throws Exception void @throws
//	 */
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void changeState(String applicationTypeActionId, String id) throws Exception {
//		DustParticle sct = dustParticleDao.get(DustParticle.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//
//		dustParticleDao.update(sct);
//		if (sct.getTemplate() != null) {
//			String iid = sct.getTemplate().getId();
//			if (iid != null) {
//				templateService.setCosIsUsedOver(iid);
//			}
//		}
//
//		// submitSample(id, null);
//
//	}

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {

		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CleanAreaDust sct = dustParticleDao.get(CleanAreaDust.class, id);
		if(applicationTypeActionId.equals("1996")) {
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName("作废");	
		}else {
			
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		}
		
		

		sct.setConfirmUser(u);
		sct.setConfirmDate(format.format(new Date()));
		
		

		if (sct.getNotifierId() != null && !"".equals(sct.getNotifierId())) {
			String userids[] = sct.getNotifierId().split(",");
			for (String userid : userids) {
				if (userid != null && !"".equals(userid)) {
					User uu = commonDAO.get(User.class, userid);
					SysRemind srz = new SysRemind();
					srz.setId(null);
					// 提醒类型
					srz.setType(null);
					// 事件标题
					srz.setTitle("洁净区尘埃粒子测试记录提醒");
					// 发起人（系统为SYSTEM）
					srz.setRemindUser(u.getName());
					// 发起时间
					srz.setStartDate(new Date());
					// 查阅提醒时间
					srz.setReadDate(new Date());
					// 提醒内容
					srz.setContent("区域：" + sct.getRegion() + "车间/部门：" + sct.getWorkshopDepartment() + "洁净区尘埃粒子测试记录提醒");
					// 状态 0.草稿 1.已阅
					srz.setState("0");
					// 提醒的用户
					srz.setHandleUser(uu);
					// 表单id
					srz.setContentId(sct.getId());
					srz.setTableId("CleanAreaDust");
					commonDAO.saveOrUpdate(srz);
				}
			}
		}

		List<CleanAreaDustItem> cads = commonService.get(CleanAreaDustItem.class, "cleanAreaDust.id", sct.getId());
		for (CleanAreaDustItem cad : cads) {
			if (cad.getRoomName() != null && !"".equals(cad.getRoomName())) {
				String zong = sct.getDeviationCorrectionLine();
				String um5 = cad.getAverageZeropointFive();
				if (zong != null && !"".equals(zong) && um5 != null && !"".equals(um5)) {
					if (Double.valueOf(um5) > Double.valueOf(zong)) {
						List<RoomManagement> rms = commonService.get(RoomManagement.class, "roomName",
								cad.getRoomName());
						if (rms.size() > 0) {
							RoomManagement rm = rms.get(0);
							rm.setRoomState("0");
							commonDAO.saveOrUpdate(rm);
						}
					}
				}
			}
		}

		dustParticleDao.update(sct);
	}

	/**
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : @date @param id @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		DustParticle sc = this.dustParticleDao.get(DustParticle.class, id);
		// 获取结果表样本信息
		List<DustParticleInfo> list;
		if (ids == null)
			list = this.dustParticleDao.selectAllResultListById(id);
		else
			list = this.dustParticleDao.selectAllResultListByIds(ids);

		for (DustParticleInfo scp : list) {
			if (scp != null && scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("DustParticleInfo");
						dustParticleDao.saveOrUpdate(st);

					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0017")) {// 核酸提取
						// 核酸提取
						DnaTaskTemp d = new DnaTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						dustParticleDao.saveOrUpdate(d);
					} else if (next.equals("0018")) {// 文库构建
						// 文库构建
						WkTaskTemp d = new WkTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						dustParticleDao.saveOrUpdate(d);

					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					DustParticleAbnormal pa = new DustParticleAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService.saveSampleState(scp.getCode(), scp.getSampleCode(), scp.getProductId(),
						scp.getProductName(), "", sc.getCreateDate(), format.format(new Date()), "DustParticle",
						"尘埃粒子检测",
						(User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY),
						id, scp.getNextFlow(), scp.getResult(), null, null, null, null, null, null, null, null);

				scp.setSubmit("1");
				dustParticleDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findDustParticleTable @Description: 展示主表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findDustParticleTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return dustParticleDao.findDustParticleTable(start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: selectDustParticleTempTable @Description: 展示临时表 @author
	 *         : @date @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectDustParticleTempTable(String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return dustParticleDao.selectDustParticleTempTable(codes, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String itemJson, String[] tempId, String userId, String templateId,
			String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			DustParticle pt = new DustParticle();
			pt = (DustParticle) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				String modelName = "DustParticle";
				String markCode = "DP";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			} else {
				id = pt.getId();
				pt = commonDAO.get(DustParticle.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String[] users = userId.split(",");
			String userName = "";
			for (int i = 0; i < users.length; i++) {
				if (i == users.length - 1) {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName();
				} else {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName() + ",";
				}
			}
			pt.setTestUserOneName(userName);
			dustParticleDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveDustParticleTemplate(pt);
			}
			if (itemJson != null) {
				List<Map<String, Object>> listJsonData = JsonUtils.toListByJsonArray(itemJson, List.class);
				DustParticleItem item = new DustParticleItem();
				item = (DustParticleItem) commonDAO.Map2Bean(listJsonData.get(0), item);
				if ((item.getId() != null && "".equals(item.getId())) || item.getId().equals("NEW")) {
					item.setId(null);
					item.setState("1");
					item.setDustParticle(pt);
					commonDAO.saveOrUpdate(item);
				} else {
					commonDAO.saveOrUpdate(item);
				}
			}
			/*
			 * if (tempId != null) { if (pt.getTemplate() != null) { for (String temp :
			 * tempId) { DustParticleTemp ptt = dustParticleDao.get( DustParticleTemp.class,
			 * temp); if (t.getIsSeparate().equals("1")) { List<MappingPrimersLibraryItem>
			 * listMPI = mappingPrimersLibraryService .getMappingPrimersLibraryItem(ptt
			 * .getProductId()); for (MappingPrimersLibraryItem mpi : listMPI) {
			 * DustParticleItem pti = new DustParticleItem();
			 * pti.setSampleCode(ptt.getSampleCode()); pti.setProductId(ptt.getProductId());
			 * pti.setProductName(ptt.getProductName());
			 * pti.setDicSampleTypeId(t.getDicSampleType() != null &&
			 * !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getId() : t
			 * .getDicSampleTypeId()); pti.setDicSampleTypeName(t .getDicSampleType() !=
			 * null && !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getName() :
			 * t .getDicSampleTypeName()); pti.setProductName(ptt.getProductName());
			 * pti.setProductNum(t.getProductNum() != null && !"".equals(t.getProductNum())
			 * ? t .getProductNum() : t .getProductNum1()); pti.setCode(ptt.getCode()); if
			 * (t.getStorageContainer() != null) { pti.setState("1"); }else{
			 * pti.setState("2"); } ptt.setState("2"); pti.setTempId(temp);
			 * pti.setDustParticle(pt); pti.setChromosomalLocation(mpi
			 * .getChromosomalLocation()); pti.setSampleInfo(ptt.getSampleInfo());
			 * dustParticleDao.saveOrUpdate(pti); } } else { DustParticleItem pti = new
			 * DustParticleItem(); pti.setSampleCode(ptt.getSampleCode());
			 * pti.setProductId(ptt.getProductId());
			 * pti.setProductName(ptt.getProductName());
			 * pti.setDicSampleTypeId(t.getDicSampleType() != null &&
			 * !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getId() : t
			 * .getDicSampleTypeId()); pti.setProductNum(t.getProductNum() != null &&
			 * !"".equals(t.getProductNum()) ? t .getProductNum() : t.getProductNum1());
			 * pti.setDicSampleTypeName(t.getDicSampleType() != null &&
			 * !"".equals(t.getDicSampleType()) ? t .getDicSampleType().getName() : t
			 * .getDicSampleTypeName()); pti.setCode(ptt.getCode());
			 * pti.setSampleInfo(ptt.getSampleInfo()); if (t.getStorageContainer() != null)
			 * { pti.setState("1"); }else{ pti.setState("2"); } ptt.setState("2");
			 * pti.setTempId(temp); pti.setDustParticle(pt);
			 * dustParticleDao.saveOrUpdate(pti); } dustParticleDao.saveOrUpdate(ptt); } }
			 * 
			 * 
			 * }
			 */

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setClassName("CleanAreaDust");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findDustParticleItemTable @Description:展示未排版样本 @author : @date @param
	 *         id @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findDustParticleItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return dustParticleDao.findDustParticleItemTable(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排板数据 @author : @date @param id @param
	 *         item @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<DustParticleItem> saveItems = new ArrayList<DustParticleItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		DustParticle pt = commonDAO.get(DustParticle.class, id);
		DustParticleItem scp = new DustParticleItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (DustParticleItem) dustParticleDao.Map2Bean(map, scp);
			DustParticleItem pti = commonDAO.get(DustParticleItem.class, scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());
			scp.setDustParticle(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("CleanAreaDust");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		dustParticleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate @Description: 展示孔板 @author : @date @param
	 *         id @return @throws Exception List<DustParticleItem> @throws
	 */
	public List<DustParticleItem> showWellPlate(String id) throws Exception {
		List<DustParticleItem> list = dustParticleDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findDustParticleItemAfTable @Description: 展示排板后 @author
	 *         : @date @param scId @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findDustParticleItemAfTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return dustParticleDao.findDustParticleItemAfTable(scId, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			DustParticleItem pti = commonDAO.get(DustParticleItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			dustParticleDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showDustParticleStepsJson @Description: 展示实验步骤 @author : @date @param
	 *         id @param code @return Map<String,Object> @throws
	 */
	public Map<String, Object> showDustParticleStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<DustParticleTemplate> pttList = dustParticleDao.showDustParticleStepsJson(id, code);
		List<DustParticleReagent> ptrList = dustParticleDao.showDustParticleReagentJson(id, code);
		List<DustParticleCos> ptcList = dustParticleDao.showDustParticleCosJson(id, code);
		List<Object> plate = dustParticleDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showDustParticleResultTableJson @Description: 展示结果 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> showDustParticleResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return dustParticleDao.showDustParticleResultTableJson(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @param id @param
	 *         counts @return Map<String,Object> @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		DustParticle pt = commonDAO.get(DustParticle.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DustParticleItem> list = dustParticleDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent @Description: 上传结果 @author : @date @param id @param
	 *         fileId @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = dustParticleDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			DustParticle pt = dustParticleDao.get(DustParticle.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<DustParticleInfo> listPTI = dustParticleDao.findDustParticleInfoByCode(code);
						if (listPTI.size() > 0) {
							DustParticleInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							dustParticleDao.saveOrUpdate(spi);
						}

					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @param id @param
	 *         templateJson @param reagentJson @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson, String cosJson, String logInfo) {
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("CleanAreaDust");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}") && !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}") && !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate @Description: 保存模板明细 @author : @date @param id @param
	 *         templateJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			DustParticleTemplate ptt = dustParticleDao.get(DustParticleTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			dustParticleDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent @Description: 保存试剂 @author : @date @param id @param
	 *         reagentJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {

		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			DustParticleReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(DustParticleReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new DustParticleReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setDustParticle(commonDAO.get(DustParticle.class, id));
				}

			}
			dustParticleDao.saveOrUpdate(ptr);
		}

	}

	/**
	 * 
	 * @Title: saveCos @Description: 保存设备 @author : @date @param id @param cosJson
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {

		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			DustParticleCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(DustParticleCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new DustParticleCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setDustParticle(commonDAO.get(DustParticle.class, id));
				}
			}
			dustParticleDao.saveOrUpdate(ptc);
		}

	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 孔板样本列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return dustParticleDao.plateSampleTable(id, counts, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @param id @throws
	 *         Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {

		List<DustParticleItem> list = dustParticleDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DustParticleInfo> spiList = dustParticleDao.selectResultListById(id);
		DustParticle dustParticle = commonDAO.get(DustParticle.class, id);
		for (DustParticleItem pti : list) {
			DustParticleInfo scp = new DustParticleInfo();
			scp.setId(pti.getId());
			scp.setRoomNumber(pti.getRoomNumber());
			scp.setSampleVolume(pti.getSampleVolume());
			scp.setSizeCalibration(pti.getSizeCalibration());
			scp.setPseudoCount(pti.getPseudoCount());
			scp.setMaximumSaturation(pti.getMaximumSaturation());
			scp.setCountingResponse(pti.getCountingResponse());
			scp.setResolutionRatio(pti.getResolutionRatio());
			scp.setCountingYield(pti.getCountingYield());
			scp.setResult("1");
			scp.setDustParticle(dustParticle);
			pti.setState("2");
			commonDAO.saveOrUpdate(pti);
			commonDAO.saveOrUpdate(scp);
			/*
			 * boolean b = true; for (DustParticleInfo spi : spiList) { if
			 * (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) { b = false; } }
			 */
			/*
			 * if (b) { if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
			 * if (pti.getProductNum() != null && !"".equals(pti.getProductNum())) { Integer
			 * pn = Integer.parseInt(pti.getProductNum()); for (int i = 0; i < pn; i++) {
			 * DustParticleInfo scp = new DustParticleInfo(); DicSampleType ds =
			 * commonDAO.get( DicSampleType.class, pti.getDicSampleTypeId());
			 * scp.setDicSampleType(ds); scp.setSampleCode(pti.getSampleCode());
			 * scp.setProductId(pti.getProductId());
			 * scp.setProductName(pti.getProductName());
			 * scp.setSampleInfo(pti.getSampleInfo());
			 * scp.setDustParticle(pti.getDustParticle()); scp.setResult("1"); String
			 * markCode = ""; if (scp.getDicSampleType() != null) { DicSampleType d =
			 * commonDAO.get( DicSampleType.class, scp .getDicSampleType().getId());
			 * String[] sampleCode = scp.getSampleCode() .split(","); if (sampleCode.length
			 * > 1) { String str = sampleCode[0].substring(0, sampleCode[0].length() - 4);
			 * for (int j = 0; j < sampleCode.length; j++) { str += sampleCode[j].substring(
			 * sampleCode[j].length() - 4, sampleCode[j].length()); } if (d == null) {
			 * markCode = str; } else { markCode = str + d.getCode(); } } else { if (d ==
			 * null) { markCode = scp.getSampleCode(); } else { markCode =
			 * scp.getSampleCode() + d.getCode(); } }
			 * 
			 * } String code = codingRuleService.getCode( "DustParticleInfo", markCode, 00,
			 * 2, null); scp.setCode(code); dustParticleDao.saveOrUpdate(scp); } } } else {
			 * if (!map.containsKey(String.valueOf(pti.getBlendCode()))) { if
			 * (pti.getProductNum() != null && !"".equals(pti.getProductNum())) { Integer pn
			 * = Integer.parseInt(pti.getProductNum()); for (int i = 0; i < pn; i++) {
			 * DustParticleInfo scp = new DustParticleInfo(); DicSampleType ds =
			 * commonDAO.get( DicSampleType.class, pti.getDicSampleTypeId());
			 * scp.setDicSampleType(ds); scp.setSampleCode(pti.getSampleCode());
			 * scp.setProductId(pti.getProductId());
			 * scp.setProductName(pti.getProductName());
			 * scp.setSampleInfo(pti.getSampleInfo());
			 * scp.setDustParticle(pti.getDustParticle()); scp.setResult("1"); String
			 * markCode = ""; if (scp.getDicSampleType() != null) { DicSampleType d =
			 * commonDAO .get(DicSampleType.class, scp .getDicSampleType().getId());
			 * String[] sampleCode = scp.getSampleCode() .split(","); if (sampleCode.length
			 * > 1) { String str = sampleCode[0].substring(0, sampleCode[0].length() - 4);
			 * for (int j = 0; j < sampleCode.length; j++) { str += sampleCode[j].substring(
			 * sampleCode[j].length() - 4, sampleCode[j].length()); } if (d == null) {
			 * markCode = str; } else { markCode = str + d.getCode(); } } else { if (d ==
			 * null) { markCode = scp.getSampleCode(); } else { markCode =
			 * scp.getSampleCode() + d.getCode(); } }
			 * 
			 * } String code = codingRuleService.getCode( "DustParticleInfo", markCode, 00,
			 * 2, null); scp.setCode(code); map.put(String.valueOf(pti.getBlendCode()),
			 * code); dustParticleDao.saveOrUpdate(scp); } } } else { String code = (String)
			 * map.get(String.valueOf(pti .getBlendCode())); DustParticleInfo spi =
			 * dustParticleDao .getResultByCode(code); spi.setSampleCode(spi.getSampleCode()
			 * + "," + pti.getSampleCode()); dustParticleDao.saveOrUpdate(spi); } } }
			 */
		}

	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果 @author : @date @param id @param
	 *         dataJson @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo, String confirmUser) throws Exception {

		List<DustParticleInfo> saveItems = new ArrayList<DustParticleInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		DustParticle pt = commonDAO.get(DustParticle.class, id);
		for (Map<String, Object> map : list) {
			DustParticleInfo scp = new DustParticleInfo();
			// 将map信息读入实体类
			scp = (DustParticleInfo) dustParticleDao.Map2Bean(map, scp);
			scp.setDustParticle(pt);
			saveItems.add(scp);
		}
		if (confirmUser != null && !"".equals(confirmUser)) {
			User confirm = commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			dustParticleDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("CleanAreaDust");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		dustParticleDao.saveOrUpdateAll(saveItems);

	}

	public List<DustParticleItem> findDustParticleItemList(String scId) throws Exception {
		List<DustParticleItem> list = dustParticleDao.selectDustParticleItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return dustParticleDao.generateBlendCode(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(DustParticle pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				DustParticleItem pti = new DustParticleItem();
				pti.setDustParticle(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				dustParticleDao.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setClassName("CleanAreaDust");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRightQuality(DustParticle pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				DustParticleItem pti = new DustParticleItem();
				pti.setDustParticle(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setClassName("CleanAreaDust");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public DustParticleInfo getInfoById(String idc) {
		return dustParticleDao.get(DustParticleInfo.class, idc);

	}

	/**
	 * 
	 * @Title: plateSampleTable @Description:沉降菌主表列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCleanAreaMicrodeTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return dustParticleDao.findCleanAreaMicrodeTable(id, start, length, query, col, sort);
	}

	// 保存尘埃子表信息
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBacteriaInfoItem(CleanAreaDust sc, String itemDataJson, String logInfo,String changeLogItem,String log) throws Exception {
		List<CleanAreaDustItem> saveItems = new ArrayList<CleanAreaDustItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CleanAreaDustItem scp = new CleanAreaDustItem();
			RoomManagement rm = new RoomManagement();
			Instrument im = new Instrument();
			// 将map信息读入实体类
			scp = (CleanAreaDustItem) dustParticleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			if("0".equals(sc.getType())) {
				rm=commonDAO.get(RoomManagement.class, scp.getRoomNum());
				/*
				//0.5um警戒线/纠偏线
				if (scp.getAverageZeropointFive() != "" 
						&& sc.getCordon() != "" 
						&& sc.getDeviationCorrectionLine() != ""
						&& sc.getDustZeropointFive()!="") {
					// 0.5最大允许数
					Double num1 = Double.valueOf(scp.getAverageZeropointFive());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine());
					// 0.5um最大允许数
					Double num4 = Double.valueOf(sc.getDustZeropointFive());

					if (num1 <= num2) {
						// 合格
						scp.setNoteCleanZone("是");
						rm.setRoomState("1");

					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setNoteCleanZone("是");
						rm.setRoomState("1");

					} else if (num1 >= num3 && num1 <= num4) {
						// 合格
						scp.setNoteCleanZone("是");
						rm.setRoomState("1");

					} else {
						// 不合格
						scp.setNoteCleanZone("否");
						rm.setRoomState("0");

					}
				} else {

				}
				//5um警戒线/纠偏线
				if (scp.getAverageMaxFive() != "" 
						&& sc.getCordon2() != "" 
						&& sc.getDeviationCorrectionLine2() != ""
						&& sc.getMaxFivedust()!="") {
					// 5最大允许数
					Double num1 = Double.valueOf(scp.getAverageMaxFive());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon2());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine2());
					// 5.0um最大允许数
					Double num4 = Double.valueOf(sc.getMaxFivedust());

					if (num1 <= num2) {
						// 合格
						scp.setNoteCleanZone("是");
						rm.setRoomState("1");

					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setNoteCleanZone("是");
						rm.setRoomState("1");

					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setNoteCleanZone("是");
						rm.setRoomState("1");

					} else {
						// 不合格
						scp.setNoteCleanZone("否");
						rm.setRoomState("0");

					}
				} else {

				}*/
				if(!"".equals(scp.getNoteCleanZone())&&scp.getNoteCleanZone().equals("是")) {
					rm.setRoomState("1");
				}else if(scp.getNoteCleanZone().equals("否")){
					rm.setRoomState("0");
				}else {
					
				}
				commonDAO.saveOrUpdate(rm);
			}else if("1".equals(sc.getType())){
				im=commonDAO.get(Instrument.class, scp.getRoomNum());
				/*
				//0.5um警戒线/纠偏线
				if (scp.getAverageZeropointFive() != "" 
						&& sc.getCordon() != "" 
						&& sc.getDeviationCorrectionLine() != ""
						&& sc.getDustZeropointFive()!="") {
					// 0.5最大允许数
					Double num1 = Double.valueOf(scp.getAverageZeropointFive());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine());
					// 0.5um最大允许数
					Double num4 = Double.valueOf(sc.getDustZeropointFive());

					if (num1 <= num2) {
						// 合格
						scp.setNoteCleanZone("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}

					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setNoteCleanZone("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setNoteCleanZone("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}
					} else {
						// 不合格
						scp.setNoteCleanZone("否");
						DicState ds = commonDAO.get(DicState.class, "0r");
						if(ds!=null) {
							im.setState(ds);
						}
					}
				} else {

				}
				//5um警戒线/纠偏线
				if (scp.getAverageMaxFive() != "" 
						&& sc.getCordon2() != "" 
						&& sc.getDeviationCorrectionLine2() != ""
						&& sc.getMaxFivedust()!="") {
					// 5最大允许数
					Double num1 = Double.valueOf(scp.getAverageMaxFive());
					// 警戒线
					Double num2 = Double.valueOf(sc.getCordon2());
					// 纠偏线
					Double num3 = Double.valueOf(sc.getDeviationCorrectionLine2());
					// 5.0um最大允许数
					Double num4 = Double.valueOf(sc.getMaxFivedust());

					if (num1 <= num2) {
						// 合格
						scp.setNoteCleanZone("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}

					} else if (num1 >= num2 && num1 <= num3) {
						// 合格
						scp.setNoteCleanZone("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}

					} else if (num1 >= num2 && num1 <= num4) {
						// 合格
						scp.setNoteCleanZone("是");
						DicState ds = commonDAO.get(DicState.class, "1r");
						if(ds!=null) {
							im.setState(ds);
						}

					} else {
						// 不合格
						scp.setNoteCleanZone("否");
						DicState ds = commonDAO.get(DicState.class, "0r");
						if(ds!=null) {
							im.setState(ds);
						}

					}
				} else {

				}*/
				if(!"".equals(scp.getNoteCleanZone())&&scp.getNoteCleanZone().equals("是")) {
					rm.setRoomState("1");
				}else if(scp.getNoteCleanZone().equals("否")){
					rm.setRoomState("0");
				}else {
					
				}
				commonDAO.saveOrUpdate(im);
			}
		

			scp.setCleanAreaDust(sc);
			saveItems.add(scp);
			
		}
		dustParticleDao.saveOrUpdateAll(saveItems);

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaDust");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			dustParticleDao.saveOrUpdate(li);
		}
		
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("CleanAreaDust");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			dustParticleDao.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 浮游菌子表列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCleanAreaMicrodeItemTable(String id, Integer start, Integer length, Map<String, String> map2Query,
			String col, String sort) throws Exception {
		return dustParticleDao.findCleanAreaMicrodeItemTable(id, start, length, map2Query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void uploadCsvFile(String id, String fileId) throws Exception {
		List<CleanAreaDustItem> saves = new ArrayList<CleanAreaDustItem>();
		CleanAreaDust cad = dustParticleDao.get(CleanAreaDust.class, id);
		FileInfo fileInfo = dustParticleDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		
		File file = new File(filepath);
		InputStream is = new FileInputStream(filepath);
		
		List<CleanAreaDustItem> itemList = dustParticleDao.findCleanAreaMicrodeItemTable(id);
		if (file.isFile()) {
			//CsvReader reader = new CsvReader(is, Charset.defaultCharset());
			CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
			reader.readHeaders();// 跳过表头
			if(itemList.size()>0) {
				while (reader.readRecord()) {
					for(CleanAreaDustItem cadi:itemList){
						if(cadi.getRoomName().equals(reader.get(0))&&cadi.getRoomNum().equals(reader.get(1))) {
							if(cadi.getMonitoringPoint().equals(reader.get(2))) {
								cadi.setCleanAreaDust(cad);
								cadi.setRoomName(reader.get(0));
								cadi.setRoomNum(reader.get(1));
								cadi.setMonitoringPoint(reader.get(2));
								cadi.setCleanZoneGrade(reader.get(3));//洁净区级别
								cadi.setAverageZeropointFive(reader.get(4));
								cadi.setAverageMaxFive(reader.get(5));
								cadi.setUclMaxpointFive(reader.get(6));
								cadi.setUclMaxFive(reader.get(7));
								cadi.setNoteCleanZone(reader.get(8));
								cadi.setRemark(reader.get(9));
								saves.add(cadi);
							}
						}
					}
				}
			}
			dustParticleDao.saveOrUpdateAll(saves);
		}
	}
	public List<CleanAreaDustItem> findCleanAreaDustItemList(String id) throws Exception {
		return dustParticleDao.findCleanAreaMicrodeItemTable(id);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delRoom(String[] ids, String delStr, String mainId, User user) {
		String idStr = "";
		for (String id : ids) {
			CleanAreaDustItem scp = dustParticleDao.get(CleanAreaDustItem.class, id);
			dustParticleDao.delete(scp);
			idStr += scp.getRoomName() + "的记录已删除";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setClassName("CleanAreaDust");
		li.setState("2");
		li.setStateName("数据删除");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	public Map<String, Object> ShowUsersTableList(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return dustParticleDao.ShowUsersTableList(start, length, query, col, sort);
	}
	public void useTemplateAddItem(String id,String cleanAreaDust_id) throws Exception {
		CleanAreaDust cleanAreaDust= dustParticleDao.get(CleanAreaDust.class, cleanAreaDust_id);
		List<RoomTemplateItem> useTemplateAddItem = roomTemplateDao.findRoomTemplateItemTable(id);
		for (RoomTemplateItem roomTemplateItem : useTemplateAddItem) {
			
			if(!"".equals(roomTemplateItem.getMonitoringPoint())&& null!=roomTemplateItem.getMonitoringPoint()) {
				int num = Integer.valueOf(roomTemplateItem.getMonitoringPoint());
				for(int i=1;i<=num;i++) {
					CleanAreaDustItem cleanAreaDustItem = new CleanAreaDustItem();
					cleanAreaDustItem.setRoomNum(roomTemplateItem.getCode());
					cleanAreaDustItem.setRoomName(roomTemplateItem.getName());
					cleanAreaDustItem.setMonitoringPoint(String.valueOf(i));
					cleanAreaDustItem.setCleanAreaDust(cleanAreaDust);
					dustParticleDao.saveOrUpdate(cleanAreaDustItem);
				}
			}else {
				CleanAreaDustItem cleanAreaDustItem = new CleanAreaDustItem();
				cleanAreaDustItem.setRoomNum(roomTemplateItem.getCode());
				cleanAreaDustItem.setRoomName(roomTemplateItem.getName());
				cleanAreaDustItem.setMonitoringPoint("1");
				cleanAreaDustItem.setCleanAreaDust(cleanAreaDust);
				dustParticleDao.saveOrUpdate(cleanAreaDustItem);
			}
			
		}
	}
}
