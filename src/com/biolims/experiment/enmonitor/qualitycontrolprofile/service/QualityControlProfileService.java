package com.biolims.experiment.enmonitor.qualitycontrolprofile.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.project.model.Project;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.qualitycontrolprofile.dao.QualityControlProfileDao;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.quality.dao.QualityProductDao;
//import com.biolims.system.quality.dao.QualityProductDataDao;
//import com.biolims.system.quality.model.QualityProduct;
//import com.biolims.system.quality.model.QualityProductData;
//import com.biolims.system.quality.model.QualityProductDetail;
//import com.biolims.system.quality.model.TargetValue;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QualityControlProfileService {
	@Resource
	private QualityControlProfileDao qualityControlProfileDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	@Resource
	private CodingRuleService codingRuleService;

	public Map<String, Object> findQualityProductList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return qualityControlProfileDao.selectQualityProductList(start, length, query, col, sort);
	}

	public Map<String, Object> findQualityProductList(Integer start, Integer length, String query, String col,
			String sort, String startDate, String endDate, String qstmk, String typeSelect, String bianma,
			String cleanStateSelect,String monitoringPoint,String uclMaxFive,String uclMaxpointFive,String inspectionPoint) throws Exception {
		Map<String, Object> result = qualityControlProfileDao.selectQualityProductList(start, length, query, col, sort,
				startDate, endDate, qstmk, typeSelect, bianma, cleanStateSelect,monitoringPoint,uclMaxpointFive,uclMaxFive,inspectionPoint);
		List<Map<String, Object>> zz = new ArrayList<Map<String, Object>>();
		// 趋势图模块
		if (qstmk != null && !"".equals(qstmk)) {
			// 尘埃粒子
			if ("1".equals(qstmk)) {
				List<CleanAreaDustItem> list = (List<CleanAreaDustItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaDustItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaDust().getTestDate());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaDust().getCordon());
						map.put("deviation", cami.getCleanAreaDust().getDeviationCorrectionLine());
						if("<1".equals(cami.getUclMaxpointFive())) {
							map.put("realNum", "0");
						}else {
							map.put("realNum", cami.getUclMaxpointFive());
						}
						
						zz.add(map);
					}
				}
			} else if ("2".equals(qstmk)) {
				List<CleanAreaDustItem> list = (List<CleanAreaDustItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaDustItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaDust().getTestDate());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaDust().getCordon2());
						map.put("deviation", cami.getCleanAreaDust().getDeviationCorrectionLine2());
						if("<1".equals(cami.getUclMaxFive())) {
							map.put("realNum", "0");
						}else {
							map.put("realNum", cami.getUclMaxFive());
						}
//						map.put("realNum", cami.getUclMaxFive());
						zz.add(map);
					}
				}
				// 浮游菌
			} else if ("3".equals(qstmk)) {
				List<CleanAreaMicrobeItem> list = (List<CleanAreaMicrobeItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaMicrobeItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaMicrobe().getTestDate());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaMicrobe().getCordon());
						map.put("deviation", cami.getCleanAreaMicrobe().getDeviationCorrectionLine());
						if("<1".equals(cami.getMonitoringResults())) {
							map.put("realNum", "0");
						}else {
							map.put("realNum", cami.getMonitoringResults());
						}
//						map.put("realNum", cami.getMonitoringResults());
						zz.add(map);
					}
				}
				// 沉降菌
			} else if ("4".equals(qstmk)) {
				List<CleanAreaBacteriaItem> list = (List<CleanAreaBacteriaItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaBacteriaItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaBacteria().getTestDate());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaBacteria().getCordon());
						map.put("deviation", cami.getCleanAreaBacteria().getDeviationCorrectionLine());
						if("<1".equals(cami.getMonitoringResults())) {
							map.put("realNum", "0");
						}else {
							map.put("realNum", cami.getMonitoringResults());
						}
//						map.put("realNum", cami.getMonitoringResults());
						zz.add(map);
					}
				}
				// 表面微生物
			} else if ("5".equals(qstmk)) {
				List<CleanAreaMicroorganismItem> list = (List<CleanAreaMicroorganismItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaMicroorganismItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaMicroorganism().getTestTime());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaMicroorganism().getCordon());
						map.put("deviation", cami.getCleanAreaMicroorganism().getDeviationCorrectionLine());
						if("<1".equals(cami.getClumpCount())) {
							map.put("realNum", "0");
						}else {
							map.put("realNum", cami.getClumpCount());
						}
//						map.put("realNum", cami.getClumpCount());
						zz.add(map);
					}
				}
			} else if ("6".equals(qstmk)) {
				List<CleanAreaVolumeItem> list = (List<CleanAreaVolumeItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaVolumeItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaVolume().getTestDate());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaVolume().getCordon());
						map.put("deviation", cami.getCleanAreaVolume().getDeviationCorrectionLine());
						map.put("realNum", cami.getVentilationRate());
						zz.add(map);
					}
				}
			} else if ("7".equals(qstmk)) {
				List<CleanAreaDiffPressureItem> list = (List<CleanAreaDiffPressureItem>) result.get("list");
				if (list != null && list.size() > 0) {
					for (CleanAreaDiffPressureItem cami : list) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", cami.getId());
						map.put("testDate", cami.getCleanAreaDiffPressure().getTestTime());
						map.put("roomName", cami.getRoomNum());

						map.put("averageNum", result.get("avgnumber"));

						map.put("cordon", cami.getCleanAreaDiffPressure().getCordon());
						map.put("deviation", cami.getCleanAreaDiffPressure().getDeviationCorrectionLine());
						map.put("realNum", cami.getRelativePa());
						zz.add(map);
					}
				}
			} else {

			}
		} else {
			List<CleanAreaDustItem> list = (List<CleanAreaDustItem>) result.get("list");
			if (list != null && list.size() > 0) {
				for (CleanAreaDustItem cami : list) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("id", cami.getId());
					map.put("testDate", cami.getCleanAreaDust().getTestDate());
					map.put("roomName", cami.getRoomNum());
					map.put("averageNum", "");
					map.put("cordon", cami.getCleanAreaDust().getCordon());
					map.put("deviation", cami.getCleanAreaDust().getDeviationCorrectionLine());
					map.put("realNum", cami.getAverageZeropointFive());
					zz.add(map);
				}
			}
		}

		result.put("list", zz);
		return result;
	}

	public Map<String, Object> findecharsdata(String startDate, String endDate, String qstmk, String typeSelect,
			String bianma, String cleanStateSelect,String monitoringPoint,String uclMaxpointFive,String uclMaxFive,String inspectionPoint) {
		return qualityControlProfileDao.findecharsdata(startDate, endDate, qstmk, typeSelect, bianma, cleanStateSelect,monitoringPoint,uclMaxpointFive,uclMaxFive,inspectionPoint);
	}


}
