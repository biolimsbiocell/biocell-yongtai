package com.biolims.experiment.enmonitor.qualitycontrolprofile.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.ContextLoader;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.enmonitor.qualitycontrolprofile.service.QualityControlProfileService;
import com.biolims.file.service.FileInfoService;
import com.biolims.supplier.common.constants.SystemCode;
import com.biolims.system.customfields.service.FieldService;
//import com.biolims.system.qu/ality.JsonDateValueProcessor;
import com.biolims.system.quality.model.QualityProduct;
//import com.biolims.system.quality.model.QualityProductData;
//import com.biolims.system.quality.model.TargetValue;
//import com.biolims.system.quality.service.QualityProductDataService;
import com.biolims.system.quality.service.QualityProductService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/enmonitor/qualitycontrolprofile")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityControlProfileAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private QualityControlProfileService qualityControlProfileService;
//	private QualityProductData qualityProductData = new QualityProductData();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	
	@Resource
	private FieldService fieldService;
	
	
	
	/**
	 * 查询待生成趋势图列表数据
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQualityProducDatatList")
	public String showQualityProductList() throws Exception {
		rightsId="90100101";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/qualitycontrolprofile/qualityProductData.jsp");
	}

	@Action(value = "showQualityProductListDataJson")
	public void showQualityProductListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		
		String  startDate = getParameterFromRequest("startDate");
		String endDate = getParameterFromRequest("endDate");
		String qstmk = getParameterFromRequest("qstmk");
		String typeSelect = getParameterFromRequest("typeSelect");
		String bianma = getParameterFromRequest("bianma");
		String cleanStateSelect = getParameterFromRequest("cleanStateSelect");
		String monitoringPoint  = getParameterFromRequest("monitoringPoint");
		String inspectionPoint  = getParameterFromRequest("inspectionPoint");
		String uclMaxpointFive  = getParameterFromRequest("uclMaxpointFive");
		String uclMaxFive = getParameterFromRequest("uclMaxFive");
				
		try {
			Map<String, Object> result = qualityControlProfileService.findQualityProductList(start, length, query, col, sort, startDate, 
					endDate, qstmk, typeSelect, bianma, cleanStateSelect,monitoringPoint,uclMaxpointFive,uclMaxFive,inspectionPoint);
			List<Map<String, Object>> list = (List<Map<String, Object>>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			
			map.put("id", "");
			map.put("testDate", "");
			map.put("roomName", "");
			map.put("averageNum", "");
			map.put("cordon", "");
			map.put("deviation", "");
			map.put("realNum", "");
			//根据模块查询自定义字段数据
			String data = JsonUtils.toJsonString(result.get("list"));
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showQualityControJson")
	public void showQualityControJson() throws Exception {
		String  startDate = getParameterFromRequest("startDate");
		String endDate = getParameterFromRequest("endDate");
		String qstmk = getParameterFromRequest("qstmk");
		String typeSelect = getParameterFromRequest("typeSelect");
		String bianma = getParameterFromRequest("bianma");
		String cleanStateSelect = getParameterFromRequest("cleanStateSelect");
		String monitoringPoint = getParameterFromRequest("monitoringPoint");
		String inspectionPoint = getParameterFromRequest("inspectionPoint");
		String uclMaxpointFive = getParameterFromRequest("uclMaxpointFive");
		String uclMaxFive = getParameterFromRequest("uclMaxFive");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Map<String,Object> list =  qualityControlProfileService.findecharsdata(startDate, 
					endDate, qstmk, typeSelect, bianma, cleanStateSelect,monitoringPoint,uclMaxpointFive,uclMaxFive,inspectionPoint);
			map.put("riqi",  list.get("riqi"));
			map.put("pingjun", list.get("pingjun"));
			map.put("jingjie", list.get("jingjie"));
			map.put("jiupian", list.get("jiupian"));
			map.put("shuzhi", list.get("shuzhi"));
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityControlProfileService getQualityControlProfileService() {
		return qualityControlProfileService;
	}

	public void setQualityControlProfileService(
			QualityControlProfileService qualityControlProfileService) {
		this.qualityControlProfileService = qualityControlProfileService;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}

	public FieldService getFieldService() {
		return fieldService;
	}

	public void setFieldService(FieldService fieldService) {
		this.fieldService = fieldService;
	}
}
