package com.biolims.experiment.enmonitor.qualitycontrolprofile.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.quality.model.QualityProduct;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

@Repository
@SuppressWarnings("unchecked")
public class QualityControlProfileDao extends BaseHibernateDao {
	public Map<String, Object> selectQualityProductList(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityProductData where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QualityProductData where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				key+=" order by "+col+" "+sort;
			}
			List<Object> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> selectQualityProductList(Integer start,
			Integer length, String query, String col,
			String sort, String startDate, String endDate, String qstmk, 
			String typeSelect,String bianma,String cleanStateSelect,String monitoringPoint,String uclMaxpointFive,String uclMaxFive,String inspectionPoint) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "";//"select count(*) from CleanAreaDustItem where 1=1 ";
		String key = "";
		//趋势图模块
		if(qstmk!=null&&!"".equals(qstmk)){
			/**
			 * 尘埃粒子0.5
			 * */
			if("1".equals(qstmk)){
				countHql = "select count(*) from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				
				//置信0.5
				if(uclMaxpointFive!=null&&!"".equals(uclMaxpointFive)) {
					key += " and uclMaxpointFive= '"+uclMaxpointFive+"' ";
				}else {
					
				}
				
				/**
				 * 尘埃粒子5
				 * */
			}else if("2".equals(qstmk)){
				countHql = "select count(*) from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				
				//置信5
				if(uclMaxFive!=null&&!"".equals(uclMaxFive)) {
					key += " and uclMaxFive= '"+uclMaxFive+"' ";
				}else {
					
				}
				/**
				 * 浮游菌
				 **/
			}else if("3".equals(qstmk)){
				countHql = "select count(*) from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.stateName='完成'";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicrobe.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicrobe.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaMicrobe.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaMicrobe.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicrobe.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicrobe.cleanState='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				/**
				 * 沉降菌
				 **/
			}else if("4".equals(qstmk)){
				countHql = "select count(*) from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.stateName='完成' ";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaBacteria.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaBacteria.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaBacteria.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaBacteria.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaBacteria.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaBacteria.cleanState='1' ";
					}
				}else{
					
				}
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				/**
				 * 表面微生物
				 * */
			}else if("5".equals(qstmk)){
				countHql = "select count(*) from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.stateName='完成' ";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicroorganism.testTime,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicroorganism.testTime,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaMicroorganism.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaMicroorganism.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicroorganism.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicroorganism.cleanState='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(inspectionPoint!=null&&!"".equals(inspectionPoint)){
					key += " and inspectionPoint= '"+inspectionPoint+"' ";
				}else{
					
				}
				/**
				 * 风量
				 * */
			}else if("6".equals(qstmk)){
				countHql = "select count(*) from CleanAreaVolumeItem where 1=1 ";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaVolume.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaVolume.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaVolume.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaVolume.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaVolume.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaVolume.cleanState='1' ";
					}
				}else{
					
				}
				/**
				 * 压差
				 * */
			}else if("7".equals(qstmk)){
				countHql = "select count(*) from CleanAreaDiffPressureItem where 1=1 ";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaDiffPressure.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaDiffPressure.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaDiffPressure.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaDiffPressure.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaDiffPressure.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaDiffPressure.cleanState='1' ";
					}
				}else{
					
				}
				
			}
		}else{
			countHql = "select count(*) from CleanAreaDustItem where 1=1 ";
			key = " and 1=2 ";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "";//"from CleanAreaDustItem where 1=1";
			//趋势图模块
			if(qstmk!=null&&!"".equals(qstmk)){
				if("1".equals(qstmk)){
					hql = "from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成'";
				}else if("2".equals(qstmk)){
					hql = "from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成'";
				}else if("3".equals(qstmk)){
					hql = "from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.stateName='完成'";
				}else if("4".equals(qstmk)){
					hql = "from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.stateName='完成'";
				}else if("5".equals(qstmk)){
					hql = "from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.stateName='完成'";
				}else if("6".equals(qstmk)){
					hql = "from CleanAreaVolumeItem where 1=1";
				}else if("7".equals(qstmk)){
					hql = "from CleanAreaDiffPressureItem where 1=1";
				}else{
					
				}
			}else{
				hql = "from CleanAreaDustItem where 1=1";
				key = " and 1=2 ";
			}
			if(qstmk!=null&&!"".equals(qstmk)){
				if("1".equals(qstmk)){
					List<CleanAreaDustItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(uclMaxpointFive),2) from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else if("2".equals(qstmk)){
					List<CleanAreaDustItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(uclMaxFive),2) from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else if("3".equals(qstmk)){
					List<CleanAreaMicrobeItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(monitoringResults),2) from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else if("4".equals(qstmk)){
					List<CleanAreaBacteriaItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(monitoringResults),2) from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else if("5".equals(qstmk)){
					List<CleanAreaMicroorganismItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(clumpCount),2) from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else if("6".equals(qstmk)){
					List<CleanAreaVolumeItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(ventilationRate),2) from CleanAreaVolumeItem where 1=1 ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else if("7".equals(qstmk)){
					List<CleanAreaDiffPressureItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
					String avgnum = "select round(AVG(relativePa),2) from CleanAreaDiffPressureItem where 1=1 ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					map.put("recordsTotal", sumCount);
					map.put("recordsFiltered", filterCount);
					map.put("list", list);
					
					map.put("avgnumber", avgnumber);
				}else{
					
				}
			}else{
				List<CleanAreaDustItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				String avgnum = "select round(AVG(averageZeropointFive),2) from CleanAreaDustItem where 1=1 ";
				Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				
				map.put("avgnumber", avgnumber);
			}
			
		}else{
			map.put("recordsTotal", 0);
			map.put("recordsFiltered", 0);
			map.put("list", null);
			
			map.put("avgnumber", 0);
		}
		return map;

	}
	public Map<String, Object> findecharsdata(String startDate,
			String endDate, String qstmk, String typeSelect, String bianma,
			String cleanStateSelect,String monitoringPoint,String uclMaxpointFive,String uclMaxFive,String inspectionPoint) {
		Map<String, Object> lis = new HashMap<String, Object>();
		if(qstmk!=null&&!"".equals(qstmk)){
			if("1".equals(qstmk)){
				String hql = "from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaDustItem> list = new ArrayList<CleanAreaDustItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(uclMaxpointFive),2) from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaDustItem cad:list){
						riqi.add(cad.getCleanAreaDust().getTestDate());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaDust().getCordon());
						jiupian.add(cad.getCleanAreaDust().getDeviationCorrectionLine());
						if("<1".equals(cad.getUclMaxpointFive())) {
							shuzhi.add("0");
						}else {
							shuzhi.add(cad.getUclMaxpointFive());
						}
//						shuzhi.add(cad.getUclMaxpointFive());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
			}else if("2".equals(qstmk)){
				String hql = "from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaDust.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaDust.cleanZoneStatus='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaDustItem> list = new ArrayList<CleanAreaDustItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(uclMaxFive),2) from CleanAreaDustItem where 1=1 and cleanAreaDust.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaDustItem cad:list){
						riqi.add(cad.getCleanAreaDust().getTestDate());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaDust().getCordon2());
						jiupian.add(cad.getCleanAreaDust().getDeviationCorrectionLine2());
						if("<1".equals(cad.getUclMaxFive())) {
							shuzhi.add("0");
						}else {
							shuzhi.add(cad.getUclMaxFive());
						}
//						shuzhi.add(cad.getUclMaxFive());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
				//浮游菌
			}else if("3".equals(qstmk)){
				String hql = "from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.stateName='完成' ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicrobe.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicrobe.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaMicrobe.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaMicrobe.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicrobe.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicrobe.cleanState='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaMicrobeItem> list = new ArrayList<CleanAreaMicrobeItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaMicrobe.testDate,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(monitoringResults),2) from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaMicrobeItem cad:list){
						riqi.add(cad.getCleanAreaMicrobe().getTestDate());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaMicrobe().getCordon());
						jiupian.add(cad.getCleanAreaMicrobe().getDeviationCorrectionLine());
						if("<1".equals(cad.getMonitoringResults())) {
							shuzhi.add("0");
						}else {
							shuzhi.add(cad.getMonitoringResults());
						}
//						shuzhi.add(cad.getMonitoringResults());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
				//沉降菌
			}else if("4".equals(qstmk)){
				String hql = "from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.stateName='完成' ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaBacteria.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaBacteria.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaBacteria.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaBacteria.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaBacteria.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaBacteria.cleanState='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
					key += " and monitoringPoint= '"+monitoringPoint+"' ";
				}else{
					
				}
				
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaBacteriaItem> list = new ArrayList<CleanAreaBacteriaItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaBacteria.testDate,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(monitoringResults()),2) from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaBacteriaItem cad:list){
						riqi.add(cad.getCleanAreaBacteria().getTestDate());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaBacteria().getCordon());
						jiupian.add(cad.getCleanAreaBacteria().getDeviationCorrectionLine());
						if("<1".equals(cad.getMonitoringResults())) {
							shuzhi.add("0");
						}else {
							shuzhi.add(cad.getMonitoringResults());
						}
//						shuzhi.add(cad.getMonitoringResults());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
				//表面微生物
			}else if("5".equals(qstmk)){
				String hql = "from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.stateName='完成' ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicroorganism.testTime,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaMicroorganism.testTime,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaMicroorganism.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaMicroorganism.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicroorganism.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaMicroorganism.cleanState='1' ";
					}
				}else{
					
				}
				
				//监测点
				if(inspectionPoint!=null&&!"".equals(inspectionPoint)){
					key += " and inspectionPoint= '"+inspectionPoint+"' ";
				}else{
					
				}
				
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaMicroorganismItem> list = new ArrayList<CleanAreaMicroorganismItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaMicroorganism.testTime,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(clumpCount),2) from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.stateName='完成' ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaMicroorganismItem cad:list){
						riqi.add(cad.getCleanAreaMicroorganism().getTestTime());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaMicroorganism().getCordon());
						jiupian.add(cad.getCleanAreaMicroorganism().getDeviationCorrectionLine());
						if("<1".equals(cad.getClumpCount())) {
							shuzhi.add("0");
						}else {
							shuzhi.add(cad.getClumpCount());
						}
//						shuzhi.add(cad.getClumpCount());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
				//风量
			}else if("6".equals(qstmk)){
				String hql = "from CleanAreaVolumeItem where 1=1 ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaVolume.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaVolume.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaVolume.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaVolume.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaVolume.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaVolume.cleanState='1' ";
					}
				}else{
					
				}
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaVolumeItem> list = new ArrayList<CleanAreaVolumeItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaVolume.testDate,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(ventilationRate),2) from CleanAreaVolumeItem where 1=1 ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaVolumeItem cad:list){
						riqi.add(cad.getCleanAreaVolume().getTestDate());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaVolume().getCordon());
						jiupian.add(cad.getCleanAreaVolume().getDeviationCorrectionLine());
						shuzhi.add(cad.getVentilationRate());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
				//压差
			}else if("7".equals(qstmk)){
				String hql = "from CleanAreaDiffPressureItem where 1=1 ";
				String key = "";
				//开始日期
				if(startDate!=null&&!"".equals(startDate)){
					key = key + " and STR_TO_DATE(cleanAreaDiffPressure.testTime,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//结束日期
				if(endDate!=null&&!"".equals(endDate)){
					key = key + " and STR_TO_DATE(cleanAreaDiffPressure.testTime,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
				}else{
					
				}
				//类型0房间1设备
				if(typeSelect!=null&&!"".equals(typeSelect)){
					if("0".equals(typeSelect)){
						key = key + " and cleanAreaDiffPressure.type='0' ";
					}else if("1".equals(typeSelect)){
						key = key + " and cleanAreaDiffPressure.type='1' ";
					}
				}else{
					
				}
				//房间设备（名称）
				if(bianma!=null&&!"".equals(bianma)){
					key += " and roomNum= '"+bianma+"' ";
				}else{
					
				}
				//清洁区状态0动态1静态
				if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
					if("0".equals(cleanStateSelect)){
						key = key + " and cleanAreaDiffPressure.cleanState='0' ";
					}else if("1".equals(cleanStateSelect)){
						key = key + " and cleanAreaDiffPressure.cleanState='1' ";
					}
				}else{
					
				}
				Long total = (Long) this.getSession()
						.createQuery("select count(*) " + hql + key).uniqueResult();
				List<CleanAreaDiffPressureItem> list = new ArrayList<CleanAreaDiffPressureItem>();
				if (total > 0) {
					key = key +"  order by STR_TO_DATE(cleanAreaDiffPressure.testTime,'%Y-%m-%d') ASC";
					list = this.getSession().createQuery(hql + key).list();
					
					String avgnum = "select round(AVG(relativePa),2) from CleanAreaDiffPressureItem where 1=1 ";
					Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
					
					ArrayList<String> riqi = new ArrayList<String>();
					ArrayList<String> pingjun = new ArrayList<String>();
					ArrayList<String> jingjie = new ArrayList<String>();
					ArrayList<String> jiupian = new ArrayList<String>();
					ArrayList<String> shuzhi = new ArrayList<String>();
					for(CleanAreaDiffPressureItem cad:list){
						riqi.add(cad.getCleanAreaDiffPressure().getTestTime());
						pingjun.add(String.valueOf(avgnumber));
						jingjie.add(cad.getCleanAreaDiffPressure().getCordon());
						jiupian.add(cad.getCleanAreaDiffPressure().getDeviationCorrectionLine());
						shuzhi.add(cad.getRelativePa());
					}
					lis.put("riqi", riqi);
					lis.put("pingjun", pingjun);
					lis.put("jingjie", jingjie);
					lis.put("jiupian", jiupian);
					lis.put("shuzhi", shuzhi);
				}
			}else{
				
			}
			
		}else{

			String hql = "from CleanAreaDustItem where 1=1 ";
			String key = "";
			//开始日期
			if(startDate!=null&&!"".equals(startDate)){
				key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')>=STR_TO_DATE('"+startDate+"','%Y-%m-%d') ";
			}else{
				
			}
			//结束日期
			if(endDate!=null&&!"".equals(endDate)){
				key = key + " and STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d')<=STR_TO_DATE('"+endDate+"','%Y-%m-%d') ";
			}else{
				
			}
			//类型0房间1设备
			if(typeSelect!=null&&!"".equals(typeSelect)){
				if("0".equals(typeSelect)){
					key = key + " and cleanAreaDust.type='0' ";
				}else if("1".equals(typeSelect)){
					key = key + " and cleanAreaDust.type='1' ";
				}
			}else{
				
			}
			//房间设备（名称）
			if(bianma!=null&&!"".equals(bianma)){
				key += " and roomNum= '"+bianma+"' ";
			}else{
				
			}
			//清洁区状态0动态1静态
			if(cleanStateSelect!=null&&!"".equals(cleanStateSelect)){
				if("0".equals(cleanStateSelect)){
					key = key + " and cleanAreaDust.cleanZoneStatus='0' ";
				}else if("1".equals(cleanStateSelect)){
					key = key + " and cleanAreaDust.cleanZoneStatus='1' ";
				}
			}else{
				
			}
			
			//监测点
			if(monitoringPoint!=null&&!"".equals(monitoringPoint)){
				key += " and monitoringPoint= '"+monitoringPoint+"' ";
			}else{
				
			}
			
			Long total = (Long) this.getSession()
					.createQuery("select count(*) " + hql + key).uniqueResult();
			List<CleanAreaDustItem> list = new ArrayList<CleanAreaDustItem>();
			if (total > 0) {
				key = key +"  order by STR_TO_DATE(cleanAreaDust.testDate,'%Y-%m-%d') ASC";
				list = this.getSession().createQuery(hql + key).list();
				
				String avgnum = "select round(AVG(averageZeropointFive),2) from CleanAreaDustItem where 1=1 ";
				Double avgnumber = (Double) getSession().createQuery(avgnum+key).uniqueResult();
				
				ArrayList<String> riqi = new ArrayList<String>();
				ArrayList<String> pingjun = new ArrayList<String>();
				ArrayList<String> jingjie = new ArrayList<String>();
				ArrayList<String> jiupian = new ArrayList<String>();
				ArrayList<String> shuzhi = new ArrayList<String>();
				for(CleanAreaDustItem cad:list){
					riqi.add(cad.getCleanAreaDust().getTestDate());
					pingjun.add(String.valueOf(avgnumber));
					jingjie.add(cad.getCleanAreaDust().getCordon());
					jiupian.add(cad.getCleanAreaDust().getDeviationCorrectionLine());
					shuzhi.add(cad.getAverageZeropointFive());
				}
				lis.put("riqi", riqi);
				lis.put("pingjun", pingjun);
				lis.put("jingjie", jingjie);
				lis.put("jiupian", jiupian);
				lis.put("shuzhi", shuzhi);
			}
		
		}
		return lis;
	}
	
}