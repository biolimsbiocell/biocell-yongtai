package com.biolims.experiment.enmonitor.bacteria.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;

/**
 * @Title: Model
 * @Description: 沉降菌  主表
 * @author lims-platform
 * @date 2019-03-22 17:06:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "clean_area_bacteria")
@SuppressWarnings("serial")
public class CleanAreaBacteria extends EntityDao<CleanAreaBacteria> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	@Column(name="name")
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	@Column(name="create_date")
	private String createDate;
	/** 车间部门 */
	@Column(name="workshop_department")
	private String workshopDepartment;
	/** 区域 */
	@Column(name="region")
	private String region;
	/** 区域ID */
	@Column(name="regionId")
	private String regionId;
	/** 测试时间开始 */
	@Column(name="test_date")
	private String testDate;
	/** 测试时间结束 */
	@Column(name="TEST_DATE1")
	private String testDate1;
	/** 测试人 */
	@Column(name="test_user")
	private String testUser;
	/** 培养基名称*/
	@Column(name="name_culture_medium")
	private String nameCultureMedium;
	/** 培养基批号  */
	@Column(name="batch_number_medium")
	private String batchNumberMedium;
	/** 培养基编号 */
	@Column(name="medium_code")
	private String mediumCode;
	/** 培养基温度 */
	@Column(name="medium_temperature")
	private String mediumTemperature;
	/** 观察人 */
	@Column(name="observer_user")
	private String observerUser;
	/** 观察日期备注*/
	@Column(name="notes_observation_date")
	private String notesObservationDate;
	/** 备注 */
	@Column(name="note")
	private String note;
	/** 阴性对照 */
	@Column(name="negative_control")
	private String negativeControl;
	/**状态 */
	@Column(name="state")
	private String state;
	/**类型*/
	@Column(name="type")
	private String type;
	/** 审核人 */
	private User confirmUser;
	
	/**状态名称 */
	@Column(name="STATE_NAME")
	private String stateName;
	/** 复核时间*/
	private String confirmDate;
	/** 通知人id */
	private String notifierId;
	/** 通知人姓名 */
	private String notifierName;
	
	/**清洁区状态*/
	@Column(name="clean_state")
	private String cleanState;
	/**警戒线*/
	@Column(name="cordon")
	private String cordon;
	/**纠偏线*/
	@Column(name="deviationCorrectionLine")
	private String deviationCorrectionLine;
	
	//批准人
	private User approver;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPROVER")
	public User getApprover() {
		return approver;
	}

	public void setApprover(User approver) {
		this.approver = approver;
	}
	
	
	
	
	public String getRegionId() {
		return regionId;
	}


	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}


	public String getCleanState() {
		return cleanState;
	}


	public void setCleanState(String cleanState) {
		this.cleanState = cleanState;
	}

	public String getCordon() {
		return cordon;
	}

	public void setCordon(String cordon) {
		this.cordon = cordon;
	}

	
	public String getDeviationCorrectionLine() {
		return deviationCorrectionLine;
	}


	public void setDeviationCorrectionLine(String deviationCorrectionLine) {
		this.deviationCorrectionLine = deviationCorrectionLine;
	}


	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getNotifierId() {
		return notifierId;
	}

	public void setNotifierId(String notifierId) {
		this.notifierId = notifierId;
	}

	public String getNotifierName() {
		return notifierName;
	}

	public void setNotifierName(String notifierName) {
		this.notifierName = notifierName;
	}

	public String getTestDate1() {
		return testDate1;
	}

	public void setTestDate1(String testDate1) {
		this.testDate1 = testDate1;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public String getWorkshopDepartment() {
		return workshopDepartment;
	}

	public void setWorkshopDepartment(String workshopDepartment) {
		this.workshopDepartment = workshopDepartment;
	}
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTestDate() {
		return testDate;
	}

	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}

	public String getTestUser() {
		return testUser;
	}

	public void setTestUser(String testUser) {
		this.testUser = testUser;
	}

	public String getNameCultureMedium() {
		return nameCultureMedium;
	}

	public void setNameCultureMedium(String nameCultureMedium) {
		this.nameCultureMedium = nameCultureMedium;
	}

	public String getBatchNumberMedium() {
		return batchNumberMedium;
	}

	public void setBatchNumberMedium(String batchNumberMedium) {
		this.batchNumberMedium = batchNumberMedium;
	}

	public String getMediumCode() {
		return mediumCode;
	}

	public void setMediumCode(String mediumCode) {
		this.mediumCode = mediumCode;
	}

	public String getMediumTemperature() {
		return mediumTemperature;
	}

	public void setMediumTemperature(String mediumTemperature) {
		this.mediumTemperature = mediumTemperature;
	}

	public String getObserverUser() {
		return observerUser;
	}

	public void setObserverUser(String observerUser) {
		this.observerUser = observerUser;
	}

	public String getNotesObservationDate() {
		return notesObservationDate;
	}

	public void setNotesObservationDate(String notesObservationDate) {
		this.notesObservationDate = notesObservationDate;
	}

	public String getNegativeControl() {
		return negativeControl;
	}

	public void setNegativeControl(String negativeControl) {
		this.negativeControl = negativeControl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


}