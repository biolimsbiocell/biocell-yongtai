﻿package com.biolims.experiment.enmonitor.bacteria.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItemNegative;
import com.biolims.experiment.enmonitor.bacteria.service.CleanAreaBacteriaService;
import com.biolims.experiment.enmonitor.dust.dao.DustParticleDao;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/enmonitor/celanAreaBacteria")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CleanAreaBacteriaAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "211030402";
	@Autowired
	private CleanAreaBacteriaService cleanAreaBacteriaService;
	private CleanAreaBacteria dif = new CleanAreaBacteria();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private DustParticleDao dustParticleDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	
	
	
	/**
	 *	明细上传CSV文件
     * @Title: uploadCsvFile  
     * @Description: TODO  
     * @param @throws Exception    
     * @return void  
	 * @author 孙灵达  
     * @date 2019年4月19日
     * @throws
	 */
	@Action(value = "uploadCsvFile")
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String flag = getParameterFromRequest("flag");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cleanAreaBacteriaService.uploadCsvFile(id, fileId, flag);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 *	明细下载CSV模板文件
     * @Title: downloadCsvFile  
     * 
     * @throws
	 */
	@Action(value = "downloadCsvFile")
	public void downloadCsvFile() throws Exception {
		String mainTable_id=getParameterFromRequest("id");
				
		Map<String, Object> result = new HashMap<String, Object>();
		Date date= new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("system.properties");

		properties.load(is);
				
		String filePath = properties.getProperty("sample.path")+"\\";// 写入csv路径
		String fileName = filePath + "cleanAreaBacteriaItem" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
				parent.mkdirs();
		} else {
				parent.delete();
				parent.mkdirs();
		}
			csvFile.createNewFile();
				// GB2312使正确读取分隔符","
				csvWtriter = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(csvFile), "GBK"), 1024);
				// 写入文件头部
				Object[] head = { "房间名称", "房间编号","监测点", "洁净区级别","监测结果cfu/皿", "合格标准cfu/皿","结果是否合格", "备注" };
				List<Object> headList = Arrays.asList(head);
				for (Object data : headList) {
					StringBuffer sb = new StringBuffer();
					String rowStr = sb.append("\"").append(data).append("\",")
							.toString();
					csvWtriter.write(rowStr);
				}
				csvWtriter.newLine();
				List<CleanAreaBacteriaItem> list = cleanAreaBacteriaService.findCleanAreaBacteriaItemTable(mainTable_id);
				for (int i = 0; i < list.size(); i++) {
					StringBuffer sb = new StringBuffer();
					setMolecularMarkersData(list.get(i), sb);
					String rowStr = sb.toString();
					csvWtriter.write(rowStr);
					csvWtriter.newLine();
					result.put("success", true);

			}
				csvWtriter.flush(); 
				csvWtriter.close();
				//HttpUtils.write(JsonUtils.toJsonString(result));
				downLoadTemp3("cleanAreaBacteriaItem", filePath);
	}
			
			/**
			 * 
			 * @Title: setMolecularMarkersData  
			 * @Description: 数据添加到csv内
			 * @author qi.yan
			 * @date 2018-12-24上午11:53:44
			 * 
			 */
			public void setMolecularMarkersData(CleanAreaBacteriaItem sr, StringBuffer sb)
					throws Exception {
				if (null!=sr.getRoomName() && !"".equals(sr.getRoomName())) {
					sb.append("\"").append(sr.getRoomName()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				} 
				//编号
				if (null!=sr.getRoomNum() && !"".equals(sr.getRoomNum())) {
					sb.append("\"").append(sr.getRoomNum()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				}
				if (null!=sr.getMonitoringPoint()&& !"".equals(sr.getMonitoringPoint())) {
					sb.append("\"").append(sr.getMonitoringPoint()).append("\",");
				} else {
					sb.append("\"").append("").append("\",");
				}
				
			}
			/**
			 * 
			 * @Title: downLoadTemp3  
			 * @Description: 下载文件
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:26
			 * @param a
			 * @param filePath2
			 * @throws Exception
			 * void
			 * @throws
			 */
			@Action(value = "downLoadTemp3")
			public void downLoadTemp3(String a, String filePath2) throws Exception {
				String filePath = filePath2;// 保存窗口中显示的文件名
				String fileName = a + ".csv";// 保存窗口中显示的文件名
				super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

				/*
				 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
				 */
				ServletOutputStream out = null;
				// PrintWriter out = null;
				InputStream inStream = null;
				try {
					fileName = super.getResponse().encodeURL(
							new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

					super.getResponse().setHeader("Content-Disposition",
							"attachment; filename=\"" + fileName + "\"");
					// inline
					out = super.getResponse().getOutputStream();

					inStream = new FileInputStream(filePath + toUtf8String(fileName));

					// 循环取出流中的数据
					byte[] b = new byte[1024];
					int len;
					while ((len = inStream.read(b)) > 0)
						out.write(b, 0, len);
					super.getResponse().setStatus(super.getResponse().SC_OK);
					super.getResponse().flushBuffer();

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (RuntimeException e) {
					e.printStackTrace();
				} finally {
					if (out != null)
						out.close();
					inStream.close();
				}
			}
			/**
			 * 
			 * @Title: toUtf8String  
			 * @Description: 解决乱码问题
			 * @author qi.yan
			 * @date 2018-12-24上午11:55:11
			 * @param s
			 * @return
			 * String
			 * @throws
			 */
			public static String toUtf8String(String s) {
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < s.length(); i++) {
					char c = s.charAt(i);
					if (c >= 0 && c <= 255) {
						sb.append(c);
					} else {
						byte[] b;
						try {
							b = Character.toString(c).getBytes("utf-8");
						} catch (Exception ex) {
							System.out.println(ex);
							b = new byte[0];
						}
						for (int j = 0; j < b.length; j++) {
							int k = b[j];
							if (k < 0)
								k += 256;
							sb.append("%" + Integer.toHexString(k).toUpperCase());
						}
					}
				}
				return sb.toString();
			}

	/**
	 * 
	 * @Title: showDustParticleList @Description:展示主表 @author 洁净区
	 *         沉降菌模块 @date @return @throws Exception String @throws
	 */
	@Action(value = "showCleanAreaBacteriaEdit")
	public String showCleanAreaBacteriaTable() throws Exception {
		rightsId = "211030401";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			dif = commonService.get(CleanAreaBacteria.class, id);
			toState(dif.getState());
		}else{
			dif.setState("3");
			dif.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/bacteria/cleanAreaBacteriaEdit.jsp");
	}

	/**
	 * 
	 * @Title: showDustParticleList @Description:展示主表 @author 洁净区
	 *         沉降菌模块 @date @return @throws Exception String @throws
	 */
	@Action(value = "showCleanAreaBacteriaTableJson")
	public String showCleanAreaBacteriaTableJson() throws Exception {
		rightsId = "211030402";
		String id = getParameterFromRequest("id");
		dif = commonService.get(CleanAreaBacteria.class, id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/bacteria/cleanAreaBacteria.jsp");
	}

	// 沉降菌主表 列表数据
	@Action(value = "showCleanAreaBacteriaTableJsonList")
	public void showCleanAreaBacteriaTableJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cleanAreaBacteriaService.findCleanAreaMicrodeTable(id, start, length, query,
					col, sort);
			List<CleanAreaBacteria> list = (List<CleanAreaBacteria>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaBacteria cleanAreaMicrobe = new CleanAreaBacteria();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMicrobe);
			map.put("createUser-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 浮游菌子表 列表数据
	@Action(value = "showCleanAreaBacteriaItemTableJsonList")
	public void showCleanAreaBacteriaItemTableJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (query != null && !"".equals(query)) {
			map2Query = JsonUtils.toObjectByJson(query, Map.class);
			id=(String) map2Query.get("id");
			map2Query.remove("id");
		}
		try {
			Map<String, Object> result = cleanAreaBacteriaService.findCleanAreaMicrodeItemTable(id, start, length,
					map2Query, col, sort);
			List<CleanAreaBacteriaItem> list = (List<CleanAreaBacteriaItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaBacteriaItem cleanAreaMicrobe = new CleanAreaBacteriaItem();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMicrobe);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//阴性对照  子表 列表数据
	@Action(value = "showBacteriaNegativeTableJsonList")
	public void showBacteriaNegativeTableJsonList() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cleanAreaBacteriaService.findMicrodeNegativeTable(id, start, length,
					query, col, sort);
			List<CleanAreaBacteriaItemNegative> list = (List<CleanAreaBacteriaItemNegative>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CleanAreaBacteriaItemNegative cleanAreaMicrobe = new CleanAreaBacteriaItemNegative();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cleanAreaMicrobe);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveItem")
	public String saveItem() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String changeLogItem2 = getParameterFromRequest("changeLogItem2");
		String dataJson = getParameterFromRequest("documentInfoItemJson");
		String dataJsontow = getParameterFromRequest("documentInfoItemJsontow");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String id = dif.getId();
		String log="";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log="123";
			String modelName = "CleanAreaBacteria";
			String markCode = "CB";
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yy");
			String stime = format.format(date);
			String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
			dif.setId(autoID);
			dif.setCreateUser(user);
			dif.setCreateDate(ObjectToMapUtils.getTimeString());
		}
		commonService.saveOrUpdate(dif);
		cleanAreaBacteriaService.saveBacteriaInfoItem(dif, dataJson, dataJsontow, changeLog,changeLogItem,changeLogItem2,log);
		return redirect("/experiment/enmonitor/celanAreaBacteria/showCleanAreaBacteriaEdit.action?id=" + dif.getId()+"&bpmTaskId="+bpmTaskId);
	}
	
	/**
	 * 选择房间
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selectRoomTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectRoomTable() throws Exception {
//		return dispatcher("/WEB-INF/page/experiment/roomManagement/selectRoomTable.jsp");
		return dispatcher("/WEB-INF/page/experiment/roomManagement/selectRoomTableAll.jsp");
	}
	//删除房间
	@Action(value = "delRoom", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delRoom() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			cleanAreaBacteriaService.delRoom(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "delNegative", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delNegative() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			cleanAreaBacteriaService.delNegative(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "useTemplateAddItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void useTemplateAddItem() throws Exception {
		String id = getParameterFromRequest("id");
		String cleanAreaBacteriaId = getParameterFromRequest("cleanAreaBacteriaId");
		cleanAreaBacteriaService.useTemplateAddItem(id,cleanAreaBacteriaId);
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DustParticleDao getDustParticleDao() {
		return dustParticleDao;
	}

	public CleanAreaBacteria getDif() {
		return dif;
	}

	public void setDif(CleanAreaBacteria dif) {
		this.dif = dif;
	}

}
