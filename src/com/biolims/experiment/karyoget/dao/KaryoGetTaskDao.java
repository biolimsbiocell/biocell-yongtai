package com.biolims.experiment.karyoget.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyoget.model.KaryoGetTask;
import com.biolims.experiment.karyoget.model.KaryoGetTaskReagent;
import com.biolims.experiment.karyoget.model.KaryoGetTaskCos;
import com.biolims.experiment.karyoget.model.KaryoGetTaskResult;
import com.biolims.experiment.karyoget.model.KaryoGetTaskItem;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemplate;

@Repository
@SuppressWarnings("unchecked")
public class KaryoGetTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectKaryoGetTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoGetTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTask> list = new ArrayList<KaryoGetTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectKaryoGetTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoGetTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoGetTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTaskItem> list = new ArrayList<KaryoGetTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoGetTaskTemplateList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoGetTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoGetTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTaskTemplate> list = new ArrayList<KaryoGetTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoGetTaskAgentiaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoGetTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoGetTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTaskReagent> list = new ArrayList<KaryoGetTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoGetTaskCosList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoGetTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoGetTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTaskCos> list = new ArrayList<KaryoGetTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoGetTaskHarvestList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoGetTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoGetTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTaskResult> list = new ArrayList<KaryoGetTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		/*public Map<String, Object> selectKaryoGetTaskTempList(String scId, Integer startNum, Integer limitNum,
				String dir, String sort) throws Exception {
			String hql = "from KaryoGetTaskTemp where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<KaryoGetTaskTemp> list = new ArrayList<KaryoGetTaskTemp>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}*/
	public Map<String, Object> selectKaryoGetTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoGetTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null){
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoGetTaskTemp> list = new ArrayList<KaryoGetTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	/**
	 * 根据主表ID查询结果明细
	 * @param id
	 * @return
	 */
	public List<KaryoGetTaskResult> getKaryoGetTaskResultById(String id){
		String hql = " from KaryoGetTaskResult where 1=1 and karyoGetTask='"+id+"'";
		List<KaryoGetTaskResult> list=this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<KaryoGetTaskResult> setResultById(String code) {
		String hql = "from KaryoGetTaskResult t where (isCommit is null or isCommit='') and karyoGetTask.id='"
				+ code + "'";
		List<KaryoGetTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<KaryoGetTaskResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from KaryoGetTaskResult t where (isCommit is null or isCommit='') and id in ("
				+ insql + ")";
		List<KaryoGetTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<KaryoGetTaskItem> getItem(String id) {
		String hql = "from KaryoGetTaskItem t where 1=1  and karyoGetTask='"+ id + "'";
		List<KaryoGetTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<KaryoGetTaskReagent> getReagent(String id) {
		String hql = "from KaryoGetTaskReagent t where 1=1  and karyoGetTask='"+ id + "'";
		List<KaryoGetTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
}