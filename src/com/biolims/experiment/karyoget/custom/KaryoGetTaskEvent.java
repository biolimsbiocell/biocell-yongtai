package com.biolims.experiment.karyoget.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.karyoget.service.KaryoGetTaskService;

public class KaryoGetTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		KaryoGetTaskService mbService = (KaryoGetTaskService) ctx.getBean("karyoGetTaskService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
