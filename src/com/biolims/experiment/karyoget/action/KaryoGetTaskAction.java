package com.biolims.experiment.karyoget.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyo.model.KaryoTypeTask;
import com.biolims.experiment.karyoget.model.KaryoGetTask;
import com.biolims.experiment.karyoget.model.KaryoGetTaskReagent;
import com.biolims.experiment.karyoget.model.KaryoGetTaskCos;
import com.biolims.experiment.karyoget.model.KaryoGetTaskResult;
import com.biolims.experiment.karyoget.model.KaryoGetTaskItem;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemplate;
import com.biolims.experiment.karyoget.service.KaryoGetTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/karyoget/karyoGetTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KaryoGetTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249002";
	@Autowired
	private KaryoGetTaskService karyoGetTaskService;
	private KaryoGetTask karyoGetTask = new KaryoGetTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showKaryoGetTaskList")
	public String showKaryoGetTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTask.jsp");
	}

	@Action(value = "showKaryoGetTaskListJson")
	public void showKaryoGetTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoGetTaskService.findKaryoGetTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoGetTask> list = (List<KaryoGetTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "karyoGetTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKaryoGetTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskDialog.jsp");
	}

	@Action(value = "showDialogKaryoGetTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKaryoGetTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoGetTaskService.findKaryoGetTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoGetTask> list = (List<KaryoGetTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKaryoGetTask")
	public String editKaryoGetTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			karyoGetTask = karyoGetTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "karyoGetTask");
		} else {
			karyoGetTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			karyoGetTask.setCreateUser(user);
			karyoGetTask.setCreateDate(new Date());
			karyoGetTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			karyoGetTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doCkaGet");
//			Template t=tlist.get(0);
//			karyoGetTask.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				karyoGetTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(karyoGetTask.getState());
		putObjToContext("fileNum", num);
		User user2 = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		putObjToContext("user2", user2.getName());
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskEdit.jsp");
	}

	@Action(value = "copyKaryoGetTask")
	public String copyKaryoGetTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		karyoGetTask = karyoGetTaskService.get(id);
		karyoGetTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = karyoGetTask.getId();
		/*
		 * if(id!=null&&id.equals("")){ karyoGetTask.setId(null); }
		 */
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "KaryoGetTask";
			String markCode = "YBSH";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			karyoGetTask.setId(autoID);
//			karyoGetTask.setName("样本收获"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("karyoGetTaskItem",
				getParameterFromRequest("karyoGetTaskItemJson"));

		aMap.put("karyoGetTaskTemplate",
				getParameterFromRequest("karyoGetTaskTemplateJson"));

		aMap.put("karyoGetTaskReagent",
				getParameterFromRequest("karyoGetTaskReagentJson"));

		aMap.put("karyoGetTaskCos",
				getParameterFromRequest("karyoGetTaskCosJson"));

		aMap.put("karyoGetTaskResult",
				getParameterFromRequest("karyoGetTaskResultJson"));

		karyoGetTaskService.save(karyoGetTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/karyoget/karyoGetTask/editKaryoGetTask.action?id="
				+ karyoGetTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/karyoget/karyoGetTask/editKaryoGetTask.action?id="
		// + karyoGetTask.getId());

	}

	@Action(value = "viewKaryoGetTask")
	public String toViewKaryoGetTask() throws Exception {
		String id = getParameterFromRequest("id");
		karyoGetTask = karyoGetTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskEdit.jsp");
	}

	@Action(value = "showKaryoGetTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoGetTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskItem.jsp");
	}

	@Action(value = "showKaryoGetTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoGetTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoGetTaskService
					.findKaryoGetTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoGetTaskItem> list = (List<KaryoGetTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("experimentCode", "");
			map.put("checkCode", "");
			map.put("inoculateDate", "yyyy-MM-dd");
			map.put("preReapDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("sampleType", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("productNum", "");
			map.put("orderId", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("karyoGetTask-name", "");
			map.put("karyoGetTask-id", "");

			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("reapDate", "yyyy-MM-dd");
			
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskItem")
	public void delKaryoGetTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoGetTaskService.delKaryoGetTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoGetTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoGetTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskTemplate.jsp");
	}

	@Action(value = "showKaryoGetTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoGetTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoGetTaskService
					.findKaryoGetTaskTemplateList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KaryoGetTaskTemplate> list = (List<KaryoGetTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("stepNum", "");
			map.put("stepDescribe", "");
			map.put("experimentUser-id", "");
			map.put("experimentUser-name", "");
			map.put("startDate", "");
			map.put("endDate", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("karyoGetTask-name", "");
			map.put("karyoGetTask-id", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("tItem", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskTemplate")
	public void delKaryoGetTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoGetTaskService.delKaryoGetTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoGetTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoGetTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskReagent.jsp");
	}

	@Action(value = "showKaryoGetTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoGetTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoGetTaskService
					.findKaryoGetTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoGetTaskReagent> list = (List<KaryoGetTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("agentiaCode", "");
			map.put("agentiaName", "");
			map.put("batch", "");
			map.put("isCheck", "");
			map.put("singleDosage", "");
			map.put("reactionDosage", "");
			map.put("dosage", "");
			map.put("karyoGetTask-name", "");
			map.put("karyoGetTask-id", "");
			map.put("note", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskReagent")
	public void delKaryoGetTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoGetTaskService.delKaryoGetTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoGetTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoGetTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskCos.jsp");
	}

	@Action(value = "showKaryoGetTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoGetTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoGetTaskService
					.findKaryoGetTaskCosList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoGetTaskCos> list = (List<KaryoGetTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrumentCode", "");
			map.put("instrumentName", "");
			map.put("isCheck", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "yyyy-MM-dd");
			map.put("karyoGetTask-name", "");
			map.put("karyoGetTask-id", "");
			map.put("note", "");
			map.put("tCos", "");
			map.put("itemId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskCos")
	public void delKaryoGetTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoGetTaskService.delKaryoGetTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoGetTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoGetTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskResult.jsp");
	}

	@Action(value = "showKaryoGetTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoGetTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoGetTaskService
					.findKaryoGetTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoGetTaskResult> list = (List<KaryoGetTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("experimentCode", "");
			map.put("code", "");
			map.put("zjCode", "");
			map.put("harvestDate", "yyyy-MM-dd");
			map.put("inoculateDate", "yyyy-MM-dd");
			map.put("preReapDate", "yyyy-MM-dd");
			map.put("harversUser", "");
			map.put("nextFlow", "");
			map.put("nextFlowId", "");
			map.put("isrStandard", "");
			map.put("isCommit", "");
			map.put("karyoGetTask-name", "");
			map.put("karyoGetTask-id", "");
			map.put("note", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskResult")
	public void delKaryoGetTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoGetTaskService.delKaryoGetTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoGetTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoGetTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoget/karyoGetTaskTemp.jsp");
	}

	@Action(value = "showKaryoGetTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoGetTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoGetTaskService
				.findKaryoGetTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long total = (Long) result.get("total");
		List<KaryoGetTaskTemp> list = (List<KaryoGetTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");

		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("state", "");
		map.put("note", "");
		map.put("sampleType", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("inoculateDate", "yyyy-MM-dd");
		map.put("preReapDate", "yyyy-MM-dd");
		//缴费状态
		map.put("chargeNote", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskTemp")
	public void delKaryoGetTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoGetTaskService.delKaryoGetTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KaryoGetTaskService getKaryoGetTaskService() {
		return karyoGetTaskService;
	}

	public void setKaryoGetTaskService(KaryoGetTaskService karyoGetTaskService) {
		this.karyoGetTaskService = karyoGetTaskService;
	}

	public KaryoGetTask getKaryoGetTask() {
		return karyoGetTask;
	}

	public void setKaryoGetTask(KaryoGetTask karyoGetTask) {
		this.karyoGetTask = karyoGetTask;
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskReagentOne")
	public void delKaryoGetTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoGetTaskService.delKaryoGetTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskTemplateOne")
	public void delKaryoGetTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoGetTaskService.delKaryoGetTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoGetTaskCosOne")
	public void delKaryoGetTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoGetTaskService.delKaryoGetTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.karyoGetTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	  /**
		 * 保存样本收获明细
		 * @throws Exception
		 */
		@Action(value = "saveKaryoGetTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveKaryoGetTaskItem() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				KaryoGetTask sc=commonDAO.get(KaryoGetTask.class, id);
				Map aMap = new HashMap();
				aMap.put("karyoGetTaskItem",
						itemDataJson);
				if(sc!=null){
					karyoGetTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
		 /**
		 * 保存收获结果
		 * @throws Exception
		 */
		@Action(value = "saveKaryoGetTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveKaryoGetTaskResult() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				KaryoGetTask sc=commonDAO.get(KaryoGetTask.class, id);
				Map aMap = new HashMap();
				aMap.put("karyoGetTaskResult",
						itemDataJson);
				if(sc!=null){
					karyoGetTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}

}

