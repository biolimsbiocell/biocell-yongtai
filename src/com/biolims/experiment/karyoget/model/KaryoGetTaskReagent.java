package com.biolims.experiment.karyoget.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2016-05-30 10:16:23
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYO_GET_TASK_REAGENT")
@SuppressWarnings("serial")
public class KaryoGetTaskReagent extends EntityDao<KaryoGetTaskReagent> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**试剂编号*/
	private String agentiaCode;
	/**试剂名称*/
	private String agentiaName;
	/**批次*/
	private Double batch;
	/**是否检验*/
	private String isCheck;
	/**单个用量*/
	private Double singleDosage;
	/**反应用量*/
	private Double reactionDosage;
	/**用量*/
	private Double dosage;
	/**样本数量*/
	private String sampleNum;
	/**相关主表*/
	private KaryoGetTask karyoGetTask;
	/**备注*/
	private String note;
	/**关联步骤的id*/
	private String itemId;
	/**实验模板主数据试剂id*/
	private String tReagent;
	/**sn*/
	private String sn;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  试剂编号
	 */
	@Column(name ="AGENTIA_CODE", length = 50)
	public String getAgentiaCode(){
		return this.agentiaCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  试剂编号
	 */
	public void setAgentiaCode(String agentiaCode){
		this.agentiaCode = agentiaCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  试剂名称
	 */
	@Column(name ="AGENTIA_NAME", length = 50)
	public String getAgentiaName(){
		return this.agentiaName;
	}
	/**
	 *方法: 设置String
	 *@param: String  试剂名称
	 */
	public void setAgentiaName(String agentiaName){
		this.agentiaName = agentiaName;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  批次
	 */
	@Column(name ="BATCH", length = 50)
	public Double getBatch(){
		return this.batch;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  批次
	 */
	public void setBatch(Double batch){
		this.batch = batch;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否检验
	 */
	@Column(name ="IS_CHECK", length = 50)
	public String getIsCheck(){
		return this.isCheck;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否检验
	 */
	public void setIsCheck(String isCheck){
		this.isCheck = isCheck;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  单个用量
	 */
	@Column(name ="SINGLE_DOSAGE", length = 50)
	public Double getSingleDosage(){
		return this.singleDosage;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  单个用量
	 */
	public void setSingleDosage(Double singleDosage){
		this.singleDosage = singleDosage;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  反应用量
	 */
	@Column(name ="REACTION_DOSAGE", length = 50)
	public Double getReactionDosage(){
		return this.reactionDosage;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  反应用量
	 */
	public void setReactionDosage(Double reactionDosage){
		this.reactionDosage = reactionDosage;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  用量
	 */
	@Column(name ="DOSAGE", length = 50)
	public Double getDosage(){
		return this.dosage;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  用量
	 */
	public void setDosage(Double dosage){
		this.dosage = dosage;
	}
	/**
	 *方法: 取得KaryoGetTask
	 *@return: KaryoGetTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KARYO_GET_TASK")
	public KaryoGetTask getKaryoGetTask(){
		return this.karyoGetTask;
	}
	/**
	 *方法: 设置KaryoGetTask
	 *@param: KaryoGetTask  相关主表
	 */
	public void setKaryoGetTask(KaryoGetTask karyoGetTask){
		this.karyoGetTask = karyoGetTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String gettReagent() {
		return tReagent;
	}
	public void settReagent(String tReagent) {
		this.tReagent = tReagent;
	}
}