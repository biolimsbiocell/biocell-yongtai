package com.biolims.experiment.karyoget.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 设备明细
 * @author lims-platform
 * @date 2016-05-30 10:16:25
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYO_GET_TASK_COS")
@SuppressWarnings("serial")
public class KaryoGetTaskCos extends EntityDao<KaryoGetTaskCos> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**设备编号*/
	private String instrumentCode;
	/**设备名称*/
	private String instrumentName;
	/**是否检验*/
	private String isCheck;
	/**温度*/
	private String temperature;
	/**转速*/
	private String speed;
	/**时间*/
	private Date time;
	/**相关主表*/
	private KaryoGetTask karyoGetTask;
	/**备注*/
	private String note;
	/**关联步骤的id*/
	private String itemId;
	/**实验模板主数据设备id*/
	private String tCos;
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  设备编号
	 */
	@Column(name ="INSTRUMENT_CODE", length = 50)
	public String getInstrumentCode(){
		return this.instrumentCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  设备编号
	 */
	public void setInstrumentCode(String instrumentCode){
		this.instrumentCode = instrumentCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  设备名称
	 */
	@Column(name ="INSTRUMENT_NAME", length = 50)
	public String getInstrumentName(){
		return this.instrumentName;
	}
	/**
	 *方法: 设置String
	 *@param: String  设备名称
	 */
	public void setInstrumentName(String instrumentName){
		this.instrumentName = instrumentName;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否检验
	 */
	@Column(name ="IS_CHECK", length = 50)
	public String getIsCheck(){
		return this.isCheck;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否检验
	 */
	public void setIsCheck(String isCheck){
		this.isCheck = isCheck;
	}
	/**
	 *方法: 取得String
	 *@return: String  温度
	 */
	@Column(name ="TEMPERATURE", length = 50)
	public String getTemperature(){
		return this.temperature;
	}
	/**
	 *方法: 设置String
	 *@param: String  温度
	 */
	public void setTemperature(String temperature){
		this.temperature = temperature;
	}
	/**
	 *方法: 取得String
	 *@return: String  转速
	 */
	@Column(name ="SPEED", length = 50)
	public String getSpeed(){
		return this.speed;
	}
	/**
	 *方法: 设置String
	 *@param: String  转速
	 */
	public void setSpeed(String speed){
		this.speed = speed;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  时间
	 */
	@Column(name ="TIME", length = 50)
	public Date getTime(){
		return this.time;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  时间
	 */
	public void setTime(Date time){
		this.time = time;
	}
	/**
	 *方法: 取得KaryoGetTask
	 *@return: KaryoGetTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KARYO_GET_TASK")
	public KaryoGetTask getKaryoGetTask(){
		return this.karyoGetTask;
	}
	/**
	 *方法: 设置KaryoGetTask
	 *@param: KaryoGetTask  相关主表
	 */
	public void setKaryoGetTask(KaryoGetTask karyoGetTask){
		this.karyoGetTask = karyoGetTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}

	public String gettCos() {
		return tCos;
	}

	public void settCos(String tCos) {
		this.tCos = tCos;
	}
	
}