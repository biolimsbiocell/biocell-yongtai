package com.biolims.experiment.karyoget.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 临时表
 * @author lims-platform
 * @date 2016-05-30 10:16:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYO_GET_TASK_TEMP")
@SuppressWarnings("serial")
public class KaryoGetTaskTemp extends EntityDao<KaryoGetTaskTemp> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	/**患者姓名*/
	private String patientName;
	/**检测项目编号*/
	private String productId;
	/**检测项目名称*/
	private String productName;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**任务单*/
	private String orderId;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/** 样本类型 */
	private String sampleType;
	/**接种时间*/
	private Date inoculateDate;
	/**预计收获时间*/
	private Date preReapDate;
	/**缴费状态*/
	private String chargeNote;

	public String getChargeNote() {
		return chargeNote;
	}

	public void setChargeNote(String chargeNote) {
		this.chargeNote = chargeNote;
	}

	public Date getInoculateDate() {
		return inoculateDate;
	}

	public void setInoculateDate(Date inoculateDate) {
		this.inoculateDate = inoculateDate;
	}

	public Date getPreReapDate() {
		return preReapDate;
	}

	public void setPreReapDate(Date preReapDate) {
		this.preReapDate = preReapDate;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  患者姓名
	 */
	@Column(name ="PATIENT_NAME", length = 50)
	public String getPatientName(){
		return this.patientName;
	}
	/**
	 *方法: 设置String
	 *@param: String  患者姓名
	 */
	public void setPatientName(String patientName){
		this.patientName = patientName;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目编号
	 */
	@Column(name ="PRODUCT_ID", length = 50)
	public String getProductId(){
		return this.productId;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目编号
	 */
	public void setProductId(String productId){
		this.productId = productId;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目名称
	 */
	@Column(name ="PRODUCT_NAME", length = 50)
	public String getProductName(){
		return this.productName;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目名称
	 */
	public void setProductName(String productName){
		this.productName = productName;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="ACCEPT_DATE", length = 50)
	public Date getAcceptDate(){
		return this.acceptDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  接收日期
	 */
	public void setAcceptDate(Date acceptDate){
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  应出报告日期
	 */
	@Column(name ="REPORT_DATE", length = 50)
	public Date getReportDate(){
		return this.reportDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  应出报告日期
	 */
	public void setReportDate(Date reportDate){
		this.reportDate = reportDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  任务单
	 */
	@Column(name ="ORDER_ID", length = 50)
	public String getOrderId(){
		return this.orderId;
	}
	/**
	 *方法: 设置String
	 *@param: String  任务单
	 */
	public void setOrderId(String orderId){
		this.orderId = orderId;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	
}