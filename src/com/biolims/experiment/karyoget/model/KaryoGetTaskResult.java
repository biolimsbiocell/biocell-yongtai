package com.biolims.experiment.karyoget.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
/**   
 * @Title: Model
 * @Description: 样本收获结果
 * @author lims-platform
 * @date 2016-05-30 10:16:26
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYO_GET_TASK_RESULT")
@SuppressWarnings("serial")
public class KaryoGetTaskResult extends EntityDao<KaryoGetTaskResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**样本编号*/
	private String code;
	/**实验编号*/
	private String experimentCode;
	/**原始样本编号*/
	private String sampleCode;
	/**质检样本编号*/
	private String zjCode;
	/**接种时间*/
	private Date inoculateDate;
	/**预计收获时间*/
	private Date preReapDate;
	/**收获时间*/
	private Date harvestDate;
	/**收货人*/
	private String harversUser;
	/**下一步流向ID*/
	private String nextFlowId;
	/**下一步流向*/
	private String nextFlow;
	/**是否合格*/
	private String isrStandard;
	/**是否提交*/
	private String isCommit;
	/**相关主表*/
	private KaryoGetTask karyoGetTask;
	/**备注*/
	private String note;
	/**临时表id */
	private String tempId;
	/**状态 */
	private String state;
	/** 任务单Id */
	private String orderId;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/**样本类型*/
	private String sampleType;
	/**检测项目编号*/
	private String productId;
	/**检测项目名称*/
	private String productName;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  实验编号
	 */
	@Column(name ="EXPERIMENT_CODE", length = 50)
	public String getExperimentCode(){
		return this.experimentCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  实验编号
	 */
	public void setExperimentCode(String experimentCode){
		this.experimentCode = experimentCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  收获时间
	 */
	@Column(name ="HARVEST_DATE", length = 50)
	public Date getHarvestDate(){
		return this.harvestDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  收获时间
	 */
	public void setHarvestDate(Date harvestDate){
		this.harvestDate = harvestDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  收货人
	 */
	@Column(name ="HARVERS_USER", length = 50)
	public String getHarversUser(){
		return this.harversUser;
	}
	/**
	 *方法: 设置String
	 *@param: String  收货人
	 */
	public void setHarversUser(String harversUser){
		this.harversUser = harversUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="ISR_STANDARD", length = 50)
	public String getIsrStandard(){
		return this.isrStandard;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsrStandard(String isrStandard){
		this.isrStandard = isrStandard;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否提交
	 */
	@Column(name ="IS_COMMIT", length = 50)
	public String getIsCommit(){
		return this.isCommit;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否提交
	 */
	public void setIsCommit(String isCommit){
		this.isCommit = isCommit;
	}
	/**
	 *方法: 取得KaryoGetTask
	 *@return: KaryoGetTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KARYO_GET_TASK")
	public KaryoGetTask getKaryoGetTask(){
		return this.karyoGetTask;
	}
	/**
	 *方法: 设置KaryoGetTask
	 *@param: KaryoGetTask  相关主表
	 */
	public void setKaryoGetTask(KaryoGetTask karyoGetTask){
		this.karyoGetTask = karyoGetTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}
	public String getTempId() {
		return tempId;
	}
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	public String getNextFlowId() {
		return nextFlowId;
	}
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getZjCode() {
		return zjCode;
	}
	public void setZjCode(String zjCode) {
		this.zjCode = zjCode;
	}
	public Date getInoculateDate() {
		return inoculateDate;
	}
	public void setInoculateDate(Date inoculateDate) {
		this.inoculateDate = inoculateDate;
	}
	public Date getPreReapDate() {
		return preReapDate;
	}
	public void setPreReapDate(Date preReapDate) {
		this.preReapDate = preReapDate;
	}
}