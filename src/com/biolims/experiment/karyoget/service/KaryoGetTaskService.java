package com.biolims.experiment.karyoget.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemp;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoget.dao.KaryoGetTaskDao;
import com.biolims.experiment.karyoget.model.KaryoGetTask;
import com.biolims.experiment.karyoget.model.KaryoGetTaskCos;
import com.biolims.experiment.karyoget.model.KaryoGetTaskItem;
import com.biolims.experiment.karyoget.model.KaryoGetTaskReagent;
import com.biolims.experiment.karyoget.model.KaryoGetTaskResult;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemplate;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemp;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KaryoGetTaskService {
	@Resource
	private KaryoGetTaskDao karyoGetTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findKaryoGetTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return karyoGetTaskDao.selectKaryoGetTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryoGetTask i) throws Exception {

		karyoGetTaskDao.saveOrUpdate(i);

	}
	public KaryoGetTask get(String id) {
		KaryoGetTask karyoGetTask = commonDAO.get(KaryoGetTask.class, id);
		return karyoGetTask;
	}
	public Map<String, Object> findKaryoGetTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoGetTaskDao.selectKaryoGetTaskItemList(scId, startNum, limitNum, dir, sort);
		List<KaryoGetTaskItem> list = (List<KaryoGetTaskItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoGetTaskTemplateList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoGetTaskDao.selectKaryoGetTaskTemplateList(scId, startNum, limitNum, dir, sort);
		List<KaryoGetTaskTemplate> list = (List<KaryoGetTaskTemplate>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoGetTaskReagentList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoGetTaskDao.selectKaryoGetTaskAgentiaList(scId, startNum, limitNum, dir, sort);
		List<KaryoGetTaskReagent> list = (List<KaryoGetTaskReagent>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoGetTaskCosList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoGetTaskDao.selectKaryoGetTaskCosList(scId, startNum, limitNum, dir, sort);
		List<KaryoGetTaskCos> list = (List<KaryoGetTaskCos>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoGetTaskResultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoGetTaskDao.selectKaryoGetTaskHarvestList(scId, startNum, limitNum, dir, sort);
		List<KaryoGetTaskResult> list = (List<KaryoGetTaskResult>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoGetTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		
		Map<String, Object> result = karyoGetTaskDao.selectKaryoGetTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoGetTaskTemp> list = (List<KaryoGetTaskTemp>) result.get("list");
		
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<KaryoGetTaskTemp> list2=new ArrayList<KaryoGetTaskTemp>();
		for(KaryoGetTaskTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				t.setChargeNote(s.getSampleOrder().getChargeNote());
			}			
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoGetTaskItem(KaryoGetTask sc, String itemDataJson) throws Exception {
		List<KaryoGetTaskItem> saveItems = new ArrayList<KaryoGetTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoGetTaskItem scp = new KaryoGetTaskItem();
			// 将map信息读入实体类
			scp = (KaryoGetTaskItem) karyoGetTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoGetTask(sc);
			saveItems.add(scp);
			
			// 改变左侧样本状态
			KaryoGetTaskTemp dt = commonDAO.get(KaryoGetTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
			commonDAO.saveOrUpdate(dt);
			/*if(scp!=null){
				Template t=commonDAO.get(Template.class, sc.getTemplate().getId());
				//DicSampleType d=new DicSampleType();
				if(t!=null){
					if(t.getDicSampleType()!=null){
						DicSampleType d=commonDAO.get(DicSampleType.class,
								t.getDicSampleType().getId());
						if(scp.getDicSampleType()==null || 
								(scp.getDicSampleType()!=null && scp.getDicSampleType().equals(""))){
							if(d!=null){
								scp.setDicSampleType(d);
							}
						}
					}
					if(t.getProductNum()!=null){
						if(scp.getProductNum()==null || 
								(scp.getProductNum()!=null && scp.getProductNum().equals(""))){
							scp.setProductNum(t.getProductNum());
						}
					}
				}
			}*/
		}
		karyoGetTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoGetTaskItem scp =  karyoGetTaskDao.get(KaryoGetTaskItem.class, id);
			 karyoGetTaskDao.delete(scp);
			 
			// 改变左侧样本状态
			KaryoGetTaskTemp dt = commonDAO.get(KaryoGetTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.saveOrUpdate(dt);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoGetTaskTemplate(KaryoGetTask sc, String itemDataJson) throws Exception {
		List<KaryoGetTaskTemplate> saveItems = new ArrayList<KaryoGetTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoGetTaskTemplate scp = new KaryoGetTaskTemplate();
			// 将map信息读入实体类
			scp = (KaryoGetTaskTemplate) karyoGetTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoGetTask(sc);

			saveItems.add(scp);
		}
		karyoGetTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoGetTaskTemplate scp =  karyoGetTaskDao.get(KaryoGetTaskTemplate.class, id);
			 karyoGetTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoGetTaskReagent(KaryoGetTask sc, String itemDataJson) throws Exception {
		List<KaryoGetTaskReagent> saveItems = new ArrayList<KaryoGetTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoGetTaskReagent scp = new KaryoGetTaskReagent();
			// 将map信息读入实体类
			scp = (KaryoGetTaskReagent) karyoGetTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoGetTask(sc);

			saveItems.add(scp);
		}
		karyoGetTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoGetTaskReagent scp =  karyoGetTaskDao.get(KaryoGetTaskReagent.class, id);
			 karyoGetTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoGetTaskCos(KaryoGetTask sc, String itemDataJson) throws Exception {
		List<KaryoGetTaskCos> saveItems = new ArrayList<KaryoGetTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoGetTaskCos scp = new KaryoGetTaskCos();
			// 将map信息读入实体类
			scp = (KaryoGetTaskCos) karyoGetTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoGetTask(sc);

			saveItems.add(scp);
		}
		karyoGetTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoGetTaskCos scp =  karyoGetTaskDao.get(KaryoGetTaskCos.class, id);
			 karyoGetTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoGetTaskResult(KaryoGetTask sc, String itemDataJson) throws Exception {
		List<KaryoGetTaskResult> saveItems = new ArrayList<KaryoGetTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> map : list) {
			KaryoGetTaskResult scp = new KaryoGetTaskResult();
			// 将map信息读入实体类
			scp = (KaryoGetTaskResult) karyoGetTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoGetTask(sc);

			/*if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						&& !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());

					markCode = scp.getSampleCode() + d.getCode();
				}else{
					markCode = scp.getSampleCode();
				}
				String code = codingRuleService.getCode("KaryoGetTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);
			}*/
			saveItems.add(scp);
			karyoGetTaskDao.saveOrUpdate(scp);
			if (scp != null) {
				if (scp.getIsrStandard() != null && scp.getIsCommit() != null &&
						!scp.getIsrStandard().equals("") && !scp.getIsCommit().equals("") ){
					if(scp.getIsCommit().equals("1")){
						if(scp.getIsrStandard().equals("1")){
							if(scp.getNextFlowId()!=null && !scp.getNextFlowId().equals("")){
								if (scp.getNextFlowId().equals("0009")) {// 样本入库
									SampleInItemTemp st = new SampleInItemTemp();
									st.setCode(scp.getCode());
									st.setSampleCode(scp.getSampleCode());
									st.setState("1");
									commonDAO.saveOrUpdate(st);
									// 入库，改变SampleInfo中原始样本的状态为“待入库”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
									}
								} else if (scp.getNextFlowId().equals("0012")) {// 暂停
									// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
									}
								} else if (scp.getNextFlowId().equals("0013")) {// 终止
									// 终止，改变SampleInfo中原始样本的状态为“实验终止”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
									}
								}else {
									// 得到下一步流向的相关表单
									List<NextFlow> list_nextFlow = nextFlowDao
											.seletNextFlowById(scp.getNextFlowId());
									for (NextFlow n : list_nextFlow) {
										Object o = Class.forName(
												n.getApplicationTypeTable()
														.getClassPath())
												.newInstance();
										scp.setState("1");
										sampleInputService.copy(o, scp);
									}
								}
							}
						}else{
							KaryoAbnormal ka=new KaryoAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setOrderId(scp.getOrderId());
							ka.setAcceptDate(scp.getAcceptDate());
							ka.setReportDate(scp.getReportDate());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getKaryoGetTask().getId());
							ka.setNote(scp.getNote());
							ka.setTaskName("样本收获");
							ka.setState("1");
							
							commonDAO.saveOrUpdate(ka);
						}
						sampleStateService
						.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"KaryoGetTask",
							"样本收获",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(),
							scp.getIsrStandard(), null, null, null,
							null, null, null, null, null);
					}
				}
			}
		}
		karyoGetTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoGetTaskResult scp =  karyoGetTaskDao.get(KaryoGetTaskResult.class, id);
			 karyoGetTaskDao.delete(scp);
		}
	}
	
	
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoGetTaskTemp scp =  karyoGetTaskDao.get(KaryoGetTaskTemp.class, id);
			 karyoGetTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryoGetTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			karyoGetTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("karyoGetTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoGetTaskItem(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoGetTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoGetTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoGetTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoGetTaskReagent(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoGetTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoGetTaskCos(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoGetTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoGetTaskResult(sc, jsonStr);
			}
			List<KaryoGetTaskItem> snlist=karyoGetTaskDao.getItem(sc.getId());
			List<KaryoGetTaskReagent> rglist=karyoGetTaskDao.getReagent(sc.getId());
			 DecimalFormat df = new DecimalFormat("#.00");
			 Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(KaryoGetTaskReagent sr:rglist){
						sr.setSampleNum(String.valueOf(snlist.size()));
						if(sr.getSingleDosage()!=null && 
								sr.getSampleNum()!=null){
							nums =Double.valueOf(df.format(sr.getSingleDosage()*snlist.size()));
							sr.setDosage(nums);
						}
					}
				}
			}
		}
   }
	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		KaryoGetTask kt = karyoGetTaskDao.get(KaryoGetTask.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stime = format.format(date);
		kt.setConfirmDate(stime);
		karyoGetTaskDao.update(kt);
		
		//完成后改变左侧表的状态
		List<KaryoGetTaskResult> list=karyoGetTaskDao
				.getKaryoGetTaskResultById(kt.getId());
		if(list.size()>0){
			for(KaryoGetTaskResult k:list){
				KaryoGetTaskTemp tt=commonDAO
						.get(KaryoGetTaskTemp.class, k.getTempId());
				if(tt!=null){
					tt.setState("2");
				}
			}
		}
		submitSample(id,null);
	}
	
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskReagentOne(String ids) throws Exception {
		KaryoGetTaskReagent scp =  karyoGetTaskDao.get(KaryoGetTaskReagent.class, ids);
		karyoGetTaskDao.delete(scp);
	}
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskTemplateOne(String ids) throws Exception {
		KaryoGetTaskTemplate scp =  karyoGetTaskDao.get(KaryoGetTaskTemplate.class, ids);
		karyoGetTaskDao.delete(scp);
	}
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoGetTaskCosOne(String ids) throws Exception {
		KaryoGetTaskCos scp =  karyoGetTaskDao.get(KaryoGetTaskCos.class, ids);
		karyoGetTaskDao.delete(scp);
	}
	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		KaryoGetTask sc = this.karyoGetTaskDao.get(KaryoGetTask.class, id);
		// 获取结果表样本信息

		List<KaryoGetTaskResult> list;
		if (ids == null)
			list = this.karyoGetTaskDao.setResultById(id);
		else
			list = this.karyoGetTaskDao.setResultByIds(ids);
		for (KaryoGetTaskResult scp : list) {
			if (scp != null) {
				if (scp.getIsrStandard() != null && (scp.getIsCommit() == null||"".equals(scp.getIsCommit()))) {
					String isOk = scp.getIsrStandard();
					if (isOk.equals("1")) {
						KaryoShipTaskTemp k = new KaryoShipTaskTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
//							k.setChipNum(scp.getChipNum());
						k.setState("1");
						k.setAcceptDate(scp.getAcceptDate());
						k.setHarvestDate(scp.getHarvestDate());
						k.setInoculateDate(scp.getInoculateDate());
						karyoGetTaskDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(sc.getCreateDate()),
										format.format(new Date()),
										"KaryoGetTask",
										"样本收获",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "样本制片",
										scp.getIsrStandard(), null, null, null,
										null, null, null, null, null);
						scp.setIsCommit("1");
						karyoGetTaskDao.saveOrUpdate(scp);
					} else {
						
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("样本收获");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getKaryoGetTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
								// 不合格的到异常
								KaryoAbnormal ka = new KaryoAbnormal();
								ka.setCode(scp.getCode());
								ka.setSampleCode(scp.getSampleCode());
								ka.setProductId(scp.getProductId());
								ka.setProductName(scp.getProductName());
								ka.setSampleType(scp.getSampleType());
								ka.setTaskId(scp.getKaryoGetTask().getId());
								ka.setNote(scp.getNote());
								ka.setTaskName("样本收获");
								ka.setState("1");
								karyoGetTaskDao.saveOrUpdate(ka);
						}
						scp.setIsCommit("1");
					}
				}
			}
		}
	}
}




