package com.biolims.experiment.wk.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.wk.service.WkTaskService;

public class WkTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkTaskService mbService = (WkTaskService) ctx
				.getBean("wkTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
