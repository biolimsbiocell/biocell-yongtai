﻿package com.biolims.experiment.wk.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.wk.dao.WkTaskDao;
import com.biolims.experiment.wk.model.WkTask;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.experiment.wk.model.WkTaskItem;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.wk.service.WkTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
@Namespace("/experiment/wk/wkTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private WkTaskService wkTaskService;
	private WkTask wkTask = new WkTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WkTaskDao wkTaskDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	/**
	 * 
	 * @Title: showWkTaskList
	 * @Description:展示主表
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showWkTaskTable")
	public String showWkTaskTable() throws Exception {
		rightsId="240303";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wk/wkTask.jsp");
	}

	@Action(value = "showWkTaskTableJson")
	public void showWkTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = wkTaskService.findWkTaskTable(
					start, length, query, col, sort);
			List<WkTask> list = (List<WkTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: editWkTask
	 * @Description: 新建实验单
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editWkTask")
	public String editWkTask() throws Exception {
		rightsId="240302";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			wkTask = wkTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
			if (wkTask.getMaxNum() == null) {
				wkTask.setMaxNum(0);
			}
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			wkTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wkTask.setCreateUser(user);
			wkTask.setMaxNum(0);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wkTask.setCreateDate(stime);
			wkTask
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wkTask
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<Template> templateList = templateService
				.showDialogTemplateTableJson("WkTask",null);
		List<Template> selTemplate = new ArrayList<Template>();
		List<UserGroupUser> userList = new ArrayList<UserGroupUser>();
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int j = 0; j < templateList.size(); j++) {
			if(templateList.get(j).getAcceptUser()!=null){
				List<UserGroupUser> userTempList = (List<UserGroupUser>) userGroupUserService.getUserGroupUserBygroupId(templateList.get(j).getAcceptUser().getId())
						.get("list");
				for(UserGroupUser ugu:userTempList){
					if(!userList.contains(ugu)){
						userList.add(ugu);
					}
				}
			}
			if (wkTask.getTemplate() != null) {
				if (wkTask.getTemplate().getId()
						.equals(templateList.get(j).getId())) {
					selTemplate.add(templateList.get(j));
					templateList.remove(j);
					j--;
				}
			}
		}
		for (int i = 0; i < userList.size(); i++) {
			if (wkTask.getTestUserOneId() != null) {
				String [] userOne=wkTask.getTestUserOneId().split(",");
				one:for(String u:userOne){
					if(u.equals(userList.get(i).getUser().getId())){
						selUser.add(userList.get(i));
						userList.remove(i);
						i--;
						break one;
					}
				}
			}
		}
		putObjToContext("template", templateList);
		putObjToContext("selTemplate", selTemplate);
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		toState(wkTask.getState());
		return dispatcher("/WEB-INF/page/experiment/wk/wkTaskAllot.jsp");
	}

	/**
	 * 
	 * @Title: showWkTaskItemTable
	 * @Description: 展示待排板列表
	 * @author :
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showWkTaskItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkTaskItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		wkTask = wkTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/wk/wkTaskMakeUp.jsp");
	}

	@Action(value = "showWkTaskItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkTaskItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkTaskService
					.findWkTaskItemTable(scId, start, length, query, col,
							sort);
			List<WkTaskItem> list = (List<WkTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("chromosomalLocation", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showWkTaskItemAfTableJson
	 * @Description: 排板后样本展示
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */

	@Action(value = "showWkTaskItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkTaskItemAfTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkTaskService
					.findWkTaskItemAfTable(scId, start, length, query, col,
							sort);
			List<WkTaskItem> list = (List<WkTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("wkTask-name", "");
			map.put("wkTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("chromosomalLocation", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("color", "");
			map.put("indexa", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delWkTaskItem
	 * @Description: 删除待排板样本
	 * @author
	 * @date
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delWkTaskItem")
	public void delWkTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr=getParameterFromRequest("del");
		String id=getParameterFromRequest("id");
		User user =(User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkTaskService.delWkTaskItem(delStr,ids,user,id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: delWkTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delWkTaskItemAf")
	public void delWkTaskItemAf() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkTaskService.delWkTaskItemAf(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBWkTaskResultTable
	 * @Description
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showWkTaskResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkTaskResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		wkTask = wkTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/wk/wkTaskResult.jsp");
	}

	@Action(value = "showWkTaskResultTableJson")
	public void showWkTaskResultTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = wkTaskService
					.showWkTaskResultTableJson(id, start, length, query,
							col, sort);
			List<WkTaskInfo> list = (List<WkTaskInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("note", "");
			map.put("wkTask-id", "");
			map.put("wkTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");
			map.put("sumTotal", "");
			map.put("insertSize", "");
			map.put("indexa", "");
			map.put("patientName", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: delWkTaskResult
	 * @Description: 删除结果明细
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delWkTaskResult")
	public void delWkTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkTaskService.delWkTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showWkTaskSteps
	 * @Description: 实验步骤
	 * @author : 
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showWkTaskSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkTaskSteps() throws Exception {
		String id = getParameterFromRequest("id");
		wkTask = wkTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/wk/wkTaskSteps.jsp");
	}

	@Action(value = "showWkTaskStepsJson")
	public void showWkTaskStepsJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = wkTaskService.showWkTaskStepsJson(id, orderNum);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkTaskReagent")
	public void delWkTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			wkTaskService.delWkTaskReagent(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkTaskCos")
	public void delWkTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			wkTaskService.delWkTaskCos(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: makeCode
	 * @Description: 打印条码
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String id = getParameterFromRequest("id");
		String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
		CodeMain codeMain = null;
		codeMain = codeMainService.get(id);
		if (codeMain != null) {
			String printStr = "";
			String context="";
			for(int a=0;a<sampleCode.length;a++){
			String codeFull = sampleCode[a];
			String name= "";
			//sampleReceiveService.getNameBySampleCode(codeFull);
			printStr = codeMain.getCode();
			String code1 = sampleCode[a].substring(0, 9);
			String code2 = sampleCode[a].substring(9);
			printStr = printStr.replaceAll("@@code1@@", code1);
			printStr = printStr.replaceAll("@@code2@@", code2);
			printStr = printStr.replaceAll("@@code@@", codeFull);
			printStr = printStr.replaceAll("@@name@@",name );
			context+=printStr;
			}
			String ip = codeMain.getIp();
			Socket socket = null;
			OutputStream os;
			try {
				System.out.println(context);
				socket = new Socket();
				SocketAddress sa = new InetSocketAddress(ip, 9100);
				socket.connect(sa);
				os = socket.getOutputStream();
				os.write(context.getBytes("UTF-8"));
				os.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @Title: showWkTaskFromReceiveList
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showWkTaskTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkTaskFromReceiveList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/wk/wkTaskTemp.jsp");
	}

	@Action(value = "showWkTaskTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkTaskTempTableJson() throws Exception {
		String[] counts = getRequest().getParameterValues("counts[]");		
		String [] codes=getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = wkTaskService
					.selectWkTaskTempTable(counts,codes,start, length, query, col, sort);
			List<WkTaskTemp> list = (List<WkTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("counts", "");
			map.put("productName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("od260", "");
			map.put("od230", "");
			map.put("rin", "");
			map.put("qbcontraction", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-project-id", "");
			map.put("techJkServiceTask-id", "");
			map.put("tjItem-project-id", "");
			map.put("orderId", "");
			map.put("posId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-changeType", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}

	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配页面
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "saveAllot")
	public void saveAllot() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = wkTaskService.saveAllot(main, tempId, userId,
					templateId,logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排版界面
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveMakeUp")
	public void saveMakeUp() throws Exception {
		String blood_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wkTaskService.saveMakeUp(blood_id, item,logInfo);
			result.put("success", true);
			result.put("id", blood_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	
	@Action(value = "saveLeftQuality")
	public void saveLeftQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
				WkTask wkTask = wkTaskService.get(id);
				wkTaskService.saveLeftQuality(wkTask,ids,logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	@Action(value = "saveRightQuality")
	public void saveRightQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
				WkTask wkTask = wkTaskService.get(id);
				wkTaskService.saveRightQuality(wkTask,ids,logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveSteps")
	public void saveSteps() throws Exception {
		String id = getParameterFromRequest("id");
		String templateJson = getParameterFromRequest("templateJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String logInfo=getParameterFromRequest("logInfo");
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			wkTaskService.saveSteps(id, templateJson, reagentJson, cosJson,logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveResult")
	public void saveResult() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		String confirmUser=getParameterFromRequest("confirmUser");
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			wkTaskService.saveResult(id, dataJson,logInfo,confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示排版
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<WkTaskItem> json = wkTaskService.showWellPlate(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wkTaskService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateSample")
	public void plateSample() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = wkTaskService.plateSample(id, counts);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description:展示孔板样本列表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plateSampleTable")
	public void plateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = wkTaskService.plateSampleTable(id, counts, start,
					length, query, col, sort);
			List<WkTaskItem> list = (List<WkTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("indexa", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("wkTask-name", "");
			map.put("wkTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: uploadCsvFile
	 * @Description: 上传结果
	 * @author :
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			wkTaskService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "bringResult")
	public void bringResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			wkTaskService.bringResult(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: generateBlendCode
	 * @Description: 生成混合号
	 * @author : shengwei.wang
	 * @date 2018年3月6日上午11:02:26 void
	 * @throws
	 */
	@Action(value = "generateBlendCode")
	public void generateBlendCode() {
		String id = getParameterFromRequest("id");
		try {
			Integer blendCode = wkTaskService.generateBlendCode(id);
			HttpUtils.write(String.valueOf(blendCode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: submitSample  
	 * @Description: 提交样本  
	 * @author : shengwei.wang
	 * @date 2018年3月22日下午5:39:40
	 * void
	 * @throws
	 */
	@Action(value="submitSample")
	public void submitSample() throws Exception{
		String id=getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			wkTaskService.submitSample(id, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: getCounts  
	 * @Description: 获取板号  
	 * @author : shengwei.wang
	 * @date 2018年3月28日上午9:57:23
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "getCounts")
	public void getCounts() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = wkTaskService.getCounts(id);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: setCounts  
	 * @Description: 保存板号  
	 * @author : shengwei.wang
	 * @date 2018年3月28日上午9:25:06
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "setCounts")
	public void setCounts() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			wkTaskService.setCounts(ids, counts);
			map.put("success", true);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "checkCode")
	public void checkCode() throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		String [] codes=getRequest().getParameterValues("codes[]");
		String id=getParameterFromRequest("id");
		try {
		String code=wkTaskService.checkCode(id,codes);
			map.put("success", true);
			map.put("code", code);
		} catch (Exception e) {
			map.put("success",false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	/**
	 * 
	 * @Title: downLoadTemp @Description: TODO(核算提取模板下载) @param @param
	 * ids @param @throws Exception    设定文件 @return void    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-8-22 上午11:46:55 @throws
	 */
	@Action(value = "downLoadTemp")
	public void downLoadTemp() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids");
		String[] codes = getRequest().getParameterValues("codes");
		String id = ids[0];
		String code = codes[0];
		String[] str1 = code.split(",");
		String co = str1[0];
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = co + sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		properties.load(is);

		String filePath = properties.getProperty("result.template.file") + "\\";// 写入csv路径
		String fileName = filePath + a + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "样本编号", "原始样本编号", "产物类型", "检测项目", "浓度", "体积","文库片段大小" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		String[] sid = id.split(",");
		for (int j = 0; j < sid.length; j++) {
			String idc = sid[j];
			for (int i = 0; i < ids.length; i++) {
				WkTaskInfo sr=wkTaskService.getInfoById(idc);
				StringBuffer sb = new StringBuffer();
				setMolecularMarkersData(sr, sb);
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
				if (sr.equals("")) {
					result.put("success", false);
				} else {
					result.put("success", true);

				}
			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3(a, filePath);
	}

	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData @Description: TODO(将对应的值添加到模板里) @param @param
	 * sr @param @param sb @param @throws Exception    设定文件 @return void   
	 * 返回类型 @author zhiqiang.yang@biolims.cn @date 2017-8-22 下午1:21:58 @throws
	 */
	public void setMolecularMarkersData(WkTaskInfo sr, StringBuffer sb) throws Exception {
		if (null!=sr.getCode()) {
			sb.append("\"").append(sr.getCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null!=sr.getSampleCode()) {
			sb.append("\"").append(sr.getSampleCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		
		if (null!=sr.getDicSampleType().getName()) {
			sb.append("\"").append(sr.getDicSampleType().getName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		
		if (null!=sr.getProductName()) {
			sb.append("\"").append(sr.getProductName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
	}

	/**
	 * 
	 * @Title: toUtf8String @Description: TODO(解决乱码) @param @param
	 * s @param @return    设定文件 @return String    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-8-23 下午4:40:07 @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}
	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WkTaskService getWkTaskService() {
		return wkTaskService;
	}

	public void setWkTaskService(WkTaskService wkTaskService) {
		this.wkTaskService = wkTaskService;
	}

	public WkTask getWkTask() {
		return wkTask;
	}

	public void setWkTask(WkTask wkTask) {
		this.wkTask = wkTask;
	}

	public WkTaskDao getWkTaskDao() {
		return wkTaskDao;
	}

	public void setWkTaskDao(WkTaskDao wkTaskDao) {
		this.wkTaskDao = wkTaskDao;
	}

}
