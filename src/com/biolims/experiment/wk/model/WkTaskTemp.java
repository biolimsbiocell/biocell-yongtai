package com.biolims.experiment.wk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 待文库构建样本接收明细
 * @author lims-platform
 * @date 2015-11-27 15:41:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_TASK_TEMP")
@SuppressWarnings("serial")
public class WkTaskTemp extends EntityDao<WkTaskTemp> implements
		java.io.Serializable {
	
	/** 编码 */
	private String id;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本编号 */
	private String code;
	/** 患者姓名 */
	private String patientName;
	/** 备注 */
	private String note;
	/** 状态ID */
	private String state;
	/** 状态 */
	private String stateName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 浓度 */
	private Double concentration;
	/** 体积 */
	private Double volume;
	/** 任务单id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 临床 1 科研 2 */
	private String classify;
	/** 样本数量 */
	private String sampleNum;
	/** 样本类型 */
	private String sampleType;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/** od260/280 */
	private Double od260;
	/** od260/230 */
	private Double od230;
	/** rin */
	private Double rin;
	/** Qubit浓度 */
	private Double qbcontraction;
	/** 位置 */
	private String posId;
	/** 板号 */
	private String counts;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/**关联订单*/
	private SampleOrder sampleOrder;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/** 
	 * @return orderCode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getOrderCode() {
		return orderCode;
	}

	/**
	 * @param orderCode the orderCode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * @return the posId
	 */
	public String getPosId() {
		return posId;
	}

	/**
	 * @param posId the posId to set
	 */
	public void setPosId(String posId) {
		this.posId = posId;
	}

	/**
	 * @return the counts
	 */
	public String getCounts() {
		return counts;
	}

	/**
	 * @param counts the counts to set
	 */
	public void setCounts(String counts) {
		this.counts = counts;
	}

	/**
	 * @return the od260
	 */
	public Double getOd260() {
		return od260;
	}

	/**
	 * @param od260 the od260 to set
	 */
	public void setOd260(Double od260) {
		this.od260 = od260;
	}

	/**
	 * @return the od230
	 */
	public Double getOd230() {
		return od230;
	}

	/**
	 * @param od230 the od230 to set
	 */
	public void setOd230(Double od230) {
		this.od230 = od230;
	}

	/**
	 * @return the rin
	 */
	public Double getRin() {
		return rin;
	}

	/**
	 * @param rin the rin to set
	 */
	public void setRin(Double rin) {
		this.rin = rin;
	}

	/**
	 * @return the qbcontraction
	 */
	public Double getQbcontraction() {
		return qbcontraction;
	}

	/**
	 * @param qbcontraction the qbcontraction to set
	 */
	public void setQbcontraction(Double qbcontraction) {
		this.qbcontraction = qbcontraction;
	}
	
}