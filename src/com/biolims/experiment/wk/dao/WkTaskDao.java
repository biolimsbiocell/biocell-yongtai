package com.biolims.experiment.wk.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.wk.model.WkTask;
import com.biolims.experiment.wk.model.WkTaskCos;
import com.biolims.experiment.wk.model.WkTaskItem;
import com.biolims.experiment.wk.model.WkTaskReagent;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemplate;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class WkTaskDao extends BaseHibernateDao {

	public Map<String, Object> findWkTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectWkTaskTempTable(String[] counts, String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from WkTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if (codes != null && !codes.equals("")) {
				String cc = "";
				for (int i = 0; i < codes.length; i++) {
					if (i == codes.length - 1) {
						cc += "'" + codes[i] + "'";
					} else {
						cc += "'" + codes[i] + "',";
					}
				}
				key += " and code in (" + cc + ")";

			}
			if (counts != null && !counts.equals("")) {

				String ct = "";
				for (int i = 0; i < counts.length; i++) {
					if (i == counts.length - 1) {
						ct += "'" + counts[i] + "'";
					} else {
						ct += "'" + counts[i] + "',";
					}
				}
				key += " and counts in (" + ct + ")";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<WkTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from WkTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findWkTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkTaskItem where 1=1 and state='1' and wkTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkTaskItem  where 1=1 and state='1' and wkTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<WkTaskItem> showWellPlate(String id) {
		String hql="from WkTaskItem  where 1=1 and state='2' and  wkTask.id='"+id+"'";
		List<WkTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findWkTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkTaskItem where 1=1 and state='2' and wkTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkTaskItem  where 1=1 and state='2' and wkTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from WkTaskItem where 1=1 and state='2' and wkTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from WkTaskItem where 1=1 and state='2' and wkTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<WkTaskTemplate> showWkTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from WkTaskTemplate where 1=1 and wkTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<WkTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from WkTaskTemplate where 1=1 and wkTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<WkTaskReagent> showWkTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from WkTaskReagent where 1=1 and wkTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<WkTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from WkTaskReagent where 1=1 and wkTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<WkTaskCos> showWkTaskCosJson(String id,String code) {
		String countHql = "select count(*) from WkTaskCos where 1=1 and wkTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<WkTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from WkTaskCos where 1=1 and wkTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<WkTaskTemplate> delTemplateItem(String id) {
		List<WkTaskTemplate> list=new ArrayList<WkTaskTemplate>();
		String hql = "from WkTaskTemplate where 1=1 and wkTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<WkTaskReagent> delReagentItem(String id) {
		List<WkTaskReagent> list=new ArrayList<WkTaskReagent>();
		String hql = "from WkTaskReagent where 1=1 and wkTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<WkTaskCos> delCosItem(String id) {
		List<WkTaskCos> list=new ArrayList<WkTaskCos>();
		String hql = "from WkTaskCos where 1=1 and wkTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showWkTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkTaskInfo where 1=1 and wkTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkTaskInfo  where 1=1 and wkTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from WkTaskItem where 1=1 and wkTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<WkTaskItem> plateSample(String id, String counts) {
		String hql="from WkTaskItem where 1=1 and wkTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<WkTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkTaskItem where 1=1 and wkTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkTaskItem  where 1=1 and wkTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<WkTaskInfo> findWkTaskInfoByCode(String code) {
		String hql="from WkTaskInfo where 1=1 and code='"+code+"'";
		List<WkTaskInfo> list=new ArrayList<WkTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<WkTaskInfo> selectAllResultListById(String code) {
		String hql = "from WkTaskInfo  where (submit is null or submit='') and wkTask.id='"
				+ code + "'";
		List<WkTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<WkTaskInfo> selectResultListById(String id) {
		String hql = "from WkTaskInfo  where wkTask.id='"
				+ id + "'";
		List<WkTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<WkTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from WkTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<WkTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<WkTaskItem> selectWkTaskItemList(String scId)
			throws Exception {
		String hql = "from WkTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkTaskItem> list = new ArrayList<WkTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from WkTaskItem where 1=1 and wkTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public WkTaskInfo getResultByCode(String code) {
			String hql=" from WkTaskInfo where 1=1 and code='"+code+"'";
			WkTaskInfo spi=(WkTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}