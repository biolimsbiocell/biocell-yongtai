package com.biolims.experiment.wk.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class WkTaskAbnormalDao extends BaseHibernateDao {
	public Map<String, Object> showWkTaskAbnormalTableJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkTaskAbnormal where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from WkTaskAbnormal where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<WkTaskAbnormal> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}
}