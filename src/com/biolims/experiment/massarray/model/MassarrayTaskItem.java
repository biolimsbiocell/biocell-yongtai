package com.biolims.experiment.massarray.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 质谱实验明细
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "MASSARRAY_TASK_ITEM")
@SuppressWarnings("serial")
public class MassarrayTaskItem extends EntityDao<MassarrayTaskItem> implements
		java.io.Serializable {

	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 样本编号 */
	private String code;
	/** 样本编号 */
	private String sampleCode;
	/** 患者姓名 */
	private String patientName;
	/** 核对 */
	private String checked;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 染色体位置 */
	private String chromosomalLocation;
	/** 状态  */
	private String state;
	/** 备注 */
	private String note;
	/** 储位 */
	private StoragePosition storage;
	/** 浓度 */
	private Double concentration;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本用量 */
	private Double sampleConsume;
	/** 相关主表 */
	private MassarrayTask massarrayTask;
	/** 体积 */
	private Double volume;
	/** 取样日期 */
	private String inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	/** 任务单id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 排序号 */
	private Integer orderNumber;
	/** 位置 */
	private String posId;
	/** 板号 */
	private String counts;
	
	/** 临床 1 科研 2 */
	private String classify;
	/** 中间产物数量 */
	private String productNum;
	/** 中间产物类型编号 */
	private String dicSampleTypeId;
	/** 中间产物类型 */
	private String dicSampleTypeName;
	/** 样本类型 */
	private String sampleType;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/**混合号 */
	private Integer blendCode;
	
	/**背景色 */
	private String color;
	
	
	/**是否出库 */
	private String isOut;
	
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/** 父级 */
	private String parentId;
	/**关联订单*/
	private SampleOrder sampleOrder;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public Integer getBlendCode() {
		return blendCode;
	}

	public void setBlendCode(Integer blendCode) {
		this.blendCode = blendCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}


	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对
	 */
	@Column(name = "CHECKED", length = 60)
	public String getChecked() {
		return this.checked;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 36)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得Sting
	 * 
	 * @return: Sting 备注
	 */
	@Column(name = "NOTE", length = 36)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得BloodSplit
	 * 
	 * @return: BloodSplit 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MASSARRAY_TASK")
	public MassarrayTask getMassarrayTask() {
		return this.massarrayTask;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setMassarrayTask(MassarrayTask massarrayTask) {
		this.massarrayTask = massarrayTask;
	}


	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得StoragePosition
	 * 
	 * @return: StoragePosition 储位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE")
	public StoragePosition getStorage() {
		return this.storage;
	}

	/**
	 * 方法: 设置StoragePosition
	 * 
	 * @param: StoragePosition 储位
	 */
	public void setStorage(StoragePosition storage) {
		this.storage = storage;
	}

	@Column(name = "CONCENTRATION", length = 100)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}


	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	
	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}


	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}


	public Double getSampleConsume() {
		return sampleConsume;
	}

	public void setSampleConsume(Double sampleConsume) {
		this.sampleConsume = sampleConsume;
	}


	public String getDicSampleTypeId() {
		return dicSampleTypeId;
	}

	public void setDicSampleTypeId(String dicSampleTypeId) {
		this.dicSampleTypeId = dicSampleTypeId;
	}

	public String getDicSampleTypeName() {
		return dicSampleTypeName;
	}

	public void setDicSampleTypeName(String dicSampleTypeName) {
		this.dicSampleTypeName = dicSampleTypeName;
	}


	/**
	 * @return the posId
	 */
	public String getPosId() {
		return posId;
	}

	/**
	 * @param posId the posId to set
	 */
	public void setPosId(String posId) {
		this.posId = posId;
	}

	/**
	 * @return the isOut
	 */
	public String getIsOut() {
		return isOut;
	}

	/**
	 * @param isOut the isOut to set
	 */
	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}
	
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the chromosomalLocation
	 */
	public String getChromosomalLocation() {
		return chromosomalLocation;
	}

	/**
	 * @param chromosomalLocation the chromosomalLocation to set
	 */
	public void setChromosomalLocation(String chromosomalLocation) {
		this.chromosomalLocation = chromosomalLocation;
	}
	
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}