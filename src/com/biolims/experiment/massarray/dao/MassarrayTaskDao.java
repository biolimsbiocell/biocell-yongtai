package com.biolims.experiment.massarray.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.massarray.model.MassarrayTask;
import com.biolims.experiment.massarray.model.MassarrayTaskCos;
import com.biolims.experiment.massarray.model.MassarrayTaskItem;
import com.biolims.experiment.massarray.model.MassarrayTaskReagent;
import com.biolims.experiment.massarray.model.MassarrayTaskTemp;
import com.biolims.experiment.massarray.model.MassarrayTaskTemplate;
import com.biolims.experiment.massarray.model.MassarrayTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class MassarrayTaskDao extends BaseHibernateDao {

	public Map<String, Object> findMassarrayTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MassarrayTask where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MassarrayTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MassarrayTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectMassarrayTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from MassarrayTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<MassarrayTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from MassarrayTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findMassarrayTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MassarrayTaskItem where 1=1 and state='1' and massarrayTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MassarrayTaskItem  where 1=1 and state='1' and massarrayTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MassarrayTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<MassarrayTaskItem> showWellPlate(String id) {
		String hql="from MassarrayTaskItem  where 1=1 and state='2' and  massarrayTask.id='"+id+"'";
		List<MassarrayTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findMassarrayTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MassarrayTaskItem where 1=1 and state='2' and massarrayTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MassarrayTaskItem  where 1=1 and state='2' and massarrayTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MassarrayTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from MassarrayTaskItem where 1=1 and state='2' and massarrayTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from MassarrayTaskItem where 1=1 and state='2' and massarrayTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<MassarrayTaskTemplate> showMassarrayTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from MassarrayTaskTemplate where 1=1 and massarrayTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<MassarrayTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from MassarrayTaskTemplate where 1=1 and massarrayTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<MassarrayTaskReagent> showMassarrayTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from MassarrayTaskReagent where 1=1 and massarrayTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<MassarrayTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from MassarrayTaskReagent where 1=1 and massarrayTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<MassarrayTaskCos> showMassarrayTaskCosJson(String id,String code) {
		String countHql = "select count(*) from MassarrayTaskCos where 1=1 and massarrayTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<MassarrayTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from MassarrayTaskCos where 1=1 and massarrayTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<MassarrayTaskTemplate> delTemplateItem(String id) {
		List<MassarrayTaskTemplate> list=new ArrayList<MassarrayTaskTemplate>();
		String hql = "from MassarrayTaskTemplate where 1=1 and massarrayTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<MassarrayTaskReagent> delReagentItem(String id) {
		List<MassarrayTaskReagent> list=new ArrayList<MassarrayTaskReagent>();
		String hql = "from MassarrayTaskReagent where 1=1 and massarrayTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<MassarrayTaskCos> delCosItem(String id) {
		List<MassarrayTaskCos> list=new ArrayList<MassarrayTaskCos>();
		String hql = "from MassarrayTaskCos where 1=1 and massarrayTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showMassarrayTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MassarrayTaskInfo where 1=1 and massarrayTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MassarrayTaskInfo  where 1=1 and massarrayTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MassarrayTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from MassarrayTaskItem where 1=1 and massarrayTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<MassarrayTaskItem> plateSample(String id, String counts) {
		String hql="from MassarrayTaskItem where 1=1 and massarrayTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<MassarrayTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MassarrayTaskItem where 1=1 and massarrayTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MassarrayTaskItem  where 1=1 and massarrayTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MassarrayTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<MassarrayTaskInfo> findMassarrayTaskInfoByCode(String code) {
		String hql="from MassarrayTaskInfo where 1=1 and code='"+code+"'";
		List<MassarrayTaskInfo> list=new ArrayList<MassarrayTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<MassarrayTaskInfo> selectAllResultListById(String code) {
		String hql = "from MassarrayTaskInfo  where (submit is null or submit='') and massarrayTask.id='"
				+ code + "'";
		List<MassarrayTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<MassarrayTaskInfo> selectResultListById(String id) {
		String hql = "from MassarrayTaskInfo  where massarrayTask.id='"
				+ id + "'";
		List<MassarrayTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<MassarrayTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from MassarrayTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<MassarrayTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<MassarrayTaskItem> selectMassarrayTaskItemList(String scId)
			throws Exception {
		String hql = "from MassarrayTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayTaskItem> list = new ArrayList<MassarrayTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from MassarrayTaskItem where 1=1 and massarrayTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public MassarrayTaskInfo getResultByCode(String code) {
			String hql=" from MassarrayTaskInfo where 1=1 and code='"+code+"'";
			MassarrayTaskInfo spi=(MassarrayTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}