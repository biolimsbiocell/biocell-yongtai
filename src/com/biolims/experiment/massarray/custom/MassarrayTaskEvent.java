package com.biolims.experiment.massarray.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.massarray.service.MassarrayTaskService;

public class MassarrayTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		MassarrayTaskService mbService = (MassarrayTaskService) ctx
				.getBean("massarrayTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
