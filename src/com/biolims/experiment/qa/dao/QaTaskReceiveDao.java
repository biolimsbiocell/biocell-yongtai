package com.biolims.experiment.qa.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qa.model.QaTaskReceiveTemp;
import com.biolims.experiment.qa.model.QaTaskReceive;
import com.biolims.experiment.qa.model.QaTaskReceiveItem;

@Repository
@SuppressWarnings("unchecked")
public class QaTaskReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectQaTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QaTaskReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QaTaskReceive> list = new ArrayList<QaTaskReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQaTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskReceiveItem> list = new ArrayList<QaTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQaTaskReceiveItemByTypeList(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from QaTaskReceiveItem where 1=1 and qaTaskReceive.Type='QaTask'";
		String key = "";
		if (scId != null)
			key = key + " and qaTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskReceiveItem> list = new ArrayList<QaTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询从开向检验完成的样本
	 * @return
	 */
	public List<QaTaskReceiveTemp> selectQaTaskReceiveTempList() {
		String hql = "from QaTaskReceiveTemp where 1=1 and state='1' order by reportDate desc";
		List<QaTaskReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 修改状态
	 * @param id
	 * @return
	 */
	public List<QaTaskReceiveItem> selectReceiveByIdList(String id) {
		String hql = "from QaTaskReceiveItem where 1=1 and state='1' and method='1' and qaTaskReceive.id='"
				+ id + "'";
		List<QaTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 明细数据
	 * @param code
	 * @return
	 */
	public List<QaTaskReceiveItem> selectReceiveItemListById(String code) {
		String hql = "from QaTaskReceiveItem where 1=1 and qaTaskReceive.id='"
				+ code + "'";
		List<QaTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public Map<String, Object> selectQaTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from QaTaskReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskReceiveTemp> list = new ArrayList<QaTaskReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
}