package com.biolims.experiment.qa.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qa.model.QaTaskReceiveItem;
import com.biolims.experiment.qa.model.QaTask;
import com.biolims.experiment.qa.model.QaTaskCos;
import com.biolims.experiment.qa.model.QaTaskItem;
import com.biolims.experiment.qa.model.QaTaskReagent;
import com.biolims.experiment.qa.model.QaTaskTemp;
import com.biolims.experiment.qa.model.QaTaskTemplate;
import com.biolims.experiment.qa.model.QaTaskInfo;

@Repository
@SuppressWarnings("unchecked")
public class QaTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectQaTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QaTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QaTask> list = new ArrayList<QaTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQaTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskItem> list = new ArrayList<QaTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQaTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskInfo> list = new ArrayList<QaTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 模板对应的子表
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQaTaskTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskTemplate> list = new ArrayList<QaTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQaTaskTemplateReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskReagent> list = new ArrayList<QaTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据ItemId查询试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQaTaskTemplateReagentListByItemId(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from QaTaskReagent t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskReagent> list = new ArrayList<QaTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from QaTaskReagent t where 1=1 and t.qaTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<QaTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from QaTaskCos t where 1=1 and t.qaTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<QaTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQaTaskTemplateCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskCos> list = new ArrayList<QaTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQaTaskTemplateCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from QaTaskCos t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskCos> list = new ArrayList<QaTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据QaTaskQA任务主表ID查询试剂明细
	 */
	public List<QaTaskReagent> setReagentList(String code) {
		String hql = "from QaTaskReagent where 1=1 and qaTask.id='" + code
				+ "'";
		List<QaTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据QaTaskQA任务主表ID查询QA任务明细
	 */
	public List<QaTaskItem> setQaTaskItemList(String code) {
		String hql = "from QaTaskItem where 1=1 and qaTask.id='" + code + "'";
		List<QaTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据状态查询QaTask
	 * @return
	 */
	public List<QaTask> selectQaTaskList() {
		// String hql = "from QaTask t where 1=1 and t.state=1";
		String hql = "from QaTask t where 1=1 and t.state='1'";
		List<QaTask> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QaTaskInfo> selectQaTaskResultList(String id) {
		String hql = "from QaTaskInfo t where 1=1 and qaTask.state='1' and qaTask='"
				+ id + "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<QaTaskInfo> setQaTaskResultByCode(String code) {
		String hql = "from QaTaskInfo t where 1=1 and qaTask.id='" + code
				+ "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<QaTaskInfo> setQaTaskResultById(String code) {
		String hql = "from QaTaskInfo t where submit is null and qaTask.id='"
				+ code + "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<QaTaskInfo> setQaTaskResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from QaTaskInfo t where submit is null and id in ("
				+ insql + ")";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询QA任务样本去到QPCR或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectExperimentQaTaskReslutList(
			Integer startNum, Integer limitNum, String dir, String sort,
			String state) throws Exception {
		String hql = "from QaTaskInfo where 1=1 and experiment.qaInfo.state='完成' and qaTask.stateName='"
				+ state + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskInfo> list = new ArrayList<QaTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQaTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskReceiveItem where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskReceiveItem> list = new ArrayList<QaTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询异常QA任务
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQaTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QaTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskInfo> list = new ArrayList<QaTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询左侧中间表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectQaTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QaTaskTemp where 1=1 and state='1'";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QaTaskTemp> list = new ArrayList<QaTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询QA任务异常样本
	 * @param code
	 * @return
	 */
	public List<QaTaskInfo> selectQaTaskResultAbnormalList(String code) {
		String hql = "from QaTaskInfo where 1=1 and  qaTask='" + code + "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<QaTaskInfo> selectQaTaskAllList(String code) {
		String hql = "from QaTaskInfo  where 1=1  and qaTask='" + code
				+ "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id查询QA任务生成结果 用于更新是否合格字段
	 * @param id
	 * @return
	 */
	public List<QaTaskInfo> findQaTaskResultById(String id) {
		String hql = "from QaTaskInfo  where 1=1 and code='" + id + "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 状态完成下一步流向
	 * @param code
	 * @return
	 */
	public List<QaTaskInfo> setNextFlowList(String code) {
		String hql = "from QaTaskInfo where 1=1  and qaTask.state='1' and qaTask.id='"
				+ code + "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从上一步样本接收完成的QA任务样本
	 * @return
	 */
	public List<QaTaskReceiveItem> selectReceiveQaTaskList() {
		String hql = "from QaTaskReceiveItem where 1=1  and state='1' ";
		List<QaTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from QaTaskItem t where 1=1 and t.qaTask.id='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据id查询QA任务结果
	 * @param id
	 * @return
	 */
	public List<QaTaskInfo> selectQaTaskResultListById(String id) {
		String hql = "from QaTaskInfo where 1=1  and qaTask.id='" + id
				+ "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的QA任务次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from QaTaskItem t where 1=1 and t.sampleCode='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.Code)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 查询从样本接收完成的的样本
	 * @param id
	 * @return
	 */
	public List<QaTaskItem> findQaTaskItemList(String id) {
		String hql = "from QaTaskItem  where 1=1 and state='1' and qaTask='"
				+ id + "'";
		List<QaTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据Code查询QaTaskInfo
	 * @param code
	 * @return
	 */
	public String findQaTaskInfo(String code) {
		String hql = "from QaTaskInfo  where 1=1 and tempId='" + code + "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 根据样本编号查出QaTaskTemp对象
	 * @param code
	 * @return
	 */
	public QaTaskTemp findQaTaskTemp(String code) {
		String hql = "from QaTaskTemp t where 1=1 and t.sampleCode = '" + code
				+ "'";
		QaTaskTemp QaTaskTemp = (QaTaskTemp) this.getSession()
				.createQuery(hql).uniqueResult();
		return QaTaskTemp;
	}


	/**
	 * 根据样本查询主表
	 * @param sid
	 * @return
	 */
	public String selectTask(String sid) {
		String hql = "from TechCheckServiceOrder t where t.id = '" + sid + "'";
		String str = (String) this.getSession()
				.createQuery(" select t.id " + hql).uniqueResult();
		return str;
	}

	public List<QaTaskItem> selectQaTaskItemList(String scId) throws Exception {
		String hql = "from QaTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QaTaskItem> list = new ArrayList<QaTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * 根据样本编号查询检测临时表
	 * @param id
	 * @return
	 */
	public List<QaTaskInfo> setQaTaskInfo(String id) {
		String hql = "from QaTaskInfo where 1=1 and qaTask.id='" + id
				+ "' and nextFlowId='0009'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据入库的原始样本号查询该结果表信息
	 * @param id
	 * @param Code
	 * @return
	 */
	public List<QaTaskInfo> setQaTaskInfo2(String id, String code) {
		String hql = "from QaTaskInfo t where 1=1 and qaTask.id='" + id
				+ "' and sampleCode='" + code + "'";
		List<QaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}