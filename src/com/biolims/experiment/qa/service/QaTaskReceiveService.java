package com.biolims.experiment.qa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qa.dao.QaTaskReceiveDao;
import com.biolims.experiment.qa.model.QaTaskReceiveTemp;
import com.biolims.experiment.qa.model.QaTaskTemp;
import com.biolims.experiment.qa.model.QaTaskReceive;
import com.biolims.experiment.qa.model.QaTaskReceiveItem;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QaTaskReceiveService {
	@Resource
	private QaTaskReceiveDao qaTaskReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQaTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qaTaskReceiveDao.selectQaTaskReceiveList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QaTaskReceive i) throws Exception {
		qaTaskReceiveDao.saveOrUpdate(i);
	}

	public QaTaskReceive get(String id) {
		QaTaskReceive qaTaskReceive = commonDAO.get(QaTaskReceive.class, id);
		return qaTaskReceive;
	}

	public Map<String, Object> findQaTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskReceiveDao
				.selectQaTaskReceiveItemList(scId, startNum, limitNum,
						dir, sort);
		List<QaTaskReceiveItem> list = (List<QaTaskReceiveItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaTaskReceiveItem(QaTaskReceive sc, String itemDataJson)
			throws Exception {
		List<QaTaskReceiveItem> saveItems = new ArrayList<QaTaskReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaTaskReceiveItem scp = new QaTaskReceiveItem();
			// 将map信息读入实体类
			scp = (QaTaskReceiveItem) qaTaskReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQaTaskReceive(sc);
			saveItems.add(scp);
		}
		qaTaskReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQaTaskReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			QaTaskReceiveItem scp = qaTaskReceiveDao.get(
					QaTaskReceiveItem.class, id);
			qaTaskReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QaTaskReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qaTaskReceiveDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("qaTaskReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQaTaskReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		QaTaskReceive sct = qaTaskReceiveDao.get(QaTaskReceive.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		this.qaTaskReceiveDao.update(sct);
		try {
			List<QaTaskReceiveItem> edri = this.qaTaskReceiveDao
					.selectReceiveByIdList(sct.getId());
			for (QaTaskReceiveItem ed : edri) {
				if (ed.getMethod().equals("1")) {
					QaTaskTemp dst = new QaTaskTemp();
					dst.setConcentration(ed.getConcentration());
					dst.setLocation(ed.getLocation());
					dst.setCode(ed.getCode());
					dst.setId(ed.getId());
					dst.setSequenceFun(ed.getSequenceFun());
					dst.setPatientName(ed.getPatientName());
					dst.setProductName(ed.getProductName());
					dst.setProductId(ed.getProductId());
					dst.setIdCard(ed.getIdCard());
					dst.setInspectDate(ed.getInspectDate());
					dst.setPhone(ed.getPhone());
					dst.setOrderId(ed.getOrderId());
					dst.setReportDate(ed.getReportDate());
					dst.setState("1");
					dst.setClassify(ed.getClassify());
					dst.setCode(ed.getCode());
					dst.setSampleType(ed.getSampleType());
					dst.setSampleNum(ed.getSampleNum());
					dst.setLabCode(ed.getLabCode());
					qaTaskReceiveDao.saveOrUpdate(dst);
					QaTaskReceiveTemp temp = commonDAO.get(QaTaskReceiveTemp.class,
							ed.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}
				// 审批完成改变Info中的样本状态为“待QA任务QA任务”
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(ed
						.getCode());
				if (sf != null) {
					sf.setState("3");
					sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
				}
				ed.setState("1");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 审批完成之后，将明细表数据保存到QaTaskQA任务中间表
	 * @param code
	 */
	public void saveToQaTaskTemp(String code) {
		List<QaTaskReceiveItem> list = qaTaskReceiveDao
				.selectReceiveItemListById(code);
		if (list.size() > 0) {
			for (QaTaskReceiveItem ed : list) {
				QaTaskTemp dst = new QaTaskTemp();
				dst.setConcentration(ed.getConcentration());
				dst.setLocation(ed.getLocation());
				dst.setCode(ed.getCode());
				dst.setId(ed.getId());
				dst.setSequenceFun(ed.getSequenceFun());
				dst.setPatientName(ed.getPatientName());
				dst.setProductName(ed.getProductName());
				dst.setProductId(ed.getProductId());
				dst.setIdCard(ed.getIdCard());
				dst.setInspectDate(ed.getInspectDate());
				dst.setPhone(ed.getPhone());
				dst.setOrderId(ed.getOrderId());
				dst.setReportDate(ed.getReportDate());
				dst.setState("1");
				dst.setClassify(ed.getClassify());
				dst.setSampleCode(ed.getSampleCode());
				dst.setSampleType(ed.getSampleType());
				dst.setSampleNum(ed.getSampleNum());
				qaTaskReceiveDao.saveOrUpdate(dst);
			}
		}
	}
		public Map<String, Object> selectQaTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = qaTaskReceiveDao.selectQaTaskReceiveTempList(
				startNum, limitNum, dir, sort);
		List<QaTaskReceiveTemp> list = (List<QaTaskReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
	
}
