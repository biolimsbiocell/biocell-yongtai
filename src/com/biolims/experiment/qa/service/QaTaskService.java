package com.biolims.experiment.qa.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.qa.dao.QaTaskReceiveDao;
import com.biolims.experiment.qa.dao.QaTaskDao;
import com.biolims.experiment.qa.model.QaTaskAbnormal;
import com.biolims.experiment.qa.model.QaTaskReceiveItem;
import com.biolims.experiment.qa.model.QaTask;
import com.biolims.experiment.qa.model.QaTaskCos;
import com.biolims.experiment.qa.model.QaTaskItem;
import com.biolims.experiment.qa.model.QaTaskReagent;
import com.biolims.experiment.qa.model.QaTaskTemp;
import com.biolims.experiment.qa.model.QaTaskTemplate;
import com.biolims.experiment.qa.model.QaTaskInfo;
import com.biolims.experiment.qa.model.QaTaskInfoManager;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QaTaskService {
	@Resource
	private QaTaskDao qaTaskDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;

	StringBuffer json = new StringBuffer();
	public Map<String, Object> findQaTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qaTaskDao.selectQaTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findQaTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		    return qaTaskDao.selectQaTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QaTask i) throws Exception {
		qaTaskDao.saveOrUpdate(i);
	}

	public QaTask get(String id) {
		QaTask qaTask = commonDAO.get(QaTask.class, id);
		return qaTask;
	}

	public Map<String, Object> findQaTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<QaTaskItem> list = (List<QaTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findQaTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskInfoList(scId, startNum, limitNum, dir, sort);
		List<QaTaskInfo> list = (List<QaTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 模板
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskTemplateItemList(scId, startNum, limitNum, dir, sort);
		List<QaTaskTemplate> list = (List<QaTaskTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findReagentItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskTemplateReagentList(scId, startNum, limitNum, dir,
						sort);
		List<QaTaskReagent> list = (List<QaTaskReagent>) result.get("list");
		return result;
	}

	/**
	 * 根据步骤加载试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskTemplateReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<QaTaskReagent> list = (List<QaTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskTemplateCosListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<QaTaskCos> list = (List<QaTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskTemplateCosList(scId, startNum, limitNum, dir, sort);
		List<QaTaskCos> list = (List<QaTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaTaskItem(QaTask sc, String itemDataJson)
			throws Exception {
		List<QaTaskItem> saveItems = new ArrayList<QaTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = qaTaskDao.get(
						StorageContainer.class, temp.getStorageContainer()
								.getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();
			}
		}
		for (Map<String, Object> map : list) {
			QaTaskItem scp = new QaTaskItem();
			// 将map信息读入实体类
			scp = (QaTaskItem) qaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num = Math.floor((scp.getOrderNumber() - 1)
							/ rowCode) + 1;
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
							/ (rowCode * colCode) + 1));
				}
			}
			scp.setQaTask(sc);

			if (scp != null) {
				if(sc.getTemplate()!=null){
				Template t = commonDAO.get(Template.class, sc.getTemplate().getId());
				if (t != null) {
					if (t.getDicSampleType() != null) {
						DicSampleType d = commonDAO.get(DicSampleType.class, t
								.getDicSampleType().getId());
						if (d != null) {
							if (scp.getDicSampleType() == null
									|| (scp.getDicSampleType() != null && scp
											.getDicSampleType().equals(""))) {
								scp.setDicSampleType(d);
							}
						}
					}
					if (t.getProductNum() != null) {
						if (scp.getProductNum() == null
								|| (scp.getProductNum() != null && scp
										.getProductNum().equals(""))) {
							scp.setProductNum(t.getProductNum());
						}
					}
					if (t.getSampleNum() != null) {
						if (scp.getSampleConsume() == null) {
							scp.setSampleConsume(t.getSampleNum());
						}
					}
				}
				}
			}
			saveItems.add(scp);
			if (scp != null) {
				QaTaskInfoManager dm = new QaTaskInfoManager();
				dm.setExpCode(scp.getExpCode());
				dm.setCode(scp.getCode());
				dm.setSampleCode(scp.getSampleCode());
				dm.setSampleId(scp.getTempId());
				dm.setSampleName(scp.getSampleName());
				dm.setSampleVolume(scp.getSampleVolume());
				dm.setAddVolume(scp.getAddVolume());
				dm.setSumVolume(scp.getSumVolume());
				dm.setIndexs(scp.getIndexs());
				dm.setConcentration(scp.getConcentration());
				dm.setReason(scp.getReason());
				dm.setResult(scp.getResult());
				dm.setStepNum(scp.getStepNum());
				dm.setPatientName(scp.getPatientName());
				dm.setProductId(scp.getProductId());
				dm.setProductName(scp.getProductName());
				dm.setPhone(scp.getPhone());
				dm.setReportDate(scp.getReportDate());
				dm.setIdCard(scp.getIdCard());
				dm.setInspectDate(scp.getInspectDate());
				dm.setOrderId(scp.getOrderId());
				dm.setSequenceFun(scp.getSequenceFun());
				qaTaskDao.saveOrUpdate(dm);
			}
		}
		qaTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(qaTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQaTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			if (id != null) {
				QaTaskItem scp = qaTaskDao
						.get(QaTaskItem.class, id);
				qaTaskDao.delete(scp);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaTaskResult(QaTask sc, String itemDataJson)
			throws Exception {
		DecimalFormat df = new DecimalFormat("######.000");
		List<QaTaskInfo> saveItems = new ArrayList<QaTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaTaskInfo scp = new QaTaskInfo();
			// 将map信息读入实体类
			scp = (QaTaskInfo) qaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQaTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						|| !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());
					markCode = scp.getCode() + d.getCode();
				}
				String code = codingRuleService.getCode("QaTaskInfo",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			Double b;
			if (sc.getAcceptUser().getId().equals("D0004")) {
				// QA任务实验组-组织
				if (scp.getNextFlowId().equals("0022")) {
					// 超声破碎
					if (scp.getQbcontraction() < 11) {
						scp.setVolume(180.0);
					} else if (scp.getQbcontraction() >= 11
							&& scp.getQbcontraction() < 15) {
						b = 2000 / scp.getQbcontraction();
						scp.setVolume(Double.valueOf(df.format(b)));
					} else if (scp.getQbcontraction() >= 15
							&& scp.getQbcontraction() <= 22) {
						scp.setVolume(90.0);
					} else if (scp.getQbcontraction() > 22) {
						b = 2000 / scp.getQbcontraction();
						scp.setVolume(Double.valueOf(df.format(b)));
					}
				} else if (scp.getNextFlowId().equals("0021")) {// QPCR
					scp.setVolume(1.0);
				} else if (scp.getNextFlowId().equals("0009")) {// 样本入库

				}
			}
			if (scp.getVolume() != null) {
				if (scp.getQbcontraction() != null) {
					b = scp.getVolume() * scp.getQbcontraction();
					scp.setSampleNum(Double.valueOf(df.format(b)));
				}
				if (scp.getQbcontraction() == null
						&& scp.getContraction() != null) {
					b = scp.getVolume() * scp.getContraction();
					scp.setSampleNum(Double.valueOf(df.format(b)));
				}
			}
			List<SampleInfo> sil = qaTaskDao.findByProperty(
					SampleInfo.class, "code", scp.getCode());
			if (sil.size() > 0) {
				SampleInfo si = sil.get(0);
				scp.setSampleInfo(si);
			}
			qaTaskDao.saveOrUpdate(scp);
		}
		// 计算出组织实验组入库样本的体积
		if (sc.getAcceptUser().getId().equals("D0004")) {
			// QA任务实验组-组织
			// 根据主表id查询结果表为入库样本的信息
			List<QaTaskInfo> list1 = this.qaTaskDao
					.setQaTaskInfo(sc.getId());
			for (QaTaskInfo s : list1) {
				// 根据入库的原始样本号查询该结果表信息
				List<QaTaskInfo> list2 = this.qaTaskDao
						.setQaTaskInfo2(sc.getId(), s.getCode());
				Double v = 0.0;
				for (QaTaskInfo s2 : list2) {
					if (!s2.getNextFlowId().equals("0009")
							&& s2.getVolume() != null) {
						v += s2.getVolume();
					}
				}
				s.setVolume(Double.valueOf(df.format(200 - v)));
				if (s.getVolume() != null) {
					if (s.getQbcontraction() != null) {
						s.setSampleNum(Double.valueOf(df.format(s.getVolume()
								* s.getQbcontraction())));
					}
					if (s.getQbcontraction() == null
							&& s.getContraction() != null) {
						s.setSampleNum(Double.valueOf(df.format(s.getVolume()
								* s.getContraction())));
					}
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQaTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			QaTaskInfo scp = qaTaskDao
					.get(QaTaskInfo.class, id);
			if (scp.getSubmit() == null)
				qaTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(QaTask sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			qaTaskDao.saveOrUpdate(sc);
//			if (sc.getTemplate() != null) {
//			
//				String id = sc.getTemplate().getId();
//				templateService.setCosIsUsedSave(id);
//			}
			String jsonStr = "";
			
			jsonStr = (String) jsonMap.get("qaTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQaTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qaTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQaTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qaTaskTemplateItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTemplateItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qaTaskTemplateReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				re=saveReagentItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qaTaskTemplateCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				in=saveCosItem(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTemplateItem(QaTask sc, String itemDataJson)
			throws Exception {
		List<QaTaskTemplate> saveItems = new ArrayList<QaTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaTaskTemplate scp = new QaTaskTemplate();
			// 将map信息读入实体类
			scp = (QaTaskTemplate) qaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQaTask(sc);
			saveItems.add(scp);
		}
		qaTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItem(String[] ids) throws Exception {
		for (String id : ids) {
			QaTaskTemplate scp = qaTaskDao.get(
					QaTaskTemplate.class, id);
			qaTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItemOne(String ids) throws Exception {
		QaTaskTemplate scp = qaTaskDao.get(QaTaskTemplate.class,
				ids);
		if (scp != null) {
			qaTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveReagentItem(QaTask sc, String itemDataJson)
			throws Exception {
		Integer re=0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaTaskReagent scp = new QaTaskReagent();
			// 将map信息读入实体类
			scp = (QaTaskReagent) qaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQaTask(sc);
			if (scp.getSn() != null && !scp.getSn().equals("")) {
				StorageOutItem soi = storageOutService
						.findStorageOutItem(scp.getSn());
				if (soi != null) {
					if (scp.getCode().equals(soi.getStorage().getId())) {
						scp.setBatch(soi.getCode());
						scp.setExpireDate(soi.getExpireDate());
						re++;
					} else {
						scp.setSn("");
					}
				}
			}
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getNum());
			}
			qaTaskDao.saveOrUpdate(scp);;
		}
		return re;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItem(String[] ids) throws Exception {
		for (String id : ids) {
			QaTaskReagent scp = qaTaskDao.get(QaTaskReagent.class,
					id);
			qaTaskDao.delete(scp);
		}
	}

	/**
	 * delTemplateReagentOne 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateReagentOne(String ids) throws Exception {
		QaTaskReagent scp = qaTaskDao.get(QaTaskReagent.class, ids);
		if (scp != null) {
			qaTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveCosItem(QaTask sc, String itemDataJson) throws Exception {
		Integer in=0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaTaskCos scp = new QaTaskCos();
			// 将map信息读入实体类
			Instrument ins = null;
			scp = (QaTaskCos) qaTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getCode() != null && !scp.equals("")) {
				ins = qaTaskDao.get(Instrument.class, scp.getCode());
			}
			if (ins != null
					&& ins.getType().getId().equals(scp.getType().getId())) {
				scp.setName(ins.getName());
				scp.setState(ins.getState().getName());
				in++;
			} else {
				scp.setName("");
				scp.setCode("");
				scp.setState("");
			}
			scp.setQaTask(sc);
			qaTaskDao.saveOrUpdate(scp);;
		}
		return in;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCosItem(String[] ids) throws Exception {
		for (String id : ids) {
			QaTaskCos scp = qaTaskDao.get(QaTaskCos.class, id);
			qaTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQaTaskTemplateCosOne(String ids) throws Exception {
		QaTaskCos scp = qaTaskDao.get(QaTaskCos.class, ids);
		if (scp != null) {
			qaTaskDao.delete(scp);
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		QaTask sct = qaTaskDao.get(QaTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setReceiveDate(new Date());
		qaTaskDao.update(sct);
		// 提交样本
		submit(id, null);

		// 完成的时候判断设备解除占用
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		String did = sct.getId();
		// 将批次信息反馈到模板中
		List<QaTaskReagent> list1 = qaTaskDao.setReagentList(did);
		for (QaTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); 
			// QaTask实验中试剂的批次
			String drid = dt.gettReagent(); 
			// QaTask实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
		}
		// 将状态信息反馈到开箱检验的样本中
		List<QaTaskItem> eList = qaTaskDao.setQaTaskItemList(did);
		for (QaTaskItem edg : eList) {
			String sid = edg.getTempId();
			List<SampleReceiveItem> sList = sampleReceiveDao
					.selectItemList(sid);
			for (SampleReceiveItem sri : sList) {
				sri.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				sri.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE);
			}
			updateTempState(sid);
		}
	}

	/**
	 * 生成结果之后修改QaTaskQA任务中间表的样本状态
	 * @param code
	 */
	public void updateTempState(String code) {
		QaTaskTemp dt = commonDAO.get(QaTaskTemp.class, code);
		if (dt != null) {
			dt.setState("2");
		}
	}

	/**
	 * 到QPCR的样本或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findExperimentQaTaskReslutList(Integer startNum,
			Integer limitNum, String dir, String sort, String state)
			throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectExperimentQaTaskReslutList(startNum, limitNum, dir,
						sort, state);
		List<QaTaskInfo> list = (List<QaTaskInfo>) result.get("list");

		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQaTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskDao
				.selectQaTaskFromReceiveList(scId, startNum, limitNum, dir,
						sort);
		List<QaTaskReceiveItem> list = (List<QaTaskReceiveItem>) result.get("list");
		return result;
	}

	public Map<String, Object> selectQaTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qaTaskDao.selectQaTaskAbnormalList(
				scId, startNum, limitNum, dir, sort);
		List<QaTaskInfo> list = (List<QaTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qaTaskDao.setReagent(id, code);
		List<QaTaskReagent> list = (List<QaTaskReagent>) result.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			for (QaTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());
				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}
				if (ti.getNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}
				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				if(ti.getExpireDate()!=null){
					map.put("expireDate", sdf.format(ti.getExpireDate()));
				}
				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getQaTask().getId());
				map.put("tName", ti.getQaTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qaTaskDao.setCos(id, code);
		List<QaTaskCos> list = (List<QaTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (QaTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}
				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}
				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				if (ti.getType() != null) {
					map.put("typeId", ti.getType().getId());
					map.put("typeName", ti.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getQaTask().getId());
				map.put("tName", ti.getQaTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	
	public List<QaTaskItem> findQaTaskItemList(String scId) throws Exception {
		List<QaTaskItem> list = qaTaskDao.selectQaTaskItemList(scId);
		return list;
	}

	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submit(String id, String[] ids) throws Exception {
		// 获取主表信息
		QaTask sc = this.qaTaskDao.get(QaTask.class, id);
		// 获取结果表样本信息
		List<QaTaskInfo> list;
		if (ids == null)
			list = this.qaTaskDao.setQaTaskResultById(id);
		else
			list = this.qaTaskDao.setQaTaskResultByIds(ids);
		for (QaTaskInfo scp : list) {
			if (scp != null) {
				if (scp.getNextFlowId() != null && scp.getResult() != null
						&& scp.getSubmit() == null) {
					if (!scp.getResult().equals("")
							&& !scp.getNextFlowId().equals("")) {
						String isOk = scp.getResult();
						String nextFlowId = scp.getNextFlowId();
						if (isOk.equals("1")) {
							if (nextFlowId.equals("0009")) {
								// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setNum(scp.getSampleNum());
								st.setSampleType(scp.getSampleType());
								st.setSampleTypeId(scp.getDicSampleType()
										.getId());
								st.setInfoFrom("QaTaskInfo");
								st.setState("1");
								SampleInfo s = this.sampleInfoMainDao
										.findSampleInfo(scp.getSampleCode());
								st.setPatientName(s.getPatientName());
								qaTaskDao.saveOrUpdate(st);
							} else if (nextFlowId.equals("0010")) {
								// 重QA任务
								QaTaskAbnormal eda = new QaTaskAbnormal();
								scp.setState("4");
								// 状态为4 在重QA任务中显示
								sampleInputService.copy(eda, scp);
							} else if (nextFlowId.equals("0011")) {
								// 重建库
								WkTaskAbnormal wka = new WkTaskAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0024")) {
								// 重抽血
								PlasmaAbnormal wka = new PlasmaAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0012")) {// 暂停
							} else if (nextFlowId.equals("0013")) {// 终止
							} else if (nextFlowId.equals("0018")) {// 文库构建
								WkTaskTemp d = new WkTaskTemp();
								scp.setState("1");
								sampleInputService.copy(d, scp);
//								d.setReportDate(DateUtil.parse(scp
//										.getReportDate()));
								qaTaskDao.saveOrUpdate(d);
							} else if (nextFlowId.equals("0020")) {// 2100质控
								Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
								scp.setState("1");
								sampleInputService.copy(qc2100, scp);
								qc2100.setWkType("2");
								qaTaskDao.saveOrUpdate(qc2100);
							} else if (nextFlowId.equals("0021")) {// QPCR质控
								QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
								scp.setState("1");
								sampleInputService.copy(qpcr, scp);
								qpcr.setWkType("1");
								qaTaskDao.saveOrUpdate(qpcr);
							} else if (nextFlowId.equals("0022")) {// 超声破碎
								UfTaskTemp uf = new UfTaskTemp();
								scp.setState("1");
								sampleInputService.copy(uf, scp);
							} else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(nextFlowId);
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}

						} else {// 不合格
							if (nextFlowId.equals("0014")) {// 反馈项目组
								// 下一步流向是：反馈至项目组
								
							} else {
								// 提交的不合格样本到异常管理
								QaTaskAbnormal eda = new QaTaskAbnormal();
								scp.setState("2");// 状态为2 在QA任务异常中显示
								sampleInputService.copy(eda, scp);
							}

						}
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										sc.getCreateDate(),
										format.format(new Date()),
										"QaTask",
										"QA任务",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), scp.getNextFlow(),
										scp.getResult(), null, null, null,
										null, null, null, null, null);
						scp.setSubmit("1");
						commonService.saveOrUpdate(scp);
					}
				}
			}
		}
	}
}