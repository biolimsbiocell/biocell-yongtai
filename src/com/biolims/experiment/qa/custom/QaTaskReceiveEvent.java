package com.biolims.experiment.qa.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.qa.service.QaTaskReceiveService;

public class QaTaskReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QaTaskReceiveService mbService = (QaTaskReceiveService) ctx
				.getBean("qaTaskReceiveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
