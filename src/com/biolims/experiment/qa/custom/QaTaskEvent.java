package com.biolims.experiment.qa.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.qa.service.QaTaskService;

public class QaTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QaTaskService mbService = (QaTaskService) ctx
				.getBean("qaTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
