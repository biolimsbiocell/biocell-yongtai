﻿package com.biolims.experiment.qa.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.qa.dao.QaTaskDao;
import com.biolims.experiment.qa.model.QaTaskReceiveItem;
import com.biolims.experiment.qa.model.QaTask;
import com.biolims.experiment.qa.model.QaTaskCos;
import com.biolims.experiment.qa.model.QaTaskItem;
import com.biolims.experiment.qa.model.QaTaskReagent;
import com.biolims.experiment.qa.model.QaTaskTemp;
import com.biolims.experiment.qa.model.QaTaskTemplate;
import com.biolims.experiment.qa.model.QaTaskInfo;
import com.biolims.experiment.qa.service.QaTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/qa/qaTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QaTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260602";
	@Autowired
	private QaTaskService qaTaskService;
	private QaTask qaTask = new QaTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private QaTaskDao qaTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showQaTaskList")
	public String showQaTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTask.jsp");
	}

	@Action(value = "showQaTaskListJson")
	public void showQaTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskService
				.findQaTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<QaTask> list = (List<QaTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qaTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQaTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskDialog.jsp");
	}

	@Action(value = "showDialogQaTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQaTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskService
				.findQaTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<QaTask> list = (List<QaTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQaTask")
	public String editQaTask() throws Exception {
		System.out.println("1111111111111115");
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			System.out.println("1111111111111116");
			qaTask = qaTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qaTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			System.out.println("1111111111111117");
			qaTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qaTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			qaTask.setCreateDate(stime);
			qaTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qaTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			System.out.println("1111111111111119");
		}
		toState(qaTask.getState());
		putObjToContext("maxNum", maxNum);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskEdit.jsp");
	}

	@Action(value = "copyQaTask")
	public String copyQaTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qaTask = qaTaskService.get(id);
		qaTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qaTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QaTask";
			String markCode = "HSTQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qaTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("qaTaskItem",
				getParameterFromRequest("qaTaskItemJson"));
		aMap.put("qaTaskResult",
				getParameterFromRequest("qaTaskResultJson"));
		aMap.put("qaTaskTemplateItem",
				getParameterFromRequest("qaTaskTemplateItemJson"));
		aMap.put("qaTaskTemplateReagent",
				getParameterFromRequest("qaTaskTemplateReagentJson"));
		aMap.put("qaTaskTemplateCos",
				getParameterFromRequest("qaTaskTemplateCosJson"));
		qaTaskService.save(qaTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/qa/qaTask/editQaTask.action?id="
				+ qaTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}
	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try{
		String id = getParameterFromRequest("id");
		qaTask=commonService.get(QaTask.class, id);
		Map aMap = new HashMap();
		aMap.put("qaTaskItem",
				getParameterFromRequest("qaTaskItemJson"));
		aMap.put("qaTaskResult",
				getParameterFromRequest("qaTaskResultJson"));
		aMap.put("qaTaskTemplateItem",
				getParameterFromRequest("qaTaskTemplateItemJson"));
		aMap.put("qaTaskTemplateReagent",
				getParameterFromRequest("qaTaskTemplateReagentJson"));
		aMap.put("qaTaskTemplateCos",
				getParameterFromRequest("qaTaskTemplateCosJson"));
		map=qaTaskService.save(qaTask, aMap);
		map.put("success", true);
	} catch (Exception e) {
		e.printStackTrace();
		map.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(map));

	}
	@Action(value = "viewQaTask")
	public String toViewQaTask() throws Exception {
		String id = getParameterFromRequest("id");
		qaTask = qaTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskEdit.jsp");
	}

	/**
	 * 查询左侧中间表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQaTaskTempList")
	public String showQaTaskTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskTemp.jsp");
	}

	@Action(value = "showQaTaskTempJson")
	public void showQaTaskTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskService
				.findQaTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<QaTaskTemp> list = (List<QaTaskTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleId", "");
		map.put("sampleName", "");
		map.put("location", "");
		map.put("patientName", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("sampleNum", "");
		map.put("labCode", "");
		map.put("sampleInfo-note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showQaTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskItem.jsp");
	}

	@Action(value = "showQaTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qaTaskService
					.findQaTaskItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskItem> list = (List<QaTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("qaTask-name", "");
			map.put("qaTask-id", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			
			map.put("eventClass", "");
			map.put("eventMs", "");
			map.put("eventXgry", "");
			map.put("inflSample", "");
			map.put("handleFf", "");
			map.put("handleCs", "");
			map.put("handleJtms", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQaTaskItem")
	public void delQaTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			if (ids != null) {
				qaTaskService.delQaTaskItem(ids);
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQaTaskInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskInfo.jsp");
	}

	@Action(value = "showQaTaskInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qaTaskService
					.findQaTaskInfoList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskInfo> list = (List<QaTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("sumVolume", "");
			map.put("rin", "");
			map.put("contraction", "");
			map.put("od260", "");
			map.put("od280", "");
			map.put("tempId", "");
			map.put("isToProject", "");
			map.put("volume", "");
			map.put("note", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("qaTask-name", "");
			map.put("qaTask-id", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("dataBits", "");
			map.put("qbcontraction", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQaTaskInfo")
	public void delQaTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaTaskService.delQaTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QaTaskService getQaTaskService() {
		return qaTaskService;
	}

	public void setQaTaskService(
			QaTaskService qaTaskService) {
		this.qaTaskService = qaTaskService;
	}

	public QaTask getQaTask() {
		return qaTask;
	}

	public void setQaTask(QaTask qaTask) {
		this.qaTask = qaTask;
	}

	/**
	 * 模板
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskTemplateItem.jsp");
	}

	@Action(value = "showTemplateItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qaTaskService
					.findTemplateItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskTemplate> list = (List<QaTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("qaTask-name", "");
			map.put("qaTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItem")
	public void delTemplateItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaTaskService.delTemplateItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItemOne")
	public void delTemplateItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qaTaskService.delTemplateItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQaTaskTemplateReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskTemplateReagentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskTemplateReagent.jsp");
	}

	@Action(value = "showQaTaskTemplateReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskTemplateReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = qaTaskService.findReagentItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskReagent> list = (List<QaTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("qaTask-name", "");
			map.put("qaTask-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQaTaskTemplateReagent")
	public void delQaTaskTemplateReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaTaskService.delReagentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateReagentOne")
	public void delTemplateReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qaTaskService.delTemplateReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQaTaskTemplateCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskTemplateCosList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskTemplateCos.jsp");
	}

	@Action(value = "showQaTaskTemplateCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskTemplateCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = qaTaskService.findCosItemList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskCos> list = (List<QaTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			map.put("temperature", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("speed", "");
			map.put("state", "");
			map.put("time", "");
			map.put("note", "");
			map.put("qaTask-name", "");
			map.put("qaTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQaTaskTemplateCos")
	public void delQaTaskTemplateCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaTaskService.delCosItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateCosOne")
	public void delQaTaskTemplateCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qaTaskService.delQaTaskTemplateCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 获取从样本接收来的样本
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQaTaskFromReceiveList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskFromReceiveList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskFromReceiveLeft.jsp");
	}

	@Action(value = "showQaTaskFromReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskFromReceiveListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qaTaskService
					.selectQaTaskFromReceiveList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskReceiveItem> list = (List<QaTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("qaTaskReceive-id", "");
			map.put("qaTaskReceive-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	/**
	 * 判断样本做QA任务的次数
	 * @throws Exception
	 */
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = qaTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 模板
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qaTaskService
					.findTemplateItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskTemplate> list = (List<QaTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("qaTask-name", "");
			map.put("qaTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据选中的步骤异步加载相关的试剂数据
	 * @throws Exception
	 */
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.qaTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据选中的步骤异步加载相关的设备数据
	 * @throws Exception
	 */
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.qaTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	

	
	/**
	 * 提交样本
	 * @throws Exception
	 */
	@Action(value = "submit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submit() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.qaTaskService.submit(id, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
