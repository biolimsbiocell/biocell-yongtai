﻿package com.biolims.experiment.qa.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qa.model.QaTaskItem;
import com.biolims.experiment.qa.model.QaTaskInfo;
import com.biolims.experiment.qa.model.QaTaskWaitManage;
import com.biolims.experiment.qa.service.QaTaskManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/qa/qaTaskManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QaTaskManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240205";
	@Autowired
	private QaTaskManageService qaTaskManageService;
	private QaTaskWaitManage qaTaskManage = new QaTaskWaitManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showQaTaskManageEditList")
	public String showQaTaskManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskManageEdit.jsp");
	}

	@Action(value = "showQaTaskManageList")
	public String showQaTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskManage.jsp");
	}

	@Action(value = "showQaTaskManageListJson")
	public void showQaTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskManageService
				.findQaTaskManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QaTaskInfo> list = (List<QaTaskInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");
	
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("sumVolume", "");
		map.put("rin", "");
		map.put("contraction", "");
		map.put("od260", "");
		map.put("od280", "");
		map.put("tempId", "");
		map.put("isToProject", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("state", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("qaTask-name", "");
		map.put("qaTask-id", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("orderType", "");
		map.put("taskId", "");
		map.put("classify", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleType", "");
		map.put("labCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qaTaskManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQaTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskManageDialog.jsp");
	}

	@Action(value = "showDialogQaTaskManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQaTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskManageService
				.findQaTaskManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QaTaskWaitManage> list = (List<QaTaskWaitManage>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("method", "");
		map.put("nextFlow", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQaTaskManage")
	public String editQaTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qaTaskManage = qaTaskManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qaTaskManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskManageEdit.jsp");
	}

	@Action(value = "copyQaTaskManage")
	public String copyQaTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qaTaskManage = qaTaskManageService.get(id);
		qaTaskManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qaTaskManage.getId();
		if (id != null && id.equals("")) {
			qaTaskManage.setId(null);
		}
		Map aMap = new HashMap();
		qaTaskManageService.save(qaTaskManage, aMap);
		return redirect("/experiment/qa/qaTaskManage/editQaTaskManage.action?id="
				+ qaTaskManage.getId());

	}

	@Action(value = "viewQaTaskManage")
	public String toViewQaTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		qaTaskManage = qaTaskManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskManageEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QaTaskManageService getQaTaskManageService() {
		return qaTaskManageService;
	}

	public void setQaTaskManageService(
			QaTaskManageService qaTaskManageService) {
		this.qaTaskManageService = qaTaskManageService;
	}

	public QaTaskWaitManage getQaTaskManage() {
		return qaTaskManage;
	}

	public void setQaTaskManage(QaTaskWaitManage qaTaskManage) {
		this.qaTaskManage = qaTaskManage;
	}

	/**
	 * 保存中间产物
	 * @throws Exception
	 */
	@Action(value = "saveQaTaskManage")
	public void saveQaTaskAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			qaTaskManageService
					.saveQaTaskInfoManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 保存样本
	 * @throws Exception
	 */
	@Action(value = "saveQaTaskItemManager")
	public void saveQaTaskItemManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			qaTaskManageService.saveQaTaskItemManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showQaTaskItemManagerList")
	public String showQaTaskItemManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskItemManage.jsp");
	}

	@Action(value = "showQaTaskItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskItemManagerListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// 排序方式
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = qaTaskManageService
					.findQaTaskItemList(map2Query, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QaTaskItem> list = (List<QaTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
		
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlow", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleConsume", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 样本管理明细入库
	 * @throws Exception
	 */
	@Action(value = "QaTaskManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qaTaskManageService.QaTaskManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 样本管理明细QA任务待办
	 * @throws Exception
	 */
	@Action(value = "QaTaskManageItemTiqu")
	public void QaTaskManageItemTiqu() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qaTaskManageService.QaTaskManageItemTiqu(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
