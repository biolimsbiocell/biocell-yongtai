﻿package com.biolims.experiment.qa.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.qa.dao.QaTaskReceiveDao;
import com.biolims.experiment.qa.model.QaTaskReceive;
import com.biolims.experiment.qa.model.QaTaskReceiveTemp;
import com.biolims.experiment.qa.model.QaTaskReceiveItem;
import com.biolims.experiment.qa.service.QaTaskReceiveService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/experiment/qa/qaTaskReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QaTaskReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240206";
	@Autowired
	private QaTaskReceiveService qaTaskReceiveService;
	private QaTaskReceive qaTaskReceive = new QaTaskReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showQaTaskReceiveList")
	public String showQaTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceive.jsp");
	}

	@Action(value = "showQaTaskReceiveListJson")
	public void showQaTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskReceiveService
				.findQaTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QaTaskReceive> list = (List<QaTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qaTaskReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQaTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceiveDialog.jsp");
	}

	@Action(value = "showDialogQaTaskReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQaTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qaTaskReceiveService
				.findQaTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QaTaskReceive> list = (List<QaTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("experiment.qaCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQaTaskReceive")
	public String editQaTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qaTaskReceive = qaTaskReceiveService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qaTaskReceive");
		} else {
			qaTaskReceive.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qaTaskReceive.setReceiveUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			qaTaskReceive.setReceiverDate(stime);
			qaTaskReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qaTaskReceive
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceiveEdit.jsp");
	}

	@Action(value = "copyQaTaskReceive")
	public String copyQaTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qaTaskReceive = qaTaskReceiveService.get(id);
		qaTaskReceive.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceiveEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qaTaskReceive.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QaTaskReceive";
			String markCode = "HSJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qaTaskReceive.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("qaTaskReceiveItem",
				getParameterFromRequest("qaTaskReceiveItemJson"));
		qaTaskReceiveService.save(qaTaskReceive, aMap);
		return redirect("/experiment/qa/qaTaskReceive/editQaTaskReceive.action?id="
				+ qaTaskReceive.getId());

	}

	@Action(value = "viewQaTaskReceive")
	public String toViewQaTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		qaTaskReceive = qaTaskReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceiveEdit.jsp");
	}

	@Action(value = "showQaTaskReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceiveItem.jsp");
	}

	@Action(value = "showQaTaskReceiveItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskReceiveItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qaTaskReceiveService
					.findQaTaskReceiveItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskReceiveItem> list = (List<QaTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleId", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("qaTaskReceive-id", "");
			map.put("qaTaskReceive-name", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQaTaskReceiveItem")
	public void delQaTaskReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaTaskReceiveService.delQaTaskReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
 	/**
	* 去待接收的样本
	* @throws Exception
	 */
	@Action(value = "showQaTaskReceiveItemListTo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQaTaskReceiveItemListTo() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/qa/qaTaskReceiveItemTo.jsp");
	}

	@Action(value = "showQaTaskReceiveItemListToJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQaTaskReceiveItemListToJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, Object> result = qaTaskReceiveService
					.selectQaTaskReceiveTempList(startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QaTaskReceiveTemp> list = (List<QaTaskReceiveTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("inspectDate", "");
			map.put("sampleType", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("reportDate", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("classify", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QaTaskReceiveService getQaTaskReceiveService() {
		return qaTaskReceiveService;
	}

	public void setQaTaskReceiveService(
			QaTaskReceiveService qaTaskReceiveService) {
		this.qaTaskReceiveService = qaTaskReceiveService;
	}

	public QaTaskReceive getQaTaskReceive() {
		return qaTaskReceive;
	}

	public void setQaTaskReceive(QaTaskReceive qaTaskReceive) {
		this.qaTaskReceive = qaTaskReceive;
	}
}
