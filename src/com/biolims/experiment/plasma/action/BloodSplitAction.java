﻿package com.biolims.experiment.plasma.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.plasma.dao.BloodSplitDao;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaTaskTemplate;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/plasma/bloodSplit")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BloodSplitAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";// 240101
	@Autowired
	private BloodSplitService bloodSplitService;
	private PlasmaTask bloodSampleTask = new PlasmaTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private BloodSplitDao bloodSplitDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	
	/**
		 *	明细提交到质检
	     * @Title: submitToQualityTest  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年9月12日
	     * @throws
	 */
	@Action(value = "submitToQualityTest")
	public void submitToQualityTest() throws Exception {
		//主表id
		String id = getParameterFromRequest("id");
		//实验步骤
		String stepNum = getParameterFromRequest("stepNum");
		//模板名称
		String type = getParameterFromRequest("zj");
		//步骤中明细的id
		String[] ids = getRequest().getParameterValues("idList[]");
		//哪个模块提交过去的
		String mark = getParameterFromRequest("mark");
		Map<String ,Object> map = new HashMap<String ,Object>();
		try {
			bloodSplitService.submitToQualityTest(id ,stepNum, mark, type, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
		
	}

	/**
	 * 
	 * @Title: showBloodSplitList @Description:展示主表 @author : shengwei.wang @date
	 * 2018年2月2日下午1:51:32 @return @throws Exception String @throws
	 */
	@Action(value = "showBloodSplitTable")
	public String showBloodSplitList() throws Exception {
		rightsId = "210020302";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplit.jsp");
	}

	@Action(value = "showBloodSplitTableJson")
	public void showBloodSplitListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = bloodSplitService.findBloodSplitTable(start, length, query, col, sort);
			List<PlasmaTask> list = (List<PlasmaTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("state", "");
			map.put("scopeName", "");
//			map.put("opUser", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "showBloodSplitTableByStateJson")
	public void showBloodSplitTableByStateJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			Map<String, Object> result = bloodSplitService.findBloodSplitTableByState(start, length, query, col, sort,user);
			List<PlasmaTask> list = (List<PlasmaTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("state", "");
			map.put("scopeName", "");
			map.put("opUser", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	/**
	 * 
	 * @Title: editBloodSplit @Description: 新建实验单 @author : shengwei.wang @date
	 * 2018年2月2日下午1:51:57 @return @throws Exception String @throws
	 */
	@Action(value = "editBloodSplit")
	public String editBloodSplit() throws Exception {
		rightsId = "210020301";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			bloodSampleTask = bloodSplitService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
			if (bloodSampleTask.getMaxNum() == null) {
				bloodSampleTask.setMaxNum(0);
			}
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			bloodSampleTask.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			bloodSampleTask.setCreateUser(user);
			bloodSampleTask.setMaxNum(0);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			bloodSampleTask.setCreateDate(stime);
			bloodSampleTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			bloodSampleTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<Template> templateList = templateService.showDialogTemplateTableJson("PlasmaTask",null);
		List<Template> selTemplate = new ArrayList<Template>();
		List<UserGroupUser> userList = new ArrayList<UserGroupUser>();
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int j = 0; j < templateList.size(); j++) {
			if(templateList.get(j).getAcceptUser()!=null){
				List<UserGroupUser> userTempList = (List<UserGroupUser>) userGroupUserService.getUserGroupUserBygroupId(templateList.get(j).getAcceptUser().getId())
						.get("list");
				for(UserGroupUser ugu:userTempList){
					if(!userList.contains(ugu)){
						userList.add(ugu);
					}
				}
			}
			if (bloodSampleTask.getTemplate() != null) {
				if (bloodSampleTask.getTemplate().getId().equals(templateList.get(j).getId())) {
					selTemplate.add(templateList.get(j));
					templateList.remove(j);
					j--;
				}
			}
		}
		for (int i = 0; i < userList.size(); i++) {
			if (bloodSampleTask.getTestUserOneId() != null) {
				String [] userOne=bloodSampleTask.getTestUserOneId().split(",");
				one:for(String u:userOne){
					if(u.equals(userList.get(i).getUser().getId())){
						selUser.add(userList.get(i));
						userList.remove(i);
						i--;
						break one;
					}
				}
			}
		}
		putObjToContext("template", templateList);
		putObjToContext("selTemplate", selTemplate);
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		toState(bloodSampleTask.getState());
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplitAllot.jsp");
	}

	/**
	 * 
	 * @Title: showBloodSplitItemTable @Description: 展示待排板列表 @author :
	 * shengwei.wang @date 2018年2月2日下午1:52:14 @return @throws Exception
	 * String @throws
	 */
	@Action(value = "showBloodSplitItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBloodSplitItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		bloodSampleTask = bloodSplitService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplitMakeUp.jsp");
	}

	@Action(value = "showBloodSplitItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBloodSplitItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bloodSplitService.findBloodSplitItemTable(scId, start, length, query, col,
					sort);
			List<PlasmaTaskItem> list = (List<PlasmaTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("chromosomalLocation", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showBloodSplitItemAfTableJson @Description: 排板后样本展示 @author :
	 * shengwei.wang @date 2018年1月17日上午9:24:14 @throws Exception void @throws
	 */

	@Action(value = "showBloodSplitItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBloodSplitItemAfTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = bloodSplitService.findBloodSplitItemAfTable(scId, start, length, query, col,
					sort);
			List<PlasmaTaskItem> list = (List<PlasmaTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("bloodSampleTask-name", "");
			map.put("bloodSampleTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("chromosomalLocation", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("color", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			map.put("pronoun", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delBloodSplitItem @Description: 删除待排板样本 @author : shengwei.wang @date
	 * 2018年1月17日上午10:46:26 @throws Exception void @throws
	 */
	@Action(value = "delBloodSplitItem")
	public void delBloodSplitItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bloodSplitService.delBloodSplitItem(delStr, ids, user, id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: delBloodSplitItemAf @Description: 重新排板 @author : shengwei.wang @date
	 * 2018年1月17日上午10:46:45 @throws Exception void @throws
	 */
	@Action(value = "delBloodSplitItemAf")
	public void delBloodSplitItemAf() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bloodSplitService.delBloodSplitItemAf(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBloodSplitResultTable @Description: 结果表展示 @author :
	 * shengwei.wang @date 2018年1月26日下午1:15:49 @return @throws Exception
	 * String @throws
	 */
	@Action(value = "showBloodSplitResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBloodSplitResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		bloodSampleTask = bloodSplitService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplitResult.jsp");
	}

	@Action(value = "showBloodSplitResultTableJson")
	public void showBloodSplitResultTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = bloodSplitService.showBloodSplitResultTableJson(id, start, length, query, col,
					sort);
			List<SamplePlasmaInfo> list = (List<SamplePlasmaInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("pronoun", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("note", "");
			map.put("bloodSampleTask-id", "");
			map.put("bloodSampleTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("pronoun", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: delBloodSplitResult @Description: 删除结果明细 @author :
	 * shengwei.wang @date 2018年1月26日下午2:36:13 @throws Exception void @throws
	 */
	@Action(value = "delBloodSplitResult")
	public void delBloodSplitResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String bloodSplit_id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			bloodSplitService.delBloodSplitResult(ids, delStr, bloodSplit_id, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBloodSplitSteps @Description: 实验步骤 @author : shengwei.wang @date
	 * 2018年1月24日下午3:55:51 @return @throws Exception String @throws
	 */
	@Action(value = "showBloodSplitSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBloodSplitSteps() throws Exception {
		String id = getParameterFromRequest("id");
		bloodSampleTask = bloodSplitService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String orderNumBy = getParameterFromRequest("orderNum");
		putObjToContext("orderNumBy",orderNumBy);
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplitSteps.jsp");
	}

	@Action(value = "showBloodSplitStepsJson")
	public void showBloodSplitStepsJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = bloodSplitService.showBloodSplitStepsJson(id, orderNum);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBloodSplitReagent")
	public void delBloodSplitReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String bloodSplit_id = getParameterFromRequest("bloodSplit_id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String id = getParameterFromRequest("id");
			bloodSplitService.delBloodSplitReagent(id, delStr, bloodSplit_id, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBloodSplitCos")
	public void delBloodSplitCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String bloodSplit_id = getParameterFromRequest("bloodSplit_id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String id = getParameterFromRequest("id");
			bloodSplitService.delBloodSplitCos(id, delStr, bloodSplit_id, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: makeCode @Description: 打印条码 @author : shengwei.wang @date
	 * 2018年2月2日下午1:53:29 @throws Exception void @throws
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String ip = getParameterFromRequest("ip");
		String id = getParameterFromRequest("id");
		String code = getParameterFromRequest("code");
		String name = getParameterFromRequest("name");
		Socket socket = null;
		Template template = null;
		CodeMain codeMain = null;
		OutputStream os;
		try {
			socket = new Socket();
			// 向服务器端第一次发送字符串

			// 向服务器端第二次发送字符串
			SocketAddress sa = new InetSocketAddress(ip, 9100);
			socket.connect(sa);

			os = socket.getOutputStream();

			char c = 13;
			char b = 10;
			String cb = String.valueOf(c) + String.valueOf(b);
			PlasmaTask pt = bloodSplitService.get(id);
			if (pt != null && pt.getTemplate() != null && pt.getTemplate().getId() != null) {
				template = templateService.get(pt.getTemplate().getId());
			}
			if (template != null && template.getCodeMain() != null && template.getCodeMain().getId() != null) {
				codeMain = codeMainService.get(template.getCodeMain().getId());
			}
			if (codeMain != null) {
				String codeidx = codeMain.getCodex();
				String codeidy = codeMain.getCodey();
				String codenamex = codeMain.getNamex();
				String codenamey = codeMain.getNamey();
				String qrx = codeMain.getQrx();
				String qry = codeMain.getQry();
				// String markCode = codeMain.getCode();
				String printStr = "";
				// for(int i=1;i<5;i++){
				printStr = "e PCX;*" + cb + "e IMG;*" + cb + "mm" + cb + "zO" + cb + "J" + cb + "O R,P" + cb + "H75,0,T"
						+ cb + "D 0.0,0.0" + cb + "M l FNT;/iffs/gb2312" + cb + "F90;gb2312" + cb
						+ "Sl1;0.0,0.0,9.53,9.53,50.90,50.90,1" + cb
						// + "T4.2,4.0,0,90,3.5,q70;" + name + cb +
						// "T4.2,7.6,0,3,2.84,q90;" + id + cb
						+ "T" + codenamex + "," + codenamey + ",0,90,3.5,q70;" + name + cb + "T" + codeidx + ","
						+ codeidy + ",0,3,2.84,q90;" + code + cb + "B" + qrx + "," + qry + ",0,DATAMATRIX,0.33;" + code
						+ cb + "T" + (Float.valueOf(codenamex) + 27) + "," + codenamey + ",0,90,3.5,q70;" + name + cb
						+ "T" + (Float.valueOf(codeidx) + 27) + "," + codeidy + ",0,3,2.84,q90;" + code + cb + "B"
						+ (Float.valueOf(qrx) + 27) + "," + qry + ",0,DATAMATRIX,0.33;" + code + cb + "A 1" + cb;
				// 31.6,2.5
				// }
				os.write(printStr.getBytes("UTF-8"));
			}

			os.flush();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * 
	 * @Title: showBloodSampleTempTable @Description: 展示临时表 @author :
	 * shengwei.wang @date 2018年2月2日下午1:49:24 @return @throws Exception
	 * String @throws
	 */
	@Action(value = "showBloodSampleTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBloodSampleTempTable() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/plasma/bloodTemp.jsp");
	}

	@Action(value = "showBloodSampleTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBloodSampleTempTableJson() throws Exception {
		String[] codes = getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = bloodSplitService.selectBloodSampleTempTable(codes, start, length, query, col,
					sort);
			List<PlasmaTaskTemp> list = (List<PlasmaTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("orderCode", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-project-id", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-changeType", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配页面 @author : shengwei.wang @date
	 * 2017年12月29日下午1:58:37 void @throws
	 */
	@Action(value = "saveAllot")
	public void saveAllot() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = bloodSplitService.saveAllot(main, tempId, userId, templateId, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排版界面 @author : shengwei.wang @date
	 * 2018年1月16日下午1:30:34 @throws Exception void @throws
	 */
	@Action(value = "saveMakeUp")
	public void saveMakeUp() throws Exception {
		String blood_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			bloodSplitService.saveMakeUp(blood_id, item, logInfo);
			result.put("success", true);
			result.put("id", blood_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
		 *	步骤中明细选择下一步流向质检之后的保存
	     * @Title: saveBloodSplitItem  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月31日
	     * @throws
	 */
	@Action(value = "saveBloodSplitItem")
	public void saveBloodSplitItem() throws Exception {
		String id = getParameterFromRequest("id");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			bloodSplitService.saveBloodSplitItem(id, itemJson);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveLeftQuality")
	public void saveLeftQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			PlasmaTask plasmaTask = bloodSplitService.get(id);
			bloodSplitService.saveLeftQuality(plasmaTask, ids, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveRightQuality")
	public void saveRightQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			PlasmaTask plasmaTask = bloodSplitService.get(id);
			bloodSplitService.saveRightQuality(plasmaTask, ids, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : shengwei.wang @date
	 * 2018年2月2日下午1:55:27 @throws Exception void @throws
	 */
	@Action(value = "saveSteps")
	public void saveSteps() throws Exception {
		String id = getParameterFromRequest("id");
		String templateJson = getParameterFromRequest("templateJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			bloodSplitService.saveSteps(id, templateJson, reagentJson, cosJson, logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	/**
	 * 
	 * @Title: saveResult @Description: 保存结果表 @author : shengwei.wang @date
	 * 2018年1月29日下午5:09:24 @throws Exception void @throws
	 */
	@Action(value = "saveResult")
	public void saveResult() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		String confirmUser=getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			bloodSplitService.saveResult(id, dataJson, logInfo,confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate @Description: 展示排版 @author : shengwei.wang @date
	 * 2018年1月16日下午2:30:17 void @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<PlasmaTaskItem> json = bloodSplitService.showWellPlate(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : shengwei.wang @date
	 * 2018年1月17日上午10:07:26 @throws Exception void @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			bloodSplitService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : shengwei.wang @date
	 * 2018年1月29日下午5:09:46 @throws Exception void @throws
	 */
	@Action(value = "plateSample")
	public void plateSample() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = bloodSplitService.plateSample(id, counts);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description:展示孔板样本列表 @author : shengwei.wang @date
	 * 2018年2月2日下午1:54:47 @throws Exception void @throws
	 */
	@Action(value = "plateSampleTable")
	public void plateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = bloodSplitService.plateSampleTable(id, counts, start, length, query, col, sort);
			List<PlasmaTaskItem> list = (List<PlasmaTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("nextId", "");
			map.put("nextName", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("bloodSampleTask-name", "");
			map.put("bloodSampleTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			//质检字段
			map.put("qab", "");
			map.put("qaw1", "");
			map.put("qbw2", "");
			map.put("qbzy1", "");
			map.put("qbzy2", "");
			map.put("qcw1", "");
			map.put("qcNum", "");
			map.put("qchl", "");
			map.put("qmd", "");
			map.put("qw1", "");
			map.put("qw3", "");
			map.put("qw4", "");
			map.put("qw42", "");
			map.put("q5", "");
			map.put("q6Num", "");
			map.put("q6hl", "");
			map.put("q6w", "");
			map.put("qzy1", "");
			map.put("qzy2", "");
			map.put("qds", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: uploadCsvFile @Description: 上传结果 @author : shengwei.wang @date
	 * 2018年2月2日下午1:54:32 @throws Exception void @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			bloodSplitService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : shengwei.wang @date
	 * 2018年1月29日下午5:10:10 @throws Exception void @throws
	 */
	@Action(value = "bringResult")
	public void bringResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			bloodSplitService.bringResult(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: generateBlendCode @Description: 生成混合号 @author : shengwei.wang @date
	 * 2018年3月6日上午11:02:26 void @throws
	 */
	@Action(value = "generateBlendCode")
	public void generateBlendCode() {
		String id = getParameterFromRequest("id");
		try {
			Integer blendCode = bloodSplitService.generateBlendCode(id);
			HttpUtils.write(String.valueOf(blendCode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @throws Exception
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : shengwei.wang @date
	 * 2018年3月22日下午5:39:40 void @throws
	 */
	@Action(value = "submitSample")
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			bloodSplitService.submitSample(id, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "findPlasmaTaskTemplateList")
	public void findPlasmaTaskTemplateList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String ids = getParameterFromRequest("id");
		
		Map<String, Object> result = new HashMap<String, Object>();
		result = bloodSplitService.findPlasmaTaskTemplateList(start, length, query, col, sort,ids);
		List<PlasmaTaskTemplate> list = (List<PlasmaTaskTemplate>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("testUserList", "");
		map.put("bloodSampleTask-id", "");
		map.put("estimatedTime", "");
		map.put("endTime", "");
		map.put("startTime", "");
		map.put("contentData", "");
		map.put("content", "");
		map.put("note", "");
		map.put("orderNum", "");
		map.put("estimatedDate", "");
		map.put("sampleDeteyion-id", "");
		map.put("sampleDeteyion-name", "");
		map.put("planEndDate", "");
		map.put("zjResult", "");
		map.put("planWorkDate", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	/**
	 * 
	 * @Title: downLoadTemp @Description: TODO(核算提取模板下载) @param @param
	 * ids @param @throws Exception    设定文件 @return void    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-8-22 上午11:46:55 @throws
	 */
	@Action(value = "downLoadTemp")
	public void downLoadTemp() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids");
		String[] codes = getRequest().getParameterValues("codes");
		String id = ids[0];
		String code = codes[0];
		String[] str1 = code.split(",");
		String co = str1[0];
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = co + sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		properties.load(is);

		String filePath = properties.getProperty("result.template.file") + "\\";// 写入csv路径
		String fileName = filePath + a + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "样本编号", "原始样本编号", "产物类型", "检测项目", "浓度"
				 , "体积" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		String[] sid = id.split(",");
		for (int j = 0; j < sid.length; j++) {
			String idc = sid[j];
			for (int i = 0; i < ids.length; i++) {
				SamplePlasmaInfo sr=bloodSplitService.getInfoById(idc);
				StringBuffer sb = new StringBuffer();
				setMolecularMarkersData(sr, sb);
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
				if (sr.equals("")) {
					result.put("success", false);
				} else {
					result.put("success", true);

				}
			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3(a, filePath);
	}

	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData @Description: TODO(将对应的值添加到模板里) @param @param
	 * sr @param @param sb @param @throws Exception    设定文件 @return void   
	 * 返回类型 @author zhiqiang.yang@biolims.cn @date 2017-8-22 下午1:21:58 @throws
	 */
	public void setMolecularMarkersData(SamplePlasmaInfo sr, StringBuffer sb) throws Exception {
		if (null!=sr.getCode()) {
			sb.append("\"").append(sr.getCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null!=sr.getSampleCode()) {
			sb.append("\"").append(sr.getSampleCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		
		if (null!=sr.getDicSampleType().getName()) {
			sb.append("\"").append(sr.getDicSampleType().getName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		
		if (null!=sr.getProductName()) {
			sb.append("\"").append(sr.getProductName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
	}

	/**
	 * 
	 * @Title: toUtf8String @Description: TODO(解决乱码) @param @param
	 * s @param @return    设定文件 @return String    返回类型 @author
	 * zhiqiang.yang@biolims.cn @date 2017-8-23 下午4:40:07 @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}
	
	
	
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BloodSplitService getBloodSplitService() {
		return bloodSplitService;
	}

	public void setBloodSplitService(BloodSplitService bloodSplitService) {
		this.bloodSplitService = bloodSplitService;
	}

	public PlasmaTask getBloodSplit() {
		return bloodSampleTask;
	}

	public void setBloodSplit(PlasmaTask bloodSampleTask) {
		this.bloodSampleTask = bloodSampleTask;
	}

	public BloodSplitDao getBloodSplitDao() {
		return bloodSplitDao;
	}

	public void setBloodSplitDao(BloodSplitDao bloodSplitDao) {
		this.bloodSplitDao = bloodSplitDao;
	}

}
