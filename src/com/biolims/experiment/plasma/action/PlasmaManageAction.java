﻿package com.biolims.experiment.plasma.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.plasma.model.PlasmaManage;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.experiment.plasma.service.PlasmaManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/plasma/plasmaManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class PlasmaManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240103";
	@Autowired
	private PlasmaManageService plasmaManageService;
	private PlasmaManage plasmaManage = new PlasmaManage();
	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 
	 * @Title: plasmaManageItemRuku
	 * @Description: 样本管理入库
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:14:06
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "plasmaManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			plasmaManageService.plasmaManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: showBloodSplitManage
	 * @Description: 样本管理
	 * @author : shengwei.wang
	 * @date 2018年1月30日上午9:53:00
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showBloodSplitManage")
	public String showBloodSplitManage() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplitManage.jsp");
	}

	@Action(value = "showBloodSplitManageJson")
	public void showBloodSplitManageJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = plasmaManageService
					.showBloodSplitManageJson(start, length, query, col, sort);
			List<PlasmaTaskItem> list = (List<PlasmaTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("bloodSampleTask-name", "");
			map.put("bloodSampleTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public PlasmaManageService getPlasmaManageService() {
		return plasmaManageService;
	}

	public void setPlasmaManageService(PlasmaManageService plasmaManageService) {
		this.plasmaManageService = plasmaManageService;
	}

	public PlasmaManage getPlasmaManage() {
		return plasmaManage;
	}

	public void setPlasmaManage(PlasmaManage plasmaManage) {
		this.plasmaManage = plasmaManage;
	}

	
}
