﻿package com.biolims.experiment.plasma.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.plasma.dao.SampleReceiveExDao;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.plasma.service.SampleReceiveExService;
import com.biolims.experiment.quality.model.QualityTest;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.file.service.FileInfoService;

@Namespace("/experiment/plasma/sampleReceiveEx")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleReceiveExAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240104";
	@Autowired
	private SampleReceiveExService sampleReceiveExService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveExDao sampleReceiveExDao;
	
	/**
	 * 
	 * @Title: showSampleReceiveItemList  
	 * @Description: 展示待接收样本 
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:04:04
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "showSampleReceiveItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleReceiveItemList() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/bloodSplitReceive.jsp");
	}

	@Action(value = "showSampleReceiveItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReceiveItemListJson() throws Exception {
		String [] codes=getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = sampleReceiveExService
//					.showSampleReceiveItemListJson(codes,scId,start, length, query, col, sort);
			Map<String, Object> result = sampleReceiveExService
					.showSampleReceiveItemListJson(codes,scId,start, length, query, col, sort, type);
			List<PlasmaReceiveItem> list = (List<PlasmaReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("sampleOrder-id", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("concentration", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			
			map.put("packageResult", "");
			map.put("bagsNum", "");
			map.put("releaseAuditId", "");
			
			
			map.put("batchNumber", "");
			map.put("specifications", "");
			map.put("inspector", "");
			map.put("testingDate", "yyyy-MM-dd");
			map.put("reviewer", "");
			map.put("reviewDate", "yyyy-MM-dd");
			
			
			map.put("auditPass", "");
			
			map.put("cellNumber", "");
			map.put("sampleOrder-crmCustomer-name", "");
			map.put("sampleOrder-inspectionDepartment-name", "");
			
			map.put("inspectorOne", "");
			map.put("testingDateOne", "yyyy-MM-dd");
			map.put("auditPassOne", "");
			map.put("inspectorTwo", "");
			map.put("testingDateTwo", "yyyy-MM-dd");
			map.put("auditPassTwo", "");
			map.put("inspectorThree", "");
			map.put("testingDateThree", "yyyy-MM-dd");
			map.put("auditPassThree", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @Title: saveBloodSplitReceive  
	 * @Description: 保存  
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:03:42
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="saveBloodSplitReceive")
	public void saveBloodSplitReceive() throws Exception{
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String data=getParameterFromRequest("dataJson");
		String changeLog=getParameterFromRequest("changeLog");
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			sampleReceiveExService.saveBloodSplitReceive(user,data,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 确认放行审核
	 * @throws
	 */
	@Action(value="confirmReinfusion")
	public void confirmReinfusion() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			String zt=getParameterFromRequest("zt");
			sampleReceiveExService.confirmReinfusion(ids,zt);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 放行审核不通过
	 * @throws
	 */
	@Action(value="confirmReinfusionbu")
	public void confirmReinfusionbu() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		String zt=getParameterFromRequest("zt");
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			sampleReceiveExService.confirmReinfusionbu(ids,zt);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: executeReceive  
	 * @Description:执行  
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:03:51
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="executeReceive")
	public void executeReceive() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			sampleReceiveExService.executeReceive(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: executeReceive  
	 * @Description:执行  
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:03:51
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="toEditReleaseAudit")
	public void toEditReleaseAudit() throws Exception{
		String id = getParameterFromRequest("id");
		String itemId = getParameterFromRequest("itemId");
		String code = getParameterFromRequest("code");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			if(id==null||"".equals(id)||"NEW".equals(id)){
				id = sampleReceiveExService.editReleaseAudit(id, itemId, code);
			}
			map.put("id", id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title:   
	 * @Description: 显示批处理打印页面 
	 * @author : 
	 * @date 
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="showPiChuLi")
	public String showPiChuLi() {
		String ordernum = getParameterFromRequest("ordernum");
		putObjToContext("ordernum", ordernum);
		return dispatcher("/WEB-INF/page/experimentLab/bloodSplit/showPiChuLiDialog.jsp");
	}
	
	/**
	 * 
	 * @Title:   查询接样单
	 * @Description: 
	 * @author : 
	 * @date 
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="findsamplereceive")
	public void findsamplereceive() throws Exception{
		String id = getParameterFromRequest("id");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
//			List<SampleReceiveItem> sris = sampleReceiveExService.findsamplereceivebyordernum(id);
//			List<SampleReceive> sris = sampleReceiveExService.findsamplereceivebyordernum1(id);
			List<SampleReceiveItem> sris = sampleReceiveExService.findsamplereceivebyordernum2(id);
			map.put("sris", sris);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title:   查询实验单及步骤 （目前是按单实验做的）
	 * @Description: 
	 * @author : 
	 * @date 
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="findExeperiment")
	public void findExeperiment() throws Exception{
		String id = getParameterFromRequest("id");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			List<CellPassageTemplate> cpt = null;
			List<CellPassageItem> cpi = sampleReceiveExDao.finexeperiment(id);
			if(cpi.size()>0){
				CellPassage cp = cpi.get(0).getCellPassage();
				cpt = sampleReceiveExDao.finexeperimenttemplate(cp.getId(),cpi.get(0).getStepNum());
				map.put("cpid", cp.getId());
				map.put("cpt", cpt);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title:   质检报告单，产品，按订单号模糊查询
	 * @Description: 
	 * @author : 
	 * @date 
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="findExeperimentResult")
	public void findExeperimentResult() throws Exception{
		String id = getParameterFromRequest("id");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
//			List<QualityTest> qt = sampleReceiveExDao.finexeperimentResult(id);
			List<QualityTestInfo> qt = sampleReceiveExDao.finexeperimentResults(id);
			if(qt.size()>0){
				map.put("qt", qt);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleReceiveExService getSampleReceiveExService() {
		return sampleReceiveExService;
	}

	public void setSampleReceiveExService(
			SampleReceiveExService sampleReceiveExService) {
		this.sampleReceiveExService = sampleReceiveExService;
	}
	
}
