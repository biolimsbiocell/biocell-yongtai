package com.biolims.experiment.plasma.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;

/**
 * @Title: Model
 * @Description: 样本接收明细
 * @author lims-platform
 * @date 2015-11-18 17:00:52
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PLASMA_RECEIVE_ITEM")
@SuppressWarnings("serial")
public class PlasmaReceiveItem extends EntityDao<PlasmaReceiveItem> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 样本编号 */
	private String sampleCode;
	/** 接收人 */
	private User createUser;
	/** 接收时间 */
	private Date createDate;
	/** 姓名 */
	private String patientName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 储位 */
	private StoragePosition storage;
	/** 工作流状态 */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 结果，肉眼观察是否合格 */
	private String result;
	/** 原因 */
	private String reason;
	/** 备注 */
	private String note;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 浓度 */
	private Double concentration;
	/** 应出报告日期 */
	private Date reportDate;
	/** 任务单Id */
	private String orderId;
	/** 下一步流向编号 */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 临床 1 科研 2*/
	private String classify;
	/** 样本类型 */
	private String sampleType;
	/** 样本数量*/
	private String sampleNum;
	/** 样本主数据 */
	private SampleInfo sampleInfo;

	/** scopeId */
	private String scopeId;
	/** scopeName */
	private String scopeName;
	/**关联订单*/
	private SampleOrder sampleOrder;
	
	/** 包装是否完整 */
	private String packageResult;
	/** 回输袋数 */
	private String bagsNum;

	/** 放行审核id */
	private String releaseAuditId;
	
	/** 批号 */
	private String batchNumber;
	/** 规格 */
	private String specifications;
	/** 检验人 */
	private String inspector;
	/** 检测日期 */
	private Date testingDate;
	/** 复核人 */
	private String reviewer;
	/** 复核日期 */
	private Date reviewDate;
	
	
	/** 放行审核是否通过 */
	private String auditPass;
	/** 细胞数量 */
	private String cellNumber;
	
	
	/** 质检是否通过 */
	private String qualityTestPass;
	
	/** 第一次审核人 */
	private String inspectorOne;
	/** 第一次审核日期 */
	private Date testingDateOne;
	/** 第一次审核结果 */
	private String auditPassOne;
	/** 第二次审核人 */
	private String inspectorTwo;
	/** 第二次审核日期 */
	private Date testingDateTwo;
	/** 第二次审核结果 */
	private String auditPassTwo;
	/** 第三次审核人 */
	private String inspectorThree;
	/** 第三次审核日期 */
	private Date testingDateThree;
	/** 第三次审核结果 */
	private String auditPassThree;
	
	
	
	
	public String getInspectorOne() {
		return inspectorOne;
	}

	public void setInspectorOne(String inspectorOne) {
		this.inspectorOne = inspectorOne;
	}

	public Date getTestingDateOne() {
		return testingDateOne;
	}

	public void setTestingDateOne(Date testingDateOne) {
		this.testingDateOne = testingDateOne;
	}

	public String getAuditPassOne() {
		return auditPassOne;
	}

	public void setAuditPassOne(String auditPassOne) {
		this.auditPassOne = auditPassOne;
	}

	public String getInspectorTwo() {
		return inspectorTwo;
	}

	public void setInspectorTwo(String inspectorTwo) {
		this.inspectorTwo = inspectorTwo;
	}

	public Date getTestingDateTwo() {
		return testingDateTwo;
	}

	public void setTestingDateTwo(Date testingDateTwo) {
		this.testingDateTwo = testingDateTwo;
	}

	public String getAuditPassTwo() {
		return auditPassTwo;
	}

	public void setAuditPassTwo(String auditPassTwo) {
		this.auditPassTwo = auditPassTwo;
	}

	public String getInspectorThree() {
		return inspectorThree;
	}

	public void setInspectorThree(String inspectorThree) {
		this.inspectorThree = inspectorThree;
	}

	public Date getTestingDateThree() {
		return testingDateThree;
	}

	public void setTestingDateThree(Date testingDateThree) {
		this.testingDateThree = testingDateThree;
	}

	public String getAuditPassThree() {
		return auditPassThree;
	}

	public void setAuditPassThree(String auditPassThree) {
		this.auditPassThree = auditPassThree;
	}

	public String getQualityTestPass() {
		return qualityTestPass;
	}

	public void setQualityTestPass(String qualityTestPass) {
		this.qualityTestPass = qualityTestPass;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String getAuditPass() {
		return auditPass;
	}

	public void setAuditPass(String auditPass) {
		this.auditPass = auditPass;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public String getInspector() {
		return inspector;
	}

	public void setInspector(String inspector) {
		this.inspector = inspector;
	}

	public Date getTestingDate() {
		return testingDate;
	}

	public void setTestingDate(Date testingDate) {
		this.testingDate = testingDate;
	}

	public String getReviewer() {
		return reviewer;
	}

	public void setReviewer(String reviewer) {
		this.reviewer = reviewer;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getReleaseAuditId() {
		return releaseAuditId;
	}

	public void setReleaseAuditId(String releaseAuditId) {
		this.releaseAuditId = releaseAuditId;
	}

	public String getPackageResult() {
		return packageResult;
	}

	public void setPackageResult(String packageResult) {
		this.packageResult = packageResult;
	}

	public String getBagsNum() {
		return bagsNum;
	}

	public void setBagsNum(String bagsNum) {
		this.bagsNum = bagsNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得StoragePosition
	 * 
	 * @return: StoragePosition 储位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE")
	public StoragePosition getStorage() {
		return this.storage;
	}

	/**
	 * 方法: 设置StoragePosition
	 * 
	 * @param: StoragePosition 储位
	 */
	public void setStorage(StoragePosition storage) {
		this.storage = storage;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	// 工作流状态
	@Column(name = "STATE_NAME", length = 20)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "REASON", length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	/**
	 * @return the nextFlow
	 */
	public String getNextFlow() {
		return nextFlow;
	}

	/**
	 * @param nextFlow
	 *            the nextFlow to set
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * @return the nextFlowId
	 */
	public String getNextFlowId() {
		return nextFlowId;
	}

	/**
	 * @param nextFlowId
	 *            the nextFlowId to set
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * @return the createUser
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser
	 *            the createUser to set
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}