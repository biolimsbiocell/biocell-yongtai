﻿package com.biolims.experiment.plasma.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2015-11-18 17:00:24
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PLASMA_TASK_REAGENT")
@SuppressWarnings("serial")
public class PlasmaTaskReagent extends EntityDao<PlasmaTaskReagent> implements
		java.io.Serializable {
	/** 试剂id */
	private String id;
	/** 试剂编号 */
	private String code;
	/** 试剂名称 */
	private String name;
	/** 批次 */
	private String batch;
	/** 是否通过检验 */
	private String isGood;
	/** 备注 */
	private String note;
	/** 单个用量 */
	private Double oneNum;
	/** 样本用量 */
	private Double sampleNum;
	/** 用量 */
	private Double num;
	/** 数量 */
	private Integer count;
	/** sn */
	private String sn;
	/** 过期日期 */
	private Date expireDate;
	/** 是否用完 */
	private String isRunout;
	/** 关联步骤编号 */
	private String itemId;
	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getIsRunout() {
		return isRunout;
	}

	public void setIsRunout(String isRunout) {
		this.isRunout = isRunout;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Double getOneNum() {
		return oneNum;
	}

	public void setOneNum(Double oneNum) {
		this.oneNum = oneNum;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	private String tReagent;

	public String gettReagent() {
		return tReagent;
	}

	public void settReagent(String tReagent) {
		this.tReagent = tReagent;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂id
	 */
	/** 相关主表 */
	private PlasmaTask bloodSampleTask;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BLOOD_SAMPLE_TASK")
	public PlasmaTask getBloodSampleTask() {
		return this.bloodSampleTask;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setBloodSampleTask(PlasmaTask bloodSampleTask) {
		this.bloodSampleTask = bloodSampleTask;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂名称
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批次
	 */
	@Column(name = "BATCH", length = 50)
	public String getBatch() {
		return this.batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批次
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 关联主表
	 */
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}