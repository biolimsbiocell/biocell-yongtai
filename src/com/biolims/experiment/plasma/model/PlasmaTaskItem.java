package com.biolims.experiment.plasma.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 血浆分离（样本处理）明细
 * @author lims-platform
 * @date 2015-11-24 14:42:07
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PLASMA_TASK_ITEM")
@SuppressWarnings("serial")
public class PlasmaTaskItem extends EntityDao<PlasmaTaskItem> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 血浆编号 */
	private String code;
	/** 样本编号 */
	private String sampleCode;
	/** 患者姓名 */
	private String patientName;
	/** 核对 */
	private String checked;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 染色体位置 */
	private String chromosomalLocation;
	/** 状态  */
	private String state;
	/** 备注 */
	private String note;
	/** 储位 */
	private StoragePosition storage;
	/** 浓度 */
	private Double concentration;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本用量 */
	private Double sampleConsume;
	/** 相关主表 */
	private PlasmaTask bloodSampleTask;
	/** 体积 */
	private Double volume;
	/** 取样日期 */
	private String inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	/** 任务单id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 排序号 */
	private Integer orderNumber;
	/** 位置 */
	private String posId;
	/** 板号 */
	private String counts;
	
	/** 临床 1 科研 2 */
	private String classify;
	/** 中间产物数量 */
	private String productNum;
	/** 中间产物类型编号 */
	private String dicSampleTypeId;
	/** 中间产物类型 */
	private String dicSampleTypeName;
	/** 样本类型 */
	private String sampleType;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/**混合号 */
	private Integer blendCode;
	/**背景色 */
	private String color;
	/**是否出库 */
	private String isOut;
	
	/** scopeId */
	private String scopeId;
	/** scopeName */
	private String scopeName;
	/** 父级 */
	private String parentId;
	/**关联订单*/
	private SampleOrder sampleOrder;
	/**PBMC分离: A（Q01A01）表型检查*/
	private String qab;
	/**PBMC分离: A（Q01A02）无菌1*/
	private String qaw1;
	/** PBMC分离:B（Q01B01）无菌2*/
	private String qbw2;
	/** PBMC分离:B Q01B02）支原体1*/
	private String qbzy1;
	/**PBMC分离:B（Q01B03）支原体2*/
	private String qbzy2;
	/**PBMC分离:C（Q01C01）无菌1*/
	private String qcw1;
	/**PBMC分离:C（Q01C02）数量*/
	private String qcNum;
	/**PBMC分离:C（Q01C03）活率*/
	private String qchl;
	/**装袋培养:（Q0201）细胞密度*/
	private String qmd;
	/**装袋培养:（Q0202）无菌1*/
	private String qw1;
	/**分袋培养:（Q0301）无菌1 */
	private String qw3;
	/**分袋培养1（Q0401）无菌1*/
	private String qw4;
	//这里两个字段名是一样的 但是显示的值不同
	/**分袋培养1（Q0401）无菌1*/
	private String qw42;
	/**（Q0502）表型检测  */
	private String q5;
	/**（Q0601）细胞数量*/
	private String q6Num;
	/**（Q0602）活率*/
	private String q6hl;
	/**（（Q0603）无菌2*/
	private String q6w;
	/**（Q0604）支原体1*/
	private String qzy1;
	/**（Q0605）支原体2*/
	private String qzy2;
	/**（Q0606）细菌内毒素  */
	private String qds  ;
	/**下一步流向*/
	private String nextId;
	private String nextName;
	/**用于判断是否提交*/
	private String submit;
	/**代次*/
	private String pronoun;
	
	
	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}
	
	
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getNextId() {
		return nextId;
	}

	public void setNextId(String nextId) {
		this.nextId = nextId;
	}

	public String getNextName() {
		return nextName;
	}

	public void setNextName(String nextName) {
		this.nextName = nextName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}


	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对
	 */
	@Column(name = "CHECKED", length = 60)
	public String getChecked() {
		return this.checked;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 36)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得Sting
	 * 
	 * @return: Sting 备注
	 */
	@Column(name = "NOTE", length = 36)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得BloodSplit
	 * 
	 * @return: BloodSplit 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BLOOD_SAMPLE_TASK")
	public PlasmaTask getBloodSampleTask() {
		return this.bloodSampleTask;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setBloodSampleTask(PlasmaTask bloodSampleTask) {
		this.bloodSampleTask = bloodSampleTask;
	}


	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得StoragePosition
	 * 
	 * @return: StoragePosition 储位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE")
	public StoragePosition getStorage() {
		return this.storage;
	}

	/**
	 * 方法: 设置StoragePosition
	 * 
	 * @param: StoragePosition 储位
	 */
	public void setStorage(StoragePosition storage) {
		this.storage = storage;
	}

	@Column(name = "CONCENTRATION", length = 100)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}


	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/** 
	 * @return orderCode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getOrderCode() {
		return orderCode;
	}

	/**
	 * @param orderCode the orderCode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}


	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}


	public Double getSampleConsume() {
		return sampleConsume;
	}

	public void setSampleConsume(Double sampleConsume) {
		this.sampleConsume = sampleConsume;
	}


	public String getDicSampleTypeId() {
		return dicSampleTypeId;
	}

	public void setDicSampleTypeId(String dicSampleTypeId) {
		this.dicSampleTypeId = dicSampleTypeId;
	}

	public String getDicSampleTypeName() {
		return dicSampleTypeName;
	}

	public void setDicSampleTypeName(String dicSampleTypeName) {
		this.dicSampleTypeName = dicSampleTypeName;
	}


	/**
	 * @return the posId
	 */
	public String getPosId() {
		return posId;
	}

	/**
	 * @param posId the posId to set
	 */
	public void setPosId(String posId) {
		this.posId = posId;
	}

	/**
	 * @return the isOut
	 */
	public String getIsOut() {
		return isOut;
	}

	/**
	 * @param isOut the isOut to set
	 */
	public void setIsOut(String isOut) {
		this.isOut = isOut;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the chromosomalLocation
	 */
	public String getChromosomalLocation() {
		return chromosomalLocation;
	}

	/**
	 * @param chromosomalLocation the chromosomalLocation to set
	 */
	public void setChromosomalLocation(String chromosomalLocation) {
		this.chromosomalLocation = chromosomalLocation;
	}

	/**
	 * @return the blendCode
	 */
	public Integer getBlendCode() {
		return blendCode;
	}

	/**
	 * @param blendCode the blendCode to set
	 */
	public void setBlendCode(Integer blendCode) {
		this.blendCode = blendCode;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getQab() {
		return qab;
	}

	public String getQaw1() {
		return qaw1;
	}

	public String getQbw2() {
		return qbw2;
	}

	public String getQbzy1() {
		return qbzy1;
	}

	public String getQbzy2() {
		return qbzy2;
	}

	public String getQcw1() {
		return qcw1;
	}

	public String getQcNum() {
		return qcNum;
	}

	public String getQchl() {
		return qchl;
	}

	public String getQmd() {
		return qmd;
	}

	public String getQw1() {
		return qw1;
	}

	public String getQw3() {
		return qw3;
	}

	public String getQw4() {
		return qw4;
	}

	public String getQ5() {
		return q5;
	}

	public String getQ6Num() {
		return q6Num;
	}

	public String getQ6hl() {
		return q6hl;
	}

	public String getQ6w() {
		return q6w;
	}

	public String getQzy1() {
		return qzy1;
	}

	public String getQzy2() {
		return qzy2;
	}

	public String getQds() {
		return qds;
	}

	public void setQab(String qab) {
		this.qab = qab;
	}

	public void setQaw1(String qaw1) {
		this.qaw1 = qaw1;
	}

	public void setQbw2(String qbw2) {
		this.qbw2 = qbw2;
	}

	public void setQbzy1(String qbzy1) {
		this.qbzy1 = qbzy1;
	}

	public void setQbzy2(String qbzy2) {
		this.qbzy2 = qbzy2;
	}

	public void setQcw1(String qcw1) {
		this.qcw1 = qcw1;
	}

	public void setQcNum(String qcNum) {
		this.qcNum = qcNum;
	}

	public void setQchl(String qchl) {
		this.qchl = qchl;
	}

	public void setQmd(String qmd) {
		this.qmd = qmd;
	}

	public void setQw1(String qw1) {
		this.qw1 = qw1;
	}

	public void setQw3(String qw3) {
		this.qw3 = qw3;
	}

	public void setQw4(String qw4) {
		this.qw4 = qw4;
	}

	public void setQ5(String q5) {
		this.q5 = q5;
	}

	public void setQ6Num(String q6Num) {
		this.q6Num = q6Num;
	}

	public void setQ6hl(String q6hl) {
		this.q6hl = q6hl;
	}

	public void setQ6w(String q6w) {
		this.q6w = q6w;
	}

	public void setQzy1(String qzy1) {
		this.qzy1 = qzy1;
	}

	public void setQzy2(String qzy2) {
		this.qzy2 = qzy2;
	}

	public void setQds(String qds) {
		this.qds = qds;
	}

	public String getQw42() {
		return qw42;
	}

	public void setQw42(String qw42) {
		this.qw42 = qw42;
	}
	
}