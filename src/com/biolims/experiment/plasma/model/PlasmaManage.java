package com.biolims.experiment.plasma.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 血浆管理
 * @author lims-platform
 * @date 2015-11-18 16:59:51
 * @version V1.0   
 *
 */
@Entity
@Table(name = "PLASMA_MANAGE")
@SuppressWarnings("serial")
public class PlasmaManage extends EntityDao<PlasmaManage> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**血浆编号*/
	private String code;
	/**样本编号*/
	private String sampleCode;
	/**名称*/
	private String name;
	/**体积*/
	private Double bulk;
	/**结果判定*/
	private String resultDecide;
	/**下一步流向*/
	private String nextFlow;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  血浆编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  血浆编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  体积
	 */
	@Column(name ="BULK", length = 50)
	public Double getBulk(){
		return this.bulk;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  体积
	 */
	public void setBulk(Double bulk){
		this.bulk = bulk;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="RESULT_DECIDE", length = 50)
	public String getResultDecide(){
		return this.resultDecide;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setResultDecide(String resultDecide){
		this.resultDecide = resultDecide;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 200)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	
}