package com.biolims.experiment.plasma.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.model.PlasmaTaskCos;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.experiment.plasma.model.PlasmaTaskReagent;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaTaskTemplate;
import com.biolims.experiment.plasma.model.PlasmaTaskZhiJian;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class BloodSplitDao extends BaseHibernateDao {

	public Map<String, Object> findBloodSplitTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PlasmaTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}else{
				key+=" order by state desc ";
			}
			List<PlasmaTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectBloodSampleTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaTaskTemp where 1=1 and state='1'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		
		if(codes!=null&&!codes.equals("")){
			key+=" and code in (:alist)";
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			List<PlasmaTaskTemp> list=null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from PlasmaTaskTemp  where 1=1 and state='1'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			if(codes!=null&&!codes.equals("")){
				list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
			}else{
				list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public Map<String, Object> findBloodSplitItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaTaskItem where 1=1 and state='1' and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PlasmaTaskItem  where 1=1 and state='1' and bloodSampleTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PlasmaTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<PlasmaTaskItem> showWellPlate(String id) {
		String hql="from PlasmaTaskItem  where 1=1 and state='2' and  bloodSampleTask.id='"+id+"'";
		List<PlasmaTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findBloodSplitItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaTaskItem where 1=1 and state='2' and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PlasmaTaskItem  where 1=1 and state='2' and bloodSampleTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PlasmaTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from PlasmaTaskItem where 1=1 and state='2' and bloodSampleTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from PlasmaTaskItem where 1=1 and state='2' and bloodSampleTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<PlasmaTaskTemplate> showBloodSplitStepsJson(String id,String orderNum) {

		String countHql = "select count(*) from PlasmaTaskTemplate where 1=1 and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(orderNum!=null&&!"".equals(orderNum)){
			key+=" and orderNum='"+orderNum+"'";
		}else{
		}
		List<PlasmaTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PlasmaTaskTemplate where 1=1 and bloodSampleTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PlasmaTaskReagent> showBloodSplitReagentJson(String id,String orderNum) {
		String countHql = "select count(*) from PlasmaTaskReagent where 1=1 and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(orderNum!=null&&!"".equals(orderNum)){
			key+=" and itemId='"+orderNum+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<PlasmaTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PlasmaTaskReagent where 1=1 and bloodSampleTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PlasmaTaskCos> showBloodSplitCosJson(String id,String orderNum) {
		String countHql = "select count(*) from PlasmaTaskCos where 1=1 and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(orderNum!=null&&!"".equals(orderNum)){
			key+=" and itemId='"+orderNum+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<PlasmaTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from PlasmaTaskCos where 1=1 and bloodSampleTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<PlasmaTaskTemplate> delTemplateItem(String id) {
		List<PlasmaTaskTemplate> list=new ArrayList<PlasmaTaskTemplate>();
		String hql = "from PlasmaTaskTemplate where 1=1 and bloodSampleTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<PlasmaTaskReagent> delReagentItem(String id) {
		List<PlasmaTaskReagent> list=new ArrayList<PlasmaTaskReagent>();
		String hql = "from PlasmaTaskReagent where 1=1 and bloodSampleTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<PlasmaTaskCos> delCosItem(String id) {
		List<PlasmaTaskCos> list=new ArrayList<PlasmaTaskCos>();
		String hql = "from PlasmaTaskCos where 1=1 and bloodSampleTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showBloodSplitResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SamplePlasmaInfo where 1=1 and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SamplePlasmaInfo  where 1=1 and bloodSampleTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SamplePlasmaInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		String hql="select counts,count(*) from PlasmaTaskItem where 1=1 and bloodSampleTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
		return list;
	}

	public List<PlasmaTaskItem> plateSample(String id, String counts) {
		String hql="from PlasmaTaskItem where 1=1 and bloodSampleTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<PlasmaTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaTaskItem where 1=1 and bloodSampleTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PlasmaTaskItem  where 1=1 and bloodSampleTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<PlasmaTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<SamplePlasmaInfo> findBloodSplitInfoByCode(String code) {
		String hql="from SamplePlasmaInfo where 1=1 and code='"+code+"'";
		List<SamplePlasmaInfo> list=new ArrayList<SamplePlasmaInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	public List<SamplePlasmaInfo> selectAllResultListById(String code) {
		String hql = "from SamplePlasmaInfo  where (submit is null or submit='') and bloodSampleTask.id='"
				+ code + "'";
		List<SamplePlasmaInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<SamplePlasmaInfo> selectResultListById(String id) {
		String hql = "from SamplePlasmaInfo  where bloodSampleTask.id='"
				+ id + "'";
		List<SamplePlasmaInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<SamplePlasmaInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from SamplePlasmaInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SamplePlasmaInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<PlasmaTaskItem> selectPlasmaItemList(String scId)
			throws Exception {
		String hql = "from PlasmaTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and bloodSampleTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<PlasmaTaskItem> list = new ArrayList<PlasmaTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from PlasmaTaskItem where 1=1 and bloodSampleTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public SamplePlasmaInfo getResultByCode(String code) {
			String hql=" from SamplePlasmaInfo where 1=1 and code='"+code+"'";
			SamplePlasmaInfo spi=(SamplePlasmaInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}

		/**  
		 * @Title: findTemplateByzId  
		 * @Description: TODO  
		 * @author : nan.jiang
		 * @date 2018-8-21下午3:23:32
		 * @param businessKey
		 * @return
		 * List<PlasmaTaskTemplate>
		 * @throws  
		 */ 
		public List<PlasmaTaskTemplate> findTemplateByzId(String id) {
			String hql = "from PlasmaTaskTemplate where 1=1 and bloodSampleTask.id = '"+id+"'";
			List<PlasmaTaskTemplate> list = new ArrayList<PlasmaTaskTemplate>();
				list = this.getSession().createQuery(hql).list();
			return list;
		}

		/**
		 * @throws Exception   
		 * @Title: findPlasmaTaskTemplateList  
		 * @Description: TODO  
		 * @author : nan.jiang
		 * @date 2018-8-22上午11:39:37
		 * @param start
		 * @param length
		 * @param query
		 * @param col
		 * @param sort
		 * @return
		 * Map<String,Object>
		 * @throws  
		 */ 
		public Map<String, Object> findPlasmaTaskTemplateList(Integer start,
				Integer length, String query, String col, String sort,String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			
			String countHql = "select count(*) from PlasmaTaskTemplate where 1=1 and bloodSampleTask.id='"+id+"'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = " from PlasmaTaskTemplate where 1=1 and bloodSampleTask.id='"+id+"'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by orderNum";
				}
				List<PlasmaTaskTemplate> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		}

		/**
		 * @throws Exception   
		 * @Title: findBloodSplitTableByState  
		 * @Description: TODO  
		 * @author : nan.jiang
		 * @date 2018-8-27上午11:03:37
		 * @param cellType
		 * @param start
		 * @param length
		 * @param query
		 * @param col
		 * @param sort
		 * @return
		 * Map<String,Object>
		 * @throws  
		 */ 
		public Map<String, Object> findBloodSplitTableByState(
				Integer start, Integer length, String query, String col,
				String sort, User user) throws Exception {
			
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from PlasmaTask where 1=1 and state<>'1' and opUser is not null ";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from PlasmaTask where 1=1 and state<>'1' and opUser is not null ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}else{
					key+=" order by state desc ";
				}
				List<PlasmaTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		}

		public List<PlasmaTaskZhiJian> delZhiJianItem(String id) {
			List<PlasmaTaskZhiJian> list=new ArrayList<PlasmaTaskZhiJian>();
			String hql = "from PlasmaTaskZhiJian where 1=1 and bloodSampleTask.id='"+id+"'";
			String key="";
			list = getSession().createQuery(hql+key).list();
			return list;
		}

		public List<PlasmaTaskZhiJian> showBloodSplitZjianJson(String id, String orderNum) {
			String countHql = "select count(*) from PlasmaTaskZhiJian where 1=1 and bloodSampleTask.id='"+id+"'";
			String key = "";
			if(orderNum!=null&&!"".equals(orderNum)){
				key+=" and itemId='"+orderNum+"'";
			}else{
				key+=" and itemId='1'";
			}
			List<PlasmaTaskZhiJian> list=null;
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				String hql = "from PlasmaTaskZhiJian where 1=1 and bloodSampleTask.id='"+id+"'";
				list = getSession().createQuery(hql+key).list();
				}
			return list;
		}

		public List<PlasmaTask> findBloodSplitList() {
			List<PlasmaTask> list = new ArrayList<PlasmaTask>();
			String hql = "from PlasmaTask where 1=1 and state <> '1'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<PlasmaTask> findBloodSplitTaskListByUser(String name) {
			List<PlasmaTask> list = new ArrayList<PlasmaTask>();
			String hql = "from PlasmaTask where 1=1 and state <> '1' and workUser like '%"+name+"%'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

	
		
}