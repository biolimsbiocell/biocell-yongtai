package com.biolims.experiment.plasma.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.model.QualityTest;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SampleReceiveExDao extends BaseHibernateDao {
	public Map<String, Object> showSampleReceiveItemListJson(String[] codes,String scId,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaReceiveItem where 1=1 ";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(scId==null||scId.equals("")){
			key+=" and state='1'";
		}
		if(codes!=null&&!codes.equals("")){
			key+=" and sampleCode in (:alist)";
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			List<PlasmaReceiveItem> list=null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from PlasmaReceiveItem  where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			if(codes!=null&&!codes.equals("")){
				list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
			}else{
				list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}
	
	public Map<String, Object> showSampleReceiveItemListJson(String[] codes,String scId,
			Integer start, Integer length, String query,
			String col, String sort, String type) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from PlasmaReceiveItem where 1=1 ";//and qualityTestPass='1' ";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(scId==null||scId.equals("")){
			key+=" and state='1'";
		}
		if(codes!=null&&!codes.equals("")){
			key+=" and sampleCode in (:alist)";
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		if(type!=null
				&&!"".equals(type)) {
			if("1".equals(type)) {
				key+=" and (auditPassOne is null or auditPassOne='') and (auditPassTwo is null or auditPassTwo='') and (auditPassThree is null or auditPassThree='') and ((auditPass<>'0' and auditPass<>'1') or auditPass='' or auditPass is null)  ";
			}else if("2".equals(type)) {
				key+=" and auditPassOne='1' and (auditPassTwo is null or auditPassTwo='') and (auditPassThree is null or auditPassThree='') and ((auditPass<>'0' and auditPass<>'1') or auditPass='' or auditPass is null) and qualityTestPass='1' ";
			}else if("3".equals(type)) {
				key+=" and auditPassOne='1' and auditPassTwo='1' and (auditPassThree is null or auditPassThree='') and ((auditPass<>'0' and auditPass<>'1') or auditPass='' or auditPass is null) ";
			}else if("4".equals(type)) {
				key+=" and auditPass='1' ";
			}else if("0".equals(type)) {
				key+=" and auditPass='0' ";
			}else {
				key+=" and (auditPassOne is null or auditPassOne='') and (auditPassTwo is null or auditPassTwo='') and (auditPassThree is null or auditPassThree='')  and ((auditPass<>'0' and auditPass<>'1') or auditPass='' or auditPass is null) ";
			}
//			if("1".equals(type)) {
//				key+=" and auditPass='1' ";
//			}else if("0".equals(type)) {
//				key+=" and auditPass='0' ";
//			}else {
//				key+=" and (auditPass is null or auditPass='') ";
//			}
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			List<PlasmaReceiveItem> list=null;
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from PlasmaReceiveItem  where 1=1 ";//and qualityTestPass='1' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			if(codes!=null&&!codes.equals("")){
				list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
			}else{
				list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<SampleReceiveItem> findsamplereceivebyordernum(String id) {
		String hql = "from SampleReceiveItem  where 1=1  and  sampleCode like '%"+ id + "%' ";
		List<SampleReceiveItem> list = getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SampleReceive> findsamplereceivebyordernum1(String id) {
		String hql = "from SampleReceive t  where 1=1  and  t.id in (select z.sampleReceive.id from SampleReceiveItem z where z.batch = '"+ id + "') ";
		List<SampleReceive> list = getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SampleReceiveItem> findsamplereceivebyordernum2(String id) {
		String hql = "from SampleReceiveItem t  where 1=1  and  t.sampleReceive.id in (select z.sampleReceive.id from SampleReceiveItem z where z.batch = '"+ id + "') ";
		List<SampleReceiveItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> finexeperiment(String id) {
		String hql = "from CellPassageItem  where 1=1  and  batch = '"+ id + "' ";
		List<CellPassageItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageTemplate> finexeperimenttemplate(String id,String orderNum) {
		String hql = "from CellPassageTemplate  where 1=1  and  cellPassage.id = '"+ id + "'and orderNum='"+orderNum+"' order by orderNum ";
		List<CellPassageTemplate> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<QualityTest> finexeperimentResult(String id) {
		String hql = "from QualityTest  qt where 1=1  and  qt.id in ( select qti.qualityTest.id from  QualityTestInfo qti where qti.sampleCode like '%"+ id + "%') ";
		List<QualityTest> list = getSession().createQuery(hql).list();
		return list;
	}
	
	public List<QualityTestInfo> finexeperimentResults(String id) {
		String hql = "from  QualityTestInfo qti where qti.sampleCode like '%"+ id + "%' ";
		List<QualityTestInfo> list = getSession().createQuery(hql).list();
		return list;
	}

}