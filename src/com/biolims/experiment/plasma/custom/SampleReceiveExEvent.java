package com.biolims.experiment.plasma.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.plasma.service.SampleReceiveExService;

public class SampleReceiveExEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleReceiveExService mbService = (SampleReceiveExService) ctx.getBean("sampleReceiveExService");
//		mbService.changeState(applicationTypeActionId, contentId);
		
		return "";
	}
}
