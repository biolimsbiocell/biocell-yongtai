package com.biolims.experiment.plasma.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.plasma.dao.SampleReceiveExDao;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.releaseAudit.model.ReleaseAudit;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class SampleReceiveExService {
	@Resource
	private SampleReceiveExDao sampleReceiveExDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CommonService commonService;

	/**
	 * s
	 * 
	 * @Title: showSampleReceiveItemListJson
	 * @Description: 展示接收样本
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:00:47
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showSampleReceiveItemListJson(String [] codes,String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
//		List<String> list=new ArrayList<String>();
//		if(codes!=null){
//			for(String code:codes){
//				list.add(code);
//			}
//		}
		
		return sampleReceiveExDao.showSampleReceiveItemListJson(codes,scId, start,
				length, query, col, sort);
	}
	
	/**
	 * 根据状态显示记录
	 * 
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showSampleReceiveItemListJson(String [] codes,String scId,
			Integer start, Integer length, String query, String col, String sort, String type)
			throws Exception {
		return sampleReceiveExDao.showSampleReceiveItemListJson(codes,scId, start,
				length, query, col, sort, type);
	}
	
	
	/**
	 * @param changeLog 
	 * 
	 * @Title: saveBloodSplitReceive  
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:02:16
	 * @param user
	 * @param dataJson
	 * @throws Exception
	 * void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBloodSplitReceive(User user, String dataJson, String changeLog)
			throws Exception {

		List<PlasmaReceiveItem> saveItems = new ArrayList<PlasmaReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		PlasmaReceiveItem scp = new PlasmaReceiveItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (PlasmaReceiveItem) sampleReceiveExDao.Map2Bean(map, scp);
			PlasmaReceiveItem pti = commonDAO.get(PlasmaReceiveItem.class,
					scp.getId());
			pti.setNextFlowId(scp.getNextFlowId());
			pti.setNextFlow(scp.getNextFlow());
			pti.setResult(scp.getResult());
			pti.setNote(scp.getNote());
			pti.setCreateUser(user);
			pti.setPackageResult(scp.getPackageResult());
			pti.setBagsNum(scp.getBagsNum());
			pti.setReleaseAuditId(scp.getReleaseAuditId());
			pti.setBatchNumber(scp.getBatchNumber());
			pti.setSpecifications(scp.getSpecifications());
			pti.setInspector(scp.getInspector());
			pti.setTestingDate(scp.getTestingDate());
			pti.setReviewer(scp.getReviewer());
			pti.setReviewDate(scp.getReviewDate());
			pti.setSampleCode(scp.getSampleCode());
			pti.setPatientName(scp.getPatientName());
			
			pti.setAuditPass(scp.getAuditPass());
			saveItems.add(pti);
			
			if(pti.getAuditPass()!=null
					&&"1".equals(pti.getAuditPass())){
				List<TransportOrderTemp> tots = commonService.get(TransportOrderTemp.class, "code", pti.getSampleOrder().getBarcode());
				if(tots.size()>0){
					for(TransportOrderTemp tot:tots){
						tot.setPlanState("1");
						commonDAO.saveOrUpdate(tot);
					}
				}
			}
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pti.getSampleCode());
				li.setClassName("PlasmaReceiveItem");
				li.setModifyContent(changeLog);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}
		
		sampleReceiveExDao.saveOrUpdateAll(saveItems);

	}
	/**
	 * 
	 * @Title: executeReceive
	 * @Description: 执行
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:01:53
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeReceive(String[] ids) throws Exception {
		for (String id : ids) {
			PlasmaReceiveItem scp = commonDAO.get(PlasmaReceiveItem.class, id);
			if (scp.getResult().equals("1")) {// 合格
				String next = scp.getNextFlowId();

				if (next.equals("0009")) {// 样本入库
					SampleInItemTemp st = new SampleInItemTemp();
					sampleInputService.copy(st, scp);

					st.setSampleType(scp.getSampleType());
					st.setInfoFrom("SamplePlasmaInfo");
					st.setState("2");
					sampleReceiveExDao.saveOrUpdate(st);
				} else if (next.equals("0012")) {// 暂停
				} else if (next.equals("0013")) {// 终止
				} else if (next.equals("0017")) {// 核酸提取
					DnaTaskTemp d = new DnaTaskTemp();
					sampleInputService.copy(d, scp);
					if (scp.getProductId() != null) {
						Product p = commonDAO.get(Product.class,
								scp.getProductId());
					}
					sampleReceiveExDao.saveOrUpdate(d);
				} else if (next.equals("0018")) {// 文库构建
					WkTaskTemp d = new WkTaskTemp();
					sampleInputService.copy(d, scp);
					d.setSampleType(scp.getSampleType());
					if (scp.getProductId() != null) {
						Product p = commonDAO.get(Product.class,
								scp.getProductId());
					}
					sampleReceiveExDao.saveOrUpdate(d);
				} else {
					// 得到下一步流向的相关表单
					List<NextFlow> list_nextFlow = nextFlowDao
							.seletNextFlowById(next);
					for (NextFlow n : list_nextFlow) {
						Object o = Class.forName(
								n.getApplicationTypeTable().getClassPath())
								.newInstance();
						sampleInputService.copy(o, scp);
					}
				}
			} else {// 不合格
				PlasmaAbnormal pa = new PlasmaAbnormal();// 样本异常
				sampleInputService.copy(pa, scp);

			}
			scp.setState("2");
			scp.setCreateDate(new Date());
			sampleReceiveExDao.saveOrUpdate(scp);
		}

	}
	
	
	/**
	 * @param changeLog 
	 * 
	 * @Title: 
	 * @Description: 保存放行审核
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:02:16
	 * @param user
	 * @param dataJson
	 * @throws Exception
	 * void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String editReleaseAudit(String id, String itemId, String code)
			throws Exception {
		ReleaseAudit ra = new ReleaseAudit();
		
		id = systemCodeService.getCodeByPrefix("ReleaseAudit", "RA"
				+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
				000, 3, null);
		
		ra.setId(id);
		ra.setState("3");
		ra.setStateName("新建");
		
		PlasmaReceiveItem pri = commonDAO.get(PlasmaReceiveItem.class, itemId);
		if(pri!=null){
			pri.setReleaseAuditId(id);
			ra.setNum(pri.getCellNumber());
			ra.setSpecifications(pri.getSpecifications());
			sampleReceiveExDao.saveOrUpdate(pri);
			
			if(pri.getSampleOrder()!=null){
//				List<SampleOrder> sos = commonService.get(SampleOrder.class, "", pri.getSampleOrder().getId());
//				if(sos.size()>0){
					SampleOrder so = commonDAO.get(SampleOrder.class, pri.getSampleOrder().getId());//sos.get(0);//
					if(so!=null){
						ra.setPatientName(so.getName());
//						ra.setCode(code);
//						ra.setProductBatchNumber(code);
						ra.setProductBatchNumber(so.getBarcode());
						if(so.getCrmCustomer()!=null){
							ra.setHospital(so.getCrmCustomer().getName());
						}
						if(so.getInspectionDepartment()!=null){
							ra.setDepartment(so.getInspectionDepartment().getName());
						}
						ra.setPatientId(so.getHospitalPatientID());
						ra.setCcoi(so.getCcoi());
					}
//				}
			}
		}
		
		if(ra.getProductBatchNumber()!=null
				&&!"".equals(ra.getProductBatchNumber())) {
			List<InfluenceProduct> ips =commonService.get(InfluenceProduct.class, "orderCode", ra.getProductBatchNumber());
			if(ips.size()>0) {
				Boolean approvalStatus = true;
				for(InfluenceProduct ip:ips) {
					if(!"1".equals(ip.getDeviationHandlingReport().getApprovalStatus())) {
						approvalStatus = false;
					}
				}
				if(approvalStatus) {
					ra.setResult16("1");
					ra.setResult8("1");
					ra.setResult9("1");
				}else{
					ra.setResult16("0");
					ra.setResult8("0");
					ra.setResult9("0");
				}
			}else {
				ra.setResult16("2");
				ra.setResult8("2");
				ra.setResult9("2");
			}
		}
		
		
		sampleReceiveExDao.saveOrUpdate(ra);
		
		return id;
	}
	
	public List<SampleReceiveItem> findsamplereceivebyordernum(String id) {
		return sampleReceiveExDao.findsamplereceivebyordernum(id);
	}
	
	public List<SampleReceive> findsamplereceivebyordernum1(String id) {
		return sampleReceiveExDao.findsamplereceivebyordernum1(id);
	}
	
	public List<SampleReceiveItem> findsamplereceivebyordernum2(String id) {
		return sampleReceiveExDao.findsamplereceivebyordernum2(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void confirmReinfusion(String[] ids,String zt) {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		if(ids.length>0) {
			for(String id:ids) {
				PlasmaReceiveItem pri = commonDAO.get(PlasmaReceiveItem.class, id);
				if("1".equals(zt)) {
					pri.setInspectorOne(u.getName());
					pri.setTestingDateOne(new Date());
					pri.setAuditPassOne("1");
				}else if("2".equals(zt)) {
					pri.setInspectorTwo(u.getName());
					pri.setTestingDateTwo(new Date());
					pri.setAuditPassTwo("1");
					
					if(pri.getSampleCode()!=null
							&&!"".equals(pri.getSampleCode())) {
						List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", pri.getSampleCode());
						if(sos.size()>0) {
							for(SampleOrder so:sos) {
								so.setBatchState("3");
								so.setBatchStateName("已放行");
								commonDAO.merge(so);
							}
						}
					}
				}else if("3".equals(zt)) {
					pri.setInspectorThree(u.getName());
					pri.setTestingDateThree(new Date());
					pri.setAuditPassThree("1");
					pri.setAuditPass("1");
					
				}
				commonDAO.saveOrUpdate(pri);
				
				String hsbg = "放行审核：" + "ID为'" + id +"'且产品批号为" + pri.getSampleCode() + "的数据已通过";
				if("1".equals(zt)) {
					hsbg = hsbg+"第一次审核";
				}else if("2".equals(zt)) {
					hsbg = hsbg+"第二次审核";
				}else if("3".equals(zt)) {
					hsbg = hsbg+"第三次审核";
				}
				if (hsbg != null && !"".equals(hsbg)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setUserId(u.getId());
					li.setFileId(pri.getSampleCode());
					li.setClassName("PlasmaReceiveItem");
					li.setModifyContent(hsbg);
					li.setState("3");
					li.setStateName("数据修改");
					commonDAO.saveOrUpdate(li);
				}
			}
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void confirmReinfusionbu(String[] ids, String zt) {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		if(ids.length>0) {
			for(String id:ids) {
				PlasmaReceiveItem pri = commonDAO.get(PlasmaReceiveItem.class, id);
				if("1".equals(zt)) {
					pri.setInspectorOne(u.getName());
					pri.setTestingDateOne(new Date());
					pri.setAuditPassOne("0");
					pri.setAuditPass("0");
				}else if("2".equals(zt)) {
					pri.setInspectorTwo(u.getName());
					pri.setTestingDateTwo(new Date());
					pri.setAuditPassTwo("0");
					pri.setAuditPass("0");
				}else if("3".equals(zt)) {
					pri.setInspectorThree(u.getName());
					pri.setTestingDateThree(new Date());
					pri.setAuditPassThree("0");
					pri.setAuditPass("0");
				}
				
				
				String hsbg = "放行审核：" + "ID为'" + id +"'且产品批号为" + pri.getSampleCode() + "的数据未通过审核";
				if (hsbg != null && !"".equals(hsbg)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setUserId(u.getId());
					li.setFileId(pri.getSampleCode());
					li.setClassName("PlasmaReceiveItem");
					li.setModifyContent(hsbg);
					li.setState("3");
					li.setStateName("数据修改");
					commonDAO.saveOrUpdate(li);
				}
				commonDAO.saveOrUpdate(pri);
			}
		}
	}
	
//	public List<CellPassageTemplate> findCellPassageTemplate(String id) {
//		List<CellPassageTemplate> cpt = null;
//		List<CellPassageItem> cpi = sampleReceiveExDao.finexeperiment(id);
//		if(cpi.size()>0){
//			CellPassage cp = cpi.get(0).getCellPassage();
//			cpt = sampleReceiveExDao.finexeperimenttemplate(cp.getId());
//		}
//		return cpt;
//	}

}
