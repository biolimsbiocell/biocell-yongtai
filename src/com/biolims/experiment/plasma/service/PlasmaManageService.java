package com.biolims.experiment.plasma.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.plasma.dao.PlasmaManageDao;
import com.biolims.experiment.plasma.model.PlasmaManage;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class PlasmaManageService {
	@Resource
	private PlasmaManageDao plasmaManageDao;
	@Resource
	private CommonDAO commonDAO;

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(PlasmaManage i) throws Exception {
		plasmaManageDao.saveOrUpdate(i);
	}

	public PlasmaManage get(String id) {
		PlasmaManage plasmaManage = commonDAO.get(PlasmaManage.class, id);
		return plasmaManage;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBloodManager(String itemDataJson) throws Exception {
		List<PlasmaManage> saveItems = new ArrayList<PlasmaManage>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PlasmaManage sbi = new PlasmaManage();
			sbi = (PlasmaManage) plasmaManageDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			saveItems.add(sbi);
		}
		plasmaManageDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: plasmaManageItemRuku
	 * @Description: 入库
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:10:57
	 * @param ids
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plasmaManageItemRuku(String[] ids) {
		for (String id : ids) {
			PlasmaTaskItem scp = plasmaManageDao.get(PlasmaTaskItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				if (scp.getCode() == null) {
					st.setCode(scp.getSampleCode());
				} else {
					st.setCode(scp.getCode());
				}
				st.setSampleCode(scp.getSampleCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				plasmaManageDao.saveOrUpdate(st);
			}

		}
	}

	/**
	 * 
	 * @Title: showBloodSplitManageJson
	 * @Description: 展示样本管理明细
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午2:10:22
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showBloodSplitManageJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return plasmaManageDao.showBloodSplitManageJson(start, length, query,
				col, sort);
	}
}
