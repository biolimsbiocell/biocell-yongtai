﻿package com.biolims.experiment.plasma.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureTemp;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionTemp;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionTemp;
import com.biolims.experiment.cell.main.model.MaterailsMain;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.plasma.dao.BloodSplitDao;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.model.PlasmaTaskCos;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.experiment.plasma.model.PlasmaTaskReagent;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaTaskTemplate;
import com.biolims.experiment.plasma.model.PlasmaTaskZhiJian;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional
public class BloodSplitService {
	@Resource
	private BloodSplitDao bloodSplitDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:29:03
	 * @param id
	 * @return PlasmaTask
	 * @throws
	 */
	public PlasmaTask get(String id) {
		PlasmaTask bloodSampleTask = commonDAO.get(PlasmaTask.class, id);
		return bloodSampleTask;
	}

	/**
	 * 删除明细
	 * @param delStr 
	 * 
	 * @param ids
	 * @param user 
	 * @param bloodSplit_id 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBloodSplitItem(String delStr, String[] ids, User user, String bloodSplit_id) throws Exception {
		String delId="";
		for (String id : ids) {
			PlasmaTaskItem scp = bloodSplitDao.get(PlasmaTaskItem.class, id);
			if (scp.getId() != null) {
				PlasmaTask pt = bloodSplitDao.get(PlasmaTask.class, scp
						.getBloodSampleTask().getId());
				// 改变左侧样本状态
				PlasmaTaskTemp plasmaTaskTemp = this.commonDAO.get(
						PlasmaTaskTemp.class, scp.getTempId());
				if (plasmaTaskTemp != null
						&& !plasmaTaskTemp.getState().equals("1")) {
					pt.setSampleNum(pt.getSampleNum() - 1);
					plasmaTaskTemp.setState("1");
					bloodSplitDao.update(plasmaTaskTemp);
				}
				bloodSplitDao.update(pt);
				bloodSplitDao.delete(scp);
			}
			delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(bloodSplit_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
		
	}

	/**
	 * 
	 * @Title: delBloodSplitItemAf
	 * @Description: 重新排板
	 * @author : shengwei.wang
	 * @date 2018年1月17日上午10:48:30
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBloodSplitItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			PlasmaTaskItem scp = bloodSplitDao.get(PlasmaTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				bloodSplitDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @param user 
	 * @param bloodSplit_id 
	 * @param delStr 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBloodSplitResult(String[] ids, String delStr, String bloodSplit_id, User user) throws Exception {
		String idStr="";
		for (String id : ids) {
			SamplePlasmaInfo scp = bloodSplitDao
					.get(SamplePlasmaInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				bloodSplitDao.delete(scp);
			idStr+=scp.getSampleCode()+",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		li.setUserId(user.getId());
		li.setFileId(bloodSplit_id);
		li.setModifyContent(delStr+idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * @param user 
	 * @param bloodSplit_id 
	 * @param delStr 
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBloodSplitReagent(String id, String delStr, String bloodSplit_id, User user) throws Exception {
		
		if (id != null && !"".equals(id)) {
			PlasmaTaskReagent scp = bloodSplitDao.get(PlasmaTaskReagent.class,
					id);
			bloodSplitDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(bloodSplit_id);
			li.setModifyContent(delStr+scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @param user 
	 * @param bloodSplit_id 
	 * @param delStr 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBloodSplitCos(String id, String delStr, String bloodSplit_id, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			PlasmaTaskCos scp = bloodSplitDao.get(PlasmaTaskCos.class, id);
			bloodSplitDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(bloodSplit_id);
			li.setModifyContent(delStr+scp.getName());
		}
	}

	/**
	 * @throws ParseException 
	 * 
	 * @Title: saveBloodSplitTemplate
	 * @Description: 保存模板
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:30:26
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveBloodSplitTemplate(PlasmaTask sc) throws ParseException {
		List<PlasmaTaskTemplate> tlist2 = bloodSplitDao.delTemplateItem(sc
				.getId());
		List<PlasmaTaskReagent> rlist2 = bloodSplitDao.delReagentItem(sc
				.getId());
		List<PlasmaTaskCos> clist2 = bloodSplitDao.delCosItem(sc.getId());
		List<PlasmaTaskZhiJian> zlist2 = bloodSplitDao.delZhiJianItem(sc.getId());
		
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		commonDAO.deleteAll(zlist2);
		
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		List<ZhiJianItem> zlist = templateService.getZjItem(sc.getTemplate()
				.getId(), null);
		
		for (ZhiJianItem t : zlist) {
			PlasmaTaskZhiJian ptr = new PlasmaTaskZhiJian();
			ptr.setBloodSampleTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			ptr.setTypeId(t.getTypeId());
			ptr.setTypeName(t.getTypeName());
			bloodSplitDao.saveOrUpdate(ptr);
		}
		//定义一个增长的变量
		Integer i = 0;
		for (TemplateItem t : tlist) {
			PlasmaTaskTemplate ptt = new PlasmaTaskTemplate();
			
			Integer result = 0;
			if(t.getEstimatedTime()!=null&&!"".equals(t.getEstimatedTime())) {
				ptt.setEstimatedDate(t.getEstimatedTime());
				
				i += Integer.parseInt(t.getEstimatedTime());
				result = i - Integer.parseInt(t.getEstimatedTime());
				String createDate = sc.getCreateDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse = format.parse(createDate);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.DATE, result);
				parse = calendar.getTime();
				String date = format.format(parse);
				ptt.setPlanEndDate(date);
				
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse1 = format1.parse(createDate);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(calendar1.DATE, i);
				parse1 = calendar1.getTime();
				String date1 = format.format(parse1);
				ptt.setPlanEndDate(date1);
			}
			ptt.setBloodSampleTask(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			
			bloodSplitDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			PlasmaTaskReagent ptr = new PlasmaTaskReagent();
			ptr.setBloodSampleTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			bloodSplitDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			PlasmaTaskCos ptc = new PlasmaTaskCos();
			ptc.setBloodSampleTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			bloodSplitDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:37:27
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		PlasmaTask sct = bloodSplitDao.get(PlasmaTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		bloodSplitDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:37:45
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		PlasmaTask sc = this.bloodSplitDao.get(PlasmaTask.class, id);
		// 获取结果表样本信息
		List<SamplePlasmaInfo> list;
		if (ids == null)
			list = this.bloodSplitDao.selectAllResultListById(id);
		else
			list = this.bloodSplitDao.selectAllResultListByIds(ids);

		for (SamplePlasmaInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();
					/*String modelName = "MaterailsMain";
					String markCode = "MM";
					String autoID = codingRuleService.genTransID(modelName, markCode);
					//细胞材料管理(细胞主数据)
					MaterailsMain mm=new MaterailsMain();
					mm.setId(autoID);
					mm.setPronoun(scp.getPronoun());
					mm.setSampleCode(scp.getSampleCode());
					mm.setParentId(scp.getSampleCode());
					mm.setCode(scp.getCode());
					bloodSplitDao.saveOrUpdate(mm);*/
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setParentId(scp.getCode());
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("SamplePlasmaInfo");
						if(scp.getSampleOrder()!=null) {
							st.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						bloodSplitDao.saveOrUpdate(st);
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					}else if(next.equals("0094")) {//产品细胞运输
						TransportOrderTemp cpt=new TransportOrderTemp();
						scp.setState("1");
						cpt.setState("1");
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setCode(scp.getCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setConcentration(scp.getConcentration());
						cpt.setVolume(scp.getVolume());
						cpt.setOrderId(sc.getId());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						if(scp.getSampleOrder()!=null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						cpt.setPronoun(scp.getPronoun());
						bloodSplitDao.saveOrUpdate(cpt);
					}else if(next.equals("0083")) {//产品干细胞质检(自主)
						QualityTestTemp cpt=new QualityTestTemp();
						scp.setState("1");
						cpt.setState("1");
						cpt.setCellType("2");
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setCode(scp.getCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setConcentration(scp.getConcentration());
						cpt.setVolume(scp.getVolume());
						cpt.setOrderId(sc.getId());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						if(scp.getSampleOrder()!=null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						bloodSplitDao.saveOrUpdate(cpt);
					}else if(next.equals("0087")) {//产品干细胞质检(第三方)
						QualityTestTemp cpt=new QualityTestTemp();
						scp.setState("1");
						cpt.setState("1");
						cpt.setCellType("6");
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setCode(scp.getCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setConcentration(scp.getConcentration());
						cpt.setVolume(scp.getVolume());
						cpt.setOrderId(sc.getId());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						if(scp.getSampleOrder()!=null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						bloodSplitDao.saveOrUpdate(cpt);
					}else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					PlasmaAbnormal pa = new PlasmaAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					sampleInputService.copy(pa, scp);
					pa.setState("1");
					commonDAO.saveOrUpdate(pa);
				}
				if(scp.getSampleOrder()!=null) {
					scp.getSampleOrder().setNewTask(id);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState1(
								scp.getSampleOrder(),
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"PlasmaTask",
								"样本处理",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				// 设备状态记录
//				Instrument ins = null;
//				String code = scp.getBloodSampleTask().getId();
//				String cosString = "PLASMA_TASK_COS";
//				List<String> result = experimentDnaGetDao.findInstrumentId(
//						code, cosString);
//				if (!result.equals(null)) {
//					for (int i = 0; i < result.size(); i++) {
//						String codes = result.get(i);
//
//						if (!codes.equals(null)) {
//							ins = experimentDnaGetDao.get(Instrument.class,
//									codes);
//
//							instrumentStateService.saveInstrumentState(
//									ins.getId(), scp.getSampleCode(),
//									ins.getName(), ins.getState().toString(),
//									scp.getProductId(), scp.getProductName(),
//									"", sc.getCreateDate(),
//									format.format(new Date()), "PlasmaTask",
//									"样本处理", sc.getCreateUser().getName()
//											.toString(), sc.getId(),
//									scp.getNextFlow(), scp.getResult(), "",
//									sc.getTemplate().getName().toString(),
//									sc.getTestUserOneName(), null, null, null,
//									null, null, null, null, null);
//						}
//					}
//				}
				scp.setSubmit("1");
				bloodSplitDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findBloodSplitTable
	 * @Description: 展示主表
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:38:07
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findBloodSplitTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		//1.查询所有的数据
		Map<String, Object> map1 = bloodSplitDao.findBloodSplitTable(start, length, query, col,sort);
		//查询实验不知所有的操作人
		List<PlasmaTask> list = (List<PlasmaTask>) map1.get("list");
		String opUserString="";
		for (PlasmaTask plasmaTask : list) {
			//将主表id带去实验步骤表查询所有的操作人
			List<PlasmaTaskTemplate> templateList = bloodSplitDao.findTemplateByzId(plasmaTask.getId());
			if(templateList!=null&&templateList.size()>0){
				//遍历所有实验步骤
				for (PlasmaTaskTemplate pt : templateList) {
					if(pt.getTestUserList()!=null&&!pt.getTestUserList().equals("")){
						//将操作人的姓名拼接
						if(pt.getEndTime()==null||pt.getEndTime().equals("")){
						if(opUserString.equals("")){
							opUserString=pt.getTestUserList();
						}else{
							opUserString+=","+pt.getTestUserList();
						}
						}
					}
				}
				if(opUserString!=null&&!opUserString.equals("")){
//					 plasmaTask.setOpUser(opUserString);
					 bloodSplitDao.saveOrUpdate(plasmaTask);
					 opUserString="";
					}
			}
				
			 
			 }
		
		//在返回将数据查询一次
		Map<String, Object> map2 = bloodSplitDao.findBloodSplitTable(start, length, query, col,sort);
 
		return map2;
	}

	/**
	 * @param codes
	 * 
	 * @Title: selectBloodSampleTempTable
	 * @Description: 展示临时表
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:38:25
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectBloodSampleTempTable(String[] codes,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return bloodSplitDao.selectBloodSampleTempTable(codes, start, length,
				query, col, sort);
	}

	/**
	 * @param logInfo
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:32:26
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			PlasmaTask pt = new PlasmaTask();
			pt = (PlasmaTask) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getName();
			Integer sampleNum=pt.getSampleNum();
			Integer maxNum=pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "PlasmaTask";
				String markCode = "PL";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			} else {
				id = pt.getId();
				pt=commonDAO.get(PlasmaTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			

			pt.setTestUserOneName(userName);
			bloodSplitDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				//保存实验模板
				saveBloodSplitTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							PlasmaTaskTemp ptt = bloodSplitDao.get(
									PlasmaTaskTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									PlasmaTaskItem pti = new PlasmaTaskItem();
									pti.setParentId(ptt.getParentId());
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setSampleType(ptt.getSampleType());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									pti.setTempId(temp);
									pti.setBloodSampleTask(pt);
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									pti.setSampleInfo(ptt.getSampleInfo());
									if(ptt.getSampleOrder()!=null) {
										pti.setSampleOrder(commonDAO.get(SampleOrder.class,
												ptt.getSampleOrder().getId()));
									}
									pti.setPronoun(ptt.getPronoun());
									
									bloodSplitDao.saveOrUpdate(pti);
								}
							} else {
								PlasmaTaskItem pti = new PlasmaTaskItem();
								pti.setParentId(ptt.getParentId());
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setSampleType(ptt.getSampleType());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (t.getStorageContainer() != null) {
									pti.setState("1");
								}else{
									pti.setState("2");
								}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setBloodSampleTask(pt);
								if(ptt.getSampleOrder()!=null) {
									pti.setSampleOrder(commonDAO.get(SampleOrder.class,
											ptt.getSampleOrder().getId()));
								}
								pti.setPronoun(ptt.getPronoun());
								
								bloodSplitDao.saveOrUpdate(pti);
							}
							bloodSplitDao.saveOrUpdate(ptt);
						}
				}

			}
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}

		}
		return id;

	}

	/**
	 * 
	 * @Title: findBloodSplitItemTable
	 * @Description:展示未排版样本
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:38:54
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findBloodSplitItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return bloodSplitDao.findBloodSplitItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * @param logInfo
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:39:11
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo)
			throws Exception {
		List<PlasmaTaskItem> saveItems = new ArrayList<PlasmaTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		PlasmaTask pt = commonDAO.get(PlasmaTask.class, id);
		PlasmaTaskItem scp = new PlasmaTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (PlasmaTaskItem) bloodSplitDao.Map2Bean(map, scp);
			PlasmaTaskItem pti = commonDAO.get(PlasmaTaskItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());
			scp.setBloodSampleTask(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(pt.getCreateUser().getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		bloodSplitDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:32:58
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<PlasmaTaskItem>
	 * @throws
	 */
	public List<PlasmaTaskItem> showWellPlate(String id) throws Exception {
		List<PlasmaTaskItem> list = bloodSplitDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findBloodSplitItemAfTable
	 * @Description: 展示排板后
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:39:58
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findBloodSplitItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return bloodSplitDao.findBloodSplitItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:45:07
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			PlasmaTaskItem pti = commonDAO.get(PlasmaTaskItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			bloodSplitDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showBloodSplitStepsJson
	 * @Description: 展示实验步骤
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:44:48
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showBloodSplitStepsJson(String id,
			String orderNum) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<PlasmaTaskTemplate> pttList = bloodSplitDao
				.showBloodSplitStepsJson(id, orderNum);
		List<PlasmaTaskReagent> ptrList = bloodSplitDao
				.showBloodSplitReagentJson(id, orderNum);
		List<PlasmaTaskCos> ptcList = bloodSplitDao.showBloodSplitCosJson(id,
				orderNum);
		List<PlasmaTaskZhiJian> ptzList = bloodSplitDao.showBloodSplitZjianJson(id,
				orderNum);
		List<Object> plate = bloodSplitDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("zhiJian", ptzList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showBloodSplitResultTableJson
	 * @Description: 展示结果
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:44:34
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showBloodSplitResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return bloodSplitDao.showBloodSplitResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:44:15
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		PlasmaTask pt = commonDAO.get(PlasmaTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<PlasmaTaskItem> list = bloodSplitDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:43:21
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = bloodSplitDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			PlasmaTask pt = bloodSplitDao.get(PlasmaTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<SamplePlasmaInfo> listPTI = bloodSplitDao
								.findBloodSplitInfoByCode(code);
						if (listPTI.size() > 0) {
							SamplePlasmaInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader
									.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							bloodSplitDao.saveOrUpdate(spi);
						}
					}
				}
			}
		}
	}

	/**
	 * @throws ParseException 
	 * @param logInfo
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:43:04
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) throws ParseException {
		PlasmaTask pt = commonDAO.get(PlasmaTask.class, id);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(pt.getCreateUser().getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * @throws ParseException 
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:42:37
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) throws ParseException {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			PlasmaTaskTemplate ptt = bloodSplitDao.get(
					PlasmaTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			String testUsers=(String) object.get("testUserList");
			if(testUsers!=null&&!"".equals(templateId)){
				ptt.setTestUserList(testUsers);
			}
			String estimatedTime=(String) object.get("estimatedTime");
			if(estimatedTime!=null&&!"".equals(estimatedTime)){
				ptt.setEstimatedTime(estimatedTime);
			}
			
			String sampleDeteyion=(String) object.get("sampleDeteyion");
			if(sampleDeteyion!=null&&!"".equals(sampleDeteyion)){
				SampleDeteyion sampleDeteyion2 = commonDAO.get(SampleDeteyion.class, sampleDeteyion);
				ptt.setSampleDeteyion(sampleDeteyion2);
			}
			
			bloodSplitDao.saveOrUpdate(ptt);
			SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
			if(endTime!=null&&!endTime.equals("")){
		//定义时间变量
			String shijiDate="";
	  int days=0;
			//查询当前所有的实验步骤
		List<PlasmaTaskTemplate> list=	bloodSplitDao.findTemplateByzId(id);
		if(list!=null||list.size()>0){
			//判断当前所有实验步骤的结束时间
			for (PlasmaTaskTemplate pt : list) {
				if(pt.getEndTime()!=null&&!pt.getEndTime().equals("")){
					Date parse = df.parse(pt.getEndTime());
					shijiDate = df.format(parse);
				
				}else{
				   //没有结束时间的实验步骤
					if(shijiDate!=null&&!shijiDate.equals("")&&pt.getEstimatedDate()!=null&&!pt.getEstimatedDate().equals("")){
				
					if(!shijiDate.equals(pt.getEstimatedDate())){
						long time1 = df.parse(shijiDate).getTime();
						long time2 = df.parse(pt.getEstimatedDate()).getTime();
						long time3= time1-time2;
						if(days==0){
							
					
						long date1= time3 / (1000 * 60 * 60 * 24);  
						days = new Long(date1).intValue();  
						Calendar rightNow = Calendar.getInstance();
						rightNow.setTime(df.parse(pt.getEstimatedDate())); 
						rightNow.add(Calendar.DAY_OF_YEAR,days);
						String  format = df.format(rightNow.getTime());
						pt.setEstimatedDate(format);
						shijiDate="2017-01-01";
						}else{
							Calendar rightNow = Calendar.getInstance();
							rightNow.setTime(df.parse(pt.getEstimatedDate())); 
							rightNow.add(Calendar.DAY_OF_YEAR,days);
							String  format = df.format(rightNow.getTime());
							pt.setEstimatedDate(format);
							shijiDate="2017-01-01";
						}
						
					
				}else{
					shijiDate="";
				}
					}
				}
				bloodSplitDao.saveOrUpdate(pt);
				
			}
		
			
		}
			}
		}
	}

	
	

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:42:26
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			PlasmaTaskReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(PlasmaTaskReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new PlasmaTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setBloodSampleTask(commonDAO.get(PlasmaTask.class, id));
				}

			}
			bloodSplitDao.saveOrUpdate(ptr);
		}
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:42:12
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			PlasmaTaskCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(PlasmaTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new PlasmaTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setBloodSampleTask(commonDAO.get(PlasmaTask.class, id));
				}
			}
			bloodSplitDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:41:42
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return bloodSplitDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:41:27
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
		List<PlasmaTaskItem> list = bloodSplitDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SamplePlasmaInfo> spiList = bloodSplitDao.selectResultListById(id);
		for (PlasmaTaskItem pti : list) {
			boolean b = true;
			String p = pti.getPronoun();
			for (SamplePlasmaInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							SamplePlasmaInfo scp = new SamplePlasmaInfo();
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setSampleType(pti.getSampleType());
							scp.setProductId(pti.getProductId());
							scp.setSampleInfo(pti.getSampleInfo());
							scp.setProductName(pti.getProductName());
							scp.setBloodSampleTask(pti.getBloodSampleTask());
							scp.setResult("1");
							String markCode = "HT";
							/*if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = "C"+str;
									} else {
										markCode = "C"+str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = "C"+scp.getSampleCode();
									} else {
										markCode = "C"+scp.getSampleCode()
												+ d.getCode();
									}
								}

							}*/
							markCode += DateUtil.dateFormatterByPattern(
									new Date(), "yyMMdd")+ds.getCode()+p;
							String code = codingRuleService.getCode(
									"SamplePlasmaInfo", markCode, 00000, 5, null);
							scp.setCode(code);
							scp.setParentId(pti.getCode());
							if(pti.getSampleOrder()!=null) {
								scp.setSampleOrder(commonDAO.get(SampleOrder.class,
										pti.getSampleOrder().getId()));
							}
							bloodSplitDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								SamplePlasmaInfo scp = new SamplePlasmaInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setSampleInfo(pti.getSampleInfo());
								scp.setSampleType(pti.getSampleType());
								scp.setProductName(pti.getProductName());
								scp.setBloodSampleTask(pti.getBloodSampleTask());
								scp.setResult("1");
								String markCode = "HT";
								/*if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}*/
								markCode += DateUtil.dateFormatterByPattern(
										new Date(), "yyMMdd")+ds.getCode()+p;
								String code = codingRuleService.getCode(
										"SamplePlasmaInfo", markCode, 00000, 5, null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								if(pti.getSampleOrder()!=null) {
									scp.setSampleOrder(commonDAO.get(SampleOrder.class,
											pti.getSampleOrder().getId()));
								}
								bloodSplitDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						SamplePlasmaInfo spi = bloodSplitDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						bloodSplitDao.saveOrUpdate(spi);
					}
				}
			}
		}
	}

	/**
	 * @param logInfo
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : shengwei.wang
	 * @date 2018年2月2日下午1:41:01
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser)
			throws Exception {

		List<SamplePlasmaInfo> saveItems = new ArrayList<SamplePlasmaInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		PlasmaTask pt = commonDAO.get(PlasmaTask.class, id);
		for (Map<String, Object> map : list) {
			SamplePlasmaInfo scp = new SamplePlasmaInfo();
			// 将map信息读入实体类
			scp = (SamplePlasmaInfo) bloodSplitDao.Map2Bean(map, scp);
			scp.setBloodSampleTask(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			bloodSplitDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(pt.getCreateUser().getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		bloodSplitDao.saveOrUpdateAll(saveItems);

	}

	public List<PlasmaTaskItem> findPlasmaItemList(String scId)
			throws Exception {
		List<PlasmaTaskItem> list = bloodSplitDao.selectPlasmaItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return bloodSplitDao.generateBlendCode(id);
	}

//	public void save(PlasmaTaskItem plasmaTaskItem) {
//		
//		bloodSplitDao.save(plasmaTaskItem);
//	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(PlasmaTask plasmaTask,String []ids,String logInfo) throws Exception {
		if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				PlasmaTaskItem plasmaTaskItem = new PlasmaTaskItem();
				plasmaTaskItem.setBloodSampleTask(plasmaTask);
				plasmaTaskItem.setCode(ids[i]);
				plasmaTaskItem.setSampleCode(ids[i]);
				plasmaTaskItem.setState("1");
				bloodSplitDao.save(plasmaTaskItem);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(plasmaTask.getCreateUser().getId());
			li.setFileId(plasmaTask.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		
	}

@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void saveRightQuality(PlasmaTask plasmaTask,String []ids,String logInfo) throws Exception {
	if(ids!=null&&!"".equals(ids)){
		for (int i = 0; i < ids.length; i++) {
			PlasmaTaskItem plasmaTaskItem = new PlasmaTaskItem();
			plasmaTaskItem.setBloodSampleTask(plasmaTask);
			plasmaTaskItem.setCode(ids[i]);
			plasmaTaskItem.setSampleCode(ids[i]);
			plasmaTaskItem.setState("2");
			bloodSplitDao.save(plasmaTaskItem);
		}
	}
	
	if (logInfo != null && !"".equals(logInfo)) {
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		li.setUserId(plasmaTask.getCreateUser().getId());
		li.setFileId(plasmaTask.getId());
		li.setModifyContent(logInfo);
		commonDAO.saveOrUpdate(li);
	}
	
}

public SamplePlasmaInfo getInfoById(String idc) {
	return bloodSplitDao.get(SamplePlasmaInfo.class,idc);
	
}

/**  
 * @Title: findTemplate  
 * @Description: TODO  
 * @author : nan.jiang
 * @date 2018-8-21下午3:22:38
 * @param businessKey
 * @return
 * List<com.biolims.workflow.listener.PlasmaTaskTemplate>
 * @throws  
 */ 
public List<PlasmaTaskTemplate> findTemplate(
		String businessKey) {
	// TODO Auto-generated method stub
	return bloodSplitDao.findTemplateByzId(businessKey);
}

/**
 * @throws Exception   
 * @Title: findPlasmaTaskTemplateList  
 * @Description: TODO  
 * @author : nan.jiang
 * @date 2018-8-22上午11:38:53
 * @param start
 * @param length
 * @param query
 * @param col
 * @param sort
 * @return
 * Map<String,Object>
 * @throws  
 */ 
public Map<String, Object> findPlasmaTaskTemplateList(Integer start,
		Integer length, String query, String col, String sort,String id) throws Exception {
	
	return bloodSplitDao.findPlasmaTaskTemplateList(start, length, query, col,
			sort,id);
}

/**
 * @throws Exception   
 * @Title: findBloodSplitTableByState  
 * @Description: TODO  
 * @author : nan.jiang
 * @date 2018-8-27上午10:55:55
 * @param cellType
 * @param start
 * @param length
 * @param query
 * @param col
 * @param sort
 * @return
 * Map<String,Object>
 * @throws  
 */ 
@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public Map<String, Object> findBloodSplitTableByState(
		Integer start, Integer length, String query, String col, String sort,User user) throws Exception {
	//1.查询所有的数据
	Map<String, Object> map1 = bloodSplitDao.findBloodSplitTable(start, length, query, col,sort);
	//查询实验不知所有的操作人
	List<PlasmaTask> list = (List<PlasmaTask>) map1.get("list");
	String opUserString="";
	for (PlasmaTask plasmaTask : list) {
		//将主表id带去实验步骤表查询所有的操作人
		List<PlasmaTaskTemplate> templateList = bloodSplitDao.findTemplateByzId(plasmaTask.getId());
		if(templateList!=null&&templateList.size()>0){
			//遍历所有实验步骤
			for (PlasmaTaskTemplate pt : templateList) {
				if(pt.getTestUserList()!=null&&!pt.getTestUserList().equals("")){
					//将操作人的姓名拼接
					if(pt.getEndTime()==null||pt.getEndTime().equals("")){
					if(opUserString.equals("")){
						opUserString=pt.getTestUserList();
					}else{
						opUserString+=","+pt.getTestUserList();
					}
					}
				}
			}
			if(opUserString!=null&&!opUserString.equals("")){
//				 plasmaTask.setOpUser(opUserString);
				 bloodSplitDao.saveOrUpdate(plasmaTask);
				 opUserString="";
				}
		}
			
		 
		 }

	//在返回将数据查询一次
	  //查询新建
	Map<String, Object> map2 = bloodSplitDao.findBloodSplitTableByState(start, length, query, col,sort,user);

	return map2;

}



@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void saveBloodSplitItem(String id, String itemJson) throws Exception {

	PlasmaTask plasmaTask = commonDAO.get(PlasmaTask.class, id);
	List<Map<String,Object>> list = JsonUtils.toListByJsonArray(itemJson, List.class);
	for (Map<String, Object> map : list) {
		PlasmaTaskItem item = new PlasmaTaskItem();
		item = (PlasmaTaskItem) commonDAO.Map2Bean(map, item);
		item.setState("2");
		item.setBloodSampleTask(plasmaTask);
		commonDAO.saveOrUpdate(item);
	}
}

@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void submitToQualityTest(String id, String stepNum, String mark, String type, String[] ids) throws Exception {
	List<PlasmaTaskZhiJian> ptzList = bloodSplitDao.showBloodSplitZjianJson(id,
			stepNum);
	if(ids.length>0) {
		for (String itemId : ids) {
			PlasmaTaskItem scp = commonDAO.get(PlasmaTaskItem.class, itemId);
			if(ptzList.size()>0) {
				for (PlasmaTaskZhiJian zj : ptzList) {
						
						String markCode = "QC" + scp.getCode();
						String code = codingRuleService.getCode(
								"QualityTestTemp", markCode, 0000, 4,
								null);
						QualityTestTemp cpt=new QualityTestTemp();
						cpt.setCode(code);
						scp.setState("1");
						cpt.setState("1");
						cpt.setMark(mark);
						cpt.setZjName(zj.getName());
						cpt.setTestItem(zj.getName());
						cpt.setExperimentalSteps(zj.getItemId());
						cpt.setOrderCode(scp.getOrderCode());
						cpt.setParentId(scp.getCode());
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						
						cpt.setSampleOrder(scp.getSampleOrder());
						if(zj.getCode()!=null) {
							cpt.setSampleDeteyion(commonDAO.get(SampleDeteyion.class, zj.getCode()));
						}
						if(type.equals("zj01")) {//自主过程干细胞检测
							cpt.setCellType("2");
						}else if(type.equals("zj02")){
							cpt.setCellType("6");
						}
						bloodSplitDao.saveOrUpdate(cpt);
						bloodSplitDao.saveOrUpdate(scp);
					/*PlasmaTask sc = commonDAO.get(PlasmaTask.class, id);
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/
					/*sampleStateService
							.saveSampleState1(
									scp.getSampleOrder(),
									scp.getCode(),
									scp.getSampleCode(),
									scp.getProductId(),
									scp.getProductName(),
									"",
									sc.getCreateDate(),
									format.format(new Date()),
									"PlasmaTask",
									"产品干细胞生产明细",
									(User) ServletActionContext
											.getRequest()
											.getSession()
											.getAttribute(
													SystemConstants.USER_SESSION_KEY),
									id, next, null, null,
									null, null, null, null, null, null, null);*/
				}
			}
		}
	}
}

}

