package com.biolims.experiment.dna.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 模版明细
 * @author lims-platform
 * @date 2015-11-18 17:00:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "DNA_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class DnaTaskTemplate extends EntityDao<DnaTaskTemplate> implements java.io.Serializable {
	
	/**步骤id*/
	private String id;
	/**步骤编号*/
	private String code;
	/**排序号*/
	private int orderNum;
	/**步骤名称*/
	private String name;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**自定义字段*/
	private String content;
	/**自定义字段值*/
	private String contentData;
	/**关联样本*/
	private String sampleCodes;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**核对编码*/
	private String isCheck;
	/**相关主表*/
	private DnaTask dnaTask;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DNA_TASK")
	public DnaTask getDnaTask(){
		return this.dnaTask;
	}
	/**
	 *方法: 设置BloodSplit
	 *@param: BloodSplit  相关主表
	 */
	public void setDnaTask(DnaTask dnaTask){
		this.dnaTask = dnaTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE")
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得Template
	 *@return: Template  关联主表
	 */
//	public SampleCommonTask getTask() {
//		return task;
//	}
//	public void setTask(SampleCommonTask task) {
//		this.task = task;
//	}
	
	@Column(name ="SAMPLE_CODES", length = 5000)
	public String getSampleCodes() {
		return sampleCodes;
	}
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
	@Column(name="START_TIME",length=50)
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@Column(name="END_TIME",length=50)
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the contentData
	 */
	public String getContentData() {
		return contentData;
	}
	/**
	 * @param contentData the contentData to set
	 */
	public void setContentData(String contentData) {
		this.contentData = contentData;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the orderNum
	 */
	public int getOrderNum() {
		return orderNum;
	}
	/**
	 * @param orderNum the orderNum to set
	 */
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the isCheck
	 */
	public String getIsCheck() {
		return isCheck;
	}
	/**
	 * @param isCheck the isCheck to set
	 */
	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}
}