﻿package com.biolims.experiment.dna.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.dna.service.DnaTaskManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/dna/dnaTaskManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DnaTaskManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private DnaTaskManageService dnaTaskManageService;
	
	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 
	 * @Title: dnaTaskManageItemRuku
	 * @Description: 样本管理入库
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "dnaTaskManageItemRuku")
	public void dnaTaskManageItemRuku() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			dnaTaskManageService.dnaTaskManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: showDnaTaskManage
	 * @Description: 样本管理
	 * @author : 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showDnaTaskManage")
	public String showDnaTaskManage() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/dna/dnaTaskManage.jsp");
	}

	@Action(value = "showDnaTaskManageJson")
	public void showDnaTaskManageJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = dnaTaskManageService
					.showDnaTaskManageJson(start, length, query, col, sort);
			List<DnaTaskItem> list = (List<DnaTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("dnaTask-name", "");
			map.put("dnaTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	
}

