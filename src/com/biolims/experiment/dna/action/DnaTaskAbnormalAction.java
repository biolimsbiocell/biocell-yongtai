﻿package com.biolims.experiment.dna.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.dna.service.DnaTaskAbnormalService;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/dna/dnaTaskAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DnaTaskAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";// 240102
	@Autowired
	private DnaTaskAbnormalService dnaTaskAbnormalService;
	@Resource
	private FileInfoService fileInfoService;
	@Autowired
	private DnaTaskService dnaTaskService;
	
	/**
	 * 
	 * @Title: showDnaTaskAbnormalTable
	 * @Description: 展示异常样本列表
	 * @author 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showDnaTaskAbnormalTable")
	public String showDnaTaskAbnormalTable() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/dna/dnaTaskAbnormal.jsp");
	}

	@Action(value = "showDnaTaskAbnormalTableJson")
	public void showDnaTaskAbnormalTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = dnaTaskAbnormalService
				.showDnaTaskAbnormalTableJson(start, length, query, col, sort);
		List<DnaTaskAbnormal> list = (List<DnaTaskAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("isExecute", "");
		map.put("note", "");
		map.put("code", "");
		map.put("method", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("result", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("patient", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("reason", "");
		map.put("concentration", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("orderId", "");
		map.put("classify", "");
		map.put("sampleNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description:执行异常列表
	 * @author 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "executeAbnormal")
	public void executeAbnormal() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String dataJson = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			dnaTaskAbnormalService.executeAbnormal(ids, dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年5月3日下午2:13:02
	 * @throws Exception
	 *             void
	 * @throws
	 * 修改纪录：dwb 增加日志功能 
	 * 2018-05-11 17:55:29
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String dataJson = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			dnaTaskAbnormalService.save(dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: feedbackAbnormal  
	 * @Description: 反馈至项目组  
	 * @author : shengwei.wang
	 * @date 2018年5月4日上午10:01:32
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="feedbackAbnormal")
	public void feedbackAbnormal() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			dnaTaskAbnormalService.feedbackAbnormal(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));	
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DnaTaskAbnormalService getDnaTaskAbnormalService() {
		return dnaTaskAbnormalService;
	}

	public void setDnaTaskAbnormalService(
			DnaTaskAbnormalService dnaTaskAbnormalService) {
		this.dnaTaskAbnormalService =dnaTaskAbnormalService;
	}

}
