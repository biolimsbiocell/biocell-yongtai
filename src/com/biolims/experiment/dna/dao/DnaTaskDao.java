package com.biolims.experiment.dna.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.model.DnaTaskCos;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.dna.model.DnaTaskReagent;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskTemplate;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DnaTaskDao extends BaseHibernateDao {

	public Map<String, Object> findDnaTaskTable(Integer start, Integer length,
			String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTask where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DnaTask where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DnaTask> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectDnaTaskTempTable(String[] counts,
			String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTaskTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (codes != null && !codes.equals("")) {
			String cc = "";
			for (int i = 0; i < codes.length; i++) {
				if (i == codes.length - 1) {
					cc += "'" + codes[i] + "'";
				} else {
					cc += "'" + codes[i] + "',";
				}
			}
			key += " and code in (" + cc + ")";

		}
		if (counts != null && !counts.equals("")) {

			String ct = "";
			for (int i = 0; i < counts.length; i++) {
				if (i == counts.length - 1) {
					ct += "'" + counts[i] + "'";
				} else {
					ct += "'" + counts[i] + "',";
				}
			}
			key += " and counts in (" + ct + ")";
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			List<DnaTaskTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql)
					.uniqueResult();
			String hql = "from DnaTaskTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findDnaTaskItemTable(String id, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTaskItem where 1=1 and state='1' and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DnaTaskItem  where 1=1 and state='1' and dnaTask.id='"
					+ id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DnaTaskItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<DnaTaskItem> showWellPlate(String id) {
		String hql = "from DnaTaskItem  where 1=1 and state='2' and  dnaTask.id='"
				+ id + "'";
		List<DnaTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findDnaTaskItemAfTable(String id, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTaskItem where 1=1 and state='2' and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DnaTaskItem  where 1=1 and state='2' and dnaTask.id='"
					+ id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DnaTaskItem> list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from DnaTaskItem where 1=1 and state='2' and dnaTask.id='"
				+ id + "'";
		String key = "";
		List<String> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			String hql = "select counts from DnaTaskItem where 1=1 and state='2' and dnaTask.id='"
					+ id + "' group by counts";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<DnaTaskTemplate> showDnaTaskStepsJson(String id, String code) {

		String countHql = "select count(*) from DnaTaskTemplate where 1=1 and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and orderNum ='" + code + "'";
		} else {
		}
		List<DnaTaskTemplate> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			String hql = "from DnaTaskTemplate where 1=1 and dnaTask.id='" + id
					+ "'";
			key += " order by orderNum asc";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<DnaTaskReagent> showDnaTaskReagentJson(String id, String code) {
		String countHql = "select count(*) from DnaTaskReagent where 1=1 and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<DnaTaskReagent> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			String hql = "from DnaTaskReagent where 1=1 and dnaTask.id='" + id
					+ "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<DnaTaskCos> showDnaTaskCosJson(String id, String code) {
		String countHql = "select count(*) from DnaTaskCos where 1=1 and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<DnaTaskCos> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			String hql = "from DnaTaskCos where 1=1 and dnaTask.id='" + id
					+ "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<DnaTaskTemplate> delTemplateItem(String id) {
		List<DnaTaskTemplate> list = new ArrayList<DnaTaskTemplate>();
		String hql = "from DnaTaskTemplate where 1=1 and dnaTask.id='" + id
				+ "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DnaTaskReagent> delReagentItem(String id) {
		List<DnaTaskReagent> list = new ArrayList<DnaTaskReagent>();
		String hql = "from DnaTaskReagent where 1=1 and dnaTask.id='" + id
				+ "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DnaTaskCos> delCosItem(String id) {
		List<DnaTaskCos> list = new ArrayList<DnaTaskCos>();
		String hql = "from DnaTaskCos where 1=1 and dnaTask.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> showDnaTaskResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTaskInfo where 1=1 and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DnaTaskInfo  where 1=1 and dnaTask.id='" + id
					+ "'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DnaTaskInfo> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Object> showWellList(String id) {

		String hql = "select counts,count(*) from DnaTaskItem where 1=1 and dnaTask.id='"
				+ id + "' group by counts";
		List<Object> list = getSession().createQuery(hql).list();

		return list;
	}

	public List<DnaTaskItem> plateSample(String id, String counts) {
		String hql = "from DnaTaskItem where 1=1 and dnaTask.id='" + id + "'";
		String key = "";
		if (counts == null || counts.equals("")) {
			key = " and counts is null";
		} else {
			key = "and counts='" + counts + "'";
		}
		List<DnaTaskItem> list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTaskItem where 1=1 and dnaTask.id='"
				+ id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (counts == null || counts.equals("")) {
			key += " and counts is null";
		} else {
			key += "and counts='" + counts + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DnaTaskItem  where 1=1 and dnaTask.id='" + id
					+ "'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DnaTaskItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<DnaTaskInfo> findDnaTaskInfoByCode(String code) {
		String hql = "from DnaTaskInfo where 1=1 and code='" + code + "'";
		List<DnaTaskInfo> list = new ArrayList<DnaTaskInfo>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<DnaTaskInfo> selectAllResultListById(String code) {
		String hql = "from DnaTaskInfo  where (submit is null or submit='') and dnaTask.id='"
				+ code + "'";
		List<DnaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<DnaTaskInfo> selectResultListById(String id) {
		String hql = "from DnaTaskInfo  where dnaTask.id='" + id + "'";
		List<DnaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<DnaTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from DnaTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<DnaTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<DnaTaskItem> selectDnaTaskItemList(String scId)
			throws Exception {
		String hql = "from DnaTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dnaTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DnaTaskItem> list = new ArrayList<DnaTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Integer generateBlendCode(String id) {
		String hql = "select max(blendCode) from DnaTaskItem where 1=1 and dnaTask.id='"
				+ id + "'";
		Integer blendCode = (Integer) this.getSession().createQuery(hql)
				.uniqueResult();
		return blendCode;
	}

	public DnaTaskInfo getResultByCode(String code) {
		String hql = " from DnaTaskInfo where 1=1 and code='" + code + "'";
		DnaTaskInfo spi = (DnaTaskInfo) this.getSession().createQuery(hql)
				.uniqueResult();
		return spi;
	}

}