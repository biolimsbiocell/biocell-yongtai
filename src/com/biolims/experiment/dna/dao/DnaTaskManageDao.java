package com.biolims.experiment.dna.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DnaTaskManageDao extends BaseHibernateDao {
	public Map<String, Object> showDnaTaskManageJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DnaTaskItem where 1=1 ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and dnaTask.scopeId='" + scopeId + "'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			
			
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DnaTaskItem  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DnaTaskItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}