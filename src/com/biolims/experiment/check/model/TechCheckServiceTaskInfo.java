package com.biolims.experiment.check.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import java.util.Date;
import com.biolims.dic.model.DicType;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 实验检测结果
 * @author lims-platform
 * @date
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TECH_CHECK_SERVICE_TASK_INFO")
@SuppressWarnings("serial")
public class TechCheckServiceTaskInfo extends
		EntityDao<TechCheckServiceTaskInfo> implements java.io.Serializable {

	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private TechCheckServiceTask techCheckServiceTask;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 是否提交 */
	private String submit;
	/** 患者姓名 */
	private String patientName;
	/** 浓度 */
	private Double concentration;
	/** 任务单Id */
	private String orderId;
	/** 订单编号 */
	private String orderCode;
	/** 临床 1 科研 2 */
	private String classify;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 样本类型 */
	private String sampleType;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/** 科技服务 */
	private TechJkServiceTask techJkServiceTask;
	/** 科技服务明细 */
	private TechJkServiceTaskItem tjItem;
	/** 应出报告日期 */
	private Date reportDate;
	/** 单位组 */
	private DicType unitGroup;
	/**总量*/
	private Double sumTotal;
	/** od260/280 */
	private Double od260;
	/** od260/230 */
	private Double od230;
	/** rin */
	private Double rin;
	/** Qubit浓度 */
	private Double qbcontraction;
	/**质量评级*/
	private String zlGrade;
	/**状态*/
	private String state;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/** 代次 */
	private String pronoun;
	/** 父级 */
	private String parentId;


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}


	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血浆编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血浆编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 36)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得DicUnit
	 * 
	 * @return: DicUnit 单位
	 */
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得TechCheckServiceTask
	 * 
	 * @return: TechCheckServiceTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_CHECK_SERVICE_TASK")
	public TechCheckServiceTask getTechCheckServiceTask() {
		return this.techCheckServiceTask;
	}

	/**
	 * 方法: 设置TechCheckServiceTask
	 * 
	 * @param: TechCheckServiceTask 相关主表
	 */
	public void setTechCheckServiceTask(
			TechCheckServiceTask techCheckServiceTask) {
		this.techCheckServiceTask = techCheckServiceTask;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}


	/**
	 * @return the sumTotal
	 */
	public Double getSumTotal() {
		return sumTotal;
	}

	/**
	 * @param sumTotal the sumTotal to set
	 */
	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	/**
	 * @return the od260
	 */
	public Double getOd260() {
		return od260;
	}

	/**
	 * @param od260 the od260 to set
	 */
	public void setOd260(Double od260) {
		this.od260 = od260;
	}

	/**
	 * @return the od230
	 */
	public Double getOd230() {
		return od230;
	}

	/**
	 * @param od230 the od230 to set
	 */
	public void setOd230(Double od230) {
		this.od230 = od230;
	}


	/**
	 * @return the zlGrade
	 */
	public String getZlGrade() {
		return zlGrade;
	}

	/**
	 * @param zlGrade the zlGrade to set
	 */
	public void setZlGrade(String zlGrade) {
		this.zlGrade = zlGrade;
	}

	/**
	 * @return the rin
	 */
	public Double getRin() {
		return rin;
	}

	/**
	 * @param rin the rin to set
	 */
	public void setRin(Double rin) {
		this.rin = rin;
	}

	/**
	 * @return the qbcontraction
	 */
	public Double getQbcontraction() {
		return qbcontraction;
	}

	/**
	 * @param qbcontraction the qbcontraction to set
	 */
	public void setQbcontraction(Double qbcontraction) {
		this.qbcontraction = qbcontraction;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}