package com.biolims.experiment.check.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.check.model.TechCheckServiceTask;
import com.biolims.experiment.check.model.TechCheckServiceTaskCos;
import com.biolims.experiment.check.model.TechCheckServiceTaskInfo;
import com.biolims.experiment.check.model.TechCheckServiceTaskItem;
import com.biolims.experiment.check.model.TechCheckServiceTaskReagent;
import com.biolims.experiment.check.model.TechCheckServiceTaskTemp;
import com.biolims.experiment.check.model.TechCheckServiceTaskTemplate;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class TechCheckServiceTaskDao extends BaseHibernateDao {

	public Map<String, Object> findTechCheckServiceTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechCheckServiceTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechCheckServiceTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechCheckServiceTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectTechCheckServiceTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from TechCheckServiceTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<TechCheckServiceTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from TechCheckServiceTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findTechCheckServiceTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechCheckServiceTaskItem where 1=1 and state='1' and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechCheckServiceTaskItem  where 1=1 and state='1' and techCheckServiceTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechCheckServiceTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<TechCheckServiceTaskItem> showWellPlate(String id) {
		String hql="from TechCheckServiceTaskItem  where 1=1 and state='2' and  techCheckServiceTask.id='"+id+"'";
		List<TechCheckServiceTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findTechCheckServiceTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechCheckServiceTaskItem where 1=1 and state='2' and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechCheckServiceTaskItem  where 1=1 and state='2' and techCheckServiceTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechCheckServiceTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from TechCheckServiceTaskItem where 1=1 and state='2' and techCheckServiceTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from TechCheckServiceTaskItem where 1=1 and state='2' and techCheckServiceTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<TechCheckServiceTaskTemplate> showTechCheckServiceTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from TechCheckServiceTaskTemplate where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<TechCheckServiceTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from TechCheckServiceTaskTemplate where 1=1 and techCheckServiceTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<TechCheckServiceTaskReagent> showTechCheckServiceTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from TechCheckServiceTaskReagent where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<TechCheckServiceTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from TechCheckServiceTaskReagent where 1=1 and techCheckServiceTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<TechCheckServiceTaskCos> showTechCheckServiceTaskCosJson(String id,String code) {
		String countHql = "select count(*) from TechCheckServiceTaskCos where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<TechCheckServiceTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from TechCheckServiceTaskCos where 1=1 and techCheckServiceTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<TechCheckServiceTaskTemplate> delTemplateItem(String id) {
		List<TechCheckServiceTaskTemplate> list=new ArrayList<TechCheckServiceTaskTemplate>();
		String hql = "from TechCheckServiceTaskTemplate where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<TechCheckServiceTaskReagent> delReagentItem(String id) {
		List<TechCheckServiceTaskReagent> list=new ArrayList<TechCheckServiceTaskReagent>();
		String hql = "from TechCheckServiceTaskReagent where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<TechCheckServiceTaskCos> delCosItem(String id) {
		List<TechCheckServiceTaskCos> list=new ArrayList<TechCheckServiceTaskCos>();
		String hql = "from TechCheckServiceTaskCos where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showTechCheckServiceTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechCheckServiceTaskInfo where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechCheckServiceTaskInfo  where 1=1 and techCheckServiceTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechCheckServiceTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from TechCheckServiceTaskItem where 1=1 and techCheckServiceTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<TechCheckServiceTaskItem> plateSample(String id, String counts) {
		String hql="from TechCheckServiceTaskItem where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<TechCheckServiceTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TechCheckServiceTaskItem where 1=1 and techCheckServiceTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from TechCheckServiceTaskItem  where 1=1 and techCheckServiceTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<TechCheckServiceTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<TechCheckServiceTaskInfo> findTechCheckServiceTaskInfoByCode(String code) {
		String hql="from TechCheckServiceTaskInfo where 1=1 and code='"+code+"'";
		List<TechCheckServiceTaskInfo> list=new ArrayList<TechCheckServiceTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<TechCheckServiceTaskInfo> selectAllResultListById(String code) {
		String hql = "from TechCheckServiceTaskInfo  where (submit is null or submit='') and techCheckServiceTask.id='"
				+ code + "'";
		List<TechCheckServiceTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<TechCheckServiceTaskInfo> selectResultListById(String id) {
		String hql = "from TechCheckServiceTaskInfo  where techCheckServiceTask.id='"
				+ id + "'";
		List<TechCheckServiceTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<TechCheckServiceTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from TechCheckServiceTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<TechCheckServiceTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<TechCheckServiceTaskItem> selectTechCheckServiceTaskItemList(String scId)
			throws Exception {
		String hql = "from TechCheckServiceTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and techCheckServiceTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechCheckServiceTaskItem> list = new ArrayList<TechCheckServiceTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from TechCheckServiceTaskItem where 1=1 and techCheckServiceTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public TechCheckServiceTaskInfo getResultByCode(String code) {
			String hql=" from TechCheckServiceTaskInfo where 1=1 and code='"+code+"'";
			TechCheckServiceTaskInfo spi=(TechCheckServiceTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}