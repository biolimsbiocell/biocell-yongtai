package com.biolims.experiment.check.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.check.service.TechCheckServiceTaskService;

public class TechCheckServiceTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		TechCheckServiceTaskService mbService = (TechCheckServiceTaskService) ctx
				.getBean("techCheckServiceTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
