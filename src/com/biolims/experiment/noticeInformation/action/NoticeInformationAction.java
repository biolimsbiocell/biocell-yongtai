package com.biolims.experiment.noticeInformation.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.project.model.Project;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.noticeInformation.model.NoticeInformation;
import com.biolims.experiment.noticeInformation.service.NoticeInformationService;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.model.RoomState;
import com.biolims.experiment.roomManagement.service.RoomManagementService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.mysql.fabric.xmlrpc.base.Data;

@Namespace("/experiment/noticeInformation/noticeInformation")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class NoticeInformationAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "24010909001";
	@Autowired
	private NoticeInformationService noticeInformationService;
	@Resource
	private CommonDAO commonDAO;
	private NoticeInformation noticeInformation = new NoticeInformation();
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FieldService fieldService;
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showNoticeInformationTable")
	public String showNoticeInformationTable() throws Exception {
		String id = getParameterFromRequest("id");
		noticeInformation = noticeInformationService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/noticeInformation/noticeInformation.jsp");
	}

	@Action(value = "showNoticeInformationTableJson")
	public void showNoticeInformationTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = noticeInformationService.findNoticeInformationTable(start, length, query, col, sort);

			List<NoticeInformation> list = (List<NoticeInformation>) result.get("list");
		
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("describe", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("startDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("NoticeInformation");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	

	@Action(value = "editNoticeInformation")
	public String editNoticeInformation() throws Exception {
		String id = getParameterFromRequest("id");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		rightsId = "24010909001";
		long num = 0;
		if (id != null && !id.equals("")) {
			this.noticeInformation = noticeInformationService.get(id);
			if (noticeInformation == null) {
				noticeInformation = new NoticeInformation();
				noticeInformation.setId(id);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				noticeInformation.setCreateUser(user);
				noticeInformation.setCreateDate(new Date());
				noticeInformation.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				noticeInformation.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("bpmTaskId", bpmTaskId);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("bpmTaskId", bpmTaskId);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "noticeInformation");
			}

		} else {
			noticeInformation.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			noticeInformation.setCreateUser(user);
			noticeInformation.setCreateDate(new Date());
			noticeInformation.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			noticeInformation.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
//		toState(noticeInformation.getState());
		return dispatcher("/WEB-INF/page/experiment/noticeInformation/noticeInformationEdit.jsp");
	}

	/**
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		synchronized (NoticeInformation.class) {
			String id = noticeInformation.getId();
			String log = "";
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				log="123";
				String modelName = "NoticeInformation";
				String markCode = "NI";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);

				noticeInformation.setId(autoID);
			}

			String changeLog = getParameterFromRequest("changeLog");
			String string = noticeInformationService.save(noticeInformation, changeLog,log);
			String[] split = string.split(",");
			String samId = split[0];

			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			String url = "/experiment/noticeInformation/noticeInformation/editNoticeInformation.action?id=" + samId;
			if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
				url += "&bpmTaskId=" + bpmTaskId;
			}
			return redirect(url);
		}
	}
	
	// 删除
	@Action(value = "delect")
	public void delect() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			noticeInformationService.delect(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public NoticeInformationService getNoticeInformationService() {
		return noticeInformationService;
	}

	public void setNoticeInformationService(NoticeInformationService noticeInformationService) {
		this.noticeInformationService = noticeInformationService;
	}

	public NoticeInformation getNoticeInformation() {
		return noticeInformation;
	}

	public void setNoticeInformation(NoticeInformation noticeInformation) {
		this.noticeInformation = noticeInformation;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}
	
}
