package com.biolims.experiment.noticeInformation.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 公告栏
 * @author lims-platform
 * @date 2016-03-07 11:01:47
 * @version V1.0
 * 
 */
@Entity
@Table(name = "NOTICE_INFORMATION")
@SuppressWarnings("serial")
public class NoticeInformation extends EntityDao<NoticeInformation> implements java.io.Serializable, Cloneable {
	/** 公告编号 */
	private String id;
	/** 公告简述 */
	private String name;
	/** 公告详细 */
	private String describe;
	/** 创建日期 */
	private Date createDate;
	/** 创建人 */
	private User createUser;
	/** 通知开始日期 */
	private Date startDate;
	/** 通知截止日期 */
	private Date endDate;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;

	


	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "NOTE", length = 4000)
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

}
