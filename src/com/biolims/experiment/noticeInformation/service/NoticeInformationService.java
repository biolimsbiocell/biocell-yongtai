package com.biolims.experiment.noticeInformation.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.agent.primarynew.model.PrimaryTask;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.crm.customer.patient.service.CrmPatientService;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.noticeInformation.dao.NoticeInformationDao;
import com.biolims.experiment.noticeInformation.model.NoticeInformation;
import com.biolims.experiment.qaAudit.dao.QaAuditDao;
import com.biolims.experiment.qaAudit.model.QaAudit;
import com.biolims.experiment.roomManagement.dao.RoomManagementDao;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.model.RoomState;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleCancerTempItem;
import com.biolims.sample.model.SampleCancerTempPersonnel;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleOrderPersonnel;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class NoticeInformationService {
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NoticeInformationDao noticeInformationDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(NoticeInformation i) throws Exception {

		noticeInformationDao.saveOrUpdate(i);

	}

	public NoticeInformation get(String id) {
		NoticeInformation noticeInformation = commonDAO.get(NoticeInformation.class, id);
		return noticeInformation;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(NoticeInformation sc, Map jsonMap) throws Exception {
		if (sc != null) {
			noticeInformationDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}
	
	public Map<String, Object> findNoticeInformationTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		
		return noticeInformationDao.findNoticeInformationTable(start, length, query, col,
				sort);
	}
	//后加的
	public List<NoticeInformation> finAllNoticeInformation() throws Exception{
		List<NoticeInformation> noticeInformations= noticeInformationDao.finAllNoticeInformation();
         
		return noticeInformations;
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(NoticeInformation noticeInformation, String logInfo,String log) throws Exception {
		String id = "";
		if (noticeInformation != null) {
			noticeInformationDao.saveOrUpdate(noticeInformation);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(noticeInformation.getId());
				li.setClassName("NoticeInformation");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
			id = noticeInformation.getId();
		}
		return id;
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeStateFb(String applicationTypeActionId, String id) throws Exception {
		NoticeInformation sc = commonDAO.get(NoticeInformation.class, id);
		if (sc != null) {
			sc.setState("1");
			sc.setStateName("已发布");
			noticeInformationDao.saveOrUpdate(sc);
		}

	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeStateZf(String applicationTypeActionId, String id) throws Exception {
		NoticeInformation sc = commonDAO.get(NoticeInformation.class, id);
		if (sc != null) {
			sc.setState("1");
			sc.setStateName("作废");
			noticeInformationDao.saveOrUpdate(sc);
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delect(String id) {
		NoticeInformation sc = commonDAO.get(NoticeInformation.class, id);
		if (sc != null) {
			noticeInformationDao.delete(sc);
		}
	}
	

}
