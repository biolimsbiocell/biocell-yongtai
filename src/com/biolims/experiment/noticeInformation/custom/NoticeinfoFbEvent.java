package com.biolims.experiment.noticeInformation.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.noticeInformation.service.NoticeInformationService;

public class NoticeinfoFbEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		NoticeInformationService mbService = (NoticeInformationService) ctx
				.getBean("noticeInformationService");
		mbService.changeStateFb(applicationTypeActionId, contentId);

		return "";
	}
}
