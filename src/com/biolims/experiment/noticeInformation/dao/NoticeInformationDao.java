package com.biolims.experiment.noticeInformation.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.noticeInformation.model.NoticeInformation;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.model.RoomState;

@Repository
@SuppressWarnings("unchecked")
public class NoticeInformationDao extends BaseHibernateDao {
	public Map<String, Object> findNoticeInformationTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from NoticeInformation where 1=1 and stateName!='删除' ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from NoticeInformation where 1=1 and stateName!='删除' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<NoticeInformation> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	//后添加的
	public List<NoticeInformation> finAllNoticeInformation(){
		String hql="from NoticeInformation where 1=1";
		
		List<NoticeInformation> list = new ArrayList<NoticeInformation>();
		list=getSession().createQuery(hql).list();

		
		return list;
	}

}