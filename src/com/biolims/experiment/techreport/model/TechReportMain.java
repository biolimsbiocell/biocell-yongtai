package com.biolims.experiment.techreport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.common.model.user.User;

/**
 * @Title: Model
 * @Description: 检测报告
 * @author lims-platform
 * @date 2016-02-24 19:47:57
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_TECH_REPORT_MAIN")
@SuppressWarnings("serial")
public class TechReportMain extends EntityDao<TechReportMain> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 报告人 */
	private User createUser;
	/** 报告日期日期 */
	private String createDate;
	/** 任务单号 */
	private String orderId;
	/** 报告模板 */
	private ReportTemplateInfo reportTemplateInfo;
	/** 模板id */
	private String retiId;
	/** 模板名称 */
	private String retiName;
	/** 模板路径 */
	private String retiNote;
	/** 工作流状态 */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 保存的报告单ID */
	private String bgId;
	// 科研任务单
	private TechJkServiceTask techJkServiceTask;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 60)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 报告人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 报告人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 报告日期日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 报告日期日期
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务单号
	 */
	@Column(name = "ORDER_ID", length = 60)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务单号
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_TEMPLATE_INFO")
	public ReportTemplateInfo getReportTemplateInfo() {
		return reportTemplateInfo;
	}

	public void setReportTemplateInfo(ReportTemplateInfo reportTemplateInfo) {
		this.reportTemplateInfo = reportTemplateInfo;
	}

	public String getRetiId() {
		return retiId;
	}

	public void setRetiId(String retiId) {
		this.retiId = retiId;
	}

	public String getRetiName() {
		return retiName;
	}

	public void setRetiName(String retiName) {
		this.retiName = retiName;
	}

	public String getRetiNote() {
		return retiNote;
	}

	public void setRetiNote(String retiNote) {
		this.retiNote = retiNote;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getBgId() {
		return bgId;
	}

	public void setBgId(String bgId) {
		this.bgId = bgId;
	}

}