package com.biolims.experiment.techreport.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * 报告明细
 * 
 * @author Vera
 */
@Entity
@Table(name = "SYS_TECH_REPORT")
public class TechReport extends EntityDao<TechReport> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3483551441456784825L;
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 32)
	private String id;// 编号

	@Column(name = "NOTE", length = 200)
	private String note;// 备注

	@Column(name = "SAMPLE_CODE", length = 200)
	private String sampleCode;// 样本号

	@Column(name = "DNA_CODE", length = 200)
	private String dnaCode;// 核算编号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILE_INFO_ID")
	private FileInfo reportFile;// 生成报告
	@Column(name = "REPORT_RESULE", length = 2000)
	private String reportResult;//
	@Column(name = "REPORT_RESULE_ID", length = 2000)
	private String reportResultId;//
	@Column(name = "DISEASE", length = 2000)
	private String disease;// 疾病类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_TEMPLATE_INFO_ID")
	private ReportTemplateInfo reti; // 常染色体报告模版

	private String contractId;// 合同号
	private String projectId;// 项目号
	private String orderType;// 任务单类型
	private String orderId;// 任务单号

	@Column(name = "REMARK", length = 200)
	private String remark;// 报告说明

	@Column(name = "RETI_ID", length = 200)
	private String retiId;// 模板ID

	@Column(name = "RETI_NAME", length = 200)
	private String retiName;// 模板名称

	@Column(name = "RETI_NOTE", length = 200)
	private String retiNote;// 模板路径

	private String state;// 状态
	@Column(name = "COMPLETE_DATE", length = 110)
	private Date completeDate;// 报告完成日期

	@Column(name = "SEND_DATE", length = 110)
	private Date sendDate;// 报告完成日期
	@Column(name = "ARC", length = 110)
	private String arc;// 是否发送
	@Column(name = "FILENUM", length = 50)
	private String fileNum;// 附件数量

	// 科研任务单
	private TechJkServiceTask techJkServiceTask;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public FileInfo getReportFile() {
		return reportFile;
	}

	public void setReportFile(FileInfo reportFile) {
		this.reportFile = reportFile;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public String getReportResult() {
		return reportResult;
	}

	public void setReportResult(String reportResult) {
		this.reportResult = reportResult;
	}

	public String getReportResultId() {
		return reportResultId;
	}

	public void setReportResultId(String reportResultId) {
		this.reportResultId = reportResultId;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getArc() {
		return arc;
	}

	public void setArc(String arc) {
		this.arc = arc;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public ReportTemplateInfo getReti() {
		return reti;
	}

	public void setReti(ReportTemplateInfo reti) {
		this.reti = reti;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getDnaCode() {
		return dnaCode;
	}

	public void setDnaCode(String dnaCode) {
		this.dnaCode = dnaCode;
	}

	public String getRetiId() {
		return retiId;
	}

	public void setRetiId(String retiId) {
		this.retiId = retiId;
	}

	public String getRetiName() {
		return retiName;
	}

	public void setRetiName(String retiName) {
		this.retiName = retiName;
	}

	public String getRetiNote() {
		return retiNote;
	}

	public void setRetiNote(String retiNote) {
		this.retiNote = retiNote;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
