package com.biolims.experiment.techreport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.doctor.model.CrmDoctor;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;

/**
 * @Title: Model
 * @Description: 报告明细
 * @author lims-platform
 * @date 2016-02-24 19:47:42
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SYS_TECH_REPORT_ITEM")
@SuppressWarnings("serial")
public class TechReportItem extends EntityDao<TechReportItem> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本名称 */
	private String sampleName;
	/** 样本编号 */
	private String code;
	/** 报告文件 */
	private FileInfo fileInfo;
	/** 报告模板 */
	private ReportTemplateInfo reportTemplateInfo;
	/** 项目编号 */
	private String productId;
	/** 项目名称 */
	private String productName;
	/** 合同编号 */
	private String contentId;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private TechReportMain sysTechReportMain;
	/** 模板id */
	private String retiId;
	/** 模板名称 */
	private String retiName;
	/** 模板路径 */
	private String retiNote;
	/** 附件数量 */
	private String fileNum;
	private String orderType;// 任务单类型
	private String orderId;// 任务单号

	// 科研项目
	private String projectId;
	private String projectName;
	// 客户
	private String crmCustomerId;
	private String crmCustomerName;
	// 医生
	private String crmDoctorId;
	private String crmDoctorName;

	// /**
	// * 科研项目
	// *
	// * @return
	// */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "T_PROJECT")
	// public Project getProject() {
	// return project;
	// }
	//
	// public void setProject(Project project) {
	// this.project = project;
	// }
	//
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "CRM_CUSTOMER")
	// public CrmCustomer getCrmCustomer() {
	// return crmCustomer;
	// }
	//
	// public void setCrmCustomer(CrmCustomer crmCustomer) {
	// this.crmCustomer = crmCustomer;
	// }
	//
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "CRM_DOCTOR")
	// public CrmDoctor getCrmDoctor() {
	// return crmDoctor;
	// }
	//
	// public void setCrmDoctor(CrmDoctor crmDoctor) {
	// this.crmDoctor = crmDoctor;
	// }

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getCrmCustomerId() {
		return crmCustomerId;
	}

	public void setCrmCustomerId(String crmCustomerId) {
		this.crmCustomerId = crmCustomerId;
	}

	public String getCrmCustomerName() {
		return crmCustomerName;
	}

	public void setCrmCustomerName(String crmCustomerName) {
		this.crmCustomerName = crmCustomerName;
	}

	public String getCrmDoctorId() {
		return crmDoctorId;
	}

	public void setCrmDoctorId(String crmDoctorId) {
		this.crmDoctorId = crmDoctorId;
	}

	public String getCrmDoctorName() {
		return crmDoctorName;
	}

	public void setCrmDoctorName(String crmDoctorName) {
		this.crmDoctorName = crmDoctorName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 60)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本名称
	 */
	@Column(name = "SAMPLE_NAME", length = 60)
	public String getSampleName() {
		return this.sampleName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本名称
	 */
	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得FileInfo
	 * 
	 * @return: FileInfo 报告文件
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILE_INFO")
	public FileInfo getFileInfo() {
		return this.fileInfo;
	}

	/**
	 * 方法: 设置FileInfo
	 * 
	 * @param: FileInfo 报告文件
	 */
	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	/**
	 * 方法: 取得ReportTemplateInfo
	 * 
	 * @return: ReportTemplateInfo 报告模板
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_TEMPLATE_INFO")
	public ReportTemplateInfo getReportTemplateInfo() {
		return this.reportTemplateInfo;
	}

	/**
	 * 方法: 设置ReportTemplateInfo
	 * 
	 * @param: ReportTemplateInfo 报告模板
	 */
	public void setReportTemplateInfo(ReportTemplateInfo reportTemplateInfo) {
		this.reportTemplateInfo = reportTemplateInfo;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 36)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 36)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 合同编号
	 */
	@Column(name = "CONTENT_ID", length = 36)
	public String getContentId() {
		return this.contentId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 合同编号
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 36)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得TechReportMain
	 * 
	 * @return: TechReportMain 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SYS_TECH_REPORT_MAIN")
	public TechReportMain getSysTechReportMain() {
		return this.sysTechReportMain;
	}

	/**
	 * 方法: 设置TechReportMain
	 * 
	 * @param: TechReportMain 相关主表
	 */
	public void setSysTechReportMain(TechReportMain sysTechReportMain) {
		this.sysTechReportMain = sysTechReportMain;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 模板id
	 */
	@Column(name = "RETI_ID", length = 60)
	public String getRetiId() {
		return this.retiId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 模板id
	 */
	public void setRetiId(String retiId) {
		this.retiId = retiId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 模板名称
	 */
	@Column(name = "RETI_NAME", length = 60)
	public String getRetiName() {
		return this.retiName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 模板名称
	 */
	public void setRetiName(String retiName) {
		this.retiName = retiName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 模板路径
	 */
	@Column(name = "RETI_NOTE", length = 60)
	public String getRetiNote() {
		return this.retiNote;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 模板路径
	 */
	public void setRetiNote(String retiNote) {
		this.retiNote = retiNote;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 附件数量
	 */
	@Column(name = "FILE_NUM", length = 60)
	public String getFileNum() {
		return this.fileNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 附件数量
	 */
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}