package com.biolims.experiment.techreport.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * 主页面任务明细
 * 
 * @author Vera
 */
@Entity
@Table(name = "T_MAIN_TASK")
public class MainTask extends EntityDao<MainTask> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3483551441456784825L;
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 32)
	private String id;// 编号

	@Column(name = "TASK_ID", length = 200)
	private String taskId;// 任务单号

	@Column(name = "TASK_NAME", length = 200)
	private String taskName;// 任务名称

	@Column(name = "TASK_NODE", length = 200)
	private String taskNode;// 描述

	@Column(name = "TASK_USER", length = 200)
	private String taskUser;// 发放人

	@Column(name = "CREATEDATE", length = 200)
	private String createDate;// 创建日期

	@Column(name = "TASK_NUM", length = 200)
	private String taskNum;// 完成数量
	
	@Column(name = "TASK_URL", length = 600)
	private String taskUrl;// 链接

	/** 
	 * @return id
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setId(String id) {
		this.id = id;
	}

	/** 
	 * @return taskId
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/** 
	 * @return taskName
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @param taskName the taskName to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/** 
	 * @return taskNode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getTaskNode() {
		return taskNode;
	}

	/**
	 * @param taskNode the taskNode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setTaskNode(String taskNode) {
		this.taskNode = taskNode;
	}

	/** 
	 * @return taskUser
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getTaskUser() {
		return taskUser;
	}

	/**
	 * @param taskUser the taskUser to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setTaskUser(String taskUser) {
		this.taskUser = taskUser;
	}

	/** 
	 * @return createDate
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/** 
	 * @return taskNum
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getTaskNum() {
		return taskNum;
	}

	/**
	 * @param taskNum the taskNum to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setTaskNum(String taskNum) {
		this.taskNum = taskNum;
	}

	/** 
	 * @return taskUrl
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getTaskUrl() {
		return taskUrl;
	}

	/**
	 * @param taskUrl the taskUrl to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setTaskUrl(String taskUrl) {
		this.taskUrl = taskUrl;
	}


}
