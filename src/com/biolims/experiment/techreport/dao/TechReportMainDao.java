package com.biolims.experiment.techreport.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.techreport.model.MainTask;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.experiment.techreport.model.TechReportItem;
import com.biolims.experiment.techreport.model.TechReportMain;
import com.biolims.sample.model.SampleInfo;

@Repository
@SuppressWarnings("unchecked")
public class TechReportMainDao extends BaseHibernateDao {
	public Map<String, Object> selectTechReportMainList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TechReportMain where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechReportMain> list = new ArrayList<TechReportMain>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectTechReportItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TechReportItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sysTechReportMain.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechReportItem> list = new ArrayList<TechReportItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectReportList(Map<String, String> mapForQuery, int startNum, int limitNum,
				String dir, String sort) throws Exception {
			String key = "";
			String hql = "from TechReport where 1 = 1";
			if (mapForQuery != null) {
				key = map2where(mapForQuery);
			}
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<TechReport> list = new ArrayList<TechReport>();

			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} else {

					key = key + " order by id DESC";
				}
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		public Map<String, Object> selectReportItemList(Map<String, String> mapForQuery, int startNum, int limitNum,
				String dir, String sort) throws Exception {
			String key = "";
			String hql = "from TechReport where 1=1";
			if (mapForQuery.size()>0) {
				key = map2where(mapForQuery);
			}else{
				key=" and  state='1'";
			}
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<TechReport> list = new ArrayList<TechReport>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				}
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		/**
		 * 根据主表ID获取子表集合
		 * @param id
		 * @return
		 */
		public List<TechReportItem> getItemById(String id){
			String hql = "from TechReportItem where 1=1 and sysTechReportMain='"+id+"'";
			List<TechReportItem> list=this.getSession().createQuery(hql).list();
			return list;
		}
		/**
		 * 根据样本号查询左侧表对象
		 * @param code
		 * @return
		 */
		public TechReport getItemBySampleCode(String code){
			String hql = "from TechReport where 1=1 and sampleCode='"+code+"'";
			TechReport list=(TechReport)this.getSession().createQuery(hql).list().get(0);
			return list;
		}
		/**
		 * 查询SampleInfo中样本号与报告子表中样本号一致的文库
		 * @param id
		 * @return
		 */
		public List<SampleInfo> sampleWkList(String id){
			String hql = "from SampleInfo where 1=1 and code=(select sampleCode from TechReportItem where 1=1 and sysTechReportMain='"+id+"')";
			List<SampleInfo> list=this.getSession().createQuery(hql).list();
			return list;
		}
		/***
		 * 
		 * @Title: showMainTaskJson
		 * @Description: TODO(主页面任务信息提醒查询)
		 * @param @param scId
		 * @param @param startNum
		 * @param @param limitNum
		 * @param @param dir
		 * @param @param sort
		 * @param @return    设定文件
		 * @return Map<String,Object>    返回类型
		 * @author zhiqiang.yang@biolims.cn
		 * @date 2017-11-16 下午4:59:28
		 * @throws
		 */
		public Map<String, Object> showMainTaskJson(String scId, Integer startNum,
				Integer limitNum, String dir, String sort, String userId) {
			Integer unaccomplispnumber = 0;
			String ss = "";
			List<String> list12 = new ArrayList<String>();
			List<MainTask> list2 = new ArrayList<MainTask>();
			List<String> listss = new ArrayList<String>();
			String sqlString = "SELECT tj.id FROM tech_jk_service_task tj WHERE tj.test_user_one_id='"
					+ userId + "'";
			list12 = this.getSession().createSQLQuery(sqlString).list();
			for (int i = 0; i < list12.size(); i++) {
				String id = list12.get(i);
				if (id != null) {

					String hql = "SELECT\n"
							+ "  ji.tech_jk_service_task   AS taskId,\n"
							+ "  ( SELECT NAME FROM tech_jk_service_task WHERE id=taskId) AS taskName,\n"
							+ "  (SELECT\n"
							+ "     COUNT(*)\n"
							+ "   FROM tech_jk_service_task_item itm\n"
							+ "   WHERE itm.tech_jk_service_task = taskId) AS countNumber,\n" 
							+ " ( SELECT create_date FROM tech_jk_service_task WHERE id = taskId ) " 
							+ "AS createDate\n"
							+ "FROM tech_jk_service_task_item ji\n"
							+ "WHERE ji.tech_jk_service_task = '" + id + "';";

					String key = "";
					int total = this.getSession().createSQLQuery(hql + key).list()
							.size();
					List<Object[]> list = new ArrayList<Object[]>();
					if (total > 0) {

						if (startNum == null || limitNum == null) {
							list = this.getSession().createSQLQuery(hql + key)
									.list();
						} else {
							list = this.getSession().createSQLQuery(hql + key)
									.list();
						}

						MainTask p = null;
						for (int j = 0; j < list.size(); j++) {

							Object[] object1 = list.get(j);
							String ids = (String) object1[0];
							String name="";
							if (object1[1].equals("")&&""==object1[1]) {
								name="";						}else {
								
								 name = (String) object1[1];;
							}
							String countNumber = object1[2].toString();
							String Cdate = object1[3].toString();
							p = new MainTask();
							p.setTaskId(ids);
							String nid = id.substring(0, 2);
							if (nid.equals("TQ")) {
								ss = "SELECT COUNT(*) FROM DNA_TASK_INFO  dna WHERE dna.tech_jk_service_task='"
										+ id + "' AND dna.submit='1'";
								p.setTaskName("质检任务单");
								p.setTaskUrl("/experiment/dna/dnaTask/editDnaTask.action");
							} else if (nid.equals("JK")) {
								ss = "SELECT COUNT(*) FROM WK_TASK_INFO  jk WHERE jk.tech_jk_service_task='"
										+ id + "' AND jk.submit='1'";
								p.setTaskName("建库任务单");
								p.setTaskUrl("/experiment/wk/wkTask/editWkTask.action");
							} else if (nid.equals("SJ")) {
								ss = "SELECT COUNT(*) FROM SEQUENCING_TASK_INFO  sq WHERE sq.TECH_JK_SERVICE_TASK='"
										+ id + "' AND sq.result='1'";
								p.setTaskName("上机任务单");
								p.setTaskUrl("/experiment/sequencing/sequencingTask/editSequencingTask.action");
							} else if (nid.equals("SX")) {
								ss = "SELECT COUNT(*) FROM tech_jk_service_task  tt WHERE tt.id='"
										+ id + "' AND tt.state='1'";
								p.setTaskName("生信任务单");
								p.setTaskUrl("");
							}
							listss = this.getSession().createSQLQuery(ss).list();
							for (int jj = 0; jj < listss.size(); jj++) {

								String objectss = String.valueOf(listss.get(jj));
								//未完成数量
								unaccomplispnumber = (Integer) Integer
										.parseInt(countNumber)
										- Integer.parseInt(objectss);
							}
							if (unaccomplispnumber < 0 ) {
								p.setTaskNum("0");
							}else {
								
								p.setTaskNum(unaccomplispnumber.toString());
							}
							p.setId(UUID.randomUUID().toString());
							p.setCreateDate(Cdate);
							p.setTaskNode(name);
							p.setTaskUser(userId);
						}
						list2.add(p);
						// saveOrUpdateAll(list2);
					}
				}
			}

			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", list2.size());
			result.put("list", list2);
			return result;
		}
}