package com.biolims.experiment.techreport.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.check.model.TechCheckServiceTask;
import com.biolims.experiment.check.model.TechCheckServiceTaskInfo;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.experiment.techreport.model.TechReportItem;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.SampleInputTechnology;

@Repository
@SuppressWarnings("unchecked")
public class TechReportDao extends BaseHibernateDao {

	public Map<String, Object> selectReportList(Map<String, String> mapForQuery, int startNum, int limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from TechReport where 1 = 1";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechReport> list = new ArrayList<TechReport>();

		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectReportItemList(Map<String, String> mapForQuery, int startNum, int limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from TechReport where 1=1 and  (state = '0' or state is null) ";

		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		//		key = " state <> '1'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechReport> list = new ArrayList<TechReport>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectReportSendList(Map<String, String> mapForQuery, int startNum, int limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from TechReport where 1=1";

		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		//		key = " state <> '1'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechReport> list = new ArrayList<TechReport>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<TechReport> selectReportItemListAll(String id) throws Exception {
		String key = "";
		String hql = "from TechReport where report.id = '" + id + "'";

		List<TechReport> list = this.getSession().createQuery(hql + key).list();

		return list;
	}
	/**
	 * 根据任务单号查询TechReportItem
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public TechReportItem selectTechReport(String orderId) throws Exception {
		String hql = "from TechReportItem t where t.orderId= '" + orderId + "'";
		TechReportItem techReportItem = (TechReportItem)this.getSession().createQuery(hql).list().get(0);
		return techReportItem;
	}
	/**
	 * 根据样本号号查询科技服信息录入表
	 * @param sampleCode
	 * @return
	 * @throws Exception
	 */
	public SampleInputTechnology selectSampleInputTechnology(String sampleCode) throws Exception {
		String hql = "from SampleInputTechnology t where t.sampleCode= '" + sampleCode + "'";
		SampleInputTechnology s = (SampleInputTechnology)this.getSession().createQuery(hql).list().get(0);
		return s;
	}
	/**
	 * 根据任务单号查询TechCheckServiceInfo的样本集合
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	public List<TechCheckServiceTaskInfo> selectTechCheckServiceInfo(String orderId) throws Exception {
		String hql = "from TechCheckServiceInfo t where t.orderId= '" + orderId + "'";
		List<TechCheckServiceTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 核酸检测结果
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<TechCheckServiceTaskInfo> getTechReport(String id,String tid) throws Exception {
		String key = "";
		String hql = "from TechCheckServiceInfo t where t.sampleCode in (select sampleCode from TechReportItem r where r.orderId='"+ id +"' and r.sysTechReportMain='"+tid+"')";
		List<TechCheckServiceTaskInfo> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	/**
	 * 文库检测结果
	 * @param id
	 * @return
	 * @throws Exception
	 */
//	public List<TechCheckServiceWkInfo> getTechReportWk(String id,String tid) throws Exception {
//		String key = "";
//		String hql = "from TechCheckServiceWkInfo t where t.sampleCode in (select sampleCode from TechReportItem r where r.orderId='"+ id +"' and r.sysTechReportMain='"+tid+"')";
//		List<TechCheckServiceWkInfo> list = this.getSession().createQuery(hql + key).list();
//		return list;
//	}
	/**
	 * FileInfo reportFile获取上传的图片集合
	 * @param mod
	 * @return
	 * @throws Exception
	 */
	public List<FileInfo> getFileInfo(String mod,String id) throws Exception {
		String key = "";
		String hql = "from FileInfo t where t.ownerModel='"+mod+"' and t.modelContentId in (select id from TechReportItem r where r.sysTechReportMain='"+id+"')";
		List<FileInfo> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	/**
	 * 获取时间最近的TechCheckServiceTask
	 * @return
	 * @throws Exception
	 */
	public TechCheckServiceTask selectTechCheckServiceTask() throws Exception {
		String hql = "from TechCheckServiceTask t where t.createDate=(select max(createDate) from TechCheckServiceTask)";
		TechCheckServiceTask list = (TechCheckServiceTask)this.getSession().createQuery(hql).list().get(0);
		return list;
	}
	/**
	 * 选择任务单加载相关样本
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setTechReportList(String code) throws Exception {
		String hql = "from TechReport t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.orderId='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechReport> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}

