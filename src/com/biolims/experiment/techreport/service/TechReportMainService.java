package com.biolims.experiment.techreport.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.techreport.dao.TechReportDao;
import com.biolims.experiment.techreport.dao.TechReportMainDao;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.experiment.techreport.model.TechReportItem;
import com.biolims.experiment.techreport.model.TechReportMain;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TechReportMainService {
	@Resource
	private TechReportMainDao techReportMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private TechReportDao techReportDao;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private SystemCodeService systemCodeService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTechReportMainList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return techReportMainDao.selectTechReportMainList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechReportMain i) throws Exception {

		techReportMainDao.saveOrUpdate(i);

	}

	public TechReportMain get(String id) {
		TechReportMain techReportMain = commonDAO.get(TechReportMain.class, id);
		return techReportMain;
	}

	public Map<String, Object> findTechReportItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = techReportMainDao
				.selectTechReportItemList(scId, startNum, limitNum, dir, sort);
		List<TechReportItem> list = (List<TechReportItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTechReportItem(TechReportMain sc, String itemDataJson)
			throws Exception {
		List<TechReportItem> saveItems = new ArrayList<TechReportItem>();
		// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
		// itemDataJson, List.class);
		// for (Map<String, Object> map : list) {
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			TechReportItem sri = new TechReportItem();
			// 将map信息读入实体类
			sri = (TechReportItem) techReportMainDao.Map2Bean(map, sri);
			// if (scp.getId() != null && scp.getId().equals(""))
			// scp.setId(null);
			// scp.setSysTechReportMain(sc);
			//
			// saveItems.add(scp);
			if (sri.getId() == null) {
				String code = systemCodeService.getCodeByPrefix(
						"TechReportItem",
						"JC"
								+ DateUtil.dateFormatterByPattern(new Date(),
										"yyMMdd"), 00000, 5, null);
				sri.setId(code);
			}
			sri.setSysTechReportMain(sc);
			techReportMainDao.saveOrUpdate(sri);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTechReportItem(String[] ids) throws Exception {
		for (String id : ids) {
			TechReportItem scp = techReportMainDao
					.get(TechReportItem.class, id);
			techReportMainDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechReportMain sc, Map jsonMap) throws Exception {
		if (sc != null) {
			techReportMainDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("techReportItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTechReportItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 左侧列表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReportList(Map<String, String> mapForQuery,
			int startNum, int limitNum, String dir, String sort)
			throws Exception {
		return techReportMainDao.selectReportList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findReportItemList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map = techReportMainDao.selectReportItemList(mapForQuery, startNum,
				limitNum, dir, sort);
		List<TechReport> list = (List<TechReport>) map.get("list");
		for (TechReport sri : list) {
			long num = fileInfoService.findFileInfoCount(sri.getId(),
					"sampleReportItem");
			sri.setFileNum(String.valueOf(num));
		}
		map.put("list", list);
		return map;
	}

	/**
	 * 审批完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		TechReportMain sct = techReportMainDao.get(TechReportMain.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		techReportMainDao.update(sct);

		List<TechReportItem> list = techReportMainDao.getItemById(id);
		// if (list.size() > 0) {
		// for (TechReportItem ti : list) {
		// TechReport t = techReportMainDao.getItemBySampleCode(ti
		// .getSampleCode());
		// t.setState("2");
		// }
		// }
	}

	/**
	 * 根据任务单加载出报告的数据
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showTechReportList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = techReportDao.setTechReportList(code);
		List<TechReport> list = (List<TechReport>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TechReport srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("projectId", srai.getProjectId());
				map.put("contractId", srai.getContractId());
				map.put("orderType", srai.getOrderType());
				if (srai.getCompleteDate() != null) {
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					String d = format.format(srai.getCompleteDate());
					map.put("completeDate", d);
				} else {
					map.put("completeDate", "");
				}
				if (srai.getReportFile() != null) {
					map.put("reportFileId", srai.getReportFile().getId());
					map.put("reportFileName", srai.getReportFile()
							.getFileName());
				}

				map.put("reportResult", srai.getReportResult());
				map.put("reportResultId", srai.getReportResultId());
				if (srai.getReti() != null) {
					map.put("retiId", srai.getReti().getId());
					map.put("retiName", srai.getReti().getName());
					map.put("retiNote", srai.getReti().getNote());
				} else {
					map.put("retiId", "");
					map.put("retiName", "");
					map.put("retiNote", "");
				}
				map.put("orderId", srai.getOrderId());
				map.put("disease", srai.getDisease());

				map.put("state", srai.getState());
				map.put("dnaCode", srai.getDnaCode());
				map.put("sampleCode", srai.getSampleCode());
				map.put("fileNum", srai.getFileNum());
				mapList.add(map);
			}
		}
		return mapList;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleReportItemById(String id, String fileRetId)
			throws Exception {

		// TechReportItem sril = commonService.get(TechReportItem.class, id);
		TechReportMain tm = commonDAO.get(TechReportMain.class, id);
		FileInfo rfi = commonService.get(FileInfo.class, fileRetId);
		if (tm != null && rfi != null) {
			// TechReportMain tm=commonDAO.get(TechReportMain.class,
			// sril.getSysTechReportMain().getId());
			tm.setBgId(fileRetId);
		}
	}
	/**
	 * 
	 * @Title: showMainTaskJson
	 * @Description: TODO(主页面项目任务信息提醒查询)
	 * @param @param scId
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-16 下午4:57:19
	 * @throws
	 */
	public Map<String, Object> showMainTaskJson(String scId, int startNum,
			int limitNum, String dir, String sort,String userId) {
		Map<String, Object> result = techReportMainDao.showMainTaskJson(scId,
				startNum, limitNum, dir, sort, userId);
		return result;
	}
}
