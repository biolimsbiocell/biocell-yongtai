package com.biolims.experiment.techreport.service;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.WorkDay;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.techreport.dao.TechReportDao;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.system.code.SystemCode;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings( { "rawtypes", "unchecked" })
public class TechReportService {

	@Resource
	private TechReportDao reportDao;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private UserGroupService userGroupService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CommonDAO commonDAO;
	/**
	 * 报表列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReportList(Map<String, String> mapForQuery, int startNum, int limitNum, String dir,
			String sort) throws Exception {
		return reportDao.selectReportList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public TechReport get(String id) throws Exception {
		return reportDao.get(TechReport.class, id);
	}

	public TechReport getSampleReportItemById(String id) throws Exception {
		return reportDao.get(TechReport.class, id);
	}

//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public String saveSampleReportItemById(String id, String fileRetId, String type) throws Exception {
//
//		TechReport sri = commonService.get(TechReport.class, id);
//
//		FileInfo rfi = commonService.get(FileInfo.class, fileRetId);
//		rfi.setFileName(sri.getBlood().getCode() + "_" + sri.getBlood().getPatientName() + ".doc");
//		//		if (sri.getState() == null || (sri.getState() != null && !sri.getState().equals("1"))) {
//		//			rfi.setOwnerModel("");
//		//		}
//
//		commonService.saveOrUpdate(rfi);
//		sri.setReportFile(rfi);
//		saveReportItem(sri);
//		String deskFileName = sri.getBlood().getCode() + "_" + sri.getBlood().getPatientName() + ".doc";
//		return deskFileName;
//	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReportItem(TechReport sc) throws Exception {
		if (sc != null) {

			this.reportDao.saveOrUpdate(sc);

		}
	}

	/**
	 * 报表明细信息检索
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */


	public Map<String, Object> findReportItemList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map = reportDao.selectReportItemList(mapForQuery, startNum, limitNum, dir, sort);
		List<TechReport> list = (List<TechReport>) map.get("list");
		for (TechReport sri : list) {
			long num = fileInfoService.findFileInfoCount(sri.getId(), "sampleReportItem");
			sri.setFileNum(String.valueOf(num));
		}
		map.put("list", list);
		return map;
	}

	public Map<String, Object> findReportSendList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map = reportDao.selectReportSendList(mapForQuery, startNum, limitNum, dir, sort);
		List<TechReport> list = (List<TechReport>) map.get("list");
		for (TechReport sri : list) {
			long num = fileInfoService.findFileInfoCount(sri.getId(), "sampleReportItem");
			sri.setFileNum(String.valueOf(num));
		}
		map.put("list", list);
		return map;
	}

	public List<TechReport> findReportItemListAll(String id) throws Exception {

		List<TechReport> list = reportDao.selectReportItemListAll(id);

		return list;
	}

	/**
	 * 保存
	 * @param itemDataJson
	 * @param sr
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String itemDataJson, TechReport sr) throws Exception {
		if (sr != null) {
			this.reportDao.saveOrUpdate(sr);
		}
		if (itemDataJson != null && itemDataJson.length() > 0)
			saveReportItem(sr, itemDataJson);
	}

	/**
	 * 保存报表发送明细
	 * @param taskId
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReportItem(TechReport sr, String itemDataJson) throws Exception {
		List<TechReport> saveItems = new ArrayList<TechReport>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			TechReport sri = new TechReport();
			// 将map信息读入实体类
			sri = (TechReport) reportDao.Map2Bean(map, sri);
			if (sri.getId().equals("NEW")) {
				//				CrmProduct a = commonService.get(CrmProduct.class, sri.getProduct().getId());
				String code = systemCodeService.getCodeByPrefix(SystemCode.PEPORT_NAME, "PE"
						+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), SystemCode.PEPORT_CODE, 3, null);
				sri.setId(code);
			}
			if (sri.getReportResult() == null || (sri.getReportResult() != null && sri.getReportResult().equals(""))) {
				if (sri.getReportResultId() != null && !sri.getReportResultId().equals("")) {
					String resultId = sri.getReportResultId().substring(0, sri.getReportResultId().length() - 1);
					//				resultId = resultId.replaceAll("@", "'");
//					String result = bloodService.saveReport(sri.getId(), resultId.replaceAll("@", "'"));
//					sri.setReportResult(result);
				}
			}

			reportDao.saveOrUpdate(sri);

//			if (sri.getState() != null && !sri.getState().equals("") && sri.getState().equals("1")) {
//				SampleBloodInfo sbiItem = sri.getBlood();
//				sbiItem.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//				sbiItem.setStateName(com.biolims.workflow.WorkflowConstants.BG_COMPLETE_NAME);
//				commonService.saveOrUpdate(sbiItem);
//				SampleState ss = new SampleState();
//				String q = SystemConstants.FSZ1_11_CDOE;
//				ss.setStartTime(sri.getCompleteDate());
//				ss.setCode(sri.getBlood().getCode());
//				ss.setEndTime(new Date());
//				ss.setTaskId(sri.getId());
//				ss.setStage(q);
//				ss.setStageName(SystemConstants.BG_COMPLETE_NAME);
//				String a = String.valueOf(WorkDay.showTime(sri.getCompleteDate(), new Date()));
//				ss.setStageTime(a);
//				if (sri.getCrmProduct() != null)
//					ss.setNote(sri.getCrmProduct().getName());
//				commonService.saveOrUpdate(ss);
//
//			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleReportItem(String itemDataJson) throws Exception {
		List<TechReport> saveItems = new ArrayList<TechReport>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TechReport sri = new TechReport();
			// 将map信息读入实体类
			sri = (TechReport) reportDao.Map2Bean(map, sri);
			List<FileInfo> lf = fileInfoService.findFileInfoList(sri.getId(), "sampleReportItemTemp");
			if (lf.size() > 0) {

				FileInfo a = lf.get(0);
				a.setOwnerModel("sampleReportItem");
				commonService.saveOrUpdate(a);
			}
//			if (sri.getId().equals("NEW")) {
//				//				CrmProduct a = commonService.get(CrmProduct.class, sri.getProduct().getId());
//				String code = systemCodeService.getCodeByPrefix(SystemCode.PEPORT_NAME, "PE"
//						+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), SystemCode.PEPORT_CODE, 3, null);
//				sri.setId(code);
//			}
//			if (sri.getReportResult() == null || (sri.getReportResult() != null && sri.getReportResult().equals(""))) {
//				if (sri.getReportResultId() != null && !sri.getReportResultId().equals("")) {
//					String resultId = sri.getReportResultId().substring(0, sri.getReportResultId().length() - 1);
//					//				resultId = resultId.replaceAll("@", "'");
//					String result = bloodService.saveReport(sri.getId(), resultId.replaceAll("@", "'"));
//					sri.setReportResult(result);
//				}
//			}

//			String sampleId = sri.getBlood().getId();
//			List<SamplePoolingTaskItem> spi = poolingTaskDao.selectPoolingList(sampleId);
//			if (spi.size() > 0) {
//				String poolId = spi.get(0).getTask().getId();
//				sri.setPoolId(poolId);
//			} else {
//				sri.setPoolId("");
//			}
//			if (sri.getState() != null && !sri.getState().equals("") && sri.getState().equals("0")) {
//				String sbiId = sri.getBlood().getId();
//				SampleBloodInfo sbiItem = commonService.get(SampleBloodInfo.class, sbiId);
//				sbiItem.setState(com.biolims.workflow.WorkflowConstants.READY_REPORTS_CODE);
//				sbiItem.setStateName(com.biolims.workflow.WorkflowConstants.WAIT_REPORTS_NAME);
//				commonService.saveOrUpdate(sbiItem);
//			}
//			if (sri.getState() != null && !sri.getState().equals("") && sri.getState().equals("1")) {
//				sri.setCompleteDate(new Date());
//				reportDao.saveOrUpdate(sri);
//				String sbiId = sri.getBlood().getId();
//				SampleBloodInfo sbiItem = commonService.get(SampleBloodInfo.class, sbiId);
//				sbiItem.setComReportDate(sri.getCompleteDate());
//				sbiItem.setNewDate(new Date());
//				sbiItem.setState(com.biolims.workflow.WorkflowConstants.BG_COMPLETE_CODE);
//				sbiItem.setStateName(com.biolims.workflow.WorkflowConstants.BG_COMPLETE_NAME);
//				commonService.saveOrUpdate(sbiItem);
//				SampleState ss = new SampleState();
//				String q = SystemConstants.FSZ1_11_CDOE;
//				ss.setStartTime(sri.getCompleteDate());
//				ss.setCode(sbiItem.getCode());
//				ss.setEndTime(new Date());
//				ss.setTaskId(sri.getId());
//				ss.setStage(q);
//				ss.setStageName(SystemConstants.BG_COMPLETE_NAME);
//				String a = String.valueOf(WorkDay.showTime(sri.getCompleteDate(), new Date()));
//				ss.setStageTime(a);
//				if (sri.getCrmProduct() != null)
//					ss.setNote(sri.getCrmProduct().getName());
//				commonService.saveOrUpdate(ss);
				saveItems.add(sri);
			}
			commonDAO.saveOrUpdateAll(saveItems);
//			reportDao.saveOrUpdate(sri);
			List<String> l = new ArrayList<String>();
			l.add("SYSZG");
			l.add("CWRYZ");
			List<UserGroupUser> l1 = userGroupService.findUserGroupUserListByList(l);
			User zg = null;
//			if (l1.size() > 0) {
//				for (UserGroupUser ugu : l1) {
//					zg = ugu.getUser();
//					if (sri.getState() != null && !sri.getState().equals("") && sri.getState().equals("1")) {
//						sampleTaskService.createSampleSysRemind("单号:" + sri.getId() + "报告任务于"
//								+ DateUtil.dateFormatterByPattern(new Date(), "yyyy-MM-dd") + "已经完成,待后续工作!", zg,
//								"sampleReport", sri.getId());
//					}
//				}
//			}
//		}

	}


	/**
	 * 删除报表列表,由ext ajax调用
	 * @param id
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleReportItemList(String[] ids) {
		for (String id : ids) {
			TechReport sri = new TechReport();
			sri.setId(id);
			reportDao.delete(sri);
		}
	}

	/**
	 * 生成常染色体报告文件
	 * @param ids
	 * @throws Exception
	 */

	private void scanDeskImgDir(String path, Map<String, File> map) throws Exception {
		File[] files = new File(path).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				String fileName = file.getName().substring(0, file.getName().indexOf("."));
				map.put(fileName, file);
			} else {
				scanDeskImgDir(file.getAbsolutePath(), map);
			}
		}
	}


	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReprotPayment(String itemDataJson) throws Exception {
		List<TechReport> saveItems = new ArrayList<TechReport>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		int s1 = 0;
		String s2 = "";
		boolean blood = false;
		boolean dna = false;
		String s3 = "";
		for (Map map : list) {
			TechReport sri = new TechReport();
			sri = (TechReport) reportDao.Map2Bean(map, sri);
			// sbi.setConsumptionTime(new Date());

			commonService.saveOrUpdate(sri);

//			if (sri.getState() != null && !sri.getState().equals("") && sri.getState().equals("1")) {
//				SampleBloodInfo sa = reportDao.get(SampleBloodInfo.class, sri.getBlood().getId());
//				sa.setState(com.biolims.workflow.WorkflowConstants.BG_COMPLETE_CODE);
//				sa.setStateName(com.biolims.workflow.WorkflowConstants.BG_COMPLETE_NAME);
//				commonService.saveOrUpdate(sa);
//			}
//			String a = String.valueOf(WorkDay.showTime(sri.getCompleteDate(), new Date()));
//			SampleState ss3 = new SampleState();
//			ss3.setStage(SystemConstants.FSZ1_3_CDOE);
//			SampleBloodInfo sai = reportDao.get(SampleBloodInfo.class, sri.getBlood().getId());
//			ss3.setCode(sai.getCode());
//			ss3.setEndTime(new Date());
//			ss3.setStageTime(a);
//			ss3.setCost(0.0);
//			ss3.setStageName(SystemConstants.BG_COMPLETE_NAME);
//			sai.setNewDate(new Date());
//			commonService.saveOrUpdate(sai);
//			commonService.saveOrUpdate(ss3);
		}
	}
	/**
	 * 根据任务单加载出报告的数据
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showTechReportList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = reportDao.setTechReportList(code);
		List<TechReport> list = (List<TechReport>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TechReport srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("projectId", srai.getProjectId());
				map.put("contractId", srai.getContractId());
				map.put("orderType", srai.getOrderType());
				if(srai.getCompleteDate()!=null){
					DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String d=format.format(srai.getCompleteDate());
					map.put("completeDate", d);
				}else{
					map.put("completeDate", "");
				}
				if(srai.getReportFile()!=null){
					map.put("reportFileId", srai.getReportFile().getId());
					map.put("reportFileName", srai.getReportFile().getFileName());
				}
				
				map.put("reportResult", srai.getReportResult());
				map.put("reportResultId", srai.getReportResultId());
				if(srai.getReti()!=null){
					map.put("retiId", srai.getReti().getId());
					map.put("retiName", srai.getReti().getName());
					map.put("retiNote", srai.getReti().getNote());
				}else{
					map.put("retiId", "");
					map.put("retiName", "");
					map.put("retiNote", "");
				}
				map.put("orderId", srai.getOrderId());
				map.put("disease", srai.getDisease());
				
				map.put("state", srai.getState());
				map.put("dnaCode", srai.getDnaCode());
				map.put("sampleCode", srai.getSampleCode());
				map.put("fileNum", srai.getFileNum());
				mapList.add(map);
			}
		}
		return mapList;
	}
}

