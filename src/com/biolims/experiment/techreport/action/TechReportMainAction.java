package com.biolims.experiment.techreport.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.check.model.TechCheckServiceTask;
import com.biolims.experiment.techreport.dao.TechReportDao;
import com.biolims.experiment.techreport.dao.TechReportMainDao;
import com.biolims.experiment.techreport.model.MainTask;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.experiment.techreport.model.TechReportItem;
import com.biolims.experiment.techreport.model.TechReportMain;
import com.biolims.experiment.techreport.service.TechReportMainService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleInputTechnology;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/techreport/techReportMain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TechReportMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240602";
	@Autowired
	private TechReportMainService techReportMainService;
	private TechReportMain techReportMain = new TechReportMain();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private TechReportDao techReportDao;
	@Resource
	private TechReportMainDao reportMainDao;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showTechReportMainList")
	public String showTechReportMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/techreport/techReportMain.jsp");
	}

	@Action(value = "showTechReportMainListJson")
	public void showTechReportMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techReportMainService
				.findTechReportMainList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<TechReportMain> list = (List<TechReportMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("techJkServiceTask-id", "");
		map.put("techJkServiceTask-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "techReportMainSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTechReportMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/techreport/techReportMainDialog.jsp");
	}

	@Action(value = "showDialogTechReportMainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTechReportMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techReportMainService
				.findTechReportMainList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<TechReportMain> list = (List<TechReportMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editTechReportMain")
	public String editTechReportMain() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			techReportMain = techReportMainService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "techReportMain");
		} else {
			techReportMain.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			techReportMain.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			techReportMain.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			techReportMain.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String st = format.format(date);
			techReportMain.setCreateDate(st);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(techReportMain.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/techreport/techReportMainEdit.jsp");
	}

	@Action(value = "copyTechReportMain")
	public String copyTechReportMain() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		techReportMain = techReportMainService.get(id);
		techReportMain.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/techreport/techReportMainEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = techReportMain.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "TechReportMain";
			String markCode = "JCBG";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			techReportMain.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("techReportItem",
				getParameterFromRequest("techReportItemJson"));

		techReportMainService.save(techReportMain, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/techreport/techReportMain/editTechReportMain.action?id="
				+ techReportMain.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewTechReportMain")
	public String toViewTechReportMain() throws Exception {
		String id = getParameterFromRequest("id");
		techReportMain = techReportMainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/techreport/techReportMainEdit.jsp");
	}

	@Action(value = "showTechReportItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTechReportItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/techreport/techReportItem.jsp");
	}

	@Action(value = "showTechReportItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechReportItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = techReportMainService
					.findTechReportItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<TechReportItem> list = (List<TechReportItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("sampleName", "");
			map.put("code", "");
			map.put("fileInfo-name", "");
			map.put("fileInfo-id", "");
			map.put("reportTemplateInfo-name", "");
			map.put("reportTemplateInfo-id", "");
			map.put("orderId", "");
			map.put("orderType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("contentId", "");
			map.put("note", "");
			map.put("sysTechReportMain-name", "");
			map.put("sysTechReportMain-id", "");
			map.put("retiId", "");
			map.put("retiName", "");
			map.put("retiNote", "");
			map.put("fileNum", "");
			map.put("projectName", "");
			map.put("projectId", "");
			map.put("crmCustomerName", "");
			map.put("crmCustomerId", "");
			map.put("crmDoctorName", "");
			map.put("crmDoctorId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTechReportItem")
	public void delTechReportItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			techReportMainService.delTechReportItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSampleReportList")
	public String showSampleMainReportList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/techreport/showReportList.jsp");
	}

	@Action(value = "showSampleReportListJson")
	public void showSampleMainReportListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String queryData = getRequest().getParameter("data");
		Map<String, String> mapForQuery = new HashMap<String, String>();

		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		} else {
		}
		try {
			Map<String, Object> result = this.techReportMainService
					.findReportItemList(mapForQuery, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<TechReport> list = (List<TechReport>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("reportResult", "");
			map.put("reportResultId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderId", "");
			map.put("orderType", "");
			map.put("completeDate", "yyyy-MM-dd");
			map.put("reti-id", "");
			map.put("reti-name", "");
			map.put("reti-note", "");
			map.put("disease", "");
			map.put("reportFile-id", "");
			map.put("reportFile-fileName", "");
			map.put("state", "");
			map.put("sampleCode", "");
			map.put("dnaCode", "");
			map.put("fileNum", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TechReportMainService getTechReportMainService() {
		return techReportMainService;
	}

	public void setTechReportMainService(
			TechReportMainService techReportMainService) {
		this.techReportMainService = techReportMainService;
	}

	public TechReportMain getTechReportMain() {
		return techReportMain;
	}

	public void setTechReportMain(TechReportMain techReportMain) {
		this.techReportMain = techReportMain;
	}

//	@Action(value = "toReportGen", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String toReportGen() throws Exception {
//		String tid = getParameterFromRequest("tid");// 主表第id
//		TechReportMain tm = commonDAO.get(TechReportMain.class, tid);
//		putObjToContext("contentId", tid);
//
//		// String ids = getParameterFromRequest("ids");//选中数据的id
//		String orderId = getParameterFromRequest("orderId");// 任务单ID
//		String userId = getParameterFromRequest("user");
//		User u = commonDAO.get(User.class, userId);
//		//
//		String uName = u.getName();
//		String reloadAction = getParameterFromRequest("reloadAction");
//		// 最近一次的检测表
//		TechCheckServiceTask tst = techReportDao.selectTechCheckServiceTask();
//		TechReportItem techReportItem = techReportDao.selectTechReport(orderId);
//		// 检测模板
//		ReportTemplateInfo rti = tm.getReportTemplateInfo();
//		String type = rti.getType();
//		// 检测任务单
//		// TechCheckServiceOrder
//		// td=commonDAO.get(TechCheckServiceOrder.class,orderId);
//		TechJkServiceTask td = commonDAO.get(TechJkServiceTask.class, orderId);
//		// 科技服信息录入表
//		SampleInputTechnology st = techReportDao
//				.selectSampleInputTechnology(techReportItem.getSampleCode()
//						.trim());
//		// 合同号
//		if (tm.getBgId() == null || reloadAction.equals("true")) {
//			if (td.getContractId() != null) {
//				putObjToContext("contractId", td.getContractId());
//			} else {
//				putObjToContext("contractId", "");
//			}
//			// 项目名称
//			// if (td.getProjectName() != null) {
//			// putObjToContext("projectId", td.getProjectName());
//			// } else {
//			// putObjToContext("projectId", "");
//			// }
//			//
//			// // 合作单位
//			// if (td.getCooperation() != null) {
//			// putObjToContext("contentDept", td.getCooperation());
//			// } else {
//			// putObjToContext("contentDept", "");
//			// }
//			// 项目联系人
//			if (td.getProjectLeader() != null) {
//				putObjToContext("projectUser", td.getProjectLeader());
//			} else {
//				putObjToContext("projectUser", "");
//			}
//			// 接收日期
//			if (st.getSendDate() != null) {
//				DateFormat format0 = new SimpleDateFormat("yyyy-MM-dd");
//				String rd = format0.format(st.getSendDate());
//				putObjToContext("receiveDate", rd);
//			} else {
//				putObjToContext("receiveDate", "");
//			}
//			// 检测日期
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//			String cd = tst.getCreateDate();
//			putObjToContext("checkDate", cd);
//			// 样品检测人
//			putObjToContext("checkUser", tst.getCreateUser().getName());
//			// 报告撰写人
//			putObjToContext("reportUser", uName);
//			// 报告审核人
//			putObjToContext("confirmUser", uName);
//			// 发布日期
//			Date d = new Date();
//			DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			String stime = format1.format(d);
//			putObjToContext("publishDate", stime);
//			if (type.equals("0")) {
//				// 核酸检测结果表===========================================
//				List<TechCheckServiceInfo> tsList = techReportDao
//						.getTechReport(orderId, tid);
//				// 获取上传的图片
//				List<FileInfo> fList = techReportDao.getFileInfo(
//						"techReportItem", tid);
//				putObjToContext("orderType", "核酸");
//				putObjToContext("tsList", tsList);
//				putObjToContext("fList", fList);
//			} else {
//				// 文库检测任务单============================================
//				// 文库检测结果表
//				List<TechCheckServiceWkInfo> wkList = techReportDao
//						.getTechReportWk(orderId, tid);
//				// 获取原始的文库信息
//				// List<SampleInfo>
//				// sampleWkList=reportMainDao.sampleWkList(tid);
//				putObjToContext("orderType", "文库");
//				putObjToContext("wkList", wkList);
//				// putObjToContext("sampleWkList", sampleWkList);
//			}
//
//			try {
//				if (rti != null) {
//					FileInfo a = rti.getAttach();
//
//					if (techReportItem.getFileInfo() != null
//							&& reloadAction.equals("")) {
//						putObjToContext("fileId", techReportItem.getFileInfo()
//								.getId());
//						putObjToContext("fileExist", "true");
//					} else {
//						if (a != null) {
//							putObjToContext("fileId", a.getId());
//						}
//					}
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//				return dispatcher("");
//			}
//		} else {
//			putObjToContext("fileId", tm.getBgId());
//		}
//		return dispatcher("/WEB-INF/page/experiment/techreport/editReportGen.jsp");
//	}
	/***
	 * 
	 * @Title: showApplicationTypeActionLogListZJJson
	 * @Description: TODO(主页面任务单查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-16 下午4:50:56
	 * @throws
	 */
	@Action(value = "showMainTaskJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMainTaskJson() throws Exception {
		// 开始记录数
		int startNum = 0;
		// limit
		int limitNum = 200;
		// 字段
		String dir = "createDate";
		String userId = getParameterFromRequest("user");
		// 排序方式
		String sort = "";
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = techReportMainService.showMainTaskJson(
					scId, startNum, limitNum, dir, sort, userId);
			List<MainTask> list = (List<MainTask>) result.get("list");

//			Map<String, String> map = new HashMap<String, String>();
//			map.put("taskId", "");
//			map.put("taskName", "");
//			map.put("taskNode", "");
//			map.put("taskUser", "");
//			map.put("createDate", "");
//			map.put("taskNum", "");
//			map.put("taskUrl", "");
//			new SendData().sendDateJson(map, list, list.size(),
//					ServletActionContext.getResponse());
			String json = JSONArray.fromObject(list).toString();
			getResponse().getWriter().write(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
