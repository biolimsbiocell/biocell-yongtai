package com.biolims.experiment.techreport.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.techreport.dao.TechReportDao;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.experiment.techreport.service.TechReportService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/techreport")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class TechReportAction extends BaseActionSupport {
	private static final long serialVersionUID = -8354314800608144980L;

	private String rightsId = "240602";

	@Resource
	private TechReportService reportService;
	@Resource
	private TechReportDao reportDao;
	private TechReport techReport;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showSampleReportList")
	public String showSampleMainReportList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/techreport/showReportList.jsp");
	}

	@Action(value = "showSampleReportListJson")
	public void showSampleMainReportListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String queryData = getRequest().getParameter("data");
		Map<String, String> mapForQuery = new HashMap<String, String>();

		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		} else {
		}
		try {
			Map<String, Object> result = this.reportService.findReportItemList(
					mapForQuery, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<TechReport> list = (List<TechReport>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("reportResult", "");
			map.put("reportResultId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderId", "");
			map.put("orderType", "");
			map.put("completeDate", "yyyy-MM-dd");
			map.put("reti-id", "");
			map.put("reti-name", "");
			map.put("reti-note", "");
			map.put("disease", "");
			map.put("reportFile-id", "");
			map.put("reportFile-fileName", "");
			map.put("state", "");
			map.put("sampleCode", "");
			map.put("dnaCode", "");
			map.put("fileNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "saveSampleReportList")
	public void saveSampleReportList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");

			// TechReport sr = new TechReport();

			reportService.saveSampleReportItem(dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveReprotPayment")
	public void saveReprotPayment() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			reportService.saveReprotPayment(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除报表明细列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleReportItemList")
	public void delSamplePlasmaInfo() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] id = getRequest().getParameterValues("ids[]");
			reportService.delSampleReportItemList(id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "toReportGen")
	// public String toReportGen() throws Exception {
	// String ids = getParameterFromRequest("ids");//选中数据的id
	// String orderId = getParameterFromRequest("orderId");//任务单ID
	// String user = getParameterFromRequest("user");
	// String reloadAction = getParameterFromRequest("reloadAction");
	// //最近一次的检测表
	// TechCheckServiceTask tst=reportDao.selectTechCheckServiceTask();
	// //获取上传的图片集合
	// List<FileInfo> fList=reportDao.getFileInfo("sampleReportItem");
	// TechReport techReport = reportDao.selectTechReport(orderId);
	// ReportTemplateInfo rti = techReport.getReti();
	// //检测任务单
	// TechCheckServiceOrder
	// td=commonDAO.get(TechCheckServiceOrder.class,orderId);
	// //科技服信息录入表
	// SampleInputTechnology
	// st=reportDao.selectSampleInputTechnology(techReport.getSampleCode());
	// if(td.getOrderType().equals("0")){
	// String[] sList=ids.split(",");
	// for(int i=0;i<sList.length;i++){
	//
	// }
	// //核酸检测结果表
	// List<TechCheckServiceInfo> tsList=reportDao.getTechReport(orderId);
	// //核酸检测任务单
	// String projectId=td.getProjectId(); //项目编号
	// String contentId=td.getContractId(); //合同编号
	// String orderType=td.getOrderType(); //任务类型
	// //项目联系人
	// if(td.getProjectLeader()!=null){
	// putObjToContext("projectUser", td.getProjectLeader());
	// }else{
	// putObjToContext("projectUser", "");
	// }
	//
	// //合作单位
	// if(td.getCooperation()!=null){
	// putObjToContext("contentDept", td.getCooperation());
	// }else{
	// putObjToContext("contentDept", "");
	// }
	//
	// //接收日期
	// if(st.getSendDate()!=null){
	// DateFormat format0=new SimpleDateFormat("yyyy-MM-dd");
	// String rd=format0.format(st.getSendDate());
	// putObjToContext("receiveDate", rd);
	// }else{
	// putObjToContext("receiveDate", "");
	// }
	// //项目名称
	// if(td.getProjectName()!=null){
	// putObjToContext("projectId", td.getProjectName());
	// }else{
	// putObjToContext("projectId", "");
	// }
	// //检测日期
	// DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
	// String cd=format.format(tst.getCreateDate());
	// putObjToContext("checkDate", cd);
	// //样品检测人
	// putObjToContext("checkUser", tst.getCreateUser().getName());
	// //报告撰写人
	// putObjToContext("reportUser", user);
	// //报告审核人
	// putObjToContext("confirmUser",user);
	// //发布日期
	// Date d=new Date();
	// DateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// String stime=format1.format(d);
	// putObjToContext("publishDate", stime);
	// //putObjToContext("projectId", projectId);
	// putObjToContext("contentId", contentId);
	// putObjToContext("orderType", orderType);
	// putObjToContext("tsList", tsList);
	// putObjToContext("fList", fList);
	// }else{
	// //文库检测任务单
	// //文库检测结果表
	// List<TechCheckServiceWkInfo> tsList=reportDao.getTechReportWk(orderId);
	// //合同号
	// if(td.getContractId()!=null){
	// putObjToContext("contentId", td.getContractId());
	// }else{
	// putObjToContext("contentId", "");
	// }
	// //项目名称
	// if(td.getProjectName()!=null){
	// putObjToContext("projectId", td.getProjectName());
	// }else{
	// putObjToContext("projectId", "");
	// }
	//
	// //合作单位
	// if(td.getCooperation()!=null){
	// putObjToContext("contentDept", td.getCooperation());
	// }else{
	// putObjToContext("contentDept", "");
	// }
	// //项目联系人
	// if(td.getProjectLeader()!=null){
	// putObjToContext("projectUser", td.getProjectLeader());
	// }else{
	// putObjToContext("projectUser", "");
	// }
	// //接收日期
	// if(st.getSendDate()!=null){
	// DateFormat format0=new SimpleDateFormat("yyyy-MM-dd");
	// String rd=format0.format(st.getSendDate());
	// putObjToContext("receiveDate", rd);
	// }else{
	// putObjToContext("receiveDate", "");
	// }
	// //检测日期
	// DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
	// String cd=format.format(tst.getCreateDate());
	// putObjToContext("checkDate", cd);
	// //样品检测人
	// putObjToContext("checkUser", tst.getCreateUser().getName());
	// //报告撰写人
	// putObjToContext("reportUser", user);
	// //报告审核人
	// putObjToContext("confirmUser",user);
	// //发布日期
	// Date d=new Date();
	// DateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// String stime=format1.format(d);
	// putObjToContext("publishDate", stime);
	// }
	//
	// //文库============================================================
	//
	// try {
	// if (rti != null) {
	// FileInfo a = rti.getAttach();
	//
	// if (techReport.getReportFile() != null && reloadAction.equals("")) {
	// putObjToContext("fileId", techReport.getReportFile().getId());
	// putObjToContext("fileExist", "true");
	// } else {
	// if (a != null){
	// putObjToContext("fileId", a.getId());
	// }
	// }
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// return dispatcher("");
	// }
	//
	// return
	// dispatcher("/WEB-INF/page/experiment/techreport/editReportGen.jsp");
	//
	// }
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TechReportService getReportService() {
		return reportService;
	}

	public void setReportService(TechReportService reportService) {
		this.reportService = reportService;
	}

	public TechReport getTechReport() {
		return techReport;
	}

	public void setTechReport(TechReport techReport) {
		this.techReport = techReport;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * ajax加载出报告样本信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "showReportData")
	public void showReportData() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.reportService
					.showTechReportList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
