package com.biolims.experiment.techreport.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.techreport.service.TechReportMainService;

public class TechReportEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		TechReportMainService mbService = (TechReportMainService) ctx.getBean("techReportMainService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
