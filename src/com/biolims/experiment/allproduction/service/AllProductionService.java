package com.biolims.experiment.allproduction.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.cell.culture.dao.CellPrimaryCultureDao;
import com.biolims.experiment.cell.cytokine.dao.CytokineProductionDao;
import com.biolims.experiment.cell.immunecell.dao.ImmuneCellProductionDao;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.plasma.dao.BloodSplitDao;
import com.biolims.experiment.production.dao.ProductionPlanDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleOrder;

@Service
@Transactional
public class AllProductionService {

	@Resource
	private CellPrimaryCultureDao cellPrimaryCultureDao;
	@Resource
	private CellPassageDao cellPassageDao;
	@Resource
	private BloodSplitDao bloodSplitDao;
	@Resource
	private ImmuneCellProductionDao cellProductionDao;
	@Resource
	private CytokineProductionDao cytokineProductionDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private ProductionPlanDao productionPlanDao;

	@SuppressWarnings("unchecked")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> findAllProductionList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> list = new ArrayList<Object>();
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		// //原代
		// //除完成的数据
		// List<CellPrimaryCulture> l1 =
		// cellPrimaryCultureDao.findCellPrimaryCultureList();
		//
		// if(l1.size()>0) {
		// for (CellPrimaryCulture cellPrimaryCulture : l1) {
		// String workUser1 = "";
		// List<CellPrimaryCultureTemplate> tl1 =
		// cellPrimaryCultureDao.findTemplateList(cellPrimaryCulture.getId());
		// if(tl1.size()>0) {
		// for (CellPrimaryCultureTemplate t1 : tl1) {
		// if(t1.getTestUserList()!=null&&!"".equals(t1.getTestUserList())) {
		// workUser1 += t1.getTestUserList() + ",";
		// }
		// }
		// }
		// if(!"".equals(workUser1)) {
		// workUser1 = workUser1.substring(0, workUser1.length()-1);
		// }
		// cellPrimaryCulture.setWorkUser(workUser1);
		// commonDAO.saveOrUpdate(cellPrimaryCulture);
		// }
		// }
		// List<CellPrimaryCulture> list1 =
		// cellPrimaryCultureDao.findCellPrimaryCultureListByUser(user.getName());
		//
		// 传代
		// List<CellPassage> l2 = cellPassageDao.findCellPassageList();
		// if(l2.size()>0) {
		// for (CellPassage cellPassage : l2) {
		// String workUser2 = "";
		// List<CellPassageTemplate> tl2 =
		// cellPassageDao.findTemplateList(cellPassage.getId());
		// for (CellPassageTemplate t2 : tl2) {
		// if(t2.getTestUserList()!=null&&!"".equals(t2.getTestUserList())) {
		// workUser2 += t2.getTestUserList() + ",";
		// }
		// }
		// if(!"".equals(workUser2)) {
		// workUser2 = workUser2.substring(0, workUser2.length()-1);
		// }
		// cellPassage.setWorkUser(workUser2);
		// commonDAO.saveOrUpdate(cellPassage);
		// }
		// }
		// 传代
		List<CellPassage> l2 = productionPlanDao.findCellPassageList();
		if (l2.size() > 0) {
			for (CellPassage cellPassage : l2) {
				String workUser2 = "";
				List<CellProductionRecord> tl2 = productionPlanDao.findTemplateList(cellPassage.getId());
				for (CellProductionRecord t2 : tl2) {
					if (t2.getTempleOperator() != null && !"".equals(t2.getTempleOperator())) {
						if (workUser2.indexOf(t2.getTempleOperator().getName()) != -1) {

						} else {
							workUser2 += t2.getTempleOperator().getName() + ",";
						}
					}
				}
				if (!"".equals(workUser2)) {
					workUser2 = workUser2.substring(0, workUser2.length() - 1);
				}
				cellPassage.setWorkUser(workUser2);
				commonDAO.saveOrUpdate(cellPassage);
			}
		}
		List<CellPassage> list2 = null;
		// if("admin".equals(user.getId())){
		// list2 = productionPlanDao.findCellPassageList("1");
		// }else{
		list2 = cellPassageDao.findCellPassageListByUser(user.getId());
		// }
		if (!list2.isEmpty()) {
			for (int i = 0; i < list2.size(); i++) {
				String pc = list2.get(i).getBatch();
				SampleOrder so = new SampleOrder();
				so = productionPlanDao.getSampleOrderBybatch(pc);
				if (so != null) {
					list2.get(i).setPatientName(so.getName());
					list2.get(i).setFiltrateCode(so.getFiltrateCode());
					commonDAO.saveOrUpdate(list2.get(i));
				}
			}
		}
		//
		// //产品干
		// List<PlasmaTask> l3 = bloodSplitDao.findBloodSplitList();
		// if(l3.size()>0) {
		// for (PlasmaTask plasmaTask : l3) {
		// String workUser3 = "";
		// List<PlasmaTaskTemplate> tl3 =
		// bloodSplitDao.findTemplateByzId(plasmaTask.getId());
		// if(tl3.size()>0) {
		// for (PlasmaTaskTemplate t3 : tl3) {
		// if(t3.getTestUserList()!=null&&!"".equals(t3.getTestUserList())) {
		// workUser3 += t3.getTestUserList() + ",";
		// }
		// }
		// if(!"".equals(workUser3)) {
		// workUser3 = workUser3.substring(0, workUser3.length()-1);
		// }
		// plasmaTask.setWorkUser(workUser3);
		// commonDAO.saveOrUpdate(plasmaTask);
		// }
		// }
		// }
		// List<PlasmaTask> list3 =
		// bloodSplitDao.findBloodSplitTaskListByUser(user.getName());
		//
		// //免疫细胞
		// List<ImmuneCellProduction> l4 = cellProductionDao.findImmuneCellList();
		// if(l4.size()>0) {
		// for (ImmuneCellProduction immuneCellProduction : l4) {
		// String workUser4 = "";
		// List<ImmuneCellProductionTemplate> tl4 =
		// cellProductionDao.findTemplateList(immuneCellProduction.getId());
		// if(tl4.size()>0) {
		// for (ImmuneCellProductionTemplate t4 : tl4) {
		// if(t4.getTestUserList()!=null&&!"".equals(t4.getTestUserList())) {
		// workUser4 += t4.getTestUserList() + ",";
		// }
		// }
		// }
		// if(!"".equals(workUser4)) {
		// workUser4 = workUser4.substring(0, workUser4.length()-1);
		// }
		// immuneCellProduction.setWorkUser(workUser4);
		// commonDAO.saveOrUpdate(immuneCellProduction);
		// }
		// }
		//
		// List<ImmuneCellProduction> list4 =
		// cellProductionDao.findImmuneCellListByUser(user.getName());
		//
		// //细胞因子
		// List<CytokineProduction> l5 = cytokineProductionDao.findCytokineList();
		// if(l5.size()>0) {
		// for (CytokineProduction cytokineProduction : l5) {
		// String workUser5 = "";
		// List<CytokineProductionTemplate> tl5 =
		// cytokineProductionDao.findTemplateList(cytokineProduction.getId());
		// if(tl5.size()>0) {
		// for (CytokineProductionTemplate t5 : tl5) {
		// if(t5.getTestUserList()!=null&&!"".equals(t5.getTestUserList())) {
		// workUser5 += t5.getTestUserList() + ",";
		// }
		// }
		// }
		// if(!"".equals(workUser5)) {
		// workUser5 = workUser5.substring(0, workUser5.length()-1);
		// }
		// cytokineProduction.setWorkUser(workUser5);
		// commonDAO.saveOrUpdate(cytokineProduction);
		// }
		// }
		// List<CytokineProduction> list5 =
		// cytokineProductionDao.findCytokineListByUser(user.getName());
		// list.addAll(list1);
		list.addAll(list2);
		// list.addAll(list3);
		// list.addAll(list4);
		// list.addAll(list5);
		map.put("list", list);
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		return map;
	}
	@SuppressWarnings("unchecked")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> findAllProductionList1(Integer start, Integer length, String query, String col,
			String sort, String pici) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> list = new ArrayList<Object>();
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		// 传代
		List<CellPassage> l2 = productionPlanDao.findCellPassageList1(pici);
		if (l2.size() > 0) {
			for (CellPassage cellPassage : l2) {
				String workUser2 = "";
				List<CellProductionRecord> tl2 = productionPlanDao.findTemplateList(cellPassage.getId());
				for (CellProductionRecord t2 : tl2) {
					if (t2.getTempleOperator() != null && !"".equals(t2.getTempleOperator())) {
						if (workUser2.indexOf(t2.getTempleOperator().getName()) != -1) {

						} else {
							workUser2 += t2.getTempleOperator().getName() + ",";
						}
					}
				}
				if (!"".equals(workUser2)) {
					workUser2 = workUser2.substring(0, workUser2.length() - 1);
				}
				cellPassage.setWorkUser(workUser2);
				commonDAO.saveOrUpdate(cellPassage);
			}
		}
		List<CellPassage> list2 = null;
		list2 = cellPassageDao.findCellPassageListByUser1(user.getId(),pici);
		if (!list2.isEmpty()) {
			for (int i = 0; i < list2.size(); i++) {
				String pc = list2.get(i).getBatch();
				SampleOrder so = new SampleOrder();
				so = productionPlanDao.getSampleOrderBybatch(pc);
				if (so != null) {
					list2.get(i).setName(so.getName());
					list2.get(i).setFiltrateCode(so.getFiltrateCode());
					commonDAO.saveOrUpdate(list2.get(i));
				}
			}
		}
		list.addAll(list2);
		map.put("list", list);
		map.put("recordsTotal", list.size());
		map.put("recordsFiltered", list.size());
		return map;
	}
}
