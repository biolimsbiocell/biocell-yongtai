package com.biolims.experiment.allproduction.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.experiment.allproduction.service.AllProductionService;
import com.biolims.experiment.cell.culture.service.CellPrimaryCultureService;
import com.biolims.experiment.cell.cytokine.service.CytokineProductionService;
import com.biolims.experiment.cell.immunecell.service.ImmuneCellProductionService;
import com.biolims.experiment.cell.passage.service.CellPassageService;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.util.HttpUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/allproduction")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
/**
 * 用于主页生产代办
 * 
 * @ClassName: AllProductionAction
 * @Description: TODO
 * @author 孙灵达
 * @date 2018年9月20日
 */
public class AllProductionAction extends BaseActionSupport {

	private static final long serialVersionUID = -4860419239694620118L;
	@Resource
	private CellPrimaryCultureService cellPrimaryCultureService;
	@Resource
	private CellPassageService cellPassageService;
	@Resource
	private BloodSplitService bloodSplitService;
	@Resource
	private ImmuneCellProductionService immuneCellProductionService;
	@Resource
	private CytokineProductionService cytokineProductionService;
	@Autowired
	private AllProductionService allProductionService;

	/**
	 * 查询所有的生产任务template中有结束时间的,并显示对应的操作员生产待办上面 @Title:
	 * showAllProductionList @Description: TODO @param @return void @author
	 * 孙灵达 @date 2018年9月20日 @throws
	 */
	@Action(value = "showAllProductionList")
	public void showAllProductionList() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");

		try {
			Map<String, Object> result = allProductionService.findAllProductionList(start, length, query, col, sort);
			List<Object> list = (List<Object>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("state", "");
			map.put("scopeName", "");
			map.put("workUser", "");
			map.put("batch", "");
			map.put("filtrateCode", "");
			
			map.put("patientName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 查询所有的生产任务template中有结束时间的,并显示对应的操作员生产待办上面 @Title:
	 * showAllProductionList @Description: TODO @param @return void @author
	 * 孙灵达 @date 2018年9月20日 @throws
	 */
	@Action(value = "showAllProductionList1")
	public void showAllProductionList1() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		
		String pici = getParameterFromRequest("pici");

		try {
			Map<String, Object> result = allProductionService.findAllProductionList1(start, length, query, col, sort, pici);
			List<Object> list = (List<Object>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("state", "");
			map.put("scopeName", "");
			map.put("workUser", "");
			map.put("batch", "");
			map.put("filtrateCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
