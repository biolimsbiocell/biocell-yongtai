package com.biolims.experiment.snpjc.cross.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.snpjc.cross.service.SnpCrossService;

public class SnpCrossEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SnpCrossService mbService = (SnpCrossService) ctx.getBean("snpCrossService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
