package com.biolims.experiment.snpjc.cross.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.producer.model.ProducerTaskResult;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisTemp;
import com.biolims.experiment.snpjc.cross.dao.SnpCrossDao;
import com.biolims.experiment.snpjc.cross.model.SnpCross;
import com.biolims.experiment.snpjc.cross.model.SnpCrossCos;
import com.biolims.experiment.snpjc.cross.model.SnpCrossItem;
import com.biolims.experiment.snpjc.cross.model.SnpCrossReagent;
import com.biolims.experiment.snpjc.cross.model.SnpCrossResult;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemp;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemplate;
import com.biolims.experiment.snpjc.sample.model.SnpSample;
import com.biolims.experiment.snpjc.sample.model.SnpSampleCos;
import com.biolims.experiment.snpjc.sample.model.SnpSampleItem;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleReagent;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemplate;
import com.biolims.experiment.snpjc.sj.model.SnpSj;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpCrossService {
	@Resource
	private SnpCrossDao snpCrossDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSnpCrossList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpCrossDao.selectSnpCrossList(mapForQuery, startNum, limitNum,
				dir, sort);
	}
	
	public Map<String, Object> findSnpCrossListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpCrossDao.selectSnpCrossListByState(mapForQuery, startNum, limitNum,
				dir, sort);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpCross i) throws Exception {

		snpCrossDao.saveOrUpdate(i);

	}

	public SnpCross get(String id) {
		SnpCross snpCross = commonDAO.get(SnpCross.class, id);
		return snpCross;
	}

	public Map<String, Object> findSnpCrossItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpCrossDao.selectSnpCrossItemList(scId,
				startNum, limitNum, dir, sort);
		List<SnpCrossItem> list = (List<SnpCrossItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpCrossTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpCrossDao.selectSnpCrossTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<SnpCrossTemplate> list = (List<SnpCrossTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSnpCrossReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpCrossDao.selectSnpCrossReagentList(
				scId, startNum, limitNum, dir, sort);
		List<SnpCrossReagent> list = (List<SnpCrossReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpCrossCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpCrossDao.selectSnpCrossCosList(scId,
				startNum, limitNum, dir, sort);
		List<SnpCrossCos> list = (List<SnpCrossCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpCrossResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpCrossDao.selectSnpCrossResultList(scId,
				startNum, limitNum, dir, sort);
		List<SnpCrossResult> list = (List<SnpCrossResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpCrossTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpCrossDao.selectSnpCrossTempList(mapForQuery, startNum, limitNum, dir, sort);
		Long count=(Long) result.get("total");
		List<SnpCrossTemp> list = (List<SnpCrossTemp>) result.get("list");
		
		Map<String,Object> result2=new HashMap<String, Object>();
		List<SnpCrossTemp> list2=new ArrayList<SnpCrossTemp>();
		for(SnpCrossTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				SampleOrder so=commonDAO.get(SampleOrder.class, s.getSampleOrder().getId());
				if(so.getChargeNote()!=null && !so.getChargeNote().equals("1")){
					t.setChargeNote(so.getChargeNote());
					t.setPatientName(so.getName());
					list2.add(t);
				}
			}
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpCrossItem(SnpCross sc, String itemDataJson)
			throws Exception {
		List<SnpCrossItem> saveItems = new ArrayList<SnpCrossItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpCrossItem scp = new SnpCrossItem();
			// 将map信息读入实体类
			scp = (SnpCrossItem) snpCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpCross(sc);

			saveItems.add(scp);
		}
		snpCrossDao.saveOrUpdateAll(saveItems);
		/*List<SnpCrossItem> snlist=snpCrossDao.getSnpItem(sc.getId());
		List<SnpCrossReagent> rglist=snpCrossDao.getSnpReagent(sc.getId());
		if(snlist.size()>0){
			if(rglist.size()>0){
				for(SnpCrossReagent sr:rglist){
					sr.setSampleNum(Double.valueOf(snlist.size()));
				}
			}
		}*/
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpCrossItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpCrossItem scp = snpCrossDao.get(SnpCrossItem.class, id);
			snpCrossDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpCrossTemplate(SnpCross sc, String itemDataJson)
			throws Exception {
		List<SnpCrossTemplate> saveItems = new ArrayList<SnpCrossTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpCrossTemplate scp = new SnpCrossTemplate();
			// 将map信息读入实体类
			scp = (SnpCrossTemplate) snpCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpCross(sc);

			saveItems.add(scp);
		}
		snpCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpCrossTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			SnpCrossTemplate scp = snpCrossDao.get(SnpCrossTemplate.class, id);
			snpCrossDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpCrossReagent(SnpCross sc, String itemDataJson)
			throws Exception {
		List<SnpCrossReagent> saveItems = new ArrayList<SnpCrossReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpCrossReagent scp = new SnpCrossReagent();
			// 将map信息读入实体类
			scp = (SnpCrossReagent) snpCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpCross(sc);
			
			saveItems.add(scp);
		}
		snpCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpCrossReagent(String[] ids) throws Exception {
		for (String id : ids) {
			SnpCrossReagent scp = snpCrossDao.get(SnpCrossReagent.class, id);
			snpCrossDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpCrossCos(SnpCross sc, String itemDataJson)
			throws Exception {
		List<SnpCrossCos> saveItems = new ArrayList<SnpCrossCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpCrossCos scp = new SnpCrossCos();
			// 将map信息读入实体类
			scp = (SnpCrossCos) snpCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpCross(sc);

			saveItems.add(scp);
		}
		snpCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpCrossCos(String[] ids) throws Exception {
		for (String id : ids) {
			SnpCrossCos scp = snpCrossDao.get(SnpCrossCos.class, id);
			snpCrossDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOne(String ids, String model) throws Exception {
		if (model.equals("1")) {
			SnpCrossTemplate scp = snpCrossDao.get(SnpCrossTemplate.class, ids);
			if (scp != null) {
				snpCrossDao.delete(scp);
			}
		}
		if (model.equals("2")) {
			SnpCrossReagent scp2 = snpCrossDao.get(SnpCrossReagent.class, ids);
			if (scp2 != null) {
				snpCrossDao.delete(scp2);
			}
		}

		if (model.equals("3")) {
			SnpCrossCos scp3 = snpCrossDao.get(SnpCrossCos.class, ids);
			if (scp3 != null) {
				snpCrossDao.delete(scp3);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpCrossResult(SnpCross sc, String itemDataJson)
			throws Exception {
		List<SnpCrossResult> saveItems = new ArrayList<SnpCrossResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpCrossResult scp = new SnpCrossResult();
			// 将map信息读入实体类
			scp = (SnpCrossResult) snpCrossDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpCross(sc);
			saveItems.add(scp);
//			if (scp != null){
//				if (scp.getResult() != null && scp.getSubmit() != null &&
//						!scp.getResult().equals("") && !scp.getSubmit().equals("") ){
//					if(scp.getSubmit().equals("1")){
//						if(scp.getResult().equals("0")){
//							SnpAbnormal ka=new SnpAbnormal();
//							ka.setCode(scp.getCode());
//							ka.setSampleCode(scp.getSampleCode());
//							ka.setProductId(scp.getProductId());
//							ka.setProductName(scp.getProductName());
//							ka.setSampleType(scp.getSampleType());
//							ka.setTaskId(scp.getSnpCross().getId());
//							ka.setNote(scp.getNote());
//							ka.setTaskName("SNP杂交");
//							ka.setState("1");
//							
//							commonDAO.saveOrUpdate(ka);
//						}
//					}
//				}
//			}
		}
		snpCrossDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpCrossResult(String[] ids) throws Exception {
		for (String id : ids) {
			SnpCrossResult scp = snpCrossDao.get(SnpCrossResult.class, id);
			snpCrossDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpCrossTemp(String[] ids) throws Exception {
		for (String id : ids) {
			SnpCrossTemp scp = snpCrossDao.get(SnpCrossTemp.class, id);
			snpCrossDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpCross sc, Map jsonMap) throws Exception {
		if (sc != null) {
			snpCrossDao.saveOrUpdate(sc);
			/*List<SnpCrossReagent> rglist=snpCrossDao.getSnpReagent(sc.getId());
			if(rglist.size()>0){
				for(SnpCrossReagent sr:rglist){
					if(sr.getOneNum()!=null && 
							sr.getSampleNum()!=null){
						sr.setNum(sr.getOneNum()*sr.getSampleNum());
					}
				}
			}*/
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("snpCrossItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpCrossItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpCrossTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpCrossTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpCrossReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpCrossReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpCrossCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpCrossCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpCrossResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpCrossResult(sc, jsonStr);
			}
			List<SnpCrossItem> snlist=snpCrossDao.getSnpItem(sc.getId());
			List<SnpCrossReagent> rglist=snpCrossDao.getSnpReagent(sc.getId());
			 DecimalFormat df = new DecimalFormat("#.00");
			 Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(SnpCrossReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && sr.getXishu()!=null&&
								sr.getSampleNum()!=null){
							nums =Double.valueOf(df.format(sr.getOneNum()*sr.getSampleNum()*Double.valueOf(sr.getXishu())));
							sr.setNum(nums);
						}
					}
				}
			}
		}
	}

	/**
	 * 查询杂交结果
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showProducerTaskResultList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = snpCrossDao
				.setProducerTaskResultList(code);
		List<SnpCrossResult> list = (List<SnpCrossResult>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SnpCrossResult pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("chipNum", pt.getChipNum());
				map.put("sampleType", pt.getSampleType());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("note", pt.getNote());
				map.put("result", pt.getResult());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SnpCross kt = snpCrossDao.get(SnpCross.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		snpCrossDao.update(kt);
		
		submitSample(id,null);
		//完成后改变左侧表的状态
		List<SnpCrossResult> list=snpCrossDao
				.getSnpCrossResultList(kt.getId());
		if(list.size()>0){
			for(SnpCrossResult scp:list){
				SnpCrossTemp tt=commonDAO
						.get(SnpCrossTemp.class, scp.getTempId());
				if(tt!=null){
					tt.setState("2");
				}
				
//				if (scp != null) {
//					if (scp.getResult() != null) {
//						String isOk = scp.getResult();
//						if (isOk.equals("1")) {
//							DateFormat format = new SimpleDateFormat(
//									"yyyy-MM-dd HH:mm:ss");
//							sampleStateService
//									.saveSampleState(
//											scp.getCode(),
//											scp.getSampleCode(),
//											scp.getProductId(),
//											scp.getProductName(),
//											"",
//											DateUtil.format(kt.getCreateDate()),
//											format.format(new Date()),
//											"SnpCross",
//											"SNP杂交",
//											(User) ServletActionContext
//													.getRequest()
//													.getSession()
//													.getAttribute(
//															SystemConstants.USER_SESSION_KEY),
//											kt.getId(), "洗染扫描",
//											scp.getResult(), null, null, null,
//											null, null, null, null, null);
//							scp.setSubmit("1");
//							snpCrossDao.saveOrUpdate(scp);
//						} else {
//							SnpAbnormal ka=new SnpAbnormal();
//							ka.setCode(scp.getCode());
//							ka.setSampleCode(scp.getSampleCode());
//							ka.setProductId(scp.getProductId());
//							ka.setProductName(scp.getProductName());
//							ka.setSampleType(scp.getSampleType());
//							ka.setTaskId(scp.getSnpCross().getId());
//							ka.setNote(scp.getNote());
//							ka.setNextFlow("SNP样本处理");
//							ka.setNextFlowId("0031");
//							ka.setTaskName("SNP杂交");
//							ka.setState("1");
//							snpCrossDao.saveOrUpdate(ka);
//						}
//					}
//				}
			}
	   }
	}

// 提交样本
@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void submitSample(String id, String[] ids) throws Exception {
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// 获取主表信息
	SnpCross sc = this.snpCrossDao.get(SnpCross.class, id);
	// 获取结果表样本信息
	List<SnpCrossResult> list;
	if (ids == null)
		list = this.snpCrossDao.setResultById(id);
	else
		list = this.snpCrossDao.setResultByIds(ids);
	for (SnpCrossResult scp : list) {
		if (scp != null) {
					if(scp.getResult().equals("1")){
				        sampleStateService
						.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							DateUtil.format(sc.getCreateDate()),
							format.format(new Date()),
							"SnpCross",
							"SNP杂交",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), "洗染扫描",
							scp.getResult(), null, null, null,
							null, null, null, null, null);
				        scp.setSubmit("1");
						snpCrossDao.saveOrUpdate(scp);
					}else{
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("SNP样本处理");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getSnpCross().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else {
							SnpAbnormal ka=new SnpAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getSnpCross().getId());
							ka.setNote(scp.getNote());
							ka.setNextFlow("SNP样本处理");
							ka.setNextFlowId("0031");
							ka.setTaskName("SNP杂交");
							ka.setState("1");
							snpCrossDao.saveOrUpdate(ka);
						}
						scp.setSubmit("1");
					}
				}
			}	
      }

}

