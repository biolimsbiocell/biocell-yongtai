package com.biolims.experiment.snpjc.cross.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.snpjc.cross.model.SnpCross;
import com.biolims.experiment.snpjc.cross.model.SnpCrossCos;
import com.biolims.experiment.snpjc.cross.model.SnpCrossItem;
import com.biolims.experiment.snpjc.cross.model.SnpCrossReagent;
import com.biolims.experiment.snpjc.cross.model.SnpCrossResult;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemp;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemplate;
import com.biolims.experiment.snpjc.cross.service.SnpCrossService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/snpjc/cross/snpCross")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SnpCrossAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249103";
	@Autowired
	private SnpCrossService snpCrossService;
	private SnpCross snpCross = new SnpCross();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;
    //

	@Action(value = "showSnpCrossList1")
	public String showSnpCrossList1() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/showSnpReason.jsp");
	}

	@Action(value = "showSnpCrossListJson1")
	public void showSnpCrossListJson1() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpCrossService.findSnpCrossListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpCross> list = (List<SnpCross>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("crossDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	//
	@Action(value = "showSnpCrossList")
	public String showSnpCrossList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCross.jsp");
	}

	@Action(value = "showSnpCrossListJson")
	public void showSnpCrossListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpCrossService.findSnpCrossList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpCross> list = (List<SnpCross>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("crossDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "snpCrossSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSnpCrossList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossDialog.jsp");
	}

	@Action(value = "showDialogSnpCrossListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSnpCrossListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0) {
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		} else {
			map2Query.put("state", "1");
		}
		Map<String, Object> result = snpCrossService.findSnpCrossListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpCross> list = (List<SnpCross>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("crossDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSnpCross")
	public String editSnpCross() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			snpCross = snpCrossService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "snpCross");
		} else {
			snpCross.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			snpCross.setCreateUser(user);
			snpCross.setCreateDate(new Date());
			snpCross.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			snpCross.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doSnpCross");
//			Template t=tlist.get(0);
//			snpCross.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				snpCross.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(snpCross.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossEdit.jsp");
	}

	@Action(value = "copySnpCross")
	public String copySnpCross() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		snpCross = snpCrossService.get(id);
		snpCross.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = snpCross.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SnpCross";
			String markCode = "SNPZJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			snpCross.setId(autoID);
			snpCross.setName("SNP杂交-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("snpCrossItem", getParameterFromRequest("snpCrossItemJson"));

		aMap.put("snpCrossTemplate",
				getParameterFromRequest("snpCrossTemplateJson"));

		aMap.put("snpCrossReagent",
				getParameterFromRequest("snpCrossReagentJson"));

		aMap.put("snpCrossCos", getParameterFromRequest("snpCrossCosJson"));

		aMap.put("snpCrossResult",
				getParameterFromRequest("snpCrossResultJson"));

		snpCrossService.save(snpCross, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/snpjc/cross/snpCross/editSnpCross.action?id="
				+ snpCross.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/snpjc/cross/snpCross/editSnpCross.action?id="
		// + snpCross.getId());

	}

	@Action(value = "viewSnpCross")
	public String toViewSnpCross() throws Exception {
		String id = getParameterFromRequest("id");
		snpCross = snpCrossService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossEdit.jsp");
	}

	@Action(value = "showSnpCrossItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpCrossItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossItem.jsp");
	}

	@Action(value = "showSnpCrossItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpCrossItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpCrossService.findSnpCrossItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SnpCrossItem> list = (List<SnpCrossItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleType", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("snpCross-name", "");
			map.put("snpCross-id", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("chipNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpCrossItem")
	public void delSnpCrossItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpCrossService.delSnpCrossItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpCrossTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpCrossTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossTemplate.jsp");
	}

	@Action(value = "showSnpCrossTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpCrossTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpCrossService
					.findSnpCrossTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpCrossTemplate> list = (List<SnpCrossTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("snpCross-name", "");
			map.put("snpCross-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpCrossTemplate")
	public void delSnpCrossTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpCrossService.delSnpCrossTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpCrossReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpCrossReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossReagent.jsp");
	}

	@Action(value = "showSnpCrossReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpCrossReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpCrossService
					.findSnpCrossReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpCrossReagent> list = (List<SnpCrossReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("snpCross-name", "");
			map.put("snpCross-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("saveCondition", "");
			map.put("xishu", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpCrossReagent")
	public void delSnpCrossReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpCrossService.delSnpCrossReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpCrossCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpCrossCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossCos.jsp");
	}

	@Action(value = "showSnpCrossCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpCrossCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpCrossService.findSnpCrossCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SnpCrossCos> list = (List<SnpCrossCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("snpCross-name", "");
			map.put("snpCross-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpCrossCos")
	public void delSnpCrossCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpCrossService.delSnpCrossCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			String model = getParameterFromRequest("model");
			snpCrossService.delOne(ids, model);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpCrossResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpCrossResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossResult.jsp");
	}

	@Action(value = "showSnpCrossResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpCrossResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpCrossService
					.findSnpCrossResultList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SnpCrossResult> list = (List<SnpCrossResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("chipNum", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("state", "");
			map.put("snpCross-name", "");
			map.put("snpCross-id", "");
			map.put("note", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
/**
 * 提交样本
 * @throws Exception
 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.snpCrossService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpCrossResult")
	public void delSnpCrossResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpCrossService.delSnpCrossResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpCrossTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpCrossTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/cross/snpCrossTemp.jsp");
	}

	@Action(value = "showSnpCrossTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpCrossTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpCrossService.findSnpCrossTempList(
				map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<SnpCrossTemp> list = (List<SnpCrossTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("state", "");
		map.put("note", "");
		map.put("chargeNote", "");//缴费状态
		map.put("orderNumber", "");
		map.put("orderId", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpCrossTemp")
	public void delSnpCrossTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpCrossService.delSnpCrossTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 查询杂交结果
	 * 
	 * @throws Exception
	 */
	@Action(value = "setSnpCrossResult", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setSnpCrossResult() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.snpCrossService
					.showProducerTaskResultList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	 /**
	 * 保存SNP杂交明细
	 * @throws Exception
	 */
	@Action(value = "saveSnpCrossItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpCrossItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SnpCross sc=commonDAO.get(SnpCross.class, id);
			Map aMap = new HashMap();
			aMap.put("snpCrossItem",
					itemDataJson);
			if(sc!=null){
				snpCrossService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	 /**
	 * 保存SNP杂交结果
	 * @throws Exception
	 */
	@Action(value = "saveSnpCrossResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpCrossResult() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SnpCross sc=commonDAO.get(SnpCross.class, id);
			Map aMap = new HashMap();
			aMap.put("snpCrossResult",
					itemDataJson);
			if(sc!=null){
				snpCrossService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SnpCrossService getSnpCrossService() {
		return snpCrossService;
	}

	public void setSnpCrossService(SnpCrossService snpCrossService) {
		this.snpCrossService = snpCrossService;
	}

	public SnpCross getSnpCross() {
		return snpCross;
	}

	public void setSnpCross(SnpCross snpCross) {
		this.snpCross = snpCross;
	}

}
