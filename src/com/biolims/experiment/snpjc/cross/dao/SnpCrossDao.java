package com.biolims.experiment.snpjc.cross.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.producer.model.ProducerTaskResult;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.experiment.snpjc.cross.model.SnpCross;
import com.biolims.experiment.snpjc.cross.model.SnpCrossCos;
import com.biolims.experiment.snpjc.cross.model.SnpCrossItem;
import com.biolims.experiment.snpjc.cross.model.SnpCrossReagent;
import com.biolims.experiment.snpjc.cross.model.SnpCrossResult;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemp;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemplate;
import com.biolims.experiment.snpjc.sample.model.SnpSampleItem;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleReagent;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;

@Repository
@SuppressWarnings("unchecked")
public class SnpCrossDao extends BaseHibernateDao {
	public Map<String, Object> selectSnpCrossList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpCross where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpCross> list = new ArrayList<SnpCross>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> selectSnpCrossListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpCross where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key=" and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpCross> list = new ArrayList<SnpCross>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public Map<String, Object> selectSnpCrossItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpCrossItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossItem> list = new ArrayList<SnpCrossItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpCrossTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpCrossTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossTemplate> list = new ArrayList<SnpCrossTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpCrossReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpCrossReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossReagent> list = new ArrayList<SnpCrossReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpCrossCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpCrossCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossCos> list = new ArrayList<SnpCrossCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpCrossResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpCrossResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossResult> list = new ArrayList<SnpCrossResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*public Map<String, Object> selectSnpCrossTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpCrossTemp where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpCross.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossTemp> list = new ArrayList<SnpCrossTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}*/
	public Map<String, Object> selectSnpCrossTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpCrossTemp where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'  order by orderId,orderNumber";//and (productId like '%A0008%' or productId like '%A0014%')
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpCrossTemp> list = new ArrayList<SnpCrossTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * 查询杂交结果
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setProducerTaskResultList(String code)
			throws Exception {
		String hql = "from SnpCrossResult t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.snpCross in ("+ code +") and  t.result = '1'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpCrossResult> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 根据主表ID查询结果表
	 */
	public List<SnpCrossResult> getSnpCrossResultList(String id){
		String hql = " from SnpCrossResult where 1=1  and snpCross= '"+id+"'";
		List<SnpCrossResult> list=this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<SnpCrossItem> getSnpItem(String id) {
		String hql = "from SnpCrossItem t where 1=1  and snpCross='"+ id + "'";
		List<SnpCrossItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<SnpCrossReagent> getSnpReagent(String id) {
		String hql = "from SnpCrossReagent t where 1=1  and snpCross='"+ id + "'";
		List<SnpCrossReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<SnpCrossResult> setResultById(String code) {
		String hql = "from SnpCrossResult t where (submit is null or submit='') and snpCross.id='"
				+ code + "'";
		List<SnpCrossResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<SnpCrossResult> setResultByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from SnpCrossResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SnpCrossResult> list = this.getSession().createQuery(hql).list();
		return list;
	   }
	
}