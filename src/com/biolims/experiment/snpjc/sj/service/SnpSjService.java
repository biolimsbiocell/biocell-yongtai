package com.biolims.experiment.snpjc.sj.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisTemp;
import com.biolims.experiment.snpjc.cross.model.SnpCross;
import com.biolims.experiment.snpjc.sj.dao.SnpSjDao;
import com.biolims.experiment.snpjc.sj.model.SnpSj;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpSjService {
	@Resource
	private SnpSjDao snpSjDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSnpSjList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return snpSjDao.selectSnpSjList(mapForQuery, startNum, limitNum, dir,
				sort);
	}

	public Map<String, Object> findSnpSjListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpSjDao.selectSnpSjListByState(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpSj i) throws Exception {

		snpSjDao.saveOrUpdate(i);

	}

	public SnpSj get(String id) {
		SnpSj snpSj = commonDAO.get(SnpSj.class, id);
		return snpSj;
	}

	public Map<String, Object> findSnpSjItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = snpSjDao.selectSnpSjItemList(scId,
				startNum, limitNum, dir, sort);
		List<SnpSjItem> list = (List<SnpSjItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSjItem(SnpSj sc, String itemDataJson) throws Exception {
		List<SnpSjItem> saveItems = new ArrayList<SnpSjItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSjItem scp = new SnpSjItem();
			// 将map信息读入实体类
			scp = (SnpSjItem) snpSjDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSj(sc);

			saveItems.add(scp);
			// if (scp != null) {
			// if (scp.getResult() != null && scp.getSubmit() != null
			// && !scp.getResult().equals("")
			// && !scp.getSubmit().equals("")) {
			// if (scp.getSubmit().equals("1")) {
			// if (scp.getResult().equals("0")) {
			// SnpAbnormal ka = new SnpAbnormal();
			// ka.setCode(scp.getCode());
			// ka.setSampleCode(scp.getSampleCode());
			// ka.setProductId(scp.getProductId());
			// ka.setProductName(scp.getProductName());
			// ka.setSampleType(scp.getSampleType());
			// ka.setTaskId(scp.getSnpSj().getId());
			// ka.setNote(scp.getNote());
			// ka.setTaskName("洗染扫描");
			// ka.setState("1");
			//
			// commonDAO.saveOrUpdate(ka);
			// }
			// }
			// }
			// }
		}
		snpSjDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSjItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSjItem scp = snpSjDao.get(SnpSjItem.class, id);
			snpSjDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSjItemOne(String ids) throws Exception {
		SnpSjItem scp = snpSjDao.get(SnpSjItem.class, ids);
		snpSjDao.delete(scp);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpSj sc, Map jsonMap) throws Exception {
		if (sc != null) {
			snpSjDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("snpSjItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSjItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 查询杂交结果
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showSnpSjItemList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = snpSjDao.setSnpSjItemList(code);
		List<SnpSjItem> list = (List<SnpSjItem>) result.get("list");

		if (list != null && list.size() > 0) {
			for (SnpSjItem pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("chipNum", pt.getChipNum());
				map.put("sampleType", pt.getSampleType());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("note", pt.getNote());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SnpSj kt = snpSjDao.get(SnpSj.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		snpSjDao.update(kt);

		// 完成后改变所选杂交实验的状态
		if (kt.getSnpCross() != null) {
			SnpCross sc = commonDAO.get(SnpCross.class, kt.getSnpCross()
					.getId());
			if (sc != null) {
				sc.setState("2");
			}
		}
		submitSample(id, null);
	}

	// 提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		SnpSj sc = this.snpSjDao.get(SnpSj.class, id);
		// 获取结果表样本信息

		List<SnpSjItem> list;
		if (ids == null)
			list = this.snpSjDao.setResultById(id);
		else
			list = this.snpSjDao.setResultByIds(ids);
		for (SnpSjItem scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && (scp.getSubmit() == null||"".equals(scp.getSubmit()))) {
					String isOk = scp.getResult();
					if (isOk.equals("1")) {
						SnpAnalysisTemp k = new SnpAnalysisTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
						k.setChipNum(scp.getChipNum());
						k.setState("1");
						snpSjDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(sc.getCreateDate()),
										format.format(new Date()),
										"SnpSj",
										"SNP-Array（芯片）洗染扫描",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "SNP-Array（芯片）数据分析",
										scp.getResult(), null, null, null,
										null, null, null, null, null);
						scp.setSubmit("1");
						snpSjDao.saveOrUpdate(scp);
					} else {
						SnpAbnormal ka = new SnpAbnormal();
						ka.setCode(scp.getCode());
						ka.setSampleCode(scp.getSampleCode());
						ka.setProductId(scp.getProductId());
						ka.setProductName(scp.getProductName());
						ka.setSampleType(scp.getSampleType());
						ka.setTaskId(scp.getSnpSj().getId());
						ka.setNote(scp.getNote());
						ka.setNextFlow("SNP样本处理");
						ka.setNextFlowId("0031");
						ka.setTaskName("洗染扫描");
						ka.setState("1");
						snpSjDao.saveOrUpdate(ka);
					}
				}
			}
		}
	}
}
