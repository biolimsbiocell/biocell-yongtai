package com.biolims.experiment.snpjc.sj.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.snpjc.sj.service.SnpSjService;

public class SnpSjEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SnpSjService mbService = (SnpSjService) ctx.getBean("snpSjService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
