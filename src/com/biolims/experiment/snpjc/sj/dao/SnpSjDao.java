package com.biolims.experiment.snpjc.sj.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.producer.model.ProducerTaskResult;
import com.biolims.experiment.snpjc.sj.model.SnpSj;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;

@Repository
@SuppressWarnings("unchecked")
public class SnpSjDao extends BaseHibernateDao {
	public Map<String, Object> selectSnpSjList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpSj where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpSj> list = new ArrayList<SnpSj>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSnpSjListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpSj where 1=1 ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpSj> list = new ArrayList<SnpSj>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSnpSjItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSjItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSj.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSjItem> list = new ArrayList<SnpSjItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 选择上机加载子表信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setSnpSjItemList(String code) throws Exception {
		String hql = "from SnpSjItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.snpSj='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSjItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据样本编号查询
	public List<SnpSjItem> setResultById(String code) {
		String hql = "from SnpSjItem t where (submit is null or submit='') and snpSj.id='"
				+ code + "'";
		List<SnpSjItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SnpSjItem> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SnpSjItem t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SnpSjItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
}