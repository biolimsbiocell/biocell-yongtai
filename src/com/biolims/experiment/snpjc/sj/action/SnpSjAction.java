package com.biolims.experiment.snpjc.sj.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.bobs.sample.model.BobsSample;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.snpjc.cross.service.SnpCrossService;
import com.biolims.experiment.snpjc.sj.model.SnpSj;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;
import com.biolims.experiment.snpjc.sj.service.SnpSjService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/snpjc/sj/snpSj")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SnpSjAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249104";
	@Autowired
	private SnpSjService snpSjService;
	private SnpSj snpSj = new SnpSj();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private SnpCrossService snpCrossService;
	

	//我的方法
	@Action(value = "showSnpSjList1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSubmitReason1() throws Exception {
		String type = getRequest().getParameter("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/showSnpReason.jsp");
	}
//	@Action(value = "showBobsSampleListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showBobsSampleListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = snpCrossService.findSnpCrossListByState(
//				map2Query, startNum, limitNum, dir, sort);
//		Long count = (Long) result.get("total");
//		List<BobsSample> list = (List<BobsSample>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("createUser-id", "");
//		map.put("createUser-name", "");
//		map.put("createDate", "yyyy-MM-dd");
//		map.put("template-id", "");
//		map.put("template-name", "");
//		map.put("state", "");
//		map.put("stateName", "");
//		map.put("note", "");
//		map.put("acceptUser-id", "");
//		map.put("acceptUser-name", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
	@Action(value = "showSnpSjList")
	public String showSnpSjList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/snpSj.jsp");
	}

	@Action(value = "showSnpSjListJson")
	public void showSnpSjListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpSjService.findSnpSjList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpSj> list = (List<SnpSj>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sjDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		
		map.put("snpCrossId", "");
		map.put("snpCrossName", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "snpSjSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSnpSjList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/snpSjDialog.jsp");
	}

	@Action(value = "showDialogSnpSjListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSnpSjListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpSjService.findSnpSjListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpSj> list = (List<SnpSj>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sjDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("snpCross-id", "");
		map.put("snpCross-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSnpSj")
	public String editSnpSj() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			snpSj = snpSjService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "snpSj");
		} else {
			snpSj.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			snpSj.setCreateUser(user);
			snpSj.setCreateDate(new Date());
			UserGroup u=comSearchDao.get(UserGroup.class,"SNP001");
			snpSj.setAcceptUser(u);
			snpSj.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			snpSj.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(snpSj.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/snpSjEdit.jsp");
	}

	@Action(value = "copySnpSj")
	public String copySnpSj() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		snpSj = snpSjService.get(id);
		snpSj.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/snpSjEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = snpSj.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SnpSj";
			String markCode = "SNPSJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			snpSj.setId(autoID);
			snpSj.setName("SNP洗染扫描-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("snpSjItem", getParameterFromRequest("snpSjItemJson"));

		snpSjService.save(snpSj, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/snpjc/sj/snpSj/editSnpSj.action?id="
				+ snpSj.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return redirect("/experiment/snpjc/sj/snpSj/editSnpSj.action?id="
		// + snpSj.getId());

	}

	@Action(value = "viewSnpSj")
	public String toViewSnpSj() throws Exception {
		String id = getParameterFromRequest("id");
		snpSj = snpSjService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/snpSjEdit.jsp");
	}

	@Action(value = "showSnpSjItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSjItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sj/snpSjItem.jsp");
	}

	@Action(value = "showSnpSjItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSjItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSjService.findSnpSjItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SnpSjItem> list = (List<SnpSjItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("chipNum", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("state", "");
			map.put("snpSj-name", "");
			map.put("snpSj-id", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSjItem")
	public void delSnpSjItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSjService.delSnpSjItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSjItemOne")
	public void delSnpSjItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			snpSjService.delSnpSjItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 选择上机加载子表信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "setSnpSjResult", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setSnpSjResult() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.snpSjService
					.showSnpSjItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.snpSjService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	 /**
	 * 保存洗染扫描结果
	 * @throws Exception
	 */
	@Action(value = "saveSnpSjItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpSjItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SnpSj sc=commonDAO.get(SnpSj.class, id);
			Map aMap = new HashMap();
			aMap.put("snpSjItem",
					itemDataJson);
			if(sc!=null){
				snpSjService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SnpSjService getSnpSjService() {
		return snpSjService;
	}

	public void setSnpSjService(SnpSjService snpSjService) {
		this.snpSjService = snpSjService;
	}

	public SnpSj getSnpSj() {
		return snpSj;
	}

	public void setSnpSj(SnpSj snpSj) {
		this.snpSj = snpSj;
	}

}
