package com.biolims.experiment.snpjc.sample.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.snpjc.sample.service.SnpSampleService;

public class SnpSampleEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SnpSampleService mbService = (SnpSampleService) ctx.getBean("snpSampleService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
