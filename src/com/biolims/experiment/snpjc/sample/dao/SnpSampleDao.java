package com.biolims.experiment.snpjc.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSampelChResult;
import com.biolims.experiment.snpjc.sample.model.SnpSample;
import com.biolims.experiment.snpjc.sample.model.SnpSampleCos;
import com.biolims.experiment.snpjc.sample.model.SnpSampleItem;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePcrResult;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleReagent;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemplate;
import com.biolims.experiment.snpjc.sj.model.SnpSjItem;
import com.biolims.file.model.FileInfo;

@Repository
@SuppressWarnings("unchecked")
public class SnpSampleDao extends BaseHibernateDao {
	public Map<String, Object> selectSnpSampleList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpSample where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpSample> list = new ArrayList<SnpSample>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSnpSampleItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSampleItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSampleItem> list = new ArrayList<SnpSampleItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpSampleTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSampleTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSampleTemplate> list = new ArrayList<SnpSampleTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpSampleReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSampleReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSampleReagent> list = new ArrayList<SnpSampleReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpSampleCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSampleCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSampleCos> list = new ArrayList<SnpSampleCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpSamplePcrResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSamplePcrResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSamplePcrResult> list = new ArrayList<SnpSamplePcrResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpSampelChResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSampelChResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSampelChResult> list = new ArrayList<SnpSampelChResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpSamplePdhResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpSamplePdhResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpSample.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpSamplePdhResult> list = new ArrayList<SnpSamplePdhResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

//	public Map<String, Object> selectSnpSampleTempList(String scId,
//			Integer startNum, Integer limitNum, String dir, String sort)
//			throws Exception {
//		String hql = "from SnpSampleTemp where 1=1 ";
//		String key = "";
//		if (scId != null)
//			key = key + " and snpSample.id='" + scId + "'";
//		Long total = (Long) this.getSession()
//				.createQuery("select count(*) " + hql + key).uniqueResult();
//		List<SnpSampleTemp> list = new ArrayList<SnpSampleTemp>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null
//					&& sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			}
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key)
//						.setFirstResult(startNum).setMaxResults(limitNum)
//						.list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
	public Map<String, Object> selectSnpSampleTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpSampleTemp where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1' ";//and (productId like '%A0008%' or productId like '%A0014%')
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpSampleTemp> list = new ArrayList<SnpSampleTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	/**
	 * 根据主表ID查询结果表
	 */
	public List<SnpSamplePdhResult> getSnpSamplePdhResultList(String id){
		String hql = " from SnpSamplePdhResult where 1=1  and snpSample='"+id+"'";
		List<SnpSamplePdhResult> list=this.getSession().createQuery(hql).list();
		return list;
	}
	/** 根据实体明和id查询图片
	 * @param id
	 * @return
	 */
	public List<FileInfo> findFileName(String id,String model){
		String hql = "from FileInfo t where t.modelContentId='"+id+"' and t.ownerModel='"+model+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	

	// 根据样本编号查询
	public List<SnpSamplePdhResult> setResultById(String code) {
		String hql = "from SnpSamplePdhResult t where (submit is null or submit='') and snpSample.id='"
				+ code + "'";
		List<SnpSamplePdhResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SnpSamplePdhResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SnpSamplePdhResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SnpSamplePdhResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本编号查询临时表样本信息
	 * @param code
	 * @return
	 */
	public List<SnpSampleTemp> getTempByCode(String code) {
		String hql = "from SnpSampleTemp t where 1=1  and state='2' and code='"+ code + "'";
		List<SnpSampleTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<SnpSampleItem> getSnpSampleItem(String id) {
		String hql = "from SnpSampleItem t where 1=1  and snpSample='"+ id + "'";
		List<SnpSampleItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<SnpSampleReagent> getSnpSampleReagent(String id) {
		String hql = "from SnpSampleReagent t where 1=1  and snpSample='"+ id + "'";
		List<SnpSampleReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
}
