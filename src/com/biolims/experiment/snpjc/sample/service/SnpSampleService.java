package com.biolims.experiment.snpjc.sample.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.experiment.snpjc.cross.model.SnpCrossTemp;
import com.biolims.experiment.snpjc.sample.dao.SnpSampleDao;
import com.biolims.experiment.snpjc.sample.model.SnpSampelChResult;
import com.biolims.experiment.snpjc.sample.model.SnpSample;
import com.biolims.experiment.snpjc.sample.model.SnpSampleCos;
import com.biolims.experiment.snpjc.sample.model.SnpSampleItem;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePcrResult;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleReagent;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemplate;
import com.biolims.experiment.snpjc.sj.model.SnpSj;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.CreateReport;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpSampleService {
	@Resource
	private SnpSampleDao snpSampleDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSnpSampleList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpSampleDao.selectSnpSampleList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpSample i) throws Exception {

		snpSampleDao.saveOrUpdate(i);

	}

	public SnpSample get(String id) {
		SnpSample snpSample = commonDAO.get(SnpSample.class, id);
		return snpSample;
	}

	public Map<String, Object> findSnpSampleItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSampleItemList(scId,
				startNum, limitNum, dir, sort);
		List<SnpSampleItem> list = (List<SnpSampleItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpSampleTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSampleTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<SnpSampleTemplate> list = (List<SnpSampleTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSnpSampleReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSampleReagentList(
				scId, startNum, limitNum, dir, sort);
		List<SnpSampleReagent> list = (List<SnpSampleReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSnpSampleCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSampleCosList(scId,
				startNum, limitNum, dir, sort);
		List<SnpSampleCos> list = (List<SnpSampleCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpSamplePcrResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSamplePcrResultList(
				scId, startNum, limitNum, dir, sort);
		List<SnpSamplePcrResult> list = (List<SnpSamplePcrResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSnpSampelChResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSampelChResultList(
				scId, startNum, limitNum, dir, sort);
		List<SnpSampelChResult> list = (List<SnpSampelChResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSnpSamplePdhResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpSampleDao.selectSnpSamplePdhResultList(
				scId, startNum, limitNum, dir, sort);
		List<SnpSamplePdhResult> list = (List<SnpSamplePdhResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSnpSampleTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		
		Map<String, Object> result = snpSampleDao.selectSnpSampleTempList(mapForQuery, startNum, limitNum, dir, sort);
		List<SnpSampleTemp> list = (List<SnpSampleTemp>) result.get("list");
		Long count = (Long) result.get("total");
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<SnpSampleTemp> list2=new ArrayList<SnpSampleTemp>();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for(SnpSampleTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				SampleOrder so=commonDAO.get(SampleOrder.class, s.getSampleOrder().getId());
				if(so.getChargeNote()!=null && !so.getChargeNote().equals("1")){
					t.setChargeNote(so.getChargeNote());
					t.setPatientName(so.getName());
					if(so.getSamplingDate()!=null && !so.getSamplingDate().equals("")){
						t.setSamplingDate(format.format(so.getSamplingDate()));
					}
					list2.add(t);
				}
			}
		}
		result2.put("total", count);
		result2.put("list", list);
		return result2;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSampleItem(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSampleItem> saveItems = new ArrayList<SnpSampleItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSampleItem scp = new SnpSampleItem();
			// 将map信息读入实体类
			scp = (SnpSampleItem) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);

			saveItems.add(scp);
			/*if(scp!=null){
				Template t=commonDAO.get(Template.class, sc.getTemplate().getId());
				if(t!=null){
					if(t.getDicSampleType()!=null){
						DicSampleType d=commonDAO.get(DicSampleType.class,
								t.getDicSampleType().getId());
						if(scp.getDicSampleType()==null ||
								(scp.getDicSampleType()!=null && scp.getDicSampleType().equals(""))){
							if(d!=null){
								scp.setDicSampleType(d);
							}
						}
					}
					if(t.getProductNum()!=null){
						if(scp.getProductNum()==null || 
								(scp.getProductNum()!=null && scp.getProductNum().equals(""))){
							scp.setProductNum(t.getProductNum());
						}
					}
				}
			}*/
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
		/*List<SnpSampleItem> snlist=snpSampleDao.getSnpSampleItem(sc.getId());
		List<SnpSampleReagent> rglist=snpSampleDao.getSnpSampleReagent(sc.getId());
		if(snlist.size()>0){
			if(rglist.size()>0){
				for(SnpSampleReagent sr:rglist){
					sr.setSampleNum(Double.valueOf(snlist.size()));
				}
			}
		}*/
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSampleItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSampleItem scp = snpSampleDao.get(SnpSampleItem.class, id);
			snpSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSampleTemplate(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSampleTemplate> saveItems = new ArrayList<SnpSampleTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSampleTemplate scp = new SnpSampleTemplate();
			// 将map信息读入实体类
			scp = (SnpSampleTemplate) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);

			saveItems.add(scp);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSampleTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSampleTemplate scp = snpSampleDao.get(SnpSampleTemplate.class,
					id);
			snpSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSampleReagent(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSampleReagent> saveItems = new ArrayList<SnpSampleReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSampleReagent scp = new SnpSampleReagent();
			// 将map信息读入实体类
			scp = (SnpSampleReagent) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);
			
			saveItems.add(scp);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSampleReagent(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSampleReagent scp = snpSampleDao.get(SnpSampleReagent.class, id);
			snpSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSampleCos(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSampleCos> saveItems = new ArrayList<SnpSampleCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSampleCos scp = new SnpSampleCos();
			// 将map信息读入实体类
			scp = (SnpSampleCos) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);

			saveItems.add(scp);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSampleCos(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSampleCos scp = snpSampleDao.get(SnpSampleCos.class, id);
			snpSampleDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOne(String ids, String model) throws Exception {
		if (model.equals("1")) {
			SnpSampleTemplate scp = snpSampleDao.get(SnpSampleTemplate.class,
					ids);
			if (scp != null) {
				snpSampleDao.delete(scp);
			}
		}
		if (model.equals("2")) {
			SnpSampleReagent scp2 = snpSampleDao.get(SnpSampleReagent.class,
					ids);
			if (scp2 != null) {
				snpSampleDao.delete(scp2);
			}
		}

		if (model.equals("3")) {
			SnpSampleCos scp3 = snpSampleDao.get(SnpSampleCos.class, ids);
			if (scp3 != null) {
				snpSampleDao.delete(scp3);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSamplePcrResult(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSamplePcrResult> saveItems = new ArrayList<SnpSamplePcrResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSamplePcrResult scp = new SnpSamplePcrResult();
			// 将map信息读入实体类
			scp = (SnpSamplePcrResult) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);

			saveItems.add(scp);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSamplePcrResult(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSamplePcrResult scp = snpSampleDao.get(SnpSamplePcrResult.class,
					id);
			snpSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSampelChResult(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSampelChResult> saveItems = new ArrayList<SnpSampelChResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSampelChResult scp = new SnpSampelChResult();
			// 将map信息读入实体类
			scp = (SnpSampelChResult) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);

			saveItems.add(scp);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSampelChResult(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSampelChResult scp = snpSampleDao.get(SnpSampelChResult.class,
					id);
			snpSampleDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSamplePdhResult(SnpSample sc, String itemDataJson)
			throws Exception {
		List<SnpSamplePdhResult> saveItems = new ArrayList<SnpSamplePdhResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSamplePdhResult scp = new SnpSamplePdhResult();
			// 将map信息读入实体类
			scp = (SnpSamplePdhResult) snpSampleDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpSample(sc);

			saveItems.add(scp);
			commonDAO.saveOrUpdate(scp);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (scp != null){
				if (scp.getResult() != null && scp.getSubmit() != null &&
						!scp.getResult().equals("") && !scp.getSubmit().equals("") ){
				if(scp.getSubmit().equals("1")){
					if(scp.getResult().equals("1")){
						if(scp.getNextFlowId()!=null && !scp.getNextFlowId().equals("")){
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setState("1");
								commonDAO.saveOrUpdate(st);
								// 入库，改变SampleInfo中原始样本的状态为“待入库”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
								}
							} else if (scp.getNextFlowId().equals("0012")) {// 暂停
								// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
								}
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
								// 终止，改变SampleInfo中原始样本的状态为“实验终止”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
								}
							}else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}
						}
					}else{
						SnpAbnormal ka=new SnpAbnormal();
						ka.setCode(scp.getCode());
						ka.setSampleCode(scp.getSampleCode());
						ka.setProductId(scp.getProductId());
						ka.setProductName(scp.getProductName());
						ka.setOrderId(scp.getOrderId());
						ka.setSampleType(scp.getSampleType());
						ka.setTaskId(scp.getSnpSample().getId());
						ka.setNote(scp.getNote());
						ka.setTaskName("SNP样本处理");
						ka.setState("1");
						
						commonDAO.saveOrUpdate(ka);
					}
					sampleStateService
					.saveSampleState(
						scp.getCode(),
						scp.getSampleCode(),
						scp.getProductId(),
						scp.getProductName(),
						"",
						format.format(sc.getCreateDate()),
						format.format(new Date()),
						"SnpSample",
						"SNP样本处理",
						(User) ServletActionContext
								.getRequest()
								.getSession()
								.getAttribute(
										SystemConstants.USER_SESSION_KEY),
						sc.getId(), scp.getNextFlow(),
						scp.getResult(), null, null, null,
						null, null, null, null, null);
					}
				}
			}
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSamplePdhResult(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSamplePdhResult scp = snpSampleDao.get(SnpSamplePdhResult.class,
					id);
			snpSampleDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpSampleTemp(String[] ids) throws Exception {
		for (String id : ids) {
			SnpSampleTemp scp = snpSampleDao.get(SnpSampleTemp.class, id);
			snpSampleDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpSample sc, Map jsonMap) throws Exception {
		if (sc != null) {
			snpSampleDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("snpSampleItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSampleItem(sc, jsonStr);
			}
			//List<SnpSampleReagent> rglist=snpSampleDao.getSnpSampleReagent(sc.getId());
			
			jsonStr = (String) jsonMap.get("snpSampleTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSampleTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpSampleReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSampleReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpSampleCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSampleCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpSamplePcrResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSamplePcrResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpSampelChResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSampelChResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpSamplePdhResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpSamplePdhResult(sc, jsonStr);
			}
			List<SnpSampleItem> snlist=snpSampleDao.getSnpSampleItem(sc.getId());
			List<SnpSampleReagent> rglist=snpSampleDao.getSnpSampleReagent(sc.getId());
			 DecimalFormat df = new DecimalFormat("#.00");
			 Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(SnpSampleReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && 
								sr.getSampleNum()!=null && sr.getXishu()!=null){
			nums =Double.valueOf(df.format(sr.getOneNum()*sr.getSampleNum()*Double.valueOf(sr.getXishu())));
							sr.setNum(nums);
						//sr.setNum(sr.getOneNum()*sr.getSampleNum()*Double.valueOf(sr.getXishu()));
						}
					}
				}
			}
//			if(rglist.size()>0){
//				for(SnpSampleReagent sr:rglist){
//					if(sr.getOneNum()!=null && 
//							sr.getSampleNum()!=null){
//						sr.setNum(sr.getOneNum()*sr.getSampleNum());
//					}
//				}
//			}
		}
	}
	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SnpSample kt = snpSampleDao.get(SnpSample.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		snpSampleDao.update(kt);
		
		submitSample(id, null);
		//完成后改变左侧表的状态
//		List<SnpSamplePdhResult> list=snpSampleDao
//				.getSnpSamplePdhResultList(kt.getId());
//		if(list.size()>0){
//			for(SnpSamplePdhResult k:list){
//				SnpSampleTemp tt=commonDAO
//						.get(SnpSampleTemp.class, k.getTempId());
//				if(tt!=null){
//					tt.setState("2");
//				}
//			}
//		}
	}

	// 提交样本
		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void submitSample(String id, String[] ids) throws Exception {
			// 获取主表信息
			SnpSample sc = this.snpSampleDao.get(SnpSample.class, id);
			// 获取结果表样本信息

			List<SnpSamplePdhResult> list;
			if (ids == null)
				list = this.snpSampleDao.setResultById(id);
			else
				list = this.snpSampleDao.setResultByIds(ids);
			for (SnpSamplePdhResult scp : list) {
				if (scp != null) {
					if (scp.getResult() != null && scp.getSubmit() == null) {
						String isOk = scp.getResult();
						if (isOk.equals("1")) {
							SnpSampleTemp tt=commonDAO
									.get(SnpSampleTemp.class, scp.getTempId());
							if(tt!=null){
								tt.setState("2");
							}
							SnpCrossTemp k = new SnpCrossTemp();
							k.setCode(scp.getCode());
							k.setSampleCode(scp.getSampleCode());
							k.setProductId(scp.getProductId());
							k.setProductName(scp.getProductName());
							k.setSampleType(scp.getSampleType());
//							k.setChipNum(scp.getChipNum());
							k.setState("1");
							k.setOrderNumber(scp.getOrderNumber());
							k.setOrderId(id);
							snpSampleDao.saveOrUpdate(k);
							DateFormat format = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							sampleStateService
									.saveSampleState(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											DateUtil.format(sc.getCreateDate()),
											format.format(new Date()),
											"SnpSample",
											"SNP样本处理",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											sc.getId(), "SNP杂交",
											scp.getResult(), null, null, null,
											null, null, null, null, null);
							scp.setSubmit("1");
							snpSampleDao.saveOrUpdate(scp);
						} else {
							
							if(scp.getNextFlowId().equals("0038")){
								//提交不和合格的到报告终止
						    	EndReport cr = new EndReport();
								cr.setCode(scp.getCode());
								cr.setSampleCode(scp.getSampleCode());
								cr.setSampleType(scp.getSampleType());
								cr.setProductId(scp.getProductId());
								cr.setProductName(scp.getProductName());
								cr.setAdvice(scp.getNote());
								cr.setTaskType("SNP样本处理");
								cr.setTaskId(scp.getId());
								cr.setTaskResultId(scp.getSnpSample().getId());
								cr.setWaitDate(new Date());
						    	commonDAO.saveOrUpdate(cr);
							}else{
									SnpAbnormal ka = new SnpAbnormal();
									ka.setCode(scp.getCode());
									ka.setSampleCode(scp.getSampleCode());
									ka.setProductId(scp.getProductId());
									ka.setProductName(scp.getProductName());
									ka.setSampleType(scp.getSampleType());
									ka.setTaskId(scp.getSnpSample().getId());
									ka.setNote(scp.getNote());
									ka.setNextFlow("SNP样本处理");
									ka.setNextFlowId("0031");
									ka.setTaskName("SNP样本处理");
									ka.setState("1");
									snpSampleDao.saveOrUpdate(ka);
							}
							scp.setSubmit("1");
						}
					}
				}
			}
		}
	
	/**
	 * 保存PCR结果
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSamplePcrResult(String itemDataJson,String id) throws Exception {
		List<SnpSamplePcrResult> saveItems = new ArrayList<SnpSamplePcrResult>();
		SnpSample s=commonDAO.get(SnpSample.class, id);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSamplePcrResult sbi = new SnpSamplePcrResult();
			sbi = (SnpSamplePcrResult) snpSampleDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			sbi.setSnpSample(s);
			
			saveItems.add(sbi);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 保存纯化结果
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSampleCh(String itemDataJson,String id) throws Exception {
		List<SnpSampelChResult> saveItems = new ArrayList<SnpSampelChResult>();
		SnpSample s=commonDAO.get(SnpSample.class, id);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSampelChResult sbi = new SnpSampelChResult();
			sbi = (SnpSampelChResult) snpSampleDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			sbi.setSnpSample(s);
			
			saveItems.add(sbi);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 保存片段化结果
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpSamplePdh(String itemDataJson,String id) throws Exception {
		List<SnpSamplePdhResult> saveItems = new ArrayList<SnpSamplePdhResult>();
		SnpSample s=commonDAO.get(SnpSample.class, id);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpSamplePdhResult sbi = new SnpSamplePdhResult();
			sbi = (SnpSamplePdhResult) snpSampleDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			sbi.setSnpSample(s);
			
			saveItems.add(sbi);
		}
		snpSampleDao.saveOrUpdateAll(saveItems);
	}
}


	
