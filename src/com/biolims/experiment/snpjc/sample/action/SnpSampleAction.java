package com.biolims.experiment.snpjc.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.snpjc.sample.dao.SnpSampleDao;
import com.biolims.experiment.snpjc.sample.model.SnpSampelChResult;
import com.biolims.experiment.snpjc.sample.model.SnpSample;
import com.biolims.experiment.snpjc.sample.model.SnpSampleCos;
import com.biolims.experiment.snpjc.sample.model.SnpSampleItem;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePcrResult;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleReagent;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemplate;
import com.biolims.experiment.snpjc.sample.service.SnpSampleService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/snpjc/sample/snpSample")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SnpSampleAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249101";
	@Autowired
	private SnpSampleService snpSampleService;
	private SnpSample snpSample = new SnpSample();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SnpSampleDao snpSampleDao;
	@Resource
	private ComSearchDao comSearchDao;
	
	@Action(value = "showSnpSampleList")
	public String showSnpSampleList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSample.jsp");
	}

	@Action(value = "showSnpSampleListJson")
	public void showSnpSampleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpSampleService.findSnpSampleList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpSample> list = (List<SnpSample>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "snpSampleSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSnpSampleList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleDialog.jsp");
	}

	@Action(value = "showDialogSnpSampleListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSnpSampleListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpSampleService.findSnpSampleList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpSample> list = (List<SnpSample>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSnpSample")
	public String editSnpSample() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			snpSample = snpSampleService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "snpSample");
		} else {
			snpSample.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			snpSample.setCreateUser(user);
			snpSample.setCreateDate(new Date());
			snpSample.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			snpSample.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doSnpSample");
//			Template t=tlist.get(0);
//			snpSample.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				snpSample.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(snpSample.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleEdit.jsp");
	}

	@Action(value = "copySnpSample")
	public String copySnpSample() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		snpSample = snpSampleService.get(id);
		snpSample.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = snpSample.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SnpSample";
			String markCode = "SNPCL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			snpSample.setId(autoID);
			snpSample.setName("SNP样本处理-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("snpSampleItem", getParameterFromRequest("snpSampleItemJson"));

		aMap.put("snpSampleTemplate",
				getParameterFromRequest("snpSampleTemplateJson"));

		aMap.put("snpSampleReagent",
				getParameterFromRequest("snpSampleReagentJson"));

		aMap.put("snpSampleCos", getParameterFromRequest("snpSampleCosJson"));

		aMap.put("snpSamplePcrResult",
				getParameterFromRequest("snpSamplePcrResultJson"));

		aMap.put("snpSampelChResult",
				getParameterFromRequest("snpSampelChResultJson"));

		aMap.put("snpSamplePdhResult",
				getParameterFromRequest("snpSamplePdhResultJson"));

		snpSampleService.save(snpSample, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/snpjc/sample/snpSample/editSnpSample.action?id="
				+ snpSample.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/snpjc/sample/snpSample/editSnpSample.action?id="
		// + snpSample.getId());

	}

	@Action(value = "viewSnpSample")
	public String toViewSnpSample() throws Exception {
		String id = getParameterFromRequest("id");
		snpSample = snpSampleService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleEdit.jsp");
	}

	@Action(value = "showSnpSampleItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSampleItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleItem.jsp");
	}

	@Action(value = "showSnpSampleItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSampleItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService
					.findSnpSampleItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SnpSampleItem> list = (List<SnpSampleItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			//患者姓名
			map.put("patientName", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleType", "");
			map.put("sampleCode", "");
			//字段280、260
			map.put("od280", "");
			map.put("od260", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("xsyVolume", "");
			map.put("addVolume", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("note", "");
			map.put("tempId", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSampleItem")
	public void delSnpSampleItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
	
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSampleItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSampleTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSampleTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleTemplate.jsp");
	}

	@Action(value = "showSnpSampleTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSampleTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService
					.findSnpSampleTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpSampleTemplate> list = (List<SnpSampleTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSampleTemplate")
	public void delSnpSampleTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSampleTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSampleReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSampleReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleReagent.jsp");
	}

	@Action(value = "showSnpSampleReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSampleReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService
					.findSnpSampleReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpSampleReagent> list = (List<SnpSampleReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("saveCondition", "");
			map.put("xishu", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSampleReagent")
	public void delSnpSampleReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSampleReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSampleCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSampleCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleCos.jsp");
	}

	@Action(value = "showSnpSampleCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSampleCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService.findSnpSampleCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SnpSampleCos> list = (List<SnpSampleCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSampleCos")
	public void delSnpSampleCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSampleCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOne", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			String model = getParameterFromRequest("model");
			snpSampleService.delOne(ids, model);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSamplePcrResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSamplePcrResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSamplePcrResult.jsp");
	}

	@Action(value = "showSnpSamplePcrResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSamplePcrResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService
					.findSnpSamplePcrResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpSamplePcrResult> list = (List<SnpSamplePcrResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("dyResult", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("reason", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			map.put("note", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSamplePcrResult")
	public void delSnpSamplePcrResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSamplePcrResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSampelChResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSampelChResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampelChResult.jsp");
	}

	@Action(value = "showSnpSampelChResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSampelChResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService
					.findSnpSampelChResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpSampelChResult> list = (List<SnpSampelChResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("od280", "");
			map.put("od230", "");
			map.put("concentration", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("reason", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			map.put("note", "");
			map.put("tempId", "");
			//患者名字
			map.put("patientName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSampelChResult")
	public void delSnpSampelChResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSampelChResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSamplePdhResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSamplePdhResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSamplePdhResult.jsp");
	}

	@Action(value = "showSnpSamplePdhResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSamplePdhResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpSampleService
					.findSnpSamplePdhResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpSamplePdhResult> list = (List<SnpSamplePdhResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("orderNumber", "");
			map.put("checkCode", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("reason", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("snpSample-name", "");
			map.put("snpSample-id", "");
			map.put("note", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSamplePdhResult")
	public void delSnpSamplePdhResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSamplePdhResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpSampleTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpSampleTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/snpSampleTemp.jsp");
	}

	@Action(value = "showSnpSampleTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpSampleTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpSampleService.findSnpSampleTempList(
				map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<SnpSampleTemp> list = (List<SnpSampleTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("orderId", "");
		map.put("state", "");
		map.put("note", "");
		map.put("chargeNote", "");//缴费状态
		map.put("samplingDate", "");//采样日期-样本送检时间
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSampleTemp")
	public void delSnpSampleTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSampleService.delSnpSampleTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SnpSampleService getSnpSampleService() {
		return snpSampleService;
	}

	public void setSnpSampleService(SnpSampleService snpSampleService) {
		this.snpSampleService = snpSampleService;
	}

	public SnpSample getSnpSample() {
		return snpSample;
	}

	public void setSnpSample(SnpSample snpSample) {
		this.snpSample = snpSample;
	}
	/**
	 * 加载图片
	 * @return
	 * @throws Exception
	 */
	@Action(value = "loadPic", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String loadPic() throws Exception {
		String id=getRequest().getParameter("id");
		String model=getRequest().getParameter("model");
		List<FileInfo> flist=snpSampleDao.findFileName(id,model);
		putObjToContext("flist",flist);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/showTechAnalysisPic.jsp");
	}

	/**
	 * 提交样本
	 * @throws Exception
	 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.snpSampleService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 保存PCR结果
	 */
	@Action(value = "saveSnpSamplePcrResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpSamplePcrResult() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			String id = getRequest().getParameter("id");
			snpSampleService.saveSnpSamplePcrResult(itemDataJson,id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 保存纯化结果
	 */
	@Action(value = "saveSnpSampleCh",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpSampleCh() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			String id = getRequest().getParameter("id");
			snpSampleService.saveSnpSampleCh(itemDataJson,id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 保存片段化结果
	 */
	@Action(value = "saveSnpSamplePdh",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpSamplePdh() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			String id = getRequest().getParameter("id");
			snpSampleService.saveSnpSamplePdh(itemDataJson,id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	 /**
		 * 保存SNP样本处理明细
		 * @throws Exception
		 */
		@Action(value = "saveSnpSampleItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveSnpSampleItem() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				SnpSample sc=commonDAO.get(SnpSample.class, id);
				Map aMap = new HashMap();
				aMap.put("snpSampleItem",
						itemDataJson);
				if(sc!=null){
					snpSampleService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
}
