package com.biolims.experiment.snpjc.analysis.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.experiment.snpjc.analysis.service.SnpAnalysisService;

public class SnpAnalysisEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SnpAnalysisService mbService = (SnpAnalysisService) ctx.getBean("snpAnalysisService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
