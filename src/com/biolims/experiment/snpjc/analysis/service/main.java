package com.biolims.experiment.snpjc.analysis.service;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.cxf.binding.corba.wsdl.Array;

public class main {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		int count = 0;      
        String regEx = "[\\u4e00-\\u9fa5]";      
        //System.out.println(regEx);      
        String str = "arr[hg19] Xp22.33(599,293-1,356,065)x3 or Yp11.32(549,293-1,306,065)x3, 13q13.3q34(36,212,962-115,107,733)x3胎儿父母的SNP芯片检测有助于确定该重复片段是遗传而来或者是新发生的异常。";  
        str=str.replace("\r","");       //去回车键
        Pattern p = Pattern.compile(regEx);      
        Matcher m = p.matcher(str);      
        while (m.find()) {      
           for (int i = 0; i <= m.groupCount(); i++) {      
                count = count + 1;      
           }      
        } 
        //System.out.println(str.getBytes("ISO8859-1").length);
        // 运行结果：1
        //System.out.println(str.getBytes("GB2312").length);
        // 运行结果：2
        //System.out.println(str.getBytes("GBK").length);
        // 运行结果：3
        System.out.println("字节长度是："+str.getBytes("UTF-8").length);
        System.out.println("字节长度是："+str.getBytes("Unicode").length);
        System.out.println("字符串长度是："+str.length());
        System.out.println("初始字符串："+str);
        System.out.println("非汉字字符串："+str.replaceAll(regEx, ""));
        int num=str.length();					//字段长度
        int num0=str.getBytes("UTF-8").length;	//字节长度
        int num1=str.getBytes("Unicode").length;//字节长度
        System.out.println("初始字符串长度："+num);
        System.out.println("汉字字符的个数 :" + count);
		int count1=num-count;		//非汉字字符的个数
		int count2=(int)((count1)*0.5);//两个非汉字字符长度等于一个汉字长度
		int count3=count2+count;
		System.out.println("非汉字字符的个数:"+count1);
		String str1="";
		String str2="";
		String str3="";
		int n=0;
		if(num0>0){
			if(num0<=90){
				str1=str;
			}else{
		        for (int i=0; i <=(num0/90); i++) {
	        		str2=str.replace(str3, "");
	        		n=str2.getBytes("UTF-8").length;
	        		if(n>=90){
	        			str1+=bSubstring(str2, 90)+"\r";
	        			str3+=bSubstring(str2, 90);
	        		}else if(n<90 && n>0){
	        			str1+=bSubstring(str2, n)+"\r";
	        			str3+=bSubstring(str2, n);
	        		}
		        }
			}
		}
		System.out.println(n);
		System.out.println("输出已换行的字符串:");
		if(!str.replace(str3, "").equals("")){ 
			System.out.println(str1+bSubstring(str.replace(str3, ""), n));
		}else{
			System.out.println(str1);
		}
		System.out.println("输出字符串:");
		System.out.println(bSubstring(str, 90));
		System.out.println(bSubstring(str.replace(bSubstring(str, 90), ""), 90));
		//System.out.println(bSubstring(str.replace(bSubstring(str, 144), ""), 72));
		/*if(num0>0){
			if(num0<=20){
				str1=str;
			}else{
				int n=0;;
		        for (int i=1; i < (num0/20); i++) {
		        		str1+=(str.substring(20*(i-1), 20*i))+("\r");
		        		n=i;
		        }
		        str1+=(str.substring(20*n,num0));
			}
			System.out.println(str1);
		}*/
	}
	public boolean vd(String str){  
	    char[] chars=str.toCharArray();   
	    boolean isGB2312=false;   
	    for(int i=0;i<chars.length;i++){  
            byte[] bytes=(""+chars[i]).getBytes();   
            if(bytes.length==2){   
	            int[] ints=new int[2];   
	            ints[0]=bytes[0]& 0xff;   
	            ints[1]=bytes[1]& 0xff;   
	            if(ints[0]>=0x81 && ints[0]<=0xFE && ints[1]>=0x40 && ints[1]<=0xFE){   
                    isGB2312=true;   
                    break;   
	            }   
            }   
	    }   
	    return isGB2312;   
	}
	
	
	public boolean isNumeric(String str){  
	       Pattern pattern = Pattern.compile("[0-9]*");  
	       Matcher isNum = pattern.matcher(str);  
	       if( !isNum.matches() ) {  
	          return false;  
	       }  
	       return true;  
	}  
	
	
	public static String bSubstring(String s, int length) throws Exception
    {

        byte[] bytes = s.getBytes("Unicode");
        int n = 0; // 表示当前的字节数
        int i = 2; // 要截取的字节数，从第3个字节开始
        for (; i < bytes.length && n < length; i++)
        {
            // 奇数位置，如3、5、7等，为UCS2编码中两个字节的第二个字节
            if (i % 2 == 1)
            {
                n++; // 在UCS2第二个字节时n加1
            }
            else
            {
                // 当UCS2编码的第一个字节不等于0时，该UCS2字符为汉字，一个汉字算两个字节
                if (bytes[i] != 0)
                {
                    n++;
                }
            }
        }
        // 如果i为奇数时，处理成偶数
        if (i % 2 == 1)

        {
            // 该UCS2字符是汉字时，去掉这个截一半的汉字
            if (bytes[i - 1] != 0)
                i = i - 1;
            // 该UCS2字符是字母或数字，则保留该字符
            else
                i = i + 1;
        }

        return new String(bytes, 0, i, "Unicode");
    }
}
