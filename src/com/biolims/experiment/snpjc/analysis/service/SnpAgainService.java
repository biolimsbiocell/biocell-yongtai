package com.biolims.experiment.snpjc.analysis.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
import com.biolims.experiment.fish.fx.model.FishFxTaskTemp;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.experiment.snpjc.analysis.dao.SnpAgainDao;
import com.biolims.experiment.snpjc.analysis.dao.SnpAnalysisDao;
import com.biolims.experiment.snpjc.analysis.model.SnpAgainInstance;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysis;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
import com.biolims.experiment.snpjc.sample.model.SnpSampleTemp;
import com.biolims.experiment.snpjc.second.model.SnpSecondInstance;
import com.biolims.experiment.snpjc.sj.model.SnpSj;
import com.biolims.experiment.snpjc.first.dao.SnpFirstDao;
import com.biolims.experiment.snpjc.first.model.SnpFirstInstance;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.CreateReport;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpAgainService {
	@Resource
	private SnpAgainDao snpAgainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private ComSearchService comSearchService;
	@Resource
	private CommonService commonService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSnpAgainInstanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpAgainDao.selectSnpAgainInstanceList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpAgainInstance i) throws Exception {

		snpAgainDao.saveOrUpdate(i);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpAgainInstance(String itemDataJson)
			throws Exception {
		List<SnpAgainInstance> saveItems = new ArrayList<SnpAgainInstance>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpAgainInstance scp = new SnpAgainInstance();
			// 将map信息读入实体类
			scp = (SnpAgainInstance) snpAgainDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			snpAgainDao.saveOrUpdate(scp);
			//获取相关图片信息
			List<FileInfo> picList=comSearchDao.findPicture(scp.getTaskResultId(), "snpAnalysisItem");
			if(picList.size()>0){
				//根据图片数量查询对应页数的模板
				List<ReportTemplateInfo> rlist=comSearchDao.
						getReportTemplateInfo(scp.getProductId(),String.valueOf(picList.size()));
				scp.setReportInfo(rlist.get(0));
				scp.setTemplate(rlist.get(0).getAttach());
			}else{
				scp.setReportInfo(null);
				scp.setTemplate(null);
			}
			if (scp != null) {
				if ( scp.getSubmit() != null
					 //&&scp.getResult() != null
					 //&& !scp.getResult().equals("")
					 && !scp.getSubmit().equals("")) {
					 if (scp.getSubmit().equals("1")) {
					    //if (scp.getResult().equals("1")) {
					    	//提交合格的到1审
					    	SnpFirstInstance sf=new SnpFirstInstance();
					    	sf.setCode(scp.getCode());
					    	sf.setSampleCode(scp.getSampleCode());
					    	sf.setChipNum(scp.getChipNum());
					    	sf.setJg(scp.getJg());
					    	sf.setJgjs(scp.getJgjs());
					    	sf.setLcjy(scp.getLcjy());
					    	sf.setYcbg(scp.getYcbg());
					    	sf.setProductId(scp.getProductId());
					    	sf.setProductName(scp.getProductName());
					    	sf.setSampleType(scp.getSampleType());
					    	if(scp.getTemplate()!=null){
					    		sf.setTemplate(scp.getTemplate());
					    	}
					    	if(scp.getReportInfo()!=null){
					    		sf.setReportInfo(scp.getReportInfo());
					    	}
					    	sf.setReportInfoId(scp.getReportInfoId());
					    	sf.setReportInfoName(scp.getReportInfoName());
					    	sf.setSampleType(scp.getSampleType());
					    	sf.setTaskId(scp.getTaskId());
					    	sf.setTaskResultId(scp.getTaskResultId());
					    	sf.setTaskType(scp.getTaskType());
					    	sf.setState("1");
					    	sf.setFormerDate(new Date());
					    	commonDAO.saveOrUpdate(sf);
					    	
					    	//完成后改变状态
							scp.setState("2");
					    //}
							sampleStateService
									.saveSampleState1(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											format.format(new Date()),
											format.format(new Date()),
											"SnpAgainInstance",
											"SNP数据复核",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											null, "SNP数据审核", "1",
											null, null, null, null, null, null, null, null,scp.getJg(),scp.getJgjs(),scp.getLcjy(),scp.getYcbg());
					 }
				 }
			}
		}
		snpAgainDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpAgainInstance(String[] ids) throws Exception {
		for (String id : ids) {
			SnpAgainInstance scp = snpAgainDao.get(SnpAgainInstance.class, id);
			snpAgainDao.delete(scp);
		}
	}
	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFilePDF(String[] ids) throws Exception {
		// 临时地址
		String tempFile = ConfigFileUtil
				.getValueByKey("file.report.temp.path");
		// 正式地址
		String formFile = ConfigFileUtil
				.getValueByKey("file.report.form.path");
		//格式化日期到时分秒
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//格式化日期到日
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		for (String id : ids) {
			SnpAgainInstance sri = commonDAO.get(SnpAgainInstance.class, id);
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
					.getSampleCode());
			/*SampleOrder so = new SampleOrder();
			if (sf != null) {
				if (sf.getSampleOrder() != null) {
					so = sf.getSampleOrder();
				}
			}*/
			SampleOrder so=commonDAO.get(SampleOrder.class, sri
					.getSampleCode());
			//获取相关图片
			List<FileInfo> picList=comSearchDao.findPictureOrderByNum(sri.getTaskResultId(), "snpAnalysisItem");
			//报告模板
			ReportTemplateInfo reportInfo=sri.getReportInfo();
			//PDF文件
			FileInfo fileInfo = sri.getTemplate();
			if (fileInfo != null) {
				FileInputStream in = new FileInputStream(
						fileInfo.getFilePath());
				PdfReader reader = new PdfReader(in);
				String root = ConfigFileUtil.getRootPath() + File.separator
						+ DateUtil.format(new Date(), "yyyyMMdd");
				if (!new File(root).exists()) {
					new File(root).mkdirs();
				}
				File deskFile = new File(tempFile, "PDF" + sri.getCode()
						+ ".pdf");
				PdfStamper ps = new PdfStamper(reader,
						new FileOutputStream(deskFile)); // 生成的输出流
				
				AcroFields s = ps.getAcroFields();//生成新的PDF文件
				int count=picList.size();//获取图片数量
				if(count>0){
					String jg=sri.getJg();
					jg=jg.replace("\r","");
					jg=jg.replace("\n","");
					int num0=jg.getBytes("UTF-8").length;	//字节长度
					String jgjs=sri.getJgjs();	//结果解释
					jgjs=jgjs.replace("\r","");
					jgjs=jgjs.replace("\n","");
					int num1=jgjs.getBytes("UTF-8").length;	//字节长度
					int n0=0;
					int n1=0;
					String jg1="";
					String jg2="";
					String jg3="";
					
					String jgjs1="";
					String jgjs2="";
					String jgjs3="";
					//结果
					if(num0>0){
						if(num0<=88){
							jg1=jg;
						}else{
					        for (int i=1; i <=(num0/88); i++) {
				        		jg2=jg.replace(jg3, "");
				        		n0=jg2.getBytes("UTF-8").length;
				        		if(n0>=88){
				        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
				        			jg3+=comSearchService.bSubstring(jg2, 88);
				        		}else if(n0<90 && n0>0){
				        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
				        			jg3+=comSearchService.bSubstring(jg2, 88);
				        		}
					        }
						}
					}
					//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
					System.out.println(jg1+jg.replace(jg3, ""));
					if(!jg.replace(jg3, "").equals("")){
						if(sri.getJg()!=null){
							s.setField("result", jg1+jg.replace(jg3, ""));
						}else{
							s.setField("result", "");
						}
					}else{
						if(sri.getJg()!=null){
							s.setField("result", jg1);
						}else{
							s.setField("result", "");
						}
					}
					
					//结果解释
					if(num1>0){
						if(num1<=84){
							jgjs1=jgjs;
						}else{
					        for (int i=0; i <=(num1/84); i++) {
				        		jgjs2=jgjs.replace(jgjs3, "");
				        		n1=jgjs2.getBytes("UTF-8").length;
				        		if(n1>=84){
				        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
				        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
				        		}else if(n1<84 && n1>0){
				        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
				        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
				        		}
					        }
						}
					}
					System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
					if(!jgjs.replace(jgjs3, "").equals("")){
						//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
						if(sri.getJgjs()!=null){
							s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
						}else{
							s.setField("resultDescription", "");
						}
					}else{
						if(sri.getJgjs()!=null){
							s.setField("resultDescription", jgjs1);
						}else{
							s.setField("resultDescription", "");
						}
					}
					if(sri.getLcjy()!=null){
						if(sri.getLcjy().equals("1")){
							s.setField("advice", "门诊随访");
						}else{
							s.setField("advice", "遗传门诊随访");
						}
					}else{
						s.setField("advice", "");
					}
					
					//临床症状描述
					if(so.getSubmitReasonName()!=null){
						s.setField("lczz", so.getSubmitReasonName());
					}else{
						s.setField("lczz", "");
					}
					s.setField("reportDate", format1.format(new Date()));
					SnpAnalysis kt=commonDAO.get(SnpAnalysis.class, sri.getTaskId());
					s.setField("checkUser", kt
							.getAcceptUser().getName());
					s.setField("confirmUser", kt
							.getCreateUser().getName());
					if(count==1){
						insertData(so,sri,s);
						insertImage(ps,picList.get(0).getFilePath(),s,"Text1");
					}else{
						for(int i=0;i<count;i++){
							insertDatas(so,sri,s,String.valueOf(i+1),String.valueOf(count));
							insertImage(ps,picList.get(i).getFilePath(),s,"Text"+String.valueOf(i+1));
							if(i>0){
								String picName=picList.get(i).getFileName();
								int j=picName.indexOf("-");
								String str=picName.substring(j+1,picName.length()-6);
								s.setField("picName"+String.valueOf(i), str);
							}
						}
					}
				}
				ps.setFormFlattening(true);// 这句不能少
				ps.close();
				reader.close();
				in.close();
				commonDAO.update(sri);
			}
		}
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertData(SampleOrder so,SnpAgainInstance sri,
			AcroFields s) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String names=so.getName();
		if(so.getName()!=null){
			s.setField("names", names);
		}else{
			s.setField("names", "");
		}
		System.out.println("姓名"+names);
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders", m);
			}else{
				s.setField("genders", w);
			}
		}else{
			s.setField("genders", "");
		}
		if (so.getAge() != null) {
			s.setField("age", so.getAge());
		} else {
			s.setField("age", "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay", format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay", "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode", sri.getSampleCode());
		}else{
			s.setField("slideCode", "");
		}
		s.setField("medicalNum", so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes", t);
		}else{
			s.setField("sampleTypes", "");
		}
	
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer", so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer", "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate",
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate", "");
		}
		commonDAO.update(sri);
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertDatas(SampleOrder so,SnpAgainInstance sri,
			AcroFields s,String i,String a) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(so.getName()!=null){
			String names=so.getName();
			s.setField("names"+i, names);
		}else{
			s.setField("names"+i, "");
		}
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders"+i, m);
			}else{
				s.setField("genders"+i, w);
			}
		}else{
			s.setField("genders"+i, "");
		}
		if (so.getAge() != null) {
			s.setField("age"+i, so.getAge());
		} else {
			s.setField("age"+i, "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay"+i, format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay"+i, "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode"+i, sri.getSampleCode());
		}else{
			s.setField("slideCode"+i, "");
		}
		s.setField("medicalNum"+i, so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes"+i, t);
		}else{
			s.setField("sampleTypes"+i, "");
		}
	
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer"+i, so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer"+i, "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate"+i,
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate"+i, "");
		}
		//s.setField("pageNum"+i, i+"/"+a);
		commonDAO.update(sri);
	}
	/**
	 * 向第PDF插入图片
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertImage(PdfStamper ps,String path,AcroFields s,String text) throws Exception{
		try { 
		    int pageNo = s.getFieldPositions(text).get(0).page;
		    Rectangle signRect = s.getFieldPositions(text).get(0).position;
	        float x = signRect.getLeft();
	        float y = signRect.getBottom();

	        // 读图片
	        Image image = Image.getInstance(path);
	        // 获取操作的页面
	        PdfContentByte under = ps.getOverContent(pageNo);
	        // 根据域的大小缩放图片
	        image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	        System.out.println(x);  
			System.out.println(y);  
	        // 添加图片
	        image.setAbsolutePosition(x, y);
	        under.addImage(image);

		}catch (Exception e){  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  
	}

/**
 * 提交样本
 * @param id
 * @param ids
 * @throws Exception
 */
@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void submitSample( String[] ids) throws Exception {
	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// 获取结果表样本信息
	List<SnpAgainInstance> list = this.snpAgainDao.setAgainByIds(ids);
	for (SnpAgainInstance scp : list) {
		if (scp != null) {
//
//		if ( scp.getSubmit() != null
//			 //&&scp.getResult() != null
//			 //&& !scp.getResult().equals("")
//			 && !scp.getSubmit().equals("")) {
//			 if (scp.getSubmit().equals("1")) {
			    //if (scp.getResult().equals("1")) {
			    	//提交合格的到1审
			SampleReportTemp st = new SampleReportTemp();
			
			st.setCode(scp.getCode());
			st.setSampleCode(scp.getSampleCode());
			st.setProductId(scp.getProductId());
			st.setProductName(scp.getProductName());
			st.setState("1");
			st.setNote(scp.getNote());
			String orderNum="";
			List<SampleInfo> ab = commonService.get(SampleInfo.class, "code",
					scp.getSampleCode());
			if (ab.size() > 0){
				orderNum=ab.get(0).getOrderNum();
			}else{
				orderNum="";
			}
			st.setOrderNum(orderNum);
			st.setSampleCode(scp.getSampleCode());
			commonDAO.saveOrUpdate(st);
//			    	SnpFirstInstance sf=new SnpFirstInstance();
//			    	sf.setCode(scp.getCode());
//			    	sf.setSampleCode(scp.getSampleCode());
//			    	sf.setChipNum(scp.getChipNum());
//			    	sf.setJg(scp.getJg());
//			    	sf.setJgjs(scp.getJgjs());
//			    	sf.setLcjy(scp.getLcjy());
//			    	sf.setYcbg(scp.getYcbg());
//			    	sf.setProductId(scp.getProductId());
//			    	sf.setProductName(scp.getProductName());
//			    	sf.setSampleType(scp.getSampleType());
//			    	if(scp.getTemplate()!=null){
//			    		sf.setTemplate(scp.getTemplate());
//			    	}
//			    	if(scp.getReportInfo()!=null){
//			    		sf.setReportInfo(scp.getReportInfo());
//			    	}
//			    	sf.setReportInfoId(scp.getReportInfoId());
//			    	sf.setReportInfoName(scp.getReportInfoName());
//			    	sf.setSampleType(scp.getSampleType());
//			    	sf.setTaskId(scp.getTaskId());
//			    	sf.setTaskResultId(scp.getTaskResultId());
//			    	sf.setTaskType(scp.getTaskType());
//			    	sf.setState("1");
//			    	sf.setFormerDate(new Date());
//			    	commonDAO.saveOrUpdate(sf);
			    	
			    	//完成后改变状态
					scp.setState("2");
			    //}
//					sampleStateService
//					.saveSampleState1(
//					scp.getCode(),
//					scp.getSampleCode(),
//					scp.getProductId(),
//					scp.getProductName(),
//					"",
//					format.format(new Date()),
//					format.format(new Date()),
//					"SnpAgainInstance",
//					"SNP数据复核",
//					(User) ServletActionContext
//							.getRequest()
//							.getSession()
//							.getAttribute(
//									SystemConstants.USER_SESSION_KEY),
//					null, "SNP数据审核", "1",
//					null, null, null, null, null, null, null, null,scp.getJg(),scp.getJgjs(),scp.getLcjy(),scp.getYcbg());
					sampleStateService
					.saveSampleState1(
					scp.getCode(),
					scp.getSampleCode(),
					scp.getProductId(),
					scp.getProductName(),
					"",
					format.format(new Date()),
					format.format(new Date()),
					"SnpAgainInstance",
					"SNP数据复核",
					(User) ServletActionContext
							.getRequest()
							.getSession()
							.getAttribute(
									SystemConstants.USER_SESSION_KEY),
					null, "上传报告", "1",
					null, null, null, null, null, null, null, null,scp.getJg(),scp.getJgjs(),scp.getLcjy(),scp.getYcbg());
		}scp.setSubmit("1");
		 }
	}	
	
}
