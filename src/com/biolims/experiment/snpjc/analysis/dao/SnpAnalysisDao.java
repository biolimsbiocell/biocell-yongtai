package com.biolims.experiment.snpjc.analysis.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.fx.model.FishAgainInstance;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysis;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisTemp;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;

@Repository
@SuppressWarnings("unchecked")
public class SnpAnalysisDao extends BaseHibernateDao {
public Map<String, Object> selectSnpAnalysisList(
		Map<String, String> mapForQuery, Integer startNum,
		Integer limitNum, String dir, String sort) {
	String key = " ";
	String hql = " from SnpAnalysis where 1=1 ";
	if (mapForQuery != null)
		key = map2where(mapForQuery);
	Long total = (Long) this.getSession()
			.createQuery(" select count(*) " + hql + key).uniqueResult();
	List<SnpAnalysis> list = new ArrayList<SnpAnalysis>();
	if (total > 0) {
		if (dir != null && dir.length() > 0 && sort != null
				&& sort.length() > 0) {
			if (sort.indexOf("-") != -1) {
				sort = sort.substring(0, sort.indexOf("-"));
			}
			key = key + " order by " + sort + " " + dir;
		}
		if (startNum == null || limitNum == null) {
			list = this.getSession().createQuery(hql + key).list();
		} else {
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(startNum).setMaxResults(limitNum)
					.list();
		}
	}
	Map<String, Object> result = new HashMap<String, Object>();
	result.put("total", total);
	result.put("list", list);
	return result;

}

public Map<String, Object> selectSnpAnalysisItemList(String scId,
		Integer startNum, Integer limitNum, String dir, String sort)
		throws Exception {
	String hql = "from SnpAnalysisItem where 1=1 ";
	String key = "";
	if (scId != null)
		key = key + " and snpAnalysis.id='" + scId + "' and submit is null";
	Long total = (Long) this.getSession()
			.createQuery("select count(*) " + hql + key).uniqueResult();
	List<SnpAnalysisItem> list = new ArrayList<SnpAnalysisItem>();
	if (total > 0) {
		if (dir != null && dir.length() > 0 && sort != null
				&& sort.length() > 0) {
			if (sort.indexOf("-") != -1) {
				sort = sort.substring(0, sort.indexOf("-"));
			}
			key = key + " order by " + sort + " " + dir;
		}
		if (startNum == null || limitNum == null) {
			list = this.getSession().createQuery(hql + key).list();
		} else {
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(startNum).setMaxResults(limitNum)
					.list();
		}
	}
	Map<String, Object> result = new HashMap<String, Object>();
	result.put("total", total);
	result.put("list", list);
	return result;
}

public Map<String, Object> selectSnpAnalysisTempList(
		Map<String, String> mapForQuery, Integer startNum,
		Integer limitNum, String dir, String sort)
		throws Exception {
	String hql = "from SnpAnalysisTemp where 1=1 and state='1'";
	String key = "";
	Long total = (Long) this.getSession()
			.createQuery("select count(*) " + hql + key).uniqueResult();
	List<SnpAnalysisTemp> list = new ArrayList<SnpAnalysisTemp>();
	if (total > 0) {
		if (dir != null && dir.length() > 0 && sort != null
				&& sort.length() > 0) {
			if (sort.indexOf("-") != -1) {
				sort = sort.substring(0, sort.indexOf("-"));
			}
			key = key + " order by " + sort + " " + dir;
		}
		if (startNum == null || limitNum == null) {
			list = this.getSession().createQuery(hql + key).list();
		} else {
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(startNum).setMaxResults(limitNum)
					.list();
		}
	}
	Map<String, Object> result = new HashMap<String, Object>();
	result.put("total", total);
	result.put("list", list);
	return result;
}

public List<SnpAnalysisItem> getItemList(String id) {
	String hql = "from SnpAnalysisItem where 1=1 and snpAnalysis.id='" + id
			+ "'";
	List<SnpAnalysisItem> list = this.getSession().createQuery(hql).list();
	return list;
}
/**
 * 根据主表ID查询结果表
 */
public List<SnpAnalysisItem> getSnpAnalysisItemList(String id){
	String hql = " from SnpAnalysisItem where 1=1  and snpAnalysis='"+id+"'";
	List<SnpAnalysisItem> list=this.getSession().createQuery(hql).list();
	return list;
}

/**
 * 根据样本编号查询
 * @param code
 * @return
 */
public List<SnpAnalysisItem> setAnalysisById(String code) {
	String hql = "from SnpAnalysisItem t where (isCommit is null or isCommit='') and snpAnalysis.id='"
			+ code + "'";
	List<SnpAnalysisItem> list = this.getSession().createQuery(hql).list();
	return list;
}
/**
 * 根据样本编号查询
 * @param codes
 * @return
 */
public List<SnpAnalysisItem> setAnalysisByIds(String[] codes) {
	String insql = "''";
	for (String code : codes) {
		insql += ",'" + code + "'";
	}
	String hql = "from SnpAnalysisItem t where (submit is null or submit='') and id in ("
			+ insql + ")";
	List<SnpAnalysisItem> list = this.getSession().createQuery(hql).list();
	return list;
   }

}