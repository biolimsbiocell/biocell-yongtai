package com.biolims.experiment.snpjc.analysis.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysis;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisTemp;
import com.biolims.experiment.snpjc.analysis.service.SnpAnalysisService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/snpjc/analysis/snpAnalysis")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SnpAnalysisAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249105";
	@Autowired
	private SnpAnalysisService snpAnalysisService;
	private SnpAnalysis snpAnalysis = new SnpAnalysis();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showSnpAnalysisList")
	public String showSnpAnalysisList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysis.jsp");
	}
	
	@Action(value = "showSnpAnalysisListJson")
	public void showSnpAnalysisListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpAnalysisService.findSnpAnalysisList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpAnalysis> list = (List<SnpAnalysis>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("fxDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("snpSj-id", "");
		map.put("snpSj-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "snpAnalysisSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSnpAnalysisList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysisDialog.jsp");
	}

	@Action(value = "showDialogSnpAnalysisListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSnpAnalysisListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpAnalysisService.findSnpAnalysisList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpAnalysis> list = (List<SnpAnalysis>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("fxDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("snpSj-id", "");
		map.put("snpSj-name", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSnpAnalysis")
	public String editSnpAnalysis() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			snpAnalysis = snpAnalysisService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "snpAnalysis");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			snpAnalysis.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			snpAnalysis.setCreateUser(user);
			snpAnalysis.setCreateDate(new Date());
			snpAnalysis.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			snpAnalysis.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			UserGroup u=comSearchDao.get(UserGroup.class,"SNP001");
			//if(snpAnalysis.getAcceptUser()!=null){
				snpAnalysis.setAcceptUser(u);
			//}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(snpAnalysis.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysisEdit.jsp");
	}

	@Action(value = "copySnpAnalysis")
	public String copySnpAnalysis() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		snpAnalysis = snpAnalysisService.get(id);
		snpAnalysis.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysisEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = snpAnalysis.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SnpAnalysis";
			String markCode = "SNPFX";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			snpAnalysis.setId(autoID);
			snpAnalysis.setName("SNP分析-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("snpAnalysisItem",
				getParameterFromRequest("snpAnalysisItemJson"));

		snpAnalysisService.save(snpAnalysis, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/snpjc/analysis/snpAnalysis/editSnpAnalysis.action?id="
				+ snpAnalysis.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/snpjc/analysis/snpAnalysis/editSnpAnalysis.action?id="
		// + snpAnalysis.getId());

	}

	@Action(value = "viewSnpAnalysis")
	public String toViewSnpAnalysis() throws Exception {
		String id = getParameterFromRequest("id");
		snpAnalysis = snpAnalysisService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysisEdit.jsp");
	}

	@Action(value = "showSnpAnalysisItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpAnalysisItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysisItem.jsp");
	}

	@Action(value = "showSnpAnalysisItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpAnalysisItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = snpAnalysisService
					.findSnpAnalysisItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpAnalysisItem> list = (List<SnpAnalysisItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("chipNum", "");
			map.put("lcjy", "");
			map.put("ycbg", "");
			map.put("jg", "");
			map.put("jgjs", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("state", "");
			map.put("snpAnalysis-name", "");
			map.put("snpAnalysis-id", "");
			map.put("note", "");
			map.put("template-id", "");
			map.put("template-fileName", "");
			map.put("reportInfo-id", "");
			map.put("reportInfo-name", "");
			map.put("reportInfoId", "");
			map.put("reportInfoName", "");
			map.put("fileNum", "");
			map.put("upTime", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpAnalysisItem")
	public void delSnpAnalysisItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpAnalysisService.delSnpAnalysisItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpAnalysisItemOne")
	public void delSnpAnalysisItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			snpAnalysisService.delSnpAnalysisItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSnpAnalysisTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSnpAnalysisTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/snpjc/analysis/snpAnalysisTemp.jsp");
	}

	@Action(value = "showSnpAnalysisTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpAnalysisTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpAnalysisService
				.findSnpAnalysisTempList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<SnpAnalysisTemp> list = (List<SnpAnalysisTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("state", "");
		map.put("note", "");

		map.put("chipNum", "");
		
		map.put("chargeNote", "");//缴费状态
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpAnalysisTemp")
	public void delSnpAnalysisTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpAnalysisService.delSnpAnalysisTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SnpAnalysisService getSnpAnalysisService() {
		return snpAnalysisService;
	}

	public void setSnpAnalysisService(SnpAnalysisService snpAnalysisService) {
		this.snpAnalysisService = snpAnalysisService;
	}

	public SnpAnalysis getSnpAnalysis() {
		return snpAnalysis;
	}

	public void setSnpAnalysis(SnpAnalysis snpAnalysis) {
		this.snpAnalysis = snpAnalysis;
	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpAnalysisService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 保存SNP分析结果结果
	 */
	/*@Action(value = "saveSnpAnalysisResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpAnalysisResult() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			String id = getRequest().getParameter("id");
			SnpAnalysis sc=commonDAO.get(SnpAnalysis.class, id);
			snpAnalysisService.saveSnpAnalysisItem(sc, itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}*/
	
	 /**
	 * 保存SNP分析明细
	 * @throws Exception
	 */
	@Action(value = "saveSnpAnalysisItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpAnalysisItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SnpAnalysis sc=commonDAO.get(SnpAnalysis.class, id);
			Map aMap = new HashMap();
			aMap.put("snpAnalysisItem",
					itemDataJson);
			if(sc!=null){
				snpAnalysisService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	
/**
 * 提交样本
 * @throws Exception
 */
@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
public void submitSample() throws Exception {
	String id = getParameterFromRequest("id");
	String[] ids = getRequest().getParameterValues("ids[]");
	Map<String, Object> result = new HashMap<String, Object>();
	try {
		this.snpAnalysisService.submitSample(id,ids);

		result.put("success", true);

	} catch (Exception e) {
		result.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(result));
   }	

}
