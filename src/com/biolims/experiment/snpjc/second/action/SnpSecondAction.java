package com.biolims.experiment.snpjc.second.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.snpjc.second.model.SnpSecondInstance;
import com.biolims.experiment.snpjc.second.service.SnpSecondService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/snpjc/second/snpSecond")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SnpSecondAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "241807";
	@Autowired
	private SnpSecondService snpSecondService;
	private SnpSecondInstance snpSecondInstance = new SnpSecondInstance();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showSnpSecondInstanceList")
	public String showSnpSecondInstanceList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snpjc/second/snpSecondInstance.jsp");
	}

	@Action(value = "showSnpSecondInstanceListJson")
	public void showSnpSecondInstanceListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpSecondService.findSnpSecondInstanceList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpSecondInstance> list = (List<SnpSecondInstance>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("chipNum", "");
		map.put("lcjy", "");
		map.put("ycbg", "");
		map.put("jg", "");
		map.put("jgjs", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("state", "");
		map.put("note", "");
		map.put("template-id", "");
		map.put("template-fileName", "");
		map.put("reportInfo-id", "");
		map.put("reportInfo-name", "");
		map.put("reportInfoId", "");
		map.put("reportInfoName", "");
		map.put("fileNum", "");
		map.put("taskType", "");
		map.put("taskResultId", "");
		map.put("taskId", "");
		map.put("upTime", "");
		map.put("formerDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

/**
 * 提交样本
 * @throws Exception
 */
@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
public void submitSample() throws Exception {
	String[] ids = getRequest().getParameterValues("ids[]");
	Map<String, Object> result = new HashMap<String, Object>();
	try {
		this.snpSecondService.submitSample(ids);

		result.put("success", true);

	} catch (Exception e) {
		result.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(result));
}	
	
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSnpSecondInstance")
	public void delSnpSecondInstance() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSecondService.delSnpSecondInstance(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SnpSecondService getSnpSecondService() {
		return snpSecondService;
	}

	public void setSnpSecondService(SnpSecondService snpSecondService) {
		this.snpSecondService = snpSecondService;
	}

	public SnpSecondInstance getSnpSecondInstance() {
		return snpSecondInstance;
	}

	public void setSnpSecondInstance(SnpSecondInstance snpSecondInstance) {
		this.snpSecondInstance = snpSecondInstance;
	}
	/**
	 * 保存二审结果
	 */
	@Action(value = "saveSnpSecondInstance",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSnpSecondInstance() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			snpSecondService.saveSnpSecondInstance(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			snpSecondService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
