package com.biolims.experiment.productconfirm.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.productconfirm.service.ProductConfirmService;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlan;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanTemp;
import com.biolims.experiment.reinfusionPlan.service.ReinfusionPlanService;
import com.biolims.file.service.FileInfoService;
//import com.biolims.report.newreport.model.SampleReportItemNew;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.code.SystemConstants;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 生产确认
 * 
 * @author admin
 * 
 */
@Namespace("/experiment/productConfirm/productConfirm")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ProductConfirmAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "210000200101";
	@Autowired
	private ProductConfirmService productConfirmService;
	private SampleTransportItem sampleTransportItem = new SampleTransportItem();
	@Resource
	private CommonService commonService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;

//	/**
//	 * 菜单栏链接
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "showProductConfirmList")
//	public String showProductConfirmList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/experiment/productConfirm/productConfirmTable.jsp");
//	}
//
//	@Action(value = "showProductConfirmListJson")
//	public void showProductConfirmListJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			Map<String, Object> result = productConfirmService.findReinfusionPlanList(start, length, query, col, sort);
//			List<ReinfusionPlan> list = (List<ReinfusionPlan>) result.get("list");
//
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("name", "");
//			map.put("createUser-name", "");
//			map.put("createDate", "yyyy-MM-dd");
//			map.put("acceptUser-name", "");
//			map.put("acceptDate", "yyyy-MM-dd");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("reinfusionPlanDate", "yyyy-MM-dd");
//			map.put("scopeId", "");
//			map.put("scopeName", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 添加、修改回输计划页面
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "toEditReinfusionPlan")
//	public String toEditReinfusionPlan() throws Exception {
//		String id = super.getRequest().getParameter("id");
//		String handlemethod = "";
//		if (id != null) {
//			this.reinfusionPlan = this.reinfusionPlanService.findReinfusionPlanById(id);
//			toState(reinfusionPlan.getState());
//			if (reinfusionPlan.getState().equals(SystemConstants.DIC_STATE_YES))
//				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
//			else
//				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
//			toToolBar(rightsId, "", "", handlemethod);
//
//		} else {
//			this.reinfusionPlan = new ReinfusionPlan();
//			this.reinfusionPlan.setId(SystemCode.DEFAULT_SYSTEMCODE);
//			User createUser = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
//			reinfusionPlan.setCreateUser(createUser);
//			reinfusionPlan.setCreateDate(new Date());
//			reinfusionPlan.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//			reinfusionPlan.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toState(reinfusionPlan.getState());
//		}
//		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/reinfusionPlanEdit.jsp");
//	}
//
//	// 查看
//	@Action(value = "toViewReinfusionPlan")
//	public String toViewReinfusionPlan() throws Exception {
//		String id = getParameterFromRequest("id");
//		reinfusionPlan = reinfusionPlanService.get(id);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/reinfusionPlanEdit.jsp");
//	}
//
//	/**
//	 * 
//	 * @Title: @Description: 查询回输计划待选列表 @author qi.yan @date
//	 * 2018-4-10下午3:55:04 @throws Exception void @throws
//	 */
//	@Action(value = "showReinfusionPlanTempTableJson")
//	public void showReinfusionPlanTempTableJson() throws Exception {
//		// 显示的数据类型
//		String query = getRequest().getParameter("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			Map<String, Object> result = reinfusionPlanService.selReinfusionPlanTempTable(start, length, query, col,
//					sort);
//			List<ReinfusionPlanTemp> list = (List<ReinfusionPlanTemp>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("orderCode", "");
//			map.put("code", "");
//			map.put("state", "");
//			map.put("name", "");
//			map.put("age", "");
//			map.put("gender", "");
//			map.put("nation", "");
//			map.put("hospital", "");
//			map.put("hospitalPhone", "");
//			map.put("note", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: @Description: 回输计划明细 @author qi.yan @date 2018-4-10下午4:15:11 @throws
//	 * Exception void @throws
//	 */
//	@Action(value = "showReinfusionPlanItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showReinfusionPlanItemTableJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = reinfusionPlanService.selReinfusionPlanItemTable(scId, start, length, query,
//					col, sort);
//			List<ReinfusionPlanItem> list = (List<ReinfusionPlanItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("state", "");
//			map.put("orderCode", "");
//			map.put("tempId", "");
//			map.put("reinfusionPlan-id", "");
//			map.put("name", "");
//			map.put("age", "");
//			map.put("gender", "");
//			map.put("nation", "");
//			map.put("hospital", "");
//			map.put("hospitalPhone", "");
//			map.put("feedBack", "");
//			map.put("delay", "");
//			map.put("result", "");
//			map.put("note", "");
//			map.put("scopeId", "");
//			map.put("scopeName", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * 
//	 * @Title: saveSampleOutApplyAndItem @Description: 大保存 @author qi.yan @date
//	 * 2018-4-11下午3:01:56 @throws Exception void @throws
//	 */
//	@Action(value = "savereinfusionPlanAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void savereinfusionPlanAndItem() throws Exception {
//		String dataValue = getParameterFromRequest("dataValue");
//		// 主表changeLog
//		String changeLog = getParameterFromRequest("changeLog");
//		// 子表changeLogItem
//		String changeLogItem = getParameterFromRequest("changeLogItem");
//
//		String id = getParameterFromRequest("id");
//		String note = getParameterFromRequest("note");
//		String reinfusionPlanDate = getParameterFromRequest("reinfusionPlanDate");
//
//		String str = "[" + dataValue + "]";
//
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//
//			for (Map<String, Object> map1 : list) {
//				ReinfusionPlan soa = new ReinfusionPlan();
//				Map aMap = new HashMap();
//				soa = (ReinfusionPlan) commonDAO.Map2Bean(map1, soa);
//				// 保存主表信息
//				ReinfusionPlan newSoa = reinfusionPlanService.saveSampleOutApplyById(soa, id, reinfusionPlanDate, note,
//						changeLog);
//
//				newSoa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
//				newSoa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
//
//				// 保存子表信息
//				aMap.put("ImteJson", getParameterFromRequest("ImteJson"));
//				reinfusionPlanService.saveReinfusionPlanAndItem(newSoa, aMap, changeLog, changeLogItem);
//				map.put("id", newSoa.getId());
//			}
//			map.put("success", true);
//
//		} catch (Exception e) {
//			map.put("success", false);
//			map.put("msg", e.getMessage());
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 
//	 * @Title: addSampleOutApplyItem @Description: 添加库存样本到出库申请明细 @author
//	 * qi.yan @date 2018-4-11下午3:01:25 @throws Exception void @throws
//	 */
//	@Action(value = "addReinfusionPlanItem")
//	public void addReinfusionPlanItem() throws Exception {
//		String note = getParameterFromRequest("note");
//		String reinfusionPlanDate = getParameterFromRequest("reinfusionPlanDate");
//		String id = getParameterFromRequest("id");
//		String createDate = getParameterFromRequest("createDate");
//		String createUser = getParameterFromRequest("createUser");
//		String[] ids = getRequest().getParameterValues("ids[]");
//
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String sampleOutId = reinfusionPlanService.addReinfusionPlanItem(ids, note, reinfusionPlanDate, id,
//					createUser, createDate);
//			result.put("success", true);
//			result.put("data", sampleOutId);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 删除明细信息
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "delReinfusionPlanItem")
//	public void delReinfusionPlanItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			reinfusionPlanService.delReinfusionPlanItem(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	
//	
	/**
	 * 
	 * @Title: 
	 * @Description: 生产确认列表
	 * @author : 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showProductConfirmItemList")
	public String showProductConfirmItemList() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/productConfirm/showProductConfirmList.jsp");
	}

	@Action(value = "showProductConfirmItemListJson")
	public void showProductConfirmItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			Map<String, Object> result = this.productConfirmService.findProductConfirmList(
					start, length, query, col, sort, type);
			List<TransportOrderCell> list = (List<TransportOrderCell>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("pronoun", "");
			map.put("sampleType", "");
			map.put("sampleOrder-crmCustomer-name", "");
			map.put("transportDate", "yyyy-MM-dd HH:mm");
			map.put("numTubes", "");
			map.put("transportTemperature", "");
			map.put("state", "");
			
			map.put("harvestDate", "yyyy-MM-dd HH:mm");
			map.put("endHarvestDate", "yyyy-MM-dd HH:mm");
			map.put("cellDate", "yyyy-MM-dd HH:mm");
			
			map.put("sampleOrder-ccoi", "");
			map.put("templeOperator-id","");
			map.put("templeOperator-name","");
			map.put("operatingRoomName","");
			map.put("operatingRoomId","");
			
			map.put("sampleOrder-barcode", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @Title: 生产确认保存  
	 * @Description: 保存  
	 * @author : 
	 * @date 
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "saveProductConfirmItemList")
	public void saveProductConfirmItemList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("dataJson");
			String changeLog = getRequest().getParameter("logInfo");

			TransportOrderCell sr = new TransportOrderCell();

			productConfirmService.saveTransportOrderCell(sr, dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value="findCprPossibleModify")
	public void findCprPossibleModify() throws Exception {
		Map<String, Object> map = new HashMap<String , Object>();
		String code = getParameterFromRequest("code");
		if(!"".equals(code)&&code!=null) {
			CellProductionRecord cellProductionRecord = productConfirmService.findCprPossibleModify(code);
			if(cellProductionRecord!=null) {
			    if("1".equals(cellProductionRecord.getPossibleModify())) {
					map.put("success", true);

			    }else {
					map.put("success", false);
			    }
			}else {
				map.put("success", false);
	
			}
		}else {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "findC")
	public void findC() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		Boolean p=true;
	    String[] codes=getRequest().getParameterValues("codes[]");
		for (int i = 0; i < codes.length; i++) {
		p= productConfirmService.findC(codes[i]);
		break;
		}
	    map.put("success", p);

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
//	
//	/**
//	 * 
//	 * @Title: 回输计划变更  
//	 * @Description: 保存  
//	 * @author : 
//	 * @date 
//	 * @throws Exception
//	 * void
//	 * @throws
//	 */
//	@Action(value = "saveReinfusionPlanItemChangPlan")
//	public void saveReinfusionPlanItemChangPlan() throws Exception {
//		Map<String, Boolean> map = new HashMap<String, Boolean>();
//		try {
//			String dataJson = getRequest().getParameter("dataJson");
//			String changeLog = getRequest().getParameter("changeLog");
//			reinfusionPlanService.saveReinfusionPlanItemChangePlan(dataJson,changeLog);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	
	/**
	 * 
	 * @Title: 生产确认  
	 * @Description: 生产确认  
	 * @author : 
	 * @date 
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "saveProductConfirmItemConfirmList")
	public void saveProductConfirmItemConfirmList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			productConfirmService.saveProductConfirmItemConfirm(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "cancelProductConfirmItemConfirmList")
	public void cancelProductConfirmItemConfirmList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			productConfirmService.saveProductConfirmItemConfirm(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ProductConfirmService getProductConfirmService() {
		return productConfirmService;
	}

	public void setProductConfirmService(ProductConfirmService productConfirmService) {
		this.productConfirmService = productConfirmService;
	}

	public SampleTransportItem getSampleTransportItem() {
		return sampleTransportItem;
	}

	public void setSampleTransportItem(SampleTransportItem sampleTransportItem) {
		this.sampleTransportItem = sampleTransportItem;
	}


}
