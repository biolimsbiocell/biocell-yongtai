package com.biolims.experiment.productconfirm.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.productconfirm.dao.ProductConfirmDao;
import com.biolims.experiment.production.dao.ProductionPlanDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.service.SampleStateService;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.JsonUtils;

import net.sf.json.JSONObject;

/**
 * 生产确认
 * 
 * @author admin
 * 
 */
@Service
@SuppressWarnings("unchecked")
@Transactional
public class ProductConfirmService {
	@Resource
	private ProductConfirmDao productConfirmDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ProductionPlanDao productionPlanDao;
	

	StringBuffer json = new StringBuffer();

//	public Map<String, Object> findReinfusionPlanList(Integer start, Integer length, String query, String col,
//			String sort) throws Exception {
//		return reinfusionPlanDao.selectReinfusionPlanList(start, length, query, col, sort);
//	}
//
//	public ReinfusionPlan findReinfusionPlanById(String id) {
//		return reinfusionPlanDao.get(ReinfusionPlan.class, id);
//	}
//
//	public Map<String, Object> selReinfusionPlanTempTable(Integer start, Integer length, String query, String col,
//			String sort) throws Exception {
//		return reinfusionPlanDao.findReinfusionPlanTempTable(start, length, query, col, sort);
//	}
//
//	public Map<String, Object> selReinfusionPlanItemTable(String scId, Integer start, Integer length, String query,
//			String col, String sort) throws Exception {
//		return reinfusionPlanDao.selReinfusionPlanItemTable(scId, start, length, query, col, sort);
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public ReinfusionPlan saveSampleOutApplyById(ReinfusionPlan soa, String id, String reinfusionPlanDate, String note,
//			String logInfo) throws Exception {
//		ReinfusionPlan newSoa = commonDAO.get(ReinfusionPlan.class, id);
//		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
//			newSoa = new ReinfusionPlan();
//			String code = systemCodeService.getCodeByPrefix("ReinfusionPlan",
//					"RP" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
//			newSoa.setId(code);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			if (reinfusionPlanDate != null && !"".equals(reinfusionPlanDate)) {
//				newSoa.setReinfusionPlanDate(sdf.parse(reinfusionPlanDate));
//			}
//			User user = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			newSoa.setCreateUser(user);
//			newSoa.setCreateDate(new Date());
//			newSoa.setState("3");
//			newSoa.setStateName("新建");
//			newSoa.setName(note);
//			newSoa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
//			newSoa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
//			id = soa.getId();
//			commonDAO.saveOrUpdate(newSoa);
//		} else {
//			newSoa = commonDAO.get(ReinfusionPlan.class, id);
//		}
//
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			User u = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			li.setUserId(u.getId());
//			li.setFileId(newSoa.getId());
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//		newSoa.setName(note);
//		if (reinfusionPlanDate != null && !"".equals(reinfusionPlanDate)) {
//			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
//			Date da = sdf1.parse(reinfusionPlanDate);
//			newSoa.setReinfusionPlanDate(da);
//		}
//		commonDAO.saveOrUpdate(newSoa);
//		return newSoa;
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveReinfusionPlanAndItem(ReinfusionPlan newSoa, Map jsonMap, String logInfo, String logInfoItem)
//			throws Exception {
//		if (newSoa != null) {
//			if (logInfo != null && !"".equals(logInfo)) {
//				LogInfo li = new LogInfo();
//				li.setLogDate(new Date());
//				User u = (User) ServletActionContext.getRequest().getSession()
//						.getAttribute(SystemConstants.USER_SESSION_KEY);
//				li.setUserId(u.getId());
//				li.setFileId(newSoa.getId());
//				li.setModifyContent(logInfo);
//				commonDAO.saveOrUpdate(li);
//			}
//
//			String jsonStr = "";
//			jsonStr = (String) jsonMap.get("ImteJson");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveItem(newSoa, jsonStr, logInfoItem);
//			}
//		}
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	private void saveItem(ReinfusionPlan newSoa, String itemDataJson, String logInfo) throws Exception {
//		List<ReinfusionPlanItem> saveItems = new ArrayList<ReinfusionPlanItem>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			ReinfusionPlanItem scp = new ReinfusionPlanItem();
//			// 将map信息读入实体类
//			scp = (ReinfusionPlanItem) commonDAO.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			scp.setReinfusionPlan(newSoa);
//
//			saveItems.add(scp);
//		}
//		commonDAO.saveOrUpdateAll(saveItems);
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			User u = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			li.setUserId(u.getId());
//			li.setFileId(newSoa.getId());
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//	}
//
//	// 主数据内选择数据添加到入库明细表
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public String addReinfusionPlanItem(String[] ids, String note, String reinfusionPlanDate, String id,
//			String createUser, String createDate) throws Exception {
//
//		// 保存主表信息
//		ReinfusionPlan soa = new ReinfusionPlan();
//		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
//			soa = new ReinfusionPlan();
//			String code = systemCodeService.getCodeByPrefix("ReinfusionPlan",
//					"RP" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
//			soa.setId(code);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			if (reinfusionPlanDate != null && !"".equals(reinfusionPlanDate)) {
//				soa.setReinfusionPlanDate(sdf.parse(reinfusionPlanDate));
//			}
//			User u = commonDAO.get(User.class, createUser);
//			soa.setCreateUser(u);
//			soa.setCreateDate(sdf.parse(createDate));
//			soa.setState("3");
//			soa.setStateName("新建");
//			soa.setName(note);
//			soa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
//			soa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
//			id = soa.getId();
//			commonDAO.saveOrUpdate(soa);
//		} else {
//			soa = commonDAO.get(ReinfusionPlan.class, id);
//		}
//		for (int i = 0; i < ids.length; i++) {
//			String idz = ids[i];
//			// 通过id查询库存样本
//			ReinfusionPlanTemp sii = commonDAO.get(ReinfusionPlanTemp.class, idz);
//			ReinfusionPlanItem soai = new ReinfusionPlanItem();
//			// 将数据放到出库申请明细表
//			soai.setReinfusionPlan(soa);
//			soai.setCode(sii.getCode());
//			sii.setState("2");
//			soai.setTempId(sii.getId());
//			soai.setOrderCode(sii.getOrderCode());
//			soai.setName(sii.getName());
//			soai.setAge(sii.getAge());
//			soai.setGender(sii.getGender());
//			soai.setNation(sii.getNation());
//			soai.setHospital(sii.getHospital());
//			soai.setHospitalPhone(sii.getHospitalPhone());
//			soai.setNote(sii.getNote());
//			soai.setScopeId(sii.getScopeId());
//			soai.setScopeName(sii.getScopeName());
//			commonDAO.saveOrUpdate(soai);
//			commonDAO.saveOrUpdate(sii);
//		}
//		return id;
//
//	}
//
//	/**
//	 * 删除明细
//	 * 
//	 * @param ids
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delReinfusionPlanItem(String[] ids) throws Exception {
//		for (String id : ids) {
//			ReinfusionPlanItem scp = reinfusionPlanDao.get(ReinfusionPlanItem.class, id);
//			reinfusionPlanDao.delete(scp);
//			// 改变出库样本状态
//			ReinfusionPlanTemp out = commonDAO.get(ReinfusionPlanTemp.class, scp.getTempId());
//			out.setState("1");
//			commonDAO.saveOrUpdate(out);
//		}
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public ReinfusionPlan get(String id) {
//		ReinfusionPlan rp = new ReinfusionPlan();
//		rp = commonDAO.get(ReinfusionPlan.class, id);
//		return rp;
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void changeState(String applicationTypeActionId, String id) {
//		ReinfusionPlan rp = new ReinfusionPlan();
//		rp = commonDAO.get(ReinfusionPlan.class, id);
//		rp.setState("1");
//		rp.setStateName("完成");
//		commonDAO.saveOrUpdate(rp);
//		List<ReinfusionPlanItem> list = new ArrayList<ReinfusionPlanItem>();
//		list = reinfusionPlanDao.showReinfusionPlanItemList(id);
//		if (!list.isEmpty()) {
//			for (ReinfusionPlanItem rpi : list) {
//				SampleOrder so = new SampleOrder();
//				so = commonDAO.get(SampleOrder.class, rpi.getOrderCode());
//				TransportApplyTemp tat = new TransportApplyTemp();
//				if (so != null) {
//					tat.setSampleOrder(so);
//					tat.setProductId(so.getProductId());
//					tat.setProductName(so.getProductName());
//					tat.setPatientName(so.getName());
//					tat.setOrderCode(so.getId());
//				}
//				tat.setReinfusionPlanDate(rp.getReinfusionPlanDate());
//				tat.setCode(rpi.getCode());
//				tat.setState("1");
//				tat.setPlanState("1");
//				List<SampleInfo> siList = new ArrayList<SampleInfo>();
//				siList = commonDAO.getSampleInfoFromOrderId(rpi.getOrderCode());
//				if (!siList.isEmpty()) {
//					tat.setSampleInfo(siList.get(0));
//				}
//				commonDAO.saveOrUpdate(tat);
//			}
//		}
//	}

	public Map<String, Object> findProductConfirmList(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		return productConfirmDao.findProductConfirmList(start, length, query, col, sort, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderCell(TransportOrderCell sr, String dataJson, String changeLogItem) throws Exception {
		List<TransportOrderCell> saveItems = new ArrayList<TransportOrderCell>();
		List<Map> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		for (Map map : list) {
			TransportOrderCell sri = new TransportOrderCell();
			// 将map信息读入实体类
			sri = (TransportOrderCell) productConfirmDao.Map2Bean(map, sri);

			TransportOrderCell pti = commonDAO.get(TransportOrderCell.class, sri.getId());

			pti.setTransportDate(sri.getTransportDate());
			pti.setNumTubes(sri.getNumTubes());
			pti.setTransportTemperature(sri.getTransportTemperature());
			pti.setState(sri.getState());
			pti.setHarvestDate(sri.getHarvestDate());
			pti.setCellDate(sri.getCellDate());
			pti.setEndHarvestDate(sri.getEndHarvestDate());
			if (!"".equals(sri.getOperatingRoomId()) && sri.getOperatingRoomId() != null) {
				pti.setOperatingRoomId(sri.getOperatingRoomId());
			}
			if (!"".equals(sri.getOperatingRoomName()) && sri.getOperatingRoomName() != null) {
				pti.setOperatingRoomName(sri.getOperatingRoomName());
			}
			if (sri.getTempleOperator() != null) {
				pti.setTempleOperator(sri.getTempleOperator());
			}
			productConfirmDao.saveOrUpdate(pti);

			List<CellPassage> listOne = new ArrayList<CellPassage>();
//			List<CellProductionRecord> listTwo = new ArrayList<CellProductionRecord>();
			CellProductionRecord record = new CellProductionRecord();

			listOne = productConfirmDao.findCellPassageByCode(pti.getSampleOrder().getBarcode());

			if (!listOne.isEmpty()) {
				for (CellPassage cellPassage : listOne) {
					record = productConfirmDao.findCellProductionRecord(cellPassage.getId());
					if (!"".equals(pti.getOperatingRoomId()) && pti.getOperatingRoomId() != null) {
						record.setOperatingRoomId(pti.getOperatingRoomId());
						String roomId = pti.getOperatingRoomId();
						if(roomId!=null
								&&!"".equals(roomId)) {
							//查询该房间下仪器
							List<Instrument> its = productionPlanDao.getRoomInstruments(roomId);
							if(its.size()>0) {
								//循环该步骤是否配置仪器  存在仪器 将仪器放到该步骤
								List<CellProducOperation> cpos = productionPlanDao.getCellProducOperations(cellPassage.getId(), record.getOrderNum());
								if(cpos.size()>0) {
									for(CellProducOperation cpo:cpos) {
										List<CellproductCos> cpss = productionPlanDao.getCellproductCosinstruments(cpo.getId());
										if(cpss.size()>0) {
											for(CellproductCos cps:cpss) {
												for(Instrument it:its) {
													if(it.getMainType().getId().equals(cps.getTemplateCos().getTypeId())) {
														cps.setInstrumentCode(it.getId());
														cps.setInstrumentName(it.getName());
														cps.setInstrumentId(it.getMainType().getId());
														commonDAO.saveOrUpdate(cps);
														break;
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if (!"".equals(pti.getOperatingRoomName()) && pti.getOperatingRoomName() != null) {
						record.setOperatingRoomName(pti.getOperatingRoomName());
					}
					if (pti.getTempleOperator() != null) {
						record.setTempleOperator(pti.getTempleOperator());
					}
					if (record.getContent() != null&&!"".equals(record.getContent())) {
						JSONObject jsonCon = JSONObject.fromObject(record.getContent());
						if (!"".equals(pti.getOperatingRoomName()) && pti.getOperatingRoomName() != null) {
							jsonCon.put("operatingRoomName", pti.getOperatingRoomName());
						}
						if (pti.getTempleOperator() != null) {
							jsonCon.put("templeOperator", pti.getTempleOperator().getId());

						}
						if (!"".equals(pti.getOperatingRoomId()) && pti.getOperatingRoomId() != null) {
							jsonCon.put("operatingRoomId", pti.getOperatingRoomId());
						}
						record.setContent(jsonCon.toString());
					} else {
						JSONObject jsonCon = new JSONObject();
						if (!"".equals(pti.getOperatingRoomName()) && pti.getOperatingRoomName() != null) {
							jsonCon.put("operatingRoomName", pti.getOperatingRoomName());
						}
						if (pti.getTempleOperator() != null) {
							jsonCon.put("templeOperator", pti.getTempleOperator().getId());

						}
						if (!"".equals(pti.getOperatingRoomId()) && pti.getOperatingRoomId() != null) {
							jsonCon.put("operatingRoomId", pti.getOperatingRoomId());
						}
						record.setContent(jsonCon.toString());
					}
					commonDAO.saveOrUpdate(record);

				}
//			for (CellProductionRecord cellProductionRecord : listOne) {
//		       System.out.println("1");
//				
			}

			if (changeLogItem != null && !"".equals(changeLogItem)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("TransportOrderCell");
				li.setFileId(pti.getSampleOrder().getBarcode());
				li.setModifyContent(changeLogItem);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}

	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public CellProductionRecord findCprPossibleModify(String coed) throws Exception {
		List<CellPassage> listOne = new ArrayList<CellPassage>();
		CellProductionRecord record = new CellProductionRecord();

		listOne = productConfirmDao.findCellPassageByCode(coed);
		if (!listOne.isEmpty()) {
			for (CellPassage cellPassage : listOne) {
			record = productConfirmDao.findCellProductionRecord(cellPassage.getId());
			}
		}
		return record;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProductConfirmItemConfirm(String[] ids) throws Exception {
		if (ids.length > 0) {
			for (String id : ids) {
				if (id != null && !"".equals(id)) {
					TransportOrderCell sri = commonDAO.get(TransportOrderCell.class, id);
					if (sri != null) {
						if ("1".equals(sri.getState())) {
							sri.setState("2");
						} else {
							sri.setState("1");
							commonDAO.saveOrUpdate(sri);
							String hsbg = "生产确认：" + "ID为'" + id + "'，产品批号为" + sri.getCode() + "且CCOI为"
									+ sri.getSampleOrder().getCcoi() + "的数据已确认";
							if (hsbg != null && !"".equals(hsbg)) {
								LogInfo li = new LogInfo();
								li.setLogDate(new Date());
								User u = (User) ServletActionContext.getRequest().getSession()
										.getAttribute(SystemConstants.USER_SESSION_KEY);
								li.setUserId(u.getId());
								li.setFileId(sri.getCode());
								li.setClassName("TransportOrderCell");
								li.setModifyContent(hsbg);
								li.setState("3");
								li.setStateName("数据修改");
								commonDAO.saveOrUpdate(li);
							}
						}
					}

				}
			}
		} else {

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Boolean findC(String code) throws Exception {
		List<TransportOrderCell> list = new ArrayList<TransportOrderCell>();
		list = productConfirmDao.findC(code);
		Boolean p = true;
		if (list.size() == 1) {
			for (TransportOrderCell transportOrderCell : list) {
				if ("".equals(transportOrderCell.getHarvestDate()) || transportOrderCell.getHarvestDate() == null) {
					p = false;
					break;
				}
				if ("".equals(transportOrderCell.getCellDate()) || transportOrderCell.getCellDate() == null) {
					p = false;
					break;
				}
				if ("".equals(transportOrderCell.getEndHarvestDate())
						|| transportOrderCell.getEndHarvestDate() == null) {
					p = false;
					break;
				}
			}
		}
		return p;

	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveReinfusionPlanItemChangePlan(String dataJson,String changeLog) throws Exception {
//		if(dataJson!=null
//				&&!"".equals(dataJson)){
//			ReinfusionPlanItem rpi = commonDAO.get(ReinfusionPlanItem.class, dataJson);
//			if(rpi!=null){
//				ChangePlan cp = new ChangePlan();
//				String modelName = "ChangePlan";
//				String markCode = "CPP";
//				Date date = new Date();
//				DateFormat format = new SimpleDateFormat("yy");
//				String stime = format.format(date);
//				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
//
//				cp.setId(autoID);
//				User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
//				cp.setCreateUser(u);
//				cp.setCreateDate(new Date());
//				cp.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//				cp.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//				cp.setChangeTaskId(rpi.getId());
//				cp.setBatch(rpi.getBatch());
//				cp.setChangeContent(changeLog);
//				DicType dt = reinfusionPlanDao.getDicByMainTypeAndCode("changeType","ReinfusionPlanItem");
//				if(dt!=null){
//					cp.setChangeType(dt);
//				}
//				commonDAO.saveOrUpdate(cp);
//			}
//		}
//	}

}
