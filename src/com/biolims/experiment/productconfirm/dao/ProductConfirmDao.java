package com.biolims.experiment.productconfirm.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlan;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanTemp;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.opensymphony.xwork2.ActionContext;

/**
 * 生产确认
 * 
 * @author admin
 * 
 */
@Repository
@SuppressWarnings("unchecked")
public class ProductConfirmDao extends BaseHibernateDao {
//	public Map<String, Object> selectReinfusionPlanList(Integer start, Integer length, String query, String col,
//			String sort) throws Exception {
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		String countHql = "select count(*) from ReinfusionPlan where 1=1  ";
//		String key = "";
//		if (query != null) {
//			key = map2Where(query);
//		}
//		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
//		if (!"all".equals(scopeId)) {
//			key += " and scopeId='" + scopeId + "'";
//		}
//
//		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
//		if (0l != sumCount) {
//			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//			String hql = "from ReinfusionPlan where 1=1  ";
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key += " order by " + col + " " + sort;
//			}
//			List<ReinfusionPlan> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
//					.list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//		}
//		return map;
//	}
//
//	public Map<String, Object> findReinfusionPlanTempTable(Integer start, Integer length, String query, String col,
//			String sort) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String countHql = "select count(*) from ReinfusionPlanTemp where 1=1 and state='1'";
//		String key = "";
//		if (query != null) {
//			key = map2Where(query);
//		}
//		// 集团的查询条件
//		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
//		if (!"all".equals(scopeId)) {
//			key += " and scopeId='" + scopeId + "'";
//		}
//		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
//		if (0l != sumCount) {
//			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//			String hql = "from ReinfusionPlanTemp  where 1=1 and state='1'";
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				col = col.replace("-", ".");
//				key += " order by " + col + " " + sort;
//			}
//			List<ReinfusionPlanTemp> list = this.getSession().createQuery(hql + key).setFirstResult(start)
//					.setMaxResults(length).list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//		}
//		return map;
//	}
//
//	public Map<String, Object> selReinfusionPlanItemTable(String scId, Integer start, Integer length, String query,
//			String col, String sort) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String countHql = "select count(*) from ReinfusionPlanItem where 1=1 and reinfusionPlan.id='" + scId + "'";
//		String key = "";
//		if (query != null) {
//			key = map2Where(query);
//		}
//		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
//		if (0l != sumCount) {
//			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//			String hql = "from ReinfusionPlanItem  where 1=1  and reinfusionPlan.id='" + scId + "'";
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				col = col.replace("-", ".");
//				key += " order by " + col + " " + sort;
//			}
//			List<ReinfusionPlanItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
//					.setMaxResults(length).list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//		}
//		return map;
//	}
//
//	public List<ReinfusionPlanItem> showReinfusionPlanItemList(String id) {
//		List<ReinfusionPlanItem> list = new ArrayList<ReinfusionPlanItem>();
//		String hql = "from ReinfusionPlanItem where 1=1 and reinfusionPlan='" + id + "'";
//		list = getSession().createQuery(hql).list();
//		return list;
//	}

	public Map<String, Object> findProductConfirmList(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportOrderCell where 1=1 and submitData = '是' ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		} else {

		}

		if (type != null && !"".equals(type)) {
			if ("1".equals(type)) {
				key = key + " and state='1' ";
			} else if ("2".equals(type)) {
				key = key + " and state='2' ";
			}
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransportOrderCell where 1=1 and submitData = '是' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.substring(0, col.indexOf("-"));
				}
				if ("id".equals(col)) {
					key += " order by id desc ";
				} else {
					key += " order by " + col + " " + sort;
				}
			} else {
				key += " order by id desc ";
			}
			List<SampleTransportItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<TransportOrderCell> findC(String code) {
		List<TransportOrderCell> list = new ArrayList<TransportOrderCell>();
		String hql = "from TransportOrderCell where 1=1 and sampleOrder.barcode ='" + code + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassage> findCellPassageByCode(String code) {
		List<CellPassage> list = new ArrayList<CellPassage>();

		String hql = "from CellPassage where 1=1 and batch ='" + code + "'";
		list = getSession().createQuery(hql).list();

		return list;

	}

	public CellProductionRecord findCellProductionRecord(String code) {
//		List<CellProductionRecord> listOne = new ArrayList<CellProductionRecord>();
		CellProductionRecord record = new CellProductionRecord();

		String hqlOne = "from CellProductionRecord c where 1=1 and c.cellPassage ='" + code + "' and c.templeItem.harvest='1'";
		record = (CellProductionRecord) getSession().createQuery(hqlOne).uniqueResult();
//		if (!listOne.isEmpty()) {
//			for (CellProductionRecord cellProductionRecord : listOne) {
//				if ("1".equals(cellProductionRecord.getTempleItem().getHarvest())) {
//					CellProductionRecord record = new CellProductionRecord();
//					record=cellProductionRecord;
//					return record;
//				}
//			}
//		}
		return record;
	}
//	public CellProductionRecord findAboveCellProductionRecord(String code,String  parseInt) {
//		CellProductionRecord record = new CellProductionRecord();
//		
//		String hqlOne = "from CellProductionRecord c where 1=1 and c.cellPassage ='" + code + "' and c.orderNum='"+parseInt+"'";
//		record = (CellProductionRecord) getSession().createQuery(hqlOne).uniqueResult();
//		return record;
//	}

}