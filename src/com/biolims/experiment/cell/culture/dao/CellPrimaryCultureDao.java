package com.biolims.experiment.cell.culture.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.culture.model.CellPrimaryCulture;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureCos;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureInfo;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureItem;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureReagent;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureTemp;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureTemplate;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureZhiJian;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class CellPrimaryCultureDao extends BaseHibernateDao {

	public Map<String, Object> findCellPrimaryCultureTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPrimaryCulture where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellPrimaryCulture where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellPrimaryCulture> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectCellPrimaryCultureTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from CellPrimaryCultureTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<CellPrimaryCultureTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from CellPrimaryCultureTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findCellPrimaryCultureItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPrimaryCultureItem where 1=1 and state='1' and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellPrimaryCultureItem  where 1=1 and state='1' and cellPrimaryCulture.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellPrimaryCultureItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<CellPrimaryCultureItem> showWellPlate(String id) {
		String hql="from CellPrimaryCultureItem  where 1=1 and state='2' and  cellPrimaryCulture.id='"+id+"'";
		List<CellPrimaryCultureItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findCellPrimaryCultureItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPrimaryCultureItem where 1=1 and state='2' and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellPrimaryCultureItem  where 1=1 and state='2' and cellPrimaryCulture.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellPrimaryCultureItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from CellPrimaryCultureItem where 1=1 and state='2' and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from CellPrimaryCultureItem where 1=1 and state='2' and cellPrimaryCulture.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<CellPrimaryCultureTemplate> showCellPrimaryCultureStepsJson(String id,String code) {

		String countHql = "select count(*) from CellPrimaryCultureTemplate where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<CellPrimaryCultureTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CellPrimaryCultureTemplate where 1=1 and cellPrimaryCulture.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CellPrimaryCultureReagent> showCellPrimaryCultureReagentJson(String id,String code) {
		String countHql = "select count(*) from CellPrimaryCultureReagent where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<CellPrimaryCultureReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CellPrimaryCultureReagent where 1=1 and cellPrimaryCulture.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CellPrimaryCultureCos> showCellPrimaryCultureCosJson(String id,String code) {
		String countHql = "select count(*) from CellPrimaryCultureCos where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<CellPrimaryCultureCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CellPrimaryCultureCos where 1=1 and cellPrimaryCulture.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CellPrimaryCultureTemplate> delTemplateItem(String id) {
		List<CellPrimaryCultureTemplate> list=new ArrayList<CellPrimaryCultureTemplate>();
		String hql = "from CellPrimaryCultureTemplate where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<CellPrimaryCultureReagent> delReagentItem(String id) {
		List<CellPrimaryCultureReagent> list=new ArrayList<CellPrimaryCultureReagent>();
		String hql = "from CellPrimaryCultureReagent where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<CellPrimaryCultureCos> delCosItem(String id) {
		List<CellPrimaryCultureCos> list=new ArrayList<CellPrimaryCultureCos>();
		String hql = "from CellPrimaryCultureCos where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showCellPrimaryCultureResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPrimaryCultureInfo where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellPrimaryCultureInfo  where 1=1 and cellPrimaryCulture.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellPrimaryCultureInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from CellPrimaryCultureItem where 1=1 and cellPrimaryCulture.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<CellPrimaryCultureItem> plateSample(String id, String counts) {
		String hql="from CellPrimaryCultureItem where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<CellPrimaryCultureItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPrimaryCultureItem where 1=1 and cellPrimaryCulture.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellPrimaryCultureItem  where 1=1 and cellPrimaryCulture.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellPrimaryCultureItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<CellPrimaryCultureInfo> findCellPrimaryCultureInfoByCode(String code) {
		String hql="from CellPrimaryCultureInfo where 1=1 and code='"+code+"'";
		List<CellPrimaryCultureInfo> list=new ArrayList<CellPrimaryCultureInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellPrimaryCultureInfo> selectAllResultListById(String code) {
		String hql = "from CellPrimaryCultureInfo  where (submit is null or submit='') and cellPrimaryCulture.id='"
				+ code + "'";
		List<CellPrimaryCultureInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellPrimaryCultureInfo> selectResultListById(String id) {
		String hql = "from CellPrimaryCultureInfo  where cellPrimaryCulture.id='"
				+ id + "'";
		List<CellPrimaryCultureInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPrimaryCultureInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from CellPrimaryCultureInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<CellPrimaryCultureInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<CellPrimaryCultureItem> selectCellPrimaryCultureItemList(String scId)
			throws Exception {
		String hql = "from CellPrimaryCultureItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and cellPrimaryCulture.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CellPrimaryCultureItem> list = new ArrayList<CellPrimaryCultureItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from CellPrimaryCultureItem where 1=1 and cellPrimaryCulture.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public CellPrimaryCultureInfo getResultByCode(String code) {
			String hql=" from CellPrimaryCultureInfo where 1=1 and code='"+code+"'";
			CellPrimaryCultureInfo spi=(CellPrimaryCultureInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}

		/**
		 * @throws Exception   
		 * @Title: findCellPrimaryCultureTemplateList  
		 * @Description: TODO  
		 * @author : nan.jiang
		 * @date 2018-8-30上午9:13:07
		 * @param start
		 * @param length
		 * @param query
		 * @param col
		 * @param sort
		 * @param id
		 * @return
		 * Map<String,Object>
		 * @throws  
		 */ 
		public Map<String, Object> findCellPrimaryCultureTemplateList(
				Integer start, Integer length, String query, String col,
				String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();			
			String countHql = "select count(*) from CellPrimaryCultureTemplate where 1=1 and cellPrimaryCulture.id='"+id+"'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = " from CellPrimaryCultureTemplate where 1=1 and cellPrimaryCulture.id='"+id+"'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by orderNum";
				}
				List<CellPrimaryCultureTemplate> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		}

		public List<CellPrimaryCultureZhiJian> delZhijianItem(String id) {
			List<CellPrimaryCultureZhiJian> list=new ArrayList<CellPrimaryCultureZhiJian>();
			String hql = "from CellPrimaryCultureZhiJian where 1=1 and cellPrimaryCulture.id='"+id+"'";
			String key="";
			list = getSession().createQuery(hql+key).list();
			return list;
			}

		public List<CellPrimaryCultureZhiJian> showCellPrimaryCultureZjianJson(String id, String code) {
			String countHql = "select count(*) from CellPrimaryCultureZhiJian where 1=1 and cellPrimaryCulture.id='"+id+"'";
			String key = "";
			if(code!=null&&!"".equals(code)){
				key+=" and itemId='"+code+"'";
			}else{
				key+=" and itemId='1'";
			}
			List<CellPrimaryCultureZhiJian> list=null;
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				String hql = "from CellPrimaryCultureZhiJian where 1=1 and cellPrimaryCulture.id='"+id+"'";
				list = getSession().createQuery(hql+key).list();
				}
			return list;
		}

		public List<CellPrimaryCulture> findCellPrimaryCultureList(){
			List<CellPrimaryCulture> list = new ArrayList<CellPrimaryCulture>();
			String hql = "from CellPrimaryCulture where 1=1 and state <> '1'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<CellPrimaryCultureTemplate> findTemplateList(String id) {
			List<CellPrimaryCultureTemplate> list = new ArrayList<CellPrimaryCultureTemplate>();
			String hql = "from CellPrimaryCultureTemplate where 1=1 and cellPrimaryCulture.id = '"+id+"'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<CellPrimaryCulture> findCellPrimaryCultureListByUser(String user) {
			List<CellPrimaryCulture> list = new ArrayList<CellPrimaryCulture>();
			String hql = "from CellPrimaryCulture where 1=1 and state <> '1' and workUser like '%"+user+"%'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}
	
}