package com.biolims.experiment.cell.culture.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.cell.culture.service.CellPrimaryCultureService;

public class CellPrimaryCultureEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CellPrimaryCultureService mbService = (CellPrimaryCultureService) ctx
				.getBean("cellPrimaryCultureService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
