package com.biolims.experiment.cell.culture.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.culture.dao.CellPrimaryCultureDao;
import com.biolims.experiment.cell.culture.model.CellPrimaryCulture;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureAbnormal;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureCos;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureInfo;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureItem;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureReagent;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureTemp;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureTemplate;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureZhiJian;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service
@Transactional
public class CellPrimaryCultureService {
	
	@Resource
	private CellPrimaryCultureDao cellPrimaryCultureDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : 
	 * @date 
	 * @param id
	 * @return CellPrimaryCulture
	 * @throws
	 */
	public CellPrimaryCulture get(String id) {
		CellPrimaryCulture cellPrimaryCulture = commonDAO.get(CellPrimaryCulture.class, id);
		return cellPrimaryCulture;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPrimaryCultureItem(String delStr, String[] ids, User user, String cellPrimaryCulture_id) throws Exception {
		String delId="";
		for (String id : ids) {
			CellPrimaryCultureItem scp = cellPrimaryCultureDao.get(CellPrimaryCultureItem.class, id);
			if (scp.getId() != null) {
				CellPrimaryCulture pt = cellPrimaryCultureDao.get(CellPrimaryCulture.class, scp
						.getCellPrimaryCulture().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				CellPrimaryCultureTemp cellPrimaryCultureTemp = this.commonDAO.get(
						CellPrimaryCultureTemp.class, scp.getTempId());
				if (cellPrimaryCultureTemp != null) {
				pt.setSampleNum(pt.getSampleNum() - 1);
					cellPrimaryCultureTemp.setState("1");
					cellPrimaryCultureDao.update(cellPrimaryCultureTemp);
				}
				cellPrimaryCultureDao.update(pt);
				cellPrimaryCultureDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(cellPrimaryCulture_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delCellPrimaryCultureItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPrimaryCultureItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			CellPrimaryCultureItem scp = cellPrimaryCultureDao.get(CellPrimaryCultureItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				cellPrimaryCultureDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPrimaryCultureResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr="";
		for (String id : ids) {
			CellPrimaryCultureInfo scp = cellPrimaryCultureDao
					.get(CellPrimaryCultureInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				cellPrimaryCultureDao.delete(scp);
			idStr+=scp.getSampleCode()+",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setModifyContent(delStr+idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPrimaryCultureReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			CellPrimaryCultureReagent scp = cellPrimaryCultureDao.get(CellPrimaryCultureReagent.class,
					id);
			cellPrimaryCultureDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr+scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPrimaryCultureCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			CellPrimaryCultureCos scp = cellPrimaryCultureDao.get(CellPrimaryCultureCos.class, id);
			cellPrimaryCultureDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr+scp.getName());
		}
	}

	/**
	 * @throws ParseException 
	 * 
	 * @Title: saveCellPrimaryCultureTemplate
	 * @Description: 保存模板
	 * @author : 
	 * @date 
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCellPrimaryCultureTemplate(CellPrimaryCulture sc) throws ParseException {
		List<CellPrimaryCultureTemplate> tlist2 = cellPrimaryCultureDao.delTemplateItem(sc
				.getId());
		List<CellPrimaryCultureReagent> rlist2 = cellPrimaryCultureDao.delReagentItem(sc
				.getId());
		List<CellPrimaryCultureCos> clist2 = cellPrimaryCultureDao.delCosItem(sc.getId());
		List<CellPrimaryCultureZhiJian> zlist2 = cellPrimaryCultureDao.delZhijianItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		commonDAO.deleteAll(zlist2);
		
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		List<ZhiJianItem> zlist = templateService.getZjItem(sc.getTemplate()
				.getId(), null);
		for (ZhiJianItem t : zlist) {
			CellPrimaryCultureZhiJian ptr = new CellPrimaryCultureZhiJian();
			ptr.setCellPrimaryCulture(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			ptr.setNextId(t.getNextId());
			ptr.setTypeId(t.getTypeId());
			ptr.setTypeName(t.getTypeName());
			cellPrimaryCultureDao.saveOrUpdate(ptr);
		}
		
		//定义一个增长的变量
		Integer i = 0;
		for (TemplateItem t : tlist) {
			CellPrimaryCultureTemplate ptt = new CellPrimaryCultureTemplate();
			
			Integer result = 0;
			if(t.getEstimatedTime()!=null&&!"".equals(t.getEstimatedTime())) {
				ptt.setEstimatedDate(t.getEstimatedTime());
				
				i += Integer.parseInt(t.getEstimatedTime());
				result = i - Integer.parseInt(t.getEstimatedTime());
				String createDate = sc.getCreateDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse = format.parse(createDate);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.DATE, result);
				parse = calendar.getTime();
				String date = format.format(parse);
				ptt.setPlanWorkDate(date);
				
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse1 = format1.parse(createDate);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(calendar1.DATE, i);
				parse1 = calendar1.getTime();
				String date1 = format.format(parse1);
				ptt.setPlanEndDate(date1);
				
			}
			ptt.setCellPrimaryCulture(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			
			cellPrimaryCultureDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			CellPrimaryCultureReagent ptr = new CellPrimaryCultureReagent();
			ptr.setCellPrimaryCulture(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			cellPrimaryCultureDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			CellPrimaryCultureCos ptc = new CellPrimaryCultureCos();
			ptc.setCellPrimaryCulture(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			cellPrimaryCultureDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : 
	 * @date 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		CellPrimaryCulture sct = cellPrimaryCultureDao.get(CellPrimaryCulture.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		cellPrimaryCultureDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		CellPrimaryCulture sc = this.cellPrimaryCultureDao.get(CellPrimaryCulture.class, id);
		// 获取结果表样本信息
		List<CellPrimaryCultureInfo> list;
		if (ids == null)
			list = this.cellPrimaryCultureDao.selectAllResultListById(id);
		else
			list = this.cellPrimaryCultureDao.selectAllResultListByIds(ids);

		for (CellPrimaryCultureInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("CellPrimaryCultureInfo");
						if(scp.getSampleOrder()!=null) {
							st.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						st.setPronoun(scp.getPronoun());
						cellPrimaryCultureDao.saveOrUpdate(st);
					
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0073")) {// 细胞传代
						CellPassageTemp d = new CellPassageTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						if(scp.getSampleOrder()!=null) {
							d.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						d.setPronoun(scp.getPronoun());
						cellPrimaryCultureDao.saveOrUpdate(d);
					}else if (next.equals("0082")) {// 自主过程干细胞检测
						// 自主过程干细胞检测
						QualityTestTemp d = new QualityTestTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						if(scp.getSampleOrder()!=null) {
							d.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						d.setCellType("1");
						cellPrimaryCultureDao.saveOrUpdate(d);
					} else if (next.equals("0086")) {// 第三方过程干细胞检测
						// 第三方过程干细胞检测
						QualityTestTemp d = new QualityTestTemp();
						scp.setState("1");
						d.setState("1");
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						if(scp.getSampleOrder()!=null) {
							d.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						d.setCellType("5");
						cellPrimaryCultureDao.saveOrUpdate(d);
					
					} else {
					    scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					CellPrimaryCultureAbnormal pa = new CellPrimaryCultureAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				if(scp.getSampleOrder()!=null) {
					scp.getSampleOrder().setNewTask(id);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState1(
								scp.getSampleOrder(),
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"CellPrimaryCulture",
								"细胞原代培养",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				
				scp.setSubmit("1");
				cellPrimaryCultureDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findCellPrimaryCultureTable
	 * @Description: 展示主表
	 * @author :
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return cellPrimaryCultureDao.findCellPrimaryCultureTable(start, length, query, col,
				sort);
	}

	/**
	 * 
	 * @Title: selectCellPrimaryCultureTempTable
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectCellPrimaryCultureTempTable(String[] codes, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return cellPrimaryCultureDao.selectCellPrimaryCultureTempTable(codes,start, length, query,
				col, sort);
	}
	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : 
	 * @date 
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			CellPrimaryCulture pt = new CellPrimaryCulture();
			pt = (CellPrimaryCulture) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getName();
			Integer sampleNum=pt.getSampleNum();
			Integer maxNum=pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "CellPrimaryCulture";
				String markCode = "PC";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				
			} else {
				id = pt.getId();
				pt=commonDAO.get(CellPrimaryCulture.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			pt.setTestUserOneName(userName);
			cellPrimaryCultureDao.saveOrUpdate(pt);			
            if (pt.getTemplate() != null) {
				saveCellPrimaryCultureTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							CellPrimaryCultureTemp ptt = cellPrimaryCultureDao.get(
									CellPrimaryCultureTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									CellPrimaryCultureItem pti = new CellPrimaryCultureItem();
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									pti.setTempId(temp);
									pti.setCellPrimaryCulture(pt);
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									pti.setSampleInfo(ptt.getSampleInfo());
									if(ptt.getSampleOrder()!=null) {
										pti.setSampleOrder(commonDAO.get(SampleOrder.class,
												ptt.getSampleOrder().getId()));
									}
									cellPrimaryCultureDao.saveOrUpdate(pti);
								}
							} else {
								CellPrimaryCultureItem pti = new CellPrimaryCultureItem();
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setCellPrimaryCulture(pt);
								if(ptt.getSampleOrder()!=null) {
									pti.setSampleOrder(commonDAO.get(SampleOrder.class,
											ptt.getSampleOrder().getId()));
								}
								cellPrimaryCultureDao.saveOrUpdate(pti);
							}
							cellPrimaryCultureDao.saveOrUpdate(ptt);
						}
				}

			
			}
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findCellPrimaryCultureItemTable
	 * @Description:展示未排版样本
	 * @author : 
	 * @date 
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findCellPrimaryCultureItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return cellPrimaryCultureDao.findCellPrimaryCultureItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : 
	 * @date 
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<CellPrimaryCultureItem> saveItems = new ArrayList<CellPrimaryCultureItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		CellPrimaryCulture pt = commonDAO.get(CellPrimaryCulture.class, id);
		CellPrimaryCultureItem scp = new CellPrimaryCultureItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (CellPrimaryCultureItem) cellPrimaryCultureDao.Map2Bean(map, scp);
			CellPrimaryCultureItem pti = commonDAO.get(CellPrimaryCultureItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());    
			scp.setCellPrimaryCulture(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		cellPrimaryCultureDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : 
	 * @date 
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<CellPrimaryCultureItem>
	 * @throws
	 */
	public List<CellPrimaryCultureItem> showWellPlate(String id) throws Exception {
		List<CellPrimaryCultureItem> list = cellPrimaryCultureDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findCellPrimaryCultureItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date 
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findCellPrimaryCultureItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return cellPrimaryCultureDao.findCellPrimaryCultureItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			CellPrimaryCultureItem pti = commonDAO.get(CellPrimaryCultureItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			cellPrimaryCultureDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showCellPrimaryCultureStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date 
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showCellPrimaryCultureStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellPrimaryCultureTemplate> pttList = cellPrimaryCultureDao
				.showCellPrimaryCultureStepsJson(id, code);
		List<CellPrimaryCultureReagent> ptrList = cellPrimaryCultureDao
				.showCellPrimaryCultureReagentJson(id, code);
		List<CellPrimaryCultureCos> ptcList = cellPrimaryCultureDao.showCellPrimaryCultureCosJson(id,
				code);
		List<CellPrimaryCultureZhiJian> ptzList = cellPrimaryCultureDao.showCellPrimaryCultureZjianJson(id,
				code);
		List<Object> plate = cellPrimaryCultureDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("zhiJian", ptzList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showCellPrimaryCultureResultTableJson
	 * @Description: 展示结果
	 * @author : 
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showCellPrimaryCultureResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return cellPrimaryCultureDao.showCellPrimaryCultureResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		CellPrimaryCulture pt = commonDAO.get(CellPrimaryCulture.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellPrimaryCultureItem> list = cellPrimaryCultureDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
		row = pt.getTemplate().getStorageContainer().getRowNum();
		col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = cellPrimaryCultureDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			CellPrimaryCulture pt = cellPrimaryCultureDao.get(CellPrimaryCulture.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<CellPrimaryCultureInfo> listPTI = cellPrimaryCultureDao
								.findCellPrimaryCultureInfoByCode(code);
						if (listPTI.size() > 0) {
							CellPrimaryCultureInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader
									.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							cellPrimaryCultureDao.saveOrUpdate(spi);
						}
					
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : 
	 * @date 
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}	
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date 
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			CellPrimaryCultureTemplate ptt = cellPrimaryCultureDao.get(
					CellPrimaryCultureTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			String testUsers=(String) object.get("testUserList");
			if(testUsers!=null&&!"".equals(templateId)){
				ptt.setTestUserList(testUsers);
			}
			String estimatedTime=(String) object.get("estimatedTime");
			if(estimatedTime!=null&&!"".equals(estimatedTime)){
				ptt.setEstimatedDate(estimatedTime);
			}
			String sampleDeteyion=(String) object.get("sampleDeteyion");
			if(sampleDeteyion!=null&&!"".equals(sampleDeteyion)){
				SampleDeteyion sampleDeteyion2 = commonDAO.get(SampleDeteyion.class, sampleDeteyion);
				ptt.setSampleDeteyion(sampleDeteyion2);
			}
			cellPrimaryCultureDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : 
	 * @date 
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
	
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			CellPrimaryCultureReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(CellPrimaryCultureReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new CellPrimaryCultureReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setCellPrimaryCulture(commonDAO.get(CellPrimaryCulture.class, id));
				}

			}
			cellPrimaryCultureDao.saveOrUpdate(ptr);
		}
	
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
	
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			CellPrimaryCultureCos ptc=null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(CellPrimaryCultureCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new CellPrimaryCultureCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setCellPrimaryCulture(commonDAO.get(CellPrimaryCulture.class, id));
				}
			}
			cellPrimaryCultureDao.saveOrUpdate(ptc);
		}
	
	
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : 
	 * @date 
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return cellPrimaryCultureDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
	
	
		List<CellPrimaryCultureItem> list = cellPrimaryCultureDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellPrimaryCultureInfo> spiList = cellPrimaryCultureDao.selectResultListById(id);
		for (CellPrimaryCultureItem pti : list) {
			boolean b = true;
			for (CellPrimaryCultureInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							CellPrimaryCultureInfo scp = new CellPrimaryCultureInfo();
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setSampleInfo(pti.getSampleInfo());
							scp.setCellPrimaryCulture(pti.getCellPrimaryCulture());
							scp.setResult("1");
							String markCode = "HT";
							/*if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = str;
									} else {
										markCode = str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = scp.getSampleCode();
									} else {
										markCode = scp.getSampleCode()
												+ d.getCode();
									}
								}

							}*/
							markCode += DateUtil.dateFormatterByPattern(
									new Date(), "yyMMdd")+ds.getCode()+"P0";
							String code = codingRuleService.getCode(
									"CellPrimaryCultureInfo", markCode, 00000, 5, null);
							scp.setCode(code);
							if(pti.getSampleOrder()!=null) {
								scp.setSampleOrder(commonDAO.get(SampleOrder.class,
										pti.getSampleOrder().getId()));
							}
							scp.setPronoun("P0");
							cellPrimaryCultureDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								CellPrimaryCultureInfo scp = new CellPrimaryCultureInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setSampleInfo(pti.getSampleInfo());
								scp.setCellPrimaryCulture(pti.getCellPrimaryCulture());
								scp.setResult("1");
								String markCode = "HT";
								/*if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}*/
								markCode += DateUtil.dateFormatterByPattern(
										new Date(), "yyMMdd")+ds.getCode()+"P0";
								String code = codingRuleService.getCode(
										"CellPrimaryCultureInfo", markCode, 00000, 5,
										null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								if(pti.getSampleOrder()!=null) {
									scp.setSampleOrder(commonDAO.get(SampleOrder.class,
											pti.getSampleOrder().getId()));
								}
								cellPrimaryCultureDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						CellPrimaryCultureInfo spi = cellPrimaryCultureDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						cellPrimaryCultureDao.saveOrUpdate(spi);
					}
				}
			}
		}
	
	
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser) throws Exception {

		List<CellPrimaryCultureInfo> saveItems = new ArrayList<CellPrimaryCultureInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		CellPrimaryCulture pt = commonDAO.get(CellPrimaryCulture.class, id);		
		for (Map<String, Object> map : list) {
		CellPrimaryCultureInfo scp = new CellPrimaryCultureInfo();
			// 将map信息读入实体类
			scp = (CellPrimaryCultureInfo) cellPrimaryCultureDao.Map2Bean(map, scp);
			scp.setCellPrimaryCulture(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			cellPrimaryCultureDao.saveOrUpdate(pt);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		cellPrimaryCultureDao.saveOrUpdateAll(saveItems);

	}

	public List<CellPrimaryCultureItem> findCellPrimaryCultureItemList(String scId)
			throws Exception {
		List<CellPrimaryCultureItem> list = cellPrimaryCultureDao.selectCellPrimaryCultureItemList(scId);
		return list;
	}
	public Integer generateBlendCode(String id) {
		return cellPrimaryCultureDao.generateBlendCode(id);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(CellPrimaryCulture pt,String []ids,String logInfo) throws Exception {
		if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				CellPrimaryCultureItem pti = new CellPrimaryCultureItem();
				pti.setCellPrimaryCulture(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				cellPrimaryCultureDao.save(pti);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		
	}

@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void saveRightQuality(CellPrimaryCulture pt,String []ids,String logInfo) throws Exception {
	if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				CellPrimaryCultureItem pti = new CellPrimaryCultureItem();
				pti.setCellPrimaryCulture(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	
}
public CellPrimaryCultureInfo getInfoById(String idc) {
	return cellPrimaryCultureDao.get(CellPrimaryCultureInfo.class,idc);
	
}

/**
 * @throws Exception   
 * @Title: findCellPrimaryCultureTemplateList  
 * @Description: TODO  
 * @author : nan.jiang
 * @date 2018-8-30上午9:11:58
 * @param start
 * @param length
 * @param query
 * @param col
 * @param sort
 * @param ids
 * @return
 * Map<String,Object>
 * @throws  
 */ 
public Map<String, Object> findCellPrimaryCultureTemplateList(Integer start,
		Integer length, String query, String col, String sort, String id) throws Exception {
	return cellPrimaryCultureDao.findCellPrimaryCultureTemplateList(start, length, query, col,
			sort,id);
}

@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void submitToQualityTest(String id, String type, String stepNum, String mark, String[] ids) throws Exception {
	List<CellPrimaryCultureZhiJian> ptzList = cellPrimaryCultureDao.showCellPrimaryCultureZjianJson(id,
			stepNum);
	if(ids.length>0) {
		for (String itemId : ids) {
			CellPrimaryCultureItem scp = commonDAO.get(CellPrimaryCultureItem.class, itemId);
			if(ptzList.size()>0) {
				for (CellPrimaryCultureZhiJian zj : ptzList) {
						
						String markCode = "QC" + scp.getCode();
						String code = codingRuleService.getCode(
								"QualityTestTemp", markCode, 0000, 4,
								null);
						QualityTestTemp cpt=new QualityTestTemp();
						cpt.setCode(code);
						scp.setState("1");
						cpt.setState("1");
						cpt.setZjId(zj.getId());
						cpt.setTestItem(zj.getName());
						cpt.setZjName(zj.getName());
						cpt.setExperimentalSteps(zj.getItemId());
						cpt.setMark(mark);
						cpt.setOrderCode(scp.getOrderCode());
						cpt.setParentId(scp.getCode());
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						if(zj.getCode()!=null) {
							SampleDeteyion sampleDeteyion = commonDAO.get(SampleDeteyion.class, zj.getCode());
							cpt.setSampleDeteyion(sampleDeteyion);
						}
						if(scp.getSampleOrder()!=null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						if(type.equals("zj01")) {//自主过程干细胞检测
							cpt.setCellType("1");
						}else if(type.equals("zj02")) {//第三方过程干细胞检测
							cpt.setCellType("5");
						}
						cellPrimaryCultureDao.saveOrUpdate(cpt);
						cellPrimaryCultureDao.saveOrUpdate(scp);
					/*CellPrimaryCulture sc = commonDAO.get(CellPrimaryCulture.class, id);
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/
					/*sampleStateService
							.saveSampleState1(
									scp.getSampleOrder(),
									scp.getCode(),
									scp.getSampleCode(),
									scp.getProductId(),
									scp.getProductName(),
									"",
									sc.getCreateDate(),
									format.format(new Date()),
									"CellPrimaryCulture",
									"原代细胞生产明细",
									(User) ServletActionContext
											.getRequest()
											.getSession()
											.getAttribute(
													SystemConstants.USER_SESSION_KEY),
									id, next, null, null,
									null, null, null, null, null, null, null);*/
				}
			}
		}
	}
}
}

