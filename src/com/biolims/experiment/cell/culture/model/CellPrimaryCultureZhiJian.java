package com.biolims.experiment.cell.culture.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

@Entity
@Table(name = "CELL_PRIMARY_CULTURE_ZHIJIAN")
@SuppressWarnings("serial")
public class CellPrimaryCultureZhiJian extends EntityDao<CellPrimaryCultureZhiJian> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/**质检编号*/
	private String code;
	/** 名称 */
	private String name;
	/** 质检类型 */
	private String type;
	// 备注
	private String note;
	/** 关联步骤*/
	private String itemId;
	/** 相关主表 */
	private CellPrimaryCulture cellPrimaryCulture;
	/**下一步质检的流向*/
	private String nextId;
	/**质检类型id*/
	private String typeId;
	/**质检类型名称*/
	private String typeName;
	

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	

	public String getNextId() {
		return nextId;
	}

	public void setNextId(String nextId) {
		this.nextId = nextId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_PRIMARY_CULTURE")
	public CellPrimaryCulture getCellPrimaryCulture() {
		return cellPrimaryCulture;
	}

	public void setCellPrimaryCulture(CellPrimaryCulture cellPrimaryCulture) {
		this.cellPrimaryCulture = cellPrimaryCulture;
	}

	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}


}