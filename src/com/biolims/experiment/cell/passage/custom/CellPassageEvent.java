package com.biolims.experiment.cell.passage.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.cell.passage.service.CellPassageService;

public class CellPassageEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CellPassageService mbService = (CellPassageService) ctx
				.getBean("cellPassageService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
