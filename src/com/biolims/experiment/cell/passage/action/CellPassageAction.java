﻿package com.biolims.experiment.cell.passage.action;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageBeforeReagent;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageObservation;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.cell.passage.model.CellproductReagent;
import com.biolims.experiment.cell.passage.service.CellPassageNewService;
import com.biolims.experiment.cell.passage.service.CellPassageService;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.service.CleanAreaBacteriaService;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressure;
import com.biolims.experiment.enmonitor.differentialpressure.service.CleanAreaDiffPressureService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobe;
import com.biolims.experiment.enmonitor.microbe.service.SettlingMicrobeService;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.microorganism.service.CleanAreaMicroorganismService;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.experiment.quality.model.QualityTestAbnormal;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.service.RoomManagementService;
import com.biolims.experiment.transferWindowManage.model.TransferWindowState;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.product.model.Product;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/cell/passage/cellPassage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CellPassageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "242202";
	@Autowired
	private CellPassageService cellPassageService;
	@Autowired
	private SampleSearchService sampleSearchService;
	private CellPassage cellPassage = new CellPassage();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CellPassageDao cellPassageDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Autowired
	private CleanAreaMicroorganismService microorganismService;
	private CleanAreaMicroorganism cam = new CleanAreaMicroorganism();
	@Autowired
	private DustParticleService dustParticleService;
	private CleanAreaDust dif = new CleanAreaDust();
	private CleanAreaVolume dif2 = new CleanAreaVolume();
	@Autowired
	private SettlingMicrobeService settlingMicrobeService;
	private CleanAreaMicrobe dif1 = new CleanAreaMicrobe();
	@Autowired
	private CleanAreaBacteriaService cleanAreaBacteriaService;
	private CleanAreaBacteria dif3 = new CleanAreaBacteria();
	@Autowired
	private CleanAreaDiffPressureService diffPressureService;
	private CleanAreaDiffPressure cad = new CleanAreaDiffPressure();
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	@Resource
	private CellPassageNewService cellPassageNewService;
	@Resource
	private RoomManagementService roomManagementService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 回输样本到回输计划 @Title: feedBack @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2019年4月16日 @throws
	 */
	@Action(value = "feedBack")
	public synchronized void feedBack() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.feedBack(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	public String bianhuan(String orderNum){
		if(orderNum.indexOf(".")==-1){
			
		}else{
			String[] onm = orderNum.split("\\.");
			orderNum = onm[0];
		}
		return orderNum;
	}
	
	/**
	 * 查询匹配模板
	 * 
	 * @throws Exception
	 */
	@Action(value = "getTemplateId")
	public void getTemplateId() throws Exception {
		String proId = getParameterFromRequest("proId");
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String tpId = cellPassageService.getTemplate(proId, id);
			map.put("tpId", tpId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	@Action(value = "showCleanAreaDustEdit")
	public String showCleanAreaBacteriaTable() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			dif = commonService.get(CleanAreaDust.class, id);
			toState(dif.getState());
		}else{
			dif.setState("3");
			dif.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/dust/dustParticleEdit.jsp");
	}
	
	@Action(value = "showCleanAreaBacteriaEdit")
	public String showCleanAreaBacteriaEdit() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			dif3 = commonService.get(CleanAreaBacteria.class, id);
			toState(dif3.getState());
		}else{
			dif3.setState("3");
			dif3.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/bacteria/cleanAreaBacteriaEdit.jsp");
	}
	@Action(value = "showCleanAreaMicrodeEdit")
	public String showSettlingMicrobeTable() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			dif1 = commonService.get(CleanAreaMicrobe.class, id);
			toState(dif1.getState());
		}else{
			dif1.setId("NEW");
			dif1.setState("3");
			dif1.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/microbe/cleanAreaMicrobeEdit.jsp");
	}
	
	@Action(value = "showDifferentialpressureEdit")
	public String showDifferentialpressureEdit() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			cad = commonService.get(CleanAreaDiffPressure.class, id);
			toState(cad.getState());
		}else{
			cad.setId("NEW");
			cad.setState("3");
			cad.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/differentialpresure/cleanAreaDiffpressureEdit.jsp");
	}
	
	@Action(value = "showCleanAreaVolumeEdit")
	public String showCleanAreaVolumeEdit() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			dif2 = commonService.get(CleanAreaVolume.class, id);
			toState(dif2.getState());
		}else{
			dif2.setState("3");
			dif2.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/volume/cleanAreaVolumeEdit.jsp");
	}
	
	
	
	public CleanAreaDust getDif() {
		return dif;
	}

	public void setDif(CleanAreaDust dif) {
		this.dif = dif;
	}

	@Action(value = "showMicroorganismEdit")
	public String showMicroorganismEdit() throws Exception {
		String rightsIds = "211030501";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			cam = commonService.get(CleanAreaMicroorganism.class, id);
			toState(cam.getState());
		}else{
			cam.setId("NEW");
			cam.setState("3");
			cam.setStateName("新建");
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		toToolBar(rightsIds, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		return dispatcher("/WEB-INF/page/experiment/enmonitor/microorganism/cleanAreaMicroorganismEdit.jsp");
	}
	/**
	 * 查看模板明细中那一步是需要混的 并且判断该步骤是否收获，运输计划是否通过 @Title: findBlendState @Description:
	 * 新加逻辑，判断子表数据是否合格  根据质检表判断，只要质检表有不合格数据，样本表数据需要是警戒或者不合格  不合格样本不走当前方法，走不合格终止方法
	 * TODO @param @throws Exception @return void @author 孙灵达 @date
	 * 2019年4月16日 @throws
	 */
	@Action(value = "findBlendState")
	public void findBlendState() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			CellPassage cp = cellPassageService.get(mainId);
			if (cp != null) {
				//判断该样本是否存在偏差，并且是否完成偏差
				List<InfluenceProduct> ips = cellPassageDao.findInfluenceProducts(cp.getBatch());
				if(ips.size()>0){
					map.put("deviation", true);
				}else{
					map.put("deviation", false);
				}
				TemplateItem ti = cellPassageNewService.getBlendStateByStep(cp.getTemplate().getId(), bianhuan(stepNum));
				if (ti != null) {
					// 判断该步骤是否收获，运输计划是否通过
					if (ti.getHarvest() != null && !"".equals(ti.getHarvest())) {
						if ("1".equals(ti.getHarvest())) {
							map.put("isHarvest", true);
							List<CellPassageItem> cpis = cellPassageDao.findCellPassageItemListByStep(stepNum, mainId);
							if (cpis.size() > 0) {
								CellPassageItem cpi = cpis.get(0);
								if (cpi != null) {
									if (cpi.getOrderCode() != null && !"".equals(cpi.getOrderCode())) {
										List<ReinfusionPlanItem> rpis = commonService.get(ReinfusionPlanItem.class,
												"sampleOrder.id", cpi.getOrderCode());
										if (rpis.size() > 0) {
											boolean pandaun = false;
											for (ReinfusionPlanItem rpi : rpis) {
												if ("1".equals(rpi.getFeedBack())) {// "1".equals(rpi.getResult()) &&
													pandaun = true;
												}
											}
											if (pandaun) {
												map.put("harvest", true);
											} else {
												map.put("harvest", false);
											}
										} else {
											map.put("harvest", false);
										}
									} else {
										map.put("harvest", false);
									}
								} else {
									map.put("harvest", false);
								}
							} else {
								map.put("harvest", false);
							}
						} else {
							map.put("harvest", true);
							map.put("isHarvest", false);
						}
					} else {
						map.put("harvest", true);
						map.put("isHarvest", false);
					}
					map.put("ti", ti);
				} else {
					map.put("harvest", true);
					map.put("isHarvest", false);
				}
				String message = "";
				List<CellPassageQualityItem> cpqis = cellPassageDao.findCellPassageQualityItemsById(mainId);
				
				List<CellPassageItem> cpiss = cellPassageDao.findCellPassageItemListByStepXia(stepNum, mainId);
				if (cpiss.size() > 0) {
					for(CellPassageItem cpi:cpiss){
						if(cpi.getQualified()!=null
								&&!"".equals(cpi.getQualified())){
							if(cpqis.size()>0){
								if(cpi.getQualified()!=null
										&&!"".equals(cpi.getQualified())
										&&"2".equals(cpi.getQualified())){
									message = "质检中存在争议数据，样本需选择警戒流转！";
									map.put("result", false);//样本结果表状态
									break;
								}
								if(cpi.getQualified()!=null
										&&!"".equals(cpi.getQualified())
										&&"1".equals(cpi.getQualified())){
									message = "样本不合格作废请点击不合格终止！";
									map.put("result", false);//样本结果表状态
									break;
								}
								map.put("result", true);//样本结果表状态
							}else{
								if(cpi.getQualified()!=null
										&&!"".equals(cpi.getQualified())
										&&"1".equals(cpi.getQualified())){
									message = "样本不合格作废请点击不合格终止！";
									map.put("result", false);//样本结果表状态
									break;
								}
								map.put("result", true);//样本结果表状态
							}
						}else{
							message = "样本质检状态不能为空！";
							map.put("result", false);//样本结果表状态
							break;
						}
						
					}
					map.put("message", message);//样本结果描述
					map.put("resultNum", true);//样本结果表数量
				}else{
					map.put("message", "样本结果表为空，请确认！");//样本结果描述
					map.put("result", false);//样本结果表状态
					map.put("resultNum", false);//样本结果表数量
				}
			} else {
				map.put("harvest", true);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 *查找生产操作的开始时间和结束时间
	 * @throws Exception 
	 * 
	 */
	@Action(value ="findProductTime")
	public void findProductTime() throws Exception {
		String mainId =getParameterFromRequest("mainId");
		String stepNum = getParameterFromRequest("stepNum");
		boolean state = cellPassageService.findProductTime(mainId,stepNum);
		
		Map<String, Object> ruku = cellPassageService.findCellPassageRecords(mainId,stepNum);
		
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", state);
		
		map.put("samplestate", ruku.get("samplestate"));
		map.put("state", ruku.get("state"));
		
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 重复当前步骤
	 * TODO @param @throws Exception @return void @author 
	 * 
	 */
	@Action(value = "chongfu")
	public void chongfu() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			CellPassage cp = cellPassageService.get(mainId);
			if (cp != null) {
				CellPassageTemplate cpt = cellPassageNewService.getCellPassageTemplateByCellAndStep(stepNum,mainId);
				if(cpt!=null){
					if(cpt.getEndTime()!=null
							&&!"".equals(cpt.getEndTime())){
						map.put("success", false);
					}else{
						String yuanSteNum = "";

						if(stepNum.indexOf(".")==-1){
							yuanSteNum = stepNum;
						}else{
							String[] stepNums = stepNum.split("\\.");
							yuanSteNum = stepNums[0];
						}
						
						
						TemplateItem ti = cellPassageNewService.getBlendStateByStep(cp.getTemplate().getId(), yuanSteNum);
						if (ti != null) {

							cellPassageService.saveshuju(ti,cp,stepNum);
							
							map.put("success", true);
						} else {
							map.put("success", false);
						}
					
						
					}
				}else{
					map.put("success", false);
				}
				
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 判断当前步骤是否是最后一步
	 * TODO @param @throws Exception @return void @author 
	 * 
	 */
	@Action(value = "changeButtonBlock")
	public void changeButtonBlock() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<CellProductionRecord> cp = cellPassageService.getCellProductionRecordByIdAndStepNum(mainId,stepNum);
			if (cp.size()>0) {
				map.put("zhyb", false);
			} else {
				map.put("zhyb", true);
			}
			List<CellProductionRecord> cps = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(mainId,stepNum);
			if (cps.size()>0) {
				CellProductionRecord cpr = cps.get(0);
				if(cpr.getEndTime()!=null
						&&!"".equals(cpr.getEndTime())){
					map.put("dqbz", false);
				}else{
					map.put("dqbz", true);
				}
			} else {
				map.put("dqbz", true);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "changeButtonBlockNew")
	public void changeButtonBlockNew() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellProductionRecord> cp = cellPassageService.getCellProductionRecordByIdAndStepNum(mainId,stepNum);
		if (cp.size()>0) {
			map.put("zhyb", false);
		} else {
			map.put("zhyb", true);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	/**
	 * 确认当前步骤,如果混合将混合后得样本插入到下一步中 @Title: finishPresentStep @Description:
	 * TODO @param @throws Exception @return void @author 孙灵达 @date
	 * 2019年4月11日 @throws
	 */
	@Action(value = "finishPresentStep")
	public void finishPresentStep() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		String startTime = getParameterFromRequest("startTime");
		String endTime = getParameterFromRequest("endTime");
		String hhzt = getParameterFromRequest("hhzt");
		String ultravioletlamptime = getParameterFromRequest("ultravioletlamptime");
		
		String roomName = getParameterFromRequest("roomName");
		String roomId = getParameterFromRequest("roomId");
		String rwdid = getParameterFromRequest("rwdid");
		String bzname = getParameterFromRequest("bzname");
		String batch = getParameterFromRequest("batch");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.finishPresentStep(id, ids, stepNum, mainId, startTime, endTime, hhzt,ultravioletlamptime);
			map.put("success", true);
		} catch (CloneNotSupportedException e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * 不合格样本完成 改变主表状态 不合格终止（暂时不考虑回输计划，运输计划，QC部分）同时通知医事服务部人员和QC
	 */
	@Action(value = "finishPresentStepFailed")
	public void finishPresentStepFailed() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		String startTime = getParameterFromRequest("startTime");
		String endTime = getParameterFromRequest("endTime");
		String hhzt = getParameterFromRequest("hhzt");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.finishPresentStepFailed(id, ids, stepNum, mainId, startTime, endTime, hhzt);
			map.put("success", true);
		} catch (CloneNotSupportedException e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 样本出库
	 * Exception @return void @author 
	 */
	@Action(value = "getblendSample")
	public void getblendSample() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.getblendSample(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 判断当前步骤是否是重复步骤
	 * Exception @return void @author 
	 */
	@Action(value = "getStepChongfu")
	public void getStepChongfu() throws Exception {
		String barCod = getParameterFromRequest("barCod");
		String mc = getParameterFromRequest("mc");
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			CellPassageTemplate cpt = cellPassageService.getStepChongfu( id, orderNum);
			if(cpt!=null){
				if(cpt.getChongfu()!=null
						&&"1".equals(cpt.getChongfu())){
					map.put("success", true);
					map.put("chongfu", true);
					sampleSearchService.insertLog(barCod, mc+"查看批记录");

				}else{
					map.put("success", true);
					map.put("chongfu", false);
					sampleSearchService.insertLog(barCod, mc+"查看批记录");
				}
			}else{
				map.put("success", false);
			}
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 步骤中得明细进行样本混合几管混合为1管 @Title: blendSample @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2019年4月11日 @throws
	 */
	@Action(value = "blendSample")
	public void blendSample() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.blendSample(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	/**
	 * 步骤中得明细进行样本混合几管混合为1管 @Title: blendSample @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2019年4月11日 @throws
	 */
	@Action(value = "blendSampleClick", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void blendSampleClick() throws Exception {
		// 主表id
		String id = getParameterFromRequest("id");
		// 步骤号
		String stepNum = getParameterFromRequest("stepNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.blendSampleClick(id, stepNum);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	
	@Action(value = "saveChildTable")
	public void saveChildTable() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		// 主表id
		String id = getParameterFromRequest("id");
		// 子表template的json数据
		String dataJson = getParameterFromRequest("dataJson");

		try {
			map.put("success", true);
			cellPassageService.saveChildTable(id, dataJson);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	/**
	 * 提到明细到质检 选质检项 @Title: submitToQualityTest @Description: TODO @param @throws
	 * Exception @return void @author @date 2018年9月12日 @throws
	 */
	@Action(value = "submitToQualityTestType")
	public void submitToQualityTestType() throws Exception {
		// 主表id
		String id = getParameterFromRequest("id");
		// 实验步骤
		String stepNum = getParameterFromRequest("stepNum");
		//实验名称
		String stepName = getParameterFromRequest("stepName");
		// 模板名称
		String type = getParameterFromRequest("zj");
		// 步骤中明细的id
		String[] ids = getRequest().getParameterValues("idList[]");
		// 要质检的项
		String[] chosedIDs = getRequest().getParameterValues("chosedID[]");
		// 哪个模块提交过去的
		String mark = getParameterFromRequest("mark");
		// 提交过来json数组
		String itemjson = getParameterFromRequest("data");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.submitToQualityTestType(id, stepNum,stepName, mark, ids, chosedIDs, type, itemjson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}
	
	/**
	 * 质检明细提交质检方法
	 * Exception @return void @author @date 2018年9月12日 @throws
	 */
	@Action(value = "submitToQualityTestSample")
	public void submitToQualityTestSample() throws Exception {
		// 质检明细id集合
		Map<String, Object> map = new HashMap<String, Object>();
		String zhiJianCode = getParameterFromRequest("zhiJianCode");
		String orderNum = getParameterFromRequest("orderNum");
		List<CellPassageQualityItem> findQualityTestSampleByCode = cellPassageService.findQualityTestSampleByCode(zhiJianCode);
		if(!findQualityTestSampleByCode.isEmpty()) {
		String id = findQualityTestSampleByCode.get(0).getId();

		if("1".equals(findQualityTestSampleByCode.get(0).getSubmit())) {
			map.put("success", false);
			
		}else {
			String[] ids = getRequest().getParameterValues("ids[]");
			
			String changeLogItem = getParameterFromRequest("changeLogItem");
			cellPassageService.submitToQualityTestSample(ids,changeLogItem,id,orderNum);
			map.put("success", true);

		}
		
		}else {
			map.put("success", false);
		}
	
		HttpUtils.write(JsonUtils.toJsonString(map));

	}
	
	
	//开始质检
	@Action(value = "findQualityTestSampleStatr", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findQualityTestSampleStatr() throws Exception {
		// 质检明细id集合\

		Map<String, Object> map = new HashMap<String, Object>();
		String code = getParameterFromRequest("inspectionStart");
		
		List<QualityTestTemp> findQualityTestSample = cellPassageService.findQualityTestSampleStart(code);
		if(findQualityTestSample.size()>0) {
			//已经进行条码确认了
			if("1".equals(findQualityTestSample.get(0).getConfirm())) {
				if(!"2".equals(findQualityTestSample.get(0).getState())) {
				
				List<TransferWindowState> findTransferWindowState = cellPassageService.findTransferWindowState(code);
				if(findTransferWindowState.size()>0) {
					
					map.put("qualityTestSample", findQualityTestSample.get(0));
					map.put("success", true);
				
				}else {
					map.put("success", true);
					map.put("pan", true);
				}
				
				}else {
					map.put("success", true);
					map.put("pan", true);
				}
			
			}else {
				map.put("success", false);
			}
		}else {
			map.put("success", false);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

		}
	/**
	 * 质检条码确认
	 * Exception @return void @author @date 2018年9月12日 @throws
	 */
	@Action(value = "findQualityTestSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findQualityTestSample() throws Exception {
		// 质检明细id集合
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Date date =new Date();
		Map<String, Object> map = new HashMap<String, Object>();
		String codeConfirm = getParameterFromRequest("codeConfirm");
		List<QualityTestTemp> findQualityTestSample = cellPassageService.findQualityTestSample(codeConfirm);
		if(findQualityTestSample.size()>0) {
			List<TransferWindowState> findTransferWindowState = cellPassageService.findTransferWindowStateNew(codeConfirm);
			if(findTransferWindowState.size()>0) {
				Date qualitySubmitTime = findTransferWindowState.get(0).getQualitySubmitTime();
				date.getTime();
				if((date.getTime()-qualitySubmitTime.getTime())<=1800000) {
					map.put("success", true);
					findQualityTestSample.get(0).setConfirm("1");
					findTransferWindowState.get(0).setSampleState("1");
					findTransferWindowState.get(0).setQualityReceiveTime(date);
					findTransferWindowState.get(0).setQualityReceiveUser(user.getName());
					cellPassageService.saveTransferWindowState(findTransferWindowState.get(0));
					cellPassageService.saveQualityTestSample(findQualityTestSample.get(0));
				}else {
					//超过30 分钟
					map.put("success", true);
					map.put("pan", true);
					if(!"2".equals(findQualityTestSample.get(0).getState())) {
					
					findQualityTestSample.get(0).setState("2");
					findTransferWindowState.get(0).setSampleState("0");
					findTransferWindowState.get(0).setQualityReceiveTime(date);
					findTransferWindowState.get(0).setQualityReceiveUser(user.getName());
					cellPassageService.saveTransferWindowState(findTransferWindowState.get(0));
					cellPassageService.saveQualityTestSample(findQualityTestSample.get(0));
					QualityTestAbnormal qualityTestAbnormal = new QualityTestAbnormal();
					//样本编号	
					
					qualityTestAbnormal.setCode(findQualityTestSample.get(0).getSampleNumber());
					//CCOI//产品批号//产品//筛选好
					qualityTestAbnormal.setSampleOrder(findQualityTestSample.get(0).getSampleOrder());
				
					//检测项
					if(findQualityTestSample.get(0).getSampleDeteyion()!=null) {
						qualityTestAbnormal.setProductName(findQualityTestSample.get(0).getSampleDeteyion().getName());
						//监测项ID
						qualityTestAbnormal.setProductId(findQualityTestSample.get(0).getSampleDeteyion().getId());

					}
					//样本类型
					qualityTestAbnormal.setSampleType(findQualityTestSample.get(0).getSampleType());
					//步骤号
					//步骤名称
					qualityTestAbnormal.setExperimentalStepsName(findQualityTestSample.get(0).getExperimentalStepsName());
					//检测量
					if(!"".equals(findQualityTestSample.get(0).getSampleNum())&&findQualityTestSample.get(0).getSampleNum()!=null) {
						Double valueOf = Double.valueOf(findQualityTestSample.get(0).getSampleNum());
						qualityTestAbnormal.setSampleNum(valueOf);

					}
					//监测单位
					qualityTestAbnormal.setUnit(findQualityTestSample.get(0).getSampleNumUnit());
					qualityTestAbnormal.setCellType("1");
					qualityTestAbnormal.setState("1");
					cellPassageService.saveQualityTestAbnormal(qualityTestAbnormal);
					
					}else {
						map.put("pan", true);
					}
				}
			}else {
				map.put("success", true);
				map.put("find", true);
			}
		}else {
			map.put("success", false);
			
		}
//		if(!findQualityTestSampleByCode.isEmpty()) {
//		String id = findQualityTestSampleByCode.get(0).getId();
//		if("1".equals(findQualityTestSampleByCode.get(0).getSubmit())) {
//			map.put("success", false);
//		}else {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			
//			String changeLogItem = getParameterFromRequest("changeLogItem");
//			cellPassageService.submitToQualityTestSample(ids,changeLogItem,id,orderNum);
		HttpUtils.write(JsonUtils.toJsonString(map));

		}
		
//		}else {
//			map.put("success", false);
//		}
	

//	}

	/**
	 * 生产步骤第一步保存设备 @Title: submitToQualityTest @Description: TODO @param @throws
	 * Exception @return void @author @date 2018年9月12日 @throws
	 */
	@Action(value = "saveCosPrepare", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCosPrepare() throws Exception {
		String cosListJson = getParameterFromRequest("data");
		String roomName = getParameterFromRequest("roomName");
		String roomId = getParameterFromRequest("roomId");
		String zbid = getParameterFromRequest("zbid");
		String bzs = getParameterFromRequest("bzs");
		String changeLog = getParameterFromRequest("changeLog");
		// JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.saveCosPrepare(cosListJson, roomName, zbid, bzs, roomId,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "startPrepare", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void startPrepare() throws Exception {
		String cosListJson = getParameterFromRequest("data");
		String roomName = getParameterFromRequest("roomName");
		String roomId = getParameterFromRequest("roomId");
		
		String rwdid = getParameterFromRequest("rwdid");
		String bzname = getParameterFromRequest("bzname");
		String batch = getParameterFromRequest("batch");
		String orderNum = getParameterFromRequest("orderNum");

		// JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Boolean zy = cellPassageService.startPrepare(cosListJson, roomId);
			if(zy){
				map.put("zy", true);
				cellPassageService.startPreparezy(rwdid,bzname,batch,orderNum,cosListJson, roomName, roomId);
			}else{
				map.put("zy", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 提到明细到质检 @Title: submitToQualityTest @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2018年9月12日 @throws
	 */
	@Action(value = "submitToQualityTest")
	public void submitToQualityTest() throws Exception {
		// 主表id
		String id = getParameterFromRequest("id");
		// 实验步骤
		String stepNum = getParameterFromRequest("stepNum");
		// 模板名称
		String type = getParameterFromRequest("zj");
		// 步骤中明细的id
		String[] ids = getRequest().getParameterValues("idList[]");
		// 哪个模块提交过去的
		String mark = getParameterFromRequest("mark");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.submitToQualityTest(id, stepNum, mark, type, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	/**
	 * 
	 * @Title: showCellPassageList @Description:展示主表 @author @date @return @throws
	 *         Exception String @throws
	 */
	@Action(value = "showCellPassageTable")
	public String showCellPassageTable() throws Exception {
		rightsId = "242202";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// return
		// dispatcher("/WEB-INF/page/experiment/cell/culture/cellPrimaryCulture.jsp");
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassage.jsp");
	}

	/* 细胞生成主表列表 */
	@Action(value = "showCellPassageTableJson")
	public void showCellPassageTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cellPassageService.findCellPassageTable(start, length, query, col, sort);
			List<CellPassage> list = (List<CellPassage>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("scopeName", "");
			map.put("batch", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: editCellPassage @Description: 新建实验单 @author @date @return @throws
	 *         Exception String @throws
	 */
	@Action(value = "editCellPassage")
	public String editCellPassage() throws Exception {
		rightsId = "242201";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			cellPassage = cellPassageService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
			if (cellPassage.getMaxNum() == null) {
				cellPassage.setMaxNum(0);
			}
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			cellPassage.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			cellPassage.setCreateUser(user);
			cellPassage.setMaxNum(0);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			cellPassage.setCreateDate(stime);
			cellPassage.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			cellPassage.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<Template> templateList = templateService.showDialogTemplateSCTableJson("CellPassage", null);
		List<Template> selTemplate = new ArrayList<Template>();
		List<UserGroupUser> userList = new ArrayList<UserGroupUser>();
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int j = 0; j < templateList.size(); j++) {
			if (templateList.get(j).getAcceptUser() != null) {
				List<UserGroupUser> userTempList = (List<UserGroupUser>) userGroupUserService
						.getUserGroupUserBygroupId(templateList.get(j).getAcceptUser().getId()).get("list");
				for (UserGroupUser ugu : userTempList) {
					if (!userList.contains(ugu)) {
						userList.add(ugu);
					}
				}
			}
			if (cellPassage.getTemplate() != null) {
				if (cellPassage.getTemplate().getId().equals(templateList.get(j).getId())) {
					selTemplate.add(templateList.get(j));
					templateList.remove(j);
					j--;
				}
			}
		}
		for (int i = 0; i < userList.size(); i++) {
			if (cellPassage.getTestUserOneId() != null) {
				String[] userOne = cellPassage.getTestUserOneId().split(",");
				one: for (String u : userOne) {
					if (u.equals(userList.get(i).getUser().getId())) {
						selUser.add(userList.get(i));
						userList.remove(i);
						i--;
						break one;
					}
				}

			}
		}
		putObjToContext("template", templateList);
		putObjToContext("selTemplate", selTemplate);
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		toState(cellPassage.getState());
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassageAllot.jsp");
	}

	/**
	 * 
	 * @Title: showCellPassageItemTable @Description: 展示待排板列表 @author
	 *         : @date @return @throws Exception String @throws
	 */
	@Action(value = "showCellPassageItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCellPassageItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		cellPassage = cellPassageService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassageMakeUp.jsp");
	}

	@Action(value = "showCellPassageItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCellPassageItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = cellPassageService.findCellPassageItemTable(scId, start, length, query, col,
					sort);
			List<CellPassageItem> list = (List<CellPassageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("chromosomalLocation", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showCellPassageItemAfTableJson @Description: 排板后样本展示 @author
	 *         : @date @throws Exception void @throws
	 */

	@Action(value = "showCellPassageItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCellPassageItemAfTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = cellPassageService.findCellPassageItemAfTable(scId, start, length, query, col,
					sort);
			List<CellPassageItem> list = (List<CellPassageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("cellPassage-name", "");
			map.put("cellPassage-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("chromosomalLocation", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("color", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			map.put("pronoun", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delCellPassageItem @Description: 删除待排板样本 @author @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "delCellPassageItem")
	public void delCellPassageItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			cellPassageService.delCellPassageItem(delStr, ids, user, id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: 删除待提交质检样本
	 *         Exception void @throws
	 */
	@Action(value = "delQualityTestSample")
	public void delQualityTestSample() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			cellPassageService.delQualityTestSampleType(ids, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: delCellPassageItemAf @Description: 重新排板 @author : @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "delCellPassageItemAf")
	public void delCellPassageItemAf() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			cellPassageService.delCellPassageItemAf(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBCellPassageResultTable @Description @author @date @return @throws
	 *         Exception String @throws
	 */
	@Action(value = "showCellPassageResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCellPassageResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		cellPassage = cellPassageService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		putObjToContext("orderNumBy", orderNum);
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassageResult.jsp");
	}

	@Action(value = "showCellPassageResultTableJson")
	public void showCellPassageResultTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cellPassageService.showCellPassageResultTableJson(id, start, length, query,
					col, sort);
			List<CellPassageInfo> list = (List<CellPassageInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleOrder-id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("pronoun", "");
			map.put("cellCount", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("parentId", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("note", "");
			map.put("cellPassage-id", "");
			map.put("cellPassage-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("reportDate", "yyyy-MM-dd");
			// map.put("concentration", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("specifications", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: delCellPassageResult @Description: 删除结果明细 @author : @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "delCellPassageResult")
	public void delCellPassageResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			cellPassageService.delCellPassageResult(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	@Action(value = "chooseCellPassageSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void chooseCellPassageSteps() throws Exception {
		String orderNumBy = getParameterFromRequest("orderNum");
		String id = getParameterFromRequest("id");   


		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

		cellPassageService.savecprNew(orderNumBy,id,user);

	}
	/**
	 * 
	 * @Title: showCellPassageSteps @Description: 实验步骤 @author
	 *         : @date @return @throws Exception String @throws
	 */
	@Action(value = "showCellPassageSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCellPassageSteps() throws Exception {
		String id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

		
		cellPassage = cellPassageService.get(id);
		String orderNumBy = getParameterFromRequest("orderNum");
		putObjToContext("orderNumBy", orderNumBy);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		
//		String cprId = getParameterFromRequest("cprId");
		cellPassageService.savecprNew(orderNumBy,id,user);
		
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassageSteps.jsp");
	}
	

	@Action(value = "showCellPassageStepsJson")
	public void showCellPassageStepsJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = cellPassageService.showCellPassageStepsJson(id, orderNum);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCellPassageReagent")
	public void delCellPassageReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("main_id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String id = getParameterFromRequest("id");
			cellPassageService.delCellPassageReagent(id, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delCellPassageCos")
	public void delCellPassageCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("main_id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String id = getParameterFromRequest("id");
			cellPassageService.delCellPassageCos(id, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: makeCode @Description: 打印条码 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String id = getParameterFromRequest("id");
		String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
		CodeMain codeMain = null;
		codeMain = codeMainService.get(id);
		if (codeMain != null) {
			String printStr = "";
			String context = "";
			for (int a = 0; a < sampleCode.length; a++) {
				String codeFull = sampleCode[a];
				String name = "";
				// sampleReceiveService.getNameBySampleCode(codeFull);
				printStr = codeMain.getCode();
				String code1 = sampleCode[a].substring(0, 9);
				String code2 = sampleCode[a].substring(9);
				printStr = printStr.replaceAll("@@code1@@", code1);
				printStr = printStr.replaceAll("@@code2@@", code2);
				printStr = printStr.replaceAll("@@code@@", codeFull);
				printStr = printStr.replaceAll("@@name@@", name);
				context += printStr;
			}
			String ip = codeMain.getIp();
			Socket socket = null;
			OutputStream os;
			try {
				System.out.println(context);
				socket = new Socket();
				SocketAddress sa = new InetSocketAddress(ip, 9100);
				socket.connect(sa);
				os = socket.getOutputStream();
				os.write(context.getBytes("UTF-8"));
				os.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: showCellPassageFromReceiveList @Description: 展示临时表 @author
	 *         : @date @return @throws Exception String @throws
	 */
	@Action(value = "showCellPassageTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCellPassageFromReceiveList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassageTemp.jsp");
	}
	@Action(value = "findBachIsDifference", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findBachIsDifference() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		String batch = getParameterFromRequest("batch");
		String length = getParameterFromRequest("length");
		boolean findBachIsDifference = cellPassageService.findBachIsDifference(batch,length);
		map.put("success", findBachIsDifference);
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "showCellPassageTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCellPassageTempTableJson() throws Exception {

		String[] codes = getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cellPassageService.selectCellPassageTempTable(codes, start, length, query, col,
					sort);
			List<CellPassageTemp> list = (List<CellPassageTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("pronoun", "");
			// map.put("concentration", "");
			map.put("volume", "");
			map.put("orderCode", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-project-id", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-changeType", "");

			map.put("batch", "");

			map.put("sampleInfo-sampleOrder-name", "");
			
			map.put("patientName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * finishPresentStep
	 * 
	 * @Title: saveAllot @Description: 保存任务分配页面 @author : @date @throws
	 */
	@Action(value = "saveAllot")
	public void saveAllot() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = cellPassageService.saveAllot(main, tempId, userId, templateId, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排版界面 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveMakeUp")
	public void saveMakeUp() throws Exception {
		String blood_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			cellPassageService.saveMakeUp(blood_id, item, logInfo);
			result.put("success", true);
			result.put("id", blood_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveLeftQuality")
	public void saveLeftQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			CellPassage cellPassage = cellPassageService.get(id);
			cellPassageService.saveLeftQuality(cellPassage, ids, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveRightQuality")
	public void saveRightQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			CellPassage cellPassage = cellPassageService.get(id);
			cellPassageService.saveRightQuality(cellPassage, ids, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveSteps")
	public void saveSteps() throws Exception {
		String id = getParameterFromRequest("id");
		String templateJson = getParameterFromRequest("templateJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String logInfo = getParameterFromRequest("logInfo");

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.saveSteps(id, templateJson, reagentJson, cosJson, logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果表 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveResult")
	public void saveResult() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		String confirmUser = getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.saveResult(id, dataJson, logInfo, confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate @Description: 展示排版 @author : @date @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<CellPassageItem> json = cellPassageService.showWellPlate(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			cellPassageService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "plateSample")
	public void plateSample() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = cellPassageService.plateSample(id, counts);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 混合之前显示的明细 @Title: noBlendPlateSampleTable @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2019年4月10日 @throws
	 */
	@Action(value = "noBlendPlateSampleTable")
	public void noBlendPlateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String[] codes = getRequest().getParameterValues("codes[]");
		String stepNum = getParameterFromRequest("stepNum");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = cellPassageService.noBlendPlateSampleTable(id, stepNum, codes, start, length, query, col, sort);
			List<CellPassageItem> list = (List<CellPassageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("cellPassage-name", "");
			map.put("cellPassage-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			map.put("qualified", "");

			map.put("batch", "");
			map.put("sampleOrder-filtrateCode", "");
			
			
			map.put("counts", "");
			map.put("instrument-id", "");
			map.put("instrument-storageContainer-rowNum", "");
			map.put("instrument-storageContainer-colNum", "");
			map.put("posId", "");
			
			map.put("countsOld", "");
			map.put("instrumentOld-id", "");
			map.put("instrumentOld-storageContainer-rowNum", "");
			map.put("instrumentOld-storageContainer-colNum", "");
			map.put("posIdOld", "");
			
			map.put("inventoryStatus", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 质检明细查询 @Title: noBlendPlateSampleTable @Description: TODO @param @throws
	 * Exception @return void @author @date @throws
	 */
	@Action(value = "findQualityItem")
	public void findQualityItem() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = cellPassageService.findQualityItem(id, start, length, query, col, sort);
			List<CellPassageQualityItem> list = (List<CellPassageQualityItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			/** 编码 */
			map.put("id", "");
			/** 提交表编码 */
			map.put("tempId", "");
			/** 样本编号 */
			map.put("code", "");
			/** 批次号 */
			map.put("batch", "");
			/** 订单编号 */
			map.put("orderCode", "");
			/** 检测项目 */
			map.put("productId", "");
			/** 检测项目 */
			map.put("productName", "");
			/** 状态 */
			map.put("state", "");
			/** 备注 */
			map.put("note", "");
			/** 浓度 */
			map.put("concentration", "");
			/** 样本数量 */
			map.put("sampleNum", "");
			/** 相关主表 */
			map.put("cellPassage-id", "");
			map.put("cellPassage-name", "");
			/** 体积 */
			map.put("volume", "");
			/** 任务单id */
			map.put("orderId", "");
			/** 中间产物数量 */
			map.put("productNum", "");
			/** 中间产物类型编号 */
			map.put("dicSampleTypeId", "");
			/** 中间产物类型 */
			map.put("dicSampleTypeName", "");
			/** 样本类型 */
			map.put("sampleType", "");
			/** 样本主数据 */
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-name", "");
			/** 实验的步骤号 */
			map.put("stepNum", "");
			/** 检测项 */
			map.put("sampleDeteyion-id", "");
			map.put("sampleDeteyion-name", "");
			/** 关联订单 */
			map.put("sampleOrder-id", "");
			map.put("sampleOrder-name", "");
			/** 是否合格 */
			map.put("qualified", "");
			/** 质检结果表id */
			map.put("qualityInfoId", "");
			/** 质检是否接收 */
			map.put("qualityReceive", "");
			/** 质检提交时间 */
			map.put("qualitySubmitTime", "yyyy-MM-dd HH:mm:ss");
			/** 质检接收时间 */
			map.put("qualityReceiveTime", "yyyy-MM-dd HH:mm:ss");
			/** 质检完成时间 */
			map.put("qualityFinishTime", "yyyy-MM-dd HH:mm:ss");

			map.put("sampleOrder-filtrateCode", "");
			
			/** 质检提交人 */
			map.put("qualitySubmitUser", "");
			/** 质检接收人 */
			map.put("qualityReceiveUser", "");
			/** 检测方式 */
			map.put("cellType", "");
			/** 是否提交 */
			map.put("submit", "");
			
			
			map.put("sampleNumUnit", "");
			
			map.put("experimentalStepsName", "");
			//样本编号New
			map.put("SampleNumber", "");
			//条形码号
			map.put("productCode", "");
			
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "blendPlateSampleTable")
	public void blendPlateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String stepNum = getParameterFromRequest("stepNum");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = cellPassageService.blendPlateSampleTable(id, stepNum, start, length, query, col, sort);
			List<CellPassageItem> list = (List<CellPassageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("cellPassage-name", "");
			map.put("cellPassage-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			map.put("qualified", "");

			map.put("batch", "");
			map.put("sampleOrder-filtrateCode", "");
			
			map.put("posId", "");
			map.put("counts", "");
			
			map.put("instrument-id", "");
			map.put("instrument-storageContainer-rowNum", "");
			map.put("instrument-storageContainer-colNum", "");
			
			map.put("posIdOld", "");
			map.put("countsOld", "");
			
			map.put("instrumentOld-id", "");
			map.put("instrumentOld-storageContainer-rowNum", "");
			map.put("instrumentOld-storageContainer-colNum", "");
			
			
			map.put("SampleNumber", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

//	/**
//	 * 
//	 * @Title: @Description: 查询生产步骤要用的设备 @author : @date @throws Exception
//	 *         void @throws
//	 */
//	@Action(value = "findProductCos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void findProductCos() throws Exception {
//		String id = getParameterFromRequest("id");
//		String stepNum = getParameterFromRequest("stepNum");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			List<CellproductCos> ccs = cellPassageService.getCellproductCoss(id, stepNum);
//			map.put("CellproductCoss", ccs);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
	
	/**
	 * 
	 * @Title: @Description: 查询生产步骤要用的设备 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "findProductCos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductCos() throws Exception {
		String id = getParameterFromRequest("id");
		String stepNum = getParameterFromRequest("stepNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

			List<CellproductCos> ccs = cellPassageService.getCellproductCoss(id, stepNum);
			if (!ccs.isEmpty()) {
				for (int j = 0; j < ccs.size(); j++) {
					Map<String, Object> result = new HashMap<String, Object>();
					result.put("instrumentName", ccs.get(j).getInstrumentName());
					result.put("id", ccs.get(j).getId());
					if (ccs.get(j).getCellProduc() != null) {
						if (ccs.get(j).getCellProduc().getTempleCell() != null) {
							result.put("producingTitle",
									ccs.get(j).getCellProduc().getTempleCell().getProducingTitle());
						}
					}
					if (ccs.get(j).getTemplateCos() != null) {
						result.put("typeName", ccs.get(j).getTemplateCos().getTypeName());
						result.put("templateCosId", ccs.get(j).getTemplateCos().getId());
						result.put("templateCosTypeId", ccs.get(j).getTemplateCos().getTypeId());
					}
					result.put("instrumentCode", ccs.get(j).getInstrumentCode());
					result.put("operationState", ccs.get(j).getOperationState());
					list.add(result);
				}
			
			}
			map.put("CellproductCoss", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	/**
	 * 
	 * @Title: @Description: 查询生产观察细胞 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "findCellPassageObservations", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findCellPassageObservations() throws Exception {
		String id = getParameterFromRequest("id");
		String stepNum = getParameterFromRequest("stepNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<CellPassageObservation> ccs = cellPassageService.getCellPassageObservations(id, stepNum);
			map.put("CellPassageObservations", ccs);
			//查询是否显示 细胞观察
			//查询是否显示 细胞计数（只有bp环节有细胞计数）
			List<CellProductionRecord> crs = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(id, stepNum);
			if(crs.size()>0) {
				CellProductionRecord cr = crs.get(0);
				if("0".equals(cr.getTempleItem().getCellObservation())) {
					map.put("CellObservation", false);
				}else if("1".equals(cr.getTempleItem().getCellObservation())) {
					map.put("CellObservation", true);
				}
				if("0".equals(cr.getTempleItem().getReinfusionPlan())) {
					map.put("xbjs", false);
				}else if("1".equals(cr.getTempleItem().getReinfusionPlan())) {
					map.put("xbjs", true);
				}
			}else {
				map.put("CellObservation", false);
				map.put("xbjs", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: @Description: 查询试剂初始量
	 *         void @throws
	 */
	@Action(value = "findProductReagent", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductReagent() throws Exception {
		String id = getParameterFromRequest("id");
		String stepNum = getParameterFromRequest("stepNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<CellPassageBeforeReagent> ccs = cellPassageService.getCellPassageBeforeReagents(id, stepNum);
			map.put("CellproductReagent", ccs);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: @Description: 根据质检结果id查询质检结果表 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "findQualityInfoByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String findQualityInfoByid() {
		String infoid = getParameterFromRequest("infoid");
		String id = "";
		String sczj="sc";
		if (infoid != null && !"".equals(infoid)) {
			QualityTestInfo qti = commonDAO.get(QualityTestInfo.class, infoid);
			if (qti != null) {
				id = qti.getQualityTest().getId();
			}
		} else {

		}
		return redirect("/experiment/quality/qualityTest/showQualityTestResultTable.action?id=" + id+"&falg=false"+"&sczj=sczj");
	}

	/**
	 * 
	 * @Title: uploadCsvFile @Description: 上传结果 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "bringResult")
	public void bringResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			cellPassageService.bringResult(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: generateBlendCode @Description: 生成混合号 @author : shengwei.wang @date
	 *         2018年3月6日上午11:02:26 void @throws
	 */
	@Action(value = "generateBlendCode")
	public void generateBlendCode() {
		String id = getParameterFromRequest("id");
		try {
			Integer blendCode = cellPassageService.generateBlendCode(id);
			HttpUtils.write(String.valueOf(blendCode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @throws Exception
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : shengwei.wang @date
	 *         2018年3月22日下午5:39:40 void @throws
	 */
	@Action(value = "submitSample")
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			cellPassageService.submitSample(id, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: downLoadTemp @Description: TODO(核算提取模板下载) @param @param
	 *         ids @param @throws Exception    设定文件 @return void    返回类型 @author
	 *         zhiqiang.yang@biolims.cn @date 2017-8-22 上午11:46:55 @throws
	 */
	@Action(value = "downLoadTemp")
	public void downLoadTemp() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids");
		String[] codes = getRequest().getParameterValues("codes");
		String id = ids[0];
		String code = codes[0];
		String[] str1 = code.split(",");
		String co = str1[0];
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = co + sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		properties.load(is);

		String filePath = properties.getProperty("result.template.file") + "\\";// 写入csv路径
		String fileName = filePath + a + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "样本编号", "原始样本编号", "产物类型", "检测项目", "浓度", "体积" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		String[] sid = id.split(",");
		for (int j = 0; j < sid.length; j++) {
			String idc = sid[j];
			for (int i = 0; i < ids.length; i++) {
				CellPassageInfo sr = cellPassageService.getInfoById(idc);
				StringBuffer sb = new StringBuffer();
				setMolecularMarkersData(sr, sb);
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
				if (sr.equals("")) {
					result.put("success", false);
				} else {
					result.put("success", true);

				}
			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3New(a, filePath);
	}

	@Action(value = "findCellPassageTemplateList")
	public void findCellPassageTemplateList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String ids = getParameterFromRequest("id");

		Map<String, Object> result = new HashMap<String, Object>();
		result = cellPassageService.findCellPrimaryCultureTemplateList(start, length, query, col, sort, ids);
		List<CellPassageTemplate> list = (List<CellPassageTemplate>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("cellPassage-id", "");
		map.put("endTime", "");
		map.put("startTime", "");
		map.put("contentData", "");
		map.put("content", "");
		map.put("code", "");
		map.put("note", "");
		map.put("orderNum", "");
		map.put("estimatedDate", "");
		map.put("zjResult", "");
		map.put("testUserList", "");
		map.put("planEndDate", "");
		map.put("planWorkDate", "");
		map.put("delay", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData @Description: TODO(将对应的值添加到模板里) @param @param
	 *         sr @param @param sb @param @throws Exception    设定文件 @return void   
	 *         返回类型 @author zhiqiang.yang@biolims.cn @date 2017-8-22
	 *         下午1:21:58 @throws
	 */
	public void setMolecularMarkersData(CellPassageInfo sr, StringBuffer sb) throws Exception {
		if (null != sr.getCode()) {
			sb.append("\"").append(sr.getCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null != sr.getSampleCode()) {
			sb.append("\"").append(sr.getSampleCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}

		if (null != sr.getDicSampleType().getName()) {
			sb.append("\"").append(sr.getDicSampleType().getName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}

		if (null != sr.getProductName()) {
			sb.append("\"").append(sr.getProductName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
	}

	/**
	 * 
	 * @Title: toUtf8String @Description: TODO(解决乱码) @param @param
	 *         s @param @return    设定文件 @return String    返回类型 @author
	 *         zhiqiang.yang@biolims.cn @date 2017-8-23 下午4:40:07 @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}

	/**
	 * 选择房间
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selectRoomTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectRoomTable() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/roomManagement/selectRoomTable.jsp");
	}

	@Action(value = "selectRoomTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectRoomTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = roomManagementService.selectRoomTableJson(start, length, query, col, sort);
		List<RoomManagement> list = (List<RoomManagement>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("roomName", "");
		map.put("roomState", "");
		map.put("createUser-name", "");
		map.put("regionalManagement-name", "");
		map.put("isFull", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	@Action(value = "selectRoomTableAllJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectRoomTableAllJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = roomManagementService.selectRoomTableAllJson(start, length, query, col, sort,id);
		List<RoomManagement> list = (List<RoomManagement>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("roomName", "");
		map.put("roomState", "");
		map.put("createUser-name", "");
		map.put("regionalManagement-name", "");
		map.put("isFull", "");
		map.put("centiare", "");
		map.put("stere", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	/**
	 * 
	 * @Title: selectCos @Description: 选择设备 @author : shengwei.wang @date
	 *         2018年1月26日上午11:56:18 @return @throws Exception String @throws
	 */
	@Action(value = "selectCos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectCos() throws Exception {
		String typeId = getParameterFromRequest("typeId");
		String roomName = URLDecoder.decode(getParameterFromRequest("roomName"), "UTF-8");
		putObjToContext("typeId", typeId);
		putObjToContext("roomName", roomName);
		return dispatcher("/WEB-INF/page/system/template/cosTemplateDialog.jsp");

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CellPassageService getCellPassageService() {
		return cellPassageService;
	}

	public void setCellPassageService(CellPassageService cellPassageService) {
		this.cellPassageService = cellPassageService;
	}

	public CellPassage getCellPassage() {
		return cellPassage;
	}

	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}

	public CellPassageDao getCellPassageDao() {
		return cellPassageDao;
	}

	public void setCellPassageDao(CellPassageDao cellPassageDao) {
		this.cellPassageDao = cellPassageDao;
	}

	
	@Action(value = "serchInfos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void serchInfos() throws Exception {
		String mainId = getParameterFromRequest("mainId");
		String orderNums1 = getParameterFromRequest("orderNums1");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = cellPassageService.serchInfos(mainId, orderNums1);
			map.put("sueccess", true);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "selectDate")
	public void selectDate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		CellPassage cellPassage =new CellPassage();
		cellPassage =cellPassageService.selectDate(id);
		
		String createDate =cellPassage.getCreateDate();
		map.put("da",createDate);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	/**
	 *  扫码批次信息
	 * @throws Exception
	 */
	@Action(value = "checkReagent")
	public void checkReagent() throws Exception {
		String pici = getParameterFromRequest("pici");
		String sjid = getParameterFromRequest("sjid");
		String cellPassageId = getParameterFromRequest("cellPassageId");
		String orderNum = getParameterFromRequest("orderNum");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//判断是否出库
			//判断是否在有效期
			//判断培养基是否是当前登陆人
			
			CellPassageBeforeReagent cr = commonDAO.get(CellPassageBeforeReagent.class, sjid);
			if(cr!=null&&cr.getReagentId()!=null&&!"".equals(cr.getReagentId())) {
				Storage st = commonDAO.get(Storage.class, cr.getReagentId());
				if(st!=null) {
					
					List<StorageReagentBuySerial> strs = cellPassageService.getfindStorageReagentBuySerials(st,pici);
					
					if(strs.size()>0) {
						//试剂是否存在
						map.put("sjcz", true);
//							StorageReagentBuySerial srbs = 
						List<StorageOutItem> sois = cellPassageService.getfindStorageOutItems(st,pici);
						if(sois.size()>0) {
							//试剂是否出库
							map.put("sjck", true);
							if(sois.get(0).getExpireDate()!=null) {
								//试剂是否维护过期日期
								map.put("gqrq", true);
								Date startTime= df.parse(df.format(sois.get(0).getExpireDate()));

								Date endTime= df.parse(df.format(new Date()));

								if (startTime.getTime() > endTime.getTime()) {
									//试剂是否失效
									map.put("yxq", true);
									if(st.getMedium()!=null
											&&!"".equals(st.getMedium())
											&&"1".equals(st.getMedium())) {
										//是否是培养基
										map.put("pyj", true);
										if(sois.get(0).getProductMark()!=null
												&&!"".equals(sois.get(0).getProductMark())
												&&cr.getCellPassage().getBatch().equals(sois.get(0).getProductMark())) {
											//该培养基是否是当前人员的
											map.put("pyjry", true);
											cellPassageService.saveCellPassageBeforeReagent(st,pici,sois.get(0),cr,cellPassageId,orderNum,"0");
											
											map.put("reagentNo", pici);
											map.put("reagentValidityTime", df.format(sois.get(0).getExpireDate()));
										}else if("".equals(sois.get(0).getProductMark())) {
											
											List<StorageOutItem> soiis = cellPassageService.getfindStorageOutItems1(st,cr.getCellPassage().getBatch());
											if(soiis.size()>0) {
												map.put("pyjry", false);
												map.put("note",st.getName()+",批号："+pici+",与当前实验不符！");
											}else {
												//该培养基是否是当前人员的
												map.put("pyjry", true);
												cellPassageService.saveCellPassageBeforeReagent(st,pici,sois.get(0),cr,cellPassageId,orderNum,"1");
												
												map.put("reagentNo", pici);
												map.put("reagentValidityTime", df.format(sois.get(0).getExpireDate()));
											}
										}else {
											map.put("pyjry", false);
											map.put("note",st.getName()+",批号："+pici+",与当前实验不符！");
										}
									}else {
										map.put("pyj", false);
										cellPassageService.saveCellPassageBeforeReagent(st,pici,sois.get(0),cr,cellPassageId,orderNum,"0");
										map.put("reagentNo", pici);
										map.put("reagentValidityTime", df.format(sois.get(0).getExpireDate()));
									}
								}else {
									map.put("yxq", false);
									map.put("note",st.getName()+",批号："+pici+"已失效！");
								}
							}else {
								map.put("gqrq", false);
								map.put("note",st.getName()+",批号："+pici+"失效期未维护，请联系管理员！");
							}
						}else {
							map.put("sjck", false);
							map.put("note",st.getName()+",批号："+pici+"未出库！");
						}
					}else {
						map.put("sjcz", false);
						map.put("note",st.getName()+",不存在批号"+pici+"的物料！");
					}
					

				}
			}
			map.put("sueccess", true);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	
	//
	@Action(value = "downloadCsvFileTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadCsvFileTwo() throws Exception {
		String mainTable_id = getParameterFromRequest("id");
		String sample = getParameterFromRequest("sampleId");

		
		String sampleId="";
		List<SampleOrder> findSampleOrders = cellPassageDao.findSampleOrders(sample);
		if(!findSampleOrders.isEmpty()) {
			for (SampleOrder sampleOrder : findSampleOrders) {
				sampleId=sampleOrder.getId();

			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");

		properties.load(is);

		String filePath = properties.getProperty("sampleReceiveItemZhiJian.path") + "\\";// 写入csv路径
		String fileName = filePath + "sampleReceiveItemZhiJian" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "质检条码" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();

		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, sampleId);
		String codes = "";
		if (sampleOrder != null) {
			String productId = sampleOrder.getProductId();
			if (!"".equals(productId) && productId != null) {
				Product product = commonDAO.get(Product.class, productId);
				if (product != null) {
					if (product.getTemplate() != null) {
						Template template = commonDAO.get(Template.class, product.getTemplate().getId());
						Double stepsNum = template.getStepsNum();
						DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
						String format = decimalFormat.format(stepsNum);
						int stepsN = Integer.parseInt(format);

						for (int i = 1; i <= stepsN; i++) {
							List<TemplateItem> tpi = cellPassageDao.getTemplateItems(template, String.valueOf(i));

							String printLabelCoding = tpi.get(0).getPrintLabelCoding();
							List<TempleProducingCell> templeProductingCell = cellPassageDao
									.getTempleProductingCell(template.getId(), String.valueOf(i));

							if (templeProductingCell != null) {
								String s = "";
								for (TempleProducingCell templeProducingCell : templeProductingCell) {
									if ("".equals(s)) {
										s += "'" + templeProducingCell.getId() + "'";
									} else {
										s += "," + "'" + templeProducingCell.getId() + "'";
									}

								}
								String aa = "in (" + s + ")";
								List<ZhiJianItem> findCellZhiJianAll = cellPassageDao.findCellZhiJianAll(aa,
										String.valueOf(i));
								for (ZhiJianItem zhiJianItem : findCellZhiJianAll) {
									List<ZhiJianItem> zhiJian = cellPassageDao.findZhiJianNewById(aa,
											zhiJianItem.getCode(), String.valueOf(i));
									for (int j = 1; j <= zhiJian.size(); j++) {

										SampleDeteyion sd = commonDAO.get(SampleDeteyion.class,
												zhiJian.get(j - 1).getCode());
										DicSampleType dd = commonDAO.get(DicSampleType.class,
												zhiJian.get(j - 1).getSampleTypeId());

										// 质检条形码
										// 姓名缩写-筛选好-监测项目-随机号-采血轮次-生产工序-监测项缩写
										codes = sampleOrder.getAbbreviation() + "-" + sampleOrder.getFiltrateCode()
												+ "-" + sampleOrder.getProductId() + "-" + sampleOrder.getRandomCode()
												+ "-" + sampleOrder.getRound() + "-" + printLabelCoding + "-"
												+ sd.getDeteyionName() + "-" + j;
										StringBuffer sb = new StringBuffer();
										setMolecularMarkersData(codes, sb);
										String rowStr = sb.toString();
										csvWtriter.write(rowStr);
										csvWtriter.newLine();
										result.put("success", true);
									}
								}
							}
						}

					}
				}

			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		downLoadTemp3New("sampleReceiveItemZhiJian", filePath);
	}
	
	
	/**
	 * 明细下载CSV模板文件 @Title: downloadCsvFile
	 * 
	 * @throws
	 */
	@Action(value = "downloadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadCsvFile() throws Exception {
		String mainTable_id = getParameterFromRequest("id");
		String sample = getParameterFromRequest("sampleId");
		String sampleId="";
		List<SampleOrder> findSampleOrders = cellPassageDao.findSampleOrders(sample);
		if(!findSampleOrders.isEmpty()) {
			for (SampleOrder sampleOrder : findSampleOrders) {
				sampleId=sampleOrder.getId();

			}
		}

		Map<String, Object> result = new HashMap<String, Object>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");

		properties.load(is);

		String filePath = properties.getProperty("sampleReceiveItem.path") + "\\";// 写入csv路径
		String fileName = filePath + "sampleReceiveItem" + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "生产条码"};
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();

		SampleOrder sampleOrder = commonDAO.get(SampleOrder.class, sampleId);
		String codes = "";

		if (sampleOrder != null) {
			String productId = sampleOrder.getProductId();
			if (!"".equals(productId) && productId != null) {
				Product product = commonDAO.get(Product.class, productId);
				if (product != null) {
					if (product.getTemplate() != null) {
						Template template = commonDAO.get(Template.class, product.getTemplate().getId());
						Double stepsNum = template.getStepsNum();
						DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
						String format = decimalFormat.format(stepsNum);
						int stepsN = Integer.parseInt(format);
						for (int i = 1; i <= stepsN; i++) {

							List<TemplateItem> tpi = cellPassageDao.getTemplateItems(template, String.valueOf(i));

							String printLabelCoding = tpi.get(0).getPrintLabelCoding();
//									if (sos.size() > 0 && tis.size() > 0) {
							codes = sampleOrder.getAbbreviation() + "-" + sampleOrder.getFiltrateCode() + "-"
									+ sampleOrder.getProductId() + "-" + sampleOrder.getRandomCode() + "-"
									+ sampleOrder.getRound() + "-" + printLabelCoding;

							StringBuffer sb = new StringBuffer();
							setMolecularMarkersData(codes, sb);
							String rowStr = sb.toString();
							csvWtriter.write(rowStr);
							csvWtriter.newLine();
							result.put("success", true);

						}
					}

				}
			}
		}
//				// 质检条形码
//				// 姓名缩写-筛选好-监测项目-随机号-采血轮次-生产工序-监测项缩写
//				if (sos.size() > 0 && tis.size() > 0) {
//					productCodes = sos.get(0).getAbbreviation() + "-" + sos.get(0).getFiltrateCode()
//							+ "-" + sos.get(0).getProductId() + "-" + sos.get(0).getRandomCode() + "-"
//							+ sos.get(0).getRound() + "-" + printLabelCoding + "-"
//							+ sd.getDeteyionName() + "-" + i;
//				}
//				for (int i = 0; i < list.size(); i++) {
//					StringBuffer sb = new StringBuffer();
//					setMolecularMarkersData(list.get(i), sb);
//					String rowStr = sb.toString();
//					csvWtriter.write(rowStr);
//					csvWtriter.newLine();
//					result.put("success", true);
//
//			}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3("sampleReceiveItem", filePath);
	}
	
	
	public void setMolecularMarkersData(String codes, StringBuffer sb) throws Exception {
		if (!"".equals(codes) && codes != null) {
			sb.append("\"").append(codes).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}

	}
	
	@Action(value = "downLoadTemp3New")
	public void downLoadTemp3New(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8StringNew(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}
	/**
	 * 
	 * @Title: toUtf8String @Description: 解决乱码问题 @author qi.yan @date
	 *         2018-12-24上午11:55:11 @param s @return String @throws
	 */
	public static String toUtf8StringNew(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}
	
	
	//
	public CleanAreaMicroorganism getCam() {
		return cam;
	}

	public void setCam(CleanAreaMicroorganism cam) {
		this.cam = cam;
	}

	public CleanAreaMicrobe getDif1() {
		return dif1;
	}

	public void setDif1(CleanAreaMicrobe dif1) {
		this.dif1 = dif1;
	}

	public CleanAreaVolume getDif2() {
		return dif2;
	}

	public void setDif2(CleanAreaVolume dif2) {
		this.dif2 = dif2;
	}
	public CleanAreaBacteria getDif3() {
		return dif3;
	}

	public void setDif3(CleanAreaBacteria dif3) {
		this.dif3 = dif3;
	}

	public CleanAreaDiffPressure getCad() {
		return cad;
	}

	public void setCad(CleanAreaDiffPressure cad) {
		this.cad = cad;
	}
	
	
	
	
}
