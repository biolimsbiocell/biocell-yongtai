package com.biolims.experiment.cell.passage.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.views.xslt.StringAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageBeforeReagent;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageItemCheckCode;
import com.biolims.experiment.cell.passage.model.CellPassagePreparat;
import com.biolims.experiment.cell.passage.model.CellPassagePreparatEnd;

import com.biolims.experiment.cell.passage.model.CellPassageTemp;


import com.biolims.experiment.cell.passage.model.CellPassageRoomPreparat;

import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProducResults;
import com.biolims.experiment.cell.passage.model.CellProductionCompleted;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellZhiJian;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.cell.passage.model.CellproductReagent;
import com.biolims.experiment.cell.passage.service.CellPassageNewService;
import com.biolims.experiment.cell.passage.service.CellPassageService;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.file.service.FileInfoService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oracle.net.aso.u;

@Namespace("/experiment/cell/passage/cellPassageNow")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class CellPassageNowAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private CellPassageService cellPassageService;// 以前老的
	@Autowired
	private CellPassageNewService cellPassageNewService;// 新的
	private CellPassage cellPassage = new CellPassage();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CellPassageDao cellPassageDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 
	 * @Title: showCellPassageSteps @Description: 转发到新的生产页面 @author : @date
	 *         2019-03-21 @return @throws Exception String @throws
	 */
	@Action(value = "showCellPassageEdit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCellPassageEdit() throws Exception {
		String id = getParameterFromRequest("id");
		cellPassage = cellPassageService.get(id);
		String orderNumBy = getParameterFromRequest("orderNum");
		putObjToContext("orderNumBy", orderNumBy);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellPassageSteps.jsp");
	}

	@Action(value = "showCellPassage", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCellPassage() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/cell/passage/cellpassageShowNew.jsp");
	}

	/* 细胞生成主表列表 */
	@Action(value = "showCellPassageJson")
	public void showCellPassageJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cellPassageService.showCellPassageJson(start, length, query, col, sort);
			List<CellPassage> list = (List<CellPassage>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("stateName", "");
			map.put("batch", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: 获取实验步骤 @Description: @author @date @return @throws Exception
	 *         String @throws
	 */
	@Action(value = "findCellNumbers")
	public void findCellNumbers() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			CellPassage cellPassage = cellPassageService.get(id);
			Template template = templateService.get(cellPassage.getTemplate().getId());
			map.put("template", template);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: saveAllot @Description: 保存任务分配页面 @author : @date @throws
	 */
	@Action(value = "saveAllotNow")
	public void saveAllotNow() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo = getParameterFromRequest("logInfo");
		String place = getParameterFromRequest("place");
		String infection = getParameterFromRequest("infection");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = cellPassageNewService.saveAllot(main, tempId, userId, templateId, logInfo, place, infection);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取左邊步骤号 @author : @date @throws
	 */
	@Action(value = "findTempleItemNumber")
	public void findTempleItemNumber() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			JSONArray jsonArray = new JSONArray();
			CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
			if (cellPassage != null) {
				// 新修改方法 从当前模块的模板拿数据
				List<CellPassageTemplate> temItemList = templateService
						.getCellPassageTemplateListPage(cellPassage.getId());
				if (temItemList.size() > 0) {
					for (int j = 0; j < temItemList.size(); j++) {
						map.put("id", temItemList.get(j).getId());
						map.put("code", ""/* temItemList.get(j).getCode() */);
						map.put("orderNum", temItemList.get(j).getOrderNum());
						map.put("name", temItemList.get(j).getName());
						map.put("today", temItemList.get(j).getEstimatedDate());
						if (temItemList.get(j).getPlanWorkDate() != null
								&& !"".equals(temItemList.get(j).getPlanWorkDate())) {
							map.put("startTime", temItemList.get(j).getPlanWorkDate().substring(0, 10));
						} else {
							map.put("startTime", "");
						}
						jsonArray.add(map);
					}
//					result.put("list", temItemList);
				}

//				// 通过模板主表id查询 模板明细数据
//				List<TemplateItem> temItemList = templateService
//						.getTemplateItemListPage(cellPassage.getTemplate().getId());
//				if (temItemList.size() > 0) {
//					for (int j = 0; j < temItemList.size(); j++) {
//						map.put("id", temItemList.get(j).getId());
//						map.put("code", temItemList.get(j).getCode());
//						map.put("orderNum", temItemList.get(j).getOrderNum());
//						map.put("name", temItemList.get(j).getName());
//						map.put("today", temItemList.get(j).getEstimatedTime());
//						jsonArray.add(map);
//					}
//					result.put("list", temItemList);
//				}
			}
			result.put("success", true);
			result.put("data", jsonArray);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取工前操作 指令实验模板数据 @author :
	 * @date @throws
	 */
	@Action(value = "findNumberNstructionsListinfo")
	public void findNumberNstructionsListinfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> scjz = new HashMap<String, Object>();

			Map<String, Object> map = new HashMap<String, Object>();
//			Map<String, Object> mapto = new HashMap<String, Object>();
			CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
			if (cellPassage != null) {
				// 通过模板主表id查询 模板明细数据
				List<TemplateItem> temItemList = templateService.getTemplateItem(cellPassage.getTemplate().getId(),
						orderNum);
				if (temItemList.size() > 0) {
					for (int j = 0; j < temItemList.size(); j++) {
						map.put("content", temItemList.get(j).getContent().toString().replaceAll("\'", "\""));
						map.put("id", temItemList.get(j).getId());
					}
				}
			}
//			// 获取第一步生產数据 指令
//			List<CellPassagePreparat> cellPass = cellPassageNewService.findCellNstructionsInfo(id, orderNum);
			JSONArray jsonArray = new JSONArray();
			// 获取第一步生產数据 指令
			List<CellPassagePreparat> cellPass = cellPassageNewService.findCellNstructionsInfo(id, orderNum);
			if (!cellPass.isEmpty()) {
				for (int j = 0; j < cellPass.size(); j++) {
					Map<String, Object> mapCellPassagePreparat = new HashMap<String, Object>();

					if (cellPass.get(j).getTempleInsId() != null) {
						mapCellPassagePreparat.put("sort", cellPass.get(j).getTempleInsId().getSort());
						mapCellPassagePreparat.put("name", cellPass.get(j).getTempleInsId().getName());

					}
					mapCellPassagePreparat.put("productionInspection", cellPass.get(j).getProductionInspection());
					if (cellPass.get(j).getOperationNotes() != null) {
						mapCellPassagePreparat.put("operationNotes", cellPass.get(j).getOperationNotes());

					} else {
						mapCellPassagePreparat.put("operationNotes", "");

					}
					mapCellPassagePreparat.put("id", cellPass.get(j).getId());
					jsonArray.add(mapCellPassagePreparat);
				}

			}
			// 获取操作指令
//			List<TempleNstructions> temNstr = templateService
//					.findTemNstructionsListPage(cellPassage.getTemplate().getId(), orderNum);
			// 获取实验数据 生产检查
			List<CellProductionRecord> cellReco = cellPassageNewService.findProductionRecordInfo(id, orderNum);
//			result.put("cellPass", cellPass);
			result.put("cellPass", jsonArray);

			result.put("cellReco", cellReco.get(0));
			result.put("templeContent", cellReco.get(0).getTempleItem().getContent());
			result.put("cellRecoContent", cellReco.get(0).getContent());
			result.put("cellRecoId", cellReco.get(0).getId());
			if (cellReco.size() > 0) {
				CellProductionRecord scjzsj = cellReco.get(0);
				if (scjzsj.getTempleOperator() != null) {
					scjz.put("templeOperator", scjzsj.getTempleOperator().getId());
					scjz.put("templeOperator-name", scjzsj.getTempleOperator().getName());
				} else {
					scjz.put("templeOperator", "");
					scjz.put("templeOperator-name", "");
				}

				if (scjzsj.getTempleReviewer() != null) {
					scjz.put("templeReviewer", scjzsj.getTempleReviewer().getId());
					scjz.put("templeReviewer-name", scjzsj.getTempleReviewer().getName());
				} else {
					scjz.put("templeReviewer", "");
					scjz.put("templeReviewer-name", "");
				}

				if (scjzsj.getQaTempleReviewer() != null) {
					scjz.put("qaTempleReviewer", scjzsj.getQaTempleReviewer().getId());
					scjz.put("qaTempleReviewer-name", scjzsj.getQaTempleReviewer().getName());
				} else {
					scjz.put("qaTempleReviewer", "");
					scjz.put("qaTempleReviewer-name", "");
				}

				if (scjzsj.getOperatingRoomName() != null) {
					scjz.put("operatingRoomName", scjzsj.getOperatingRoomName());
					scjz.put("operatingRoomId", scjzsj.getOperatingRoomId());
				} else {
					scjz.put("operatingRoomName", "");
					scjz.put("operatingRoomId", "");
				}
			}

			
			
			result.put("scjz", scjz);
			
			
			// 获取生产明细表数据
			List<CellPassageItemCheckCode> cpis = cellPassageNewService.findCellPassageItemCheckCodes(id, orderNum,
					"0");
			JSONArray cpi = new JSONArray();
			if (!cpis.isEmpty() && cpis.size() > 0) {
				for (int z = 0; z < cpis.size(); z++) {
					Map<String, Object> mapCellPassageItemCheckCode = new HashMap<String, Object>();

					mapCellPassageItemCheckCode.put("id", cpis.get(z).getId());
					mapCellPassageItemCheckCode.put("code", cpis.get(z).getCode());
					mapCellPassageItemCheckCode.put("state", cpis.get(z).getState());
					cpi.add(mapCellPassageItemCheckCode);
				}
				result.put("checkCodeBefore", cpi);
			} else {
				result.put("checkCodeBefore", "");
			}

			// 获取生产明细表数据
			List<CellPassageItemCheckCode> cpis2 = cellPassageNewService.findCellPassageItemCheckCodes(id, orderNum,
					"1");
			JSONArray cpi2 = new JSONArray();
			if (!cpis2.isEmpty() && cpis2.size() > 0) {
				for (int z = 0; z < cpis2.size(); z++) {
					Map<String, Object> mapCellPassageItemCheckCode = new HashMap<String, Object>();

					mapCellPassageItemCheckCode.put("id", cpis2.get(z).getId());
					mapCellPassageItemCheckCode.put("code", cpis2.get(z).getCode());
					mapCellPassageItemCheckCode.put("state", cpis2.get(z).getState());
					cpi2.add(mapCellPassageItemCheckCode);
				}
				result.put("checkCodeAfter", cpi2);
			} else {
				result.put("checkCodeAfter", "");
			}

			
			
			
			JSONArray jsonArrayRoom = new JSONArray();
			// 获取第一步房间监控记录
			List<CellPassageRoomPreparat> cellRoomPass = cellPassageNewService.findCellPassageRoomPreparats(id, orderNum);
			if (!cellRoomPass.isEmpty()) {
				for (int j = 0; j < cellRoomPass.size(); j++) {
					Map<String, Object> mapCellPassageRoomPreparat = new HashMap<String, Object>();

					mapCellPassageRoomPreparat.put("orderNum", cellRoomPass.get(j).getOrderNum());
					mapCellPassageRoomPreparat.put("orderTypeNum", cellRoomPass.get(j).getOrderTypeNum());
					mapCellPassageRoomPreparat.put("state", cellRoomPass.get(j).getState());
					mapCellPassageRoomPreparat.put("id", cellRoomPass.get(j).getId());
					
					jsonArrayRoom.add(mapCellPassageRoomPreparat);
				}

			}
			result.put("cellRoomPass", jsonArrayRoom);
			//查询房间监测记录  反馈房间监测记录房间状态
			List<CleanAreaDustItem> item1s = cellPassageNewService.findCleanAreaDustItems(cellReco.get(0).getOperatingRoomId(),cellReco.get(0).getPlanWorkDate());
			if(item1s.size()>0) {
				//查询不合格  没有不合格即合格
				List<CleanAreaDustItem> item11s = cellPassageNewService.findCleanAreaDustItem1s(cellReco.get(0).getOperatingRoomId(),item1s.get(0).getCleanAreaDust().getId());
				if(item11s.size()>0) {
					result.put("room1state", false);
				}else {
					result.put("room1state", true);
				}
			}else {
				result.put("room1state", false);
			}
			List<CleanAreaMicrobeItem> item2s = cellPassageNewService.findCleanAreaMicrobeItems(cellReco.get(0).getOperatingRoomId(),cellReco.get(0).getPlanWorkDate());
			if(item2s.size()>0) {
				List<CleanAreaMicrobeItem> item22s = cellPassageNewService.findCleanAreaMicrobeItem1s(cellReco.get(0).getOperatingRoomId(),item2s.get(0).getCleanAreaMicrobe().getId());
				if(item22s.size()>0) {
					result.put("room2state", false);
				}else {
					result.put("room2state", true);
				}
			}else {
				result.put("room2state", false);
			}
			List<CleanAreaVolumeItem> item3s = cellPassageNewService.findCleanAreaVolumeItems(cellReco.get(0).getOperatingRoomId(),cellReco.get(0).getPlanWorkDate());
			if(item3s.size()>0) {
				List<CleanAreaVolumeItem> item33s = cellPassageNewService.findCleanAreaVolumeItem1s(cellReco.get(0).getOperatingRoomId(),item3s.get(0).getCleanAreaVolume().getId());
				if(item33s.size()>0) {
					result.put("room3state", false);
				}else {
					result.put("room3state", true);
				}
			}else {
				result.put("room3state", false);
			}
			List<CleanAreaBacteriaItem> item4s = cellPassageNewService.findCleanAreaBacteriaItems(cellReco.get(0).getOperatingRoomId(),cellReco.get(0).getPlanWorkDate());
			if(item4s.size()>0) {
				List<CleanAreaBacteriaItem> item44s = cellPassageNewService.findCleanAreaBacteriaItem1s(cellReco.get(0).getOperatingRoomId(),item4s.get(0).getCleanAreaBacteria().getId());
				if(item44s.size()>0) {
					result.put("room4state", false);
				}else {
					result.put("room4state", true);
				}
			}else {
				result.put("room4state", false);
			}
			List<CleanAreaMicroorganismItem> item5s = cellPassageNewService.findCleanAreaMicroorganismItems(cellReco.get(0).getOperatingRoomId(),cellReco.get(0).getPlanWorkDate());
			if(item5s.size()>0) {
				List<CleanAreaMicroorganismItem> item55s = cellPassageNewService.findCleanAreaMicroorganismItem1s(cellReco.get(0).getOperatingRoomId(),item5s.get(0).getCleanAreaMicroorganism().getId());
				if(item55s.size()>0) {
					result.put("room5state", false);
				}else {
					result.put("room5state", true);
				}
			}else {
				result.put("room5state", false);
			}
			List<CleanAreaDiffPressureItem> item6s = cellPassageNewService.findCleanAreaDiffPressureItems(cellReco.get(0).getOperatingRoomId(),cellReco.get(0).getPlanWorkDate());
			if(item6s.size()>0) {
				List<CleanAreaDiffPressureItem> item66s = cellPassageNewService.findCleanAreaDiffPressureItem1s(cellReco.get(0).getOperatingRoomId(),item6s.get(0).getCleanAreaDiffPressure().getId());
				if(item66s.size()>0) {
					result.put("room6state", false);
				}else {
					result.put("room6state", true);
				}
			}else {
				result.put("room6state", false);
			}
			
			
			
			// result.put("temNstr", temNstr);
			// result.put("content", map);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取质检信息 @author :
	 * @date @throws
	 */
	@Action(value = "findZhijianItemInfo")
	public void findZhijianItemInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String stepId = getParameterFromRequest("stepId");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
//			if (cellPassage != null) {
//				// 通过模板主表id查询 模板明细数据
//				List<ZhiJianItem> temItemList = templateService.getZjItem(cellPassage.getTemplate().getId(),
//						bianhuan(orderNum));
//				TemplateItem cpt = cellPassageNewService.getBlendStateByStep(cellPassage.getTemplate().getId(),
//						bianhuan(orderNum));
//				result.put("temItemList", temItemList);
//				result.put("cpt", cpt);
//			}
			if (cellPassage != null) {
				// 通过模板主表id查询 模板明细数据
				List<ZhiJianItem> temItemList = templateService.getZjItem(cellPassage.getTemplate().getId(),
						bianhuan(orderNum));
				TemplateItem cpt = cellPassageNewService.getBlendStateByStep(cellPassage.getTemplate().getId(),
						bianhuan(orderNum));
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

				if (cpt != null) {
					result.put("cpt", cpt.getBlend());

				}
				if (!temItemList.isEmpty()) {
					for (int j = 0; j < temItemList.size(); j++) {
						Map<String, Object> map = new HashMap<String, Object>();

						map.put("typeName", temItemList.get(j).getTypeName());
						map.put("typeId", temItemList.get(j).getTypeId());
						map.put("name", temItemList.get(j).getName());
						map.put("id", temItemList.get(j).getId());
						map.put("code", temItemList.get(j).getCode());
						map.put("nextId", temItemList.get(j).getNextId());
						map.put("itemId", temItemList.get(j).getItemId());
						list.add(map);
					}
				}
				result.put("temItemList", list);

//				result.put("cpt", cpt);
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取BL明细<试剂 设备>操作 生产结果 以及生产开始时间和生产结束时间 @author
	 *         :
	 * @date @throws
	 */
	@Action(value = "findCosAndReagentInfo")
	public void findCosAndReagentInfo() throws Exception {
		System.out.println(new Date() + "开始");
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapJson = new HashMap<String, Object>();
		JSONArray jsonArray = new JSONArray();
		JSONArray jsonRes = new JSONArray();
		JSONArray jsonReagen = new JSONArray();
		JSONArray jsonCos = new JSONArray();
		JSONArray jsonZhijian = new JSONArray();

		JSONArray jsontem = new JSONArray();

		try {
			CellProductionRecord cellProductionRecord = cellPassageDao.findStepNumOrValue(id, orderNum);
			if (cellProductionRecord != null) {
				if (cellProductionRecord.getEndTime() != "" && cellProductionRecord.getEndTime() != null) {
					result.put("haveEndTime", true);
				} else {
					result.put("haveEndTime", false);

				}
			}
			CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
			if (cellPassage != null) {
				// 通过模板主表id查询 模板明细数据 主表
				List<TempleProducingCell> temProduc = templateService
						.getTempleProducingCellListPage(cellPassage.getTemplate().getId(), bianhuan(orderNum));
				System.out.println(new Date() + "第一循环开始");

				//////////////////////////
				String s = "";
				for (TempleProducingCell templeProducingCell : temProduc) {
					s += "'" + templeProducingCell.getId() + "'" + ",";

				}
				s = s.substring(0, s.length() - 1);
				String a = "in (" + s + ")";
				//////////////////////

				if (temProduc.size() > 0) {
//					for (int j = 0; j < temProduc.size(); j++) {
					// 获取每一步的明细
//						List<CellProducOperation> cpoList = cellPassageNewService
//								.getCellProducOperationListPage(temProduc.get(j).getId());
					// 通过第三步生产主表 获取试剂明细
//						List<TempleProducingReagent> reagentList = templateService
//								.getTempleProducingReagentListPage(temProduc.get(j).getId());
					// 通过第三步生产主表 获取设备明细
//					List<TempleProducingCos> cosList = templateService
//							.getTempleProducingCosListPage(temProduc.get(j).getId());
					List<TempleProducingReagent> reagentList = templateService.getTempleProducingReagentListPage1(a);
					List<TempleProducingCos> cosList = templateService.getTempleProducingCosListPage2(a);
					//质检
					List<ZhiJianItem> zijianList = templateService.getTempleProducingZhiJianItem1(a);
					for (int j = 0; j < temProduc.size(); j++) {

						JSONArray jsontemRet = new JSONArray();
						if (reagentList.size() > 0) {
							Map<String, Object> maptemret = new HashMap<String, Object>();
							for (int jj = 0; jj < reagentList.size(); jj++) {
								if (reagentList.get(jj).getTempleProducingCell().equals(temProduc.get(j).getId())) {
									maptemret.put("id", reagentList.get(jj).getId());
									maptemret.put("code", reagentList.get(jj).getCode());
									maptemret.put("name", reagentList.get(jj).getName());
									jsontemRet.add(maptemret);
								}
							}
						}
						map.put("reagentList", jsontemRet);

						
						//质检
					    JSONArray jsontemZhJ = new JSONArray();
					    if (zijianList.size() > 0) {
							Map<String, Object> maptemZhj = new HashMap<String, Object>();
							for (int jj = 0; jj < zijianList.size(); jj++) {
								if (zijianList.get(jj).getTempleProducingCell().equals(temProduc.get(j).getId())) {
									maptemZhj.put("id", zijianList.get(jj).getId());
									maptemZhj.put("code", zijianList.get(jj).getCode());
									maptemZhj.put("name", zijianList.get(jj).getName());
									maptemZhj.put("num", zijianList.get(jj).getSampleNum());
									maptemZhj.put("unit", zijianList.get(jj).getSampleNumUnit());
									maptemZhj.put("sname", zijianList.get(jj).getSampleType());
									jsontemZhJ.add(maptemZhj);
								}
							}
						}
					    map.put("zhiJianList", jsontemZhJ);
					    
						map.put("id", temProduc.get(j).getId());
						map.put("producingName", temProduc.get(j).getProducingName());
						map.put("producingTitle", temProduc.get(j).getProducingTitle());

						map.put("customTest", temProduc.get(j).getCustomTest());

						// 设备
						JSONArray jsontemCos = new JSONArray();
						if (cosList.size() > 0) {
							Map<String, Object> maptemcos = new HashMap<String, Object>();
							for (int jjjj = 0; jjjj < cosList.size(); jjjj++) {
								if (cosList.get(jjjj).getTempleProducingCell().equals(temProduc.get(j).getId())) {
									maptemcos.put("id", cosList.get(jjjj).getId());
									maptemcos.put("typeId", cosList.get(jjjj).getTypeId());
									maptemcos.put("typeName", cosList.get(jjjj).getTypeName());
									jsontemCos.add(maptemcos);
								}
							}
						}

						map.put("cosList", jsontemCos);
						jsonArray.add(map);

					}
					result.put("customTest", temProduc.get(0).getCustomTest().toString());

					if (temProduc.size() > 0) {
						Map<String, Object> maptem = new HashMap<String, Object>();
						for (int j = 0; j < temProduc.size(); j++) {
							maptem.put("id", temProduc.get(j).getId());
							maptem.put("customTest", temProduc.get(j).getCustomTest());
							// 小步骤的开始时间结束时间
							maptem.put("hideTime", temProduc.get(j).getHideSmalleTime());
							jsontem.add(maptem);
						}
					}
					result.put("temProduc", jsontem);

//						map.put("cpoList", cpoList);
//						JSONArray jsontemRet = new JSONArray();
//
//						if (reagentList.size() > 0) {
//							Map<String, Object> maptemret = new HashMap<String, Object>();
//							for (int jj = 0; jj < reagentList.size(); jj++) {
//								maptemret.put("id", reagentList.get(jj).getId());
//								maptemret.put("code", reagentList.get(jj).getCode());
//								maptemret.put("name", reagentList.get(jj).getName());
//								jsontemRet.add(maptemret);
//							}
//						}
//						map.put("reagentList", jsontemRet);
//						map.put("reagentList", reagentList);
//						map.put("customTest", temProduc.get(j).getCustomTest());
//						JSONArray jsontemCos = new JSONArray();
//						if (cosList.size() > 0) {
//							Map<String, Object> maptemcos = new HashMap<String, Object>();
//							for (int jjjj = 0; jjjj < cosList.size(); jjjj++) {
//								maptemcos.put("id", cosList.get(jjjj).getId());
//								maptemcos.put("typeId", cosList.get(jjjj).getTypeId());
//								maptemcos.put("typeName", cosList.get(jjjj).getTypeName());
//								jsontemCos.add(maptemcos);
//							}
//						}
//						map.put("cosList", jsontemCos);
////						map.put("cosList", cosList);
//						jsonArray.add(map);
//					}
					// 放自定义字段
//					result.put("customTest", temProduc.get(0).getCustomTest().toString());

//					if (temProduc.size() > 0) {
//						Map<String, Object> maptem = new HashMap<String, Object>();
//						for (int j = 0; j < temProduc.size(); j++) {
//							maptem.put("id", temProduc.get(j).getId());
//							maptem.put("customTest", temProduc.get(j).getCustomTest());
//							// 小步骤的开始时间结束时间
//							maptem.put("hideTime", temProduc.get(j).getHideSmalleTime());
//
//							jsontem.add(maptem);
//						}
//					}
//					result.put("temProduc", jsontem);
//					result.put("temProduc", temProduc);
				}
				System.out.println(new Date() + "第一循环结束");
				// 通过模板主表id查询 模板明细数据 主表
				List<CellProducOperation> temProd = cellPassageNewService.findByIdAndOrderNumProductOperation(id,
						orderNum);
				String ss = "";
				for (CellProducOperation cellProducOperation : temProd) {
					ss += "'" + cellProducOperation.getId() + "'" + ",";

				}
				ss = ss.substring(0, ss.length() - 1);
				String aa = "in (" + ss + ")";

				System.out.println(new Date() + "第二循环开始");
				if (temProd.size() > 0) {
					// 试剂列表
					List<CellproductReagent> regList = cellPassageNewService.findByIdProductRegentList1(aa);
					
					// 设备列表
					List<CellproductCos> cosList = cellPassageNewService.findByIdCellproductCosList1(aa);
					List<CellZhiJian> zhiJianList = cellPassageNewService.findByIdCellproductZhiJianList1(aa);

					for (int i = 0; i < temProd.size(); i++) {
//						// 试剂列表
//						List<CellproductReagent> regList = cellPassageNewService
//								.findByIdProductRegentList(temProd.get(i).getId());
						mapJson.put("id", temProd.get(i).getId());
						mapJson.put("templeId", temProd.get(i).getTempleCell().getId());
						mapJson.put("state", temProd.get(i).getState());

						if (regList.size() > 0) {
							Map<String, Object> mapJont = new HashMap<String, Object>();
							for (int j = 0; j < regList.size(); j++) {
								if (regList.get(j).getCellProduc().getId().equals(temProd.get(i).getId())) {
									mapJont.put("id", regList.get(j).getId());
									mapJont.put("batch", regList.get(j).getBatch());
									mapJont.put("cellPassSn", regList.get(j).getCellPassSn());
									mapJont.put("reagenName", regList.get(j).getReagenName());
									mapJont.put("reagenCode", regList.get(j).getReagenCode());
									mapJont.put("reagentDosage", regList.get(j).getReagentDosage());
									mapJont.put("unit", regList.get(j).getUnit());
									mapJont.put("templateReagentId", regList.get(j).getTemplateReagent().getId());
									mapJont.put("cellProduc", regList.get(j).getCellProduc().getId());
									jsonReagen.add(mapJont);
								}
							}
						}
						mapJson.put("reagent", jsonReagen);
						//质检
						if (zhiJianList.size() > 0) {
							Map<String, Object> mapJont = new HashMap<String, Object>();
							for (int j = 0; j < zhiJianList.size(); j++) {
								mapJont.put("id", zhiJianList.get(j).getId());
								mapJont.put("code", zhiJianList.get(j).getCode());
								mapJont.put("name", zhiJianList.get(j).getName());
								mapJont.put("sampleNum", zhiJianList.get(j).getSampleNum());
								mapJont.put("sampleNumUnit", zhiJianList.get(j).getSampleNumUnit());
								mapJont.put("sampleType", zhiJianList.get(j).getSampleType());
								mapJont.put("zhiJianItem", zhiJianList.get(j).getItem().getId());
								mapJont.put("cellProduc", zhiJianList.get(j).getCellProduc().getId());
								jsonZhijian.add(mapJont);
							}
						}
						mapJson.put("zhijian", jsonZhijian);
						// mapJson.put("reagent", regList);

						// 设备列表
//						List<CellproductCos> cosList = cellPassageNewService
//								.findByIdCellproductCosList(temProd.get(i).getId());

						if (cosList.size() > 0) {
							Map<String, Object> mapJonts = new HashMap<String, Object>();
							for (int j = 0; j < cosList.size(); j++) {
								if (temProd.get(i).getId().equals(cosList.get(j).getCellProduc().getId())) {
									mapJonts.put("id", cosList.get(j).getId());
									mapJonts.put("instrumentName", cosList.get(j).getInstrumentName());
									mapJonts.put("instrumentCode", cosList.get(j).getInstrumentCode());
									mapJonts.put("instrumentId", cosList.get(j).getInstrumentId());
									mapJonts.put("templateCosId", cosList.get(j).getTemplateCos().getId());
									mapJonts.put("cellProducId", cosList.get(j).getCellProduc().getId());
									jsonCos.add(mapJonts);
								}
							}
						}
						mapJson.put("cosList", jsonCos);

//						if (cosList.size() > 0) {
//							Map<String, Object> mapJonts = new HashMap<String, Object>();
//							for (int j = 0; j < cosList.size(); j++) {
//								mapJonts.put("id", cosList.get(j).getId());
//								mapJonts.put("instrumentName", cosList.get(j).getInstrumentName());
//								mapJonts.put("instrumentCode", cosList.get(j).getInstrumentCode());
//								mapJonts.put("instrumentId", cosList.get(j).getInstrumentId());
//								mapJonts.put("templateCosId", cosList.get(j).getTemplateCos().getId());
//								mapJonts.put("cellProducId", cosList.get(j).getCellProduc().getId());
//								jsonCos.add(mapJonts);
//							}
//						}
//						mapJson.put("cosList", jsonCos);
						jsonRes.add(mapJson);
					}
				}
				System.out.println(new Date() + "第二循环结束");
				// 获取生产操作结果 以及开始时间和结束时间
//				List<CellProducResults> cellResult = cellPassageNewService
//						.findCellProducResultsPage(cellPassage.getId(), orderNum);
				// 获取生产操作开始时间和结束时间（放在步骤上了）
				CellPassageTemplate cellResult = cellPassageNewService.getCellPassageTemplateByCellAndStep(orderNum,
						cellPassage.getId());
				System.out.println(new Date() + "第三循环开始");
				Map<String, Object> cellResultTime = new HashMap<String, Object>();
				if (cellResult != null) {
					cellResultTime.put("productStartTime", cellResult.getProductStartTime());
					cellResultTime.put("productEndTime", cellResult.getProductEndTime());
					cellResultTime.put("notes", cellResult.getNotes());
				}
				System.out.println(new Date() + "第三循环结束");
				result.put("cellResult", cellResultTime);
				result.put("temProd", temProd);
				result.put("jsonRes", jsonRes.toString());
			}
			result.put("success", true);
			result.put("reagenOrCos", jsonArray.toString());
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String bianhuan(String orderNum) {
		if (orderNum.indexOf(".") == -1) {

		} else {
			String[] onm = orderNum.split("\\.");
			orderNum = onm[0];
		}
		return orderNum;
	}

	/**
	 * @Title: saveAllot @Description: 保存第一步的工前及检查准备数据 以及生产检查数据 @author :
	 * @date @throws
	 */
	@Action(value = "savepreparationProductionRecord")
	public void savepreparationProductionRecord() throws Exception {
		String saveData = getParameterFromRequest("saveData");
		String changeLog = getParameterFromRequest("changeLog");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// 创建生产检查明细表
			CellProductionRecord cellProduc = new CellProductionRecord();
			CellPassage cellPassage = new CellPassage();
			JSONObject json = JSONObject.fromObject(saveData);
			if (!"".equals(json.getString("id"))) {
				cellPassage = commonService.get(CellPassage.class, json.getString("id"));
			}
			if (!"".equals(json.getString("content"))) {
				JSONObject jsonCon = JSONObject.fromObject(json.getString("content"));
				if (!"".equals(jsonCon.getString("guohkId"))) {
					List<CellProductionRecord> cellProdList = cellPassageNewService
							.findProductionRecordInfo(json.getString("id"), json.getString("orderNum").toString());
					if (cellProdList.size() > 0) {
						cellProduc = cellProdList.get(0);
					}
				}
				cellProduc.setCreateUser(user.getId());
				cellProduc.setCreateDate(ObjectToMapUtils.getTimeString());
				// 如何获取一个对象中的属性名称
				JSONArray jsonis = JSONArray.fromObject(cellProduc.getTempleItem().getContent());
				if (jsonis.size() > 0) {
					for (int j = 0; j < jsonis.size(); j++) {
						JSONObject jso = JSONObject.fromObject(jsonis.get(j));
						if ("operatingRoomName".equals(jso.getString("fieldName"))) {
							cellProduc.setOperatingRoomName(jsonCon.getString("operatingRoomName"));
							cellProduc.setOperatingRoomId(jsonCon.getString("operatingRoomId"));
						}
//						if ("operatingRoomId".equals(jso.getString("fieldName"))) {
//							cellProduc.setOperatingRoomId(jsonCon.getString("operatingRoomId"));
//						}
						if ("ambientTemperature".equals(jso.getString("fieldName"))) {
							cellProduc.setAmbientTemperature(jsonCon.getString("ambientTemperature"));
						}
						if ("templeOperator".equals(jso.getString("fieldName"))) {
//							cellProduc.setTempleOperator(jsonCon.getString("templeOperator"));
//							111111111111111111111111
							if (jso.getString("fieldName") != null && !"".equals(jso.getString("fieldName"))) {
								User caozuouser = commonDAO.get(User.class, jsonCon.getString("templeOperator"));
								if (caozuouser != null) {
									cellProduc.setTempleOperator(caozuouser);
								} else {
									cellProduc.setTempleOperator(null);
								}
							}
						}
						if ("templeReviewer".equals(jso.getString("fieldName"))) {
							if (jso.getString("fieldName") != null && !"".equals(jso.getString("fieldName"))) {
								User caozuouser = commonDAO.get(User.class, jsonCon.getString("templeReviewer"));
								if (caozuouser != null) {
									cellProduc.setTempleReviewer(caozuouser);
								} else {
									cellProduc.setTempleReviewer(null);
								}
							}
//							cellProduc.setTempleReviewer(jsonCon.getString("templeReviewer"));
						}
						if ("qaTempleReviewer".equals(jso.getString("fieldName"))) {
							if (jso.getString("fieldName") != null && !"".equals(jso.getString("fieldName"))) {
								User caozuouser = commonDAO.get(User.class, jsonCon.getString("qaTempleReviewer"));
								if (caozuouser != null) {
									cellProduc.setQaTempleReviewer(caozuouser);
								} else {
									cellProduc.setQaTempleReviewer(null);
								}
							}
//							cellProduc.setTempleReviewer(jsonCon.getString("templeReviewer"));
						}
						if ("ambientHumidity".equals(jso.getString("fieldName"))) {
							cellProduc.setAmbientHumidity(jsonCon.getString("ambientHumidity"));
						}

					}
				}
				cellProduc.setOrderNum(json.getString("orderNum"));
				cellProduc.setState("0");
				// 保存自定义字段字符串 包括默认的 以及自己定义的
				cellProduc.setContent(jsonCon.toString());
				commonService.saveOrUpdate(cellProduc);
			}
			if (!"".equals(json.getString("cellproductCos"))) {
				// 创建验工前准备设备表
				JSONArray CellproductCo = JSONArray.fromObject(json.getString("cellproductCos"));
				if (CellproductCo.size() > 0) {
					for (int n = 0; n < CellproductCo.size(); n++) {
						JSONObject jsonNs = JSONObject.fromObject(CellproductCo.get(n));
						CellproductCos cellproductCos = new CellproductCos();
						// 获取第一步生產数据 指令
						cellproductCos = commonService.get(CellproductCos.class, jsonNs.getString("id"));
						if (cellproductCos == null) {
							cellproductCos = new CellproductCos();
						}
						cellproductCos.setInstrumentCode(jsonNs.getString("code"));
						cellproductCos.setInstrumentName(jsonNs.getString("name"));
						cellproductCos.setOperationState(jsonNs.getString("operationState"));
						commonService.saveOrUpdate(cellproductCos);
					}

				}
			}
			
			if (!"".equals(json.getString("cellRoomPass"))) {
				// 创建验工前准备设备表
				JSONArray cellRoomPass = JSONArray.fromObject(json.getString("cellRoomPass"));
				if (cellRoomPass.size() > 0) {
					for (int n = 0; n < cellRoomPass.size(); n++) {
						JSONObject jsonNs = JSONObject.fromObject(cellRoomPass.get(n));
						CellPassageRoomPreparat cellPassageRoomPreparat = new CellPassageRoomPreparat();
						// 获取第一步生產数据 指令
						cellPassageRoomPreparat = commonService.get(CellPassageRoomPreparat.class, jsonNs.getString("id"));
						if (cellPassageRoomPreparat == null) {
							cellPassageRoomPreparat = new CellPassageRoomPreparat();
						}
						cellPassageRoomPreparat.setState(jsonNs.getString("state"));
						commonService.saveOrUpdate(cellPassageRoomPreparat);
					}

				}
			}
			
				if (!"".equals(json.getString("productionReagent"))) {
					// 创建验工前准备试剂表
					JSONArray jsonArrproductionReagent = JSONArray.fromObject(json.getString("productionReagent"));
					if (jsonArrproductionReagent.size() > 0) {
						String c = "";
						for (int n = 0; n < jsonArrproductionReagent.size(); n++) {
							JSONObject jsonNs = JSONObject.fromObject(jsonArrproductionReagent.get(n));
							CellPassageBeforeReagent cellPreparat = new CellPassageBeforeReagent();
							// 获取第一步生產数据 指令
							cellPreparat = commonService.get(CellPassageBeforeReagent.class, jsonNs.getString("id"));
							if (cellPreparat == null) {
								cellPreparat = new CellPassageBeforeReagent();
							}
							cellPreparat.setReagentNo(jsonNs.getString("reagentNo"));
							cellPreparat.setReagentValidityTime(jsonNs.getString("reagentValidityTime"));
							cellPreparat.setReagentNum(jsonNs.getString("reagentNum"));
							cellPreparat.setStorageItemId(jsonNs.getString("storageItemId"));
							commonService.saveOrUpdate(cellPreparat);
							if (!"".equals(cellPreparat.getReagentNo()) && cellPreparat.getReagentNo() != null) {
	
								List<CellProducOperation> cellProducOperations = cellPassageNewService
										.findCellProducOperation(json.getString("id"),
												json.getString("orderNum").toString());
								if (!cellProducOperations.isEmpty()) {
									for (CellProducOperation cellProducOperation : cellProducOperations) {
										List<CellproductReagent> findByIdProductRegentList = cellPassageNewService
												.findByIdProductRegentList(cellProducOperation.getId());
										for (CellproductReagent cellproductReagents : findByIdProductRegentList) {
											if (cellproductReagents.getTemplateReagent().getCode()
													.equals(cellPreparat.getReagentId())) {
												cellproductReagents.setBatch(cellPreparat.getReagentNo());
	//											cellproductReagents.setCellPassSn(jsonRes.getString("sn"));
	//											cellproductReagents.setCreateDate(user.getId());
	//											cellproductReagents.setReagentDosage(jsonRes.getString("inputVal"));
												// 单位
												cellproductReagents.setUnit(cellPreparat.getReagentUnit());
//												//使用量
//												cellproductReagents.setReagentDosage(cellPreparat.getAmount());
												cellproductReagents.setStorageItemId(cellPreparat.getStorageItemId());
												cellproductReagents.setReagenName(cellPreparat.getReagentName());
												cellproductReagents.setReagenCode(cellPreparat.getReagentId());
												
												commonService.saveOrUpdate(cellproductReagents);
											}
										}
									}
								}
							

						}

						// commonService.get(CellProducOperation.class, jsonRes.getString("id"));

					}

				}
			}

//			// 保存试剂
//			JSONArray jsonRegent = JSONArray.fromObject(jsonOb.getString("reagent"));
//			CellproductReagent reagen = new CellproductReagent();
//			if (jsonRegent.size() > 0) {
//				for (int j = 0; j < jsonRegent.size(); j++) {
//					JSONObject jsonRes = JSONObject.fromObject(jsonRegent.get(j));
//					// 保存第三步操作 试剂 生产主表 步骤 模板主表
//					reagen = commonService.get(CellproductReagent.class, jsonRes.getString("id"));
//					if (reagen != null) {
//						reagen.setBatch(jsonRes.getString("batch"));
//						reagen.setCellPassSn(jsonRes.getString("sn"));
//						reagen.setCreateDate(user.getId());
//						reagen.setReagentDosage(jsonRes.getString("inputVal"));
//						//单位
//						reagen.setUnit(jsonRes.getString("unit"));
//						reagen.setReagenName(jsonRes.getString("name"));
//						reagen.setReagenCode(jsonRes.getString("code"));
//						commonService.saveOrUpdate(reagen);
//					}
//				}
//			}

			if (!"".equals(json.getString("temNstr"))) {
				// 创建验工前准备及检查明细表
				JSONArray jsonArr = JSONArray.fromObject(json.getString("temNstr"));
				if (jsonArr.size() > 0) {
					String c = "";
					for (int n = 0; n < jsonArr.size(); n++) {
						JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
						CellPassagePreparat cellPreparat = new CellPassagePreparat();
						// 获取第一步生產数据 指令
						cellPreparat = commonService.get(CellPassagePreparat.class, jsonNs.getString("id"));
						if (cellPreparat == null) {
							cellPreparat = new CellPassagePreparat();
						}
						cellPreparat.setCreateDate(ObjectToMapUtils.getTimeString());
						cellPreparat.setCreateUser(user.getId());
						cellPreparat.setOperationNotes(jsonNs.getString("operationNotes"));
						if (json.has("orderNumTwo") && !"".equals(json.getString("orderNumTwo"))) {
							cellPreparat.setOrderNum(json.getString("orderNumTwo"));

						} else {
							cellPreparat.setOrderNum(json.getString("orderNum"));

						}
						cellPreparat.setProductionInspection(jsonNs.getString("productionInspection"));
						cellPreparat.setCellPassage(cellPassage);
						cellPreparat.setState("0");

						String a = jsonNs.getString("operationNotes");
						String b = jsonNs.getString("productionInspection");
						String d = json.getString("orderNum");
						if (b.equals("0")) {
							b = "由:是 '变为':否";
						} else {
							b = "由:否 '变为':是";
						}
						if (n == 0) {
							c += "步骤" + d + ":" + "生产检查:" + b + ":" + "备注:" + a;
						} else {
							c += d + "生产检查:" + b + ":" + "备注:" + a;
						}
						commonService.saveOrUpdate(cellPreparat);

					}
					if (c != null && !"".equals(c)) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						li.setFileId(cellPassage.getId());
						li.setClassName("CellPassage");
						li.setUserId(user.getId());
						li.setModifyContent(c);
						li.setState("1");
						li.setStateName("数据新增");
						commonService.saveOrUpdate(li);
					}

					if (changeLog != null && !"".equals(changeLog)) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						li.setFileId(cellPassage.getId());
						li.setClassName("CellPassage");
						li.setUserId(user.getId());
						li.setState("1");
						li.setStateName("数据新增");
						li.setModifyContent(changeLog);
						commonService.saveOrUpdate(li);
					}
//					cellPassageNewService.saveChangeLog1(c);
//					cellPassageNewService.saveChangeLog(changeLog);

				}
			}

			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 获取第二步细胞传代实验明细
	@Action(value = "showCellPassageItemListPagebleJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCellPassageItemListPagebleJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = cellPassageNewService.selectCellPassageItemListPageTable(id, orderNum, start,
					length, query, col, sort);
			List<CellPassageItem> list = (List<CellPassageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			CellPassageItem cellPaaItem = new CellPassageItem();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cellPaaItem);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Title: saveCosOrReagent @Description: 保存<试剂 设备>操作 @author :
	 * @date @throws
	 */
	@Action(value = "saveCosandReagent")
	public void saveCosandReagent() throws Exception {
		String saveData = getParameterFromRequest("saveData");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> result = new HashMap<String, Object>();
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		CellProducOperation cellProdu = new CellProducOperation();
		try {
			JSONObject json = JSONObject.fromObject(saveData);
			CellPassage cellPassage = cellPassageNewService.findCellPassage(json.getString("id"));
			if (cellPassage != null) {
				String stepId = json.getString("stepId");
				String productStartTime = json.getString("productStartTime");
				String productEndTime = json.getString("productEndTime");
				String notes = json.getString("notes");

				JSONArray jsonc = JSONArray.fromObject(json.getString("content"));
				if (jsonc.size() > 0) {
					for (int i = 0; i < jsonc.size(); i++) {
						JSONObject jsonOb = JSONObject.fromObject(jsonc.get(i));
						// 保存第三步操作主表 生产主表 步骤 模板主表
						cellProdu = commonService.get(CellProducOperation.class, jsonOb.getString("id"));
						if (cellProdu != null) {
							cellProdu.setCreateDate(ObjectToMapUtils.getTimeString());
							cellProdu.setCreateUser(user.getId());
							cellProdu.setState(jsonOb.getString("state"));
							String startTime = jsonOb.getString("startTime");
							String endTime = jsonOb.getString("endTime");
							cellProdu.setStartTime(startTime);
							cellProdu.setEndTime(endTime);
							cellProdu.setCustomTest(jsonOb.getString("customTest"));
							cellProdu.setCustomTestValue(jsonOb.getString("customTestValue"));
							commonService.saveOrUpdate(cellProdu);
						}
						// 保存试剂
						JSONArray jsonRegent = JSONArray.fromObject(jsonOb.getString("reagent"));
						CellproductReagent reagen = new CellproductReagent();
						if (jsonRegent.size() > 0) {
							for (int j = 0; j < jsonRegent.size(); j++) {
								JSONObject jsonRes = JSONObject.fromObject(jsonRegent.get(j));
								// 保存第三步操作 试剂 生产主表 步骤 模板主表
								reagen = commonService.get(CellproductReagent.class, jsonRes.getString("id"));
								if (reagen != null) {
									reagen.setBatch(jsonRes.getString("batch"));
									reagen.setCellPassSn(jsonRes.getString("sn"));
									reagen.setCreateDate(user.getId());
									reagen.setReagentDosage(jsonRes.getString("inputVal"));
									// 单位
									reagen.setUnit(jsonRes.getString("unit"));
									reagen.setReagenName(jsonRes.getString("name"));
									reagen.setReagenCode(jsonRes.getString("code"));
									commonService.saveOrUpdate(reagen);
								}
							}
						}

						// 保存设备数据
						JSONArray jsonCos = JSONArray.fromObject(jsonOb.getString("cos"));
						CellproductCos cos = new CellproductCos();
						if (jsonCos.size() > 0) {
							for (int j = 0; j < jsonCos.size(); j++) {
								JSONObject jsoncos = JSONObject.fromObject(jsonCos.get(j));
								// 保存第三步操作 试剂 生产主表 步骤 模板主表
								cos = commonService.get(CellproductCos.class, jsoncos.getString("id"));
								if (cos != null) {
									cos.setCellProduc(cellProdu);
									cos.setCreateDate(ObjectToMapUtils.getTimeString());
									cos.setCreateUser(user.getId());
									cos.setInstrumentId(jsoncos.getString("typeId"));
									cos.setInstrumentName(jsoncos.getString("name"));
									cos.setInstrumentCode(jsoncos.getString("code"));
									commonService.saveOrUpdate(cos);
								}
							}
						}
					}
					// 保存实验结果
					JSONObject jsoresu = JSONObject.fromObject(json.getString("result"));
					// 保存第三步操作 试剂 生产主表 步骤
					List<CellProducResults> CosList = cellPassageNewService
							.findCellProducResultsPage(cellPassage.getId(), json.getString("orderNum"));
					if (CosList.size() > 0) {
						CellProducResults cellresu = commonService.get(CellProducResults.class, CosList.get(0).getId());
						if (cellresu == null) {
							cellresu = new CellProducResults();
						}
						cellresu.setCreateUser(user.getId());
						cellresu.setContent(jsoresu.toString());
						cellresu.setCellPassage(cellPassage);
						// cellresu.setEndTime(json.getString("endtime"));
						cellresu.setOrderNum(json.getString("orderNum"));
						// cellresu.setStartTime(json.getString("startime"));
						commonService.saveOrUpdate(cellresu);
					}
				}
				CellPassageTemplate cpt = cellPassageNewService
						.getCellPassageTemplateByCellAndStep(json.getString("orderNum"), cellPassage.getId());
				if (cpt != null) {
					cpt.setProductStartTime(productStartTime);
					cpt.setProductEndTime(productEndTime);
					cpt.setNotes(notes);
					commonService.saveOrUpdate(cpt);
				}
				if (changeLog != null && !"".equals(changeLog)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(cellPassage.getId());
					li.setUserId(user.getId());
					li.setClassName("CellPassage");
					li.setState("1");
					li.setStateName("数据新增");
					li.setModifyContent(changeLog);
					commonService.saveOrUpdate(li);
				}
//				cellPassageNewService.saveChangeLog(changeLog);

			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveStartTimeOrEndTime")
	public void saveStartTimeOrEndTime() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String caozuoId = getParameterFromRequest("caozuoId");
		String smallTime = getParameterFromRequest("smallTime");
		String startOrEnd = getParameterFromRequest("startOrEnd");
		String stepName = getParameterFromRequest("stepName");
		String modify = getParameterFromRequest("modify");
		String userId = getParameterFromRequest("user");
		String time = getParameterFromRequest("time");
		String start = getParameterFromRequest("start");
		CellProducOperation cellProdu = new CellProducOperation();
		CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
		Map<String, Object> result = new HashMap<String, Object>();
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String endTime = "";
		String startAndEnd = "";
		// 小的开始和结束
		if (!"".equals(caozuoId) && caozuoId != null) {
			// 小的开始
			if (startOrEnd.equals("start")) {
				if (cellPassage != null) {
					cellProdu = commonService.get(CellProducOperation.class, caozuoId);
					cellProdu.setStartTime(smallTime);
					if ("yesStart".equals(start)) {
						endTime = cellProdu.getEndTime();
						cellProdu.setEndTime("");
					}
					commonService.saveOrUpdate(cellProdu);
					result.put("success", true);
					startAndEnd = "开始时间";

				}

			} else if (startOrEnd.equals("end")) {
				// 小的结束
				if (cellPassage != null) {
					cellProdu = commonService.get(CellProducOperation.class, caozuoId);
					cellProdu.setEndTime(smallTime);
					commonService.saveOrUpdate(cellProdu);
					result.put("success", true);
					startAndEnd = "结束时间";
				}
			}

		} else {
			// 大的开始和结束
			if (cellPassage != null) {
				CellPassageTemplate cpt = cellPassageNewService.getCellPassageTemplateByCellAndStep(orderNum,
						cellPassage.getId());
				if (startOrEnd.equals("start")) {
					cpt.setProductStartTime(smallTime);
					if ("yesStart".equals(start)) {
						endTime = cpt.getProductEndTime();

						cpt.setProductEndTime("");
					}
					commonService.saveOrUpdate(cpt);
					result.put("success", true);
					startAndEnd = "开始时间";

				} else if (startOrEnd.equals("end")) {
					cpt.setProductEndTime(smallTime);
					commonService.saveOrUpdate(cpt);
					result.put("success", true);
					startAndEnd = "结束时间";
				}
			}

		}

		if ("yes".equals(modify)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setFileId(cellPassage.getId());
			li.setUserId(user.getId());
			li.setClassName("CellPassage");
			li.setState("1");
			li.setStateName("数据新增");
			if (!"".equals(stepName) && stepName != "") {
				if ("yesStart".equals(start)) {
					li.setModifyContent("生产操作：" + stepName + "的" + startAndEnd + ":" + time + "经" + userId + "确认过被操作员："
							+ user.getName() + " 修改成" + smallTime + " 结束时间由" + endTime + "变成了空");
				} else {
					li.setModifyContent("生产操作：" + stepName + "的" + startAndEnd + ":" + time + "经" + userId + "确认过被操作员："
							+ user.getName() + " 修改成" + smallTime);
				}

			} else {
				if ("yesStart".equals(start)) {
					li.setModifyContent("生产操作：" + startAndEnd + ":" + time + "经" + userId + "确认过被操作员：" + user.getName()
							+ " 修改成" + smallTime + " 结束时间由" + endTime + "变成了空");
				} else {
					li.setModifyContent("生产操作：" + startAndEnd + ":" + time + "经" + userId + "确认过被操作员：" + user.getName()
							+ " 修改成" + smallTime);
				}

			}
			commonService.saveOrUpdate(li);
		} else {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setFileId(cellPassage.getId());
			li.setUserId(user.getId());
			li.setClassName("CellPassage");
			li.setState("1");
			li.setStateName("数据新增");
			if (!"".equals(stepName) && stepName != "") {
				li.setModifyContent("生产操作：" + stepName + "的" + startAndEnd + ":" + smallTime + " 已确认保存");
			} else {
				li.setModifyContent("生产操作：" + startAndEnd + ":" + smallTime + " 已确认保存");
			}
			commonService.saveOrUpdate(li);

		}

		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取第四步 完工操作 指令实验模板数据 @author :
	 * @date @throws
	 */
//	@Action(value = "findNstructionsEndListinfo")
//	public void findNstructionsEndListinfo() throws Exception {
//		String id = getParameterFromRequest("id");
//		String orderNum = getParameterFromRequest("orderNum");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			Map<String, Object> map = new HashMap<String, Object>();
//			CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
//			if (cellPassage != null) {
//				// 通过模板主表id查询 模板明细数据
//				List<TemplateItem> temItemList = templateService.getTemplateItem(cellPassage.getTemplate().getId(),
//						orderNum);
//				if (temItemList.size() > 0) {
//					for (int j = 0; j < temItemList.size(); j++) {
//						map.put("content", temItemList.get(j).getContent().toString().replaceAll("\'", "\""));
//						map.put("id", temItemList.get(j).getId());
//					}
//				}
//			}
//			// 获取第一步生產数据 指令
//			List<CellProductionCompleted> cellPass = cellPassageNewService.findCellProductionCompletedInfo(id,
//					orderNum);
//			// 获取实验数据 指令
//			if (cellPass.size() > 0) {
//				result.put("cellReco", cellPass.get(0));
//			} else {
//				result.put("cellReco", null);
//			}
//			// 获取实验数据 生产检查
//			List<CellPassagePreparatEnd> cellReco = cellPassageNewService.findCellPassagePreparatEndInfo(id, orderNum);
//			// 获取第一步生產数据 指令
//			result.put("cellPass", cellReco);
//			result.put("success", true);
//		} catch (Exception e) {
//			result.put("success", false);
//			e.printStackTrace();
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	@Action(value = "findNstructionsEndListinfo")
	public void findNstructionsEndListinfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			CellPassage cellPassage = cellPassageNewService.findCellPassage(id);
			if (cellPassage != null) {
				// 通过模板主表id查询 模板明细数据
				List<TemplateItem> temItemList = templateService.getTemplateItem(cellPassage.getTemplate().getId(),
						orderNum);
				if (temItemList.size() > 0) {
					for (int j = 0; j < temItemList.size(); j++) {
						map.put("content", temItemList.get(j).getContent().toString().replaceAll("\'", "\""));
						map.put("id", temItemList.get(j).getId());
					}
				}
			}
			// 获取第一步生產数据 指令
			List<CellProductionCompleted> cellPass = cellPassageNewService.findCellProductionCompletedInfo(id,
					orderNum);
			// 获取实验数据 指令
			if (cellPass.size() > 0) {
				result.put("cellReco", cellPass.get(0));
				result.put("templeContent", cellPass.get(0).getTempleItem().getContent());
				result.put("cellRecoContent", cellPass.get(0).getContent());
				result.put("cellRecoId", cellPass.get(0).getId());
			} else {
				result.put("cellReco", null);
				result.put("templeContent", null);
				result.put("cellRecoContent", null);
				result.put("cellRecoId", null);

			}
			// 获取实验数据 生产检查
			List<CellPassagePreparatEnd> cellReco = cellPassageNewService.findCellPassagePreparatEndInfo(id, orderNum);
			// 获取第一步生產数据 指令
			List<Map<String, Object>> arrList = new ArrayList<Map<String, Object>>();

			if (!cellReco.isEmpty()) {
				for (int j = 0; j < cellReco.size(); j++) {
					Map<String, Object> mapCellReco = new HashMap<String, Object>();
					if (cellReco.get(j).getTempleInsId() != null) {
						mapCellReco.put("sort", cellReco.get(j).getTempleInsId().getSort());
						mapCellReco.put("name", cellReco.get(j).getTempleInsId().getName());

					}
					mapCellReco.put("productionInspection", cellReco.get(j).getProductionInspection());
					mapCellReco.put("id", cellReco.get(j).getId());
					arrList.add(mapCellReco);
				}

			}
//			result.put("cellPass", cellReco);
			result.put("cellPass", arrList);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveCosOrReagent @Description: 保存第四步的完工清除数据操作 @author :
	 * @date @throws
	 */
	@Action(value = "savefourthStepCellproductResult")
	public void savefourthStepCellproductResult() throws Exception {
		String saveData = getParameterFromRequest("saveData");
		String changeLog = getParameterFromRequest("changeLog");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// 创建 完工清场 生产检查明细表
			CellProductionCompleted cellCompled = new CellProductionCompleted();
			CellPassage cellPassage = new CellPassage();
			JSONObject json = JSONObject.fromObject(saveData);
			if (!"".equals(json.getString("id"))) {
				cellPassage = commonService.get(CellPassage.class, json.getString("id"));
			}
			if (!"".equals(json.getString("content"))) {
				JSONObject jsonCon = JSONObject.fromObject(json.getString("content"));
				if (!"".equals(jsonCon.getString("guohkId"))) {
					cellCompled = commonService.get(CellProductionCompleted.class, jsonCon.getString("guohkId"));
					if (cellCompled == null) {
						cellCompled = new CellProductionCompleted();
						cellCompled.setCellPassage(cellPassage);
						// cellCompled.setTempleItem();
					}
				}
				cellCompled.setCreateUser(user.getId());
				cellCompled.setCreateDate(ObjectToMapUtils.getTimeString());
				cellCompled.setOrderNum(json.getString("orderNum"));

				// 如何获取一个对象中的属性名称
				JSONArray jsonis = JSONArray.fromObject(cellCompled.getTempleItem().getContent());
				if (jsonis.size() > 0) {
					for (int j = 0; j < jsonis.size(); j++) {
						JSONObject jso = JSONObject.fromObject(jsonis.get(j));
						if ("templeOperator".equals(jso.getString("fieldName"))) {
							cellCompled.setTempleOperator(jsonCon.getString("templeOperator"));
						}
						if ("templeReviewer".equals(jso.getString("fieldName"))) {
							cellCompled.setTempleReviewer(jsonCon.getString("templeReviewer"));
						}
						if ("disinfectant".equals(jso.getString("fieldName"))) {
							cellCompled.setDisinfectant(jsonCon.getString("disinfectant"));
						}
						if ("ultravioletlamptime".equals(jso.getString("fieldName"))) {
							cellCompled.setUltravioletlamptime(jsonCon.getString("ultravioletlamptime"));
						}
					}
					if (cellCompled.getUltravioletlamptime() == null
							|| "".equals(cellCompled.getUltravioletlamptime())) {
						cellCompled.setUltravioletlamptime("");
					}
				}
				// 放自定义结果
				cellCompled.setContent(jsonCon.toString());
				commonService.saveOrUpdate(cellCompled);
			}
			// 创建验工前准备及检查明细表
			JSONArray jsonArr = JSONArray.fromObject(json.getString("temNstr"));
			if (jsonArr.size() > 0) {
				String c = "";
				for (int n = 0; n < jsonArr.size(); n++) {
					JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
					CellPassagePreparatEnd cellPreparat = new CellPassagePreparatEnd();
					// 获取第一步生產数据 指令
					cellPreparat = commonService.get(CellPassagePreparatEnd.class, jsonNs.getString("id"));
					if (cellPreparat == null) {
						cellPreparat = new CellPassagePreparatEnd();
						cellPreparat.setCellPassage(cellPassage);
						// templateService.getTempleNstructionsEndListPage(templeId)
						// cellPreparat.setTempleInsId(templeInsId);
					}
					cellPreparat.setCreateDate(ObjectToMapUtils.getTimeString());
					cellPreparat.setCreateUser(user.getId());
					cellPreparat.setOrderNum(json.getString("orderNum"));
					cellPreparat.setProductionInspection(jsonNs.getString("productionInspection"));

					String b = jsonNs.getString("productionInspection");
					String d = json.getString("orderNum");
					if (b.equals("0")) {
						b = "由:是 '变为':否";
					} else {
						b = "由:否 '变为':是";
					}
					if (n == 0) {
						c += "步骤" + d + ":" + "生产检查:" + b;
					} else {
						c += d + "生产检查:" + b;
					}

					commonService.saveOrUpdate(cellPreparat);
				}
				if (c != null && !"".equals(c)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(cellPassage.getId());
					li.setUserId(user.getId());
					li.setState("1");
					li.setStateName("数据新增");
					li.setClassName("CellPassage");
					li.setModifyContent(c);
					commonService.saveOrUpdate(li);
				}

				if (changeLog != null && !"".equals(changeLog)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(cellPassage.getId());
					li.setUserId(user.getId());
					li.setState("1");
					li.setStateName("数据新增");
					li.setClassName("CellPassage");
					li.setModifyContent(changeLog);
					commonService.saveOrUpdate(li);
				}
//				cellPassageNewService.saveChangeLog1(c);

//				cellPassageNewService.saveChangeLog(changeLog);

			}
//			// 获取第一步主表模板明细 更新结束时间
//			// templateService.findt
//			CellProductionRecord cellPros = new CellProductionRecord();
//			List<CellProductionRecord> recList = cellPassageNewService.findProductionRecordInfo(cellPassage.getId(),
//					json.getString("orderNum"));
//			if (recList.size() > 0) {
//				for (int n = 0; n < recList.size(); n++) {
//					cellPros = commonService.get(CellProductionRecord.class, recList.get(n).getId());
//					if (cellPros != null) {
//						cellPros.setEndTime(ObjectToMapUtils.getTimeString());
//						commonService.saveOrUpdate(cellPros);
//					}
//				}
//			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 保存第二步质检结果数据 @author :
	 * @date @throws
	 */
	@SuppressWarnings("null")
	@Action(value = "saveQualityInspectionResults")
	public void saveQualityInspectionResults() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String saveData = getParameterFromRequest("saveData");
		String changeLog = getParameterFromRequest("changeLogItem");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			CellPassage cellPassage = new CellPassage();
			cellPassage = commonService.get(CellPassage.class, id);
			JSONObject json = JSONObject.fromObject(saveData);
			// 混合前样本
			if (!"".equals(json.getString("noBlendTabledata"))) {
				// 创建验工前准备及检查明细表
				JSONArray jsonArr = JSONArray.fromObject(json.getString("noBlendTabledata"));
				if (jsonArr.size() > 0) {
					for (int n = 0; n < jsonArr.size(); n++) {
						JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
						CellPassageItem cellPassageItem = new CellPassageItem();
						// 获取第一步生產数据 指令
						cellPassageItem = commonService.get(CellPassageItem.class, jsonNs.getString("id"));
						if (cellPassageItem == null) {
							cellPassageItem.setId(null);
						}
						cellPassageItem.setQualified(jsonNs.getString("qualified"));
						commonService.saveOrUpdate(cellPassageItem);
					}
				}
			}
			// 混合后样本
			if (!"".equals(json.getString("blendTabledata"))) {
				// 创建验工前准备及检查明细表
				JSONArray jsonArr = JSONArray.fromObject(json.getString("blendTabledata"));
				if (jsonArr.size() > 0) {
					for (int n = 0; n < jsonArr.size(); n++) {
						JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
						CellPassageItem cellPassageItem = new CellPassageItem();
						// 获取第一步生產数据 指令
						cellPassageItem = commonService.get(CellPassageItem.class, jsonNs.getString("id"));
						if (cellPassageItem == null) {
							cellPassageItem.setId(null);
						}
//						cellPassageItem.setQualified(jsonNs.getString("qualified"));
						cellPassageItem.setCounts(jsonNs.getString("counts"));
						cellPassageItem.setSampleType(jsonNs.getString("sampleType"));
						if (jsonNs.getString("counts") != null && !"".equals(jsonNs.getString("counts"))) {
							Instrument i = commonDAO.get(Instrument.class, jsonNs.getString("counts"));
							if (i != null) {
								cellPassageItem.setInstrument(i);

								List<CellProducOperation> cos = cellPassageNewService.getCellProducOperationList(
										cellPassageItem.getCellPassage().getId(), cellPassageItem.getStepNum());
								if (cos.size() > 0) {
									for (CellProducOperation co : cos) {
										List<CellproductCos> ccs = cellPassageNewService
												.getCellproductCosList(co.getId());
										if (ccs.size() > 0) {
											for (CellproductCos cc : ccs) {
												cc.setInstrumentCode(i.getId());
												commonDAO.saveOrUpdate(cc);
											}
										}
									}
								}
							}
						}
						commonService.saveOrUpdate(cellPassageItem);

						if (changeLog != null && !"".equals(changeLog)) {
							LogInfo li = new LogInfo();
							li.setLogDate(new Date());
							li.setUserId(user.getId());
							li.setState("1");
							li.setStateName("数据新增");
							li.setFileId(cellPassageItem.getCellPassage().getId());
							li.setClassName("CellPassage");
							li.setModifyContent(changeLog);
							commonService.saveOrUpdate(li);
						}
					}

//					cellPassageNewService.saveChangeLog(changeLog);

				}
			}
			// 保存生产的总 结果
			CellPassageInfo passinfo = new CellPassageInfo();
			passinfo.setCellPassage(cellPassage);
			commonService.saveOrUpdate(passinfo);

			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 获取首页下来树形列表
	@Action(value = "findCellPassageTemplateListPage")
	public void findCellPassageTemplateListPage() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String ids = getParameterFromRequest("id");

		Map<String, Object> result = new HashMap<String, Object>();
		result = (Map<String, Object>) cellPassageNewService.findCellPrimaryCultureTempRecordList(start, length, query,
				col, sort, ids);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cds = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cds);
		map.put("cellPassage-id", "");
		map.put("templeItem-name", "");
		/*
		 * map.put("id", ""); map.put("name", ""); map.put("cellPassage-id", "");
		 * map.put("endTime", ""); map.put("startTime", ""); map.put("contentData", "");
		 * map.put("content", ""); map.put("code", ""); map.put("note", "");
		 * map.put("orderNum", ""); map.put("estimatedDate", ""); map.put("zjResult",
		 * ""); map.put("testUserList", ""); map.put("planEndDate", "");
		 * map.put("planWorkDate", ""); map.put("delay", "");
		 */
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public CellPassage getCellPassage() {
		return cellPassage;
	}

	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}

	public CellPassageDao getCellPassageDao() {
		return cellPassageDao;
	}

	public void setCellPassageDao(CellPassageDao cellPassageDao) {
		this.cellPassageDao = cellPassageDao;
	}

	public CellPassageService getCellPassageService() {
		return cellPassageService;
	}

	public void setCellPassageService(CellPassageService cellPassageService) {
		this.cellPassageService = cellPassageService;
	}

	public CellPassageNewService getCellPassageNewService() {
		return cellPassageNewService;
	}

	public void setCellPassageNewService(CellPassageNewService cellPassageNewService) {
		this.cellPassageNewService = cellPassageNewService;
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate @Description: 展示排版 @author : @date @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			List<CellPassageItem> json = cellPassageNewService.showWellPlate(id);
			List<IncubatorSampleInfo> json = cellPassageNewService.showWellPlate1(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			cellPassageNewService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 选择二氧化碳培养箱
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selectInstrumentOne")
	public String selectInstrumentOne() throws Exception {
		String orderNum = getParameterFromRequest("orderNum");
		String id = getParameterFromRequest("id");
		putObjToContext("orderNum", orderNum);
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/cell/passage/instrumentOne.jsp");
	}

	@Action(value = "showDialogDicSampleTypeTableJson")
	public void showDialogDicSampleTypeTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String orderNum = getParameterFromRequest("orderNum");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = null;
		if (id != "" && orderNum != "") {
			result = cellPassageNewService.showChooseInstrumentOneJson(start, length, query, col, sort, orderNum, id);
		} else {
			result = cellPassageNewService.showInstrumentOneJson(start, length, query, col, sort);
		}
		List<Instrument> list = (List<Instrument>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("totalLocationsNumber", "");
		map.put("surplusLocationsNumber", "");

		map.put("storageContainer-name", "");
		map.put("storageContainer-rowNum", "");
		map.put("storageContainer-colNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * @throws UnsupportedEncodingException
	 * 
	 * @Title: shownextFlowDialogTable @Description: 选择下一步流向 @author :
	 *         shengwei.wang @date 2018年2月27日下午2:58:52 @return String @throws
	 */
	@Action(value = "shownextFlowDialogTable")
	public String shownextFlowDialogTable() throws UnsupportedEncodingException {
		String model = getParameterFromRequest("model");
		String productId = getParameterFromRequest("productId");
//		String sampleType = getParameterFromRequest("sampleType");
		String sampleType = URLDecoder.decode(getParameterFromRequest("sampleType"), "UTF-8");
		String sampleTypeid = getParameterFromRequest("sampleType");
		if (sampleType == null || "".equals(sampleType)) {
			sampleType = "";
		} else {
			DicSampleType dst = commonDAO.get(DicSampleType.class, sampleType);
			if (dst != null) {
				sampleType = dst.getId();
			} else {
				List<DicSampleType> dsts = commonService.get(DicSampleType.class, "name", sampleTypeid);
				if (dsts.size() > 0) {
					DicSampleType ds = dsts.get(0);
					sampleType = ds.getId();
				} else {
					sampleType = "";
				}
			}
		}
		putObjToContext("model", model);
		putObjToContext("productId", productId);
		putObjToContext("sampleType", sampleType);
		return dispatcher("/WEB-INF/page/system/nextFlow/shownextFlowDialogTable.jsp");
	}

	/**
	 * 
	 * 条码扫码确认
	 */
	@Action(value = "sampleCodeConfirm")
	public void sampleCodeConfirm() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String id = getParameterFromRequest("id");
		String sampleCodeConfirm = getParameterFromRequest("sampleCodeConfirm");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<CellPassageItemCheckCode> cpics = cellPassageNewService.sampleCodeConfirm(id, stepNum,
					sampleCodeConfirm, "0");
			if (cpics.size() > 0) {
				result.put("cz", true);
				if ("未确认".equals(cpics.get(0).getState())) {
					result.put("qr", true);
					cellPassageNewService.saveCellPassageItemCheckCode(cpics.get(0));
				} else {
					result.put("qr", false);
				}
			} else {
				result.put("cz", false);
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * 下一步条码扫码确认
	 */
	@Action(value = "sampleCodeAfterConfirm")
	public void sampleCodeAfterConfirm() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String id = getParameterFromRequest("id");
		String sampleCodeAfterConfirm = getParameterFromRequest("sampleCodeAfterConfirm");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<CellPassageItemCheckCode> cpics = cellPassageNewService.sampleCodeConfirm(id, stepNum,
					sampleCodeAfterConfirm, "1");
			if (cpics.size() > 0) {
				result.put("cz", true);
				if ("未确认".equals(cpics.get(0).getState())) {
					result.put("qr", true);
					cellPassageNewService.saveCellPassageItemCheckCode(cpics.get(0));
				} else {
					result.put("qr", false);
				}
			} else {
				result.put("cz", false);
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 
	 * 房间监测记录查询
	 */
	@Action(value = "findEnmonitor")
	public void findEnmonitor() throws Exception {
		String operatingRoomId = getParameterFromRequest("operatingRoomId");
		String estimatedDateTime = getParameterFromRequest("estimatedDateTime");
		String type = getParameterFromRequest("type");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if("1".equals(type)) {
				List<CleanAreaDustItem> items = cellPassageNewService.findCleanAreaDustItems(operatingRoomId,estimatedDateTime);
				if(items.size()>0) {
					result.put("roomstate", true);
					result.put("taskid", items.get(0).getCleanAreaDust().getId());
				}else {
					result.put("roomstate", false);
				}
			}else if("2".equals(type)) {
				List<CleanAreaMicrobeItem> items = cellPassageNewService.findCleanAreaMicrobeItems(operatingRoomId,estimatedDateTime);
				if(items.size()>0) {
					result.put("roomstate", true);
					result.put("taskid", items.get(0).getCleanAreaMicrobe().getId());
				}else {
					result.put("roomstate", false);
				}
			}else if("3".equals(type)) {
				List<CleanAreaVolumeItem> items = cellPassageNewService.findCleanAreaVolumeItems(operatingRoomId,estimatedDateTime);
				if(items.size()>0) {
					result.put("roomstate", true);
					result.put("taskid", items.get(0).getCleanAreaVolume().getId());
				}else {
					result.put("roomstate", false);
				}
			}else if("4".equals(type)) {
				List<CleanAreaBacteriaItem> items = cellPassageNewService.findCleanAreaBacteriaItems(operatingRoomId,estimatedDateTime);
				if(items.size()>0) {
					result.put("roomstate", true);
					result.put("taskid", items.get(0).getCleanAreaBacteria().getId());
				}else {
					result.put("roomstate", false);
				}
			}else if("5".equals(type)) {
				List<CleanAreaMicroorganismItem> items = cellPassageNewService.findCleanAreaMicroorganismItems(operatingRoomId,estimatedDateTime);
				if(items.size()>0) {
					result.put("roomstate", true);
					result.put("taskid", items.get(0).getCleanAreaMicroorganism().getId());
				}else {
					result.put("roomstate", false);
				}
			}else if("6".equals(type)) {
				List<CleanAreaDiffPressureItem> items = cellPassageNewService.findCleanAreaDiffPressureItems(operatingRoomId,estimatedDateTime);
				if(items.size()>0) {
					result.put("roomstate", true);
					result.put("taskid", items.get(0).getCleanAreaDiffPressure().getId());
				}else {
					result.put("roomstate", false);
				}
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/***
	 * 样本退回生产异常
	 * 
	 * @throws Exception
	 */
	@Action(value = "backSample")
	public void backSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] batch = getRequest().getParameterValues("batch[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			cellPassageNewService.backSample(id, batch);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/***
	 * 扫描样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "choseSampleCode")
	public void choseSampleCode() throws Exception {
		String id = getParameterFromRequest("yangben");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//样本编号查询产品批号
			CellPassageTemp cpt = cellPassageNewService.choseSampleCode(id);
			//产品批号有几个
			List<CellPassageTemp> list = cellPassageNewService.queryBatch(cpt.getBatch());
			if (cpt != null) {
				if (!list.isEmpty()) {
					result.put("data", cpt.getBatch());
					result.put("code", cpt.getCode());
					result.put("list", list.size());
					result.put("success", true);
				}
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	@Action(value = "saveStepState")
	public void saveStepState() throws Exception {
		String id = getParameterFromRequest("id");
		String[] state = getRequest().getParameterValues("state[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			cellPassageNewService.saveStepState(id, state);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	@Action(value = "selStepState")
	public void selStepState() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> map=cellPassageNewService.selStepState(id);
			result.put("success", true);
			result.put("val", map);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
