package com.biolims.experiment.cell.passage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageBeforeReagent;
import com.biolims.experiment.cell.passage.model.CellPassageCos;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageItemCheckCode;
import com.biolims.experiment.cell.passage.model.CellPassageObservation;
import com.biolims.experiment.cell.passage.model.CellPassagePreparat;
import com.biolims.experiment.cell.passage.model.CellPassagePreparatEnd;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageReagent;
import com.biolims.experiment.cell.passage.model.CellPassageRoomPreparat;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellPassageZhiJian;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProducResults;
import com.biolims.experiment.cell.passage.model.CellProductionCompleted;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellZhiJian;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.cell.passage.model.CellproductReagent;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.transferWindowManage.model.TransferWindowManage;
import com.biolims.experiment.transferWindowManage.model.TransferWindowState;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.ZhiJianItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CellPassageDao extends BaseHibernateDao {
	public Map<String, Object> findCellPassageTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassage where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassage where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showCellPassageJson(Integer start, Integer length, String query, String col, String sort)
			throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassage where 1=1 and state='3'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassage where 1=1 and state='3' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectCellPassageTempTable(String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (codes != null && !codes.equals("")) {
			key += " and code in (:alist)";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			List<CellPassageTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from CellPassageTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			if (codes != null && !codes.equals("")) {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
						.setParameterList("alist", codes).list();
			} else {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}
	public Long selectCellPassageTempTable(String batch) throws Exception {
		String countHql = "select count(*) from CellPassageTemp where 1=1 and state='1' and batch='"+batch+"'";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();

		return sumCount;
		
	}
	public List<QualityTestTemp> findQualityTestSample(String codeConfirm) throws Exception {
		String hql = "from QualityTestTemp where 1=1  and sampleNumber='"+codeConfirm+"' and (confirm is null or confirm='') " ;
		List<QualityTestTemp> list = getSession().createQuery(hql).list();

		return list;
		
	}
	public List<QualityTestTemp> findQualityTestSampleStart(String codeConfirm) throws Exception {
		String hql = "from QualityTestTemp where 1=1 and confirm='1' and sampleNumber='"+codeConfirm+"'";
		List<QualityTestTemp> list = getSession().createQuery(hql).list();
		
		return list;
		
	}
	public List<TransferWindowState> findTransferWindowState(String codeConfirm) throws Exception {
		String hql = "from TransferWindowState where 1=1 and code='"+codeConfirm+"' and sampleState ='1' and state='1'";
		List<TransferWindowState> list = getSession().createQuery(hql).list();
		
		return list;
		
	}
	public List<TransferWindowState> findTransferWindowStateNew(String codeConfirm) throws Exception {
		String hql = "from TransferWindowState where 1=1 and code='"+codeConfirm+"' and state='1'";
		List<TransferWindowState> list = getSession().createQuery(hql).list();
		
		return list;
		
	}

	
	public Map<String, Object> findCellPassageItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageItem where 1=1 and state='1' and cellPassage.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageItem  where 1=1 and state='1' and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CellPassageItem> showWellPlate(String id) {
		String hql = "from CellPassageItem  where 1=1 and state='2'  and  cellPassage.id='" + id + "'  ";
		List<CellPassageItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<IncubatorSampleInfo> showWellPlate1(String id) {
		String hql = "from IncubatorSampleInfo  where 1=1 and state='1'  and  incubatorId='" + id + "'  ";
		List<IncubatorSampleInfo> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> showWellPlate(String id, int ordernum) {
		String hql = "from CellPassageItem  where 1=1  and  cellPassage.id='" + id + "' and stepNum='" + ordernum
				+ "'  ";// and state='2'
		List<CellPassageItem> list = getSession().createQuery(hql).list();
		return list;
	}
	public List<TransferWindowManage> findTransferWindowManage(String id) {
		String hql = "from TransferWindowManage  where 1=1  and  regionalManagement.id='" + id + "' and  transferWindowState='1'";// and state='2'
		List<TransferWindowManage> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> showWellPlates(String id, String ordernum) {
		String hql = "from CellPassageItem  where 1=1  and  cellPassage.id='" + id + "' and stepNum='" + ordernum
				+ "' and blendState='1' ";// and state='2'
		List<CellPassageItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findCellPassageItemAfTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageItem where 1=1 and state='2' and cellPassage.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageItem  where 1=1 and state='2' and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from CellPassageItem where 1=1 and state='2' and cellPassage.id='" + id
				+ "'";
		String key = "";
		List<String> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "select counts from CellPassageItem where 1=1 and state='2' and cellPassage.id='" + id
					+ "' group by counts";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<CellPassageTemplate> showCellPassageStepsJson(String id, String code) {

		String countHql = "select count(*) from CellPassageTemplate where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and orderNum ='" + code + "'";
		} else {
		}
		List<CellPassageTemplate> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from CellPassageTemplate where 1=1 and cellPassage.id='" + id + "'";
			key += " order by orderNum asc";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<CellPassageReagent> showCellPassageReagentJson(String id, String code) {
		String countHql = "select count(*) from CellPassageReagent where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<CellPassageReagent> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from CellPassageReagent where 1=1 and cellPassage.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<CellPassageCos> showCellPassageCosJson(String id, String code) {
		String countHql = "select count(*) from CellPassageCos where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<CellPassageCos> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from CellPassageCos where 1=1 and cellPassage.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<CellPassageTemplate> delTemplateItem(String id) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = "from CellPassageTemplate where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}
	
	public List<CellPassageRoomPreparat> delCellPassageRoomPreparat(String id) {
		List<CellPassageRoomPreparat> list = new ArrayList<CellPassageRoomPreparat>();
		String hql = "from CellPassageRoomPreparat where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<CellPassageReagent> delReagentItem(String id) {
		List<CellPassageReagent> list = new ArrayList<CellPassageReagent>();
		String hql = "from CellPassageReagent where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<CellPassageCos> delCosItem(String id) {
		List<CellPassageCos> list = new ArrayList<CellPassageCos>();
		String hql = "from CellPassageCos where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> showCellPassageResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageInfo where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageInfo  where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Object> showWellList(String id) {

		String hql = "select counts,count(*) from CellPassageItem where 1=1 and cellPassage.id='" + id
				+ "' group by counts";
		List<Object> list = getSession().createQuery(hql).list();

		return list;
	}

	public List<CellPassageItem> plateSample(String id, String counts) {
		String hql = "from CellPassageItem where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (counts == null || counts.equals("")) {
			key = " and counts is null";
		} else {
			key = "and counts='" + counts + "'";
		}
		List<CellPassageItem> list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> noBlendPlateSampleTable(String id, String stepNum, String inCodes, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageItem where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (inCodes != null && !"".equals(inCodes)) {
			key += " and code in (" + inCodes + ")";
		}
		key += " and blendState = '0' and stepNum = '" + stepNum + "'";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageItem  where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findQualityItem(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageQualityItem where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageQualityItem  where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> blendPlateSampleTable(String id, String stepNum, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageItem where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		key += " and blendState = '1' and stepNum = '" + stepNum + "'";
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageItem  where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<CellPassageInfo> findCellPassageInfoByCode(String code) {
		String hql = "from CellPassageInfo where 1=1 and code='" + code + "'";
		List<CellPassageInfo> list = new ArrayList<CellPassageInfo>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageInfo> selectAllResultListById(String code) {
		String hql = "from CellPassageInfo  where (submit is null or submit='') and cellPassage.id='" + code + "'";
		List<CellPassageInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageInfo> selectResultListById(String id) {
		String hql = "from CellPassageInfo  where cellPassage.id='" + id + "'";
		List<CellPassageInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from CellPassageInfo t where (submit is null or submit='') and id in (" + insql + ")";
		List<CellPassageInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> selectCellPassageItemList(String scId) throws Exception {
		String hql = "from CellPassageItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and cellPassage.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CellPassageItem> list = new ArrayList<CellPassageItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Integer generateBlendCode(String id) {
		String hql = "select max(blendCode) from CellPassageItem where 1=1 and cellPassage.id='" + id + "'";
		Integer blendCode = (Integer) this.getSession().createQuery(hql).uniqueResult();
		return blendCode;
	}

	public CellPassageInfo getResultByCode(String code) {
		String hql = " from CellPassageInfo where 1=1 and code='" + code + "'";
		CellPassageInfo spi = (CellPassageInfo) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}

	/**
	 * @throws Exception
	 * @Title: findCellPrimaryCultureTemplateList @Description: TODO @author :
	 *         nan.jiang @date 2018-8-30上午9:40:58 @param start @param length @param
	 *         query @param col @param sort @param ids @return
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTemplateList(Integer start, Integer length, String query,
			String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageTemplate where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellPassageTemplate where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellPassageTemplate> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CellPassageZhiJian> delZhijianItem(String id) {
		List<CellPassageZhiJian> list = new ArrayList<CellPassageZhiJian>();
		String hql = "from CellPassageZhiJian where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<ZhiJianItem> showCellPassageZjianJson(String id, String code) {
		String countHql = "select count(*) from ZhiJianItem where 1=1 and template.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<ZhiJianItem> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<CellPassage> findCellPassageList() {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1 and state <> '1'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageTemplate> findTemplateList(String id) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = " from CellPassageTemplate where 1=1 and cellPassage.id = '" + id + "'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassage> findCellPassageListByUser(String name) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage cp where 1=1 and cp.state <> '1'  and cp.id in ( " + // and cp.workUser like '%"
																								// + name + "%'
				"SELECT cpt.cellPassage.id from CellProductionRecord cpt where cpt.templeOperator.id = '" + name
				+ "' and (cpt.endTime is null or cpt.endTime='') "
				+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 " + ")";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassage> findCellPassageListByUser1(String name, String pici) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage cp where 1=1 and cp.state <> '1'  and cp.id in ( " + // and cp.workUser like '%"
																								// + name + "%'
				"SELECT cpt.cellPassage.id from CellProductionRecord cpt where cpt.templeOperator.id = '" + name
				+ "' and (cpt.endTime is null or cpt.endTime='') "
				+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 " + ") and batch='" + pici + "' ";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageTemplate> findNextDataByOrderNum(String id, String orderNum) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = " from CellPassageTemplate where 1=1 and orderNum > '" + orderNum + "' and cellPassage.id = '" + id
				+ "'";
		String key = " order by orderNum";
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号查询操作指令
	public List<CellPassagePreparat> findCellNstructionsInfo(String id, String number) {
		List<CellPassagePreparat> list = new ArrayList<CellPassagePreparat>();
		String key = "and orderNum='" + number + "'";
		String hql = "from CellPassagePreparat where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 生产检查
	public List<CellProductionRecord> findProductionRecordInfo(String id, String number) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String key = "and orderNum='" + number + "'";
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 生产检查
	public List<CellProducOperation> findCellProducOperation(String id, String number) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String key = "and orderNum='" + number + "'";
		String hql = "from CellProducOperation where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第四步的 完工 生产检查
	public List<CellProductionCompleted> findCellProductionCompletedInfo(String id, String number) {
		List<CellProductionCompleted> list = new ArrayList<CellProductionCompleted>();
		String key = "and orderNum='" + number + "'";
		String hql = "from CellProductionCompleted where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第四步的 完工 指令
	public List<CellPassagePreparatEnd> findCellPassagePreparatEndInfo(String id, String number) {
		List<CellPassagePreparatEnd> list = new ArrayList<CellPassagePreparatEnd>();
		String key = "and orderNum='" + number + "'";
		String hql = "from CellPassagePreparatEnd where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 获取传代干细胞
	public Map<String, Object> selectCellPassageItemListPageTable(String id, String orderNum, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassageItem where 1=1 and state='2' and cellPassage.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from CellPassageItem  where 1=1 and state='2' and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CellPassageItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 通过实验主表id和步骤号 第三步生产操作
	public List<CellProducOperation> findProductOperationListPage(String id, String number, String templeId) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String key = "and orderNum='" + number + "' and templeCell.id='" + templeId + "'";
		String hql = "from CellProducOperation where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第三步生产操作 试剂明细
	public List<CellproductReagent> findProductRegentListPage(String id, String number, String templeId) {
		List<CellproductReagent> list = new ArrayList<CellproductReagent>();
		String key = " and templateReagent.id='" + templeId + "'";
		String hql = "from CellproductReagent where 1=1 and state !='1' and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第三步生产操作 设备明细
	public List<CellproductCos> findCellproductCosListPage(String id, String number, String templeId) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String key = " and templateCos.id='" + templeId + "'";
		String hql = "from CellproductCos where 1=1 and state !='1' and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第三步生产操作 实验结果
	public List<CellProducResults> findCellProducResultsListPage(String id, String number, String templeId) {
		List<CellProducResults> list = new ArrayList<CellProducResults>();
		String key = "and orderNum='" + number + "' and templeCell.id='" + templeId + "'";
		String hql = "from CellProducResults where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作
	public List<CellProducOperation> findByIdProductOperationList(String id) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String hql = "from CellProducOperation where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 试剂
	public List<CellproductReagent> findByIdProductRegentList(String id) {
		List<CellproductReagent> list = new ArrayList<CellproductReagent>();
		String hql = "from CellproductReagent where 1=1 and state !='1' and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}
	

	// 通过实验主表id 第三步生产操作 试剂
	public List<CellproductReagent> findByIdProductRegentList1(String id) {
		List<CellproductReagent> list = new ArrayList<CellproductReagent>();
		String hql = "from CellproductReagent where 1=1 and state !='1' and cellProduc.id " + id + " ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 设备
	public List<CellproductCos> findByIdCellproductCosList(String id) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String hql = "from CellproductCos where 1=1 and state !='1' and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 设备
	public List<CellproductCos> findByIdCellproductCosList1(String id) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String hql = "from CellproductCos where 1=1 and state !='1' and cellProduc.id " + id + " ";
		list = getSession().createQuery(hql).list();
		return list;
	}
	// 通过实验主表id 第三步生产操作 质检
	public List<CellZhiJian> findByIdCellproductZhiJianList1(String id) {
		List<CellZhiJian> list = new ArrayList<CellZhiJian>();
		String hql = "from CellZhiJian where 1=1 and state !='1' and cellProduc.id " + id + " ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作
	public List<CellProducOperation> findByIdAndOrderNumProductOperation(String id, String orderNum) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String key = "and orderNum='" + orderNum + "'";
		String hql = "from CellProducOperation where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 设备
	public List<CellproductCos> findByIdAndOrderNumCellproductCos(String id, String orderNum) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String key = "and orderNum='" + orderNum + "'";
		String hql = "from CellproductCos where 1=1 and state !='1' and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id 步骤号 第三步生产操作 生产结果数据
	public List<CellProducResults> findCellProducResultsPage(String id, String orderNum) {
		List<CellProducResults> list = new ArrayList<CellProducResults>();
		String key = "and orderNum='" + orderNum + "'";
		String hql = "from CellProducResults where 1=1 and cellPassage.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	/**
	 * @throws Exception
	 * @Title: findCellPrimaryCultureTemplateList @Description: TODO @author :
	 *         nan.jiang @date 2018-8-30上午9:40:58 @param start @param length @param
	 *         query @param col @param sort @param ids @return
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTempRecordList(Integer start, Integer length, String query,
			String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellPassageTemplate> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public TemplateItem getBlendStateByStep(String id, String orderNum) {
		String hql = "from TemplateItem where 1=1 ";
		String key = " and template.id = '" + id + "' and orderNum = '" + orderNum + "'";
		TemplateItem ct = (TemplateItem) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<CellPassageItem> findCellPassageItemListByStep(String stepNum, String mainId, String hhzt) {
		String hql = "from CellPassageItem where cellPassage.id = '" + mainId + "' and stepNum = '" + stepNum
				+ "' and blendState = '" + hhzt + "' ";
		List<CellPassageItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> findCellPassageItemListByStep(String stepNum, String mainId) {
		String hql = "from CellPassageItem where cellPassage.id = '" + mainId + "' and stepNum = '" + stepNum + "'  ";
		List<CellPassageItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查下面那个表
	public List<CellPassageItem> findCellPassageItemListByStepXia(String stepNum, String mainId) {
		String hql = "from CellPassageItem where cellPassage.id = '" + mainId + "' and stepNum = '" + stepNum
				+ "' and blendState='1' ";
		List<CellPassageItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public CellPassageTemplate findCellPassageTemplateByStep(String step, String mainId) {
		String hql = "from CellPassageTemplate where 1=1 ";
		String key = " and cellPassage.id = '" + mainId + "' and orderNum = '" + step + "'";
		CellPassageTemplate ct = (CellPassageTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public CellProductionRecord findCellProductionRecordByStep(String step, String mainId) {
		String hql = "from CellProductionRecord where 1=1 ";
		String key = " and cellPassage.id = '" + mainId + "' and orderNum = '" + step + "'";
		CellProductionRecord ct = (CellProductionRecord) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<CellProducOperation> findCellProducOperationByStep(String step, String mainId) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String hql = "from CellProducOperation where 1=1 ";
		String key = " and cellPassage.id = '" + mainId + "' and orderNum = '" + step + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public TemplateItem findTemplateItemByStep(String step, String templateid) {
		String hql = "from TemplateItem where 1=1 ";
		String key = " and template.id = '" + templateid + "' and orderNum = '" + step + "'";
		TemplateItem ct = (TemplateItem) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<CellProducOperation> getCellProducOperationListPage(String id) {
		String hql = "from CellProducOperation where templeCell.id = '" + id + "'";
		List<CellProducOperation> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellproductCos> findCellproductCoss(String id, String stepNum) {
		String hql = "from CellproductCos where cellProduc.cellPassage.id = '" + id + "' and cellProduc.orderNum='"
				+ stepNum + "' ";
		List<CellproductCos> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageObservation> findCellPassageObservations(String id, String stepNum) {
		String hql = "from CellPassageObservation where cellPassage.id = '" + id + "' and stepNum='" + stepNum + "' ";
		List<CellPassageObservation> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageBeforeReagent> findCellPassageBeforeReagents(String id, String stepNum) {
		String hql = "from CellPassageBeforeReagent where cellPassage.id = '" + id + "' and stepNum='" + stepNum + "' ";
		List<CellPassageBeforeReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellproductCos> getCellproductCos(String id) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String hql = "from CellproductCos where 1=1 and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProductionRecord> getCellproductionRecords(String mainId, String stepNum) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + mainId + "' and orderNum>'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageTemplate> getCellPassageTemplates(String mainId, String stepNum) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = "from CellPassageTemplate where 1=1 and cellPassage.id='" + mainId + "' and orderNum>'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageQualityItem> findCellPassageQualityItemsById(String mainId) {
		List<CellPassageQualityItem> list = new ArrayList<CellPassageQualityItem>();
		String hql = "from CellPassageQualityItem where 1=1 and cellPassage.id='" + mainId
				+ "' and (qualified='1' or qualified='3') ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageTemplate> getCellPassageTemplates(String id) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = "from CellPassageTemplate where 1=1 and cellPassage.id='" + id + "' order by orderNum ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public CellPassageTemplate getCellPassageTemplateByCellAndStep(String stepNum, String mainId) {
		String hql = "from CellPassageTemplate where 1=1 ";
		String key = " and cellPassage.id = '" + mainId + "' and orderNum = '" + stepNum + "'";
		CellPassageTemplate ct = (CellPassageTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<CellProductionRecord> getCellProductionRecordByIdAndStepNum(String mainId, String stepNum) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + mainId + "' and orderNum>'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProductionRecord> getCellProductionRecordByIdAndStepNumUnder(String mainId, String stepNum) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + mainId + "' and orderNum<'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProductionRecord> getCellProductionRecordByIdAndStepNumForzy(String mainId, String stepNum) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + mainId + "' and orderNum='" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<InfluenceProduct> findInfluenceProducts(String batch) {
		List<InfluenceProduct> list = new ArrayList<InfluenceProduct>();
		String hql = "from InfluenceProduct where 1=1 and orderCode='" + batch
				+ "' and (deviationHandlingReport.approvalStatus is null or deviationHandlingReport.approvalStatus='' or deviationHandlingReport.approvalStatus !='1' ) ";
		list = getSession().createQuery(hql).list();
		return list;
	}
	// 查询 生产操作的开始时间和结束时间

	public CellPassageTemplate findProductTime(String mainId, String stepNum) {

		String hql = "from CellPassageTemplate where 1=1 ";
		String key = " and cellPassage.id = '" + mainId + "' and orderNum = '" + stepNum + "'";
		CellPassageTemplate ct = (CellPassageTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;

	}

	public Map<String, Object> showInstrumentOneJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Instrument where 1=1 and mainType.id='281616366a9b6ff9016a9bf060f70019' ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from Instrument where 1=1 and mainType.id='281616366a9b6ff9016a9bf060f70019' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by id";
			}
			List<Instrument> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CellProducOperation> showCellProducOperationJson(String orderNum, String id) throws Exception {
		String hql = "from CellProducOperation where 1=1 and orderNum='" + orderNum + "' and cellPassage.id='" + id
				+ "' ";
		List<CellProducOperation> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellproductCos> findCellproductCos(String cellPoroducId) throws Exception {
		String hql = "from CellproductCos where 1=1 and cellProduc.id='" + cellPoroducId
				+ "' and instrumentId='281616366a9b6ff9016a9bf060f70019' ";
		List<CellproductCos> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showChooseInstrumentOneJson(Integer start, Integer length, String query, String col,
			String sort, String instrumentCode) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Instrument where 1=1 and mainType.id='281616366a9b6ff9016a9bf060f70019' and id in ("
				+ instrumentCode + ")";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from Instrument where 1=1 and mainType.id='281616366a9b6ff9016a9bf060f70019'  and id in ("
					+ instrumentCode + ") ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by id";
			}
			List<Instrument> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<IncubatorSampleInfo> getIncubatorSampleInfoBySid(String sid) {
		List<IncubatorSampleInfo> list = new ArrayList<IncubatorSampleInfo>();
		String hql = "from IncubatorSampleInfo where 1=1 and sId='" + sid + "' and state='1' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public IncubatorSampleInfo getIncubatorSampleInfoByCountsPosid(String counts, String posId) {
		String hql = "from IncubatorSampleInfo where 1=1 ";
		String key = " and incubatorId = '" + counts + "' and location = '" + posId + "' and state='1' ";
		IncubatorSampleInfo ct = (IncubatorSampleInfo) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<UserGroupUser> getUserGroupUsers(String id) {
		List<UserGroupUser> list = new ArrayList<UserGroupUser>();
		String hql = "from UserGroupUser where 1=1 and user.id='" + id
				+ "' and (userGroup.id='su088' or userGroup.id='admin' ) ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public CellPassageTemplate getStepChongfu(String id, String orderNum) {
		String hql = "from CellPassageTemplate where 1=1 ";
		String key = " and cellPassage.id = '" + id + "' and orderNum = '" + orderNum + "' ";
		CellPassageTemplate ct = (CellPassageTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public CellPassageItem getCellPassageItems(String id, String stepNum, String string) {
		String hql = "from CellPassageItem where 1=1 ";
		String key = " and cellPassage.id = '" + id + "' and stepNum = '" + stepNum + "' and blendState='1' ";
		CellPassageItem ct = (CellPassageItem) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<CellPassageItem> getCellPassageItemInfos(String id, String stepNum, String string) {
		List<CellPassageItem> list = new ArrayList<CellPassageItem>();
		String hql = "from CellPassageItem where 1=1 ";
		String key = " and cellPassage.id = '" + id + "' and stepNum = '" + stepNum + "' and blendState='" + string
				+ "' ";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<CellProductionRecord> findCellProductionRecordByMainId(String mainId) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + mainId
				+ "' and templeItem.harvest='1' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<IncubatorSampleInfo> getIncubatorSampleInfoByCode(String batch) {
		List<IncubatorSampleInfo> list = new ArrayList<IncubatorSampleInfo>();
		String hql = "from IncubatorSampleInfo where 1=1 and batch='" + batch + "' and state='1' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProducOperation> getCellProducOperationList(String id, String stepNum) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String hql = "from CellProducOperation where 1=1 and cellPassage.id='" + id + "' and orderNum='" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellproductCos> getCellproductCosList(String id) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String hql = "from CellproductCos where 1=1 and cellProduc.id='" + id
				+ "' and instrumentId='281616366a9b6ff9016a9bf060f70019' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public SampleOrder findNameByBach(String bach) {
		String hql = "from SampleOrder where 1=1 and barcode='" + bach + "' ";

		SampleOrder sampleOrder = (SampleOrder) this.getSession().createQuery(hql).uniqueResult();

		return sampleOrder;

	}

	public CellPassage findNameByBach(String id, String stepNum) {
		String hql = "from CellPassage where 1=1 and id='" + id + "'";

		CellPassage cellPassage = (CellPassage) this.getSession().createQuery(hql).uniqueResult();

		return cellPassage;

	}

	public CellPassage selectDate(String id) {
		String hql = "from CellPassage where 1=1 and id='" + id + "'";

		CellPassage cellPassage = (CellPassage) this.getSession().createQuery(hql).uniqueResult();
		return cellPassage;
	}

	public CellProductionRecord findStepNumOrValue(String id, String stepNum) {

		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + id + "' and orderNum='" + stepNum
				+ "' ";

		CellProductionRecord cell = (CellProductionRecord) this.getSession().createQuery(hql).uniqueResult();

		return cell;
	}
	public CellPassageItem getCellPassageItem(String id) {
		
		String hql = "from CellPassageItem where 1=1 and id='" + id + "'";
		
		CellPassageItem cell = (CellPassageItem) this.getSession().createQuery(hql).uniqueResult();
		
		return cell;
	}

	public List<StorageOutItem> getfindStorageOutItems(Storage st, String pici) {
		String hql = "from StorageOutItem where 1=1 and storage.id='"+st.getId()+"' and code='"+pici+"' and (usedFinish is null or usedFinish='0') and storageOut.state='1'     ";
		  
		List<StorageOutItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<StorageReagentBuySerial> getfindStorageReagentBuySerials(Storage st, String pici) {
		String hql = "from StorageReagentBuySerial where 1=1 and storage.id='"+st.getId()+"' and serial='"+pici+"' ";
		  
		List<StorageReagentBuySerial> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CellPassageItemCheckCode> findCellPassageItemCheckCodes(String id, String orderNum, String blendState) {
		String hql = "from CellPassageItemCheckCode where 1=1 and cellPassage.id='" + id + "' and stepNum='" +orderNum + "' and blendState='"+blendState+"' ";
		  
		List<CellPassageItemCheckCode> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<TemplateItem> getTemplateItems(Template template, String stepNum) {
		String hql = "from TemplateItem where 1=1 and template.id='" + template.getId() + "' and orderNum='" +stepNum + "' ";
		  
		List<TemplateItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	public List<TemplateItem> getTemplateItems(String template, String stepNum) {
		String hql = "from TemplateItem where 1=1 and template.id='" + template + "' and orderNum='" +stepNum + "' ";
		
		List<TemplateItem> cell = getSession().createQuery(hql).list();
		
		return cell;
	}
	public List<TempleProducingCell> getTempleProductingCell(String templateId, String stepNum) {
		String hql = "from TempleProducingCell where 1=1 and template.id='" + templateId + "' and orderNum='" +stepNum + "' ";
		
		List<TempleProducingCell> cell = getSession().createQuery(hql).list();
		
		return cell;
	}
	public List<TempleProducingCell> getTempleProductingCellNew(String templateId, int stepNum) {
		String hql = "from TempleProducingCell where 1=1 and template.id='" + templateId + "' and orderNum='" +stepNum + "' ";
		
		List<TempleProducingCell> cell = getSession().createQuery(hql).list();
		
		return cell;
	}
	public List<ZhiJianItem> findZhiJian(String templateId) {
		String hql = "from ZhiJianItem where 1=1 and state!='1' and templeProducingCell='" + templateId + "'";
		
		List<ZhiJianItem> zhijian = getSession().createQuery(hql).list();
		
		return zhijian;
	}
	public List<ZhiJianItem> findZhiJianNewById(String templateId,String code,String stepNum) {
		String hql = "from ZhiJianItem where 1=1  and state!='1' and templeProducingCell " + templateId + "  and code='"+code+"' and itemId='"+stepNum+"' ";
		
		List<ZhiJianItem> zhijian = getSession().createQuery(hql).list();
		
		return zhijian;
	}
	public List<ZhiJianItem> findCellZhiJianAll(String templateId,String itemId) {
		String hql = "from ZhiJianItem  where 1=1  and state!='1' and templeProducingCell " + templateId + " and  itemId='"+itemId+"' group by code  ";
		
		List<ZhiJianItem> ZhiJianItem = getSession().createQuery(hql).list();
		
		return ZhiJianItem;
	}
	public Long findZhiJianCount(String templateId,String code) {
		String countHql = "select count(*) from ZhiJianItem where 1=1 and state!='1' and templeProducingCell='" + templateId + "' and code ='"+code+"'";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		return sumCount;
	}

	public List<SampleOrder> findSampleOrders(String batch) {
		String hql = "from SampleOrder where 1=1 and barcode='" + batch + "' and state='1' and stateName='实际订单' ";
		  
		List<SampleOrder> cell = getSession().createQuery(hql).list();

		return cell;
	}
	public List<CellPassageQualityItem> findQualityTestSampleByCode(String code) {
		String hql = "from CellPassageQualityItem where 1=1 and sampleNumber='"+code+"'";
		
		List<CellPassageQualityItem> cellPassageQualityItems = getSession().createQuery(hql).list();
		
		return cellPassageQualityItems;
	}

	public List<CellPassageItemCheckCode> findCellPassageItemCheckCodess(String codes) {
		String hql = "from CellPassageItemCheckCode where 1=1 and code='" + codes + "' ";
		  
		List<CellPassageItemCheckCode> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CellPassageItemCheckCode> sampleCodeConfirm(String id, String stepNum, String sampleCodeConfirm, String type) {
		String hql = "from CellPassageItemCheckCode where 1=1 and code='" + sampleCodeConfirm + "' and cellPassage.id='"+id+"' and stepNum='"+stepNum+"' ";
		  
		List<CellPassageItemCheckCode> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<StorageOutItem> getfindStorageOutItems1(Storage st, String batch) {
		String hql = "from StorageOutItem where 1=1 and storage.id='"+st.getId()+"' and (usedFinish is null or usedFinish='0') and storageOut.state='1' and  productMark='"+batch+"'   ";
		  
		List<StorageOutItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	public List<CellProductionRecord> fidnCellProductionRecordByCellId(String id, String orderNum) {
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='"+id+"' and orderNum='"+orderNum+"' ";
		
		List<CellProductionRecord> cell = getSession().createQuery(hql).list();
		
		return cell;
	}


	public List<CellPassageTemp> backSample(String s) {
		String hql = "from CellPassageTemp where 1=1 and batch='"+s+"'";
		List<CellPassageTemp> cell = getSession().createQuery(hql).list();
		return cell;
	}

	public CellPassageTemp choseSampleCode(String id) {
		String hql = "from CellPassageTemp where 1=1 and code='"+id+"' ";
		CellPassageTemp cell = (CellPassageTemp) this.getSession().createQuery(hql).uniqueResult();
		return cell;
	}

	public List<CellPassageTemp> queryBatch(String batch) {
		String hql = "from CellPassageTemp where 1=1 and batch='"+batch+"'";
		List<CellPassageTemp> cell = getSession().createQuery(hql).list();
		return cell;
	}


	public List<CellPassageRoomPreparat> findCellPassageRoomPreparats(String id, String orderNum) {
		String hql = "from CellPassageRoomPreparat where 1=1 and cellPassage.id='"+id+"' and orderNum='"+orderNum+"' ";
		  
		List<CellPassageRoomPreparat> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CleanAreaDustItem> findCleanAreaDustItems(String operatingRoomId, String estimatedDateTime) {
		String hql = "from CleanAreaDustItem where 1=1 and cleanAreaDust.state='1' and cleanAreaDust.testDate<='"+estimatedDateTime+"' and roomNum='"+operatingRoomId+"' order by cleanAreaDust.id desc ";
		  
		List<CleanAreaDustItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<CleanAreaDustItem> findCleanAreaDustItem1s(String operatingRoomId, String id) {
		String hql = "from CleanAreaDustItem where 1=1 and cleanAreaDust.state='1' and cleanAreaDust.id='"+id+"' and roomNum='"+operatingRoomId+"' and noteCleanZone='否' order by cleanAreaDust.id desc ";
		  
		List<CleanAreaDustItem> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CleanAreaMicrobeItem> findCleanAreaMicrobeItems(String operatingRoomId, String estimatedDateTime) {
		String hql = "from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.state='1' and cleanAreaMicrobe.testDate<='"+estimatedDateTime+"' and roomNum='"+operatingRoomId+"' order by cleanAreaMicrobe.id desc ";
		  
		List<CleanAreaMicrobeItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<CleanAreaMicrobeItem> findCleanAreaMicrobeItem1s(String operatingRoomId, String id) {
		String hql = "from CleanAreaMicrobeItem where 1=1 and cleanAreaMicrobe.state='1' and cleanAreaMicrobe.id='"+id+"' and roomNum='"+operatingRoomId+"' and result='否' order by cleanAreaMicrobe.id desc ";
		  
		List<CleanAreaMicrobeItem> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CleanAreaVolumeItem> findCleanAreaVolumeItems(String operatingRoomId, String estimatedDateTime) {
		String hql = "from CleanAreaVolumeItem where 1=1 and cleanAreaVolume.state='1' and cleanAreaVolume.testDate<='"+estimatedDateTime+"' and roomNum='"+operatingRoomId+"' order by cleanAreaVolume.id desc ";
		  
		List<CleanAreaVolumeItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<CleanAreaVolumeItem> findCleanAreaVolumeItem1s(String operatingRoomId, String id) {
		String hql = "from CleanAreaVolumeItem where 1=1 and cleanAreaVolume.state='1' and cleanAreaVolume.id='"+id+"' and roomNum='"+operatingRoomId+"' and dieligibilityCriteria='否' order by cleanAreaVolume.id desc ";
		  
		List<CleanAreaVolumeItem> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CleanAreaBacteriaItem> findCleanAreaBacteriaItems(String operatingRoomId, String estimatedDateTime) {
		String hql = "from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.state='1' and cleanAreaBacteria.testDate<='"+estimatedDateTime+"' and roomNum='"+operatingRoomId+"' order by cleanAreaBacteria.id desc ";
		  
		List<CleanAreaBacteriaItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<CleanAreaBacteriaItem> findCleanAreaBacteriaItem1s(String operatingRoomId, String id) {
		String hql = "from CleanAreaBacteriaItem where 1=1 and cleanAreaBacteria.state='1' and cleanAreaBacteria.id='"+id+"' and roomNum='"+operatingRoomId+"' and result='否' order by cleanAreaBacteria.id desc ";
		  
		List<CleanAreaBacteriaItem> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CleanAreaMicroorganismItem> findCleanAreaMicroorganismItems(String operatingRoomId,
			String estimatedDateTime) {
		String hql = "from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.state='1' and cleanAreaMicroorganism.testTime<='"+estimatedDateTime+"' and roomNum='"+operatingRoomId+"' order by cleanAreaMicroorganism.id desc ";
		  
		List<CleanAreaMicroorganismItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<CleanAreaMicroorganismItem> findCleanAreaMicroorganismItem1s(String operatingRoomId,
			String id) {
		String hql = "from CleanAreaMicroorganismItem where 1=1 and cleanAreaMicroorganism.state='1' and cleanAreaMicroorganism.id='"+id+"' and roomNum='"+operatingRoomId+"' and result='否' order by cleanAreaMicroorganism.id desc ";
		  
		List<CleanAreaMicroorganismItem> cell = getSession().createQuery(hql).list();

		return cell;
	}

	public List<CleanAreaDiffPressureItem> findCleanAreaDiffPressureItems(String operatingRoomId,
			String estimatedDateTime) {
		String hql = "from CleanAreaDiffPressureItem where 1=1 and cleanAreaDiffPressure.state='1' and cleanAreaDiffPressure.testTime<='"+estimatedDateTime+"' and roomNum='"+operatingRoomId+"' order by cleanAreaDiffPressure.id desc ";
		  
		List<CleanAreaDiffPressureItem> cell = getSession().createQuery(hql).list();

		return cell;
	}
	
	public List<CleanAreaDiffPressureItem> findCleanAreaDiffPressureItem1s(String operatingRoomId,
			String id) {
		String hql = "from CleanAreaDiffPressureItem where 1=1 and cleanAreaDiffPressure.state='1' and cleanAreaDiffPressure.id='"+id+"' and roomNum='"+operatingRoomId+"' and result='否' order by cleanAreaDiffPressure.id desc ";
		  
		List<CleanAreaDiffPressureItem> cell = getSession().createQuery(hql).list();

		return cell;
	}


}