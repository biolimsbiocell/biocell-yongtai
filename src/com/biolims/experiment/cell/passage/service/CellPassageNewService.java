package com.biolims.experiment.cell.passage.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.IncubatorSampleInfoIn;
import com.biolims.equipment.model.IncubatorSampleInfoOut;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageAbnormal;
import com.biolims.experiment.cell.passage.model.CellPassageBeforeReagent;
import com.biolims.experiment.cell.passage.model.CellPassageCos;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageItemCheckCode;
import com.biolims.experiment.cell.passage.model.CellPassagePreparat;
import com.biolims.experiment.cell.passage.model.CellPassagePreparatEnd;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageReagent;
import com.biolims.experiment.cell.passage.model.CellPassageRoomPreparat;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellPassageZhiJian;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProducResults;
import com.biolims.experiment.cell.passage.model.CellProductionCompleted;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellZhiJian;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.cell.passage.model.CellproductReagent;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteriaItem;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressureItem;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobeItem;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganismItem;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolumeItem;
import com.biolims.experiment.quality.model.QualityTestItem;
import com.biolims.experiment.production.dao.ProductionPlanDao;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleBeforeReagent;
import com.biolims.system.template.model.TempleFinished;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional
public class CellPassageNewService {

	@Resource
	private CellPassageDao cellPassageDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonService commonService;
	@Resource
	private ProductionPlanDao productionPlanDao;

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId, String templateId, String logInfo,
			String place, String infection) throws Exception {

		User user1 = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		String id = "";
		int amount = 0;
		String planTime = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			CellPassage pt = new CellPassage();
			pt = (CellPassage) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			// 时间格式化
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Template t = commonDAO.get(Template.class, templateId);
			String log = "";
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				String modelName = "CellPassage";
				String markCode = "CP";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				log = "123";
				String batch = "";
				if (tempId != null) {
					for (String temp : tempId) {
						CellPassageTemp cpt = cellPassageDao.get(CellPassageTemp.class, temp);
						if (cpt != null) {
							if (cpt.getBatch() != null && !"".equals(cpt.getBatch())) {
								batch = cpt.getBatch();
								pt.setBatch(batch);
								pt.setPlace(place);
								pt.setInfectionType(infection);
								SampleOrder so = new SampleOrder();
								so = productionPlanDao.getSampleOrderBybatch(batch);
								if (so != null) {
									pt.setPatientName(so.getName());
									pt.setFiltrateCode(so.getFiltrateCode());
//									if((!"".equals(so.getHepatitisHbv())&&so.getHepatitisHbv()!=null)&&(!"".equals(so.getHepatitisHcv())&&so.getHepatitisHcv()!=null)) {
									if ("0".equals(so.getHepatitisHbv()) && !"0".equals(so.getHepatitisHcv())) {
										pt.setInfectionType("Hcv");
									}
									if (!"0".equals(so.getHepatitisHbv()) && "0".equals(so.getHepatitisHcv())) {
										pt.setInfectionType("Hbv");
									}
									if ("0".equals(so.getHepatitisHbv()) && "0".equals(so.getHepatitisHcv())) {
										pt.setInfectionType("无感染");
									}
									if ("1".equals(so.getHepatitisHbv()) && "1".equals(so.getHepatitisHcv())) {
										pt.setInfectionType("Hbv,Hcv");
									}
//									}
								}
								so.setBatchState("2");
								so.setBatchStateName("正在生产");
								commonDAO.merge(so);
							}
						}
					}

					String codes = "";

					List<TemplateItem> tis = cellPassageDao.getTemplateItems(t, "1");
					List<SampleOrder> sos = cellPassageDao.findSampleOrders(pt.getBatch());
					String printLabelCoding = tis.get(0).getPrintLabelCoding();
					if (sos.size() > 0 && tis.size() > 0) {
						codes = sos.get(0).getAbbreviation() + "-" + sos.get(0).getFiltrateCode() + "-"
								+ sos.get(0).getProductId() + "-" + sos.get(0).getRandomCode() + "-"
								+ sos.get(0).getRound() + "-" + printLabelCoding;

					}

					String id1 = "";
					for (int i = 0; i < tempId.length; i++) {
						id1 = tempId[0];
					}
//
//					CellPassageItem item = commonDAO.get(CellPassageItem.class, id1);
//					CellPassageItem cloneItem = new CellPassageItem();
//					String orderCode = "";
//					if (item != null) {
//
//						orderCode = item.getBatch();
//						cloneItem = cellPassageDao.getCellPassageItems(item.getCellPassage().getId(), item.getStepNum(),
//								"1");
//
//					}
////					CellPassageItem item = commonDAO.get(CellPassageItem.class, id1);
////					String orderCode = item.getBatch();
////					CellPassageItem cloneItem = cellPassageDao.getCellPassageItems(item.getCellPassage().getId(), item.getStepNum(),
////							"1");
//					
//					CellPassageItem item = commonDAO.get(CellPassageItem.class, id1);
//				     CellPassageItem cloneItem = new CellPassageItem();
//				     String orderCode="";
//				     if(item!=null) {
//				     
//				      orderCode = item.getBatch();
//				     cloneItem= cellPassageDao.getCellPassageItems(item.getCellPassage().getId(), item.getStepNum(),
//				       "1");
//				     
//				     }
////					CellPassageItem item = commonDAO.get(CellPassageItem.class, id1);
////					String orderCode = item.getBatch();
////					CellPassageItem cloneItem = cellPassageDao.getCellPassageItems(item.getCellPassage().getId(), item.getStepNum(),
////							"1");
					CellPassageTemp item = commonDAO.get(CellPassageTemp.class, id1);
					CellPassageItem cloneItem = cellPassageDao.getCellPassageItems(id, "1", "1");
					String orderCode = "";
					if (item != null) {
						orderCode = item.getBatch();
					}
					if (cloneItem != null) {

					} else {
//						cloneItem = (CellPassageItem) item.clone();
						cloneItem = new CellPassageItem();
						cloneItem.setId(null);
						cloneItem.setSampleType(null);
						cloneItem.setCode(codes);
						cloneItem.setSampleCode(orderCode);
						cloneItem.setBlendState("1");

						cloneItem.setStepNum("1");// 步骤数
//						pti.setBlendState("0");// 没混合
						cloneItem.setOrderCode(item.getOrderCode());// 订单编号
//						pti.setSampleType(ptt.getSampleType());// 样本类型
//						if (ptt.getSampleNum() != null) {
//							pti.setSampleNum(Double.valueOf(ptt.getSampleNum()));// 样本数量
//						} else {
						cloneItem.setSampleNum(null);
//						}
						cloneItem.setPatientName(item.getPatientName());// 姓名
						cloneItem.setParentId(item.getParentId());
						cloneItem.setPronoun(item.getPronoun());
						cloneItem.setParentId(item.getParentId());
						cloneItem.setSampleCode(item.getSampleCode());
						cloneItem.setProductId(item.getProductId());
						cloneItem.setProductName(item.getProductName());
//						pti.setDicSampleTypeId(
//								t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
//										? t.getDicSampleType().getId()
//										: t.getDicSampleTypeId());
//						pti.setDicSampleTypeName(
//								t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
//										? t.getDicSampleType().getName()
//										: t.getDicSampleTypeName());
						cloneItem.setProductName(item.getProductName());
//						pti.setProductNum(
//								t.getProductNum() != null && !"".equals(t.getProductNum())
//										? t.getProductNum()
//										: t.getProductNum1());
//						pti.setCode(ptt.getCode());
//						if (t.getStorageContainer() != null) {
//							pti.setState("1");
//						} else {
//							pti.setState("2");
//						}
//						ptt.setState("2");
//						pti.setTempId(temp);
						cloneItem.setCellPassage(pt);
//						cloneItem.setChromosomalLocation(mpi.getChromosomalLocation());
						cloneItem.setSampleInfo(item.getSampleInfo());
						if (item.getSampleOrder() != null) {
							cloneItem.setOrderId(item.getOrderId());
							cloneItem.setSampleOrder(commonDAO.get(SampleOrder.class, item.getSampleOrder().getId()));
						}
						cloneItem.setBatch(item.getBatch());

//							IncubatorSampleInfo isi = cellPassageDao
//									.getIncubatorSampleInfoByCountsPosid(item.getCounts(), item.getPosId());
//							if (isi != null) {
//								Instrument ii = commonDAO.get(Instrument.class, item.getCounts());
//
//								isi.setState("0");
//								commonDAO.saveOrUpdate(isi);
//
//								IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
//								isio.setId(null);
//								isio.setBatch(isi.getBatch());
//								isio.setIncubatorId(isi.getIncubatorId());
//								isio.setLocation(isi.getLocation());
//								isio.setSampleOutDate(new Date());
//								isio.setSampleOutUser(user1);
//								isio.setTaskId(item.getCellPassage().getId());
//								isio.setTaskStepNum(String.valueOf(item.getStepNum()));
//								cellPassageDao.saveOrUpdate(isio);
//
//								ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);
//
//								commonDAO.saveOrUpdate(ii);
//							}
//
//							item.setInventoryStatus("1");
//						}
//							IncubatorSampleInfo isi = cellPassageDao.getIncubatorSampleInfoByCountsPosid(item.getCounts(),
//									item.getPosId());
//							if (isi != null) {
//								Instrument ii = commonDAO.get(Instrument.class, item.getCounts());
//
//								isi.setState("0");
//								commonDAO.saveOrUpdate(isi);
//
//								IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
//								isio.setId(null);
//								isio.setBatch(isi.getBatch());
//								isio.setIncubatorId(isi.getIncubatorId());
//								isio.setLocation(isi.getLocation());
//								isio.setSampleOutDate(new Date());
//								isio.setSampleOutUser(user1);
//								isio.setTaskId(item.getCellPassage().getId());
//								isio.setTaskStepNum(String.valueOf(item.getStepNum()));
//								cellPassageDao.saveOrUpdate(isio);
//
//								ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);
//
//								commonDAO.saveOrUpdate(ii);
//							}
//
//							item.setInventoryStatus("1");
//						}
//						if (item.getInstrument() != null && item.getCounts() != null && item.getPosId() != null
//								&& !"1".equals(item.getInventoryStatus())) {
//
//							IncubatorSampleInfo isi = cellPassageDao.getIncubatorSampleInfoByCountsPosid(item.getCounts(),
//									item.getPosId());
//							if (isi != null) {
//								Instrument ii = commonDAO.get(Instrument.class, item.getCounts());
//
//								isi.setState("0");
//								commonDAO.saveOrUpdate(isi);
//
//								IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
//								isio.setId(null);
//								isio.setBatch(isi.getBatch());
//								isio.setIncubatorId(isi.getIncubatorId());
//								isio.setLocation(isi.getLocation());
//								isio.setSampleOutDate(new Date());
//								isio.setSampleOutUser(user1);
//								isio.setTaskId(item.getCellPassage().getId());
//								isio.setTaskStepNum(String.valueOf(item.getStepNum()));
//								cellPassageDao.saveOrUpdate(isio);
//
//								ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);
//
//								commonDAO.saveOrUpdate(ii);
//							}
//
//							item.setInventoryStatus("1");
//						}
					}

					cloneItem.setInstrument(null);
					cloneItem.setPosId("");
					cloneItem.setCounts("");
					cloneItem.setInventoryStatus("");
					commonDAO.saveOrUpdate(cloneItem);

					
//					String codes = "";
//
//					List<TemplateItem> tis = cellPassageDao.getTemplateItems(t, "01");
//					List<SampleOrder> sos = cellPassageDao.findSampleOrders(pt.getBatch());
//
//					if (sos.size() > 0 && tis.size() > 0) {
//						codes = sos.get(0).getAbbreviation() + "-" + sos.get(0).getFiltrateCode() + "-"
//								+ sos.get(0).getProductId() + "-" + sos.get(0).getRandomCode() + "-"
//								+ sos.get(0).getRound() + "-" + tis.get(0).getPrintLabelCoding();
//
//					}
//
//					String codes = "";
//					
//					List<TemplateItem> tis = cellPassageDao.getTemplateItems(t,"01");
//					List<SampleOrder> sos = cellPassageDao.findSampleOrders(pt.getBatch());
//					
//					if(sos.size()>0 && tis.size()>0) {
//						codes = sos.get(0).getAbbreviation()+"-"+sos.get(0).getFiltrateCode()
//								+"-"+sos.get(0).getProductId()+"-"+sos.get(0).getRandomCode()+"-"
//								+sos.get(0).getRound()+"-"+tis.get(0).getPrintLabelCoding();
//						
//					}
//					

					List<CellPassageItemCheckCode> ciccs = cellPassageDao.findCellPassageItemCheckCodess(codes);
					if (ciccs.size() > 0) {

					} else {
						CellPassageItemCheckCode cicc2 = new CellPassageItemCheckCode();
						cicc2.setId(null);
						cicc2.setCellPassage(pt);

						cicc2.setCode(codes);
						cicc2.setBlendState("1");
						cicc2.setState("未确认");
						cicc2.setStepNum("1");
						commonDAO.saveOrUpdate(cicc2);
					}
					
					String productCodes = "";
					List<TempleProducingCell> templeProductingCell = cellPassageDao.getTempleProductingCell(t.getId(),
							"1");
					if (templeProductingCell != null) {
						String s = "";
						for (TempleProducingCell templeProducingCell : templeProductingCell) {
							if ("".equals(s)) {
								s += "'" + templeProducingCell.getId() + "'";
							} else {
								s += "," + "'" + templeProducingCell.getId() + "'";
							}

						}
						String aa = "in (" + s + ")";
						List<ZhiJianItem> findCellZhiJianAll = cellPassageDao.findCellZhiJianAll(aa, "1");
						for (ZhiJianItem zhiJianItem : findCellZhiJianAll) {
							List<ZhiJianItem> zhiJian = cellPassageDao.findZhiJianNewById(aa, zhiJianItem.getCode(),
									"1");
							for (int i = 1; i <= zhiJian.size(); i++) {

//						for (TempleProducingCell templeProducingCell : templeProductingCell) {
//							List<ZhiJianItem> findZhiJian = cellPassageDao.findZhiJian(templeProducingCell.getId());
//							if(!findZhiJian.isEmpty()) {
//							for (ZhiJianItem zhiJian : findZhiJian) {

								SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, zhiJian.get(i - 1).getCode());
								DicSampleType dd = commonDAO.get(DicSampleType.class,
										zhiJian.get(i - 1).getSampleTypeId());

								// 质检条形码
								// 姓名缩写-筛选好-监测项目-随机号-采血轮次-生产工序-监测项缩写
								if (sos.size() > 0 && tis.size() > 0) {
									productCodes = sos.get(0).getAbbreviation() + "-" + sos.get(0).getFiltrateCode()
											+ "-" + sos.get(0).getProductId() + "-" + sos.get(0).getRandomCode() + "-"
											+ sos.get(0).getRound() + "-" + printLabelCoding + "-"
											+ sd.getDeteyionName() + "-" + i;
								}

								String scgxdm = tis.get(0).getPrintLabelCoding();
								String jcid = sd.getDeteyionName();
								String ybbh = dd.getCode();

								int a = cloneItem.getCode().lastIndexOf("-");
								String b = cloneItem.getCode().substring(0, a + 1);
								String c = b + scgxdm + ybbh + jcid;

								CellPassageQualityItem cpqi = new CellPassageQualityItem();
								/** 提交表编码 */
								cpqi.setTempId(cloneItem.getId());
								/** 样本编号 */
//								cpqi.setCode(cloneItem.getCode());
								cpqi.setSampleNumber(productCodes);
								/** 批次号 */
								cpqi.setBatch(cloneItem.getBatch());
								/** 订单编号 */
								cpqi.setOrderCode(cloneItem.getOrderCode());
								/** 检测项目 */
								cpqi.setProductId(cloneItem.getProductId());
								/** 检测项目 */
								cpqi.setProductName(cloneItem.getProductName());
								/** 状态 */
								cpqi.setState("1");
//								/** 备注 */
//								cpqi.setNote(jcbz);
								/** 浓度 */
								cpqi.setConcentration(null);
								/** 样本数量 */
								if (!"".equals(zhiJian.get(i - 1).getSampleNum())
										&& zhiJian.get(i - 1).getSampleNum() != null) {
									Double jcsls = Double.valueOf(zhiJian.get(i - 1).getSampleNum());
									cpqi.setSampleNum(jcsls);
								}

								/** 检测项数量单位 */
								cpqi.setSampleNumUnit(zhiJian.get(i - 1).getSampleNumUnit());
								/** 相关主表 */
								cpqi.setCellPassage(pt);
								/** 体积 */
								cpqi.setVolume(null);
								/** 任务单id */
								cpqi.setOrderId(pt.getId());
								/** 中间产物数量 */
								cpqi.setProductNum(null);
								/** 中间产物类型编号 */
								cpqi.setDicSampleTypeId(null);
								/** 中间产物类型 */
								cpqi.setDicSampleTypeName(null);
								/** 样本类型 */
								cpqi.setSampleType(dd.getName());
								/** 样本主数据 */
								cpqi.setSampleInfo(cloneItem.getSampleInfo());
								/** 实验的步骤号 */
								cpqi.setStepNum("1");
								/** 实验步骤名称 */
								cpqi.setExperimentalStepsName(tis.get(0).getName());
								/** 检测项 */
								cpqi.setSampleDeteyion(sd);
								/** 关联订单 */
								cpqi.setSampleOrder(cloneItem.getSampleOrder());
								/** 是否合格 */
								cpqi.setQualified(null);
								/** 质检结果表id */
								cpqi.setQualityInfoId(null);
								/** 质检是否接收 */
								cpqi.setQualityReceive(null);
								/** 质检提交时间 */
								cpqi.setQualitySubmitTime(null);
								/** 质检接收时间 */
								cpqi.setQualityReceiveTime(null);
								/** 质检完成时间 */
								cpqi.setQualityFinishTime(null);
								/** 质检提交人 */
								cpqi.setQualitySubmitUser(null);
								/** 质检接收人 */
								cpqi.setQualityReceiveUser(null);
								/** 质检是否提交 */
								cpqi.setSubmit("0");
//								cpqi.setProductCode(productCodes);

								String ct = "";
								if ("zj01".equals(zhiJian.get(i - 1).getTypeId())) {// 自主过程干细胞检测
									ct = "自主检测";
									cpqi.setCellType("1");

								} else if ("zj02".equals(zhiJian.get(i - 1).getTypeId())) {// 第三方检测
									ct = "第三方检测";
									cpqi.setCellType("5");
								}
								commonDAO.saveOrUpdate(cpqi);

								CellPassageItemCheckCode cicc2QC = new CellPassageItemCheckCode();
								cicc2QC.setId(null);
								cicc2QC.setCellPassage(pt);

								cicc2QC.setCode(productCodes);
								cicc2QC.setBlendState("1");
								cicc2QC.setState("未确认");
								cicc2QC.setStepNum("1");
								commonDAO.saveOrUpdate(cicc2QC);

							}

						}

					}

				}

				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				pt.setTemplate(t);

				saveTemplate(pt);
				// 算预计时间 第一次创建的生产的时候固定 计划完成时间 创建第一步
				List<TemplateItem> temList = templateService.getTemplateItemListPage(t.getId());
				if (temList.size() > 0) {
					for (int n = 0; n < temList.size(); n++) {
						CellProductionRecord cellProduc = new CellProductionRecord();
						cellProduc.setEstimatedDate(temList.get(n).getEstimatedTime());
						cellProduc.setPlanWorkDate(ObjectToMapUtils.getTimeString());
						cellProduc.setCellPassage(pt);
						cellProduc.setTempleItem(temList.get(n));
						if (!"".equals(temList.get(n).getEstimatedTime())) {
							Calendar calendar1 = Calendar.getInstance();
							SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
							if (n == 0) {
								String three_days_ago = sdf1.format(calendar1.getTime());
								cellProduc.setPlanWorkDate(three_days_ago);//
							} else {
								calendar1.add(Calendar.DATE, amount);
								String three_days_ago = sdf1.format(calendar1.getTime());
								cellProduc.setPlanWorkDate(three_days_ago);//
							}
							amount += Integer.parseInt(temList.get(n).getEstimatedTime());
						}
						cellProduc.setOrderNum(String.valueOf(temList.get(n).getOrderNum()));
						commonDAO.saveOrUpdate(cellProduc);

						// 工前准备 试剂表
						// 查询该大步骤下生产步骤
						List<TempleProducingCell> temCell = templateService.findByIdTempleProducingCells(
								temList.get(n).getTemplate().getId(), String.valueOf(temList.get(n).getOrderNum()));
						if (temCell.size() > 0) {
							String bzids = "";
							for (TempleProducingCell tpc : temCell) {
								if ("".equals(bzids)) {
									bzids = bzids + "'" + tpc.getId() + "'";
								} else {
									bzids = bzids + ",'" + tpc.getId() + "'";
								}
							}
							List<TempleProducingReagent> regenLis = templateService
									.getTempleProducingReagentListPages(bzids);
							if (regenLis.size() > 0) {
								for (TempleProducingReagent tpr : regenLis) {
									CellPassageBeforeReagent cpbr = new CellPassageBeforeReagent();
									cpbr.setId(null);
									cpbr.setCellPassage(pt);
									cpbr.setStepNum(String.valueOf(temList.get(n).getOrderNum()));

									cpbr.setReagentId(tpr.getCode());
									cpbr.setReagentName(tpr.getName());
									cpbr.setAmount(tpr.getAmount());
									if (!"".equals(tpr.getCode())) {
										Storage ssg = commonDAO.get(Storage.class, tpr.getCode());
										if (ssg != null && ssg.getProducer() != null) {
											cpbr.setReagentManufactor(ssg.getProducer().getName());
										}
										if (ssg != null && ssg.getSpec() != null) {
											cpbr.setReagentSpecifications(ssg.getSpec());
										}
//										if(ssg!=null&&ssg.getUnitGroup()!=null&&ssg.getUnitGroup().getMark2()!=null) {
//											cpbr.setReagentUnit(ssg.getUnitGroup().getMark2().getName());
//										}
										if (ssg != null && ssg.getOutUnitGroup() != null
												&& ssg.getOutUnitGroup().getMark2() != null) {
											cpbr.setReagentUnit(ssg.getOutUnitGroup().getMark2().getName());
										}
									}

									cpbr.setReagentNo("");
									cpbr.setReagentValidityTime("");
									List<TempleBeforeReagent> trs = templateService
											.getTempleBeforeReagents(temList.get(n).getId(), tpr.getCode());
									if (trs.size() > 0) {
										cpbr.setReagentNum(trs.get(0).getReagentNum());
									}
									commonDAO.saveOrUpdate(cpbr);
								}
							}
						}

						// 明细赋值
						if (tempId != null) {
							if (pt.getTemplate() != null) {
								for (String temp : tempId) {
									CellPassageTemp ptt = cellPassageDao.get(CellPassageTemp.class, temp);
									if ("1".equals(t.getIsSeparate())) {
										List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
												.getMappingPrimersLibraryItem(ptt.getProductId());
										for (MappingPrimersLibraryItem mpi : listMPI) {
											if ("1".equals(String.valueOf(temList.get(n).getOrderNum()))) {
												CellPassageItem pti = new CellPassageItem();
												pti.setStepNum(String.valueOf(temList.get(n).getOrderNum()));// 步骤数
												pti.setBlendState("0");// 没混合
												pti.setOrderCode(ptt.getOrderCode());// 订单编号
												pti.setSampleType(ptt.getSampleType());// 样本类型
												if (ptt.getSampleNum() != null) {
													pti.setSampleNum(Double.valueOf(ptt.getSampleNum()));// 样本数量
												} else {
													pti.setSampleNum(null);
												}
												pti.setPatientName(ptt.getPatientName());// 姓名
												pti.setParentId(ptt.getParentId());
												pti.setPronoun(ptt.getPronoun());
												pti.setParentId(ptt.getParentId());
												pti.setSampleCode(ptt.getSampleCode());
												pti.setProductId(ptt.getProductId());
												pti.setProductName(ptt.getProductName());
												pti.setDicSampleTypeId(
														t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
																? t.getDicSampleType().getId()
																: t.getDicSampleTypeId());
												pti.setDicSampleTypeName(
														t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
																? t.getDicSampleType().getName()
																: t.getDicSampleTypeName());
												pti.setProductName(ptt.getProductName());
												pti.setProductNum(
														t.getProductNum() != null && !"".equals(t.getProductNum())
																? t.getProductNum()
																: t.getProductNum1());
												pti.setCode(ptt.getCode());
												if (t.getStorageContainer() != null) {
													pti.setState("1");
												} else {
													pti.setState("2");
												}
												ptt.setState("2");
												pti.setTempId(temp);
												pti.setCellPassage(pt);
												pti.setChromosomalLocation(mpi.getChromosomalLocation());
												pti.setSampleInfo(ptt.getSampleInfo());
												if (ptt.getSampleOrder() != null) {
													pti.setSampleOrder(commonDAO.get(SampleOrder.class,
															ptt.getSampleOrder().getId()));
												}
												pti.setBatch(ptt.getBatch());
												cellPassageDao.saveOrUpdate(pti);

												CellPassageItemCheckCode cicc = new CellPassageItemCheckCode();
												cicc.setId(null);
												cicc.setBlendState("0");
												cicc.setCellPassage(pti.getCellPassage());
												cicc.setCode(pti.getCode());
												cicc.setState("未确认");
												cicc.setStepNum("1");
												commonDAO.saveOrUpdate(cicc);
											}
										}
									} else {
										if ("1".equals(String.valueOf(temList.get(n).getOrderNum()))) {
											CellPassageItem pti = new CellPassageItem();
											pti.setStepNum(String.valueOf(temList.get(n).getOrderNum()));// 步骤数
											pti.setBlendState("0");// 没混合
											pti.setOrderCode(ptt.getOrderCode());// 订单编号
											pti.setSampleType(ptt.getSampleType());// 样本类型
											if (ptt.getSampleNum() != null) {
												pti.setSampleNum(Double.valueOf(ptt.getSampleNum()));// 样本数量
											} else {
												pti.setSampleNum(null);
											}
											pti.setPatientName(ptt.getPatientName());// 姓名
											pti.setParentId(ptt.getParentId());
											pti.setPronoun(ptt.getPronoun());
											pti.setParentId(ptt.getParentId());
											pti.setSampleCode(ptt.getSampleCode());
											pti.setCode(ptt.getCode());
											pti.setOrderId(ptt.getOrderId());
											pti.setPronoun(ptt.getPronoun());
											pti.setProductId(ptt.getProductId());
											pti.setProductName(ptt.getProductName());
											pti.setDicSampleTypeId(
													t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
															? t.getDicSampleType().getId()
															: t.getDicSampleTypeId());
											pti.setProductNum(t.getProductNum() != null && !"".equals(t.getProductNum())
													? t.getProductNum()
													: t.getProductNum1());
											pti.setDicSampleTypeName(
													t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
															? t.getDicSampleType().getName()
															: t.getDicSampleTypeName());
											pti.setCode(ptt.getCode());
											pti.setSampleInfo(ptt.getSampleInfo());
											if (t.getStorageContainer() != null) {
												pti.setState("1");
											} else {
												pti.setState("2");
											}
											ptt.setState("2");
											pti.setTempId(temp);
											pti.setCellPassage(pt);
											if (ptt.getSampleOrder() != null) {
												pti.setSampleOrder(
														commonDAO.get(SampleOrder.class, ptt.getSampleOrder().getId()));
											}
											pti.setBatch(ptt.getBatch());
											cellPassageDao.saveOrUpdate(pti);

											CellPassageItemCheckCode cicc = new CellPassageItemCheckCode();
											cicc.setId(null);
											cicc.setBlendState("0");
											cicc.setCellPassage(pti.getCellPassage());
											cicc.setCode(pti.getCode());
											cicc.setState("未确认");
											cicc.setStepNum("1");
											commonDAO.saveOrUpdate(cicc);
										}
									}
									cellPassageDao.saveOrUpdate(ptt);
								}
							}
						}
						String planWorkDate = cellProduc.getPlanWorkDate();
						if (n < temList.size() - 1) {
							planTime += planWorkDate + ",";
						} else {
							planTime += planWorkDate;
						}
					}
				}

				pt.setPlanWorkDate(planTime);
				// 保存生产操作指令 --模板
				List<TempleNstructions> temNs = templateService.getTempleNstructionsListPage(t.getId());
				if (temNs.size() > 0) {
					for (int m = 0; m < temNs.size(); m++) {
						CellPassagePreparat cellpa = new CellPassagePreparat();
						cellpa.setCellPassage(pt);
						cellpa.setOrderNum(temNs.get(m).getItemId());
						cellpa.setState("0");
						cellpa.setProductionInspection(temNs.get(m).getTempleProduction());
						cellpa.setOperationNotes(temNs.get(m).getOperationNote());
						cellpa.setTempleInsId(temNs.get(m));
						commonDAO.saveOrUpdate(cellpa);
					}
				}

				// 保存完成清场操作指令 --模板
				List<TempleNstructionsEnd> temNsEnd = templateService.getTempleNstructionsEndListPage(t.getId());
				if (temNsEnd.size() > 0) {
					for (int m = 0; m < temNsEnd.size(); m++) {
						CellPassagePreparatEnd cellpaEnd = new CellPassagePreparatEnd();
						cellpaEnd.setCellPassage(pt);
						cellpaEnd.setOrderNum(temNsEnd.get(m).getOrderNum());
						cellpaEnd.setState("0");
						cellpaEnd.setProductionInspection(temNsEnd.get(m).getTempleProduction());
						cellpaEnd.setOperationNotes(temNsEnd.get(m).getOperationNote());
						cellpaEnd.setTempleInsId(temNsEnd.get(m));
						commonDAO.saveOrUpdate(cellpaEnd);
					}
				}

				// 保存完工生产检查
				List<TempleFinished> templef = templateService.findByTempleIdFinishedInfo(t.getId());
				if (templef.size() > 0) {
					for (int n = 0; n < templef.size(); n++) {
						CellProductionCompleted cellcomple = new CellProductionCompleted();
						cellcomple.setCellPassage(pt);
						cellcomple.setTempleItem(templef.get(n));
						cellcomple.setOrderNum(templef.get(n).getOrderNum());
						cellcomple.setState("0");
						commonDAO.saveOrUpdate(cellcomple);
					}
				}
				// 保存第三步模板
				if (pt.getTemplate() != null) {
					saveCellPassageTems(pt);
				}
			} else {
				id = pt.getId();
				pt = commonDAO.get(CellPassage.class, id);

				String batch = "";
				if (tempId != null) {
					for (String temp : tempId) {
						CellPassageTemp cpt = cellPassageDao.get(CellPassageTemp.class, temp);
						if (cpt != null) {
							if (cpt.getBatch() != null && !"".equals(cpt.getBatch())) {
								batch = cpt.getBatch();
								pt.setBatch(batch);
							}
						}
					}
				}
				List<CellPassageItem> cpItems = commonService.get(CellPassageItem.class, "cellPassage.id", id);
				if (cpItems.size() > 0) {
					for (CellPassageItem cpi : cpItems) {
						if (cpi.getBatch() != null && !"".equals(cpi.getBatch())) {
							batch = cpi.getBatch();
							pt.setBatch(batch);
						}
					}
				}

				pt.setName(name);
				pt.setPlace(place);
				pt.setInfectionType(infection);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
				// 修改模板

			}

//			pt.setTestUserOneId(userId);
//			String[] users = userId.split(",");
//			String userName = "";
//			for (int i = 0; i < users.length; i++) {
//				if (i == users.length - 1) {
//					User user = commonDAO.get(User.class, users[i]);
//					userName += user.getName();
//				} else {
//					User user = commonDAO.get(User.class, users[i]);
//					userName += user.getName() + ",";
//				}
//			}
//			pt.setTestUserOneName(userName);
			cellPassageDao.saveOrUpdate(pt);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setClassName("CellPassage");
				li.setModifyContent(logInfo);
				if (!log.equals("") || log.equals("123")) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(CellPassage sc) throws ParseException {
		List<CellPassageTemplate> tlist2 = cellPassageDao.delTemplateItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		
		List<CellPassageRoomPreparat> tlist3 = cellPassageDao.delCellPassageRoomPreparat(sc.getId());
		commonDAO.deleteAll(tlist3);
		
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		// 定义一个增长的变量
		Integer i = 0;
		for (TemplateItem t : tlist) {
			CellPassageTemplate ptt = new CellPassageTemplate();
			if (i == 0) {
				ptt.setStepState("2");
			} else {
				ptt.setStepState("3");
			}
			Integer result = 0;
			if (t.getEstimatedTime() != null && !"".equals(t.getEstimatedTime())) {
				ptt.setEstimatedDate(t.getEstimatedTime());

				i += Integer.parseInt(t.getEstimatedTime());
				result = i - Integer.parseInt(t.getEstimatedTime());
				String createDate = sc.getCreateDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse = format.parse(createDate);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.DATE, result);
				parse = calendar.getTime();
				String date = format.format(parse);
				ptt.setPlanWorkDate(date);

				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse1 = format1.parse(createDate);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(calendar1.DATE, i);
				parse1 = calendar1.getTime();
				String date1 = format.format(parse1);
				ptt.setPlanEndDate(date1);
			}

			ptt.setCellPassage(sc);
			ptt.setOrderNum(String.valueOf(t.getOrderNum()));
			ptt.setName(t.getName());
//			ptt.setNote(t.getNote());
//			ptt.setBlend(t.getBlend());
//			ptt.setContent(t.getContent());
			cellPassageDao.saveOrUpdate(ptt);
			
			for(int zz=1;zz<=4;zz++) {
				CellPassageRoomPreparat crps = new CellPassageRoomPreparat();
				crps.setCellPassage(sc);
				crps.setState("0");
				crps.setOrderNum(ptt.getOrderNum());
				crps.setOrderTypeNum(zz+"");
				commonDAO.saveOrUpdate(crps);
			}
			
		}
	}

	/**
	 * @throws ParseException
	 * @Title: saveCellPassageTemplate @Description: 保存模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCellPassageTemplate(CellPassage sc) throws ParseException {
		List<CellPassageTemplate> tlist2 = cellPassageDao.delTemplateItem(sc.getId());
		List<CellPassageReagent> rlist2 = cellPassageDao.delReagentItem(sc.getId());
		List<CellPassageCos> clist2 = cellPassageDao.delCosItem(sc.getId());
		List<CellPassageZhiJian> zlist2 = cellPassageDao.delZhijianItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		commonDAO.deleteAll(zlist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate().getId(), null);
		List<ZhiJianItem> zlist = templateService.getZjItem(sc.getTemplate().getId(), null);
		for (ZhiJianItem t : zlist) {
			CellPassageZhiJian ptr = new CellPassageZhiJian();
			ptr.setCellPassage(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			ptr.setTypeId(t.getTypeId());
			ptr.setTypeName(t.getTypeName());
			cellPassageDao.saveOrUpdate(ptr);
		}

		// 定义一个增长的变量
		Integer i = 0;
		for (TemplateItem t : tlist) {
			CellPassageTemplate ptt = new CellPassageTemplate();
			Integer result = 0;
			if (t.getEstimatedTime() != null && !"".equals(t.getEstimatedTime())) {
				ptt.setEstimatedDate(t.getEstimatedTime());

				i += Integer.parseInt(t.getEstimatedTime());
				result = i - Integer.parseInt(t.getEstimatedTime());
				String createDate = sc.getCreateDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse = format.parse(createDate);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.DATE, result);
				parse = calendar.getTime();
				String date = format.format(parse);
				ptt.setPlanWorkDate(date);

				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse1 = format1.parse(createDate);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(calendar1.DATE, i);
				parse1 = calendar1.getTime();
				String date1 = format.format(parse1);
				ptt.setPlanEndDate(date1);
			}

			ptt.setCellPassage(sc);
			ptt.setOrderNum(String.valueOf(t.getOrderNum()));
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			cellPassageDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			CellPassageReagent ptr = new CellPassageReagent();
			ptr.setCellPassage(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			cellPassageDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			CellPassageCos ptc = new CellPassageCos();
			ptc.setCellPassage(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			cellPassageDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * @throws ParseException
	 * @Title: saveCellPassageTemplate @Description: 获取实验主表数据
	 * @author : @date @param sc void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public CellPassage findCellPassage(String id) {
		return cellPassageDao.get(CellPassage.class, id);
	}

	// 获取第一步的实验操作数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellPassagePreparat> findCellNstructionsInfo(String id, String orderNum) {
		return cellPassageDao.findCellNstructionsInfo(id, orderNum);
	}

	// 获取第一步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellProductionRecord> findProductionRecordInfo(String id, String orderNum) {
		return cellPassageDao.findProductionRecordInfo(id, orderNum);
	}

	// 获取第一步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellProducOperation> findCellProducOperation(String id, String orderNum) {
		return cellPassageDao.findCellProducOperation(id, orderNum);
	}

	// 获取第四步的实验生产检查 操作指令
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellPassagePreparatEnd> findCellPassagePreparatEndInfo(String id, String orderNum) {
		return cellPassageDao.findCellPassagePreparatEndInfo(id, orderNum);
	}

	// 获取第四步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellProductionCompleted> findCellProductionCompletedInfo(String id, String orderNum) {
		return cellPassageDao.findCellProductionCompletedInfo(id, orderNum);
	}

	/**
	 * 
	 * @Title: selectCellPassageTempTable @Description: 展示样本表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectCellPassageItemListPageTable(String id, String orderNum, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return cellPassageDao.selectCellPassageItemListPageTable(id, orderNum, start, length, query, col, sort);
	}

	/**
	 * @throws ParseException
	 * @Title: saveCellPassageTemplate @Description: 保存新模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCellPassageTems(CellPassage sc) throws ParseException {
		// 保存生产主表
		List<TempleProducingCell> temCell = templateService.findByIdTempleProducingCell(sc.getTemplate().getId());
		if (temCell.size() > 0) {
			for (int i = 0; i < temCell.size(); i++) {
				CellProducOperation cp = new CellProducOperation();
				TempleProducingCell tem = commonDAO.get(TempleProducingCell.class, temCell.get(i).getId());
				if (tem != null) {
					cp.setTempleCell(tem);
					cp.setCellPassage(sc);
					cp.setOrderNum(temCell.get(i).getOrderNum());
					cp.setCustomTest("{}");
					commonDAO.saveOrUpdate(cp);
				}
				// 保存 生产 试剂
				List<TempleProducingReagent> regenLis = templateService
						.getTempleProducingReagentListPage(temCell.get(i).getId());
				if (regenLis.size() > 0) {
					for (int n = 0; n < regenLis.size(); n++) {
						CellproductReagent cellReagent = new CellproductReagent();
						TempleProducingReagent templereagent = commonDAO.get(TempleProducingReagent.class,
								regenLis.get(n).getId());
						if (templereagent != null) {
							cellReagent.setTemplateReagent(templereagent);
							cellReagent.setState("0");
							cellReagent.setCellProduc(cp);
							commonDAO.saveOrUpdate(cellReagent);
						}
					}
				}
				// 保存 生产 质检
				List<ZhiJianItem> zhiJianItemList = templateService
						.getTempleProducingZhiJianItem(temCell.get(i).getId());
				if (zhiJianItemList.size() > 0) {
					for (int n = 0; n < zhiJianItemList.size(); n++) {
						CellZhiJian cellZhiJian = new CellZhiJian();
						ZhiJianItem zhiJianItem = commonDAO.get(ZhiJianItem.class, zhiJianItemList.get(n).getId());
						if (zhiJianItem != null) {
							cellZhiJian.setCode(zhiJianItem.getCode());
							cellZhiJian.setName(zhiJianItem.getName());
							cellZhiJian.setSampleNum(zhiJianItem.getSampleNum());
							cellZhiJian.setSampleNumUnit(zhiJianItem.getSampleNumUnit());
							cellZhiJian.setSampleType(zhiJianItem.getSampleType());
							cellZhiJian.setItem(zhiJianItem);
							cellZhiJian.setCellProduc(cp);
							commonDAO.saveOrUpdate(cellZhiJian);
						}
					}
				}

				// 保存 生产 设备
				List<TempleProducingCos> cosLis = templateService.getTempleProducingCosListPage(temCell.get(i).getId());
				if (cosLis.size() > 0) {
					for (int m = 0; m < cosLis.size(); m++) {
						CellproductCos cellCos = new CellproductCos();
						TempleProducingCos templerCos = commonDAO.get(TempleProducingCos.class, cosLis.get(m).getId());
						if (templerCos != null) {
							cellCos.setTemplateCos(templerCos);
							cellCos.setState("0");
							cellCos.setCellProduc(cp);
							commonDAO.saveOrUpdate(cellCos);
						}
					}
				}
			}

			// 第三步生产 保存实验结果
			CellProducResults produ = new CellProducResults();
			produ.setState("0");
			produ.setCellPassage(sc);
			produ.setTempleCell(temCell.get(0));
			produ.setOrderNum(temCell.get(0).getOrderNum());
			commonDAO.saveOrUpdate(produ);
		}
	}

	// 获取第三步的实验生产检查
	public List<CellProducOperation> findProductOperationListPage(String id, String orderNum, String templeId) {
		return cellPassageDao.findProductOperationListPage(id, orderNum, templeId);
	}

	// 获取第三步的生产操作 试剂
	public List<CellproductReagent> findProductRegentListPage(String id, String orderNum, String templeId) {
		return cellPassageDao.findProductRegentListPage(id, orderNum, templeId);
	}

	// 获取第三步的生产操作
	public List<CellproductCos> findCellproductCosListPage(String id, String orderNum, String templeId) {
		return cellPassageDao.findCellproductCosListPage(id, orderNum, templeId);
	}

	// 获取第三步的生产操作 操作结果
	public List<CellProducResults> findCellProducResultsListPage(String id, String orderNum, String templeId) {
		return cellPassageDao.findCellProducResultsListPage(id, orderNum, templeId);
	}

	// 获取第三步的实验生产检查
	public List<CellProducOperation> findByIdProductOperationList(String id) {
		return cellPassageDao.findByIdProductOperationList(id);
	}

	// 获取第三步的实验生产检查 试剂
	public List<CellproductReagent> findByIdProductRegentList(String id) {
		return cellPassageDao.findByIdProductRegentList(id);
	}

	// 获取第三步的实验生产检查 试剂
	public List<CellproductReagent> findByIdProductRegentList1(String id) {
		return cellPassageDao.findByIdProductRegentList1(id);
	}

	// 获取第三步的实验生产检查 设备
	public List<CellproductCos> findByIdCellproductCosList(String id) {
		return cellPassageDao.findByIdCellproductCosList(id);
	}

	// 获取第三步的实验生产检查 设备
	public List<CellproductCos> findByIdCellproductCosList1(String id) {
		return cellPassageDao.findByIdCellproductCosList1(id);
	}

	// 获取第三步的实验生产质检
	public List<CellZhiJian> findByIdCellproductZhiJianList1(String id) {
		return cellPassageDao.findByIdCellproductZhiJianList1(id);
	}

	// 获取第三步的实验生产检查
	public List<CellProducOperation> findByIdAndOrderNumProductOperation(String id, String orderNum) {
		return cellPassageDao.findByIdAndOrderNumProductOperation(id, orderNum);
	}

	// 获取第三步的实验生产检查 设备
	public List<CellproductCos> findByIdAndOrderNumCellproductCos(String id, String orderNum) {
		return cellPassageDao.findByIdAndOrderNumCellproductCos(id, orderNum);
	}

	// 获取第三步的 的生产结果 数据 以及开始时间和结束时间
	public List<CellProducResults> findCellProducResultsPage(String id, String orderNum) {
		return cellPassageDao.findCellProducResultsPage(id, orderNum);
	}

	/**
	 * @throws Exception @Title: findCellPrimaryCultureTemplateList @Description:
	 *                   TODO @author : nan.jiang @date 2018-8-30上午9:40:13 @param
	 *                   start @param length @param query @param col @param
	 *                   sort @param ids @return Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTempRecordList(Integer start, Integer length, String query,
			String col, String sort, String ids) throws Exception {
		return cellPassageDao.findCellPrimaryCultureTempRecordList(start, length, query, col, sort, ids);
	}

	public TemplateItem getBlendStateByStep(String id, String orderNum) {
		return cellPassageDao.getBlendStateByStep(id, orderNum);
	}

	public List<CellProducOperation> getCellProducOperationListPage(String id) {
		return cellPassageDao.getCellProducOperationListPage(id);
	}

	public CellPassageTemplate getCellPassageTemplateByCellAndStep(String stepNum, String mainId) {
		return cellPassageDao.getCellPassageTemplateByCellAndStep(stepNum, mainId);
	}

	public List<CellPassageItem> showWellPlate(String id) {
		List<CellPassageItem> list = cellPassageDao.showWellPlate(id);
		return list;
	}

	public List<IncubatorSampleInfo> showWellPlate1(String id) {
		List<IncubatorSampleInfo> list = cellPassageDao.showWellPlate1(id);
		return list;
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		for (String da : data) {
			String[] d = da.split(",");
			CellPassageItem pti = commonDAO.get(CellPassageItem.class, d[0]);

			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			cellPassageDao.saveOrUpdate(pti);

			List<IncubatorSampleInfo> isis = cellPassageDao.getIncubatorSampleInfoBySid(d[0]);
			if (isis.size() > 0) {
				Instrument ii = commonDAO.get(Instrument.class, d[2]);

				IncubatorSampleInfo isi = isis.get(0);

				IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
				isio.setId(null);
				isio.setBatch(pti.getBatch());
//				isio.setBatch(pti.getCode());
				isio.setIncubatorId(isi.getIncubatorId());
				isio.setLocation(isi.getLocation());
				isio.setSampleOutDate(new Date());
				isio.setSampleOutUser(user);
				isio.setTaskId(pti.getCellPassage().getId());
				isio.setTaskStepNum(String.valueOf(pti.getStepNum()));
				cellPassageDao.saveOrUpdate(isio);

				if (d[2].equals(isi.getIncubatorId())) {
					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);
				}

				isi.setBatch(pti.getBatch());
//				isi.setBatch(pti.getCode());
				isi.setIncubatorId(pti.getInstrument().getId());
				isi.setLocation(d[1]);
				isi.setSampleOrder(pti.getSampleOrder());
				isi.setState("1");
				isi.setTaskId(pti.getCellPassage().getId());
				isi.setTaskStepNum(String.valueOf(pti.getStepNum()));
				isi.setsId(d[0]);
				cellPassageDao.saveOrUpdate(isi);

				IncubatorSampleInfoIn isii = new IncubatorSampleInfoIn();
				isii.setId(null);
				isii.setBatch(pti.getBatch());
//				isii.setBatch(pti.getCode());
				isii.setIncubatorId(pti.getInstrument().getId());
				isii.setLocation(d[1]);
				isii.setSampleInDate(new Date());
				isii.setSampleInUser(user);
				isii.setTaskId(pti.getCellPassage().getId());
				isii.setTaskStepNum(String.valueOf(pti.getStepNum()));
				cellPassageDao.saveOrUpdate(isii);

				if (d[2].equals(isi.getIncubatorId())) {
					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() - 1);
				} else {
					Instrument ii2 = commonDAO.get(Instrument.class, isi.getIncubatorId());
					ii2.setSurplusLocationsNumber(ii2.getSurplusLocationsNumber() - 1);
					commonDAO.saveOrUpdate(ii2);
				}
				commonDAO.saveOrUpdate(ii);

			} else {
				IncubatorSampleInfo isi = new IncubatorSampleInfo();
				isi.setId(null);
				isi.setBatch(pti.getBatch());
//				isi.setBatch(pti.getCode());
				isi.setIncubatorId(pti.getInstrument().getId());
				isi.setLocation(d[1]);
				isi.setSampleOrder(pti.getSampleOrder());
				isi.setState("1");
				isi.setTaskId(pti.getCellPassage().getId());
				isi.setTaskStepNum(String.valueOf(pti.getStepNum()));
				isi.setsId(d[0]);
				cellPassageDao.saveOrUpdate(isi);

				IncubatorSampleInfoIn isii = new IncubatorSampleInfoIn();
				isii.setBatch(pti.getBatch());
//				isii.setBatch(pti.getCode());
				isii.setId(null);
				isii.setIncubatorId(pti.getInstrument().getId());
				isii.setLocation(d[1]);
				isii.setSampleInDate(new Date());
				isii.setSampleInUser(user);
				isii.setTaskId(pti.getCellPassage().getId());
				isii.setTaskStepNum(String.valueOf(pti.getStepNum()));
				cellPassageDao.saveOrUpdate(isii);

				Instrument ii = commonDAO.get(Instrument.class, d[2]);
				ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() - 1);
				commonDAO.saveOrUpdate(ii);
			}
		}
	}

	public Map<String, Object> showInstrumentOneJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return cellPassageDao.showInstrumentOneJson(start, length, query, col, sort);
	}

	public Map<String, Object> showChooseInstrumentOneJson(Integer start, Integer length, String query, String col,
			String sort, String orderNum, String id) throws Exception {
		List<CellProducOperation> cellProducOperations = cellPassageDao.showCellProducOperationJson(orderNum, id);
		List<CellproductCos> cellproductCos = new ArrayList<CellproductCos>();
//		String[] instrumentCode=null;
		List<String> instrumentCode = new ArrayList<String>();
		if (!cellProducOperations.isEmpty()) {
			for (CellProducOperation cellProducOperation : cellProducOperations) {
				cellproductCos = cellPassageDao.findCellproductCos(cellProducOperation.getId());
				if (!cellproductCos.isEmpty()) {
					for (CellproductCos cellproductCos2 : cellproductCos) {
						instrumentCode.add(cellproductCos2.getInstrumentCode());
					}
				}
			}
			if (instrumentCode.size() >= 1) {
				String sj = "";
				for (String sjb : instrumentCode) {
					if ("".equals(sj)) {
						sj = sj + "'" + sjb + "'";
					} else {
						sj = sj + ",'" + sjb + "'";
					}
				}
				return cellPassageDao.showChooseInstrumentOneJson(start, length, query, col, sort, sj);
			} else {
				return cellPassageDao.showInstrumentOneJson(start, length, query, col, sort);
			}
		}
		return null;
	}

	public List<CellProducOperation> getCellProducOperationList(String id, String stepNum) {
		return cellPassageDao.getCellProducOperationList(id, stepNum);
	}

	public List<CellproductCos> getCellproductCosList(String id) {
		return cellPassageDao.getCellproductCosList(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveChangeLog(String changeLog) {
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setModifyContent(changeLog);
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveChangeLog1(String c) {
		if (c != null && !"".equals(c)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setModifyContent(c);
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}

	}

	public List<CellPassageItemCheckCode> findCellPassageItemCheckCodes(String id, String orderNum, String blendState) {
		return cellPassageDao.findCellPassageItemCheckCodes(id, orderNum, blendState);
	}

	public List<CellPassageItemCheckCode> sampleCodeConfirm(String id, String stepNum, String sampleCodeConfirm,
			String type) {
		return cellPassageDao.sampleCodeConfirm(id, stepNum, sampleCodeConfirm, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCellPassageItemCheckCode(CellPassageItemCheckCode cellPassageItemCheckCode) {
		cellPassageItemCheckCode.setState("已确认");
		commonDAO.saveOrUpdate(cellPassageItemCheckCode);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void backSample(String id, String[] batch) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		String batchNum = batch[0];
		List<CellPassageTemp> cpt = cellPassageDao.backSample(batchNum);
		String orderid = "";
		for (CellPassageTemp cellPassageTemp : cpt) {
			CellPassageAbnormal cpa = new CellPassageAbnormal();
			cpa.setState("1");
			cpa.setCode(cellPassageTemp.getCode());
			cpa.setSampleCode(cellPassageTemp.getSampleCode());
			cpa.setOrderId(cellPassageTemp.getOrderId());
			cpa.setProductId(cellPassageTemp.getProductId());
			cpa.setProductName(cellPassageTemp.getProductName());
			cpa.setResult("0");
			cellPassageTemp.setState("3");
			commonDAO.save(cpa);
			commonDAO.save(cellPassageTemp);
			
			orderid = cellPassageTemp.getOrderId();
		}
		
		if(!"".equals(orderid)) {
			SampleReceive sr = commonDAO.get(SampleReceive.class, orderid);
			if(sr!=null) {
				if(sr.getAcceptUser()!=null) {
					SysRemind srz = new SysRemind();
					srz.setId(null);
					// 提醒类型
					srz.setType(null);
					// 事件标题
					srz.setTitle("生产退回不合格样本");
					// 发起人（系统为SYSTEM）
					srz.setRemindUser(u.getName());
					// 发起时间
					srz.setStartDate(new Date());
					// 查阅提醒时间
					srz.setReadDate(new Date());
					// 提醒内容
					srz.setContent("病人：" + sr.getCcoi() +",批次："+sr.getBarcode()+ "的样本，生产认定不合格。");
					// 状态 0.草稿 1.已阅
					srz.setState("0");
					// 提醒的用户
					srz.setHandleUser(sr.getAcceptUser());
					// 表单id
					srz.setContentId(sr.getId());
					srz.setTableId("CellPassageSampleReceive");
					commonDAO.saveOrUpdate(srz);
				}

				if(sr.getQualityCheckUser()!=null) {
					SysRemind srz1 = new SysRemind();
					srz1.setId(null);
					// 提醒类型
					srz1.setType(null);
					// 事件标题
					srz1.setTitle("生产退回不合格样本");
					// 发起人（系统为SYSTEM）
					srz1.setRemindUser(u.getName());
					// 发起时间
					srz1.setStartDate(new Date());
					// 查阅提醒时间
					srz1.setReadDate(new Date());
					// 提醒内容
					srz1.setContent("病人：" + sr.getCcoi() +",批次："+sr.getBarcode()+ "的样本，生产认定不合格。");
					// 状态 0.草稿 1.已阅
					srz1.setState("0");
					// 提醒的用户
					srz1.setHandleUser(sr.getQualityCheckUser());
					// 表单id
					srz1.setContentId(sr.getId());
					srz1.setTableId("CellPassageSampleReceive");
					commonDAO.saveOrUpdate(srz1);
				}
				

			}
		}

	}

	public CellPassageTemp choseSampleCode(String id) {
		return cellPassageDao.choseSampleCode(id);
	}

	public List<CellPassageTemp> queryBatch(String batch) {
		return cellPassageDao.queryBatch(batch);
	}


	public List<CellPassageRoomPreparat> findCellPassageRoomPreparats(String id, String orderNum) {
		return cellPassageDao.findCellPassageRoomPreparats(id, orderNum);
	}

	public List<CleanAreaDustItem> findCleanAreaDustItems(String operatingRoomId, String estimatedDateTime) {
		return cellPassageDao.findCleanAreaDustItems(operatingRoomId, estimatedDateTime);
	}
	
	public List<CleanAreaDustItem> findCleanAreaDustItem1s(String operatingRoomId, String id) {
		return cellPassageDao.findCleanAreaDustItem1s(operatingRoomId, id);
	}

	public List<CleanAreaMicrobeItem> findCleanAreaMicrobeItems(String operatingRoomId, String estimatedDateTime) {
		return cellPassageDao.findCleanAreaMicrobeItems(operatingRoomId, estimatedDateTime);
	}
	
	public List<CleanAreaMicrobeItem> findCleanAreaMicrobeItem1s(String operatingRoomId, String id) {
		return cellPassageDao.findCleanAreaMicrobeItem1s(operatingRoomId, id);
	}

	public List<CleanAreaVolumeItem> findCleanAreaVolumeItems(String operatingRoomId, String estimatedDateTime) {
		return cellPassageDao.findCleanAreaVolumeItems(operatingRoomId, estimatedDateTime);
	}
	
	public List<CleanAreaVolumeItem> findCleanAreaVolumeItem1s(String operatingRoomId, String id) {
		return cellPassageDao.findCleanAreaVolumeItem1s(operatingRoomId, id);
	}

	public List<CleanAreaBacteriaItem> findCleanAreaBacteriaItems(String operatingRoomId, String estimatedDateTime) {
		return cellPassageDao.findCleanAreaBacteriaItems(operatingRoomId, estimatedDateTime);
	}
	
	public List<CleanAreaBacteriaItem> findCleanAreaBacteriaItem1s(String operatingRoomId, String id) {
		return cellPassageDao.findCleanAreaBacteriaItem1s(operatingRoomId, id);
	}

	public List<CleanAreaMicroorganismItem> findCleanAreaMicroorganismItems(String operatingRoomId, String estimatedDateTime) {
		return cellPassageDao.findCleanAreaMicroorganismItems(operatingRoomId, estimatedDateTime);
	}
	
	public List<CleanAreaMicroorganismItem> findCleanAreaMicroorganismItem1s(String operatingRoomId, String id) {
		return cellPassageDao.findCleanAreaMicroorganismItem1s(operatingRoomId, id);
	}

	public List<CleanAreaDiffPressureItem> findCleanAreaDiffPressureItems(String operatingRoomId,
			String estimatedDateTime) {
		return cellPassageDao.findCleanAreaDiffPressureItems(operatingRoomId, estimatedDateTime);
	}
	
	public List<CleanAreaDiffPressureItem> findCleanAreaDiffPressureItem1s(String operatingRoomId, String id) {
		return cellPassageDao.findCleanAreaDiffPressureItem1s(operatingRoomId, id);
	}
	public void saveStepState(String id,String[] state) throws Exception {
		
			CellPassageTemplate cpt = commonDAO.get(CellPassageTemplate.class, id);
			if(!"".equals(state[0])) {
				cpt.setStep1State(state[0]);
			}
			if(!"".equals(state[1])) {
				cpt.setStep2State(state[1]);
			}
			if(!"".equals(state[2])) {
				cpt.setStep3State(state[2]);
			}
			if(!"".equals(state[3])) {
				cpt.setStep4State(state[3]);
			}
			
			commonDAO.saveOrUpdate(cpt);
		
	}
	public Map<String,Object> selStepState(String id) throws Exception {
		CellPassageTemplate cpt = commonDAO.get(CellPassageTemplate.class, id);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("step1State", cpt.getStep1State());
		result.put("step2State", cpt.getStep2State());
		result.put("step3State", cpt.getStep3State());
		result.put("step4State", cpt.getStep4State());
		return result;
	}
}
