package com.biolims.experiment.cell.passage.service;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.roomManagement.model.RoomManagement;

//紫外线照射 延长 解除占用线程
public class CellPassageRunnable implements Runnable {

	

	private long sleepTime;
	private String operatingRoomName;
	private String stepNum;
	private String mainId;
	private String batch;
	private String templeItemName;
	private RoomManagement rm;

	WebApplicationContext ctx = WebApplicationContextUtils
			.getWebApplicationContext(ServletActionContext
					.getServletContext());
	CellPassageService mbService = (CellPassageService) ctx
			.getBean("cellPassageService");

	public CellPassageRunnable(long sleepTime, String operatingRoomName, String stepNum, String mainId, String batch,
			String templeItemName,RoomManagement roomManagement) {
		this.sleepTime = sleepTime;
		this.operatingRoomName = operatingRoomName;
		this.stepNum = stepNum;
		this.mainId = mainId;
		this.batch = batch;
		this.templeItemName = templeItemName;
		this.rm = roomManagement;
	}

	
	@Override
	public void run() {

		try {
		    Thread.sleep(sleepTime);
			mbService.stopRoom(sleepTime, operatingRoomName, stepNum, mainId,
					batch, templeItemName,rm); 
			Thread.interrupted();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
