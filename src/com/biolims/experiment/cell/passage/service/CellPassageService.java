package com.biolims.experiment.cell.passage.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.Thread.State;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.struts2.ServletActionContext;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.IncubatorSampleInfoIn;
import com.biolims.equipment.model.IncubatorSampleInfoOut;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.cell.main.model.MaterailsMain;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageAbnormal;
import com.biolims.experiment.cell.passage.model.CellPassageBeforeReagent;
import com.biolims.experiment.cell.passage.model.CellPassageCos;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageItemCheckCode;
import com.biolims.experiment.cell.passage.model.CellPassageObservation;
import com.biolims.experiment.cell.passage.model.CellPassagePreparat;
import com.biolims.experiment.cell.passage.model.CellPassagePreparatEnd;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageReagent;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellPassageZhiJian;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProducResults;
import com.biolims.experiment.cell.passage.model.CellProductionCompleted;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.cell.passage.model.CellproductReagent;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.quality.model.QualityTestAbnormal;
import com.biolims.experiment.quality.model.QualityTestResultManage;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.regionalManagement.model.RegionalManagement;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanTemp;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.model.RoomState;
import com.biolims.experiment.transferWindowManage.model.TransferWindowManage;
import com.biolims.experiment.transferWindowManage.model.TransferWindowState;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleFinished;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.tra.transport.model.TransportApplyTemp;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oracle.net.aso.u;

@Service
@Transactional
public class CellPassageService {

	@Resource
	private CellPassageDao cellPassageDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonService commonService;

	public boolean findProductTime(String mainId, String stepNum) {
		boolean state = false;
		CellPassageTemplate cellPassageTemplates = new CellPassageTemplate();

		cellPassageTemplates = cellPassageDao.findProductTime(mainId, stepNum);

		String productStartTime = cellPassageTemplates.getProductStartTime();
		String productEndTime = cellPassageTemplates.getProductEndTime();

		if ((!"".equals(productStartTime) && productStartTime != null)
				&& (!"".equals(productEndTime) && productEndTime != null)) {
			state = true;
		}

		return state;

	}

	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         CellPassage @throws
	 */
	public CellPassage get(String id) {
		CellPassage cellPassage = commonDAO.get(CellPassage.class, id);
		return cellPassage;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPassageItem(String delStr, String[] ids, User user, String cellPassage_id) throws Exception {
		String delId = "";
		for (String id : ids) {
			CellPassageItem scp = cellPassageDao.get(CellPassageItem.class, id);
			if (scp.getId() != null) {
				CellPassage pt = cellPassageDao.get(CellPassage.class, scp.getCellPassage().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				CellPassageTemp cellPassageTemp = this.commonDAO.get(CellPassageTemp.class, scp.getTempId());
				if (cellPassageTemp != null) {
					pt.setSampleNum(pt.getSampleNum() - 1);
					cellPassageTemp.setState("1");
					cellPassageDao.update(cellPassageTemp);
				}
				cellPassageDao.update(pt);
				cellPassageDao.delete(scp);
			}
			delId += scp.getSampleCode() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(cellPassage_id);
			li.setClassName("CellPassage");
			li.setModifyContent(delStr + delId);
			li.setState("2");
			li.setStateName("数据删除");
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除待提交质检样本
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestSampleType(String[] ids, User user) throws Exception {
		for (String id : ids) {
			CellPassageQualityItem scp = cellPassageDao.get(CellPassageQualityItem.class, id);
			if (scp.getId() != null) {
				if (scp.getSubmit() != null && "1".equals(scp.getSubmit())) {

				} else {
					cellPassageDao.delete(scp);
				}
			}
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getCellPassage().getId());
				li.setClassName("CellPassage");
				li.setModifyContent("质检明细删除：" + scp.getSampleNumber());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	/**
	 * 
	 * @Title: delCellPassageItemAf @Description: 重新排板 @author : @date @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPassageItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			CellPassageItem scp = cellPassageDao.get(CellPassageItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				cellPassageDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPassageResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr = "";
		for (String id : ids) {
			CellPassageInfo scp = cellPassageDao.get(CellPassageInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				cellPassageDao.delete(scp);
			idStr += scp.getSampleCode() + ",";
		}

		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setState("2");
		li.setStateName("数据删除");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPassageReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			CellPassageReagent scp = cellPassageDao.get(CellPassageReagent.class, id);
			cellPassageDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setState("2");
			li.setStateName("数据删除");
			li.setModifyContent(delStr + scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCellPassageCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			CellPassageCos scp = cellPassageDao.get(CellPassageCos.class, id);
			cellPassageDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setState("2");
			li.setStateName("数据删除");
			li.setModifyContent(delStr + scp.getName());
		}
	}

	/**
	 * @throws ParseException
	 * 
	 * @Title: saveCellPassageTemplate @Description: 保存模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCellPassageTemplate(CellPassage sc) throws ParseException {
		List<CellPassageTemplate> tlist2 = cellPassageDao.delTemplateItem(sc.getId());
		List<CellPassageReagent> rlist2 = cellPassageDao.delReagentItem(sc.getId());
		List<CellPassageCos> clist2 = cellPassageDao.delCosItem(sc.getId());
		List<CellPassageZhiJian> zlist2 = cellPassageDao.delZhijianItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		commonDAO.deleteAll(zlist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate().getId(), null);
		List<ZhiJianItem> zlist = templateService.getZjItem(sc.getTemplate().getId(), null);
		for (ZhiJianItem t : zlist) {
			CellPassageZhiJian ptr = new CellPassageZhiJian();
			ptr.setCellPassage(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			ptr.setTypeId(t.getTypeId());
			ptr.setTypeName(t.getTypeName());
			cellPassageDao.saveOrUpdate(ptr);
		}

		// 定义一个增长的变量
		Integer i = 0;
		for (TemplateItem t : tlist) {
			CellPassageTemplate ptt = new CellPassageTemplate();
			Integer result = 0;
			if (t.getEstimatedTime() != null && !"".equals(t.getEstimatedTime())) {
				ptt.setEstimatedDate(t.getEstimatedTime());

				i += Integer.parseInt(t.getEstimatedTime());
				result = i - Integer.parseInt(t.getEstimatedTime());
				String createDate = sc.getCreateDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse = format.parse(createDate);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.DATE, result);
				parse = calendar.getTime();
				String date = format.format(parse);
				ptt.setPlanWorkDate(date);

				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse1 = format1.parse(createDate);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(calendar1.DATE, i);
				parse1 = calendar1.getTime();
				String date1 = format.format(parse1);
				ptt.setPlanEndDate(date1);
			}

			ptt.setCellPassage(sc);
			ptt.setOrderNum(String.valueOf(t.getOrderNum()));
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setBlend(t.getBlend());
			ptt.setContent(t.getContent());
			cellPassageDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			CellPassageReagent ptr = new CellPassageReagent();
			ptr.setCellPassage(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			cellPassageDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			CellPassageCos ptc = new CellPassageCos();
			ptc.setCellPassage(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			cellPassageDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		CellPassage sct = cellPassageDao.get(CellPassage.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		cellPassageDao.update(sct);
		
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			Double stepsNum = sct.getTemplate().getStepsNum();

	        DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
	        String valueOf = decimalFormat.format(stepsNum);
//			String valueOf = String.valueOf(stepsNum);
			List<CellProductionRecord> findProductionRecordInfo = cellPassageDao.findProductionRecordInfo(sct.getId(),valueOf);
			if(!findProductionRecordInfo.isEmpty()) {
				findProductionRecordInfo.get(0).setEndTime(df.format(new Date()));
				commonDAO.saveOrUpdate(findProductionRecordInfo.get(0));
			}
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : @date @param id @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		CellPassage sc = this.cellPassageDao.get(CellPassage.class, id);
		// 获取结果表样本信息
		List<CellPassageInfo> list;
		if (ids == null)
			list = this.cellPassageDao.selectAllResultListById(id);
		else
			list = this.cellPassageDao.selectAllResultListByIds(ids);

		for (CellPassageInfo scp : list) {
			if (scp != null && scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();
					// 细胞主数据
					MaterailsMain mm = new MaterailsMain();
					String modelName = "MaterailsMain";
					String markCode = "MM";
					String autoID = codingRuleService.genTransID(modelName, markCode);
					mm.setId(autoID);
					mm.setPronoun(scp.getPronoun());
					mm.setCode(scp.getCode());
					mm.setParentId(scp.getParentId());
					mm.setSampleCode(scp.getSampleCode());
					cellPassageDao.saveOrUpdate(mm);

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setParentId(scp.getCode());
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("CellPassageInfo");
						if (scp.getSampleOrder() != null) {
							st.setSampleOrder(commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId()));
						}
						st.setPronoun(scp.getPronoun());
						cellPassageDao.saveOrUpdate(st);

					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0082")) {// 自主过程干细胞检测
						// 自主过程干细胞检测
						QualityTestTemp d = new QualityTestTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						if (scp.getSampleOrder() != null) {
							d.setSampleOrder(commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId()));
						}
						d.setCellType("1");
						cellPassageDao.saveOrUpdate(d);
					} else if (next.equals("0086")) {// 第三方过程干细胞检测
						// 第三方过程干细胞检测
						QualityTestTemp d = new QualityTestTemp();
						scp.setState("1");
						d.setState("1");
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						if (scp.getSampleOrder() != null) {
							d.setSampleOrder(commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId()));
						}
						d.setCellType("5");
						cellPassageDao.saveOrUpdate(d);

					} else if (next.equals("0073")) {// 再次传代
						CellPassageTemp cpt = new CellPassageTemp();
						scp.setState("1");
						cpt.setState("1");
						scp.setParentId(scp.getCode());
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setCode(scp.getCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setConcentration(scp.getConcentration());
						cpt.setVolume(scp.getVolume());
						cpt.setOrderId(sc.getId());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						cpt.setPronoun(scp.getPronoun());
						if (scp.getSampleOrder() != null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId()));
						}
						cellPassageDao.saveOrUpdate(cpt);
					} else if (next.equals("0016")) {// 产品干细胞生产
						PlasmaTaskTemp cpt = new PlasmaTaskTemp();
						scp.setState("1");
						cpt.setState("1");
						scp.setParentId(scp.getCode());
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setCode(scp.getCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setConcentration(scp.getConcentration());
						cpt.setVolume(scp.getVolume());
						cpt.setOrderId(sc.getId());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						cpt.setPronoun(scp.getPronoun());
						if (scp.getSampleOrder() != null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId()));
						}
						cellPassageDao.saveOrUpdate(cpt);
					} else if (next.equals("0094")) {// 产品运输 同时流向放行审核
						// TransportOrderTemp cpt=new TransportOrderTemp();
						// cpt.setId(null);
						// /** 原始样本编号 */
						// cpt.setSampleCode(scp.getCode());
						// /** 样本编号 */
						// cpt.setCode(scp.getCode());
						// if(scp.getSampleOrder()!=null){
						// SampleOrder so = commonDAO.get(SampleOrder.class,
						// scp.getSampleOrder().getId());
						// if(so!=null){
						// /** 患者姓名 */
						// cpt.setPatientName(so.getName());
						// /** 检测项目 */
						// cpt.setProductId(so.getProductId());
						// /** 检测项目 */
						// cpt.setProductName(so.getProductName());
						// /**关联订单*/
						// cpt.setSampleOrder(so);
						// /** 订单编号 */
						// cpt.setOrderCode(so.getId());
						// }
						// }
						//
						// /** 备注 */
						// cpt.setNote(scp.getNote());
						// /** 状态ID */
						// cpt.setState("1");
						// /**产品运输左侧表的状态 1放行审核通过0放行审核未通过*/
						// cpt.setPlanState("0");
						// /** 体积 */
						// cpt.setVolume(scp.getVolume());
						// /** 任务单id */
						// cpt.setOrderId(sc.getId());
						// /** 样本数量 */
						// cpt.setSampleNum(scp.getCellCount());
						// /** 样本类型 */
						// if(scp.getDicSampleType()!=null){
						// cpt.setSampleType(scp.getDicSampleType().getName());
						// }
						// /** 样本主数据 */
						// cpt.setSampleInfo(scp.getSampleInfo());
						// cellPassageDao.saveOrUpdate(cpt);

						User u = (User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY);

						List<PlasmaReceiveItem> pris = commonService.get(PlasmaReceiveItem.class, "batchNumber",
								sc.getBatch());
						if (pris.size() > 0) {
							PlasmaReceiveItem pri = pris.get(0);
							pri.setSampleCode(scp.getCode());
							if (scp.getSampleOrder() != null) {
								// pri.setBatchNumber(scp.getSampleOrder().getId());
								pri.setBatchNumber(scp.getSampleOrder().getBarcode());
							} else {
								pri.setBatchNumber(sc.getBatch());
							}
							pri.setSpecifications(scp.getSpecifications());
							pri.setCellNumber(scp.getCellCount());
							if (scp.getSampleOrder() != null) {
								SampleOrder so = commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId());
								if (so != null) {
									pri.setSampleOrder(so);
									so.setBatchState("6");
									so.setBatchStateName("待放行");
									commonDAO.merge(so);
								}
								List<QualityTestResultManage> qtrms = commonService.get(QualityTestResultManage.class,
										"sampleOrder.id", scp.getSampleOrder().getId());
								if (qtrms.size() > 0) {
									for (QualityTestResultManage qtrm : qtrms) {
										if ("1".equals(qtrm.getState()) && "完成".equals(qtrm.getStateName())) {
											pri.setQualityTestPass("1");
										}
									}
								}
							}
							cellPassageDao.saveOrUpdate(pri);
						} else {
							PlasmaReceiveItem pri = new PlasmaReceiveItem();
							pri.setId(null);
							pri.setSampleCode(scp.getCode());
							if (scp.getSampleOrder() != null) {
								// pri.setBatchNumber(scp.getSampleOrder().getId());
								pri.setBatchNumber(scp.getSampleOrder().getBarcode());
							} else {
								pri.setBatchNumber(sc.getBatch());
							}
							pri.setState("1");
							pri.setCreateUser(u);
							pri.setCreateDate(new Date());
							pri.setSpecifications(scp.getSpecifications());
							pri.setCellNumber(scp.getCellCount());
							if (scp.getSampleOrder() != null) {
								SampleOrder so = commonDAO.get(SampleOrder.class, scp.getSampleOrder().getId());
								if (so != null) {
									pri.setSampleOrder(so);
									so.setBatchState("6");
									so.setBatchStateName("待放行");
									commonDAO.merge(so);
								}
								List<QualityTestResultManage> qtrms = commonService.get(QualityTestResultManage.class,
										"sampleOrder.id", scp.getSampleOrder().getId());
								if (qtrms.size() > 0) {
									for (QualityTestResultManage qtrm : qtrms) {
										if ("1".equals(qtrm.getState()) && "完成".equals(qtrm.getStateName())) {
											pri.setQualityTestPass("1");
										}
									}
								}
							}
							cellPassageDao.saveOrUpdate(pri);
						}
					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					CellPassageAbnormal pa = new CellPassageAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				if (scp.getSampleOrder() != null) {
					scp.getSampleOrder().setNewTask(id);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService.saveSampleState1(scp.getSampleOrder(), scp.getCode(), scp.getSampleCode(),
						scp.getProductId(), scp.getProductName(), "", sc.getCreateDate(), format.format(new Date()),
						"CellPassage", "传代细胞生产",
						(User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY),
						id, scp.getNextFlow(), scp.getResult(), null, null, null, null, null, null, null, null);

				scp.setSubmit("1");
				cellPassageDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findCellPassageTable @Description: 展示主表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPassageTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return cellPassageDao.findCellPassageTable(start, length, query, col, sort);
	}

	public Map<String, Object> showCellPassageJson(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return cellPassageDao.showCellPassageJson(start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: selectCellPassageTempTable @Description: 展示临时表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectCellPassageTempTable(String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return cellPassageDao.selectCellPassageTempTable(codes, start, length, query, col, sort);
	}

	public boolean findBachIsDifference(String batch, String length) throws Exception {

		Long selectCellPassageTempTable = cellPassageDao.selectCellPassageTempTable(batch);
		if (!"".equals(length) && length != null) {
			Long valueOf = Long.valueOf(length);
			if (selectCellPassageTempTable == valueOf) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId, String templateId, String logInfo)
			throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			CellPassage pt = new CellPassage();
			pt = (CellPassage) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				String batch = "";
				String modelName = "CellPassage";
				if (tempId != null) {
					for (String temp : tempId) {
						CellPassageTemp cpt = cellPassageDao.get(CellPassageTemp.class, temp);
						if (cpt != null) {
							if (cpt.getBatch() != null && !"".equals(cpt.getBatch())) {
								batch = cpt.getBatch();
								pt.setBatch(batch);
							}
						}
					}
				}
				String markCode = "CP";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			} else {
				String batch = "";
				id = pt.getId();
				pt = commonDAO.get(CellPassage.class, id);
				if (tempId != null) {
					for (String temp : tempId) {
						CellPassageTemp cpt = cellPassageDao.get(CellPassageTemp.class, temp);
						if (cpt != null) {
							if (cpt.getBatch() != null && !"".equals(cpt.getBatch())) {
								batch = cpt.getBatch();
								pt.setBatch(batch);
							}
						}
					}
				}
				List<CellPassageItem> cpItems = commonService.get(CellPassageItem.class, "cellPassage.id", id);
				if (cpItems.size() > 0) {
					for (CellPassageItem cpi : cpItems) {
						if (cpi.getBatch() != null && !"".equals(cpi.getBatch())) {
							batch = cpi.getBatch();
							pt.setBatch(batch);
						}
					}
				}
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String[] users = userId.split(",");
			String userName = "";
			for (int i = 0; i < users.length; i++) {
				if (i == users.length - 1) {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName();
				} else {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName() + ",";
				}
			}
			pt.setTestUserOneName(userName);
			cellPassageDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveCellPassageTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
					for (String temp : tempId) {
						CellPassageTemp ptt = cellPassageDao.get(CellPassageTemp.class, temp);
						if (t.getIsSeparate().equals("1")) {
							List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
									.getMappingPrimersLibraryItem(ptt.getProductId());
							for (MappingPrimersLibraryItem mpi : listMPI) {
								CellPassageItem pti = new CellPassageItem();
								pti.setParentId(ptt.getParentId());
								pti.setPronoun(ptt.getPronoun());
								pti.setParentId(ptt.getParentId());
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
										? t.getDicSampleType().getId()
										: t.getDicSampleTypeId());
								pti.setDicSampleTypeName(
										t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
												? t.getDicSampleType().getName()
												: t.getDicSampleTypeName());
								pti.setProductName(ptt.getProductName());
								pti.setProductNum(
										t.getProductNum() != null && !"".equals(t.getProductNum()) ? t.getProductNum()
												: t.getProductNum1());
								pti.setCode(ptt.getCode());
								if (t.getStorageContainer() != null) {
									pti.setState("1");
								} else {
									pti.setState("2");
								}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setCellPassage(pt);
								pti.setChromosomalLocation(mpi.getChromosomalLocation());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (ptt.getSampleOrder() != null) {
									pti.setSampleOrder(commonDAO.get(SampleOrder.class, ptt.getSampleOrder().getId()));
								}
								pti.setStepNum("1");
								pti.setBatch(ptt.getBatch());
								cellPassageDao.saveOrUpdate(pti);
							}
						} else {
							CellPassageItem pti = new CellPassageItem();
							pti.setParentId(ptt.getParentId());
							pti.setPronoun(ptt.getPronoun());
							pti.setParentId(ptt.getParentId());
							pti.setSampleCode(ptt.getSampleCode());
							pti.setCode(ptt.getCode());
							pti.setOrderId(ptt.getOrderId());
							pti.setPronoun(ptt.getPronoun());
							pti.setProductId(ptt.getProductId());
							pti.setProductName(ptt.getProductName());
							pti.setDicSampleTypeId(t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
									? t.getDicSampleType().getId()
									: t.getDicSampleTypeId());
							pti.setProductNum(
									t.getProductNum() != null && !"".equals(t.getProductNum()) ? t.getProductNum()
											: t.getProductNum1());
							pti.setDicSampleTypeName(t.getDicSampleType() != null && !"".equals(t.getDicSampleType())
									? t.getDicSampleType().getName()
									: t.getDicSampleTypeName());
							pti.setCode(ptt.getCode());
							pti.setSampleInfo(ptt.getSampleInfo());
							if (t.getStorageContainer() != null) {
								pti.setState("1");
							} else {
								pti.setState("2");
							}
							ptt.setState("2");
							pti.setTempId(temp);
							pti.setCellPassage(pt);
							if (ptt.getSampleOrder() != null) {
								pti.setSampleOrder(commonDAO.get(SampleOrder.class, ptt.getSampleOrder().getId()));
							}
							pti.setStepNum("1");
							pti.setBatch(ptt.getBatch());
							cellPassageDao.saveOrUpdate(pti);
						}
						cellPassageDao.saveOrUpdate(ptt);
					}
				}

			}

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setState("1");
				li.setStateName("数据新增");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findCellPassageItemTable @Description:展示未排版样本 @author : @date @param
	 *         id @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPassageItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return cellPassageDao.findCellPassageItemTable(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排板数据 @author : @date @param id @param
	 *         item @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<CellPassageItem> saveItems = new ArrayList<CellPassageItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		CellPassage pt = commonDAO.get(CellPassage.class, id);
		CellPassageItem scp = new CellPassageItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (CellPassageItem) cellPassageDao.Map2Bean(map, scp);
			CellPassageItem pti = commonDAO.get(CellPassageItem.class, scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());
			scp.setCellPassage(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("1");
			li.setStateName("数据新增");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		cellPassageDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate @Description: 展示孔板 @author : @date @param
	 *         id @return @throws Exception List<CellPassageItem> @throws
	 */
	public List<CellPassageItem> showWellPlate(String id) throws Exception {
		List<CellPassageItem> list = cellPassageDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findCellPassageItemAfTable @Description: 展示排板后 @author : @date @param
	 *         scId @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPassageItemAfTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return cellPassageDao.findCellPassageItemAfTable(scId, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			CellPassageItem pti = commonDAO.get(CellPassageItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			cellPassageDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showCellPassageStepsJson @Description: 展示实验步骤 @author : @date @param
	 *         id @param code @return Map<String,Object> @throws
	 */
	public Map<String, Object> showCellPassageStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellPassageTemplate> pttList = cellPassageDao.showCellPassageStepsJson(id, code);
		CellPassageTemplate cellPassageTemplate = pttList.get(0);

//		List<CellPassageReagent> ptrList = cellPassageDao.showCellPassageReagentJson(id, code);
//		List<CellPassageCos> ptcList = cellPassageDao.showCellPassageCosJson(id, code);
//		List<ZhiJianItem> ptzList = cellPassageDao.showCellPassageZjianJson(id, code);
//		List<Object> plate = cellPassageDao.showWellList(id);
//		List<CellPassageReagent> ptrList = cellPassageDao.showCellPassageReagentJson(id, code);
//		List<CellPassageCos> ptcList = cellPassageDao.showCellPassageCosJson(id, code);
//		List<ZhiJianItem> ptzList = cellPassageDao.showCellPassageZjianJson(id, code);
//		List<Object> plate = cellPassageDao.showWellList(id);
//		map.put("template", pttList);
		map.put("startTime", cellPassageTemplate.getStartTime());
		map.put("endTime", cellPassageTemplate.getEndTime());
		// map.put("reagent", ptrList);
//		map.put("cos", ptcList);
//		map.put("zhiJian", ptzList);
//		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showCellPassageResultTableJson @Description: 展示结果 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> showCellPassageResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return cellPassageDao.showCellPassageResultTableJson(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @param id @param
	 *         counts @return Map<String,Object> @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		CellPassage pt = commonDAO.get(CellPassage.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellPassageItem> list = cellPassageDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent @Description: 上传结果 @author : @date @param id @param
	 *         fileId @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = cellPassageDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			CellPassage pt = cellPassageDao.get(CellPassage.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<CellPassageInfo> listPTI = cellPassageDao.findCellPassageInfoByCode(code);
						if (listPTI.size() > 0) {
							CellPassageInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							cellPassageDao.saveOrUpdate(spi);
						}

					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @param id @param
	 *         templateJson @param reagentJson @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson, String cosJson, String logInfo) {
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("1");
			li.setStateName("数据新增");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}") && !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}") && !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate @Description: 保存模板明细 @author : @date @param id @param
	 *         templateJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			CellPassageTemplate ptt = cellPassageDao.get(CellPassageTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			String testUsers = (String) object.get("testUserList");
			if (testUsers != null && !"".equals(templateId)) {
				ptt.setTestUserList(testUsers);
			}
			String estimatedTime = (String) object.get("estimatedTime");
			if (estimatedTime != null && !"".equals(estimatedTime)) {
				ptt.setEstimatedDate(estimatedTime);
			}
			String sampleDeteyion = (String) object.get("sampleDeteyion");
			if (sampleDeteyion != null && !"".equals(sampleDeteyion)) {
				SampleDeteyion sampleDeteyion2 = commonDAO.get(SampleDeteyion.class, sampleDeteyion);
				ptt.setSampleDeteyion(sampleDeteyion2);
			}
			cellPassageDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent @Description: 保存试剂 @author : @date @param id @param
	 *         reagentJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {

		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			CellPassageReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(CellPassageReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new CellPassageReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setCellPassage(commonDAO.get(CellPassage.class, id));
				}

			}
			cellPassageDao.saveOrUpdate(ptr);
		}

	}

	/**
	 * 
	 * @Title: saveCos @Description: 保存设备 @author : @date @param id @param cosJson
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {

		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			CellPassageCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(CellPassageCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new CellPassageCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setCellPassage(commonDAO.get(CellPassage.class, id));
				}
			}
			cellPassageDao.saveOrUpdate(ptc);
		}

	}

	public Map<String, Object> findQualityItem(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return cellPassageDao.findQualityItem(id, start, length, query, col, sort);
	}

	public Map<String, Object> noBlendPlateSampleTable(String id, String stepNum, String[] codes, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		String inCodes = "";
		if (codes != null) {
			for (int i = 0; i < codes.length; i++) {
				if (i == codes.length - 1) {
					inCodes += "'" + codes[i] + "'";
				} else {
					inCodes += "'" + codes[i] + "',";
				}
			}
		}
		return cellPassageDao.noBlendPlateSampleTable(id, stepNum, inCodes, start, length, query, col, sort);
	}

	public Map<String, Object> blendPlateSampleTable(String id, String stepNum, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return cellPassageDao.blendPlateSampleTable(id, stepNum, start, length, query, col, sort);
	}
	public List<QualityTestTemp> findQualityTestSample(String codeConfirm) throws Exception {
		return cellPassageDao.findQualityTestSample(codeConfirm);
	}
	public List<QualityTestTemp> findQualityTestSampleStart(String codeConfirm) throws Exception {
		return cellPassageDao.findQualityTestSampleStart(codeConfirm);
	}
	public List<TransferWindowState> findTransferWindowState(String codeConfirm) throws Exception {
		return cellPassageDao.findTransferWindowState(codeConfirm);
	}
	public List<TransferWindowState> findTransferWindowStateNew(String codeConfirm) throws Exception {
		return cellPassageDao.findTransferWindowStateNew(codeConfirm);
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @param id @throws
	 *         Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {

		List<CellPassageTemplate> cpts = cellPassageDao.getCellPassageTemplates(id);// commonService.get(CellPassageTemplate.class,
																					// "cellPassage.id", id);
		List<CellPassageItem> list = cellPassageDao.showWellPlates(id, cpts.get(cpts.size() - 1).getOrderNum());
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellPassageInfo> spiList = cellPassageDao.selectResultListById(id);
		for (CellPassageItem pti : list) {
			// String p = pti.getPronoun();//获取明细的代次
			// String substring = p.substring(1);
			// int intValue = Integer.valueOf(substring).intValue()+1;
			// String pp = "P" + String.valueOf(intValue);
			boolean b = true;
			for (CellPassageInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				// if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
				// if (pti.getProductNum() != null
				// && !"".equals(pti.getProductNum())) {
				// Integer pn = Integer.parseInt(pti.getProductNum());
				// for (int i = 0; i < pn; i++) {
				CellPassageInfo scp = new CellPassageInfo();
				scp.setParentId(pti.getParentId());
//				DicSampleType ds = commonDAO.get(DicSampleType.class, pti.getDicSampleTypeId());
//				scp.setDicSampleType(ds);
				scp.setParentId(pti.getParentId());
				scp.setOrderId(pti.getOrderId());
				scp.setSampleType(pti.getSampleType());
				scp.setSampleCode(pti.getSampleCode());
				scp.setProductId(pti.getProductId());
				scp.setProductName(pti.getProductName());
				scp.setSampleInfo(pti.getSampleInfo());
				scp.setCellPassage(pti.getCellPassage());
				scp.setResult("1");
				String markCode = "HT";
				/*
				 * if (scp.getDicSampleType() != null) { DicSampleType d = commonDAO.get(
				 * DicSampleType.class, scp .getDicSampleType().getId()); String[] sampleCode =
				 * scp.getSampleCode() .split(","); if (sampleCode.length > 1) { String str =
				 * sampleCode[0].substring(0, sampleCode[0].length() - 4); for (int j = 0; j <
				 * sampleCode.length; j++) { str += sampleCode[j].substring(
				 * sampleCode[j].length() - 4, sampleCode[j].length()); } if (d == null) {
				 * markCode = "P"+str; } else { markCode = "P"+str + d.getCode(); } } else { if
				 * (d == null) { markCode = "P"+scp.getSampleCode(); } else { markCode =
				 * "P"+scp.getSampleCode() + d.getCode(); } }
				 * 
				 * }
				 */
				// markCode += DateUtil.dateFormatterByPattern(
				// new Date(), "yyMMdd")+ds.getCode()+pp;
				// String code = codingRuleService.getCode(
				// "CellPassageInfo", markCode, 00000, 5, null);
				scp.setCode(pti.getCode());
				// scp.setCode(code);
				// if(pti.getSampleOrder()!=null) {
				if (pti.getOrderCode() != null) {
					// scp.setSampleOrder(commonDAO.get(SampleOrder.class,
					// pti.getSampleOrder().getId()));
					scp.setSampleOrder(commonDAO.get(SampleOrder.class, pti.getOrderCode()));
				}
				// scp.setPronoun(pp);
				cellPassageDao.saveOrUpdate(scp);
				// }
				// }
				// } else {
				// if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
				// if (pti.getProductNum() != null
				// && !"".equals(pti.getProductNum())) {
				// Integer pn = Integer.parseInt(pti.getProductNum());
				// for (int i = 0; i < pn; i++) {
				// CellPassageInfo scp = new CellPassageInfo();
				// DicSampleType ds = commonDAO.get(
				// DicSampleType.class,
				// pti.getDicSampleTypeId());
				// scp.setDicSampleType(ds);
				// scp.setParentId(pti.getParentId());
				// scp.setOrderId(pti.getOrderId());
				// scp.setSampleType(pti.getSampleType());
				// scp.setSampleCode(pti.getSampleCode());
				// scp.setProductId(pti.getProductId());
				// scp.setProductName(pti.getProductName());
				// scp.setSampleInfo(pti.getSampleInfo());
				// scp.setCellPassage(pti.getCellPassage());
				// scp.setResult("1");
				// String markCode = "HT";
				// /*if (scp.getDicSampleType() != null) {
				// DicSampleType d = commonDAO
				// .get(DicSampleType.class, scp
				// .getDicSampleType().getId());
				// String[] sampleCode = scp.getSampleCode()
				// .split(",");
				// if (sampleCode.length > 1) {
				// String str = sampleCode[0].substring(0,
				// sampleCode[0].length() - 4);
				// for (int j = 0; j < sampleCode.length; j++) {
				// str += sampleCode[j].substring(
				// sampleCode[j].length() - 4,
				// sampleCode[j].length());
				// }
				// if (d == null) {
				// markCode = str;
				// } else {
				// markCode = str + d.getCode();
				// }
				// } else {
				// if (d == null) {
				// markCode = scp.getSampleCode();
				// } else {
				// markCode = scp.getSampleCode()
				// + d.getCode();
				// }
				// }
				//
				// }*/
				//// markCode += DateUtil.dateFormatterByPattern(
				//// new Date(), "yyMMdd")+ds.getCode()+pp;
				//// String code = codingRuleService.getCode(
				//// "CellPassageInfo", markCode, 00000, 5,
				//// null);
				//// scp.setCode(code);
				// scp.setCode(pti.getCode());
				//// map.put(String.valueOf(pti.getBlendCode()),
				//// code);
				// map.put(String.valueOf(pti.getBlendCode()),
				// pti.getCode());
				// if(pti.getSampleOrder()!=null) {
				// scp.setSampleOrder(commonDAO.get(SampleOrder.class,
				// pti.getSampleOrder().getId()));
				// }
				//// scp.setPronoun(pp);
				// cellPassageDao.saveOrUpdate(scp);
				// }
				// }
				// } else {
				// String code = (String) map.get(String.valueOf(pti
				// .getBlendCode()));
				// CellPassageInfo spi = cellPassageDao
				// .getResultByCode(code);
				// spi.setSampleCode(spi.getSampleCode() + ","
				// + pti.getSampleCode());
				// cellPassageDao.saveOrUpdate(spi);
				// }
				// }
			}
			String scjg = "细胞传代实验结果:" + "样本编号:" + pti.getSampleCode() + ",产物类型:" + pti.getDicSampleTypeName()
					+ " 的结果已生成";
			if (scjg != null && !"".equals(scjg)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setState("1");
				li.setStateName("数据新增");
				li.setModifyContent(scjg);
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果 @author : @date @param id @param
	 *         dataJson @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo, String confirmUser) throws Exception {

		List<CellPassageInfo> saveItems = new ArrayList<CellPassageInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		CellPassage pt = commonDAO.get(CellPassage.class, id);
		for (Map<String, Object> map : list) {
			CellPassageInfo scp = new CellPassageInfo();
			// 将map信息读入实体类
			scp = (CellPassageInfo) cellPassageDao.Map2Bean(map, scp);
			scp.setCellPassage(pt);
			saveItems.add(scp);
		}
		if (confirmUser != null && !"".equals(confirmUser)) {
			User confirm = commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			cellPassageDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("1");
			li.setStateName("数据新增");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		cellPassageDao.saveOrUpdateAll(saveItems);

	}

	public List<CellPassageItem> findCellPassageItemList(String scId) throws Exception {
		List<CellPassageItem> list = cellPassageDao.selectCellPassageItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return cellPassageDao.generateBlendCode(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(CellPassage pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				CellPassageItem pti = new CellPassageItem();
				pti.setCellPassage(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				cellPassageDao.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setState("1");
			li.setStateName("数据新增");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRightQuality(CellPassage pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				CellPassageItem pti = new CellPassageItem();
				pti.setCellPassage(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}

	}

	public CellPassageInfo getInfoById(String idc) {
		return cellPassageDao.get(CellPassageInfo.class, idc);

	}

	/**
	 * @throws Exception
	 * @Title: findCellPrimaryCultureTemplateList @Description: TODO @author :
	 *         nan.jiang @date 2018-8-30上午9:40:13 @param start @param length @param
	 *         query @param col @param sort @param ids @return
	 *         Map<String,Object> @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTemplateList(Integer start, Integer length, String query,
			String col, String sort, String ids) throws Exception {
		// TODO Auto-generated method stub
		return cellPassageDao.findCellPrimaryCultureTemplateList(start, length, query, col, sort, ids);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitToQualityTest(String id, String stepNum, String mark, String type, String[] ids)
			throws Exception {
		CellPassage cp = commonDAO.get(CellPassage.class, id);
		List<ZhiJianItem> ptzList = cellPassageDao.showCellPassageZjianJson(cp.getTemplate().getId(), stepNum);
		if (ids == null || ids.length < 1) {

		} else {

			for (String itemId : ids) {
				CellPassageItem scp = commonDAO.get(CellPassageItem.class, itemId);
				if (ptzList.size() > 0) {
					for (ZhiJianItem zj : ptzList) {

						String markCode = "QC" + scp.getCode();
						String code = codingRuleService.getCode("QualityTestTemp", markCode, 0000, 4, null);
						QualityTestTemp cpt = new QualityTestTemp();
						cpt.setCode(code);
						cpt.setState("1");
						cpt.setMark(mark);
						cpt.setZjName(zj.getName());
						cpt.setTestItem(zj.getName());
						cpt.setExperimentalSteps(zj.getItemId());
						cpt.setOrderCode(scp.getOrderCode());
						cpt.setParentId(scp.getCode());
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());

						cpt.setSampleOrder(scp.getSampleOrder());
						if (zj.getCode() != null) {
							cpt.setSampleDeteyion(commonDAO.get(SampleDeteyion.class, zj.getCode()));
						}
						if (type.equals("zj01")) {// 自主过程干细胞检测
							cpt.setCellType("1");
						} else if (type.equals("zj02")) {// 第三方检测
							cpt.setCellType("5");
						}
						cellPassageDao.saveOrUpdate(cpt);
						cellPassageDao.saveOrUpdate(scp);
						/*
						 * CellPassage sc = commonDAO.get(CellPassage.class, id); DateFormat format =
						 * new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						 */
						/*
						 * sampleStateService .saveSampleState1( scp.getSampleOrder(), scp.getCode(),
						 * scp.getSampleCode(), scp.getProductId(), scp.getProductName(), "",
						 * sc.getCreateDate(), format.format(new Date()), "CellPassage", "传代细胞生产明细",
						 * (User) ServletActionContext .getRequest() .getSession() .getAttribute(
						 * SystemConstants.USER_SESSION_KEY), id, next, null, null, null, null, null,
						 * null, null, null, null);
						 */
					}
				}
			}

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitToQualityTestType(String id, String stepNum, String stepName, String mark, String[] ids,
			String[] chosedIDs, String type, String data) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		CellPassage cp = commonDAO.get(CellPassage.class, id);
		CellProductionRecord cpt = cellPassageDao.findStepNumOrValue(id, stepNum);
//		 List<ZhiJianItem> ptzList =
//		 cellPassageDao.showCellPassageZjianJson(cp.getTemplate().getId(),
//		 stepNum);
		Date d = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (ids == null || ids.length < 1) {

		} else {

			for (String itemId : ids) {
				CellPassageItem scp = commonDAO.get(CellPassageItem.class, itemId);
				//
				// if(chosedIDs==null || chosedIDs.length<1) {
				//
				// }else{
				JSONArray array = JSONArray.fromObject(data);
				if (array.size() > 0) {
					for (int i = 0; i < array.size(); i++) {
						JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
						String jcxmc = (String) job.get("jcxmc");
						String jcxid = (String) job.get("jcxid");
						String jcsl = (String) job.get("jcsl");
						String jcxlxmc = (String) job.get("jcxlxmc");
						String jcxlx = (String) job.get("jcxlx");
						String jcbz = (String) job.get("jcbz");
						String jcsldz = (String) job.get("jcxsldw");
						// for (String chosedID : chosedIDs) {
						SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, jcxid);
						DicSampleType dd = commonDAO.get(DicSampleType.class, jcxlx);

						String scgxdm = cpt.getTempleItem().getPrintLabelCoding();

						String jcid = sd.getDeteyionName();
						String ybbh = dd.getCode();

						int a = scp.getCode().lastIndexOf("-");
						String b = scp.getCode().substring(0, a + 1);
						String c = b + scgxdm + ybbh + jcid;

						// String markCode = "QC" + scp.getCode();
						// String code = codingRuleService.getCode(
						// "QualityTestTemp", markCode, 0000, 4,
						// null);
//						QualityTestTemp cpt = new QualityTestTemp();
//						cpt.setCode(scp.getCode());
//						cpt.setState("1");
//						cpt.setMark(mark);
//						cpt.setZjName(sd.getName());
//						cpt.setTestItem(sd.getName());
//						cpt.setExperimentalSteps(stepNum);
//
//						cpt.setOrderCode(scp.getOrderCode());
//						if (scp.getOrderCode() != null && !"".equals(scp.getOrderCode())) {
//							SampleOrder so = commonDAO.get(SampleOrder.class, scp.getOrderCode());
//							cpt.setSampleOrder(so);
//						}
//
//						cpt.setParentId(scp.getCode());
//						cpt.setSampleCode(scp.getSampleCode());
//						cpt.setProductId(scp.getProductId());
//						cpt.setProductName(scp.getProductName());
//
//						// cpt.setSampleType(scp.getSampleType());
//						cpt.setSampleType(jcxlxmc);
//						cpt.setNote(jcbz);
//						cpt.setSampleNum(jcsl);
//
//						cpt.setSampleInfo(scp.getSampleInfo());
//						cpt.setScopeId(scp.getScopeId());
//						cpt.setScopeName(scp.getScopeName());
//
//						cpt.setSampleDeteyion(sd);
//						if (type.equals("zj01")) {// 自主过程干细胞检测
//							cpt.setCellType("1");
//						} else if (type.equals("zj02")) {// 第三方检测
//							cpt.setCellType("5");
//						}
//
//						cpt.setBatch(scp.getBatch());// 批次

						CellPassageQualityItem cpqi = new CellPassageQualityItem();
						/** 提交表编码 */
						cpqi.setTempId(itemId);
						/** 样本编号 */
						cpqi.setCode(scp.getCode());
						cpqi.setSampleNumber(c);
						/** 批次号 */
						cpqi.setBatch(scp.getBatch());
						/** 订单编号 */
						cpqi.setOrderCode(scp.getOrderCode());
						/** 检测项目 */
						cpqi.setProductId(scp.getProductId());
						/** 检测项目 */
						cpqi.setProductName(scp.getProductName());
						/** 状态 */
						cpqi.setState("1");
						/** 备注 */
						cpqi.setNote(jcbz);
						/** 浓度 */
						cpqi.setConcentration(null);
						/** 样本数量 */
						if (jcsl != null && !"".equals(jcsl)) {
							Double jcsls = Double.valueOf(jcsl);
							cpqi.setSampleNum(jcsls);
						}
						/** 检测项数量单位 */
						cpqi.setSampleNumUnit(jcsldz);
						/** 相关主表 */
						cpqi.setCellPassage(cp);
						/** 体积 */
						cpqi.setVolume(null);
						/** 任务单id */
						cpqi.setOrderId(cp.getId());
						/** 中间产物数量 */
						cpqi.setProductNum(null);
						/** 中间产物类型编号 */
						cpqi.setDicSampleTypeId(null);
						/** 中间产物类型 */
						cpqi.setDicSampleTypeName(null);
						/** 样本类型 */
						cpqi.setSampleType(dd.getName());
						/** 样本主数据 */
						cpqi.setSampleInfo(scp.getSampleInfo());
						/** 实验的步骤号 */
						cpqi.setStepNum(stepNum);
						/** 实验步骤名称 */
						cpqi.setExperimentalStepsName(stepName);
						/** 检测项 */
						cpqi.setSampleDeteyion(sd);
						/** 关联订单 */
						cpqi.setSampleOrder(scp.getSampleOrder());
						/** 是否合格 */
						cpqi.setQualified(null);
						/** 质检结果表id */
						cpqi.setQualityInfoId(null);
						/** 质检是否接收 */
						cpqi.setQualityReceive(null);
						/** 质检提交时间 */
						cpqi.setQualitySubmitTime(null);
						/** 质检接收时间 */
						cpqi.setQualityReceiveTime(null);
						/** 质检完成时间 */
						cpqi.setQualityFinishTime(null);
						/** 质检提交人 */
						cpqi.setQualitySubmitUser(null);
						/** 质检接收人 */
						cpqi.setQualityReceiveUser(null);
						/** 质检是否提交 */
						cpqi.setSubmit("0");

						String ct = "";
						if (type.equals("zj01")) {// 自主过程干细胞检测
							ct = "自主检测";
							cpqi.setCellType("1");

						} else if (type.equals("zj02")) {// 第三方检测
							ct = "第三方检测";
							cpqi.setCellType("5");
						}
						String pj = "质检明细:" + "样本编号:" + c + "样本名称:" + dd.getName() + "检验量:" + jcsl + "单位:" + jcsldz
								+ "产品批号:" + scp.getBatch() + "姓名:" + scp.getSampleOrder().getName() + "筛选号:"
								+ scp.getSampleOrder().getFiltrateCode() + "订单编号:" + scp.getOrderCode() + "检验项目:"
								+ scp.getProductName() + "检验方式:" + ct + "检验项:" + sd.getName() + "步骤:" + stepNum
								+ "步骤名称:" + stepName;
						if (pj != null && !"".equals(pj)) {
							LogInfo li = new LogInfo();
							li.setLogDate(new Date());
							li.setUserId(u.getId());
							li.setState("1");
							li.setStateName("数据新增");
							li.setClassName("CellPassage");
							li.setFileId(scp.getCellPassage().getId());
							li.setModifyContent(pj);
							commonDAO.saveOrUpdate(li);
						}
						cellPassageDao.saveOrUpdate(cpqi);

//						cpt.setCellSampleTaleId(cpqi.getId());
//						cellPassageDao.saveOrUpdate(cpt);
						cellPassageDao.saveOrUpdate(scp);

					}

				}
			}

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitToQualityTestSample(String[] ids, String changeLogItem, String itemId,String orderNum) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		Date date2 = new Date();
		Date date = date2;
		Date d = date;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

//		if (ids == null || ids.length < 1) {
//
//		} else {
//			for (String itemId : ids) {
		CellPassageQualityItem scp = commonDAO.get(CellPassageQualityItem.class, itemId);
		if (scp != null) {
//					CellPassage cp = commonDAO.get(CellPassage.class,);
			SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyion().getId());

			QualityTestTemp cpt = new QualityTestTemp();
			cpt.setCode(scp.getCode());
			cpt.setSampleNumber(scp.getSampleNumber());
			cpt.setState("1");
			cpt.setMark("CellPassage");
			cpt.setZjName(sd.getName());
			cpt.setTestItem(sd.getName());
			cpt.setExperimentalSteps(scp.getStepNum());
			cpt.setExperimentalStepsName(scp.getExperimentalStepsName());
			cpt.setSampleNumUnit(scp.getSampleNumUnit());

			cpt.setOrderCode(scp.getOrderCode());
			if (scp.getOrderCode() != null && !"".equals(scp.getOrderCode())) {
				SampleOrder so = commonDAO.get(SampleOrder.class, scp.getOrderCode());
				cpt.setSampleOrder(so);
			}

			cpt.setParentId(scp.getCode());
			cpt.setSampleCode(scp.getCode());
			cpt.setProductId(scp.getProductId());
			cpt.setProductName(scp.getProductName());

			cpt.setSampleType(scp.getSampleType());
			cpt.setNote(scp.getNote());
			if (scp.getSampleNum() != null && !"".equals(scp.getSampleNum())) {
				cpt.setSampleNum(String.valueOf(scp.getSampleNum()));
			}

			cpt.setSampleInfo(scp.getSampleInfo());
//					cpt.setScopeId(scp.getScopeId());
//					cpt.setScopeName(scp.getScopeName());

			cpt.setSampleDeteyion(sd);
			cpt.setCellType(scp.getCellType());

			cpt.setBatch(scp.getBatch());// 批次
			cpt.setCellSampleTaleId(scp.getId());

			cpt.setQualitySubmitTime(d);
			commonDAO.saveOrUpdate(cpt);

			/** 是否合格 */
			scp.setQualified(null);
			/** 质检结果表id */
			scp.setQualityInfoId(null);
			/** 质检是否接收 */
			scp.setQualityReceive(null);
			/** 质检提交时间 */
			scp.setQualitySubmitTime(d);
			/** 质检接收时间 */
			scp.setQualityReceiveTime(null);
			/** 质检完成时间 */
			scp.setQualityFinishTime(null);
			/** 质检提交人 */
			scp.setQualitySubmitUser(u.getName());
			/** 质检接收人 */
			scp.setQualityReceiveUser(null);
			/** 质检是否提交 */
			scp.setSubmit("1");
			commonDAO.saveOrUpdate(scp);
			
			String now = format.format(d);
			Date parse = format.parse(now);
			List<CellProductionRecord> cellRecord = cellPassageDao.fidnCellProductionRecordByCellId(scp.getCellPassage().getId(),orderNum);
			if(!cellRecord.isEmpty()) {
			
			String operatingRoomId = cellRecord.get(0).getOperatingRoomId();
			
			if(!"".equals(operatingRoomId)&&operatingRoomId!=null) {
				RoomManagement roomManagement = commonDAO.get(RoomManagement.class, operatingRoomId);
				RegionalManagement regionalManagement = roomManagement.getRegionalManagement();
				if(regionalManagement!=null) {
					List<TransferWindowManage> findTransferWindowManage = cellPassageDao.findTransferWindowManage(regionalManagement.getId());
					if(!findTransferWindowManage.isEmpty()) {
						TransferWindowState transferWindowState = new TransferWindowState();
						transferWindowState.setBatch(cpt.getBatch());
						transferWindowState.setCode(scp.getSampleNumber());
						SampleDeteyion sampleDeteyion = commonDAO.get(SampleDeteyion.class,scp.getSampleDeteyion().getId());
						if(sampleDeteyion!=null) {
							transferWindowState.setSampleDetecyion(sampleDeteyion);
						}
						transferWindowState.setQualitySubmitTime(d);	
						transferWindowState.setQualitySubmitUser(u.getName());
						transferWindowState.setState("1");
						transferWindowState.setTransferWindowManage(findTransferWindowManage.get(0));
						commonDAO.saveOrUpdate(transferWindowState);
						
					}
				}
			}
			
			}
//			CellProductionRecord
			// 提醒事项
			List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "QC001");
			for (UserGroupUser QAugu : QAugus) {
				SysRemind srz = new SysRemind();
				srz.setId(null);
				// 提醒类型
				srz.setType(null);
				// 事件标题
				srz.setTitle("质检提交提醒");
				// 发起人（系统为SYSTEM）
				srz.setRemindUser(u.getName());
				// 发起时间
				srz.setStartDate(date);
				// 查阅提醒时间
				srz.setReadDate(date);
				// 提醒内容
				srz.setContent("样本" + scp.getCode() + "的" + scp.getStepNum() + "质检" + sd.getName() + "已提交,请确认!");
				// 状态 0.草稿 1.已阅
				srz.setState("0");
				// 提醒的用户
				srz.setHandleUser(QAugu.getUser());
				if ("1".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp1");
				} else if ("2".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp2");
				} else if ("3".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp3");
				} else if ("4".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp4");
				} else if ("5".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp5");
				} else if ("6".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp6");
				} else if ("7".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp7");
				} else if ("8".equals(scp.getCellType())) {
					// 表单id
					srz.setContentId("");
					srz.setTableId("QualityTestTemp8");
				}
				commonDAO.saveOrUpdate(srz);
			}
			String tjzj = "细胞生产：" + "步骤" + scp.getStepNum() + "：样本为" + scp.getCode() + " 检测项为" + sd.getName()
					+ "的数据已提交";
			if (tjzj != null && !"".equals(tjzj)) {
				LogInfo li = new LogInfo();
				li.setLogDate(date);
				li.setFileId(scp.getCellPassage().getId());
				li.setClassName("CellPassage");
				li.setUserId(u.getId());
				li.setState("1");
				li.setStateName("数据新增");
				li.setModifyContent(tjzj);
				commonDAO.saveOrUpdate(li);
			}
		}

//			}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellPassageQualityItem> findQualityTestSampleByCode(String code) throws Exception {
		return cellPassageDao.findQualityTestSampleByCode(code);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveChildTable(String id, String dataJson) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar instance = Calendar.getInstance();
		int amount = 0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		CellPassage cp = commonDAO.get(CellPassage.class, id);
		for (Map<String, Object> map : list) {
			CellPassageTemplate template = new CellPassageTemplate();
			template = (CellPassageTemplate) commonDAO.Map2Bean(map, template);
			if (template.getId() != null && !"".equals(template.getId())) {
				String planEndDate = template.getPlanEndDate();
				String planWorkDate = template.getPlanWorkDate();
				String delay = template.getDelay();
				if (planWorkDate != null && !"".equals(planWorkDate) && planEndDate != null && !"".equals(planEndDate)
						&& delay != null && !"".equals(delay)) {

					Date date = format.parse(planEndDate);
					amount += Integer.parseInt(delay);
					instance.setTime(date);
					instance.add(Calendar.HOUR, amount);
					date = instance.getTime();
					planEndDate = format.format(date);
					template.setPlanEndDate(planEndDate);

					Date date1 = format.parse(planWorkDate);
					amount += Integer.parseInt(delay);
					instance.setTime(date1);
					instance.add(Calendar.HOUR, amount);
					date1 = instance.getTime();
					planWorkDate = format.format(date);
					template.setPlanWorkDate(planWorkDate);

					updateNextPlanDate(cp.getId(), template.getOrderNum(), amount);
					// json自定义字段,将table中填入的姓名同步到到步骤的操作人上
					CellPassageTemplate t = commonDAO.get(CellPassageTemplate.class, template.getId());
					String contentData = t.getContentData();
					if (contentData != null && !"".equals(contentData)) {
						JSONObject jsonObject = JSONObject.fromObject(contentData);
						jsonObject.put("testUserList", template.getTestUserList());
						template.setContentData(jsonObject.toString());
					} else {
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("testUserList", template.getTestUserList());
						template.setContentData(jsonObject.toString());
					}
				}
				template.setCellPassage(cp);
				commonDAO.merge(template);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void updateNextPlanDate(String id, String orderNum, int amount) throws Exception {
		Calendar instance = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<CellPassageTemplate> list = cellPassageDao.findNextDataByOrderNum(id, orderNum);
		for (CellPassageTemplate tt : list) {
			String planEndDate = tt.getPlanEndDate();
			String planWorkDate = tt.getPlanWorkDate();
			if (planEndDate != null && !"".equals(planEndDate)) {
				Date date = format.parse(planEndDate);
				instance.setTime(date);
				instance.add(Calendar.HOUR, amount);
				date = instance.getTime();
				planEndDate = format.format(date);
				tt.setPlanEndDate(planEndDate);
			}
			if (planWorkDate != null && !"".equals(planWorkDate)) {
				Date parse = format.parse(planWorkDate);
				instance.setTime(parse);
				instance.add(Calendar.HOUR, amount);
				parse = instance.getTime();
				planWorkDate = format.format(parse);
				tt.setPlanWorkDate(planWorkDate);
			}
			commonDAO.saveOrUpdate(tt);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public synchronized void blendSample(String[] ids) throws CloneNotSupportedException, InterruptedException {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Thread.sleep(2000);

		String id = "";
		for (int i = 0; i < ids.length; i++) {
			id = ids[0];
		}
		CellPassageItem item = commonDAO.get(CellPassageItem.class, id);
//		item.setSampleType(null);
//		item.setDicSampleTypeName(null);
//		item.setDicSampleTypeId(null);
//		commonDAO.saveOrUpdate(item);
		String orderCode = item.getBatch();// item.getOrderCode();
//		String code= item.getCode();

		CellPassageItem cloneItem = cellPassageDao.getCellPassageItems(item.getCellPassage().getId(), item.getStepNum(),
				"1");
		if (cloneItem != null) {

		} else {
			cloneItem = (CellPassageItem) item.clone();
			cloneItem.setSampleType(null);
			cloneItem.setId(null);
			cloneItem.setCode(orderCode);
			cloneItem.setSampleCode(orderCode);
			cloneItem.setBlendState("1");

			if (item.getInstrument() != null && item.getCounts() != null && item.getPosId() != null
					&& !"1".equals(item.getInventoryStatus())) {

				IncubatorSampleInfo isi = cellPassageDao.getIncubatorSampleInfoByCountsPosid(item.getCounts(),
						item.getPosId());
				if (isi != null) {
					Instrument ii = commonDAO.get(Instrument.class, item.getCounts());

					isi.setState("0");
					commonDAO.saveOrUpdate(isi);

					IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
					isio.setId(null);
					isio.setBatch(isi.getBatch());
					isio.setIncubatorId(isi.getIncubatorId());
					isio.setLocation(isi.getLocation());
					isio.setSampleOutDate(new Date());
					isio.setSampleOutUser(user);
					isio.setTaskId(item.getCellPassage().getId());
					isio.setTaskStepNum(String.valueOf(item.getStepNum()));
					cellPassageDao.saveOrUpdate(isio);

					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);

					commonDAO.saveOrUpdate(ii);
				}

				item.setInventoryStatus("1");
			}

			cloneItem.setInstrument(null);
			cloneItem.setPosId("");
			cloneItem.setCounts("");
			cloneItem.setInventoryStatus("");

//			if(item.getInventoryStatus()!=null
//					&&"1".equals(item.getInventoryStatus())){
//				cloneItem.setInstrument(null);
//				cloneItem.setPosId("");
//				cloneItem.setCounts("");
//				cloneItem.setInventoryStatus("");
//			}else{
//				
//			}
			commonDAO.saveOrUpdate(cloneItem);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(item.getCellPassage().getId());
				li.setClassName("CellPassage");
				li.setModifyContent("样本信息生成结果：" + item.getBatch());
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public synchronized void blendSampleClick(String id, String stepNum)
			throws CloneNotSupportedException, InterruptedException {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Thread.sleep(2000);

		// 主表id和步骤号查询子表 有数据不继续，没有数据继续
		List<CellPassageItem> cpis = cellPassageDao.getCellPassageItemInfos(id, stepNum, "1");
		if (cpis.size() > 0) {

		} else {
			List<CellPassageItem> cpiis = cellPassageDao.getCellPassageItemInfos(id, stepNum, "0");
			if (cpiis.size() > 0) {
				CellPassageItem item = cpiis.get(0);
				String orderCode = item.getBatch();// item.getOrderCode();

				CellPassageItem cloneItem = cellPassageDao.getCellPassageItems(item.getCellPassage().getId(),
						item.getStepNum(), "1");
				if (cloneItem != null) {

				} else {
					cloneItem = (CellPassageItem) item.clone();
					cloneItem.setSampleType(null);
					cloneItem.setId(null);
					cloneItem.setCode(orderCode);
					cloneItem.setSampleCode(orderCode);
					cloneItem.setBlendState("1");

					if (item.getInstrument() != null && item.getCounts() != null && item.getPosId() != null
							&& !"1".equals(item.getInventoryStatus())) {

						IncubatorSampleInfo isi = cellPassageDao.getIncubatorSampleInfoByCountsPosid(item.getCounts(),
								item.getPosId());
						if (isi != null) {
							Instrument ii = commonDAO.get(Instrument.class, item.getCounts());

							isi.setState("0");
							commonDAO.saveOrUpdate(isi);

							IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
							isio.setId(null);
							isio.setBatch(isi.getBatch());
							isio.setIncubatorId(isi.getIncubatorId());
							isio.setLocation(isi.getLocation());
							isio.setSampleOutDate(new Date());
							isio.setSampleOutUser(user);
							isio.setTaskId(item.getCellPassage().getId());
							isio.setTaskStepNum(String.valueOf(item.getStepNum()));
							cellPassageDao.saveOrUpdate(isio);

							ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);

							commonDAO.saveOrUpdate(ii);
						}

						item.setInventoryStatus("1");
					}

					cloneItem.setInstrument(null);
					cloneItem.setPosId("");
					cloneItem.setCounts("");
					cloneItem.setInventoryStatus("");

					commonDAO.saveOrUpdate(cloneItem);
					if (cpiis.size() > 0) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						User u = (User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY);
						li.setUserId(u.getId());
						li.setFileId(item.getCellPassage().getId());
						li.setClassName("CellPassage");
						li.setModifyContent("样本信息生成结果：" + item.getBatch());
						li.setState("1");
						li.setStateName("数据新增");
						commonDAO.saveOrUpdate(li);
					}
				}
			} else {

			}
		}

	}

	// 样本出库
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getblendSample(String[] ids) throws CloneNotSupportedException {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		String id = "";
		for (int i = 0; i < ids.length; i++) {
			id = ids[i];

			CellPassageItem item = commonDAO.get(CellPassageItem.class, id);

			if (item.getInstrument() != null && item.getCounts() != null && item.getPosId() != null
					&& !"1".equals(item.getInventoryStatus())) {

				IncubatorSampleInfo isi = cellPassageDao.getIncubatorSampleInfoByCountsPosid(item.getCounts(),
						item.getPosId());
				if (isi != null) {
					Instrument ii = commonDAO.get(Instrument.class, item.getCounts());

					isi.setState("0");
					commonDAO.saveOrUpdate(isi);

					IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
					isio.setId(null);
					isio.setBatch(isi.getBatch());
					isio.setIncubatorId(isi.getIncubatorId());
					isio.setLocation(isi.getLocation());
					isio.setSampleOutDate(new Date());
					isio.setSampleOutUser(user);
					isio.setTaskId(item.getCellPassage().getId());
					isio.setTaskStepNum(String.valueOf(item.getStepNum()));
					cellPassageDao.saveOrUpdate(isio);

					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);

					commonDAO.saveOrUpdate(ii);
				}

				item.setInventoryStatus("1");
			}

			String scjg = "细胞生产质检项:" + "产品批号为" + item.getBatch() + "的结果已生成";
			if (scjg != null && !"".equals(scjg)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setClassName("CellPassage");
				li.setFileId(item.getCellPassage().getId());
				li.setUserId(user.getId());
				li.setModifyContent(scjg);
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}

		}

	}

	public void stopRoom(long sleepTime, String operatingRoomName, String stepNum, String mainId, String batch,
			String templeItemName, RoomManagement rm) {
		SimpleDateFormat df111 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		if (rm != null) {
			rm.setIsFull("0");
			commonDAO.saveOrUpdate(rm);
			RoomState rs = new RoomState();
			rs.setId(null);
			rs.setRoomCode(rm.getId());
			rs.setStageName("细胞生产");
			rs.setTableTypeId(mainId);
			rs.setNote(stepNum);
			rs.setNote2(templeItemName);
			rs.setBatch(batch);

			if (batch != null && !"".equals(batch)) {
				SampleOrder order = cellPassageDao.findNameByBach(batch);
				if (order != null) {
					if (!"".equals(order.getName()) && order.getName() != null) {
						rs.setName(order.getName());
					}
				}
			} else {
				rs.setName(null);
			}
			List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
			if (cp.size() > 0) {
				CellProductionRecord cprr = cp.get(0);
				if (cprr.getTempleOperator() != null) {
					rs.setAcceptUser(cprr.getTempleOperator().getName());
				} else {
					rs.setAcceptUser("");
				}
			} else {
				rs.setAcceptUser("");
			}

			rs.setStartDate("");
			rs.setEndDate(df111.format(new Date()));
			rs.setRoomState("解除占用");
			rs.setRoomManagement(rm);
			commonDAO.saveOrUpdate(rs);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void finishPresentStep(String id, String[] ids, String stepNum, String mainId, String startTime,
			String endTime, String hhzt, String ultravioletlamptime) throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		SimpleDateFormat df111 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		List<CellProductionRecord> cprs = cellPassageDao.getCellproductionRecords(mainId, stepNum);
		String step = "";// String.valueOf(Integer.valueOf(stepNum) + 1);// 下一步
		if (cprs.size() > 0) {
			step = cprs.get(0).getOrderNum();
		} else {
			step = String.valueOf(Integer.valueOf(Double.valueOf(stepNum).intValue()) + 1);// 下一步
		}
		CellPassageTemplate ct = cellPassageDao.findCellPassageTemplateByStep(stepNum, mainId);
		CellProductionRecord cpr = cellPassageDao.findCellProductionRecordByStep(stepNum, mainId);
		CellPassageTemplate ctc = cellPassageDao.findCellPassageTemplateByStep(step, mainId);
		CellPassage cellPassage = cellPassageDao.findNameByBach(mainId, stepNum);
		String batch = cellPassage.getBatch();

		if (ctc != null) {
			ctc.setStepState("2");
			commonDAO.saveOrUpdate(ctc);
			
			List<IncubatorSampleInfo> isis = cellPassageDao
					.getIncubatorSampleInfoByCode(ct.getCellPassage().getBatch());
			if (isis.size() > 0) {
				for (IncubatorSampleInfo isi : isis) {

					Instrument ii = commonDAO.get(Instrument.class, isi.getIncubatorId());

					isi.setState("0");
					commonDAO.saveOrUpdate(isi);

					IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
					isio.setId(null);
					isio.setBatch(isi.getBatch());
					isio.setIncubatorId(isi.getIncubatorId());
					isio.setLocation(isi.getLocation());
					isio.setSampleOutDate(new Date());
					isio.setSampleOutUser(user);
					isio.setTaskId(ct.getCellPassage().getId());
					isio.setTaskStepNum(stepNum);
					cellPassageDao.saveOrUpdate(isio);

					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);

					commonDAO.saveOrUpdate(ii);
				}
			}
		} else {
			List<IncubatorSampleInfo> isis = cellPassageDao
					.getIncubatorSampleInfoByCode(ct.getCellPassage().getBatch());
			if (isis.size() > 0) {
				for (IncubatorSampleInfo isi : isis) {

					Instrument ii = commonDAO.get(Instrument.class, isi.getIncubatorId());

					isi.setState("0");
					commonDAO.saveOrUpdate(isi);

					IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
					isio.setId(null);
					isio.setBatch(isi.getBatch());
					isio.setIncubatorId(isi.getIncubatorId());
					isio.setLocation(isi.getLocation());
					isio.setSampleOutDate(new Date());
					isio.setSampleOutUser(user);
					isio.setTaskId(ct.getCellPassage().getId());
					isio.setTaskStepNum(stepNum);
					cellPassageDao.saveOrUpdate(isio);

					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);

					commonDAO.saveOrUpdate(ii);
				}
			}
		}

		List<CellProducOperation> cpoList = cellPassageDao.findCellProducOperationByStep(stepNum, mainId);

		// =================================================================================
		if (!"".equals(ultravioletlamptime) && ultravioletlamptime != null) {
			final long sleepTime;
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
			// 紫外线灯照射时间段
			String[] shineTimeSplite = ultravioletlamptime.split("-");
			String shineTime = shineTimeSplite[1];
			String[] shineHourMinSplite = shineTime.split(":");
			long parseLong = Long.parseLong(shineHourMinSplite[0]);
			long parseLong2 = Long.parseLong(shineHourMinSplite[1]);
			// 紫外线灯照射时间
			long l = parseLong * 3600000 + parseLong2 * 60000;

			String date = dateFormat.format(new Date());
			String[] split3 = date.split(":");
			long parseLong3 = Long.parseLong(split3[0]);
			long parseLong4 = Long.parseLong(split3[1]);
			// 系统时间
			long l1 = parseLong3 * 3600000 + parseLong4 * 60000;
			// 如果紫外线结束时间小
			// 如果系统时间大于紫外线照射结束时间
			if (l1 >= l) {
				// 解除房间占用
				List<RoomManagement> rmList = new ArrayList<RoomManagement>();
				rmList = commonDAO.findByProperty(RoomManagement.class, "roomName", cpr.getOperatingRoomName());
				if (!rmList.isEmpty()) {
					RoomManagement rm = new RoomManagement();
					rm = rmList.get(0);

					rm.setIsFull("0");
					commonDAO.saveOrUpdate(rm);
					RoomState rs = new RoomState();
					rs.setId(null);
					rs.setRoomCode(rm.getId());
					rs.setStageName("细胞生产");
					rs.setTableTypeId(mainId);
					rs.setNote(stepNum);
					rs.setNote2(cpr.getTempleItem().getName());
					rs.setBatch(batch);

					if (batch != null && !"".equals(batch)) {
						SampleOrder order = cellPassageDao.findNameByBach(batch);
						if (order != null) {
							if (!"".equals(order.getName()) && order.getName() != null) {
								rs.setName(order.getName());
							}
						}
					} else {
						rs.setName(null);
					}
					List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(mainId,
							stepNum);
					if (cp.size() > 0) {
						CellProductionRecord cprr = cp.get(0);
						if (cprr.getTempleOperator() != null) {
							rs.setAcceptUser(cprr.getTempleOperator().getName());
						} else {
							rs.setAcceptUser("");
						}
					} else {
						rs.setAcceptUser("");
					}

					rs.setStartDate("");
					rs.setEndDate(df111.format(new Date()));
					rs.setRoomState("解除占用");
					rs.setRoomManagement(rm);
					commonDAO.saveOrUpdate(rs);
				}
			} else {
				List<RoomManagement> list = commonDAO.findByProperty(RoomManagement.class, "roomName",
						cpr.getOperatingRoomName());

				long a = l - l1;
				sleepTime = a;
				CellPassageRunnable testRunnable = new CellPassageRunnable(sleepTime, cpr.getOperatingRoomName(),
						stepNum, mainId, batch, cpr.getTempleItem().getName(), list.get(0));
				Thread thread = new Thread(testRunnable);
				thread.start();
//				ThreadGroup currentGroup = Thread.currentThread().getThreadGroup();
//				int noThreads = currentGroup.activeCount();
//				Thread[] lstThreads = new Thread[noThreads];
//				currentGroup.enumerate(lstThreads);
//				for (int i = 0; i < noThreads; i++) {
//					System.out.println("线程号：" + lstThreads[i].getId() + " = " + lstThreads[i].getName());
//				}
//				String name = thread.getName();
//				State state = thread.getState();
//				System.out.println("name=" + name);
//				System.out.println("state=" + state);

			}

		} else {

			// 解除房间占用
			List<RoomManagement> rmList = new ArrayList<RoomManagement>();
			rmList = commonDAO.findByProperty(RoomManagement.class, "roomName", cpr.getOperatingRoomName());
			if (!rmList.isEmpty()) {
				RoomManagement rm = new RoomManagement();
				rm = rmList.get(0);

				rm.setIsFull("0");
				commonDAO.saveOrUpdate(rm);
				RoomState rs = new RoomState();
				rs.setId(null);
				rs.setRoomCode(rm.getId());
				rs.setStageName("细胞生产");
				rs.setTableTypeId(mainId);
				rs.setNote(stepNum);
				rs.setNote2(cpr.getTempleItem().getName());
				rs.setBatch(batch);

				if (batch != null && !"".equals(batch)) {
					SampleOrder order = cellPassageDao.findNameByBach(batch);
					if (order != null) {
						if (!"".equals(order.getName()) && order.getName() != null) {
							rs.setName(order.getName());
						}
					}
				} else {
					rs.setName(null);
				}
				List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(mainId,
						stepNum);
				if (cp.size() > 0) {
					CellProductionRecord cprr = cp.get(0);
					if (cprr.getTempleOperator() != null) {
						rs.setAcceptUser(cprr.getTempleOperator().getName());
					} else {
						rs.setAcceptUser("");
					}
				} else {
					rs.setAcceptUser("");
				}

				rs.setStartDate("");
				rs.setEndDate(df111.format(new Date()));
				rs.setRoomState("解除占用");
				rs.setRoomManagement(rm);
				commonDAO.saveOrUpdate(rs);
			}

		}
		// 解除仪器
		List<CellproductCos> ccList = new ArrayList<CellproductCos>();
		if (!cpoList.isEmpty()) {
			for (CellProducOperation cpo : cpoList) {
				ccList = cellPassageDao.getCellproductCos(cpo.getId());
				if (!ccList.isEmpty()) {
					for (CellproductCos cc : ccList) {
						if (cc.getInstrumentCode() != null && !"".equals(cc.getInstrumentCode())) {
							Instrument it = new Instrument();
							it = commonDAO.get(Instrument.class, cc.getInstrumentCode());

							if (it.getIsFull() != null && "1".equals(it.getIsFull())) {
								InstrumentState is = new InstrumentState();
								is.setId(null);
								is.setInstrumentCode(it.getId());
								is.setStageName("细胞生产");
								is.setTableTypeId(mainId);

								is.setBatch(batch);

								if (batch != null && !"".equals(batch)) {
									SampleOrder order = cellPassageDao.findNameByBach(batch);
									if (order != null) {
										if (!"".equals(order.getName()) && order.getName() != null) {
											is.setName(order.getName());

										}
									}
								} else {
									is.setName(null);

								}

								is.setNote(stepNum);
								is.setNote2(cpr.getTempleItem().getName());

								// 查询第一步实验操作人 按实验id和步骤查询

								List<CellProductionRecord> cp = cellPassageDao
										.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
								if (cp.size() > 0) {
									CellProductionRecord cprr = cp.get(0);
									if (cprr.getTempleOperator() != null) {
										is.setAcceptUser(cprr.getTempleOperator().getName());

									} else {
										is.setAcceptUser("");

									}
								} else {
									is.setAcceptUser("");

								}
								is.setStartDate("");
								is.setEndDate(df111.format(new Date()));
								is.setInstrumentState("解除占用");
								is.setInstrument(it);

								commonDAO.saveOrUpdate(is);

							} else {
								if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
									InstrumentState is = new InstrumentState();
									is.setId(null);
									is.setInstrumentCode(it.getId());
									is.setStageName("细胞生产");
									is.setTableTypeId(mainId);
									is.setBatch(batch);
									is.setNote(stepNum);
									is.setNote2(cpr.getTempleItem().getName());

									if (batch != null && !"".equals(batch)) {
										SampleOrder order = cellPassageDao.findNameByBach(batch);
										if (order != null) {
											if (!"".equals(order.getName()) && order.getName() != null) {
												is.setName(order.getName());

											}
										}
									} else {
										is.setName(null);

									}

									// 查询第一步实验操作人 按实验id和步骤查询

									List<CellProductionRecord> cp1 = cellPassageDao
											.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
									if (cp1.size() > 0) {
										CellProductionRecord cprr = cp1.get(0);
										if (cprr.getTempleOperator() != null) {
											is.setAcceptUser(cprr.getTempleOperator().getName());
										} else {
											is.setAcceptUser("");
										}
									} else {
										is.setAcceptUser("");
									}
									is.setStartDate("");
									is.setEndDate(df111.format(new Date()));
									is.setInstrumentState("结束使用");
									is.setInstrument(it);

									commonDAO.saveOrUpdate(is);

								}
							}

							it.setIsFull("0");
							commonDAO.saveOrUpdate(it);
						}
					}
				}
			}
		}
		if (ct != null) {
			if (ct.getEndTime() != null && !"".equals(ct.getEndTime())) {

			} else {
				ct.setStepState("0");

				Date t = new Date();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				ct.setEndTime(df.format(t));
				List<CellPassageItem> itemList = cellPassageDao.findCellPassageItemListByStep(stepNum, mainId, hhzt);
				if (itemList != null && itemList.size() > 0) {
					// commonDAO.deleteAll(itemList);
					if (ctc != null) {
						for (CellPassageItem item : itemList) {
							// CellPassageItem item = commonDAO.get(CellPassageItem.class, id);
							CellPassageItem cloneItem = (CellPassageItem) item.clone();
							cloneItem.setId(null);
							cloneItem.setBlendState("0");
							cloneItem.setStepNum(step);
							
							
							if(item.getCounts()!=null
									&&item.getPosId()!=null
									&&item.getInstrument()!=null) {
								cloneItem.setInstrument(item.getInstrument());
								cloneItem.setPosId(item.getPosId());
								cloneItem.setCounts(item.getCounts());
								cloneItem.setInstrumentOld(item.getInstrument());
								cloneItem.setPosIdOld(item.getPosId());
								cloneItem.setCountsOld(item.getCounts());
							}else {
								cloneItem.setInstrumentOld(null);
								cloneItem.setPosIdOld("");
								cloneItem.setCountsOld("");
								cloneItem.setInstrument(null);
								cloneItem.setPosId("");
								cloneItem.setCounts("");
							}
							
							
							commonDAO.saveOrUpdate(cloneItem);

							CellPassageItemCheckCode cicc = new CellPassageItemCheckCode();
							cicc.setId(null);
							cicc.setBlendState("0");
							cicc.setCellPassage(cloneItem.getCellPassage());
							cicc.setCode(cloneItem.getCode());
							cicc.setState("未确认");
							cicc.setStepNum(cloneItem.getStepNum());
							commonDAO.saveOrUpdate(cicc);

							// 下一步直接生成 质检信息结果直接生成
							CellPassageItem cloneItem2 = (CellPassageItem) item.clone();
							cloneItem2.setId(null);

							if (cloneItem2.getCode() != null && !"".equals(cloneItem2.getCode())) {
								String itemaftercode = cloneItem2.getCode().substring(0,
										cloneItem2.getCode().length() - 1);
								List<TemplateItem> tis = cellPassageDao
										.getTemplateItems(cloneItem2.getCellPassage().getTemplate(), step);
								itemaftercode = itemaftercode + tis.get(0).getPrintLabelCoding();
								cloneItem2.setCode(itemaftercode);
							}

							cloneItem2.setBlendState("1");
							cloneItem2.setStepNum(step);
							
							
							
//							cloneItem.setInstrument(item.getInstrument());
//							cloneItem.setPosId(item.getPosId());
//							cloneItem.setCounts(item.getCounts());
							
							if(item.getCounts()!=null
									&&item.getPosId()!=null
									&&item.getInstrument()!=null) {
								cloneItem2.setInstrumentOld(item.getInstrument());
								cloneItem2.setPosIdOld(item.getPosId());
								cloneItem2.setCountsOld(item.getCounts());
								TemplateItem cp = cellPassageDao.findTemplateItemByStep(cloneItem2.getStepNum(),
										ct.getCellPassage().getTemplate().getId());
								if(cp!=null
										&&"1".equals(cp.getIncubator())) {
									cloneItem2.setInstrument(item.getInstrument());
									cloneItem2.setPosId(item.getPosId());
									cloneItem2.setCounts(item.getCounts());
								}
							}else {
								cloneItem2.setInstrumentOld(null);
								cloneItem2.setPosIdOld("");
								cloneItem2.setCountsOld("");
								cloneItem2.setInstrument(null);
								cloneItem2.setPosId("");
								cloneItem2.setCounts("");
							}
							
							commonDAO.saveOrUpdate(cloneItem2);
							
							if(item.getCounts()!=null
									&&item.getPosId()!=null
									&&item.getInstrument()!=null) {
								
								IncubatorSampleInfo isi = new IncubatorSampleInfo();
								isi.setId(null);
								isi.setBatch(item.getBatch());
								isi.setIncubatorId(item.getInstrument().getId());
								isi.setLocation(item.getPosId());
								isi.setSampleOrder(item.getSampleOrder());
								isi.setState("1");
								isi.setTaskId(item.getCellPassage().getId());
								isi.setTaskStepNum(String.valueOf(cloneItem2.getStepNum()));
								isi.setsId(cloneItem2.getId());
								cellPassageDao.saveOrUpdate(isi);

								IncubatorSampleInfoIn isii = new IncubatorSampleInfoIn();
								isii.setBatch(item.getBatch());
								isii.setId(null);
								isii.setIncubatorId(item.getInstrument().getId());
								isii.setLocation(item.getPosId());
								isii.setSampleInDate(new Date());
								isii.setSampleInUser(user);
								isii.setTaskId(item.getCellPassage().getId());
								isii.setTaskStepNum(String.valueOf(cloneItem2.getStepNum()));
								cellPassageDao.saveOrUpdate(isii);

								Instrument ii = commonDAO.get(Instrument.class, item.getInstrument().getId());
								ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() - 1);
								commonDAO.saveOrUpdate(ii);
							}
							

							CellPassageItemCheckCode cicc2 = new CellPassageItemCheckCode();
							cicc2.setId(null);
							cicc2.setBlendState("1");
							cicc2.setCellPassage(cloneItem2.getCellPassage());
							cicc2.setCode(cloneItem2.getCode());
							cicc2.setState("未确认");
							cicc2.setStepNum(cloneItem2.getStepNum());
							commonDAO.saveOrUpdate(cicc2);
							
							
							
							
							
							List<SampleOrder> sos = cellPassageDao.findSampleOrders(cellPassage.getBatch());
							List<TemplateItem> tis = cellPassageDao.getTemplateItems(cellPassage.getTemplate().getId(),
									stepNum);
							String printLabelCoding = tis.get(0).getPrintLabelCoding();
							int parseInt = Integer.parseInt(stepNum);
							parseInt += 1;
							String productCodes = "";
							List<TempleProducingCell> templeProductingCell = cellPassageDao
									.getTempleProductingCellNew(cellPassage.getTemplate().getId(), parseInt);

							if (templeProductingCell != null) {
								
								String s = "";
								for (TempleProducingCell templeProducingCell : templeProductingCell) {
									if ("".equals(s)) {
										s += "'" + templeProducingCell.getId() + "'";
									} else {
										s += "," + "'" + templeProducingCell.getId() + "'";
									}

								}
								String aa = "in (" + s + ")";
								List<ZhiJianItem> findCellZhiJianAll = cellPassageDao.findCellZhiJianAll(aa, String.valueOf(parseInt));
								for (ZhiJianItem zhiJianItem : findCellZhiJianAll) {
									List<ZhiJianItem> zhiJian = cellPassageDao.findZhiJianNewById(aa, zhiJianItem.getCode(),
											String.valueOf(parseInt));
									for (int i = 1; i <= zhiJian.size(); i++) {
										
//								for (TempleProducingCell templeProducingCell : templeProductingCell) {
//									List<ZhiJianItem> findZhiJian = cellPassageDao
//											.findZhiJian(templeProducingCell.getId());

//									if (!findZhiJian.isEmpty()) {
//										for (ZhiJianItem zhiJian : findZhiJian) {

											SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, zhiJian.get(i-1).getCode());
											DicSampleType dd = commonDAO.get(DicSampleType.class,
													zhiJian.get(i-1).getSampleTypeId());

											// 质检条形码
											// 姓名缩写-筛选好-监测项目-随机号-采血轮次-生产工序-监测项缩写
											if (sos.size() > 0 && tis.size() > 0) {
												productCodes = sos.get(0).getAbbreviation() + "-" + sos.get(0).getFiltrateCode()
														+ "-" + sos.get(0).getProductId() + "-" + sos.get(0).getRandomCode() + "-"
														+ sos.get(0).getRound() + "-" + printLabelCoding + "-"
														+ sd.getDeteyionName() + "-" + i;
											}

											String scgxdm = tis.get(0).getPrintLabelCoding();
											String jcid = sd.getDeteyionName();
											String ybbh = dd.getCode();

											int a = cloneItem.getCode().lastIndexOf("-");
											String b = cloneItem.getCode().substring(0, a + 1);
											String c = b + scgxdm + ybbh + jcid;

											CellPassageQualityItem cpqi = new CellPassageQualityItem();
											/** 提交表编码 */
											cpqi.setTempId(cloneItem.getId());
											/** 样本编号 */
//											cpqi.setCode(cloneItem.getCode());
											cpqi.setSampleNumber(productCodes);
											/** 批次号 */
											cpqi.setBatch(cloneItem.getBatch());
											/** 订单编号 */
											cpqi.setOrderCode(cloneItem.getOrderCode());
											/** 检测项目 */
											cpqi.setProductId(cloneItem.getProductId());
											/** 检测项目 */
											cpqi.setProductName(cloneItem.getProductName());
											/** 状态 */
											cpqi.setState("1");
//										/** 备注 */
//										cpqi.setNote(jcbz);
											/** 浓度 */
											cpqi.setConcentration(null);
											/** 样本数量 */
											if (!"".equals(zhiJian.get(i-1).getSampleNum()) && zhiJian.get(i-1).getSampleNum() != null) {
												Double jcsls = Double.valueOf(zhiJian.get(i-1).getSampleNum());
												cpqi.setSampleNum(jcsls);
											}

											/** 检测项数量单位 */
											cpqi.setSampleNumUnit(zhiJian.get(i-1).getSampleNumUnit());
											/** 相关主表 */
											cpqi.setCellPassage(cellPassage);
											/** 体积 */
											cpqi.setVolume(null);
											/** 任务单id */
											cpqi.setOrderId(cellPassage.getId());
											/** 中间产物数量 */
											cpqi.setProductNum(null);
											/** 中间产物类型编号 */
											cpqi.setDicSampleTypeId(null);
											/** 中间产物类型 */
											cpqi.setDicSampleTypeName(null);
											/** 样本类型 */
											cpqi.setSampleType(dd.getName());
											/** 样本主数据 */
											cpqi.setSampleInfo(cloneItem.getSampleInfo());
											/** 实验的步骤号 */
											cpqi.setStepNum("1");
											/** 实验步骤名称 */
											cpqi.setExperimentalStepsName(tis.get(0).getName());
											/** 检测项 */
											cpqi.setSampleDeteyion(sd);
											/** 关联订单 */
											cpqi.setSampleOrder(cloneItem.getSampleOrder());
											/** 是否合格 */
											cpqi.setQualified(null);
											/** 质检结果表id */
											cpqi.setQualityInfoId(null);
											/** 质检是否接收 */
											cpqi.setQualityReceive(null);
											/** 质检提交时间 */
											cpqi.setQualitySubmitTime(null);
											/** 质检接收时间 */
											cpqi.setQualityReceiveTime(null);
											/** 质检完成时间 */
											cpqi.setQualityFinishTime(null);
											/** 质检提交人 */
											cpqi.setQualitySubmitUser(null);
											/** 质检接收人 */
											cpqi.setQualityReceiveUser(null);
											/** 质检是否提交 */
											cpqi.setSubmit("0");
//											cpqi.setProductCode(productCodes);

											String ct1 = "";
											if ("zj01".equals(zhiJian.get(i-1).getType())) {// 自主过程干细胞检测
												ct1 = "自主检测";
												cpqi.setCellType("1");

											} else if ("zj02".equals(zhiJian.get(i-1).getType())) {// 第三方检测
												ct1 = "第三方检测";
												cpqi.setCellType("5");
											}
											commonDAO.saveOrUpdate(cpqi);
											
											CellPassageItemCheckCode cicc2QC = new CellPassageItemCheckCode();
											cicc2QC.setId(null);
											cicc2QC.setCellPassage(cellPassage);

											cicc2QC.setCode(productCodes);
											cicc2QC.setBlendState("1");
											cicc2QC.setState("未确认");
											cicc2QC.setStepNum(String.valueOf(parseInt));
											commonDAO.saveOrUpdate(cicc2QC);

//										}
									}

								}
							}

						}

						// if (id != null && !"".equals(id)) {
						// CellPassageItem item = commonDAO.get(CellPassageItem.class, id);
						// CellPassageItem cloneItem = (CellPassageItem) item.clone();
						// cloneItem.setId(null);
						// cloneItem.setBlendState("0");
						// cloneItem.setStepNum(step);
						// commonDAO.saveOrUpdate(cloneItem);
						// } else {
						// for (String iid : ids) {
						// CellPassageItem item = commonDAO.get(CellPassageItem.class, iid);
						// CellPassageItem cloneItem = (CellPassageItem) item.clone();
						// cloneItem.setId(null);
						// cloneItem.setBlendState("0");
						// cloneItem.setStepNum(step);
						// commonDAO.saveOrUpdate(cloneItem);
						// }
						// }
					}

					String[] aa = stepNum.split("\\.");
					TemplateItem cp = cellPassageDao.findTemplateItemByStep(aa[0],
							ct.getCellPassage().getTemplate().getId());
					for (CellPassageItem item : itemList) {
						CellPassage cpp = commonDAO.get(CellPassage.class, mainId);
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						SampleOrder so = null;
						if (cpp.getBatch() != null && !"".equals(cpp.getBatch())) {
							List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", cpp.getBatch());
							if (sos.size() > 0) {
								so = sos.get(0);
							}
						}
						sampleStateService.saveSampleState1(cpp.getBatch(), cpp.getBatch(),
								so == null ? "" : so.getProductId(), so == null ? "" : so.getProductName(), "",
								ct.getPlanWorkDate(), ct.getEndTime(), "CellPassage", cp != null ? cp.getName() : "",
								(User) ServletActionContext.getRequest().getSession()
										.getAttribute(SystemConstants.USER_SESSION_KEY),
								cpp != null ? cpp.getId() : "", ctc == null ? "" : ctc.getName(),
								item.getQualified() == "2" ? "合格" : (item.getQualified() == "1" ? "不合格" : "警戒"), null,
								null, null, null, null, null, null, null, null, null, null, null);
					}
					if (cp != null) {
						User uu = (User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY);
						// 完工清场时下一步 如果当前步骤的模板关联的字段流到回输计划 要在回输计划左侧表插入一条数据
						/** 是否流入回输计划 */
						String reinfusionPlan = cp.getReinfusionPlan();
						if (reinfusionPlan != null && !"".equals(reinfusionPlan)) {
							
						
							if ("1".equals(reinfusionPlan)) {
								for (CellPassageItem item : itemList) {
									String substring="";
									if(cp.getTemplate()!=null) {
										 DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
									      
										Double stepsNum = cp.getTemplate().getStepsNum();
										String format = decimalFormat.format(stepsNum);
										TemplateItem ti = cellPassageDao.getBlendStateByStep(cp.getTemplate().getId(),format);
										if(ti!=null) {
											String printLabelCoding= ti.getPrintLabelCoding();
											String code =item.getCode();
											substring = code.substring(0,code.length()-1);
											substring+=printLabelCoding;
										}
									
									}
									ReinfusionPlanItem rpt = new ReinfusionPlanItem();
									/** 申请明细id */
									rpt.setId(null);
									/** 样本编号 */
									
//									rpt.setCode(item.getCode());
									rpt.setCode(substring);
									/** 状态 */
									rpt.setState("2");
									rpt.setOrderCode(item.getOrderCode());

									// 根据当前bp取当前实验收获步骤的预计操作时间
									List<CellProductionRecord> cprrs = cellPassageDao
											.findCellProductionRecordByMainId(mainId);
									if (cprrs.size() > 0) {
										CellProductionRecord cprr = cprrs.get(0);
										if (cprr != null) {
											rpt.setReinfusionPlanDate(cprr.getPlanWorkDate());
										}
									}

									SampleOrder so = commonDAO.get(SampleOrder.class, item.getOrderCode());
									if (so != null) {
										/** 病人姓名 */
										rpt.setName(so.getName());
										/** 性别 男:1 女:0 空:未知 */
										rpt.setGender(so.getGender());
										/** 产品ID */
										rpt.setProductId(so.getProductId());
										/** 产品名称 */
										rpt.setProductName(so.getProductName());
										if (so.getCrmCustomer() != null) {
											/** 医院 */
											rpt.setHospital(so.getCrmCustomer().getName());
										}
										/** 医院的联系电话 */
										rpt.setHospitalPhone(so.getMedicalInstitutionsPhone());
										/** 批次号 */
										rpt.setBatch(so.getBarcode());
//										if (ct.getEndTime() != null) {
//											SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//											String endtime = ct.getEndTime();
//											String endtimeday = endtime.substring(0, 10);
//											String endtimetime = endtime.substring(10);
//											Calendar calendar = Calendar.getInstance();
//											calendar.setTime(sdf.parse(endtimeday));
//											calendar.set(Calendar.DAY_OF_MONTH,
//													calendar.get(Calendar.DAY_OF_MONTH) + 7);
//											endtimeday = sdf.format(calendar.getTime());
//
//											endtime = endtimeday;// + endtimetime;
//
//											/** 计划回输日期 */
//											rpt.setReinfusionPlanDate(endtime);
//										}
										/** 筛选号 */
										rpt.setFiltrateCode(so.getFiltrateCode());
										if (so.getInspectionDepartment() != null) {
											/** 科室 */
											rpt.setInspectionDepartment(so.getInspectionDepartment().getName());
										}
										/** 医生 */
										rpt.setAttendingDoctor(so.getAttendingDoctor());
										/** 医生电话 */
										rpt.setAttendingDoctorPhone(so.getAttendingDoctorPhone());
										/** 家属联系方式 */
										rpt.setFamilyPhone(so.getFamilyPhone());
										/** 创建时间 */
										Date cDateT = new Date();

										SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
										rpt.setCreateDateTime(sf.format(cDateT));

										/** 订单实体 */
										rpt.setSampleOrder(so);

									}

									PlasmaReceiveItem pri = new PlasmaReceiveItem();
									pri.setId(null);
//									pri.setSampleCode(item.getCode());
									pri.setSampleCode(substring);
									if (item.getSampleOrder() != null) {
										pri.setSampleOrder(item.getSampleOrder());
									}
									if (item.getBatch() != null) {
										// pri.setBatchNumber(scp.getSampleOrder().getId());
										pri.setBatchNumber(item.getBatch());
									} else {
										pri.setBatchNumber(item.getCellPassage().getBatch());
									}
									pri.setState("1");
									pri.setCreateUser(uu);
									pri.setCreateDate(new Date());
									pri.setSpecifications("");
									pri.setCellNumber("");
									if (item.getOrderCode() != null) {
										List<QualityTestResultManage> qtrms = commonService.get(
												QualityTestResultManage.class, "sampleOrder.id", item.getOrderCode());
										if (qtrms.size() > 0) {
											for (QualityTestResultManage qtrm : qtrms) {
												if ("1".equals(qtrm.getState()) && "完成".equals(qtrm.getStateName())) {
													pri.setQualityTestPass("1");
												}
											}
										}
									}
									cellPassageDao.saveOrUpdate(pri);

									// ReinfusionPlanTemp rpt = new ReinfusionPlanTemp();
									// rpt.setId(null);
									// rpt.setCode(item.getCode());
									// rpt.setState("1");
									// rpt.setNote(item.getNote());
									// rpt.setOrderCode(item.getOrderCode());
									// SampleOrder so = commonDAO.get(SampleOrder.class, item.getOrderCode());
									// if(so!=null){
									// rpt.setName(so.getName());
									// rpt.setAge(so.getAge());
									// rpt.setGender(so.getGender());
									// rpt.setNation(so.getNation());
									// rpt.setProductId(so.getProductId());
									// rpt.setProductName(so.getProductName());
									// if(so.getCrmCustomer()!=null){
									// rpt.setHospital(so.getCrmCustomer().getName());
									// }
									// rpt.setHospitalPhone(so.getMedicalInstitutionsPhone());
									// }
									commonDAO.saveOrUpdate(rpt);

									// 提醒事项
									List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id",
											"YS001");
									for (UserGroupUser QAugu : QAugus) {
										SysRemind srz = new SysRemind();
										srz.setId(null);
										// 提醒类型
										srz.setType(null);
										// 事件标题
										srz.setTitle("回输计划");
										// 发起人（系统为SYSTEM）
										srz.setRemindUser(uu.getName());
										// 发起时间
										srz.setStartDate(new Date());
										// 查阅提醒时间
										srz.setReadDate(new Date());
										// 提醒内容
										srz.setContent("样本" + item.getCode() + "需确定回输计划!");
										// 状态 0.草稿 1.已阅
										srz.setState("0");
										// 提醒的用户
										srz.setHandleUser(QAugu.getUser());
										// 表单id
										srz.setContentId(rpt.getId());
										srz.setTableId("ReinfusionPlanItem");
										commonDAO.saveOrUpdate(srz);
									}

								}
							}
						}
						// 完工清场时下一步 如果当前步骤的模板关联的字段流到运输计划 要在运输计划左侧表插入一条数据
						/** 是否流到运输计划 */
						String transportPlan = cp.getTransportPlan();
						if (transportPlan != null && !"".equals(transportPlan)) {
							if ("1".equals(transportPlan)) {
								for (CellPassageItem item : itemList) {
									TransportApplyTemp rpt = new TransportApplyTemp();
									rpt.setId(null);
									rpt.setCode(item.getCode());
									rpt.setState("1");
									rpt.setNote(item.getNote());
									rpt.setOrderCode(item.getOrderCode());
									SampleOrder so = commonDAO.get(SampleOrder.class, item.getOrderCode());
									if (so != null) {
										rpt.setPatientName(so.getName());
										rpt.setProductId(so.getProductId());
										rpt.setProductName(so.getProductName());
										rpt.setSampleOrder(so);
										rpt.setPlanState("1");
									}
									commonDAO.saveOrUpdate(rpt);
								}
							}
						}
					}

				}
				// ct.setStartTime(startTime);
				commonDAO.saveOrUpdate(ct);
				//在填写结果表完成时添加end tIme 
				cpr.setEndTime(df.format(t));
				commonDAO.saveOrUpdate(cpr);
			}

		}
	}

	/**
	 * 
	 * 不合格样本完成 改变主表状态 不合格终止（暂时不考虑回输计划，运输计划，QC部分,QA）同时通知医事服务部人员和QCQA
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void finishPresentStepFailed(String id, String[] ids, String stepNum, String mainId, String startTime,
			String endTime, String hhzt) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		// 主表状态改为不合格终止
		CellPassage cp = commonDAO.get(CellPassage.class, mainId);
		if (cp.getState() != null && !"".equals(cp.getState()) && "1".equals(cp.getState())) {

		} else {
			cp.setState("1");
			cp.setStateName("不合格终止");
			cp.setConfirmUser(u);
			cp.setConfirmDate(df.parse(df.format(new Date())));
			commonDAO.saveOrUpdate(cp);
			// 下面步骤状态改为“0” 不显示 页面显示是null或“1”
			List<CellPassageTemplate> cpts = cellPassageDao.getCellPassageTemplates(mainId, stepNum);
			for (CellPassageTemplate cpt : cpts) {
				cpt.setState("0");
				commonDAO.saveOrUpdate(cpt);
			}
			List<CellProductionRecord> cprs = cellPassageDao.getCellproductionRecords(mainId, stepNum);
			for (CellProductionRecord cprr : cprs) {
				cprr.setState("1");
				commonDAO.saveOrUpdate(cprr);
			}
			CellPassageTemplate ct = cellPassageDao.findCellPassageTemplateByStep(stepNum, mainId);

			if (ct != null) {
				List<IncubatorSampleInfo> isis = cellPassageDao
						.getIncubatorSampleInfoByCode(ct.getCellPassage().getBatch());
				if (isis.size() > 0) {
					for (IncubatorSampleInfo isi : isis) {

						Instrument ii = commonDAO.get(Instrument.class, isi.getIncubatorId());

						isi.setState("0");
						commonDAO.saveOrUpdate(isi);

						IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
						isio.setId(null);
						isio.setBatch(isi.getBatch());
						isio.setIncubatorId(isi.getIncubatorId());
						isio.setLocation(isi.getLocation());
						isio.setSampleOutDate(new Date());
						isio.setSampleOutUser(u);
						isio.setTaskId(ct.getCellPassage().getId());
						isio.setTaskStepNum(stepNum);
						cellPassageDao.saveOrUpdate(isio);

						ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);

						commonDAO.saveOrUpdate(ii);
					}
				}
			}

			CellProductionRecord cpr = cellPassageDao.findCellProductionRecordByStep(stepNum, mainId);
			List<CellProducOperation> cpoList = cellPassageDao.findCellProducOperationByStep(stepNum, mainId);
			CellPassage cellPassage = cellPassageDao.findNameByBach(mainId, stepNum);
			String batch = cellPassage.getBatch();
			// 解除房间占用
			List<RoomManagement> rmList = new ArrayList<RoomManagement>();
			rmList = commonDAO.findByProperty(RoomManagement.class, "roomName", cpr.getOperatingRoomName());
			if (!rmList.isEmpty()) {
				RoomManagement rm = new RoomManagement();
				rm = rmList.get(0);
				rm.setIsFull("0");
				commonDAO.saveOrUpdate(rm);

				RoomState rs = new RoomState();
				rs.setId(null);
				rs.setRoomCode(rm.getId());
				rs.setStageName("细胞生产");
				rs.setTableTypeId(mainId);
				rs.setNote(stepNum);
				rs.setNote2(cpr.getTempleItem().getName());

				rs.setBatch(batch);
				if (batch != null && !"".equals(batch)) {
					SampleOrder order = cellPassageDao.findNameByBach(batch);
					if (order != null) {
						if (!"".equals(order.getName()) && order.getName() != null) {
							rs.setName(order.getName());
						}
					}
				} else {
					rs.setName(null);
				}

				// 查询第一步实验操作人 按实验id和步骤查询

				List<CellProductionRecord> cp1 = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(mainId,
						stepNum);
				if (cp1.size() > 0) {
					CellProductionRecord cprr = cp1.get(0);
					if (cprr.getTempleOperator() != null) {
						rs.setAcceptUser(cprr.getTempleOperator().getName());
					} else {

						rs.setAcceptUser("");
					}
				} else {

					rs.setAcceptUser("");
				}

				rs.setStartDate("");
				rs.setEndDate(df.format(new Date()));
				rs.setRoomState("解除占用");
				rs.setRoomManagement(rm);
				commonDAO.saveOrUpdate(rs);
			}

			// 解除仪器
			List<CellproductCos> ccList = new ArrayList<CellproductCos>();
			if (!cpoList.isEmpty()) {
				for (CellProducOperation cpo : cpoList) {
					ccList = cellPassageDao.getCellproductCos(cpo.getId());
					if (!ccList.isEmpty()) {
						for (CellproductCos cc : ccList) {
							if (cc.getInstrumentCode() != null && !"".equals(cc.getInstrumentCode())) {
								Instrument it = new Instrument();
								it = commonDAO.get(Instrument.class, cc.getInstrumentCode());
								if (it != null) {

									if (it.getIsFull() != null && "1".equals(it.getIsFull())) {
										InstrumentState is = new InstrumentState();
										is.setId(null);
										is.setInstrumentCode(it.getId());
										is.setStageName("细胞生产");
										is.setTableTypeId(mainId);
										is.setNote(stepNum);
										is.setNote2(cpr.getTempleItem().getName());

										is.setBatch(batch);

										if (batch != null && !"".equals(batch)) {
											SampleOrder order = cellPassageDao.findNameByBach(batch);
											if (order != null) {
												if (!"".equals(order.getName()) && order.getName() != null) {
													is.setName(order.getName());

												}
											}
										} else {
											is.setName(null);

										}

										// 查询第一步实验操作人 按实验id和步骤查询

										List<CellProductionRecord> cp1 = cellPassageDao
												.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
										if (cp1.size() > 0) {
											CellProductionRecord cprr = cp1.get(0);
											if (cprr.getTempleOperator() != null) {
												is.setAcceptUser(cprr.getTempleOperator().getName());
											} else {
												is.setAcceptUser("");
											}
										} else {
											is.setAcceptUser("");
										}
										is.setStartDate("");
										is.setEndDate(df.format(new Date()));
										is.setInstrumentState("解除占用");
										is.setInstrument(it);

										commonDAO.saveOrUpdate(is);

									} else {
										if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
											InstrumentState is = new InstrumentState();
											is.setId(null);
											is.setInstrumentCode(it.getId());
											is.setStageName("细胞生产");
											is.setTableTypeId(cp.getId());
											is.setNote(stepNum);
											is.setNote2(cpr.getTempleItem().getName());

											// 查询第一步实验操作人 按实验id和步骤查询
											is.setBatch(batch);

											if (batch != null && !"".equals(batch)) {
												SampleOrder order = cellPassageDao.findNameByBach(batch);
												if (order != null) {
													if (!"".equals(order.getName()) && order.getName() != null) {
														is.setName(order.getName());

													}
												}
											} else {
												is.setName(null);

											}
											List<CellProductionRecord> cp1 = cellPassageDao
													.getCellProductionRecordByIdAndStepNumForzy(cp.getId(), stepNum);
											if (cp1.size() > 0) {
												CellProductionRecord cprr = cp1.get(0);
												if (cprr.getTempleOperator() != null) {
													is.setAcceptUser(cprr.getTempleOperator().getName());

												} else {
													is.setAcceptUser("");

												}
											} else {
												is.setAcceptUser("");

											}
											is.setStartDate("");
											is.setEndDate(df.format(new Date()));
											is.setInstrumentState("结束使用");
											is.setInstrument(it);
											commonDAO.saveOrUpdate(is);
										}
									}

									it.setIsFull("0");
									commonDAO.saveOrUpdate(it);
								}
							}
						}
					}
				}
			}
			if (ct != null) {
				if (ct.getEndTime() != null && !"".equals(ct.getEndTime())) {

				} else {
					ct.setStepState("1");

					Date t = new Date();
					ct.setEndTime(df.format(t));
					commonDAO.saveOrUpdate(ct);
					if (cpr != null) {
						cpr.setEndTime(df.format(t));
						commonDAO.saveOrUpdate(cpr);
					}

					CellPassage cpp = commonDAO.get(CellPassage.class, mainId);
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SampleOrder so = null;
					if (cpp.getBatch() != null && !"".equals(cpp.getBatch())) {
						List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", cpp.getBatch());
						if (sos.size() > 0) {
							so = sos.get(0);
						}
					}
					if (so != null) {
						so.setBatchState("5");
						so.setBatchStateName("不合格");
					}
					sampleStateService.saveSampleState1(cpp.getBatch(), cpp.getBatch(),
							so == null ? "" : so.getProductId(), so == null ? "" : so.getProductName(), "",
							ct.getPlanWorkDate(), ct.getEndTime(), "CellPassage", ct != null ? ct.getName() : "",
							(User) ServletActionContext.getRequest().getSession()
									.getAttribute(SystemConstants.USER_SESSION_KEY),
							cpp != null ? cpp.getId() : "", "终止", "不合格", null, null, null, null, null, null, null, null,
							null, null, null, null);

				}

			}

			// 通知医事服务部人员和QCQA 医事服务部YS002 QA QA001 QC QC003
			List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "QA001");
			for (UserGroupUser QAugu : QAugus) {
				SysRemind sr = new SysRemind();
				sr.setId(null);
				// 提醒类型
				sr.setType(null);
				// 事件标题
				sr.setTitle("不合格终止");
				// 发起人（系统为SYSTEM）
				sr.setRemindUser(u.getName());
				// 发起时间
				sr.setStartDate(new Date());
				// 查阅提醒时间
				sr.setReadDate(new Date());
				// 提醒内容
				sr.setContent("批次号" + cp.getBatch() + "的样本不合格终止！");
				// 状态 0.草稿 1.已阅
				sr.setState("0");
				// 提醒的用户
				sr.setHandleUser(QAugu.getUser());
				// 表单id
				sr.setContentId(mainId);
				sr.setTableId("CellPassage");
				commonDAO.saveOrUpdate(sr);
			}
			List<UserGroupUser> QCugus = commonService.get(UserGroupUser.class, "userGroup.id", "QC003");// "QC001");
			for (UserGroupUser QAugu : QCugus) {
				SysRemind sr = new SysRemind();
				sr.setId(null);
				// 提醒类型
				sr.setType(null);
				// 事件标题
				sr.setTitle("不合格终止");
				// 发起人（系统为SYSTEM）
				sr.setRemindUser(u.getName());
				// 发起时间
				sr.setStartDate(new Date());
				// 查阅提醒时间
				sr.setReadDate(new Date());
				// 提醒内容
				sr.setContent("批次号" + cp.getBatch() + "的样本不合格终止！");
				// 状态 0.草稿 1.已阅
				sr.setState("0");
				// 提醒的用户
				sr.setHandleUser(QAugu.getUser());
				// 表单id
				sr.setContentId(mainId);
				sr.setTableId("CellPassage");
				commonDAO.saveOrUpdate(sr);
			}
			List<UserGroupUser> YSugus = commonService.get(UserGroupUser.class, "userGroup.id", "YS002"); // "YS001");
			for (UserGroupUser QAugu : YSugus) {
				SysRemind sr = new SysRemind();
				sr.setId(null);
				// 提醒类型
				sr.setType(null);
				// 事件标题
				sr.setTitle("不合格终止");
				// 发起人（系统为SYSTEM）
				sr.setRemindUser(u.getName());
				// 发起时间
				sr.setStartDate(new Date());
				// 查阅提醒时间
				sr.setReadDate(new Date());
				// 提醒内容
				sr.setContent("批次号" + cp.getBatch() + "的样本不合格终止！");
				// 状态 0.草稿 1.已阅
				sr.setState("0");
				// 提醒的用户
				sr.setHandleUser(QAugu.getUser());
				// 表单id
				sr.setContentId(mainId);
				sr.setTableId("CellPassage");
				commonDAO.saveOrUpdate(sr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void feedBack(String id) {
		CellPassageItem cpi = commonDAO.get(CellPassageItem.class, id);
		if (cpi != null) {
			String orderCode = cpi.getOrderCode();
			SampleOrder so = commonDAO.get(SampleOrder.class, orderCode);
			ReinfusionPlanTemp rpt = new ReinfusionPlanTemp();
			rpt.setState("1");
			rpt.setCode(cpi.getCode());
			rpt.setOrderCode(cpi.getOrderCode());
			rpt.setName(cpi.getPatientName());
			rpt.setProductId(cpi.getProductId());
			rpt.setProductName(cpi.getProductName());
			if (so != null) {
				rpt.setAge(so.getAge());
				rpt.setGender(so.getGender());
				rpt.setNation(so.getNation());
				rpt.setHospitalPhone(so.getMedicalInstitutionsPhone());
				if (so.getCrmCustomer() != null) {
					rpt.setHospital(so.getCrmCustomer().getName());
				}
			}
			commonDAO.saveOrUpdate(rpt);
		}
	}

	public List<CellproductCos> getCellproductCoss(String id, String stepNum) {
		return cellPassageDao.findCellproductCoss(id, stepNum);
	}

	public List<CellPassageObservation> getCellPassageObservations(String id, String stepNum) {
		return cellPassageDao.findCellPassageObservations(id, stepNum);
	}

	public List<CellPassageBeforeReagent> getCellPassageBeforeReagents(String id, String stepNum) {
		return cellPassageDao.findCellPassageBeforeReagents(id, stepNum);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCosPrepare(String cosListJson, String roomName, String zbid, String bzs, String roomId,
			String changeLog) {
		JSONArray array = JSONArray.fromObject(cosListJson);
		if (array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
				String id = (String) job.get("id");
				String code = (String) job.get("code");
				String name = (String) job.get("name");
				if (id != null && !"".equals(id)) {
					CellproductCos cpc = commonDAO.get(CellproductCos.class, id);
					if (cpc != null) {
						if (code != null && !"".equals(code)) {
							cpc.setInstrumentCode(code);
						}
						if (name != null && !"".equals(name)) {
							cpc.setInstrumentName(name);
						}
						cellPassageDao.saveOrUpdate(cpc);
					}
				}
			}
		}
		List<CellProductionRecord> cpct = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(zbid, bzs);
		if (cpct.size() > 0) {
			CellProductionRecord cpctz = cpct.get(0);
			if (roomName != null && !"".equals(roomName)) {
				if (cpctz != null) {
					if (cpctz.getContent() != null) {
						JSONObject jsonCon = JSONObject.fromObject(cpctz.getContent());
						jsonCon.put("operatingRoomName", roomName);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
					} else {
						JSONObject jsonCon = new JSONObject();
						jsonCon.put("operatingRoomName", roomName);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
					}
				}
			}
			if (roomId != null && !"".equals(roomId)) {
				if (cpctz != null) {
					if (cpctz.getContent() != null) {
						JSONObject jsonCon = JSONObject.fromObject(cpctz.getContent());
						jsonCon.put("operatingRoomId", roomId);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
					} else {
						JSONObject jsonCon = new JSONObject();
						jsonCon.put("operatingRoomId", roomId);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomId);
					}
				}
			}
			if (cpctz != null) {
				cellPassageDao.saveOrUpdate(cpctz);
			}

			if (changeLog != null && !"".equals(changeLog)) {
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setClassName("CellPassage");
				li.setFileId(cpctz.getCellPassage().getId());
				li.setUserId(u.getId());
				li.setState("1");
				li.setStateName("数据新增");
				li.setModifyContent(changeLog);
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void startPreparezy(String scd, String bzNa, String scp, String bzNu, String cosListJson, String roomName,
			String roomId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Boolean zy = true;

		/**
		 * 房间 设备占用 if(房间占用){ }else{ . 没占用 for(设备){ if(){ if(设备占用){ }else{ } }
		 */

		if (!"".equals(roomId) && roomId != null) {
			List<RoomManagement> list = new ArrayList<RoomManagement>();
			list = commonDAO.findByProperty(RoomManagement.class, "id", roomId);
			if (!list.isEmpty()) {
				RoomManagement rm = new RoomManagement();
				rm = list.get(0);
				if (rm != null) {
					if ("1".equals(rm.getIsFull())) {
						zy = false;
					} else {

						rm.setIsFull("1");
						commonDAO.saveOrUpdate(rm);

						RoomState rs = new RoomState();
						rs.setId(null);
						rs.setRoomCode(rm.getId());
						rs.setStageName("细胞生产");
						rs.setTableTypeId(scd);
						rs.setNote(bzNu);
						rs.setNote2(bzNa);
						rs.setBatch(scp);
						if (scp != null && !"".equals(scp)) {
							SampleOrder order = cellPassageDao.findNameByBach(scp);
							if (order != null) {
								if (!"".equals(order.getName()) && order.getName() != null) {
									rs.setName(order.getName());
								}
							}
						} else {
							rs.setName(null);
						}
						List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(scd,
								bzNu);
						if (cp.size() > 0) {
							CellProductionRecord cpr = cp.get(0);
							if (cpr.getTempleOperator() != null) {

								rs.setAcceptUser(cpr.getTempleOperator().getName());
							} else {

								rs.setAcceptUser("");
							}
						} else {

							rs.setAcceptUser("");
						}
						rs.setStartDate(sdf.format(new Date()));
						rs.setEndDate("");
						rs.setRoomState("开始占用");
						rs.setRoomManagement(rm);
						commonDAO.saveOrUpdate(rs);
					}

				}
			}
		}
		JSONArray array = JSONArray.fromObject(cosListJson);
		Map<String, String> map = new HashMap<String, String>();
		String rwdid = "";
		String bzorderNum = "";
		String bzname = "";
		String batch = "";
		if (array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
				String code = (String) job.get("code");
				if (code != null && !"".equals(code)) {
					if (map.get(code) != null && !"".equals(map.get(code)) && code.equals(map.get(code))) {

					} else {
						map.put(code, code);
						rwdid = (String) job.get("rwdid");
						bzorderNum = (String) job.get("bzorderNum");
						bzname = (String) job.get("bzname");
						batch = (String) job.get("batch");
						Instrument it = new Instrument();
						it = commonDAO.get(Instrument.class, code);
						if (it != null) {
							if ("1".equals(it.getIsFull())) {
								zy = false;
							} else {

							}
						}
					}
				}
			}
			if (zy) {
				for (String key : map.keySet()) {// keySet获取map集合key的集合  然后在遍历key即可
					String code = map.get(key).toString();
					if (code != null && !"".equals(code)) {
						Instrument it = new Instrument();
						it = commonDAO.get(Instrument.class, code);
						if (it != null) {
							if ("1".equals(it.getIsFull())) {
								zy = false;
							} else {
								if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {

								} else {
									it.setIsFull("1");
								}

								commonDAO.saveOrUpdate(it);

								InstrumentState is = new InstrumentState();
								is.setId(null);
								is.setInstrumentCode(it.getId());
								is.setStageName("细胞生产");
								is.setTableTypeId(rwdid);
								is.setNote(bzorderNum);
								is.setNote2(bzname);
								is.setBatch(batch);

								if (batch != null && !"".equals(batch)) {
									SampleOrder order = cellPassageDao.findNameByBach(batch);
									if (order != null) {
										if (!"".equals(order.getName()) && order.getName() != null) {
											is.setName(order.getName());

										}
									}
								} else {
									is.setName(null);

								}

								// 查询第一步实验操作人 按实验id和步骤查询

								List<CellProductionRecord> cp = cellPassageDao
										.getCellProductionRecordByIdAndStepNumForzy(rwdid, bzorderNum);
								if (cp.size() > 0) {
									CellProductionRecord cpr = cp.get(0);
									if (cpr.getTempleOperator() != null) {
										is.setAcceptUser(cpr.getTempleOperator().getName());

									} else {
										is.setAcceptUser("");

									}
								} else {
									is.setAcceptUser("");

								}
								is.setStartDate(sdf.format(new Date()));
								is.setEndDate("");

								if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
									is.setInstrumentState("开始使用");
								} else {
									is.setInstrumentState("开始占用");
								}

								is.setInstrument(it);
								commonDAO.saveOrUpdate(is);

							}
						}
					}
				}
//				for (int i = 0; i < array.size(); i++) {
//					JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
//					String code = (String) job.get("code");
//					if (code != null && !"".equals(code)) {
//						Instrument it = new Instrument();
//						it = commonDAO.get(Instrument.class, code);
//						if(it!=null){
//							if("1".equals(it.getIsFull())){
//								zy = false;
//							}else{
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									
//								}else{
//									it.setIsFull("1");
//								}
//								
//								commonDAO.saveOrUpdate(it);
//								
//								InstrumentState is = new InstrumentState();
//								is.setId(null);
//								is.setInstrumentCode(it.getId());
//								is.setStageName("细胞生产");
//								is.setTableTypeId((String) job.get("rwdid"));
//								is.setNote((String) job.get("bzorderNum"));
//								is.setNote2((String) job.get("bzname"));
//								//查询第一步实验操作人  按实验id和步骤查询
//								
//								List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy((String) job.get("rwdid"),(String) job.get("bzorderNum"));
//								if(cp.size()>0){
//									CellProductionRecord cpr = cp.get(0);
//									if(cpr.getTempleOperator()!=null){
//										is.setAcceptUser(cpr.getTempleOperator().getName());
//									}else{
//										is.setAcceptUser("");
//									}
//								}else{
//									is.setAcceptUser("");
//								}
//								is.setStartDate(sdf.format(new Date()));
//								is.setEndDate("");
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									is.setInstrumentState("开始使用");
//								}else{
//									is.setInstrumentState("开始占用");
//								}
//								is.setInstrument(it);
//								commonDAO.saveOrUpdate(is);
//							}
//						}
//					}
//				}
			} else {

			}
		}
//		return zy;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Boolean startPrepare(String cosListJson, String roomId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Boolean zy = true;

		if (!"".equals(roomId) && roomId != null) {
			List<RoomManagement> list = new ArrayList<RoomManagement>();
			list = commonDAO.findByProperty(RoomManagement.class, "id", roomId);
			if (!list.isEmpty()) {
				RoomManagement rm = new RoomManagement();
				rm = list.get(0);
				if (rm != null) {
					if ("1".equals(rm.getIsFull())) {
						zy = false;
					} else {
//						rm.setIsFull("1");
//						commonDAO.saveOrUpdate(rm);
					}
				}
			}
		}
		JSONArray array = JSONArray.fromObject(cosListJson);
		Map<String, String> map = new HashMap<String, String>();
		if (array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
				String code = (String) job.get("code");
				if (code != null && !"".equals(code)) {
					if (map.get(code) != null && !"".equals(map.get(code)) && code.equals(map.get(code))) {

					} else {
						map.put(code, code);
						Instrument it = new Instrument();
						it = commonDAO.get(Instrument.class, code);
						if (it != null) {
							if ("1".equals(it.getIsFull())) {
								zy = false;
							} else {

							}
						}
					}
				}
			}
//			if(zy){
//				for (int i = 0; i < array.size(); i++) {
//					JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
//					String code = (String) job.get("code");
//					if (code != null && !"".equals(code)) {
//						Instrument it = new Instrument();
//						it = commonDAO.get(Instrument.class, code);
//						if(it!=null){
//							if("1".equals(it.getIsFull())){
//								zy = false;
//							}else{
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									
//								}else{
//									it.setIsFull("1");
//								}
//								
//								commonDAO.saveOrUpdate(it);
//								
//								InstrumentState is = new InstrumentState();
//								is.setId(null);
//								is.setInstrumentCode(it.getId());
//								is.setStageName("细胞生产");
//								is.setTableTypeId((String) job.get("rwdid"));
//								is.setNote((String) job.get("bzorderNum"));
//								is.setNote2((String) job.get("bzname"));
//								//查询第一步实验操作人  按实验id和步骤查询
//								
//								List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy((String) job.get("rwdid"),(String) job.get("bzorderNum"));
//								if(cp.size()>0){
//									CellProductionRecord cpr = cp.get(0);
//									if(cpr.getTempleOperator()!=null){
//										is.setAcceptUser(cpr.getTempleOperator().getName());
//									}else{
//										is.setAcceptUser("");
//									}
//								}else{
//									is.setAcceptUser("");
//								}
//								is.setStartDate(sdf.format(new Date()));
//								is.setEndDate("");
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									is.setInstrumentState("开始使用");
//								}else{
//									is.setInstrumentState("开始占用");
//								}
//								is.setInstrument(it);
//								commonDAO.saveOrUpdate(is);
//							}
//						}
//					}
//				}
//			}else{
//				
//			}
		}
		return zy;
	}

	/**
	 * 保存复制的数据数据
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveshuju(TemplateItem ti, CellPassage cp, String stepNum) throws Exception {

		DateFormat df111 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");

		List<CellProductionRecord> cprs = templateService.getCellProductionRecords(ti.getId(), cp.getId());

		int num = cprs.size();

		// 解除占用
		CellPassage cellPassage = cellPassageDao.findNameByBach(cp.getId(), stepNum);
		String batch = cellPassage.getBatch();
		CellPassageTemplate ct = cellPassageDao.findCellPassageTemplateByStep(stepNum, cp.getId());
		CellProductionRecord cpr = cellPassageDao.findCellProductionRecordByStep(stepNum, cp.getId());
		List<CellProducOperation> cpoList = cellPassageDao.findCellProducOperationByStep(stepNum, cp.getId());
		// 解除房间占用
		List<RoomManagement> rmList = new ArrayList<RoomManagement>();
		rmList = commonDAO.findByProperty(RoomManagement.class, "roomName", cpr.getOperatingRoomName());
		if (!rmList.isEmpty()) {
			RoomManagement rm = new RoomManagement();
			rm = rmList.get(0);
			rm.setIsFull("0");
			commonDAO.saveOrUpdate(rm);

			RoomState rs = new RoomState();
			rs.setId(null);
			rs.setRoomCode(rm.getId());
			rs.setStageName("细胞生产");
			rs.setTableTypeId(cp.getId());
			rs.setNote(stepNum);
			rs.setNote2(cpr.getTempleItem().getName());

			rs.setBatch(batch);
			if (batch != null && !"".equals(batch)) {
				SampleOrder order = cellPassageDao.findNameByBach(batch);
				if (order != null) {
					if (!"".equals(order.getName()) && order.getName() != null) {
						rs.setName(order.getName());
					}
				}
			} else {
				rs.setName(null);
			}

			// 查询第一步实验操作人 按实验id和步骤查询

			List<CellProductionRecord> cp1 = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(cp.getId(),
					stepNum);
			if (cp1.size() > 0) {
				CellProductionRecord cprr = cp1.get(0);
				if (cprr.getTempleOperator() != null) {
					rs.setAcceptUser(cprr.getTempleOperator().getName());
				} else {

					rs.setAcceptUser("");
				}
			} else {

				rs.setAcceptUser("");
			}

			rs.setStartDate("");
			rs.setEndDate(df111.format(new Date()));
			rs.setRoomState("解除占用");
			rs.setRoomManagement(rm);
			commonDAO.saveOrUpdate(rs);

		}

		// 解除仪器
		List<CellproductCos> ccList = new ArrayList<CellproductCos>();
		if (!cpoList.isEmpty()) {
			for (CellProducOperation cpo : cpoList) {
				ccList = cellPassageDao.getCellproductCos(cpo.getId());
				if (!ccList.isEmpty()) {
					for (CellproductCos cc : ccList) {
						if (cc.getInstrumentCode() != null && !"".equals(cc.getInstrumentCode())) {
							Instrument it = new Instrument();
							it = commonDAO.get(Instrument.class, cc.getInstrumentCode());
							if (it != null) {

								if (it.getIsFull() != null && "1".equals(it.getIsFull())) {
									InstrumentState is = new InstrumentState();
									is.setId(null);
									is.setInstrumentCode(it.getId());
									is.setStageName("细胞生产");
									is.setTableTypeId(cp.getId());
									is.setNote(stepNum);
									is.setNote2(cpr.getTempleItem().getName());
									is.setBatch(batch);
									if (batch != null && !"".equals(batch)) {
										SampleOrder order = cellPassageDao.findNameByBach(batch);
										if (order != null) {
											if (!"".equals(order.getName()) && order.getName() != null) {
												is.setName(order.getName());
											}
										}
									} else {
										is.setName(null);
									}
									// 查询第一步实验操作人 按实验id和步骤查询

									List<CellProductionRecord> cp1 = cellPassageDao
											.getCellProductionRecordByIdAndStepNumForzy(cp.getId(), stepNum);
									if (cp1.size() > 0) {
										CellProductionRecord cprr = cp1.get(0);
										if (cprr.getTempleOperator() != null) {
											is.setAcceptUser(cprr.getTempleOperator().getName());
										} else {
											is.setAcceptUser("");
										}
									} else {
										is.setAcceptUser("");
									}
									is.setStartDate("");
									is.setEndDate(df111.format(new Date()));
									is.setInstrumentState("解除占用");
									is.setInstrument(it);
									commonDAO.saveOrUpdate(is);
								} else {
									if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
										InstrumentState is = new InstrumentState();
										is.setId(null);
										is.setInstrumentCode(it.getId());
										is.setStageName("细胞生产");
										is.setTableTypeId(cp.getId());
										is.setNote(stepNum);
										is.setNote2(cpr.getTempleItem().getName());
										is.setBatch(batch);
										if (batch != null && !"".equals(batch)) {
											SampleOrder order = cellPassageDao.findNameByBach(batch);
											if (order != null) {
												if (!"".equals(order.getName()) && order.getName() != null) {
													is.setName(order.getName());
												}
											}
										} else {
											is.setName(null);
										}
										// 查询第一步实验操作人 按实验id和步骤查询

										List<CellProductionRecord> cp1 = cellPassageDao
												.getCellProductionRecordByIdAndStepNumForzy(cp.getId(), stepNum);
										if (cp1.size() > 0) {
											CellProductionRecord cprr = cp1.get(0);
											if (cprr.getTempleOperator() != null) {
												is.setAcceptUser(cprr.getTempleOperator().getName());
											} else {
												is.setAcceptUser("");
											}
										} else {
											is.setAcceptUser("");
										}
										is.setStartDate("");
										is.setEndDate(df111.format(new Date()));
										is.setInstrumentState("结束使用");
										is.setInstrument(it);
										commonDAO.saveOrUpdate(is);
									}
								}

								it.setIsFull("0");
								commonDAO.saveOrUpdate(it);
							}
						}
					}
				}
			}
		}
		if (ct != null) {
			if (ct.getEndTime() != null && !"".equals(ct.getEndTime())) {

			} else {

				ct.setStepState("0");

				List<CellPassageItem> itemList = cellPassageDao.findCellPassageItemListByStep(stepNum, cp.getId(), "1");
				if (itemList != null && itemList.size() > 0) {
					for (CellPassageItem item : itemList) {
						CellPassageItem cloneItem = (CellPassageItem) item.clone();
						cloneItem.setId(null);
						cloneItem.setBlendState("0");
						cloneItem.setStepNum(String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
						commonDAO.saveOrUpdate(cloneItem);
					}
				} else {
					itemList = cellPassageDao.findCellPassageItemListByStep(stepNum, cp.getId(), "0");
					if (itemList != null && itemList.size() > 0) {
						for (CellPassageItem item1 : itemList) {
							CellPassageItem cloneItem1 = (CellPassageItem) item1.clone();
							cloneItem1.setId(null);
							cloneItem1.setBlendState("0");
							cloneItem1.setStepNum(String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
							commonDAO.saveOrUpdate(cloneItem1);
						}
					}
				}

				for (CellPassageItem item : itemList) {
					CellPassage cpp = commonDAO.get(CellPassage.class, cp.getId());
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					SampleOrder so = null;
					if (cpp.getBatch() != null && !"".equals(cpp.getBatch())) {
						List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", cpp.getBatch());
						if (sos.size() > 0) {
							so = sos.get(0);
						}
					}
					sampleStateService.saveSampleState1(cpp.getBatch(), cpp.getBatch(),
							so == null ? "" : so.getProductId(), so == null ? "" : so.getProductName(), "",
							ct.getPlanWorkDate(), ct.getEndTime(), "CellPassage", ct != null ? ct.getName() : "",
							(User) ServletActionContext.getRequest().getSession()
									.getAttribute(SystemConstants.USER_SESSION_KEY),
							cpp != null ? cpp.getId() : "", ct.getName(),
							item.getQualified() == "2" ? "合格" : (item.getQualified() == "1" ? "不合格" : "警戒"), null, null,
							null, null, null, null, null, null, null, null, null, null);
				}

				Date t = new Date();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				ct.setEndTime(df.format(t));

				/**
				 * 新加标记 标记步骤是重复步骤 为了打印任务单用
				 */
				ct.setChongfu("1");

				commonDAO.saveOrUpdate(ct);
				if (cpr != null) {
					cpr.setEndTime(df.format(t));
					commonDAO.saveOrUpdate(cpr);
				}
			}

		}

		CellPassageTemplate ptt = new CellPassageTemplate();
//		Integer result = 0;
		if (ti.getEstimatedTime() != null && !"".equals(ti.getEstimatedTime())) {

			int i = Integer.parseInt(ti.getEstimatedTime());
//			result = i - Integer.parseInt(t.getEstimatedTime());
			Date sj = new Date();
			String createDate = sdff.format(sj);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			Date parse = format.parse(createDate);
//			Calendar calendar = Calendar.getInstance();
//			calendar.add(calendar.DATE, result);
//			parse = calendar.getTime();
//			String date = format.format(parse);
			ptt.setPlanWorkDate(createDate);

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date parse1 = sdff.parse(createDate);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.add(calendar1.DATE, i);
			parse1 = calendar1.getTime();
			String date1 = sdff.format(parse1);
			ptt.setPlanEndDate(date1);
		}
		ptt.setStepState("2");
		ptt.setEstimatedDate(ti.getEstimatedTime());
		ptt.setCellPassage(cp);
		ptt.setOrderNum(ti.getOrderNum() + "." + String.valueOf(num));
		ptt.setName(ti.getName());
		cellPassageDao.saveOrUpdate(ptt);

		CellProductionRecord cellProduc = new CellProductionRecord();
		cellProduc.setEstimatedDate(ti.getEstimatedTime());
		cellProduc.setCellPassage(cp);
		cellProduc.setTempleItem(ti);
		cellProduc.setPlanWorkDate(ptt.getPlanWorkDate());
		cellProduc.setPlanEndDate(ptt.getPlanEndDate());
//		if (!"".equals(temList.get(n).getEstimatedTime())) {
//			Calendar calendar1 = Calendar.getInstance();
//			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
//			if (n == 0) {
//				String three_days_ago = sdf1.format(calendar1.getTime());
//				cellProduc.setPlanWorkDate(three_days_ago);//
//			} else {
//				calendar1.add(Calendar.DATE, amount);
//				String three_days_ago = sdf1.format(calendar1.getTime());
//				cellProduc.setPlanWorkDate(three_days_ago);//
//			}
//			amount += Integer.parseInt(temList.get(n).getEstimatedTime());
//		}
		cellProduc.setOrderNum(String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
		commonDAO.saveOrUpdate(cellProduc);
//		String planWorkDate = cellProduc.getPlanWorkDate();
//		if (n < temList.size() - 1) {
//			planTime += planWorkDate + ",";
//		} else {
//			planTime += planWorkDate;
//		}

		// 保存生产操作指令 --模板
		List<TempleNstructions> temNs = templateService
				.getTempleNstructionsListPageAndOrderNum(ti.getTemplate().getId(), String.valueOf(ti.getOrderNum()));
		if (temNs.size() > 0) {
			for (int m = 0; m < temNs.size(); m++) {
				CellPassagePreparat cellpa = new CellPassagePreparat();
				cellpa.setCellPassage(cp);
				cellpa.setOrderNum(String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
				cellpa.setState("0");
				cellpa.setProductionInspection(temNs.get(m).getTempleProduction());
				cellpa.setOperationNotes(temNs.get(m).getOperationNote());
				cellpa.setTempleInsId(temNs.get(m));
				commonDAO.saveOrUpdate(cellpa);
			}
		}

		// 保存完成清场操作指令 --模板
		List<TempleNstructionsEnd> temNsEnd = templateService
				.getTempleNstructionsEndListPageAndOrderNum(ti.getTemplate().getId(), String.valueOf(ti.getOrderNum()));
		if (temNsEnd.size() > 0) {
			for (int m = 0; m < temNsEnd.size(); m++) {
				CellPassagePreparatEnd cellpaEnd = new CellPassagePreparatEnd();
				cellpaEnd.setCellPassage(cp);
				cellpaEnd.setOrderNum(String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
				cellpaEnd.setState("0");
				cellpaEnd.setProductionInspection(temNsEnd.get(m).getTempleProduction());
				cellpaEnd.setOperationNotes(temNsEnd.get(m).getOperationNote());
				cellpaEnd.setTempleInsId(temNsEnd.get(m));
				commonDAO.saveOrUpdate(cellpaEnd);
			}
		}

		// 保存完工生产检查
		List<TempleFinished> templef = templateService.findByTempleIdFinishedInfoAndOrderNum(ti.getTemplate().getId(),
				String.valueOf(ti.getOrderNum()));
		if (templef.size() > 0) {
			for (int n = 0; n < templef.size(); n++) {
				CellProductionCompleted cellcomple = new CellProductionCompleted();
				cellcomple.setCellPassage(cp);
				cellcomple.setTempleItem(templef.get(n));
				cellcomple.setOrderNum(String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
				cellcomple.setState("0");
				commonDAO.saveOrUpdate(cellcomple);
			}
		}
		// 保存第三步模板
		if (cp.getTemplate() != null) {
			saveCellPassageTems(cp, String.valueOf(ti.getOrderNum()),
					String.valueOf(ti.getOrderNum()) + "." + String.valueOf(num));
		}

	}

	/**
	 * @throws ParseException
	 * @Title: saveCellPassageTemplate @Description: 保存新模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCellPassageTems(CellPassage sc, String orderNum, String orderNumAfter) throws ParseException {
		// 保存生产主表
		List<TempleProducingCell> temCell = templateService
				.findByIdTempleProducingCellAndOrderNum(sc.getTemplate().getId(), orderNum);
		if (temCell.size() > 0) {
			for (int i = 0; i < temCell.size(); i++) {
				CellProducOperation cp = new CellProducOperation();
				TempleProducingCell tem = commonDAO.get(TempleProducingCell.class, temCell.get(i).getId());
				if (tem != null) {
					cp.setTempleCell(tem);
					cp.setCellPassage(sc);
					cp.setOrderNum(orderNumAfter);
					commonDAO.saveOrUpdate(cp);
				}
				// 保存 生产 试剂
				List<TempleProducingReagent> regenLis = templateService
						.getTempleProducingReagentListPage(temCell.get(i).getId());
				if (regenLis.size() > 0) {
					for (int n = 0; n < regenLis.size(); n++) {
						CellproductReagent cellReagent = new CellproductReagent();
						TempleProducingReagent templereagent = commonDAO.get(TempleProducingReagent.class,
								regenLis.get(n).getId());
						if (templereagent != null) {
							cellReagent.setTemplateReagent(templereagent);
							cellReagent.setState("0");
							cellReagent.setCellProduc(cp);
							commonDAO.saveOrUpdate(cellReagent);
						}
					}
				}

				// 保存 生产 设备
				List<TempleProducingCos> cosLis = templateService.getTempleProducingCosListPage(temCell.get(i).getId());
				if (cosLis.size() > 0) {
					for (int m = 0; m < cosLis.size(); m++) {
						CellproductCos cellCos = new CellproductCos();
						TempleProducingCos templerCos = commonDAO.get(TempleProducingCos.class, cosLis.get(m).getId());
						if (templerCos != null) {
							cellCos.setTemplateCos(templerCos);
							cellCos.setState("0");
							cellCos.setCellProduc(cp);
							commonDAO.saveOrUpdate(cellCos);
						}
					}
				}
			}

			// 第三步生产 保存实验结果
			CellProducResults produ = new CellProducResults();
			produ.setState("0");
			produ.setCellPassage(sc);
			produ.setTempleCell(temCell.get(0));
			produ.setOrderNum(orderNumAfter);
			commonDAO.saveOrUpdate(produ);
		}
	}

	public List<CellProductionRecord> getCellProductionRecordByIdAndStepNum(String mainId, String stepNum) {
		return cellPassageDao.getCellProductionRecordByIdAndStepNum(mainId, stepNum);
	}

	/**
	 * 
	 * @Title: 查询基础信息 id @param code @return Map<String,Object> @throws
	 */
	public Map<String, Object> serchInfos(String mainId, String orderNums1) {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> map = new HashMap<String, Object>();
		List<CellProductionRecord> cprs = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(mainId, orderNums1);
		if (cprs.size() > 0) {
			CellProductionRecord cpr = cprs.get(0);
			// 还要做判断，这个步骤的上一步是否做完，上一步做完才能保存 第一步只考虑自己
			List<CellProductionRecord> cprrs = cellPassageDao.getCellProductionRecordByIdAndStepNumUnder(mainId,
					orderNums1);
			if (cpr.getEndTime() != null && !"".equals(cpr.getEndTime())) {
				map.put("baocun", false);
			} else {
				if ("1".equals(orderNums1)) {
					map.put("baocun", true);
				} else {
					if (cprrs.size() > 0) {
						CellProductionRecord cprr = cprrs.get(cprrs.size() - 1);
						if (cprr.getEndTime() != null && !"".equals(cprr.getEndTime())) {
							map.put("baocun", true);
						} else {
							map.put("baocun", false);
						}
					} else {
						map.put("baocun", true);
					}
				}
			}
			if (cpr.getTempleOperator() != null) {
				if ((u.getId()).equals(cpr.getTempleOperator().getId())) {
					map.put("caozuo", true);
				} else {
					map.put("caozuo", false);
				}
			} else {
				map.put("caozuo", false);
			}
		} else {
			map.put("baocun", false);
			map.put("caozuo", false);
		}

		List<UserGroupUser> ugus = cellPassageDao.getUserGroupUsers(u.getId());
		if (ugus.size() > 0) {
			map.put("adminUser", true);
		} else {
			map.put("adminUser", false);
		}
		return map;
	}

	public CellPassageTemplate getStepChongfu(String id, String orderNum) {
		return cellPassageDao.getStepChongfu(id, orderNum);
	}
	public void saveTransferWindowState(TransferWindowState transferWindowState) {
		commonDAO.saveOrUpdate(transferWindowState);
	}
	public void saveQualityTestSample(QualityTestTemp qualityTestTemp) {
		commonDAO.saveOrUpdate(qualityTestTemp);
	}
	public void saveQualityTestAbnormal(QualityTestAbnormal qualityTestTemp) {
		commonDAO.saveOrUpdate(qualityTestTemp);
	}

	public Map<String, Object> findCellPassageRecords(String mainId, String stepNum) {
		Map<String, Object> map = new HashMap<String, Object>();

		// 是否要入库
		boolean state = false;
		// 是否已入库
		boolean samplestate = false;
		List<CellProductionRecord> cellProductionRecords = cellPassageDao
				.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);

		if (cellProductionRecords.size() > 0) {
			CellProductionRecord cpr = cellProductionRecords.get(0);
			if (cpr.getTempleItem() != null) {
				if (cpr.getTempleItem().getIncubator() != null && "1".equals(cpr.getTempleItem().getIncubator())) {
					List<CellPassageItem> cpis = cellPassageDao.findCellPassageItemListByStepXia(stepNum, mainId);
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						if (cpi.getCounts() != null && !"".equals(cpi.getCounts()) && cpi.getPosId() != null
								&& !"".equals(cpi.getPosId())) {
							samplestate = true;
							state = true;
						} else {
							state = true;
						}
					} else {
						state = true;
					}
				} else {
					state = false;
				}
			} else {
				state = false;
			}
		} else {
			state = false;
		}

		map.put("state", state);
		map.put("samplestate", samplestate);

		return map;

	}

	public CellPassage selectDate(String id) {
		return cellPassageDao.selectDate(id);
	}

	public String getTemplate(String proId, String id) {
		String productId = proId;
		String[] pro = productId.split(",");
		Product sd = cellPassageDao.get(Product.class, pro[0]);
		if (id.equals("NEW")) {
			if (sd != null) {
				if (sd.getTemplate() != null) {
					return sd.getTemplate().getId();
				}
			}
		}
		return productId;
	}

	public CellProductionRecord finCellProductRecode(String orderNum, String cellPassageId) {
		CellProductionRecord cpr = cellPassageDao.findStepNumOrValue(orderNum, cellPassageId);
		return cpr;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savecpr(String orderNumBy, String id) {
//		if(!"".equals(cprId)&&cprId!=null) {
		CellProductionRecord cellProductionRecord = cellPassageDao.findStepNumOrValue(id, orderNumBy);

		if ("1".equals(orderNumBy)) {
			cellProductionRecord.setPossibleModify("1");
			commonDAO.saveOrUpdate(cellProductionRecord);

		} else {
			int parseInt = Integer.parseInt(orderNumBy);
			parseInt = parseInt - 1;
			String orderNum = String.valueOf(parseInt);
			CellProductionRecord finCellProductRecode = cellPassageDao.findStepNumOrValue(id, orderNum);
			if (finCellProductRecode != null) {
				String endTime = finCellProductRecode.getEndTime();
				if (!"".equals(endTime) && endTime != null) {
					cellProductionRecord.setPossibleModify("1");
					commonDAO.saveOrUpdate(cellProductionRecord);
				}
			}
		}
//		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savecprNew(String orderNumBy, String id, User user) {
//		if(!"".equals(cprId)&&cprId!=null) {
		CellProductionRecord cellProductionRecord = cellPassageDao.findStepNumOrValue(id, orderNumBy);

		if (cellProductionRecord != null) {

			if (cellProductionRecord.getTempleOperator() != null) {
				if (cellProductionRecord.getTempleOperator().getId().equals(user.getId())) {

					if ("1".equals(orderNumBy)) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						Boolean zy = true;
						//是否可修改状态不是1  说明可操作
//						if(!"1".equals(cellProductionRecord.getPossibleModify())) {
//							if(cellProductionRecord.getOperatingRoomId()!=null
//									&&!"".equals(cellProductionRecord.getOperatingRoomId())) {
//								String roomId = cellProductionRecord.getOperatingRoomId();
//								if (!"".equals(roomId) && roomId != null) {
//									List<RoomManagement> list = new ArrayList<RoomManagement>();
//									list = commonDAO.findByProperty(RoomManagement.class, "id", roomId);
//									if (!list.isEmpty()) {
//										RoomManagement rm = new RoomManagement();
//										rm = list.get(0);
//										if (rm != null) {
//											if ("1".equals(rm.getIsFull())) {
//												zy = false;
//											} else {
//
//												rm.setIsFull("1");
//												commonDAO.saveOrUpdate(rm);
//
//												RoomState rs = new RoomState();
//												rs.setId(null);
//												rs.setRoomCode(rm.getId());
//												rs.setStageName("细胞生产");
//												rs.setTableTypeId(cellProductionRecord.getCellPassage().getId());
//												rs.setNote(cellProductionRecord.getOrderNum());
//												rs.setNote2(cellProductionRecord.getTempleItem().getName());
//												rs.setBatch(cellProductionRecord.getCellPassage().getBatch());
//												if (cellProductionRecord.getCellPassage().getBatch() != null && !"".equals(cellProductionRecord.getCellPassage().getBatch())) {
//													SampleOrder order = cellPassageDao.findNameByBach(cellProductionRecord.getCellPassage().getBatch());
//													if (order != null) {
//														if (!"".equals(order.getName()) && order.getName() != null) {
//															rs.setName(order.getName());
//														}
//													}
//												} else {
//													rs.setName(null);
//												}
//												List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy(cellProductionRecord.getCellPassage().getId(),
//														cellProductionRecord.getOrderNum());
//												if (cp.size() > 0) {
//													CellProductionRecord cpr = cp.get(0);
//													if (cpr.getTempleOperator() != null) {
//
//														rs.setAcceptUser(cpr.getTempleOperator().getName());
//													} else {
//
//														rs.setAcceptUser("");
//													}
//												} else {
//
//													rs.setAcceptUser("");
//												}
//												rs.setStartDate(sdf.format(new Date()));
//												rs.setEndDate("");
//												rs.setRoomState("开始占用");
//												rs.setRoomManagement(rm);
//												commonDAO.saveOrUpdate(rs);
//											}
//
//										}
//									}
//									
//									List<CellPassageCos> cpcs = commonService.get(CellPassageCos.class, propertyName, value);
//
//									Instrument it = new Instrument();
//									it = commonDAO.get(Instrument.class, code);
//									if (it != null) {
//										if ("1".equals(it.getIsFull())) {
//											zy = false;
//										} else {
//											if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
//
//											} else {
//												it.setIsFull("1");
//											}
//
//											commonDAO.saveOrUpdate(it);
//
//											InstrumentState is = new InstrumentState();
//											is.setId(null);
//											is.setInstrumentCode(it.getId());
//											is.setStageName("细胞生产");
//											is.setTableTypeId(cellProductionRecord.getCellPassage().getId());
//											is.setNote(cellProductionRecord.getOrderNum());
//											is.setNote2(cellProductionRecord.getTempleItem().getName());
//											is.setBatch(cellProductionRecord.getCellPassage().getBatch());
//
//											if (cellProductionRecord.getCellPassage().getBatch() != null && !"".equals(cellProductionRecord.getCellPassage().getBatch())) {
//												SampleOrder order = cellPassageDao.findNameByBach(cellProductionRecord.getCellPassage().getBatch());
//												if (order != null) {
//													if (!"".equals(order.getName()) && order.getName() != null) {
//														is.setName(order.getName());
//
//													}
//												}
//											} else {
//												is.setName(null);
//
//											}
//
//											// 查询第一步实验操作人 按实验id和步骤查询
//
//											List<CellProductionRecord> cp = cellPassageDao
//													.getCellProductionRecordByIdAndStepNumForzy(cellProductionRecord.getCellPassage().getId(), cellProductionRecord.getOrderNum());
//											if (cp.size() > 0) {
//												CellProductionRecord cpr = cp.get(0);
//												if (cpr.getTempleOperator() != null) {
//													is.setAcceptUser(cpr.getTempleOperator().getName());
//
//												} else {
//													is.setAcceptUser("");
//
//												}
//											} else {
//												is.setAcceptUser("");
//
//											}
//											is.setStartDate(sdf.format(new Date()));
//											is.setEndDate("");
//
//											if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
//												is.setInstrumentState("开始使用");
//											} else {
//												is.setInstrumentState("开始占用");
//											}
//
//											is.setInstrument(it);
//											commonDAO.saveOrUpdate(is);
//
//										}
//									}
//								
//									
//									
//								}
//							}
//						}
						
						cellProductionRecord.setPossibleModify("1");
						commonDAO.saveOrUpdate(cellProductionRecord);

					} else {
						int parseInt = Integer.parseInt(orderNumBy);
						parseInt = parseInt - 1;
						String orderNum = String.valueOf(parseInt);
						CellProductionRecord finCellProductRecode = cellPassageDao.findStepNumOrValue(id, orderNum);
						if (finCellProductRecode != null) {
							String endTime = finCellProductRecode.getEndTime();
							if (!"".equals(endTime) && endTime != null) {
								//是否可修改状态不是1  说明可操作
								if(!"1".equals(cellProductionRecord.getPossibleModify())) {
									
								}
								
								cellProductionRecord.setPossibleModify("1");
								commonDAO.saveOrUpdate(cellProductionRecord);
							}
						}
					}

				}
			}
		}
	}

	public List<StorageOutItem> getfindStorageOutItems(Storage st, String pici) {
		return cellPassageDao.getfindStorageOutItems(st, pici);
	}

	public List<StorageReagentBuySerial> getfindStorageReagentBuySerials(Storage st, String pici) {
		return cellPassageDao.getfindStorageReagentBuySerials(st, pici);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCellPassageBeforeReagent(Storage st, String pici, StorageOutItem storageOutItem,
			CellPassageBeforeReagent cr, String cellpassgaeId, String ouderNum, String bs) {
		List<CellProducOperation> cellProducOperations = cellPassageDao.findCellProducOperation(cellpassgaeId,
				ouderNum);
		if (!cellProducOperations.isEmpty()) {
			for (CellProducOperation cellProducOperation : cellProducOperations) {
				List<CellproductReagent> findByIdProductRegentList = cellPassageDao
						.findByIdProductRegentList(cellProducOperation.getId());
				for (CellproductReagent cellproductReagents : findByIdProductRegentList) {
					if (cellproductReagents.getTemplateReagent().getCode().equals(cr.getReagentId())) {
						cellproductReagents.setBatch(cr.getReagentNo());
//											cellproductReagents.setCellPassSn(jsonRes.getString("sn"));
//											cellproductReagents.setCreateDate(user.getId());
//											cellproductReagents.setReagentDosage(jsonRes.getString("inputVal"));
						// 单位
						cellproductReagents.setUnit(cr.getReagentUnit());
						// 使用量
						cellproductReagents.setReagentDosage(cr.getAmount());
						cellproductReagents.setStorageItemId(cr.getStorageItemId());
						cellproductReagents.setReagenName(cr.getReagentName());
						cellproductReagents.setReagenCode(cr.getReagentId());

						commonService.saveOrUpdate(cellproductReagents);
					}
				}
			}
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		cr.setReagentNo(pici);
		cr.setReagentValidityTime(df.format(storageOutItem.getExpireDate()));
		cr.setStorageItemId(storageOutItem.getId());
		cellPassageDao.saveOrUpdate(cr);
		if ("1".equals(bs)) {
			storageOutItem.setBatch(cr.getCellPassage().getBatch());
			cellPassageDao.saveOrUpdate(storageOutItem);
		}
	}

	public List<StorageOutItem> getfindStorageOutItems1(Storage st, String batch) {
		return cellPassageDao.getfindStorageOutItems1(st, batch);
	}

}
