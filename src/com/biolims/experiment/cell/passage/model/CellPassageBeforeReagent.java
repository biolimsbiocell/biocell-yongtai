package com.biolims.experiment.cell.passage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.equipment.model.Instrument;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 生产观察
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CELL_PASSAGE_BEFORE_REAGENT")
@SuppressWarnings("serial")
public class CellPassageBeforeReagent extends EntityDao<CellPassageBeforeReagent> implements
		java.io.Serializable,Cloneable {

	/** 编码 */
	private String id;
	/** 相关主表 */
	private CellPassage cellPassage;
	/**实验的步骤号*/
	private String stepNum;
	
	/**名称id*/
	private String reagentId;
	/**名称*/
	private String reagentName;
	/**厂家/品牌*/
	private String reagentManufactor;
	/**规格*/
	private String reagentSpecifications;
	/**批号*/
	private String reagentNo;
	/**有效期*/
	private String reagentValidityTime;
	/**数量*/
	private String reagentNum;
	
	/**使用量*/
	private String amount;
	/**单位*/
	private String reagentUnit;
	
	/*出库记录ID*/
	private String storageItemId;
	
	
	

	public String getReagentId() {
		return reagentId;
	}

	public void setReagentId(String reagentId) {
		this.reagentId = reagentId;
	}

	public String getReagentName() {
		return reagentName;
	}

	public void setReagentName(String reagentName) {
		this.reagentName = reagentName;
	}

	public String getReagentManufactor() {
		return reagentManufactor;
	}

	public void setReagentManufactor(String reagentManufactor) {
		this.reagentManufactor = reagentManufactor;
	}

	public String getReagentSpecifications() {
		return reagentSpecifications;
	}

	public void setReagentSpecifications(String reagentSpecifications) {
		this.reagentSpecifications = reagentSpecifications;
	}

	public String getReagentNo() {
		return reagentNo;
	}

	public void setReagentNo(String reagentNo) {
		this.reagentNo = reagentNo;
	}

	public String getReagentValidityTime() {
		return reagentValidityTime;
	}

	public void setReagentValidityTime(String reagentValidityTime) {
		this.reagentValidityTime = reagentValidityTime;
	}

	public String getReagentNum() {
		return reagentNum;
	}

	public void setReagentNum(String reagentNum) {
		this.reagentNum = reagentNum;
	}

	public String getReagentUnit() {
		return reagentUnit;
	}

	public void setReagentUnit(String reagentUnit) {
		this.reagentUnit = reagentUnit;
	}

	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}


	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * 方法: 取得BloodSplit
	 * 
	 * @return: BloodSplit 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_PASSAGE")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getStorageItemId() {
		return storageItemId;
	}

	public void setStorageItemId(String storageItemId) {
		this.storageItemId = storageItemId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}