package com.biolims.experiment.cell.passage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.equipment.model.Instrument;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 细胞传代实验明细
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CELL_PASSAGE_ITEM_CHECK_CODE")
@SuppressWarnings("serial")
public class CellPassageItemCheckCode extends EntityDao<CellPassageItemCheckCode> implements
		java.io.Serializable,Cloneable {

	/** 编码 */
	private String id;
	/** 样本编号 */
	private String code;
	/**混合状态,根据这个值区分显示混合前和混合后的数据*/
	private String blendState;
	/** 相关主表 */
	private CellPassage cellPassage;
	/**实验的步骤号*/
	private String stepNum;
	/** 状态  */
	private String state;
	
	

	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	public String getBlendState() {
		return blendState;
	}

	public void setBlendState(String blendState) {
		this.blendState = blendState;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 36)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}


	/**
	 * 方法: 取得BloodSplit
	 * 
	 * @return: BloodSplit 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_PASSAGE")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}



	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}