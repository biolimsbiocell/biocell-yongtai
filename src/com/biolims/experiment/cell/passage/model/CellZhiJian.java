package com.biolims.experiment.cell.passage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;

@Entity
@Table(name = "cell_zhi_jian")
@SuppressWarnings("serial")
public class CellZhiJian  extends EntityDao<CellZhiJian> implements java.io.Serializable  {
	
	
	/** id */
	private String id;
	
	/**质检名称 */
	@Column(name="name")
	private String name;
	/**质检编号 */
	@Column(name="code")
	private String code;
	/**相关主表*/
	@Column(name="cell_produc")
	private CellProducOperation cellProduc;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;
	
	
	/**样本量*/
	private String sampleNum;
	/**单位*/
	private String sampleNumUnit;
	/**样本名称*/
	private String sampleType;
	/** 实验对应的质检模板*/
	private ZhiJianItem item;
	
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}

	

	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_produc")
	public CellProducOperation getCellProduc() {
		return cellProduc;
	}

	public void setCellProduc(CellProducOperation cellProduc) {
		this.cellProduc = cellProduc;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public ZhiJianItem getItem() {
		return item;
	}

	public void setItem(ZhiJianItem item) {
		this.item = item;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleNumUnit() {
		return sampleNumUnit;
	}

	public void setSampleNumUnit(String sampleNumUnit) {
		this.sampleNumUnit = sampleNumUnit;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	
	
}
