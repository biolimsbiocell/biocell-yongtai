package com.biolims.experiment.cell.passage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.system.template.model.TemplateItem;
/**   
 * @Title: Model
 * @Description: 实验第一步生产检查明细表     
 * @author lims-platform
 * @date 2019-03-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "Cell_production_record")
@SuppressWarnings("serial")
public class CellProductionRecord extends EntityDao<CellProductionRecord> implements java.io.Serializable {
	/** 设备id */
	private String id;
	/** 操作间名称 */
	@Column(name="operating_room_name")
	private String operatingRoomName;
	/** 操作间id */
	@Column(name="operating_room_id")
	private String operatingRoomId;
	/** 第一步结束时间*/
	@Column(name="create_date")
	private String createDate;
	/** 创建人--实验员 */
	@Column(name="create_date")
	private String createUser;
	/** 步骤号 */
	@Column(name="order_num")
	private String orderNum;
	/** 环境温度*/
	@Column(name="ambient_temperature")
	private String ambientTemperature;
	/** 环境湿度） */
	@Column(name="ambient_humidity")
	private String ambientHumidity;
	/** 操作人 */
	@Column(name="temple_operator")
	private User templeOperator;
	/** 审核人 */
	@Column(name="temple_reviewer")
	private User templeReviewer;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;
	/** 对应的模板id*/
	private TemplateItem templeItem;
	/** 自定义字段结果*/
	private String content;
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	/** 备注*/
	@Column(name="note")
	private String note;
	
	
	//新加字段
	/**预计用时*/
	@Column(name="estimated_date")
	private String estimatedDate;
	/**预计结束日期*/
	@Column(name="plan_end_date")
	private String planEndDate;
	/**预计开始日期*/
	@Column(name="plan_work_date")
	private String planWorkDate;
	/**延时*/
	@Column(name="delay")
	private String delay;
	/**当前生产大步骤的    结束时间 */
	@Column(name="end_time")
	private String endTime;
	/** 相关主表 */
	private CellPassage cellPassage;
	
	/** QA复核人 */
	@Column(name="qa_temple_reviewer")
	private User qaTempleReviewer;
	
	/** 二氧化碳培养箱编号 */
	private String counts;
	/** 位置 */
	private String posId;
	
	
	/**观察日期*/
	private Date observationDate;
	/**观察结果*/
	private String observationResult;
	/**是否操作*/
	private String operationState;
	/**观察人*/
	private User observationUser;
	/**细胞计数 1：*/
	private String cellNum;
	/**细胞计数 2：*/
	private String cellNumber;
	/**活细胞密度*/
	private String cellDensity;
	/**观察备注*/
	private String observationNote;
	
	/**显微镜编号*/
	private String microscopeNo;
	/**生产计划点击操作之后是否可以修改*/
	private String possibleModify ;
	/**显微镜编号*/
	private String microscopeId;
	/**显微镜名称*/
	private String microscopeName;
	/**显微镜状态*/
	private String microscopeStateName;
	private String sampleId;
	private String sampleCheck;
	private String incubator;
	private String incubatorState;
	
	
	
	
	
	
	public String getIncubator() {
		return incubator;
	}
	public void setIncubator(String incubator) {
		this.incubator = incubator;
	}
	public String getIncubatorState() {
		return incubatorState;
	}
	public void setIncubatorState(String incubatorState) {
		this.incubatorState = incubatorState;
	}
	public String getSampleCheck() {
		return sampleCheck;
	}
	public void setSampleCheck(String sampleCheck) {
		this.sampleCheck = sampleCheck;
	}
	public String getSampleId() {
		return sampleId;
	}
	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}
	public String getMicroscopeId() {
		return microscopeId;
	}
	public void setMicroscopeId(String microscopeId) {
		this.microscopeId = microscopeId;
	}
	public String getMicroscopeName() {
		return microscopeName;
	}
	public void setMicroscopeName(String microscopeName) {
		this.microscopeName = microscopeName;
	}
	public String getMicroscopeStateName() {
		return microscopeStateName;
	}
	public void setMicroscopeStateName(String microscopeStateName) {
		this.microscopeStateName = microscopeStateName;
	}
	public String getMicroscopeNo() {
		return microscopeNo;
	}
	public void setMicroscopeNo(String microscopeNo) {
		this.microscopeNo = microscopeNo;
	}
	public Date getObservationDate() {
		return observationDate;
	}
	public void setObservationDate(Date observationDate) {
		this.observationDate = observationDate;
	}
	public String getObservationResult() {
		return observationResult;
	}
	public void setObservationResult(String observationResult) {
		this.observationResult = observationResult;
	}
	public String getOperationState() {
		return operationState;
	}
	public void setOperationState(String operationState) {
		this.operationState = operationState;
	}
	public String getCellNum() {
		return cellNum;
	}
	public void setCellNum(String cellNum) {
		this.cellNum = cellNum;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getCellDensity() {
		return cellDensity;
	}
	public void setCellDensity(String cellDensity) {
		this.cellDensity = cellDensity;
	}
	public String getObservationNote() {
		return observationNote;
	}
	public void setObservationNote(String observationNote) {
		this.observationNote = observationNote;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OBSERVATION_USER")
	public User getObservationUser() {
		return observationUser;
	}

	public void setObservationUser(User observationUser) {
		this.observationUser = observationUser;
	}
	
	
	public String getOperatingRoomId() {
		return operatingRoomId;
	}
	public void setOperatingRoomId(String operatingRoomId) {
		this.operatingRoomId = operatingRoomId;
	}
	@Transient
	public String getCounts() {
		return counts;
	}
	
	public void setCounts(String counts) {
		this.counts = counts;
	}
	
	@Transient
	public String getPosId() {
		return posId;
	}
	
	public void setPosId(String posId) {
		this.posId = posId;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "qa_temple_reviewer")
	public User getQaTempleReviewer() {
		return qaTempleReviewer;
	}
	public void setQaTempleReviewer(User qaTempleReviewer) {
		this.qaTempleReviewer = qaTempleReviewer;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_reviewer")
	public User getTempleReviewer() {
		return templeReviewer;
	}
	public void setTempleReviewer(User templeReviewer) {
		this.templeReviewer = templeReviewer;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_operator")
	public User getTempleOperator() {
		return templeOperator;
	}
	public void setTempleOperator(User templeOperator) {
		this.templeOperator = templeOperator;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_passage")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getCreateDate() {
		return createDate;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_item")
	public TemplateItem getTempleItem() {
		return templeItem;
	}
	public void setTempleItem(TemplateItem templeItem) {
		this.templeItem = templeItem;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}
	public String getOperatingRoomName() {
		return operatingRoomName;
	}
	public void setOperatingRoomName(String operatingRoomName) {
		this.operatingRoomName = operatingRoomName;
	}
	public String getAmbientTemperature() {
		return ambientTemperature;
	}
	public void setAmbientTemperature(String ambientTemperature) {
		this.ambientTemperature = ambientTemperature;
	}
	public String getAmbientHumidity() {
		return ambientHumidity;
	}
	public void setAmbientHumidity(String ambientHumidity) {
		this.ambientHumidity = ambientHumidity;
	}
//	public String getTempleOperator() {
//		return templeOperator;
//	}
//	public void setTempleOperator(String templeOperator) {
//		this.templeOperator = templeOperator;
//	}
//	public String getTempleReviewer() {
//		return templeReviewer;
//	}
//	public void setTempleReviewer(String templeReviewer) {
//		this.templeReviewer = templeReviewer;
//	}
	public String getEstimatedDate() {
		return estimatedDate;
	}
	public void setEstimatedDate(String estimatedDate) {
		this.estimatedDate = estimatedDate;
	}
	public String getPlanEndDate() {
		return planEndDate;
	}
	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}
	public String getPlanWorkDate() {
		return planWorkDate;
	}
	public void setPlanWorkDate(String planWorkDate) {
		this.planWorkDate = planWorkDate;
	}
	public String getDelay() {
		return delay;
	}
	public void setDelay(String delay) {
		this.delay = delay;
	}
	public String getPossibleModify() {
		return possibleModify;
	}
	public void setPossibleModify(String possibleModify) {
		this.possibleModify = possibleModify;
	}
	
}