package com.biolims.experiment.cell.passage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 细胞传代实验提交质检明细
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CELL_PASSAGE_QUALITY_ITEM")
@SuppressWarnings("serial")
public class CellPassageQualityItem extends EntityDao<CellPassageQualityItem> implements
		java.io.Serializable,Cloneable {

	/** 编码 */
	private String id;
	/** 提交表编码 */
	private String tempId;
	/** 样本编号 */
	private String code;
	/** 批次号 */
	private String batch;
	/** 订单编号 */
	private String orderCode;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 状态  */
	private String state;
	/** 备注 */
	private String note;
	/** 浓度 */
	private Double concentration;
	/** 样本数量 */
	private Double sampleNum;
	/** 相关主表 */
	private CellPassage cellPassage;
	/** 体积 */
	private Double volume;
	/** 任务单id */
	private String orderId;
	/** 中间产物数量 */
	private String productNum;
	/** 中间产物类型编号 */
	private String dicSampleTypeId;
	/** 中间产物类型 */
	private String dicSampleTypeName;
	/** 样本类型 */
	private String sampleType;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	/**实验的步骤号*/
	private String stepNum;
	/**实验步骤名称*/
	private String experimentalStepsName;
	/**检测项*/
	private SampleDeteyion sampleDeteyion;
	/**关联订单*/
	private SampleOrder sampleOrder;
	/**是否合格*/
	private String qualified;
	/**质检结果表id*/
	private String qualityInfoId;
	/**质检是否接收*/
	private String qualityReceive;
	/**质检提交时间*/
	private Date qualitySubmitTime;
	/**质检接收时间*/
	private Date qualityReceiveTime;
	/**质检完成时间*/
	private Date qualityFinishTime;
	/**质检提交人*/
	private String qualitySubmitUser;
	/**质检接收人*/
	private String qualityReceiveUser;
	/**质检是否提交*/
	private String submit;
	/** 检测方式 */
	private String cellType;
	
	/** 质检量单位 */
	private String sampleNumUnit;
	
	//样本编号New
	private String SampleNumber;
	//质检条码号
	private String productCode;
	
	
	
	
	
	public String getSampleNumber() {
		return SampleNumber;
	}

	public void setSampleNumber(String sampleNumber) {
		SampleNumber = sampleNumber;
	}

	public String getExperimentalStepsName() {
		return experimentalStepsName;
	}

	public void setExperimentalStepsName(String experimentalStepsName) {
		this.experimentalStepsName = experimentalStepsName;
	}

	public String getSampleNumUnit() {
		return sampleNumUnit;
	}

	public void setSampleNumUnit(String sampleNumUnit) {
		this.sampleNumUnit = sampleNumUnit;
	}

	public String getCellType() {
		return cellType;
	}

	public void setCellType(String cellType) {
		this.cellType = cellType;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getQualitySubmitUser() {
		return qualitySubmitUser;
	}

	public void setQualitySubmitUser(String qualitySubmitUser) {
		this.qualitySubmitUser = qualitySubmitUser;
	}

	public String getQualityReceiveUser() {
		return qualityReceiveUser;
	}

	public void setQualityReceiveUser(String qualityReceiveUser) {
		this.qualityReceiveUser = qualityReceiveUser;
	}

	public Date getQualitySubmitTime() {
		return qualitySubmitTime;
	}

	public void setQualitySubmitTime(Date qualitySubmitTime) {
		this.qualitySubmitTime = qualitySubmitTime;
	}

	public Date getQualityReceiveTime() {
		return qualityReceiveTime;
	}

	public void setQualityReceiveTime(Date qualityReceiveTime) {
		this.qualityReceiveTime = qualityReceiveTime;
	}

	public Date getQualityFinishTime() {
		return qualityFinishTime;
	}

	public void setQualityFinishTime(Date qualityFinishTime) {
		this.qualityFinishTime = qualityFinishTime;
	}

	public String getQualityReceive() {
		return qualityReceive;
	}

	public void setQualityReceive(String qualityReceive) {
		this.qualityReceive = qualityReceive;
	}

	public String getQualityInfoId() {
		return qualityInfoId;
	}

	public void setQualityInfoId(String qualityInfoId) {
		this.qualityInfoId = qualityInfoId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETECYION")
	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}

	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}
	
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}


	public String getQualified() {
		return qualified;
	}

	public void setQualified(String qualified) {
		this.qualified = qualified;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	
	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}


	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 36)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得Sting
	 * 
	 * @return: Sting 备注
	 */
	@Column(name = "NOTE", length = 36)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得BloodSplit
	 * 
	 * @return: BloodSplit 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_PASSAGE")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}


	@Column(name = "CONCENTRATION", length = 100)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	public String getDicSampleTypeId() {
		return dicSampleTypeId;
	}

	public void setDicSampleTypeId(String dicSampleTypeId) {
		this.dicSampleTypeId = dicSampleTypeId;
	}

	public String getDicSampleTypeName() {
		return dicSampleTypeName;
	}

	public void setDicSampleTypeName(String dicSampleTypeName) {
		this.dicSampleTypeName = dicSampleTypeName;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
}