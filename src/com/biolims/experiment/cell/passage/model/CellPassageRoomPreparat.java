package com.biolims.experiment.cell.passage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TempleNstructions;
/**   
 * @Title: Model
 * @Description: 实验工前准备房间准备
 * @author lims-platform
 * @date 2015-11-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "Cell_passage_room_preparat")
@SuppressWarnings("serial")
public class CellPassageRoomPreparat extends EntityDao<CellPassageRoomPreparat> implements java.io.Serializable {
	/** id */
	private String id;
	/** 内容  */
	@Column(name="name")
	private String name;
	/** 步骤号  */
	@Column(name="order_num")
	private String orderNum;
	/** 序号  */
	@Column(name="order_type_num")
	private String orderTypeNum;
	/** 状态  */
	@Column(name="state")
	private String state;
	/** 相关主表  */
	private CellPassage cellPassage;
	
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrderTypeNum() {
		return orderTypeNum;
	}
	public void setOrderTypeNum(String orderTypeNum) {
		this.orderTypeNum = orderTypeNum;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_passage")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}

}