package com.biolims.experiment.cell.passage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleFinished;
/**   
 * @Title: Model
 * @Description: 实验第四步生产检查明细表   完工清除
 * @author lims-platform
 * @date 2019-03-21 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "Cell_production_completed")
@SuppressWarnings("serial")
public class CellProductionCompleted extends EntityDao<CellProductionCompleted> implements java.io.Serializable {
	/** id */
	private String id;
	/** 第最后一步结束时间*/
	@Column(name="create_date")
	private String createDate;
	/** 创建人--实验员 */
	@Column(name="create_date")
	private String createUser;
	/** 步骤号 */
	@Column(name="order_num")
	private String orderNum;
	/** 操作人 */
	@Column(name="temple_operator")
	private String templeOperator;
	/** QA复核人 */
	@Column(name="temple_reviewer")
	private String templeReviewer;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;
	/** 对应的模板id*/
	private TempleFinished templeItem;
	/** 相关主表 */
	private CellPassage cellPassage;
	/** 自定义字段结果*/
	@Column(name="content")
	private String content;
	
	/** 消毒剂*/
	@Column(name="disinfectant")
	private String disinfectant;
	/** 紫外灯时间*/
	@Column(name="ultravioletlamptime")
	private String ultravioletlamptime;
	

	public String getDisinfectant() {
		return disinfectant;
	}
	public void setDisinfectant(String disinfectant) {
		this.disinfectant = disinfectant;
	}
	public String getUltravioletlamptime() {
		return ultravioletlamptime;
	}
	public void setUltravioletlamptime(String ultravioletlamptime) {
		this.ultravioletlamptime = ultravioletlamptime;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_passage")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getCreateDate() {
		return createDate;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_item")
	public TempleFinished getTempleItem() {
		return templeItem;
	}
	public void setTempleItem(TempleFinished templeItem) {
		this.templeItem = templeItem;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}
	public String getTempleOperator() {
		return templeOperator;
	}
	public void setTempleOperator(String templeOperator) {
		this.templeOperator = templeOperator;
	}
	public String getTempleReviewer() {
		return templeReviewer;
	}
	public void setTempleReviewer(String templeReviewer) {
		this.templeReviewer = templeReviewer;
	}
}