package com.biolims.experiment.cell.passage.model;

import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.system.detecyion.model.SampleDeteyion;

/**   
 * @Title: Model
 * @Description: 模版明细
 * @author lims-platform
 * @date 2015-11-18 17:00:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CELL_PASSAGE_TEMPLATE")
@SuppressWarnings("serial")
public class CellPassageTemplate extends EntityDao<CellPassageTemplate> implements java.io.Serializable {
	
	/**步骤id*/
	private String id;
	/**步骤编号*/
	private String code;
	/**排序号*/
	private String orderNum;
	/**步骤名称*/
	private String name;
	/**备注*/
	private String note;
	/**自定义字段*/
	private String content;
	/**自定义字段值*/
	private String contentData;
	/**关联样本*/
	private String sampleCodes;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**相关主表*/
	private CellPassage cellPassage;
	/**检测项*/
	private SampleDeteyion sampleDeteyion;
	/**预计用时*/
	private String estimatedDate;
	/**实验员*/
	private String testUserList;
	/**预计结束日期*/
	private String planEndDate;
	/**质检结果*/
	private String zjResult;
	/**预计开始日期*/
	private String planWorkDate;
	/**延时*/
	private String delay;
	/**是否在当前步骤混合样本*/
	private String blend;
	
	/** 当前步骤是否显示 显示null或1  不显示0 */
	private String state;
	/** 生产步骤开始时间 */
	private String productStartTime;
	/** 生产步骤结束时间 */
	private String productEndTime;
	/** 生产备注 */
	private String notes;
	
	
	/** 标记  当前步骤是否是重复的步骤1重复  其他不重复 */
	private String chongfu;
	
	/** 当前步骤状态0已完成1不合格2正在进行3未做 */
	private String stepState;
	
	private String step1State;
	private String step2State;
	private String step3State;
	private String step4State;
	
	@Column(name ="step1_state", length = 255)
	public String getStep1State() {
		return step1State;
	}
	public void setStep1State(String step1State) {
		this.step1State = step1State;
	}
	
	@Column(name ="step2_state", length = 255)
	public String getStep2State() {
		return step2State;
	}
	public void setStep2State(String step2State) {
		this.step2State = step2State;
	}
	
	@Column(name ="step3_state", length = 255)
	public String getStep3State() {
		return step3State;
	}
	public void setStep3State(String step3State) {
		this.step3State = step3State;
	}
	
	@Column(name ="step4_state", length = 255)
	public String getStep4State() {
		return step4State;
	}
	public void setStep4State(String step4State) {
		this.step4State = step4State;
	}
	public String getStepState() {
		return stepState;
	}
	public void setStepState(String stepState) {
		this.stepState = stepState;
	}
	public String getChongfu() {
		return chongfu;
	}
	public void setChongfu(String chongfu) {
		this.chongfu = chongfu;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getProductStartTime() {
		return productStartTime;
	}
	public void setProductStartTime(String productStartTime) {
		this.productStartTime = productStartTime;
	}
	public String getProductEndTime() {
		return productEndTime;
	}
	public void setProductEndTime(String productEndTime) {
		this.productEndTime = productEndTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getBlend() {
		return blend;
	}
	public void setBlend(String blend) {
		this.blend = blend;
	}
	public String getDelay() {
		return delay;
	}
	public void setDelay(String delay) {
		this.delay = delay;
	}
	public String getPlanWorkDate() {
		return planWorkDate;
	}
	public void setPlanWorkDate(String planWorkDate) {
		this.planWorkDate = planWorkDate;
	}
	
	
	
	public String getZjResult() {
		return zjResult;
	}
	public void setZjResult(String zjResult) {
		this.zjResult = zjResult;
	}
	public String getPlanEndDate() {
		return planEndDate;
	}
	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}
	public String getEstimatedDate() {
		return estimatedDate;
	}
	public void setEstimatedDate(String estimatedDate) {
		this.estimatedDate = estimatedDate;
	}
	public String getTestUserList() {
		return testUserList;
	}
	public void setTestUserList(String testUserList) {
		this.testUserList = testUserList;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETECYION")
	public SampleDeteyion getSampleDeteyion() {
		return sampleDeteyion;
	}
	public void setSampleDeteyion(SampleDeteyion sampleDeteyion) {
		this.sampleDeteyion = sampleDeteyion;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_PASSAGE")
	public CellPassage getCellPassage(){
		return this.cellPassage;
	}
	/**
	 *方法: 设置BloodSplit
	 *@param: BloodSplit  相关主表
	 */
	public void setCellPassage(CellPassage cellPassage){
		this.cellPassage = cellPassage;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE")
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得Template
	 *@return: Template  关联主表
	 */
//	public SampleCommonTask getTask() {
//		return task;
//	}
//	public void setTask(SampleCommonTask task) {
//		this.task = task;
//	}
	
	@Column(name ="SAMPLE_CODES", length = 5000)
	public String getSampleCodes() {
		return sampleCodes;
	}
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
	@Column(name="START_TIME",length=50)
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@Column(name="END_TIME",length=50)
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * @return the contentData
	 */
	public String getContentData() {
		return contentData;
	}
	/**
	 * @param contentData the contentData to set
	 */
	public void setContentData(String contentData) {
		this.contentData = contentData;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the orderNum
	 */
	public String getOrderNum() {
		return orderNum;
	}
	/**
	 * @param orderNum the orderNum to set
	 */
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	
	

}