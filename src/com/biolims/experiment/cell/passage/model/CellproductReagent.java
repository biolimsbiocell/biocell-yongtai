package com.biolims.experiment.cell.passage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingReagent;
/**   
 * @Title: Model
 * @Description: 生产第三步     生产试剂明细
 * @author lims-platform
 * @date 2015-11-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "cell_produc_reagent")
@SuppressWarnings("serial")
public class CellproductReagent extends EntityDao<CellproductReagent> implements java.io.Serializable {
	/** id */
	private String id;
	/** 创建人--实验员 */
	@Column(name="create_user")
	private String createUser;
	/** 创建时间 */
	@Column(name="create_date")
	private String createDate;
	/** 批次 */
	@Column(name="batch")
	private String batch;
	/** cellSn  */
	@Column(name="cell_pass_sn")
	private String cellPassSn;
	/**试剂名称 */
	@Column(name="reagen_name")
	private String reagenName;
	/**试剂code */
	@Column(name="reagen_code")
	private String reagenCode;
	/** 试剂用量ml*/
	@Column(name="reagent_dosage")
	private String reagentDosage;
	/**相关主表*/
	@Column(name="cell_produc")
	private CellProducOperation cellProduc;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;
	/**单位*/
	private String unit;
	/*出库记录ID*/
	private String storageItemId;
	/** 实验对应的试剂模板*/
	private TempleProducingReagent templateReagent;
	
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getReagenName() {
		return reagenName;
	}
	public void setReagenName(String reagenName) {
		this.reagenName = reagenName;
	}
	public String getReagenCode() {
		return reagenCode;
	}
	public void setReagenCode(String reagenCode) {
		this.reagenCode = reagenCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getCellPassSn() {
		return cellPassSn;
	}

	public void setCellPassSn(String cellPassSn) {
		this.cellPassSn = cellPassSn;
	}

	public String getReagentDosage() {
		return reagentDosage;
	}

	public void setReagentDosage(String reagentDosage) {
		this.reagentDosage = reagentDosage;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_produc")
	public CellProducOperation getCellProduc() {
		return cellProduc;
	}

	public void setCellProduc(CellProducOperation cellProduc) {
		this.cellProduc = cellProduc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "template_reagent")
	public TempleProducingReagent getTemplateReagent() {
		return templateReagent;
	}

	public void setTemplateReagent(TempleProducingReagent templateReagent) {
		this.templateReagent = templateReagent;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getStorageItemId() {
		return storageItemId;
	}
	public void setStorageItemId(String storageItemId) {
		this.storageItemId = storageItemId;
	}
	
}