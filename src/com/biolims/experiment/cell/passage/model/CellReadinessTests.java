package com.biolims.experiment.cell.passage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 细胞传代实验  工前准备及检查
 * @author lims-platform
 * @date 2019-03-08 17:06:17
 * @version V1.0
 */
@Entity
@Table(name = "CELL_READINESS_TEST")
@SuppressWarnings("serial")
public class CellReadinessTests extends EntityDao<CellReadinessTests>implements java.io.Serializable {
	/**
	 * 方法: 取得String
	 * @return: String 开始指令id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	private String id;
	/** 指令名称 */
	@Column(name = "name", length = 50)
	private String name;
	/** 创建人 */
	@Column(name = "create_user", length = 50)
	private User createUser;
	/** 创建日期 */
	@Column(name = "create_date", length = 50)
	private Date createDate;
}
