package com.biolims.experiment.cell.passage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
/**   
 * @Title: Model
 * @Description: 生产第三步     生产设备明细
 * @author lims-platform
 * @date 2015-11-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "cell_produc_cos")
@SuppressWarnings("serial")
public class CellproductCos extends EntityDao<CellproductCos> implements java.io.Serializable {
	/** id */
	private String id;
	/** 创建人--实验员 */
	@Column(name="create_user")
	private String createUser;
	/** 创建时间 */
	@Column(name="create_date")
	private String createDate;
	/** 设备名称 */
	@Column(name="instrument_name")
	private String instrumentName;
	/** 设备code*/
	@Column(name="instrument_code")
	private String instrumentCode;
	/** 设备id  */
	@Column(name="instrument_id")
	private String instrumentId;
	/**相关主表*/
	@Column(name="cell_produc")
	private CellProducOperation cellProduc;

	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;
	/** 实验对应的试剂模板*/
	private TempleProducingCos templateCos;
	
	/** 核对状态 0未核对    1已核对*/
	@Column(name="operation_state")
	private String operationState;
	
	
	
	public String getOperationState() {
		return operationState;
	}
	public void setOperationState(String operationState) {
		this.operationState = operationState;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getInstrumentCode() {
		return instrumentCode;
	}
	public void setInstrumentCode(String instrumentCode) {
		this.instrumentCode = instrumentCode;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_produc")
	public CellProducOperation getCellProduc() {
		return cellProduc;
	}

	public void setCellProduc(CellProducOperation cellProduc) {
		this.cellProduc = cellProduc;
	}
	public String getInstrumentId() {
		return instrumentId;
	}
	public void setInstrumentId(String instrumentId) {
		this.instrumentId = instrumentId;
	}
	public String getInstrumentName() {
		return instrumentName;
	}
	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "template_cos")
	public TempleProducingCos getTemplateCos() {
		return templateCos;
	}
	public void setTemplateCos(TempleProducingCos templateCos) {
		this.templateCos = templateCos;
	}
}