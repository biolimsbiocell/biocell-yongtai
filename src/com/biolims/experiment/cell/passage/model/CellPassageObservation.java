package com.biolims.experiment.cell.passage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.equipment.model.Instrument;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
/**
 * @Title: Model
 * @Description: 生产观察
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CELL_PASSAGE_OBSERVATION")
@SuppressWarnings("serial")
public class CellPassageObservation extends EntityDao<CellPassageObservation> implements
		java.io.Serializable,Cloneable {

	/** 编码 */
	private String id;
	/** 相关主表 */
	private CellPassage cellPassage;
	/**实验的步骤号*/
	private String stepNum;
	/**观察日期*/
	private Date observationDate;
	/**观察结果*/
	private String observationResult;
	/**是否操作*/
	private String operationState;
	/**观察人*/
	private User observationUser;
	/**备注*/
	private String note;
	
	/**细胞计数 1：*/
	private String cellNum;
	/**细胞计数 2：*/
	private String cellNumber;
	/**活细胞密度*/
	private String cellDensity;
	
	/**显微镜编号*/
	private String microscopeNo;
	
	
	
	public String getMicroscopeNo() {
		return microscopeNo;
	}

	public void setMicroscopeNo(String microscopeNo) {
		this.microscopeNo = microscopeNo;
	}

	public String getCellNum() {
		return cellNum;
	}

	public void setCellNum(String cellNum) {
		this.cellNum = cellNum;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String getCellDensity() {
		return cellDensity;
	}

	public void setCellDensity(String cellDensity) {
		this.cellDensity = cellDensity;
	}

	public Date getObservationDate() {
		return observationDate;
	}

	public void setObservationDate(Date observationDate) {
		this.observationDate = observationDate;
	}

	public String getObservationResult() {
		return observationResult;
	}

	public void setObservationResult(String observationResult) {
		this.observationResult = observationResult;
	}

	public String getOperationState() {
		return operationState;
	}

	public void setOperationState(String operationState) {
		this.operationState = operationState;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OBSERVATION_USER")
	public User getObservationUser() {
		return observationUser;
	}

	public void setObservationUser(User observationUser) {
		this.observationUser = observationUser;
	}

	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}


	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得Sting
	 * 
	 * @return: Sting 备注
	 */
	@Column(name = "NOTE", length = 36)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得BloodSplit
	 * 
	 * @return: BloodSplit 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_PASSAGE")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}

	/**
	 * 方法: 设置BloodSplit
	 * 
	 * @param: BloodSplit 相关主表
	 */
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}