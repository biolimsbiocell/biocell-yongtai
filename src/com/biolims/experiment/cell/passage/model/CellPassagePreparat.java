package com.biolims.experiment.cell.passage.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TempleNstructions;
/**   
 * @Title: Model
 * @Description: 实验工前准备及检查明细表
 * @author lims-platform
 * @date 2015-11-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "Cell_passage_preparat")
@SuppressWarnings("serial")
public class CellPassagePreparat extends EntityDao<CellPassagePreparat> implements java.io.Serializable {
	/** 设备id */
	private String id;
	/** 设备名称 */
	@Column(name="name")
	private String name;
	/** 创建时间*/
	@Column(name="create_date")
	private String createDate;
	/** 创建人 */
	@Column(name="create_date")
	private String createUser;
	/** 步骤号 */
	@Column(name="order_num")
	private String orderNum;
	/** 关联的指令id*/
	private TempleNstructions templeInsId;
	/** 生产检查（0否       1是） */
	@Column(name="production_inspection")
	private String productionInspection;
	/** 操作备注 */
	@Column(name="operation_notes")
	private String operationNotes;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;

	/** 相关主表 */
	private CellPassage cellPassage;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "cell_passage")
	public CellPassage getCellPassage() {
		return this.cellPassage;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getName() {
		return name;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_ins_id")
	public TempleNstructions getTempleInsId() {
		return templeInsId;
	}
	public void setTempleInsId(TempleNstructions templeInsId) {
		this.templeInsId = templeInsId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(CellPassage cellPassage) {
		this.cellPassage = cellPassage;
	}
	public String getProductionInspection() {
		return productionInspection;
	}
	public void setProductionInspection(String productionInspection) {
		this.productionInspection = productionInspection;
	}
	public String getOperationNotes() {
		return operationNotes;
	}
	public void setOperationNotes(String operationNotes) {
		this.operationNotes = operationNotes;
	}

}