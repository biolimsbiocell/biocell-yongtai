package com.biolims.experiment.cell.cytokine.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionItem;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionInfo;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class CytokineProductionManageDao extends BaseHibernateDao {
	public Map<String, Object> showCytokineProductionManageJson(String cellType, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CytokineProductionItem where 1=1 ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if(cellType!=null) {
			key += " and cellType = '"+cellType+"'";
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and cytokineProductionTask.scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from CytokineProductionItem  where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<CytokineProductionInfo> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}