package com.biolims.experiment.cell.cytokine.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.cytokine.model.CytokineProduction;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionCos;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionInfo;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionItem;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionReagent;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionTemp;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionTemplate;
import com.biolims.experiment.cell.cytokine.model.CytokineProductionZhiJian;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionZhiJian;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class CytokineProductionDao extends BaseHibernateDao {

	public Map<String, Object> findCytokineProductionTable(String cellType, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CytokineProduction where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(cellType!=null) {
			key += " and cellType = '"+cellType+"'";
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CytokineProduction where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CytokineProduction> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectCytokineProductionTempTable(String cellType, String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from CytokineProductionTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(cellType!=null&&!"".equals(cellType)) {
				key += " and cellType = '"+cellType+"'";
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<CytokineProductionTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from CytokineProductionTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findCytokineProductionItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CytokineProductionItem where 1=1 and state='1' and cytokineProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CytokineProductionItem  where 1=1 and state='1' and cytokineProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CytokineProductionItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<CytokineProductionItem> showWellPlate(String id) {
		String hql="from CytokineProductionItem  where 1=1 and state='2' and  cytokineProduction.id='"+id+"'";
		List<CytokineProductionItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findCytokineProductionItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CytokineProductionItem where 1=1 and state='2' and cytokineProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CytokineProductionItem  where 1=1 and state='2' and cytokineProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CytokineProductionItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from CytokineProductionItem where 1=1 and state='2' and cytokineProduction.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from CytokineProductionItem where 1=1 and state='2' and cytokineProduction.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<CytokineProductionTemplate> showCytokineProductionStepsJson(String id,String code) {

		String countHql = "select count(*) from CytokineProductionTemplate where 1=1 and cytokineProduction.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<CytokineProductionTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CytokineProductionTemplate where 1=1 and cytokineProduction.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CytokineProductionReagent> showCytokineProductionReagentJson(String id,String code) {
		String countHql = "select count(*) from CytokineProductionReagent where 1=1 and cytokineProduction.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<CytokineProductionReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CytokineProductionReagent where 1=1 and cytokineProduction.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CytokineProductionCos> showCytokineProductionCosJson(String id,String code) {
		String countHql = "select count(*) from CytokineProductionCos where 1=1 and cytokineProduction.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<CytokineProductionCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CytokineProductionCos where 1=1 and cytokineProduction.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CytokineProductionTemplate> delTemplateItem(String id) {
		List<CytokineProductionTemplate> list=new ArrayList<CytokineProductionTemplate>();
		String hql = "from CytokineProductionTemplate where 1=1 and cytokineProduction.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<CytokineProductionReagent> delReagentItem(String id) {
		List<CytokineProductionReagent> list=new ArrayList<CytokineProductionReagent>();
		String hql = "from CytokineProductionReagent where 1=1 and cytokineProduction.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<CytokineProductionCos> delCosItem(String id) {
		List<CytokineProductionCos> list=new ArrayList<CytokineProductionCos>();
		String hql = "from CytokineProductionCos where 1=1 and cytokineProduction.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showCytokineProductionResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CytokineProductionInfo where 1=1 and cytokineProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CytokineProductionInfo  where 1=1 and cytokineProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CytokineProductionInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from CytokineProductionItem where 1=1 and cytokineProduction.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<CytokineProductionItem> plateSample(String id, String counts) {
		String hql="from CytokineProductionItem where 1=1 and cytokineProduction.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<CytokineProductionItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CytokineProductionItem where 1=1 and cytokineProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CytokineProductionItem  where 1=1 and cytokineProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CytokineProductionItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<CytokineProductionInfo> findCytokineProductionInfoByCode(String code) {
		String hql="from CytokineProductionInfo where 1=1 and code='"+code+"'";
		List<CytokineProductionInfo> list=new ArrayList<CytokineProductionInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CytokineProductionInfo> selectAllResultListById(String code) {
		String hql = "from CytokineProductionInfo  where (submit is null or submit='') and cytokineProduction.id='"
				+ code + "'";
		List<CytokineProductionInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CytokineProductionInfo> selectResultListById(String id) {
		String hql = "from CytokineProductionInfo  where cytokineProduction.id='"
				+ id + "'";
		List<CytokineProductionInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CytokineProductionInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from CytokineProductionInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<CytokineProductionInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<CytokineProductionItem> selectCytokineProductionItemList(String scId)
			throws Exception {
		String hql = "from CytokineProductionItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and cytokineProduction.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CytokineProductionItem> list = new ArrayList<CytokineProductionItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from CytokineProductionItem where 1=1 and cytokineProduction.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public CytokineProductionInfo getResultByCode(String code) {
			String hql=" from CytokineProductionInfo where 1=1 and code='"+code+"'";
			CytokineProductionInfo spi=(CytokineProductionInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}

		/**
		 * @throws Exception   
		 * @Title: findCellPrimaryCultureTemplateList  
		 * @Description: TODO  
		 * @author : nan.jiang
		 * @date 2018-8-30上午11:28:37
		 * @param start
		 * @param length
		 * @param query
		 * @param col
		 * @param sort
		 * @param ids
		 * @return
		 * Map<String,Object>
		 * @throws  
		 */ 
		public Map<String, Object> findCellPrimaryCultureTemplateList(
				Integer start, Integer length, String query, String col,
				String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();			
			String countHql = "select count(*) from CytokineProductionTemplate where 1=1 and cytokineProduction.id='"+id+"'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = " from CytokineProductionTemplate where 1=1 and cytokineProduction.id='"+id+"'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by orderNum";
				}
				List<CytokineProductionTemplate> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		}

		public List<CytokineProductionZhiJian> delZhijianItem(String id) {
			List<CytokineProductionZhiJian> list=new ArrayList<CytokineProductionZhiJian>();
			String hql = "from CytokineProductionZhiJian where 1=1 and cytokineProduction.id='"+id+"'";
			String key="";
			list = getSession().createQuery(hql+key).list();
			return list;
		}

		public List<CytokineProductionZhiJian> showCytokineProductionZjianJson(String id, String code) {
			String countHql = "select count(*) from CytokineProductionZhiJian where 1=1 and cytokineProduction.id='"+id+"'";
			String key = "";
			if(code!=null&&!"".equals(code)){
				key+=" and itemId='"+code+"'";
			}else{
				key+=" and itemId='1'";
			}
			List<CytokineProductionZhiJian> list=null;
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				String hql = "from CytokineProductionZhiJian where 1=1 and cytokineProduction.id='"+id+"'";
				list = getSession().createQuery(hql+key).list();
				}
			return list;
		}

		public List<CytokineProduction> findCytokineList(){
			List<CytokineProduction> list = new ArrayList<CytokineProduction>();
			String hql = " from CytokineProduction where 1=1 and state <> '1'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<CytokineProductionTemplate> findTemplateList(String id) {
			List<CytokineProductionTemplate> list = new ArrayList<CytokineProductionTemplate>();
			String hql = " from CytokineProductionTemplate where 1=1 and cytokineProduction.id= '"+id+"'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<CytokineProduction> findCytokineListByUser(String name) {
			List<CytokineProduction> list = new ArrayList<CytokineProduction>();
			String hql = " from CytokineProduction where 1=1 and state <> '1' and workUser like '%"+name+"%'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}
		
	
}