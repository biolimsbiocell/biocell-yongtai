package com.biolims.experiment.cell.cytokine.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.cell.cytokine.service.CytokineProductionService;

public class CytokineProductionEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CytokineProductionService mbService = (CytokineProductionService) ctx
				.getBean("cytokineProductionService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
