﻿package com.biolims.experiment.cell.immunecell.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import com.biolims.abnormal.model.AbnormalItem;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.cell.immunecell.dao.ImmuneCellProductionAbnormalDao;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.model.LogInfo;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;
import com.biolims.sample.service.SampleInputService;

@Service
@Transactional
public class ImmuneCellProductionAbnormalService {
	@Resource
	private ImmuneCellProductionAbnormalDao immuneCellProductionAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;

	/**
	 * 
	 * @Title: showImmuneCellProductionAbnormalTableJson
	 * @Description: 展示异常样本列表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showImmuneCellProductionAbnormalTableJson(String cellType, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return immuneCellProductionAbnormalDao.showImmuneCellProductionAbnormalTableJson(cellType, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description: 保存并执行
	 * @author : 
	 * @date 
	 * @param ids
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson) throws Exception {
		List<ImmuneCellProductionAbnormal> saveItems1 = new ArrayList<ImmuneCellProductionAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		ImmuneCellProductionAbnormal scp = new ImmuneCellProductionAbnormal();
		for (Map<String, Object> map : list) {
			scp = (ImmuneCellProductionAbnormal) immuneCellProductionAbnormalDao.Map2Bean(map, scp);
			ImmuneCellProductionAbnormal sbi = commonDAO.get(ImmuneCellProductionAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}

		immuneCellProductionAbnormalDao.saveOrUpdateAll(saveItems1);
		List<ImmuneCellProductionAbnormal> saveItems = new ArrayList<ImmuneCellProductionAbnormal>();
		for (String id : ids) {
			ImmuneCellProductionAbnormal sbi = commonDAO.get(ImmuneCellProductionAbnormal.class, id);
			sbi.setIsExecute("1");
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi.getCode());
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {// 确认执行
					String next = sbi.getNextFlowId();
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setCode(sbi.getCode());
						st.setSampleCode(sbi.getSampleCode());
						st.setNum(sbi.getSampleNum());
						st.setState("1");
						immuneCellProductionAbnormalDao.saveOrUpdate(st);
						// 入库，改变SampleInfo中原始样本的状态为“待入库”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
						}
					} else if (next.equals("0012")) {// 暂停
						// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
						}
					} else if (next.equals("0013")) {// 终止
						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					} else if (next.equals("0014")) {// 反馈项目组
					} else {
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sbi.setState("1");
							sampleInputService.copy(o, sbi);
						}
					}
					// 改变状态不显示在异常样本中
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		immuneCellProductionAbnormalDao.saveOrUpdateAll(saveItems);
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String dataJson, String changeLog) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		ImmuneCellProductionAbnormal scp = new ImmuneCellProductionAbnormal();
		for (Map<String, Object> map : list) {
			scp = (ImmuneCellProductionAbnormal) immuneCellProductionAbnormalDao.Map2Bean(map, scp);
			ImmuneCellProductionAbnormal sbi = commonDAO.get(ImmuneCellProductionAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setResult(scp.getResult());
			sbi.setNote(scp.getNote());
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void feedbackAbnormal(String[] ids) {
		if(ids!=null&&ids.length>0){
			for(String id:ids){
				ImmuneCellProductionAbnormal sa=commonDAO.get(ImmuneCellProductionAbnormal.class, id);
				sa.setState("2");
				AbnormalItem ai=new AbnormalItem();
				ai.setDicSampleType(sa.getDicSampleType());
				ai.setMethod(sa.getMethod());
				ai.setNextFlow(sa.getNextFlow());
				ai.setNextFlowId(sa.getNextFlowId());
				ai.setNote(sa.getNote());
				ai.setOrderId(sa.getOrderId());
				ai.setProductId(sa.getProductId());
				ai.setProductName(sa.getProductName());
				ai.setProject(sa.getProject());
				ai.setSampleCode(sa.getSampleCode());
				ai.setState("1");
				commonDAO.saveOrUpdate(sa);
				commonDAO.saveOrUpdate(ai);
			}
		}
		
	}

}
