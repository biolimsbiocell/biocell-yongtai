package com.biolims.experiment.cell.immunecell.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.cell.immunecell.service.ImmuneCellProductionService;

public class ImmuneCellProductionEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		ImmuneCellProductionService mbService = (ImmuneCellProductionService) ctx
				.getBean("immuneCellProductionService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
