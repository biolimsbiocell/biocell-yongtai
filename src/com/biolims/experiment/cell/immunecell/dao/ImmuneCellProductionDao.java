package com.biolims.experiment.cell.immunecell.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.culture.model.CellPrimaryCultureTemplate;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProduction;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionCos;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionItem;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionReagent;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionTemp;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionTemplate;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionZhiJian;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class ImmuneCellProductionDao extends BaseHibernateDao {

	public Map<String, Object> findImmuneCellProductionTable(String cellType, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ImmuneCellProduction where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(cellType!=null) {
			key += " and cellType = '"+cellType+"'";
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ImmuneCellProduction where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ImmuneCellProduction> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectImmuneCellProductionTempTable(String cellType, String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from ImmuneCellProductionTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(cellType!=null&&!"".equals(cellType)) {
				key += " and cellType ='"+cellType+"'";
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<ImmuneCellProductionTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from ImmuneCellProductionTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findImmuneCellProductionItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ImmuneCellProductionItem where 1=1 and state='1' and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ImmuneCellProductionItem  where 1=1 and state='1' and immuneCellProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ImmuneCellProductionItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<ImmuneCellProductionItem> showWellPlate(String id) {
		String hql="from ImmuneCellProductionItem  where 1=1 and state='2' and  immuneCellProduction.id='"+id+"'";
		List<ImmuneCellProductionItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findImmuneCellProductionItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ImmuneCellProductionItem where 1=1 and state='2' and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ImmuneCellProductionItem  where 1=1 and state='2' and immuneCellProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ImmuneCellProductionItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from ImmuneCellProductionItem where 1=1 and state='2' and immuneCellProduction.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from ImmuneCellProductionItem where 1=1 and state='2' and immuneCellProduction.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<ImmuneCellProductionTemplate> showImmuneCellProductionStepsJson(String id,String code) {

		String countHql = "select count(*) from ImmuneCellProductionTemplate where 1=1 and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<ImmuneCellProductionTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ImmuneCellProductionTemplate where 1=1 and immuneCellProduction.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ImmuneCellProductionReagent> showImmuneCellProductionReagentJson(String id,String code) {
		String countHql = "select count(*) from ImmuneCellProductionReagent where 1=1 and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<ImmuneCellProductionReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ImmuneCellProductionReagent where 1=1 and immuneCellProduction.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ImmuneCellProductionCos> showImmuneCellProductionCosJson(String id,String code) {
		String countHql = "select count(*) from ImmuneCellProductionCos where 1=1 and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<ImmuneCellProductionCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ImmuneCellProductionCos where 1=1 and immuneCellProduction.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ImmuneCellProductionTemplate> delTemplateItem(String id) {
		List<ImmuneCellProductionTemplate> list=new ArrayList<ImmuneCellProductionTemplate>();
		String hql = "from ImmuneCellProductionTemplate where 1=1 and immuneCellProduction.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<ImmuneCellProductionReagent> delReagentItem(String id) {
		List<ImmuneCellProductionReagent> list=new ArrayList<ImmuneCellProductionReagent>();
		String hql = "from ImmuneCellProductionReagent where 1=1 and immuneCellProduction.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<ImmuneCellProductionCos> delCosItem(String id) {
		List<ImmuneCellProductionCos> list=new ArrayList<ImmuneCellProductionCos>();
		String hql = "from ImmuneCellProductionCos where 1=1 and immuneCellProduction.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showImmuneCellProductionResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ImmuneCellProductionInfo where 1=1 and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ImmuneCellProductionInfo  where 1=1 and immuneCellProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ImmuneCellProductionInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from ImmuneCellProductionItem where 1=1 and immuneCellProduction.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<ImmuneCellProductionItem> plateSample(String id, String counts) {
		String hql="from ImmuneCellProductionItem where 1=1 and immuneCellProduction.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<ImmuneCellProductionItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ImmuneCellProductionItem where 1=1 and immuneCellProduction.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ImmuneCellProductionItem  where 1=1 and immuneCellProduction.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ImmuneCellProductionItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<ImmuneCellProductionInfo> findImmuneCellProductionInfoByCode(String code) {
		String hql="from ImmuneCellProductionInfo where 1=1 and code='"+code+"'";
		List<ImmuneCellProductionInfo> list=new ArrayList<ImmuneCellProductionInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<ImmuneCellProductionInfo> selectAllResultListById(String code) {
		String hql = "from ImmuneCellProductionInfo  where (submit is null or submit='') and immuneCellProduction.id='"
				+ code + "'";
		List<ImmuneCellProductionInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<ImmuneCellProductionInfo> selectResultListById(String id) {
		String hql = "from ImmuneCellProductionInfo  where immuneCellProduction.id='"
				+ id + "'";
		List<ImmuneCellProductionInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ImmuneCellProductionInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from ImmuneCellProductionInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<ImmuneCellProductionInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<ImmuneCellProductionItem> selectImmuneCellProductionItemList(String scId)
			throws Exception {
		String hql = "from ImmuneCellProductionItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and immuneCellProduction.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ImmuneCellProductionItem> list = new ArrayList<ImmuneCellProductionItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from ImmuneCellProductionItem where 1=1 and immuneCellProduction.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public ImmuneCellProductionInfo getResultByCode(String code) {
			String hql=" from ImmuneCellProductionInfo where 1=1 and code='"+code+"'";
			ImmuneCellProductionInfo spi=(ImmuneCellProductionInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}

		/**
		 * @throws Exception   
		 * @Title: findCellPrimaryCultureTemplateList  
		 * @Description: TODO  
		 * @author : nan.jiang
		 * @date 2018-8-30上午10:45:01
		 * @param start
		 * @param length
		 * @param query
		 * @param col
		 * @param sort
		 * @param id
		 * @return
		 * Map<String,Object>
		 * @throws  
		 */ 
		public Map<String, Object> findCellPrimaryCultureTemplateList(
				Integer start, Integer length, String query, String col,
				String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();			
			String countHql = "select count(*) from ImmuneCellProductionTemplate where 1=1 and immuneCellProduction.id='"+id+"'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = " from ImmuneCellProductionTemplate where 1=1 and immuneCellProduction.id='"+id+"'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by orderNum";
				}
				List<ImmuneCellProductionTemplate> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		}

		public List<ImmuneCellProductionZhiJian> delZhijianItem(String id) {
			List<ImmuneCellProductionZhiJian> list=new ArrayList<ImmuneCellProductionZhiJian>();
			String hql = "from ImmuneCellProductionZhiJian where 1=1 and immuneCellProduction.id='"+id+"'";
			String key="";
			list = getSession().createQuery(hql+key).list();
			return list;
		}

		public List<ImmuneCellProductionZhiJian> showImmuneCellProductionZjianJson(String id, String code) {
			String countHql = "select count(*) from ImmuneCellProductionZhiJian where 1=1 and immuneCellProduction.id='"+id+"'";
			String key = "";
			if(code!=null&&!"".equals(code)){
				key+=" and itemId='"+code+"'";
			}else{
				key+=" and itemId='1'";
			}
			List<ImmuneCellProductionZhiJian> list=null;
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				String hql = "from ImmuneCellProductionZhiJian where 1=1 and immuneCellProduction.id='"+id+"'";
				list = getSession().createQuery(hql+key).list();
				}
			return list;
		}

		public List<ImmuneCellProduction> findImmuneCellList() {
			List<ImmuneCellProduction> list = new ArrayList<ImmuneCellProduction>();
			String hql = " from ImmuneCellProduction where 1=1 and state <> '1'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<ImmuneCellProductionTemplate> findTemplateList(String id) {
			List<ImmuneCellProductionTemplate> list = new ArrayList<ImmuneCellProductionTemplate>();
			String hql = " from ImmuneCellProductionTemplate where 1=1 and immuneCellProduction.id = '"+id+"'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<ImmuneCellProduction> findImmuneCellListByUser(String name) {
			List<ImmuneCellProduction> list = new ArrayList<ImmuneCellProduction>();
			String hql = " from ImmuneCellProduction where 1=1 and state <> '1' and workUser like '%"+name+"%'";
			list = this.getSession().createQuery(hql).list();
			return list;
		}
		
	
}