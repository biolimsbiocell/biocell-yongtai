﻿package com.biolims.experiment.cell.immunecell.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.cell.immunecell.model.ImmuneCellProductionAbnormal;
import com.biolims.experiment.cell.immunecell.service.ImmuneCellProductionAbnormalService;
import com.biolims.experiment.cell.immunecell.service.ImmuneCellProductionService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.common.PushData;
@Namespace("/experiment/cell/immunecell/immuneCellProductionAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ImmuneCellProductionAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";// 240102
	@Autowired
	private ImmuneCellProductionAbnormalService immuneCellProductionAbnormalService;
	@Resource
	private FileInfoService fileInfoService;
	@Autowired
	private ImmuneCellProductionService immuneCellProductionService;
	
	/**
	 * 
	 * @Title: showImmuneCellProductionAbnormalTable
	 * @Description: 展示异常样本列表
	 * @author 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showImmuneCellProductionAbnormalTable")
	public String showImmuneCellProductionAbnormalTable() throws Exception {
		String cellType = getParameterFromRequest("cellType");
		if(cellType.equals("1")) {//cik免疫细胞生产
			rightsId = "210030103";
		}else {//nk免疫细胞生产
			rightsId = "210030203";
		}
		putObjToContext("cellType", cellType);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/cell/immunecell/immuneCellProductionAbnormal.jsp");
	}

	@Action(value = "showImmuneCellProductionAbnormalTableJson")
	public void showImmuneCellProductionAbnormalTableJson() throws Exception {
		String cellType = getParameterFromRequest("cellType");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = immuneCellProductionAbnormalService
				.showImmuneCellProductionAbnormalTableJson(cellType, start, length, query, col, sort);
		List<ImmuneCellProductionAbnormal> list = (List<ImmuneCellProductionAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("isExecute", "");
		map.put("note", "");
		map.put("code", "");
		map.put("method", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("result", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("patient", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("reason", "");
//		map.put("concentration", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("orderId", "");
		map.put("classify", "");
		map.put("sampleNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
     /**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年5月3日下午2:13:02
	 * @throws Exception
	 *             void
	 * 修改纪录：dwb 增加日志功能 
	 * 2018-05-11 17:56:35
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String dataJson = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			immuneCellProductionAbnormalService.save(dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: feedbackAbnormal  
	 * @Description: 反馈至项目组  
	 * @author : shengwei.wang
	 * @date 2018年5月4日上午10:01:32
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value="feedbackAbnormal")
	public void feedbackAbnormal() throws Exception{
		String [] ids=getRequest().getParameterValues("ids[]");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			immuneCellProductionAbnormalService.feedbackAbnormal(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));	
	}
	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description:执行异常列表
	 * @author 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "executeAbnormal")
	public void executeAbnormal() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String dataJson = getParameterFromRequest("dataJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			immuneCellProductionAbnormalService.executeAbnormal(ids, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ImmuneCellProductionAbnormalService getImmuneCellProductionAbnormalService() {
		return immuneCellProductionAbnormalService;
	}

	public void setImmuneCellProductionAbnormalService(
			ImmuneCellProductionAbnormalService immuneCellProductionAbnormalService) {
		this.immuneCellProductionAbnormalService =immuneCellProductionAbnormalService;
	}

}
