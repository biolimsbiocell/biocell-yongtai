package com.biolims.experiment.cell.main.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 实验材料管理
 * @author lims-platform
 * @date 2015-08-26 11:53:53
 * @version V1.0
 * 
 */
@Entity
@Table(name = "MATERAILS_MAIN")
@SuppressWarnings("serial")
public class MaterailsMain extends EntityDao<MaterailsMain> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 描述 */
	private String name;
	/** 原始样本编号*/
	private String sampleCode;
	/** 样本编号*/
	private String code;
	/** 代次 */
	private String pronoun;
	/** 类型 */
	private String type;
	/** 父级 */
	private String parentId;
	/** 细胞类型 */
	private DicType cellType;
	/** 种属 */
	private DicType genus;
	/** 品系 */
	private DicType strain;
	/** 在库数量 */
	private Integer stockNum;
	/** 入库日期 */
	private Date inDate;
	/** 来源 */
	private String source;
	/** 状态 */
	private DicState dicState;
	/** 冻存液 */
	private String frozenLiquid;
	/** 保种编号 */
	private String saveId;
	/** 冻存位置 */
	private String frozenLocation;
	/** 孔颜色 */
	private String color;
	/** 组织部位 */
	private DicType tissue;
	/** 摘取人 */
	private User pickUser;
	/** 摘取日期 */
	private Date pickDate;
	/** 主数据名称 */
	private String materailsName;
	/** 细胞来源 */
	private String cellSource;
	/** 细胞培养方式 */
	private DicType cultureType;
	/** 胰酶浓度 */
	private DicType steapsinConcentration;
	/** 消化时间 */
	private String digestionDate;
	/** 吹打次数 */
	private String percussionNum;
	/** 特殊条件 */
	private DicType specialCondition;
	/** 冷冻液 */
	private DicType refrigerantLiquid;
	/** content2 */
	private String content2;
	/** content3 */
	private String content3;
	/** content4 */
	private String content4;
	/** content5 */
	private Double content5;
	/** content6 */
	private Double content6;
	/** content7 */
	private Double content7;
	/** content8 */
	private Double content8;
	/** content9 */
	private Date content9;
	/** content10 */
	private Date content10;
	/** content11 */
	private Date content11;
	/** content12 */
	private Date content12;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STEAPSIN_CONCENTRATION")
	@ForeignKey(name="null")
	public DicType getSteapsinConcentration() {
		return steapsinConcentration;
	}

	public void setSteapsinConcentration(DicType steapsinConcentration) {
		this.steapsinConcentration = steapsinConcentration;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SPECIAL_CONDITION")
	@ForeignKey(name="null")
	public DicType getSpecialCondition() {
		return specialCondition;
	}

	public void setSpecialCondition(DicType specialCondition) {
		this.specialCondition = specialCondition;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REFRIGERANT_LIQUID")
	@ForeignKey(name="null")
	public DicType getRefrigerantLiquid() {
		return refrigerantLiquid;
	}

	public void setRefrigerantLiquid(DicType refrigerantLiquid) {
		this.refrigerantLiquid = refrigerantLiquid;
	}

	@Column(name = "PERCUSSION_NUM", length = 36)
	public String getPercussionNum() {
		return percussionNum;
	}

	public void setPercussionNum(String percussionNum) {
		this.percussionNum = percussionNum;
	}

	@Column(name = "DIGESTION_DATE", length = 36)
	public String getDigestionDate() {
		return digestionDate;
	}

	public void setDigestionDate(String digestionDate) {
		this.digestionDate = digestionDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CULTURE_TYPE")
	@ForeignKey(name="null")
	public DicType getCultureType() {
		return cultureType;
	}

	public void setCultureType(DicType cultureType) {
		this.cultureType = cultureType;
	}

	@Column(name = "CELL_SOURCE", length = 36)
	public String getCellSource() {
		return cellSource;
	}

	public void setCellSource(String cellSource) {
		this.cellSource = cellSource;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 代次
	 */
	@Column(name = "PRONOUN", length = 20)
	public String getPronoun() {
		return pronoun;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 代次
	 */
	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 类型
	 */
	@Column(name = "TYPE", length = 36)
	public String getType() {
		return this.type;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 细胞类型
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CELL_TYPE")
	public DicType getCellType() {
		return cellType;
	}

	/**
	 * 细胞类型
	 * 
	 * @return
	 */
	public void setCellType(DicType cellType) {
		this.cellType = cellType;
	}

	/**
	 * 种属
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENUS")
	public DicType getGenus() {
		return genus;
	}

	/**
	 * 种属
	 * 
	 * @return
	 */
	public void setGenus(DicType genus) {
		this.genus = genus;
	}

	/**
	 * 品系
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STRAIN")
	public DicType getStrain() {
		return strain;
	}

	/**
	 * 品系
	 * 
	 * @return
	 */
	public void setStrain(DicType strain) {
		this.strain = strain;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 在库数量
	 */
	@Column(name = "STOCK_NUM", length = 40)
	public Integer getStockNum() {
		return this.stockNum;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer 在库数量
	 */
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 入库日期
	 */
	@Column(name = "IN_DATE", length = 255)
	public Date getInDate() {
		return this.inDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 入库日期
	 */
	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 存储位置
	 */
	// @Column(name = "LOCATION", length = 40)
	// public String getLocation() {
	// return this.location;
	// }
	//
	// /**
	// *方法: 设置String
	// *@param: String 存储位置
	// */
	// public void setLocation(String location) {
	// this.location = location;
	// }

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 来源
	 */
	@Column(name = "SOURCE", length = 40)
	public String getSource() {
		return this.source;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 来源
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * 方法: 取得DicState
	 * 
	 * @return: DicState 状态
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STATE")
	public DicState getDicState() {
		return this.dicState;
	}

	/**
	 * 方法: 设置DicState
	 * 
	 * @param: DicState 状态
	 */
	public void setDicState(DicState dicState) {
		this.dicState = dicState;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String materailsName
	 */
	@Column(name = "MATERAILS_NAME", length = 50)
	public String getMaterailsName() {
		return this.materailsName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String materailsName
	 */
	public void setMaterailsName(String materailsName) {
		this.materailsName = materailsName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 冻存位置
	 */
	@Column(name = "FROZEN_LOCATION", length = 36)
	public String getFrozenLocation() {
		return frozenLocation;
	}

	public void setFrozenLocation(String frozenLocation) {
		this.frozenLocation = frozenLocation;
	}

	@Column(name = "FROZEN_LIQUID", length = 40)
	public String getFrozenLiquid() {
		return frozenLiquid;
	}

	public void setFrozenLiquid(String frozenLiquid) {
		this.frozenLiquid = frozenLiquid;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 保种编号
	 */
	@Column(name = "SAVE_ID", length = 40)
	public String getSaveId() {
		return this.saveId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 保种编号
	 */
	public void setSaveId(String saveId) {
		this.saveId = saveId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 孔颜色
	 */
	@Column(name = "COLOR", length = 20)
	public String getColor() {
		return this.color;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 孔颜色
	 */
	public void setColor(String color) {
		this.color = color;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TISSUE")
	public DicType getTissue() {
		return tissue;
	}

	public void setTissue(DicType tissue) {
		this.tissue = tissue;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PICK_USER")
	public User getPickUser() {
		return pickUser;
	}

	public void setPickUser(User pickUser) {
		this.pickUser = pickUser;
	}

	@Column(name = "PICK_DATE", length = 20)
	public Date getPickDate() {
		return pickDate;
	}

	public void setPickDate(Date pickDate) {
		this.pickDate = pickDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content2
	 */
	@Column(name = "CONTENT2", length = 50)
	public String getContent2() {
		return this.content2;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content2
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content3
	 */
	@Column(name = "CONTENT3", length = 50)
	public String getContent3() {
		return this.content3;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content4
	 */
	@Column(name = "CONTENT4", length = 50)
	public String getContent4() {
		return this.content4;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content5
	 */
	@Column(name = "CONTENT5", length = 50)
	public Double getContent5() {
		return this.content5;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content5
	 */
	public void setContent5(Double content5) {
		this.content5 = content5;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content6
	 */
	@Column(name = "CONTENT6", length = 50)
	public Double getContent6() {
		return this.content6;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content6
	 */
	public void setContent6(Double content6) {
		this.content6 = content6;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content7
	 */
	@Column(name = "CONTENT7", length = 50)
	public Double getContent7() {
		return this.content7;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content7
	 */
	public void setContent7(Double content7) {
		this.content7 = content7;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content8
	 */
	@Column(name = "CONTENT8", length = 50)
	public Double getContent8() {
		return this.content8;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content8
	 */
	public void setContent8(Double content8) {
		this.content8 = content8;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content9
	 */
	@Column(name = "CONTENT9", length = 255)
	public Date getContent9() {
		return this.content9;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content9
	 */
	public void setContent9(Date content9) {
		this.content9 = content9;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content10
	 */
	@Column(name = "CONTENT10", length = 255)
	public Date getContent10() {
		return this.content10;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content10
	 */
	public void setContent10(Date content10) {
		this.content10 = content10;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content11
	 */
	@Column(name = "CONTENT11", length = 255)
	public Date getContent11() {
		return this.content11;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content11
	 */
	public void setContent11(Date content11) {
		this.content11 = content11;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content12
	 */
	@Column(name = "CONTENT12", length = 255)
	public Date getContent12() {
		return this.content12;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content12
	 */
	public void setContent12(Date content12) {
		this.content12 = content12;
	}

	/**
	 * @return the sampleCode
	 */
	public String getSampleCode() {
		return sampleCode;
	}

	/**
	 * @param sampleCode the sampleCode to set
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
}