package com.biolims.experiment.cell.main.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.experiment.cell.main.dao.MaterailsMainDao;
import com.biolims.experiment.cell.main.model.MaterailsMain;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.system.family.model.Family;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MaterailsMainService {
	@Resource
	private MaterailsMainDao materailsMainDao;
	@Resource
	private CodingRuleService codingRuleService;

	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findMaterailsMainList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return materailsMainDao.selectMaterailsMainList(start, length, query, col,
				sort);
	}

	// 按类型筛选主数据
	public Map<String, Object> findMaterailsMainListByType(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type,
			String projectId) {
		return materailsMainDao.selectMaterailsMainListByType(mapForQuery,
				startNum, limitNum, dir, sort, type, projectId);
	}

	// 按类型筛选主数据
	public Map<String, Object> findMaterailsMainListByP(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String projectId) {
		return materailsMainDao.selectMaterailsMainListByP(mapForQuery,
				startNum, limitNum, dir, sort, projectId);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MaterailsMain i) throws Exception {

		materailsMainDao.saveOrUpdate(i);

	}

	public MaterailsMain get(String id) {
		MaterailsMain materailsMain = commonDAO.get(MaterailsMain.class, id);
		return materailsMain;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MaterailsMain sc, Map jsonMap) throws Exception {
		if (sc != null) {
			materailsMainDao.saveOrUpdate(sc);

		}
	}


	// 根据主表ID和子表ID查询组织明细
	public List<Map<String, String>> showMainTissueListById(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		MaterailsMain srai = materailsMainDao.get(MaterailsMain.class, code);

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", srai.getId());
		map.put("name", srai.getName());
		map.put("type", srai.getType());
		if (srai.getPickUser() != null) {
			map.put("pickUserId", srai.getPickUser().getId());
			map.put("pickUserName", srai.getPickUser().getName());
		} else {
			map.put("pickUserId", "");
			map.put("pickUserName", "");
		}
		// map.put("typeName", srai.getType().getName());
		if (srai.getStockNum() != null)
			map.put("stockNum", srai.getStockNum().toString());
		else
			map.put("stockNum", "");
		if (srai.getTissue() != null) {
			map.put("tissueId", srai.getTissue().getId());
			map.put("tissueName", srai.getTissue().getName());
		} else {
			map.put("tissueId", "");
			map.put("tissueName", "");
		}
		// map.put("hostTypeName", srai.getHostType().getName());
		if (srai.getPickDate() != null)
			map.put("pickDate", srai.getPickDate().toString());
		else
			map.put("pickDate", "");
		mapList.add(map);
		return mapList;
	}

	public String showCellTree(String sampleCode,String code) {
		
		List<MaterailsMain> list= materailsMainDao.showCellTree(sampleCode);
		StringBuffer sb=new StringBuffer();
		if(list!=null){
			List<MaterailsMain> newList=new ArrayList<MaterailsMain>();
			for(MaterailsMain mm:list){
				if(sampleCode.equals(mm.getParentId())){
					newList.add(mm);
				}
			}
			if(newList.size()>0){
				sb.append("{\"name\":\""+sampleCode+"\",\"children\":[");
			
			for(int i=0;i<newList.size();i++){
				Integer b=new Integer(0);
				for(MaterailsMain mm2:list){
					if(newList.get(i).getCode().equals(mm2.getParentId())){
						b++;
					}
				}
				if(b>0){
					sb.append("{\"name\":\""+newList.get(i).getCode()+"\",\"children\":[");
					sb.append(getChildren(list,newList.get(i).getCode(), newList.get(i).getPronoun()));
					if(i<newList.size()-1){
						sb.append("]},");
					}else{
						sb.append("]}");
					}
				}else if(i<newList.size()-1){
					sb.append("{\"name\":\""+code+"\",\"value\":\""+newList.get(i).getPronoun()+"\"},");
				}else{
					sb.append("{\"name\":\""+code+"\",\"value\":\""+newList.get(i).getPronoun()+"\"}");
				}
			}
			sb.append("]}");
			
			}else{
				sb.append("{\"name\":\""+sampleCode+"\",\"value\":\"p0\"}");
			}
		}
		return sb.toString();
	}
	public String getChildren(List<MaterailsMain> list, String parentId,String pronoun){
		String jsonStr="";
		if(list!=null){
			List<MaterailsMain> newList=new ArrayList<MaterailsMain>();
			for(MaterailsMain mm:list){
				if(parentId.equals(mm.getParentId())){
					newList.add(mm);
				}
			}
			for(int i=0;i<newList.size();i++){
					Integer b=new Integer(0);
					for(MaterailsMain mm2:list){
						if(newList.get(i).getCode().equals(mm2.getParentId())){
							b++;
						}
					}
					if(b>0){
						jsonStr+="{\"name\":\""+newList.get(i).getCode()+"\",\"children\":[";
						jsonStr+=getChildren(list,newList.get(i).getCode(), newList.get(i).getPronoun());
						if(i<newList.size()-1){
							jsonStr+="]},";
						}else{
							jsonStr+="]}";
						}
					}else if(i<newList.size()-1){
						jsonStr+="{\"name\":\""+newList.get(i).getCode()+"\",\"value\":\""+pronoun+"\"},";
					}else{
						jsonStr+="{\"name\":\""+newList.get(i).getCode()+"\",\"value\":\""+pronoun+"\"}";
					}
			}
//			jsonStr+="]}";
		}
		return jsonStr;
	}

	public MaterailsMain findMaterailsMainByCode(String code) {
		return materailsMainDao.findMaterailsMainByCode(code);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String main,String ids, String changeLog) throws Exception {
		String id = null;
		if (main != null) {
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				if(u!=null) {
					li.setUserId(u.getId());
				}
				li.setFileId(ids);
				li.setModifyContent(changeLog);
				commonDAO.saveOrUpdate(li);
			}
			//主表数据
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			MaterailsMain sr = new MaterailsMain();
			sr = (MaterailsMain) commonDAO.Map2Bean(list.get(0), sr);
			if ((sr.getId() != null && "".equals(sr.getId()))
					|| sr.getId().equals("NEW")) {
				String modelName = "MaterailsMain";
				String markCode = "MM";
				String autoID = codingRuleService.genTransID(modelName,
						markCode);
				id = autoID;
				sr.setId(autoID);
				commonDAO.saveOrUpdate(sr);
			} else {
				id = sr.getId();
				commonDAO.saveOrUpdate(sr);
			}
	}
			return id;
	}
}