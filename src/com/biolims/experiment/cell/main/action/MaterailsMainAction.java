package com.biolims.experiment.cell.main.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.dic.service.DicTypeService;
import com.biolims.experiment.cell.main.dao.MaterailsMainDao;
import com.biolims.experiment.cell.main.model.MaterailsMain;
import com.biolims.experiment.cell.main.service.MaterailsMainService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/cell/materailsMain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MaterailsMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "cell";
	@Autowired
	private MaterailsMainService materailsMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private MaterailsMainDao materailsMainDao;
	private MaterailsMain materailsMain = new MaterailsMain();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private DicTypeService dicTypeService;
	@Resource
	private FieldService fieldService;

	/**
	 * 列表的展示
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showMaterailsMainList")
	public String showMaterailsMainList() throws Exception {
		rightsId="cell";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMain.jsp");
	}

	@Action(value = "showMaterailsMainListJson")
	public void showMaterailsMainListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = materailsMainService
					.findMaterailsMainList(start, length, query, col, sort);
			List<MaterailsMain> list = (List<MaterailsMain>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("cellType-id", "");
			map.put("cellType-name", "");
			map.put("cultureType-id", "");
			map.put("cultureType-name", "");
			map.put("pronoun", "");
			map.put("type", "");
			map.put("strain-id", "");
			map.put("strain-name", "");
			map.put("stockNum", "");
			map.put("inDate", "yyyy-MM-dd");
			map.put("source", "");
			map.put("dicState-id", "");
			map.put("dicState-name", "");
			map.put("materailsName", "");
			map.put("content2", "");
			map.put("content3", "");
			map.put("content4", "");
			map.put("content5", "");
			map.put("content6", "");
			map.put("content7", "");
			map.put("content8", "");
			map.put("content9", "");
			map.put("content10", "");
			map.put("content11", "");
			map.put("content12", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("MaterailsMain");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "materailsMainSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMaterailsMainList() throws Exception {
		String typeId = getRequest().getParameter("typeId");
		String projectId = getRequest().getParameter("projectId");
		putObjToContext("type", typeId);
		putObjToContext("projectId", projectId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainDialog.jsp");
	}

	@Action(value = "showDialogMaterailsMainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMaterailsMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		String type = getRequest().getParameter("type"); // 材料类型
		String projectId = getRequest().getParameter("projectId"); // 课题ID
		Map<String, Object> result = materailsMainService
				.findMaterailsMainListByType(map2Query, startNum, limitNum,
						dir, sort, type, projectId);
		Long count = (Long) result.get("total");
		List<MaterailsMain> list = (List<MaterailsMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type", "");
		map.put("pronoun", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("project-projectId", "");
		map.put("strain-id", "");
		map.put("strain-name", "");
		map.put("cellType-id", "");
		map.put("cellType-name", "");
		map.put("genus-id", "");
		map.put("genus-name", "");
		map.put("stockNum", "");
		map.put("inDate", "yyyy-MM-dd");
		map.put("source", "");
		map.put("dicState-id", "");
		map.put("dicState-name", "");
		map.put("materailsName", "");
		map.put("animalMain-geneType", "");
		map.put("frozenLiquid", "");
		map.put("saveId", "");
		map.put("frozenLocation", "");
		map.put("color", "");
		map.put("tissue-id", "");
		map.put("tissue-name", "");
		map.put("pickUser-id", "");
		map.put("pickUser-name", "");
		map.put("pickDate", "yyyy-MM-dd");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 编辑新建Action
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editMaterailsMain")
	public String editMaterailsMain() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			materailsMain = materailsMainService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "materailsMain");
		} else {
			materailsMain.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEdit.jsp");
	}
	
	
	@Action(value = "viewMasterailsMain")
	public String viewMasterailsMain() throws Exception {
		String id = getParameterFromRequest("id");
		materailsMain = materailsMainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEdit.jsp");
	}
	
	@Action(value = "editMaterailsMainByCode")
	public String editMaterailsMainByCode() throws Exception {
		String code = getParameterFromRequest("code");
		long num = 0;
			materailsMain = materailsMainService.findMaterailsMainByCode(code);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(materailsMain.getId(), "materailsMain");
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEdit.jsp");
	}
	@Action(value = "copyMaterailsMain")
	public String copyMaterailsMain() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		materailsMain = materailsMainService.get(id);
		materailsMain.setId("NEW");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {
		String ids = getParameterFromRequest("ids");
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogs=new String(changeLog.toString().getBytes("ISO-8859-1"), "UTF-8");
		String main=getParameterFromRequest("main");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
		String id=	materailsMainService.save(main,ids,changeLogs);
			map.put("id",id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewMaterailsMain")
	public String toViewMaterailsMain() throws Exception {
		String id = getParameterFromRequest("id");
		materailsMain = materailsMainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEdit.jsp");
	}




	@Action(value = "showMaterailsMainBACList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMaterailsMainBACList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainBAC.jsp");
	}


	@Action(value = "showMaterailsMainEmbryoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMaterailsMainEmbryoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEmbryo.jsp");
	}




	// 新改进的方法，将程序体系表中的内容 加载到实验页面的程序体系子表中
	@Action(value = "findDicType", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findDicType() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, String> result = new HashMap<String, String>();
		try {
			DicType dicType = dicTypeService.get(DicType.class, id);

			System.err.println(dicType.getId() + "----" + dicType.getNote());
			result.put("success", "true");
			result.put("id", dicType.getId());
			result.put("name", dicType.getName());
			result.put("note", dicType.getNote());
		} catch (Exception e) {
			result.put("success", "false");
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MaterailsMainService getMaterailsMainService() {
		return materailsMainService;
	}

	public void setMaterailsMainService(
			MaterailsMainService materailsMainService) {
		this.materailsMainService = materailsMainService;
	}

	public MaterailsMain getMaterailsMain() {
		return materailsMain;
	}

	public void setMaterailsMain(MaterailsMain materailsMain) {
		this.materailsMain = materailsMain;
	}

	// @Action(value = "showCellNum")
	// public String showCellNum() throws Exception {
	//
	// //int num =
	// Integer.parseInt(materailsMainDao.selectCellNum().toString());
	// String num1 = materailsMainDao.selectCellNum().toString();
	// return num1;
	// // putObjToContext("num1", num1);
	// // return
	// dispatcher("/WEB-INF/page/experiment/cell/main/materailsMainEdit.jsp");
	// }
	//
	// @Action(value = "showPlasmidNum")
	// public void showPlasmidNum(String id) throws Exception {
	// MaterailsMain m = materailsMainDao.get(MaterailsMain.class, id);
	// int num =
	// Integer.parseInt(materailsMainDao.selectPlasmidNum().toString());
	// m.setStockNum(num);
	// materailsMainDao.saveOrUpdate(m);
	// }
	// 活性检测调用
	@Action(value = "materailsMainSelectByP", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String materailsMainSelectByP() throws Exception {
		// String typeId = getRequest().getParameter("typeId");
		String projectId = getRequest().getParameter("projectId");
		// putObjToContext("type", typeId);
		putObjToContext("projectId", projectId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/cell/main/activeMaterailsMainDialog.jsp");
	}

	@Action(value = "showDialogMaterailsMainListJsonByP", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMaterailsMainListJsonByP() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		// String type = getRequest().getParameter("type"); //材料类型
		String projectId = getRequest().getParameter("projectId"); // 课题ID
		Map<String, Object> result = materailsMainService
				.findMaterailsMainListByP(map2Query, startNum, limitNum, dir,
						sort, projectId);
		Long count = (Long) result.get("total");
		List<MaterailsMain> list = (List<MaterailsMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("stockNum", "");
		map.put("inDate", "yyyy-MM-dd");
		map.put("source", "");
		map.put("dicState-id", "");
		map.put("dicState-name", "");
		map.put("materailsName", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	@Action(value="showCellTree")
	public void showCellTree() throws Exception{
		String sampleCode=getParameterFromRequest("sampleCode");
		String code=getParameterFromRequest("code");
		String data=materailsMainService.showCellTree(sampleCode,code);
		HttpUtils.write("["+data+"]");
	}
}
