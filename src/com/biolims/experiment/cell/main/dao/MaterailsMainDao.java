package com.biolims.experiment.cell.main.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.main.model.MaterailsMain;
import com.biolims.system.family.model.Family;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class MaterailsMainDao extends BaseHibernateDao {
	public Map<String, Object> selectMaterailsMainList(Integer start, Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MaterailsMain where 1=1";
		String key = " ";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from MaterailsMain where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<MaterailsMain> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	// 按类别、状态、课题筛选主数据
	public Map<String, Object> selectMaterailsMainListByType(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String type,
			String projectId) {
		String key = " ";
		String hql = " from MaterailsMain where type='" + type
				+ "' and  dicState='1cs' and stockNum>=0 ";
		if (projectId != null && !projectId.equals("")) {
			key = key + " and project.id = '" + projectId + "' ";
		}
		if (mapForQuery != null)
			key = key + map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MaterailsMain> list = new ArrayList<MaterailsMain>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 查询细胞在库数量
	public Long selectCellNum(String id) {
		String hql = "from MaterailsMainCell t where 1=1 and materailsMain.id='"
				+ id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select sum(t.num) " + hql).uniqueResult();
		return total;
	}

	// 查询质粒在库数量
	public Long selectPlasmidNum(String id) {
		String hql = "from MaterailsMainPlasmid t where 1=1 and materailsMain.id='"
				+ id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select sum(t.num) " + hql).uniqueResult();
		return total;
	}

	// 查询BAC在库数量
	public Long selectBacNum(String id) {
		String hql = "from MaterailsMainBAC t where 1=1 and materailsMain.id='"
				+ id + "'";
		Long total = (Long) this.getSession()
				.createQuery("select sum(t.num) " + hql).uniqueResult();
		return total;
	}

	// 活性检测页面调用按类别、状态、课题筛选主数据
	public Map<String, Object> selectMaterailsMainListByP(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String projectId) {
		String key = " ";
		String hql = " from MaterailsMain where type='1' and  dicState='1cs'";
		if (projectId != null && !projectId.equals("")) {
			key = key + " and project.id = '" + projectId + "' ";
		}
		if (mapForQuery != null)
			key = key + map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MaterailsMain> list = new ArrayList<MaterailsMain>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public List<MaterailsMain> showCellTree(String sampleCode) {
		String hql=" from MaterailsMain where 1=1 and sampleCode='"+sampleCode+"'";
		List<MaterailsMain> list=this.getSession().createQuery(hql).list();
		return list;
	}

	public MaterailsMain findMaterailsMainByCode(String code) {
		String hql=" from MaterailsMain where 1=1 and code='"+code+"'";
		List<MaterailsMain> list=new ArrayList<MaterailsMain>();
			list=	this.getSession().createQuery(hql).list();
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}
}