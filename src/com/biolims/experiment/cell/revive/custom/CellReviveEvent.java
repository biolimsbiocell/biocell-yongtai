package com.biolims.experiment.cell.revive.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.cell.revive.service.CellReviveService;

public class CellReviveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CellReviveService mbService = (CellReviveService) ctx
				.getBean("cellReviveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
