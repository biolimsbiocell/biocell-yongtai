package com.biolims.experiment.cell.revive.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.revive.model.CellRevive;
import com.biolims.experiment.cell.revive.model.CellReviveCos;
import com.biolims.experiment.cell.revive.model.CellReviveItem;
import com.biolims.experiment.cell.revive.model.CellReviveReagent;
import com.biolims.experiment.cell.revive.model.CellReviveTemp;
import com.biolims.experiment.cell.revive.model.CellReviveTemplate;
import com.biolims.experiment.cell.revive.model.CellReviveInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class CellReviveDao extends BaseHibernateDao {

	public Map<String, Object> findCellReviveTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellRevive where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellRevive where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellRevive> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectCellReviveTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from CellReviveTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<CellReviveTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from CellReviveTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findCellReviveItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellReviveItem where 1=1 and state='1' and cellRevive.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellReviveItem  where 1=1 and state='1' and cellRevive.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellReviveItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<CellReviveItem> showWellPlate(String id) {
		String hql="from CellReviveItem  where 1=1 and state='2' and  cellRevive.id='"+id+"'";
		List<CellReviveItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findCellReviveItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellReviveItem where 1=1 and state='2' and cellRevive.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellReviveItem  where 1=1 and state='2' and cellRevive.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellReviveItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from CellReviveItem where 1=1 and state='2' and cellRevive.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from CellReviveItem where 1=1 and state='2' and cellRevive.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<CellReviveTemplate> showCellReviveStepsJson(String id,String code) {

		String countHql = "select count(*) from CellReviveTemplate where 1=1 and cellRevive.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<CellReviveTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CellReviveTemplate where 1=1 and cellRevive.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CellReviveReagent> showCellReviveReagentJson(String id,String code) {
		String countHql = "select count(*) from CellReviveReagent where 1=1 and cellRevive.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<CellReviveReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CellReviveReagent where 1=1 and cellRevive.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CellReviveCos> showCellReviveCosJson(String id,String code) {
		String countHql = "select count(*) from CellReviveCos where 1=1 and cellRevive.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<CellReviveCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from CellReviveCos where 1=1 and cellRevive.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<CellReviveTemplate> delTemplateItem(String id) {
		List<CellReviveTemplate> list=new ArrayList<CellReviveTemplate>();
		String hql = "from CellReviveTemplate where 1=1 and cellRevive.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<CellReviveReagent> delReagentItem(String id) {
		List<CellReviveReagent> list=new ArrayList<CellReviveReagent>();
		String hql = "from CellReviveReagent where 1=1 and cellRevive.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<CellReviveCos> delCosItem(String id) {
		List<CellReviveCos> list=new ArrayList<CellReviveCos>();
		String hql = "from CellReviveCos where 1=1 and cellRevive.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showCellReviveResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellReviveInfo where 1=1 and cellRevive.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellReviveInfo  where 1=1 and cellRevive.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellReviveInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from CellReviveItem where 1=1 and cellRevive.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<CellReviveItem> plateSample(String id, String counts) {
		String hql="from CellReviveItem where 1=1 and cellRevive.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<CellReviveItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellReviveItem where 1=1 and cellRevive.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from CellReviveItem  where 1=1 and cellRevive.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<CellReviveItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<CellReviveInfo> findCellReviveInfoByCode(String code) {
		String hql="from CellReviveInfo where 1=1 and code='"+code+"'";
		List<CellReviveInfo> list=new ArrayList<CellReviveInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellReviveInfo> selectAllResultListById(String code) {
		String hql = "from CellReviveInfo  where (submit is null or submit='') and cellRevive.id='"
				+ code + "'";
		List<CellReviveInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellReviveInfo> selectResultListById(String id) {
		String hql = "from CellReviveInfo  where cellRevive.id='"
				+ id + "'";
		List<CellReviveInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellReviveInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from CellReviveInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<CellReviveInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<CellReviveItem> selectCellReviveItemList(String scId)
			throws Exception {
		String hql = "from CellReviveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and cellRevive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CellReviveItem> list = new ArrayList<CellReviveItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from CellReviveItem where 1=1 and cellRevive.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public CellReviveInfo getResultByCode(String code) {
			String hql=" from CellReviveInfo where 1=1 and code='"+code+"'";
			CellReviveInfo spi=(CellReviveInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}