package com.biolims.experiment.cell.revive.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.abnormal.model.AbnormalItem;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.cell.revive.dao.CellReviveAbnormalDao;
import com.biolims.experiment.cell.revive.model.CellReviveAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class CellReviveAbnormalService {
	@Resource
	private CellReviveAbnormalDao cellReviveAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;

	/**
	 * 
	 * @Title: showCellReviveAbnormalTableJson
	 * @Description: 展示异常样本列表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showCellReviveAbnormalTableJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return cellReviveAbnormalDao.showCellReviveAbnormalTableJson(start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description: 保存并执行
	 * @author : 
	 * @date 
	 * @param ids
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson) throws Exception {
		List<CellReviveAbnormal> saveItems1 = new ArrayList<CellReviveAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		CellReviveAbnormal scp = new CellReviveAbnormal();
		for (Map<String, Object> map : list) {
			scp = (CellReviveAbnormal) cellReviveAbnormalDao.Map2Bean(map, scp);
			CellReviveAbnormal sbi = commonDAO.get(CellReviveAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}

		cellReviveAbnormalDao.saveOrUpdateAll(saveItems1);
		List<CellReviveAbnormal> saveItems = new ArrayList<CellReviveAbnormal>();
		for (String id : ids) {
			CellReviveAbnormal sbi = commonDAO.get(CellReviveAbnormal.class, id);
			sbi.setIsExecute("1");
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi.getCode());
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {// 确认执行
					String next = sbi.getNextFlowId();
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setCode(sbi.getCode());
						st.setSampleCode(sbi.getSampleCode());
						st.setNum(sbi.getSampleNum());
						st.setState("1");
						if(sbi.getSampleOrder()!=null) {
							st.setSampleOrder(commonDAO.get(SampleOrder.class,
									sbi.getSampleOrder().getId()));
						}
						cellReviveAbnormalDao.saveOrUpdate(st);
						// 入库，改变SampleInfo中原始样本的状态为“待入库”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
						}
					} else if (next.equals("0012")) {// 暂停
						// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
						}
					} else if (next.equals("0013")) {// 终止
						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						}
					} else if (next.equals("0014")) {// 反馈项目组
					} else {
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sbi.setState("1");
							sampleInputService.copy(o, sbi);
						}
					}
					// 改变状态不显示在异常样本中
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		cellReviveAbnormalDao.saveOrUpdateAll(saveItems);
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String dataJson, String changeLog) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		CellReviveAbnormal scp = new CellReviveAbnormal();
		for (Map<String, Object> map : list) {
			scp = (CellReviveAbnormal) cellReviveAbnormalDao.Map2Bean(map, scp);
			CellReviveAbnormal sbi = commonDAO.get(CellReviveAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setResult(scp.getResult());
			sbi.setNote(scp.getNote());
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void feedbackAbnormal(String[] ids) {
		if(ids!=null&&ids.length>0){
			for(String id:ids){
				CellReviveAbnormal sa=commonDAO.get(CellReviveAbnormal.class, id);
				sa.setState("2");
				AbnormalItem ai=new AbnormalItem();
				ai.setDicSampleType(sa.getDicSampleType());
				ai.setMethod(sa.getMethod());
				ai.setNextFlow(sa.getNextFlow());
				ai.setNextFlowId(sa.getNextFlowId());
				ai.setNote(sa.getNote());
				ai.setOrderId(sa.getOrderId());
				ai.setProductId(sa.getProductId());
				ai.setProductName(sa.getProductName());
				ai.setProject(sa.getProject());
				ai.setSampleCode(sa.getSampleCode());
				ai.setState("1");
				commonDAO.saveOrUpdate(sa);
				commonDAO.saveOrUpdate(ai);
			}
		}
		
	}

}
