package com.biolims.experiment.sequencingLife.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.dao.DeSequencingTaskDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.sequencingLife.dao.SequencingLifeDao;
import com.biolims.experiment.sequencingLife.dao.SequencingLifeRepeatDao;
import com.biolims.experiment.sequencingLife.model.SequencingLifeAbnormal;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SequencingLifeRepeatService {
	@Resource
	private SequencingLifeRepeatDao sequencingLifeRepeatDao;
	
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private PoolingDao poolingDao;
	@Resource
	private SequencingLifeDao sequencingLifeDao;
	public SequencingLifeRepeatDao getSequencingLifeRepeatDao() {
		return sequencingLifeRepeatDao;
	}


	public void setSequencingLifeRepeatDao(
			SequencingLifeRepeatDao sequencingLifeRepeatDao) {
		this.sequencingLifeRepeatDao = sequencingLifeRepeatDao;
	}


	public SequencingLifeDao getSequencingLifeDao() {
		return sequencingLifeDao;
	}


	public void setSequencingLifeDao(SequencingLifeDao sequencingLifeDao) {
		this.sequencingLifeDao = sequencingLifeDao;
	}


	@Resource
	private DeSequencingTaskDao deSequencingTaskDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSequencingRepeatList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)  {
		return sequencingLifeRepeatDao.selectSampleSequencingInfo(mapForQuery, startNum, limitNum, dir, sort) ;
	}
	

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSequencingRepeat(SequencingLifeAbnormal taskId, String itemDataJson) throws Exception {
		List<SequencingLifeAbnormal> saveItems = new ArrayList<SequencingLifeAbnormal>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			SequencingLifeAbnormal sbti = new SequencingLifeAbnormal();
			// 将map信息读入实体类
			sbti = (SequencingLifeAbnormal) sequencingLifeRepeatDao.Map2Bean(map, sbti);
			if(sbti.getMethod()!=null && sbti.getMethod().equals("1")){
				SequencingLifeTaskTemp stt = sequencingLifeDao.findSequencingTempListByPooling(sbti.getPoolingCode());
				stt.setState("1");
				sequencingLifeRepeatDao.saveOrUpdate(stt);
				sbti.setState(com.biolims.workflow.WorkflowConstants.CREATE_SEQ_MORE);
			}
			sequencingLifeRepeatDao.saveOrUpdate(sbti);
		}

	}
}
