package com.biolims.experiment.sequencingLife.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.dao.DeSequencingTaskDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.sequencingLife.dao.SequencingLifeAbnormalDao;
import com.biolims.experiment.sequencingLife.dao.SequencingLifeDao;
import com.biolims.experiment.sequencingLife.model.SequencingLifeAbnormal;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SequencingLifeAbnormalService {
	@Resource
	private SequencingLifeAbnormalDao sequencingLifeAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private PoolingDao poolingDao;
	@Resource
	private SequencingLifeDao sequencingLifeDao;
	@Resource
	private DeSequencingTaskDao deSequencingTaskDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSequencingAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sequencingLifeAbnormalDao.selectSequencingAbnormalList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SequencingLifeAbnormal i) throws Exception {

		sequencingLifeAbnormalDao.saveOrUpdate(i);

	}

	public SequencingLifeAbnormal get(String id) {
		SequencingLifeAbnormal sequencingLifeAbnormal = commonDAO.get(
				SequencingLifeAbnormal.class, id);
		return sequencingLifeAbnormal;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SequencingLifeAbnormal sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sequencingLifeAbnormalDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	// 保存异常样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSequencingAbnormalList(String itemDataJson)
			throws Exception {
		List<SequencingLifeAbnormal> saveItems = new ArrayList<SequencingLifeAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SequencingLifeAbnormal sbi = new SequencingLifeAbnormal();
			sbi = (SequencingLifeAbnormal) sequencingLifeAbnormalDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				// 将确认执行的pooling拆开成样本，放到下机质控的子表中    ?这是重上机？
				if (sbi.getIsExecute() != null
						&& sbi.getIsExecute().equals("1")) {// 提交
					SequencingLifeTaskTemp stt = sequencingLifeDao
							.findSequencingTempListByPooling(sbi
									.getPoolingCode());
					stt.setState("1");
					stt.setClassify(sbi.getClassify());
					sequencingLifeAbnormalDao.saveOrUpdate(stt);
					// if (sbi.getNextFlow() != null
					// && sbi.getNextFlow().equals("3")) {// 项目管理
					// if (sbi.getMethod() != null
					// && sbi.getMethod().equals("0")) {// 重测序
					// SequencingTaskTemp stt = sequencingDao
					// .findSequencingTempListByPooling(sbi
					// .getPoolingCode());
					// stt.setState("1");
					// stt.setClassify(sbi.getClassify());
					// sequencingAbnormalDao.saveOrUpdate(stt);
					// sbi.setState(com.biolims.workflow.WorkflowConstants.CREATE_SEQ_MORE);
					// }
					// } else if (sbi.getNextFlow() != null
					// && sbi.getNextFlow().equals("1")) {// 重测序
					// SequencingTaskTemp stt = sequencingDao
					// .findSequencingTempListByPooling(sbi
					// .getPoolingCode());
					// stt.setState("1");
					// stt.setClassify(sbi.getClassify());
					// sequencingAbnormalDao.saveOrUpdate(stt);
					// sbi.setState(com.biolims.workflow.WorkflowConstants.CREATE_SEQ_MORE);
					// }
				}
			sbi.setState("2");
			saveItems.add(sbi);
		}
		sequencingLifeAbnormalDao.saveOrUpdateAll(saveItems);
	}
}
