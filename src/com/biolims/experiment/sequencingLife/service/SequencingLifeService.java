package com.biolims.experiment.sequencingLife.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.analy.dao.AnalysisTaskDao;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.analysis.filt.dao.FiltrateTaskDao;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.sequencingLife.dao.SequencingLifeDao;
import com.biolims.experiment.sequencingLife.model.SampleSequencingLifeInfo;
import com.biolims.experiment.sequencingLife.model.SequencingLifeAbnormal;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTask;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskCos;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskItem;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskReagent;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemp;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemplate;
import com.biolims.experiment.wkLife.dao.WKLifeSampleTaskDao;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.SampleInputDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SequencingLifeService {
	@Resource
	private SequencingLifeDao sequencingLifeDao;
	@Resource
	private FiltrateTaskDao filtrateTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WKLifeSampleTaskDao wkLifeSampleTaskDao;
	public SequencingLifeDao getSequencingLifeDao() {
		return sequencingLifeDao;
	}

	public void setSequencingLifeDao(SequencingLifeDao sequencingLifeDao) {
		this.sequencingLifeDao = sequencingLifeDao;
	}

	public WKLifeSampleTaskDao getWkLifeSampleTaskDao() {
		return wkLifeSampleTaskDao;
	}

	public void setWkLifeSampleTaskDao(WKLifeSampleTaskDao wkLifeSampleTaskDao) {
		this.wkLifeSampleTaskDao = wkLifeSampleTaskDao;
	}

	@Resource
	private PoolingDao poolingDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputDao sampleInputDao;
	@Resource
	private AnalysisTaskDao analysisTaskDao;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private StorageService storageService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private StorageOutService storageOutService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSequencingList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String flag) {
		return sequencingLifeDao.selectSequencingList(mapForQuery, startNum,
				limitNum, dir, sort, flag);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SequencingLifeTask i) throws Exception {

		sequencingLifeDao.saveOrUpdate(i);

	}

	public List<SequencingLifeTaskItem> findSequenceItemList(String scId)
			throws Exception {
		List<SequencingLifeTaskItem> list = sequencingLifeDao
				.selectSequenceItemList(scId);
		return list;
	}

	public SequencingLifeTask get(String id) {
		SequencingLifeTask sequencing = commonDAO.get(SequencingLifeTask.class, id);
		return sequencing;
	}

	public Map<String, Object> findSequencingItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sequencingLifeDao.selectSequencingItemList(
				scId, startNum, limitNum, dir, sort);
		List<SequencingLifeTaskItem> list = (List<SequencingLifeTaskItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSequencingTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sequencingLifeDao
				.selectSequencingTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<SequencingLifeTaskTemplate> list = (List<SequencingLifeTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSequencingReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sequencingLifeDao.selectSequencingReagentList(
				scId, startNum, limitNum, dir, sort);
		List<SequencingLifeTaskReagent> list = (List<SequencingLifeTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSequencingCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sequencingLifeDao.selectSequencingCosList(
				scId, startNum, limitNum, dir, sort);
		List<SequencingLifeTaskCos> list = (List<SequencingLifeTaskCos>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSequencingResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sequencingLifeDao.selectSequencingResultList(
				scId, startNum, limitNum, dir, sort);
		List<SampleSequencingLifeInfo> list = (List<SampleSequencingLifeInfo>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSequencingItem(SequencingLifeTask sc, String itemDataJson)
			throws Exception {
		List<SequencingLifeTaskItem> saveItems = new ArrayList<SequencingLifeTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SequencingLifeTaskItem scp = new SequencingLifeTaskItem();
			// 将map信息读入实体类
			scp = (SequencingLifeTaskItem) sequencingLifeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSequencing(sc);

			saveItems.add(scp);

			SequencingLifeTaskTemp temp = this.commonDAO.get(
					SequencingLifeTaskTemp.class, scp.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
			// String result = scp.getResult();
			// if (scp != null) {
			// if (result != null && result.equals("0")) {// 如果结果判定为不合格 样本回到左侧
			// SequencingTaskTemp st = new SequencingTaskTemp();
			// st.setCode(scp.getCode());
			// st.setName(scp.getName());
			// st.setNote(scp.getNote());
			// st.setState("1");
			// this.sequencingDao.saveOrUpdate(st);
			// }
			//
			// // {样本添加到明细，改变SampleInfo中原始样本的状态为“待上机”
			// SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp.getCode());
			// if (sf != null) {
			// sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQUENCING_NEW);
			// sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQUENCING_NEW_NAME);
			// }
			//
			// // // 浓度计算
			// // if (scp.getUpNd() != null && scp.getQpcrNd() != null) {
			// // calculateNd(scp);
			// // }
			// }
		}
		sequencingLifeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingItem(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingLifeTaskItem scp = sequencingLifeDao.get(
					SequencingLifeTaskItem.class, id);
			sequencingLifeDao.delete(scp);
			SequencingLifeTaskTemp temp = this.commonDAO.get(
					SequencingLifeTaskTemp.class, scp.getTempId());
			if (temp != null) {
				temp.setState("1");
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSequencingTemplate(SequencingLifeTask sc, String itemDataJson)
			throws Exception {
		List<SequencingLifeTaskTemplate> saveItems = new ArrayList<SequencingLifeTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SequencingLifeTaskTemplate scp = new SequencingLifeTaskTemplate();
			// 将map信息读入实体类
			scp = (SequencingLifeTaskTemplate) sequencingLifeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSequencing(sc);

			saveItems.add(scp);
		}
		sequencingLifeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingLifeTaskTemplate scp = sequencingLifeDao.get(
					SequencingLifeTaskTemplate.class, id);
			sequencingLifeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveSequencingReagent(SequencingLifeTask sc, String itemDataJson)
			throws Exception {
		List<SequencingLifeTaskItem> item = this.sequencingLifeDao
				.findSequencingItemList(sc.getId());
		Integer re = 0;
		int n = item.size();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SequencingLifeTaskReagent scp = new SequencingLifeTaskReagent();
			// 将map信息读入实体类
			scp = (SequencingLifeTaskReagent) sequencingLifeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSequencing(sc);
			scp.setSampleNum((double) n);
			// 用量 = 单个用量*数量
			if (scp != null && scp.getOneNum() != null
					&& scp.getSampleNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}
			if (scp.getSn() != null && !scp.getSn().equals("")) {
				StorageOutItem soi = storageOutService
						.findStorageOutItem(scp.getSn());
				if (soi != null) {
					if (scp.getCode().equals(soi.getStorage().getId())) {
						scp.setBatch(soi.getCode());
						scp.setExpireDate(soi.getExpireDate());
						re++;
					} else {
						scp.setSn("");
					}
				}
			}
			sequencingLifeDao.saveOrUpdate(scp);
		}
		return re;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingReagent(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingLifeTaskReagent scp = sequencingLifeDao.get(
					SequencingLifeTaskReagent.class, id);
			sequencingLifeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveSequencingCos(SequencingLifeTask sc, String itemDataJson)
			throws Exception {
		Integer saveItem = 0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SequencingLifeTaskCos scp = new SequencingLifeTaskCos();
			// 将map信息读入实体类
			scp = (SequencingLifeTaskCos) sequencingLifeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			Instrument ins = null;
			if (scp.getCode() != null && !scp.equals("")) {
				ins = sequencingLifeDao.get(Instrument.class, scp.getCode());
			}
			scp.setSequencing(sc);
			if (ins != null
					&& ins.getType().getId().equals(scp.getType().getId())) {
				scp.setName(ins.getName());
				scp.setState(ins.getState().getName());
				saveItem++;
			} else {
				scp.setName("");
				scp.setCode("");
				scp.setState("");
			}
			sequencingLifeDao.saveOrUpdate(scp);
		}
		return saveItem;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingCos(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingLifeTaskCos scp = sequencingLifeDao.get(SequencingLifeTaskCos.class,
					id);
			sequencingLifeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSequencingResult(SequencingLifeTask sc, String itemDataJson)
			throws Exception {
		List<SampleSequencingLifeInfo> saveItems = new ArrayList<SampleSequencingLifeInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleSequencingLifeInfo scp = new SampleSequencingLifeInfo();
			// 将map信息读入实体类
			scp = (SampleSequencingLifeInfo) sequencingLifeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setState("1");
			scp.setSequencing(sc);
			scp.setComputerDate(new Date());
			scp.setFcCode(sc.getFcCode());
			saveItems.add(scp);
			// 更改临时表中数据的状态，为不显示
			// List<SequencingTaskTemp> st = sequencingDao
			// .findSequencingTempList();
			// for (SequencingTaskTemp sti : st) {
			// if (scp.getPoolingCode().equals(sti.getPoolingCode())) {
			// sti.setState("2");
			// }
			// }

			// if (scp != null && scp.getResult() != null
			// && scp.getResult().equals("1") && scp.getNextFlow() != null) {
			// if (scp.getNextFlow().equals("0")) {// 下一步流向：文库构建
			// WkTaskTemp wt = new WkTaskTemp();
			// wt.setAcceptDate(scp.getAcceptDate());
			// wt.setCode(scp.getCode());
			// wt.setIdCard(scp.getIdCard());
			// wt.setInspectDate(scp.getInspectDate());
			// wt.setNote(scp.getNote());
			//
			// wt.setOrderId(scp.getOrderId());
			// wt.setPatientName(scp.getPatientName());
			// wt.setPhone(scp.getPhone());
			// wt.setProductId(scp.getProductId());
			// wt.setProductName(scp.getProductName());
			//
			// wt.setReportDate(scp.getReportDate());
			// wt.setSampleCode(scp.getSampleCode());
			// wt.setSequenceFun(scp.getSequenceFun());
			// wt.setState("1");
			// wt.setUnit(scp.getUnit());
			//
			// if (scp.getVolume() != null) {
			// wt.setVolume(Double.parseDouble(scp.getVolume()));
			// } else {
			// wt.setVolume(0d);
			// }
			// wt.setClassify(scp.getClassify());
			// this.sequencingDao.saveOrUpdate(wt);
			// } else if (scp.getNextFlow().equals("1")) {// 下一步流向：重测序
			// SequencingAbnormal sa = new SequencingAbnormal();
			// sa.setAcceptDate(scp.getAcceptDate());
			// sa.setCode(scp.getCode());
			// sa.setFcCode(scp.getFcCode());
			// sa.setIdCard(scp.getIdCard());
			//
			// sa.setInspectDate(scp.getInspectDate());
			// sa.setLaneCode(scp.getLaneCode());
			// sa.setMachineNum(scp.getMachineCode());
			// sa.setMethod(scp.getMethod());
			// sa.setName(scp.getName());
			// sa.setNextFlow(scp.getNextFlow());
			//
			// sa.setNote(scp.getNote());
			// sa.setOrderId(scp.getOrderId());
			// sa.setPatientName(scp.getPatientName());
			// sa.setPf(scp.getPfPercent());
			// sa.setPhone(scp.getPhone());
			//
			// sa.setPoolingCode(scp.getPoolingCode());
			// sa.setProductId(scp.getProductId());
			// sa.setProductName(scp.getProductName());
			// sa.setReportDate(scp.getReportDate());
			// sa.setResult(scp.getResult());
			//
			// sa.setSampleCode(scp.getSampleCode());
			// sa.setSequenceFun(scp.getSequenceFun());
			// sa.setClassify(scp.getClassify());
			// this.sequencingDao.saveOrUpdate(sa);
			// } else if (scp.getNextFlow().equals("2")) {// 下一步流向：入库
			// SampleInItemTemp si = new SampleInItemTemp();
			// si.setNote(scp.getNote());
			// si.setSampleCode(scp.getSampleCode());
			// si.setState("1");
			// this.sequencingDao.saveOrUpdate(si);
			// } else if (scp.getNextFlow().equals("3")) {// 下一步流向：反馈至项目组
			// FeedbackSequencing fs = new FeedbackSequencing();
			// fs.setAcceptDate(scp.getAcceptDate());
			// fs.setCode(scp.getCode());
			// fs.setConcentration(scp.getConcentration());
			// fs.setDataDemand(scp.getDataDemand());
			//
			// fs.setFcCode(scp.getFcCode());
			// fs.setIdCard(scp.getIdCard());
			// fs.setInspectDate(scp.getInspectDate());
			// fs.setLaneCode(scp.getLaneCode());
			// fs.setMachineCode(scp.getMachineCode());
			//
			// fs.setMethod(scp.getMethod());
			// fs.setName(scp.getName());
			// fs.setNextFlow(scp.getNextFlow());
			// fs.setNote(scp.getNote());
			// fs.setOrderId(scp.getOrderId());
			//
			// fs.setPatientName(scp.getPatientName());
			// fs.setPfPercent(scp.getPfPercent());
			// fs.setPhone(scp.getPhone());
			// fs.setPoolingCode(scp.getPoolingCode());
			// fs.setProductId(scp.getProductId());
			//
			// fs.setProductName(scp.getProductName());
			// fs.setQuantity(scp.getQuantity());
			// fs.setReportDate(scp.getReportDate());
			// fs.setResult(scp.getResult());
			// fs.setSampleCode(scp.getSampleCode());
			//
			// fs.setSequenceFun(scp.getSequenceFun());
			// fs.setState("2");
			// fs.setUnit(scp.getUnit());
			// fs.setVolume(scp.getVolume());
			// fs.setClassify(scp.getClassify());
			// this.sequencingDao.saveOrUpdate(fs);
			// } else if (scp.getNextFlow().equals("4")) {// 下一步流向：终止
			// FeedbackSequencing fs = new FeedbackSequencing();
			// fs.setAcceptDate(scp.getAcceptDate());
			// fs.setCode(scp.getCode());
			// fs.setConcentration(scp.getConcentration());
			// fs.setDataDemand(scp.getDataDemand());
			//
			// fs.setFcCode(scp.getFcCode());
			// fs.setIdCard(scp.getIdCard());
			// fs.setInspectDate(scp.getInspectDate());
			// fs.setLaneCode(scp.getLaneCode());
			// fs.setMachineCode(scp.getMachineCode());
			//
			// fs.setMethod(scp.getMethod());
			// fs.setName(scp.getName());
			// fs.setNextFlow(scp.getNextFlow());
			// fs.setNote(scp.getNote());
			// fs.setOrderId(scp.getOrderId());
			//
			// fs.setPatientName(scp.getPatientName());
			// fs.setPfPercent(scp.getPfPercent());
			// fs.setPhone(scp.getPhone());
			// fs.setPoolingCode(scp.getPoolingCode());
			// fs.setProductId(scp.getProductId());
			//
			// fs.setProductName(scp.getProductName());
			// fs.setQuantity(scp.getQuantity());
			// fs.setReportDate(scp.getReportDate());
			// fs.setResult(scp.getResult());
			// fs.setSampleCode(scp.getSampleCode());
			//
			// fs.setSequenceFun(scp.getSequenceFun());
			// fs.setState("3");
			// fs.setUnit(scp.getUnit());
			// fs.setVolume(scp.getVolume());
			// fs.setClassify(scp.getClassify());
			// this.sequencingDao.saveOrUpdate(fs);
			// } else { // 下一步流向：下机质控
			// scp.setResult("1");
			//
			// // SampleDeSequencingInfo sd = new SampleDeSequencingInfo();
			// // sd.setProjectId(scp.getProjectId());
			// // sd.setTechTaskId(scp.getTechTaskId());
			// // sd.setContractId(scp.getContractId());
			// // sd.setOrderType(scp.getOrderType());
			// this.sequencingDao.saveOrUpdate(scp);
			// }
			// }
		}
		sequencingLifeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingResult(String[] ids) throws Exception {
		for (String id : ids) {
			SampleSequencingLifeInfo scp = sequencingLifeDao.get(
					SampleSequencingLifeInfo.class, id);
			sequencingLifeDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(SequencingLifeTask sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			sequencingLifeDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sequencingItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSequencingItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sequencingTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSequencingTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sequencingReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				re =saveSequencingReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sequencingCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				in =saveSequencingCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sequencingResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSequencingResult(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}

	/**
	 * 待上机测序样本
	 * 
	 * @param i
	 * @throws Exception
	 */
	public Map<String, Object> findSequencingTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sequencingLifeDao.selectSequencingTempList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// 审批完成
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void changeState(String applicationTypeActionId, String id)
//			throws Exception {
//		SequencingLifeTask sct = sequencingLifeDao.get(SequencingLifeTask.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
//		sct.setConfirmUser(user);
//		sct.setConfirmDate(new Date());
//		sequencingLifeDao.update(sct);
//
//		submitSample(id,null);
//		// 将批次信息反馈到模板中
//		List<SequencingLifeTaskReagent> list1 = sequencingLifeDao.setReagentList(id);
//		for (SequencingLifeTaskReagent dt : list1) {
//			String bat1 = dt.getBatch(); // 实验中试剂的批次
//			String drid = dt.gettReagent(); // 实验中保存的试剂ID
//			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
//			for (ReagentItem ri : list2) {
//				if (bat1 != null) {
//					ri.setBatch(bat1);
//				}
//			}
//			// 改变库存主数据试剂数量
//			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
//					dt.getBatch(), dt.getNum());
//		}
//
//		// 将主表的数据存放到，下机质控表
//		DeSequencingTask dst = null;
//		dst = new DeSequencingTask();
//		if (dst.getId() == null || dst.getId().equals(""))
//			dst.setId(codingRuleService.genTransID("DeSequencingTask", "XJZK"));
//		dst.setName(sct.getName());
//		dst.setMachineTime(sct.getExpectDate());
//		dst.setREF(sct.getRefCode());
//		dst.setFlowCode(sct.getFcCode());
//		if (sct.getDeviceCode() != null)
//			dst.setMachineCode(sct.getDeviceCode().getId());
//		Date date = new Date();
//		dst.setCreateDate(date);
//		dst.setCreateUser(user);
//		dst.setState("3");
//		dst.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
//		sequencingLifeDao.saveOrUpdate(dst);
//		/**
//		 * 上机测序life生成下机质控明细
//		 */
//		List<SequencingLifeTaskItem> sti = sequencingLifeDao.findSequencingItemList1(id);
//		for(SequencingLifeTaskItem stii:sti){
//			List<SampleWkLifeInfo> wkinfos= wKSampleTaskService.showWKSplitInfoList2(stii.getPoolingCode());
//			for(SampleWkLifeInfo wkinfo:wkinfos){
//				DeSequencingTaskItem dsti = new DeSequencingTaskItem();
//				dsti.setSampleID(wkinfo.getCode());
//				dsti.setOrderCode(wkinfo.getSampleInfo().getOrderNum());
//				if(wkinfo.getSampleInfo().getProject()==null){
//
//				}else{
//					dsti.setProjectId(wkinfo.getSampleInfo().getProject().getId());
//				}
//				
//				dsti.setProductName(wkinfo.getProductName());
//				dsti.setRunId(stii.getRunCode());
//				dsti.setDesequencingTask(dst);
//				dsti.setPoolingCode(stii.getPoolingCode());
//				dsti.setResult("1");
//				sequencingLifeDao.saveOrUpdate(dsti);
//			}
//		}
//		
//		
//		// // 将主表的数据存放到，信息分析表
//		// for (SampleSequencingInfo info : seqInfo) {
//		// if (info.getResult().equals("1")) {
//		// // 合格的上机結果表到下机质控
//		// // List<TechJkServiceTask> tjstlist = new
//		// // ArrayList<TechJkServiceTask>();
//		// if (info.getPoolingCode() != null) {
//		// PoolingTask pt = sequencingDao.get(PoolingTask.class,
//		// info.getPoolingId());
//		// List<PoolingTaskItem> ptiList = poolingDao
//		// .getPoolingItem(pt.getId());
//		// TechJkServiceTask tjst = null;
//		// for (PoolingTaskItem pti : ptiList) {
//		// if (pti != null && pti.getClassify() != null
//		// && pti.getClassify().equals("1"))
//		// tjst = sampleInfoMainDao.get(
//		// TechJkServiceTask.class, pti.getOrderId());
//		// // if (!projectList.contains(tjst.getProjectId()))
//		// // projectList.add(tjst.getProjectId());
//		//
//		// }
//		//
//		// }
//		//
//		// }
//		// }
//		// if (projectList.size() > 0) {
//		// for (String project : projectList) {
//		// TechJkServiceTask tjst1 = wkSampleTaskDao
//		// .selectTechTaskByProject(project);
//		// FiltrateTask ft = new FiltrateTask();
//		// if (ft.getId() == null || ft.getId().equals(""))
//		// ft.setId(codingRuleService.genTransID("FiltrateTask", "GL"));
//		// ft.setName(tjst1.getName());
//		// ft.setTechTaskId(tjst1.getId());
//		// // ft.setContractId(tjst1.getContractId());
//		// // ft.setProjectId(tjst1.getProjectId());
//		// // ft.setHeadUser(tjst1.getProjectResponsiblePerson());
//		// // ft.setCreateDate(new Date());
//		// // ft.setCreateUser(sct.getCreateUser());
//		// ft.setState("3");
//		// ft.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
//		// filtrateTaskDao.saveOrUpdate(ft);
//		//
//		// TechAnalysisTask atkk = new TechAnalysisTask();
//		// if (atkk.getId() == null || atkk.getId().equals(""))
//		// atkk.setId(codingRuleService.genTransID("TechAnalysisTask",
//		// "KXFX"));
//		// atkk.setName(tjst1.getName());
//		// atkk.setTechTaskId(tjst1.getId());
//		// // atkk.setContractId(tjst1.getContractId());
//		// // atkk.setProjectId(tjst1.getProjectId());
//		// // atkk.setHeadUser(tjst1.getProjectResponsiblePerson());
//		// DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		// String stime = format.format(new Date());
//		// atkk.setCreateDate(stime);
//		// // atkk.setCreateUser(sct.getCreateUser());
//		// atkk.setState("3");
//		// atkk.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
//		// analysisTaskDao.saveOrUpdate(atkk);
//		// }
//		// } else {
//		// atk = new AnalysisTask();
//		// if (atk.getId() == null || atk.getId().equals(""))
//		// atk.setId(codingRuleService.genTransID("AnalysisTask", "XXFX"));
//		// atk.setFlowCell(sct.getFcCode());
//		// atk.setDownComputerTime(sct.getExpectDate());
//		// DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		// String stime = format.format(new Date());
//		// atk.setCreateDate(stime);
//		// atk.setCreateUser(sct.getCreateUser());
//		// atk.setName(sct.getName());
//		// atk.setState("3");
//		// atk.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
//		// analysisTaskDao.saveOrUpdate(atk);
//		// }
//		//
//		// List<SampleSequencingInfo> sr = sequencingDao
//		// .getSequencingResultList(sct.getId());
//		// if (sr.size() > 0) {
//		// for (SampleSequencingInfo sri : sr) {
//		// // 完成的时候改变原始的样本状态为“完成上机”
//		// List<PoolingTaskItem> sp = poolingDao.getPoolingItem(sri
//		// .getPoolingCode());
//		// for (PoolingTaskItem spp : sp) {
//		// if (spp.getSampleCode() != null
//		// && !spp.getSampleCode().equals("")
//		// && !spp.getSampleCode().contains("CT")) {
//		// // SampleInfo sf =
//		// // sampleInputDao.findSampleInfoByCode(spp
//		// // .getSampleCode());
//		// // if (sf != null) {
//		// // sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQUENCING_COMPLETE);
//		// // sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQUENCING_COMPLETE_NAME);
//		// // }
//		// }
//		// }
//		// // 到下机质控数据明细表
//		// // DeSequencingTaskItem dati=new DeSequencingTaskItem();
//		// // dati.setFlowCode(sri.getFcCode());
//		// // dati.setPoolingCode(sri.getCode());
//		// // dati.setDesequencingTask(dst);
//		// // // dati.setLaneCode(sri.getLaneCode());
//		// // analysisTaskDao.saveOrUpdate(dati);
//		// }
//		// }
//		// /*
//		// * List<SequencingResult> sr =
//		// * sequencingDao.getSequencingResultList(mainID); for(SequencingResult
//		// * sri : sr){ //将子表中合格的pooling拆开成样本，放到下机质控的子表中 //获取pooling的明细表
//		// * List<PoolingItem> pi = poolingDao.getPoolingItem(sri.getCode());
//		// * for(PoolingItem pii : pi){ codes += pii.getCode()+","; } }
//		// * dst.setCodes(codes); deSequencingTaskDao.saveOrUpdate(dst);
//		// *
//		// * /*if(sri.getResult()!=null&&sri.getResult().equals("0")){ }else
//		// * if(sri.getResult()!=null&&sri.getResult().equals("1")){
//		// * //将子表中不合格的pooling，放到异常项目管理 SequencingFeedback sfb = new
//		// * SequencingFeedback(); sfb.setName(sri.getName());
//		// * sfb.setCode(sri.getCode()); sfb.setMixVolume(sri.getVolume());
//		// * sfb.setFcCode(sri.getFcCode()); sfb.setLaneCode(sri.getLaneCode());
//		// * sfb.setMachineNum(sri.getMachineCode());
//		// * sfb.setSequencingDate(sri.getComputerDate());
//		// * sfb.setDensity(sri.getConcentration());
//		// * sfb.setPf(sri.getPfPercent()); sfb.setIsgood(sri.getResult());
//		// * sfb.setNextflow(sri.getNext()); sfb.setAdvice(sri.getAdvice());
//		// * sfb.setNote(sri.getNote()); sfb.setSequencing(sri.getSequencing());
//		// *
//		// * sequencingFeedbackDao.saveOrUpdate(sfb); }
//		// *
//		// * }
//		// */
//		// // 生成CSV文件
//		createCSV(sct.getId());
//
//	}

	public void createSampleSysRemind(String title1, User user, String tableId,
			String contentId) {
		SysRemind sr = new SysRemind();
		String title11 = "";
		title11 = title1;
		title1 = title1.substring(0, title1.length() - 1);
		sr.setHandleUser(user);
		sr.setTitle(title11);
		sr.setState("0");
		sr.setTableId(tableId);
		sr.setContent(title1);
		sr.setType("1");
		sr.setRemindUser("SYSTEM");
		sr.setStartDate(new Date());
		sr.setContentId(contentId);
		sequencingLifeDao.saveOrUpdate(sr);
	}

	// ========
	public List<Map<String, String>> getSequencingItemList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sequencingLifeDao
				.setSequencingResultItemList(code);
		List<SampleSequencingLifeInfo> list = (List<SampleSequencingLifeInfo>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SampleSequencingLifeInfo sr : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("sName", sr.getName());
				map.put("sampleCode", sr.getSampleCode());
				map.put("flowCell", code);
				mapList.add(map);
			}
		}
		return mapList;
	}

	// ================2015-12-02 ly==============
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTemplateOne(String ids) throws Exception {
		SequencingLifeTaskTemplate scp = sequencingLifeDao.get(
				SequencingLifeTaskTemplate.class, ids);
		sequencingLifeDao.delete(scp);
	}

	// ==============================
	// =============2015-12-02 ly================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingReagentOne(String ids) throws Exception {
		SequencingLifeTaskReagent scp = sequencingLifeDao.get(
				SequencingLifeTaskReagent.class, ids);
		sequencingLifeDao.delete(scp);
	}

	// =============================
	// ===========2015-12-02 ly==============
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingCosOne(String ids) throws Exception {
		SequencingLifeTaskCos scp = sequencingLifeDao.get(SequencingLifeTaskCos.class, ids);
		sequencingLifeDao.delete(scp);
	}

	// =========================

	// 根据步骤加载试剂明细
	public Map<String, Object> findSequencingReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = sequencingLifeDao
				.selectSequencingReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<SequencingLifeTaskReagent> list = (List<SequencingLifeTaskReagent>) result
				.get("list");
		return result;
	}

	// 根据步骤加载设备明细
	public Map<String, Object> findSequencingCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = sequencingLifeDao
				.selectSequencingCosListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		List<SequencingLifeTaskCos> list = (List<SequencingLifeTaskCos>) result
				.get("list");
		return result;
	}

	/*
	 * 浓度计算
	 */
	private void calculateNd(SequencingLifeTaskItem sbti) {

		Map<String, String> result = new HashMap<String, String>();
		/*
		 * X=QPCR浓度 Y=上机浓度 x-90>0或者x-90<=0&& 0.11x-y>0需要稀释
		 */
		Double x = 0d;
		Double y = 0d;
		String _x = sbti.getQpcrNd();
		String _y = sbti.getUpNd();
		String type = sbti.getSequencingPlatform();
		if (_x.equals(""))
			_x = "0";

		if (_y.equals(""))
			_y = "0";

		x = Double.parseDouble(_x);
		y = Double.parseDouble(_y);
		if (x - 90 > 0 || (x - 90 <= 0 && 0.11 * x - y > 0)) {
			String dilutionOne = sbti.getDilutionOne();
			if (dilutionOne == null || dilutionOne.equals(""))
				sbti.setDilutionOne("3+");
			String dilutionTwo = sbti.getDilutionTwo();
			if (dilutionTwo == null || dilutionTwo.equals(""))
				sbti.setDilutionTwo(3 * x / 10 - 3 + "");
			String dilutionThree = sbti.getDilutionThree();
			if (dilutionThree == null || dilutionThree.equals(""))
				sbti.setDilutionThree("-->");
			String dilutionFour = sbti.getDilutionFour();
			if (dilutionFour == null || dilutionFour.equals(""))
				sbti.setDilutionFour("10nM");
			x = 10d;
		}

		// result = calculateByyblAndHyb(x, y, 10);
		// if (result.size() > 0) {
		// float dnaTake = Float.parseFloat(result.get("dnaTake"));
		//
		// if (!(dnaTake >= 1.1 && dnaTake <= 9.5))
		result = calculateByyblAndHyb(x, y, 20, type);

		sbti.setBxybl(result.get("bxyp"));
		sbti.setHyb(result.get("hyb"));
		sbti.setBxhnd(result.get("bxhnd"));
		sbti.setNaOhTake(result.get("naohTake"));
		sbti.setDnaTake(result.get("dnaTake"));
		sbti.setHybTake(result.get("hybTake"));
		// }

	}

	/*
	 * 变性样本量和
	 */
	private Map<String, String> calculateByyblAndHyb(Double x, Double y,
			int setup, String type) {
		// 变性样品常量
		float[] bxyps = { 4f, 3.5f, 3f, 2.5f, 2f, 1.5f, 1.25f, 1f };

		Map<String, String> temp = new HashMap<String, String>();
		float minDna = 10f;
		int DL = 0;
		if (type != null) {
			if (type.equals("0") || type.equals("1")) {
				DL = 1500;
			} else if (type.equals("3") || type.equals("4")) {
				DL = 500;
			} else {
				DL = 1000;
			}
		}
		for (int i = 0; i < bxyps.length; i++) {
			String byhnd = formatDouble((y / 1000) * (DL / bxyps[i]),
					"####.###");
			String dnaTake = formatDouble(setup * Float.parseFloat(byhnd) / x,
					"####.##");
			if (new Float(0).equals(minDna))
				minDna = Float.parseFloat(dnaTake);

			if ((Float.parseFloat(dnaTake) >= 1.1 && Float.parseFloat(dnaTake) <= 9.5)
					&& minDna >= Float.parseFloat(dnaTake)) {
				minDna = Float.parseFloat(dnaTake);
				temp.put("bxyp", bxyps[i] + "");
				temp.put("hyb", (DL - bxyps[i]) + "");
				temp.put("bxhnd", byhnd);
				temp.put("dnaTake", dnaTake);

				String hybTake = "";
				// if (setup == 10) {
				// temp.put("naohTake", "0.5");
				// hybTake = formatDouble(10 -
				// Double.parseDouble(temp.get("dnaTake")) - 0.5, "####.##");
				// } else {
				temp.put("naohTake", "1");
				hybTake = formatDouble(
						20 - Double.parseDouble(temp.get("dnaTake")) - 1,
						"####.##");
				// }

				temp.put("hybTake", hybTake);
			}
		}
		return temp;
	}

	private String formatDouble(Double dou, String str) {
		DecimalFormat df = new DecimalFormat(str);
		return df.format(dou);
	}

	// 根据选中的步骤查询相关的试剂明细
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sequencingLifeDao.setReagent(id, code);
		List<SequencingLifeTaskReagent> list = (List<SequencingLifeTaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			for (SequencingLifeTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				// map.put("note",ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				if(ti.getExpireDate()!=null){
					map.put("expireDate", sdf.format(ti.getExpireDate()));
				}
				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getSequencing().getId());
				map.put("tName", ti.getSequencing().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据选中的步骤查询相关的设备明细
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sequencingLifeDao.setCos(id, code);
		List<SequencingLifeTaskCos> list = (List<SequencingLifeTaskCos>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SequencingLifeTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				if (ti.getType() != null) {
					map.put("typeId", ti.getType().getId());
					map.put("typeName", ti.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getSequencing().getId());
				map.put("tName", ti.getSequencing().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	/**
	 * 生成浓度计算EXCEL
	 * 
	 * @return
	 * @throws Exception
	 */
	/*
	 * @WriteOperLog
	 * 
	 * @WriteExOperLog
	 * 
	 * @Transactional(rollbackFor = Exception.class) public String
	 * createExcel(String taskId) throws Exception { Map<String, String>
	 * mapForQuery = new HashMap<String, String>(); mapForQuery.put("task.id",
	 * taskId); SequencingTask at = sequencingDao.get(SequencingTask.class,
	 * taskId); Map<String, Object> result =
	 * sequencingDao.selectSequencingItemList(taskId, null, null, null, null);
	 * List<SequencingTaskItem> list = (List<SequencingTaskItem>)
	 * result.get("list"); Map<String, Object> resultInfo =
	 * sequencingDao.selectSequencingResultList(taskId, null, null, null, null);
	 * List<SampleSequencingInfo> listInfo = (List<SampleSequencingInfo>)
	 * resultInfo.get("list"); if (listInfo == null || listInfo.size() <= 0)
	 * return null;
	 * 
	 * 
	 * File root = new File(ConfigFileUtil.getRootPath()); File file = new
	 * File(root,DateUtil.format(new Date(),
	 * "yyyyMMdd")+listInfo.get(0).getFcCode()); if(!file.exists())
	 * file.mkdirs(); // InputStream in = new
	 * FileInputStream("D:/biolims/uploadFileRoot/20160112/seqtask.xls"); //
	 * ExcelSheet excel = new ExcelSheet(in); String fcCode="";
	 * if(listInfo.size()>0){ fcCode =
	 * listInfo.get(0).getSequencing().getFcCode()+"_"; } File file1 = new
	 * File(file,fcCode+SystemConstants.SEQITEM_CERATE_TASK_EXECL_FILE_NAME);
	 * if(!file1.exists()) file1.createNewFile(); OutputStream os = new
	 * FileOutputStream(file1); HSSFWorkbook wb = new HSSFWorkbook(); HSSFFont
	 * font = wb.createFont();//字体 HSSFSheet sheet = wb.createSheet("上机结果");
	 * HSSFRow row = sheet.createRow((int)0); HSSFCellStyle style =
	 * wb.createCellStyle(); sheet.setDefaultColumnWidth(20);//设置宽度
	 * sheet.setDefaultRowHeightInPoints(10);//设置行高
	 * style.setFillForegroundColor((short) 13);// 设置背景色
	 * style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
	 * style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	 * style.setWrapText(true);//设置自动换行 String [] header =
	 * {"项目名","项目代码","项目类型代码","要求测序类型","样品名","数据量需求","文库号","i7index",
	 * "i5index","read1index","read2index","adapter","混合比例","混合后文库号",
	 * "项目负责人邮箱","信息负责人邮箱","实验负责人邮箱"}; HSSFCell cell = null; for (int
	 * i=0;i<header.length;i++) { cell = row.createCell(i);
	 * cell.setCellValue(header[i]); cell.setCellStyle(style);
	 * cell.setCellType(HSSFCell.CELL_TYPE_STRING); sheet.setColumnWidth(0,
	 * 16000); sheet.setColumnWidth(1, 4000); sheet.setColumnWidth(2, 3500);
	 * sheet.setColumnWidth(3, 3500); sheet.setColumnWidth(4, 4000);
	 * sheet.setColumnWidth(5, 4000);
	 * 
	 * sheet.setColumnWidth(6, 4000); sheet.setColumnWidth(7, 4000);
	 * sheet.setColumnWidth(8, 4000); sheet.setColumnWidth(9, 4000);
	 * sheet.setColumnWidth(10, 4000); sheet.setColumnWidth(11, 4000);
	 * sheet.setColumnWidth(12, 4000); sheet.setColumnWidth(13, 5500);
	 * sheet.setColumnWidth(14, 5500); sheet.setColumnWidth(15, 5500);
	 * sheet.setColumnWidth(16, 5500); font.setFontName("仿宋_GB2312"); //
	 * font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//粗体显示
	 * font.setFontHeightInPoints((short) 14); style.setFont(font); }
	 * 
	 * for (int i = 0; i < listInfo.size(); i++) { row = sheet.createRow(i + 1);
	 * SampleSequencingInfo item = listInfo.get(i); if(item.getName()==null){
	 * row.createCell(0).setCellValue(item.getFcCode()+""); }else{
	 * row.createCell(0).setCellValue(item.getFcCode()+item.getName()); }
	 * row.createCell(1).setCellValue(item.getFcCode());
	 * row.createCell(2).setCellValue(item.getProductName());
	 * row.createCell(3).setCellValue(item.getMethod());
	 * row.createCell(4).setCellValue(item.getName());
	 * row.createCell(5).setCellValue(item.getDataDemand());
	 * row.createCell(6).setCellValue("");
	 * row.createCell(7).setCellValue(item.getPoolingCode());
	 * row.createCell(8).setCellValue(item.getPfPercent());
	 * row.createCell(9).setCellValue(item.getMachineCode());
	 * row.createCell(10).setCellValue(""); row.createCell(11).setCellValue("");
	 * row.createCell(12).setCellValue(""); row.createCell(13).setCellValue("");
	 * row.createCell(14).setCellValue(""); row.createCell(15).setCellValue("");
	 * row.createCell(16).setCellValue("");
	 * sheet.setDefaultColumnWidth(50);//设置宽度
	 * style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 居中
	 * row.setRowStyle(style); } wb.write(os); os.close();
	 * 
	 * // excel.setValue(1, 0, taskId); // excel.setValue(1, 1, at.getName());
	 * // excel.setValue(1, 2, at.getFcCode()); // excel.setValue(1, 3,
	 * at.getRefCode()); // excel.setValue(1, 4, at.getExpectDate()); //
	 * excel.setValue(1, 5, at.getTemplate().getName());
	 * 
	 * // InputStream in1 =new
	 * FileInputStream("D:/biolims/uploadFileRoot/20160112/seqitem.xls"); //
	 * File file1 = new File(root,
	 * SystemConstants.SEQITEM_CERATE_TASK_EXECL_FILE_NAME + ".xls"); //
	 * ExcelSheet excel1 = new ExcelSheet(in1); // int x = 1;//excel要填写数据的起始行 //
	 * int cxjldX = 7; // for (SequencingTaskItem ssti : list) { // String note
	 * = ssti.getNote(); // String laneCode = ssti.getLaneCode(); // String
	 * poolingCode = ssti.getPoolingCode(); // String hyb = ssti.getHyb(); //
	 * String bxybl = ssti.getBxybl();//变性样本量 // String bxhnd =
	 * ssti.getBxhnd();//变性后浓度 // String prepareNd = ssti.getPrepareNd();//预期密度
	 * // String qpcrNd = ssti.getQpcrNd(); // String hybTake =
	 * ssti.getHybTake(); // String yw = ssti.getYw(); // String dnaTake =
	 * ssti.getDnaTake(); // String naohTake = ssti.getNaOhTake(); // String
	 * upNd = ssti.getUpNd(); // excel1.setValue(x, 0, laneCode); //
	 * excel1.setValue(x, 0, poolingCode); // excel1.setValue(x, 1, qpcrNd); //
	 * excel1.setValue(x, 2, upNd); // excel1.setValue(x, 3, yw); //
	 * excel1.setValue(x, 4, bxybl); // excel1.setValue(x, 5, hyb); //
	 * excel1.setValue(x, 6, bxhnd); // excel1.setValue(x, 7, dnaTake); //
	 * excel1.setValue(x, 8, hybTake); // excel1.setValue(x, 4, naohTake); // x
	 * ++;
	 * 
	 * // String location = ""; // location = ssti.getLocation(); //
	 * excel1.setValue(cxjldX, 1, tubeCode);//任务单名称描述 // excel1.setValue(cxjldX,
	 * 2, poolingCode);// // excel1.setValue(cxjldX, 3, "");//样品名称 //
	 * excel1.setValue(cxjldX, 4, upNd);//上机浓度 // excel1.setValue(cxjldX, 5,
	 * prepareNd);//预期密度
	 * 
	 * // String _yw = ""; // if ("1".equals(yw)) { // _yw = "DNA Primer"; // }
	 * else if ("2".equals(yw)) { // _yw = "sRNA Primer"; // } else if
	 * ("3".equals(yw)) { // _yw = "expression Primer"; // } //
	 * excel1.setValue(cxjldX, 6, _yw);//引物 // // excel1.setValue(cxjldX, 7,
	 * note);//备注 // if (note != null && note.length() > 0) // if
	 * (note.equals("旧库") || note.equals("旧库Control")) //
	 * excel1.setValue(cxjldX, 8, location);// // cxjldX++; // } // String root
	 * = ConfigFileUtil.getRootPath() + File.separator + DateUtil.format(new
	 * Date(), "yyyyMMdd"); // if (!new File(root).exists()) // new
	 * File(root).mkdirs();
	 * 
	 * // 磁盘文件文件名称 // String deskFileName = UUID.randomUUID().toString(); //
	 * File deskFile = new File(root, deskFileName + ".xls");
	 * 
	 * // String csjldDeskFileName = UUID.randomUUID().toString(); // File
	 * csjldDeskFile = new File(root, csjldDeskFileName + ".xls");
	 * 
	 * // FileInfo excelFile = new FileInfo(); //
	 * excelFile.setFileName("REC-SC-032 浓度计算单_" + taskId + ".xls"); //
	 * excelFile.setFilePath(deskFile.getAbsolutePath()); //
	 * sequencingDao.save(excelFile);
	 * 
	 * // FileInfo csjldExcelFile = new FileInfo(); //
	 * csjldExcelFile.setFileName("REC-SC-034 测序记录单_" + taskId + ".xls"); //
	 * csjldExcelFile.setFilePath(csjldDeskFile.getAbsolutePath()); //
	 * sequencingDao.save(csjldExcelFile);
	 * 
	 * // SequencingTask sst = sequencingDao.get(SequencingTask.class, taskId);
	 * // sst.setExcelFile(excelFile); // sst.setCxjldExcelFile(csjldExcelFile);
	 * // sequencingDao.update(sst);
	 * 
	 * // excel.save(deskFile); // excel1.save(csjldDeskFile); // return "'" +
	 * excelFile.getId() + "','" + csjldExcelFile.getId() + "'"; FileInfo info =
	 * new FileInfo(); if(listInfo.size() >0){
	 * info.setFileName(listInfo.get(0).getSequencing
	 * ().getId()+SystemConstants.ANALY_CERATE_TASK_EXECL_FILE_NAME);
	 * info.setFilePath(file1.toString());
	 * this.sequencingDao.saveOrUpdate(info); } String path =
	 * info.getFilePath(); String fileName = info.getFileName(); InputStream is
	 * = new FileInputStream(path); OutputStream outs = new
	 * FileOutputStream("E:\\"+fileName); byte[] bytes = new byte[1024];
	 * while(is.read(bytes)!=-1){ outs.write(bytes); } outs.flush();
	 * outs.close();
	 * 
	 * return file1.getName(); }
	 */

	/**
	 * 生成上机测序CSV文件
	 * 
	 * @return
	 * @throws Exception
	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public String createCSV(String taskId) throws Exception {
//
//		// 时间转换
//		SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
//		Map<String, String> mapForQuery = new HashMap<String, String>();
//		mapForQuery.put("task.id", taskId);
//		// 根据ID获取上机测序信息（"上机任务单号", "上机时间", "上机机器号", "上机试剂盒"）
//		SequencingLifeTask sat = sequencingLifeDao.get(SequencingLifeTask.class, taskId);
//		// 查询测序明细
//		Map<String, Object> result = sequencingLifeDao.selectSequencingItemList(
//				taskId, null, null, null, null);
//		List<SequencingLifeTaskItem> list = (List<SequencingLifeTaskItem>) result
//				.get("list");
//		if (list == null || list.size() <= 0)
//			return null;
//		String a = "/out";
//		String b = "/in/\\";
//		String filePath = SystemConstants.WRITE_QC_PATH + "/" + sat.getFcCode();// 写入csv路径,以fc号为准
//		// 往qcdata中写入两个csv文件
//		String fileName = filePath + a + "/sampleSheets.csv";// 文件名称
//
//		String fileNames = filePath + "/in";
//		File file = new File(fileNames);
//		file.mkdirs();
//
//		// String fName = filePath + b + "";// 生成in 文件夹
//		File csvFile = null;
//		File csvFile1 = null;
//		BufferedWriter csvWtriter = null;
//		BufferedWriter csvWtriter1 = null;
//		csvFile = new File(fileName);
//		// csvFile1 = new File(fName);
//		File parent = csvFile.getParentFile();
//		if (parent != null && !parent.exists()) {
//			parent.mkdirs();
//		}
//		// File parent1 = csvFile1.getParentFile();
//		// if (parent1 != null && !parent1.exists()) {
//		// parent1.mkdirs();
//		// }
//		csvFile.createNewFile();
//		// csvFile1.createNewFile();
//
//		// GB2312使正确读取分隔符","
//		csvWtriter = new BufferedWriter(new OutputStreamWriter(
//				new FileOutputStream(csvFile), "GBK"), 1024);
//		// 写入文件头部
//		Object[] head = { "文库名称", "浓度ng/ul", "体积ul", "文库合格片段范围",
//				"index Primer NO.", "index Primer序列", "index Primer反向互补序列",
//				"pooling比例", "备注", "文库号", "文库类型", "物种", "要求数据量", "实际数据量（G）",
//				"项目编号", "项目名称" };
//
//		List<Object> headList = Arrays.asList(head);
//		for (Object data : headList) {
//			StringBuffer sb = new StringBuffer();
//			String rowStr = sb.append("\"").append(data).append("\",")
//					.toString();
//			csvWtriter.write(rowStr);
//		}
//		csvWtriter.newLine();
//		for (int i = 0; i < list.size(); i++) {
//			SequencingLifeTaskItem info = list.get(i);
//			if (info != null) {
//				// 根据上机明细上机分组号查询富集结果表 获取富集富集文库
//				List<PoolingBlendInfo> pooling_result = this.sequencingLifeDao
//						.selectPoolingBlendInfoBysjfz(info.getPoolingCode());
//				for (PoolingBlendInfo pfp : pooling_result) {
//					// 根据富集文库查询富集明细样本信息
//					List<PoolingTaskItem> ptl = this.sequencingLifeDao
//							.selectPoolingTaskItemByfjWkCode(pfp.getFjWkCode());
//					for (PoolingTaskItem poolingTaskItem : ptl) {
//						// 根据样本编号查询文库构建结果表
//						List<SampleWkLifeInfo> swi = this.sequencingLifeDao
//								.selectSampleInfoByCode(poolingTaskItem
//										.getCode());
//						for (SampleWkLifeInfo wk : swi) {
//							// 根据文库结果表样本编号查询文库明细表信息
//							// List<WkTaskItem> wti = this.sequencingDao
//							// .selectWkTaskItemByCode(wk.getCode());
//							// for (WkTaskItem wkitem : wti) {
//							// 查询样本信息
//							SampleInfo yangben = this.sequencingLifeDao
//									.selectSampleInfoCode(poolingTaskItem
//											.getSampleCode());
//							// // 查询订单
//							// SampleOrder so = new SampleOrder();
//							// if (yangben.getOrderNum() != null) {
//							// so = this.sequencingDao.get(
//							// SampleOrder.class,
//							// yangben.getOrderNum());
//							// }
//							// // 查询电子病历
//							// CrmPatient cp = new CrmPatient();
//							// if (so.getMedicalNumber() != null) {
//							// cp = this.sequencingDao.get(
//							// CrmPatient.class,
//							// so.getMedicalNumber());
//							// }
//							StringBuffer sb = new StringBuffer();
//							sb.append("\"")
//									.append(poolingTaskItem.getFjWkCode())
//									.append("\",");// 文库名称
//							sb.append("\"").append(wk.getWkConcentration())
//									.append("\",");// 浓度ng/ul
//							sb.append("\"").append(wk.getWkVolume())
//									.append("\",");// 体积ul
//							sb.append("\"").append("").append("\",");// 文库合格片段范围
//							sb.append("\"").append("").append("\",");// index
//																		// Primer
//																		// NO.
//							sb.append("\"").append("").append("\",");// index
//																		// Primer序列
//							sb.append("\"").append("").append("\",");// index
//																		// Primer反向互补序列
//							sb.append("\"").append("").append("\",");// pooling比例
//							sb.append("\"").append(wk.getNote()).append("\",");// 备注
//							sb.append("\"").append(wk.getCode()).append("\",");// 文库号
//							sb.append("\"").append(pfp.getWkType())
//									.append("\",");// 文库类型
//							sb.append("\"").append("").append("\",");// 物种
//							sb.append("\"").append("").append("\",");// 要求数据量
//							sb.append("\"").append("").append("\",");// 实际数据量（G）
//							if (yangben.getProject() != null) {
//
//								sb.append("\"")
//										.append(yangben.getProject().getId())
//										.append("\",");// 项目编号
//								sb.append("\"")
//										.append(yangben.getProject().getName())
//										.append("\",");// 项目名称
//							} else {
//								sb.append("\"").append("").append("\",");// 项目编号
//								sb.append("\"").append("").append("\",");// 项目名称
//
//							}
//							String rowStr = sb.toString();
//							csvWtriter.write(rowStr);
//							csvWtriter.newLine();
//							// }
//
//						}
//
//					}
//				}
//
//			}
//		}
//
//		csvWtriter.flush();
//		csvWtriter.close();
//
//		/**
//		 * 第二个.scv文件
//		 * 
//		 */
//
//		// // GB2312使正确读取分隔符","
//		// csvWtriter1 = new BufferedWriter(new OutputStreamWriter(
//		// new FileOutputStream(csvFile1), "GBK"), 1024);
//		// // 写入文件头部
//		// Object[] header = { "科研项目编号", "电子病历编号", "年龄", "性别", "癌症类型", "癌症分期",
//		// "订单编号", "检测项目", "样本编号", "采样日期", "样本类型", "探针库", "标签", "上机任务单号",
//		// "上机时间", "上机机器号", "上机试剂盒" };
//		// List<Object> headerList = Arrays.asList(header);
//		// for (Object data : headerList) {
//		// StringBuffer sb = new StringBuffer();
//		// String rowStr = sb.append("\"").append(data).append("\",")
//		// .toString();
//		// csvWtriter1.write(rowStr);
//		// }
//		// csvWtriter1.newLine();
//		// for (int i = 0; i < list.size(); i++) {// list.size()
//		// SequencingTaskItem info = list.get(i);
//		// if (info != null) {
//		// // List<PoolingTaskItem> ptl = this.sequencingDao
//		// // .selectWkCodeBySampleCodeKy(info.getPoolingCode());
//		//
//		// // 根据上机明细上机分组号查询富集结果表 获取富集富集文库
//		// List<PoolingBlendInfo> pooling_result = this.sequencingDao
//		// .selectPoolingBlendInfoBysjfz(info.getCode());
//		// for (PoolingBlendInfo pfp : pooling_result) {
//		// // 根据富集文库查询富集明细样本信息
//		// List<PoolingTaskItem> ptl = this.sequencingDao
//		// .selectPoolingTaskItemByfjWkCode(pfp.getFjWkCode());
//		// for (PoolingTaskItem poolingTaskItem : ptl) {
//		// // 查询样本
//		// SampleInfo yangben = this.sequencingDao
//		// .selectSampleInfoCode(poolingTaskItem
//		// .getSampleCode());
//		// SampleOrder so = new SampleOrder();
//		// if (yangben.getOrderNum() != null)
//		// so = this.sequencingDao.get(SampleOrder.class,
//		// yangben.getOrderNum());
//		//
//		// // 查询电子病历
//		// CrmPatient cp = new CrmPatient();
//		// if (so.getMedicalNumber() != null) {
//		// cp = this.sequencingDao.get(CrmPatient.class,
//		// so.getMedicalNumber());
//		// }
//		//
//		// // 查询pooling
//		// PoolingTask ptk = this.sequencingDao.get(
//		// PoolingTask.class, poolingTaskItem
//		// .getPoolingTask().getId());
//		//
//		// StringBuffer sb = new StringBuffer();
//		// // 查询任务单的项目
//		// // sb.append("\"").append(yangben.getProject().getName())
//		// // .append("\",");// 科研项目编号
//		// sb.append("\"").append(cp.getId()).append("\",");// 电子病历编号
//		// sb.append("\"").append(cp.getAge()).append("\",");// 年龄
//		// sb.append("\"").append(cp.getGender()).append("\",");// 性别
//		// sb.append("\"").append(so.getDicType()).append("\",");// 癌症类型
//		// sb.append("\"").append(so.getSampleStage())
//		// .append("\",");// 癌症分期
//		// sb.append("\"").append(so.getId()).append("\",");// 订单编号
//		// sb.append("\"").append(so.getProductName())
//		// .append("\",");// 检测项目
//		// sb.append("\"").append(yangben.getCode()).append("\",");// 样本编号
//		// // 转换时间
//		// sb.append("\"").append("").append("\",");// 采样日期
//		// sb.append("\"").append(so.getSampleTypeName())
//		// .append("\",");// 样本类型
//		// sb.append("\"")
//		// .append(poolingTaskItem.getProbeCode()
//		// .getName()).append("\",");// 探针库
//		// sb.append("\"").append("").append("\",");// 标签
//		// sb.append("\"").append(sat.getId()).append("\",");// "上机任务单号"
//		// sb.append("\"").append(sat.getCreateDate())
//		// .append("\",");// "上机时间"
//		// sb.append("\"").append(sat.getDeviceCode().getName())
//		// .append("\",");// 上机机器号
//		// sb.append("\"").append(sat.getSpec()).append("\",");// 上机试剂盒
//		//
//		// String rowStr = sb.toString();
//		// csvWtriter1.write(rowStr);
//		// csvWtriter1.newLine();
//		// }
//		// }
//		// }
//		//
//		// }
//		//
//		// csvWtriter1.flush();
//		// csvWtriter1.close();
//		// InputStream is = new FileInputStream(csvFile.getPath());
//		// FtpUtil.uploadFile(listInfo.get(0).getSequencing().getFcCode(),
//		// csvFile.getName(), is);
//		// InputStream is1 = new FileInputStream(csvFile1.getPath());
//		// FtpUtil.uploadFile(listInfo.get(0).getSequencing().getFcCode(),
//		// csvFile1.getName(), is1);
//
//		return csvFile.getName();
//
//	}

	/**
	 * 明细入库
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void rukuSequencingTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingLifeTaskItem scp = sequencingLifeDao.get(
					SequencingLifeTaskItem.class, id);
			SampleInItemTemp st = new SampleInItemTemp();
			st.setCode(scp.getCode());
			st.setSampleCode(scp.getPoolingCode());
			Double num = Double.parseDouble(scp.getLaneCode())
					- Double.parseDouble(scp.getDataDemand());
			st.setNum(num);
			st.setInfoFrom("SequencingLifeTaskItem");
			st.setState("1");
			this.sequencingLifeDao.saveOrUpdate(st);
			scp.setState("1");
		}
	}
	
	public void submitSample(String id, String[] ids) throws Exception{
		SequencingLifeTask sct = sequencingLifeDao.get(SequencingLifeTask.class, id);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date createDate = format.parse(sct.getCreateDate());
		List<SampleSequencingLifeInfo> list = sequencingLifeDao.selectSeqInfoById(sct
				.getId());
		for (SampleSequencingLifeInfo info : list) {
			if (info.getResult().equals("1")) {// 合格
				if (info.getSampleCode() != null) {
					String[] sampleCode1 = info.getSampleCode().split(",");
					List<String> list2 = new ArrayList<String>();
					for(int i=0;i<sampleCode1.length;i++){
						for(int j=i+1;j<sampleCode1.length;j++){
							if(sampleCode1[i] == sampleCode1[j]){
								j = ++i;
							}
						}
						list2.add(sampleCode1[i]);
					}
					String[] sampleCode = (String[]) list2.toArray(new String[list2.size()]);
					for (int i = 0; i < sampleCode.length; i++) {
						SampleInfo s = this.sampleInfoMainDao
								.findSampleInfo(sampleCode[i]);
						if (s != null) {
							sampleStateService
									.saveSampleState(
											info.getPoolingCode(),
											s.getCode(),
											s.getProductId(),
											s.getProductName(),
											"",
											/*format.format(*/sct.getCreateDate()/*)*/,
											format.format(new Date()),
											"SequencingLifeTask",
											"上机测序",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											id, "下机质控", info.getResult(), null,
											null, null, null, null, null, null,
											null);
						}
					}
				}
			} else {// 不合格
				SequencingLifeAbnormal sa = new SequencingLifeAbnormal();
				sampleInputService.copy(sa, info);
				if (info.getSampleCode() != null) {
					String[] sampleCode1 = info.getSampleCode().split(",");
					List<String> list2 = new ArrayList<String>();
					for(int i=0;i<sampleCode1.length;i++){
						for(int j=i+1;j<sampleCode1.length;j++){
							if(sampleCode1[i] == sampleCode1[j]){
								j = ++i;
							}
						}
						list2.add(sampleCode1[i]);
					}
					String[] sampleCode = (String[]) list2.toArray(new String[list2.size()]);
					for (int i = 0; i < sampleCode.length; i++) {
						SampleInfo s = this.sampleInfoMainDao
								.findSampleInfo(sampleCode[i]);
						if (s != null) {
							sampleStateService
									.saveSampleState(
											info.getPoolingCode(),
											s.getCode(),
											s.getProductId(),
											s.getProductName(),
											"",
											sct.getCreateDate(),
											format.format(new Date()),
											"SequencingLifeTask",
											"上机测序",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											id, "上机测序异常", info.getResult(),
											null, null, null, null, null, null,
											null, null);
						}
					}
				}
			}

		}
	}
}
