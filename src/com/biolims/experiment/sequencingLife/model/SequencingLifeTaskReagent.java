package com.biolims.experiment.sequencingLife.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2015-11-29 16:40:27
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SEQUENCING_LIFE_TASK_REAGENT")
@SuppressWarnings("serial")
public class SequencingLifeTaskReagent extends EntityDao<SequencingLifeTaskReagent>
		implements java.io.Serializable {
	/** id */
	private String id;
	/** 试剂编号 */
	private String code;
	/** 描述 */
	private String name;
	/** 批次 */
	private String batch;
	/** 数量 */
	private Integer count;
	/** 是否通过检验 */
	private String isGood;
	/** 相关主表 */
	private SequencingLifeTask sequencing;
	// 单个用量
	private Double oneNum;
	// 样本数量
	private Double sampleNum;
	// 用量
	private Double num;
	// 关联步骤的id
	private String itemId;

	private String tReagent;
	// 备注
	private String note;
	// sn
	private String sn;
	// 过期日期
	private Date expireDate;
	// 试剂编码
	private String reagentCode;
	// 是否用完
	private String isRunout;

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getReagentCode() {
		return reagentCode;
	}

	public void setReagentCode(String reagentCode) {
		this.reagentCode = reagentCode;
	}

	public String getIsRunout() {
		return isRunout;
	}

	public void setIsRunout(String isRunout) {
		this.isRunout = isRunout;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批次
	 */
	@Column(name = "BATCH", length = 50)
	public String getBatch() {
		return this.batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批次
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 数量
	 */
	@Column(name = "COUNT", length = 50)
	public Integer getCount() {
		return this.count;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer 数量
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	/**
	 * 方法: 取得Sequencing
	 * 
	 * @return: Sequencing 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SEQUENCING")
	public SequencingLifeTask getSequencing() {
		return this.sequencing;
	}

	/**
	 * 方法: 设置Sequencing
	 * 
	 * @param: Sequencing 相关主表
	 */
	public void setSequencing(SequencingLifeTask sequencing) {
		this.sequencing = sequencing;
	}

	public Double getOneNum() {
		return oneNum;
	}

	public void setOneNum(Double oneNum) {
		this.oneNum = oneNum;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String gettReagent() {
		return tReagent;
	}

	public void settReagent(String tReagent) {
		this.tReagent = tReagent;
	}
}