package com.biolims.experiment.sequencingLife.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.model.Template;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * @Title: Model
 * @Description: 待上机测序样本
 * @author lims-platform
 * @date 2015-11-29 19:15:38
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SEQUENCING_LIFE_TASK_TEMP")
@SuppressWarnings("serial")
public class SequencingLifeTaskTemp extends EntityDao<SequencingLifeTaskTemp> implements
		java.io.Serializable {
	/** id */
	private String id;
	/* 混合POOLING编号 */
	private String poolingCode;
	/** 样本编号 */
	private String code;
	/** 文库信息 */
	private String name;
	/** 原始样本编号 */
	private String sampleCode;
	/** 混合日期 */
	private Date mixDate;
	/** 备注 */
	private String note;
	/** 状态ID */
	private String state;
	// /**状态*/
	// private String stateName;
	/** 测序类型 */
	private String sequenceType;
	/** 选择模板 */
	private Template template;
	/** 测序读长 */
	private String sequencingReadLong;
	/** 测序平台 */
	private String sequencingPlatform;
	/** 业务类型 */
	private Product product;
	/** 实验员 */
	private User reciveUser;
	/** 实验时间 */
	private Date reciveDate;
	/** 下达人 */
	private User createUser;
	/** 下达时间 */
	private String createDate;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/** 理论QPCR浓度 */
	private String qpcrConcentration;
	/** 预期密度 */
	private String density;
	/** 引物 */
	private String yw;
	/** 混合比例 */
	private Double mixRatio;

	/** 总数据量 */
	private Double totalAmount;
	/** 总体积 */
	private Double totalVolume;
	/** 实验组 */
	private UserGroup acceptUser;
	/** ReadsOne */
	private String readsOne;
	/** ReadsTwo */
	private String readsTwo;
	/** Index1 */
	private String index1;
	/** Index2 */
	private String index2;

	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	/** 科技服务任务单 */
	private String techTaskId;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// Lane号
	private String lane;
	// poolingId
	private String poolingId;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 是否质控品
	private String isZkp;
	// Lane数
	private String laneNum;

	public String getLaneNum() {
		return laneNum;
	}

	public void setLaneNum(String laneNum) {
		this.laneNum = laneNum;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	public String getPoolingId() {
		return poolingId;
	}

	public void setPoolingId(String poolingId) {
		this.poolingId = poolingId;
	}

	public String getLane() {
		return lane;
	}

	public void setLane(String lane) {
		this.lane = lane;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return poolingCode;
	}

	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String pooling编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String pooling编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "MIX_DATE", length = 50)
	public Date getMixDate() {
		return mixDate;
	}

	public void setMixDate(Date mixDate) {
		this.mixDate = mixDate;
	}

	@Column(name = "SEQUENCE_TYPE", length = 50)
	public String getSequenceType() {
		return sequenceType;
	}

	public void setSequenceType(String sequenceType) {
		this.sequenceType = sequenceType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	// @Column(name ="STATE_NAME", length = 50)
	// public String getStateName() {
	// return stateName;
	// }
	// public void setStateName(String stateName) {
	// this.stateName = stateName;
	// }
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		if(template!=null&&template.getId()!=null&&template.getId().equals(""))  template= null; this.template = template;
	}

	// 读长
	public String getSequencingReadLong() {
		return sequencingReadLong;
	}

	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}

	// 测序平台
	public String getSequencingPlatform() {
		return sequencingPlatform;
	}

	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}

	// 业务类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "RECIVE_USER")
	public User getReciveUser() {
		return this.reciveUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 实验员
	 */
	public void setReciveUser(User reciveUser) {
		if(reciveUser!=null&&reciveUser.getId()!=null&&reciveUser.getId().equals("")) reciveUser= null; this.reciveUser = reciveUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 实验时间
	 */
	@Column(name = "RECIVE_DATE", length = 50)
	public Date getReciveDate() {
		return this.reciveDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 实验时间
	 */
	public void setReciveDate(Date reciveDate) {
		this.reciveDate = reciveDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "ORDER_ID", length = 20)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public Double getMixRatio() {
		return mixRatio;
	}

	public void setMixRatio(Double mixRatio) {
		this.mixRatio = mixRatio;
	}

	public String getYw() {
		return yw;
	}

	public void setYw(String yw) {
		this.yw = yw;
	}

	public String getDensity() {
		return density;
	}

	public void setDensity(String density) {
		this.density = density;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}

	public void setAcceptUser(UserGroup acceptUser) {
		if(acceptUser!=null&&acceptUser.getId()!=null&&acceptUser.getId().equals("")) acceptUser= null; this.acceptUser = acceptUser;
	}

	public String getReadsOne() {
		return readsOne;
	}

	public void setReadsOne(String readsOne) {
		this.readsOne = readsOne;
	}

	public String getReadsTwo() {
		return readsTwo;
	}

	public void setReadsTwo(String readsTwo) {
		this.readsTwo = readsTwo;
	}

	public String getIndex1() {
		return index1;
	}

	public void setIndex1(String index1) {
		this.index1 = index1;
	}

	public String getIndex2() {
		return index2;
	}

	public void setIndex2(String index2) {
		this.index2 = index2;
	}

	public String getQpcrConcentration() {
		return qpcrConcentration;
	}

	public void setQpcrConcentration(String qpcrConcentration) {
		this.qpcrConcentration = qpcrConcentration;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

}