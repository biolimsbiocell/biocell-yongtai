package com.biolims.experiment.sequencingLife.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 执行步骤
 * @author lims-platform
 * @date 2015-11-29 16:40:24
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SEQUENCING_LIFE_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class SequencingLifeTaskTemplate extends EntityDao<SequencingLifeTaskTemplate> implements java.io.Serializable {
	/**id*/
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	private String id;
	/**描述*/
	private String name;
	/**步骤编号*/
	private Integer stepNum;
	/**步骤名称*/
	private String stepName;
	/**步骤描述*/
	@Column(name ="DESCRIBE_", length = 50)
	private String describe;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**备注*/
	private String note;
	/*
	 * Item id
	 */
	@Column(name ="T_ITEM_NEW", length = 50)
	private String tItem;
	
	//实验员
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TEST_USER")
	private User testUser;
	//状态
	private String state;
	//状态
	private String stateName;
	/**相关主表*/
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SEQUENCING")
	private SequencingLifeTask sequencing;
	/**相关样本*/
	private String sampleCodes;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}

	public Integer getStepNum() {
		return stepNum;
	}
	public void setStepNum(Integer stepNum) {
		this.stepNum = stepNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="STEP_NAME", length = 50)
	public String getStepName(){
		return this.stepName;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setStepName(String stepName){
		this.stepName = stepName;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤描述
	 */

	public String getDescribe(){
		return this.describe;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤描述
	 */
	public void setDescribe(String describe){
		this.describe = describe;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始时间
	 */
	@Column(name ="START_TIME", length = 50)
	public String getStartTime(){
		return this.startTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束时间
	 */
	@Column(name ="END_TIME", length = 50)
	public String getEndTime(){
		return this.endTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE",  columnDefinition="clob")
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得Sequencing
	 *@return: Sequencing  相关主表
	 */
	
	public SequencingLifeTask getSequencing(){
		return this.sequencing;
	}
	/**
	 *方法: 设置Sequencing
	 *@param: Sequencing  相关主表
	 */
	public void setSequencing(SequencingLifeTask sequencing){
		this.sequencing = sequencing;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */

	public String gettItem() {
		return tItem;
	}
	public void settItem(String tItem) {
		this.tItem = tItem;
	}
	
	@Column(name="STATE",length=20)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public User getTestUser() {
		return testUser;
	}
	public void setTestUser(User testUser) {
		this.testUser = testUser;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getSampleCodes() {
		return sampleCodes;
	}
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
}