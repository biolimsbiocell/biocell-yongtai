package com.biolims.experiment.sequencingLife.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;


import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
/**   
 * @Title: Model
 * @Description: 设备明细
 * @author lims-platform
 * @date 2015-11-29 16:40:30
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SEQUENCING_LIFE_TASK_COS")
@SuppressWarnings("serial")
public class SequencingLifeTaskCos extends EntityDao<SequencingLifeTaskCos> implements java.io.Serializable {
	/**id*/
	private String id;
	/**设备编号*/
	private String code;
	/**名称*/
	private String name;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/**是否通过检测*/
	private String isGood;
	/**相关主表*/
	private SequencingLifeTask sequencing;
	//关联步骤的id
	private String itemId;
	//温度
	private Double temperature;
	//转速
	private String speed;
	//时间
	private Double time;
	/*
	 * Cos id
	 */
	private String tCos;
	/** 设备类型*/
	private DicType type;

	/**
	 * @return the type
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TYPE")
	public DicType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(DicType type) {
		this.type = type;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  设备编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  设备编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否通过检测
	 */
	@Column(name ="IS_GOOD", length = 50)
	public String getIsGood(){
		return this.isGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否通过检测
	 */
	public void setIsGood(String isGood){
		this.isGood = isGood;
	}
	/**
	 *方法: 取得Sequencing
	 *@return: Sequencing  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SEQUENCING")
	public SequencingLifeTask getSequencing(){
		return this.sequencing;
	}
	/**
	 *方法: 设置Sequencing
	 *@param: Sequencing  相关主表
	 */
	public void setSequencing(SequencingLifeTask sequencing){
		this.sequencing = sequencing;
	}
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public Double getTime() {
		return time;
	}
	public void setTime(Double time) {
		this.time = time;
	}
	public String gettCos() {
		return tCos;
	}
	public void settCos(String tCos) {
		this.tCos = tCos;
	}
	/**
	 * @return the state
	 */
	@Column(name ="STATE", length = 50)
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	
}