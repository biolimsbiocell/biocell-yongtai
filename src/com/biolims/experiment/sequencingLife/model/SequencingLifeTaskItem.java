package com.biolims.experiment.sequencingLife.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 测序明细
 * @author lims-platform
 * @date 2015-11-29 16:40:19
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SEQUENCING_LIFE_TASK_ITEM")
@SuppressWarnings("serial")
public class SequencingLifeTaskItem extends EntityDao<SequencingLifeTaskItem> implements
		java.io.Serializable {
	/** id */
	private String id;
	/* 序号 */
	private Integer orderNumber;
	/** 描述 */
	private String name;
	/** 混合POOLING编号 */
	private String poolingCode;
	/* 样本编号 */
	private String code;
	/* 原始样本编号 */
	private String sampleCode;
	/* LANE号 */
	private String laneCode;
	/* 上机定量 */
	private String quantity;
	/* 预期密度 */
	private String density;
	/* 数据需求量 */
	private String dataDemand;
	/* 检测项目 */
	private String productId;
	// 测序平台
	private String sequencingPlatform;
	// 测序读长
	private String sequencingReadLong;
	// 测序类型
	private String sequenceType;
	/* 检测项目 */
	private String productName;
	/** 是否合格 */
	private String result;

	private String tempId;
	/** 失败原因 */

	private String reason;
	/** 备注 */
	private String note;
	/** 相关实验步骤 */
	private String stepNum;
	/* 患者姓名 */
	private String patientName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/** 相关主表 */
	private SequencingLifeTask sequencing;
	/** 状态 */
	private String stateName;
	/** 状态id */
	private String state;
	// 下调
	private String cut;
	// 取样（20pM文库取样体积）
	private String sample;
	// 加水（HT1补加体积）
	private String addWater;
	// 调至
	private String toShift;
	// 上调
	private String raise;
	// 干燥
	private String dry;
	// 原始质量浓度（PhiX添加比例）
	private String yszlnd;
	// 调后质量浓度
	private String thzlnd;
	// 原始摩尔浓度
	private String ysmend;
	// 调后摩尔浓度
	private String thmend;
	// poolingId
	private String poolingId;
//Life测序明细
	//Run号
	private String runCode;
	//芯片条码
	private String chipBarcode;
	//Proton编号
	private String protonNumber;
	//Proton状态
	private String protonState;
	//Proton氯洗
	private String protonClWash;
	//Proton水洗
	private String protonWash;
	//Proton初始化
	private String protonInit;
	
	public String getPoolingId() {
		return poolingId;
	}

	public void setPoolingId(String poolingId) {
		this.poolingId = poolingId;
	}

	public String getRaise() {
		return raise;
	}

	public void setRaise(String raise) {
		this.raise = raise;
	}

	public String getDry() {
		return dry;
	}

	public void setDry(String dry) {
		this.dry = dry;
	}

	public String getYszlnd() {
		return yszlnd;
	}

	public void setYszlnd(String yszlnd) {
		this.yszlnd = yszlnd;
	}

	public String getThzlnd() {
		return thzlnd;
	}

	public void setThzlnd(String thzlnd) {
		this.thzlnd = thzlnd;
	}

	public String getYsmend() {
		return ysmend;
	}

	public void setYsmend(String ysmend) {
		this.ysmend = ysmend;
	}

	public String getThmend() {
		return thmend;
	}

	public void setThmend(String thmend) {
		this.thmend = thmend;
	}

	public String getCut() {
		return cut;
	}

	public void setCut(String cut) {
		this.cut = cut;
	}

	public String getSample() {
		return sample;
	}

	public void setSample(String sample) {
		this.sample = sample;
	}

	public String getAddWater() {
		return addWater;
	}

	public void setAddWater(String addWater) {
		this.addWater = addWater;
	}

	public String getToShift() {
		return toShift;
	}

	public void setToShift(String toShift) {
		this.toShift = toShift;
	}

	public String getRunCode() {
		return runCode;
	}

	public void setRunCode(String runCode) {
		this.runCode = runCode;
	}

	public String getChipBarcode() {
		return chipBarcode;
	}

	public void setChipBarcode(String chipBarcode) {
		this.chipBarcode = chipBarcode;
	}

	public String getProtonNumber() {
		return protonNumber;
	}

	public void setProtonNumber(String protonNumber) {
		this.protonNumber = protonNumber;
	}

	public String getProtonState() {
		return protonState;
	}

	public void setProtonState(String protonState) {
		this.protonState = protonState;
	}

	public String getProtonClWash() {
		return protonClWash;
	}

	public void setProtonClWash(String protonClWash) {
		this.protonClWash = protonClWash;
	}

	public String getProtonWash() {
		return protonWash;
	}

	public void setProtonWash(String protonWash) {
		this.protonWash = protonWash;
	}

	public String getProtonInit() {
		return protonInit;
	}

	public void setProtonInit(String protonInit) {
		this.protonInit = protonInit;
	}

	@Column(name = "QPCR_ND", length = 32)
	private String qpcrNd;// QPCR浓度

	@Column(name = "UP_CODE", length = 32)
	private String upNd;// 上机浓度

	@Column(name = "YW", length = 32)
	private String yw;// 引物

	@Column(name = "PREPARE_ND", length = 32)
	private String prepareNd;// 预期密度

	@Column(name = "QUANTITATIVE_METHOD", length = 200)
	private String quantitativeMethod;// 定量方法

	// 稀释管信息
	@Column(name = "DILUTION_ONE", length = 20)
	private String dilutionOne;// 稀释信息1
	@Column(name = "DILUTION_TWO", length = 20)
	private String dilutionTwo;// 稀释信息2
	@Column(name = "DILUTION_THREE", length = 20)
	private String dilutionThree;// 稀释信息3
	@Column(name = "DILUTION_FOUR", length = 20)
	private String dilutionFour;// 稀释信息4

	// 1ml稀释管信息
	@Column(name = "BXYBL")
	private String bxybl;// 变性样本量
	@Column(name = "hyb")
	private String hyb;// Hyb

	// 变性管信息
	@Column(name = "BXH_ND", length = 20)
	private String bxhnd;// 变性后浓度
	@Column(name = "NAOH_TAKE", length = 20)
	private String naOhTake;// NaOH
	@Column(name = "DNA_TAKE", length = 20)
	private String dnaTake;// DNA
	@Column(name = "HYB_TAKE", length = 20)
	private String hybTake;// hyb取用量

	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	/** 科技服务任务单 */
	private String techTaskId;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ORDER_NUMBER", length = 20)
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return poolingCode;
	}

	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}

	@Column(name = "CODE", length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "LANE_CODE", length = 50)
	public String getLaneCode() {
		return laneCode;
	}

	public void setLaneCode(String laneCode) {
		this.laneCode = laneCode;
	}

	@Column(name = "QUANTITY", length = 50)
	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Column(name = "DENSITY", length = 50)
	public String getDensity() {
		return density;
	}

	public void setDensity(String density) {
		this.density = density;
	}

	@Column(name = "DATA_DEMAND", length = 50)
	public String getDataDemand() {
		return dataDemand;
	}

	public void setDataDemand(String dataDemand) {
		this.dataDemand = dataDemand;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否合格
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否合格
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得Sequencing
	 * 
	 * @return: Sequencing 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SEQUENCING")
	public SequencingLifeTask getSequencing() {
		return this.sequencing;
	}

	/**
	 * 方法: 设置Sequencing
	 * 
	 * @param: Sequencing 相关主表
	 */
	public void setSequencing(SequencingLifeTask sequencing) {
		this.sequencing = sequencing;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法：获取String
	 * 
	 * @return 相关步骤编号
	 */
	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return stepNum;
	}

	/**
	 * 方法：设置String
	 * 
	 * @return 相关步骤编号
	 */
	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "ORDER_ID", length = 20)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getQpcrNd() {
		return qpcrNd;
	}

	public void setQpcrNd(String qpcrNd) {
		this.qpcrNd = qpcrNd;
	}

	public String getUpNd() {
		return upNd;
	}

	public void setUpNd(String upNd) {
		this.upNd = upNd;
	}

	public String getYw() {
		return yw;
	}

	public void setYw(String yw) {
		this.yw = yw;
	}

	public String getPrepareNd() {
		return prepareNd;
	}

	public void setPrepareNd(String prepareNd) {
		this.prepareNd = prepareNd;
	}

	public String getQuantitativeMethod() {
		return quantitativeMethod;
	}

	public void setQuantitativeMethod(String quantitativeMethod) {
		this.quantitativeMethod = quantitativeMethod;
	}

	public String getDilutionOne() {
		return dilutionOne;
	}

	public void setDilutionOne(String dilutionOne) {
		this.dilutionOne = dilutionOne;
	}

	public String getDilutionTwo() {
		return dilutionTwo;
	}

	public void setDilutionTwo(String dilutionTwo) {
		this.dilutionTwo = dilutionTwo;
	}

	public String getDilutionThree() {
		return dilutionThree;
	}

	public void setDilutionThree(String dilutionThree) {
		this.dilutionThree = dilutionThree;
	}

	public String getDilutionFour() {
		return dilutionFour;
	}

	public void setDilutionFour(String dilutionFour) {
		this.dilutionFour = dilutionFour;
	}

	public String getBxybl() {
		return bxybl;
	}

	public void setBxybl(String bxybl) {
		this.bxybl = bxybl;
	}

	public String getHyb() {
		return hyb;
	}

	public void setHyb(String hyb) {
		this.hyb = hyb;
	}

	public String getBxhnd() {
		return bxhnd;
	}

	public void setBxhnd(String bxhnd) {
		this.bxhnd = bxhnd;
	}

	public String getNaOhTake() {
		return naOhTake;
	}

	public void setNaOhTake(String naOhTake) {
		this.naOhTake = naOhTake;
	}

	public String getDnaTake() {
		return dnaTake;
	}

	public void setDnaTake(String dnaTake) {
		this.dnaTake = dnaTake;
	}

	public String getHybTake() {
		return hybTake;
	}

	public void setHybTake(String hybTake) {
		this.hybTake = hybTake;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSequencingPlatform() {
		return sequencingPlatform;
	}

	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}

	public String getSequencingReadLong() {
		return sequencingReadLong;
	}

	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}

	public String getSequenceType() {
		return sequenceType;
	}

	public void setSequenceType(String sequenceType) {
		this.sequenceType = sequenceType;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

}