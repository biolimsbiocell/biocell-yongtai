﻿package com.biolims.experiment.sequencingLife.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;

import com.biolims.experiment.sequencingLife.model.SampleSequencingLifeInfo;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTask;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskCos;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskItem;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskReagent;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemp;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemplate;
import com.biolims.experiment.sequencingLife.service.SequencingLifeService;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/sequencingLife")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SequencingLifeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250902";
	@Autowired
	private SequencingLifeService sequencingLifeService;
	public SequencingLifeService getSequencingLifeService() {
		return sequencingLifeService;
	}

	public void setSequencingLifeService(SequencingLifeService sequencingLifeService) {
		this.sequencingLifeService = sequencingLifeService;
	}

	private SequencingLifeTask sequencing = new SequencingLifeTask();
	public SequencingLifeTask getSequencing() {
		return sequencing;
	}

	public void setSequencing(SequencingLifeTask sequencing) {
		this.sequencing = sequencing;
	}

	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;

	@Action(value = "showSequencingList")
	public String showSequencingList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLife.jsp");
	}

	// editSequencing
	@Action(value = "editSequencingList")
	public String editSequencingList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLife.jsp");
	}

	// ============
	@Action(value = "getSequencingList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getSequencingList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sequencingLifeService
					.getSequencingItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// ============

	@Action(value = "showSequencingListJson")
	public void showSequencingListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String flag = getParameterFromRequest("flag");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingLifeService.findSequencingList(
				map2Query, startNum, limitNum, dir, sort, flag);
		Long count = (Long) result.get("total");
		List<SequencingLifeTask> list = (List<SequencingLifeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("fcCode", "");
		map.put("refCode", "");
		map.put("spec", "");
		map.put("refName", "");
		map.put("reagent", "");
		map.put("expectDate", "yyyy-MM-dd");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("deviceCode-id", "");
		map.put("deviceCode-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("type", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		//Life上机
		map.put("keySignal", "");
		map.put("ispLoding", "");
		map.put("totalReads", "");
		map.put("polyclonal", "");
		map.put("lowQuality", "");
		map.put("readLength", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "sequencingSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSequencingList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		String flag = getRequest().getParameter("code");
		putObjToContext("flag", flag);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeDialog.jsp");
	}

	@Action(value = "showDialogSequencingListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSequencingListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String flag = getParameterFromRequest("flag");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingLifeService.findSequencingList(
				map2Query, startNum, limitNum, dir, sort, flag);
		Long count = (Long) result.get("total");
		List<SequencingLifeTask> list = (List<SequencingLifeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSequencing")
	public String editSequencing() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sequencing = sequencingLifeService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sequencing");
		} else {
			sequencing.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sequencing.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			sequencing.setCreateDate(stime);
			sequencing.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sequencing.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(sequencing.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeEdit.jsp");
	}

	@Action(value = "copySequencing")
	public String copySequencing() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sequencing = sequencingLifeService.get(id);
		sequencing.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = sequencing.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SequencingLifeTask";
			String markCode = "SJL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sequencing.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("sequencingItem",
				getParameterFromRequest("sequencingItemJson"));

		aMap.put("sequencingTemplate",
				getParameterFromRequest("sequencingTemplateJson"));

		aMap.put("sequencingReagent",
				getParameterFromRequest("sequencingReagentJson"));

		aMap.put("sequencingCos", getParameterFromRequest("sequencingCosJson"));

		aMap.put("sequencingResult",
				getParameterFromRequest("sequencingResultJson"));

		sequencingLifeService.save(sequencing, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/sequencingLife/editSequencing.action?id="
				+ sequencing.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return redirect("/experiment/sequencing/editSequencing.action?id="
		// + sequencing.getId());

	}
	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String modifyInfo = getParameterFromRequest("modifyInfo");
			sequencing = commonService.get(SequencingLifeTask.class, id);

			Map aMap = new HashMap();
			aMap.put("sequencingItem",
					getParameterFromRequest("sequencingItemJson"));

			aMap.put("sequencingResult",
					getParameterFromRequest("sequencingResultJson"));

			aMap.put("sequencingTemplate",
					getParameterFromRequest("sequencingTemplateJson"));
			aMap.put("sequencingReagent",
					getParameterFromRequest("sequencingReagentJson"));
			aMap.put("sequencingCos",
					getParameterFromRequest("sequencingCosJson"));
			aMap.put("modifyInfo", modifyInfo);
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			aMap.put("userId", user.getId());
			map = sequencingLifeService.save(sequencing, aMap);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewSequencing")
	public String toViewSequencing() throws Exception {
		String id = getParameterFromRequest("id");
		sequencing = sequencingLifeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeEdit.jsp");
	}

	@Action(value = "showSequencingItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeItem.jsp");
	}

	@Action(value = "showSequencingItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sequencingLifeService
					.findSequencingItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SequencingLifeTaskItem> list = (List<SequencingLifeTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderNumber", "");
			map.put("name", "");
			map.put("code", "");
			map.put("poolingCode", "");
			map.put("sampleCode", "");
			map.put("laneCode", "");
			map.put("quantity", "");
			map.put("density", "");
			map.put("dataDemand", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("stepNum", "");
			map.put("patientName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("sequenceType", "");
			map.put("sequencingReadLong", "");
			map.put("sequencingPlatform", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("sequencing-name", "");
			map.put("sequencing-id", "");
			map.put("stateName", "");
			map.put("state", "");
			map.put("tempId", "");
			map.put("qpcrNd", "");
			map.put("upNd", "");
			map.put("yw", "");
			map.put("prepareNd", "");
			map.put("quantitativeMethod", "");
			map.put("dilutionOne", "");
			map.put("dilutionTwo", "");
			map.put("dilutionThree", "");
			map.put("dilutionFour", "");

			map.put("bxybl", "");
			map.put("hyb", "");
			map.put("bxhnd", "");
			map.put("naOhTake", "");
			map.put("dnaTake", "");
			map.put("hybTake", "");

			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("techTaskId", "");
			map.put("classify", "");
			map.put("cut", "");
			map.put("sample", "");
			map.put("addWater", "");
			map.put("toShift", "");

			map.put("raise", "");
			map.put("dry", "");
			map.put("yszlnd", "");
			map.put("thzlnd", "");
			map.put("ysmend", "");
			map.put("thmend", "");
			map.put("poolingId", "");
			
			map.put("runCode", "");
			map.put("chipBarcode", "");
			map.put("protonNumber", "");
			map.put("protonState", "");
			map.put("protonClWash", "");
			map.put("protonWash", "");
			map.put("protonInit", "");
			
			
			
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingItem")
	public void delSequencingItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sequencingLifeService.delSequencingItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 加载模板
	@Action(value = "showSequencingTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/showSeqLTemplateWaitList.jsp");
	}

	@Action(value = "showSequencingTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sequencingLifeService
					.findSequencingTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SequencingLifeTaskTemplate> list = (List<SequencingLifeTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("stepNum", "");
			map.put("stepName", "");
			map.put("describe", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("note", "");
			map.put("sequencing-name", "");
			map.put("sequencing-id", "");
			map.put("tItem", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCodes", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showSequencingTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeTemplate.jsp");
	}

	@Action(value = "showSequencingTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sequencingLifeService
					.findSequencingTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SequencingLifeTaskTemplate> list = (List<SequencingLifeTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("stepNum", "");
			map.put("stepName", "");
			map.put("describe", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("note", "");
			map.put("sequencing-name", "");
			map.put("sequencing-id", "");
			map.put("tItem", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCodes", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingTemplate")
	public void delSequencingTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sequencingLifeService.delSequencingTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSequencingReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingReagentList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeReagent.jsp");
	}

	@Action(value = "showSequencingReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			// String itemId = getParameterFromRequest("itemId");
			Map<String, Object> result = new HashMap<String, Object>();
			// if(itemId.equals("")){//如果步骤为空 加载默认
			result = sequencingLifeService.findSequencingReagentList(scId,
					startNum, limitNum, dir, sort);
			// }else{//如果步骤不为空 根据步骤加载试剂
			// result =
			// sequencingService.findSequencingReagentListByItemId(scId,
			// startNum, limitNum, dir, sort, itemId);
			// }
			Long total = (Long) result.get("total");
			List<SequencingLifeTaskReagent> list = (List<SequencingLifeTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("isGood", "");
			map.put("sequencing-name", "");
			map.put("sequencing-id", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("note", "");
			map.put("sn", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("reagentCode", "");
			map.put("isRunout", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingReagent")
	public void delSequencingReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sequencingLifeService.delSequencingReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSequencingCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingCosList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeCos.jsp");
	}

	@Action(value = "showSequencingCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			// String itemId = getParameterFromRequest("itemId");
			Map<String, Object> result = new HashMap<String, Object>();
			// if(itemId.equals("")){
			result = sequencingLifeService.findSequencingCosList(scId, startNum,
					limitNum, dir, sort);
			// }else{
			// result =
			// sequencingService.findSequencingCosItemListByItemId(scId,
			// startNum, limitNum, dir, sort, itemId);
			// }
			Long total = (Long) result.get("total");
			List<SequencingLifeTaskCos> list = (List<SequencingLifeTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("state", "");
			map.put("name", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("sequencing-name", "");
			map.put("sequencing-id", "");

			map.put("itemId", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingCos")
	public void delSequencingCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sequencingLifeService.delSequencingCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSequencingResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeResult.jsp");
	}

	@Action(value = "showSequencingResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sequencingLifeService
					.findSequencingResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleSequencingLifeInfo> list = (List<SampleSequencingLifeInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fcCode", "");
			map.put("laneCode", "");
			map.put("machineCode", "");
			map.put("poolingCode", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("code", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("patientName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("state", "");
			map.put("molarity", "");
			map.put("quantity", "");
			map.put("concentration", "");
			map.put("computerDate", "yyyy-MM-dd");
			map.put("expectDate", "yyyy-MM-dd");
			map.put("pfPercent", "");
			map.put("sequencing-name", "");
			map.put("sequencing-id", "");
			map.put("dataDemand", "");

			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("techTaskId", "");
			map.put("classify", "");
			map.put("poolingId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingResult")
	public void delSequencingResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sequencingLifeService.delSequencingResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 待上机测序样本
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSequencingTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSequencingTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeTemp.jsp");
	}

	@Action(value = "showSequencingTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSequencingTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingLifeService.findSequencingTempList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SequencingLifeTaskTemp> list = (List<SequencingLifeTaskTemp>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("poolingCode", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("mixDate", "yyyy-MM-dd");
		map.put("qpcrConcentration", "");
		map.put("note", "");
		map.put("state", "");
		map.put("sequenceType", "");
		map.put("template-id", "");
		map.put("template-name", "");

		map.put("sequencingReadLong", "");
		map.put("sequencingPlatform", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("reciveUser-id", "");

		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");

		map.put("reportDate", "yyyy-MM-dd");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "yyyy-MM-dd");

		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("orderId", "");
		map.put("phone", "");
		map.put("sequenceFun", "");

		map.put("reportDate", "yyyy-MM-dd");
		map.put("density", "");// 预期密度
		map.put("yw", "");// 引物
		map.put("mixRatio", "");// 混合比例
		map.put("totalAmount", "");

		map.put("totalVolume", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("readsOne", "");
		map.put("readsTwo", "");

		map.put("index1", "");
		map.put("index2", "");

		map.put("contractId", "");
		map.put("projectId", "");
		map.put("orderType", "");
		map.put("techTaskId", "");
		map.put("classify", "");
		map.put("lane", "");
		map.put("poolingId", "");
		map.put("techJkServiceTask-id", "");
		map.put("techJkServiceTask-name", "");
		map.put("techJkServiceTask-sequenceBillDate", "yyyy-MM-dd");
		map.put("isZkp", "");
		map.put("laneNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// =============2015-12-02 ly=================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingTemplateOne")
	public void delSequencingTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			sequencingLifeService.delSequencingTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ==============================
	// ==============2015-12-02 ly===================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingReagentOne")
	public void delSequencingReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			sequencingLifeService.delSequencingReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// =================================
	// ============2015-12-02 ly================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSequencingCosOne")
	public void delSequencingCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			sequencingLifeService.delSequencingCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ============================

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	

	

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sequencingLifeService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sequencingLifeService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 明细入库信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "rukuSequencingTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void rukuSequencingTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sequencingLifeService.rukuSequencingTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 生成上机测序，测序明细，测序结果的Excel
	 * 
	 * @throws Exception
	 */
	/*
	 * @Action(value = "createExcel") public void createExcel() throws Exception
	 * { String taskId = getRequest().getParameter("taskId"); Map<String,
	 * Object> map = new HashMap<String, Object>(); try { String ids =
	 * sequencingService.createCSV(taskId); map.put("success", true); if (ids !=
	 * null) { map.put("fileId", ids); } else { map.put("fileId", ""); } } catch
	 * (Exception e) { e.printStackTrace(); map.put("error", false); }
	 * 
	 * HttpUtils.write(JsonUtils.toJsonString(map)); }
	 */
	// 查询科研样本
	@Action(value = "selTechJkServicTaskByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selTechJkServicTaskByid() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, String> map = new HashMap<String, String>();
		map.put("techJkServiceTask.id", id);
		Map<String, Object> map2 = sequencingLifeService.findSequencingTempList(
				map, null, null, null, null);
		List<SequencingLifeTaskTemp> list = (List<SequencingLifeTaskTemp>) map2
				.get("list");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
