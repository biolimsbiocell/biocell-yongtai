﻿package com.biolims.experiment.sequencingLife.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;

import com.biolims.experiment.sequencingLife.model.SequencingLifeAbnormal;
import com.biolims.experiment.sequencingLife.service.SequencingLifeAbnormalService;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackSequencing;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/sequencingLife/sequencingAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SequencingLifeAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250903";
	@Autowired
	private SequencingLifeAbnormalService sequencingLifeAbnormalService;
	

	public SequencingLifeAbnormalService getSequencingLifeAbnormalService() {
		return sequencingLifeAbnormalService;
	}

	public void setSequencingLifeAbnormalService(
			SequencingLifeAbnormalService sequencingLifeAbnormalService) {
		this.sequencingLifeAbnormalService = sequencingLifeAbnormalService;
	}

	private SequencingLifeAbnormal sequencingAbnormal = new SequencingLifeAbnormal();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showSequencingAbnormalList")
	public String showSequencingAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeAbnormal.jsp");
	}

	@Action(value = "showSequencingAbnormalListJson")
	public void showSequencingAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingLifeAbnormalService 
				.findSequencingAbnormalList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<SequencingLifeAbnormal> list = (List<SequencingLifeAbnormal>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("poolingCode", "");
		map.put("mixVolume", "");
		map.put("fcCode", "");
		map.put("laneCode", "");
		map.put("machineNum", "");
		map.put("sequencingDate", "yyyy-MM-dd");
		map.put("result", "");
		map.put("isExecute", "");
		map.put("method", "");
		map.put("sequencing-name", "");
		map.put("sequencing-id", "");
		map.put("state", "");
		map.put("note", "");
		map.put("idCard", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "sequencingAbnormalSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSequencingAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeAbnormalDialog.jsp");
	}

	@Action(value = "showDialogSequencingAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSequencingAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sequencingLifeAbnormalService
				.findSequencingAbnormalList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<SequencingLifeAbnormal> list = (List<SequencingLifeAbnormal>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("mixVolume", "");
		map.put("fcCode", "");
		map.put("laneCode", "");
		map.put("machineNum", "");
		map.put("sequencingDate", "yyyy-MM-dd");
		map.put("density", "");
		map.put("pf", "");
		map.put("resultDecide", "");
		map.put("flowStep", "");
		map.put("opinion", "");
		map.put("confirm", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSequencingAbnormal")
	public String editSequencingAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sequencingAbnormal = sequencingLifeAbnormalService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sequencingAbnormal");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// sequencingAbnormal.setCreateUser(user);
			// sequencingAbnormal.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/sequencing/sequencingLifeAbnormalEdit.jsp");
	}

	@Action(value = "copySequencingAbnormal")
	public String copySequencingAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sequencingAbnormal = sequencingLifeAbnormalService.get(id);
		sequencingAbnormal.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeAbnormalEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = sequencingAbnormal.getId();
		if (id != null && id.equals("")) {
			sequencingAbnormal.setId(null);
		}
		Map aMap = new HashMap();
		sequencingLifeAbnormalService.save(sequencingAbnormal, aMap);
		return redirect("/experiment/sequencingLife/sequencingAbnormal/editSequencingAbnormal.action?id="
				+ sequencingAbnormal.getId());

	}

	@Action(value = "viewSequencingAbnormal")
	public String toViewSequencingAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		sequencingAbnormal = sequencingLifeAbnormalService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeAbnormalEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	


	// 保存上机异常
	@Action(value = "saveSequencingAbnormal", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSequencingAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			sequencingLifeAbnormalService.saveSequencingAbnormalList(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public SequencingLifeAbnormal getSequencingAbnormal() {
		return sequencingAbnormal;
	}

	public void setSequencingAbnormal(SequencingLifeAbnormal sequencingAbnormal) {
		this.sequencingAbnormal = sequencingAbnormal;
	}
}
