
package com.biolims.experiment.sequencingLife.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;


import com.biolims.experiment.sequencingLife.model.SequencingLifeAbnormal;
import com.biolims.experiment.sequencingLife.service.SequencingLifeRepeatService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/sequencingLife/repeat")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SequencingLifeRepeatAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250201";
	@Autowired
	private SequencingLifeRepeatService sequencingLifeRepeatService;
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showSequencingRepeatList")
	public String showSequencingRepeatList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sequencingLife/sequencingLifeRepeat.jsp");
	}

	@Action(value = "showSequencingRepeatListJson")
	public void showSequencingRepeatListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		else
			map2Query.put("state", "is##@@##null"); 
		Map<String, Object> result =this.getSequencingLifeRepeatService().findSequencingRepeatList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SequencingLifeAbnormal> list = (List<SequencingLifeAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("poolingCode", "");
		map.put("mixVolume", "");
		map.put("fcCode", "");
		map.put("laneCode", "");
		map.put("machineNum", "");
		map.put("sequencingDate", "yyyy-MM-dd");
		map.put("density", "");
		map.put("pf", "");
		map.put("sequencing-id", "");
		map.put("sequencing-name", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("isExecute", "");
		map.put("state", "");
		map.put("note", "");
		map.put("method", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "");
		map.put("classify", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	@Action(value = "saveSequencingRepeat", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSequencingRepeat() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			SequencingLifeAbnormal a = new SequencingLifeAbnormal();
			sequencingLifeRepeatService.saveSequencingRepeat(a, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	public SequencingLifeRepeatService getSequencingLifeRepeatService() {
		return sequencingLifeRepeatService;
	}

	public void setSequencingLifeRepeatService(
			SequencingLifeRepeatService sequencingLifeRepeatService) {
		this.sequencingLifeRepeatService = sequencingLifeRepeatService;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}


}
