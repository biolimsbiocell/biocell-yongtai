package com.biolims.experiment.sequencingLife.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;

import com.biolims.experiment.sequencingLife.model.SequencingLifeAbnormal;


@Repository
@SuppressWarnings("unchecked")
public class SequencingLifeRepeatDao extends BaseHibernateDao {
	/*public Map<String, Object> selectSequencingRepeatList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) {
		String key = " ";
		String hql = " from SampleSequencingInfo where 1=1 and nextFlow = 1";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleSequencingInfo> list = new ArrayList<SampleSequencingInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}*/
	public Map<String, Object> selectSampleSequencingInfo(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
	String hql = "from SequencingLifeAbnormal t where 1=1";
	String key = "";
	if (mapForQuery.size()>0){
		key = map2where(mapForQuery);
	}else{
		key="";
	}
	Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
	List<SequencingLifeAbnormal> list = new ArrayList<SequencingLifeAbnormal>();
	if (total > 0) {
		if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
			if (sort.indexOf("-") != -1) {
				sort = sort.substring(0, sort.indexOf("-"));
			}
			key = key + " order by " + sort + " " + dir;
		} 
		if (startNum == null || limitNum == null) {
			list = this.getSession().createQuery(hql + key).list();
		} else {
			list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
	}
	Map<String, Object> result = new HashMap<String, Object>();
	result.put("total", total);
	result.put("list", list);
	return result;
}
}