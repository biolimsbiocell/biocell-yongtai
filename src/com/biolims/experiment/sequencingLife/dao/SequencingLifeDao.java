package com.biolims.experiment.sequencingLife.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.pooling.model.Pooling;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.sequencingLife.model.SampleSequencingLifeInfo;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTask;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskCos;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskItem;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskReagent;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemp;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemplate;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.sample.model.SampleInfo;

@Repository
@SuppressWarnings("unchecked")
public class SequencingLifeDao extends BaseHibernateDao {
	public Map<String, Object> selectSequencingList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String flag) {
		String key = " ";
		String hql = " ";
		if (flag.equals("100")) {
			hql = " from SequencingLifeTask where 1=1 and stateName = '完成' ";
		} else {
			hql = " from SequencingLifeTask where 1=1 ";
		}
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTask> list = new ArrayList<SequencingLifeTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSequencingItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SequencingLifeTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskItem> list = new ArrayList<SequencingLifeTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSequencingTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SequencingLifeTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskTemplate> list = new ArrayList<SequencingLifeTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by stepNum ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSequencingReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SequencingLifeTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskReagent> list = new ArrayList<SequencingLifeTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据选中的步骤查询相关的试剂明细
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from SequencingLifeTaskReagent t where 1=1 and t.sequencing='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SequencingLifeTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<SequencingLifeTaskReagent> setReagentList(String code) {
		String hql = "from SequencingLifeTaskReagent where 1=1 and sequencing='"
				+ code + "'";
		List<SequencingLifeTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据选中的步骤查询相关的设备明细
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from SequencingLifeTaskCos t where 1=1 and t.sequencing='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SequencingLifeTaskCos> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSequencingCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SequencingLifeTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskCos> list = new ArrayList<SequencingLifeTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSequencingResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleSequencingLifeInfo where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleSequencingLifeInfo> list = new ArrayList<SampleSequencingLifeInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询poolingtask
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectPoolingTaskList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from PoolingTask where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Pooling> list = new ArrayList<Pooling>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 待上机测序样本
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSequencingTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SequencingLifeTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskTemp> list = new ArrayList<SequencingLifeTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// ==============
	public Map<String, Object> setSequencingResultItemList(String code) {
		String hql = "from SampleSequencingLifeInfo t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.sequencing='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleSequencingLifeInfo> list = this.getSession()
				.createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// ==============

	// 根据步骤 查询试剂
	public Map<String, Object> selectSequencingReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from SequencingLifeTaskReagent where 1=1 and itemId='"
				+ itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskReagent> list = new ArrayList<SequencingLifeTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据步骤查询设备明细
	public Map<String, Object> selectSequencingCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from SequencingLifeTaskCos t where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskCos> list = new ArrayList<SequencingLifeTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 获取子表所有的数据
	public List<SampleSequencingLifeInfo> getSequencingResultList(String id) {
		String hql = "from SampleSequencingLifeInfo where 1=1 and sequencing.id='"
				+ id + "'";
		List<SampleSequencingLifeInfo> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据上机明细上机分组号查询富集结果表 获取富集富集文库
//	public List<PoolingBlendInfo> selectPoolingBlendInfoBysjfz(String id) {
//		String hql = "from PoolingBlendInfo where 1=1 and groupNum='" + id
//				+ "'";
//		List<PoolingBlendInfo> list = this.getSession().createQuery(hql).list();
//		return list;
//	}

	// 根据富集文库查询富集明细样本信息
	public List<PoolingItem> selectPoolingTaskItemByfjWkCode(String id) {
		String hql = "from PoolingTaskItem where 1=1 and fjWkCode='" + id + "'";
		List<PoolingItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据富集明细样本编号查询文库构建结果表信息
	public List<SampleWkLifeInfo> selectSampleInfoByCode(String id) {
		String hql = "from SampleWkLifeInfo where 1=1 and code='" + id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据文库构建结果样本编号查询文库构建明细表信息
	public List<WkLifeTaskItem> selectWkTaskItemByCode(String id) {
		String hql = "from WkLifeTaskItem where 1=1 and code='" + id + "'";
		List<WkLifeTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询上机测序临时表中状态为有效的数据
	public List<SequencingLifeTaskTemp> findSequencingTempList() {
		String hql = "from SequencingLifeTaskTemp where 1=1 and state='1'";
		List<SequencingLifeTaskTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 查询上机测序子表数据
	public List<SequencingLifeTaskItem> findSequencingItemList(String id) {
		String hql = "from SequencingLifeTaskItem where 1=1 and sequencing.id='"
				+ id + "'";
		List<SequencingLifeTaskItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据poooling号查询上机临时数据
	public SequencingLifeTaskTemp findSequencingTempListByPooling(String code) {
		String hql = "from SequencingLifeTaskTemp where 1=1 and poolingCode= '"
				+ code + "'";
		List<SequencingLifeTaskTemp> list = this.getSession().createQuery(hql)
				.list();
		return list.get(0);
	}

	// 查询两个表里的字段
	// public List<Object> findSeqAndSampleList(String poolingCode){
	// String
	// hql="select * from SequencingTaskItem t ,SampleSequencingInfo s where t.poolingCode = s.poolingCode and s.sequencing ='"+poolingCode+"' and t.sequencing='"+poolingCode+"';";
	// List<Object> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	// 根据pooling编号查询文库编号
	public List<PoolingItem> selectWkCodeBySampleCode(String poolingCode) {
		String hql = " from PoolingTaskItem t where t.poolingTask.id = '"
				+ poolingCode + "'";
		List<PoolingItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据pooling编号查询文库编号
	public List<PoolingItem> selectWkCodeBySampleCodeKy(String poolingCode) {
		String hql = " from PoolingTaskItem t where techTaskId !=null and t.poolingTask.id = '"
				+ poolingCode + "'";
		List<PoolingItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询文库sampleinfo
	public SampleInfo selectSampleInfoCode(String poolingCode) {
		String hql = " from SampleInfo t where t.code = '" + poolingCode + "'";
		SampleInfo list = (SampleInfo) this.getSession().createQuery(hql)
				.list().get(0);
		return list;
	}

	// 根据主表id查詢上机結果表
	public List<SampleSequencingLifeInfo> selectSeqInfoById(String scId) {
		String hql = "from SampleSequencingLifeInfo t where t.sequencing = '"
				+ scId + "'";
		List<SampleSequencingLifeInfo> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SequencingLifeTaskItem> selectSequenceItemList(String scId)
			throws Exception {
		String hql = "from SequencingLifeTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sequencing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingLifeTaskItem> list = new ArrayList<SequencingLifeTaskItem>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 查询上机测序子表数据
	public List<SequencingLifeTaskItem> findSequencingItemList1(String id) {
		String hql = "from SequencingLifeTaskItem where 1=1 and sequencing.id='"
				+ id + "'";
		List<SequencingLifeTaskItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

}