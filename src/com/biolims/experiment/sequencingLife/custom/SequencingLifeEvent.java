package com.biolims.experiment.sequencingLife.custom;



import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;

import com.biolims.experiment.sequencingLife.service.SequencingLifeService;

public class SequencingLifeEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SequencingLifeService mbService = (SequencingLifeService) ctx.getBean("sequencingLifeService");
//		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
