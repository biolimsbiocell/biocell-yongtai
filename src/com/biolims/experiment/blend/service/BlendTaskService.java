package com.biolims.experiment.blend.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.blend.dao.BlendTaskDao;
import com.biolims.experiment.blend.model.BlendTask;
import com.biolims.experiment.blend.model.BlendTaskCos;
import com.biolims.experiment.blend.model.BlendTaskItem;
import com.biolims.experiment.blend.model.BlendTaskReagent;
import com.biolims.experiment.blend.model.BlendTaskResult;
import com.biolims.experiment.blend.model.BlendTaskTemp;
import com.biolims.experiment.blend.model.BlendTaskTemplate;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BlendTaskService {
	@Resource
	private BlendTaskDao blendTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findBlendTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return blendTaskDao.selectBlendTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BlendTask i) throws Exception {

		blendTaskDao.saveOrUpdate(i);

	}

	public BlendTask get(String id) {
		BlendTask blendTask = commonDAO.get(BlendTask.class, id);
		return blendTask;
	}

	public Map<String, Object> findBlendTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = blendTaskDao.selectBlendTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<BlendTaskItem> list = (List<BlendTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findBlendTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = blendTaskDao.selectBlendTaskTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<BlendTaskTemplate> list = (List<BlendTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBlendTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = blendTaskDao.selectBlendTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<BlendTaskReagent> list = (List<BlendTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findBlendTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = blendTaskDao.selectBlendTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<BlendTaskCos> list = (List<BlendTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findBlendTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = blendTaskDao.selectBlendTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<BlendTaskResult> list = (List<BlendTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findBlendTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = blendTaskDao.selectBlendTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<BlendTaskTemp> list = (List<BlendTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBlendTaskItem(BlendTask sc, String itemDataJson)
			throws Exception {
		List<BlendTaskItem> saveItems = new ArrayList<BlendTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BlendTaskItem scp = new BlendTaskItem();
			// 将map信息读入实体类
			scp = (BlendTaskItem) blendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBlendTask(sc);

			saveItems.add(scp);
		}
		blendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBlendTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			BlendTaskItem scp = blendTaskDao.get(BlendTaskItem.class, id);
			blendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBlendTaskTemplate(BlendTask sc, String itemDataJson)
			throws Exception {
		List<BlendTaskTemplate> saveItems = new ArrayList<BlendTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BlendTaskTemplate scp = new BlendTaskTemplate();
			// 将map信息读入实体类
			scp = (BlendTaskTemplate) blendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBlendTask(sc);

			saveItems.add(scp);
		}
		blendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBlendTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			BlendTaskTemplate scp = blendTaskDao.get(BlendTaskTemplate.class,
					id);
			blendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBlendTaskReagent(BlendTask sc, String itemDataJson)
			throws Exception {
		List<BlendTaskReagent> saveItems = new ArrayList<BlendTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BlendTaskReagent scp = new BlendTaskReagent();
			// 将map信息读入实体类
			scp = (BlendTaskReagent) blendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBlendTask(sc);

			saveItems.add(scp);
		}
		blendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBlendTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			BlendTaskReagent scp = blendTaskDao.get(BlendTaskReagent.class, id);
			blendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBlendTaskCos(BlendTask sc, String itemDataJson)
			throws Exception {
		List<BlendTaskCos> saveItems = new ArrayList<BlendTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BlendTaskCos scp = new BlendTaskCos();
			// 将map信息读入实体类
			scp = (BlendTaskCos) blendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBlendTask(sc);

			saveItems.add(scp);
		}
		blendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBlendTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			BlendTaskCos scp = blendTaskDao.get(BlendTaskCos.class, id);
			blendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBlendTaskResult(BlendTask sc, String itemDataJson)
			throws Exception {
		List<BlendTaskResult> saveItems = new ArrayList<BlendTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			BlendTaskResult scp = new BlendTaskResult();
			// 将map信息读入实体类
			scp = (BlendTaskResult) blendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setBlendTask(sc);

			saveItems.add(scp);
		}
		blendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBlendTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			BlendTaskResult scp = blendTaskDao.get(BlendTaskResult.class, id);
			blendTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delBlendTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			BlendTaskTemp scp = blendTaskDao.get(BlendTaskTemp.class, id);
			blendTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(BlendTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			blendTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("blendTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBlendTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("blendTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBlendTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("blendTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBlendTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("blendTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBlendTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("blendTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveBlendTaskResult(sc, jsonStr);
			}
		}
	}
}
