package com.biolims.experiment.blend.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.blend.model.BlendTask;
import com.biolims.experiment.blend.model.BlendTaskCos;
import com.biolims.experiment.blend.model.BlendTaskItem;
import com.biolims.experiment.blend.model.BlendTaskReagent;
import com.biolims.experiment.blend.model.BlendTaskResult;
import com.biolims.experiment.blend.model.BlendTaskTemp;
import com.biolims.experiment.blend.model.BlendTaskTemplate;
import com.biolims.experiment.blend.service.BlendTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.remind.model.SysRemind;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/blend/blendTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BlendTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2413";
	@Autowired
	private BlendTaskService blendTaskService;
	private BlendTask blendTask = new BlendTask();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showBlendTaskList")
	public String showBlendTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/blend/blendTask.jsp");
	}

	@Action(value = "showBlendTaskListJson")
	public void showBlendTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = blendTaskService.findBlendTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BlendTask> list = (List<BlendTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("lane", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "blendTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogBlendTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskDialog.jsp");
	}

	@Action(value = "showDialogBlendTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogBlendTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = blendTaskService.findBlendTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<BlendTask> list = (List<BlendTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("lane", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editBlendTask")
	public String editBlendTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			blendTask = blendTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "blendTask");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			blendTask.setCreateUser(user);
			blendTask.setCreateDate(new Date());
			blendTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			blendTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskEdit.jsp");
	}

	@Action(value = "copyBlendTask")
	public String copyBlendTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		blendTask = blendTaskService.get(id);
		blendTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = blendTask.getId();
		if (id != null && id.equals("")) {
			blendTask.setId(null);
		}
		Map aMap = new HashMap();
		aMap.put("blendTaskItem", getParameterFromRequest("blendTaskItemJson"));

		aMap.put("blendTaskTemplate",
				getParameterFromRequest("blendTaskTemplateJson"));

		aMap.put("blendTaskReagent",
				getParameterFromRequest("blendTaskReagentJson"));

		aMap.put("blendTaskCos", getParameterFromRequest("blendTaskCosJson"));

		aMap.put("blendTaskResult",
				getParameterFromRequest("blendTaskResultJson"));

		aMap.put("blendTaskTemp", getParameterFromRequest("blendTaskTempJson"));

		blendTaskService.save(blendTask, aMap);
		return redirect("/experiment/blend/blendTask/editBlendTask.action?id="
				+ blendTask.getId());

	}

	@Action(value = "viewBlendTask")
	public String toViewBlendTask() throws Exception {
		String id = getParameterFromRequest("id");
		blendTask = blendTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskEdit.jsp");
	}

	@Action(value = "showBlendTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBlendTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskItem.jsp");
	}

	@Action(value = "showBlendTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBlendTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = blendTaskService
					.findBlendTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BlendTaskItem> list = (List<BlendTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("poolingName", "");
			map.put("wkNum", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("blendTask-name", "");
			map.put("blendTask-id", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("unit", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBlendTaskItem")
	public void delBlendTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			blendTaskService.delBlendTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBlendTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBlendTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskTemplate.jsp");
	}

	@Action(value = "showBlendTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBlendTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = blendTaskService
					.findBlendTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BlendTaskTemplate> list = (List<BlendTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("blendTask-name", "");
			map.put("blendTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBlendTaskTemplate")
	public void delBlendTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			blendTaskService.delBlendTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBlendTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBlendTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskReagent.jsp");
	}

	@Action(value = "showBlendTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBlendTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = blendTaskService
					.findBlendTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BlendTaskReagent> list = (List<BlendTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("blendTask-name", "");
			map.put("blendTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBlendTaskReagent")
	public void delBlendTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			blendTaskService.delBlendTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBlendTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBlendTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskCos.jsp");
	}

	@Action(value = "showBlendTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBlendTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = blendTaskService.findBlendTaskCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BlendTaskCos> list = (List<BlendTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("blendTask-name", "");
			map.put("blendTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBlendTaskCos")
	public void delBlendTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			blendTaskService.delBlendTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBlendTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBlendTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskResult.jsp");
	}

	@Action(value = "showBlendTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBlendTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = blendTaskService
					.findBlendTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<BlendTaskResult> list = (List<BlendTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("poolingName", "");
			map.put("wkNum", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("blendTask-name", "");
			map.put("blendTask-id", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("unit", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBlendTaskResult")
	public void delBlendTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			blendTaskService.delBlendTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showBlendTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBlendTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/blend/blendTaskTemp.jsp");
	}

	@Action(value = "showBlendTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBlendTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = blendTaskService
					.findBlendTaskTempList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<BlendTaskTemp> list = (List<BlendTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note ", "");
			map.put("classify", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delBlendTaskTemp")
	public void delBlendTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			blendTaskService.delBlendTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public BlendTaskService getBlendTaskService() {
		return blendTaskService;
	}

	public void setBlendTaskService(BlendTaskService blendTaskService) {
		this.blendTaskService = blendTaskService;
	}

	public BlendTask getBlendTask() {
		return blendTask;
	}

	public void setBlendTask(BlendTask blendTask) {
		this.blendTask = blendTask;
	}

}
