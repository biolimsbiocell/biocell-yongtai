package com.biolims.experiment.blend.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 设备明细
 * @author lims-platform
 * @date 2016-04-21 14:20:04
 * @version V1.0   
 *
 */
@Entity
@Table(name = "BLEND_TASK_COS")
@SuppressWarnings("serial")
public class BlendTaskCos extends EntityDao<BlendTaskCos> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**设备编号*/
	private String code;
	/**温度*/
	private Double temperature;
	/**转速*/
	private String  speed;
	/**时间*/
	private Double time;
	/**备注*/
	private String note;
	/**是否通过检测*/
	private String isGood;
	/**相关主表*/
	private BlendTask blendTask;
	/**关联步骤id*/
	private String itemId;
	/**模板设备id*/
	private String tCos;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  设备编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  设备编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  温度
	 */
	@Column(name ="TEMPERATURE", length = 50)
	public Double getTemperature(){
		return this.temperature;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  温度
	 */
	public void setTemperature(Double temperature){
		this.temperature = temperature;
	}
	/**
	 *方法: 取得String 
	 *@return: String   转速
	 */
	@Column(name ="SPEED", length = 50)
	public String  getSpeed(){
		return this.speed;
	}
	/**
	 *方法: 设置String 
	 *@param: String   转速
	 */
	public void setSpeed(String  speed){
		this.speed = speed;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  时间
	 */
	@Column(name ="TIME", length = 50)
	public Double getTime(){
		return this.time;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  时间
	 */
	public void setTime(Double time){
		this.time = time;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否通过检测
	 */
	@Column(name ="IS_GOOD", length = 50)
	public String getIsGood(){
		return this.isGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否通过检测
	 */
	public void setIsGood(String isGood){
		this.isGood = isGood;
	}
	/**
	 *方法: 取得BlendTask
	 *@return: BlendTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BLEND_TASK")
	public BlendTask getBlendTask(){
		return this.blendTask;
	}
	/**
	 *方法: 设置BlendTask
	 *@param: BlendTask  相关主表
	 */
	public void setBlendTask(BlendTask blendTask){
		this.blendTask = blendTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  关联步骤id
	 */
	@Column(name ="ITEM_ID", length = 50)
	public String getItemId(){
		return this.itemId;
	}
	/**
	 *方法: 设置String
	 *@param: String  关联步骤id
	 */
	public void setItemId(String itemId){
		this.itemId = itemId;
	}
	/**
	 *方法: 取得String
	 *@return: String  模板设备id
	 */
	@Column(name ="T_COS", length = 50)
	public String getTCos(){
		return this.tCos;
	}
	/**
	 *方法: 设置String
	 *@param: String  模板设备id
	 */
	public void setTCos(String tCos){
		this.tCos = tCos;
	}
}