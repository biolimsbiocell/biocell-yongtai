package com.biolims.experiment.karyo.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
/**   
 * @Title: Model
 * @Description: 细胞接种明细
 * @author lims-platform
 * @date 2016-05-30 10:16:20
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYOTYPE_TASK_ITEM")
@SuppressWarnings("serial")
public class KaryoTypeTaskItem extends EntityDao<KaryoTypeTaskItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	/**实验编号*/
	private String experimentCode;
	/**编号核对*/
	private String checkCode;
	/**接种时间*/
	private Date inoculateDate;
	/**预计收获时间*/
	private Date preReapDate;
	/**备注*/
	private String note;
	/**相关主表*/
	private KaryoTypeTask karyotypeTask;
	/** 临时表id */
	private String tempId;
	/** 结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 状态 */
	private String state;
	/** 任务单id */
	private String orderId;
	/**中间产物类型*/
	private DicSampleType dicSampleType;
	/**中间产物数量*/
	private String productNum;
	/**样本类型*/
	private String sampleType;
	/**检测项目编号*/
	private String productId;
	/**检测项目名称*/
	private String productName;
	/**接收日期*/
	private Date acceptDate;
	/**应出报告日期*/
	private Date reportDate;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  实验编号
	 */
	@Column(name ="EXPERIMENT_CODE", length = 50)
	public String getExperimentCode(){
		return this.experimentCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  实验编号
	 */
	public void setExperimentCode(String experimentCode){
		this.experimentCode = experimentCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号核对
	 */
	@Column(name ="CHECK_CODE", length = 50)
	public String getCheckCode(){
		return this.checkCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号核对
	 */
	public void setCheckCode(String checkCode){
		this.checkCode = checkCode;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接种时间
	 */
	@Column(name ="INOCULATE_DATE", length = 100)
	public Date getInoculateDate(){
		return this.inoculateDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  接种时间
	 */
	public void setInoculateDate(Date inoculateDate){
		this.inoculateDate = inoculateDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  预计收获时间
	 */
	@Column(name ="PRE_REAP_DATE", length = 100)
	public Date getPreReapDate(){
		return this.preReapDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  预计收获时间
	 */
	public void setPreReapDate(Date preReapDate){
		this.preReapDate = preReapDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得KaryoTypeTask
	 *@return: KaryoTypeTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KARYOTYPE_TASK")
	public KaryoTypeTask getKaryotypeTask(){
		return this.karyotypeTask;
	}
	/**
	 *方法: 设置KaryoTypeTask
	 *@param: KaryoTypeTask  相关主表
	 */
	public void setKaryotypeTask(KaryoTypeTask karyotypeTask){
		this.karyotypeTask = karyotypeTask;
	}
	
	
	public String getTempId() {
		return tempId;
	}
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getProductNum() {
		return productNum;
	}
	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}