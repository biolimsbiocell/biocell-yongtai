package com.biolims.experiment.karyo.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 模板明细
 * @author lims-platform
 * @date 2016-05-30 10:16:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYOTYPE_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class KaryoTypeTaskTemplate extends EntityDao<KaryoTypeTaskTemplate> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**步骤编号*/
	private String stepNum;
	/**步骤描述*/
	private String stepDescribe;
	/**实验员*/
	private User experimentUser;
	/**开始时间*/
	private String startDate;
	/**结束时间*/
	private String endDate;
	/**关联*/
	private String patientName;
	/**关联样本*/
	private String sampleCodes;
	/**状态*/
	private String state;
	/**相关主表*/
	private KaryoTypeTask karyotypeTask;
	/**备注*/
	private String note;
	/**模板步骤ID*/
	private String tItem;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="STEP_NUM", length = 50)
	public String getStepNum(){
		return this.stepNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setStepNum(String stepNum){
		this.stepNum = stepNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤描述
	 */
	@Column(name ="STEP_DESCRIBE", length = 50)
	public String getStepDescribe(){
		return this.stepDescribe;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤描述
	 */
	public void setStepDescribe(String stepDescribe){
		this.stepDescribe = stepDescribe;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EXPERIMENT_USER")
	public User getExperimentUser() {
		return experimentUser;
	}
	public void setExperimentUser(User experimentUser) {
		this.experimentUser = experimentUser;
	}
	public String gettItem() {
		return tItem;
	}
	public void settItem(String tItem) {
		this.tItem = tItem;
	}
	/**
	 *方法: 取得String
	 *@return: String  开始时间
	 */
	@Column(name ="START_DATE", length = 50)
	public String getStartDate(){
		return this.startDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  开始时间
	 */
	public void setStartDate(String startDate){
		this.startDate = startDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  结束时间
	 */
	@Column(name ="END_DATE", length = 50)
	public String getEndDate(){
		return this.endDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  结束时间
	 */
	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	@Column(name ="PATIENT_NAME", length = 50)
	public String getPatientName(){
		return this.patientName;
	}

	public void setPatientName(String patientName){
		this.patientName = patientName;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得KaryoTypeTask
	 *@return: KaryoTypeTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KARYOTYPE_TASK")
	public KaryoTypeTask getKaryotypeTask(){
		return this.karyotypeTask;
	}
	/**
	 *方法: 设置KaryoTypeTask
	 *@param: KaryoTypeTask  相关主表
	 */
	public void setKaryotypeTask(KaryoTypeTask karyotypeTask){
		this.karyotypeTask = karyotypeTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  关联样本
	 */
	@Column(name ="SAMPLE_CODES", length = 4000)
	public String getSampleCodes(){
		return this.sampleCodes;
	}
	/**
	 *方法: 设置String
	 *@param: String  关联样本
	 */
	public void setSampleCodes(String sampleCodes){
		this.sampleCodes = sampleCodes;
	}
}