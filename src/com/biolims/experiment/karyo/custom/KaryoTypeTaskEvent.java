package com.biolims.experiment.karyo.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.karyo.service.KaryoTypeTaskService;

public class KaryoTypeTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		KaryoTypeTaskService mbService = (KaryoTypeTaskService) ctx.getBean("karyoTypeTaskService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
