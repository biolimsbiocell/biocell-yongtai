package com.biolims.experiment.karyo.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
import com.biolims.experiment.karyo.model.KaryoTypeTask;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskCos;
import com.biolims.experiment.karyo.model.KaryoTypeTaskHarvest;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemp;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemplate;
import com.biolims.experiment.snpjc.cross.model.SnpCrossItem;
import com.biolims.experiment.snpjc.cross.model.SnpCrossReagent;

@Repository
@SuppressWarnings("unchecked")
public class KaryoTypeTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectKaryoTypeTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoTypeTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTask> list = new ArrayList<KaryoTypeTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectKaryoTypeTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoTypeTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyotypeTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTaskItem> list = new ArrayList<KaryoTypeTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoTypeTaskTemplateList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoTypeTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyotypeTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTaskTemplate> list = new ArrayList<KaryoTypeTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoTypeTaskAgentiaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoTypeTaskAgentia where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyotypeTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTaskAgentia> list = new ArrayList<KaryoTypeTaskAgentia>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoTypeTaskCosList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoTypeTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyotypeTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTaskCos> list = new ArrayList<KaryoTypeTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoTypeTaskHarvestList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoTypeTaskHarvest where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyotypeTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTaskHarvest> list = new ArrayList<KaryoTypeTaskHarvest>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		/*public Map<String, Object> selectKaryoTypeTaskTempList(String scId, Integer startNum, Integer limitNum,
				String dir, String sort) throws Exception {
			String hql = "from KaryoTypeTaskTemp where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<KaryoTypeTaskTemp> list = new ArrayList<KaryoTypeTaskTemp>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}*/
	public Map<String, Object> selectKaryoTypeTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoTypeTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null){
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoTypeTaskTemp> list = new ArrayList<KaryoTypeTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	/**
	 * 根据主表ID查询结果明细
	 * @param id
	 * @return
	 */
	public List<KaryoTypeTaskHarvest> getKaryoTypeTaskResultById(String id){
		String hql = " from KaryoTypeTaskHarvest where 1=1 and karyotypeTask='"+id+"'";
		List<KaryoTypeTaskHarvest> list=this.getSession().createQuery(hql).list();
		return list;
	}
	// 根据样本编号查询
		public List<KaryoTypeTaskHarvest> setResultById(String code) {
			String hql = "from KaryoTypeTaskHarvest t where (isCommit is null or isCommit='') and karyotypeTask.id='"
					+ code + "'";
			List<KaryoTypeTaskHarvest> list = this.getSession().createQuery(hql).list();
			return list;
		}

		/**
		 * 根据样本编号查询
		 * @param codes
		 * @return
		 */
		public List<KaryoTypeTaskHarvest> setResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}

			String hql = "from KaryoTypeTaskHarvest t where (isCommit is null or isCommit='') and id in ("
					+ insql + ")";
			List<KaryoTypeTaskHarvest> list = this.getSession().createQuery(hql).list();
			return list;
		}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<KaryoTypeTaskItem> getItem(String id) {
		String hql = "from KaryoTypeTaskItem t where 1=1  and karyotypeTask='"+ id + "'";
		List<KaryoTypeTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<KaryoTypeTaskAgentia> getReagent(String id) {
		String hql = "from KaryoTypeTaskAgentia t where 1=1  and karyotypeTask='"+ id + "'";
		List<KaryoTypeTaskAgentia> list = this.getSession().createQuery(hql).list();
		return list;
	}
}
