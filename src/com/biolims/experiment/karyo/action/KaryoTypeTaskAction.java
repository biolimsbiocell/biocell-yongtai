package com.biolims.experiment.karyo.action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.karyo.model.KaryoTypeTask;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskCos;
import com.biolims.experiment.karyo.model.KaryoTypeTaskHarvest;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemp;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemplate;
import com.biolims.experiment.karyo.service.KaryoTypeTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/karyo/karyoTypeTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KaryoTypeTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249001";
	@Autowired
	private KaryoTypeTaskService karyoTypeTaskService;
	private KaryoTypeTask karyoTypeTask = new KaryoTypeTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showKaryoTypeTaskList")
	public String showKaryoTypeTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTask.jsp");
	}

	@Action(value = "showKaryoTypeTaskListJson")
	public void showKaryoTypeTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoTypeTaskService
				.findKaryoTypeTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoTypeTask> list = (List<KaryoTypeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "karyoTypeTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKaryoTypeTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskDialog.jsp");
	}

	@Action(value = "showDialogKaryoTypeTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKaryoTypeTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoTypeTaskService
				.findKaryoTypeTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoTypeTask> list = (List<KaryoTypeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKaryoTypeTask")
	public String editKaryoTypeTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			karyoTypeTask = karyoTypeTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "karyoTypeTask");
		} else {
			karyoTypeTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			karyoTypeTask.setCreateUser(user);
			karyoTypeTask.setCreateDate(new Date());
			karyoTypeTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			karyoTypeTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doCka");
//			Template t=tlist.get(0);
//			karyoTypeTask.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				karyoTypeTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(karyoTypeTask.getState());
		putObjToContext("fileNum", num);
		User user2 = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		putObjToContext("user2", user2.getName());
		
	
		
		
		
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskEdit.jsp");
	}

	@Action(value = "copyKaryoTypeTask")
	public String copyKaryoTypeTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		karyoTypeTask = karyoTypeTaskService.get(id);
		karyoTypeTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = karyoTypeTask.getId();
		/*
		 * if(id!=null&&id.equals("")){ karyoTypeTask.setId(null); }
		 */
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "KaryoTypeTask";
			String markCode = "JZSH";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			karyoTypeTask.setId(autoID);
//			karyoTypeTask.setName("样本接种-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("karyoTypeTaskItem",
				getParameterFromRequest("karyoTypeTaskItemJson"));

		aMap.put("karyoTypeTaskTemplate",
				getParameterFromRequest("karyoTypeTaskTemplateJson"));

		aMap.put("karyoTypeTaskAgentia",
				getParameterFromRequest("karyoTypeTaskAgentiaJson"));

		aMap.put("karyoTypeTaskCos",
				getParameterFromRequest("karyoTypeTaskCosJson"));

		aMap.put("karyoTypeTaskHarvest",
				getParameterFromRequest("karyoTypeTaskHarvestJson"));

		karyoTypeTaskService.save(karyoTypeTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/karyo/karyoTypeTask/editKaryoTypeTask.action?id="
				+ karyoTypeTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/karyo/karyoTypeTask/editKaryoTypeTask.action?id="
		// + karyoTypeTask.getId());

	}

	@Action(value = "viewKaryoTypeTask")
	public String toViewKaryoTypeTask() throws Exception {
		String id = getParameterFromRequest("id");
		karyoTypeTask = karyoTypeTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskEdit.jsp");
	}

	@Action(value = "showKaryoTypeTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoTypeTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskItem.jsp");
	}

	@Action(value = "showKaryoTypeTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoTypeTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoTypeTaskService
					.findKaryoTypeTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoTypeTaskItem> list = (List<KaryoTypeTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("experimentCode", "");
			map.put("checkCode", "");
			map.put("inoculateDate", "yyyy-MM-dd");
			map.put("preReapDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("sampleType", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("productNum", "");
			map.put("orderId", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("karyotypeTask-name", "");
			map.put("karyotypeTask-id", "");

			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskItem")
	public void delKaryoTypeTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoTypeTaskService.delKaryoTypeTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoTypeTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoTypeTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskTemplate.jsp");
	}

	@Action(value = "showKaryoTypeTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoTypeTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoTypeTaskService
					.findKaryoTypeTaskTemplateList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KaryoTypeTaskTemplate> list = (List<KaryoTypeTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("stepNum", "");
			map.put("stepDescribe", "");
			map.put("experimentUser-id", "");
			map.put("experimentUser-name", "");
			map.put("startDate", "");
			map.put("endDate", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("karyotypeTask-name", "");
			map.put("karyotypeTask-id", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("tItem", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskTemplate")
	public void delKaryoTypeTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoTypeTaskService.delKaryoTypeTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoTypeTaskAgentiaList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoTypeTaskAgentiaList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskAgentia.jsp");
	}

	@Action(value = "showKaryoTypeTaskAgentiaListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoTypeTaskAgentiaListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoTypeTaskService
					.findKaryoTypeTaskAgentiaList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KaryoTypeTaskAgentia> list = (List<KaryoTypeTaskAgentia>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("agentiaCode", "");
			map.put("agentiaName", "");
			map.put("batch", "");
			map.put("isCheck", "");
			map.put("singleDosage", "");
			map.put("reactionDosage", "");
			map.put("dosage", "");
			map.put("karyotypeTask-name", "");
			map.put("karyotypeTask-id", "");
			map.put("note", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskAgentia")
	public void delKaryoTypeTaskAgentia() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoTypeTaskService.delKaryoTypeTaskAgentia(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoTypeTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoTypeTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskCos.jsp");
	}

	@Action(value = "showKaryoTypeTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoTypeTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoTypeTaskService
					.findKaryoTypeTaskCosList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoTypeTaskCos> list = (List<KaryoTypeTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrumentCode", "");
			map.put("instrumentName", "");
			map.put("isCheck", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "yyyy-MM-dd");
			map.put("karyotypeTask-name", "");
			map.put("karyotypeTask-id", "");
			map.put("note", "");
			map.put("tCos", "");
			map.put("itemId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskCos")
	public void delKaryoTypeTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoTypeTaskService.delKaryoTypeTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoTypeTaskHarvestList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoTypeTaskHarvestList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskHarvest.jsp");
	}

	@Action(value = "showKaryoTypeTaskHarvestListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoTypeTaskHarvestListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoTypeTaskService
					.findKaryoTypeTaskHarvestList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KaryoTypeTaskHarvest> list = (List<KaryoTypeTaskHarvest>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("experimentCode", "");
			map.put("code", "");
			map.put("zjCode", "");
			map.put("harvestDate", "yyyy-MM-dd");
			map.put("harversUser", "");
			map.put("nextFlow", "");
			map.put("nextFlowId", "");
			map.put("isrStandard", "");
			map.put("isCommit", "");
			map.put("karyotypeTask-name", "");
			map.put("karyotypeTask-id", "");
			map.put("note", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("preReapDate", "yyyy-MM-dd");
			map.put("inoculateDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskHarvest")
	public void delKaryoTypeTaskHarvest() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoTypeTaskService.delKaryoTypeTaskHarvest(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoTypeTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoTypeTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyo/karyoTypeTaskTemp.jsp");
	}

	@Action(value = "showKaryoTypeTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoTypeTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoTypeTaskService
				.findKaryoTypeTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long total = (Long) result.get("total");
		List<KaryoTypeTaskTemp> list = (List<KaryoTypeTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("state", "");
		map.put("note", "");
		map.put("sampleType", "");
		map.put("chargeNote", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskTemp")
	public void delKaryoTypeTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoTypeTaskService.delKaryoTypeTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KaryoTypeTaskService getKaryoTypeTaskService() {
		return karyoTypeTaskService;
	}

	public void setKaryoTypeTaskService(
			KaryoTypeTaskService karyoTypeTaskService) {
		this.karyoTypeTaskService = karyoTypeTaskService;
	}

	public KaryoTypeTask getKaryoTypeTask() {
		return karyoTypeTask;
	}

	public void setKaryoTypeTask(KaryoTypeTask karyoTypeTask) {
		this.karyoTypeTask = karyoTypeTask;
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskReagentOne")
	public void delKaryoTypeTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoTypeTaskService.delKaryoTypeTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskTemplateOne")
	public void delKaryoTypeTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoTypeTaskService.delKaryoTypeTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoTypeTaskCosOne")
	public void delKaryoTypeTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoTypeTaskService.delKaryoTypeTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	// 提交样本
		@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void submitSample() throws Exception {
			String id = getParameterFromRequest("id");
			String[] ids = getRequest().getParameterValues("ids[]");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				this.karyoTypeTaskService.submitSample(id, ids);

				result.put("success", true);

			} catch (Exception e) {
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
		  /**
		 * 保存细胞接种明细
		 * @throws Exception
		 */
		@Action(value = "saveKaryoTypeTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveKaryoTypeTaskItem() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				KaryoTypeTask sc=commonDAO.get(KaryoTypeTask.class, id);
				Map aMap = new HashMap();
				aMap.put("karyoTypeTaskItem",
						itemDataJson);
				if(sc!=null){
					karyoTypeTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
		 /**
		 * 保存接种结果明细
		 * @throws Exception
		 */
		@Action(value = "saveKaryoTypeTaskHarvest",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveKaryoTypeTaskHarvest() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				KaryoTypeTask sc=commonDAO.get(KaryoTypeTask.class, id);
				Map aMap = new HashMap();
				aMap.put("karyoTypeTaskHarvest",
						itemDataJson);
				if(sc!=null){
					karyoTypeTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
}
