package com.biolims.experiment.karyo.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.WorkDay;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.dna.model.DnaTaskReagent;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.karyo.dao.KaryoTypeTaskDao;
import com.biolims.experiment.karyo.model.KaryoTypeTask;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskCos;
import com.biolims.experiment.karyo.model.KaryoTypeTaskHarvest;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemp;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemplate;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.experiment.snpjc.cross.model.SnpCrossItem;
import com.biolims.experiment.snpjc.cross.model.SnpCrossReagent;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.model.Template;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KaryoTypeTaskService {
	@Resource
	private KaryoTypeTaskDao karyoTypeTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findKaryoTypeTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return karyoTypeTaskDao.selectKaryoTypeTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryoTypeTask i) throws Exception {

		karyoTypeTaskDao.saveOrUpdate(i);

	}

	public KaryoTypeTask get(String id) {
		KaryoTypeTask karyoTypeTask = commonDAO.get(KaryoTypeTask.class, id);
		return karyoTypeTask;
	}

	public Map<String, Object> findKaryoTypeTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = karyoTypeTaskDao
				.selectKaryoTypeTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<KaryoTypeTaskItem> list = (List<KaryoTypeTaskItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKaryoTypeTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = karyoTypeTaskDao
				.selectKaryoTypeTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<KaryoTypeTaskTemplate> list = (List<KaryoTypeTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKaryoTypeTaskAgentiaList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = karyoTypeTaskDao
				.selectKaryoTypeTaskAgentiaList(scId, startNum, limitNum, dir,
						sort);
		List<KaryoTypeTaskAgentia> list = (List<KaryoTypeTaskAgentia>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKaryoTypeTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = karyoTypeTaskDao
				.selectKaryoTypeTaskCosList(scId, startNum, limitNum, dir, sort);
		List<KaryoTypeTaskCos> list = (List<KaryoTypeTaskCos>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKaryoTypeTaskHarvestList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = karyoTypeTaskDao
				.selectKaryoTypeTaskHarvestList(scId, startNum, limitNum, dir,
						sort);
		List<KaryoTypeTaskHarvest> list = (List<KaryoTypeTaskHarvest>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKaryoTypeTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		/*Map<String, Object> result = karyoTypeTaskDao
				.selectKaryoTypeTaskTempList(mapForQuery, startNum, limitNum,
						dir, sort);
		List<KaryoTypeTaskTemp> list = (List<KaryoTypeTaskTemp>) result
				.get("list");
		return result;*/
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<KaryoTypeTaskTemp> list2=new ArrayList<KaryoTypeTaskTemp>();
		Map<String, Object> result =  karyoTypeTaskDao
				.selectKaryoTypeTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoTypeTaskTemp> list = (List<KaryoTypeTaskTemp>) result.get("list");
		for(KaryoTypeTaskTemp t:list){
			SampleOrder s=commonDAO.get(SampleOrder.class,t.getSampleCode());
			if(s!=null){
				t.setChargeNote(s.getChargeNote());
				t.setAcceptDate(s.getCreateDate());
				t.setReportDate(s.getReportDate());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoTypeTaskItem(KaryoTypeTask sc, String itemDataJson)
			throws Exception {
		List<KaryoTypeTaskItem> saveItems = new ArrayList<KaryoTypeTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoTypeTaskItem scp = new KaryoTypeTaskItem();
			// 将map信息读入实体类
			scp = (KaryoTypeTaskItem) karyoTypeTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryotypeTask(sc);
			if (scp.getInoculateDate() == null
					|| "".equals(scp.getInoculateDate())) {
				scp.setInoculateDate(new Date());
			}
			if (scp != null) {
				Template t = commonDAO.get(Template.class, sc.getTemplate()
						.getId());
				// DicSampleType d=new DicSampleType();
				if (t != null) {
					if (t.getDicSampleType() != null) {
						DicSampleType d = commonDAO.get(DicSampleType.class, t
								.getDicSampleType().getId());
						if (scp.getDicSampleType() == null
								|| (scp.getDicSampleType() != null && scp
										.getDicSampleType().equals(""))) {
							if (d != null) {
								scp.setDicSampleType(d);
							}
						}
					}
					if (t.getProductNum() != null) {
						if (scp.getProductNum() == null
								|| (scp.getProductNum() != null && scp
										.getProductNum().equals(""))) {
							scp.setProductNum(t.getProductNum());
						}
					}
					if (t.getDuringDays() != null) {
//						if (scp.getPreReapDate() == null
//								|| "".equals(scp.getPreReapDate())) {
							// 只算工作日
//							Calendar calendar = Calendar.getInstance();
//							calendar.setTime(scp.getInoculateDate());

//							Calendar ca = Calendar.getInstance();
//							ca = WorkDay.addDateByWorkDay(calendar,
//									t.getDuringDays());
						
//							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Calendar ca = Calendar.getInstance();
							ca.setTime(scp.getInoculateDate());
							ca.add(ca.DATE, t.getDuringDays());
//							Date d = new Date();
//							d = ca.getTime();
//							String backTime = format.format(d);
							
							
							
							Date a = (Date) ca.getTime();
							scp.setPreReapDate(a);
//						}
					}

				}
			}
			// 改变左侧样本状态
			KaryoTypeTaskTemp dt = commonDAO.get(KaryoTypeTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
			commonDAO.saveOrUpdate(dt);
			saveItems.add(scp);
		}
		karyoTypeTaskDao.saveOrUpdateAll(saveItems);
		/*List<KaryoTypeTaskItem> snlist=karyoTypeTaskDao.getItem(sc.getId());
		List<KaryoTypeTaskAgentia> rglist=karyoTypeTaskDao.getReagent(sc.getId());
		if(snlist.size()>0){
			if(rglist.size()>0){
				for(KaryoTypeTaskAgentia sr:rglist){
				
				}
			}
		}*/
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoTypeTaskItem scp = karyoTypeTaskDao.get(
					KaryoTypeTaskItem.class, id);
			karyoTypeTaskDao.delete(scp);
			// 改变左侧样本状态
			KaryoTypeTaskTemp dt = commonDAO.get(KaryoTypeTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.saveOrUpdate(dt);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoTypeTaskTemplate(KaryoTypeTask sc, String itemDataJson)
			throws Exception {
		List<KaryoTypeTaskTemplate> saveItems = new ArrayList<KaryoTypeTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoTypeTaskTemplate scp = new KaryoTypeTaskTemplate();
			// 将map信息读入实体类
			scp = (KaryoTypeTaskTemplate) karyoTypeTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryotypeTask(sc);

			saveItems.add(scp);
		}
		karyoTypeTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoTypeTaskTemplate scp = karyoTypeTaskDao.get(
					KaryoTypeTaskTemplate.class, id);
			karyoTypeTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoTypeTaskAgentia(KaryoTypeTask sc, String itemDataJson)
			throws Exception {
		List<KaryoTypeTaskAgentia> saveItems = new ArrayList<KaryoTypeTaskAgentia>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoTypeTaskAgentia scp = new KaryoTypeTaskAgentia();
			// 将map信息读入实体类
			scp = (KaryoTypeTaskAgentia) karyoTypeTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryotypeTask(sc);

			saveItems.add(scp);
		}
		karyoTypeTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskAgentia(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoTypeTaskAgentia scp = karyoTypeTaskDao.get(
					KaryoTypeTaskAgentia.class, id);
			karyoTypeTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoTypeTaskCos(KaryoTypeTask sc, String itemDataJson)
			throws Exception {
		List<KaryoTypeTaskCos> saveItems = new ArrayList<KaryoTypeTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoTypeTaskCos scp = new KaryoTypeTaskCos();
			// 将map信息读入实体类
			scp = (KaryoTypeTaskCos) karyoTypeTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryotypeTask(sc);

			saveItems.add(scp);
		}
		karyoTypeTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoTypeTaskCos scp = karyoTypeTaskDao.get(KaryoTypeTaskCos.class,
					id);
			karyoTypeTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoTypeTaskHarvest(KaryoTypeTask sc, String itemDataJson)
			throws Exception {
		List<KaryoTypeTaskHarvest> saveItems = new ArrayList<KaryoTypeTaskHarvest>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> map : list) {
			KaryoTypeTaskHarvest scp = new KaryoTypeTaskHarvest();
			// 将map信息读入实体类
			scp = (KaryoTypeTaskHarvest) karyoTypeTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryotypeTask(sc);

			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						&& !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());

					markCode = scp.getSampleCode() + d.getCode();
				} else {
					markCode = scp.getSampleCode();
				}
				String code = codingRuleService.getCode("KaryoTypeTaskHarvest",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			saveItems.add(scp);
			karyoTypeTaskDao.saveOrUpdate(scp);
			/*if (scp != null) {
				if (scp.getIsrStandard() != null && scp.getIsCommit() != null
						&& !scp.getIsrStandard().equals("")
						&& !scp.getIsCommit().equals("")) {
					if (scp.getIsCommit().equals("1")) {
						if (scp.getIsrStandard().equals("1")) {
							if (scp.getNextFlowId() != null
									&& !scp.getNextFlowId().equals("")) {
								if (scp.getNextFlowId().equals("0009")) {// 样本入库
									SampleInItemTemp st = new SampleInItemTemp();
									st.setCode(scp.getCode());
									st.setSampleCode(scp.getSampleCode());
									st.setState("1");
									commonDAO.saveOrUpdate(st);
									// 入库，改变SampleInfo中原始样本的状态为“待入库”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
									}
								} else if (scp.getNextFlowId().equals("0012")) {// 暂停
									// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
									}
								} else if (scp.getNextFlowId().equals("0013")) {// 终止
									// 终止，改变SampleInfo中原始样本的状态为“实验终止”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
									}
								} else {
									// 得到下一步流向的相关表单
									List<NextFlow> list_nextFlow = nextFlowDao
											.seletNextFlowById(scp
													.getNextFlowId());
									for (NextFlow n : list_nextFlow) {
										Object o = Class.forName(
												n.getApplicationTypeTable()
														.getClassPath())
												.newInstance();
										scp.setState("1");
										sampleInputService.copy(o, scp);
									}
								}
							}
						} else {
							KaryoAbnormal ka = new KaryoAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setOrderId(scp.getOrderId());
							ka.setAcceptDate(scp.getAcceptDate());
							ka.setReportDate(scp.getReportDate());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getKaryotypeTask().getId());
							ka.setNote(scp.getNote());
							ka.setTaskName("样本接种");
							ka.setState("1");

							commonDAO.saveOrUpdate(ka);
						}
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										format.format(sc.getCreateDate()),
										format.format(new Date()),
										"KaryoTypeTask",
										"" + "样本接种",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), scp.getNextFlow(),
										scp.getIsrStandard(), null, null, null,
										null, null, null, null, null);
					}
				}
			}*/
		}
		karyoTypeTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskHarvest(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoTypeTaskHarvest scp = karyoTypeTaskDao.get(
					KaryoTypeTaskHarvest.class, id);
			karyoTypeTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoTypeTaskTemp scp = karyoTypeTaskDao.get(
					KaryoTypeTaskTemp.class, id);
			karyoTypeTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryoTypeTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			karyoTypeTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("karyoTypeTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoTypeTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("karyoTypeTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoTypeTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("karyoTypeTaskAgentia");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoTypeTaskAgentia(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("karyoTypeTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoTypeTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("karyoTypeTaskHarvest");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoTypeTaskHarvest(sc, jsonStr);
			}
			List<KaryoTypeTaskItem> snlist=karyoTypeTaskDao.getItem(sc.getId());
			List<KaryoTypeTaskAgentia> rglist=karyoTypeTaskDao.getReagent(sc.getId());
			DecimalFormat df = new DecimalFormat("#.00");
			Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(KaryoTypeTaskAgentia sr:rglist){
						sr.setSampleNum(String.valueOf(snlist.size()));
						if(sr.getSingleDosage()!=null && 
								sr.getSampleNum()!=null){
							nums =Double.valueOf(df.format(sr.getSingleDosage()*snlist.size()));
							sr.setDosage(nums);
						}
					}
				}
			}
		}
	}

	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		KaryoTypeTask kt = karyoTypeTaskDao.get(KaryoTypeTask.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stime = format.format(date);
		kt.setConfirmDate(stime);
		karyoTypeTaskDao.update(kt);

		// 完成后改变左侧表的状态
		List<KaryoTypeTaskHarvest> list = karyoTypeTaskDao
				.getKaryoTypeTaskResultById(kt.getId());
		if (list.size() > 0) {
			for (KaryoTypeTaskHarvest k : list) {
				KaryoTypeTaskTemp tt = commonDAO.get(KaryoTypeTaskTemp.class,
						k.getTempId());
				if (tt != null) {
					tt.setState("2");
				}
			}
		}
		submitSample(id,null);
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskReagentOne(String ids) throws Exception {
		KaryoTypeTaskAgentia scp = karyoTypeTaskDao.get(
				KaryoTypeTaskAgentia.class, ids);
		karyoTypeTaskDao.delete(scp);
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskTemplateOne(String ids) throws Exception {
		KaryoTypeTaskTemplate scp = karyoTypeTaskDao.get(
				KaryoTypeTaskTemplate.class, ids);
		karyoTypeTaskDao.delete(scp);
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoTypeTaskCosOne(String ids) throws Exception {
		KaryoTypeTaskCos scp = karyoTypeTaskDao
				.get(KaryoTypeTaskCos.class, ids);
		karyoTypeTaskDao.delete(scp);
	}
	
	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		KaryoTypeTask sc = this.karyoTypeTaskDao.get(KaryoTypeTask.class, id);
		// 获取结果表样本信息
		List<KaryoTypeTaskHarvest> list;
		if (ids == null)
			list = this.karyoTypeTaskDao.setResultById(id);
		else
			list = this.karyoTypeTaskDao.setResultByIds(ids);
		for (KaryoTypeTaskHarvest scp : list) {
			if (scp != null) {
				if (scp.getIsrStandard() != null && (scp.getIsCommit() == null||"".equals(scp.getIsCommit()))) {
					String isOk = scp.getIsrStandard();
					if (isOk.equals("1")) {
						KaryoGetTaskTemp k = new KaryoGetTaskTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
//							k.setChipNum(scp.getChipNum());
						k.setState("1");
//						k.setAcceptDate(scp.getAcceptDate());
//						k.setInoculateDate(scp.getInoculateDate());
//						k.setPreReapDate(scp.getPreReapDate());
						k.setAcceptDate(scp.getAcceptDate());
						k.setInoculateDate(scp.getInoculateDate());
						k.setPreReapDate(scp.getPreReapDate());
						
						karyoTypeTaskDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(sc.getCreateDate()),
										format.format(new Date()),
										"KaryoTypeTask",
										"样本接种",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "样本收获",
										scp.getIsrStandard(), null, null, null,
										null, null, null, null, null);
						scp.setIsCommit("1");
						karyoTypeTaskDao.saveOrUpdate(scp);
					} else {
						
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("样本接种");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getKaryotypeTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
								// 不合格的到异常
								KaryoAbnormal ka = new KaryoAbnormal();
								ka.setCode(scp.getCode());
								ka.setSampleCode(scp.getSampleCode());
								ka.setProductId(scp.getProductId());
								ka.setProductName(scp.getProductName());
								ka.setSampleType(scp.getSampleType());
								ka.setAcceptDate(scp.getAcceptDate());
								ka.setInoculateDate(scp.getInoculateDate());
								ka.setPreReapDate(scp.getPreReapDate());
								ka.setTaskId(scp.getKaryotypeTask().getId());
								ka.setNote(scp.getNote());
								ka.setTaskName("样本接种");
								ka.setState("1");
								karyoTypeTaskDao.saveOrUpdate(ka);
						}
						scp.setIsCommit("1");
					}
				}
			}
		}
	}
}




