package com.biolims.experiment.qaAudit.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.experiment.qaAudit.dao.QaAuditDao;
import com.biolims.experiment.qaAudit.model.QaAudit;
import com.biolims.experiment.qaAudit.model.QaAuditItem;
import com.biolims.experiment.qaAudit.model.QaAuditStorageItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QaAuditService {
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private QaAuditDao qaAuditDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QaAudit i) throws Exception {

		qaAuditDao.saveOrUpdate(i);

	}

	public QaAudit get(String id) {
		QaAudit qaAudit = commonDAO.get(QaAudit.class, id);
		return qaAudit;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(QaAudit sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qaAuditDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}

	public Map<String, Object> findQaAuditTable(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return qaAuditDao.findQaAuditTable(start, length, query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(QaAudit qaAudit, String logInfo, Map aMap, String changeLogItem,String log) throws Exception {
		String id = "";
		if (qaAudit != null) {
			qaAuditDao.saveOrUpdate(qaAudit);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(qaAudit.getId());
				li.setClassName("QaAudit");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
			id = qaAudit.getId();
			String jsonStr = "";
			jsonStr = (String) aMap.get("qaAuditItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQaAuditItem(qaAudit, jsonStr, changeLogItem,log);
			}
			
			jsonStr = (String) aMap.get("qaAuditStorageItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQaAuditStorageItem(qaAudit, jsonStr, "",log);
			}
		}
		return id;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaAuditItem(QaAudit sc, String itemDataJson, String changeLogItem,String log) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaAuditItem scp = new QaAuditItem();
			// 将map信息读入实体类
			scp = (QaAuditItem) qaAuditDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQaAudit(sc);
			commonDAO.saveOrUpdate(scp);
		}
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("QaAuditItem");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaAuditStorageItem(QaAudit sc, String itemDataJson, String changeLogItem,String log) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QaAuditStorageItem scp = new QaAuditStorageItem();
			// 将map信息读入实体类
			scp = (QaAuditStorageItem) qaAuditDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQaAudit(sc);
			commonDAO.saveOrUpdate(scp);
		}
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("QaAuditItem");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	public Map<String, Object> showQaAuditItemTableJson(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		return qaAuditDao.showQaAuditItemTableJson(start, length, query, col, sort, id);
	}
	
	public Map<String, Object> showQaAuditStorageItemTableJson(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		return qaAuditDao.showQaAuditStorageItemTableJson(start, length, query, col, sort, id);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaAuditItem(String zid, String item, String logInfo) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		QaAudit pt = commonDAO.get(QaAudit.class, zid);
		for (Map<String, Object> map : list) {
			QaAuditItem scp = new QaAuditItem();
			// 将map信息读入实体类
			scp = (QaAuditItem) qaAuditDao.Map2Bean(map, scp);
			if (scp.getId() == null || "".equals(scp.getId())) {
				scp.setId(null);
			}
			scp.setQaAudit(pt);
			commonDAO.saveOrUpdate(scp);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(zid);
			li.setClassName("QaAuditItem");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQaAuditStorageItem(String zid, String item, String logInfo) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		QaAudit pt = commonDAO.get(QaAudit.class, zid);
		for (Map<String, Object> map : list) {
			QaAuditStorageItem scp = new QaAuditStorageItem();
			// 将map信息读入实体类
			scp = (QaAuditStorageItem) qaAuditDao.Map2Bean(map, scp);
			if (scp.getId() == null || "".equals(scp.getId())) {
				scp.setId(null);
			}
			scp.setQaAudit(pt);
			commonDAO.saveOrUpdate(scp);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(zid);
			li.setClassName("QaAuditItem");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQaAuditItem(String[] ids) {
		for (String id : ids) {
			QaAuditItem scp = qaAuditDao.get(QaAuditItem.class, id);
			if (scp != null) {
				qaAuditDao.delete(scp);
			}
			String pc = "不合格管理:"+"产品:"+scp.getProductName()+",产品批号:"+scp.getProductBatchNumber()+"的数据已删除";
			if (pc != null && !"".equals(pc)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("QaAudit");
				li.setFileId(scp.getQaAudit().getId());
				li.setModifyContent(pc);
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}

		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQaAuditStorageItem(String[] ids) {
		for (String id : ids) {
			QaAuditStorageItem scp = qaAuditDao.get(QaAuditStorageItem.class, id);
			if (scp != null) {
				qaAuditDao.delete(scp);
			}
			
			String pc = "不合格管理:"+"物料编号:"+scp.getMaterielId()+",物料:"+scp.getMaterielName()+"的数据已删除";
			if (pc != null && !"".equals(pc)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("QaAudit");
				li.setFileId(scp.getQaAudit().getId());
				li.setModifyContent(pc);
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}

		}
	}

	// // 查询子表记录
	// public List<SampleOrderItem> querySampleItem(String id) {
	// return this.sampleOrderDao.querySampleItem(id);
	// }
	//
	// public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
	// return this.sampleOrderDao.querySamplePersonnel(id);
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSampleOrderPersonnel(SampleOrder sc, String itemDataJson)
	// throws Exception {
	// List<SampleOrderPersonnel> saveItems = new ArrayList<SampleOrderPersonnel>();
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
	// itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// SampleOrderPersonnel scp = new SampleOrderPersonnel();
	// // 将map信息读入实体类
	// scp = (SampleOrderPersonnel) sampleOrderDao.Map2Bean(map, scp);
	// if (scp.getId() != null) {
	// if (scp.getId().equals(""))
	// scp.setId(null);
	// }
	// scp.setSampleOrder(sc);
	// saveItems.add(scp);
	// }
	// sampleOrderDao.saveOrUpdateAll(saveItems);
	// }
	//
	// /**
	// * 删除明细
	// *
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delSampleOrderPersonnel(String[] ids) throws Exception {
	// for (String id : ids) {
	// SampleOrderPersonnel scp = sampleOrderDao.get(
	// SampleOrderPersonnel.class, id);
	// sampleOrderDao.delete(scp);
	// }
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSampleOrderItem(SampleOrder sc, String itemDataJson)
	// throws Exception {
	// List<SampleOrderItem> saveItems = new ArrayList<SampleOrderItem>();
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
	// itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// SampleOrderItem scp = new SampleOrderItem();
	// // 将map信息读入实体类
	// scp = (SampleOrderItem) sampleOrderDao.Map2Bean(map, scp);
	// if (scp.getId() != null && scp.getId().equals(""))
	// scp.setId(null);
	// scp.setSampleOrder(sc);
	//
	// saveItems.add(scp);
	// }
	// sampleOrderDao.saveOrUpdateAll(saveItems);
	// }
	//
	// /**
	// * 删除明细
	// *
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delSampleOrderItem(String[] ids) throws Exception {
	// for (String id : ids) {
	// SampleOrderItem scp = sampleOrderDao.get(SampleOrderItem.class, id);
	// sampleOrderDao.delete(scp);
	// }
	// }
	//
	// // 根据模板ID加载子表明细
	// public List<Map<String, String>> setTemplateItem(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = this.sampleOrderDao.setTemplateItem(code);
	// List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>)
	// result
	// .get("list");
	// if (list != null && list.size() > 0) {
	// for (SampleCancerTempPersonnel ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("checkOutTheAge", ti.getCheckOutTheAge());
	// if (ti.getSampleCancerTemp().getOrderNumber() != null)
	// map.put("sampleorder", ti.getSampleCancerTemp()
	// .getOrderNumber());
	// else
	// map.put("sampleorder", "");
	// if (ti.getTumorCategory() != null)
	// map.put("tumorCategory", ti.getTumorCategory().getId());
	// else
	// map.put("tumorCategory", "");
	// map.put("familyRelation", ti.getFamilyRelation());
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }
	//
	// // 根据模板ID加载子表明细2
	// public List<Map<String, String>> setSampleOrderItem2(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = this.sampleOrderDao
	// .setSampleOrderItem(code);
	// List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result
	// .get("list");
	// if (list != null && list.size() > 0) {
	// for (SampleCancerTempItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("drugDate", ti.getDrugDate().toString());
	// map.put("useDrugName", ti.getUseDrugName());
	// map.put("effectOfProgress", ti.getEffectOfProgress());
	// map.put("effectOfProgressSpeed", ti.getEffectOfProgressSpeed());
	// map.put("geneticTestHistory", ti.getGeneticTestHistory());
	// map.put("sampleDetectionName", ti.getSampleDetectionName());
	// map.put("sampleExonRegion", ti.getSampleExonRegion());
	// map.put("sampleDetectionResult", ti.getSampleDetectionResult());
	//
	// map.put("sampleOrder", ti.getSampleCancerTemp()
	// .getOrderNumber());
	//
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }
	//
	// @WriteOperLogTable
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(SampleOrder sc, Map jsonMap) throws Exception {
	// if (sc != null) {
	// sampleOrderDao.saveOrUpdate(sc);
	// String jsonStr = "";
	// jsonStr = (String) jsonMap.get("sampleOrderPersonnel");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveSampleOrderPersonnel(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("sampleOrderItem");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveSampleOrderItem(sc, jsonStr);
	// }
	// }
	// }
	//
	// public SampleCancerTemp fuzZhi(SampleOrder so) throws Exception {
	// SampleCancerTemp a = new SampleCancerTemp();
	// a.setOrderNumber(so.getId());
	// a.setId(so.getId());
	// a.setName(so.getName());
	// a.setGender(so.getGender());
	// a.setBirthDate(so.getBirthDate());
	// a.setDiagnosisDate(DateUtil.parse(so.getDiagnosisDate()));
	// a.setDicType(so.getDicType());
	// a.setSampleStage(so.getSampleStage());
	// a.setInspectionDepartment(so.getInspectionDepartment());
	// a.setCrmProduct(so.getCrmProduct());
	// a.setProductId(so.getProductId());
	// a.setProductName(so.getProductName());
	// a.setSamplingDate(so.getSamplingDate());
	// a.setSamplingLocation(so.getSamplingLocation());
	// a.setSamplingNumber(so.getSamplingNumber());
	// a.setPathologyConfirmed(so.getPathologyConfirmed());
	// // a.setBloodSampleDate(so.getBloodSampleDate().toString());
	// // a.setPlasmapheresisDate(so.getPlasmapheresisDate());
	// a.setCommissioner(so.getCommissioner());
	// a.setReceivedDate(so.getReceivedDate());
	// a.setSampleTypeId(so.getSampleTypeId());
	// a.setSampleTypeName(so.getSampleTypeName());
	// a.setMedicalNumber(so.getMedicalNumber());
	// a.setSampleCode(so.getSampleCode());
	// a.setFamily(so.getFamily());
	// a.setFamilyPhone(so.getFamilyPhone());
	// a.setFamilySite(so.getFamilySite());
	// a.setCrmCustomer(so.getCrmCustomer());
	// a.setMedicalInstitutions(so.getMedicalInstitutions());
	// a.setMedicalInstitutionsPhone(so.getMedicalInstitutionsPhone());
	// a.setMedicalInstitutionsSite(so.getMedicalInstitutionsSite());
	// a.setCrmDoctor(so.getCrmDoctor());
	// a.setAttendingDoctor(so.getAttendingDoctor());
	// a.setAttendingDoctorPhone(so.getAttendingDoctorPhone());
	// a.setAttendingDoctorSite(so.getAttendingDoctorSite());
	// a.setNote(so.getNote());
	// a.setCreateUser(so.getCreateUser());
	// a.setCreateDate(so.getCreateDate());
	// a.setConfirmUser(so.getConfirmUser());
	// a.setConfirmDate(so.getConfirmDate());
	// a.setState(so.getState());
	// a.setStateName(so.getStateName());
	//
	// return a;
	// }
	//
	// @WriteOperLogTable
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void chengeState(String sampleOrderId) throws Exception {
	//
	// SampleOrder sampleOrder = commonDAO.get(SampleOrder.class,
	// sampleOrderId);
	// User user = (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY);
	// CrmPatient r = null;
	// // 电子病历号为空,创建一个
	// if (sampleOrder.getMedicalNumber() != null
	// && !"".equals(sampleOrder.getMedicalNumber())) {
	// r = crmPatientService.get(sampleOrder.getMedicalNumber());
	// // 该电子病历记录存在: 修改电子病历
	// r.setId(sampleOrder.getMedicalNumber());
	// r.setName(sampleOrder.getName());
	// r.setGender(sampleOrder.getGender());
	// r.setDateOfBirth(sampleOrder.getBirthDate());
	// r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
	// r.setCreateDate(sampleOrder.getCreateDate());
	// r.setCustomer(sampleOrder.getCrmCustomer());
	// r.setCustomerDoctor(sampleOrder.getCrmDoctor());
	// r.setCreateUser(sampleOrder.getCreateUser());
	// r.setCancerType(sampleOrder.getCancerType());
	// r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
	// r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
	// r.setKs(sampleOrder.getInspectionDepartment());
	// crmPatientService.save(r);
	//
	// } else {
	// String modelName = "CrmPatient";
	// String markCode = "P";
	// String autoID = codingRuleService.genTransID(modelName, markCode);
	// sampleOrder.setMedicalNumber(autoID);
	// // 该电子记录不存在:添加电子病历
	// r = new CrmPatient();
	// r.setId(autoID);
	// r.setId(sampleOrder.getMedicalNumber());
	// r.setCustomer(sampleOrder.getCrmCustomer());
	// r.setCustomerDoctor(sampleOrder.getCrmDoctor());
	// r.setName(sampleOrder.getName());
	// r.setGender(sampleOrder.getGender());
	// r.setDateOfBirth(sampleOrder.getBirthDate());
	// r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
	// r.setCreateDate(sampleOrder.getCreateDate());
	// r.setCreateUser(sampleOrder.getCreateUser());
	// r.setCancerType(sampleOrder.getCancerType());
	// r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
	// r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
	// r.setKs(sampleOrder.getInspectionDepartment());
	// crmPatientService.save(r);
	// }
	// sampleOrder.setConfirmUser(user);
	// sampleOrder.setConfirmDate(new Date());
	// sampleOrder.setState("1");
	// sampleOrder.setStateName("完成");
	//
	// }
	//
	//
	//
	// /**
	// * 根据编号查询代理商
	// */
	// public List<Map<String, Object>> findPrimaryToSample(String code)
	// throws Exception {
	// List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
	// Map<String, Object> result = findPrimary(code);
	// List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (PrimaryTask srai : list) {
	// Map<String, Object> map = new HashMap<String, Object>();
	// map.put("id", srai.getId());
	// map.put("name", srai.getName());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }
	//
	// public Map<String, Object> findPrimary(String code) throws Exception {
	// String[] codes = code.split(",");
	//
	// List<PrimaryTask> list = new ArrayList<PrimaryTask>();
	// for (int i = 0; i < codes.length; i++) {
	// PrimaryTask pro = commonDAO.get(PrimaryTask.class, codes[i].trim());
	// list.add(pro);
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("list", list);
	// return result;
	// }
	//
	// //按年查询订单柱状图
	// public List selOrderNumberByYear(String dateYear) {
	// return sampleOrderDao.findOrderNumberByYear(dateYear);
	// }
	// //按年查询订单中检测项目病状图
	// public List selProductByOrderByYear(String dateYear) {
	// return sampleOrderDao.findProductByOrderByYear(dateYear);
	// }
	// //按月查询订单中检测项目病状图
	// public List selProductByOrderByMonth(String dateYearMonth) {
	// return sampleOrderDao.findProductByOrderByMonth(dateYearMonth);
	// }
	// //查询订单中图片路径
	// public List<String> selPic(String id) {
	// return sampleOrderDao.findPic(id);
	// }
	//
	// public List<FileInfo> findFileInfoList(String sId) throws Exception {
	// return sampleOrderDao.selectFileInfoList(sId);
	// }
	//
	// public void getCsvContent(String id, String fileId) throws Exception {
	//
	// FileInfo fileInfo = sampleOrderDao.get(FileInfo.class, fileId);
	// String filepath = fileInfo.getFilePath();
	// File a = new File(filepath);
	// if (a.isFile()) {
	// SampleOrder so=sampleOrderDao.get(SampleOrder.class, id);
	// InputStream is = new FileInputStream(filepath);
	// if (is != null) {
	// CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
	// SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd");
	// reader.readHeaders();// 去除表头
	// while (reader.readRecord()) {
	// SampleOrderItem sampleOrderItem = new SampleOrderItem();
	// sampleOrderItem.setName(reader.get(0));
	// if("男".equals(reader.get(1))){
	// sampleOrderItem.setGender("1");
	// }else if("女".equals(reader.get(1))){
	// sampleOrderItem.setGender("0");
	// }
	// sampleOrderItem.setSampleCode(reader.get(2));
	// sampleOrderItem.setSlideCode(reader.get(3));
	// DicSampleType dst=dicSampleTypeDao.selectDicSampleTypeByName(reader.get(4));
	// sampleOrderItem.setSampleType(dst);
	// sampleOrderItem.setSamplingDate(sdf.parse(reader.get(5)));
	// sampleOrderItem.setSampleOrder(so);
	// sampleOrderDao.saveOrUpdate(sampleOrderItem);
	// }
	// }}
	// }
	//
	//
}
