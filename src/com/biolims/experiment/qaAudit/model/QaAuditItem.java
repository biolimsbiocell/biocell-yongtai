package com.biolims.experiment.qaAudit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @author created by yunhe.Qin
 * @date 2019年5月8日---上午10:10:24
 */
@Entity
@Table(name = "QA_AUDIT_ITEM")
@SuppressWarnings("serial")
public class QaAuditItem extends EntityDao<QaAuditItem> implements java.io.Serializable, Cloneable {

	/** 编码 */
	private String id;
	/** 产品名称 */
	private String productName;
	/** 产品批号 */
	private String productBatchNumber;
	/** 产品规格 */
	private String productSpecifications;
	/** 备注 */
	private String note;
	/** 主表ID */
	private QaAudit qaAudit;
	
	/** 不合格原因 */
	private String unqualifiedReason;
	/** 数量 */
	private String productNumber;
	
	

	public String getUnqualifiedReason() {
		return unqualifiedReason;
	}

	public void setUnqualifiedReason(String unqualifiedReason) {
		this.unqualifiedReason = unqualifiedReason;
	}

	public String getProductNumber() {
		return productNumber;
	}

	public void setProductNumber(String productNumber) {
		this.productNumber = productNumber;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public String getProductName() {
		return productName;
	}

	public String getProductBatchNumber() {
		return productBatchNumber;
	}

	public String getProductSpecifications() {
		return productSpecifications;
	}

	public String getNote() {
		return note;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setProductBatchNumber(String productBatchNumber) {
		this.productBatchNumber = productBatchNumber;
	}

	public void setProductSpecifications(String productSpecifications) {
		this.productSpecifications = productSpecifications;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public QaAudit getQaAudit() {
		return qaAudit;
	}

	public void setQaAudit(QaAudit qaAudit) {
		this.qaAudit = qaAudit;
	}

}
