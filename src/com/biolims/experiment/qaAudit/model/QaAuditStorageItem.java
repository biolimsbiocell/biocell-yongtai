package com.biolims.experiment.qaAudit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @author
 * @date 
 */
@Entity
@Table(name = "QA_AUDIT_STORAGE_ITEM")
@SuppressWarnings("serial")
public class QaAuditStorageItem extends EntityDao<QaAuditStorageItem> implements java.io.Serializable, Cloneable {

	/** 编码 */
	private String id;
	/** 物料名称 */
	private String materielName;
	/** 物料批号 */
	private String materielBatchNumber;
	/** 物料规格 */
	private String materielSpecifications;
	/** 备注 */
	private String note;
	/** 主表ID */
	private QaAudit qaAudit;
	
	/** 不合格原因 */
	private String unqualifiedReason;
	/** 数量 */
	private String materielNumber;
	/** 物料编号 */
	private String materielId;
	
	

	public String getMaterielId() {
		return materielId;
	}

	public void setMaterielId(String materielId) {
		this.materielId = materielId;
	}

	public String getMaterielNumber() {
		return materielNumber;
	}

	public void setMaterielNumber(String materielNumber) {
		this.materielNumber = materielNumber;
	}

	public String getMaterielName() {
		return materielName;
	}

	public void setMaterielName(String materielName) {
		this.materielName = materielName;
	}

	public String getMaterielBatchNumber() {
		return materielBatchNumber;
	}

	public void setMaterielBatchNumber(String materielBatchNumber) {
		this.materielBatchNumber = materielBatchNumber;
	}

	public String getMaterielSpecifications() {
		return materielSpecifications;
	}

	public void setMaterielSpecifications(String materielSpecifications) {
		this.materielSpecifications = materielSpecifications;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUnqualifiedReason() {
		return unqualifiedReason;
	}

	public void setUnqualifiedReason(String unqualifiedReason) {
		this.unqualifiedReason = unqualifiedReason;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public QaAudit getQaAudit() {
		return qaAudit;
	}

	public void setQaAudit(QaAudit qaAudit) {
		this.qaAudit = qaAudit;
	}

}
