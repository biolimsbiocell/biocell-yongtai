package com.biolims.experiment.qaAudit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: QA流程
 * @author lims-platform
 * @date 2016-03-07 11:01:47
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QA_AUDIT")
@SuppressWarnings("serial")
public class QaAudit extends EntityDao<QaAudit> implements java.io.Serializable, Cloneable {
	/** 编码 */
	private String id;
	/** 产品名称 */
	private String productName;
	/** 产品批号 */
	private String productBatchNumber;
	/** 产品规格 */
	private String productSpecifications;
	/** 数量 */
	private String num;
	/** 样本编号 */
	private String sampleCode;
	/** 事件类型 */
	private DicType eventType;
	/** 描述 */
	private String describe;
	/** 创建日期 */
	private Date createDate;
	/** 创建人 */
	private User createUser;
	/** 审核人1 */
	private User auditor1;
	/** 审核人2 */
	private User auditor2;
	/** 审核人3 */
	private User auditor3;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 备注 */
	private String name;
	/** 调查组组长 */
	private User groupLeader;
	/** 调查组组员ID */
	private String groupMemberIds;
	private String groupMembers;
	/** 部门负责人 */
	private User departmentUser;
	/** 监控人 */
	private User monitor;

	public String getGroupMemberIds() {
		return groupMemberIds;
	}

	public String getGroupMembers() {
		return groupMembers;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getDepartmentUser() {
		return departmentUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getMonitor() {
		return monitor;
	}

	public void setGroupMemberIds(String groupMemberIds) {
		this.groupMemberIds = groupMemberIds;
	}

	public void setGroupMembers(String groupMembers) {
		this.groupMembers = groupMembers;
	}

	public void setDepartmentUser(User departmentUser) {
		this.departmentUser = departmentUser;
	}

	public void setMonitor(User monitor) {
		this.monitor = monitor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getGroupLeader() {
		return groupLeader;
	}

	public void setGroupLeader(User groupLeader) {
		this.groupLeader = groupLeader;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductBatchNumber() {
		return productBatchNumber;
	}

	public void setProductBatchNumber(String productBatchNumber) {
		this.productBatchNumber = productBatchNumber;
	}

	public String getProductSpecifications() {
		return productSpecifications;
	}

	public void setProductSpecifications(String productSpecifications) {
		this.productSpecifications = productSpecifications;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "EVENT_TYPE")
	public DicType getEventType() {
		return eventType;
	}

	public void setEventType(DicType eventType) {
		this.eventType = eventType;
	}

	@Column(name = "NOTE", length = 4000)
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITOR1")
	public User getAuditor1() {
		return auditor1;
	}

	public void setAuditor1(User auditor1) {
		this.auditor1 = auditor1;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITOR2")
	public User getAuditor2() {
		return auditor2;
	}

	public void setAuditor2(User auditor2) {
		this.auditor2 = auditor2;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDITOR3")
	public User getAuditor3() {
		return auditor3;
	}

	public void setAuditor3(User auditor3) {
		this.auditor3 = auditor3;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

}
