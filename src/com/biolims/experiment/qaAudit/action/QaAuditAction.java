package com.biolims.experiment.qaAudit.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qaAudit.model.QaAudit;
import com.biolims.experiment.qaAudit.model.QaAuditItem;
import com.biolims.experiment.qaAudit.model.QaAuditStorageItem;
import com.biolims.experiment.qaAudit.service.QaAuditService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/qaAudit/qaAudit")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QaAuditAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240190";
	@Autowired
	private QaAuditService qaAuditService;
	@Resource
	private CommonDAO commonDAO;
	private QaAudit qaAudit = new QaAudit();
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FieldService fieldService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SampleOrderService sampleOrderService;
	@Resource
	private StorageService storageService;
	
	

	@Action(value = "showqaAuditTable")
	public String showqaAuditTable() throws Exception {
		String id = getParameterFromRequest("id");
		qaAudit = qaAuditService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qaAudit/qaAudit.jsp");
	}

	@Action(value = "showqaAuditTableJson")
	public void showqaAuditTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = qaAuditService.findQaAuditTable(start, length, query, col, sort);
			List<QaAudit> list = (List<QaAudit>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("eventType-id", "");
			map.put("eventType-name", "");
			map.put("productName", "");
			map.put("name", "");
			map.put("productBatchNumber", "");
			map.put("productSpecifications", "");
			map.put("num", "");
			map.put("sampleCode", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SampleOrder");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showQaAuditItemTableJson")
	public void showQaAuditItemTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String id = getParameterFromRequest("id");
			Map<String, Object> result = qaAuditService.showQaAuditItemTableJson(start, length, query, col, sort, id);
			List<QaAuditItem> list = (List<QaAuditItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("productName", "");
			map.put("productBatchNumber", "");
			map.put("note", "");
			map.put("qaAudit-id", "");
			
			map.put("unqualifiedReason", "");
			map.put("productSpecifications", "");
			map.put("productNumber", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Action(value = "showQaAuditStorageItemTableJson")
	public void showQaAuditStorageItemTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String id = getParameterFromRequest("id");
			Map<String, Object> result = qaAuditService.showQaAuditStorageItemTableJson(start, length, query, col, sort, id);
			List<QaAuditStorageItem> list = (List<QaAuditStorageItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("materielName", "");
			map.put("materielBatchNumber", "");
			map.put("note", "");
			map.put("qaAudit-id", "");
			
			map.put("unqualifiedReason", "");
			map.put("materielSpecifications", "");
			map.put("materielNumber", "");
			map.put("materielId", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "saveQaAuditItem")
	public void saveQaAuditItem() throws Exception {
		String zid = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qaAuditService.saveQaAuditItem(zid, item, logInfo);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	@Action(value = "saveQaAuditStorageItem")
	public void saveQaAuditStorageItem() throws Exception {
		String zid = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qaAuditService.saveQaAuditStorageItem(zid, item, logInfo);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "delQaAuditItem")
	public void delQaAuditItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaAuditService.delQaAuditItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "delQaAuditStorageItem")
	public void delQaAuditStorageItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qaAuditService.delQaAuditStorageItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "editQaAudit")
	public String editQaAudit() throws Exception {
		String id = getParameterFromRequest("id");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		rightsId = "240190";
		long num = 0;
		if (id != null && !id.equals("")) {
			this.qaAudit = qaAuditService.get(id);
			if (qaAudit == null) {
				qaAudit = new QaAudit();
				qaAudit.setId(id);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				qaAudit.setCreateUser(user);
				qaAudit.setCreateDate(new Date());
				qaAudit.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				qaAudit.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("bpmTaskId", bpmTaskId);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("bpmTaskId", bpmTaskId);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "qaAudit");
			}

		} else {
			qaAudit.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qaAudit.setCreateUser(user);
			qaAudit.setCreateDate(new Date());
			qaAudit.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qaAudit.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		toState(qaAudit.getState());
		return dispatcher("/WEB-INF/page/experiment/qaAudit/qaAuditEdit.jsp");
	}

	/**
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		synchronized (QaAuditAction.class) {
			String id = qaAudit.getId();
			String log = "";
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				log="123";
				String modelName = "QaAudit";
				String markCode = "QA";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);

				qaAudit.setId(autoID);
			}
			String changeLog = getParameterFromRequest("changeLog");
			Map aMap = new HashMap();
			aMap.put("qaAuditItem", getParameterFromRequest("qaAuditItemJson"));
			aMap.put("qaAuditStorageItem", getParameterFromRequest("qaAuditStorageItemJson"));
			String changeLogItem = getParameterFromRequest("changeLogItem");
			String string = qaAuditService.save(qaAudit, changeLog, aMap, changeLogItem,log);
			String[] split = string.split(",");
			String samId = split[0];

			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			String url = "/experiment/qaAudit/qaAudit/editQaAudit.action?id=" + samId;
			if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
				url += "&bpmTaskId=" + bpmTaskId;
			}
			return redirect(url);
		}
	}
	
	/***
	 * Dialog
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleOrderDialogList")
	public String showSampleOrderDialogList() throws Exception {
		String flag = getParameterFromRequest("flag");
		String mark = getParameterFromRequest("mark");
		putObjToContext("flag", flag);
		putObjToContext("mark", mark);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qaAudit/sampleOrderDialog.jsp");
	}

	/**
	 * 展示所有得订单
	 * 
	 * @Title: showAllSampleOrderDialogListJson @Description: TODO @param @throws
	 *         Exception @return void @author 孙灵达 @date 2018年8月14日 @throws
	 */
	@Action(value = "showAllSampleOrderDialogListJson")
	public void showAllSampleOrderDialogListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleOrderService.findSampleOrderTable(null, start, length, query, col, sort);
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("productName", "");
			map.put("stateName", "");
			map.put("barcode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @Title: getStrogeReagent
	 * @Description: 选择试剂明细
	 * @author : shengwei.wang
	 * @date 2018年1月26日上午11:33:23
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "getStrogeReagent")
	public String getStrogeReagent() {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/qaAudit/reagentTemplateDialog.jsp");
	}

	@Action(value = "getStrogeReagentJson")
	public void getStrogeReagentJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = storageService.getStrogeReagents(id, start,
				length, query, col, sort);
		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("note", "");
		map.put("serial", "");
		map.put("expireDate", "yyyy-MM-dd");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QaAuditService getQaAuditService() {
		return qaAuditService;
	}

	public void setQaAuditService(QaAuditService qaAuditService) {
		this.qaAuditService = qaAuditService;
	}

	public QaAudit getQaAudit() {
		return qaAudit;
	}

	public void setQaAudit(QaAudit qaAudit) {
		this.qaAudit = qaAudit;
	}

	public CodingRuleService getCodingRuleService() {
		return codingRuleService;
	}

	public void setCodingRuleService(CodingRuleService codingRuleService) {
		this.codingRuleService = codingRuleService;
	}

	// @Action(value = "showStep1")
	// public String showStep1() throws Exception {
	// String id = getParameterFromRequest("id");
	// sampleOrder = sampleOrderService.get(id);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// return dispatcher("/WEB-INF/page/system/sample/showStep1.jsp");
	// }
	//
	// @Action(value = "showStep2")
	// public String showStep2() throws Exception {
	// String id = getParameterFromRequest("id");
	// putObjToContext("id", id);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// return dispatcher("/WEB-INF/page/system/sample/showStep2.jsp");
	// }
	//
	// @Action(value = "showStep3")
	// public String showStep3() throws Exception {
	// String id = getParameterFromRequest("id");
	// putObjToContext("id", id);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// return dispatcher("/WEB-INF/page/system/sample/showStep3.jsp");
	// }
	//
	// /**
	// * 任务看板 @Title: showSampleOrderTaskKanban @Description:
	// * TODO @param @return @param @throws Exception @return String @author
	// * 孙灵达 @date 2018年9月7日 @throws
	// */
	// @Action(value = "showSampleOrderTaskKanban")
	// public String showSampleOrderTaskKanban() throws Exception {
	// rightsId = "2811";
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/sample/taskKanbanTable.jsp");
	// }
	//
	// @Action(value = "showSampleOrderTaskKanbanJson")
	// public void showSampleOrderTaskKanbanJson() throws Exception {
	// String query = getRequest().getParameter("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// Map<String, Object> result =
	// sampleOrderService.showSampleOrderTaskKanban(start, length, query, col,
	// sort);
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("techJkService-id", "");
	// map.put("techJkService-confirmDate", "");
	// map.put("id", "");
	// map.put("productName", "");
	// map.put("newTask", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// // 根据模块查询自定义字段数据
	// Map<String, Object> mapField =
	// fieldService.findFieldByModuleValue("SampleOrder");
	// String dataStr = PushData.pushFieldData(data, mapField);
	// HttpUtils.write(PushData.pushData(draw, result, dataStr));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }
	//
	// @Action(value = "showSampleStateNewListJson")
	// public void showSampleStatenewListJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String id = getParameterFromRequest("id");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// Map<String, Object> result = sampleOrderService.findSampleStateNewList(start,
	// length, query, col, sort, id);
	// List<SampleState> list = (List<SampleState>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("sampleCode", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("stageTime", "");
	// map.put("startDate", "");
	// map.put("endDate", "");
	// map.put("tableTypeId", "");
	// map.put("stageName", "");
	// map.put("acceptUser-id", "");
	// map.put("acceptUser-name", "");
	// map.put("taskId", "");
	//
	// map.put("taskMethod", "");
	// map.put("taskResult", "");
	// map.put("techTaskId", "");
	// map.put("note", "");
	// map.put("note2", "");
	// map.put("note3", "");
	//
	// map.put("note4", "");
	// map.put("note5", "");
	// map.put("note6", "");
	// map.put("note7", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// }
	//
	// /**
	// * 查看任务看板 @Title: viewSampleOrderKanban @Description:
	// * TODO @param @return @param @throws Exception @return String @author
	// * 孙灵达 @date 2018年9月7日 @throws
	// */
	// @Action(value = "viewSampleOrderKanban")
	// public String viewSampleOrderKanban() throws Exception {
	// String id = getParameterFromRequest("id");
	// sampleOrder = sampleOrderService.get(id);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// return dispatcher("/WEB-INF/page/system/sample/taskKanbanEdit.jsp");
	// }
	//
	// /**
	// * 跟据录入的出生日期回填年龄 @Title: fillAgeByBirthDate @Description:
	// * TODO @param @return void @author 孙灵达 @date 2018年9月7日 @throws
	// */
	// @Action(value = "fillAgeByBirthDate")
	// public void fillAgeByBirthDate() {
	// String birthday = getParameterFromRequest("birth");
	// Calendar instance = Calendar.getInstance();
	// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	// String nowDate = simpleDateFormat.format(instance.getTime());
	// int age = Integer.parseInt(nowDate.substring(0, 4)) -
	// Integer.parseInt(birthday.substring(0, 4)) + 1;
	// String strAge = Integer.toString(age);
	// HttpUtils.write(strAge);
	// }
	//
	// /**
	// * 展示状态完成的订单
	// *
	// * @Title: showStateCompleteSampleOrderList @Description:
	// * TODO @param @return void @author 孙灵达 @date 2018年8月28日 @throws
	// */
	// /*
	// * @Action(value = "showStateCompleteSampleOrderList") public void
	// * showStateCompleteSampleOrderList() { String draw =
	// * getParameterFromRequest("draw"); try { Map<String, Object> result =
	// * sampleOrderService.showStateCompleteSampleOrderList(); List<SampleOrder>
	// * list = (List<SampleOrder>) result.get("list"); Map<String, String> map =
	// * new HashMap<String, String>(); map.put("id", ""); map.put("name", "");
	// * map.put("gender", ""); map.put("age", ""); map.put("medicalNumber", "");
	// * map.put("stateName", ""); String data = new
	// * SendData().getDateJsonForDatatable(map, list);
	// * HttpUtils.write(PushData.pushData(draw, result, data)); } catch
	// * (Exception e) { e.printStackTrace(); } }
	// */
	//
	// /**
	// * @throws Exception
	// * 通过id查询订单 @Title: findSampleOrderById @Description:
	// * TODO @param @return void @author 孙灵达 @date 2018年8月14日 @throws
	// */
	// @Action(value = "findSampleOrderById")
	// public void findSampleOrderById() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// String id = getParameterFromRequest("id");
	// SampleOrder so = commonDAO.get(SampleOrder.class, id);
	// Date birthDate = so.getBirthDate();
	// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	// String birth = format.format(birthDate);
	// map.put("sampleOrder", so);
	// map.put("birthDate", birth);
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// /***
	// * Dialog
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "showSampleOrderDialogList")
	// public String showSampleOrderDialogList() throws Exception {
	// String flag = getParameterFromRequest("flag");
	// String mark = getParameterFromRequest("mark");
	// putObjToContext("flag", flag);
	// putObjToContext("mark", mark);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/sample/sampleOrderDialog.jsp");
	// }
	//
	// @Action(value = "showSampleOrderDialogListJson")
	// public void showSampleOrderDialogListJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// Map<String, Object> result =
	// sampleOrderService.findSampleOrderDialogList(start, length, query, col,
	// sort);
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("age", "");
	// map.put("productName", "");
	// map.put("stateName", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// /**
	// * 展示所有得订单
	// *
	// * @Title: showAllSampleOrderDialogListJson @Description:
	// * TODO @param @throws Exception @return void @author 孙灵达 @date
	// * 2018年8月14日 @throws
	// */
	// @Action(value = "showAllSampleOrderDialogListJson")
	// public void showAllSampleOrderDialogListJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// Map<String, Object> result = sampleOrderService.findSampleOrderTable(null,
	// start, length, query, col, sort);
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("age", "");
	// map.put("productName", "");
	// map.put("stateName", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// @Action(value = "showSampleOrderSelList")
	// public String showSampleOrderSelList() throws Exception {
	// String orderNum = getParameterFromRequest("orderNum");
	// putObjToContext("orderNum", orderNum);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/sample/sampleOrderSel.jsp");
	// }
	//
	// @Action(value = "showSampleOrderSelListJson")
	// public void showSampleOrderSelListJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// String orderNum = getParameterFromRequest("orderNum");
	// try {
	// Map<String, Object> result =
	// sampleOrderService.findSampleOrderSelTable(start, length, query, col, sort,
	// orderNum);
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("age", "");
	// map.put("medicalNumber", "");
	// map.put("stateName", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// /***
	// * 添加到Item表
	// *
	// * @throws Exception
	// */
	// @Action(value = "addPurchaseApplyMakeUp")
	// public void addPurchaseApplyMakeUp() throws Exception {
	// String id = getParameterFromRequest("id");
	// String sampleOrderChangId = getParameterFromRequest("sampleOrderChangId");
	// SampleOrderChange sa = new SampleOrderChange();
	// if ((sampleOrderChangId != null && !"".equals(sampleOrderChangId)) ||
	// sampleOrderChangId.equals("NEW")) {
	// String modelName = "SampleOrderChange";
	// String markCode = "SOC";
	// Date date = new Date();
	// DateFormat format = new SimpleDateFormat("yy");
	// String stime = format.format(date);
	// String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime,
	// 000000, 6, null);
	// sa.setId(autoID);
	// sa.setChangeType("取消申请单");
	// sa.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// sa.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// sa.setCreateUser(user);
	// sa.setCreateDate(new Date());
	// commonDAO.saveOrUpdate(sa);
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// String itemId = sampleOrderService.addPurchaseApplyMakeUp(id, sa);
	// result.put("success", true);
	// result.put("itemId", itemId);
	// result.put("id", sa.getId());
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 列表页面
	// @Action(value = "showSampleOrderTable")
	// public String showSampleOrderTable() throws Exception {
	// String type = getRequest().getParameter("type");
	// if (type.equals("0")) {// 用户订单
	// rightsId = "980702";
	// } else {// 客服订单
	// rightsId = "980802";
	// }
	// putObjToContext("type", type);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/sample/sampleOrderMain.jsp");
	// }
	//
	// @Action(value = "showSampleOrderTableJson")
	// public void showSampleOrderTableJson() throws Exception {
	// String query = getRequest().getParameter("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// String type = getRequest().getParameter("type");
	// Map<String, Object> result = sampleOrderService.findSampleOrderTable(type,
	// start, length, query, col, sort);
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("age", "");
	// map.put("nation", "");
	// // map.put("orderType", "");
	// map.put("email", "");
	// map.put("zipCode", "");
	// map.put("diagnosisDate", "");
	// map.put("commissioner-id", "");
	// map.put("commissioner-name", "");
	// map.put("nativePlace", "");
	// map.put("medicalNumber", "");
	// map.put("familyCode", "");
	// map.put("family", "");
	// map.put("familyPhone", "");
	// map.put("familySite", "");
	// map.put("crmCustomer-id", "");
	// map.put("crmCustomer-name", "");
	// map.put("createUser-id", "");
	// map.put("createUser-name", "");
	// map.put("stateName", "");
	// map.put("barcode", "");
	// map.put("subjectID", "");
	// map.put("familyHistorysummary", "");
	// map.put("prenatal", "");
	// map.put("hospitalPatientID", "");
	// map.put("weights", "");
	// map.put("gestationalWeeks", "");
	// map.put("birthDate", "yyyy-MM-dd");
	// map.put("createDate", "yyyy-MM-dd");
	// map.put("idCard", "");
	// map.put("productName", "");
	// map.put("fieldContent", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// // 根据模块查询自定义字段数据
	// Map<String, Object> mapField =
	// fieldService.findFieldByModuleValue("SampleOrder");
	// String dataStr = PushData.pushFieldData(data, mapField);
	// HttpUtils.write(PushData.pushData(draw, result, dataStr));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }
	//
	// /**
	// * 异步加载代理商名称
	// *
	// * @return
	// */
	// @Action(value = "findPrimaryToSample", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void findPrimaryToSample() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, Object>> dataListMap =
	// this.sampleOrderMainService.findPrimaryToSample(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // @Action(value = "sampleOrderSelect", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public String showDialogSampleOrderList() throws Exception {
	// // putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// // toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// // return
	// // dispatcher("/WEB-INF/page/system/sample/sampleOrderMainDialog.jsp");
	// // }
	// //
	// // @Action(value = "showDialogSampleOrderListJson", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public void showDialogSampleOrderListJson() throws Exception {
	// // int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // String dir = getParameterFromRequest("dir");
	// // String sort = getParameterFromRequest("sort");
	// // String data = getParameterFromRequest("data");
	// // Map<String, String> map2Query = new HashMap<String, String>();
	// // if (data != null && data.length() > 0)
	// // map2Query = JsonUtils.toObjectByJson(data, Map.class);
	// // Map<String, Object> result = sampleOrderService.findSampleOrderList(
	// // map2Query, startNum, limitNum, dir, sort);
	// // Long count = (Long) result.get("total");
	// // List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// //
	// // Map<String, String> map = new HashMap<String, String>();
	// // map.put("id", "");
	// // map.put("name", "");
	// // map.put("gender", "");
	// // map.put("birthDate", "yyyy-MM-dd");
	// // map.put("diagnosisDate", "");
	// // map.put("dicType-id", "");
	// // map.put("dicType-name", "");
	// // map.put("sampleStage", "");
	// // map.put("inspectionDepartment-id", "");
	// // map.put("inspectionDepartment-name", "");
	// // map.put("crmProduct-id", "");
	// // map.put("crmProduct-name", "");
	// // map.put("samplingDate", "yyyy-MM-dd");
	// // map.put("samplingLocation-id", "");
	// // map.put("samplingLocation-name", "");
	// // map.put("samplingNumber", "");
	// // map.put("pathologyConfirmed", "");
	// // map.put("bloodSampleDate", "yyyy-MM-dd");
	// // map.put("plasmapheresisDate", "yyyy-MM-dd");
	// // map.put("commissioner-id", "");
	// // map.put("commissioner-name", "");
	// // map.put("receivedDate", "yyyy-MM-dd");
	// // map.put("sampleTypeId", "");
	// // map.put("sampleTypeName", "");
	// // map.put("sampleCode", "");
	// // map.put("medicalNumber", "");
	// // map.put("familyCode", "");
	// // map.put("family", "");
	// // map.put("familyPhone", "");
	// // map.put("familySite", "");
	// // map.put("medicalInstitutions", "");
	// // map.put("medicalInstitutionsPhone", "");
	// // map.put("medicalInstitutionsSite", "");
	// // map.put("attendingDoctor", "");
	// // map.put("attendingDoctorPhone", "");
	// // map.put("attendingDoctorSite", "");
	// // map.put("note", "");
	// // map.put("createUser-id", "");
	// // map.put("createUser-name", "");
	// // map.put("createDate", "yyyy-MM-dd");
	// // map.put("confirmUser-id", "");
	// // map.put("confirmUser-name", "");
	// // map.put("confirmDate", "yyyy-MM-dd");
	// // map.put("state", "");
	// // map.put("stateName", "");
	// // map.put("sampleFlag", "");
	// // map.put("successFlag", "");
	// // map.put("age", "");
	// //
	// // map.put("barcode", "");
	// // map.put("subjectID", "");
	// // map.put("visit", "");
	// // map.put("CRCName", "");
	// // map.put("CRCPhone", "");
	// // map.put("CRCEmail", "");
	// // map.put("CRAName", "");
	// // map.put("CRAPhone", "");
	// // map.put("CRAEmail", "");
	// // map.put("sponsor", "");
	// // map.put("sponsorProjectID", "");
	// // map.put("specimenPurpose", "");
	// // map.put("blockID", "");
	// // map.put("collectionTime", "yyyy-MM-dd");
	// // map.put("sectionTime", "yyyy-MM-dd");
	// // map.put("tissueType", "");
	// // map.put("siteCollection", "");
	// // map.put("collectionMethod", "");
	// // map.put("fixedBuffer", "");
	// // map.put("histologyType", "");
	// // map.put("bloodCollectionTime", "yyyy-MM-dd");
	// //
	// // map.put("ill", "");
	// // map.put("transfusion", "");
	// // map.put("sameTime", "");
	// // map.put("nativePlace", "");
	// // map.put("subjectEmail", "");
	// // map.put("subjectPhone", "");
	// // map.put("adopted", "");
	// // map.put("summary", "");
	// // map.put("phenotype", "");
	// // map.put("familyHistory", "");
	// // map.put("familyHistorysummary", "");
	// // map.put("prenatal", "");
	// // map.put("Institute", "");
	// // map.put("hospitalPatientID", "");
	// // map.put("customerAdress", "");
	// // map.put("physicianName", "");
	// // map.put("physicianPhone", "");
	// // map.put("physicianFax", "");
	// // map.put("physicianEmail", "");
	// // map.put("type", "");
	// //
	// // map.put("bedNo", "");
	// // map.put("diagnosis", "");
	// // map.put("guomin", "");
	// //
	// // new SendData().sendDateJson(map, list, count,
	// // ServletActionContext.getResponse());
	// // }
	//
	// // @Action(value = "showHistorySelectTreeJson", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public void showHistorySelectTreeJson() throws Exception {
	// // List<DicType> listCheck1 =
	// // sampleCancerTempService.findDicSampleType1(null);
	// //
	// // String a = sampleCancerTempService.getTreeJson(listCheck1);
	// //
	// // new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// // }
	// //
	// // @Action(value = "showHistorySelectTree")
	// // public String showHistorySelectTree() throws Exception{
	// //
	// //
	// // putObjToContext(
	// // "path",
	// // ServletActionContext.getRequest().getContextPath()
	// // + "/system/sample/sampleOrder/showHistorySelectTreeJson.action");
	// //
	// // return
	// // dispatcher("/WEB-INF/page/system/sample/sampleHistorySelectTree.jsp");
	// // }
	//
	// // @Action(value = "showYongYaoSelectTreeJson", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public void showYongYaoSelectTreeJson() throws Exception {
	// // List<DicType> listCheck2 =
	// // sampleCancerTempService.findDicSampleType2(null);
	// //
	// // String a = sampleCancerTempService.getTreeJson(listCheck2);
	// //
	// // new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// // }
	// //
	// // @Action(value = "showYongYaoSelectTree")
	// // public String showYongYaoSelectTree(){
	// // putObjToContext(
	// // "path",
	// // ServletActionContext.getRequest().getContextPath()
	// // + "/system/sample/sampleOrder/showYongYaoSelectTreeJson.action");
	// //
	// // return
	// // dispatcher("/WEB-INF/page/system/sample/sampleYongYaoSelectTree.jsp");
	// // }
	//
	// // @Action(value = "showzhengzhuangSelectTreeJson", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public void showzhengzhuangSelectTreeJson() throws Exception {
	// // List<DicType> listCheck3 =
	// // sampleCancerTempService.findDicSampleType3(null);
	// //
	// // String a = sampleCancerTempService.getTreeJson(listCheck3);
	// //
	// // new SendData().sendDataJson(a, ServletActionContext.getResponse());
	// // }
	// //
	// // @Action(value = "showzhengzhuangSelectTree")
	// // public String showzhengzhuangSelectTree(){
	// // putObjToContext(
	// // "path",
	// // ServletActionContext.getRequest().getContextPath()
	// // + "/system/sample/sampleOrder/showzhengzhuangSelectTreeJson.action");
	// //
	// // return
	// // dispatcher("/WEB-INF/page/system/sample/samplezhengzhuangSelectTree.jsp");
	// // }
	//
	// @Action(value = "editSampleOrder")
	// public String editSampleOrder() throws Exception {
	// String id = getParameterFromRequest("id");
	// String changeId = getParameterFromRequest("changeId");
	// String type = getParameterFromRequest("type");
	// String bpmTaskId = getParameterFromRequest("bpmTaskId");
	// // 订单追加申请标识
	// String flag = getParameterFromRequest("flag");
	// if (type.equals("0")) {
	// rightsId = "980701";
	// } else {
	// rightsId = "980801";
	// }
	// putObjToContext("type", type);
	// // 订单追加申请
	// if (flag != null && !"".equals(flag)) {
	// long num = 0;
	// if (id != null && !id.equals("")) {
	// this.sampleOrder = sampleOrderService.get(id);
	// if (sampleOrder == null) {
	// sampleOrder = new SampleOrder();
	// sampleOrder.setId(id);
	// User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// sampleOrder.setCreateUser(user);
	// sampleOrder.setCreateDate(new Date());
	// sampleOrder.setReceivedDate(new Date());
	// putObjToContext("changeId", changeId);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// } else {
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	// putObjToContext("changeId", changeId);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	// num = fileInfoService.findFileInfoCount(id, "sampleOrder");
	// }
	//
	// } else {
	// sampleOrder.setId("NEW");
	// sampleOrder.setOrderType(type);
	// User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// sampleOrder.setCreateUser(user);
	// sampleOrder.setCreateDate(new Date());
	// sampleOrder.setReceivedDate(new Date());
	// sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// }
	// putObjToContext("flag", flag);
	// putObjToContext("fileNum", num);
	// putObjToContext("type", type);
	// } else {
	// long num = 0;
	// if (id != null && !id.equals("")) {
	// this.sampleOrder = sampleOrderService.get(id);
	// if (sampleOrder == null) {
	// sampleOrder = new SampleOrder();
	// sampleOrder.setId(id);
	// User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// sampleOrder.setCreateUser(user);
	// sampleOrder.setCreateDate(new Date());
	// sampleOrder.setReceivedDate(new Date());
	// sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// putObjToContext("bpmTaskId", bpmTaskId);
	// putObjToContext("changeId", changeId);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// } else {
	// putObjToContext("bpmTaskId", bpmTaskId);
	// putObjToContext("changeId", changeId);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	// num = fileInfoService.findFileInfoCount(id, "sampleOrder");
	// }
	//
	// } else {
	// sampleOrder.setId("NEW");
	// sampleOrder.setOrderType(type);
	// User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// sampleOrder.setCreateUser(user);
	// sampleOrder.setCreateDate(new Date());
	// sampleOrder.setReceivedDate(new Date());
	// sampleOrder.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// sampleOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// }
	// putObjToContext("flag", flag);
	// putObjToContext("fileNum", num);
	// putObjToContext("type", type);
	// toState(sampleOrder.getState());
	// }
	// /*
	// * long num = 0; if (id != null && !id.equals("")) { this.sampleOrder =
	// * sampleOrderService.get(id); if (sampleOrder == null) { sampleOrder =
	// * new SampleOrder(); sampleOrder.setId(id); User user = (User)
	// * this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// * sampleOrder.setCreateUser(user); sampleOrder.setCreateDate(new
	// * Date()); sampleOrder.setReceivedDate(new Date());
	// * sampleOrder.setState
	// * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// * sampleOrder.setStateName
	// * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// * putObjToContext("handlemethod",
	// * SystemConstants.PAGE_HANDLE_METHOD_ADD); toToolBar(rightsId, "", "",
	// * SystemConstants.PAGE_HANDLE_METHOD_ADD); } else {
	// * putObjToContext("handlemethod",
	// * SystemConstants.PAGE_HANDLE_METHOD_MODIFY); toToolBar(rightsId, "",
	// * "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY); num =
	// * fileInfoService.findFileInfoCount(id, "sampleOrder"); }
	// *
	// * } else { sampleOrder.setId("NEW"); User user = (User)
	// * this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// * sampleOrder.setCreateUser(user); sampleOrder.setCreateDate(new
	// * Date()); sampleOrder.setReceivedDate(new Date());
	// * sampleOrder.setState
	// * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// * sampleOrder.setStateName
	// * (com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// * putObjToContext("handlemethod",
	// * SystemConstants.PAGE_HANDLE_METHOD_ADD); toToolBar(rightsId, "", "",
	// * SystemConstants.PAGE_HANDLE_METHOD_ADD); } putObjToContext("flag",
	// * flag); putObjToContext("fileNum", num); putObjToContext("type",
	// * type); toState(sampleOrder.getState());
	// */
	// return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	// }
	//
	// /**
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "save")
	// public String save() throws Exception {
	// synchronized (SampleOrderMainAction.class) {
	// String id = sampleOrder.getId();
	// String number = sampleOrder.getMedicalNumber();
	// if ((id != null && id.equals("")) || id.equals("NEW")) {
	// String modelName = "SampleOrder";
	// String markCode = "DD";
	// Date date = new Date();
	// DateFormat format = new SimpleDateFormat("yy");
	// String stime = format.format(date);
	// String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime,
	// 000000, 6, null);
	//
	// sampleOrder.setId(autoID);
	// }
	//
	// boolean result = crmPatientService.queryByCount(number);
	// CrmPatient r = null;
	//
	// if (!"".equals(sampleOrder.getMedicalNumber())) {
	// r = crmPatientService.get(sampleOrder.getMedicalNumber());
	// if (r == null)
	// r = new CrmPatient();
	// // 该电子病历记录存在: 修改电子病历
	//
	// r.setId(sampleOrder.getMedicalNumber());
	// r.setName(sampleOrder.getName());
	// r.setGender(sampleOrder.getGender());
	// r.setDateOfBirth(sampleOrder.getBirthDate());
	// r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
	// r.setCreateDate(sampleOrder.getCreateDate());
	// r.setCreateUser(sampleOrder.getCreateUser());
	//
	// crmPatientService.save(r);
	//
	// } else {
	// // 该电子记录不存在:添加电子病历
	// String modelName = "CrmPatient";
	// String markCode = "P";
	// String autoID = codingRuleService.genTransID(modelName, markCode);
	//
	// sampleOrder.setMedicalNumber(autoID);
	// r = new CrmPatient();
	// r.setId(autoID);
	// r.setName(sampleOrder.getName());
	// r.setGender(sampleOrder.getGender());
	// r.setDateOfBirth(sampleOrder.getBirthDate());
	// r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
	// r.setCreateDate(sampleOrder.getCreateDate());
	// r.setCreateUser(sampleOrder.getCreateUser());
	// crmPatientService.save(r);
	// }
	// Map aMap = new HashMap();
	// // 保存病史和用药信息
	// // aMap.put("sampleOrderPersonnel",
	// // getParameterFromRequest("sampleOrderPersonnelJson"));
	// aMap.put("sampleOrderItem", getParameterFromRequest("sampleOrderInfoJson"));
	// String changeLog = getParameterFromRequest("changeLog");
	// String flag = getParameterFromRequest("flag");
	// String changeLogItem = getParameterFromRequest("changeLogItem");
	// String string = sampleOrderService.save(sampleOrder, aMap, changeLog,
	// changeLogItem, flag);
	// String[] split = string.split(",");
	// String samId = split[0];
	// String changeId = split[1];
	//
	// String bpmTaskId = getParameterFromRequest("bpmTaskId");
	// // String url =
	// // "/system/sample/sampleOrder/editSampleOrder.action?id=" +
	// // sampleOrder.getId();
	// String url = "/system/sample/sampleOrder/editSampleOrder.action?id=" + samId
	// + "&changeId=" + changeId
	// + "&type=" + sampleOrder.getOrderType();
	// if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null"))
	// {
	// url += "&bpmTaskId=" + bpmTaskId;
	// }
	// return redirect(url);
	// }
	// }
	//
	// @Action(value = "viewSampleOrder")
	// public String toViewSampleOrder() throws Exception {
	// String id = getParameterFromRequest("id");
	// sampleOrder = sampleOrderService.get(id);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainEdit.jsp");
	// }
	//
	// @Action(value = "showSampleOrderItemTable", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public String showSampleOrderItemList() throws Exception {
	// return dispatcher("/WEB-INF/page/system/sample/sampleOrderMainItem.jsp");
	// }
	//
	// @Action(value = "showSampleOrderItemTableJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void showSampleOrderItemListJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String id = getParameterFromRequest("id");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// Map<String, Object> result =
	// sampleOrderService.findSampleOrderItemTable(start, length, query, col, sort,
	// id);
	// List<SampleOrderItem> list = (List<SampleOrderItem>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("sampleCode", "");
	// map.put("slideCode", "");
	// map.put("sampleType-id", "");
	// map.put("sampleType-name", "");
	// map.put("samplingDate", "yyyy-MM-dd");
	// map.put("receiveUser-id", "");
	// map.put("receiveUser-name", "");
	// map.put("receiveDate", "yyyy-MM-dd");
	// map.put("stateName", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// }
	//
	// /**
	// * 删除明细信息
	// *
	// * @throws Exception
	// */
	// @Action(value = "delSampleOrderItemTable", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void delSampleOrderItem() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// String[] ids = getRequest().getParameterValues("ids[]");
	// sampleOrderService.delSampleOrderItem(ids);
	// map.put("success", true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// map.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// // 根据主数据加载子表明细
	// // @Action(value = "setSamplePerson", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public void setTemplateItem() throws Exception {
	// // String code = getRequest().getParameter("code");
	// // Map<String, Object> result = new HashMap<String, Object>();
	// // try {
	// // List<Map<String, String>> dataListMap = this.sampleOrderService
	// // .setTemplateItem(code);
	// // result.put("success", true);
	// // result.put("data", dataListMap);
	// //
	// // } catch (Exception e) {
	// // result.put("success", false);
	// // }
	// // HttpUtils.write(JsonUtils.toJsonString(result));
	// // }
	//
	// // 根据主数据加载子表明细2
	// // @Action(value = "setSampleItem", interceptorRefs =
	// // @InterceptorRef("biolimsDefaultStack"))
	// // public void setSampleOrderItem() throws Exception {
	// // String code = getRequest().getParameter("code");
	// // Map<String, Object> result = new HashMap<String, Object>();
	// // try {
	// // List<Map<String, String>> dataListMap = this.sampleOrderService
	// // .setSampleOrderItem2(code);
	// // result.put("success", true);
	// // result.put("data", dataListMap);
	// //
	// // } catch (Exception e) {
	// // result.put("success", false);
	// // }
	// // HttpUtils.write(JsonUtils.toJsonString(result));
	// // }
	//
	// // 查询订单编号是否存在 并查询 是否使用
	// @Action(value = "selSampleOrderId", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void selSampleOrderId() throws Exception {
	// String id = getRequest().getParameter("id");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// int total = this.sampleOrderService.selSampleOrderId(id);
	// result.put("success", true);
	// result.put("data", total);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 按年查询订单柱状图
	// @Action(value = "selOrderNumberByYear", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void selOrderNumberByYear() throws Exception {
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// String dateYear = getRequest().getParameter("dateYear");
	// List list = this.sampleOrderMainService.selOrderNumberByYear(dateYear);
	// result.put("data", list);
	// result.put("success", true);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 按年查询订单中检测项目病状图
	// @Action(value = "selProductByOrderByYear", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void selProductByOrderByYear() throws Exception {
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// String dateYear = getRequest().getParameter("dateYear");
	// List list = this.sampleOrderMainService.selProductByOrderByYear(dateYear);
	// result.put("data", list);
	// result.put("success", true);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 按月查询订单中检测项目病状图
	// @Action(value = "selProductByOrderByMonth", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void selProductByOrderByMonth() throws Exception {
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// String dateYearMonth = getRequest().getParameter("dateYearMonth");
	// List list =
	// this.sampleOrderMainService.selProductByOrderByMonth(dateYearMonth);
	// result.put("data", list);
	// result.put("success", true);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// // 查询订单图片路径
	// @Action(value = "selPic", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void selPic() throws Exception {
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// String id = getRequest().getParameter("id");
	// List<String> list = this.sampleOrderMainService.selPic(id);
	// result.put("data", list);
	// result.put("success", true);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// @Action(value = "saveItem")
	// public void saveItem() throws Exception {
	// String id = getParameterFromRequest("id");
	// String changeLog = getParameterFromRequest("changeLog");
	// String dataJson = getParameterFromRequest("dataJson");
	// sampleOrder = commonDAO.get(SampleOrder.class, id);
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// sampleOrderService.saveSampleOrderItem(sampleOrder, dataJson, changeLog);
	// map.put("success", true);
	// } catch (Exception e) {
	// map.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// /*
	// * Map<String, Object> aMap = new HashMap<String, Object>(); String id =
	// * getParameterFromRequest("id"); sampleOrder =
	// * commonDAO.get(SampleOrder.class, id); Map<String, Object> map = new
	// * HashMap<String, Object>(); aMap.put("sampleOrderItem",
	// * getParameterFromRequest("dataJson")); try {
	// * sampleOrderService.save(sampleOrder, aMap); map.put("success", true);
	// * } catch (Exception e) { map.put("success", false); }
	// * HttpUtils.write(JsonUtils.toJsonString(map));
	// */
	// }
	//
	// // 订单左侧显示图片
	// @Action(value = "showPic")
	// public String showPic() throws Exception {
	// // putObjToContext("handlemethod",
	// // SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// // toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/sample/changeView/index.html");
	// }
	//
	// // 批量上传csv
	// @Action(value = "uploadCsvFile", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack") )
	// public void uploadCsvFile() throws Exception {
	// String id = getParameterFromRequest("id");
	// String fileId = getParameterFromRequest("fileId");
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// sampleOrderMainService.getCsvContent(id, fileId);
	// map.put("success", true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// map.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// @Action(value = "searchSampleOrder")
	// public String searchSampleOrder() throws Exception {
	// String type = getRequest().getParameter("type");
	// if (type.equals("0")) {// 用户
	// rightsId = "980703";
	// } else {
	// rightsId = "980803";
	// }
	// putObjToContext("type", type);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/system/sample/sampleMainSearch.jsp");
	// }
	//
	// @Action(value = "searchOptions")
	// public void searchOptions() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// String type = getRequest().getParameter("type");
	// try {
	// map = sampleOrderService.searchOptions(type);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// @Action(value = "sampleOrderChart")
	// public void sampleOrderChart() throws Exception {
	// String query = getRequest().getParameter("query");
	// String year = getParameterFromRequest("year");
	// String month = getParameterFromRequest("month");
	// String[] product = getRequest().getParameterValues("product[]");
	// String[] customer = getRequest().getParameterValues("customer[]");
	// String[] sale = getRequest().getParameterValues("sale[]");
	// String[] doctor = getRequest().getParameterValues("doctor[]");
	//
	// String type = getRequest().getParameter("type");
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// map = sampleOrderService.sampleOrderChart(type, query, year, month, product,
	// customer, sale, doctor);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// @Action(value = "sampleOrderSearchTable")
	// public void sampleOrderSearchTable() {
	// String year = getParameterFromRequest("year");
	// String month = getParameterFromRequest("month");
	// String[] product = getRequest().getParameterValues("product[]");
	// String[] customer = getRequest().getParameterValues("customer[]");
	// String[] sale = getRequest().getParameterValues("sale[]");
	// String[] doctor = getRequest().getParameterValues("doctor[]");
	// String query = getRequest().getParameter("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// String type = getParameterFromRequest("type");
	// try {
	// Map<String, Object> result = sampleOrderService.sampleOrderSearchTable(type,
	// year, month, product, customer,
	// sale, doctor, start, length, query, col, sort);
	// List<SampleOrder> list = (List<SampleOrder>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("gender", "");
	// map.put("age", "");
	// map.put("nation", "");
	// map.put("email", "");
	// map.put("zipCode", "");
	// map.put("diagnosisDate", "");
	// map.put("commissioner-id", "");
	// map.put("commissioner-name", "");
	// map.put("nativePlace", "");
	// map.put("medicalNumber", "");
	// map.put("familyCode", "");
	// map.put("family", "");
	// map.put("familyPhone", "");
	// map.put("familySite", "");
	// map.put("crmCustomer-id", "");
	// map.put("crmCustomer-name", "");
	// map.put("createUser-id", "");
	// map.put("createUser-name", "");
	// map.put("stateName", "");
	// map.put("barcode", "");
	// map.put("subjectID", "");
	// map.put("familyHistorysummary", "");
	// map.put("prenatal", "");
	// map.put("hospitalPatientID", "");
	// map.put("weights", "");
	// map.put("gestationalWeeks", "");
	// map.put("birthDate", "");
	// map.put("createDate", "");
	// map.put("idCard", "");
	// map.put("productName", "");
	// map.put("fieldContent", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// // 根据模块查询自定义字段数据
	// Map<String, Object> mapField =
	// fieldService.findFieldByModuleValue("SampleOrder");
	// String dataStr = PushData.pushFieldData(data, mapField);
	// HttpUtils.write(PushData.pushData(draw, result, dataStr));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }
	//
	// @Action(value = "showImg")
	// public String showPicture() throws Exception {
	// String id = getParameterFromRequest("id");
	// List<FileInfo> list = sampleOrderService.showPicture(id);
	// putObjToContext("fList", list);
	// return dispatcher("/lims/changeView/index.jsp");
	// }
	//
	// /**
	// *
	// * @Title: 输入采血管只数动态生成数据 @Description: TODO @param @return @param @throws
	// * Exception @return String @author @date 2019年3月17日 @throws
	// */
	// @Action(value = "saveNumSampleOrderItem")
	// public void saveNumSampleOrderItem() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// String id = getParameterFromRequest("id");
	// String barCode = getParameterFromRequest("barCode");
	// int number = Integer.parseInt(getParameterFromRequest("number"));
	// SampleOrder sampleOrder = sampleOrderService.get(id);
	// String num = "";
	// int nums = 0;
	// if (sampleOrder != null) {
	// List<SampleOrderItem> sampleList =
	// sampleOrderService.getSampleOrderItemListPage(sampleOrder.getId());
	// if (sampleList.size() < number) {
	// for (int i = 0; i < number; i++) {
	// SampleOrderItem item = new SampleOrderItem();
	// item.setName(sampleOrder.getName());
	// item.setGender(sampleOrder.getGender());
	// item.setSampleCode(sampleOrder.getSampleCode());
	// item.setSamplingDate(sampleOrder.getDrawBloodTime());
	// item.setSampleOrder(sampleOrder);
	// // 实现样本条码号 自增
	// String num2 = barCode.substring(0, barCode.length() - 1);
	// int nus = Integer.parseInt(barCode.substring(barCode.length() - 1));
	// nums = nus + i;
	// item.setSlideCode(num2 + Integer.toString(nums));
	// sampleOrderService.saveUploadOrdItem(item);
	// }
	// }
	// }
	// map.put("success", true);
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// public String getRightsId() {
	// return rightsId;
	// }
	//
	// public void setRightsId(String rightsId) {
	// this.rightsId = rightsId;
	// }
	//
	// public SampleOrderService getSampleOrderService() {
	// return sampleOrderService;
	// }
	//
	// public void setSampleOrderService(SampleOrderService sampleOrderService) {
	// this.sampleOrderService = sampleOrderService;
	// }
	//
	// public SampleOrder getSampleOrder() {
	// return sampleOrder;
	// }
	//
	// public void setSampleOrder(SampleOrder sampleOrder) {
	// this.sampleOrder = sampleOrder;
	// }
	//
	// /**
	// * 癌症信息审核的get和set方法
	// *
	// * @return
	// */
	// public SampleCancerTempService getSampleCancerTempService() {
	// return sampleCancerTempService;
	// }
	//
	// public void setSampleCancerTempService(SampleCancerTempService
	// sampleCancerTempService) {
	// this.sampleCancerTempService = sampleCancerTempService;
	// }
	//
	// public SampleCancerTemp getSampleCancerTempOne() {
	// return sampleCancerTempOne;
	// }
	//
	// public void setSampleCancerTempOne(SampleCancerTemp sampleCancerTempOne) {
	// this.sampleCancerTempOne = sampleCancerTempOne;
	// }
	//
	// public SampleCancerTemp getSampleCancerTempTwo() {
	// return sampleCancerTempTwo;
	// }
	//
	// public CrmPatientService getCrmPatientService() {
	// return crmPatientService;
	// }
	//
	// public void setCrmPatientService(CrmPatientService crmPatientService) {
	// this.crmPatientService = crmPatientService;
	// }
	//
	// public void setSampleCancerTempTwo(SampleCancerTemp sampleCancerTempTwo) {
	// this.sampleCancerTempTwo = sampleCancerTempTwo;
	// }
	//
	// public CodingRuleService getCodingRuleService() {
	// return codingRuleService;
	// }
	//
	// public void setCodingRuleService(CodingRuleService codingRuleService) {
	// this.codingRuleService = codingRuleService;
	// }
}
