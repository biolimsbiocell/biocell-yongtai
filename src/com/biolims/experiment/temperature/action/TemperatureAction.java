﻿package com.biolims.experiment.temperature.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.sanger.model.SangerTaskAbnormal;
import com.biolims.experiment.sanger.service.SangerTaskAbnormalService;
import com.biolims.experiment.temperature.model.Temperature;
import com.biolims.experiment.temperature.service.TemperatureService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/temperature")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TemperatureAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2416";
	@Resource
	private TemperatureService temperatureService;
	
//	@Resource
//	private FileInfoService fileInfoService;

	@Action(value = "showTemperatureList")
	public String showTemperatureList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/temperature/temperatureControl.jsp");
	}

	@Action(value = "showTemperatureListJson")
	public void showTemperatureListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");//目录
		String sort = getParameterFromRequest("sort");//排序
		String data = getParameterFromRequest("data");//查询条件
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = temperatureService.findTemperaturelList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Temperature> list = (List<Temperature>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("temperature", "");
		map.put("humidity", "");
		map.put("controlMeasures", "");
		map.put("afterTemperature", "");
		map.put("afterHumidity", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	@Action(value = "save")
	public void save() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			User name = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			temperatureService.saveTemperature(dataJson,name);
			map.put("success", true);
			
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
		

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

//	@Action(value = "sangerTaskAbnormalSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showDialogSangerTaskAbnormalList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskAbnormalDialog.jsp");
//	}
//
//	@Action(value = "showDialogSangerTaskAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogSangerTaskAbnormalListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = sangerTaskAbnormalService
//				.findSangerTaskAbnormalList(map2Query, startNum, limitNum, dir,
//						sort);
//		Long count = (Long) result.get("total");
//		List<SangerTaskAbnormal> list = (List<SangerTaskAbnormal>) result
//				.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
//
//	@Action(value = "editSangerTaskAbnormal")
//	public String editSangerTaskAbnormal() throws Exception {
//		String id = getParameterFromRequest("id");
//		long num = 0;
//		if (id != null && !id.equals("")) {
//			sangerTaskAbnormal = sangerTaskAbnormalService.get(id);
//			putObjToContext("handlemethod",
//					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			toToolBar(rightsId, "", "",
//					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			num = fileInfoService.findFileInfoCount(id, "sangerTaskAbnormal");
//		} else {
//			User user = (User) this
//					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			putObjToContext("handlemethod",
//					SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//		}
//		putObjToContext("fileNum", num);
//		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskAbnormalEdit.jsp");
//	}
//
//	@Action(value = "copySangerTaskAbnormal")
//	public String copySangerTaskAbnormal() throws Exception {
//		String id = getParameterFromRequest("id");
//		String handlemethod = getParameterFromRequest("handlemethod");
//		sangerTaskAbnormal = sangerTaskAbnormalService.get(id);
//		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
//		toToolBar(rightsId, "", "", handlemethod);
//		toSetStateCopy();
//		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskAbnormalEdit.jsp");
//	}
//
//	@Action(value = "save")
//	public String save() throws Exception {
//		Map aMap = new HashMap();
//		sangerTaskAbnormalService.save(sangerTaskAbnormal, aMap);
//		return redirect("/experiment/sanger/sangerTaskAbnormal/editSangerTaskAbnormal.action");
//	}
//
//	@Action(value = "viewSangerTaskAbnormal")
//	public String toViewSangerTaskAbnormal() throws Exception {
//		String id = getParameterFromRequest("id");
//		sangerTaskAbnormal = sangerTaskAbnormalService.get(id);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		return dispatcher("/WEB-INF/page/experiment/sanger/sangerTaskAbnormalEdit.jsp");
//	}
//
//	public String getRightsId() {
//		return rightsId;
//	}
//
//	public void setRightsId(String rightsId) {
//		this.rightsId = rightsId;
//	}
//
//	public SangerTaskAbnormalService getSangerTaskAbnormalService() {
//		return sangerTaskAbnormalService;
//	}
//
//	public void setSangerTaskAbnormalService(
//			SangerTaskAbnormalService sangerTaskAbnormalService) {
//		this.sangerTaskAbnormalService = sangerTaskAbnormalService;
//	}
//
//	public SangerTaskAbnormal getSangerTaskAbnormal() {
//		return sangerTaskAbnormal;
//	}
//
//	public void setSangerTaskAbnormal(SangerTaskAbnormal sangerTaskAbnormal) {
//		this.sangerTaskAbnormal = sangerTaskAbnormal;
//	}
//
//	/**
//	 * 保存Sanger实验异常样本
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "saveSangerTaskAbnormal")
//	public void saveSangerTaskAbnormal() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getParameterFromRequest("itemDataJson");
//			sangerTaskAbnormalService.saveSangerTaskAbnormalList(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 根据条件检索异常样本
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "selectAbnormal")
//	public void selectAbnormal() throws Exception {
//		String code1 = getParameterFromRequest("code");
//		String code2 = getParameterFromRequest("Code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.sangerTaskAbnormalService
//					.selectAbnormal(code1, code2);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
