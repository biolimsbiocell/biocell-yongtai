package com.biolims.experiment.temperature.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

@Entity
@Table(name = "t_temperature")
@SuppressWarnings("serial")
public class Temperature extends EntityDao<Temperature> implements java.io.Serializable{
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "id", length = 50)
	private  String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	//日期
	private Date createDate;
	//温度 
	private Double temperature;
	//相对湿度%
	private Double humidity;
	//调控措施
	private String controlMeasures;
	//调控后温度℃
	private Double afterTemperature;
	//调控后相对湿度%
	private Double afterHumidity;
	//记录人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	private User createUser;
	//备注 
	
	private String note;
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getHumidity() {
		return humidity;
	}
	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}
	public String getControlMeasures() {
		return controlMeasures;
	}
	public void setControlMeasures(String controlMeasures) {
		this.controlMeasures = controlMeasures;
	}
	public Double getAfterTemperature() {
		return afterTemperature;
	}
	public void setAfterTemperature(Double afterTemperature) {
		this.afterTemperature = afterTemperature;
	}
	public Double getAfterHumidity() {
		return afterHumidity;
	}
	public void setAfterHumidity(Double afterHumidity) {
		this.afterHumidity = afterHumidity;
	}
	
	public User getCreateUser() {
		return createUser;
	}
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	
}
