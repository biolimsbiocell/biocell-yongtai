package com.biolims.experiment.temperature.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.model.user.User;
import com.biolims.experiment.temperature.dao.TemperatureDao;
import com.biolims.experiment.temperature.model.Temperature;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TemperatureService {
	@Resource
	private TemperatureDao temperatureDao;
	
	
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTemperaturelList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
			return temperatureDao.queryTemperaturellList(mapForQuery, startNum, limitNum, dir, sort);
		 
	}
	public void saveTemperature(String itemDataJson,User name)
			throws Exception {

		List<Temperature> saveItems = new ArrayList<Temperature>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			Temperature tu1 = new Temperature();
			// 将map信息读入实体类
			tu1 = (Temperature) temperatureDao.Map2Bean(map, tu1);
		
			tu1.setCreateDate(new Date());
			temperatureDao.saveOrUpdate(tu1);
			tu1.setCreateUser(name);
			tu1.setCreateDate(new Date());
		}

	}
	
}
