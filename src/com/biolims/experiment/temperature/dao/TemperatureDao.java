package com.biolims.experiment.temperature.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.temperature.model.Temperature;


@Repository
@SuppressWarnings("unchecked")
public class TemperatureDao extends BaseHibernateDao{
	public Map<String, Object> queryTemperaturellList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort){
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		String key = " ";
		String hql = " from Temperature where 1=1 ";
		
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Temperature> list = new ArrayList<Temperature>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		result.put("total", total);
		result.put("list", list);
		
		return result;
	};
}
