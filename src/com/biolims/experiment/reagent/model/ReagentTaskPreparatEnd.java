package com.biolims.experiment.reagent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
/**   
 * @Title: Model
 * @Description: 实验完工清场 指令   明细表
 * @author lims-platform
 * @date 2019-03-21 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "reagent_task_prepaend")
@SuppressWarnings("serial")
public class ReagentTaskPreparatEnd extends EntityDao<ReagentTaskPreparatEnd> implements java.io.Serializable {
	/** id */
	private String id;
	/** 名称 */
	@Column(name="name")
	private String name;
	/** 创建时间*/
	@Column(name="create_date")
	private String createDate;
	/** 创建人 */
	@Column(name="create_date")
	private String createUser;
	/** 步骤号 */
	@Column(name="order_num")
	private String orderNum;
	/** 关联的指令id*/
	private TempleNstructionsEnd templeInsId;
	/** 生产检查（0否       1是） */
	@Column(name="production_inspection")
	private String productionInspection;
	/** 操作备注 */
	@Column(name="operation_notes")
	private String operationNotes;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;

	/** 相关主表 */
	private ReagentTask reagentTask;

	public void setReagentTask(ReagentTask reagentTask) {
		this.reagentTask = reagentTask;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "reagent_task")
	public ReagentTask getReagentTask() {
		return reagentTask;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getName() {
		return name;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_ins_id")
	public TempleNstructionsEnd getTempleInsId() {
		return templeInsId;
	}
	public void setTempleInsId(TempleNstructionsEnd templeInsId) {
		this.templeInsId = templeInsId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(ReagentTask cellPassage) {
		this.reagentTask = cellPassage;
	}
	public String getProductionInspection() {
		return productionInspection;
	}
	public void setProductionInspection(String productionInspection) {
		this.productionInspection = productionInspection;
	}
	public String getOperationNotes() {
		return operationNotes;
	}
	public void setOperationNotes(String operationNotes) {
		this.operationNotes = operationNotes;
	}

}