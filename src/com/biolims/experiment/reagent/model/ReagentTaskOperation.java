package com.biolims.experiment.reagent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TempleProducingCell;
/**   
 * @Title: Model
 * @Description: 生产第三步  生产操作主表
 * @author lims-platform
 * @date 2015-11-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "reangent_task_operation")
@SuppressWarnings("serial")
public class ReagentTaskOperation extends EntityDao<ReagentTaskOperation> implements java.io.Serializable {
	/** id */
	private String id;
	/** 创建人--实验员 */
	@Column(name="create_user")
	private String createUser;
	/** 创建人时间*/
	@Column(name="create_date")
	private String createDate;
	/** 步骤号 */
	@Column(name="order_num")
	private String orderNum;
	/** 结束时间  */
	@Column(name="end_time")
	private String endTime;
	/** 开始时间*/
	@Column(name="start_time")
	private String startTime;
	/** 关联的模板*/
	private TempleProducingCell templeCell;
	/** 状态 0正常    1已删除    2已完成*/
	@Column(name="state")
	private String state;
	/** 相关主表 */
	private ReagentTask reagentTask;
	
	/** 自定义字段*/
	@Column(name="custom_test")
	private String customTest;
	/** 自定义字段值*/
	@Column(name="custom_test_value")
	private String customTestValue;
	
	
	
	
	
	
	public String getCustomTest() {
		return customTest;
	}
	public void setCustomTest(String customTest) {
		this.customTest = customTest;
	}
	public String getCustomTestValue() {
		return customTestValue;
	}
	public void setCustomTestValue(String customTestValue) {
		this.customTestValue = customTestValue;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "reagent_task")
	public ReagentTask getReagentTask() {
		return reagentTask;
	}
	public void setReagentTask(ReagentTask reagentTask) {
		this.reagentTask = reagentTask;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_cell")
	public TempleProducingCell getTempleCell() {
		return templeCell;
	}
	public void setTempleCell(TempleProducingCell templeCell) {
		this.templeCell = templeCell;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(ReagentTask cellPassage) {
		this.reagentTask = cellPassage;
	}
}