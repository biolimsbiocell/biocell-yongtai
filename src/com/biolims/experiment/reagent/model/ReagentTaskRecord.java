package com.biolims.experiment.reagent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.template.model.TemplateItem;
/**   
 * @Title: Model
 * @Description: 实验第一步生产检查明细表     
 * @author lims-platform
 * @date 2019-03-18 17:00:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "reagent_task_record")
@SuppressWarnings("serial")
public class ReagentTaskRecord extends EntityDao<ReagentTaskRecord> implements java.io.Serializable {
	/** 设备id */
	private String id;
	/** 操作间名称 */
	@Column(name="operating_room_name")
	private String operatingRoomName;
	/** 操作间id */
	@Column(name="operating_room_id")
	private String operatingRoomId;
	/** 第一步结束时间*/
	@Column(name="create_date")
	private String createDate;
	/** 创建人--实验员 */
	@Column(name="create_date")
	private String createUser;
	/** 步骤号 */
	@Column(name="order_num")
	private String orderNum;
	/** 环境温度*/
	@Column(name="ambient_temperature")
	private String ambientTemperature;
	/** 环境湿度） */
	@Column(name="ambient_humidity")
	private String ambientHumidity;
	/** 操作人 */
	@Column(name="temple_operator")
	private User templeOperator;
	/** QA复核人 */
	@Column(name="temple_reviewer")
	private User templeReviewer;
	/** 状态 0正常    1已删除*/
	@Column(name="state")
	private String state;
	/** 对应的模板id*/
	private TemplateItem templeItem;
	/** 自定义字段结果*/
	private String content;
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	/** 备注*/
	@Column(name="note")
	private String note;
	
	
	//新加字段
	/**预计用时*/
	@Column(name="estimated_date")
	private String estimatedDate;
	/**预计结束日期*/
	@Column(name="plan_end_date")
	private String planEndDate;
	/**预计开始日期*/
	@Column(name="plan_work_date")
	private String planWorkDate;
	/**延时*/
	@Column(name="delay")
	private String delay;
	/**当前生产大步骤的    结束时间 */
	@Column(name="end_time")
	private String endTime;
	/** 相关主表 */
	private ReagentTask reagentTask;
	
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "reagent_task")
	public ReagentTask getReagentTask() {
		return reagentTask;
	}
	public void setReagentTask(ReagentTask reagentTask) {
		this.reagentTask = reagentTask;
	}
	/**
	 * 方法: 取得String
	 * @return: String 设备id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	public String getCreateDate() {
		return createDate;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getCreateUser() {
		return createUser;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_item")
	public TemplateItem getTempleItem() {
		return templeItem;
	}
	public void setTempleItem(TemplateItem templeItem) {
		this.templeItem = templeItem;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCellPassage(ReagentTask cellPassage) {
		this.reagentTask = cellPassage;
	}
	public String getOperatingRoomName() {
		return operatingRoomName;
	}
	public void setOperatingRoomName(String operatingRoomName) {
		this.operatingRoomName = operatingRoomName;
	}
	public String getAmbientTemperature() {
		return ambientTemperature;
	}
	public void setAmbientTemperature(String ambientTemperature) {
		this.ambientTemperature = ambientTemperature;
	}
	public String getAmbientHumidity() {
		return ambientHumidity;
	}
	public void setAmbientHumidity(String ambientHumidity) {
		this.ambientHumidity = ambientHumidity;
	}
	public String getEstimatedDate() {
		return estimatedDate;
	}
	public void setEstimatedDate(String estimatedDate) {
		this.estimatedDate = estimatedDate;
	}
	public String getPlanEndDate() {
		return planEndDate;
	}
	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}
	public String getPlanWorkDate() {
		return planWorkDate;
	}
	public void setPlanWorkDate(String planWorkDate) {
		this.planWorkDate = planWorkDate;
	}
	public String getDelay() {
		return delay;
	}
	public void setDelay(String delay) {
		this.delay = delay;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_operator")
	public User getTempleOperator() {
		return templeOperator;
	}
	public void setTempleOperator(User templeOperator) {
		this.templeOperator = templeOperator;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_reviewer")
	public User getTempleReviewer() {
		return templeReviewer;
	}
	public void setTempleReviewer(User templeReviewer) {
		this.templeReviewer = templeReviewer;
	}
	public String getOperatingRoomId() {
		return operatingRoomId;
	}
	public void setOperatingRoomId(String operatingRoomId) {
		this.operatingRoomId = operatingRoomId;
	}
	
	
}