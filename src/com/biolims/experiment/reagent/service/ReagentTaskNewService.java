package com.biolims.experiment.reagent.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.IncubatorSampleInfoIn;
import com.biolims.equipment.model.IncubatorSampleInfoOut;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.main.model.MaterailsMain;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageAbnormal;
import com.biolims.experiment.cell.passage.model.CellPassageCos;
import com.biolims.experiment.cell.passage.model.CellPassageInfo;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassagePreparat;
import com.biolims.experiment.cell.passage.model.CellPassagePreparatEnd;
import com.biolims.experiment.cell.passage.model.CellPassageReagent;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellPassageZhiJian;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProducResults;
import com.biolims.experiment.cell.passage.model.CellProductionCompleted;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.cell.passage.model.CellproductReagent;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.reagent.dao.ReagentTaskDao;
import com.biolims.experiment.reagent.model.ReagentTask;
import com.biolims.experiment.reagent.model.ReagentTaskCompleted;
import com.biolims.experiment.reagent.model.ReagentTaskCosNew;
import com.biolims.experiment.reagent.model.ReagentTaskInfo;
import com.biolims.experiment.reagent.model.ReagentTaskOperation;
import com.biolims.experiment.reagent.model.ReagentTaskPreparat;
import com.biolims.experiment.reagent.model.ReagentTaskPreparatEnd;
import com.biolims.experiment.reagent.model.ReagentTaskReagent;
import com.biolims.experiment.reagent.model.ReagentTaskReagentNew;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.experiment.reagent.model.ReagentTaskResults;
import com.biolims.experiment.reagent.model.ReagentTaskTemplate;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleFinished;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleNstructionsEnd;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional
public class ReagentTaskNewService {

	@Resource
	private ReagentTaskDao reagentTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId, String templateId, String logInfo)
			throws Exception {
		String id = "";
		int amount = 0;
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			ReagentTask pt = new ReagentTask();
			pt = (ReagentTask) commonDAO.Map2Bean(list.get(0), pt);
			User us = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			// 时间格式化
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Template t = commonDAO.get(Template.class, templateId);
			String log = "";
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				log = "123";
				String modelName = "ReagentTask";
				String markCode = "RT";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateUser(us);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				pt.setTemplate(t);
				saveTemplate(pt);
				// 算预计时间 第一次创建的生产的时候固定 计划完成时间 创建第一步
				List<TemplateItem> temList = templateService.getTemplateItemListPage(t.getId());
				if (temList.size() > 0) {
					for (int n = 0; n < temList.size(); n++) {
						ReagentTaskRecord cellProduc = new ReagentTaskRecord();
						cellProduc.setEstimatedDate(temList.get(n).getEstimatedTime());
						cellProduc.setPlanWorkDate(ObjectToMapUtils.getTimeString());
						cellProduc.setCellPassage(pt);
						cellProduc.setTempleItem(temList.get(n));
						if (!"".equals(temList.get(n).getEstimatedTime())) {
							Calendar calendar1 = Calendar.getInstance();
							SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
							if (n == 0) {
								String three_days_ago = sdf1.format(calendar1.getTime());
								cellProduc.setPlanWorkDate(three_days_ago);//
							} else {
								calendar1.add(Calendar.DATE, amount);
								String three_days_ago = sdf1.format(calendar1.getTime());
								cellProduc.setPlanWorkDate(three_days_ago);//
							}
							amount += Integer.parseInt(temList.get(n).getEstimatedTime());
						}
						cellProduc.setOrderNum(String.valueOf(temList.get(n).getOrderNum()));
						commonDAO.saveOrUpdate(cellProduc);
					}
				}

				// 保存完工生产检查
				List<TempleFinished> templef = templateService.findByTempleIdFinishedInfo(t.getId());
				if (templef.size() > 0) {
					for (int n = 0; n < templef.size(); n++) {
						ReagentTaskCompleted cellcomple = new ReagentTaskCompleted();
						cellcomple.setCellPassage(pt);
						cellcomple.setTempleItem(templef.get(n));
						cellcomple.setOrderNum(templef.get(n).getOrderNum());
						cellcomple.setState("0");
						commonDAO.saveOrUpdate(cellcomple);
					}
				}

				// 保存完成清场操作指令 --模板
				List<TempleNstructionsEnd> temNsEnd = templateService.getTempleNstructionsEndListPage(t.getId());
				if (temNsEnd.size() > 0) {
					for (int m = 0; m < temNsEnd.size(); m++) {
						ReagentTaskPreparatEnd cellpaEnd = new ReagentTaskPreparatEnd();
						cellpaEnd.setCellPassage(pt);
						cellpaEnd.setOrderNum(temNsEnd.get(m).getOrderNum());
						cellpaEnd.setState("0");
						cellpaEnd.setProductionInspection(temNsEnd.get(m).getTempleProduction());
						cellpaEnd.setOperationNotes(temNsEnd.get(m).getOperationNote());
						cellpaEnd.setTempleInsId(temNsEnd.get(m));
						commonDAO.saveOrUpdate(cellpaEnd);
					}
				}

				// 工期准备操作指令
				List<TempleNstructions> temNs = templateService.getTempleNstructionsListPage(t.getId());
				if (temNs.size() > 0) {
					for (int m = 0; m < temNs.size(); m++) {
						ReagentTaskPreparat cellpa = new ReagentTaskPreparat();
						cellpa.setCellPassage(pt);
						cellpa.setOrderNum(temNs.get(m).getItemId());
						cellpa.setState("0");
						cellpa.setProductionInspection(temNs.get(m).getTempleProduction());
						cellpa.setOperationNotes(temNs.get(m).getOperationNote());
						cellpa.setTempleInsId(temNs.get(m));
						commonDAO.saveOrUpdate(cellpa);
					}
				}
				// 保存第三步模板
				if (pt.getTemplate() != null) {
					saveCellPassageTems(pt);
				}
				reagentTaskDao.saveOrUpdate(pt);
			} else {
				id = pt.getId();
				pt = commonDAO.get(ReagentTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);

				// 保存完成清场操作指令 --模板
				List<TempleNstructionsEnd> temNsEnd = templateService.getTempleNstructionsEndListPage(t.getId());
				if (temNsEnd.size() > 0) {
					for (int m = 0; m < temNsEnd.size(); m++) {
						List<ReagentTaskPreparatEnd> preparList = reagentTaskDao
								.findBytemIdReagenTaskPraraEndList(pt.getId(), temNsEnd.get(m).getId());
						if (preparList.size() == 0) {
							ReagentTaskPreparatEnd cellpaEnd = new ReagentTaskPreparatEnd();
							cellpaEnd.setCellPassage(pt);
							cellpaEnd.setOrderNum(temNsEnd.get(m).getOrderNum());
							cellpaEnd.setState("0");
							cellpaEnd.setProductionInspection(temNsEnd.get(m).getTempleProduction());
							cellpaEnd.setOperationNotes(temNsEnd.get(m).getOperationNote());
							cellpaEnd.setTempleInsId(temNsEnd.get(m));
							commonDAO.saveOrUpdate(cellpaEnd);
						}
					}
				}

				// 工期准备操作指令
				List<TempleNstructions> temNs = templateService.getTempleNstructionsListPage(t.getId());
				if (temNs.size() > 0) {
					for (int m = 0; m < temNs.size(); m++) {
						List<ReagentTaskPreparat> preparList = reagentTaskDao.findBytemIdReagenTaskPraraList(pt.getId(),
								temNs.get(m).getId());
						if (preparList.size() == 0) {
							ReagentTaskPreparat cellpa = new ReagentTaskPreparat();
							cellpa.setCellPassage(pt);
							cellpa.setOrderNum(temNs.get(m).getItemId());
							cellpa.setState("0");
							cellpa.setProductionInspection(temNs.get(m).getTempleProduction());
							cellpa.setOperationNotes(temNs.get(m).getOperationNote());
							cellpa.setTempleInsId(temNs.get(m));
							commonDAO.saveOrUpdate(cellpa);
						}
					}
				}

			}

			pt.setTestUserOneId(userId);
			String[] users = userId.split(",");
			String userName = "";
			for (int i = 0; i < users.length; i++) {
				if (i == users.length - 1) {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName();
				} else {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName() + ",";
				}
			}
			pt.setTestUserOneName(userName);
			reagentTaskDao.saveOrUpdate(pt);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setClassName("ReagentTask");
				li.setModifyContent(logInfo);
				if ("123".equals(log) && !"".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}

				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(ReagentTask sc) throws ParseException {
		List<ReagentTaskTemplate> tlist2 = reagentTaskDao.delTemplateItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		// 定义一个增长的变量
		Integer i = 0;
		for (TemplateItem t : tlist) {
			ReagentTaskTemplate ptt = new ReagentTaskTemplate();
			Integer result = 0;
			if (t.getEstimatedTime() != null && !"".equals(t.getEstimatedTime())) {
				ptt.setEstimatedDate(t.getEstimatedTime());

				i += Integer.parseInt(t.getEstimatedTime());
				result = i - Integer.parseInt(t.getEstimatedTime());
				String createDate = sc.getCreateDate();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse = format.parse(createDate);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.DATE, result);
				parse = calendar.getTime();
				String date = format.format(parse);
				ptt.setPlanWorkDate(date);

				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date parse1 = format1.parse(createDate);
				Calendar calendar1 = Calendar.getInstance();
				calendar1.add(calendar1.DATE, i);
				parse1 = calendar1.getTime();
				String date1 = format.format(parse1);
				ptt.setPlanEndDate(date1);
			}

			ptt.setReagentTask(sc);
			ptt.setOrderNum(String.valueOf(t.getOrderNum()));
			ptt.setName(t.getName());
//			ptt.setNote(t.getNote());
//			ptt.setBlend(t.getBlend());
//			ptt.setContent(t.getContent());
			reagentTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * @throws ParseException
	 * @Title: saveCellPassageTemplate @Description: 获取实验主表数据
	 * @author : @date @param sc void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReagentTask findreagentTask(String id) {
		return reagentTaskDao.get(ReagentTask.class, id);
	}

	// 获取第一步的实验操作数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskPreparat> findCellNstructionsInfo(String id, String orderNum) {
		return reagentTaskDao.findCellNstructionsInfo(id, orderNum);
	}

	// 获取第一步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskRecord> findProductionRecordInfo(String id, String orderNum) {
		return reagentTaskDao.findProductionRecordInfo(id, orderNum);
	}

	// 获取第四步的实验生产检查 操作指令
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskPreparatEnd> findReagentTaskPreparatEndInfo(String id, String orderNum) {
		return reagentTaskDao.findCellPassagePreparatEndInfo(id, orderNum);
	}

	// 获取第四步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskCompleted> findCellProductionCompletedInfo(String id, String orderNum) {
		return reagentTaskDao.findCellProductionCompletedInfo(id, orderNum);
	}

	/**
	 * 
	 * @Title: selectCellPassageTempTable @Description: 展示样本表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectReagentTaskItemListPageTable(String id, String orderNum, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return reagentTaskDao.selectReagentTaskItemListPageTable(id, orderNum, start, length, query, col, sort);
	}

	/**
	 * @throws ParseException
	 * @Title: saveCellPassageTemplate @Description: 保存新模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCellPassageTems(ReagentTask sc) throws ParseException {
		// 保存生产主表
		List<TempleProducingCell> temCell = templateService.findByIdTempleProducingCell(sc.getTemplate().getId());
		if (temCell.size() > 0) {
			for (int i = 0; i < temCell.size(); i++) {
				ReagentTaskOperation cp = new ReagentTaskOperation();
				TempleProducingCell tem = commonDAO.get(TempleProducingCell.class, temCell.get(i).getId());
				if (tem != null) {
					cp.setTempleCell(tem);
					cp.setCellPassage(sc);
					cp.setOrderNum(temCell.get(i).getOrderNum());
					commonDAO.saveOrUpdate(cp);
				}
				// 保存 生产 试剂
				List<TempleProducingReagent> regenLis = templateService
						.getTempleProducingReagentListPage(temCell.get(i).getId());
				if (regenLis.size() > 0) {
					for (int n = 0; n < regenLis.size(); n++) {
						ReagentTaskReagentNew cellReagent = new ReagentTaskReagentNew();
						TempleProducingReagent templereagent = commonDAO.get(TempleProducingReagent.class,
								regenLis.get(n).getId());
						if (templereagent != null) {
							cellReagent.setTemplateReagent(templereagent);
							cellReagent.setState("0");
							cellReagent.setReagentTask(cp);
							commonDAO.saveOrUpdate(cellReagent);
						}
					}
				}

				// 保存 生产 设备
				List<TempleProducingCos> cosLis = templateService.getTempleProducingCosListPage(temCell.get(i).getId());
				if (cosLis.size() > 0) {
					for (int m = 0; m < cosLis.size(); m++) {
						ReagentTaskCosNew cellCos = new ReagentTaskCosNew();
						TempleProducingCos templerCos = commonDAO.get(TempleProducingCos.class, cosLis.get(m).getId());
						if (templerCos != null) {
							cellCos.setTemplateCos(templerCos);
							cellCos.setState("0");
							cellCos.setCellProduc(cp);
							commonDAO.saveOrUpdate(cellCos);
						}
					}
				}
			}

			// 第三步生产 保存实验结果
			ReagentTaskResults produ = new ReagentTaskResults();
			produ.setState("0");
			produ.setCellPassage(sc);
			produ.setTempleCell(temCell.get(0));
			produ.setOrderNum(temCell.get(0).getOrderNum());
			commonDAO.saveOrUpdate(produ);
		}
	}

	// 获取第三步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskOperation> findProductOperationListPage(String id, String orderNum, String templeId) {
		return reagentTaskDao.findProductOperationListPage(id, orderNum, templeId);
	}

	// 获取第三步的生产操作 试剂
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskReagentNew> findProductRegentListPage(String id, String orderNum, String templeId) {
		return reagentTaskDao.findProductRegentListPage(id, orderNum, templeId);
	}

	// 获取第三步的生产操作
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskCosNew> findCellproductCosListPage(String id, String orderNum, String templeId) {
		return reagentTaskDao.findCellproductCosListPage(id, orderNum, templeId);
	}

	// 获取第三步的生产操作 操作结果
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskResults> findCellProducResultsListPage(String id, String orderNum, String templeId) {
		return reagentTaskDao.findCellProducResultsListPage(id, orderNum, templeId);
	}

	// 获取第三步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskOperation> findByIdProductOperationList(String id) {
		return reagentTaskDao.findByIdProductOperationList(id);
	}

	// 获取第三步的实验生产检查 试剂
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskReagentNew> findByIdProductRegentList(String id) {
		return reagentTaskDao.findByIdProductRegentList(id);
	}

	// 获取第三步的实验生产检查 设备
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskCosNew> findByIdCellproductCosList(String id) {
		return reagentTaskDao.findByIdCellproductCosList(id);
	}

	// 获取第三步的实验生产检查
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskOperation> findByIdAndOrderNumProductOperation(String id, String orderNum) {
		return reagentTaskDao.findByIdAndOrderNumProductOperation(id, orderNum);
	}

	// 获取第三步的实验生产检查 设备
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskCosNew> findByIdAndOrderNumCellproductCos(String id, String orderNum) {
		return reagentTaskDao.findByIdAndOrderNumCellproductCos(id, orderNum);
	}

	// 获取第三步的 的生产结果 数据 以及开始时间和结束时间
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskResults> findCellProducResultsPage(String id, String orderNum) {
		return reagentTaskDao.findCellProducResultsPage(id, orderNum);
	}

	/**
	 * @throws Exception
	 * @Title: findCellPrimaryCultureTemplateList @Description: TODO @author :
	 *         nan.jiang @date 2018-8-30上午9:40:13 @param start @param length @param
	 *         query @param col @param sort @param ids @return Map
	 *         <String,Object> @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTempRecordList(Integer start, Integer length, String query,
			String col, String sort, String ids) throws Exception {
		return reagentTaskDao.findCellPrimaryCultureTempRecordList(start, length, query, col, sort, ids);
	}

	/**
	 * @throws Exception
	 * @Title: findCellPrimaryCultureTemplateList @Description: TODO @author :
	 *         nan.jiang @date 2018-8-30上午9:40:13 @param start @param length @param
	 *         query @param col @param sort @param ids @return Map
	 *         <String,Object> @throws
	 */
	public List<ReagentTaskInfo> findByReagentTaskIdPage(String id) throws Exception {
		return reagentTaskDao.findByReagentTaskIdPage(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReagentTaskTemplate getReagentTaskResultsByCellAndStep(String stepNum, String mainId) {
		return reagentTaskDao.getReagentTaskResultsByCellAndStep(stepNum, mainId);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<ReagentTaskOperation> getReagentTaskOperationListPage(String id) {
		return reagentTaskDao.getReagentTaskOperationListPage(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReagentTaskTemplate getReagentTaskTemplateByCellAndStep(String stepNum, String mainId) {
		// TODO Auto-generated method stub
		return reagentTaskDao.getReagentTaskTemplateByCellAndStep(stepNum, mainId);
	}

	public List<ReagentTaskOperation> getReagentTaskOperationList(String id, String stepNum) {
		return reagentTaskDao.getReagentTaskOperationList(id, stepNum);
	}

	public List<ReagentTaskCosNew> getReagentTaskCosNewList(String id) {
		return reagentTaskDao.getReagentTaskCosNewList(id);
	}

	public List<IncubatorSampleInfo> showWellPlate1(String id) {
		List<IncubatorSampleInfo> list = reagentTaskDao.showWellPlate1(id);
		return list;
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		for (String da : data) {
			String[] d = da.split(",");
			ReagentTaskInfo rti = commonDAO.get(ReagentTaskInfo.class, d[0]);

			rti.setPosId(d[1]);
			rti.setCounts(d[2]);
			reagentTaskDao.saveOrUpdate(rti);

			List<IncubatorSampleInfo> isis = reagentTaskDao.getIncubatorSampleInfoBySid(d[0]);
			if (isis.size() > 0) {
				Instrument ii = commonDAO.get(Instrument.class, d[2]);

				IncubatorSampleInfo isi = isis.get(0);

				IncubatorSampleInfoOut isio = new IncubatorSampleInfoOut();
				isio.setId(null);
				if (!"".equals(rti.getSerial()) && rti.getSerial() != null) {
					isio.setBatch(rti.getSerial());
				}
				isio.setIncubatorId(isi.getIncubatorId());
				isio.setLocation(isi.getLocation());
				isio.setSampleOutDate(new Date());
				isio.setSampleOutUser(user);
				isio.setTaskId(rti.getReagentTask().getId());
//				isio.setTaskStepNum(String.valueOf(rti.getStepNum()));
				reagentTaskDao.saveOrUpdate(isio);

				if (d[2].equals(isi.getIncubatorId())) {
					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() + 1);
				}

				if (!"".equals(rti.getSerial()) && rti.getSerial() != null) {
					isio.setBatch(rti.getSerial());
				}
				isi.setIncubatorId(rti.getInstrument().getId());
				isi.setLocation(d[1]);
//				isi.setSampleOrder(rti.getSampleOrder());
				isi.setState("1");
				isi.setTaskId(rti.getReagentTask().getId());
//				isi.setTaskStepNum(String.valueOf(rti.getStepNum()));
				isi.setsId(d[0]);
				reagentTaskDao.saveOrUpdate(isi);

				IncubatorSampleInfoIn isii = new IncubatorSampleInfoIn();
				isii.setId(null);
				if (!"".equals(rti.getSerial()) && rti.getSerial() != null) {
					isio.setBatch(rti.getSerial());
				}
				isii.setIncubatorId(rti.getInstrument().getId());
				isii.setLocation(d[1]);
				isii.setSampleInDate(new Date());
				isii.setSampleInUser(user);
				isii.setTaskId(rti.getReagentTask().getId());
//				isii.setTaskStepNum(String.valueOf(pti.getStepNum()));
				reagentTaskDao.saveOrUpdate(isii);

				if (d[2].equals(isi.getIncubatorId())) {
					ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() - 1);
				} else {
					Instrument ii2 = commonDAO.get(Instrument.class, isi.getIncubatorId());
					ii2.setSurplusLocationsNumber(ii2.getSurplusLocationsNumber() - 1);
					commonDAO.saveOrUpdate(ii2);
				}
				commonDAO.saveOrUpdate(ii);

			} else {
				IncubatorSampleInfo isi = new IncubatorSampleInfo();
				isi.setId(null);
				if (!"".equals(rti.getSerial()) && rti.getSerial() != null) {
					isi.setBatch(rti.getSerial());
				}
				isi.setIncubatorId(rti.getInstrument().getId());
				isi.setLocation(d[1]);
//				isi.setSampleOrder(rti.getSampleOrder());
				isi.setState("1");
				isi.setTaskId(rti.getReagentTask().getId());
//				isi.setTaskStepNum(String.valueOf(rti.getStepNum()));
				isi.setsId(d[0]);
				reagentTaskDao.saveOrUpdate(isi);

				IncubatorSampleInfoIn isii = new IncubatorSampleInfoIn();
				if (!"".equals(rti.getSerial()) && rti.getSerial() != null) {
					isi.setBatch(rti.getSerial());
				}
				isii.setId(null);
				isii.setIncubatorId(rti.getInstrument().getId());
				isii.setLocation(d[1]);
				isii.setSampleInDate(new Date());
				isii.setSampleInUser(user);
				isii.setTaskId(rti.getReagentTask().getId());
//				isii.setTaskStepNum(String.valueOf(rti.getStepNum()));
				reagentTaskDao.saveOrUpdate(isii);

				Instrument ii = commonDAO.get(Instrument.class, d[2]);
				ii.setSurplusLocationsNumber(ii.getSurplusLocationsNumber() - 1);
				commonDAO.saveOrUpdate(ii);
			}
		}
	}

	public void saveChangeLog(String changeLog) {
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setModifyContent(changeLog);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}

	}

	public Boolean findC(String serial) {
		Boolean c = true;
		List<ReagentTaskInfo> list = new ArrayList<ReagentTaskInfo>();
		list = reagentTaskDao.findC(serial);
		if (list.size() > 1) {
			c = false;
		}
		return c;
	}

	public Boolean findCk(String serial) {
		Boolean c = true;
		List<StorageReagentBuySerial> stor = new ArrayList<StorageReagentBuySerial>();
		stor = reagentTaskDao.findCk(serial);
		if (stor.size() > 1) {
			c = false;
		}
		return c;
	}

	public String selectMaxSlideCode(String id) {
		return reagentTaskDao.selectMaxSlideCode(id);
	}
	
	public List<ReagentTaskInfo> findByReagentTaskIdPage1(String id) throws Exception {
		return reagentTaskDao.findByReagentTaskIdPage1(id);
	}

	public Integer findByReagentTaskIdPage2(String id) {
		return reagentTaskDao.findByReagentTaskIdPage2(id);
	}
}
