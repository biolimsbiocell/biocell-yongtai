package com.biolims.experiment.reagent.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.IncubatorSampleInfoOut;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.reagent.dao.ReagentTaskDao;
import com.biolims.experiment.reagent.model.ReagentTask;
import com.biolims.experiment.reagent.model.ReagentTaskAbnormal;
import com.biolims.experiment.reagent.model.ReagentTaskCos;
import com.biolims.experiment.reagent.model.ReagentTaskCosNew;
import com.biolims.experiment.reagent.model.ReagentTaskInfo;
import com.biolims.experiment.reagent.model.ReagentTaskInfoQuality;
import com.biolims.experiment.reagent.model.ReagentTaskItem;
import com.biolims.experiment.reagent.model.ReagentTaskOperation;
import com.biolims.experiment.reagent.model.ReagentTaskReagent;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.experiment.reagent.model.ReagentTaskTemp;
import com.biolims.experiment.reagent.model.ReagentTaskTemplate;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.model.RoomState;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.tra.transport.model.TransportApplyTemp;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service
@Transactional
public class ReagentTaskService {

	@Resource
	private ReagentTaskDao reagentTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private ReagentTaskNewService reagentTaskNewService;
	@Resource
	private CommonService commonService;

	/**
	 * 
	 * @Title: get @Description:根据ID获取主表 @author : @date @param id @return
	 *         ReagentTask @throws
	 */
	public ReagentTask get(String id) {
		ReagentTask reagentTask = commonDAO.get(ReagentTask.class, id);
		return reagentTask;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentTaskItem(String delStr, String[] ids, User user, String reagentTask_id) throws Exception {
		String delId = "";
		for (String id : ids) {
			ReagentTaskItem scp = reagentTaskDao.get(ReagentTaskItem.class, id);
			if (scp.getId() != null) {
				ReagentTask pt = reagentTaskDao.get(ReagentTask.class, scp.getReagentTask().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				ReagentTaskTemp reagentTaskTemp = this.commonDAO.get(ReagentTaskTemp.class, scp.getTempId());
				if (reagentTaskTemp != null) {
					pt.setSampleNum(pt.getSampleNum() - 1);
					reagentTaskTemp.setState("1");
					reagentTaskDao.update(reagentTaskTemp);
				}
				reagentTaskDao.update(pt);
				reagentTaskDao.delete(scp);
			}
			delId += scp.getSampleCode() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(reagentTask_id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(delStr + delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delReagentTaskItemAf @Description: 重新排板 @author : @date @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentTaskItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			ReagentTaskItem scp = reagentTaskDao.get(ReagentTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				reagentTaskDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentTaskResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr = "";
		for (String id : ids) {
			ReagentTaskInfo scp = reagentTaskDao.get(ReagentTaskInfo.class, id);
			if (!"1".equals(scp.getSubmit())) {
				reagentTaskDao.delete(scp);
				idStr += scp.getSampleCode() + ",";
			}
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		li.setUserId(u.getId());
		li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setState("3");
		li.setStateName("数据修改");
		li.setModifyContent(delStr + idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentTaskReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			ReagentTaskReagent scp = reagentTaskDao.get(ReagentTaskReagent.class, id);
			reagentTaskDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(delStr + scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentTaskCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			ReagentTaskCos scp = reagentTaskDao.get(ReagentTaskCos.class, id);
			reagentTaskDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(delStr + scp.getName());
		}
	}

	/**
	 * 
	 * @Title: saveReagentTaskTemplate @Description: 保存模板 @author : @date @param sc
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagentTaskTemplate(ReagentTask sc) {
		List<ReagentTaskTemplate> tlist2 = reagentTaskDao.delTemplateItem(sc.getId());
		List<ReagentTaskReagent> rlist2 = reagentTaskDao.delReagentItem(sc.getId());
		List<ReagentTaskCos> clist2 = reagentTaskDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate().getId(), null);
		for (TemplateItem t : tlist) {
			ReagentTaskTemplate ptt = new ReagentTaskTemplate();
			ptt.setReagentTask(sc);
			ptt.setOrderNum(String.valueOf(t.getOrderNum()));
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			reagentTaskDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			ReagentTaskReagent ptr = new ReagentTaskReagent();
			ptr.setReagentTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			reagentTaskDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			ReagentTaskCos ptc = new ReagentTaskCos();
			ptc.setReagentTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			reagentTaskDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState @Description:改变状态 @author : @date @param
	 *         applicationTypeActionId @param id @throws Exception void @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		ReagentTask sct = reagentTaskDao.get(ReagentTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		reagentTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		List<ReagentTaskInfo> reagentList = reagentTaskNewService.findByReagentTaskIdPage(id);
		if (!reagentList.isEmpty()) {
			Integer reagentListCount = reagentTaskNewService.findByReagentTaskIdPage2(id);
			for (int i = 0; i < reagentList.size(); i++) {
				StorageReagentBuySerial stor = new StorageReagentBuySerial();
				if (reagentListCount != null) {
					stor.setNum(Double.valueOf("1"));
				}
				Date dd = sdf.parse(reagentList.get(i).getSxDate());
				stor.setExpireDate(dd);
				stor.setSerial(reagentList.get(i).getSerial());
				stor.setProductDate(new Date());
				stor.setInDate(new Date());
				stor.setIsGood("1");
				stor.setQcState("1");
				stor.setStorage(reagentList.get(i).getStorage());

				commonDAO.saveOrUpdate(stor);
				// 保存试剂
				reagentList.get(i).setState("1");
				commonDAO.saveOrUpdate(reagentList.get(i));
			}
		}

		// submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : @date @param id @param
	 *         ids @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		ReagentTask sc = this.reagentTaskDao.get(ReagentTask.class, id);
		// 获取结果表样本信息
		List<ReagentTaskInfo> list;
		if (ids == null)
			list = this.reagentTaskDao.selectAllResultListById(id);
		else
			list = this.reagentTaskDao.selectAllResultListByIds(ids);

		for (ReagentTaskInfo scp : list) {
			if (scp != null && scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("ReagentTaskInfo");
						reagentTaskDao.saveOrUpdate(st);

					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0017")) {// 核酸提取
						// 核酸提取
						DnaTaskTemp d = new DnaTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						reagentTaskDao.saveOrUpdate(d);
					} else if (next.equals("0018")) {// 文库构建
						// 文库构建
						WkTaskTemp d = new WkTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setSampleCode(scp.getSampleCode());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						d.setSampleInfo(scp.getSampleInfo());
						reagentTaskDao.saveOrUpdate(d);

					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					ReagentTaskAbnormal pa = new ReagentTaskAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService.saveSampleState(scp.getCode(), scp.getSampleCode(), scp.getProductId(),
						scp.getProductName(), "", sc.getCreateDate(), format.format(new Date()), "ReagentTask", "试剂制备",
						(User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY),
						id, scp.getNextFlow(), scp.getResult(), null, null, null, null, null, null, null, null);

				scp.setSubmit("1");
				reagentTaskDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findReagentTaskTable @Description: 展示主表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findReagentTaskTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return reagentTaskDao.findReagentTaskTable(start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: selectReagentTaskTempTable @Description: 展示临时表 @author : @date @param
	 *         start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> selectReagentTaskTempTable(String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return reagentTaskDao.selectReagentTaskTempTable(codes, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配 @author : @date @param main @param
	 *         tempId @param userId @param templateId @return @throws Exception
	 *         String @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String itemJson, String userId, String templateId, String logInfo)
			throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			ReagentTask pt = new ReagentTask();
			pt = (ReagentTask) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals("")) || pt.getId().equals("NEW")) {
				String modelName = "ReagentTask";
				String markCode = "RT";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			} else {
				id = pt.getId();
				pt = commonDAO.get(ReagentTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String[] users = userId.split(",");
			String userName = "";
			for (int i = 0; i < users.length; i++) {
				if (i == users.length - 1) {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName();
				} else {
					User user = commonDAO.get(User.class, users[i]);
					userName += user.getName() + ",";
				}
			}
			pt.setTestUserOneName(userName);
			reagentTaskDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveReagentTaskTemplate(pt);
			}
			if (itemJson != null) {
				List<Map<String, Object>> listJsonData = JsonUtils.toListByJsonArray(itemJson, List.class);
				ReagentTaskItem item = new ReagentTaskItem();
				item = (ReagentTaskItem) commonDAO.Map2Bean(listJsonData.get(0), item);
				if ((item.getId() == null || "".equals(item.getId())) || item.getId().equals("NEW")) {
					String modelName = "ReagentTaskItem";
					String markCode = "RTI";
					String itemId = codingRuleService.genTransID(modelName, markCode);
					item.setId(itemId);
					item.setReagentTask(pt);
					commonDAO.saveOrUpdate(item);
				} else {
					commonDAO.saveOrUpdate(item);
				}
			}
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setState("3");
				li.setStateName("数据修改");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findReagentTaskItemTable @Description:展示未排版样本 @author : @date @param
	 *         id @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findReagentTaskItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return reagentTaskDao.findReagentTaskItemTable(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排板数据 @author : @date @param id @param
	 *         item @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<ReagentTaskItem> saveItems = new ArrayList<ReagentTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		ReagentTask pt = commonDAO.get(ReagentTask.class, id);
		ReagentTaskItem scp = new ReagentTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (ReagentTaskItem) reagentTaskDao.Map2Bean(map, scp);
			ReagentTaskItem pti = commonDAO.get(ReagentTaskItem.class, scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());
			scp.setReagentTask(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		reagentTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate @Description: 展示孔板 @author : @date @param
	 *         id @return @throws Exception List<ReagentTaskItem> @throws
	 */
	public List<ReagentTaskItem> showWellPlate(String id) throws Exception {
		List<ReagentTaskItem> list = reagentTaskDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findReagentTaskItemAfTable @Description: 展示排板后 @author : @date @param
	 *         scId @param start @param length @param query @param col @param
	 *         sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> findReagentTaskItemAfTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return reagentTaskDao.findReagentTaskItemAfTable(scId, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @param data void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			ReagentTaskItem pti = commonDAO.get(ReagentTaskItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			reagentTaskDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showReagentTaskStepsJson @Description: 展示实验步骤 @author : @date @param
	 *         id @param code @return Map<String,Object> @throws
	 */
	public Map<String, Object> showReagentTaskStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<ReagentTaskTemplate> pttList = reagentTaskDao.showReagentTaskStepsJson(id, code);
		List<ReagentTaskReagent> ptrList = reagentTaskDao.showReagentTaskReagentJson(id, code);
		List<ReagentTaskCos> ptcList = reagentTaskDao.showReagentTaskCosJson(id, code);
		List<Object> plate = reagentTaskDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showReagentTaskResultTableJson @Description: 展示结果 @author
	 *         : @date @param id @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> showReagentTaskResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return reagentTaskDao.showReagentTaskResultTableJson(id, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @param id @param
	 *         counts @return Map<String,Object> @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		ReagentTask pt = commonDAO.get(ReagentTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<ReagentTaskItem> list = reagentTaskDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent @Description: 上传结果 @author : @date @param id @param
	 *         fileId @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = reagentTaskDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			ReagentTask pt = reagentTaskDao.get(ReagentTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<ReagentTaskInfo> listPTI = reagentTaskDao.findReagentTaskInfoByCode(code);
						if (listPTI.size() > 0) {
							ReagentTaskInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							reagentTaskDao.saveOrUpdate(spi);
						}

					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @param id @param
	 *         templateJson @param reagentJson @param cosJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson, String cosJson, String logInfo) {
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}") && !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}") && !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate @Description: 保存模板明细 @author : @date @param id @param
	 *         templateJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			ReagentTaskTemplate ptt = reagentTaskDao.get(ReagentTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			reagentTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent @Description: 保存试剂 @author : @date @param id @param
	 *         reagentJson void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {

		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			ReagentTaskReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(ReagentTaskReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new ReagentTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setReagentTask(commonDAO.get(ReagentTask.class, id));
				}

			}
			reagentTaskDao.saveOrUpdate(ptr);
		}

	}

	/**
	 * 
	 * @Title: saveCos @Description: 保存设备 @author : @date @param id @param cosJson
	 *         void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {

		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			ReagentTaskCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(ReagentTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new ReagentTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setReagentTask(commonDAO.get(ReagentTask.class, id));
				}
			}
			reagentTaskDao.saveOrUpdate(ptc);
		}

	}

	/**
	 * 
	 * @Title: plateSampleTable @Description: 孔板样本列表 @author : @date @param
	 *         id @param counts @param start @param length @param query @param
	 *         col @param sort @return @throws Exception Map<String,Object> @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return reagentTaskDao.plateSampleTable(id, counts, start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @param id @throws
	 *         Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {

		List<ReagentTaskItem> list = reagentTaskDao.showWellPlate(id);
		ReagentTask reagentTask = commonDAO.get(ReagentTask.class, id);
		ArrayList<ReagentTaskInfo> infoList = new ArrayList<ReagentTaskInfo>();
		for (ReagentTaskItem item : list) {
			ReagentTaskInfo info = new ReagentTaskInfo();
			info.setCode(item.getQcSampleId());
			info.setResult(item.getQcResult());
			info.setReagentTask(reagentTask);
			infoList.add(info);
		}
		commonDAO.saveOrUpdateAll(infoList);
	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果 @author : @date @param id @param
	 *         dataJson @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo, String confirmUser) throws Exception {

		List<ReagentTaskInfo> saveItems = new ArrayList<ReagentTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		ReagentTask pt = commonDAO.get(ReagentTask.class, id);
		if (list.size() > 0) {
			ReagentTaskInfo scp = new ReagentTaskInfo();
			// 将map信息读入实体类
			scp = (ReagentTaskInfo) reagentTaskDao.Map2Bean(list.get(0), scp);
			scp.setReagentTask(pt);
			saveItems.add(scp);
		}
		/*
		 * for (Map<String, Object> map : list) { ReagentTaskInfo scp = new
		 * ReagentTaskInfo(); // 将map信息读入实体类 scp = (ReagentTaskInfo)
		 * reagentTaskDao.Map2Bean(map, scp);
		 * 
		 * scp.setReagentTask(pt); saveItems.add(scp); }
		 */
		if (confirmUser != null && !"".equals(confirmUser)) {
			User confirm = commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			reagentTaskDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		reagentTaskDao.saveOrUpdateAll(saveItems);

	}

	public List<ReagentTaskItem> findReagentTaskItemList(String scId) throws Exception {
		List<ReagentTaskItem> list = reagentTaskDao.selectReagentTaskItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return reagentTaskDao.generateBlendCode(id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(ReagentTask pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				ReagentTaskItem pti = new ReagentTaskItem();
				pti.setReagentTask(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				reagentTaskDao.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRightQuality(ReagentTask pt, String[] ids, String logInfo) throws Exception {
		if (ids != null && !"".equals(ids)) {
			for (int i = 0; i < ids.length; i++) {
				ReagentTaskItem pti = new ReagentTaskItem();
				pti.setReagentTask(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public ReagentTaskInfo getInfoById(String idc) {
		return reagentTaskDao.get(ReagentTaskInfo.class, idc);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSJQualityTest(String[] ids) {
		for (String id : ids) {
			ReagentTaskInfo qti = new ReagentTaskInfo();
			qti = commonDAO.get(ReagentTaskInfo.class, id);
			List<ZhiJianItem> temItemList = reagentTaskDao.getZjItem(qti.getReagentTask().getTemplate().getId());
			for (ZhiJianItem zji : temItemList) {
				if (!"1".equals(qti.getSubmit())) {
					if ("zj01".equals(zji.getTypeId())) {// 自主
						QualityTestTemp qtt = new QualityTestTemp();
						qtt.setState("1");
						Storage st = new Storage();
						st = commonDAO.get(Storage.class, qti.getStorage().getId());
						qtt.setStorage(st);
//						qtt.setSerial(qti.getSerial());
						qtt.setCode(qti.getSerial());
						qtt.setCellType("3");
						SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, zji.getCode());
						if (sd != null) {
							qtt.setSampleDeteyion(sd);
						}
						commonDAO.saveOrUpdate(qtt);
					} else if ("zj02".equals(zji.getTypeId())) {// 第三方
						QualityTestTemp qtt = new QualityTestTemp();
						qtt.setState("1");
						Storage st = new Storage();
						st = commonDAO.get(Storage.class, qti.getStorage().getId());
						qtt.setStorage(st);
//						qtt.setSerial(qti.getSerial());
						qtt.setCode(qti.getSerial());
						qtt.setCellType("7");
						SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, zji.getCode());
						if (sd != null) {
							qtt.setSampleDeteyion(sd);
						}
						commonDAO.saveOrUpdate(qtt);
					}

				}

			}
			qti.setSubmit("1");
			commonDAO.saveOrUpdate(qti);

		}
	}

	public List<ReagentTaskCosNew> getCellproductCoss(String id, String stepNum) {
		return reagentTaskDao.findCellproductCoss(id, stepNum);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCosPrepare(String cosListJson, String roomName, String zbid, String bzs, String roomId,
			String changeLog) {

		JSONArray array = JSONArray.fromObject(cosListJson);
		if (array.size() > 0) {
			// 遍历 jsonarray 数组，把每一个对象转成 json 对象
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i);
				String id = (String) job.get("id");
				String code = (String) job.get("code");
				String name = (String) job.get("name");
				if (id != null && !"".equals(id)) {
					ReagentTaskCosNew cpc = commonDAO.get(ReagentTaskCosNew.class, id);
					if (cpc != null) {
						if (code != null && !"".equals(code)) {
							cpc.setInstrumentCode(code);
						}
						if (name != null && !"".equals(name)) {
							cpc.setInstrumentName(name);
						}
						reagentTaskDao.saveOrUpdate(cpc);
					}
				}
			}
		}
		List<ReagentTaskRecord> cpct = reagentTaskDao.getCellProductionRecordByIdAndStepNumForzy(zbid, bzs);
		if (cpct.size() > 0) {
			ReagentTaskRecord cpctz = cpct.get(0);
			if (roomName != null && !"".equals(roomName)) {
				if (cpctz != null) {
					if (cpctz.getContent() != null) {
						JSONObject jsonCon = JSONObject.fromObject(cpctz.getContent());
						jsonCon.put("operatingRoomName", roomName);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
					} else {
						JSONObject jsonCon = new JSONObject();
						jsonCon.put("operatingRoomName", roomName);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
					}
				}
			}
			if (roomId != null && !"".equals(roomId)) {
				if (cpctz != null) {
					if (cpctz.getContent() != null) {
						JSONObject jsonCon = JSONObject.fromObject(cpctz.getContent());
						jsonCon.put("operatingRoomId", roomId);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
						cpctz.setOperatingRoomId(roomId);
					} else {
						JSONObject jsonCon = new JSONObject();
						jsonCon.put("operatingRoomId", roomId);
						cpctz.setContent(jsonCon.toString());
						cpctz.setOperatingRoomName(roomName);
						cpctz.setOperatingRoomId(roomId);
					}
				}
			}
			if (cpctz != null) {
				reagentTaskDao.saveOrUpdate(cpctz);
			}

			if (changeLog != null && !"".equals(changeLog)) {
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setClassName("ReagentTask");
				li.setFileId(cpctz.getReagentTask().getId());
				li.setUserId(u.getId());
				li.setModifyContent(changeLog);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	public Boolean startPrepare(String cosListJson, String roomId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Boolean zy = true;

		if (!"".equals(roomId) && roomId != null) {
			List<RoomManagement> list = new ArrayList<RoomManagement>();
			list = commonDAO.findByProperty(RoomManagement.class, "id", roomId);
			if (!list.isEmpty()) {
				RoomManagement rm = new RoomManagement();
				rm = list.get(0);
				if (rm != null) {
					if ("1".equals(rm.getIsFull())) {
						zy = false;
					} else {
//						rm.setIsFull("1");
//						commonDAO.saveOrUpdate(rm);
					}
				}
			}
		}
		JSONArray array = JSONArray.fromObject(cosListJson);
		Map<String, String> map = new HashMap<String, String>();
		if (array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
				String code = (String) job.get("code");
				if (code != null && !"".equals(code)) {
					if (map.get(code) != null && !"".equals(map.get(code)) && code.equals(map.get(code))) {

					} else {
						map.put(code, code);
						Instrument it = new Instrument();
						it = commonDAO.get(Instrument.class, code);
						if (it != null) {
							if ("1".equals(it.getIsFull())) {
								zy = false;
							} else {

							}
						}
					}
				}
			}
//			if(zy){
//				for (int i = 0; i < array.size(); i++) {
//					JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
//					String code = (String) job.get("code");
//					if (code != null && !"".equals(code)) {
//						Instrument it = new Instrument();
//						it = commonDAO.get(Instrument.class, code);
//						if(it!=null){
//							if("1".equals(it.getIsFull())){
//								zy = false;
//							}else{
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									
//								}else{
//									it.setIsFull("1");
//								}
//								
//								commonDAO.saveOrUpdate(it);
//								
//								InstrumentState is = new InstrumentState();
//								is.setId(null);
//								is.setInstrumentCode(it.getId());
//								is.setStageName("细胞生产");
//								is.setTableTypeId((String) job.get("rwdid"));
//								is.setNote((String) job.get("bzorderNum"));
//								is.setNote2((String) job.get("bzname"));
//								//查询第一步实验操作人  按实验id和步骤查询
//								
//								List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy((String) job.get("rwdid"),(String) job.get("bzorderNum"));
//								if(cp.size()>0){
//									CellProductionRecord cpr = cp.get(0);
//									if(cpr.getTempleOperator()!=null){
//										is.setAcceptUser(cpr.getTempleOperator().getName());
//									}else{
//										is.setAcceptUser("");
//									}
//								}else{
//									is.setAcceptUser("");
//								}
//								is.setStartDate(sdf.format(new Date()));
//								is.setEndDate("");
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									is.setInstrumentState("开始使用");
//								}else{
//									is.setInstrumentState("开始占用");
//								}
//								is.setInstrument(it);
//								commonDAO.saveOrUpdate(is);
//							}
//						}
//					}
//				}
//			}else{
//				
//			}
		}
		return zy;

	}

	public void startPreparezy(String cosListJson, String roomName, String roomId) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Boolean zy = true;

		/**
		 * 房间 设备占用 if(房间占用){ }else{ . 没占用 for(设备){ if(){ if(设备占用){ }else{ } }
		 */

		JSONArray array = JSONArray.fromObject(cosListJson);
		Map<String, String> map = new HashMap<String, String>();
		String rwdid = "";
		String bzorderNum = "";
		String bzname = "";
		String batch = "";
		if (array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
				rwdid = (String) job.get("rwdid");
				bzorderNum = (String) job.get("bzorderNum");
				bzname = (String) job.get("bzname");
				batch = (String) job.get("batch");
			}
		}
		if (!"".equals(roomId) && roomId != null) {
			List<RoomManagement> list = new ArrayList<RoomManagement>();
			list = commonDAO.findByProperty(RoomManagement.class, "id", roomId);
			if (!list.isEmpty()) {
				RoomManagement rm = new RoomManagement();
				rm = list.get(0);
				if (rm != null) {
					if ("1".equals(rm.getIsFull())) {
						zy = false;
					} else {
						rm.setIsFull("1");
						commonDAO.saveOrUpdate(rm);

						RoomState rs = new RoomState();

						rs.setId(null);
						rs.setRoomCode(rm.getId());
						rs.setStageName("试剂制备");
						rs.setTableTypeId(rwdid);
						rs.setNote(bzorderNum);
						rs.setNote2(bzname);

						// 查询第一步实验操作人 按实验id和步骤查询

						List<ReagentTaskRecord> cp = reagentTaskDao.getCellProductionRecordByIdAndStepNumForzy(rwdid,
								bzorderNum);
						if (cp.size() > 0) {
							ReagentTaskRecord cpr = cp.get(0);
							if (cpr.getTempleOperator() != null) {

								rs.setAcceptUser(cpr.getTempleOperator().getName());
							} else {

								rs.setAcceptUser("");
							}
						} else {

							rs.setAcceptUser("");
						}

						rs.setStartDate(sdf.format(new Date()));
						rs.setEndDate("");

						rs.setRoomState("开始占用");

						rs.setRoomManagement(rm);
						commonDAO.saveOrUpdate(rs);
					}
				}
			}
		}
		if (array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
				String code = (String) job.get("code");
				if (code != null && !"".equals(code)) {
					if (map.get(code) != null && !"".equals(map.get(code)) && code.equals(map.get(code))) {

					} else {
						map.put(code, code);
						rwdid = (String) job.get("rwdid");
						bzorderNum = (String) job.get("bzorderNum");
						bzname = (String) job.get("bzname");
						Instrument it = new Instrument();
						it = commonDAO.get(Instrument.class, code);
						if (it != null) {
							if ("1".equals(it.getIsFull())) {
								zy = false;
							} else {

							}
						}
					}
				}
			}
			if (zy) {
				for (String key : map.keySet()) {// keySet获取map集合key的集合  然后在遍历key即可
					String code = map.get(key).toString();
					if (code != null && !"".equals(code)) {
						Instrument it = new Instrument();
						it = commonDAO.get(Instrument.class, code);
						if (it != null) {
							if ("1".equals(it.getIsFull())) {
								zy = false;
							} else {
								if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
								} else {
									it.setIsFull("1");
								}

								commonDAO.saveOrUpdate(it);

								InstrumentState is = new InstrumentState();
								is.setId(null);
								is.setInstrumentCode(it.getId());
								is.setStageName("试剂制备");
								is.setTableTypeId(rwdid);
								is.setNote(bzorderNum);
								is.setNote2(bzname);
								// 查询第一步实验操作人 按实验id和步骤查询

								List<ReagentTaskRecord> cp = reagentTaskDao
										.getCellProductionRecordByIdAndStepNumForzy(rwdid, bzorderNum);
								if (cp.size() > 0) {
									ReagentTaskRecord cpr = cp.get(0);
									if (cpr.getTempleOperator() != null) {
										is.setAcceptUser(cpr.getTempleOperator().getName());
									} else {
										is.setAcceptUser("");
									}
								} else {
									is.setAcceptUser("");
								}
								is.setStartDate(sdf.format(new Date()));
								is.setEndDate("");
								if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
									is.setInstrumentState("开始使用");
								} else {
									is.setInstrumentState("开始占用");
								}
								is.setInstrument(it);

								commonDAO.saveOrUpdate(is);

							}
						}
					}
				}
//				for (int i = 0; i < array.size(); i++) {
//					JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
//					String code = (String) job.get("code");
//					if (code != null && !"".equals(code)) {
//						Instrument it = new Instrument();
//						it = commonDAO.get(Instrument.class, code);
//						if(it!=null){
//							if("1".equals(it.getIsFull())){
//								zy = false;
//							}else{
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									
//								}else{
//									it.setIsFull("1");
//								}
//								
//								commonDAO.saveOrUpdate(it);
//								
//								InstrumentState is = new InstrumentState();
//								is.setId(null);
//								is.setInstrumentCode(it.getId());
//								is.setStageName("细胞生产");
//								is.setTableTypeId((String) job.get("rwdid"));
//								is.setNote((String) job.get("bzorderNum"));
//								is.setNote2((String) job.get("bzname"));
//								//查询第一步实验操作人  按实验id和步骤查询
//								
//								List<CellProductionRecord> cp = cellPassageDao.getCellProductionRecordByIdAndStepNumForzy((String) job.get("rwdid"),(String) job.get("bzorderNum"));
//								if(cp.size()>0){
//									CellProductionRecord cpr = cp.get(0);
//									if(cpr.getTempleOperator()!=null){
//										is.setAcceptUser(cpr.getTempleOperator().getName());
//									}else{
//										is.setAcceptUser("");
//									}
//								}else{
//									is.setAcceptUser("");
//								}
//								is.setStartDate(sdf.format(new Date()));
//								is.setEndDate("");
//								if("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())){
//									is.setInstrumentState("开始使用");
//								}else{
//									is.setInstrumentState("开始占用");
//								}
//								is.setInstrument(it);
//								commonDAO.saveOrUpdate(is);
//							}
//						}
//					}
//				}
			} else {

			}
		}
//		return zy;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void finishPresentStep(String id, String[] ids, String stepNum, String mainId, String startTime,
			String endTime, String hhzt) {

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		SimpleDateFormat df111 = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		List<ReagentTaskRecord> cprs = reagentTaskDao.getReagentTaskRecords(mainId, stepNum);
		String step = "";// String.valueOf(Integer.valueOf(stepNum) + 1);// 下一步
		if (cprs.size() > 0) {
			step = cprs.get(0).getOrderNum();
		} else {
			step = String.valueOf(Integer.valueOf(Double.valueOf(stepNum).intValue()) + 1);// 下一步
		}
		ReagentTaskTemplate ct = reagentTaskDao.findCellPassageTemplateByStep(stepNum, mainId);
		ReagentTaskRecord cpr = reagentTaskDao.findCellProductionRecordByStep(stepNum, mainId);
		ReagentTaskTemplate ctc = reagentTaskDao.findCellPassageTemplateByStep(step, mainId);

		List<ReagentTaskOperation> cpoList = reagentTaskDao.findReagentTaskOperationByStep(stepNum, mainId);
		// 解除房间占用
		List<RoomManagement> rmList = new ArrayList<RoomManagement>();
		rmList = commonDAO.findByProperty(RoomManagement.class, "roomName", cpr.getOperatingRoomName());
		if (!rmList.isEmpty()) {
			RoomManagement rm = new RoomManagement();
			rm = rmList.get(0);
			rm.setIsFull("0");
			RoomState rs = new RoomState();
			rs.setId(null);
			rs.setRoomCode(rm.getId());
			rs.setStageName("试剂制备");
			rs.setTableTypeId(mainId);
			rs.setNote(stepNum);
			rs.setNote2(cpr.getTempleItem().getName());
			List<ReagentTaskRecord> cp = reagentTaskDao.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
			if (cp.size() > 0) {
				ReagentTaskRecord cprr = cp.get(0);
				if (cprr.getTempleOperator() != null) {
					rs.setAcceptUser(cprr.getTempleOperator().getName());
				} else {
					rs.setAcceptUser("");
				}
			} else {
				rs.setAcceptUser("");
			}
			rs.setStartDate("");
			rs.setEndDate(df111.format(new Date()));
			rs.setRoomState("解除占用");
			rs.setRoomManagement(rm);
			commonDAO.saveOrUpdate(rs);
			commonDAO.saveOrUpdate(rm);
		}
		// 解除仪器
		List<ReagentTaskCosNew> ccList = new ArrayList<ReagentTaskCosNew>();
		if (!cpoList.isEmpty()) {
			for (ReagentTaskOperation cpo : cpoList) {
				ccList = reagentTaskDao.getReagentTaskCosNew(cpo.getId());
				if (!ccList.isEmpty()) {
					for (ReagentTaskCosNew cc : ccList) {
						if (cc.getInstrumentCode() != null && !"".equals(cc.getInstrumentCode())) {
							Instrument it = new Instrument();
							it = commonDAO.get(Instrument.class, cc.getInstrumentCode());

							if (it.getIsFull() != null && "1".equals(it.getIsFull())) {
								InstrumentState is = new InstrumentState();
								is.setId(null);
								is.setInstrumentCode(it.getId());
								is.setStageName("试剂制备");
								is.setTableTypeId(mainId);
								is.setNote(stepNum);
								is.setNote2(cpr.getTempleItem().getName());
								// 查询第一步实验操作人 按实验id和步骤查询

								List<ReagentTaskRecord> cp = reagentTaskDao
										.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
								if (cp.size() > 0) {
									ReagentTaskRecord cprr = cp.get(0);
									if (cprr.getTempleOperator() != null) {
										is.setAcceptUser(cprr.getTempleOperator().getName());
									} else {
										is.setAcceptUser("");
									}
								} else {
									is.setAcceptUser("");
								}
								is.setStartDate("");
								is.setEndDate(df111.format(new Date()));
								is.setInstrumentState("解除占用");
								is.setInstrument(it);
								commonDAO.saveOrUpdate(is);
							} else {
								if ("281616366a9b6ff9016a9bf060f70019".equals(it.getMainType().getId())) {
									InstrumentState is = new InstrumentState();
									is.setId(null);
									is.setInstrumentCode(it.getId());
									is.setStageName("试剂制备");
									is.setTableTypeId(mainId);
									is.setNote(stepNum);
									is.setNote2(cpr.getTempleItem().getName());
									// 查询第一步实验操作人 按实验id和步骤查询

									List<ReagentTaskRecord> cp1 = reagentTaskDao
											.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
									if (cp1.size() > 0) {
										ReagentTaskRecord cprr = cp1.get(0);
										if (cprr.getTempleOperator() != null) {
											is.setAcceptUser(cprr.getTempleOperator().getName());
										} else {
											is.setAcceptUser("");
										}
									} else {
										is.setAcceptUser("");
									}
									is.setStartDate("");
									is.setEndDate(df111.format(new Date()));
									is.setInstrumentState("结束使用");
									is.setInstrument(it);
									commonDAO.saveOrUpdate(is);
								}
							}

							it.setIsFull("0");
							commonDAO.saveOrUpdate(it);
						}
					}
				}
			}
		}
		if (ct != null) {
			if (ct.getEndTime() != null && !"".equals(ct.getEndTime())) {

			} else {
				Date t = new Date();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				ct.setEndTime(df.format(t));
				// ct.setStartTime(startTime);
				commonDAO.saveOrUpdate(ct);
				cpr.setEndTime(df.format(t));
				commonDAO.saveOrUpdate(cpr);
			}

		}
		// 改变状态
		if (cprs.size()>0) {
			
		}else {
			try {
				changeState(stepNum, mainId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public List<ReagentTaskRecord> getCellProductionRecordByIdAndStepNum(String mainId, String stepNum) {
		return reagentTaskDao.getCellProductionRecordByIdAndStepNum(mainId, stepNum);

	}

	public Map<String, Object> serchInfos(String mainId, String orderNums1) {

		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> map = new HashMap<String, Object>();
		List<ReagentTaskRecord> cprs = reagentTaskDao.getCellProductionRecordByIdAndStepNumForzy(mainId, orderNums1);
		if (cprs.size() > 0) {
			ReagentTaskRecord cpr = cprs.get(0);
			// 还要做判断，这个步骤的上一步是否做完，上一步做完才能保存 第一步只考虑自己
			List<ReagentTaskRecord> cprrs = reagentTaskDao.getCellProductionRecordByIdAndStepNumUnder(mainId,
					orderNums1);
			if (cpr.getEndTime() != null && !"".equals(cpr.getEndTime())) {
				map.put("baocun", false);
			} else {
				if ("1".equals(orderNums1)) {
					map.put("baocun", true);
				} else {
					if (cprrs.size() > 0) {
						ReagentTaskRecord cprr = cprrs.get(cprrs.size() - 1);
						if (cprr.getEndTime() != null && !"".equals(cprr.getEndTime())) {
							map.put("baocun", true);
						} else {
							map.put("baocun", false);
						}
					} else {
						map.put("baocun", true);
					}
				}
			}
//			if(cpr.getTempleOperator()!=null){
//				if((u.getId()).equals(cpr.getTempleOperator().getId())){
//					map.put("caozuo", true);
//				}else{
//					map.put("caozuo", false);
//				}
//			}else{
//				map.put("caozuo", false);
//			}
		} else {
			map.put("baocun", false);
			map.put("caozuo", false);
		}

//		List<UserGroupUser> ugus = reagentTaskDao.getUserGroupUsers(u.getId());
//		if(ugus.size()>0){
//			map.put("adminUser", true);
//		}else{
//			map.put("adminUser", false);
//		}
		return map;

	}

	public boolean findProductTime(String mainId, String stepNum) {
		boolean state = false;
		ReagentTaskTemplate cellPassageTemplates = new ReagentTaskTemplate();

		cellPassageTemplates = reagentTaskDao.findProductTime(mainId, stepNum);

		String productStartTime = cellPassageTemplates.getProductStartTime();
		String productEndTime = cellPassageTemplates.getProductEndTime();

		if ((!"".equals(productStartTime) && productStartTime != null)
				&& (!"".equals(productEndTime) && productEndTime != null)) {
			state = true;
		}

		return state;

	}

	public Map<String, Object> findCellPassageRecords(String mainId, String stepNum) {
		Map<String, Object> map = new HashMap<String, Object>();

		// 是否要入库
		boolean state = false;
		// 是否已入库
		boolean samplestate = false;
		List<ReagentTaskRecord> cellProductionRecords = reagentTaskDao
				.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);

		if (cellProductionRecords.size() > 0) {
			ReagentTaskRecord cpr = cellProductionRecords.get(0);
			if (cpr.getTempleItem() != null) {
				if (cpr.getTempleItem().getIncubator() != null && "1".equals(cpr.getTempleItem().getIncubator())) {
					List<ReagentTaskItem> cpis = reagentTaskDao.findCellPassageItemListByStepXia(stepNum, mainId);
					if (cpis.size() > 0) {
						ReagentTaskItem cpi = cpis.get(0);
						if (cpi.getCounts() != null && !"".equals(cpi.getCounts()) && cpi.getPosId() != null
								&& !"".equals(cpi.getCounts())) {
							samplestate = true;
							state = true;
						} else {
							state = true;
						}
					} else {
						state = true;
					}
				} else {
					state = false;
				}
			} else {
				state = false;
			}
		} else {
			state = false;
		}

		map.put("state", state);
		map.put("samplestate", samplestate);

		return map;

	}

	// 试剂制备提交质检明细
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitToQualityTestType(String id, String stepNum, String stepName, String mark, String[] ids,
			String[] chosedIDs, String type, String data) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		ReagentTask rt = commonDAO.get(ReagentTask.class, id);
		ReagentTaskRecord rtr = reagentTaskDao.findStepNumOrValue(id, stepNum);

		Date d = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (ids == null || ids.length < 1) {

		} else {

			for (String itemId : ids) {
				ReagentTaskInfo rti = commonDAO.get(ReagentTaskInfo.class, itemId);

				JSONArray array = JSONArray.fromObject(data);
				if (array.size() > 0) {
					for (int i = 0; i < array.size(); i++) {
						JSONObject job = array.getJSONObject(i); // 遍历 jsonarray 数组，把每一个对象转成 json 对象
						String jcxmc = (String) job.get("jcxmc");
						String jcxid = (String) job.get("jcxid");
						String jcsl = (String) job.get("jcsl");
						String jcxlxmc = (String) job.get("jcxlxmc");
						String jcxlx = (String) job.get("jcxlx");
						String jcbz = (String) job.get("jcbz");
						String jcsldz = (String) job.get("jcxsldw");
						SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, jcxid);
						DicSampleType dd = commonDAO.get(DicSampleType.class, jcxlx);

						String scgxdm = rtr.getTempleItem().getPrintLabelCoding();

						String jcid = sd.getDeteyionName();
						String ybbh = dd.getCode();

						ReagentTaskInfoQuality rtiq = new ReagentTaskInfoQuality();
						/** 提交表编码 */
						rtiq.setTempId(itemId);
						if (!"".equals(rti.getSerial()) && rti.getSerial() != null) {

							rtiq.setSampleNumber(rti.getSerial());
						}

						/** 检测项目 */
						rtiq.setProductId(rti.getProductId());
						/** 检测项目 */
						rtiq.setProductName(rti.getProductName());
						/** 状态 */
						rtiq.setState("1");
						/** 备注 */
						rtiq.setNote(jcbz);
						/** 浓度 */
						rtiq.setConcentration(null);
						/** 样本数量 */
						if (jcsl != null && !"".equals(jcsl)) {
							Double jcsls = Double.valueOf(jcsl);
							rtiq.setSampleNum(jcsls);
						}
						/** 检测项数量单位 */
						rtiq.setSampleNumUnit(jcsldz);
						/** 相关主表 */
						rtiq.setReagentTask(rt);
						/** 体积 */
						rtiq.setVolume(null);
						/** 任务单id */
						rtiq.setOrderId(rt.getId());
						/** 中间产物数量 */
						rtiq.setProductNum(null);
						/** 中间产物类型编号 */
						rtiq.setDicSampleTypeId(null);
						/** 中间产物类型 */
						rtiq.setDicSampleTypeName(null);
						/** 样本类型 */
						rtiq.setSampleType(dd.getName());
						/** 样本主数据 */
						rtiq.setSampleInfo(rti.getSampleInfo());
						/** 实验的步骤号 */
						rtiq.setStepNum(stepNum);
						/** 实验步骤名称 */
						rtiq.setExperimentalStepsName(stepName);
						/** 检测项 */
						rtiq.setSampleDeteyion(sd);
						/** 是否合格 */
						rtiq.setQualified(null);
						/** 质检结果表id */
						rtiq.setQualityInfoId(null);
						/** 质检是否接收 */
						rtiq.setQualityReceive(null);
						/** 质检提交时间 */
						rtiq.setQualitySubmitTime(null);
						/** 质检接收时间 */
						rtiq.setQualityReceiveTime(null);
						/** 质检完成时间 */
						rtiq.setQualityFinishTime(null);
						/** 质检提交人 */
						rtiq.setQualitySubmitUser(null);
						/** 质检接收人 */
						rtiq.setQualityReceiveUser(null);
						/** 质检是否提交 */
						rtiq.setSubmit("0");
						/** 原辅料 */
						rtiq.setStorage(rti.getStorage());

						String ct = "";
						if (type.equals("zj01")) {// 自主过程干细胞检测
							ct = "自主检测";
							rtiq.setCellType("3");

						} else if (type.equals("zj02")) {// 第三方检测
							ct = "第三方检测";
							rtiq.setCellType("7");
						}
						String pj = "质检明细:" + "批号:" + rti.getSerial() + "样本名称:" + dd.getName() + "检验量:" + jcsl + "单位:"
								+ jcsldz + "原辅料:" + rti.getStorage().getName() + "检验方式:" + ct + "检验项:" + sd.getName()
								+ "步骤:" + stepNum + "步骤名称:" + stepName;
						if (pj != null && !"".equals(pj)) {
							LogInfo li = new LogInfo();
							li.setLogDate(new Date());
							li.setUserId(u.getId());
							li.setModifyContent(pj);
							li.setFileId(id);
							li.setState("3");
							li.setStateName("数据修改");
							li.setClassName("ReagentTask");
							commonDAO.saveOrUpdate(li);
						}
						reagentTaskDao.saveOrUpdate(rtiq);
						reagentTaskDao.saveOrUpdate(rti);

					}

				}
			}

		}
	}

	// 质检明细查询
	public Map<String, Object> findQualityItem(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return reagentTaskDao.findQualityItem(id, start, length, query, col, sort);
	}

	/**
	 * 删除待提交质检样本
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityTestSampleType(String[] ids, User user, String qid) throws Exception {
		for (String id : ids) {
			ReagentTaskInfoQuality scp = reagentTaskDao.get(ReagentTaskInfoQuality.class, id);
			if (scp.getId() != null) {
				if (scp.getSubmit() != null && "1".equals(scp.getSubmit())) {

				} else {
					reagentTaskDao.delete(scp);
					String pj = "质检明细:" + "批号:" + scp.getSampleNumber() + "样本名称:" + scp.getSampleType() + "检验量:"
							+ scp.getSampleNum() + "单位:" + scp.getSampleNumUnit() + "原辅料:" + scp.getStorage().getName()
							+ "已被删除";
					if (pj != null && !"".equals(pj)) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						li.setUserId(user.getId());
						li.setModifyContent(pj);
						li.setFileId(qid);
						li.setState("2");
						li.setStateName("数据删除");
						li.setClassName("ReagentTask");
						commonDAO.saveOrUpdate(li);
					}
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitToQualityTestSample(String[] ids, String changeLogItem, String qid) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		Date d = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (ids == null || ids.length < 1) {

		} else {

			for (String itemId : ids) {
				ReagentTaskInfoQuality scp = commonDAO.get(ReagentTaskInfoQuality.class, itemId);
				if (scp != null) {
//					CellPassage cp = commonDAO.get(CellPassage.class,);
					SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyion().getId());

					QualityTestTemp cpt = new QualityTestTemp();
					cpt.setCode(scp.getCode());
					cpt.setSampleNumber(scp.getSampleNumber());
					cpt.setState("1");
//					cpt.setMark("ReagentTask");
					cpt.setZjName(sd.getName());
					cpt.setTestItem(sd.getName());
					cpt.setExperimentalSteps(scp.getStepNum());
					cpt.setExperimentalStepsName(scp.getExperimentalStepsName());
					cpt.setSampleNumUnit(scp.getSampleNumUnit());
					cpt.setStorage(scp.getStorage());
					cpt.setCode(scp.getSampleNumber());

//					cpt.setOrderCode(scp.getOrderCode());
//					if (scp.getOrderCode() != null && !"".equals(scp.getOrderCode())) {
//						SampleOrder so = commonDAO.get(SampleOrder.class, scp.getOrderCode());
//						cpt.setSampleOrder(so);
//					}

					cpt.setParentId(scp.getCode());
					cpt.setSampleCode(scp.getCode());
					cpt.setProductId(scp.getProductId());
					cpt.setProductName(scp.getProductName());

					cpt.setSampleType(scp.getSampleType());
					cpt.setNote(scp.getNote());
					if (scp.getSampleNum() != null && !"".equals(scp.getSampleNum())) {
						cpt.setSampleNum(String.valueOf(scp.getSampleNum()));
					}

					cpt.setSampleInfo(scp.getSampleInfo());
//					cpt.setScopeId(scp.getScopeId());
//					cpt.setScopeName(scp.getScopeName());

					cpt.setSampleDeteyion(sd);
					cpt.setCellType(scp.getCellType());

//					cpt.setBatch(scp.getBatch());// 批次
					cpt.setCellSampleTaleId(scp.getId());

					cpt.setQualitySubmitTime(d);
					commonDAO.saveOrUpdate(cpt);

					/** 是否合格 */
					scp.setQualified(null);
					/** 质检结果表id */
					scp.setQualityInfoId(null);
					/** 质检是否接收 */
					scp.setQualityReceive(null);
					/** 质检提交时间 */
					scp.setQualitySubmitTime(d);
					/** 质检接收时间 */
					scp.setQualityReceiveTime(null);
					/** 质检完成时间 */
					scp.setQualityFinishTime(null);
					/** 质检提交人 */
					scp.setQualitySubmitUser(u.getName());
					scp.setTempleOperator(u);
					/** 质检接收人 */
					scp.setQualityReceiveUser(null);
					/** 质检是否提交 */
					scp.setSubmit("1");
					commonDAO.saveOrUpdate(scp);
					// 提醒事项
					List<UserGroupUser> QAugus = commonService.get(UserGroupUser.class, "userGroup.id", "QC001");
					for (UserGroupUser QAugu : QAugus) {
						SysRemind srz = new SysRemind();
						srz.setId(null);
						// 提醒类型
						srz.setType(null);
						// 事件标题
						srz.setTitle("质检提交提醒");
						// 发起人（系统为SYSTEM）
						srz.setRemindUser(u.getName());
						// 发起时间
						srz.setStartDate(new Date());
						// 查阅提醒时间
						srz.setReadDate(new Date());
						// 提醒内容
						srz.setContent("试剂" + scp.getSampleNumber() + "的" + scp.getStepNum() + "质检" + sd.getName()
								+ "已提交,请确认!");
						// 状态 0.草稿 1.已阅
						srz.setState("0");
						// 提醒的用户
						srz.setHandleUser(QAugu.getUser());
						if ("1".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp1");
						} else if ("2".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp2");
						} else if ("3".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp3");
						} else if ("4".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp4");
						} else if ("5".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp5");
						} else if ("6".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp6");
						} else if ("7".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp7");
						} else if ("8".equals(scp.getCellType())) {
							// 表单id
							srz.setContentId("");
							srz.setTableId("QualityTestTemp8");
						}
						commonDAO.saveOrUpdate(srz);
					}
					String tjzj = "试剂制备提交质检：" + "步骤" + scp.getStepNum() + "：样本为" + scp.getSampleType() + " 检测项为"
							+ sd.getName() + "的数据已提交";
					if (tjzj != null && !"".equals(tjzj)) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						li.setModifyContent(tjzj);
						li.setFileId(qid);
						li.setState("3");
						li.setStateName("数据修改");
						li.setUserId(u.getId());
						li.setClassName("ReagentTask");
						commonDAO.saveOrUpdate(li);
					}
				}

			}

		}

	}

}
