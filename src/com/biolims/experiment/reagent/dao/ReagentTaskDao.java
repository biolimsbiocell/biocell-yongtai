package com.biolims.experiment.reagent.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.reagent.model.ReagentTask;
import com.biolims.experiment.reagent.model.ReagentTaskCompleted;
import com.biolims.experiment.reagent.model.ReagentTaskCos;
import com.biolims.experiment.reagent.model.ReagentTaskCosNew;
import com.biolims.experiment.reagent.model.ReagentTaskInfo;
import com.biolims.experiment.reagent.model.ReagentTaskInfoQuality;
import com.biolims.experiment.reagent.model.ReagentTaskItem;
import com.biolims.experiment.reagent.model.ReagentTaskOperation;
import com.biolims.experiment.reagent.model.ReagentTaskPreparat;
import com.biolims.experiment.reagent.model.ReagentTaskPreparatEnd;
import com.biolims.experiment.reagent.model.ReagentTaskReagent;
import com.biolims.experiment.reagent.model.ReagentTaskReagentNew;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.experiment.reagent.model.ReagentTaskResults;
import com.biolims.experiment.reagent.model.ReagentTaskTemp;
import com.biolims.experiment.reagent.model.ReagentTaskTemplate;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.template.model.ZhiJianItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class ReagentTaskDao extends BaseHibernateDao {

	public Map<String, Object> findReagentTaskTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTask where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTask where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTask> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectReagentTaskTempTable(String[] codes, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (codes != null && !codes.equals("")) {
			key += " and code in (:alist)";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			List<ReagentTaskTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from ReagentTaskTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			if (codes != null && !codes.equals("")) {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
						.setParameterList("alist", codes).list();
			} else {
				list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			}
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findReagentTaskItemTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskItem where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTaskItem  where 1=1 and reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTaskItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<ReagentTaskItem> showWellPlate(String id) {
		String hql = "from ReagentTaskItem  where 1=1 and  reagentTask.id='" + id + "'";
		List<ReagentTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findReagentTaskItemAfTable(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskItem where 1=1 and state='2' and reagentTask.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTaskItem  where 1=1 and state='2' and reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTaskItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from ReagentTaskItem where 1=1 and state='2' and reagentTask.id='" + id
				+ "'";
		String key = "";
		List<String> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "select counts from ReagentTaskItem where 1=1 and state='2' and reagentTask.id='" + id
					+ "' group by counts";
			list = getSession().createQuery(hql + key).list();
		}
		return list;

	}

	public List<ReagentTaskTemplate> showReagentTaskStepsJson(String id, String code) {

		String countHql = "select count(*) from ReagentTaskTemplate where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and orderNum ='" + code + "'";
		} else {
		}
		List<ReagentTaskTemplate> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ReagentTaskTemplate where 1=1 and reagentTask.id='" + id + "'";
			key += " order by orderNum asc";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<ReagentTaskReagent> showReagentTaskReagentJson(String id, String code) {
		String countHql = "select count(*) from ReagentTaskReagent where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<ReagentTaskReagent> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ReagentTaskReagent where 1=1 and reagentTask.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<ReagentTaskCos> showReagentTaskCosJson(String id, String code) {
		String countHql = "select count(*) from ReagentTaskCos where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (code != null && !"".equals(code)) {
			key += " and itemId='" + code + "'";
		} else {
			key += " and itemId='1'";
		}
		List<ReagentTaskCos> list = null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ReagentTaskCos where 1=1 and reagentTask.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public List<ReagentTaskTemplate> delTemplateItem(String id) {
		List<ReagentTaskTemplate> list = new ArrayList<ReagentTaskTemplate>();
		String hql = "from ReagentTaskTemplate where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<ReagentTaskReagent> delReagentItem(String id) {
		List<ReagentTaskReagent> list = new ArrayList<ReagentTaskReagent>();
		String hql = "from ReagentTaskReagent where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<ReagentTaskCos> delCosItem(String id) {
		List<ReagentTaskCos> list = new ArrayList<ReagentTaskCos>();
		String hql = "from ReagentTaskCos where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> showReagentTaskResultTableJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskInfo where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTaskInfo  where 1=1 and  reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTaskInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Object> showWellList(String id) {

		String hql = "select counts,count(*) from ReagentTaskItem where 1=1 and reagentTask.id='" + id
				+ "' group by counts";
		List<Object> list = getSession().createQuery(hql).list();

		return list;
	}

	public List<ReagentTaskItem> plateSample(String id, String counts) {
		String hql = "from ReagentTaskItem where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (counts == null || counts.equals("")) {
			key = " and counts is null";
		} else {
			key = "and counts='" + counts + "'";
		}
		List<ReagentTaskItem> list = getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskItem where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if (counts == null || counts.equals("")) {
			key += " and counts is null";
		} else {
			key += "and counts='" + counts + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTaskItem  where 1=1 and reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTaskItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<ReagentTaskInfo> findReagentTaskInfoByCode(String code) {
		String hql = "from ReagentTaskInfo where 1=1 and code='" + code + "'";
		List<ReagentTaskInfo> list = new ArrayList<ReagentTaskInfo>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskInfo> selectAllResultListById(String code) {
		String hql = "from ReagentTaskInfo  where (submit is null or submit='') and reagentTask.id='" + code + "'";
		List<ReagentTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskInfo> selectResultListById(String id) {
		String hql = "from ReagentTaskInfo  where reagentTask.id='" + id + "'";
		List<ReagentTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from ReagentTaskInfo t where (submit is null or submit='') and id in (" + insql + ")";
		List<ReagentTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskItem> selectReagentTaskItemList(String scId) throws Exception {
		String hql = "from ReagentTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and reagentTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ReagentTaskItem> list = new ArrayList<ReagentTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public Integer generateBlendCode(String id) {
		String hql = "select max(blendCode) from ReagentTaskItem where 1=1 and reagentTask.id='" + id + "'";
		Integer blendCode = (Integer) this.getSession().createQuery(hql).uniqueResult();
		return blendCode;
	}

	public ReagentTaskInfo getResultByCode(String code) {
		String hql = " from ReagentTaskInfo where 1=1 and code='" + code + "'";
		ReagentTaskInfo spi = (ReagentTaskInfo) this.getSession().createQuery(hql).uniqueResult();
		return spi;
	}

	public List<CellPassage> findCellPassageListByUser(String name) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1 and state <> '1' and workUser like '%" + name + "%'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageTemplate> findNextDataByOrderNum(String id, int orderNum) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = " from CellPassageTemplate where 1=1 and orderNum > '" + orderNum + "' and reagentTask.id = '" + id
				+ "'";
		String key = " order by orderNum";
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号查询操作指令
	public List<ReagentTaskPreparat> findCellNstructionsInfo(String id, String number) {
		List<ReagentTaskPreparat> list = new ArrayList<ReagentTaskPreparat>();
		String key = "and orderNum='" + number + "'";
		String hql = "from ReagentTaskPreparat where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 生产检查
	public List<ReagentTaskRecord> findProductionRecordInfo(String id, String number) {
		List<ReagentTaskRecord> list = new ArrayList<ReagentTaskRecord>();
		String key = "and orderNum='" + number + "'";
		String hql = "from ReagentTaskRecord where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第四步的 完工 生产检查
	public List<ReagentTaskCompleted> findCellProductionCompletedInfo(String id, String number) {
		List<ReagentTaskCompleted> list = new ArrayList<ReagentTaskCompleted>();
		String key = "and orderNum='" + number + "'";
		String hql = "from ReagentTaskCompleted where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第四步的 完工 指令
	public List<ReagentTaskPreparatEnd> findCellPassagePreparatEndInfo(String id, String number) {
		List<ReagentTaskPreparatEnd> list = new ArrayList<ReagentTaskPreparatEnd>();
		String key = "and orderNum='" + number + "'";
		String hql = "from ReagentTaskPreparatEnd where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 获取传代干细胞
	public Map<String, Object> selectReagentTaskItemListPageTable(String id, String orderNum, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskItem where 1=1 and state='2' and reagentTask.id='" + id
				+ "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTaskItem  where 1=1 and state='2' and reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTaskItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 通过实验主表id和步骤号 第三步生产操作
	public List<ReagentTaskOperation> findProductOperationListPage(String id, String number, String templeId) {
		List<ReagentTaskOperation> list = new ArrayList<ReagentTaskOperation>();
		String key = "and orderNum='" + number + "' and templeCell.id='" + templeId + "'";
		String hql = "from ReagentTaskOperation where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第三步生产操作 试剂明细
	public List<ReagentTaskReagentNew> findProductRegentListPage(String id, String number, String templeId) {
		List<ReagentTaskReagentNew> list = new ArrayList<ReagentTaskReagentNew>();
		String key = " and templateReagent.id='" + templeId + "'";
		String hql = "from ReagentTaskReagentNew where 1=1 and state !='1' and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第三步生产操作 设备明细
	public List<ReagentTaskCosNew> findCellproductCosListPage(String id, String number, String templeId) {
		List<ReagentTaskCosNew> list = new ArrayList<ReagentTaskCosNew>();
		String key = " and templateCos.id='" + templeId + "'";
		String hql = "from ReagentTaskCosNew where 1=1 and state !='1' and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id和步骤号 第三步生产操作 实验结果
	public List<ReagentTaskResults> findCellProducResultsListPage(String id, String number, String templeId) {
		List<ReagentTaskResults> list = new ArrayList<ReagentTaskResults>();
		String key = "and orderNum='" + number + "' and templeCell.id='" + templeId + "'";
		String hql = "from ReagentTaskResults where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作
	public List<ReagentTaskOperation> findByIdProductOperationList(String id) {
		List<ReagentTaskOperation> list = new ArrayList<ReagentTaskOperation>();
		String hql = "from ReagentTaskOperation where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 试剂
	public List<ReagentTaskReagentNew> findByIdProductRegentList(String id) {
		List<ReagentTaskReagentNew> list = new ArrayList<ReagentTaskReagentNew>();
		String hql = "from ReagentTaskReagentNew where 1=1 and state !='1' and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 设备
	public List<ReagentTaskCosNew> findByIdCellproductCosList(String id) {
		List<ReagentTaskCosNew> list = new ArrayList<ReagentTaskCosNew>();
		String hql = "from ReagentTaskCosNew where 1=1 and state !='1' and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作
	public List<ReagentTaskOperation> findByIdAndOrderNumProductOperation(String id, String orderNum) {
		List<ReagentTaskOperation> list = new ArrayList<ReagentTaskOperation>();
		String key = "and orderNum='" + orderNum + "'";
		String hql = "from ReagentTaskOperation where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id 第三步生产操作 设备
	public List<ReagentTaskCosNew> findByIdAndOrderNumCellproductCos(String id, String orderNum) {
		List<ReagentTaskCosNew> list = new ArrayList<ReagentTaskCosNew>();
		String key = "and orderNum='" + orderNum + "'";
		String hql = "from ReagentTaskCosNew where 1=1 and state !='1' and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过实验主表id 步骤号 第三步生产操作 生产结果数据
	public List<ReagentTaskResults> findCellProducResultsPage(String id, String orderNum) {
		List<ReagentTaskResults> list = new ArrayList<ReagentTaskResults>();
		String key = "and orderNum='" + orderNum + "'";
		String hql = "from ReagentTaskResults where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	/**
	 * @throws Exception
	 * @Title: findCellPrimaryCultureTemplateList @Description: TODO @author :
	 *         nan.jiang @date 2018-8-30上午9:40:58 @param start @param length @param
	 *         query @param col @param sort @param ids @return Map
	 *         <String,Object> @throws
	 */
	public Map<String, Object> findCellPrimaryCultureTempRecordList(Integer start, Integer length, String query,
			String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskRecord where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from ReagentTaskRecord where 1=1 and reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<ReagentTaskRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 第一步通过开始指令模板id查询模板指令数据
	public List<ReagentTaskPreparat> findBytemIdReagenTaskPraraList(String id, String templeId) {
		List<ReagentTaskPreparat> list = new ArrayList<ReagentTaskPreparat>();
		String key = " and templeInsId.id='" + templeId + "'";
		String hql = "from ReagentTaskPreparat where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 第四步 通过结束指令模板id查询模板指令数据
	public List<ReagentTaskPreparatEnd> findBytemIdReagenTaskPraraEndList(String id, String templeId) {
		List<ReagentTaskPreparatEnd> list = new ArrayList<ReagentTaskPreparatEnd>();
		String key = " and templeInsId.id='" + templeId + "'";
		String hql = "from ReagentTaskPreparatEnd where 1=1 and reagentTask.id='" + id + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 通过试剂制备主表id查询 试剂完成结果
	public List<ReagentTaskInfo> findByReagentTaskIdPage(String id) {
		List<ReagentTaskInfo> list = new ArrayList<ReagentTaskInfo>();
		String hql = "from ReagentTaskInfo where 1=1 and (submit !='1' or submit is null) and reagentTask.id='" + id
				+ "'";
		list = getSession().createQuery(hql).list();
		return list;
	}
	
	public Integer findByReagentTaskIdPage2(String id) {
		String countHql = "select count(*) from ReagentTaskInfo where 1=1 and (submit !='1' or submit is null) and reagentTask.id='" + id
				+ "'";
		Long sum = (Long) getSession().createQuery(countHql).uniqueResult();
		Integer result = sum.intValue();
		return result;
	}

	public List<ZhiJianItem> getZjItem(String id) {
		List<ZhiJianItem> list = new ArrayList<ZhiJianItem>();
		String countHql = "select count(*) from ZhiJianItem where 1=1 and template.id='" + id + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			String hql = "from ZhiJianItem where 1=1 and template.id='" + id + "'";
			list = getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public ReagentTaskTemplate getReagentTaskResultsByCellAndStep(String stepNum, String mainId) {
		String hql = "from ReagentTaskTemplate where 1=1 ";
		String key = " and reagentTask.id = '" + mainId + "' and orderNum = '" + stepNum + "'";
		ReagentTaskTemplate rt = (ReagentTaskTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return rt;
	}

	public List<ReagentTaskOperation> getReagentTaskOperationListPage(String id) {
		String hql = "from ReagentTaskOperation where templeCell.id = '" + id + "'";
		List<ReagentTaskOperation> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public ReagentTaskTemplate getReagentTaskTemplateByCellAndStep(String stepNum, String mainId) {
		String hql = "from ReagentTaskTemplate where 1=1 ";
		String key = " and reagentTask.id = '" + mainId + "' and orderNum = '" + stepNum + "'";
		ReagentTaskTemplate rt = (ReagentTaskTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return rt;
	}

	public List<ReagentTaskCosNew> findCellproductCoss(String id, String stepNum) {
		String hql = "from ReagentTaskCosNew where cellProduc.reagentTask.id = '" + id + "' and cellProduc.orderNum='"
				+ stepNum + "' ";
		List<ReagentTaskCosNew> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskRecord> getCellProductionRecordByIdAndStepNumForzy(String mainId, String stepNum) {
		List<ReagentTaskRecord> list = new ArrayList<ReagentTaskRecord>();
		String hql = "from ReagentTaskRecord where 1=1 and reagentTask.id='" + mainId + "' and orderNum='" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskRecord> getReagentTaskRecords(String mainId, String stepNum) {
		List<ReagentTaskRecord> list = new ArrayList<ReagentTaskRecord>();
		String hql = "from ReagentTaskRecord where 1=1 and reagentTask.id='" + mainId + "' and orderNum>'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public ReagentTaskTemplate findCellPassageTemplateByStep(String step, String mainId) {
		String hql = "from ReagentTaskTemplate where 1=1 ";
		String key = " and reagentTask.id = '" + mainId + "' and orderNum = '" + step + "'";
		ReagentTaskTemplate ct = (ReagentTaskTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public ReagentTaskRecord findCellProductionRecordByStep(String step, String mainId) {
		String hql = "from ReagentTaskRecord where 1=1 ";
		String key = " and reagentTask.id = '" + mainId + "' and orderNum = '" + step + "'";
		ReagentTaskRecord ct = (ReagentTaskRecord) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	public List<ReagentTaskOperation> findReagentTaskOperationByStep(String step, String mainId) {
		List<ReagentTaskOperation> list = new ArrayList<ReagentTaskOperation>();
		String hql = "from ReagentTaskOperation where 1=1 ";
		String key = " and reagentTask.id = '" + mainId + "' and orderNum = '" + step + "'";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<ReagentTaskCosNew> getReagentTaskCosNew(String id) {
		List<ReagentTaskCosNew> list = new ArrayList<ReagentTaskCosNew>();
		String hql = "from ReagentTaskCosNew where 1=1 and cellProduc.id='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskRecord> getCellProductionRecordByIdAndStepNum(String mainId, String stepNum) {
		List<ReagentTaskRecord> list = new ArrayList<ReagentTaskRecord>();
		String hql = "from ReagentTaskRecord where 1=1 and reagentTask.id='" + mainId + "' and orderNum>'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskRecord> getCellProductionRecordByIdAndStepNumUnder(String mainId, String stepNum) {
		List<ReagentTaskRecord> list = new ArrayList<ReagentTaskRecord>();
		String hql = "from ReagentTaskRecord where 1=1 and reagentTask.id='" + mainId + "' and orderNum<'" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<UserGroupUser> getUserGroupUsers(String id) {
		List<UserGroupUser> list = new ArrayList<UserGroupUser>();
		String hql = "from UserGroupUser where 1=1 and user.id='" + id
				+ "' and (userGroup.id='su088' or userGroup.id='admin' ) ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskItem> findCellPassageItemListByStepXia(String stepNum, String mainId) {
		String hql = "from ReagentTaskItem where reagentTask.id = '" + mainId + "' and stepNum = '" + stepNum
				+ "' and blendState='1' ";
		List<ReagentTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public ReagentTaskTemplate findProductTime(String mainId, String stepNum) {
		String hql = "from ReagentTaskTemplate where 1=1 ";
		String key = " and reagentTask.id = '" + mainId + "' and orderNum = '" + stepNum + "'";
		ReagentTaskTemplate ct = (ReagentTaskTemplate) this.getSession().createQuery(hql + key).uniqueResult();
		return ct;
	}

	// 试剂制备质检
	public ReagentTaskRecord findStepNumOrValue(String id, String stepNum) {

		String hql = "from ReagentTaskRecord where 1=1 and reagentTask.id='" + id + "'and orderNum='" + stepNum + "' ";

		ReagentTaskRecord cell = (ReagentTaskRecord) this.getSession().createQuery(hql).uniqueResult();

		return cell;
	}
	// 质检明细查询

	public Map<String, Object> findQualityItem(String id, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReagentTaskInfoQuality where 1=1 and reagentTask.id='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReagentTaskInfoQuality  where 1=1 and reagentTask.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReagentTaskInfoQuality> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<ReagentTaskOperation> getReagentTaskOperationList(String id, String stepNum) {
		List<ReagentTaskOperation> list = new ArrayList<ReagentTaskOperation>();
		String hql = "from ReagentTaskOperation where 1=1 and reagentTask.id='" + id + "' and orderNum='" + stepNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskCosNew> getReagentTaskCosNewList(String id) {
		List<ReagentTaskCosNew> list = new ArrayList<ReagentTaskCosNew>();
		String hql = "from ReagentTaskCosNew where 1=1 and cellProduc.id='" + id
				+ "' and instrumentId='281616366a9b6ff9016a9bf060f70019' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<IncubatorSampleInfo> showWellPlate1(String id) {
		String hql = "from IncubatorSampleInfo  where 1=1 and state='1'  and  incubatorId='" + id + "'  ";
		List<IncubatorSampleInfo> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<IncubatorSampleInfo> getIncubatorSampleInfoBySid(String sid) {
		List<IncubatorSampleInfo> list = new ArrayList<IncubatorSampleInfo>();
		String hql = "from IncubatorSampleInfo where 1=1 and sId='" + sid + "' and state='1' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<ReagentTaskInfo> findC(String serial) {
		List<ReagentTaskInfo> list = new ArrayList<ReagentTaskInfo>();
		String hql = "from ReagentTaskInfo where 1=1 and serial='" + serial + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<StorageReagentBuySerial> findCk(String serial) {
		List<StorageReagentBuySerial> stor = new ArrayList<StorageReagentBuySerial>();
		String hql = "from StorageReagentBuySerial where 1=1 and serial='" + serial + "'";
		stor = getSession().createQuery(hql).list();
		return stor;
	}

	public String selectMaxSlideCode(String id) {
		String hql = "select MAX(serial) from ReagentTaskInfo where 1=1 and serial like '" + id + "%'";
		String result = (String) getSession().createQuery(hql).uniqueResult();
		return result;
	}

	public List<ReagentTaskInfo> findByReagentTaskIdPage1(String id) {
		List<ReagentTaskInfo> list = new ArrayList<ReagentTaskInfo>();
		String hql = "from ReagentTaskInfo where 1=1 and (submit !='1' or submit is null) and serial like '" + id + "%'";
		list = getSession().createQuery(hql).list();
		return list;
	}
}