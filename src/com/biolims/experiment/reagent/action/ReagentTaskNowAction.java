package com.biolims.experiment.reagent.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.reagent.model.ReagentTask;
import com.biolims.experiment.reagent.model.ReagentTaskCompleted;
import com.biolims.experiment.reagent.model.ReagentTaskCosNew;
import com.biolims.experiment.reagent.model.ReagentTaskInfo;
import com.biolims.experiment.reagent.model.ReagentTaskItem;
import com.biolims.experiment.reagent.model.ReagentTaskOperation;
import com.biolims.experiment.reagent.model.ReagentTaskPreparat;
import com.biolims.experiment.reagent.model.ReagentTaskPreparatEnd;
import com.biolims.experiment.reagent.model.ReagentTaskReagentNew;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.experiment.reagent.model.ReagentTaskResults;
import com.biolims.experiment.reagent.model.ReagentTaskTemplate;
import com.biolims.experiment.reagent.service.ReagentTaskNewService;
import com.biolims.experiment.reagent.service.ReagentTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.model.TempleNstructions;
import com.biolims.system.template.model.TempleProducingCell;
import com.biolims.system.template.model.TempleProducingCos;
import com.biolims.system.template.model.TempleProducingReagent;
import com.biolims.system.template.model.ZhiJianItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Namespace("/experiment/reagent/reagentNow")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ReagentTaskNowAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private ReagentTaskService reagentTaskService;// 以前老的
	@Autowired
	private ReagentTaskNewService reagentTaskNewService;// 新的
	private ReagentTask reagentTask = new ReagentTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 
	 * @Title: showreagentTaskSteps @Description: 转发到新的生产页面 @author : @date
	 *         2019-03-21 @return @throws Exception String @throws
	 */
	@Action(value = "showreagentTaskEdit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showreagentTaskEdit() throws Exception {
		String id = getParameterFromRequest("id");
		reagentTask = reagentTaskService.get(id);
		String orderNumBy = getParameterFromRequest("orderNum");
		putObjToContext("orderNumBy", orderNumBy);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskSteps.jsp");
	}

	/**
	 * 
	 * @Title: 获取实验步骤 @Description: @author @date @return @throws Exception
	 *         String @throws
	 */
	@Action(value = "findCellNumbers")
	public void findCellNumbers() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			ReagentTask reagentTask = reagentTaskService.get(id);
			Template template = templateService.get(reagentTask.getTemplate().getId());
			map.put("template", template);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: saveAllot @Description: 保存任务分配页面 @author : @date @throws
	 */
	@Action(value = "saveAllotNow")
	public void saveAllotNow() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String templateId = getParameterFromRequest("template");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = reagentTaskNewService.saveAllot(main, tempId, userId, templateId, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取左邊步骤号 @author : @date @throws
	 */
	@Action(value = "findTempleItemNumber")
	public void findTempleItemNumber() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			JSONArray jsonArray = new JSONArray();
			ReagentTask reagentTask = reagentTaskNewService.findreagentTask(id);
			if (reagentTask != null) {
				// 通过模板主表id查询 模板明细数据
				List<TemplateItem> temItemList = templateService
						.getTemplateItemListPage(reagentTask.getTemplate().getId());
				if (temItemList.size() > 0) {
					for (int j = 0; j < temItemList.size(); j++) {
						map.put("id", temItemList.get(j).getId());
						map.put("code", temItemList.get(j).getCode());
						map.put("orderNum", temItemList.get(j).getOrderNum());
						map.put("name", temItemList.get(j).getName());
						map.put("today", temItemList.get(j).getEstimatedTime());
						jsonArray.add(map);
					}
				}
			}
			result.put("success", true);
			result.put("data", jsonArray);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取工前操作 指令实验模板数据 @author :
	 * @date @throws
	 */
	@Action(value = "findNumberNstructionsListinfo")
	public void findNumberNstructionsListinfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> scjz = new HashMap<String, Object>();

			Map<String, Object> map = new HashMap<String, Object>();
			Map<String, Object> mapto = new HashMap<String, Object>();
			ReagentTask reagentTask = reagentTaskNewService.findreagentTask(id);
			if (reagentTask != null) {
				// 通过模板主表id查询 模板明细数据
				List<TemplateItem> temItemList = templateService.getTemplateItem(reagentTask.getTemplate().getId(),
						orderNum);
				if (temItemList.size() > 0) {
					for (int j = 0; j < temItemList.size(); j++) {
						map.put("content", temItemList.get(j).getContent().toString().replaceAll("\'", "\""));
						map.put("id", temItemList.get(j).getId());
					}
				}
			}
			// 获取第一步生產数据 指令
			List<ReagentTaskPreparat> cellPass = reagentTaskNewService.findCellNstructionsInfo(id, orderNum);
			// 获取操作指令
			List<TempleNstructions> temNstr = templateService
					.findTemNstructionsListPage(reagentTask.getTemplate().getId(), orderNum);
			// 获取实验数据 生产检查
			List<ReagentTaskRecord> cellReco = reagentTaskNewService.findProductionRecordInfo(id, orderNum);
			result.put("cellPass", cellPass);
			result.put("cellReco", cellReco.get(0));
			if (cellReco.size() > 0) {
				ReagentTaskRecord scjzsj = cellReco.get(0);
				if (scjzsj.getTempleOperator() != null) {
					scjz.put("templeOperator", scjzsj.getTempleOperator().getId());
					scjz.put("templeOperator-name", scjzsj.getTempleOperator().getName());
				} else {
					scjz.put("templeOperator", "");
					scjz.put("templeOperator-name", "");
				}

				if (scjzsj.getTempleReviewer() != null) {
					scjz.put("templeReviewer", scjzsj.getTempleReviewer().getId());
					scjz.put("templeReviewer-name", scjzsj.getTempleReviewer().getName());
				} else {
					scjz.put("templeReviewer", "");
					scjz.put("templeReviewer-name", "");
				}
				if (scjzsj.getOperatingRoomName() != null) {
					scjz.put("operatingRoomName", scjzsj.getOperatingRoomName());
					scjz.put("operatingRoomId", scjzsj.getOperatingRoomId());
				} else {
					scjz.put("operatingRoomName", "");
					scjz.put("operatingRoomId", "");
				}
			}
			result.put("scjz", scjz);
			// result.put("temNstr", temNstr);
			// result.put("content", map);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取质检信息 @author :
	 * @date @throws
	 */
	@Action(value = "findZhijianItemInfo")
	public void findZhijianItemInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ReagentTask reagentTask = reagentTaskNewService.findreagentTask(id);
			if (reagentTask != null) {
				// 通过模板主表id查询 模板明细数据
				List<ZhiJianItem> temItemList = templateService.getZjItem(reagentTask.getTemplate().getId(), orderNum);
				result.put("temItemList", temItemList);
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取BL明细<试剂 设备>操作 生产结果 以及生产开始时间和生产结束时间 @author
	 *         :
	 * @date @throws
	 */
	@Action(value = "findCosAndReagentInfo")
	public void findCosAndReagentInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> mapJson = new HashMap<String, Object>();
		JSONArray jsonArray = new JSONArray();
		JSONArray jsonRes = new JSONArray();
		JSONArray jsonReagen = new JSONArray();
		JSONArray jsonCos = new JSONArray();
		try {
			ReagentTask reagentTask = reagentTaskNewService.findreagentTask(id);
			if (reagentTask != null) {
				// 通过模板主表id查询 模板明细数据 主表
				List<TempleProducingCell> temProduc = templateService
						.getTempleProducingCellListPage(reagentTask.getTemplate().getId(), orderNum);
				if (temProduc.size() > 0) {
					for (int j = 0; j < temProduc.size(); j++) {
						// 获取每一步的明细
						List<ReagentTaskOperation> cpoList = reagentTaskNewService
								.getReagentTaskOperationListPage(temProduc.get(j).getId());
						// 通过第三步生产主表 获取试剂明细
						List<TempleProducingReagent> reagentList = templateService
								.getTempleProducingReagentListPage(temProduc.get(j).getId());
						// 通过第三步生产主表 获取设备明细
						List<TempleProducingCos> cosList = templateService
								.getTempleProducingCosListPage(temProduc.get(j).getId());
						map.put("id", temProduc.get(j).getId());
						map.put("producingName", temProduc.get(j).getProducingName());
						map.put("producingTitle", temProduc.get(j).getProducingTitle());
						map.put("reagentList", reagentList);
						map.put("customTest", temProduc.get(j).getCustomTest());
						map.put("cosList", cosList);
						jsonArray.add(map);

					}
					// 放自定义字段
					result.put("customTest", temProduc.get(0).getCustomTest().toString());
					result.put("temProduc", temProduc);
				}
				// 通过模板主表id查询 模板明细数据 主表
				List<ReagentTaskOperation> temProd = reagentTaskNewService.findByIdAndOrderNumProductOperation(id,
						orderNum);
				if (temProd.size() > 0) {
					for (int i = 0; i < temProd.size(); i++) {
						// 试剂列表
						List<ReagentTaskReagentNew> regList = reagentTaskNewService
								.findByIdProductRegentList(temProd.get(i).getId());
						mapJson.put("id", temProd.get(i).getId());
						mapJson.put("templeId", temProd.get(i).getTempleCell().getId());
						mapJson.put("state", temProd.get(i).getState());
						if (regList.size() > 0) {
							Map<String, Object> mapJont = new HashMap<String, Object>();
							for (int j = 0; j < regList.size(); j++) {
								mapJont.put("id", regList.get(j).getId());
								mapJont.put("batch", regList.get(j).getBatch());
								mapJont.put("cellPassSn", regList.get(j).getCellPassSn());
								mapJont.put("reagenName", regList.get(j).getReagenName());
								mapJont.put("reagenCode", regList.get(j).getReagenCode());
								mapJont.put("reagentDosage", regList.get(j).getReagentDosage());
								mapJont.put("templateReagentId", regList.get(j).getTemplateReagent().getId());
								mapJont.put("cellProduc", regList.get(j).getReagentTask().getId());
								jsonReagen.add(mapJont);
							}
						}
						mapJson.put("reagent", jsonReagen);
						// mapJson.put("reagent", regList);

						// 设备列表
						List<ReagentTaskCosNew> cosList = reagentTaskNewService
								.findByIdCellproductCosList(temProd.get(i).getId());
						if (cosList.size() > 0) {
							Map<String, Object> mapJonts = new HashMap<String, Object>();
							for (int j = 0; j < cosList.size(); j++) {
								mapJonts.put("id", cosList.get(j).getId());
								mapJonts.put("instrumentName", cosList.get(j).getInstrumentName());
								mapJonts.put("instrumentCode", cosList.get(j).getInstrumentCode());
								mapJonts.put("instrumentId", cosList.get(j).getInstrumentId());
								mapJonts.put("templateCosId", cosList.get(j).getTemplateCos().getId());
								mapJonts.put("cellProducId", cosList.get(j).getCellProduc().getId());
								jsonCos.add(mapJonts);
							}
						}
						mapJson.put("cosList", jsonCos);
						jsonRes.add(mapJson);
					}
				}
				// 获取生产操作结果 以及开始时间和结束时间
//				List<ReagentTaskResults> cellResult = reagentTaskNewService
//						.findCellProducResultsPage(reagentTask.getId(), orderNum);
				ReagentTaskTemplate cellResult = reagentTaskNewService.getReagentTaskTemplateByCellAndStep(orderNum,
						reagentTask.getId());
				result.put("cellResult", cellResult);
				result.put("temProd", temProd);
				result.put("jsonRes", jsonRes.toString());
			}
			result.put("success", true);
			result.put("reagenOrCos", jsonArray.toString());
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 保存第一步的工前及检查准备数据 以及生产检查数据 @author :
	 * @date @throws
	 */
	@Action(value = "savepreparationProductionRecord")
	public void savepreparationProductionRecord() throws Exception {
		String saveData = getParameterFromRequest("saveData");
		String changeLog = getParameterFromRequest("changeLog");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// 创建生产检查明细表
			ReagentTaskRecord cellProduc = new ReagentTaskRecord();
			ReagentTask reagentTask = new ReagentTask();
			JSONObject json = JSONObject.fromObject(saveData);
			if (!"".equals(json.getString("id"))) {
				reagentTask = commonService.get(ReagentTask.class, json.getString("id"));
			}
			if (!"".equals(json.getString("content"))) {
				JSONObject jsonCon = JSONObject.fromObject(json.getString("content"));
				if (!"".equals(jsonCon.getString("guohkId"))) {
					List<ReagentTaskRecord> cellProdList = reagentTaskNewService
							.findProductionRecordInfo(json.getString("id"), json.getString("orderNum").toString());
					if (cellProdList.size() > 0) {
						cellProduc = cellProdList.get(0);
					}
				}
				cellProduc.setCreateUser(user.getId());
				cellProduc.setCreateDate(ObjectToMapUtils.getTimeString());
				// 如何获取一个对象中的属性名称
				JSONArray jsonis = JSONArray.fromObject(cellProduc.getTempleItem().getContent());
				if (jsonis.size() > 0) {
					for (int j = 0; j < jsonis.size(); j++) {
						JSONObject jso = JSONObject.fromObject(jsonis.get(j));
						if ("operatingRoomName".equals(jso.getString("fieldName"))) {
							cellProduc.setOperatingRoomName(jsonCon.getString("operatingRoomName"));
						}
						if ("operatingRoomId".equals(jso.getString("fieldName"))) {
							cellProduc.setOperatingRoomId(jsonCon.getString("operatingRoomId"));
						}
						if ("ambientTemperature".equals(jso.getString("fieldName"))) {
							cellProduc.setAmbientTemperature(jsonCon.getString("ambientTemperature"));
						}
						if ("templeOperator".equals(jso.getString("fieldName"))) {
							if (jso.getString("fieldName") != null && !"".equals(jso.getString("fieldName"))) {
								User caozuouser = commonDAO.get(User.class, jsonCon.getString("templeOperator"));
								if (caozuouser != null) {
									cellProduc.setTempleOperator(caozuouser);
								} else {
									cellProduc.setTempleOperator(null);
								}
							}
						}
						if ("templeReviewer".equals(jso.getString("fieldName"))) {
							if (jso.getString("fieldName") != null && !"".equals(jso.getString("fieldName"))) {
								User caozuouser = commonDAO.get(User.class, jsonCon.getString("templeReviewer"));
								if (caozuouser != null) {
									cellProduc.setTempleReviewer(caozuouser);
								} else {
									cellProduc.setTempleReviewer(null);
								}
							}
//							cellProduc.setTempleReviewer(jsonCon.getString("templeReviewer"));
						}
						if ("ambientHumidity".equals(jso.getString("fieldName"))) {
							cellProduc.setAmbientHumidity(jsonCon.getString("ambientHumidity"));
						}
					}
				}
				cellProduc.setOrderNum(json.getString("orderNum"));
				cellProduc.setState("0");
				// 保存自定义字段字符串 包括默认的 以及自己定义的
				cellProduc.setContent(jsonCon.toString());
				commonService.saveOrUpdate(cellProduc);
			}
			// 创建验工前准备及检查明细表
			JSONArray jsonArr = JSONArray.fromObject(json.getString("temNstr"));
			if (jsonArr.size() > 0) {
				String c = "";
				for (int n = 0; n < jsonArr.size(); n++) {
					JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
					ReagentTaskPreparat cellPreparat = new ReagentTaskPreparat();
					// 获取第一步生產数据 指令
					cellPreparat = commonService.get(ReagentTaskPreparat.class, jsonNs.getString("id"));
					if (cellPreparat == null) {
						cellPreparat = new ReagentTaskPreparat();
					}
					cellPreparat.setCreateDate(ObjectToMapUtils.getTimeString());
					cellPreparat.setCreateUser(user.getId());
					cellPreparat.setOperationNotes(jsonNs.getString("operationNotes"));
					cellPreparat.setOrderNum(json.getString("orderNum"));
					cellPreparat.setProductionInspection(jsonNs.getString("productionInspection"));
					cellPreparat.setReagentTask(reagentTask);
					cellPreparat.setState("0");
					commonService.saveOrUpdate(cellPreparat);
					String a = jsonNs.getString("operationNotes");
					String b = jsonNs.getString("productionInspection");
					String d = json.getString("orderNum");
					if (b.equals("0")) {
						b = "由:是 '变为':否";
					} else {
						b = "由:否 '变为':是";
					}
					if (n == 0) {
						c += "步骤" + d + ":" + "生产检查:" + b + ":" + "备注:" + a;
					} else {
						c += d + "生产检查:" + b + ":" + "备注:" + a;
					}
				}
				if (c != null && !"".equals(c)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(reagentTask.getId());
					li.setUserId(user.getId());
					li.setClassName("ReagentTask");
					li.setModifyContent(c);
					li.setState("3");
					li.setStateName("数据修改");
					commonService.saveOrUpdate(li);
				}
				if (changeLog != null && !"".equals(changeLog)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(reagentTask.getId());
					li.setClassName("ReagentTask");
					li.setUserId(user.getId());
					li.setModifyContent(changeLog);
					li.setState("3");
					li.setStateName("数据修改");
					commonService.saveOrUpdate(li);
				}
				// reagentTaskNewService.saveChangeLog(changeLog);
			}
			reagentTaskNewService.saveChangeLog(changeLog);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 获取第二步细胞传代实验明细
	@Action(value = "showreagentTaskItemListPagebleJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showreagentTaskItemListPagebleJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reagentTaskNewService.selectReagentTaskItemListPageTable(id, orderNum, start,
					length, query, col, sort);
			List<ReagentTaskItem> list = (List<ReagentTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			ReagentTaskItem cellPaaItem = new ReagentTaskItem();
			map = (Map<String, String>) ObjectToMapUtils.getMapKey(cellPaaItem);
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Title: saveCosOrReagent @Description: 保存<试剂 设备>操作 @author :
	 * @date @throws
	 */
	@Action(value = "saveCosandReagent")
	public void saveCosandReagent() throws Exception {
		String saveData = getParameterFromRequest("saveData");
		Map<String, Object> result = new HashMap<String, Object>();
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		ReagentTaskOperation cellProdu = new ReagentTaskOperation();
		try {
			JSONObject json = JSONObject.fromObject(saveData);
			ReagentTask reagentTask = reagentTaskNewService.findreagentTask(json.getString("id"));
			if (reagentTask != null) {
				String stepId = json.getString("stepId");
				String productStartTime = json.getString("productStartTime");
				String productEndTime = json.getString("productEndTime");
				String notes = json.getString("notes");

				JSONArray jsonc = JSONArray.fromObject(json.getString("content"));
				if (jsonc.size() > 0) {
					for (int i = 0; i < jsonc.size(); i++) {
						JSONObject jsonOb = JSONObject.fromObject(jsonc.get(i));
						// 保存第三步操作主表 生产主表 步骤 模板主表
						cellProdu = commonService.get(ReagentTaskOperation.class, jsonOb.getString("id"));
						if (cellProdu != null) {
							cellProdu.setCreateDate(ObjectToMapUtils.getTimeString());
							cellProdu.setCreateUser(user.getId());
							cellProdu.setState(jsonOb.getString("state"));
							String startTime = jsonOb.getString("startTime");
							String endTime = jsonOb.getString("endTime");
							cellProdu.setStartTime(startTime);
							cellProdu.setEndTime(endTime);
							cellProdu.setCustomTest(jsonOb.getString("customTest"));
							cellProdu.setCustomTestValue(jsonOb.getString("customTestValue"));
							commonService.saveOrUpdate(cellProdu);
						}
						// 保存试剂
						JSONArray jsonRegent = JSONArray.fromObject(jsonOb.getString("reagent"));
						ReagentTaskReagentNew reagen = new ReagentTaskReagentNew();
						if (jsonRegent.size() > 0) {
							for (int j = 0; j < jsonRegent.size(); j++) {
								JSONObject jsonRes = JSONObject.fromObject(jsonRegent.get(j));
								// 保存第三步操作 试剂 生产主表 步骤 模板主表
								reagen = commonService.get(ReagentTaskReagentNew.class, jsonRes.getString("id"));
								if (reagen != null) {
									reagen.setBatch(jsonRes.getString("batch"));
									reagen.setCellPassSn(jsonRes.getString("sn"));
									reagen.setCreateDate(user.getId());
									reagen.setReagentDosage(jsonRes.getString("inputVal"));
									reagen.setReagenName(jsonRes.getString("name"));
									reagen.setReagenCode(jsonRes.getString("code"));
									commonService.saveOrUpdate(reagen);
								}
							}
						}

						// 保存设备数据
						JSONArray jsonCos = JSONArray.fromObject(jsonOb.getString("cos"));
						ReagentTaskCosNew cos = new ReagentTaskCosNew();
						if (jsonCos.size() > 0) {
							for (int j = 0; j < jsonCos.size(); j++) {
								JSONObject jsoncos = JSONObject.fromObject(jsonCos.get(j));
								// 保存第三步操作 试剂 生产主表 步骤 模板主表
								cos = commonService.get(ReagentTaskCosNew.class, jsoncos.getString("id"));
								if (cos != null) {
									cos.setCreateDate(ObjectToMapUtils.getTimeString());
									cos.setCreateUser(user.getId());
									cos.setInstrumentId(jsoncos.getString("typeId"));
									cos.setInstrumentName(jsoncos.getString("name"));
									cos.setInstrumentCode(jsoncos.getString("code"));
									commonService.saveOrUpdate(cos);
								}
							}
						}
					}
					// 保存实验结果
					JSONObject jsoresu = JSONObject.fromObject(json.getString("result"));
					// 保存第三步操作 试剂 生产主表 步骤
					List<ReagentTaskResults> CosList = reagentTaskNewService
							.findCellProducResultsPage(reagentTask.getId(), json.getString("orderNum"));
					if (CosList.size() > 0) {
						ReagentTaskResults cellresu = commonService.get(ReagentTaskResults.class,
								CosList.get(0).getId());
						if (cellresu == null) {
							cellresu = new ReagentTaskResults();
						}
						cellresu.setCreateUser(user.getId());
						cellresu.setContent(jsoresu.toString());
						cellresu.setReagentTask(reagentTask);
//						cellresu.setEndTime(json.getString("endTime"));
						cellresu.setOrderNum(json.getString("orderNum"));
//						cellresu.setStartTime(json.getString("startTime"));
						commonService.saveOrUpdate(cellresu);
					}
				}
				ReagentTaskTemplate rtt = reagentTaskNewService
						.getReagentTaskResultsByCellAndStep(json.getString("orderNum"), reagentTask.getId());
				if (rtt != null) {
					rtt.setProductStartTime(productStartTime);
					rtt.setProductEndTime(productEndTime);
					rtt.setNotes(notes);
					commonService.saveOrUpdate(rtt);
				}
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 获取第四步 完工操作 指令实验模板数据 @author :
	 * @date @throws
	 */
	@Action(value = "findNstructionsEndListinfo")
	public void findNstructionsEndListinfo() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			ReagentTask reagentTask = reagentTaskNewService.findreagentTask(id);
			if (reagentTask != null) {
				// 通过模板主表id查询 模板明细数据
				List<TemplateItem> temItemList = templateService.getTemplateItem(reagentTask.getTemplate().getId(),
						orderNum);
				if (temItemList.size() > 0) {
					for (int j = 0; j < temItemList.size(); j++) {
						map.put("content", temItemList.get(j).getContent().toString().replaceAll("\'", "\""));
						map.put("id", temItemList.get(j).getId());
					}
				}
			}
			// 获取第一步生產数据 指令
			List<ReagentTaskCompleted> cellPass = reagentTaskNewService.findCellProductionCompletedInfo(id, orderNum);
			// 获取实验数据 指令
			if (cellPass.size() > 0) {
				result.put("cellReco", cellPass.get(0));
			} else {
				result.put("cellReco", null);
			}
			// 获取实验数据 生产检查
			List<ReagentTaskPreparatEnd> cellReco = reagentTaskNewService.findReagentTaskPreparatEndInfo(id, orderNum);
			// 获取第一步生產数据 指令
			result.put("cellPass", cellReco);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveCosOrReagent @Description: 保存第四步的完工清除数据操作 @author :
	 * @date @throws
	 */
	@Action(value = "savefourthStepCellproductResult")
	public void savefourthStepCellproductResult() throws Exception {
		String saveData = getParameterFromRequest("saveData");
		String changeLog = getParameterFromRequest("changeLog");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// 创建 完工清场 生产检查明细表
			ReagentTaskCompleted cellCompled = new ReagentTaskCompleted();
			ReagentTask reagentTask = new ReagentTask();
			JSONObject json = JSONObject.fromObject(saveData);
			if (!"".equals(json.getString("id"))) {
				reagentTask = commonService.get(ReagentTask.class, json.getString("id"));
			}
			if (!"".equals(json.getString("content"))) {
				JSONObject jsonCon = JSONObject.fromObject(json.getString("content"));
				if (!"".equals(jsonCon.getString("guohkId"))) {
					cellCompled = commonService.get(ReagentTaskCompleted.class, jsonCon.getString("guohkId"));
					if (cellCompled == null) {
						cellCompled = new ReagentTaskCompleted();
						cellCompled.setReagentTask(reagentTask);
						// cellCompled.setTempleItem();
					}
				}
				cellCompled.setCreateUser(user.getId());
				cellCompled.setCreateDate(ObjectToMapUtils.getTimeString());
				cellCompled.setOrderNum(json.getString("orderNum"));

				// 如何获取一个对象中的属性名称
				JSONArray jsonis = JSONArray.fromObject(cellCompled.getTempleItem().getContent());
				if (jsonis.size() > 0) {
					for (int j = 0; j < jsonis.size(); j++) {
						JSONObject jso = JSONObject.fromObject(jsonis.get(j));
						if ("templeOperator".equals(jso.getString("fieldName"))) {
							cellCompled.setTempleOperator(jsonCon.getString("templeOperator"));
						}
						if ("templeReviewer".equals(jso.getString("fieldName"))) {
							cellCompled.setTempleReviewer(jsonCon.getString("templeReviewer"));
						}
					}
				}
				// 放自定义结果
				cellCompled.setContent(jsonCon.toString());
				commonService.saveOrUpdate(cellCompled);
			}
			// 创建验工前准备及检查明细表
			JSONArray jsonArr = JSONArray.fromObject(json.getString("temNstr"));
			if (jsonArr.size() > 0) {
				String c = "";
				for (int n = 0; n < jsonArr.size(); n++) {
					JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
					ReagentTaskPreparatEnd cellPreparat = new ReagentTaskPreparatEnd();
					// 获取第一步生產数据 指令
					cellPreparat = commonService.get(ReagentTaskPreparatEnd.class, jsonNs.getString("id"));
					if (cellPreparat == null) {
						cellPreparat = new ReagentTaskPreparatEnd();
						cellPreparat.setReagentTask(reagentTask);
						// templateService.getTempleNstructionsEndListPage(templeId)
						// cellPreparat.setTempleInsId(templeInsId);
					}
					cellPreparat.setCreateDate(ObjectToMapUtils.getTimeString());
					cellPreparat.setCreateUser(user.getId());
					cellPreparat.setOrderNum(json.getString("orderNum"));
					cellPreparat.setProductionInspection(jsonNs.getString("productionInspection"));
					String b = jsonNs.getString("productionInspection");
					String d = json.getString("orderNum");
					if (b.equals("0")) {
						b = "由:是 '变为':否";
					} else {
						b = "由:否 '变为':是";
					}
					if (n == 0) {
						c += "步骤" + d + ":" + "生产检查:" + b;
					} else {
						c += d + "生产检查:" + b;
					}

					commonService.saveOrUpdate(cellPreparat);
				}
				if (c != null && !"".equals(c)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(reagentTask.getId());
					li.setUserId(user.getId());
					li.setClassName("ReagentTask");
					li.setModifyContent(c);
					li.setState("3");
					li.setStateName("数据修改");
					commonService.saveOrUpdate(li);
				}
				if (changeLog != null && !"".equals(changeLog)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					li.setFileId(reagentTask.getId());
					li.setUserId(user.getId());
					li.setClassName("ReagentTask");
					li.setModifyContent(changeLog);
					li.setState("3");
					li.setStateName("数据修改");
					commonService.saveOrUpdate(li);
				}
			}
			// 获取第一步主表模板明细 更新结束时间
			// templateService.findt
			ReagentTaskRecord cellPros = new ReagentTaskRecord();
			List<ReagentTaskRecord> recList = reagentTaskNewService.findProductionRecordInfo(reagentTask.getId(),
					json.getString("orderNum"));
			if (recList.size() > 0) {
				for (int n = 0; n < recList.size(); n++) {
					cellPros = commonService.get(ReagentTaskRecord.class, recList.get(n).getId());
					if (cellPros != null) {
//						cellPros.setEndTime(ObjectToMapUtils.getTimeString());
						commonService.saveOrUpdate(cellPros);
					}
				}
			}

			// 插入试剂结果数据
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * @Title: saveAllot @Description: 保存第二步质检结果数据 @author :
	 * @date @throws
	 */
	@Action(value = "saveQualityInspectionResults")
	public void saveQualityInspectionResults() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		String saveData = getParameterFromRequest("saveData");
		JSONObject json = JSONObject.fromObject(saveData);
		String changeLog = getParameterFromRequest("changeLogItem");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		// 混合后样本
		if (!"".equals(json.getString("noBlendTabledata"))) {
			// 创建验工前准备及检查明细表
			JSONArray jsonArr = JSONArray.fromObject(json.getString("noBlendTabledata"));
			if (jsonArr.size() > 0) {
				for (int n = 0; n < jsonArr.size(); n++) {
					JSONObject jsonNs = JSONObject.fromObject(jsonArr.get(n));
					ReagentTaskInfo reagentTaskInfo = new ReagentTaskInfo();

					// 获取第一步生產数据 指令
					reagentTaskInfo = commonService.get(ReagentTaskInfo.class, jsonNs.getString("id"));
					if (reagentTaskInfo == null) {
						reagentTaskInfo.setId(null);
					}
					reagentTaskInfo.setQualified(jsonNs.getString("qualified"));
					// 批号
					reagentTaskInfo.setSerial(jsonNs.getString("serial"));
//					reagentTaskInfo.setNum(jsonNs.getString("num"));
//					reagentTaskInfo.setUnit(jsonNs.getString("unit"));
					reagentTaskInfo.setNote(jsonNs.getString("note"));
					
					reagentTaskInfo.setSxDate(jsonNs.getString("sxDate"));
//		reagentTaskOperation.setSampleType(jsonNs.getString("sampleType"));
//					if (jsonNs.getString("counts") != null && !"".equals(jsonNs.getString("counts"))) {
//						Instrument i = commonDAO.get(Instrument.class, jsonNs.getString("counts"));
//						if (i != null) {
//							reagentTaskInfo.setInstrument(i);
//
//							List<ReagentTaskOperation> cos = reagentTaskNewService
//									.getReagentTaskOperationList(reagentTaskInfo.getReagentTask().getId(), orderNum);
//							if (cos.size() > 0) {
//								for (ReagentTaskOperation co : cos) {
//									List<ReagentTaskCosNew> ccs = reagentTaskNewService
//											.getReagentTaskCosNewList(co.getId());
//									if (ccs.size() > 0) {
//										for (ReagentTaskCosNew cc : ccs) {
//											cc.setInstrumentCode(i.getId());
//											commonDAO.saveOrUpdate(cc);
//										}
//									}
//								}
//							}
//						}
//					}
					commonService.saveOrUpdate(reagentTaskInfo);

					if (changeLog != null && !"".equals(changeLog)) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						li.setModifyContent(changeLog);
						li.setUserId(user.getId());
						li.setFileId(reagentTaskInfo.getReagentTask().getId());
						li.setClassName("ReagentTask");
						li.setState("3");
						li.setStateName("数据修改");
						commonService.saveOrUpdate(li);
					}
				}

			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ReagentTask reagentTask = new ReagentTask();
			reagentTask = commonService.get(ReagentTask.class, id);
			// 保存生产的总 结果
			ReagentTaskInfo passinfo = new ReagentTaskInfo();
			passinfo.setReagentTask(reagentTask);
			commonService.saveOrUpdate(passinfo);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 获取首页下来树形列表
	@Action(value = "findreagentTaskTemplateListPage")
	public void findreagentTaskTemplateListPage() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String ids = getParameterFromRequest("id");

		Map<String, Object> result = new HashMap<String, Object>();
		result = (Map<String, Object>) reagentTaskNewService.findCellPrimaryCultureTempRecordList(start, length, query,
				col, sort, ids);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cds = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cds);
		map.put("reagentTask-id", "");
		map.put("templeItem-name", "");
		/*
		 * map.put("id", ""); map.put("name", ""); map.put("reagentTask-id", "");
		 * map.put("endTime", ""); map.put("startTime", ""); map.put("contentData", "");
		 * map.put("content", ""); map.put("code", ""); map.put("note", "");
		 * map.put("orderNum", ""); map.put("estimatedDate", ""); map.put("zjResult",
		 * ""); map.put("testUserList", ""); map.put("planEndDate", "");
		 * map.put("planWorkDate", ""); map.put("delay", "");
		 */
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 
	 * @Title: showReagentTaskResultTable @Description
	 *         看見试剂结果 @author @date @return @throws Exception String @throws
	 */
	@Action(value = "showReagentTaskResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showReagentTaskResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		reagentTask = reagentTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskResult.jsp");
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveRoductionResults")
	public void saveRoductionResults() throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		try {
			String id = getParameterFromRequest("id");
			int number = Integer.parseInt(getParameterFromRequest("resultNum"));
			reagentTask = reagentTaskService.get(id);
			Template Template = reagentTask.getTemplate();
			String num = "";
			int nums = 0;
//			List<ReagentTaskInfo> reagentList = reagentTaskNewService.findByReagentTaskIdPage(id);
			List<ReagentTaskInfo> reagentList = reagentTaskNewService.findByReagentTaskIdPage1(Template.getStorage().getBiaoShiMa());
			if (reagentList.size() == 0) {
				for (int i = 0; i < number; i++) {
					ReagentTaskInfo reagentInfo = new ReagentTaskInfo();
					reagentInfo.setStorage(Template.getStorage());
					reagentInfo.setReagentTask(reagentTask);
					reagentInfo.setSubmit("0");
					reagentInfo.setTestType("1");

					String sjName = Template.getStorage().getBiaoShiMa();
					Date date = new Date();
					String riqi = sdf.format(date);
					String serialCode = sjName + riqi + "01";
					System.out.println(serialCode);
					// 实现批号自增
					String num2 = serialCode.substring(0, serialCode.length() - 2);
					System.out.println(num2);
					int nus = Integer.parseInt(serialCode.substring(serialCode.length() - 2));
					System.out.println(nus);
					nums = nus + i;
					if (nums < 10) {
						reagentInfo.setSerial(num2 + "0" + Integer.toString(nums));
					} else {
						reagentInfo.setSerial(num2 + Integer.toString(nums));
					}
					commonService.saveOrUpdate(reagentInfo);
				}

			} else {
				String maxCode = reagentTaskNewService.selectMaxSlideCode(Template.getStorage().getBiaoShiMa());
				Integer length = maxCode.length();
				String hCode = maxCode.substring(length - 2);
				String hNewCode = "";
				int a = Integer.valueOf(hCode) + 1;
				if (a < 10) {
					hNewCode = "0" + a;
				} else {
					hNewCode = a + "";
				}
				String sjName = Template.getStorage().getBiaoShiMa();
				Date date = new Date();
				String riqi = sdf.format(date);
				String serialCode = sjName + riqi+hNewCode;
				System.out.println(serialCode);
				for (int i = 0; i < number; i++) {
					ReagentTaskInfo reagentInfo = new ReagentTaskInfo();
					reagentInfo.setStorage(Template.getStorage());
					reagentInfo.setReagentTask(reagentTask);
					reagentInfo.setSubmit("0");
					reagentInfo.setTestType("1");
					// 实现批号自增
					String num2 = serialCode.substring(0, serialCode.length() - 2);
					System.out.println(num2);
					int nus = Integer.parseInt(serialCode.substring(serialCode.length() - 2));
					System.out.println(nus);
					nums = nus + i;
					if (nums < 10) {
						reagentInfo.setSerial(num2 + "0" + Integer.toString(nums));
					} else {
						reagentInfo.setSerial(num2 + Integer.toString(nums));
					}
					commonService.saveOrUpdate(reagentInfo);
				}
			}
			String scjg = "样本信息结果:" + "名称:" + Template.getStorage().getName() + "的结果已生成";
			if (scjg != null && !"".equals(scjg)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setModifyContent(scjg);
				li.setUserId(u.getId());
				li.setFileId(id);
				li.setState("3");
				li.setStateName("数据修改");
				li.setClassName("ReagentTask");
				commonService.saveOrUpdate(li);
			}

			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @Title: saveAllot @Description: 保存试剂结果 将数据存入到试剂库中
	 * @author :
	 * @date @throws
	 */
	@Action(value = "saveStorageInfo")
	public void saveStorageInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String rea = getParameterFromRequest("applicationTypeActionId");
		StorageReagentBuySerial stor = new StorageReagentBuySerial();
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ReagentTask reagent = commonService.get(ReagentTask.class, id);
			List<ReagentTaskInfo> reagentList = reagentTaskNewService.findByReagentTaskIdPage(id);
			if (reagentList.size() > 0) {
				for (int i = 0; i < reagentList.size(); i++) {
					stor.setNum(Double.valueOf(reagentList.get(i).getNum()));
					stor.setSerial(reagentList.get(i).getSerial());
					stor.setProductDate(new Date());
					stor.setInDate(new Date());
					stor.setIsGood("1");
					stor.setStorage(reagentList.get(i).getStorage());

					commonService.saveOrUpdate(stor);
					// 保存试剂
					reagentList.get(i).setState("1");
					commonService.saveOrUpdate(reagentList.get(i));
				}
			}
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 选择二氧化碳培养箱
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selectInstrumentOne")
	public String selectInstrumentOne() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/cell/passage/instrumentOne.jsp");
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate @Description: 展示排版 @author : @date @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<IncubatorSampleInfo> json = reagentTaskNewService.showWellPlate1(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			reagentTaskNewService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	@Action(value = "findC")
	public void findC() throws Exception {
		String serial = getParameterFromRequest("serial");
		Map<String, Object> result = new HashMap<String, Object>();
		Boolean c = true;
		c = reagentTaskNewService.findC(serial);
		if (c) {
			result.put("success", true);
		} else {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "findCk")
	public void findCk() throws Exception {
		String serial = getParameterFromRequest("serial");
		Map<String, Object> result = new HashMap<String, Object>();
		Boolean c = true;
		c = reagentTaskNewService.findCk(serial);
		if (c) {
			result.put("success", true);
		} else {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ReagentTask getreagentTask() {
		return reagentTask;
	}

	public void setReagentTask(ReagentTask reagentTask) {
		this.reagentTask = reagentTask;
	}

	public ReagentTaskService getreagentTaskService() {
		return reagentTaskService;
	}

	public void setReagentTaskService(ReagentTaskService reagentTaskService) {
		this.reagentTaskService = reagentTaskService;
	}

	public ReagentTaskNewService getreagentTaskNewService() {
		return reagentTaskNewService;
	}

	public void setReagentTaskNewService(ReagentTaskNewService reagentTaskNewService) {
		this.reagentTaskNewService = reagentTaskNewService;
	}

	public ReagentTask getReagentTask() {
		return reagentTask;
	}

}
