﻿package com.biolims.experiment.reagent.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.reagent.dao.ReagentTaskDao;
import com.biolims.experiment.reagent.model.ReagentTask;
import com.biolims.experiment.reagent.model.ReagentTaskCosNew;
import com.biolims.experiment.reagent.model.ReagentTaskInfo;
import com.biolims.experiment.reagent.model.ReagentTaskInfoQuality;
import com.biolims.experiment.reagent.model.ReagentTaskItem;
import com.biolims.experiment.reagent.model.ReagentTaskRecord;
import com.biolims.experiment.reagent.model.ReagentTaskTemp;
import com.biolims.experiment.reagent.service.ReagentTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/reagent/reagentTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ReagentTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260502";
	@Autowired
	private ReagentTaskService reagentTaskService;
	private ReagentTask reagentTask = new ReagentTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private ReagentTaskDao reagentTaskDao;
	@Resource
	private UserGroupUserService userGroupUserService;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleReceiveService sampleReceiveService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 
	 * @Title: showReagentTaskList @Description:展示主表 @author @date @return @throws
	 *         Exception String @throws
	 */
	@Action(value = "showReagentTaskTable")
	public String showReagentTaskTable() throws Exception {
		rightsId = "260502";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTask.jsp");
	}

	@Action(value = "showReagentTaskTableJson")
	public void showReagentTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reagentTaskService.findReagentTaskTable(start, length, query, col, sort);
			List<ReagentTask> list = (List<ReagentTask>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: editReagentTask @Description: 新建实验单 @author @date @return @throws
	 *         Exception String @throws
	 */
	@Action(value = "editReagentTask")
	public String editReagentTask() throws Exception {
		rightsId = "260501";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			reagentTask = reagentTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			String tName = workflowProcessInstanceService.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
			if (reagentTask.getMaxNum() == null) {
				reagentTask.setMaxNum(0);
			}
			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			putObjToContext("bpmTaskId", bpmTaskId);
		} else {
			reagentTask.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			reagentTask.setCreateUser(user);
			reagentTask.setMaxNum(0);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			reagentTask.setCreateDate(stime);
			reagentTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			reagentTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<Template> templateList = templateService.showDialogTemplateSCTableJson("ReagentTask", null);
		List<Template> selTemplate = new ArrayList<Template>();
		List<UserGroupUser> userList = new ArrayList<UserGroupUser>();
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int j = 0; j < templateList.size(); j++) {
			if (templateList.get(j).getAcceptUser() != null) {
				List<UserGroupUser> userTempList = (List<UserGroupUser>) userGroupUserService
						.getUserGroupUserBygroupId(templateList.get(j).getAcceptUser().getId()).get("list");
				for (UserGroupUser ugu : userTempList) {
					if (!userList.contains(ugu)) {
						userList.add(ugu);
					}
				}
			}
			if (reagentTask.getTemplate() != null) {
				if (reagentTask.getTemplate().getId().equals(templateList.get(j).getId())) {
					selTemplate.add(templateList.get(j));
					templateList.remove(j);
					j--;
				}
			}
		}
		for (int i = 0; i < userList.size(); i++) {
			if (reagentTask.getTestUserOneId() != null) {
				String[] userOne = reagentTask.getTestUserOneId().split(",");
				one: for (String u : userOne) {
					if (u.equals(userList.get(i).getUser().getId())) {
						selUser.add(userList.get(i));
						userList.remove(i);
						i--;
						break one;
					}
				}

			}
		}
		putObjToContext("template", templateList);
		putObjToContext("selTemplate", selTemplate);
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		toState(reagentTask.getState());
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskAllot.jsp");
	}

	/**
	 * 
	 * @Title: showReagentTaskItemTable @Description: 展示待排板列表 @author
	 *         : @date @return @throws Exception String @throws
	 */
	@Action(value = "showReagentTaskItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showReagentTaskItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		reagentTask = reagentTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskMakeUp.jsp");
	}

	@Action(value = "showReagentTaskItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showReagentTaskItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = reagentTaskService.findReagentTaskItemTable(scId, start, length, query, col,
					sort);
			List<ReagentTaskItem> list = (List<ReagentTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sjComposition", "");
			map.put("ysConcentration", "");
			map.put("dosage", "");
			map.put("outConcentration", "");
			map.put("qcSampleId", "");
			map.put("qcResult", "");
			map.put("fPrimer", "");
			map.put("rPrimer", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showReagentTaskItemAfTableJson @Description: 排板后样本展示 @author
	 *         : @date @throws Exception void @throws
	 */

	@Action(value = "showReagentTaskItemAfTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showReagentTaskItemAfTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = reagentTaskService.findReagentTaskItemAfTable(scId, start, length, query, col,
					sort);
			List<ReagentTaskItem> list = (List<ReagentTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("reagentTask-name", "");
			map.put("reagentTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("chromosomalLocation", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("color", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delReagentTaskItem @Description: 删除待排板样本 @author @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "delReagentTaskItem")
	public void delReagentTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reagentTaskService.delReagentTaskItem(delStr, ids, user, id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: delReagentTaskItemAf @Description: 重新排板 @author : @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "delReagentTaskItemAf")
	public void delReagentTaskItemAf() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reagentTaskService.delReagentTaskItemAf(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showBReagentTaskResultTable @Description @author @date @return @throws
	 *         Exception String @throws
	 */
	@Action(value = "showReagentTaskResultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showReagentTaskResultTable() throws Exception {
		String id = getParameterFromRequest("id");
		reagentTask = reagentTaskService.get(id);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskResult.jsp");
	}

	@Action(value = "showReagentTaskResultTableJson")
	public void showReagentTaskResultTableJson() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reagentTaskService.showReagentTaskResultTableJson(id, start, length, query,
					col, sort);
			List<ReagentTaskInfo> list = (List<ReagentTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("reagents-id", "");
			map.put("reagents-storage-name", "");
			map.put("serial", "");
			// map.put("expireDate", "yyyy-mm-dd");
			map.put("num", "");
			map.put("reagents-unit-name", "");
			map.put("code", "");
			map.put("code", "");
			map.put("code", "");
			map.put("code", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("note", "");
			map.put("reagentTask-id", "");
			map.put("reagentTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("orderCode", "");
			map.put("serial", "");
			map.put("testType", "");
			map.put("submit", "");

			map.put("sampleDeteyion-id", "");
			map.put("sampleDeteyion-name", "");

			// 培养箱
			map.put("instrument-id", "");
			map.put("instrument-storageContainer-rowNum", "");
			map.put("instrument-storageContainer-colNum", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("qualified", "");
			map.put("sxDate", "yyyy-MM-dd");

			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: delReagentTaskResult @Description: 删除结果明细 @author : @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "delReagentTaskResult")
	public void delReagentTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reagentTaskService.delReagentTaskResult(ids, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showReagentTaskSteps @Description: 实验步骤 @author
	 *         : @date @return @throws Exception String @throws
	 */
	@Action(value = "showReagentTaskSteps", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showReagentTaskSteps() throws Exception {
		String id = getParameterFromRequest("id");
		reagentTask = reagentTaskService.get(id);
		String orderNumBy = getParameterFromRequest("orderNum");
		putObjToContext("orderNumBy", orderNumBy);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		putObjToContext("bpmTaskId", bpmTaskId);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskSteps.jsp");
	}

	@Action(value = "showReagentTaskStepsJson")
	public void showReagentTaskStepsJson() throws Exception {
		String id = getParameterFromRequest("id");
		String orderNum = getParameterFromRequest("orderNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = reagentTaskService.showReagentTaskStepsJson(id, orderNum);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除试剂明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delReagentTaskReagent")
	public void delReagentTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("main_id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String id = getParameterFromRequest("id");
			reagentTaskService.delReagentTaskReagent(id, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除设备明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delReagentTaskCos")
	public void delReagentTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String delStr = getParameterFromRequest("del");
		String mainId = getParameterFromRequest("main_id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		try {
			String id = getParameterFromRequest("id");
			reagentTaskService.delReagentTaskCos(id, delStr, mainId, user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: makeCode @Description: 打印条码 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String id = getParameterFromRequest("id");
		String[] sampleCode = getRequest().getParameterValues("sampleCode[]");
		CodeMain codeMain = null;
		codeMain = codeMainService.get(id);
		if (codeMain != null) {
			String printStr = "";
			String context = "";
			for (int a = 0; a < sampleCode.length; a++) {
				String codeFull = sampleCode[a];
				String name = "";
				// sampleReceiveService.getNameBySampleCode(codeFull);
				printStr = codeMain.getCode();
				String code1 = sampleCode[a].substring(0, 9);
				String code2 = sampleCode[a].substring(9);
				printStr = printStr.replaceAll("@@code1@@", code1);
				printStr = printStr.replaceAll("@@code2@@", code2);
				printStr = printStr.replaceAll("@@code@@", codeFull);
				printStr = printStr.replaceAll("@@name@@", name);
				context += printStr;
			}
			String ip = codeMain.getIp();
			Socket socket = null;
			OutputStream os;
			try {
				System.out.println(context);
				socket = new Socket();
				SocketAddress sa = new InetSocketAddress(ip, 9100);
				socket.connect(sa);
				os = socket.getOutputStream();
				os.write(context.getBytes("UTF-8"));
				os.flush();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (socket != null) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: showReagentTaskFromReceiveList @Description: 展示临时表 @author
	 *         : @date @return @throws Exception String @throws
	 */
	@Action(value = "showReagentTaskTempTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showReagentTaskFromReceiveList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/reagent/reagentTaskTemp.jsp");
	}

	@Action(value = "showReagentTaskTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showReagentTaskTempTableJson() throws Exception {

		String[] codes = getRequest().getParameterValues("codes[]");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reagentTaskService.selectReagentTaskTempTable(codes, start, length, query, col,
					sort);
			List<ReagentTaskTemp> list = (List<ReagentTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productName", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("orderCode", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-project-id", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: saveAllot @Description: 保存任务分配页面 @author : @date @throws
	 */
	@Action(value = "saveAllot")
	public void saveAllot() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String templateId = getParameterFromRequest("template");
		String logInfo = getParameterFromRequest("logInfo");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = reagentTaskService.saveAllot(main, itemJson, userId, templateId, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveMakeUp @Description: 保存排版界面 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveMakeUp")
	public void saveMakeUp() throws Exception {
		String blood_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			reagentTaskService.saveMakeUp(blood_id, item, logInfo);
			result.put("success", true);
			result.put("id", blood_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveLeftQuality")
	public void saveLeftQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ReagentTask reagentTask = reagentTaskService.get(id);
			reagentTaskService.saveLeftQuality(reagentTask, ids, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveRightQuality")
	public void saveRightQuality() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ReagentTask reagentTask = reagentTaskService.get(id);
			reagentTaskService.saveRightQuality(reagentTask, ids, logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveSteps @Description: 保存实验步骤 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveSteps")
	public void saveSteps() throws Exception {
		String id = getParameterFromRequest("id");
		String templateJson = getParameterFromRequest("templateJson");
		String reagentJson = getParameterFromRequest("reagentJson");
		String cosJson = getParameterFromRequest("cosJson");
		String logInfo = getParameterFromRequest("logInfo");

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.saveSteps(id, templateJson, reagentJson, cosJson, logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveResult @Description: 保存结果表 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "saveResult")
	public void saveResult() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		String confirmUser = getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.saveResult(id, dataJson, logInfo, confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveReagentTaskTemp")
	public void saveReagentTaskTemp() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		String confirmUser = getParameterFromRequest("confirmUser");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.saveResult(id, dataJson, logInfo, confirmUser);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: showWellPlate @Description: 展示排版 @author : @date @throws
	 */
	@Action(value = "showWellPlate")
	public void showWellPlate() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<ReagentTaskItem> json = reagentTaskService.showWellPlate(id);
			map.put("data", json);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: plateLayout @Description: 排板 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "plateLayout")
	public void plateLayout() throws Exception {
		String[] data = getRequest().getParameterValues("data[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			reagentTaskService.plateLayout(data);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
	}

	/**
	 * 
	 * @Title: plateSample @Description: 孔板样本 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "plateSample")
	public void plateSample() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = reagentTaskService.plateSample(id, counts);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: plateSampleTable @Description:展示孔板样本列表 @author : @date @throws
	 *         Exception void @throws
	 */
	@Action(value = "plateSampleTable")
	public void plateSampleTable() throws Exception {
		String id = getParameterFromRequest("id");
		String counts = getParameterFromRequest("counts");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = reagentTaskService.plateSampleTable(id, counts, start, length, query, col, sort);
			List<ReagentTaskItem> list = (List<ReagentTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("note", "");
			map.put("color", "");
			map.put("chromosomalLocation", "");
			map.put("concentration", "");
			map.put("reagentTask-name", "");
			map.put("reagentTask-id", "");
			map.put("volume", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("posId", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleTypeId", "");
			map.put("dicSampleTypeName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blendCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("isOut", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: uploadCsvFile @Description: 上传结果 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		String id = getParameterFromRequest("id");
		String fileId = getParameterFromRequest("fileId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.getCsvContent(id, fileId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: bringResult @Description: 生成结果 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "bringResult")
	public void bringResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		try {
			reagentTaskService.bringResult(id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: generateBlendCode @Description: 生成混合号 @author : shengwei.wang @date
	 *         2018年3月6日上午11:02:26 void @throws
	 */
	@Action(value = "generateBlendCode")
	public void generateBlendCode() {
		String id = getParameterFromRequest("id");
		try {
			Integer blendCode = reagentTaskService.generateBlendCode(id);
			HttpUtils.write(String.valueOf(blendCode));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @throws Exception
	 * 
	 * @Title: submitSample @Description: 提交样本 @author : shengwei.wang @date
	 *         2018年3月22日下午5:39:40 void @throws
	 */
	@Action(value = "submitSample")
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.submitSample(id, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: downLoadTemp @Description: TODO(核算提取模板下载) @param @param
	 *         ids @param @throws Exception    设定文件 @return void    返回类型 @author
	 *         zhiqiang.yang@biolims.cn @date 2017-8-22 上午11:46:55 @throws
	 */
	@Action(value = "downLoadTemp")
	public void downLoadTemp() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("ids");
		String[] codes = getRequest().getParameterValues("codes");
		String id = ids[0];
		String code = codes[0];
		String[] str1 = code.split(",");
		String co = str1[0];
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = co + sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		properties.load(is);

		String filePath = properties.getProperty("result.template.file") + "\\";// 写入csv路径
		String fileName = filePath + a + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "样本编号", "原始样本编号", "产物类型", "检测项目", "浓度", "体积" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		String[] sid = id.split(",");
		for (int j = 0; j < sid.length; j++) {
			String idc = sid[j];
			for (int i = 0; i < ids.length; i++) {
				ReagentTaskInfo sr = reagentTaskService.getInfoById(idc);
				StringBuffer sb = new StringBuffer();
				setMolecularMarkersData(sr, sb);
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
				if (sr.equals("")) {
					result.put("success", false);
				} else {
					result.put("success", true);

				}
			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3(a, filePath);
	}

	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData @Description: TODO(将对应的值添加到模板里) @param @param
	 *         sr @param @param sb @param @throws Exception    设定文件 @return void   
	 *         返回类型 @author zhiqiang.yang@biolims.cn @date 2017-8-22
	 *         下午1:21:58 @throws
	 */
	public void setMolecularMarkersData(ReagentTaskInfo sr, StringBuffer sb) throws Exception {
		if (null != sr.getCode()) {
			sb.append("\"").append(sr.getCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null != sr.getSampleCode()) {
			sb.append("\"").append(sr.getSampleCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}

		if (null != sr.getDicSampleType().getName()) {
			sb.append("\"").append(sr.getDicSampleType().getName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}

		if (null != sr.getProductName()) {
			sb.append("\"").append(sr.getProductName()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
	}

	/**
	 * 
	 * @Title: toUtf8String @Description: TODO(解决乱码) @param @param
	 *         s @param @return    设定文件 @return String    返回类型 @author
	 *         zhiqiang.yang@biolims.cn @date 2017-8-23 下午4:40:07 @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ReagentTaskService getReagentTaskService() {
		return reagentTaskService;
	}

	public void setReagentTaskService(ReagentTaskService reagentTaskService) {
		this.reagentTaskService = reagentTaskService;
	}

	public ReagentTask getReagentTask() {
		return reagentTask;
	}

	public void setReagentTask(ReagentTask reagentTask) {
		this.reagentTask = reagentTask;
	}

	public ReagentTaskDao getReagentTaskDao() {
		return reagentTaskDao;
	}

	public void setReagentTaskDao(ReagentTaskDao reagentTaskDao) {
		this.reagentTaskDao = reagentTaskDao;
	}

	@Action(value = "submitSJQualityTest")
	public void submitSJQualityTest() throws Exception {
		String[] ids = getRequest().getParameterValues("id[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.submitSJQualityTest(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: @Description: 查询生产步骤要用的设备 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "findProductCos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findProductCos() throws Exception {
		String id = getParameterFromRequest("id");
		String stepNum = getParameterFromRequest("stepNum");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<ReagentTaskCosNew> ccs = reagentTaskService.getCellproductCoss(id, stepNum);
			map.put("ReagentTaskCossNew", ccs);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 生产步骤第一步保存设备 @Title: submitToQualityTest @Description: TODO @param @throws
	 * Exception @return void @author @date 2018年9月12日 @throws
	 */
	@Action(value = "saveCosPrepare")
	public void saveCosPrepare() throws Exception {
		String cosListJson = getParameterFromRequest("data");
		String roomName = getParameterFromRequest("roomName");
		String roomId = getParameterFromRequest("roomId");
		String zbid = getParameterFromRequest("zbid");
		String bzs = getParameterFromRequest("bzs");
		String changeLog = getParameterFromRequest("changeLog");
		// JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.saveCosPrepare(cosListJson, roomName, zbid, bzs, roomId, changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "startPrepare")
	public void startPrepare() throws Exception {
		String cosListJson = getParameterFromRequest("data");
		String roomName = getParameterFromRequest("roomName");
		String roomId = getParameterFromRequest("roomId");
		// JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Boolean zy = reagentTaskService.startPrepare(cosListJson, roomId);
			if (zy) {
				map.put("zy", true);
				reagentTaskService.startPreparezy(cosListJson, roomName, roomId);
			} else {
				map.put("zy", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 确认当前步骤 @Title: finishPresentStep @Description: TODO @param @throws
	 * Exception @return void @author 孙灵达 @date 2019年4月11日 @throws
	 */
	@Action(value = "finishPresentStep")
	public void finishPresentStep() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		String startTime = getParameterFromRequest("startTime");
		String endTime = getParameterFromRequest("endTime");
		String hhzt = getParameterFromRequest("hhzt");
		Map<String, Object> map = new HashMap<String, Object>();

//		//改变状态
//		if(!"1".equals(stepNum)) {
//			
//	
//		ReagentTask sct = reagentTaskDao.get(ReagentTask.class,mainId);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//		reagentTaskDao.update(sct);
//		} 

		try {
			reagentTaskService.finishPresentStep(id, ids, stepNum, mainId, startTime, endTime, hhzt);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断当前步骤是否是最后一步 TODO @param @throws Exception @return void @author
	 * 
	 */
	@Action(value = "changeButtonBlock")
	public void changeButtonBlock() throws Exception {
		String stepNum = getParameterFromRequest("stepNum");
		String mainId = getParameterFromRequest("mainId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<ReagentTaskRecord> cp = reagentTaskService.getCellProductionRecordByIdAndStepNum(mainId, stepNum);
			if (cp.size() > 0) {
				map.put("zhyb", false);
			} else {
				map.put("zhyb", true);
			}
			List<ReagentTaskRecord> cps = reagentTaskDao.getCellProductionRecordByIdAndStepNumForzy(mainId, stepNum);
			if (cps.size() > 0) {
				ReagentTaskRecord cpr = cps.get(0);
				if (cpr.getEndTime() != null && !"".equals(cpr.getEndTime())) {
					map.put("dqbz", false);
				} else {
					map.put("dqbz", true);
				}
			} else {
				map.put("dqbz", true);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "serchInfos")
	public void serchInfos() throws Exception {
		String mainId = getParameterFromRequest("mainId");
		String orderNums1 = getParameterFromRequest("orderNums1");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = reagentTaskService.serchInfos(mainId, orderNums1);
			map.put("sueccess", true);
		} catch (Exception e) {
			map.put("sueccess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 查找生产操作的开始时间和结束时间
	 * 
	 * @throws Exception
	 * 
	 */
	@Action(value = "findProductTime")
	public void findProductTime() throws Exception {
		String mainId = getParameterFromRequest("mainId");
		String stepNum = getParameterFromRequest("stepNum");
		boolean state = reagentTaskService.findProductTime(mainId, stepNum);

		Map<String, Object> ruku = reagentTaskService.findCellPassageRecords(mainId, stepNum);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", state);

		map.put("samplestate", ruku.get("samplestate"));
		map.put("state", ruku.get("state"));

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: selectCos @Description: 选择设备 @author : shengwei.wang @date
	 *         2018年1月26日上午11:56:18 @return @throws Exception String @throws
	 */
	@Action(value = "selectCos")
	public String selectCos() throws Exception {
		String typeId = getParameterFromRequest("typeId");
		String roomName = URLDecoder.decode(getParameterFromRequest("roomName"), "UTF-8");
		putObjToContext("typeId", typeId);
		putObjToContext("roomName", roomName);
		return dispatcher("/WEB-INF/page/system/template/cosTemplateDialog.jsp");

	}

	// 试剂制备提交质检明细
	@Action(value = "submitToQualityTestType")
	public void submitToQualityTestType() throws Exception {
		// 主表id
		String id = getParameterFromRequest("id");
		// 实验步骤
		String stepNum = getParameterFromRequest("stepNum");
		// 实验名称
		String stepName = getParameterFromRequest("stepName");
		// 模板名称
		String type = getParameterFromRequest("zj");
		// 步骤中明细的id
		String[] ids = getRequest().getParameterValues("idList[]");
		// 要质检的项
		String[] chosedIDs = getRequest().getParameterValues("chosedID[]");
		// 哪个模块提交过去的
		String mark = getParameterFromRequest("mark");
		// 提交过来json数组
		String itemjson = getParameterFromRequest("data");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.submitToQualityTestType(id, stepNum, stepName, mark, ids, chosedIDs, type, itemjson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	/**
	 * 质检明细查询 @Title: noBlendPlateSampleTable @Description: TODO @param @throws
	 * Exception @return void @author @date @throws
	 */
	@Action(value = "findQualityItem")
	public void findQualityItem() throws Exception {
		String id = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = reagentTaskService.findQualityItem(id, start, length, query, col, sort);
			List<ReagentTaskInfoQuality> list = (List<ReagentTaskInfoQuality>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			/** 编码 */
			map.put("id", "");
			/** 提交表编码 */
			map.put("tempId", "");
			/** 样本编号 */
			map.put("code", "");
			/** 检测项目 */
			map.put("productId", "");
			/** 检测项目 */
			map.put("productName", "");
			/** 状态 */
			map.put("state", "");
			/** 备注 */
			map.put("note", "");
			/** 浓度 */
			map.put("concentration", "");
			/** 样本数量 */
			map.put("sampleNum", "");
			/** 相关主表 */
			map.put("reagentTask-id", "");
			map.put("reagentTask-name", "");
			/** 体积 */
			map.put("volume", "");
			/** 任务单id */
			map.put("orderId", "");
			/** 中间产物数量 */
			map.put("productNum", "");
			/** 中间产物类型编号 */
			map.put("dicSampleTypeId", "");
			/** 中间产物类型 */
			map.put("dicSampleTypeName", "");
			/** 样本类型 */
			map.put("sampleType", "");
			/** 样本主数据 */
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-name", "");
			/** 实验的步骤号 */
			map.put("stepNum", "");
			/** 检测项 */
			map.put("sampleDeteyion-id", "");
			map.put("sampleDeteyion-name", "");
			/** 是否合格 */
			map.put("qualified", "");
			/** 质检结果表id */
			map.put("qualityInfoId", "");
			/** 质检是否接收 */
			map.put("qualityReceive", "");
			/** 质检提交时间 */
			map.put("qualitySubmitTime", "yyyy-MM-dd HH:mm:ss");
			/** 质检接收时间 */
			map.put("qualityReceiveTime", "yyyy-MM-dd HH:mm:ss");
			/** 质检完成时间 */
			map.put("qualityFinishTime", "yyyy-MM-dd HH:mm:ss");
			/** 质检提交人 */
			map.put("qualitySubmitUser", "");
			/** 质检接收人 */
			map.put("qualityReceiveUser", "");
			/** 检测方式 */
			map.put("cellType", "");
			/** 是否提交 */
			map.put("submit", "");

			/** 原辅料Id */
			map.put("storage-id", "");
			/** 原辅料名称 */
			map.put("storage-name", "");

			map.put("sampleNumUnit", "");

			map.put("experimentalStepsName", "");
			// 样本编号New
			map.put("SampleNumber", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: 删除待提交质检样本 Exception void @throws
	 */
	@Action(value = "delQualityTestSample")
	public void delQualityTestSample() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String id = getParameterFromRequest("id");
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reagentTaskService.delQualityTestSampleType(ids, user, id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 质检明细提交质检方法 Exception @return void @author @date 2018年9月12日 @throws
	 */
	@Action(value = "submitToQualityTestSample")
	public void submitToQualityTestSample() throws Exception {
		// 质检明细id集合
		String[] ids = getRequest().getParameterValues("ids[]");
		String id = getParameterFromRequest("id");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			reagentTaskService.submitToQualityTestSample(ids, changeLogItem, id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	/**
	 * 
	 * @Title: @Description: 根据质检结果id查询质检结果表 @author : @date @throws Exception
	 *         void @throws
	 */
	@Action(value = "findQualityInfoByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String findQualityInfoByid() {
		String infoid = getParameterFromRequest("infoid");
		String id = "";
		String sczj = "sc";
		if (infoid != null && !"".equals(infoid)) {
			QualityTestInfo qti = commonDAO.get(QualityTestInfo.class, infoid);
			if (qti != null) {
				id = qti.getQualityTest().getId();
			}
		} else {

		}
		return redirect("/experiment/quality/qualityTest/showQualityTestResultTable.action?id=" + id + "&falg=false"
				+ "&sczj=sczj");
	}

}
