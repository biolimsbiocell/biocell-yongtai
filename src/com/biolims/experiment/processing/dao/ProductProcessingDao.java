package com.biolims.experiment.processing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.processing.model.ProductProcessing;
import com.biolims.experiment.processing.model.ProductProcessingCos;
import com.biolims.experiment.processing.model.ProductProcessingItem;
import com.biolims.experiment.processing.model.ProductProcessingReagent;
import com.biolims.experiment.processing.model.ProductProcessingTemp;
import com.biolims.experiment.processing.model.ProductProcessingTemplate;
import com.biolims.experiment.processing.model.ProductProcessingInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class ProductProcessingDao extends BaseHibernateDao {

	public Map<String, Object> findProductProcessingTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ProductProcessing where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ProductProcessing where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ProductProcessing> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectProductProcessingTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from ProductProcessingTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<ProductProcessingTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from ProductProcessingTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findProductProcessingItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ProductProcessingItem where 1=1 and state='1' and productProcessing.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ProductProcessingItem  where 1=1 and state='1' and productProcessing.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ProductProcessingItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<ProductProcessingItem> showWellPlate(String id) {
		String hql="from ProductProcessingItem  where 1=1 and state='2' and  productProcessing.id='"+id+"'";
		List<ProductProcessingItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findProductProcessingItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ProductProcessingItem where 1=1 and state='2' and productProcessing.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ProductProcessingItem  where 1=1 and state='2' and productProcessing.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ProductProcessingItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from ProductProcessingItem where 1=1 and state='2' and productProcessing.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from ProductProcessingItem where 1=1 and state='2' and productProcessing.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<ProductProcessingTemplate> showProductProcessingStepsJson(String id,String code) {

		String countHql = "select count(*) from ProductProcessingTemplate where 1=1 and productProcessing.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<ProductProcessingTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ProductProcessingTemplate where 1=1 and productProcessing.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ProductProcessingReagent> showProductProcessingReagentJson(String id,String code) {
		String countHql = "select count(*) from ProductProcessingReagent where 1=1 and productProcessing.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<ProductProcessingReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ProductProcessingReagent where 1=1 and productProcessing.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ProductProcessingCos> showProductProcessingCosJson(String id,String code) {
		String countHql = "select count(*) from ProductProcessingCos where 1=1 and productProcessing.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<ProductProcessingCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from ProductProcessingCos where 1=1 and productProcessing.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<ProductProcessingTemplate> delTemplateItem(String id) {
		List<ProductProcessingTemplate> list=new ArrayList<ProductProcessingTemplate>();
		String hql = "from ProductProcessingTemplate where 1=1 and productProcessing.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<ProductProcessingReagent> delReagentItem(String id) {
		List<ProductProcessingReagent> list=new ArrayList<ProductProcessingReagent>();
		String hql = "from ProductProcessingReagent where 1=1 and productProcessing.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<ProductProcessingCos> delCosItem(String id) {
		List<ProductProcessingCos> list=new ArrayList<ProductProcessingCos>();
		String hql = "from ProductProcessingCos where 1=1 and productProcessing.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showProductProcessingResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ProductProcessingInfo where 1=1 and productProcessing.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ProductProcessingInfo  where 1=1 and productProcessing.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ProductProcessingInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from ProductProcessingItem where 1=1 and productProcessing.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<ProductProcessingItem> plateSample(String id, String counts) {
		String hql="from ProductProcessingItem where 1=1 and productProcessing.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<ProductProcessingItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ProductProcessingItem where 1=1 and productProcessing.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from ProductProcessingItem  where 1=1 and productProcessing.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<ProductProcessingItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<ProductProcessingInfo> findProductProcessingInfoByCode(String code) {
		String hql="from ProductProcessingInfo where 1=1 and code='"+code+"'";
		List<ProductProcessingInfo> list=new ArrayList<ProductProcessingInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<ProductProcessingInfo> selectAllResultListById(String code) {
		String hql = "from ProductProcessingInfo  where (submit is null or submit='') and productProcessing.id='"
				+ code + "'";
		List<ProductProcessingInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<ProductProcessingInfo> selectResultListById(String id) {
		String hql = "from ProductProcessingInfo  where productProcessing.id='"
				+ id + "'";
		List<ProductProcessingInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ProductProcessingInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from ProductProcessingInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<ProductProcessingInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<ProductProcessingItem> selectProductProcessingItemList(String scId)
			throws Exception {
		String hql = "from ProductProcessingItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and productProcessing.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProductProcessingItem> list = new ArrayList<ProductProcessingItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from ProductProcessingItem where 1=1 and productProcessing.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public ProductProcessingInfo getResultByCode(String code) {
			String hql=" from ProductProcessingInfo where 1=1 and code='"+code+"'";
			ProductProcessingInfo spi=(ProductProcessingInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}

		public Map<String, Object> showStateCompleteInfoList(Integer start, Integer length, String query, String col,
				String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from ProductProcessing where 1=1 and state = '1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from ProductProcessing where 1=1 and state = '1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<ProductProcessing> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		}

		public Map<String, Object> findStateCompleteInfoList(String id) {
			Map<String, Object> map = new HashMap<String, Object>();
			String hql = "from ProductProcessingInfo where 1=1 and productProcessing.id = '"+id+"'";
			List<ProductProcessingInfo> list = getSession().createQuery(hql).list();
			map.put("list", list);
			return map;
		}
	
}