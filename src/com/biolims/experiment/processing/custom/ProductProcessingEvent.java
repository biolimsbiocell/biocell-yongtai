package com.biolims.experiment.processing.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.processing.service.ProductProcessingService;

public class ProductProcessingEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		ProductProcessingService mbService = (ProductProcessingService) ctx
				.getBean("productProcessingService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
