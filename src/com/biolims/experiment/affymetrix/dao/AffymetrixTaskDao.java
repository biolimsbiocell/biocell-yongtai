package com.biolims.experiment.affymetrix.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveItem;
import com.biolims.experiment.affymetrix.model.AffymetrixTask;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskCos;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskItem;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReagent;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskTemp;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskTemplate;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskInfo;

@Repository
@SuppressWarnings("unchecked")
public class AffymetrixTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectAffymetrixTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AffymetrixTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTask> list = new ArrayList<AffymetrixTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectAffymetrixTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskItem> list = new ArrayList<AffymetrixTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectAffymetrixTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskInfo> list = new ArrayList<AffymetrixTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 模板对应的子表
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAffymetrixTaskTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskTemplate> list = new ArrayList<AffymetrixTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				key = key + " order by"+castStrId("code")+"";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectAffymetrixTaskTemplateReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReagent> list = new ArrayList<AffymetrixTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据ItemId查询试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAffymetrixTaskTemplateReagentListByItemId(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from AffymetrixTaskReagent t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReagent> list = new ArrayList<AffymetrixTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from AffymetrixTaskReagent t where 1=1 and t.affymetrixTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<AffymetrixTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from AffymetrixTaskCos t where 1=1 and t.affymetrixTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<AffymetrixTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectAffymetrixTaskTemplateCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskCos> list = new ArrayList<AffymetrixTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAffymetrixTaskTemplateCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from AffymetrixTaskCos t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskCos> list = new ArrayList<AffymetrixTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据AffymetrixTaskAffymetrix实验主表ID查询试剂明细
	 */
	public List<AffymetrixTaskReagent> setReagentList(String code) {
		String hql = "from AffymetrixTaskReagent where 1=1 and affymetrixTask.id='" + code
				+ "'";
		List<AffymetrixTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据AffymetrixTaskAffymetrix实验主表ID查询Affymetrix实验明细
	 */
	public List<AffymetrixTaskItem> setAffymetrixTaskItemList(String code) {
		String hql = "from AffymetrixTaskItem where 1=1 and affymetrixTask.id='" + code + "'";
		List<AffymetrixTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据状态查询AffymetrixTask
	 * @return
	 */
	public List<AffymetrixTask> selectAffymetrixTaskList() {
		// String hql = "from AffymetrixTask t where 1=1 and t.state=1";
		String hql = "from AffymetrixTask t where 1=1 and t.state='1'";
		List<AffymetrixTask> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<AffymetrixTaskInfo> selectAffymetrixTaskResultList(String id) {
		String hql = "from AffymetrixTaskInfo t where 1=1 and affymetrixTask.state='1' and affymetrixTask='"
				+ id + "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<AffymetrixTaskInfo> setAffymetrixTaskResultByCode(String code) {
		String hql = "from AffymetrixTaskInfo t where 1=1 and affymetrixTask.id='" + code
				+ "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<AffymetrixTaskInfo> setAffymetrixTaskResultById(String code) {
		String hql = "from AffymetrixTaskInfo t where (submit is null or submit='') and affymetrixTask.id='"
				+ code + "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<AffymetrixTaskInfo> setAffymetrixTaskResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from AffymetrixTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询Affymetrix实验样本去到QPCR或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectExperimentAffymetrixTaskReslutList(
			Integer startNum, Integer limitNum, String dir, String sort,
			String state) throws Exception {
		String hql = "from AffymetrixTaskInfo where 1=1 and experiment.affymetrixInfo.state='完成' and affymetrixTask.stateName='"
				+ state + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskInfo> list = new ArrayList<AffymetrixTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAffymetrixTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskReceiveItem where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReceiveItem> list = new ArrayList<AffymetrixTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询异常Affymetrix实验
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAffymetrixTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskInfo> list = new ArrayList<AffymetrixTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询左侧中间表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectAffymetrixTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AffymetrixTaskTemp where 1=1 and state='1'";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskTemp> list = new ArrayList<AffymetrixTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询Affymetrix实验异常样本
	 * @param code
	 * @return
	 */
	public List<AffymetrixTaskInfo> selectAffymetrixTaskResultAbnormalList(String code) {
		String hql = "from AffymetrixTaskInfo where 1=1 and  affymetrixTask='" + code + "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<AffymetrixTaskInfo> selectAffymetrixTaskAllList(String code) {
		String hql = "from AffymetrixTaskInfo  where 1=1  and affymetrixTask='" + code
				+ "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id查询Affymetrix实验生成结果 用于更新是否合格字段
	 * @param id
	 * @return
	 */
	public List<AffymetrixTaskInfo> findAffymetrixTaskResultById(String id) {
		String hql = "from AffymetrixTaskInfo  where 1=1 and code='" + id + "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 状态完成下一步流向
	 * @param code
	 * @return
	 */
	public List<AffymetrixTaskInfo> setNextFlowList(String code) {
		String hql = "from AffymetrixTaskInfo where 1=1  and affymetrixTask.state='1' and affymetrixTask.id='"
				+ code + "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从上一步样本接收完成的Affymetrix实验样本
	 * @return
	 */
	public List<AffymetrixTaskReceiveItem> selectReceiveAffymetrixTaskList() {
		String hql = "from AffymetrixTaskReceiveItem where 1=1  and state='1' ";
		List<AffymetrixTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from AffymetrixTaskItem t where 1=1 and t.affymetrixTask.id='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据id查询Affymetrix实验结果
	 * @param id
	 * @return
	 */
	public List<AffymetrixTaskInfo> selectAffymetrixTaskResultListById(String id) {
		String hql = "from AffymetrixTaskInfo where 1=1  and affymetrixTask.id='" + id
				+ "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的Affymetrix实验次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from AffymetrixTaskItem t where 1=1 and t.sampleCode='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.Code)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 查询从样本接收完成的的样本
	 * @param id
	 * @return
	 */
	public List<AffymetrixTaskItem> findAffymetrixTaskItemList(String id) {
		String hql = "from AffymetrixTaskItem  where 1=1 and state='1' and affymetrixTask='"
				+ id + "'";
		List<AffymetrixTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据Code查询AffymetrixTaskInfo
	 * @param code
	 * @return
	 */
	public String findAffymetrixTaskInfo(String code) {
		String hql = "from AffymetrixTaskInfo  where 1=1 and tempId='" + code + "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 根据样本编号查出AffymetrixTaskTemp对象
	 * @param code
	 * @return
	 */
	public AffymetrixTaskTemp findAffymetrixTaskTemp(String code) {
		String hql = "from AffymetrixTaskTemp t where 1=1 and t.sampleCode = '" + code
				+ "'";
		AffymetrixTaskTemp AffymetrixTaskTemp = (AffymetrixTaskTemp) this.getSession()
				.createQuery(hql).uniqueResult();
		return AffymetrixTaskTemp;
	}


	/**
	 * 根据样本查询主表
	 * @param sid
	 * @return
	 */
	public String selectTask(String sid) {
		String hql = "from TechCheckServiceOrder t where t.id = '" + sid + "'";
		String str = (String) this.getSession()
				.createQuery(" select t.id " + hql).uniqueResult();
		return str;
	}

	public List<AffymetrixTaskItem> selectAffymetrixTaskItemList(String scId) throws Exception {
		String hql = "from AffymetrixTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskItem> list = new ArrayList<AffymetrixTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * 根据样本编号查询检测临时表
	 * @param id
	 * @return
	 */
	public List<AffymetrixTaskInfo> setAffymetrixTaskInfo(String id) {
		String hql = "from AffymetrixTaskInfo where 1=1 and affymetrixTask.id='" + id
				+ "' and nextFlowId='0009'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据入库的原始样本号查询该结果表信息
	 * @param id
	 * @param Code
	 * @return
	 */
	public List<AffymetrixTaskInfo> setAffymetrixTaskInfo2(String id, String code) {
		String hql = "from AffymetrixTaskInfo t where 1=1 and affymetrixTask.id='" + id
				+ "' and sampleCode='" + code + "'";
		List<AffymetrixTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}