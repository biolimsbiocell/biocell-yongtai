package com.biolims.experiment.affymetrix.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveTemp;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceive;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveItem;

@Repository
@SuppressWarnings("unchecked")
public class AffymetrixTaskReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectAffymetrixTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AffymetrixTaskReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReceive> list = new ArrayList<AffymetrixTaskReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectAffymetrixTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from AffymetrixTaskReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReceiveItem> list = new ArrayList<AffymetrixTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectAffymetrixTaskReceiveItemByTypeList(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from AffymetrixTaskReceiveItem where 1=1 and affymetrixTaskReceive.Type='AffymetrixTask'";
		String key = "";
		if (scId != null)
			key = key + " and affymetrixTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReceiveItem> list = new ArrayList<AffymetrixTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询从开向检验完成的样本
	 * @return
	 */
	public List<AffymetrixTaskReceiveTemp> selectAffymetrixTaskReceiveTempList() {
		String hql = "from AffymetrixTaskReceiveTemp where 1=1 and state='1' order by reportDate desc";
		List<AffymetrixTaskReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 修改状态
	 * @param id
	 * @return
	 */
	public List<AffymetrixTaskReceiveItem> selectReceiveByIdList(String id) {
		String hql = "from AffymetrixTaskReceiveItem where 1=1 and state='1' and method='1' and affymetrixTaskReceive.id='"
				+ id + "'";
		List<AffymetrixTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 明细数据
	 * @param code
	 * @return
	 */
	public List<AffymetrixTaskReceiveItem> selectReceiveItemListById(String code) {
		String hql = "from AffymetrixTaskReceiveItem where 1=1 and affymetrixTaskReceive.id='"
				+ code + "'";
		List<AffymetrixTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public Map<String, Object> selectAffymetrixTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from AffymetrixTaskReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AffymetrixTaskReceiveTemp> list = new ArrayList<AffymetrixTaskReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
}