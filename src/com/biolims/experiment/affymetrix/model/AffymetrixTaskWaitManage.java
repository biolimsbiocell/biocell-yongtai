package com.biolims.experiment.affymetrix.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: Affymetrix实验管理
 * @author lims-platform
 * @date 2015-11-27 15:41:13
 * @version V1.0   
 *
 */
@Entity
@Table(name = "AFFYMETRIX_TASK_WAIT_MANAGE")
@SuppressWarnings("serial")
public class AffymetrixTaskWaitManage extends EntityDao<AffymetrixTaskWaitManage> implements java.io.Serializable {
	/** 编号*/
	private String id;
	/** 描述*/
	private String name;
	/** 体积*/
	private Double volume;
	/** 备注*/
	private String note;
	/** 结果*/
	private String result;
	/** 下一步流向*/
	private String nextFlow;
	/** 样本编号*/
	private String sampleCode;
	/** 病人姓名*/
	private String patientName;
	/** 检测项目*/
	private String productId;
	/** 检测项目*/
	private String productName;
	/** 检测方法*/
	private String sequenceFun;
	/** 取样时间*/
	private String inspectDate;
	/** 身份证号*/
	private String idCard;
	/** 应出报告日期*/
	private String reportDate;
	/** 手机号*/
	private String phone;
	/** 任务单*/
	private String orderId;
	/** experiment.affymetrix编号*/
	private String code;
	/** 区分临床还是科技服务      0   临床   1   科技服务*/
	private String classify;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 *方法: 取得Double
	 *@return: Double  体积
	 */
	@Column(name ="VOLUME", length = 100)
	public Double getVolume(){
		return this.volume;
	}
	
	/**
	 *方法: 设置Double
	 *@param: Double  体积
	 */
	public void setVolume(Double volume){
		this.volume = volume;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getSampleCode() {
		return sampleCode;
	}
	
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	
	public String getPatientName() {
		return patientName;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getSequenceFun() {
		return sequenceFun;
	}
	
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	
	public String getInspectDate() {
		return inspectDate;
	}
	
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	
	public String getIdCard() {
		return idCard;
	}
	
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	public String getReportDate() {
		return reportDate;
	}
	
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public String getClassify() {
		return classify;
	}
	
	public void setClassify(String classify) {
		this.classify = classify;
	}
}