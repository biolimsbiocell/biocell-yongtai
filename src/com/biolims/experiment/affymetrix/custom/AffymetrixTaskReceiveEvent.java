package com.biolims.experiment.affymetrix.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.affymetrix.service.AffymetrixTaskReceiveService;

public class AffymetrixTaskReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		AffymetrixTaskReceiveService mbService = (AffymetrixTaskReceiveService) ctx
				.getBean("affymetrixTaskReceiveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
