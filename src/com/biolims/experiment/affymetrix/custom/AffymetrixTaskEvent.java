package com.biolims.experiment.affymetrix.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.affymetrix.service.AffymetrixTaskService;

public class AffymetrixTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		AffymetrixTaskService mbService = (AffymetrixTaskService) ctx
				.getBean("affymetrixTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
