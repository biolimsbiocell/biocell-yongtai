package com.biolims.experiment.affymetrix.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.affymetrix.dao.AffymetrixTaskReceiveDao;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveTemp;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskTemp;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceive;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveItem;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AffymetrixTaskReceiveService {
	@Resource
	private AffymetrixTaskReceiveDao affymetrixTaskReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findAffymetrixTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return affymetrixTaskReceiveDao.selectAffymetrixTaskReceiveList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AffymetrixTaskReceive i) throws Exception {
		affymetrixTaskReceiveDao.saveOrUpdate(i);
	}

	public AffymetrixTaskReceive get(String id) {
		AffymetrixTaskReceive affymetrixTaskReceive = commonDAO.get(AffymetrixTaskReceive.class, id);
		return affymetrixTaskReceive;
	}

	public Map<String, Object> findAffymetrixTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = affymetrixTaskReceiveDao
				.selectAffymetrixTaskReceiveItemList(scId, startNum, limitNum,
						dir, sort);
		List<AffymetrixTaskReceiveItem> list = (List<AffymetrixTaskReceiveItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAffymetrixTaskReceiveItem(AffymetrixTaskReceive sc, String itemDataJson)
			throws Exception {
		List<AffymetrixTaskReceiveItem> saveItems = new ArrayList<AffymetrixTaskReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AffymetrixTaskReceiveItem scp = new AffymetrixTaskReceiveItem();
			// 将map信息读入实体类
			scp = (AffymetrixTaskReceiveItem) affymetrixTaskReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAffymetrixTaskReceive(sc);
			saveItems.add(scp);
		}
		affymetrixTaskReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAffymetrixTaskReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			AffymetrixTaskReceiveItem scp = affymetrixTaskReceiveDao.get(
					AffymetrixTaskReceiveItem.class, id);
			affymetrixTaskReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AffymetrixTaskReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			affymetrixTaskReceiveDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("affymetrixTaskReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAffymetrixTaskReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		AffymetrixTaskReceive sct = affymetrixTaskReceiveDao.get(AffymetrixTaskReceive.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		this.affymetrixTaskReceiveDao.update(sct);
		try {
			List<AffymetrixTaskReceiveItem> edri = this.affymetrixTaskReceiveDao
					.selectReceiveByIdList(sct.getId());
			for (AffymetrixTaskReceiveItem ed : edri) {
				if (ed.getMethod().equals("1")) {
					AffymetrixTaskTemp dst = new AffymetrixTaskTemp();
					dst.setConcentration(ed.getConcentration());
					dst.setLocation(ed.getLocation());
					dst.setCode(ed.getCode());
					dst.setId(ed.getId());
					dst.setSequenceFun(ed.getSequenceFun());
					dst.setPatientName(ed.getPatientName());
					dst.setProductName(ed.getProductName());
					dst.setProductId(ed.getProductId());
					dst.setIdCard(ed.getIdCard());
					dst.setInspectDate(ed.getInspectDate());
					dst.setPhone(ed.getPhone());
					dst.setOrderId(ed.getOrderId());
					dst.setReportDate(ed.getReportDate());
					dst.setState("1");
					dst.setClassify(ed.getClassify());
					dst.setCode(ed.getCode());
					dst.setSampleType(ed.getSampleType());
					dst.setSampleNum(ed.getSampleNum());
					dst.setLabCode(ed.getLabCode());
					affymetrixTaskReceiveDao.saveOrUpdate(dst);
					AffymetrixTaskReceiveTemp temp = commonDAO.get(AffymetrixTaskReceiveTemp.class,
							ed.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}
				// 审批完成改变Info中的样本状态为“待Affymetrix实验Affymetrix实验”
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(ed
						.getCode());
				if (sf != null) {
					sf.setState("3");
					sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
				}
				ed.setState("1");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 审批完成之后，将明细表数据保存到AffymetrixTaskAffymetrix实验中间表
	 * @param code
	 */
	public void saveToAffymetrixTaskTemp(String code) {
		List<AffymetrixTaskReceiveItem> list = affymetrixTaskReceiveDao
				.selectReceiveItemListById(code);
		if (list.size() > 0) {
			for (AffymetrixTaskReceiveItem ed : list) {
				AffymetrixTaskTemp dst = new AffymetrixTaskTemp();
				dst.setConcentration(ed.getConcentration());
				dst.setLocation(ed.getLocation());
				dst.setCode(ed.getCode());
				dst.setId(ed.getId());
				dst.setSequenceFun(ed.getSequenceFun());
				dst.setPatientName(ed.getPatientName());
				dst.setProductName(ed.getProductName());
				dst.setProductId(ed.getProductId());
				dst.setIdCard(ed.getIdCard());
				dst.setInspectDate(ed.getInspectDate());
				dst.setPhone(ed.getPhone());
				dst.setOrderId(ed.getOrderId());
				dst.setReportDate(ed.getReportDate());
				dst.setState("1");
				dst.setClassify(ed.getClassify());
				dst.setSampleCode(ed.getSampleCode());
				dst.setSampleType(ed.getSampleType());
				dst.setSampleNum(ed.getSampleNum());
				affymetrixTaskReceiveDao.saveOrUpdate(dst);
			}
		}
	}
		public Map<String, Object> selectAffymetrixTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = affymetrixTaskReceiveDao.selectAffymetrixTaskReceiveTempList(
				startNum, limitNum, dir, sort);
		List<AffymetrixTaskReceiveTemp> list = (List<AffymetrixTaskReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
	
}
