﻿package com.biolims.experiment.affymetrix.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskItem;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskInfo;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskWaitManage;
import com.biolims.experiment.affymetrix.service.AffymetrixTaskManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/affymetrix/affymetrixTaskManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AffymetrixTaskManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240205";
	@Autowired
	private AffymetrixTaskManageService affymetrixTaskManageService;
	private AffymetrixTaskWaitManage affymetrixTaskManage = new AffymetrixTaskWaitManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showAffymetrixTaskManageEditList")
	public String showAffymetrixTaskManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskManageEdit.jsp");
	}

	@Action(value = "showAffymetrixTaskManageList")
	public String showAffymetrixTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskManage.jsp");
	}

	@Action(value = "showAffymetrixTaskManageListJson")
	public void showAffymetrixTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskManageService
				.findAffymetrixTaskManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTaskInfo> list = (List<AffymetrixTaskInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");
	
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("sumVolume", "");
		map.put("rin", "");
		map.put("contraction", "");
		map.put("od260", "");
		map.put("od280", "");
		map.put("tempId", "");
		map.put("isToProject", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("state", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("affymetrixTask-name", "");
		map.put("affymetrixTask-id", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("orderType", "");
		map.put("taskId", "");
		map.put("classify", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleType", "");
		map.put("labCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "affymetrixTaskManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAffymetrixTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskManageDialog.jsp");
	}

	@Action(value = "showDialogAffymetrixTaskManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAffymetrixTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskManageService
				.findAffymetrixTaskManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTaskWaitManage> list = (List<AffymetrixTaskWaitManage>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("method", "");
		map.put("nextFlow", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editAffymetrixTaskManage")
	public String editAffymetrixTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			affymetrixTaskManage = affymetrixTaskManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "affymetrixTaskManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskManageEdit.jsp");
	}

	@Action(value = "copyAffymetrixTaskManage")
	public String copyAffymetrixTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		affymetrixTaskManage = affymetrixTaskManageService.get(id);
		affymetrixTaskManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = affymetrixTaskManage.getId();
		if (id != null && id.equals("")) {
			affymetrixTaskManage.setId(null);
		}
		Map aMap = new HashMap();
		affymetrixTaskManageService.save(affymetrixTaskManage, aMap);
		return redirect("/experiment/affymetrix/affymetrixTaskManage/editAffymetrixTaskManage.action?id="
				+ affymetrixTaskManage.getId());

	}

	@Action(value = "viewAffymetrixTaskManage")
	public String toViewAffymetrixTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		affymetrixTaskManage = affymetrixTaskManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskManageEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AffymetrixTaskManageService getAffymetrixTaskManageService() {
		return affymetrixTaskManageService;
	}

	public void setAffymetrixTaskManageService(
			AffymetrixTaskManageService affymetrixTaskManageService) {
		this.affymetrixTaskManageService = affymetrixTaskManageService;
	}

	public AffymetrixTaskWaitManage getAffymetrixTaskManage() {
		return affymetrixTaskManage;
	}

	public void setAffymetrixTaskManage(AffymetrixTaskWaitManage affymetrixTaskManage) {
		this.affymetrixTaskManage = affymetrixTaskManage;
	}

	/**
	 * 保存中间产物
	 * @throws Exception
	 */
	@Action(value = "saveAffymetrixTaskManage")
	public void saveAffymetrixTaskAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			affymetrixTaskManageService
					.saveAffymetrixTaskInfoManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 保存样本
	 * @throws Exception
	 */
	@Action(value = "saveAffymetrixTaskItemManager")
	public void saveAffymetrixTaskItemManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			affymetrixTaskManageService.saveAffymetrixTaskItemManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showAffymetrixTaskItemManagerList")
	public String showAffymetrixTaskItemManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskItemManage.jsp");
	}

	@Action(value = "showAffymetrixTaskItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskItemManagerListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// 排序方式
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = affymetrixTaskManageService
					.findAffymetrixTaskItemList(map2Query, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskItem> list = (List<AffymetrixTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
		
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlow", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleConsume", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 样本管理明细入库
	 * @throws Exception
	 */
	@Action(value = "AffymetrixTaskManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			affymetrixTaskManageService.AffymetrixTaskManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 样本管理明细Affymetrix实验待办
	 * @throws Exception
	 */
	@Action(value = "AffymetrixTaskManageItemTiqu")
	public void AffymetrixTaskManageItemTiqu() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			affymetrixTaskManageService.AffymetrixTaskManageItemTiqu(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
