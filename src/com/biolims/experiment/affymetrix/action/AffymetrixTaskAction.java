﻿package com.biolims.experiment.affymetrix.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.affymetrix.dao.AffymetrixTaskDao;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveItem;
import com.biolims.experiment.affymetrix.model.AffymetrixTask;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskCos;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskItem;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReagent;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskTemp;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskTemplate;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskInfo;
import com.biolims.experiment.affymetrix.service.AffymetrixTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/affymetrix/affymetrixTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AffymetrixTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240202";
	@Autowired
	private AffymetrixTaskService affymetrixTaskService;
	private AffymetrixTask affymetrixTask = new AffymetrixTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private AffymetrixTaskDao affymetrixTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showAffymetrixTaskList")
	public String showAffymetrixTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTask.jsp");
	}

	@Action(value = "showAffymetrixTaskListJson")
	public void showAffymetrixTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskService
				.findAffymetrixTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTask> list = (List<AffymetrixTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "affymetrixTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAffymetrixTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskDialog.jsp");
	}

	@Action(value = "showDialogAffymetrixTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAffymetrixTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskService
				.findAffymetrixTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTask> list = (List<AffymetrixTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editAffymetrixTask")
	public String editAffymetrixTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			affymetrixTask = affymetrixTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "affymetrixTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			affymetrixTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			affymetrixTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			affymetrixTask.setCreateDate(stime);
			affymetrixTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			affymetrixTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(affymetrixTask.getState());
		putObjToContext("maxNum", maxNum);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskEdit.jsp");
	}

	@Action(value = "copyAffymetrixTask")
	public String copyAffymetrixTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		affymetrixTask = affymetrixTaskService.get(id);
		affymetrixTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = affymetrixTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "AffymetrixTask";
			String markCode = "HSTQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			affymetrixTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("affymetrixTaskItem",
				getParameterFromRequest("affymetrixTaskItemJson"));
		aMap.put("affymetrixTaskResult",
				getParameterFromRequest("affymetrixTaskResultJson"));
		aMap.put("affymetrixTaskTemplateItem",
				getParameterFromRequest("affymetrixTaskTemplateItemJson"));
		aMap.put("affymetrixTaskTemplateReagent",
				getParameterFromRequest("affymetrixTaskTemplateReagentJson"));
		aMap.put("affymetrixTaskTemplateCos",
				getParameterFromRequest("affymetrixTaskTemplateCosJson"));
		affymetrixTaskService.save(affymetrixTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/affymetrix/affymetrixTask/editAffymetrixTask.action?id="
				+ affymetrixTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewAffymetrixTask")
	public String toViewAffymetrixTask() throws Exception {
		String id = getParameterFromRequest("id");
		affymetrixTask = affymetrixTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskEdit.jsp");
	}

	/**
	 * 查询左侧中间表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showAffymetrixTaskTempList")
	public String showAffymetrixTaskTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskTemp.jsp");
	}

	@Action(value = "showAffymetrixTaskTempJson")
	public void showAffymetrixTaskTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskService
				.findAffymetrixTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTaskTemp> list = (List<AffymetrixTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleId", "");
		map.put("sampleName", "");
		map.put("location", "");
		map.put("patientName", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("sampleNum", "");
		map.put("labCode", "");
		map.put("sampleInfo-note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showAffymetrixTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskItem.jsp");
	}

	@Action(value = "showAffymetrixTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = affymetrixTaskService
					.findAffymetrixTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskItem> list = (List<AffymetrixTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");

			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("affymetrixTask-name", "");
			map.put("affymetrixTask-id", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAffymetrixTaskItem")
	public void delAffymetrixTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			if (ids != null) {
				affymetrixTaskService.delAffymetrixTaskItem(ids);
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showAffymetrixTaskInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskInfo.jsp");
	}

	@Action(value = "showAffymetrixTaskInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = affymetrixTaskService
					.findAffymetrixTaskInfoList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskInfo> list = (List<AffymetrixTaskInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("sumVolume", "");
			map.put("rin", "");
			map.put("contraction", "");
			map.put("od260", "");
			map.put("od280", "");
			map.put("tempId", "");
			map.put("isToProject", "");
			map.put("volume", "");
			map.put("note", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("affymetrixTask-name", "");
			map.put("affymetrixTask-id", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("dataBits", "");
			map.put("qbcontraction", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAffymetrixTaskInfo")
	public void delAffymetrixTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			affymetrixTaskService.delAffymetrixTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AffymetrixTaskService getAffymetrixTaskService() {
		return affymetrixTaskService;
	}

	public void setAffymetrixTaskService(
			AffymetrixTaskService affymetrixTaskService) {
		this.affymetrixTaskService = affymetrixTaskService;
	}

	public AffymetrixTask getAffymetrixTask() {
		return affymetrixTask;
	}

	public void setAffymetrixTask(AffymetrixTask affymetrixTask) {
		this.affymetrixTask = affymetrixTask;
	}

	/**
	 * 模板
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskTemplateItem.jsp");
	}

	@Action(value = "showTemplateItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = affymetrixTaskService
					.findTemplateItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskTemplate> list = (List<AffymetrixTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUserId", "");
			map.put("testUserName", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("affymetrixTask-name", "");
			map.put("affymetrixTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItem")
	public void delTemplateItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			affymetrixTaskService.delTemplateItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItemOne")
	public void delTemplateItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			affymetrixTaskService.delTemplateItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showAffymetrixTaskTemplateReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskTemplateReagentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskTemplateReagent.jsp");
	}

	@Action(value = "showAffymetrixTaskTemplateReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskTemplateReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = affymetrixTaskService.findReagentItemList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskReagent> list = (List<AffymetrixTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("affymetrixTask-name", "");
			map.put("affymetrixTask-id", "");
			map.put("sn", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("reagentCode", "");
			map.put("isRunout", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAffymetrixTaskTemplateReagent")
	public void delAffymetrixTaskTemplateReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			affymetrixTaskService.delReagentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateReagentOne")
	public void delTemplateReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			affymetrixTaskService.delTemplateReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showAffymetrixTaskTemplateCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskTemplateCosList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskTemplateCos.jsp");
	}

	@Action(value = "showAffymetrixTaskTemplateCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskTemplateCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = affymetrixTaskService.findCosItemList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskCos> list = (List<AffymetrixTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("state", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("affymetrixTask-name", "");
			map.put("affymetrixTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAffymetrixTaskTemplateCos")
	public void delAffymetrixTaskTemplateCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			affymetrixTaskService.delCosItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateCosOne")
	public void delAffymetrixTaskTemplateCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			affymetrixTaskService.delAffymetrixTaskTemplateCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 获取从样本接收来的样本
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showAffymetrixTaskFromReceiveList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskFromReceiveList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskFromReceiveLeft.jsp");
	}

	@Action(value = "showAffymetrixTaskFromReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskFromReceiveListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = affymetrixTaskService
					.selectAffymetrixTaskFromReceiveList(scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskReceiveItem> list = (List<AffymetrixTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("affymetrixTaskReceive-id", "");
			map.put("affymetrixTaskReceive-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 判断样本做Affymetrix实验的次数
	 * 
	 * @throws Exception
	 */
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = affymetrixTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 模板
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = affymetrixTaskService
					.findTemplateItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskTemplate> list = (List<AffymetrixTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("affymetrixTask-name", "");
			map.put("affymetrixTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据选中的步骤异步加载相关的试剂数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.affymetrixTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据选中的步骤异步加载相关的设备数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.affymetrixTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 提交样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "submit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submit() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.affymetrixTaskService.submit(id, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
