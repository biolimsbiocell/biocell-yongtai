﻿package com.biolims.experiment.affymetrix.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.affymetrix.dao.AffymetrixTaskReceiveDao;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceive;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveTemp;
import com.biolims.experiment.affymetrix.model.AffymetrixTaskReceiveItem;
import com.biolims.experiment.affymetrix.service.AffymetrixTaskReceiveService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/experiment/affymetrix/affymetrixTaskReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AffymetrixTaskReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240206";
	@Autowired
	private AffymetrixTaskReceiveService affymetrixTaskReceiveService;
	private AffymetrixTaskReceive affymetrixTaskReceive = new AffymetrixTaskReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showAffymetrixTaskReceiveList")
	public String showAffymetrixTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceive.jsp");
	}

	@Action(value = "showAffymetrixTaskReceiveListJson")
	public void showAffymetrixTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskReceiveService
				.findAffymetrixTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTaskReceive> list = (List<AffymetrixTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "affymetrixTaskReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAffymetrixTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceiveDialog.jsp");
	}

	@Action(value = "showDialogAffymetrixTaskReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAffymetrixTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = affymetrixTaskReceiveService
				.findAffymetrixTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<AffymetrixTaskReceive> list = (List<AffymetrixTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("experiment.affymetrixCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editAffymetrixTaskReceive")
	public String editAffymetrixTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			affymetrixTaskReceive = affymetrixTaskReceiveService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "affymetrixTaskReceive");
		} else {
			affymetrixTaskReceive.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			affymetrixTaskReceive.setReceiveUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			affymetrixTaskReceive.setReceiverDate(stime);
			affymetrixTaskReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			affymetrixTaskReceive
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceiveEdit.jsp");
	}

	@Action(value = "copyAffymetrixTaskReceive")
	public String copyAffymetrixTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		affymetrixTaskReceive = affymetrixTaskReceiveService.get(id);
		affymetrixTaskReceive.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceiveEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = affymetrixTaskReceive.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "AffymetrixTaskReceive";
			String markCode = "HSJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			affymetrixTaskReceive.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("affymetrixTaskReceiveItem",
				getParameterFromRequest("affymetrixTaskReceiveItemJson"));
		affymetrixTaskReceiveService.save(affymetrixTaskReceive, aMap);
		return redirect("/experiment/affymetrix/affymetrixTaskReceive/editAffymetrixTaskReceive.action?id="
				+ affymetrixTaskReceive.getId());

	}

	@Action(value = "viewAffymetrixTaskReceive")
	public String toViewAffymetrixTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		affymetrixTaskReceive = affymetrixTaskReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceiveEdit.jsp");
	}

	@Action(value = "showAffymetrixTaskReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceiveItem.jsp");
	}

	@Action(value = "showAffymetrixTaskReceiveItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskReceiveItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = affymetrixTaskReceiveService
					.findAffymetrixTaskReceiveItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskReceiveItem> list = (List<AffymetrixTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleId", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("affymetrixTaskReceive-id", "");
			map.put("affymetrixTaskReceive-name", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAffymetrixTaskReceiveItem")
	public void delAffymetrixTaskReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			affymetrixTaskReceiveService.delAffymetrixTaskReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
 	/**
	* 去待接收的样本
	* @throws Exception
	 */
	@Action(value = "showAffymetrixTaskReceiveItemListTo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAffymetrixTaskReceiveItemListTo() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/affymetrix/affymetrixTaskReceiveItemTo.jsp");
	}

	@Action(value = "showAffymetrixTaskReceiveItemListToJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAffymetrixTaskReceiveItemListToJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, Object> result = affymetrixTaskReceiveService
					.selectAffymetrixTaskReceiveTempList(startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<AffymetrixTaskReceiveTemp> list = (List<AffymetrixTaskReceiveTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("inspectDate", "");
			map.put("sampleType", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("reportDate", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("classify", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AffymetrixTaskReceiveService getAffymetrixTaskReceiveService() {
		return affymetrixTaskReceiveService;
	}

	public void setAffymetrixTaskReceiveService(
			AffymetrixTaskReceiveService affymetrixTaskReceiveService) {
		this.affymetrixTaskReceiveService = affymetrixTaskReceiveService;
	}

	public AffymetrixTaskReceive getAffymetrixTaskReceive() {
		return affymetrixTaskReceive;
	}

	public void setAffymetrixTaskReceive(AffymetrixTaskReceive affymetrixTaskReceive) {
		this.affymetrixTaskReceive = affymetrixTaskReceive;
	}
}
