package com.biolims.experiment.reinfusionPlan.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.cxf.binding.soap.Soap11;
import org.apache.struts2.ServletActionContext;
import org.apache.xmlbeans.impl.xb.xsdschema.TotalDigitsDocument.TotalDigits;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.changeplan.model.ChangePlan;
import com.biolims.experiment.reinfusionPlan.dao.ReinfusionPlanDao;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlan;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleStateService;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.tra.transport.model.TransportApplyTemp;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本出库申请
 * 
 * @author admin
 * 
 */
@Service
@SuppressWarnings("unchecked")
@Transactional
public class ReinfusionPlanService {
	@Resource
	private ReinfusionPlanDao reinfusionPlanDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findReinfusionPlanList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return reinfusionPlanDao.selectReinfusionPlanList(start, length, query, col, sort);
	}

	public ReinfusionPlan findReinfusionPlanById(String id) {
		return reinfusionPlanDao.get(ReinfusionPlan.class, id);
	}

	public Map<String, Object> selReinfusionPlanTempTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return reinfusionPlanDao.findReinfusionPlanTempTable(start, length, query, col, sort);
	}

	public Map<String, Object> selReinfusionPlanItemTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return reinfusionPlanDao.selReinfusionPlanItemTable(scId, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReinfusionPlan saveSampleOutApplyById(ReinfusionPlan soa, String id, String reinfusionPlanDate, String note,
			String logInfo) throws Exception {
		
		ReinfusionPlan newSoa = commonDAO.get(ReinfusionPlan.class, id);
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSoa.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		
		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
			newSoa = new ReinfusionPlan();
			String code = systemCodeService.getCodeByPrefix("ReinfusionPlan",
					"RP" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
			newSoa.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (reinfusionPlanDate != null && !"".equals(reinfusionPlanDate)) {
				newSoa.setReinfusionPlanDate(sdf.parse(reinfusionPlanDate));
			}
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			newSoa.setCreateUser(user);
			newSoa.setCreateDate(new Date());
			newSoa.setState("3");
			newSoa.setStateName("新建");
			newSoa.setName(note);
			newSoa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			newSoa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			id = soa.getId();
			commonDAO.saveOrUpdate(newSoa);
		} else {
			newSoa = commonDAO.get(ReinfusionPlan.class, id);
		}

		
		newSoa.setName(note);
		if (reinfusionPlanDate != null && !"".equals(reinfusionPlanDate)) {
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			Date da = sdf1.parse(reinfusionPlanDate);
			newSoa.setReinfusionPlanDate(da);
		}
		commonDAO.saveOrUpdate(newSoa);
		return newSoa;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReinfusionPlanAndItem(ReinfusionPlan newSoa, Map jsonMap, String logInfo, String logInfoItem)
			throws Exception {
		if (newSoa != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(newSoa.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("ImteJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveItem(newSoa, jsonStr, logInfoItem);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveItem(ReinfusionPlan newSoa, String itemDataJson, String logInfo) throws Exception {
		List<ReinfusionPlanItem> saveItems = new ArrayList<ReinfusionPlanItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ReinfusionPlanItem scp = new ReinfusionPlanItem();
			// 将map信息读入实体类
			scp = (ReinfusionPlanItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setReinfusionPlan(newSoa);

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSoa.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	// 主数据内选择数据添加到入库明细表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addReinfusionPlanItem(String[] ids, String note, String reinfusionPlanDate, String id,
			String createUser, String createDate) throws Exception {

		// 保存主表信息
		ReinfusionPlan soa = new ReinfusionPlan();
		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
			soa = new ReinfusionPlan();
			String code = systemCodeService.getCodeByPrefix("ReinfusionPlan",
					"RP" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
			soa.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (reinfusionPlanDate != null && !"".equals(reinfusionPlanDate)) {
				soa.setReinfusionPlanDate(sdf.parse(reinfusionPlanDate));
			}
			User u = commonDAO.get(User.class, createUser);
			soa.setCreateUser(u);
			soa.setCreateDate(sdf.parse(createDate));
			soa.setState("3");
			soa.setStateName("新建");
			soa.setName(note);
			soa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			soa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			id = soa.getId();
			commonDAO.saveOrUpdate(soa);
		} else {
			soa = commonDAO.get(ReinfusionPlan.class, id);
		}
		for (int i = 0; i < ids.length; i++) {
			String idz = ids[i];
			// 通过id查询库存样本
			ReinfusionPlanTemp sii = commonDAO.get(ReinfusionPlanTemp.class, idz);
			ReinfusionPlanItem soai = new ReinfusionPlanItem();
			// 将数据放到出库申请明细表
			soai.setReinfusionPlan(soa);
			soai.setCode(sii.getCode());
			sii.setState("2");
			soai.setTempId(sii.getId());
			soai.setOrderCode(sii.getOrderCode());
			soai.setName(sii.getName());
			soai.setAge(sii.getAge());
			soai.setGender(sii.getGender());
			soai.setNation(sii.getNation());
			soai.setHospital(sii.getHospital());
			soai.setHospitalPhone(sii.getHospitalPhone());
			soai.setNote(sii.getNote());
			soai.setScopeId(sii.getScopeId());
			soai.setScopeName(sii.getScopeName());
			commonDAO.saveOrUpdate(soai);
			commonDAO.saveOrUpdate(sii);
		}
		return id;

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReinfusionPlanItem(String[] ids) throws Exception {
		for (String id : ids) {
			ReinfusionPlanItem scp = reinfusionPlanDao.get(ReinfusionPlanItem.class, id);
			reinfusionPlanDao.delete(scp);
			// 改变出库样本状态
			ReinfusionPlanTemp out = commonDAO.get(ReinfusionPlanTemp.class, scp.getTempId());
			out.setState("1");
			commonDAO.saveOrUpdate(out);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public ReinfusionPlan get(String id) {
		ReinfusionPlan rp = new ReinfusionPlan();
		rp = commonDAO.get(ReinfusionPlan.class, id);
		return rp;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) {
		ReinfusionPlan rp = new ReinfusionPlan();
		rp = commonDAO.get(ReinfusionPlan.class, id);
		rp.setState("1");
		rp.setStateName("完成");
		commonDAO.saveOrUpdate(rp);
		List<ReinfusionPlanItem> list = new ArrayList<ReinfusionPlanItem>();
		list = reinfusionPlanDao.showReinfusionPlanItemList(id);
		if (!list.isEmpty()) {
			for (ReinfusionPlanItem rpi : list) {
				SampleOrder so = new SampleOrder();
				so = commonDAO.get(SampleOrder.class, rpi.getOrderCode());
				TransportApplyTemp tat = new TransportApplyTemp();
				if (so != null) {
					tat.setSampleOrder(so);
					tat.setProductId(so.getProductId());
					tat.setProductName(so.getProductName());
					tat.setPatientName(so.getName());
					tat.setOrderCode(so.getId());
				}
				tat.setReinfusionPlanDate(rp.getReinfusionPlanDate());
				tat.setCode(rpi.getCode());
				tat.setState("1");
				tat.setPlanState("1");
				List<SampleInfo> siList = new ArrayList<SampleInfo>();
				siList = commonDAO.getSampleInfoFromOrderId(rpi.getOrderCode());
				if (!siList.isEmpty()) {
					tat.setSampleInfo(siList.get(0));
				}
				commonDAO.saveOrUpdate(tat);
			}
		}
	}

	public Map<String, Object> findReinfusionPlanItemList(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		return reinfusionPlanDao.findReinfusionPlanItemList(start, length, query, col, sort, type);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReinfusionPlanItem(ReinfusionPlanItem sr, String dataJson, String changeLogItem) throws Exception {
		List<ReinfusionPlanItem> saveItems = new ArrayList<ReinfusionPlanItem>();
		List<Map> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		for (Map map : list) {
			ReinfusionPlanItem sri = new ReinfusionPlanItem();
			// 将map信息读入实体类
			sri = (ReinfusionPlanItem) reinfusionPlanDao.Map2Bean(map, sri);
//			if(sri.getArc().equals("1")){
//				sri.setState("1");
//				sri.setSendDate(new Date());
//			}else{
//				sri.setState("2");
//			}
			reinfusionPlanDao.saveOrUpdate(sri);
			
			if (changeLogItem != null && !"".equals(changeLogItem)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sri.getBatch());
				li.setClassName("ReinfusionPlan");
				li.setModifyContent(changeLogItem);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}
		
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReinfusionPlanItemConfirm(String[] ids) throws Exception {
		if (ids.length > 0) {
			for (String id : ids) {
				if (id != null && !"".equals(id)) {
					ReinfusionPlanItem sri = commonDAO.get(ReinfusionPlanItem.class, id);
					if (sri != null) {
						if ("1".equals(sri.getFeedBack())) {

						} else {
							sri.setFeedBack("1");
							sri.setState("1");
							commonDAO.saveOrUpdate(sri);
						}
					}
					String hsqr = "回输计划：" + "ID为'" + id +"'且产品批号为" + sri.getBatch() + "的数据已确认";
					if (hsqr != null && !"".equals(hsqr)) {
						LogInfo li = new LogInfo();
						li.setLogDate(new Date());
						User u = (User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY);
						li.setUserId(u.getId());
						li.setFileId(sri.getBatch());
						li.setClassName("ReinfusionPlan");
						li.setModifyContent(hsqr);
						li.setState("3");
						li.setStateName("数据修改");
						commonDAO.saveOrUpdate(li);
					}
				}

			}
		} else {

		}
		// 还少往运输计划插记录的步骤
		if (ids.length > 0) {
			for (String id : ids) {
				if (id != null && !"".equals(id)) {
					ReinfusionPlanItem sri = commonDAO.get(ReinfusionPlanItem.class, id);
					if (sri != null) {

						// 查询运输计划左侧表(待选)是否有值
						List<TransportOrderTemp> tots = reinfusionPlanDao
								.showTransportOrderTempListBarCode(sri.getSampleOrder().getId());
						if (tots.size() > 0) {
							TransportOrderTemp tot = tots.get(0);
							tot.setState("1");
							tot.setCode(sri.getCode());
//							tot.setReinfusionPlanDate(sri.getModificationTime());
							tot.setReinfusionPlanDate(sri.getReinfusionPlanDate());
							commonDAO.saveOrUpdate(tot);
							// 如果有值,查询明细表是否有值
							List<TransportOrderCell> tocs = commonService.get(TransportOrderCell.class, "code",
									sri.getBatch());
							if (tocs.size() > 0) {
								for (TransportOrderCell toc : tocs) {
									toc.setEfficacious("无效");
									commonDAO.saveOrUpdate(toc);
								}
							}
//							if (sri.getBatch().equals(so.getBarcode())) {
//								//有数据
//								//明细表是否有数据,若有数据置为无效
//								TransportOrderCell toc = new TransportOrderCell();
//								if(toc.getCode()!=null) {
//									//置为无效
//									toc.setEfficacious("无效");
//								}else {
//									
//								}
//							} else {
//								
//							}
						} else {
							TransportOrderTemp tot = new TransportOrderTemp();

							/** 编码 */
							tot.setId(null);
							/** 原始样本编号 */
							tot.setSampleCode(sri.getBatch());
							/** 样本编号 */
//							tot.setCode(sri.getBatch());
							tot.setCode(sri.getCode());
							/** 患者姓名 */
							tot.setPatientName(sri.getName());
							/** 备注 */
							tot.setNote(sri.getNote());
							/** 状态ID */
							tot.setState("1");
							/** 状态 */
							tot.setStateName("显示");
							/** 预计回输日期 */
							tot.setReinfusionPlanDate(sri.getReinfusionPlanDate());
							if (sri.getSampleOrder() != null) {
								SampleOrder so = commonDAO.get(SampleOrder.class, sri.getSampleOrder().getId());
								/** 检测项目 */
								tot.setProductId(so.getProductId());
								/** 检测项目 */
								tot.setProductName(so.getProductName());
								/** 订单编号 */
								tot.setOrderCode(so.getId());
								/** 关联订单 */
								tot.setSampleOrder(so);

							} else if (sri.getBatch() != null && !"".equals(sri.getBatch())) {
								List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", sri.getBatch());
								if (sos.size() > 0) {
									SampleOrder so = sos.get(0);
									if (so != null) {
										/** 检测项目 */
										tot.setProductId(so.getProductId());
										/** 检测项目 */
										tot.setProductName(so.getProductName());
										/** 订单编号 */
										tot.setOrderCode(so.getId());
										/** 关联订单 */
										tot.setSampleOrder(so);
									}
								}
							}
							commonDAO.saveOrUpdate(tot);
						}

					}
				}

			}
		} else {

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveReinfusionPlanItemChangePlan(String dataJson, String changeLog) throws Exception {
		String autoID="";
		if (dataJson != null && !"".equals(dataJson)) {
			ReinfusionPlanItem rpi = commonDAO.get(ReinfusionPlanItem.class, dataJson);
			if (rpi != null) {
				ChangePlan cp = new ChangePlan();
				String modelName = "ChangePlan";
				String markCode = "CPP";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);

				cp.setId(autoID);
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				cp.setCreateUser(u);
				cp.setCreateDate(new Date());
				cp.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				cp.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				cp.setChangeTaskId(rpi.getId());
				cp.setBatch(rpi.getBatch());
				cp.setChangeContent(changeLog);
				cp.setFiltrateCode(rpi.getFiltrateCode());
				DicType dt = reinfusionPlanDao.getDicByMainTypeAndCode("changeType", "ReinfusionPlanItem");
				if (dt != null) {
					cp.setChangeType(dt);
				}

				cp.setReinfusionPlanDateUpdate(rpi.getReinfusionPlanDate());

				commonDAO.saveOrUpdate(cp);
			}
			String hsbg = "回输计划：" + "ID为'" + dataJson +"'且产品批号为" + rpi.getBatch() + "的数据已变更";
			if (hsbg != null && !"".equals(hsbg)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(rpi.getBatch());
				li.setClassName("ReinfusionPlan");
				li.setModifyContent(hsbg);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}
		return autoID;
		
	}
	//查看质检结果
	public Map<String, Object> showQualityTestTableJsonLI(Integer start, Integer length, String query, String col,
			String sort, String batch) {
		return reinfusionPlanDao.showQualityTestTableJsonLI(start, length, query, col, sort, batch);
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(SampleOutApply i) throws Exception {
	//
	// sampleOutApplyDao.saveOrUpdate(i);
	//
	// }
	//
	// public SampleOutApply get(String id) {
	// SampleOutApply sampleOutApply = commonDAO.get(SampleOutApply.class, id);
	// return sampleOutApply;
	// }
	//
	// public Map<String, Object> findSampleOutApplyItemList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// Map<String, Object> result = sampleOutApplyDao
	// .selectSampleOutApplyItemList(scId, startNum, limitNum, dir,
	// sort);
	// List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result
	// .get("list");
	// return result;
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSampleOutApplyItem(SampleOutApply sc, String itemDataJson)
	// throws Exception {
	// List<SampleOutApplyItem> saveItems = new ArrayList<SampleOutApplyItem>();
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
	// itemDataJson, List.class);
	// DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	// for (Map<String, Object> map : list) {
	// SampleOutApplyItem scp = new SampleOutApplyItem();
	// // 将map信息读入实体类
	// scp = (SampleOutApplyItem) sampleOutApplyDao.Map2Bean(map, scp);
	// if (scp.getId() != null && scp.getId().equals(""))
	// scp.setId(null);
	// scp.setSampleOutApply(sc);
	// // 改变出库样本状态
	// SampleInfoIn out = commonDAO.get(SampleInfoIn.class,
	// scp.getSampleInItemId());
	// out.setState("0");
	// commonDAO.saveOrUpdate(out);
	//
	// /*sampleStateService.saveSampleState1(scp.getSampleOrder(), scp.getCode(),
	// scp.getSampleCode(), null, null, "",
	// format.format(sc.getCreateDate()),
	// format.format(new Date()), "SampleOutApplyItem", "出库申请明细",
	// (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY),
	// sc.getId(), null, "", null, null, null, null, null, null,
	// null, null);*/
	// saveItems.add(scp);
	// }
	// sampleOutApplyDao.saveOrUpdateAll(saveItems);
	// }
	//
	// /**
	// * 删除明细
	// *
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delSampleOutApplyItem(String[] ids) throws Exception {
	// for (String id : ids) {
	// SampleOutApplyItem scp = sampleOutApplyDao.get(
	// SampleOutApplyItem.class, id);
	// sampleOutApplyDao.delete(scp);
	// // 改变出库样本状态
	// SampleInfoIn out = commonDAO.get(SampleInfoIn.class,
	// scp.getSampleInItemId());
	// out.setState("1");
	// commonDAO.saveOrUpdate(out);
	// }
	// }
	//
	// // 根据申请单加载子表
	// public Map<String, Object> showSampleOutItemList(String code)
	// throws Exception {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = sampleOutApplyDao
	// .setSampleOutItemList(code);
	// List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result
	// .get("list");
	//
	// // if (list != null && list.size() > 0) {
	// // for (SampleOutApplyItem srai : list) {
	// // Double num = srai.getNum();
	// // Map<String, String> map = new HashMap<String, String>();
	// // map.put("id", srai.getId());
	// // map.put("sampleCode", srai.getSampleCode());
	// // map.put("code", srai.getCode());
	// // map.put("sampleType", srai.getSampleType());
	// // map.put("location", srai.getLocation());// 得到储位
	// // if (num != null) {
	// // String numStr = num.toString();
	// // map.put("num", numStr);// 得到样本存储量
	// // }
	// // mapList.add(map);
	// // }
	// // }
	// return result;
	// }
	//
	// @WriteOperLogTable
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(SampleOutApply sc, Map jsonMap) throws Exception {
	// if (sc != null) {
	// sampleOutApplyDao.saveOrUpdate(sc);
	//
	// String jsonStr = "";
	// jsonStr = (String) jsonMap.get("sampleOutApplyItem");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveSampleOutApplyItem(sc, jsonStr);
	// }
	// }
	// }
	//
	// // 审批完成
	// public void changeState(String applicationTypeActionId, String id)
	// throws Exception {
	// SampleOutApply sr = sampleOutApplyDao.get(SampleOutApply.class, id);
	// sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
	// sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
	//
	// User user = (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY);
	// // sr.setConfirmUser(user);
	// // sr.setConfirmDate(new Date());
	// List<SampleOutApplyItem> soaiList = sampleOutApplyDao
	// .selectSampleItemById(sr.getId());
	// if (soaiList.size() > 0) {
	// for (SampleOutApplyItem soai : soaiList) {
	// SampleOutTemp sot = new SampleOutTemp();
	// sot.setStockType(soai.getStockType());
	// sot.setPronoun(soai.getPronoun());
	// sot.setCode(soai.getCode());
	// sot.setSampleType(soai.getSampleType());
	// sot.setSampleInItemId(soai.getSampleInItemId());
	// sot.setSampleCode(soai.getSampleCode());
	// sot.setLocation(soai.getLocation());
	// sot.setNum(soai.getNum());
	// sot.setConcentration(soai.getConcentration());
	// sot.setVolume(soai.getVolume());
	// sot.setSumTotal(soai.getSumTotal());
	// sot.setProductId(soai.getProductId());
	// sot.setProductName(soai.getProductName());
	// sot.setSamplingDate(soai.getSamplingDate());
	// sot.setBarCode(soai.getBarCode());
	// sot.setDataTraffic(soai.getDataTraffic());
	// sot.setState("1");
	// sot.setApplyUser(sr.getCreateUser());
	// sot.setCrmCustomer(soai.getSampleOutApply().getCustomer());
	//// sot.setType(sr.getType());
	// sot.setSupplier(sr.getSupplier());
	// sot.setTaskId(id);
	// sot.setPatientName(soai.getPatientName());
	// sot.setSampleStyle(soai.getSampleStyle());
	// sot.setInfoFrom("SampleOutApply");
	// sot.setTechJkServiceTask(soai.getTechJkServiceTask());
	// sot.setQpcrConcentration(soai.getQpcrConcentration());
	// sot.setIndexa(soai.getIndexa());
	// sot.setScopeId(soai.getScopeId());
	// sot.setScopeName(soai.getScopeName());
	// if(soai.getSampleOrder()!=null) {
	// sot.setSampleOrder(commonDAO.get(SampleOrder.class,
	// soai.getSampleOrder().getId()));
	// }
	// commonDAO.saveOrUpdate(sot);
	// // SampleInItem in = commonDAO.get(SampleInItem.class,
	// // soai.getSampleInItemId());
	// // in.setState("0");
	// // commonDAO.update(in);
	// if(soai.getSampleInItemId()!=null) {
	// SampleInfoIn out = commonDAO.get(SampleInfoIn.class,
	// soai.getSampleInItemId());
	// out.setState("0");
	// commonDAO.saveOrUpdate(out);
	// }
	//
	// DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// if(soai.getSampleOrder()!=null) {
	// soai.getSampleOrder().setNewTask(id);
	// }
	// sampleStateService
	// .saveSampleState1(
	// soai.getSampleOrder(),
	// soai.getCode(),
	// soai.getSampleCode(),
	// soai.getProductId(),
	// soai.getProductName(),
	// "",
	// sr.getCreateDate().toString(),
	// format.format(new Date()),
	// "CellPrimaryCulture",
	// "出库申请",
	// (User) ServletActionContext
	// .getRequest()
	// .getSession()
	// .getAttribute(
	// SystemConstants.USER_SESSION_KEY),
	// id, null, null, null,
	// null, null, null, null, null, null, null);
	// }
	// }
	// // 添加到待出库任务单
	// SampleOutTaskId soti = new SampleOutTaskId();
	// soti.setTaskId(id);
	// soti.setName("出库申请");
	// soti.setCreateUser(sr.getCreateUser());
	// soti.setCreateDate(new Date());
	// soti.setState("1");
	// soti.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
	//// soti.setType(sr.getType());
	// commonDAO.saveOrUpdate(soti);
	// sampleOutApplyDao.update(sr);
	// }
	//
	// public Map<String, Object> selSampleInfoInTable(Integer start,
	// Integer length, String query, String col, String sort) throws Exception {
	// return sampleOutApplyDao.findSampleInfoInTable(start, length, query, col,
	// sort);
	// }
	//
	// public Map<String, Object> selSampleOutApplyItemTable(String scId,
	// Integer start, Integer length, String query, String col, String sort) throws
	// Exception {
	// return sampleOutApplyDao.findSampleOutApplyItemTable(scId, start, length,
	// query,
	// col, sort);
	// }
	//
	// public Map<String, Object> selSampleOutApplyTable(Integer start,
	// Integer length, String query, String col, String sort) throws Exception {
	// return sampleOutApplyDao.findSampleOutApplyTable(start, length, query, col,
	// sort);
	// }
	//
	// public SampleOutApply findSampleOutApplyById(String outApplyId) {
	// return sampleOutApplyDao.get(SampleOutApply.class, outApplyId);
	// }
	//
	// // 主数据内选择数据添加到入库明细表
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public String addSampleOutApplyItem(String[] ids, String note, String type,
	// String storageOutApplyId, String createUser, String createDate) throws
	// Exception {
	//
	// // 保存主表信息
	// SampleOutApply soa = new SampleOutApply();
	// if (storageOutApplyId == null || storageOutApplyId.length() <= 0
	// || "NEW".equals(storageOutApplyId)) {
	// String code = systemCodeService.getCodeByPrefix("SampleOutApply", "MI"
	// + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
	// 000, 3, null);
	// soa.setId(code);
	// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	// User u = commonDAO.get(User.class, createUser);
	//
	// DicType d = commonDAO.get(DicType.class, type);
	// soa.setCreateUser(u);
	// soa.setExperimentType(d);
	// soa.setCreateDate(sdf.parse(createDate));
	// soa.setState("3");
	// soa.setStateName("NEW");
	// soa.setName(note);
	// soa.setScopeId((String)
	// ActionContext.getContext().getSession().get("scopeId"));
	// soa.setScopeName((String)
	// ActionContext.getContext().getSession().get("scopeName"));
	// storageOutApplyId = soa.getId();
	// commonDAO.saveOrUpdate(soa);
	// }else{
	// soa = commonDAO.get(SampleOutApply.class, storageOutApplyId);
	// }
	// for (int i = 0; i < ids.length; i++) {
	// String id = ids[i];
	// // 通过id查询库存样本
	// SampleInfoIn sii = commonDAO.get(SampleInfoIn.class, id);
	// SampleOutApplyItem soai = new SampleOutApplyItem();
	// //将数据放到出库申请明细表
	// soai.setSampleOutApply(soa);
	// soai.setCode(sii.getCode());
	// soai.setSampleCode(sii.getSampleCode());
	// soai.setSampleInItemId(sii.getId());
	// soai.setLocation(sii.getLocation());
	// soai.setBarCode(sii.getBarCode());
	// soai.setDataTraffic(sii.getDataTraffic());
	// soai.setProductId(sii.getProductId());
	// soai.setProductName(sii.getProductName());
	// soai.setSamplingDate(sii.getSamplingDate());
	// soai.setSampleType(sii.getSampleType());
	// soai.setConcentration(sii.getConcentration());
	// soai.setBarCode(sii.getBarCode());
	// soai.setVolume(sii.getVolume());
	// soai.setSumTotal(sii.getSumTotal());
	// soai.setNote(sii.getNote());
	// soai.setScopeId(sii.getScopeId());
	// soai.setScopeName(sii.getScopeName());
	// sii.setState("2");
	// soai.setStockType(sii.getStockType());
	// if(sii.getSampleOrder()!=null) {
	// soai.setSampleOrder(commonDAO.get(SampleOrder.class,
	// sii.getSampleOrder().getId()));
	// }
	// soai.setPronoun(sii.getPronoun());
	// commonDAO.saveOrUpdate(soai);
	// commonDAO.saveOrUpdate(sii);
	// }
	// return storageOutApplyId;
	//
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public SampleOutApply saveSampleOutApplyById(SampleOutApply soa, String id,
	// String type,String note, String logInfo) {
	// SampleOutApply newSoa = commonDAO.get(SampleOutApply.class, id);
	//
	// if (logInfo != null && !"".equals(logInfo)) {
	// LogInfo li = new LogInfo();
	// li.setLogDate(new Date());
	// User u = (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY);
	// li.setUserId(u.getId());
	// li.setFileId(newSoa.getId());
	// li.setModifyContent(logInfo);
	// commonDAO.saveOrUpdate(li);
	// }
	// newSoa.setName(note);
	// DicType d = commonDAO.get(DicType.class, type);
	// newSoa.setType(d);
	// newSoa.setExperimentType(d);
	// commonDAO.saveOrUpdate(newSoa);
	// return newSoa;
	// }
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSampleOutApplyAndItem(SampleOutApply newSoa, Map jsonMap,
	// String logInfo, String logInfoItem) throws Exception {
	// if (newSoa != null) {
	// if (logInfo != null && !"".equals(logInfo)) {
	// LogInfo li = new LogInfo();
	// li.setLogDate(new Date());
	// User u = (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY);
	// li.setUserId(u.getId());
	// li.setFileId(newSoa.getId());
	// li.setModifyContent(logInfo);
	// commonDAO.saveOrUpdate(li);
	// }
	//
	// String jsonStr = "";
	// jsonStr = (String) jsonMap.get("ImteJson");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveItem(newSoa, jsonStr, logInfoItem);
	// }
	// }
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// private void saveItem(SampleOutApply newSoa, String itemDataJson, String
	// logInfo) throws Exception {
	// List<SampleOutApplyItem> saveItems = new ArrayList<SampleOutApplyItem>();
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
	// itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// SampleOutApplyItem scp = new SampleOutApplyItem();
	// // 将map信息读入实体类
	// scp = (SampleOutApplyItem) commonDAO.Map2Bean(map, scp);
	// if (scp.getId() != null && scp.getId().equals(""))
	// scp.setId(null);
	// scp.setSampleOutApply(newSoa);
	//
	// saveItems.add(scp);
	// }
	// commonDAO.saveOrUpdateAll(saveItems);
	// if (logInfo != null && !"".equals(logInfo)) {
	// LogInfo li = new LogInfo();
	// li.setLogDate(new Date());
	// User u = (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY);
	// li.setUserId(u.getId());
	// li.setFileId(newSoa.getId());
	// li.setModifyContent(logInfo);
	// commonDAO.saveOrUpdate(li);
	// }
	// }
}
