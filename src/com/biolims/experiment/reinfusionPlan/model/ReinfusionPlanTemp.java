package com.biolims.experiment.reinfusionPlan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 回输计划待选表
 * @author lims-platform
 * @date 2015-11-03 16:19:13
 * @version V1.0
 * 
 */
@Entity
@Table(name = "REINFUSION_PLAN_TEMP")
@SuppressWarnings("serial")
public class ReinfusionPlanTemp extends EntityDao<ReinfusionPlanTemp> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	private String scopeId;
	private String scopeName;
	/**病人姓名*/
	private String name;
	/**年龄*/
	private String age;
	/**性别   男:1 女:0 空:未知*/
	private String gender;
	/**民族*/
	private String nation;
	/**产品ID*/
	private String productId;
	/**产品名称*/
	private String productName;
	/**医院*/
	private String hospital;
	/**医院的联系电话*/
	private String hospitalPhone;
	/**订单号*/
	private String orderCode;
	
	
	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getHospitalPhone() {
		return hospitalPhone;
	}

	public void setHospitalPhone(String hospitalPhone) {
		this.hospitalPhone = hospitalPhone;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 入库明细id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 入库明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}
	

}