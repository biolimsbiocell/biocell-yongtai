package com.biolims.experiment.reinfusionPlan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * @Title: Model
 * @Description: 出库申请明细
 * @author lims-platform
 * @date 2015-11-03 16:19:50
 * @version V1.0
 * 
 */
/**
 * @author Biolims
 *
 */
@Entity
@Table(name = "REINFUSION_PLAN_ITEM")
@SuppressWarnings("serial")
public class ReinfusionPlanItem extends EntityDao<ReinfusionPlanItem> implements
		java.io.Serializable {
	/** 申请明细id */
	private String id;
	/** 样本编号 */
	private String code;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private ReinfusionPlan reinfusionPlan;
	/** 确认结果 */
	private String result;
	/** 待选列表id */
	private String tempId;
	private String scopeId;
	private String scopeName;
	
	/**病人姓名*/
	private String name;
	/**年龄*/
	private String age;
	/**性别   男:1 女:0 空:未知*/
	private String gender;
	/**民族*/
	private String nation;
	/**产品ID*/
	private String productId;
	/**产品名称*/
	private String productName;
	/**医院*/
	private String hospital;
	/**医院的联系电话*/
	private String hospitalPhone;
	/**订单号*/
	private String orderCode;
	/**是否回输*/
	private String feedBack;
	/**能否延迟*/
	private String delay;
	/**批次号*/
	private String batch;
	
	/**计划回输日期*/
	private String reinfusionPlanDate;
	/**回输修改日期*/
	private String modificationTime;
	/**是否可修改回输日期*/
	private String diReinfusionPlanDate;
	/**筛选号*/
	private String filtrateCode;
	/**科室*/
	private String inspectionDepartment;
	/**医生*/
	private String attendingDoctor;
	/**医生电话*/
	private String attendingDoctorPhone;
	/**家属联系方式*/
	private String familyPhone;
	/**通知到医生*/
	private String doctorNotice;
	/**医生回复*/
	private String doctorReply;
	/**通知到患者*/
	private String patientNotice;
	/**患者回复*/
	private String patientReply;
	
	/**创建时间*/
	private String createDateTime;
	/** 订单实体 */
	private SampleOrder sampleOrder;
	
	
	
	
	

	

	public String getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(String modificationTime) {
		this.modificationTime = modificationTime;
	}

	public String getDiReinfusionPlanDate() {
		return diReinfusionPlanDate;
	}

	public void setDiReinfusionPlanDate(String diReinfusionPlanDate) {
		this.diReinfusionPlanDate = diReinfusionPlanDate;
	}

	public String getReinfusionPlanDate() {
		return reinfusionPlanDate;
	}

	public void setReinfusionPlanDate(String reinfusionPlanDate) {
		this.reinfusionPlanDate = reinfusionPlanDate;
	}

	public String getFiltrateCode() {
		return filtrateCode;
	}

	public void setFiltrateCode(String filtrateCode) {
		this.filtrateCode = filtrateCode;
	}

	public String getInspectionDepartment() {
		return inspectionDepartment;
	}

	public void setInspectionDepartment(String inspectionDepartment) {
		this.inspectionDepartment = inspectionDepartment;
	}

	public String getAttendingDoctor() {
		return attendingDoctor;
	}

	public void setAttendingDoctor(String attendingDoctor) {
		this.attendingDoctor = attendingDoctor;
	}

	public String getAttendingDoctorPhone() {
		return attendingDoctorPhone;
	}

	public void setAttendingDoctorPhone(String attendingDoctorPhone) {
		this.attendingDoctorPhone = attendingDoctorPhone;
	}

	public String getFamilyPhone() {
		return familyPhone;
	}

	public void setFamilyPhone(String familyPhone) {
		this.familyPhone = familyPhone;
	}

	public String getDoctorNotice() {
		return doctorNotice;
	}

	public void setDoctorNotice(String doctorNotice) {
		this.doctorNotice = doctorNotice;
	}

	public String getDoctorReply() {
		return doctorReply;
	}

	public void setDoctorReply(String doctorReply) {
		this.doctorReply = doctorReply;
	}

	public String getPatientNotice() {
		return patientNotice;
	}

	public void setPatientNotice(String patientNotice) {
		this.patientNotice = patientNotice;
	}

	public String getPatientReply() {
		return patientReply;
	}

	public void setPatientReply(String patientReply) {
		this.patientReply = patientReply;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return this.sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(String feedBack) {
		this.feedBack = feedBack;
	}

	public String getDelay() {
		return delay;
	}

	public void setDelay(String delay) {
		this.delay = delay;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getHospitalPhone() {
		return hospitalPhone;
	}

	public void setHospitalPhone(String hospitalPhone) {
		this.hospitalPhone = hospitalPhone;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
	

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 申请明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 申请明细id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "CODE", length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得SampleOutApply
	 * 
	 * @return: SampleOutApply 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REINFUSION_PLAN")
	public ReinfusionPlan getReinfusionPlan() {
		return this.reinfusionPlan;
	}

	/**
	 * 方法: 设置SampleOutApply
	 * 
	 * @param: SampleOutApply 相关主表
	 */
	public void setReinfusionPlan(ReinfusionPlan reinfusionPlan) {
		this.reinfusionPlan = reinfusionPlan;
	}

	public String getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}


	
}