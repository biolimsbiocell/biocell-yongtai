package com.biolims.experiment.reinfusionPlan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 回输计划
 * @author lims-platform
 * @date 2015-11-03 16:20:05
 * @version V1.0
 * 
 */
@Entity
@Table(name = "REINFUSION_PLAN")
@SuppressWarnings("serial")
public class ReinfusionPlan extends EntityDao<ReinfusionPlan> implements
		java.io.Serializable {
	/** 回输计划id */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 审核人 */
	private User acceptUser;
	/** 审核日期 */
	private Date acceptDate;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 回输计划日期 */
	private Date reinfusionPlanDate;
	private String scopeId;
	private String scopeName;
	
	
	
	public Date getReinfusionPlanDate() {
		return reinfusionPlanDate;
	}

	public void setReinfusionPlanDate(Date reinfusionPlanDate) {
		this.reinfusionPlanDate = reinfusionPlanDate;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本出库申请id
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本出库申请id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 100)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审核人
	 */
	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审核日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 审核日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}