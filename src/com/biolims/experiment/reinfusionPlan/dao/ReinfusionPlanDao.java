package com.biolims.experiment.reinfusionPlan.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlan;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanTemp;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.opensymphony.xwork2.ActionContext;

/**
 * 样本出库申请
 * 
 * @author admin
 * 
 */
@Repository
@SuppressWarnings("unchecked")
public class ReinfusionPlanDao extends BaseHibernateDao {
	public Map<String, Object> selectReinfusionPlanList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReinfusionPlan where 1=1  ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReinfusionPlan where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key += " order by " + col + " " + sort;
			}
			List<ReinfusionPlan> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findReinfusionPlanTempTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReinfusionPlanTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		// 集团的查询条件
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReinfusionPlanTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReinfusionPlanTemp> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selReinfusionPlanItemTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReinfusionPlanItem where 1=1 and reinfusionPlan.id='" + scId + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReinfusionPlanItem  where 1=1  and reinfusionPlan.id='" + scId + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ReinfusionPlanItem> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<ReinfusionPlanItem> showReinfusionPlanItemList(String id) {
		List<ReinfusionPlanItem> list = new ArrayList<ReinfusionPlanItem>();
		String hql = "from ReinfusionPlanItem where 1=1 and reinfusionPlan='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findReinfusionPlanItemList(Integer start, Integer length, String query, String col,
			String sort, String type) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ReinfusionPlanItem where 1=1  ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		} else {

		}

		if (type != null && !"".equals(type)) {
			if ("1".equals(type)) {
				key = key + " and state='1' ";
			} else if ("2".equals(type)) {
				key = key + " and state='2' ";
			}
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ReinfusionPlanItem where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.substring(0, col.indexOf("-"));
				}
				if ("id".equals(col)) {
					key += " order by reinfusionPlanDate desc ";
				} else {
					key += " order by " + col + " " + sort;
				}
			} else {
				key += " order by id desc ";
			}
			List<ReinfusionPlanItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public DicType getDicByMainTypeAndCode(String string, String string2) {
		DicType dt = new DicType();
		String hql = "from DicType where 1=1 and code='" + string2 + "' and type.id='" + string + "' ";
		dt = (DicType) this.getSession().createQuery(hql).uniqueResult();
		return dt;
	}

	public List<TransportOrderTemp> showTransportOrderTempListBarCode(String code) {
		List<TransportOrderTemp> list = new ArrayList<TransportOrderTemp>();
		String hql = "from TransportOrderTemp where 1=1 and sampleOrder.id='" + code + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showQualityTestTableJsonLI(Integer start, Integer length, String query, String col,
			String sort, String batch) {
		//查看质检结果
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1 and batch='" + batch + "'";
		String key = "";
		// if (query != null && !"".equals(query)) {
		// key = map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1 and batch='" + batch + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
			}
			key += " order by " + col + " " + sort;
			List<QualityTestInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	// /** 根据主表ID查询子表明细 */
	// public List<SampleOutApplyItem> selectSampleItemById(String id)
	// throws Exception {
	// String key = " ";
	// String hql = " from SampleOutApplyItem where 1=1 ";
	// List<SampleOutApplyItem> list = new ArrayList<SampleOutApplyItem>();
	// if (id != null && !id.equals("")) {
	// key = "and sampleOutApply.id = '" + id + "'";
	// list = this.getSession().createQuery(hql + key).list();
	// }
	// return list;
	// }
	//
	// public Map<String, Object> selectSampleOutApplyItemList(String scId,
	// Integer startNum, Integer limitNum, String dir, String sort)
	// throws Exception {
	// String hql = "from SampleOutApplyItem where 1=1 ";
	// String key = "";
	// if (scId != null)
	// key = key + " and sampleOutApply.id='" + scId + "'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SampleOutApplyItem> list = new ArrayList<SampleOutApplyItem>();
	// if (total > 0) {
	// if (dir != null && dir.length() > 0 && sort != null
	// && sort.length() > 0) {
	// if (sort.indexOf("-") != -1) {
	// sort = sort.substring(0, sort.indexOf("-"));
	// }
	// key = key + " order by " + sort + " " + dir;
	// }
	// if (startNum == null || limitNum == null) {
	// list = this.getSession().createQuery(hql + key).list();
	// } else {
	// list = this.getSession().createQuery(hql + key)
	// .setFirstResult(startNum).setMaxResults(limitNum)
	// .list();
	// }
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }
	//
	// // 选择打包单加载子表信息
	// public Map<String, Object> setSampleOutItemList(String code)
	// throws Exception {
	// String hql = "from SampleOutApplyItem t where 1=1 ";
	// String key = "";
	// if (code != null)
	// key = key + " and t.sampleOutApply like '%" + code + "%'";
	// Long total = (Long) this.getSession()
	// .createQuery("select count(*) " + hql + key).uniqueResult();
	// List<SampleOutApplyItem> list = this.getSession()
	// .createQuery(hql + key).list();
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("total", total);
	// result.put("list", list);
	// return result;
	// }
	//
	// public Map<String, Object> findSampleInfoInTable(Integer start,
	// Integer length, String query, String col, String sort)
	// throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// String countHql = "select count(*) from SampleInfoIn where 1=1 and
	// state='1'";
	// String key = "";
	// if (query != null) {
	// key = map2Where(query);
	// }
	// // 集团的查询条件
	// String scopeId = (String) ActionContext.getContext().getSession()
	// .get("scopeId");
	// if (!"all".equals(scopeId)) {
	// key += " and scopeId='" + scopeId + "'";
	// }
	// Long sumCount = (Long) getSession().createQuery(countHql)
	// .uniqueResult();
	// if (0l != sumCount) {
	// Long filterCount = (Long) getSession().createQuery(countHql + key)
	// .uniqueResult();
	// String hql = "from SampleInfoIn where 1=1 and state='1'";
	// if (col != null && !"".equals(col) && !"".equals(sort)
	// && sort != null) {
	// col = col.replace("-", ".");
	// key += " order by " + col + " " + sort;
	// }
	// List<SampleInfoIn> list = this.getSession().createQuery(hql + key)
	// .setFirstResult(start).setMaxResults(length).list();
	// map.put("recordsTotal", sumCount);
	// map.put("recordsFiltered", filterCount);
	// map.put("list", list);
	// }
	// return map;
	// }
	//
	// public Map<String, Object> findSampleOutApplyItemTable(String scId,
	// Integer start, Integer length, String query, String col, String sort)
	// throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// String countHql = "select count(*) from SampleOutApplyItem where 1=1 and
	// sampleOutApply.id='"
	// + scId + "'";
	// String key = "";
	// if (query != null) {
	// key = map2Where(query);
	// }
	// Long sumCount = (Long) getSession().createQuery(countHql)
	// .uniqueResult();
	// if (0l != sumCount) {
	// Long filterCount = (Long) getSession().createQuery(countHql + key)
	// .uniqueResult();
	// String hql = "from SampleOutApplyItem where 1=1 and sampleOutApply.id='"
	// + scId + "'";
	// if (col != null && !"".equals(col) && !"".equals(sort)
	// && sort != null) {
	// col = col.replace("-", ".");
	// key += " order by " + col + " " + sort;
	// }
	// List<SampleOutApplyItem> list = this.getSession()
	// .createQuery(hql + key).setFirstResult(start)
	// .setMaxResults(length).list();
	// map.put("recordsTotal", sumCount);
	// map.put("recordsFiltered", filterCount);
	// map.put("list", list);
	// }
	// return map;
	// }
	//
	// public Map<String, Object> findSampleOutApplyTable(Integer start,
	// Integer length, String query, String col, String sort)
	// throws Exception {
	//
	// Map<String, Object> map = new HashMap<String, Object>();
	// String countHql = "select count(*) from SampleOutApply where 1=1";
	// String key = "";
	// if (query != null && !"".equals(query)) {
	// key = map2Where(query);
	// }
	// String scopeId = (String) ActionContext.getContext().getSession()
	// .get("scopeId");
	// if (!"all".equals(scopeId)) {
	// key += " and scopeId='" + scopeId + "'";
	// }
	//
	// Long sumCount = (Long) getSession().createQuery(countHql)
	// .uniqueResult();
	// if (0l != sumCount) {
	// Long filterCount = (Long) getSession().createQuery(countHql + key)
	// .uniqueResult();
	// String hql = "from SampleOutApply where 1=1";
	// if (col != null && !"".equals(col) && !"".equals(sort)
	// && sort != null) {
	// col = col.replace("-", ".");
	// key += " order by " + col + " " + sort;
	// }
	// List<SampleOutApply> list = getSession().createQuery(hql + key)
	// .setFirstResult(start).setMaxResults(length).list();
	// map.put("recordsTotal", sumCount);
	// map.put("recordsFiltered", filterCount);
	// map.put("list", list);
	// }
	// return map;
	// }
}