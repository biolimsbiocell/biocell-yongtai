package com.biolims.experiment.reinfusionPlan.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlan;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanTemp;
import com.biolims.experiment.reinfusionPlan.service.ReinfusionPlanService;
import com.biolims.file.service.FileInfoService;
//import com.biolims.report.newreport.model.SampleReportItemNew;
import com.biolims.report.model.SampleReportItem;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.code.SystemConstants;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 出库申请
 * 
 * @author admin
 * 
 */
@Namespace("/experiment/reinfusionPlan/reinfusionPlan")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ReinfusionPlanAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "210000201";
	@Autowired
	private ReinfusionPlanService reinfusionPlanService;
	private ReinfusionPlan reinfusionPlan = new ReinfusionPlan();
	@Resource
	private CommonService commonService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 菜单栏链接
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showReinfusionPlanList")
	public String showReinfusionPlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/reinfusionPlanTable.jsp");
	}

	@Action(value = "showReinfusionPlanListJson")
	public void showReinfusionPlanListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reinfusionPlanService.findReinfusionPlanList(start, length, query, col, sort);
			List<ReinfusionPlan> list = (List<ReinfusionPlan>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("acceptUser-name", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("reinfusionPlanDate", "yyyy-MM-dd");
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 添加、修改回输计划页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditReinfusionPlan")
	public String toEditReinfusionPlan() throws Exception {
		String id = super.getRequest().getParameter("id");
		String handlemethod = "";
		if (id != null) {
			this.reinfusionPlan = this.reinfusionPlanService.findReinfusionPlanById(id);
			toState(reinfusionPlan.getState());
			if (reinfusionPlan.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);

		} else {
			this.reinfusionPlan = new ReinfusionPlan();
			this.reinfusionPlan.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User createUser = (User) super.getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
			reinfusionPlan.setCreateUser(createUser);
			reinfusionPlan.setCreateDate(new Date());
			reinfusionPlan.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			reinfusionPlan.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(reinfusionPlan.getState());
		}
		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/reinfusionPlanEdit.jsp");
	}

	// 查看
	@Action(value = "toViewReinfusionPlan")
	public String toViewReinfusionPlan() throws Exception {
		String id = getParameterFromRequest("id");
		reinfusionPlan = reinfusionPlanService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/reinfusionPlanEdit.jsp");
	}

	/**
	 * 
	 * @Title: @Description: 查询回输计划待选列表 @author qi.yan @date
	 *         2018-4-10下午3:55:04 @throws Exception void @throws
	 */
	@Action(value = "showReinfusionPlanTempTableJson")
	public void showReinfusionPlanTempTableJson() throws Exception {
		// 显示的数据类型
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reinfusionPlanService.selReinfusionPlanTempTable(start, length, query, col,
					sort);
			List<ReinfusionPlanTemp> list = (List<ReinfusionPlanTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderCode", "");
			map.put("code", "");
			map.put("state", "");
			map.put("name", "");
			map.put("age", "");
			map.put("gender", "");
			map.put("nation", "");
			map.put("hospital", "");
			map.put("hospitalPhone", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: @Description: 回输计划明细 @author qi.yan @date 2018-4-10下午4:15:11 @throws
	 *         Exception void @throws
	 */
	@Action(value = "showReinfusionPlanItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showReinfusionPlanItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = reinfusionPlanService.selReinfusionPlanItemTable(scId, start, length, query,
					col, sort);
			List<ReinfusionPlanItem> list = (List<ReinfusionPlanItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("state", "");
			map.put("orderCode", "");
			map.put("tempId", "");
			map.put("reinfusionPlan-id", "");
			map.put("name", "");
			map.put("age", "");
			map.put("gender", "");
			map.put("nation", "");
			map.put("hospital", "");
			map.put("hospitalPhone", "");
			map.put("feedBack", "");
			map.put("delay", "");
			map.put("result", "");
			map.put("note", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: saveSampleOutApplyAndItem @Description: 大保存 @author qi.yan @date
	 *         2018-4-11下午3:01:56 @throws Exception void @throws
	 */
	@Action(value = "savereinfusionPlanAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savereinfusionPlanAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");

		String id = getParameterFromRequest("id");
		String note = getParameterFromRequest("note");
		String reinfusionPlanDate = getParameterFromRequest("reinfusionPlanDate");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				ReinfusionPlan soa = new ReinfusionPlan();
				Map aMap = new HashMap();
				soa = (ReinfusionPlan) commonDAO.Map2Bean(map1, soa);
				// 保存主表信息
				ReinfusionPlan newSoa = reinfusionPlanService.saveSampleOutApplyById(soa, id, reinfusionPlanDate, note,
						changeLog);

				newSoa.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				newSoa.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				// 保存子表信息
				aMap.put("ImteJson", getParameterFromRequest("ImteJson"));
				reinfusionPlanService.saveReinfusionPlanAndItem(newSoa, aMap, changeLog, changeLogItem);
				map.put("id", newSoa.getId());
			}
			map.put("success", true);

		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: addSampleOutApplyItem @Description: 添加库存样本到出库申请明细 @author
	 *         qi.yan @date 2018-4-11下午3:01:25 @throws Exception void @throws
	 */
	@Action(value = "addReinfusionPlanItem")
	public void addReinfusionPlanItem() throws Exception {
		String note = getParameterFromRequest("note");
		String reinfusionPlanDate = getParameterFromRequest("reinfusionPlanDate");
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String createUser = getParameterFromRequest("createUser");
		String[] ids = getRequest().getParameterValues("ids[]");

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String sampleOutId = reinfusionPlanService.addReinfusionPlanItem(ids, note, reinfusionPlanDate, id,
					createUser, createDate);
			result.put("success", true);
			result.put("data", sampleOutId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delReinfusionPlanItem")
	public void delReinfusionPlanItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reinfusionPlanService.delReinfusionPlanItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: @Description: 回输计划列表 @author : @date @return @throws Exception
	 * String @throws
	 */
	@Action(value = "showReinfusionPlanItemList")
	public String showReinfusionPlanItemList() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/showReinfusionPlanItemList.jsp");
	}

	@Action(value = "showReinfusionPlanItemListJson")
	public void showReinfusionPlanItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			Map<String, Object> result = this.reinfusionPlanService.findReinfusionPlanItemList(start, length, query,
					col, sort, type);
			List<ReinfusionPlanItem> list = (List<ReinfusionPlanItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("report-id", "");
//			map.put("patientName", "");
//			map.put("interpretationsNum", "");
//			map.put("patientId", "");
//			map.put("orderNum", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("note", "");
//			map.put("report-compileUser-name", "");
//			map.put("report-acceptUser-name", "");
//			map.put("report-qcUser-name", "");
//			map.put("completeDate", "yyyy-MM-dd HH:mm:ss");
//			map.put("reviewState", "");
//			map.put("reviewer-id", "");
//			map.put("reviewer-name", "");
//			map.put("reviewerDate", "yyyy-MM-dd HH:mm:ss");
//			map.put("isRelease", "");
//			map.put("sendDate", "yyyy-MM-dd");
//			map.put("expressCompany", "");
//			map.put("emsId", "");
//			map.put("arc", "");
//			map.put("dzbarc", "");
//			map.put("dzbAddress", "");
//			map.put("dzbSendDate", "yyyy-MM-dd");
//			map.put("remark", "");
//			map.put("fileNum", "");
//			map.put("fileNum1", "");
//			map.put("fileNum2", "");
//			map.put("state", "");

			/** 申请明细id */
			map.put("id", "");
			/** 样本编号 */
			map.put("code", "");
			/** 状态 */
			map.put("state", "");
			/** 备注 */
			map.put("note", "");
			/** 确认结果 */
			map.put("result", "");
			/** 待选列表id */
			map.put("tempId", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			/** 病人姓名 */
			map.put("name", "");
			/** 年龄 */
			map.put("age", "");
			/** 性别 男:1 女:0 空:未知 */
			map.put("gender", "");
			/** 民族 */
			map.put("nation", "");
			/** 产品ID */
			map.put("productId", "");
			/** 产品名称 */
			map.put("productName", "");
			/** 医院 */
			map.put("hospital", "");
			/** 医院的联系电话 */
			map.put("hospitalPhone", "");
			/** 订单号 */
			map.put("orderCode", "");
			/** 是否回输 */
			map.put("feedBack", "");
			/** 能否延迟 */
			map.put("delay", "");
			/** 批次号 */
			map.put("batch", "");

			/** 计划回输日期 */
			map.put("reinfusionPlanDate", "yyyy-MM-dd");
			/** 回输修改时间字段 */
			map.put("modificationTime", "yyyy-MM-dd");
//			map.put("modificationTime","yyyy-MM-dd HH:mm");
			/** 是否可修改回输日期 */
			map.put("diReinfusionPlanDate", "");
			/** 筛选号 */
			map.put("filtrateCode", "");
			/** 科室 */
			map.put("inspectionDepartment", "");
			/** 医生 */
			map.put("attendingDoctor", "");
			/** 医生电话 */
			map.put("attendingDoctorPhone", "");
			/** 家属联系方式 */
			map.put("familyPhone", "");
			/** 通知到医生 */
			map.put("doctorNotice", "");
			/** 医生回复 */
			map.put("doctorReply", "");
			/** 通知到患者 */
			map.put("patientNotice", "");
			/** 患者回复 */
			map.put("patientReply", "");
			/** 创建时间 */
			map.put("createDateTime", "");
			/** 订单实体 */
			map.put("sampleOrder-id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: 运输计划保存 @Description: 保存 @author : @date @throws Exception
	 * void @throws
	 */
	@Action(value = "saveReinfusionPlanItemList")
	public void saveReinfusionPlanItemList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("dataJson");
			String changeLog = getRequest().getParameter("logInfo");

			ReinfusionPlanItem sr = new ReinfusionPlanItem();

			reinfusionPlanService.saveReinfusionPlanItem(sr, dataJson, changeLog);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: 回输计划变更 @Description: 保存 @author : @date @throws Exception
	 * void @throws
	 */
	@Action(value = "saveReinfusionPlanItemChangPlan")
	public void saveReinfusionPlanItemChangPlan() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String dataJson = getRequest().getParameter("dataJson");
			String changeLog = getRequest().getParameter("changeLog");
			String id = reinfusionPlanService.saveReinfusionPlanItemChangePlan(dataJson, changeLog);
			map.put("success", true);
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: 运输计划确认 @Description: 计划确认 @author : @date @throws Exception
	 * void @throws
	 */
	@Action(value = "saveReinfusionPlanItemConfirmList")
	public void saveReinfusionPlanItemConfirmList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reinfusionPlanService.saveReinfusionPlanItemConfirm(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 查看质检结果
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQualityTestTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQualityTestTable() throws Exception {
		String code = getParameterFromRequest("code");
		putObjToContext("flag", code);
		return dispatcher("/WEB-INF/page/experiment/reinfusionPlan/showQualityTestTable.jsp");
	}

	/**
	 * 查看质检结果
	 * 
	 * @throws Exception
	 */
	@Action(value = "showQualityTestTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQualityTestTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String batch = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = reinfusionPlanService.showQualityTestTableJsonLI(start, length, query, col, sort,
				batch);
		List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("qualityTest-id", "");
		map.put("sampleOrder-id", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("experimentalSteps", "");
		map.put("sampleDeteyion-name", "");
		map.put("orderId", "");
		map.put("result", "");
		map.put("note", "");
		map.put("mark", "");
		map.put("qualityTest-id", "");
		map.put("batch", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ReinfusionPlanService getReinfusionPlanService() {
		return reinfusionPlanService;
	}

	public void setReinfusionPlanService(ReinfusionPlanService reinfusionPlanService) {
		this.reinfusionPlanService = reinfusionPlanService;
	}

	public ReinfusionPlan getReinfusionPlan() {
		return reinfusionPlan;
	}

	public void setReinfusionPlan(ReinfusionPlan reinfusionPlan) {
		this.reinfusionPlan = reinfusionPlan;
	}

	// /**
	// * 查询
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "toSampleOutApplyMain", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String toSampleOutApplyMain() throws Exception {
	// String reqMethodType = getRequest().getParameter("reqMethodType");
	// String outApplyId = getRequest().getParameter("outApplyId");
	// if (outApplyId != null) {
	// SampleOutApply task = sampleOutApplyService.get(outApplyId);
	// if (!SystemConstants.WORK_FLOW_NEW.equals(task.getState()))
	// reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	// else
	// reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
	// toState(task.getState());
	// }
	//
	// getRequest().setAttribute("outApplyId", outApplyId);
	// putObjToContext("handlemethod", reqMethodType);
	// toToolBar(rightsId, "", "", reqMethodType);
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyMain.jsp");
	// }
	//
	// /**
	// * 查询
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "sampleOutApplySelect", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showDialogSampleOutApplyList() throws Exception {
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyDialog.jsp");
	// }
	//
	// @Action(value = "showDialogSampleOutApplyListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showDialogSampleOutApplyListJson() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// String outType = getParameterFromRequest("outType");
	// Map<String, String> map2Query = new HashMap<String, String>();
	// if (data != null && data.length() > 0)
	// map2Query = JsonUtils.toObjectByJson(data, Map.class);
	//
	// map2Query.put("stateName",
	// com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
	// map2Query.put("type", outType);
	// try {
	// Map<String, Object> result = sampleOutApplyService
	// .findSampleOutApplyList(map2Query, startNum, limitNum, dir,
	// sort);
	// Long count = (Long) result.get("total");
	// List<SampleOutApply> list = (List<SampleOutApply>) result
	// .get("list");
	//
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("name", "");
	// map.put("createUser-id", "");
	// map.put("createUser-name", "");
	// map.put("createDate", "yyyy-MM-dd");
	// map.put("acceptUser-id", "");
	// map.put("acceptUser-name", "");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("state", "");
	// map.put("stateName", "");
	// map.put("sampleType-id", "");
	// map.put("sampleType-name", "");
	// map.put("businessType-id", "");
	// map.put("businessType-name", "");
	// map.put("workOrder-id", "");
	// map.put("workOrder-name", "");
	// map.put("type-id", "");
	// map.put("type-name", "");
	// map.put("experimentType-id", "");
	// map.put("experimentType-name", "");
	// new SendData().sendDateJson(map, list, count,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// /**
	// * 编辑
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "editSampleOutApply")
	// public String editSampleOutApply() throws Exception {
	// String id = getParameterFromRequest("id");
	// long num = 0;
	// if (id != null && !id.equals("")) {
	// sampleOutApply = sampleOutApplyService.get(id);
	// putObjToContext("handlemethod",
	// SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	// toToolBar(rightsId, "", "",
	// SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	// num = fileInfoService.findFileInfoCount(id, "sampleOutApply");
	// } else {
	// // sampleOutApply=new SampleOutApply();
	// // sampleOutApply.setId(SystemCode.DEFAULT_SYSTEMCODE);
	// sampleOutApply.setId("NEW");
	// User user = (User) this
	// .getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// sampleOutApply.setCreateUser(user);
	// sampleOutApply.setCreateDate(new Date());
	// sampleOutApply.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// sampleOutApply.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// putObjToContext("handlemethod",
	// SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// }
	// toState(sampleOutApply.getState());
	// putObjToContext("fileNum", num);
	// List<DicType> list = commonService
	// .find("from DicType where type = 'yblx'");// 复选框list
	// putObjToContext("DicCountTableList", list);
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	// }
	//
	// /**
	// * 复制
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "copySampleOutApply")
	// public String copySampleOutApply() throws Exception {
	// String id = getParameterFromRequest("id");
	// String handlemethod = getParameterFromRequest("handlemethod");
	// sampleOutApply = sampleOutApplyService.get(id);
	// sampleOutApply.setId("");
	// handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
	// toToolBar(rightsId, "", "", handlemethod);
	// toSetStateCopy();
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	// }
	//
	// /**
	// * 保存
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "save")
	// public String save() throws Exception {
	// String id = sampleOutApply.getId();
	// if ((id != null && id.equals("")) || id.equals("NEW")) {
	// String modelName = "SampleOutApply";
	// String markCode = "CKSQ";
	// String autoID = codingRuleService.genTransID(modelName, markCode);
	// sampleOutApply.setId(autoID);
	// }
	// Map aMap = new HashMap();
	// aMap.put("sampleOutApplyItem",
	// getParameterFromRequest("sampleOutApplyItemJson"));
	//
	// sampleOutApplyService.save(sampleOutApply, aMap);
	// return
	// redirect("/sample/storage/sampleOutApply/editSampleOutApply.action?id="
	// + sampleOutApply.getId());
	//
	// }
	//
	// /**
	// * 视图编辑
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "viewSampleOutApply")
	// public String toViewSampleOutApply() throws Exception {
	// String id = getParameterFromRequest("id");
	// sampleOutApply = sampleOutApplyService.get(id);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	// }
	//
	// /**
	// * 明细
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "showSampleOutApplyItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showSampleOutApplyItemList() throws Exception {
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyItem.jsp");
	// }
	//
	// @Action(value = "showSampleOutApplyItemListJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleOutApplyItemListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	// String sort = getParameterFromRequest("sort");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = sampleOutApplyService
	// .findSampleOutApplyItemList(scId, startNum, limitNum, dir,
	// sort);
	// Long total = (Long) result.get("total");
	// List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("state", "");
	// map.put("code", "");
	// map.put("dataTraffic", "");
	// map.put("num", "");
	// map.put("sampleCode", "");
	// map.put("note", "");
	// map.put("sampleInItemId", "");
	// map.put("sampleType", "");
	// map.put("location", "");
	// map.put("sampleOutApply-name", "");
	// map.put("sampleOutApply-id", "");
	// map.put("sendTime", "yyyy-MM-dd");
	// map.put("concentration", "");
	// map.put("volume", "");
	// map.put("sumTotal", "");
	//
	// map.put("unitGroup-id", "");
	// map.put("unitGroup-name", "");
	// map.put("ystj", "");
	// map.put("fyVolume", "");
	// map.put("techJkServiceTask-id", "");
	// map.put("techJkServiceTask-createUser-name", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("samplingDate", "");
	// map.put("barCode", "");
	// map.put("patientName", "");
	//
	// map.put("sampleStyle", "");
	// map.put("qpcrConcentration", "");
	// map.put("indexa", "");
	// new SendData().sendDateJson(map, list, total,
	// ServletActionContext.getResponse());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// /**
	// * 删除明细信息
	// *
	// * @throws Exception
	// */
	// @Action(value = "delSampleOutApplyItem")
	// public void delSampleOutApplyItem() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// String[] ids = getRequest().getParameterValues("ids[]");
	// sampleOutApplyService.delSampleOutApplyItem(ids);
	// map.put("success", true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// map.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	// /**
	// * 根据申请加载子表明细
	// *
	// * @throws Exception
	// */
	// @Action(value = "showSampleOutItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleOutItemList() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// Map<String, Object> dataListMap = this.sampleOutApplyService
	// .showSampleOutItemList(code);
	// List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) dataListMap
	// .get("list");
	// result.put("success", true);
	// result.put("data", list);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// public String getRightsId() {
	// return rightsId;
	// }
	//
	// public void setRightsId(String rightsId) {
	// this.rightsId = rightsId;
	// }
	//
	// public SampleOutApplyService getSampleOutApplyService() {
	// return sampleOutApplyService;
	// }
	//
	// public void setSampleOutApplyService(
	// SampleOutApplyService sampleOutApplyService) {
	// this.sampleOutApplyService = sampleOutApplyService;
	// }
	//
	// public SampleOutApply getSampleOutApply() {
	// return sampleOutApply;
	// }
	//
	// public void setSampleOutApply(SampleOutApply sampleOutApply) {
	// this.sampleOutApply = sampleOutApply;
	// }
	//
	//
	//
	//
	// /**
	// *
	// * @Title: showSampleInfoInTableJson
	// * @Description: 出库申请查询待出库样本
	// * @author qi.yan
	// * @date 2018-4-10下午3:55:04
	// * @throws Exception
	// * void
	// * @throws
	// */
	// @Action(value = "showSampleInfoInTableJson")
	// public void showSampleInfoInTableJson() throws Exception {
	// // 显示的数据类型
	// String query = getRequest().getParameter("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// Map<String, Object> result =
	// sampleOutApplyService.selSampleInfoInTable(start, length, query, col,
	// sort);
	// List<SampleInfoIn> list = (List<SampleInfoIn>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("sampleCode", "");
	// map.put("code", "");
	// map.put("checked", "");
	// map.put("dataTraffic", "");
	// map.put("name", "");
	// map.put("orderId", "");
	// map.put("sampleType", "");
	// map.put("classify", "");
	// map.put("location", "");
	// map.put("state", "");
	// map.put("note", "");
	// map.put("tempId", "");
	// map.put("num", "");
	//// map.put("concentration", "");
	//// map.put("volume", "");
	//// map.put("sumTotal", "");
	// map.put("customer-id", "");
	// map.put("customer-name", "");
	// map.put("sellPerson-id", "");
	// map.put("sellPerson-name", "");
	// map.put("customer-street", "");
	// map.put("customer-postcode", "");
	// map.put("sampleInfo-project-id", "");
	// map.put("sampleInfo-project-name", "");
	// map.put("sampleInfo-crmCustomer-id", "");
	// map.put("sampleInfo-crmCustomer-name", "");
	// map.put("sampleInfo-crmDoctor-id", "");
	// map.put("sampleInfo-crmDoctor-name", "");
	// map.put("sampleInfo-productId", "");
	// map.put("sampleInfo-productName", "");
	// map.put("sampleInfo-patientName", "");
	// map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
	// map.put("sampleInfo-id", "");
	// map.put("sampleInfo-name", "");
	// map.put("sampleInfo-idCard", "");
	// map.put("isRp", "");
	// map.put("techJkServiceTask-id", "");
	// map.put("techJkServiceTask-name", "");
	// map.put("techJkServiceTask-createUser-name", "");
	// map.put("tjItem-id", "");
	// map.put("tjItem-inwardCode", "");
	// map.put("tjItem-orderId", "");
	// map.put("sampleState", "");
	// map.put("type", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("samplingDate","");
	//// map.put("barCode","");
	// map.put("dataTraffic","");
	//
	// map.put("sampleStyle","");
	// /*map.put("qpcrConcentration","");
	// map.put("indexa","");*/
	//
	// String data = new SendData().getDateJsonForDatatable(map, list);
	//// // 根据模块查询自定义字段数据
	//// Map<String, Object> mapField = fieldService
	//// .findFieldByModuleValue("Storage");
	//// String dataStr = PushData.pushFieldData(data, mapField);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	//
	//
	//
	// /**
	// *
	// * @Title: showSampleOutApplyItemTableJson
	// * @Description: 出库申请明细
	// * @author qi.yan
	// * @date 2018-4-10下午4:15:11
	// * @throws Exception
	// * void
	// * @throws
	// */
	// @Action(value = "showSampleOutApplyItemTableJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showSampleOutApplyItemTableJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// String scId = getRequest().getParameter("id");
	// Map<String, Object> result = sampleOutApplyService
	// .selSampleOutApplyItemTable(scId, start, length, query, col,
	// sort);
	// List<SampleOutApplyItem> list = (List<SampleOutApplyItem>) result
	// .get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("sampleOrder-id", "");
	// map.put("name", "");
	// map.put("state", "");
	// map.put("code", "");
	//// map.put("dataTraffic", "");
	// map.put("num", "");
	// map.put("sampleCode", "");
	// map.put("note", "");
	// map.put("sampleInItemId", "");
	// map.put("sampleType", "");
	// map.put("location", "");
	// map.put("sampleOutApply-name", "");
	// map.put("sampleOutApply-id", "");
	// map.put("sendTime", "yyyy-MM-dd");
	//// map.put("concentration", "");
	//// map.put("volume", "");
	//// map.put("sumTotal", "");
	//
	// map.put("unitGroup-id", "");
	// map.put("unitGroup-name", "");
	// map.put("ystj", "");
	// map.put("fyVolume", "");
	// map.put("techJkServiceTask-id", "");
	// map.put("techJkServiceTask-createUser-name", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("samplingDate", "");
	//// map.put("barCode", "");
	// map.put("patientName", "");
	//
	// map.put("sampleStyle", "");
	//// map.put("qpcrConcentration", "");
	//// map.put("indexa", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	//
	// /**
	// *
	// * @Title: showSampleOutApplyTable
	// * @Description: 展示出库申请主表
	// * @author qi.yan
	// * @date 2018-4-10下午4:37:39
	// * @return
	// * @throws Exception
	// * String
	// * @throws
	// */
	// @Action(value = "showSampleOutApplyTable")
	// public String showSampleOutApplyTable() throws Exception {
	// rightsId="210501";
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyTable.jsp");
	// }
	// @Action(value = "showSampleOutApplyTableJson")
	// public void showSampleOutApplyTableJson() throws Exception {
	// String query = getParameterFromRequest("query");
	// String colNum = getParameterFromRequest("order[0][column]");
	// String col = getParameterFromRequest("columns[" + colNum + "][data]");
	// String sort = getParameterFromRequest("order[0][dir]");
	// Integer start = Integer.valueOf(getParameterFromRequest("start"));
	// Integer length = Integer.valueOf(getParameterFromRequest("length"));
	// String draw = getParameterFromRequest("draw");
	// try {
	// Map<String, Object> result = sampleOutApplyService.selSampleOutApplyTable(
	// start, length, query, col, sort);
	// List<SampleOutApply> list = (List<SampleOutApply>) result.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("code", "");
	// map.put("name", "");
	// map.put("createUser-id", "");
	// map.put("createUser-name", "");
	// map.put("createDate", "yyyy-MM-dd");
	// map.put("acceptUser-id", "");
	// map.put("acceptUser-name", "");
	// map.put("acceptDate", "yyyy-MM-dd");
	// map.put("state", "");
	// map.put("stateName", "");
	// map.put("sampleType-id", "");
	// map.put("sampleType-name", "");
	// map.put("businessType-id", "");
	// map.put("businessType-name", "");
	// map.put("workOrder-id", "");
	// map.put("workOrder-name", "");
	// map.put("customer-id", "");
	// map.put("customer-name", "");
	// map.put("fyDate", "");
	// map.put("type-id", "");
	// map.put("type-name", "");
	// map.put("experimentType-id", "");
	// map.put("experimentType-name", "");
	// map.put("scopeId", "");
	// map.put("scopeName", "");
	// String data = new SendData().getDateJsonForDatatable(map, list);
	// HttpUtils.write(PushData.pushData(draw, result, data));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	//
	//
	// /**
	// * 添加、修改出库申请页面
	// *
	// * @return
	// * @throws Exception
	// */
	// @Action(value = "toEditSampleOutApply")
	// public String toEditSampleOutApply() throws Exception {
	// rightsId="210502";
	// String outApplyId = super.getRequest().getParameter("id");
	// String handlemethod = "";
	// if (outApplyId != null) {
	// this.sampleOutApply =
	// this.sampleOutApplyService.findSampleOutApplyById(outApplyId);
	// toState(sampleOutApply.getState());
	// if (sampleOutApply.getState().equals(SystemConstants.DIC_STATE_YES))
	// handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	// else
	// handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
	// toToolBar(rightsId, "", "", handlemethod);
	// showSampleOutApplyItemList(handlemethod, handlemethod);
	//
	// } else {
	// this.sampleOutApply = new SampleOutApply();
	// this.sampleOutApply.setId(SystemCode.DEFAULT_SYSTEMCODE);
	// User createUser = (User) super.getSession().getAttribute(
	// SystemConstants.USER_SESSION_KEY);
	// sampleOutApply.setCreateUser(createUser);
	// sampleOutApply.setCreateDate(new Date());
	// sampleOutApply.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	// sampleOutApply.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	// toState(sampleOutApply.getState());
	// showSampleOutApplyItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
	// outApplyId);
	// }
	// return dispatcher("/WEB-INF/page/sample/storage/sampleOutApplyEdit.jsp");
	// }
	//
	//
	// public void showSampleOutApplyItemList(String handleMethod, String outId)
	// throws Exception {
	// putObjToContext("outId", outId);
	// putObjToContext("handlemethod", handleMethod);
	// }
	//
	//
	// /**
	// *
	// * @Title: addSampleOutApplyItem
	// * @Description: 添加库存样本到出库申请明细
	// * @author qi.yan
	// * @date 2018-4-11下午3:01:25
	// * @throws Exception
	// * void
	// * @throws
	// */
	// @Action(value = "addSampleOutApplyItem")
	// public void addSampleOutApplyItem() throws Exception {
	// String note = getParameterFromRequest("note");
	// String type = getParameterFromRequest("type");
	// String id = getParameterFromRequest("id");
	// String createDate = getParameterFromRequest("createDate");
	// String createUser = getParameterFromRequest("createUser");
	// String[] ids = getRequest().getParameterValues("ids[]");
	//
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// String sampleOutId =
	// sampleOutApplyService.addSampleOutApplyItem(ids,note,type,id,createUser,createDate);
	// result.put("success", true);
	// result.put("data", sampleOutId);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	//
	//
	// /**
	// *
	// * @Title: saveSampleOutApplyAndItem
	// * @Description: 大保存
	// * @author qi.yan
	// * @date 2018-4-11下午3:01:56
	// * @throws Exception
	// * void
	// * @throws
	// */
	// @Action(value = "saveSampleOutApplyAndItem", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void saveSampleOutApplyAndItem() throws Exception {
	// String dataValue = getParameterFromRequest("dataValue");
	// // 主表changeLog
	// String changeLog = getParameterFromRequest("changeLog");
	// // 子表changeLogItem
	// String changeLogItem = getParameterFromRequest("changeLogItem");
	//
	// String id = getParameterFromRequest("id");
	// String type = getParameterFromRequest("type");
	// String note = getParameterFromRequest("note");
	//
	//
	// String str = "[" + dataValue + "]";
	//
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
	// List.class);
	//
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	//
	// for (Map<String, Object> map1 : list) {
	// SampleOutApply soa = new SampleOutApply();
	// Map aMap = new HashMap();
	// soa = (SampleOutApply) commonDAO.Map2Bean(map1, soa);
	// //保存主表信息
	// SampleOutApply newSoa =
	// sampleOutApplyService.saveSampleOutApplyById(soa,id,type,note,changeLog);
	//
	// newSoa.setScopeId((String) ActionContext.getContext().getSession()
	// .get("scopeId"));
	// newSoa.setScopeName((String) ActionContext.getContext().getSession()
	// .get("scopeName"));
	//
	// //保存子表信息
	// aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
	// sampleOutApplyService.saveSampleOutApplyAndItem(newSoa, aMap, changeLog,
	// changeLogItem);
	// map.put("id",newSoa.getId());
	// }
	// map.put("success", true);
	//
	// } catch (Exception e) {
	// map.put("success", false);
	// map.put("msg", e.getMessage());
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }
	//
	//
	//
	//
	// @Action(value = "delSampleOutApplyItems")
	// public void delSampleOutApplyItems() throws Exception {
	// String id = getParameterFromRequest("id");
	// String[] ids = getRequest().getParameterValues("ids[]");
	//
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// sampleOutApplyService.delSampleOutApplyItem(ids);
	// result.put("success", true);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

}
