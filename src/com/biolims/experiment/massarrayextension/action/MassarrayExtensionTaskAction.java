﻿package com.biolims.experiment.massarrayextension.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.massarrayextension.dao.MassarrayExtensionTaskDao;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTask;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskCos;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskInfo;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReagent;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemp;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemplate;
import com.biolims.experiment.massarrayextension.service.MassarrayExtensionTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/massarrayextension/massarrayExtensionTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MassarrayExtensionTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "14020211";
	@Autowired
	private MassarrayExtensionTaskService massarrayExtensionTaskService;
	private MassarrayExtensionTask massarrayExtensionTask = new MassarrayExtensionTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private MassarrayExtensionTaskDao massarrayExtensionTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showMassarrayExtensionTaskList")
	public String showMassarrayExtensionTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTask.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskListJson")
	public void showMassarrayExtensionTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskService
				.findMassarrayExtensionTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTask> list = (List<MassarrayExtensionTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "massarrayExtensionTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMassarrayExtensionTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskDialog.jsp");
	}

	@Action(value = "showDialogMassarrayExtensionTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMassarrayExtensionTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskService
				.findMassarrayExtensionTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTask> list = (List<MassarrayExtensionTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editMassarrayExtensionTask")
	public String editMassarrayExtensionTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			massarrayExtensionTask = massarrayExtensionTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "massarrayExtensionTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			massarrayExtensionTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			massarrayExtensionTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			massarrayExtensionTask.setCreateDate(stime);
			massarrayExtensionTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			massarrayExtensionTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(massarrayExtensionTask.getState());
		putObjToContext("maxNum", maxNum);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskEdit.jsp");
	}

	@Action(value = "copyMassarrayExtensionTask")
	public String copyMassarrayExtensionTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		massarrayExtensionTask = massarrayExtensionTaskService.get(id);
		massarrayExtensionTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = massarrayExtensionTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "MassarrayExtensionTask";
			String markCode = "HSTQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			massarrayExtensionTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("massarrayExtensionTaskItem",
				getParameterFromRequest("massarrayExtensionTaskItemJson"));
		aMap.put("massarrayExtensionTaskResult",
				getParameterFromRequest("massarrayExtensionTaskResultJson"));
		aMap.put("massarrayExtensionTaskTemplateItem",
				getParameterFromRequest("massarrayExtensionTaskTemplateItemJson"));
		aMap.put("massarrayExtensionTaskTemplateReagent",
				getParameterFromRequest("massarrayExtensionTaskTemplateReagentJson"));
		aMap.put("massarrayExtensionTaskTemplateCos",
				getParameterFromRequest("massarrayExtensionTaskTemplateCosJson"));
		massarrayExtensionTaskService.save(massarrayExtensionTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/massarrayextension/massarrayExtensionTask/editMassarrayExtensionTask.action?id="
				+ massarrayExtensionTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}
	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
		String id = getParameterFromRequest("id");
		massarrayExtensionTask = commonService.get(MassarrayExtensionTask.class, id);
		Map aMap = new HashMap();
		aMap.put("massarrayExtensionTaskItem",
				getParameterFromRequest("massarrayExtensionTaskItemJson"));
		aMap.put("massarrayExtensionTaskResult",
				getParameterFromRequest("massarrayExtensionTaskResultJson"));
		aMap.put("massarrayExtensionTaskTemplateItem",
				getParameterFromRequest("massarrayExtensionTaskTemplateItemJson"));
		aMap.put("massarrayExtensionTaskTemplateReagent",
				getParameterFromRequest("massarrayExtensionTaskTemplateReagentJson"));
		aMap.put("massarrayExtensionTaskTemplateCos",
				getParameterFromRequest("massarrayExtensionTaskTemplateCosJson"));
		map=massarrayExtensionTaskService.save(massarrayExtensionTask, aMap);
		map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}
	@Action(value = "viewMassarrayExtensionTask")
	public String toViewMassarrayExtensionTask() throws Exception {
		String id = getParameterFromRequest("id");
		massarrayExtensionTask = massarrayExtensionTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskEdit.jsp");
	}

	/**
	 * 查询左侧中间表
	 * @return
	 * @throws Exception
	 */
//	@Action(value = "showMassarrayExtensionTaskTempList")
//	public String showMassarrayExtensionTaskTempList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskTemp.jsp");
//	}

	@Action(value = "showMassarrayExtensionTaskTempJson")
	public void showMassarrayExtensionTaskTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskService
				.findMassarrayExtensionTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTaskTemp> list = (List<MassarrayExtensionTaskTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleId", "");
		map.put("sampleName", "");
		map.put("location", "");
		map.put("patientName", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("sampleNum", "");
		map.put("labCode", "");
		map.put("sampleInfo-note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showMassarrayExtensionTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskItem.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = massarrayExtensionTaskService
					.findMassarrayExtensionTaskItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskItem> list = (List<MassarrayExtensionTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("massarrayExtensionTask-name", "");
			map.put("massarrayExtensionTask-id", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			map.put("oligoName", "");
			map.put("oligoMass", "");
			map.put("oligoConcentration", "");
			map.put("oligoConcentrationInPool", "");
			map.put("zuHe", "");
			map.put("oligoUl", "");
			map.put("yongShui", "");
			map.put("yongShuiUl", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delMassarrayExtensionTaskItem")
	public void delMassarrayExtensionTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			if (ids != null) {
				massarrayExtensionTaskService.delMassarrayExtensionTaskItem(ids);
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showMassarrayExtensionTaskInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskInfo.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = massarrayExtensionTaskService
					.findMassarrayExtensionTaskInfoList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskInfo> list = (List<MassarrayExtensionTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("sumVolume", "");
			map.put("rin", "");
			map.put("contraction", "");
			map.put("od260", "");
			map.put("od280", "");
			map.put("tempId", "");
			map.put("isToProject", "");
			map.put("volume", "");
			map.put("note", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("massarrayExtensionTask-name", "");
			map.put("massarrayExtensionTask-id", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("dataBits", "");
			map.put("qbcontraction", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			map.put("oligoName", "");
			map.put("oligoMass", "");
			map.put("oligoConcentration", "");
			map.put("oligoConcentrationInPool", "");
			map.put("zuHe", "");
			map.put("oligoUl", "");
			map.put("yongShui", "");
			map.put("yongShuiUl", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delMassarrayExtensionTaskInfo")
	public void delMassarrayExtensionTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			massarrayExtensionTaskService.delMassarrayExtensionTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MassarrayExtensionTaskService getMassarrayExtensionTaskService() {
		return massarrayExtensionTaskService;
	}

	public void setMassarrayExtensionTaskService(
			MassarrayExtensionTaskService massarrayExtensionTaskService) {
		this.massarrayExtensionTaskService = massarrayExtensionTaskService;
	}

	public MassarrayExtensionTask getMassarrayExtensionTask() {
		return massarrayExtensionTask;
	}

	public void setMassarrayExtensionTask(MassarrayExtensionTask massarrayExtensionTask) {
		this.massarrayExtensionTask = massarrayExtensionTask;
	}

	/**
	 * 模板
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskTemplateItem.jsp");
	}

	@Action(value = "showTemplateItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = massarrayExtensionTaskService
					.findTemplateItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskTemplate> list = (List<MassarrayExtensionTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("massarrayExtensionTask-name", "");
			map.put("massarrayExtensionTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItem")
	public void delTemplateItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			massarrayExtensionTaskService.delTemplateItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItemOne")
	public void delTemplateItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			massarrayExtensionTaskService.delTemplateItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showMassarrayExtensionTaskTemplateReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskTemplateReagentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskTemplateReagent.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskTemplateReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskTemplateReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = massarrayExtensionTaskService.findReagentItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskReagent> list = (List<MassarrayExtensionTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("massarrayExtensionTask-name", "");
			map.put("massarrayExtensionTask-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delMassarrayExtensionTaskTemplateReagent")
	public void delMassarrayExtensionTaskTemplateReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			massarrayExtensionTaskService.delReagentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateReagentOne")
	public void delTemplateReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			massarrayExtensionTaskService.delTemplateReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showMassarrayExtensionTaskTemplateCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskTemplateCosList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskTemplateCos.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskTemplateCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskTemplateCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = massarrayExtensionTaskService.findCosItemList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskCos> list = (List<MassarrayExtensionTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			map.put("state", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("massarrayExtensionTask-name", "");
			map.put("massarrayExtensionTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delMassarrayExtensionTaskTemplateCos")
	public void delMassarrayExtensionTaskTemplateCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			massarrayExtensionTaskService.delCosItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateCosOne")
	public void delMassarrayExtensionTaskTemplateCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			massarrayExtensionTaskService.delMassarrayExtensionTaskTemplateCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 获取从样本接收来的样本
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showMassarrayExtensionTaskFromReceiveList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskFromReceiveList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskFromReceiveLeft.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskFromReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskFromReceiveListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = massarrayExtensionTaskService
					.selectMassarrayExtensionTaskFromReceiveList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskReceiveItem> list = (List<MassarrayExtensionTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("massarrayExtensionTaskReceive-id", "");
			map.put("massarrayExtensionTaskReceive-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	/**
	 * 判断样本做MassarrayExtension任务的次数
	 * @throws Exception
	 */
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = massarrayExtensionTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 模板
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = massarrayExtensionTaskService
					.findTemplateItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskTemplate> list = (List<MassarrayExtensionTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("massarrayExtensionTask-name", "");
			map.put("massarrayExtensionTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据选中的步骤异步加载相关的试剂数据
	 * @throws Exception
	 */
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.massarrayExtensionTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据选中的步骤异步加载相关的设备数据
	 * @throws Exception
	 */
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.massarrayExtensionTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	

	
	/**
	 * 提交样本
	 * @throws Exception
	 */
	@Action(value = "submit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submit() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.massarrayExtensionTaskService.submit(id, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
