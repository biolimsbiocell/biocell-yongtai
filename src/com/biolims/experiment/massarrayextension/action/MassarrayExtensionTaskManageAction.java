﻿package com.biolims.experiment.massarrayextension.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskInfo;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskWaitManage;
import com.biolims.experiment.massarrayextension.service.MassarrayExtensionTaskManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/massarrayextension/massarrayExtensionTaskManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MassarrayExtensionTaskManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240205";
	@Autowired
	private MassarrayExtensionTaskManageService massarrayExtensionTaskManageService;
	private MassarrayExtensionTaskWaitManage massarrayExtensionTaskManage = new MassarrayExtensionTaskWaitManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showMassarrayExtensionTaskManageEditList")
	public String showMassarrayExtensionTaskManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskManageEdit.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskManageList")
	public String showMassarrayExtensionTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskManage.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskManageListJson")
	public void showMassarrayExtensionTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskManageService
				.findMassarrayExtensionTaskManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTaskInfo> list = (List<MassarrayExtensionTaskInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");
	
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("sumVolume", "");
		map.put("rin", "");
		map.put("contraction", "");
		map.put("od260", "");
		map.put("od280", "");
		map.put("tempId", "");
		map.put("isToProject", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("state", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("massarrayExtensionTask-name", "");
		map.put("massarrayExtensionTask-id", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("orderType", "");
		map.put("taskId", "");
		map.put("classify", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleType", "");
		map.put("labCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "massarrayExtensionTaskManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMassarrayExtensionTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskManageDialog.jsp");
	}

	@Action(value = "showDialogMassarrayExtensionTaskManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMassarrayExtensionTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskManageService
				.findMassarrayExtensionTaskManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTaskWaitManage> list = (List<MassarrayExtensionTaskWaitManage>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("method", "");
		map.put("nextFlow", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editMassarrayExtensionTaskManage")
	public String editMassarrayExtensionTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			massarrayExtensionTaskManage = massarrayExtensionTaskManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "massarrayExtensionTaskManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskManageEdit.jsp");
	}

	@Action(value = "copyMassarrayExtensionTaskManage")
	public String copyMassarrayExtensionTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		massarrayExtensionTaskManage = massarrayExtensionTaskManageService.get(id);
		massarrayExtensionTaskManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = massarrayExtensionTaskManage.getId();
		if (id != null && id.equals("")) {
			massarrayExtensionTaskManage.setId(null);
		}
		Map aMap = new HashMap();
		massarrayExtensionTaskManageService.save(massarrayExtensionTaskManage, aMap);
		return redirect("/experiment/massarrayextension/massarrayExtensionTaskManage/editMassarrayExtensionTaskManage.action?id="
				+ massarrayExtensionTaskManage.getId());

	}

	@Action(value = "viewMassarrayExtensionTaskManage")
	public String toViewMassarrayExtensionTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		massarrayExtensionTaskManage = massarrayExtensionTaskManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskManageEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MassarrayExtensionTaskManageService getMassarrayExtensionTaskManageService() {
		return massarrayExtensionTaskManageService;
	}

	public void setMassarrayExtensionTaskManageService(
			MassarrayExtensionTaskManageService massarrayExtensionTaskManageService) {
		this.massarrayExtensionTaskManageService = massarrayExtensionTaskManageService;
	}

	public MassarrayExtensionTaskWaitManage getMassarrayExtensionTaskManage() {
		return massarrayExtensionTaskManage;
	}

	public void setMassarrayExtensionTaskManage(MassarrayExtensionTaskWaitManage massarrayExtensionTaskManage) {
		this.massarrayExtensionTaskManage = massarrayExtensionTaskManage;
	}

	/**
	 * 保存中间产物
	 * @throws Exception
	 */
	@Action(value = "saveMassarrayExtensionTaskManage")
	public void saveMassarrayExtensionTaskAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			massarrayExtensionTaskManageService
					.saveMassarrayExtensionTaskInfoManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 保存样本
	 * @throws Exception
	 */
	@Action(value = "saveMassarrayExtensionTaskItemManager")
	public void saveMassarrayExtensionTaskItemManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			massarrayExtensionTaskManageService.saveMassarrayExtensionTaskItemManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showMassarrayExtensionTaskItemManagerList")
	public String showMassarrayExtensionTaskItemManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskItemManage.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskItemManagerListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// 排序方式
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = massarrayExtensionTaskManageService
					.findMassarrayExtensionTaskItemList(map2Query, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskItem> list = (List<MassarrayExtensionTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
		
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlow", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleConsume", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 样本管理明细入库
	 * @throws Exception
	 */
	@Action(value = "MassarrayExtensionTaskManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			massarrayExtensionTaskManageService.MassarrayExtensionTaskManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 样本管理明细MassarrayExtension任务待办
	 * @throws Exception
	 */
	@Action(value = "MassarrayExtensionTaskManageItemTiqu")
	public void MassarrayExtensionTaskManageItemTiqu() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			massarrayExtensionTaskManageService.MassarrayExtensionTaskManageItemTiqu(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
