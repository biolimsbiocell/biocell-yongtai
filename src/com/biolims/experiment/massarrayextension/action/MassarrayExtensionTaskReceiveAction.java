﻿package com.biolims.experiment.massarrayextension.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceive;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveTemp;
import com.biolims.experiment.massarrayextension.service.MassarrayExtensionTaskReceiveService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/massarrayextension/massarrayExtensionTaskReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MassarrayExtensionTaskReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240206";
	@Autowired
	private MassarrayExtensionTaskReceiveService massarrayExtensionTaskReceiveService;
	private MassarrayExtensionTaskReceive massarrayExtensionTaskReceive = new MassarrayExtensionTaskReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showMassarrayExtensionTaskReceiveList")
	public String showMassarrayExtensionTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceive.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskReceiveListJson")
	public void showMassarrayExtensionTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskReceiveService
				.findMassarrayExtensionTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTaskReceive> list = (List<MassarrayExtensionTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "massarrayExtensionTaskReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMassarrayExtensionTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceiveDialog.jsp");
	}

	@Action(value = "showDialogMassarrayExtensionTaskReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMassarrayExtensionTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = massarrayExtensionTaskReceiveService
				.findMassarrayExtensionTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<MassarrayExtensionTaskReceive> list = (List<MassarrayExtensionTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("experiment.massarrayExtensionCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editMassarrayExtensionTaskReceive")
	public String editMassarrayExtensionTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			massarrayExtensionTaskReceive = massarrayExtensionTaskReceiveService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "massarrayExtensionTaskReceive");
		} else {
			massarrayExtensionTaskReceive.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			massarrayExtensionTaskReceive.setReceiveUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			massarrayExtensionTaskReceive.setReceiverDate(stime);
			massarrayExtensionTaskReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			massarrayExtensionTaskReceive
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceiveEdit.jsp");
	}

	@Action(value = "copyMassarrayExtensionTaskReceive")
	public String copyMassarrayExtensionTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		massarrayExtensionTaskReceive = massarrayExtensionTaskReceiveService.get(id);
		massarrayExtensionTaskReceive.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceiveEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = massarrayExtensionTaskReceive.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "MassarrayExtensionTaskReceive";
			String markCode = "HSJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			massarrayExtensionTaskReceive.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("massarrayExtensionTaskReceiveItem",
				getParameterFromRequest("massarrayExtensionTaskReceiveItemJson"));
		massarrayExtensionTaskReceiveService.save(massarrayExtensionTaskReceive, aMap);
		return redirect("/experiment/massarrayextension/massarrayExtensionTaskReceive/editMassarrayExtensionTaskReceive.action?id="
				+ massarrayExtensionTaskReceive.getId());

	}

	@Action(value = "viewMassarrayExtensionTaskReceive")
	public String toViewMassarrayExtensionTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		massarrayExtensionTaskReceive = massarrayExtensionTaskReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceiveEdit.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceiveItem.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskReceiveItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskReceiveItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = massarrayExtensionTaskReceiveService
					.findMassarrayExtensionTaskReceiveItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskReceiveItem> list = (List<MassarrayExtensionTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleId", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("massarrayExtensionTaskReceive-id", "");
			map.put("massarrayExtensionTaskReceive-name", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delMassarrayExtensionTaskReceiveItem")
	public void delMassarrayExtensionTaskReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			massarrayExtensionTaskReceiveService.delMassarrayExtensionTaskReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
 	/**
	* 去待接收的样本
	* @throws Exception
	 */
	@Action(value = "showMassarrayExtensionTaskReceiveItemListTo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMassarrayExtensionTaskReceiveItemListTo() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/massarrayextension/massarrayExtensionTaskReceiveItemTo.jsp");
	}

	@Action(value = "showMassarrayExtensionTaskReceiveItemListToJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMassarrayExtensionTaskReceiveItemListToJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, Object> result = massarrayExtensionTaskReceiveService
					.selectMassarrayExtensionTaskReceiveTempList(startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<MassarrayExtensionTaskReceiveTemp> list = (List<MassarrayExtensionTaskReceiveTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("inspectDate", "");
			map.put("sampleType", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("reportDate", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("classify", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MassarrayExtensionTaskReceiveService getMassarrayExtensionTaskReceiveService() {
		return massarrayExtensionTaskReceiveService;
	}

	public void setMassarrayExtensionTaskReceiveService(
			MassarrayExtensionTaskReceiveService massarrayExtensionTaskReceiveService) {
		this.massarrayExtensionTaskReceiveService = massarrayExtensionTaskReceiveService;
	}

	public MassarrayExtensionTaskReceive getMassarrayExtensionTaskReceive() {
		return massarrayExtensionTaskReceive;
	}

	public void setMassarrayExtensionTaskReceive(MassarrayExtensionTaskReceive massarrayExtensionTaskReceive) {
		this.massarrayExtensionTaskReceive = massarrayExtensionTaskReceive;
	}
}
