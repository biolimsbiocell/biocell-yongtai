package com.biolims.experiment.massarrayextension.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.massarrayextension.service.MassarrayExtensionTaskReceiveService;

public class MassarrayExtensionTaskReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		MassarrayExtensionTaskReceiveService mbService = (MassarrayExtensionTaskReceiveService) ctx
				.getBean("massarrayExtensionTaskReceiveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
