package com.biolims.experiment.massarrayextension.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.massarrayextension.service.MassarrayExtensionTaskService;

public class MassarrayExtensionTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		MassarrayExtensionTaskService mbService = (MassarrayExtensionTaskService) ctx
				.getBean("massarrayExtensionTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
