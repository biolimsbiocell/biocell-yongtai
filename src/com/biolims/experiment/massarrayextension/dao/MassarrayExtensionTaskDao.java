package com.biolims.experiment.massarrayextension.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTask;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskCos;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskInfo;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReagent;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemp;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemplate;

@Repository
@SuppressWarnings("unchecked")
public class MassarrayExtensionTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectMassarrayExtensionTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from MassarrayExtensionTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTask> list = new ArrayList<MassarrayExtensionTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskItem> list = new ArrayList<MassarrayExtensionTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskInfo> list = new ArrayList<MassarrayExtensionTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 模板对应的子表
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMassarrayExtensionTaskTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskTemplate> list = new ArrayList<MassarrayExtensionTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskTemplateReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReagent> list = new ArrayList<MassarrayExtensionTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据ItemId查询试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMassarrayExtensionTaskTemplateReagentListByItemId(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from MassarrayExtensionTaskReagent t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReagent> list = new ArrayList<MassarrayExtensionTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from MassarrayExtensionTaskReagent t where 1=1 and t.massarrayExtensionTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<MassarrayExtensionTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from MassarrayExtensionTaskCos t where 1=1 and t.massarrayExtensionTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<MassarrayExtensionTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskTemplateCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskCos> list = new ArrayList<MassarrayExtensionTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMassarrayExtensionTaskTemplateCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from MassarrayExtensionTaskCos t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskCos> list = new ArrayList<MassarrayExtensionTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据MassarrayExtensionTaskMassarrayExtension任务主表ID查询试剂明细
	 */
	public List<MassarrayExtensionTaskReagent> setReagentList(String code) {
		String hql = "from MassarrayExtensionTaskReagent where 1=1 and massarrayExtensionTask.id='" + code
				+ "'";
		List<MassarrayExtensionTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据MassarrayExtensionTaskMassarrayExtension任务主表ID查询MassarrayExtension任务明细
	 */
	public List<MassarrayExtensionTaskItem> setMassarrayExtensionTaskItemList(String code) {
		String hql = "from MassarrayExtensionTaskItem where 1=1 and massarrayExtensionTask.id='" + code + "'";
		List<MassarrayExtensionTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据状态查询MassarrayExtensionTask
	 * @return
	 */
	public List<MassarrayExtensionTask> selectMassarrayExtensionTaskList() {
		// String hql = "from MassarrayExtensionTask t where 1=1 and t.state=1";
		String hql = "from MassarrayExtensionTask t where 1=1 and t.state='1'";
		List<MassarrayExtensionTask> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<MassarrayExtensionTaskInfo> selectMassarrayExtensionTaskResultList(String id) {
		String hql = "from MassarrayExtensionTaskInfo t where 1=1 and massarrayExtensionTask.state='1' and massarrayExtensionTask='"
				+ id + "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> setMassarrayExtensionTaskResultByCode(String code) {
		String hql = "from MassarrayExtensionTaskInfo t where 1=1 and massarrayExtensionTask.id='" + code
				+ "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> setMassarrayExtensionTaskResultById(String code) {
		String hql = "from MassarrayExtensionTaskInfo t where submit is null and massarrayExtensionTask.id='"
				+ code + "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> setMassarrayExtensionTaskResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from MassarrayExtensionTaskInfo t where submit is null and id in ("
				+ insql + ")";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询MassarrayExtension任务样本去到QPCR或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectExperimentMassarrayExtensionTaskReslutList(
			Integer startNum, Integer limitNum, String dir, String sort,
			String state) throws Exception {
		String hql = "from MassarrayExtensionTaskInfo where 1=1 and experiment.massarrayExtensionInfo.state='完成' and massarrayExtensionTask.stateName='"
				+ state + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskInfo> list = new ArrayList<MassarrayExtensionTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMassarrayExtensionTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskReceiveItem where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReceiveItem> list = new ArrayList<MassarrayExtensionTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询异常MassarrayExtension任务
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMassarrayExtensionTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskInfo> list = new ArrayList<MassarrayExtensionTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询左侧中间表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectMassarrayExtensionTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from MassarrayExtensionTaskTemp where 1=1 and state='1'";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskTemp> list = new ArrayList<MassarrayExtensionTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询MassarrayExtension任务异常样本
	 * @param code
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> selectMassarrayExtensionTaskResultAbnormalList(String code) {
		String hql = "from MassarrayExtensionTaskInfo where 1=1 and  massarrayExtensionTask='" + code + "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> selectMassarrayExtensionTaskAllList(String code) {
		String hql = "from MassarrayExtensionTaskInfo  where 1=1  and massarrayExtensionTask='" + code
				+ "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id查询MassarrayExtension任务生成结果 用于更新是否合格字段
	 * @param id
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> findMassarrayExtensionTaskResultById(String id) {
		String hql = "from MassarrayExtensionTaskInfo  where 1=1 and code='" + id + "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 状态完成下一步流向
	 * @param code
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> setNextFlowList(String code) {
		String hql = "from MassarrayExtensionTaskInfo where 1=1  and massarrayExtensionTask.state='1' and massarrayExtensionTask.id='"
				+ code + "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从上一步样本接收完成的MassarrayExtension任务样本
	 * @return
	 */
	public List<MassarrayExtensionTaskReceiveItem> selectReceiveMassarrayExtensionTaskList() {
		String hql = "from MassarrayExtensionTaskReceiveItem where 1=1  and state='1' ";
		List<MassarrayExtensionTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from MassarrayExtensionTaskItem t where 1=1 and t.massarrayExtensionTask.id='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据id查询MassarrayExtension任务结果
	 * @param id
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> selectMassarrayExtensionTaskResultListById(String id) {
		String hql = "from MassarrayExtensionTaskInfo where 1=1  and massarrayExtensionTask.id='" + id
				+ "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的MassarrayExtension任务次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from MassarrayExtensionTaskItem t where 1=1 and t.sampleCode='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.Code)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 查询从样本接收完成的的样本
	 * @param id
	 * @return
	 */
	public List<MassarrayExtensionTaskItem> findMassarrayExtensionTaskItemList(String id) {
		String hql = "from MassarrayExtensionTaskItem  where 1=1 and state='1' and massarrayExtensionTask='"
				+ id + "'";
		List<MassarrayExtensionTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据Code查询MassarrayExtensionTaskInfo
	 * @param code
	 * @return
	 */
	public String findMassarrayExtensionTaskInfo(String code) {
		String hql = "from MassarrayExtensionTaskInfo  where 1=1 and tempId='" + code + "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 根据样本编号查出MassarrayExtensionTaskTemp对象
	 * @param code
	 * @return
	 */
	public MassarrayExtensionTaskTemp findMassarrayExtensionTaskTemp(String code) {
		String hql = "from MassarrayExtensionTaskTemp t where 1=1 and t.sampleCode = '" + code
				+ "'";
		MassarrayExtensionTaskTemp MassarrayExtensionTaskTemp = (MassarrayExtensionTaskTemp) this.getSession()
				.createQuery(hql).uniqueResult();
		return MassarrayExtensionTaskTemp;
	}


	/**
	 * 根据样本查询主表
	 * @param sid
	 * @return
	 */
	public String selectTask(String sid) {
		String hql = "from TechCheckServiceOrder t where t.id = '" + sid + "'";
		String str = (String) this.getSession()
				.createQuery(" select t.id " + hql).uniqueResult();
		return str;
	}

	public List<MassarrayExtensionTaskItem> selectMassarrayExtensionTaskItemList(String scId) throws Exception {
		String hql = "from MassarrayExtensionTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskItem> list = new ArrayList<MassarrayExtensionTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * 根据样本编号查询检测临时表
	 * @param id
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> setMassarrayExtensionTaskInfo(String id) {
		String hql = "from MassarrayExtensionTaskInfo where 1=1 and massarrayExtensionTask.id='" + id
				+ "' and nextFlowId='0009'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据入库的原始样本号查询该结果表信息
	 * @param id
	 * @param Code
	 * @return
	 */
	public List<MassarrayExtensionTaskInfo> setMassarrayExtensionTaskInfo2(String id, String code) {
		String hql = "from MassarrayExtensionTaskInfo t where 1=1 and massarrayExtensionTask.id='" + id
				+ "' and sampleCode='" + code + "'";
		List<MassarrayExtensionTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}