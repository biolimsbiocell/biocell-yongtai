package com.biolims.experiment.massarrayextension.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceive;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveTemp;

@Repository
@SuppressWarnings("unchecked")
public class MassarrayExtensionTaskReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectMassarrayExtensionTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from MassarrayExtensionTaskReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReceive> list = new ArrayList<MassarrayExtensionTaskReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from MassarrayExtensionTaskReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReceiveItem> list = new ArrayList<MassarrayExtensionTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskReceiveItemByTypeList(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from MassarrayExtensionTaskReceiveItem where 1=1 and massarrayExtensionTaskReceive.Type='MassarrayExtensionTask'";
		String key = "";
		if (scId != null)
			key = key + " and massarrayExtensionTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReceiveItem> list = new ArrayList<MassarrayExtensionTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询从开向检验完成的样本
	 * @return
	 */
	public List<MassarrayExtensionTaskReceiveTemp> selectMassarrayExtensionTaskReceiveTempList() {
		String hql = "from MassarrayExtensionTaskReceiveTemp where 1=1 and state='1' order by reportDate desc";
		List<MassarrayExtensionTaskReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 修改状态
	 * @param id
	 * @return
	 */
	public List<MassarrayExtensionTaskReceiveItem> selectReceiveByIdList(String id) {
		String hql = "from MassarrayExtensionTaskReceiveItem where 1=1 and state='1' and method='1' and massarrayExtensionTaskReceive.id='"
				+ id + "'";
		List<MassarrayExtensionTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 明细数据
	 * @param code
	 * @return
	 */
	public List<MassarrayExtensionTaskReceiveItem> selectReceiveItemListById(String code) {
		String hql = "from MassarrayExtensionTaskReceiveItem where 1=1 and massarrayExtensionTaskReceive.id='"
				+ code + "'";
		List<MassarrayExtensionTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public Map<String, Object> selectMassarrayExtensionTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from MassarrayExtensionTaskReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MassarrayExtensionTaskReceiveTemp> list = new ArrayList<MassarrayExtensionTaskReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
}