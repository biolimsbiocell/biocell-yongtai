package com.biolims.experiment.massarrayextension.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.massarrayextension.dao.MassarrayExtensionTaskDao;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTask;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskAbnormal;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskCos;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskInfo;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskInfoManager;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReagent;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemp;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemplate;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MassarrayExtensionTaskService {
	@Resource
	private MassarrayExtensionTaskDao massarrayExtensionTaskDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;

	StringBuffer json = new StringBuffer();
	public Map<String, Object> findMassarrayExtensionTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return massarrayExtensionTaskDao.selectMassarrayExtensionTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findMassarrayExtensionTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		    return massarrayExtensionTaskDao.selectMassarrayExtensionTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MassarrayExtensionTask i) throws Exception {
		massarrayExtensionTaskDao.saveOrUpdate(i);
	}

	public MassarrayExtensionTask get(String id) {
		MassarrayExtensionTask massarrayExtensionTask = commonDAO.get(MassarrayExtensionTask.class, id);
		return massarrayExtensionTask;
	}

	public Map<String, Object> findMassarrayExtensionTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<MassarrayExtensionTaskItem> list = (List<MassarrayExtensionTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findMassarrayExtensionTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskInfoList(scId, startNum, limitNum, dir, sort);
		List<MassarrayExtensionTaskInfo> list = (List<MassarrayExtensionTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 模板
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskTemplateItemList(scId, startNum, limitNum, dir, sort);
		List<MassarrayExtensionTaskTemplate> list = (List<MassarrayExtensionTaskTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findReagentItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskTemplateReagentList(scId, startNum, limitNum, dir,
						sort);
		List<MassarrayExtensionTaskReagent> list = (List<MassarrayExtensionTaskReagent>) result.get("list");
		return result;
	}

	/**
	 * 根据步骤加载试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskTemplateReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<MassarrayExtensionTaskReagent> list = (List<MassarrayExtensionTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskTemplateCosListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<MassarrayExtensionTaskCos> list = (List<MassarrayExtensionTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskTemplateCosList(scId, startNum, limitNum, dir, sort);
		List<MassarrayExtensionTaskCos> list = (List<MassarrayExtensionTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMassarrayExtensionTaskItem(MassarrayExtensionTask sc, String itemDataJson)
			throws Exception {
		List<MassarrayExtensionTaskItem> saveItems = new ArrayList<MassarrayExtensionTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = massarrayExtensionTaskDao.get(
						StorageContainer.class, temp.getStorageContainer()
								.getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();
			}
		}
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskItem scp = new MassarrayExtensionTaskItem();
			// 将map信息读入实体类
			scp = (MassarrayExtensionTaskItem) massarrayExtensionTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num = Math.floor((scp.getOrderNumber() - 1)
							/ rowCode) + 1;
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
							/ (rowCode * colCode) + 1));
				}
			}
			scp.setMassarrayExtensionTask(sc);

			if (scp != null) {
				Template t = commonDAO.get(Template.class, sc.getTemplate()
						.getId());
				if (t != null) {
					if (t.getDicSampleType() != null) {
						DicSampleType d = commonDAO.get(DicSampleType.class, t
								.getDicSampleType().getId());
						if (d != null) {
							if (scp.getDicSampleType() == null
									|| (scp.getDicSampleType() != null && scp
											.getDicSampleType().equals(""))) {
								scp.setDicSampleType(d);
							}
						}
					}
					if (t.getProductNum() != null) {
						if (scp.getProductNum() == null
								|| (scp.getProductNum() != null && scp
										.getProductNum().equals(""))) {
							scp.setProductNum(t.getProductNum());
						}
					}
					if (t.getSampleNum() != null) {
						if (scp.getSampleConsume() == null) {
							scp.setSampleConsume(t.getSampleNum());
						}
					}
				}
			}
			saveItems.add(scp);
			if (scp != null) {
				MassarrayExtensionTaskInfoManager dm = new MassarrayExtensionTaskInfoManager();
				dm.setExpCode(scp.getExpCode());
				dm.setCode(scp.getCode());
				dm.setSampleCode(scp.getSampleCode());
				dm.setSampleId(scp.getTempId());
				dm.setSampleName(scp.getSampleName());
				dm.setSampleVolume(scp.getSampleVolume());
				dm.setAddVolume(scp.getAddVolume());
				dm.setSumVolume(scp.getSumVolume());
				dm.setIndexs(scp.getIndexs());
				dm.setConcentration(scp.getConcentration());
				dm.setReason(scp.getReason());
				dm.setResult(scp.getResult());
				dm.setStepNum(scp.getStepNum());
				dm.setPatientName(scp.getPatientName());
				dm.setProductId(scp.getProductId());
				dm.setProductName(scp.getProductName());
				dm.setPhone(scp.getPhone());
				dm.setReportDate(scp.getReportDate());
				dm.setIdCard(scp.getIdCard());
				dm.setInspectDate(scp.getInspectDate());
				dm.setOrderId(scp.getOrderId());
				dm.setSequenceFun(scp.getSequenceFun());
				massarrayExtensionTaskDao.saveOrUpdate(dm);
			}
		}
		massarrayExtensionTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(massarrayExtensionTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMassarrayExtensionTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			if (id != null) {
				MassarrayExtensionTaskItem scp = massarrayExtensionTaskDao
						.get(MassarrayExtensionTaskItem.class, id);
				massarrayExtensionTaskDao.delete(scp);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMassarrayExtensionTaskResult(MassarrayExtensionTask sc, String itemDataJson)
			throws Exception {
		DecimalFormat df = new DecimalFormat("######.000");
		List<MassarrayExtensionTaskInfo> saveItems = new ArrayList<MassarrayExtensionTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskInfo scp = new MassarrayExtensionTaskInfo();
			// 将map信息读入实体类
			scp = (MassarrayExtensionTaskInfo) massarrayExtensionTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMassarrayExtensionTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						|| !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());
					markCode = scp.getCode() + d.getCode();
				}
				String code = codingRuleService.getCode("MassarrayExtensionTaskInfo",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			Double b;
			if (sc.getAcceptUser().getId().equals("D0004")) {
				// MassarrayExtension任务实验组-组织
				if (scp.getNextFlowId().equals("0022")) {
					// 超声破碎
					if (scp.getQbcontraction() < 11) {
						scp.setVolume(180.0);
					} else if (scp.getQbcontraction() >= 11
							&& scp.getQbcontraction() < 15) {
						b = 2000 / scp.getQbcontraction();
						scp.setVolume(Double.valueOf(df.format(b)));
					} else if (scp.getQbcontraction() >= 15
							&& scp.getQbcontraction() <= 22) {
						scp.setVolume(90.0);
					} else if (scp.getQbcontraction() > 22) {
						b = 2000 / scp.getQbcontraction();
						scp.setVolume(Double.valueOf(df.format(b)));
					}
				} else if (scp.getNextFlowId().equals("0021")) {// QPCR
					scp.setVolume(1.0);
				} else if (scp.getNextFlowId().equals("0009")) {// 样本入库

				}
			}
			if (scp.getVolume() != null) {
				if (scp.getQbcontraction() != null) {
					b = scp.getVolume() * scp.getQbcontraction();
					scp.setSampleNum(Double.valueOf(df.format(b)));
				}
				if (scp.getQbcontraction() == null
						&& scp.getContraction() != null) {
					b = scp.getVolume() * scp.getContraction();
					scp.setSampleNum(Double.valueOf(df.format(b)));
				}
			}
			List<SampleInfo> sil = massarrayExtensionTaskDao.findByProperty(
					SampleInfo.class, "code", scp.getCode());
			if (sil.size() > 0) {
				SampleInfo si = sil.get(0);
				scp.setSampleInfo(si);
			}
			massarrayExtensionTaskDao.saveOrUpdate(scp);
		}
		// 计算出组织实验组入库样本的体积
		if (sc.getAcceptUser().getId().equals("D0004")) {
			// MassarrayExtension任务实验组-组织
			// 根据主表id查询结果表为入库样本的信息
			List<MassarrayExtensionTaskInfo> list1 = this.massarrayExtensionTaskDao
					.setMassarrayExtensionTaskInfo(sc.getId());
			for (MassarrayExtensionTaskInfo s : list1) {
				// 根据入库的原始样本号查询该结果表信息
				List<MassarrayExtensionTaskInfo> list2 = this.massarrayExtensionTaskDao
						.setMassarrayExtensionTaskInfo2(sc.getId(), s.getCode());
				Double v = 0.0;
				for (MassarrayExtensionTaskInfo s2 : list2) {
					if (!s2.getNextFlowId().equals("0009")
							&& s2.getVolume() != null) {
						v += s2.getVolume();
					}
				}
				s.setVolume(Double.valueOf(df.format(200 - v)));
				if (s.getVolume() != null) {
					if (s.getQbcontraction() != null) {
						s.setSampleNum(Double.valueOf(df.format(s.getVolume()
								* s.getQbcontraction())));
					}
					if (s.getQbcontraction() == null
							&& s.getContraction() != null) {
						s.setSampleNum(Double.valueOf(df.format(s.getVolume()
								* s.getContraction())));
					}
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMassarrayExtensionTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			MassarrayExtensionTaskInfo scp = massarrayExtensionTaskDao
					.get(MassarrayExtensionTaskInfo.class, id);
			if (scp.getSubmit() == null)
				massarrayExtensionTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(MassarrayExtensionTask sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			massarrayExtensionTaskDao.saveOrUpdate(sc);
//			if (sc.getTemplate() != null) {
//				String id = sc.getTemplate().getId();
//				templateService.setCosIsUsedSave(id);
//			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("massarrayExtensionTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMassarrayExtensionTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("massarrayExtensionTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMassarrayExtensionTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("massarrayExtensionTaskTemplateItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTemplateItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("massarrayExtensionTaskTemplateReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				re=saveReagentItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("massarrayExtensionTaskTemplateCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				in=saveCosItem(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTemplateItem(MassarrayExtensionTask sc, String itemDataJson)
			throws Exception {
		List<MassarrayExtensionTaskTemplate> saveItems = new ArrayList<MassarrayExtensionTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskTemplate scp = new MassarrayExtensionTaskTemplate();
			// 将map信息读入实体类
			scp = (MassarrayExtensionTaskTemplate) massarrayExtensionTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMassarrayExtensionTask(sc);
			saveItems.add(scp);
		}
		massarrayExtensionTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItem(String[] ids) throws Exception {
		for (String id : ids) {
			MassarrayExtensionTaskTemplate scp = massarrayExtensionTaskDao.get(
					MassarrayExtensionTaskTemplate.class, id);
			massarrayExtensionTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItemOne(String ids) throws Exception {
		MassarrayExtensionTaskTemplate scp = massarrayExtensionTaskDao.get(MassarrayExtensionTaskTemplate.class,
				ids);
		if (scp != null) {
			massarrayExtensionTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveReagentItem(MassarrayExtensionTask sc, String itemDataJson)
			throws Exception {
		Integer re = 0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskReagent scp = new MassarrayExtensionTaskReagent();
			// 将map信息读入实体类
			scp = (MassarrayExtensionTaskReagent) massarrayExtensionTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getSn() != null && !scp.getSn().equals("")) {
				StorageOutItem soi = storageOutService
						.findStorageOutItem(scp.getSn());
				if (soi != null) {
					if (scp.getCode().equals(soi.getStorage().getId())) {
						scp.setBatch(soi.getCode());
						scp.setExpireDate(soi.getExpireDate());
						re++;
					} else {
						scp.setSn("");
					}
				}
			}
			scp.setMassarrayExtensionTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getNum());
			}
			massarrayExtensionTaskDao.saveOrUpdate(scp);
		}
		return re;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItem(String[] ids) throws Exception {
		for (String id : ids) {
			MassarrayExtensionTaskReagent scp = massarrayExtensionTaskDao.get(MassarrayExtensionTaskReagent.class,
					id);
			massarrayExtensionTaskDao.delete(scp);
		}
	}

	/**
	 * delTemplateReagentOne 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateReagentOne(String ids) throws Exception {
		MassarrayExtensionTaskReagent scp = massarrayExtensionTaskDao.get(MassarrayExtensionTaskReagent.class, ids);
		if (scp != null) {
			massarrayExtensionTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveCosItem(MassarrayExtensionTask sc, String itemDataJson) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Integer saveItem = 0;
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskCos scp = new MassarrayExtensionTaskCos();
			// 将map信息读入实体类
			scp = (MassarrayExtensionTaskCos) massarrayExtensionTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			Instrument ins = null;
			if (scp.getCode() != null && !scp.equals("")) {
				ins = massarrayExtensionTaskDao.get(Instrument.class, scp.getCode());
			}
			if (ins != null
					&& ins.getType().getId().equals(scp.getType().getId())) {
				scp.setName(ins.getName());
				scp.setState(ins.getState().getName());
				saveItem++;
			} else {
				scp.setName("");
				scp.setCode("");
				scp.setState("");
			}
			scp.setMassarrayExtensionTask(sc);

			massarrayExtensionTaskDao.saveOrUpdate(scp);
		}
		return saveItem;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCosItem(String[] ids) throws Exception {
		for (String id : ids) {
			MassarrayExtensionTaskCos scp = massarrayExtensionTaskDao.get(MassarrayExtensionTaskCos.class, id);
			massarrayExtensionTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMassarrayExtensionTaskTemplateCosOne(String ids) throws Exception {
		MassarrayExtensionTaskCos scp = massarrayExtensionTaskDao.get(MassarrayExtensionTaskCos.class, ids);
		if (scp != null) {
			massarrayExtensionTaskDao.delete(scp);
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		MassarrayExtensionTask sct = massarrayExtensionTaskDao.get(MassarrayExtensionTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setReceiveDate(new Date());
		massarrayExtensionTaskDao.update(sct);
		// 提交样本
		submit(id, null);

		// 完成的时候判断设备解除占用
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		String did = sct.getId();
		// 将批次信息反馈到模板中
		List<MassarrayExtensionTaskReagent> list1 = massarrayExtensionTaskDao.setReagentList(did);
		for (MassarrayExtensionTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); 
			// MassarrayExtensionTask实验中试剂的批次
			String drid = dt.gettReagent(); 
			// MassarrayExtensionTask实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
		}
		// 将状态信息反馈到开箱检验的样本中
		List<MassarrayExtensionTaskItem> eList = massarrayExtensionTaskDao.setMassarrayExtensionTaskItemList(did);
		for (MassarrayExtensionTaskItem edg : eList) {
			String sid = edg.getTempId();
			List<SampleReceiveItem> sList = sampleReceiveDao
					.selectItemList(sid);
			for (SampleReceiveItem sri : sList) {
				sri.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				sri.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE);
			}
			updateTempState(sid);
		}
	}

	/**
	 * 生成结果之后修改MassarrayExtensionTaskMassarrayExtension任务中间表的样本状态
	 * @param code
	 */
	public void updateTempState(String code) {
		MassarrayExtensionTaskTemp dt = commonDAO.get(MassarrayExtensionTaskTemp.class, code);
		if (dt != null) {
			dt.setState("2");
		}
	}

	/**
	 * 到QPCR的样本或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findExperimentMassarrayExtensionTaskReslutList(Integer startNum,
			Integer limitNum, String dir, String sort, String state)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectExperimentMassarrayExtensionTaskReslutList(startNum, limitNum, dir,
						sort, state);
		List<MassarrayExtensionTaskInfo> list = (List<MassarrayExtensionTaskInfo>) result.get("list");

		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMassarrayExtensionTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao
				.selectMassarrayExtensionTaskFromReceiveList(scId, startNum, limitNum, dir,
						sort);
		List<MassarrayExtensionTaskReceiveItem> list = (List<MassarrayExtensionTaskReceiveItem>) result.get("list");
		return result;
	}

	public Map<String, Object> selectMassarrayExtensionTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskDao.selectMassarrayExtensionTaskAbnormalList(
				scId, startNum, limitNum, dir, sort);
		List<MassarrayExtensionTaskInfo> list = (List<MassarrayExtensionTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = massarrayExtensionTaskDao.setReagent(id, code);
		List<MassarrayExtensionTaskReagent> list = (List<MassarrayExtensionTaskReagent>) result.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			for (MassarrayExtensionTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());
				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}
				if (ti.getNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}
				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				if(ti.getExpireDate()!=null){
					map.put("expireDate", sdf.format(ti.getExpireDate()));
				}
				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getMassarrayExtensionTask().getId());
				map.put("tName", ti.getMassarrayExtensionTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = massarrayExtensionTaskDao.setCos(id, code);
		List<MassarrayExtensionTaskCos> list = (List<MassarrayExtensionTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (MassarrayExtensionTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}
				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}
				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				if (ti.getType() != null) {
					map.put("typeId", ti.getType().getId());
					map.put("typeName", ti.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getMassarrayExtensionTask().getId());
				map.put("tName", ti.getMassarrayExtensionTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	
	public List<MassarrayExtensionTaskItem> findMassarrayExtensionTaskItemList(String scId) throws Exception {
		List<MassarrayExtensionTaskItem> list = massarrayExtensionTaskDao.selectMassarrayExtensionTaskItemList(scId);
		return list;
	}

	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submit(String id, String[] ids) throws Exception {
		// 获取主表信息
		MassarrayExtensionTask sc = this.massarrayExtensionTaskDao.get(MassarrayExtensionTask.class, id);
		// 获取结果表样本信息
		List<MassarrayExtensionTaskInfo> list;
		if (ids == null)
			list = this.massarrayExtensionTaskDao.setMassarrayExtensionTaskResultById(id);
		else
			list = this.massarrayExtensionTaskDao.setMassarrayExtensionTaskResultByIds(ids);
		for (MassarrayExtensionTaskInfo scp : list) {
			if (scp != null) {
				if (scp.getNextFlowId() != null && scp.getResult() != null
						&& scp.getSubmit() == null) {
					if (!scp.getResult().equals("")
							&& !scp.getNextFlowId().equals("")) {
						String isOk = scp.getResult();
						String nextFlowId = scp.getNextFlowId();NextFlow nf = commonService.get(NextFlow.class, nextFlowId);nextFlowId = nf.getSysCode();
						if (isOk.equals("1")) {
							if (nextFlowId.equals("0009")) {
								// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setNum(scp.getSampleNum());
								st.setSampleType(scp.getSampleType());
								st.setSampleTypeId(scp.getDicSampleType()
										.getId());
								st.setInfoFrom("MassarrayExtensionTaskInfo");
								st.setState("1");
								SampleInfo s = this.sampleInfoMainDao
										.findSampleInfo(scp.getSampleCode());
								st.setPatientName(s.getPatientName());
								massarrayExtensionTaskDao.saveOrUpdate(st);
							} else if (nextFlowId.equals("0010")) {
								// 重MassarrayExtension任务
								MassarrayExtensionTaskAbnormal eda = new MassarrayExtensionTaskAbnormal();
								scp.setState("4");
								// 状态为4 在重MassarrayExtension任务中显示
								sampleInputService.copy(eda, scp);
							} else if (nextFlowId.equals("0011")) {
								// 重建库
								WkTaskAbnormal wka = new WkTaskAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0024")) {
								// 重抽血
								PlasmaAbnormal wka = new PlasmaAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0012")) {// 暂停
							} else if (nextFlowId.equals("0013")) {// 终止
							} else if (nextFlowId.equals("0018")) {// 文库构建
								WkTaskTemp d = new WkTaskTemp();
								scp.setState("1");
								sampleInputService.copy(d, scp);
//								d.setReportDate(DateUtil.parse(scp
//										.getReportDate()));
								massarrayExtensionTaskDao.saveOrUpdate(d);
							} else if (nextFlowId.equals("0020")) {// 2100质控
								Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
								scp.setState("1");
								sampleInputService.copy(qc2100, scp);
								qc2100.setWkType("2");
								massarrayExtensionTaskDao.saveOrUpdate(qc2100);
							} else if (nextFlowId.equals("0021")) {// QPCR质控
								QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
								scp.setState("1");
								sampleInputService.copy(qpcr, scp);
								qpcr.setWkType("1");
								massarrayExtensionTaskDao.saveOrUpdate(qpcr);
							} else if (nextFlowId.equals("0022")) {// 超声破碎
								UfTaskTemp uf = new UfTaskTemp();
								scp.setState("1");
								sampleInputService.copy(uf, scp);
							} else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(nextFlowId);
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}

						} else {// 不合格
							if (nextFlowId.equals("0014")) {// 反馈项目组
								// 下一步流向是：反馈至项目组
								
							} else {
								// 提交的不合格样本到异常管理
								MassarrayExtensionTaskAbnormal eda = new MassarrayExtensionTaskAbnormal();
								scp.setState("2");// 状态为2 在MassarrayExtension任务异常中显示
								sampleInputService.copy(eda, scp);
							}

						}
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										sc.getCreateDate(),
										format.format(new Date()),
										"MassarrayExtensionTask",
										"MassarrayExtension任务",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), scp.getNextFlow(),
										scp.getResult(), null, null, null,
										null, null, null, null, null);
						scp.setSubmit("1");
						commonService.saveOrUpdate(scp);
					}
				}
			}
		}
	}
}