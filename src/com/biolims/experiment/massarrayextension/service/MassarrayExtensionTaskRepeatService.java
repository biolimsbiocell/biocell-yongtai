package com.biolims.experiment.massarrayextension.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.massarrayextension.dao.MassarrayExtensionTaskRepeatDao;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskAbnormal;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskInfo;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MassarrayExtensionTaskRepeatService {
	@Resource
	private MassarrayExtensionTaskRepeatDao massarrayExtensionTaskRepeatDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findRepeatMassarrayExtensionTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return massarrayExtensionTaskRepeatDao.selectRepeatPlasmaList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	


	/**
	 * 保存重MassarrayExtension任务
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMassarrayExtensionTaskRepeatList(String itemDataJson) throws Exception {
		List<MassarrayExtensionTaskAbnormal> saveItems = new ArrayList<MassarrayExtensionTaskAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskAbnormal sbi = new MassarrayExtensionTaskAbnormal();
			sbi = (MassarrayExtensionTaskAbnormal) massarrayExtensionTaskRepeatDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			if (sbi != null && sbi.getResult() != null
					&& sbi.getIsExecute() != null) {// 如果处理意见不为null，到MassarrayExtension任务异常。
				if (sbi.getIsExecute().equals("1")) {
					if (sbi.getResult().equals("1")) {// 重新MassarrayExtension任务
						MassarrayExtensionTaskTemp dst = new MassarrayExtensionTaskTemp();
						dst.setCode(sbi.getCode());
						dst.setCode(sbi.getCode());
						dst.setSequenceFun(sbi.getSequenceFun());
						dst.setPatientName(sbi.getPatientName());
						dst.setProductName(sbi.getProductName());
						dst.setProductId(sbi.getProductId());
						dst.setIdCard(sbi.getIdCard());
						dst.setInspectDate(sbi.getInspectDate());
						dst.setPhone(sbi.getPhone());
						dst.setOrderId(sbi.getOrderId());
						dst.setReportDate(sbi.getReportDate());
						dst.setState("1");
						dst.setClassify(sbi.getClassify());
						dst.setCode(sbi.getCode());
						this.massarrayExtensionTaskRepeatDao.saveOrUpdate(dst);
						sbi.setState("1");
					} else {// 不合格
						sbi.setState("2");
						massarrayExtensionTaskRepeatDao.saveOrUpdate(sbi);
					}
				}
			}
			saveItems.add(sbi);
		}
		massarrayExtensionTaskRepeatDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 根据条件检索数据
	 * @param experiment.massarrayExtensionCode
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> selectMassarrayExtensionTaskRepeat(String code,
			String sampleCode) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = massarrayExtensionTaskRepeatDao.selectRepeat(
				code, sampleCode);
		List<MassarrayExtensionTaskInfo> list = (List<MassarrayExtensionTaskInfo>) result.get("list");
		if (list != null && list.size() > 0) {
			for (MassarrayExtensionTaskInfo srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("code", srai.getCode());
				map.put("sampleCode", srai.getSampleCode());
				map.put("isExecute", srai.getIsExecute());
				map.put("nextFlow", srai.getNextFlow());
				map.put("note", srai.getNote());
				map.put("method", srai.getMethod());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public SampleInfo findSampleInfo(String id) {
		SampleInfo findSampleInfoById = massarrayExtensionTaskRepeatDao
				.findSampleInfoById(id);
		return findSampleInfoById;
	}

	public SampleInputTemp findSampleInputTemp(String id) {
		SampleInputTemp findSampleInputTempById = massarrayExtensionTaskRepeatDao
				.findSampleInputTempById(id).get(0);
		return findSampleInputTempById;
	}
}
