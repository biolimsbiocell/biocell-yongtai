package com.biolims.experiment.massarrayextension.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.massarrayextension.dao.MassarrayExtensionTaskReceiveDao;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceive;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveItem;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskReceiveTemp;
import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MassarrayExtensionTaskReceiveService {
	@Resource
	private MassarrayExtensionTaskReceiveDao massarrayExtensionTaskReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findMassarrayExtensionTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return massarrayExtensionTaskReceiveDao.selectMassarrayExtensionTaskReceiveList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MassarrayExtensionTaskReceive i) throws Exception {
		massarrayExtensionTaskReceiveDao.saveOrUpdate(i);
	}

	public MassarrayExtensionTaskReceive get(String id) {
		MassarrayExtensionTaskReceive massarrayExtensionTaskReceive = commonDAO.get(MassarrayExtensionTaskReceive.class, id);
		return massarrayExtensionTaskReceive;
	}

	public Map<String, Object> findMassarrayExtensionTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = massarrayExtensionTaskReceiveDao
				.selectMassarrayExtensionTaskReceiveItemList(scId, startNum, limitNum,
						dir, sort);
		List<MassarrayExtensionTaskReceiveItem> list = (List<MassarrayExtensionTaskReceiveItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMassarrayExtensionTaskReceiveItem(MassarrayExtensionTaskReceive sc, String itemDataJson)
			throws Exception {
		List<MassarrayExtensionTaskReceiveItem> saveItems = new ArrayList<MassarrayExtensionTaskReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MassarrayExtensionTaskReceiveItem scp = new MassarrayExtensionTaskReceiveItem();
			// 将map信息读入实体类
			scp = (MassarrayExtensionTaskReceiveItem) massarrayExtensionTaskReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMassarrayExtensionTaskReceive(sc);
			saveItems.add(scp);
		}
		massarrayExtensionTaskReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMassarrayExtensionTaskReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			MassarrayExtensionTaskReceiveItem scp = massarrayExtensionTaskReceiveDao.get(
					MassarrayExtensionTaskReceiveItem.class, id);
			massarrayExtensionTaskReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MassarrayExtensionTaskReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			massarrayExtensionTaskReceiveDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("massarrayExtensionTaskReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMassarrayExtensionTaskReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		MassarrayExtensionTaskReceive sct = massarrayExtensionTaskReceiveDao.get(MassarrayExtensionTaskReceive.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		this.massarrayExtensionTaskReceiveDao.update(sct);
		try {
			List<MassarrayExtensionTaskReceiveItem> edri = this.massarrayExtensionTaskReceiveDao
					.selectReceiveByIdList(sct.getId());
			for (MassarrayExtensionTaskReceiveItem ed : edri) {
				if (ed.getMethod().equals("1")) {
					MassarrayExtensionTaskTemp dst = new MassarrayExtensionTaskTemp();
					dst.setConcentration(ed.getConcentration());
					dst.setLocation(ed.getLocation());
					dst.setCode(ed.getCode());
					dst.setId(ed.getId());
					dst.setSequenceFun(ed.getSequenceFun());
					dst.setPatientName(ed.getPatientName());
					dst.setProductName(ed.getProductName());
					dst.setProductId(ed.getProductId());
					dst.setIdCard(ed.getIdCard());
					dst.setInspectDate(ed.getInspectDate());
					dst.setPhone(ed.getPhone());
					dst.setOrderId(ed.getOrderId());
					dst.setReportDate(ed.getReportDate());
					dst.setState("1");
					dst.setClassify(ed.getClassify());
					dst.setCode(ed.getCode());
					dst.setSampleType(ed.getSampleType());
					dst.setSampleNum(ed.getSampleNum());
					dst.setLabCode(ed.getLabCode());
					massarrayExtensionTaskReceiveDao.saveOrUpdate(dst);
					MassarrayExtensionTaskReceiveTemp temp = commonDAO.get(MassarrayExtensionTaskReceiveTemp.class,
							ed.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}
				// 审批完成改变Info中的样本状态为“待MassarrayExtension任务MassarrayExtension任务”
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(ed
						.getCode());
				if (sf != null) {
					sf.setState("3");
					sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
				}
				ed.setState("1");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 审批完成之后，将明细表数据保存到MassarrayExtensionTaskMassarrayExtension任务中间表
	 * @param code
	 */
	public void saveToMassarrayExtensionTaskTemp(String code) {
		List<MassarrayExtensionTaskReceiveItem> list = massarrayExtensionTaskReceiveDao
				.selectReceiveItemListById(code);
		if (list.size() > 0) {
			for (MassarrayExtensionTaskReceiveItem ed : list) {
				MassarrayExtensionTaskTemp dst = new MassarrayExtensionTaskTemp();
				dst.setConcentration(ed.getConcentration());
				dst.setLocation(ed.getLocation());
				dst.setCode(ed.getCode());
				dst.setId(ed.getId());
				dst.setSequenceFun(ed.getSequenceFun());
				dst.setPatientName(ed.getPatientName());
				dst.setProductName(ed.getProductName());
				dst.setProductId(ed.getProductId());
				dst.setIdCard(ed.getIdCard());
				dst.setInspectDate(ed.getInspectDate());
				dst.setPhone(ed.getPhone());
				dst.setOrderId(ed.getOrderId());
				dst.setReportDate(ed.getReportDate());
				dst.setState("1");
				dst.setClassify(ed.getClassify());
				dst.setSampleCode(ed.getSampleCode());
				dst.setSampleType(ed.getSampleType());
				dst.setSampleNum(ed.getSampleNum());
				massarrayExtensionTaskReceiveDao.saveOrUpdate(dst);
			}
		}
	}
		public Map<String, Object> selectMassarrayExtensionTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = massarrayExtensionTaskReceiveDao.selectMassarrayExtensionTaskReceiveTempList(
				startNum, limitNum, dir, sort);
		List<MassarrayExtensionTaskReceiveTemp> list = (List<MassarrayExtensionTaskReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
	
}
