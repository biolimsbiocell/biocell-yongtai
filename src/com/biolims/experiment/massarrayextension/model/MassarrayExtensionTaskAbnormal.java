package com.biolims.experiment.massarrayextension.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: MassarrayExtension任务异常
 * @author lims-platform
 * @date 2015-11-27 15:41:24
 * @version V1.0
 * 
 */
@Entity
@Table(name = "MASSARRAY_E_TASK_ABNORMAL")
@SuppressWarnings("serial")
public class MassarrayExtensionTaskAbnormal extends EntityDao<MassarrayExtensionTaskAbnormal> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本编号 */
	private String code;
	/** 病人姓名 */
	private String patientName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 检测方法 */
	private String sequenceFun;
	/** 取样时间 */
	private String inspectDate;
	/** 身份证号 */
	private String idCard;
	/** 应出报告日期 */
	private String reportDate;
	/** 手机号 */
	private String phone;
	/** 任务单*/
	private String orderId;
	/** 样本类型 */
	private String sampleType;
	/** 样本情况 */
	private String sampleCondition;
	/** 结果判定 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理意见 */
	private String method;
	/** 确认执行 */
	private String isExecute;
	/** 反馈时间 */
	private String feedbackTime;
	/** 备注 */
	private String note;
	/** 描述 */
	private String name;
	/** 状态 */
	private String state;
	/** 区分临床还是科技服务 0 临床 1 科技服务*/
	private String classify;
	/** 样本数量*/
	private Double sampleNum;

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "SAMPLE_TYPE", length = 100)
	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	@Column(name = "SAMPLE_CONDITION", length = 100)
	public String getSampleCondition() {
		return sampleCondition;
	}

	public void setSampleCondition(String sampleCondition) {
		this.sampleCondition = sampleCondition;
	}

	@Column(name = "METHOD", length = 100)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "NEXT_FLOW", length = 100)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "IS_EXECUTE", length = 100)
	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	@Column(name = "FEEDBACK_TIME", length = 100)
	public String getFeedbackTime() {
		return feedbackTime;
	}

	public void setFeedbackTime(String feedbackTime) {
		this.feedbackTime = feedbackTime;
	}

	@Column(name = "NOTE", length = 100)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "NAME", length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

}