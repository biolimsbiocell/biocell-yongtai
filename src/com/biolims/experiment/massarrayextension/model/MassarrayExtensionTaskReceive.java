package com.biolims.experiment.massarrayextension.model;

import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;

/**   
 * @Title: Model
 * @Description: 样本接收
 * @author lims-platform
 * @date 2015-11-27 15:41:43
 * @version V1.0   
 *
 */
@Entity
@Table(name = "MASSARRAY_E_TASK_RECEIVE")
@SuppressWarnings("serial")
public class MassarrayExtensionTaskReceive extends EntityDao<MassarrayExtensionTaskReceive> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	/**接收人*/
	private User receiveUser;
	/**样本类型*/
	private String sampleType;
	/**接收日期*/
	private String receiverDate;
	/**样本情况*/
	private String sampleCondition;
	/**结果判定*/
	private String method;
	/**储位提示*/
	private String location;
	/**下一步流向*/
	private String nextFlow;
	/**工作流状态*/
	private String stateName;
	/**处理意见*/
	private String advice;
	/**状态*/
	private String state;
	/**确认执行*/
	private String isExecute;
	/**反馈时间*/
	private String feedbackTime;
	/**备注*/
	private String note;
	
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	
	/**
	 *方法: 取得User
	 *@return: User  接收人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECEIVE_USER")
	public User getReceiveUser(){
		return this.receiveUser;
	}
	
	/**
	 *方法: 设置User
	 *@param: User  接收人
	 */
	public void setReceiveUser(User receiveUser){
		this.receiveUser = receiveUser;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  样本类型
	 */
	@Column(name ="SAMPLE_TYPE", length = 100)
	public String getSampleType(){
		return this.sampleType;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  样本类型
	 */
	public void setSampleType(String sampleType){
		this.sampleType = sampleType;
	}
	
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="RECEIVER_DATE", length = 50)
	public String getReceiverDate(){
		return this.receiverDate;
	}
	
	/**
	 *方法: 设置Date
	 *@param: Date  接收日期
	 */
	public void setReceiverDate(String receiverDate){
		this.receiverDate = receiverDate;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  样本情况
	 */
	@Column(name ="SAMPLE_CONDITION", length = 50)
	public String getSampleCondition(){
		return this.sampleCondition;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  样本情况
	 */
	public void setSampleCondition(String sampleCondition){
		this.sampleCondition = sampleCondition;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="METHOD", length = 50)
	public String getMethod(){
		return this.method;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setMethod(String method){
		this.method = method;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  储位提示
	 */
	@Column(name ="LOCATION", length = 50)
	public String getLocation(){
		return this.location;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  储位提示
	 */
	public void setLocation(String location){
		this.location = location;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  处理意见
	 */
	@Column(name ="ADVICE", length = 50)
	public String getAdvice(){
		return this.advice;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  处理意见
	 */
	public void setAdvice(String advice){
		this.advice = advice;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  确认执行
	 */
	@Column(name ="IS_EXECUTE", length = 50)
	public String getIsExecute(){
		return this.isExecute;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  确认执行
	 */
	public void setIsExecute(String isExecute){
		this.isExecute = isExecute;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  反馈时间
	 */
	@Column(name ="FEEDBACK_TIME", length = 50)
	public String getFeedbackTime(){
		return this.feedbackTime;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  反馈时间
	 */
	public void setFeedbackTime(String feedbackTime){
		this.feedbackTime = feedbackTime;
	}
	
	/**
	 *方法: 取得Date
	 *@return: Date  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	
	/**
	 *方法: 设置Date
	 *@param: Date  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	
	
}