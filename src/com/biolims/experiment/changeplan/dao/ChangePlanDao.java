package com.biolims.experiment.changeplan.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.changeplan.model.ChangePlan;

@Repository
@SuppressWarnings("unchecked")
public class ChangePlanDao extends BaseHibernateDao {
	public Map<String, Object> findChangePlanTable(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ChangePlan where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ChangePlan where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<ChangePlan> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

//	public List<ChangePlan> commitState(String ys, String qa, String qc, String sc, String pzr, String rq) {
//		
//		List<ChangePlan> list = new ArrayList<ChangePlan>();
//		String hql = "from ChangePlan where 1=1 ";
//		list = getSession().createQuery(hql).list();
//		return list;
//	}

//	public Map<String, Object> showQaAuditItemTableJson(Integer start, Integer length, String query, String col,
//			String sort, String id) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String countHql = "select count(*) from QaAuditItem where 1=1 and qaAudit.id='" + id + "'";
//		String key = "";
//		if (query != null && !"".equals(query)) {
//			key = map2Where(query);
//		}
//		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//		if (0l != sumCount) {
//			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//			String hql = "from QaAuditItem where 1=1 and qaAudit.id='" + id + "'";
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				col = col.replace("-", ".");
//				key += " order by " + col + " " + sort;
//			}
//			List<QaAuditItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
//					.list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//		}
//		return map;
//	}
//	
//	public Map<String, Object> showQaAuditStorageItemTableJson(Integer start, Integer length, String query, String col,
//			String sort, String id) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String countHql = "select count(*) from QaAuditStorageItem where 1=1 and qaAudit.id='" + id + "'";
//		String key = "";
//		if (query != null && !"".equals(query)) {
//			key = map2Where(query);
//		}
//		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//		if (0l != sumCount) {
//			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
//			String hql = "from QaAuditStorageItem where 1=1 and qaAudit.id='" + id + "'";
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				col = col.replace("-", ".");
//				key += " order by " + col + " " + sort;
//			}
//			List<QaAuditStorageItem> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
//					.list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//		}
//		return map;
//	}
}