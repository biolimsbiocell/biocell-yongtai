package com.biolims.experiment.changeplan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 变更申请
 * @author lims-platform
 * @date 2015-11-22 17:06:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CHANGE_PLAN")
@SuppressWarnings("serial")
public class ChangePlan extends EntityDao<ChangePlan> implements java.io.Serializable {
	/** 变更申请单号 */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 医事服务部 */
	private User ysComfirmUser;
	/** QA审核人 */
	private User qaConfirmUser;
	/** QC审核人 */
	private User qcConfirmUser;
	/** 生产审核人 */
	private User productConfirmUser;
	/** 完成时间 */
	private Date confirmDate;
	/** 工作流状态 */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 变更类型 */
	private DicType changeType;
	/** 变更任务单单号 */
	private String changeTaskId;
	/** 批次号 */
	private String batch;
	/** 变更内容 */
	private String changeContent;
	/**筛选号*/
	private String filtrateCode;
	/** 回输计划修改时间  */
	private String reinfusionPlanDateUpdate;
	/**审核意见*/
	private String scAdvice;
	private String qaAdvice;
	private String qcAdvice;
	
	/** 批准人 */
	private User approverUser;
	/**批准人审核意见*/
	private String approverUserAdvice;
	/**样本状态*/
	private String sampleState;
	private String dateRiQi;
	
	
	
	
	
	
	public String getDateRiQi() {
		return dateRiQi;
	}

	public void setDateRiQi(String dateRiQi) {
		this.dateRiQi = dateRiQi;
	}

	public String getSampleState() {
		return sampleState;
	}

	public void setSampleState(String sampleState) {
		this.sampleState = sampleState;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "approver_user")
	public User getApproverUser() {
		return approverUser;
	}

	public void setApproverUser(User approverUser) {
		this.approverUser = approverUser;
	}

	public String getApproverUserAdvice() {
		return approverUserAdvice;
	}

	public void setApproverUserAdvice(String approverUserAdvice) {
		this.approverUserAdvice = approverUserAdvice;
	}

	public String getScAdvice() {
		return scAdvice;
	}

	public void setScAdvice(String scAdvice) {
		this.scAdvice = scAdvice;
	}

	public String getQaAdvice() {
		return qaAdvice;
	}

	public void setQaAdvice(String qaAdvice) {
		this.qaAdvice = qaAdvice;
	}

	public String getQcAdvice() {
		return qcAdvice;
	}

	public void setQcAdvice(String qcAdvice) {
		this.qcAdvice = qcAdvice;
	}

	public String getReinfusionPlanDateUpdate() {
		return reinfusionPlanDateUpdate;
	}

	public void setReinfusionPlanDateUpdate(String reinfusionPlanDateUpdate) {
		this.reinfusionPlanDateUpdate = reinfusionPlanDateUpdate;
	}

	public String getFiltrateCode() {
		return filtrateCode;
	}

	public void setFiltrateCode(String filtrateCode) {
		this.filtrateCode = filtrateCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getYsComfirmUser() {
		return ysComfirmUser;
	}

	public void setYsComfirmUser(User ysComfirmUser) {
		this.ysComfirmUser = ysComfirmUser;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getChangeTaskId() {
		return changeTaskId;
	}

	public void setChangeTaskId(String changeTaskId) {
		this.changeTaskId = changeTaskId;
	}

	public String getChangeContent() {
		return changeContent;
	}

	public void setChangeContent(String changeContent) {
		this.changeContent = changeContent;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHANGE_TYPE")
	public DicType getChangeType() {
		return changeType;
	}

	public void setChangeType(DicType changeType) {
		this.changeType = changeType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT_CONFIRM_USER")
	public User getProductConfirmUser() {
		return productConfirmUser;
	}

	public void setProductConfirmUser(User productConfirmUser) {
		this.productConfirmUser = productConfirmUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QC_CONFIRM_USER")
	public User getQcConfirmUser() {
		return qcConfirmUser;
	}

	public void setQcConfirmUser(User qcConfirmUser) {
		this.qcConfirmUser = qcConfirmUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QA_CONFIRM_USER")
	public User getQaConfirmUser() {
		return qaConfirmUser;
	}

	public void setQaConfirmUser(User qaConfirmUser) {
		this.qaConfirmUser = qaConfirmUser;
	}

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             下达日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
}