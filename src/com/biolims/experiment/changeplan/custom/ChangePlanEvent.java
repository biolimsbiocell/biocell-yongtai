package com.biolims.experiment.changeplan.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.changeplan.service.ChangePlanService;

public class ChangePlanEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		ChangePlanService mbService = (ChangePlanService) ctx
				.getBean("changePlanService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
