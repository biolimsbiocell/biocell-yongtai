package com.biolims.experiment.changeplan.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.experiment.changeplan.dao.ChangePlanDao;
import com.biolims.experiment.changeplan.model.ChangePlan;
import com.biolims.experiment.check.model.TechCheckServiceTask;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ChangePlanService {
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ChangePlanDao changePlanDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ChangePlan i) throws Exception {

		changePlanDao.saveOrUpdate(i);

	}

	public ChangePlan get(String id) {
		ChangePlan changePlan = commonDAO.get(ChangePlan.class, id);
		return changePlan;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save1(ChangePlan sc, Map jsonMap) throws Exception {
		if (sc != null) {
			changePlanDao.saveOrUpdate(sc);

			String jsonStr = "";
		}

	}

	public Map<String, Object> findChangePlanTable(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return changePlanDao.findChangePlanTable(start, length, query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(ChangePlan changePlan, String logInfo, String log) throws Exception {// , Map aMap, String
																							// changeLogItem)
		String id = "";
		if (changePlan != null) {
			changePlanDao.saveOrUpdate(changePlan);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(changePlan.getId());
				li.setClassName("ChangePlan");
				li.setModifyContent(logInfo);
				if ("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				} else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
			id = changePlan.getId();
//			String jsonStr = "";
//			jsonStr = (String) aMap.get("qaAuditItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveQaAuditItem(qaAudit, jsonStr, changeLogItem);
//			}
		}
		return id;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		ChangePlan sct = commonDAO.get(ChangePlan.class, id);

		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		sct.setConfirmDate(new Date());
		changePlanDao.saveOrUpdate(sct);
		if (sct.getChangeType() != null && "ReinfusionPlanItem".equals(sct.getChangeType().getCode())) {
			ReinfusionPlanItem rpi = commonDAO.get(ReinfusionPlanItem.class, sct.getChangeTaskId());
			rpi.setState("2");
			rpi.setFeedBack("2");
			rpi.setDiReinfusionPlanDate("1");
			rpi.setModificationTime(rpi.getReinfusionPlanDate());
			rpi.setReinfusionPlanDate(sct.getReinfusionPlanDateUpdate());
			commonDAO.saveOrUpdate(rpi);
		}
		// 回填到预排表状态
		if (sct.getChangeType().getCode().equals("ReinfusionPlanItem2") && sct.getChangeType() != null) {
				List<SampleFutureFind> sff = commonService.get(SampleFutureFind.class, "filtrateCode",
						sct.getFiltrateCode());
				if (!sff.isEmpty()) {
					for (SampleFutureFind sampleFutureFind : sff) {
						sampleFutureFind.setSampleState(sct.getSampleState());
					}
				}
		}

		// 提醒事项生产
		List<User> arrayList = new ArrayList<User>();
		arrayList.add(0, sct.getYsComfirmUser());
		arrayList.add(1, sct.getProductConfirmUser());
		arrayList.add(2, sct.getQaConfirmUser());
		arrayList.add(3, sct.getQcConfirmUser());
		arrayList.add(4, sct.getApproverUser());
		for (int i = 0; i < 5; i++) {
			SysRemind srz = new SysRemind();

			srz.setHandleUser(arrayList.get(i));

			srz.setId(null);
			// 提醒类型
			srz.setType(null);
			// 事件标题
			srz.setTitle("变更申请");
			// 发起人（系统为SYSTEM）
			srz.setRemindUser(u.getName());
			// 发起时间
			srz.setStartDate(new Date());
			// 查阅提醒时间
			srz.setReadDate(new Date());
			// 提醒内容
			srz.setContent("变更申请变更通知");
			// 状态 0.草稿 1.已阅
			srz.setState("0");
			// 提醒的用户
//			srz.setHandleUser(sct.getYsComfirmUser());
//			srz.setHandleUser(sct.getProductConfirmUser());
//			srz.setHandleUser(sct.getQaConfirmUser());
//			srz.setHandleUser(sct.getQcConfirmUser());
//			srz.setHandleUser(sct.getApproverUser());
			// 表单id
			srz.setContentId(sct.getId());
			srz.setTableId("ChangePlan");

			commonDAO.saveOrUpdate(srz);
		}
	}

	/**
	 * 变更申请查询必填项是否为空,为空不可让状态修改
	 * 
	 * @throws Exception
	 */
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public List<ChangePlan> commitState(String ys, String qa, String qc, String sc, String pzr, String rq) {
//			return changePlanDao.commitState(ys,qa,qc,sc,pzr,rq);
//	}

//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveQaAuditItem(QaAudit sc, String itemDataJson, String changeLogItem) throws Exception {
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			QaAuditItem scp = new QaAuditItem();
//			// 将map信息读入实体类
//			scp = (QaAuditItem) qaAuditDao.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			scp.setQaAudit(sc);
//			commonDAO.saveOrUpdate(scp);
//		}
//		if (changeLogItem != null && !"".equals(changeLogItem)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			User u = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			li.setUserId(u.getId());
//			li.setFileId(sc.getId());
//			li.setClassName("QaAuditItem");
//			li.setModifyContent(changeLogItem);
//			commonDAO.saveOrUpdate(li);
//		}
//	}

//	public Map<String, Object> showQaAuditItemTableJson(Integer start, Integer length, String query, String col,
//			String sort, String id) throws Exception {
//		return qaAuditDao.showQaAuditItemTableJson(start, length, query, col, sort, id);
//	}

//	public Map<String, Object> showQaAuditStorageItemTableJson(Integer start, Integer length, String query, String col,
//			String sort, String id) throws Exception {
//		return qaAuditDao.showQaAuditStorageItemTableJson(start, length, query, col, sort, id);
//	}

//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveQaAuditItem(String zid, String item, String logInfo) throws Exception {
//		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
//		QaAudit pt = commonDAO.get(QaAudit.class, zid);
//		for (Map<String, Object> map : list) {
//			QaAuditItem scp = new QaAuditItem();
//			// 将map信息读入实体类
//			scp = (QaAuditItem) qaAuditDao.Map2Bean(map, scp);
//			if (scp.getId() == null || "".equals(scp.getId())) {
//				scp.setId(null);
//			}
//			scp.setQaAudit(pt);
//			commonDAO.saveOrUpdate(scp);
//		}
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			li.setUserId(u.getId());
//			li.setFileId(zid);
//			li.setClassName("QaAuditItem");
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//	}

//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveQaAuditStorageItem(String zid, String item, String logInfo) throws Exception {
//		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
//		QaAudit pt = commonDAO.get(QaAudit.class, zid);
//		for (Map<String, Object> map : list) {
//			QaAuditStorageItem scp = new QaAuditStorageItem();
//			// 将map信息读入实体类
//			scp = (QaAuditStorageItem) qaAuditDao.Map2Bean(map, scp);
//			if (scp.getId() == null || "".equals(scp.getId())) {
//				scp.setId(null);
//			}
//			scp.setQaAudit(pt);
//			commonDAO.saveOrUpdate(scp);
//		}
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			li.setUserId(u.getId());
//			li.setFileId(zid);
//			li.setClassName("QaAuditItem");
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//	}

//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delQaAuditItem(String[] ids) {
//		for (String id : ids) {
//			QaAuditItem scp = qaAuditDao.get(QaAuditItem.class, id);
//			if (scp != null) {
//				qaAuditDao.delete(scp);
//			}
//
//		}
//	}

//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delQaAuditStorageItem(String[] ids) {
//		for (String id : ids) {
//			QaAuditStorageItem scp = qaAuditDao.get(QaAuditStorageItem.class, id);
//			if (scp != null) {
//				qaAuditDao.delete(scp);
//			}
//
//		}
//	}

	// // 查询子表记录
	// public List<SampleOrderItem> querySampleItem(String id) {
	// return this.sampleOrderDao.querySampleItem(id);
	// }
	//
	// public List<SampleOrderPersonnel> querySamplePersonnel(String id) {
	// return this.sampleOrderDao.querySamplePersonnel(id);
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSampleOrderPersonnel(SampleOrder sc, String itemDataJson)
	// throws Exception {
	// List<SampleOrderPersonnel> saveItems = new ArrayList<SampleOrderPersonnel>();
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
	// itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// SampleOrderPersonnel scp = new SampleOrderPersonnel();
	// // 将map信息读入实体类
	// scp = (SampleOrderPersonnel) sampleOrderDao.Map2Bean(map, scp);
	// if (scp.getId() != null) {
	// if (scp.getId().equals(""))
	// scp.setId(null);
	// }
	// scp.setSampleOrder(sc);
	// saveItems.add(scp);
	// }
	// sampleOrderDao.saveOrUpdateAll(saveItems);
	// }
	//
	// /**
	// * 删除明细
	// *
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delSampleOrderPersonnel(String[] ids) throws Exception {
	// for (String id : ids) {
	// SampleOrderPersonnel scp = sampleOrderDao.get(
	// SampleOrderPersonnel.class, id);
	// sampleOrderDao.delete(scp);
	// }
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSampleOrderItem(SampleOrder sc, String itemDataJson)
	// throws Exception {
	// List<SampleOrderItem> saveItems = new ArrayList<SampleOrderItem>();
	// List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
	// itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// SampleOrderItem scp = new SampleOrderItem();
	// // 将map信息读入实体类
	// scp = (SampleOrderItem) sampleOrderDao.Map2Bean(map, scp);
	// if (scp.getId() != null && scp.getId().equals(""))
	// scp.setId(null);
	// scp.setSampleOrder(sc);
	//
	// saveItems.add(scp);
	// }
	// sampleOrderDao.saveOrUpdateAll(saveItems);
	// }
	//
	// /**
	// * 删除明细
	// *
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delSampleOrderItem(String[] ids) throws Exception {
	// for (String id : ids) {
	// SampleOrderItem scp = sampleOrderDao.get(SampleOrderItem.class, id);
	// sampleOrderDao.delete(scp);
	// }
	// }
	//
	// // 根据模板ID加载子表明细
	// public List<Map<String, String>> setTemplateItem(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = this.sampleOrderDao.setTemplateItem(code);
	// List<SampleCancerTempPersonnel> list = (List<SampleCancerTempPersonnel>)
	// result
	// .get("list");
	// if (list != null && list.size() > 0) {
	// for (SampleCancerTempPersonnel ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("checkOutTheAge", ti.getCheckOutTheAge());
	// if (ti.getSampleCancerTemp().getOrderNumber() != null)
	// map.put("sampleorder", ti.getSampleCancerTemp()
	// .getOrderNumber());
	// else
	// map.put("sampleorder", "");
	// if (ti.getTumorCategory() != null)
	// map.put("tumorCategory", ti.getTumorCategory().getId());
	// else
	// map.put("tumorCategory", "");
	// map.put("familyRelation", ti.getFamilyRelation());
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }
	//
	// // 根据模板ID加载子表明细2
	// public List<Map<String, String>> setSampleOrderItem2(String code) {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = this.sampleOrderDao
	// .setSampleOrderItem(code);
	// List<SampleCancerTempItem> list = (List<SampleCancerTempItem>) result
	// .get("list");
	// if (list != null && list.size() > 0) {
	// for (SampleCancerTempItem ti : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", ti.getId());
	// map.put("drugDate", ti.getDrugDate().toString());
	// map.put("useDrugName", ti.getUseDrugName());
	// map.put("effectOfProgress", ti.getEffectOfProgress());
	// map.put("effectOfProgressSpeed", ti.getEffectOfProgressSpeed());
	// map.put("geneticTestHistory", ti.getGeneticTestHistory());
	// map.put("sampleDetectionName", ti.getSampleDetectionName());
	// map.put("sampleExonRegion", ti.getSampleExonRegion());
	// map.put("sampleDetectionResult", ti.getSampleDetectionResult());
	//
	// map.put("sampleOrder", ti.getSampleCancerTemp()
	// .getOrderNumber());
	//
	// mapList.add(map);
	// }
	//
	// }
	// return mapList;
	// }
	//
	// @WriteOperLogTable
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void save(SampleOrder sc, Map jsonMap) throws Exception {
	// if (sc != null) {
	// sampleOrderDao.saveOrUpdate(sc);
	// String jsonStr = "";
	// jsonStr = (String) jsonMap.get("sampleOrderPersonnel");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveSampleOrderPersonnel(sc, jsonStr);
	// }
	// jsonStr = (String) jsonMap.get("sampleOrderItem");
	// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	// saveSampleOrderItem(sc, jsonStr);
	// }
	// }
	// }
	//
	// public SampleCancerTemp fuzZhi(SampleOrder so) throws Exception {
	// SampleCancerTemp a = new SampleCancerTemp();
	// a.setOrderNumber(so.getId());
	// a.setId(so.getId());
	// a.setName(so.getName());
	// a.setGender(so.getGender());
	// a.setBirthDate(so.getBirthDate());
	// a.setDiagnosisDate(DateUtil.parse(so.getDiagnosisDate()));
	// a.setDicType(so.getDicType());
	// a.setSampleStage(so.getSampleStage());
	// a.setInspectionDepartment(so.getInspectionDepartment());
	// a.setCrmProduct(so.getCrmProduct());
	// a.setProductId(so.getProductId());
	// a.setProductName(so.getProductName());
	// a.setSamplingDate(so.getSamplingDate());
	// a.setSamplingLocation(so.getSamplingLocation());
	// a.setSamplingNumber(so.getSamplingNumber());
	// a.setPathologyConfirmed(so.getPathologyConfirmed());
	// // a.setBloodSampleDate(so.getBloodSampleDate().toString());
	// // a.setPlasmapheresisDate(so.getPlasmapheresisDate());
	// a.setCommissioner(so.getCommissioner());
	// a.setReceivedDate(so.getReceivedDate());
	// a.setSampleTypeId(so.getSampleTypeId());
	// a.setSampleTypeName(so.getSampleTypeName());
	// a.setMedicalNumber(so.getMedicalNumber());
	// a.setSampleCode(so.getSampleCode());
	// a.setFamily(so.getFamily());
	// a.setFamilyPhone(so.getFamilyPhone());
	// a.setFamilySite(so.getFamilySite());
	// a.setCrmCustomer(so.getCrmCustomer());
	// a.setMedicalInstitutions(so.getMedicalInstitutions());
	// a.setMedicalInstitutionsPhone(so.getMedicalInstitutionsPhone());
	// a.setMedicalInstitutionsSite(so.getMedicalInstitutionsSite());
	// a.setCrmDoctor(so.getCrmDoctor());
	// a.setAttendingDoctor(so.getAttendingDoctor());
	// a.setAttendingDoctorPhone(so.getAttendingDoctorPhone());
	// a.setAttendingDoctorSite(so.getAttendingDoctorSite());
	// a.setNote(so.getNote());
	// a.setCreateUser(so.getCreateUser());
	// a.setCreateDate(so.getCreateDate());
	// a.setConfirmUser(so.getConfirmUser());
	// a.setConfirmDate(so.getConfirmDate());
	// a.setState(so.getState());
	// a.setStateName(so.getStateName());
	//
	// return a;
	// }
	//
	// @WriteOperLogTable
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void chengeState(String sampleOrderId) throws Exception {
	//
	// SampleOrder sampleOrder = commonDAO.get(SampleOrder.class,
	// sampleOrderId);
	// User user = (User) ServletActionContext.getRequest().getSession()
	// .getAttribute(SystemConstants.USER_SESSION_KEY);
	// CrmPatient r = null;
	// // 电子病历号为空,创建一个
	// if (sampleOrder.getMedicalNumber() != null
	// && !"".equals(sampleOrder.getMedicalNumber())) {
	// r = crmPatientService.get(sampleOrder.getMedicalNumber());
	// // 该电子病历记录存在: 修改电子病历
	// r.setId(sampleOrder.getMedicalNumber());
	// r.setName(sampleOrder.getName());
	// r.setGender(sampleOrder.getGender());
	// r.setDateOfBirth(sampleOrder.getBirthDate());
	// r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
	// r.setCreateDate(sampleOrder.getCreateDate());
	// r.setCustomer(sampleOrder.getCrmCustomer());
	// r.setCustomerDoctor(sampleOrder.getCrmDoctor());
	// r.setCreateUser(sampleOrder.getCreateUser());
	// r.setCancerType(sampleOrder.getCancerType());
	// r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
	// r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
	// r.setKs(sampleOrder.getInspectionDepartment());
	// crmPatientService.save(r);
	//
	// } else {
	// String modelName = "CrmPatient";
	// String markCode = "P";
	// String autoID = codingRuleService.genTransID(modelName, markCode);
	// sampleOrder.setMedicalNumber(autoID);
	// // 该电子记录不存在:添加电子病历
	// r = new CrmPatient();
	// r.setId(autoID);
	// r.setId(sampleOrder.getMedicalNumber());
	// r.setCustomer(sampleOrder.getCrmCustomer());
	// r.setCustomerDoctor(sampleOrder.getCrmDoctor());
	// r.setName(sampleOrder.getName());
	// r.setGender(sampleOrder.getGender());
	// r.setDateOfBirth(sampleOrder.getBirthDate());
	// r.setTelphoneNumber1(sampleOrder.getFamilyPhone());
	// r.setCreateDate(sampleOrder.getCreateDate());
	// r.setCreateUser(sampleOrder.getCreateUser());
	// r.setCancerType(sampleOrder.getCancerType());
	// r.setCancerTypeSeedOne(sampleOrder.getCancerTypeSeedOne());
	// r.setCancerTypeSeedTwo(sampleOrder.getCancerTypeSeedTwo());
	// r.setKs(sampleOrder.getInspectionDepartment());
	// crmPatientService.save(r);
	// }
	// sampleOrder.setConfirmUser(user);
	// sampleOrder.setConfirmDate(new Date());
	// sampleOrder.setState("1");
	// sampleOrder.setStateName("完成");
	//
	// }
	//
	//
	//
	// /**
	// * 根据编号查询代理商
	// */
	// public List<Map<String, Object>> findPrimaryToSample(String code)
	// throws Exception {
	// List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
	// Map<String, Object> result = findPrimary(code);
	// List<PrimaryTask> list = (List<PrimaryTask>) result.get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (PrimaryTask srai : list) {
	// Map<String, Object> map = new HashMap<String, Object>();
	// map.put("id", srai.getId());
	// map.put("name", srai.getName());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }
	//
	// public Map<String, Object> findPrimary(String code) throws Exception {
	// String[] codes = code.split(",");
	//
	// List<PrimaryTask> list = new ArrayList<PrimaryTask>();
	// for (int i = 0; i < codes.length; i++) {
	// PrimaryTask pro = commonDAO.get(PrimaryTask.class, codes[i].trim());
	// list.add(pro);
	// }
	// Map<String, Object> result = new HashMap<String, Object>();
	// result.put("list", list);
	// return result;
	// }
	//
	// //按年查询订单柱状图
	// public List selOrderNumberByYear(String dateYear) {
	// return sampleOrderDao.findOrderNumberByYear(dateYear);
	// }
	// //按年查询订单中检测项目病状图
	// public List selProductByOrderByYear(String dateYear) {
	// return sampleOrderDao.findProductByOrderByYear(dateYear);
	// }
	// //按月查询订单中检测项目病状图
	// public List selProductByOrderByMonth(String dateYearMonth) {
	// return sampleOrderDao.findProductByOrderByMonth(dateYearMonth);
	// }
	// //查询订单中图片路径
	// public List<String> selPic(String id) {
	// return sampleOrderDao.findPic(id);
	// }
	//
	// public List<FileInfo> findFileInfoList(String sId) throws Exception {
	// return sampleOrderDao.selectFileInfoList(sId);
	// }
	//
	// public void getCsvContent(String id, String fileId) throws Exception {
	//
	// FileInfo fileInfo = sampleOrderDao.get(FileInfo.class, fileId);
	// String filepath = fileInfo.getFilePath();
	// File a = new File(filepath);
	// if (a.isFile()) {
	// SampleOrder so=sampleOrderDao.get(SampleOrder.class, id);
	// InputStream is = new FileInputStream(filepath);
	// if (is != null) {
	// CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
	// SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd");
	// reader.readHeaders();// 去除表头
	// while (reader.readRecord()) {
	// SampleOrderItem sampleOrderItem = new SampleOrderItem();
	// sampleOrderItem.setName(reader.get(0));
	// if("男".equals(reader.get(1))){
	// sampleOrderItem.setGender("1");
	// }else if("女".equals(reader.get(1))){
	// sampleOrderItem.setGender("0");
	// }
	// sampleOrderItem.setSampleCode(reader.get(2));
	// sampleOrderItem.setSlideCode(reader.get(3));
	// DicSampleType dst=dicSampleTypeDao.selectDicSampleTypeByName(reader.get(4));
	// sampleOrderItem.setSampleType(dst);
	// sampleOrderItem.setSamplingDate(sdf.parse(reader.get(5)));
	// sampleOrderItem.setSampleOrder(so);
	// sampleOrderDao.saveOrUpdate(sampleOrderItem);
	// }
	// }}
	// }
	//
	//
}
