﻿package com.biolims.experiment.snp.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.snp.model.SnpTaskItem;
import com.biolims.experiment.snp.model.SnpTaskInfo;
import com.biolims.experiment.snp.model.SnpTaskWaitManage;
import com.biolims.experiment.snp.service.SnpTaskManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/snp/snpTaskManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SnpTaskManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "24994";
	@Autowired
	private SnpTaskManageService snpTaskManageService;
	private SnpTaskWaitManage snpTaskManage = new SnpTaskWaitManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showSnpTaskManageEditList")
	public String showSnpTaskManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskManageEdit.jsp");
	}

	@Action(value = "showSnpTaskManageList")
	public String showSnpTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskManage.jsp");
	}

	@Action(value = "showSnpTaskManageListJson")
	public void showSnpTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpTaskManageService
				.findSnpTaskManageList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpTaskInfo> list = (List<SnpTaskInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");

		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("sumVolume", "");
		map.put("rin", "");
		map.put("contraction", "");
		map.put("od260", "");
		map.put("od280", "");
		map.put("tempId", "");
		map.put("isToProject", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("state", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("snpTask-name", "");
		map.put("snpTask-id", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("orderType", "");
		map.put("taskId", "");
		map.put("classify", "");
		map.put("dicType-id", "");
		map.put("dicType-name", "");
		map.put("sampleType", "");
		map.put("labCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "snpTaskManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSnpTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskManageDialog.jsp");
	}

	@Action(value = "showDialogSnpTaskManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSnpTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = snpTaskManageService
				.findSnpTaskManageList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SnpTaskWaitManage> list = (List<SnpTaskWaitManage>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("method", "");
		map.put("nextFlow", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSnpTaskManage")
	public String editSnpTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			snpTaskManage = snpTaskManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "snpTaskManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskManageEdit.jsp");
	}

	@Action(value = "copySnpTaskManage")
	public String copySnpTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		snpTaskManage = snpTaskManageService.get(id);
		snpTaskManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = snpTaskManage.getId();
		if (id != null && id.equals("")) {
			snpTaskManage.setId(null);
		}
		Map aMap = new HashMap();
		snpTaskManageService.save(snpTaskManage, aMap);
		return redirect("/experiment/snp/snpTaskManage/editSnpTaskManage.action?id="
				+ snpTaskManage.getId());

	}

	@Action(value = "viewSnpTaskManage")
	public String toViewSnpTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		snpTaskManage = snpTaskManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskManageEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SnpTaskManageService getSnpTaskManageService() {
		return snpTaskManageService;
	}

	public void setSnpTaskManageService(
			SnpTaskManageService snpTaskManageService) {
		this.snpTaskManageService = snpTaskManageService;
	}

	public SnpTaskWaitManage getSnpTaskManage() {
		return snpTaskManage;
	}

	public void setSnpTaskManage(SnpTaskWaitManage snpTaskManage) {
		this.snpTaskManage = snpTaskManage;
	}

	/**
	 * 保存中间产物
	 * 
	 * @throws Exception
	 */
//	@Action(value = "saveSnpTaskManage")
//	public void saveSnpTaskAbnormal() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getParameterFromRequest("itemDataJson");
//			snpTaskManageService.saveSnpTaskInfoManager(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	/**
	 * 保存样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveSnpTaskItemManager")
	public void saveSnpTaskItemManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			snpTaskManageService.saveSnpTaskItemManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showSnpTaskItemManagerList")
	public String showSnpTaskItemManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/snp/snpTaskItemManage.jsp");
	}

	@Action(value = "showSnpTaskItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSnpTaskItemManagerListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// 排序方式
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = snpTaskManageService
					.findSnpTaskItemList(map2Query, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SnpTaskItem> list = (List<SnpTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");

			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlow", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleConsume", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 样本管理明细入库
	 * 
	 * @throws Exception
	 */
	@Action(value = "SnpTaskManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			snpTaskManageService.SnpTaskManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 样本管理明细fluidigm实验待办
	 * 
	 * @throws Exception
	 */
	@Action(value = "SnpTaskManageItemTiqu")
	public void SnpTaskManageItemTiqu() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			snpTaskManageService.SnpTaskManageItemTiqu(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
