package com.biolims.experiment.snp.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: fluidigm实验明细
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SNP_TASK_INFO_MANAGER")
@SuppressWarnings("serial")
public class SnpTaskInfoManager extends EntityDao<SnpTaskInfoManager> implements java.io.Serializable {
	/** 编码*/
	private String id;
	/** 实验编号*/
	private String expCode;
	/** experiment.snp编号*/
	private String code;
	/** 样本编号*/
	private String sampleCode;
	/** 病人姓名*/
	private String patientName;
	/** 检测项目*/
	private String productId;
	/** 检测项目*/
	private String productName;
	/** 检测方法*/
	private String sequenceFun;
	/** 取样时间*/
	private String inspectDate;
	/** 身份证号*/
	private String idCard;
	/** 应出报告日期*/
	private String reportDate;
	/** 手机号*/
	private String phone;
	/** 任务单*/
	private String orderId;
	/** 样本Id*/
	private String sampleId;
	/** 浓度*/
	private Double concentration;
	/** 是否合格*/
	private String result;
	/** 步骤编号*/
	private String stepNum;
	/** 原因*/
	private String reason;
	/** 下一步流向*/
	private String nextFlow;
	/** 状态*/
	private String state;
	/** 状态*/
	private String stateName;
	/** 样本名称*/
	private String sampleName;
	/** 样本用量*/
	private Double sampleNum;
	/** 样本体积*/
	private Double sampleVolume;
	/** 补充体积*/
	private Double addVolume;
	/** 总体积*/
	private Double sumVolume;
	/** index*/
	private String indexs;
	/** 备注*/
	private String note;
	
	public String getNextFlow() {
		return nextFlow;
	}
	
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	
	public String getSampleId() {
		return sampleId;
	}
	
	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getSampleName() {
		return sampleName;
	}
	
	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}
	
	public Double getSampleNum() {
		return sampleNum;
	}
	
	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}
	
	public Double getSampleVolume() {
		return sampleVolume;
	}
	
	public void setSampleVolume(Double sampleVolume) {
		this.sampleVolume = sampleVolume;
	}
	
	public Double getAddVolume() {
		return addVolume;
	}
	
	public void setAddVolume(Double addVolume) {
		this.addVolume = addVolume;
	}
	
	public Double getSumVolume() {
		return sumVolume;
	}
	
	public void setSumVolume(Double sumVolume) {
		this.sumVolume = sumVolume;
	}
	
	public String getIndexs() {
		return indexs;
	}
	
	public void setIndexs(String indexs) {
		this.indexs = indexs;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  实验编号
	 */
	@Column(name ="EXP_CODE", length = 60)
	public String getExpCode(){
		return this.expCode;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  实验编号
	 */
	public void setExpCode(String expCode){
		this.expCode = expCode;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 36)
	public String getSampleCode(){
		return this.sampleCode;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}

	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	public String getState(){
		return this.state;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	
	@Column(name="STATE_NAME",length=20)
	public String getStateName() {
		return stateName;
	}
	
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	@Column(name="CONCENTRATION",length=50)
	public Double getConcentration() {
		return concentration;
	}
	
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}
	
	@Column(name="RESULT",length=50)
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
	
	@Column(name="STEP_NUM")
	public String getStepNum() {
		return stepNum;
	}
	
	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}
	
	@Column(name="REASON",length=50)
	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getPatientName() {
		return patientName;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getSequenceFun() {
		return sequenceFun;
	}
	
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	
	public String getInspectDate() {
		return inspectDate;
	}
	
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	
	public String getIdCard() {
		return idCard;
	}
	
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	
	public String getReportDate() {
		return reportDate;
	}
	
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getOrderId() {
		return orderId;
	}
	
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}