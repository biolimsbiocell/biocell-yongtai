package com.biolims.experiment.snp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.snp.dao.SnpTaskReceiveDao;
import com.biolims.experiment.snp.model.SnpTaskReceiveTemp;
import com.biolims.experiment.snp.model.SnpTaskTemp;
import com.biolims.experiment.snp.model.SnpTaskReceive;
import com.biolims.experiment.snp.model.SnpTaskReceiveItem;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpTaskReceiveService {
	@Resource
	private SnpTaskReceiveDao snpTaskReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSnpTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpTaskReceiveDao.selectSnpTaskReceiveList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpTaskReceive i) throws Exception {
		snpTaskReceiveDao.saveOrUpdate(i);
	}

	public SnpTaskReceive get(String id) {
		SnpTaskReceive snpTaskReceive = commonDAO.get(SnpTaskReceive.class, id);
		return snpTaskReceive;
	}

	public Map<String, Object> findSnpTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskReceiveDao
				.selectSnpTaskReceiveItemList(scId, startNum, limitNum,
						dir, sort);
		List<SnpTaskReceiveItem> list = (List<SnpTaskReceiveItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpTaskReceiveItem(SnpTaskReceive sc, String itemDataJson)
			throws Exception {
		List<SnpTaskReceiveItem> saveItems = new ArrayList<SnpTaskReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpTaskReceiveItem scp = new SnpTaskReceiveItem();
			// 将map信息读入实体类
			scp = (SnpTaskReceiveItem) snpTaskReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpTaskReceive(sc);
			saveItems.add(scp);
		}
		snpTaskReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpTaskReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpTaskReceiveItem scp = snpTaskReceiveDao.get(
					SnpTaskReceiveItem.class, id);
			snpTaskReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpTaskReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			snpTaskReceiveDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("snpTaskReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpTaskReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		SnpTaskReceive sct = snpTaskReceiveDao.get(SnpTaskReceive.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		this.snpTaskReceiveDao.update(sct);
		try {
			List<SnpTaskReceiveItem> edri = this.snpTaskReceiveDao
					.selectReceiveByIdList(sct.getId());
			for (SnpTaskReceiveItem ed : edri) {
				if (ed.getMethod().equals("1")) {
					SnpTaskTemp dst = new SnpTaskTemp();
					dst.setConcentration(ed.getConcentration());
					dst.setLocation(ed.getLocation());
					dst.setCode(ed.getCode());
					dst.setId(ed.getId());
					dst.setSequenceFun(ed.getSequenceFun());
					dst.setPatientName(ed.getPatientName());
					dst.setProductName(ed.getProductName());
					dst.setProductId(ed.getProductId());
					dst.setIdCard(ed.getIdCard());
					dst.setInspectDate(ed.getInspectDate());
					dst.setPhone(ed.getPhone());
					dst.setOrderId(ed.getOrderId());
					dst.setReportDate(ed.getReportDate());
					dst.setState("1");
					dst.setClassify(ed.getClassify());
					dst.setCode(ed.getCode());
					dst.setSampleType(ed.getSampleType());
					dst.setSampleNum(ed.getSampleNum());
					dst.setLabCode(ed.getLabCode());
					snpTaskReceiveDao.saveOrUpdate(dst);
					SnpTaskReceiveTemp temp = commonDAO.get(SnpTaskReceiveTemp.class,
							ed.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}
				// 审批完成改变Info中的样本状态为“待fluidigm实验fluidigm实验”
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(ed
						.getCode());
				if (sf != null) {
					sf.setState("3");
					sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
				}
				ed.setState("1");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 审批完成之后，将明细表数据保存到SnpTaskfluidigm实验中间表
	 * @param code
	 */
	public void saveToSnpTaskTemp(String code) {
		List<SnpTaskReceiveItem> list = snpTaskReceiveDao
				.selectReceiveItemListById(code);
		if (list.size() > 0) {
			for (SnpTaskReceiveItem ed : list) {
				SnpTaskTemp dst = new SnpTaskTemp();
				dst.setConcentration(ed.getConcentration());
				dst.setLocation(ed.getLocation());
				dst.setCode(ed.getCode());
				dst.setId(ed.getId());
				dst.setSequenceFun(ed.getSequenceFun());
				dst.setPatientName(ed.getPatientName());
				dst.setProductName(ed.getProductName());
				dst.setProductId(ed.getProductId());
				dst.setIdCard(ed.getIdCard());
				dst.setInspectDate(ed.getInspectDate());
				dst.setPhone(ed.getPhone());
				dst.setOrderId(ed.getOrderId());
				dst.setReportDate(ed.getReportDate());
				dst.setState("1");
				dst.setClassify(ed.getClassify());
				dst.setSampleCode(ed.getSampleCode());
				dst.setSampleType(ed.getSampleType());
				dst.setSampleNum(ed.getSampleNum());
				snpTaskReceiveDao.saveOrUpdate(dst);
			}
		}
	}
		public Map<String, Object> selectSnpTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = snpTaskReceiveDao.selectSnpTaskReceiveTempList(
				startNum, limitNum, dir, sort);
		List<SnpTaskReceiveTemp> list = (List<SnpTaskReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
	
}
