package com.biolims.experiment.snp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.snp.dao.SnpTaskAbnormalDao;
import com.biolims.experiment.snp.model.SnpTaskAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;
import com.biolims.sample.service.SampleInputService;
@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpTaskAbnormalService {
	@Resource
	private SnpTaskAbnormalDao snpTaskAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSnpTaskAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpTaskAbnormalDao.selectSnpTaskAbnormalList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpTaskAbnormal i) throws Exception {
		snpTaskAbnormalDao.saveOrUpdate(i);
	}

	public SnpTaskAbnormal get(String id) {
		SnpTaskAbnormal snpTaskAbnormal = commonDAO
				.get(SnpTaskAbnormal.class, id);
		return snpTaskAbnormal;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpTaskAbnormal sc, Map jsonMap) throws Exception {
		if (sc != null) {
			snpTaskAbnormalDao.saveOrUpdate(sc);
			String jsonStr = "";
		}
	}

	/**
	 * 保存异常fluidigm实验样本
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpTaskAbnormalList(String itemDataJson) throws Exception {
		List<SnpTaskAbnormal> saveItems = new ArrayList<SnpTaskAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpTaskAbnormal sbi = new SnpTaskAbnormal();
			sbi = (SnpTaskAbnormal) snpTaskAbnormalDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			if (sbi != null && sbi.getIsExecute() != null
					&& sbi.getNextFlowId() != null) {
				// 确认执行
				if (sbi.getIsExecute().equals("1")) {
					if (sbi.getResult().equals("1")) {// 合格
						String nextFlowId = sbi.getNextFlowId();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(sbi.getCode());
							st.setCode(sbi.getCode());
							st.setNum(sbi.getSampleNum());
							st.setState("1");
							snpTaskAbnormalDao.saveOrUpdate(st);
							// 入库，改变Info中原始样本的状态为“待入库”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(sbi.getSampleCode());
							if (sf != null) {
								sf.setState("3");
								sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
							}
						} else if (nextFlowId.equals("0010")) {
							// 重fluidigm实验
							SnpTaskAbnormal eda = new SnpTaskAbnormal();
							sbi.setState("4");
							// 状态为4 在重fluidigm实验中显示
							sampleInputService.copy(eda, sbi);
						} else if (nextFlowId.equals("0012")) {
							// 暂停，改变Info中原始样本的状态为“实验暂停”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(sbi.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
							}
						} else if (nextFlowId.equals("0013")) {
							// 终止，改变Info中原始样本的状态为“实验终止”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(sbi.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
							}
						} else if (nextFlowId.equals("0014")) {
							// 下一步流向是：反馈至项目组
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								sbi.setState("1");
								sampleInputService.copy(o, sbi);
							}
						}
						sbi.setState("1");
					}
				}
			}
			saveItems.add(sbi);
		}
		snpTaskAbnormalDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 根据条件检索数据
	 * @param code
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> selectAbnormal(String code,
			String Code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = snpTaskAbnormalDao.selectAbnormal(
				code, Code);
		List<SnpTaskAbnormal> list = (List<SnpTaskAbnormal>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SnpTaskAbnormal srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("code", srai.getCode());
				map.put("sampleCode", srai.getSampleCode());
				map.put("sampleType", srai.getSampleType());
				map.put("sampleCondition", srai.getSampleCondition());
				map.put("isExecute", srai.getIsExecute());
				map.put("nextFlow", srai.getNextFlow());
				map.put("note", srai.getNote());
				map.put("feedbackTime", srai.getFeedbackTime());
				map.put("method", srai.getMethod());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
