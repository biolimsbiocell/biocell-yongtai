package com.biolims.experiment.snp.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.snp.dao.SnpTaskDao;
import com.biolims.experiment.snp.model.SnpTask;
import com.biolims.experiment.snp.model.SnpTaskAbnormal;
import com.biolims.experiment.snp.model.SnpTaskCos;
import com.biolims.experiment.snp.model.SnpTaskInfo;
import com.biolims.experiment.snp.model.SnpTaskInfoManager;
import com.biolims.experiment.snp.model.SnpTaskItem;
import com.biolims.experiment.snp.model.SnpTaskReagent;
import com.biolims.experiment.snp.model.SnpTaskReceiveItem;
import com.biolims.experiment.snp.model.SnpTaskTemp;
import com.biolims.experiment.snp.model.SnpTaskTemplate;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SnpTaskService {
	@Resource
	private SnpTaskDao snpTaskDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;

	StringBuffer json = new StringBuffer();
	public Map<String, Object> findSnpTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return snpTaskDao.selectSnpTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findSnpTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		    return snpTaskDao.selectSnpTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpTask i) throws Exception {
		snpTaskDao.saveOrUpdate(i);
	}

	public SnpTask get(String id) {
		SnpTask snpTask = commonDAO.get(SnpTask.class, id);
		return snpTask;
	}

	public Map<String, Object> findSnpTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<SnpTaskItem> list = (List<SnpTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findSnpTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskInfoList(scId, startNum, limitNum, dir, sort);
		List<SnpTaskInfo> list = (List<SnpTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 模板
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskTemplateItemList(scId, startNum, limitNum, dir, sort);
		List<SnpTaskTemplate> list = (List<SnpTaskTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findReagentItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskTemplateReagentList(scId, startNum, limitNum, dir,
						sort);
		List<SnpTaskReagent> list = (List<SnpTaskReagent>) result.get("list");
		return result;
	}

	/**
	 * 根据步骤加载试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskTemplateReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<SnpTaskReagent> list = (List<SnpTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskTemplateCosListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<SnpTaskCos> list = (List<SnpTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskTemplateCosList(scId, startNum, limitNum, dir, sort);
		List<SnpTaskCos> list = (List<SnpTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpTaskItem(SnpTask sc, String itemDataJson)
			throws Exception {
		List<SnpTaskItem> saveItems = new ArrayList<SnpTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = snpTaskDao.get(
						StorageContainer.class, temp.getStorageContainer()
								.getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();
			}
		}
		for (Map<String, Object> map : list) {
			SnpTaskItem scp = new SnpTaskItem();
			// 将map信息读入实体类
			scp = (SnpTaskItem) snpTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num = Math.floor((scp.getOrderNumber() - 1)
							/ rowCode) + 1;
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
							/ (rowCode * colCode) + 1));
				}
			}
			scp.setSnpTask(sc);

			if (scp != null) {
				Template t = commonDAO.get(Template.class, sc.getTemplate()
						.getId());
				if (t != null) {
					if (t.getDicSampleType() != null) {
						DicSampleType d = commonDAO.get(DicSampleType.class, t
								.getDicSampleType().getId());
						if (d != null) {
							if (scp.getDicSampleType() == null
									|| (scp.getDicSampleType() != null && scp
											.getDicSampleType().equals(""))) {
								scp.setDicSampleType(d);
							}
						}
					}
					if (t.getProductNum() != null) {
						if (scp.getProductNum() == null
								|| (scp.getProductNum() != null && scp
										.getProductNum().equals(""))) {
							scp.setProductNum(t.getProductNum());
						}
					}
					if (t.getSampleNum() != null) {
						if (scp.getSampleConsume() == null) {
							scp.setSampleConsume(t.getSampleNum());
						}
					}
				}
			}
			saveItems.add(scp);
			if (scp != null) {
				SnpTaskInfoManager dm = new SnpTaskInfoManager();
				dm.setExpCode(scp.getExpCode());
				dm.setCode(scp.getCode());
				dm.setSampleCode(scp.getSampleCode());
				dm.setSampleId(scp.getTempId());
				dm.setSampleName(scp.getSampleName());
				dm.setSampleVolume(scp.getSampleVolume());
				dm.setAddVolume(scp.getAddVolume());
				dm.setSumVolume(scp.getSumVolume());
				dm.setIndexs(scp.getIndexs());
				dm.setConcentration(scp.getConcentration());
				dm.setReason(scp.getReason());
				dm.setResult(scp.getResult());
				dm.setStepNum(scp.getStepNum());
				dm.setPatientName(scp.getPatientName());
				dm.setProductId(scp.getProductId());
				dm.setProductName(scp.getProductName());
				dm.setPhone(scp.getPhone());
				dm.setReportDate(scp.getReportDate());
				dm.setIdCard(scp.getIdCard());
				dm.setInspectDate(scp.getInspectDate());
				dm.setOrderId(scp.getOrderId());
				dm.setSequenceFun(scp.getSequenceFun());
				snpTaskDao.saveOrUpdate(dm);
			}
		}
		snpTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(snpTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			if (id != null) {
				SnpTaskItem scp = snpTaskDao
						.get(SnpTaskItem.class, id);
				snpTaskDao.delete(scp);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSnpTaskResult(SnpTask sc, String itemDataJson)
			throws Exception {
		DecimalFormat df = new DecimalFormat("######.000");
		List<SnpTaskInfo> saveItems = new ArrayList<SnpTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpTaskInfo scp = new SnpTaskInfo();
			// 将map信息读入实体类
			scp = (SnpTaskInfo) snpTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						|| !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());
					markCode = scp.getCode() + d.getCode();
				}
				String code = codingRuleService.getCode("SnpTaskInfo",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			Double b;
			if (sc.getAcceptUser().getId().equals("D0004")) {
				// fluidigm实验实验组-组织
				if (scp.getNextFlowId().equals("0022")) {
					// 超声破碎
					if (scp.getQbcontraction() < 11) {
						scp.setVolume(180.0);
					} else if (scp.getQbcontraction() >= 11
							&& scp.getQbcontraction() < 15) {
						b = 2000 / scp.getQbcontraction();
						scp.setVolume(Double.valueOf(df.format(b)));
					} else if (scp.getQbcontraction() >= 15
							&& scp.getQbcontraction() <= 22) {
						scp.setVolume(90.0);
					} else if (scp.getQbcontraction() > 22) {
						b = 2000 / scp.getQbcontraction();
						scp.setVolume(Double.valueOf(df.format(b)));
					}
				} else if (scp.getNextFlowId().equals("0021")) {// QPCR
					scp.setVolume(1.0);
				} else if (scp.getNextFlowId().equals("0009")) {// 样本入库

				}
			}
			if (scp.getVolume() != null) {
				if (scp.getQbcontraction() != null) {
					b = scp.getVolume() * scp.getQbcontraction();
					scp.setSampleNum(Double.valueOf(df.format(b)));
				}
				if (scp.getQbcontraction() == null
						&& scp.getContraction() != null) {
					b = scp.getVolume() * scp.getContraction();
					scp.setSampleNum(Double.valueOf(df.format(b)));
				}
			}
			List<SampleInfo> sil = snpTaskDao.findByProperty(
					SampleInfo.class, "code", scp.getCode());
			if (sil.size() > 0) {
				SampleInfo si = sil.get(0);
				scp.setSampleInfo(si);
			}
			snpTaskDao.saveOrUpdate(scp);
		}
		// 计算出组织实验组入库样本的体积
		if (sc.getAcceptUser().getId().equals("D0004")) {
			// fluidigm实验实验组-组织
			// 根据主表id查询结果表为入库样本的信息
			List<SnpTaskInfo> list1 = this.snpTaskDao
					.setSnpTaskInfo(sc.getId());
			for (SnpTaskInfo s : list1) {
				// 根据入库的原始样本号查询该结果表信息
				List<SnpTaskInfo> list2 = this.snpTaskDao
						.setSnpTaskInfo2(sc.getId(), s.getCode());
				Double v = 0.0;
				for (SnpTaskInfo s2 : list2) {
					if (!s2.getNextFlowId().equals("0009")
							&& s2.getVolume() != null) {
						v += s2.getVolume();
					}
				}
				s.setVolume(Double.valueOf(df.format(200 - v)));
				if (s.getVolume() != null) {
					if (s.getQbcontraction() != null) {
						s.setSampleNum(Double.valueOf(df.format(s.getVolume()
								* s.getQbcontraction())));
					}
					if (s.getQbcontraction() == null
							&& s.getContraction() != null) {
						s.setSampleNum(Double.valueOf(df.format(s.getVolume()
								* s.getContraction())));
					}
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			SnpTaskInfo scp = snpTaskDao
					.get(SnpTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				snpTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SnpTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			snpTaskDao.saveOrUpdate(sc);
			if (sc.getTemplate() != null) {
				String id = sc.getTemplate().getId();
				templateService.setCosIsUsedSave(id);
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("snpTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSnpTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpTaskTemplateItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTemplateItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpTaskTemplateReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveReagentItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("snpTaskTemplateCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveCosItem(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTemplateItem(SnpTask sc, String itemDataJson)
			throws Exception {
		List<SnpTaskTemplate> saveItems = new ArrayList<SnpTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpTaskTemplate scp = new SnpTaskTemplate();
			// 将map信息读入实体类
			scp = (SnpTaskTemplate) snpTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpTask(sc);
			saveItems.add(scp);
		}
		snpTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpTaskTemplate scp = snpTaskDao.get(
					SnpTaskTemplate.class, id);
			snpTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItemOne(String ids) throws Exception {
		SnpTaskTemplate scp = snpTaskDao.get(SnpTaskTemplate.class,
				ids);
		if (scp != null) {
			snpTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReagentItem(SnpTask sc, String itemDataJson)
			throws Exception {
		List<SnpTaskReagent> saveItems = new ArrayList<SnpTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpTaskReagent scp = new SnpTaskReagent();
			// 将map信息读入实体类
			scp = (SnpTaskReagent) snpTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getNum());
			}
			saveItems.add(scp);
		}
		snpTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpTaskReagent scp = snpTaskDao.get(SnpTaskReagent.class,
					id);
			snpTaskDao.delete(scp);
		}
	}

	/**
	 * delTemplateReagentOne 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateReagentOne(String ids) throws Exception {
		SnpTaskReagent scp = snpTaskDao.get(SnpTaskReagent.class, ids);
		if (scp != null) {
			snpTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCosItem(SnpTask sc, String itemDataJson) throws Exception {
		List<SnpTaskCos> saveItems = new ArrayList<SnpTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SnpTaskCos scp = new SnpTaskCos();
			// 将map信息读入实体类
			scp = (SnpTaskCos) snpTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSnpTask(sc);

			saveItems.add(scp);
		}
		snpTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCosItem(String[] ids) throws Exception {
		for (String id : ids) {
			SnpTaskCos scp = snpTaskDao.get(SnpTaskCos.class, id);
			snpTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSnpTaskTemplateCosOne(String ids) throws Exception {
		SnpTaskCos scp = snpTaskDao.get(SnpTaskCos.class, ids);
		if (scp != null) {
			snpTaskDao.delete(scp);
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SnpTask sct = snpTaskDao.get(SnpTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setReceiveDate(new Date());
		snpTaskDao.update(sct);
		// 提交样本
		submit(id, null);

		// 完成的时候判断设备解除占用
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		String did = sct.getId();
		// 将批次信息反馈到模板中
		List<SnpTaskReagent> list1 = snpTaskDao.setReagentList(did);
		for (SnpTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); 
			// SnpTask实验中试剂的批次
			String drid = dt.gettReagent(); 
			// SnpTask实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
		}
		// 将状态信息反馈到开箱检验的样本中
		List<SnpTaskItem> eList = snpTaskDao.setSnpTaskItemList(did);
		for (SnpTaskItem edg : eList) {
			String sid = edg.getTempId();
			List<SampleReceiveItem> sList = sampleReceiveDao
					.selectItemList(sid);
			for (SampleReceiveItem sri : sList) {
				sri.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				sri.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE);
			}
			updateTempState(sid);
		}
	}

	/**
	 * 生成结果之后修改SnpTaskfluidigm实验中间表的样本状态
	 * @param code
	 */
	public void updateTempState(String code) {
		SnpTaskTemp dt = commonDAO.get(SnpTaskTemp.class, code);
		if (dt != null) {
			dt.setState("2");
		}
	}

	/**
	 * 到QPCR的样本或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findExperimentSnpTaskReslutList(Integer startNum,
			Integer limitNum, String dir, String sort, String state)
			throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectExperimentSnpTaskReslutList(startNum, limitNum, dir,
						sort, state);
		List<SnpTaskInfo> list = (List<SnpTaskInfo>) result.get("list");

		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSnpTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskDao
				.selectSnpTaskFromReceiveList(scId, startNum, limitNum, dir,
						sort);
		List<SnpTaskReceiveItem> list = (List<SnpTaskReceiveItem>) result.get("list");
		return result;
	}

	public Map<String, Object> selectSnpTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = snpTaskDao.selectSnpTaskAbnormalList(
				scId, startNum, limitNum, dir, sort);
		List<SnpTaskInfo> list = (List<SnpTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = snpTaskDao.setReagent(id, code);
		List<SnpTaskReagent> list = (List<SnpTaskReagent>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SnpTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());
				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}
				if (ti.getNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}
				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getSnpTask().getId());
				map.put("tName", ti.getSnpTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = snpTaskDao.setCos(id, code);
		List<SnpTaskCos> list = (List<SnpTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SnpTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}
				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}
				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getSnpTask().getId());
				map.put("tName", ti.getSnpTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	
	public List<SnpTaskItem> findSnpTaskItemList(String scId) throws Exception {
		List<SnpTaskItem> list = snpTaskDao.selectSnpTaskItemList(scId);
		return list;
	}

	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submit(String id, String[] ids) throws Exception {
		// 获取主表信息
		SnpTask sc = this.snpTaskDao.get(SnpTask.class, id);
		// 获取结果表样本信息
		List<SnpTaskInfo> list;
		if (ids == null)
			list = this.snpTaskDao.setSnpTaskResultById(id);
		else
			list = this.snpTaskDao.setSnpTaskResultByIds(ids);
		for (SnpTaskInfo scp : list) {
			if (scp != null) {
				if (scp.getNextFlowId() != null && scp.getResult() != null
						&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
					if (!scp.getResult().equals("")
							&& !scp.getNextFlowId().equals("")) {
						String isOk = scp.getResult();
						String nextFlowId = scp.getNextFlowId();NextFlow nf = commonService.get(NextFlow.class, nextFlowId);nextFlowId = nf.getSysCode();
						if (isOk.equals("1")) {
							if (nextFlowId.equals("0009")) {
								// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setNum(scp.getSampleNum());
								st.setSampleType(scp.getSampleType());
								st.setSampleTypeId(scp.getDicSampleType()
										.getId());
								st.setInfoFrom("SnpTaskInfo");
								st.setState("1");
								SampleInfo s = this.sampleInfoMainDao
										.findSampleInfo(scp.getSampleCode());
								st.setPatientName(s.getPatientName());
								snpTaskDao.saveOrUpdate(st);
							} else if (nextFlowId.equals("0010")) {
								// 重fluidigm实验
								SnpTaskAbnormal eda = new SnpTaskAbnormal();
								scp.setState("4");
								// 状态为4 在重fluidigm实验中显示
								sampleInputService.copy(eda, scp);
							} else if (nextFlowId.equals("0011")) {
								// 重建库
//								WkAbnormalBack wka = new WkAbnormalBack();
//								scp.setState("4");
//								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0024")) {
								// 重抽血
								PlasmaAbnormal wka = new PlasmaAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0012")) {// 暂停
							} else if (nextFlowId.equals("0013")) {// 终止
							} else if (nextFlowId.equals("0018")) {// 文库构建
//								WkTaskTemp d = new WkTaskTemp();
//								scp.setState("1");
//								sampleInputService.copy(d, scp);
//								d.setReportDate(DateUtil.parse(scp
//										.getReportDate()));
//								snpTaskDao.saveOrUpdate(d);
							} else if (nextFlowId.equals("0020")) {// 2100质控
								Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
								scp.setState("1");
								sampleInputService.copy(qc2100, scp);
								qc2100.setWkType("2");
								snpTaskDao.saveOrUpdate(qc2100);
							} else if (nextFlowId.equals("0021")) {// QPCR质控
								QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
								scp.setState("1");
								sampleInputService.copy(qpcr, scp);
								qpcr.setWkType("1");
								snpTaskDao.saveOrUpdate(qpcr);
							} else if (nextFlowId.equals("0022")) {// 超声破碎
								UfTaskTemp uf = new UfTaskTemp();
								scp.setState("1");
								sampleInputService.copy(uf, scp);
							} else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(nextFlowId);
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}

						} else {// 不合格
							if (nextFlowId.equals("0014")) {// 反馈项目组
								// 下一步流向是：反馈至项目组
								
							} else {
								// 提交的不合格样本到异常管理
								SnpTaskAbnormal eda = new SnpTaskAbnormal();
								scp.setState("2");// 状态为2 在fluidigm实验异常中显示
								sampleInputService.copy(eda, scp);
							}

						}
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										sc.getCreateDate(),
										format.format(new Date()),
										"SnpTask",
										"fluidigm实验",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), scp.getNextFlow(),
										scp.getResult(), null, null, null,
										null, null, null, null, null);
						scp.setSubmit("1");
						commonService.saveOrUpdate(scp);
					}
				}
			}
		}
	}
}