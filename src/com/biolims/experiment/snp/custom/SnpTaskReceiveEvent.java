package com.biolims.experiment.snp.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.snp.service.SnpTaskReceiveService;

public class SnpTaskReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SnpTaskReceiveService mbService = (SnpTaskReceiveService) ctx
				.getBean("snpTaskReceiveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
