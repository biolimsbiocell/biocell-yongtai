package com.biolims.experiment.snp.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.snp.service.SnpTaskService;

public class SnpTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SnpTaskService mbService = (SnpTaskService) ctx
				.getBean("snpTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
