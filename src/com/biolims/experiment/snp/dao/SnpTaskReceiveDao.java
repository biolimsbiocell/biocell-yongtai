package com.biolims.experiment.snp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.snp.model.SnpTaskReceiveTemp;
import com.biolims.experiment.snp.model.SnpTaskReceive;
import com.biolims.experiment.snp.model.SnpTaskReceiveItem;

@Repository
@SuppressWarnings("unchecked")
public class SnpTaskReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectSnpTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpTaskReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReceive> list = new ArrayList<SnpTaskReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReceiveItem> list = new ArrayList<SnpTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpTaskReceiveItemByTypeList(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from SnpTaskReceiveItem where 1=1 and snpTaskReceive.Type='SnpTask'";
		String key = "";
		if (scId != null)
			key = key + " and snpTaskReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReceiveItem> list = new ArrayList<SnpTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询从开向检验完成的样本
	 * @return
	 */
	public List<SnpTaskReceiveTemp> selectSnpTaskReceiveTempList() {
		String hql = "from SnpTaskReceiveTemp where 1=1 and state='1' order by reportDate desc";
		List<SnpTaskReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 修改状态
	 * @param id
	 * @return
	 */
	public List<SnpTaskReceiveItem> selectReceiveByIdList(String id) {
		String hql = "from SnpTaskReceiveItem where 1=1 and state='1' and method='1' and snpTaskReceive.id='"
				+ id + "'";
		List<SnpTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id 查询 明细数据
	 * @param code
	 * @return
	 */
	public List<SnpTaskReceiveItem> selectReceiveItemListById(String code) {
		String hql = "from SnpTaskReceiveItem where 1=1 and snpTaskReceive.id='"
				+ code + "'";
		List<SnpTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public Map<String, Object> selectSnpTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from SnpTaskReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReceiveTemp> list = new ArrayList<SnpTaskReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
}