package com.biolims.experiment.snp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.snp.model.SnpTaskReceiveItem;
import com.biolims.experiment.snp.model.SnpTask;
import com.biolims.experiment.snp.model.SnpTaskCos;
import com.biolims.experiment.snp.model.SnpTaskItem;
import com.biolims.experiment.snp.model.SnpTaskReagent;
import com.biolims.experiment.snp.model.SnpTaskTemp;
import com.biolims.experiment.snp.model.SnpTaskTemplate;
import com.biolims.experiment.snp.model.SnpTaskInfo;

@Repository
@SuppressWarnings("unchecked")
public class SnpTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectSnpTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpTask> list = new ArrayList<SnpTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskItem> list = new ArrayList<SnpTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskInfo> list = new ArrayList<SnpTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 模板对应的子表
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSnpTaskTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskTemplate> list = new ArrayList<SnpTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			else{
				key = key + " order by"+castStrId("code")+"";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpTaskTemplateReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReagent> list = new ArrayList<SnpTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据ItemId查询试剂明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSnpTaskTemplateReagentListByItemId(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from SnpTaskReagent t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReagent> list = new ArrayList<SnpTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from SnpTaskReagent t where 1=1 and t.snpTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SnpTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from SnpTaskCos t where 1=1 and t.snpTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SnpTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSnpTaskTemplateCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskCos> list = new ArrayList<SnpTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSnpTaskTemplateCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from SnpTaskCos t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskCos> list = new ArrayList<SnpTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据SnpTaskfluidigm实验主表ID查询试剂明细
	 */
	public List<SnpTaskReagent> setReagentList(String code) {
		String hql = "from SnpTaskReagent where 1=1 and snpTask.id='" + code
				+ "'";
		List<SnpTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据SnpTaskfluidigm实验主表ID查询fluidigm实验明细
	 */
	public List<SnpTaskItem> setSnpTaskItemList(String code) {
		String hql = "from SnpTaskItem where 1=1 and snpTask.id='" + code + "'";
		List<SnpTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据状态查询SnpTask
	 * @return
	 */
	public List<SnpTask> selectSnpTaskList() {
		// String hql = "from SnpTask t where 1=1 and t.state=1";
		String hql = "from SnpTask t where 1=1 and t.state='1'";
		List<SnpTask> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SnpTaskInfo> selectSnpTaskResultList(String id) {
		String hql = "from SnpTaskInfo t where 1=1 and snpTask.state='1' and snpTask='"
				+ id + "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<SnpTaskInfo> setSnpTaskResultByCode(String code) {
		String hql = "from SnpTaskInfo t where 1=1 and snpTask.id='" + code
				+ "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<SnpTaskInfo> setSnpTaskResultById(String code) {
		String hql = "from SnpTaskInfo t where (submit is null or submit='') and snpTask.id='"
				+ code + "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<SnpTaskInfo> setSnpTaskResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SnpTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询fluidigm实验样本去到QPCR或到一代测序
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectExperimentSnpTaskReslutList(
			Integer startNum, Integer limitNum, String dir, String sort,
			String state) throws Exception {
		String hql = "from SnpTaskInfo where 1=1 and experiment.snpInfo.state='完成' and snpTask.stateName='"
				+ state + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskInfo> list = new ArrayList<SnpTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSnpTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskReceiveItem where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskReceiveItem> list = new ArrayList<SnpTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询异常fluidigm实验
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSnpTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SnpTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskInfo> list = new ArrayList<SnpTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询左侧中间表
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectSnpTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SnpTaskTemp where 1=1 and state='1'";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SnpTaskTemp> list = new ArrayList<SnpTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询fluidigm实验异常样本
	 * @param code
	 * @return
	 */
	public List<SnpTaskInfo> selectSnpTaskResultAbnormalList(String code) {
		String hql = "from SnpTaskInfo where 1=1 and  snpTask='" + code + "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<SnpTaskInfo> selectSnpTaskAllList(String code) {
		String hql = "from SnpTaskInfo  where 1=1  and snpTask='" + code
				+ "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id查询fluidigm实验生成结果 用于更新是否合格字段
	 * @param id
	 * @return
	 */
	public List<SnpTaskInfo> findSnpTaskResultById(String id) {
		String hql = "from SnpTaskInfo  where 1=1 and code='" + id + "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 状态完成下一步流向
	 * @param code
	 * @return
	 */
	public List<SnpTaskInfo> setNextFlowList(String code) {
		String hql = "from SnpTaskInfo where 1=1  and snpTask.state='1' and snpTask.id='"
				+ code + "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从上一步样本接收完成的fluidigm实验样本
	 * @return
	 */
	public List<SnpTaskReceiveItem> selectReceiveSnpTaskList() {
		String hql = "from SnpTaskReceiveItem where 1=1  and state='1' ";
		List<SnpTaskReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from SnpTaskItem t where 1=1 and t.snpTask.id='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据id查询fluidigm实验结果
	 * @param id
	 * @return
	 */
	public List<SnpTaskInfo> selectSnpTaskResultListById(String id) {
		String hql = "from SnpTaskInfo where 1=1  and snpTask.id='" + id
				+ "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的fluidigm实验次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from SnpTaskItem t where 1=1 and t.sampleCode='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.Code)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 查询从样本接收完成的的样本
	 * @param id
	 * @return
	 */
	public List<SnpTaskItem> findSnpTaskItemList(String id) {
		String hql = "from SnpTaskItem  where 1=1 and state='1' and snpTask='"
				+ id + "'";
		List<SnpTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据Code查询SnpTaskInfo
	 * @param code
	 * @return
	 */
	public String findSnpTaskInfo(String code) {
		String hql = "from SnpTaskInfo  where 1=1 and tempId='" + code + "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 根据样本编号查出SnpTaskTemp对象
	 * @param code
	 * @return
	 */
	public SnpTaskTemp findSnpTaskTemp(String code) {
		String hql = "from SnpTaskTemp t where 1=1 and t.sampleCode = '" + code
				+ "'";
		SnpTaskTemp SnpTaskTemp = (SnpTaskTemp) this.getSession()
				.createQuery(hql).uniqueResult();
		return SnpTaskTemp;
	}


	/**
	 * 根据样本查询主表
	 * @param sid
	 * @return
	 */
	public String selectTask(String sid) {
		String hql = "from TechCheckServiceOrder t where t.id = '" + sid + "'";
		String str = (String) this.getSession()
				.createQuery(" select t.id " + hql).uniqueResult();
		return str;
	}

	public List<SnpTaskItem> selectSnpTaskItemList(String scId) throws Exception {
		String hql = "from SnpTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and snpTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SnpTaskItem> list = new ArrayList<SnpTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * 根据样本编号查询检测临时表
	 * @param id
	 * @return
	 */
	public List<SnpTaskInfo> setSnpTaskInfo(String id) {
		String hql = "from SnpTaskInfo where 1=1 and snpTask.id='" + id
				+ "' and nextFlowId='0009'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据入库的原始样本号查询该结果表信息
	 * @param id
	 * @param Code
	 * @return
	 */
	public List<SnpTaskInfo> setSnpTaskInfo2(String id, String code) {
		String hql = "from SnpTaskInfo t where 1=1 and snpTask.id='" + id
				+ "' and sampleCode='" + code + "'";
		List<SnpTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}