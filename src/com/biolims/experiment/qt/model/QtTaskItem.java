package com.biolims.experiment.qt.model;

import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * @Title: Model
 * @Description: QPCR实验明细
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QT_TASK_ITEM")
@SuppressWarnings("serial")
public class QtTaskItem extends EntityDao<QtTaskItem> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 实验编号 */
	private String expCode;
	/**数据通量*/
	private String dataTraffic;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 染色体位置 */
	private String chromosomalLocation;
	/** 引物编号 */
	private String primerNumber;
	/** left primer */
	private String leftPrimer;
	/** right primer */
	private String rightPrimer;
	/** ampliconid */
	private String ampliconid;
	/** 病人姓名 */
	private String patientName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 检测方法 */
	private String sequenceFun;
	/** 取样时间 */
	private String inspectDate;
	/** 身份证号 */
	private String idCard;
	/** 应出报告日期 */
	private String reportDate;
	/** 手机号 */
	private String phone;
	/** 任务单 */
	private String orderId;
	/** 临时表Id */
	private String tempId;
	/** 浓度 */
	private Double concentration;
	/** 是否合格 */
	private String result;
	/** 步骤编号 */
	private String stepNum;
	/** 原因 */
	private String reason;
	/** 下一步流向 */
	private String nextFlow;
	/** 状态 */
	private String state;
	/** 状态 */
	private String stateName;
	/** 相关主表 */
	private QtTask qtTask;
	/** 样本名称 */
	private String sampleName;
	/** 样本数量 */
	private Double sampleNum;
	/** 样本体积 */
	private Double sampleVolume;
	/** 补充体积 */
	private Double addVolume;
	/** 总体积 */
	private Double sumVolume;
	/** index */
	private String indexs;
	/** 备注 */
	private String note;
	/** 排序号 */
	private Integer orderNumber;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;
	/** 项目编号 */
	private String projectId;
	/** 合同编号 */
	private String contractId;
	/** 任务单类型 */
	private String orderType;
	/** 任务单类型 */
	private String taskId;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private String classify;
	/** 中间产物数量 */
	private String productNum;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/** 样本类型 */
	private String sampleType;
	/** 样本用量 (ng) */
	private Double sampleConsume;
	/** 实验室样本号 */
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 变量X
	private String blx;
	// 样本剩余总量
	private Double sampleSyNum;
	// 排板个数
	private Integer num;
	/** 单位组 */
	private DicType unitGroup;
	
	/********新增字段*********/
	//DNA浓度
	private String dnaConcentration;
	//qPCR行号
	private String qpcrLineNum;
	//qPCR列号
	private String qpcrColNum;
	//条形码
	private String barCode;
	
	
	
	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}
	
	public String getDnaConcentration() {
		return dnaConcentration;
	}

	public void setDnaConcentration(String dnaConcentration) {
		this.dnaConcentration = dnaConcentration;
	}

	public String getQpcrLineNum() {
		return qpcrLineNum;
	}

	public void setQpcrLineNum(String qpcrLineNum) {
		this.qpcrLineNum = qpcrLineNum;
	}

	public String getQpcrColNum() {
		return qpcrColNum;
	}

	public void setQpcrColNum(String qpcrColNum) {
		this.qpcrColNum = qpcrColNum;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getBlx() {
		return blx;
	}

	public void setBlx(String blx) {
		this.blx = blx;
	}

	public Double getSampleSyNum() {
		return sampleSyNum;
	}

	public void setSampleSyNum(Double sampleSyNum) {
		this.sampleSyNum = sampleSyNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得Info
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置Info
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getSampleConsume() {
		return sampleConsume;
	}

	public void setSampleConsume(Double sampleConsume) {
		this.sampleConsume = sampleConsume;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSampleName() {
		return sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getSampleVolume() {
		return sampleVolume;
	}

	public void setSampleVolume(Double sampleVolume) {
		this.sampleVolume = sampleVolume;
	}

	public Double getAddVolume() {
		return addVolume;
	}

	public void setAddVolume(Double addVolume) {
		this.addVolume = addVolume;
	}

	public Double getSumVolume() {
		return sumVolume;
	}

	public void setSumVolume(Double sumVolume) {
		this.sumVolume = sumVolume;
	}

	public String getIndexs() {
		return indexs;
	}

	public void setIndexs(String indexs) {
		this.indexs = indexs;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验编号
	 */
	@Column(name = "EXP_CODE", length = 60)
	public String getExpCode() {
		return this.expCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验编号
	 */
	public void setExpCode(String expCode) {
		this.expCode = expCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 36)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得QtTask
	 * 
	 * @return: QtTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QT_TASK")
	public QtTask getQtTask() {
		return qtTask;
	}

	public void setQtTask(QtTask qtTask) {
		this.qtTask = qtTask;
	}

	@Column(name = "STATE_NAME", length = 20)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "STEP_NUM")
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	@Column(name = "REASON", length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getChromosomalLocation() {
		return chromosomalLocation;
	}

	public void setChromosomalLocation(String chromosomalLocation) {
		this.chromosomalLocation = chromosomalLocation;
	}

	public String getPrimerNumber() {
		return primerNumber;
	}

	public void setPrimerNumber(String primerNumber) {
		this.primerNumber = primerNumber;
	}

	public String getLeftPrimer() {
		return leftPrimer;
	}

	public void setLeftPrimer(String leftPrimer) {
		this.leftPrimer = leftPrimer;
	}

	public String getRightPrimer() {
		return rightPrimer;
	}

	public void setRightPrimer(String rightPrimer) {
		this.rightPrimer = rightPrimer;
	}

	public String getAmpliconid() {
		return ampliconid;
	}

	public void setAmpliconid(String ampliconid) {
		this.ampliconid = ampliconid;
	}

	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}
	
}