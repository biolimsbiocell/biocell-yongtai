package com.biolims.experiment.qt.model;

import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 模版明细
 * @author lims-platform
 * @date 2015-11-18 17:00:21
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QT_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class QtTaskTemplate extends EntityDao<QtTaskTemplate> implements java.io.Serializable {
	/** 步骤id*/
	private String id;
	/** 步骤编号*/
	private String code;
	/** 步骤名称*/
	private String name;
	/** 备注*/
	private String note;
	/** 关联样本*/
	private String sampleCodes;
	/** Item id*/
	private String tItem;
	private String startTime;
	/** 执行结束时间*/
	private String endTime;
	/** 实验员*/
	private String testUserId;
	private String testUserName;
	/** 状态*/
	private String state;
	private QtTask qtTask;
	
	public String getStartTime() {
		return startTime;
	} 
	
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String gettItem() {
		return tItem;
	}
	
	public void settItem(String tItem) {
		this.tItem = tItem;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QT_TASK")
	public QtTask getQtTask(){
		return this.qtTask;
	}
	
	/**
	 *方法: 设置QtTask
	 *@param: QtTask  相关主表
	 */
	public void setQtTask(QtTask qtTask){
		this.qtTask = qtTask;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  步骤id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  步骤id
	 */
	public void setId(String id){
		this.id = id;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE",length = 4000)
	public String getNote(){
		return this.note;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	@Column(name ="SAMPLE_CODES", length = 5000)
	public String getSampleCodes() {
		return sampleCodes;
	}
	
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
	

	
	/**
	 * @return the testUserId
	 */
	public String getTestUserId() {
		return testUserId;
	}

	/**
	 * @param testUserId the testUserId to set
	 */
	public void setTestUserId(String testUserId) {
		this.testUserId = testUserId;
	}

	/**
	 * @return the testUserName
	 */
	public String getTestUserName() {
		return testUserName;
	}

	/**
	 * @param testUserName the testUserName to set
	 */
	public void setTestUserName(String testUserName) {
		this.testUserName = testUserName;
	}

	@Column(name="STATE",length=20)
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
}