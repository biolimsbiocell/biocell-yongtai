package com.biolims.experiment.qt.model;

import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import javax.persistence.GeneratedValue;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * @Title: Model
 * @Description: 待QPCR实验样本接收明细
 * @author lims-platform
 * @date 2015-11-27 15:41:28
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QT_TASK_TEMP")
@SuppressWarnings("serial")
public class QtTaskTemp extends EntityDao<QtTaskTemp> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 数据通量 */
	private String dataTraffic;
	/** 原始样本编号 */
	private String sampleCode;
	/** 病人姓名 */
	private String patientName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 检测方法 */
	private String sequenceFun;
	/** 取样时间 */
	private String inspectDate;
	/** 身份证号 */
	private String idCard;
	/** 应出报告日期 */
	private String reportDate;
	/** 手机号 */
	private String phone;
	/** 任务单 */
	private String orderId;
	/** 样本名称 */
	private String sampleName;
	/** 样本ID */
	private String sampleId;
	/** 储位 */
	private String location;
	/** 状态 */
	private String state;
	/** 状态 */
	private String stateName;
	/** 处理方式 */
	private String method;
	/** 原因 */
	private String reason;
	/** 备注 */
	private String note;
	/** 浓度 */
	private Double concentration;
	/** 是否合格 */
	private String result;
	/** 步骤编号 */
	private String stepNum;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private String classify;
	/** 样本类型 */
	private String sampleType;
	/** 样本数量 */
	private Double sampleNum;
	/** 实验室样本号 */
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	//条形码
	private String barCode;
	
	
	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得Info
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置Info
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	public String getSampleId() {
		return sampleId;
	}

	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本名称
	 */
	@Column(name = "SAMPLE_NAME", length = 50)
	public String getSampleName() {
		return this.sampleName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本名称
	 */
	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 储位
	 */
	@Column(name = "LOCATION", length = 50)
	public String getLocation() {
		return this.location;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 储位
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "STATE_NAME", length = 20)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "REASON", length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "STEP_NUM")
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}

	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}
	
}