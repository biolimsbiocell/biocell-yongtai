﻿package com.biolims.experiment.qt.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qt.model.QtTaskAbnormal;
import com.biolims.experiment.qt.service.QtTaskAbnormalService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/qt/qtTaskAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QtTaskAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2513";
	@Autowired
	private QtTaskAbnormalService qtTaskAbnormalService;
	private QtTaskAbnormal qtTaskAbnormal = new QtTaskAbnormal();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showQtTaskAbnormalList")
	public String showQtTaskAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskAbnormal.jsp");
	}

	@Action(value = "showQtTaskAbnormalListJson")
	public void showQtTaskAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskAbnormalService
				.findQtTaskAbnormalList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskAbnormal> list = (List<QtTaskAbnormal>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("inspectDate", "");
		map.put("idCard", "");
		map.put("reportDate", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sampleType", "");
		map.put("sampleCondition", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("name", "");
		map.put("state", "");
		map.put("classify", "");
		map.put("nextFlowId", "");
		map.put("sampleNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qtTaskAbnormalSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQtTaskAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskAbnormalDialog.jsp");
	}

	@Action(value = "showDialogQtTaskAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQtTaskAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskAbnormalService
				.findQtTaskAbnormalList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskAbnormal> list = (List<QtTaskAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQtTaskAbnormal")
	public String editQtTaskAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qtTaskAbnormal = qtTaskAbnormalService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService
					.findFileInfoCount(id, "qtTaskAbnormal");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskAbnormalEdit.jsp");
	}

	@Action(value = "copyQtTaskAbnormal")
	public String copyQtTaskAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qtTaskAbnormal = qtTaskAbnormalService.get(id);
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskAbnormalEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		Map aMap = new HashMap();
		qtTaskAbnormalService.save(qtTaskAbnormal, aMap);
		return redirect("/experiment/qt/qtTaskAbnormal/editQtTaskAbnormal.action");
	}

	@Action(value = "viewQtTaskAbnormal")
	public String toViewQtTaskAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		qtTaskAbnormal = qtTaskAbnormalService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskAbnormalEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QtTaskAbnormalService getQtTaskAbnormalService() {
		return qtTaskAbnormalService;
	}

	public void setQtTaskAbnormalService(
			QtTaskAbnormalService qtTaskAbnormalService) {
		this.qtTaskAbnormalService = qtTaskAbnormalService;
	}

	public QtTaskAbnormal getQtTaskAbnormal() {
		return qtTaskAbnormal;
	}

	public void setQtTaskAbnormal(QtTaskAbnormal qtTaskAbnormal) {
		this.qtTaskAbnormal = qtTaskAbnormal;
	}

	/**
	 * 保存QPCR实验异常样本
	 * @throws Exception
	 */
	@Action(value = "saveQtTaskAbnormal")
	public void saveQtTaskAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			qtTaskAbnormalService.saveQtTaskAbnormalList(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据条件检索异常样本
	 * @throws Exception
	 */
	@Action(value = "selectAbnormal")
	public void selectAbnormal() throws Exception {
		String code1 = getParameterFromRequest("code");
		String code2 = getParameterFromRequest("Code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.qtTaskAbnormalService
					.selectAbnormal(code1, code2);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
