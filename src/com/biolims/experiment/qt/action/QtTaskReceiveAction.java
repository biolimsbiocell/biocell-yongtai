﻿package com.biolims.experiment.qt.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.qt.dao.QtTaskReceiveDao;
import com.biolims.experiment.qt.model.QtTaskReceive;
import com.biolims.experiment.qt.model.QtTaskReceiveTemp;
import com.biolims.experiment.qt.model.QtTaskReceiveItem;
import com.biolims.experiment.qt.service.QtTaskReceiveService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/experiment/qt/qtTaskReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QtTaskReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2512";
	@Autowired
	private QtTaskReceiveService qtTaskReceiveService;
	private QtTaskReceive qtTaskReceive = new QtTaskReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showQtTaskReceiveList")
	public String showQtTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceive.jsp");
	}

	@Action(value = "showQtTaskReceiveListJson")
	public void showQtTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskReceiveService
				.findQtTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskReceive> list = (List<QtTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qtTaskReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQtTaskReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceiveDialog.jsp");
	}

	@Action(value = "showDialogQtTaskReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQtTaskReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskReceiveService
				.findQtTaskReceiveList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskReceive> list = (List<QtTaskReceive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("sampleType", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("sampleCondition", "");
		map.put("method", "");
		map.put("location", "");
		map.put("nextFlow", "");
		map.put("stateName", "");
		map.put("advice", "");
		map.put("state", "");
		map.put("isExecute", "");
		map.put("feedbackTime", "");
		map.put("note", "");
		map.put("experiment.qtCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQtTaskReceive")
	public String editQtTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qtTaskReceive = qtTaskReceiveService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qtTaskReceive");
		} else {
			qtTaskReceive.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qtTaskReceive.setReceiveUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			qtTaskReceive.setReceiverDate(stime);
			qtTaskReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qtTaskReceive
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceiveEdit.jsp");
	}

	@Action(value = "copyQtTaskReceive")
	public String copyQtTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qtTaskReceive = qtTaskReceiveService.get(id);
		qtTaskReceive.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceiveEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qtTaskReceive.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QtTaskReceive";
			String markCode = "HSJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qtTaskReceive.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("qtTaskReceiveItem",
				getParameterFromRequest("qtTaskReceiveItemJson"));
		qtTaskReceiveService.save(qtTaskReceive, aMap);
		return redirect("/experiment/qt/qtTaskReceive/editQtTaskReceive.action?id="
				+ qtTaskReceive.getId());

	}

	@Action(value = "viewQtTaskReceive")
	public String toViewQtTaskReceive() throws Exception {
		String id = getParameterFromRequest("id");
		qtTaskReceive = qtTaskReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceiveEdit.jsp");
	}

	@Action(value = "showQtTaskReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceiveItem.jsp");
	}

	@Action(value = "showQtTaskReceiveItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskReceiveItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qtTaskReceiveService
					.findQtTaskReceiveItemList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskReceiveItem> list = (List<QtTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleId", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("qtTaskReceive-id", "");
			map.put("qtTaskReceive-name", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQtTaskReceiveItem")
	public void delQtTaskReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qtTaskReceiveService.delQtTaskReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
 	/**
	* 去待接收的样本
	* @throws Exception
	 */
	@Action(value = "showQtTaskReceiveItemListTo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskReceiveItemListTo() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskReceiveItemTo.jsp");
	}

	@Action(value = "showQtTaskReceiveItemListToJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskReceiveItemListToJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, Object> result = qtTaskReceiveService
					.selectQtTaskReceiveTempList(startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskReceiveTemp> list = (List<QtTaskReceiveTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("productName", "");
			map.put("productId", "");
			map.put("inspectDate", "");
			map.put("sampleType", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("reportDate", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("classify", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QtTaskReceiveService getQtTaskReceiveService() {
		return qtTaskReceiveService;
	}

	public void setQtTaskReceiveService(
			QtTaskReceiveService qtTaskReceiveService) {
		this.qtTaskReceiveService = qtTaskReceiveService;
	}

	public QtTaskReceive getQtTaskReceive() {
		return qtTaskReceive;
	}

	public void setQtTaskReceive(QtTaskReceive qtTaskReceive) {
		this.qtTaskReceive = qtTaskReceive;
	}
}
