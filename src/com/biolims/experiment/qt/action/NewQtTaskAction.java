package com.biolims.experiment.qt.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.core.user.service.UserService;
import com.biolims.crm.project.dao.ProjectDao;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskCos;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskReagent;
import com.biolims.experiment.qt.model.QtTaskTemplate;
import com.biolims.experiment.qt.service.NewQtTaskService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.web.action.BaseActionSupport;


@Namespace("/experiment/qt/qtTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
public class NewQtTaskAction extends BaseActionSupport  {
	
	
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private TemplateService templateService;
	
	
	
	@Autowired
	private NewQtTaskService newQtTaskService;
	

	// 点击待办事项跳转至核酸提取新界面
	@Action(value = "showTaskItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 任务单IDHSTQ1706130001
			String formId = getRequest().getParameter("formId");
			// 表单IDDnaTask
			String formName = getRequest().getParameter("formName");
			// taskID39304
			String taskId = getRequest().getParameter("taskId");

			// 查询明细表信息
			List<QtTaskItem> item = this.newQtTaskService
					.findTaskItem(formId);
			// 查询实验模板
			List<QtTask> task = this.newQtTaskService
					.finTemplate(formId);
			String templateId = task.get(0).getTemplate().getId();
			String templateName = task.get(0).getTemplate().getName();
			// 根据实验模板ID查询实验模板主表
			List<Template> template = this.templateService
					.finTemplateTask(templateId);
			String name = "";
			String colNum = "";
			String rowNum = "";
			if (template.get(0).getStorageContainer()!=null && !"".equals(template.get(0).getStorageContainer())) {
				name = template.get(0).getStorageContainer().getName();
				colNum = template.get(0).getStorageContainer().getColNum()
						.toString();
				rowNum = template.get(0).getStorageContainer().getRowNum()
						.toString();
			}

			System.out.println(item);
			System.out.println(templateName);

			map.put("name", name);
			map.put("colNum", colNum);
			map.put("rowNum", rowNum);
			map.put("data", item);
			map.put("template", templateName);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		System.out.println(JsonUtils.toJsonString(map));
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	//保存明细表数据
	@Action(value = "saveItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void saveItem() throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			
//			{"strobj":[{"cord":"A1","code":"JB1704050004","sampleCode":"JB1704050004","productName":"医学健康","sampleNum":"1","sampleType":"精斑"},
//			           {"cord":"B1","code":"JB1704050005","sampleCode":"JB1704050005","productName":"医学健康","sampleNum":"1","sampleType":"精斑"},
//			           {"cord":"C1","code":"JB1704050006","sampleCode":"JB1704050006","productName":"科技服务","sampleNum":"1","sampleType":"精斑"}]}
			String sJson = getRequest().getParameter("strobj");
			this.newQtTaskService.saveItem(sJson);
			map.put("success", true);
		}catch (Exception e ){
			e.printStackTrace();
			map.put("scuuess", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//加载实验模板
	@Action(value = "showTemplate", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showTemplate() throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		try{
			String formId = getRequest().getParameter("formId");
			//根据主表ID查询模板步骤明细
			List<QtTaskTemplate> templeat = this.newQtTaskService.selTemplateItem(formId);
			System.out.println("模板步骤明细:"+templeat);
			//根据主表ID查询模板试剂明细
			List<QtTaskReagent> reagent = this.newQtTaskService.selReagent(formId);
			System.out.println("模板试剂明细:"+reagent);
			//根据主表ID查询模板设备明细
			List<QtTaskCos> cos = this.newQtTaskService.selCos(formId);
			System.out.println("模板设备明细:"+cos);
			map.put("templeat", templeat);
			map.put("reagent", reagent);
			map.put("cos", cos);
			map.put("success",true);
		}catch(Exception e){
			e.printStackTrace();
			map.put("success",false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//修改后点击下一步保存实验模板
	@Action(value = "saveTemplate", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void saveTemplate() throws Exception{
		Map <String,Object> map = new HashMap<String,Object>();
		try{
			//ijson = {"templeat":[{"id":"402880335b79d258015b79d4e7150004","code":"2","name":"123","note":"31","testUserName":"","startTime":"","endTime":""},
			//			{"id":"402880335b79d258015b79d4e7150003","code":"1","name":"步骤一","note":"312312312","testUserName":"","startTime":"","endTime":""},
			//			{"id":"402880335b79d258015b79d4e7150005","code":"3","name":"213","note":"12312","testUserName":"","startTime":"","endTime":""}]}
			//rJson = {"reagent":[{"id":"402880335b79d258015b79d4e7710006","name":"k1","sn":"","batch":"","expireDate":"","isGood":" 是 ","sampleNum":"3","oneNum":"3","num":"3","isRunout":"3","note":"3"}]}
			//cJson = {"cos":[{"id":"402880335b79d258015b79d4e7710007","code":"","name":"","type":{"name":"离心机"},"state":"","isgood":"","temperature":"1","speed":"1","time":"1","note":"1"}]}
			String iJson = getRequest().getParameter("templeat");//步骤
			String rJson = getRequest().getParameter("reagent");//试剂
			String cJson = getRequest().getParameter("cos");//设备
			this.newQtTaskService.saveTemplate(iJson,rJson,cJson);
			System.out.println("步骤===="+iJson);
			System.out.println("试剂===="+rJson);
			System.out.println("设备===="+cJson);
			map.put("success",true);
		}catch(Exception e ){
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//根据主表id判断结果是否已经生成
	@Action(value = "selResult", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void selResult()throws Exception{
		Map <String,Object> map = new HashMap<String,Object>();
		try{
			String formId = getRequest().getParameter("formId");
			List<QtTaskInfo> list = this.newQtTaskService.selResult(formId);
			System.out.println("结果表数据======"+list);
			if(list.size()>0){
				map.put("data", list);
			}else{
				map.put("data", "0");
			}
			map.put("success", true);
		}catch(Exception e){
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//生成结果并保存
	@Action(value = "saveResult", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void saveResult() throws Exception{
		Map <String,Object> map = new HashMap<String,Object>();
		try{
			//明细表数据
			//{"strobj":[{"id":"402880415b3d1af0015b3d3ce1810061","cord":"A1","code":"JB1704050004","sampleCode":"JB1704050004","productName":"医学健康",
			//"sampleNum":"2","sampleType":"精斑","productNum":"10000"},
			//{"id":"402880415b3d1af0015b3d3ce1880062","cord":"B1","code":"JB1704050005","sampleCode":"JB1704050005","productName":"医学健康","sampleNum":"3","sampleType":"精斑","productNum":"10000"},
			//{"id":"402880415b3d1af0015b3d3ce1880063","cord":"C1","code":"JB1704050006","sampleCode":"JB1704050006","productName":"科技服务","sampleNum":"1","sampleType":"精斑","productNum":"10000"}]}
			//获取明细表josn串
			String sJson = getRequest().getParameter("strobj");
			//获取相关主表ID
			String formId = getRequest().getParameter("formId");
			System.out.println("明细表数据======="+sJson);
			
			List<QtTaskInfo> list = this.newQtTaskService.saveResult(sJson,formId);
			
			map.put("data", list);
			map.put("success", true);
		}catch (Exception e){
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
//	//跟新结果表信息
	@Action(value = "updResult", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void updResult() throws Exception{
		
		Map <String,Object> map = new HashMap<String,Object>();
		try{
			
			//[{"id":"402880335b8e6298015b8e64a3ce0004","code":"JB1704050006BB04","sampleType":"精斑",
			//"productName":"科技服务","dicSampleType":"","contraction":"","volume":"","sampleNum":"",
			//"od260":"","od280":"","rin":"","sampleConsumeV":"","totalGrade":"","zlGrade":"",
			//"result":" 合格 ","nextFlow":"","submit":"","note":""},
			String sJson = getRequest().getParameter("strobj");
			
			this.newQtTaskService.updResult(sJson);
			
			map.put("success", true);
		}catch (Exception e){
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	
	//核对编码
		@Action(value = "selTaskItemByCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void selTaskItemByCode() throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				String formId = getParameterFromRequest("formId");
				String strobj = getParameterFromRequest("strobj");
				String code[] = strobj.split(",");
				List<String> strList = new ArrayList<String>();
				List<QtTaskItem> list = new ArrayList<QtTaskItem>();
				List<QtTaskItem> list2 = new ArrayList<QtTaskItem>();
				int j = 0;
				for (int i = 0; i < code.length; i++) {
					list = newQtTaskService.selTaskItemByCode(code[i],formId);
					if (list.size() != 0) {
						list2.addAll(list);
					} else {
						String codes = code[i];
						strList.add(codes);
					}
				}
				String[] notFindCode = (String[]) strList.toArray(new String[0]);
				if (notFindCode != null) {
					map.put("notFindCode", notFindCode);
				}else{
					map.put("notFindCode", "");
				}
				
				// 查询实验模板
				List<QtTask> task = this.newQtTaskService
						.finTemplate(formId);
				String templateId = task.get(0).getTemplate().getId();
				String templateName = task.get(0).getTemplate().getName();
				// 根据实验模板ID查询实验模板主表
				List<Template> template = this.templateService
						.finTemplateTask(templateId);
				String name = "";
				String colNum = "";
				String rowNum = "";
				if (template.get(0).getStorageContainer()!=null && !"".equals(template.get(0).getStorageContainer())) {
					name = template.get(0).getStorageContainer().getName();
					colNum = template.get(0).getStorageContainer().getColNum()
							.toString();
					rowNum = template.get(0).getStorageContainer().getRowNum()
							.toString();
				}


				map.put("name", name);
				map.put("colNum", colNum);
				map.put("rowNum", rowNum);
				
				map.put("data", list2);
				map.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
		
		
	
}
