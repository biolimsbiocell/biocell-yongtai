﻿package com.biolims.experiment.qt.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskWaitManage;
import com.biolims.experiment.qt.service.QtTaskManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/qt/qtTaskManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QtTaskManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2514";
	@Autowired
	private QtTaskManageService qtTaskManageService;
	private QtTaskWaitManage qtTaskManage = new QtTaskWaitManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showQtTaskManageEditList")
	public String showQtTaskManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskManageEdit.jsp");
	}

	@Action(value = "showQtTaskManageList")
	public String showQtTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskManage.jsp");
	}

	@Action(value = "showQtTaskManageListJson")
	public void showQtTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskManageService.findQtTaskManageList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskInfo> list = (List<QtTaskInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("patientName", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("result", "");
		map.put("submit", "");
		map.put("sumVolume", "");
		map.put("rin", "");
		map.put("contraction", "");
		map.put("od260", "");
		map.put("od280", "");
		map.put("tempId", "");
		map.put("isToProject", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("state", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("qtTask-name", "");
		map.put("qtTask-id", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("orderType", "");
		map.put("taskId", "");
		map.put("classify", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("sampleType", "");
		map.put("labCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qtTaskManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQtTaskManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskManageDialog.jsp");
	}

	@Action(value = "showDialogQtTaskManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQtTaskManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskManageService.findQtTaskManageList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskWaitManage> list = (List<QtTaskWaitManage>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("method", "");
		map.put("nextFlow", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQtTaskManage")
	public String editQtTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qtTaskManage = qtTaskManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qtTaskManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskManageEdit.jsp");
	}

	@Action(value = "copyQtTaskManage")
	public String copyQtTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qtTaskManage = qtTaskManageService.get(id);
		qtTaskManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qtTaskManage.getId();
		if (id != null && id.equals("")) {
			qtTaskManage.setId(null);
		}
		Map aMap = new HashMap();
		qtTaskManageService.save(qtTaskManage, aMap);
		return redirect("/experiment/qt/qtTaskManage/editQtTaskManage.action?id="
				+ qtTaskManage.getId());

	}

	@Action(value = "viewQtTaskManage")
	public String toViewQtTaskManage() throws Exception {
		String id = getParameterFromRequest("id");
		qtTaskManage = qtTaskManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskManageEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QtTaskManageService getQtTaskManageService() {
		return qtTaskManageService;
	}

	public void setQtTaskManageService(QtTaskManageService qtTaskManageService) {
		this.qtTaskManageService = qtTaskManageService;
	}

	public QtTaskWaitManage getQtTaskManage() {
		return qtTaskManage;
	}

	public void setQtTaskManage(QtTaskWaitManage qtTaskManage) {
		this.qtTaskManage = qtTaskManage;
	}

	/**
	 * 保存中间产物
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveQtTaskManage")
	public void saveQtTaskAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			qtTaskManageService.saveQtTaskInfoManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 保存样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveQtTaskItemManager")
	public void saveQtTaskItemManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			qtTaskManageService.saveQtTaskItemManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showQtTaskItemManagerList")
	public String showQtTaskItemManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskItemManage.jsp");
	}

	@Action(value = "showQtTaskItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskItemManagerListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// 排序方式
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = qtTaskManageService
					.findQtTaskItemList(map2Query, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QtTaskItem> list = (List<QtTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("nextFlow", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleConsume", "");
			map.put("labCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 样本管理明细入库
	 * 
	 * @throws Exception
	 */
	@Action(value = "QtTaskManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qtTaskManageService.QtTaskManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 样本管理明细QPCR实验待办
	 * 
	 * @throws Exception
	 */
	@Action(value = "QtTaskManageItemTiqu")
	public void QtTaskManageItemTiqu() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			qtTaskManageService.QtTaskManageItemTiqu(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
