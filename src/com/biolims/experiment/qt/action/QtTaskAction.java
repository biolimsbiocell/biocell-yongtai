﻿package com.biolims.experiment.qt.action;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.qt.dao.QtTaskDao;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskCos;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskReagent;
import com.biolims.experiment.qt.model.QtTaskReceiveItem;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.qt.model.QtTaskTemplate;
import com.biolims.experiment.qt.service.QtTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/qt/qtTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QtTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2511";
	@Autowired
	private QtTaskService qtTaskService;
	private QtTask qtTask = new QtTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private QtTaskDao qtTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;

	@Action(value = "showQtTaskList")
	public String showQtTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTask.jsp");
	}

	@Action(value = "showQtTaskListJson")
	public void showQtTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskService.findQtTaskList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QtTask> list = (List<QtTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("productName", "");
		map.put("productId", "");
		map.put("createDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qtTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQtTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskDialog.jsp");
	}

	@Action(value = "showDialogQtTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQtTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qtTaskService.findQtTaskList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QtTask> list = (List<QtTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQtTask")
	public String editQtTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			qtTask = qtTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qtTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			qtTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qtTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			qtTask.setCreateDate(stime);
			qtTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qtTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(qtTask.getState());
		putObjToContext("maxNum", maxNum);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskEdit.jsp");
	}

	@Action(value = "copyQtTask")
	public String copyQtTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qtTask = qtTaskService.get(id);
		qtTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qtTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QtTask";
			String markCode = "QPCR";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qtTask.setId(autoID);
			qtTask.setOrders("1");
		}
		Map aMap = new HashMap();
		aMap.put("qtTaskItem", getParameterFromRequest("qtTaskItemJson"));
		aMap.put("qtTaskResult", getParameterFromRequest("qtTaskResultJson"));
		aMap.put("qtTaskTemplateItem",
				getParameterFromRequest("qtTaskTemplateItemJson"));
		aMap.put("qtTaskTemplateReagent",
				getParameterFromRequest("qtTaskTemplateReagentJson"));
		aMap.put("qtTaskTemplateCos",
				getParameterFromRequest("qtTaskTemplateCosJson"));
		qtTaskService.save(qtTask, aMap);
		qtTaskService.sortTwo(id, qtTask.getOrders());
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/qt/qtTask/editQtTask.action?id="
				+ qtTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			qtTask = commonService.get(QtTask.class, id);

			Map aMap = new HashMap();
			aMap.put("qtTaskItem", getParameterFromRequest("qtTaskItemJson"));
			aMap.put("qtTaskResult",
					getParameterFromRequest("qtTaskResultJson"));
			aMap.put("qtTaskTemplateItem",
					getParameterFromRequest("qtTaskTemplateItemJson"));
			aMap.put("qtTaskTemplateReagent",
					getParameterFromRequest("qtTaskTemplateReagentJson"));
			aMap.put("qtTaskTemplateCos",
					getParameterFromRequest("qtTaskTemplateCosJson"));
			map = qtTaskService.save(qtTask, aMap);
			qtTaskService.sortTwo(id, qtTask.getOrders());
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewQtTask")
	public String toViewQtTask() throws Exception {
		String id = getParameterFromRequest("id");
		qtTask = qtTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskEdit.jsp");
	}

	/**
	 * 查询左侧中间表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQtTaskTempList")
	public String showQtTaskTempList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskTemp.jsp");
	}

	@Action(value = "showQtTaskTempJson")
	public void showQtTaskTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		String scId = getRequest().getParameter("id");
		Map<String, Object> result = qtTaskService.findQtTaskTempList(scId,
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QtTaskTemp> list = (List<QtTaskTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleId", "");
		map.put("sampleName", "");
		map.put("location", "");
		map.put("patientName", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sequenceFun", "");
		map.put("orderId", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("inspectDate", "");
		map.put("reportDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("method", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("sampleNum", "");
		map.put("labCode", "");
		map.put("sampleInfo-id", "");
		map.put("sampleInfo-note", "");
		map.put("techJkServiceTask-id", "");
		map.put("techJkServiceTask-name", "");
		map.put("techJkServiceTask-sequenceBillDate", "yyyy-MM-dd");
		map.put("barCode", "");
		map.put("dataTraffic", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showQtTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskItem.jsp");
	}

	@Action(value = "showQtTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qtTaskService.findQtTaskItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskItem> list = (List<QtTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("counts", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("qtTask-name", "");
			map.put("qtTask-id", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			map.put("chromosomalLocation", "");
			map.put("primerNumber", "");
			map.put("leftPrimer", "");
			map.put("rightPrimer", "");
			map.put("ampliconid", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("blx", "");
			map.put("sampleSyNum", "");
			map.put("num", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");

			map.put("dnaConcentration", "");
			map.put("qpcrLineNum", "");
			map.put("qpcrColNum", "");
			map.put("barCode", "");
			map.put("dataTraffic", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQtTaskItem")
	public void delQtTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			if (ids != null) {
				qtTaskService.delQtTaskItem(ids);
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQtTaskInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskInfo.jsp");
	}

	@Action(value = "showQtTaskInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qtTaskService.findQtTaskInfoList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskInfo> list = (List<QtTaskInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("dataTraffic", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sequenceFun", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("result", "");
			map.put("submit", "");
			map.put("sumVolume", "");
			map.put("rin", "");
			map.put("contraction", "");
			map.put("od260", "");
			map.put("od280", "");
			map.put("tempId", "");
			map.put("isToProject", "");
			map.put("volume", "");
			map.put("note", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("qtTask-name", "");
			map.put("qtTask-id", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("taskId", "");
			map.put("classify", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("dataBits", "");
			map.put("qbcontraction", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			map.put("genotype", "");
			map.put("fragmentLength", "");
			map.put("chromosomalLocation", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");

			map.put("eighteens", "");
			map.put("facility", "");
			map.put("reaction", "");
			map.put("condition", "");
			map.put("ct", "");
			map.put("fold", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");

			map.put("referenceGene", "");
			map.put("cnPredicted", "");
			map.put("confidence", "");
			map.put("replicateCount", "");
			map.put("famCtMean", "");
			map.put("vicCtMean", "");
			map.put("zScore", "");
			map.put("batchNumber", "");
			map.put("barCode", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQtTaskInfo")
	public void delQtTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qtTaskService.delQtTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QtTaskService getQtTaskService() {
		return qtTaskService;
	}

	public void setQtTaskService(QtTaskService qtTaskService) {
		this.qtTaskService = qtTaskService;
	}

	public QtTask getQtTask() {
		return qtTask;
	}

	public void setQtTask(QtTask qtTask) {
		this.qtTask = qtTask;
	}

	/**
	 * 模板
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskTemplateItem.jsp");
	}

	@Action(value = "showTemplateItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qtTaskService.findTemplateItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskTemplate> list = (List<QtTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUserId", "");
			map.put("testUserName", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("qtTask-name", "");
			map.put("qtTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItem")
	public void delTemplateItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qtTaskService.delTemplateItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateItemOne")
	public void delTemplateItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qtTaskService.delTemplateItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQtTaskTemplateReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskTemplateReagentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskTemplateReagent.jsp");
	}

	@Action(value = "showQtTaskTemplateReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskTemplateReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = qtTaskService.findReagentItemList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskReagent> list = (List<QtTaskReagent>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("qtTask-name", "");
			map.put("qtTask-id", "");
			map.put("sn", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("reagentCode", "");
			map.put("isRunout", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQtTaskTemplateReagent")
	public void delQtTaskTemplateReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qtTaskService.delReagentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateReagentOne")
	public void delTemplateReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qtTaskService.delTemplateReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQtTaskTemplateCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskTemplateCosList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskTemplateCos.jsp");
	}

	@Action(value = "showQtTaskTemplateCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskTemplateCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = qtTaskService.findCosItemList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskCos> list = (List<QtTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("state", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			map.put("temperature", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("qtTask-name", "");
			map.put("qtTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQtTaskTemplateCos")
	public void delQtTaskTemplateCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qtTaskService.delCosItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTemplateCosOne")
	public void delQtTaskTemplateCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qtTaskService.delQtTaskTemplateCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 获取从样本接收来的样本
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showQtTaskFromReceiveList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQtTaskFromReceiveList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qt/qtTaskFromReceiveLeft.jsp");
	}

	@Action(value = "showQtTaskFromReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQtTaskFromReceiveListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qtTaskService
					.selectQtTaskFromReceiveList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QtTaskReceiveItem> list = (List<QtTaskReceiveItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("patientName", "");
			map.put("sampleCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("orderId", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("sampleName", "");
			map.put("location", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("qtTaskReceive-id", "");
			map.put("qtTaskReceive-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 判断样本做QPCR实验的次数
	 * 
	 * @throws Exception
	 */
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = qtTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 模板
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qt/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qtTaskService.findTemplateItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QtTaskTemplate> list = (List<QtTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("qtTask-name", "");
			map.put("qtTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据选中的步骤异步加载相关的试剂数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.qtTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据选中的步骤异步加载相关的设备数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.qtTaskService.setCos(
					id, code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 查询引物
	 * 
	 * @throws Exception
	 */
	@Action(value = "selectMappingPrimersLibrary", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectMappingPrimersLibrary() throws Exception {
		String productId = getParameterFromRequest("productId");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<MappingPrimersLibraryItem> listItem = new ArrayList<MappingPrimersLibraryItem>();
			// 获取引物库
			// List<MappingPrimersLibrary> list = qtTaskDao
			// .getMappingPrimersLibrary(productId);
			// System.out.println("+++++++++++++++++++++++" +
			// list.get(0).getId());
			// // 获取引物库明细
			// if (list.size() > 0) {
			listItem = qtTaskDao.getMappingPrimersLibraryItem(productId);
			// }
			int num = listItem.size();
			result.put("success", true);
			result.put("data", listItem);
			result.put("num", num);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 提交样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "submit", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submit() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.qtTaskService.submit(id, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询科研样本
	@Action(value = "selTechJkServicTaskByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selTechJkServicTaskByid() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, String> map = new HashMap<String, String>();
		map.put("techJkServiceTask.id", id);
		Map<String, Object> map2 = qtTaskService.findQtTaskTempList(null, map,
				null, null, null, null);
		List<QtTaskTemp> list = (List<QtTaskTemp>) map2.get("list");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 点击横向或竖向排序按钮进行排序
	 * 
	 * @throws Exception
	 */
	@Action(value = "sortTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void sortTwo() throws Exception {
		String id = getRequest().getParameter("id"); // 主表的ID
		String order = getRequest().getParameter("order"); // 排序方式0:横向 1:竖向
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.qtTaskService.sortTwo(id, order);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 保存排序(选最后面的移动的质控品序号向前移动不打乱，移动位置之后的样本序号相应改变不打乱)
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveItem() throws Exception {
		String id = getRequest().getParameter("id"); // 主表的ID
		String ids = getRequest().getParameter("ids"); // 选中的样本（质控品）ID字符串
		int local = Integer.valueOf(getRequest().getParameter("local"));// 要移动到那个位置下
		String st = getRequest().getParameter("st"); // 选中的样本（质控品）ID数组
		String order = getRequest().getParameter("order");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.qtTaskService.saveItem(id, ids, local, st, order);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: downLoadTemp
	 * @Description: TODO(核算提取模板下载)
	 * @param @param ids
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-22 上午11:46:55
	 * @throws
	 */
	@Action(value = "downLoadTemp")
	public void downLoadTemp() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String dna = "qpcr";
		String[] ids = getRequest().getParameterValues("ids");
		String[] codes = getRequest().getParameterValues("code");
		String id = ids[0];
		String code = codes[0];
		String[] str1 = code.split(",");
		String co = str1[0];
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		String a = co + sdf.format(date);
		Properties properties = new Properties();
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("system.properties");
		properties.load(is);

		String filePath = properties.getProperty("result.template.file") + dna
				+ "\\";// 写入csv路径
		String fileName = filePath + a + ".csv";// 文件名称
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		} else {
			parent.delete();
			parent.mkdirs();
		}
		csvFile.createNewFile();
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "样本编号", "原始样本编号", "条形码", "FAM Ct Mean",
				"VIC Ct Mean", "基因型", "位点" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",")
					.toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		String[] sid = id.split(",");
		for (int j = 0; j < sid.length; j++) {
			String idc = sid[j];
			for (int i = 0; i < ids.length; i++) {
				QtTaskInfo sr = qtTaskService.getQtTaskInfoByIds(idc);
				StringBuffer sb = new StringBuffer();
				setMolecularMarkersData(sr, sb);
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
				if (sr.equals("")) {
					result.put("success", false);
				} else {
					result.put("success", true);

				}
			}
		}
		csvWtriter.flush();
		csvWtriter.close();
		// HttpUtils.write(JsonUtils.toJsonString(result));
		downLoadTemp3(a, filePath);
	}

	@Action(value = "downLoadTemp3")
	public void downLoadTemp3(String a, String filePath2) throws Exception {
		String filePath = filePath2;// 保存窗口中显示的文件名
		String fileName = a + ".csv";// 保存窗口中显示的文件名
		super.getResponse().setContentType("APPLICATION/OCTET-STREAM");

		/*
		 * 要显示到客户端的文件名转码是必需的，特别是中文名 否则可能出现文件名乱码甚至是浏览器显示无法下载的问题
		 */
		ServletOutputStream out = null;
		// PrintWriter out = null;
		InputStream inStream = null;
		try {
			fileName = super.getResponse().encodeURL(
					new String(fileName.getBytes("UTF-8"), "ISO8859_1"));//

			super.getResponse().setHeader("Content-Disposition",
					"attachment; filename=\"" + fileName + "\"");
			// inline
			out = super.getResponse().getOutputStream();

			inStream = new FileInputStream(filePath + toUtf8String(fileName));

			// 循环取出流中的数据
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			super.getResponse().setStatus(super.getResponse().SC_OK);
			super.getResponse().flushBuffer();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RuntimeException e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
			inStream.close();
		}
	}

	/**
	 * 
	 * @Title: toUtf8String
	 * @Description: TODO(解决乱码)
	 * @param @param s
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-23 下午4:40:07
	 * @throws
	 */
	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * @Title: setMolecularMarkersData
	 * @Description: TODO(将对应的值添加到模板里)
	 * @param @param sr
	 * @param @param sb
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-22 下午1:21:58
	 * @throws
	 */
	public void setMolecularMarkersData(QtTaskInfo sr, StringBuffer sb)
			throws Exception {
		sb.append("\"").append(sr.getCode()).append("\",");
		if (null != sr.getSampleCode()) {
			sb.append("\"").append(sr.getSampleCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
		if (null != sr.getBarCode()) {
			sb.append("\"").append(sr.getBarCode()).append("\",");
		} else {
			sb.append("\"").append("").append("\",");
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: updateForUpload
	 * @Description: TODO(查找对应要上次的数据)
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-23 上午11:24:06
	 * @throws
	 */
	@Action(value = "updateForUpload")
	public void updateForUpload() throws Exception {
		String[] code = getRequest().getParameterValues("code");
		String[] famCtMean = getRequest().getParameterValues("famCtMean");
		String[] vicCtMean = getRequest().getParameterValues("vicCtMean");
		String[] genotype = getRequest().getParameterValues("genotype");
		String[] chromosomalLocation = getRequest().getParameterValues(
				"chromosomalLocation");
		String[] results = getRequest().getParameterValues("result");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.qtTaskService.updateForUpload(code, famCtMean, vicCtMean,
					genotype, chromosomalLocation, results);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
