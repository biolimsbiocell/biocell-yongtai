package com.biolims.experiment.qt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.qt.dao.QtTaskAbnormalDao;
import com.biolims.experiment.qt.model.QtTaskAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QtTaskAbnormalService {
	@Resource
	private QtTaskAbnormalDao qtTaskAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;

	@Resource
	private CommonService commonService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQtTaskAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qtTaskAbnormalDao.selectQtTaskAbnormalList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QtTaskAbnormal i) throws Exception {
		qtTaskAbnormalDao.saveOrUpdate(i);
	}

	public QtTaskAbnormal get(String id) {
		QtTaskAbnormal qtTaskAbnormal = commonDAO.get(QtTaskAbnormal.class, id);
		return qtTaskAbnormal;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QtTaskAbnormal sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qtTaskAbnormalDao.saveOrUpdate(sc);
			String jsonStr = "";
		}
	}

	/**
	 * 保存异常QPCR实验样本
	 * 
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQtTaskAbnormalList(String itemDataJson) throws Exception {
		List<QtTaskAbnormal> saveItems = new ArrayList<QtTaskAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskAbnormal sbi = new QtTaskAbnormal();
			sbi = (QtTaskAbnormal) qtTaskAbnormalDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			if (sbi != null && sbi.getIsExecute() != null
					&& sbi.getNextFlowId() != null) {
				// 确认执行
				if (sbi.getIsExecute().equals("1")) {
					if (sbi.getResult().equals("1")) {// 合格
						String nextFlowId = sbi.getNextFlowId();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(sbi.getCode());
							st.setCode(sbi.getCode());
							st.setNum(sbi.getSampleNum());
							st.setState("1");
							qtTaskAbnormalDao.saveOrUpdate(st);
							// 入库，改变Info中原始样本的状态为“待入库”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(sbi.getSampleCode());
							if (sf != null) {
								sf.setState("3");
								sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
							}
						} else if (nextFlowId.equals("0010")) {
							// 重QPCR实验
							QtTaskAbnormal eda = new QtTaskAbnormal();
							sbi.setState("4");
							// 状态为4 在重QPCR实验中显示
							sampleInputService.copy(eda, sbi);
						} else if (nextFlowId.equals("0012")) {
							// 暂停，改变Info中原始样本的状态为“实验暂停”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(sbi.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
							}
						} else if (nextFlowId.equals("0013")) {
							// 终止，改变Info中原始样本的状态为“实验终止”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(sbi.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
							}
						} else if (nextFlowId.equals("0014")) {
							// 下一步流向是：反馈至项目组
						} else if (nextFlowId.equals("0031")) {

							SampleReportTemp st = new SampleReportTemp();

							// st.setGenotype(sbi.getGenotype());
							st.setCode(sbi.getCode());

							SampleInfo si = new SampleInfo();
							st.setSampleCode(sbi.getSampleCode());
							List<SampleInfo> sil = commonService.get(
									SampleInfo.class, "code",
									sbi.getSampleCode());
							if (sil.size() > 0)
								si = sil.get(0);

							st.setPatientName(si.getPatientName());
							st.setProductId(si.getProductId());
							st.setProductName(si.getProductName());
							if (sbi.getInspectDate() != null
									&& !sbi.getInspectDate().equals("")) {
								st.setInspectDate(DateUtil.parse(si
										.getInspectDate()));
							}
							if (sbi.getReportDate() != null
									&& !sbi.getReportDate().equals("")) {
								st.setReportDate(DateUtil.parse(si
										.getReportDate()));
							}
							// st.setVolume(sbi.getVolume());
							// st.setUnit(sbi.getUnit());
							st.setIdCard(sbi.getIdCard());
							st.setSequenceFun(sbi.getSequenceFun());
							st.setPhone(sbi.getPhone());
							st.setOrderNum(si.getOrderNum());
							st.setState(sbi.getState());
							st.setNote(sbi.getNote());
							st.setClassify(sbi.getClassify());
							qtTaskAbnormalDao.saveOrUpdate(st);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								sbi.setState("1");
								sampleInputService.copy(o, sbi);
							}
						}
						sbi.setState("1");
					}
				}
			}
			saveItems.add(sbi);
		}
		qtTaskAbnormalDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 根据条件检索数据
	 * 
	 * @param code
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> selectAbnormal(String code, String Code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qtTaskAbnormalDao.selectAbnormal(code,
				Code);
		List<QtTaskAbnormal> list = (List<QtTaskAbnormal>) result.get("list");
		if (list != null && list.size() > 0) {
			for (QtTaskAbnormal srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("code", srai.getCode());
				map.put("sampleCode", srai.getSampleCode());
				map.put("sampleType", srai.getSampleType());
				map.put("sampleCondition", srai.getSampleCondition());
				map.put("isExecute", srai.getIsExecute());
				map.put("nextFlow", srai.getNextFlow());
				map.put("note", srai.getNote());
				map.put("feedbackTime", srai.getFeedbackTime());
				map.put("method", srai.getMethod());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
