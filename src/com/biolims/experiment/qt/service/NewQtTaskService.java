package com.biolims.experiment.qt.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.qt.dao.NewQtTaskDao;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskCos;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskReagent;
import com.biolims.experiment.qt.model.QtTaskTemplate;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.DicSampleType;
import com.biolims.workflow.service.WorkflowService;
@Service
public class NewQtTaskService extends WorkflowService{

	
	@Resource
	private CodingRuleService codingRuleService;

	@Resource
	private NewQtTaskDao newQtTaskDao;
	
	//根据主表id判断结果是否已经生成
	public List<QtTaskInfo> selResult(String formId) throws Exception {
		return newQtTaskDao.selResult(formId);
		
	}
	//点击待办事项进入任务
	public List<QtTaskItem> findTaskItem(String formId) throws Exception{
		return newQtTaskDao.selTaskItem(formId);
	}
	
	//查询实验模板ID
	public List<QtTask> finTemplate(String formId) throws Exception{
		return newQtTaskDao.selTemplat(formId);
	}
	//根据主表ID查询模板步骤明细
	public List<QtTaskTemplate> selTemplateItem(String formId) throws Exception{
		return newQtTaskDao.selTemplateItem(formId);
	}
	//根据主表ID查询模板试剂明细
	public List<QtTaskReagent> selReagent(String formId) throws Exception{
		return newQtTaskDao.selReagent(formId);
	}
	//根据主表ID查询模板设备明细
	public List<QtTaskCos> selCos(String formId) throws Exception{
		return newQtTaskDao.selCos(formId);
	}

	
	//保存明细表数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(String sJson) throws Exception{

		JSONObject  dataJson=new JSONObject(sJson);
		JSONArray data=dataJson.getJSONArray("strobj");
		int size = data.length();
		for(int i=0;i<size;i++){
			JSONObject info=data.getJSONObject(i);
			String id=info.getString("id");
			//根据编码查询明细表数据
//			编码：id， 样本编号：code ， 原始样本号：sampleCode，DNA 浓度 （ ng/ul ）:dnaConcentration,
//			QPCR反应孔行号:qpcrLineNum,QPCR反应孔列号:qpcrColNum ,位点：chromosomalLocation
			QtTaskItem dti = newQtTaskDao.get(QtTaskItem.class,id);
			String code = info.getString("code");//样本编号
			String sampleCode = info.getString("sampleCode");//原始样本号
//			String cord = info.getString("cord");//行号列号
//			String rowCode = cord.substring(0,1);//列号
//			String colCode = cord.substring(1,cord.length());//行号
			dti.setCode(code);
			dti.setSampleCode(sampleCode);
			dti.setDnaConcentration(info.getString("dnaConcentration"));
			dti.setQpcrLineNum(info.getString("qpcrLineNum"));
			dti.setQpcrColNum(info.getString("qpcrColNum"));
			dti.setChromosomalLocation(info.getString("chromosomalLocation"));
			newQtTaskDao.saveOrUpdate(dti);
		}
	}
	//修改后点击下一步保存实验模板
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTemplate(String iJson,String rJson,String cJson) throws Exception{
		JSONObject itemJson=new JSONObject(iJson);
		JSONObject reagentJson=new JSONObject(rJson);
		JSONObject cosJson=new JSONObject(cJson);
		JSONArray item=itemJson.getJSONArray("templeat");
		JSONArray reagent=reagentJson.getJSONArray("reagent");
		JSONArray cos=cosJson.getJSONArray("cos");
		int itemSize = item.length();
		int reagentSize = reagent.length();
		int cosSize = cos.length();
		//保存更新实验步骤表
		for(int i=0;i<itemSize;i++){
			JSONObject info=item.getJSONObject(i);
			String id=info.getString("id");
			//根据编码查询明细表数据
			QtTaskTemplate dti = newQtTaskDao.get(QtTaskTemplate.class,id);
			String code = info.getString("code");
			String note = info.getString("note");
			String startTime = info.getString("startTime");
			String endTime = info.getString("endTime");
			String userId = info.getString("userId");
			if(userId!=null && !"".equals(userId)){
				User u =  newQtTaskDao.get(User.class, userId);
				dti.setTestUserId(userId);
				dti.setTestUserName(u.getName());
			}
			if(!"".equals(code)){
				dti.setCode(code);
			}
			dti.setNote(note);
			dti.setStartTime(startTime);
			dti.setEndTime(endTime);
			newQtTaskDao.saveOrUpdate(dti);
		}
		//保存更新试剂明细表
		for(int i=0;i<reagentSize;i++){
			JSONObject info=reagent.getJSONObject(i);
			String id=info.getString("id");
			//根据编码查询明细表数据
			QtTaskReagent dti = newQtTaskDao.get(QtTaskReagent.class,id);
			String sn = info.getString("sn");
			String oneNum = info.getString("oneNum");
			String num = info.getString("num");
			dti.setSn(sn);
			if(!"".equals(oneNum)){
				dti.setOneNum(Double.valueOf(oneNum));
			}
			if(!"".equals(num)){
				dti.setNum(Double.valueOf(num));
			}
			newQtTaskDao.saveOrUpdate(dti);
		}
		//保存更新设备明细表
		for(int i=0;i<cosSize;i++){
			JSONObject info=cos.getJSONObject(i);
			String id=info.getString("id");
			//根据编码查询明细表数据
			QtTaskCos dti = newQtTaskDao.get(QtTaskCos.class,id);
//			{"cos":[{"id":"402880335b79d258015b79d4e7710007","code":"","name":"","type":{"name":"离心机"},
//				"state":"","isgood":"","temperature":"1","speed":"1","time":"1","note":"1"}]}
			String temperature = info.getString("temperature");
			String speed = info.getString("speed");
			if(!"".equals(temperature)){
				dti.setTemperature(Double.valueOf(temperature));
			}
			dti.setSpeed(speed);
			newQtTaskDao.saveOrUpdate(dti); 
		}
	}
	
	//生成结果并保存
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<QtTaskInfo> saveResult(String sJson,String formId) throws Exception{

		JSONObject  dataJson=new JSONObject(sJson);
		JSONArray data=dataJson.getJSONArray("strobj");
		int size = data.length();
		List<QtTaskInfo> list =new ArrayList<QtTaskInfo>(); 
		for(int i=0;i<size;i++){
			JSONObject info=data.getJSONObject(i);
//			JSONArray dicSampleType=info.getJSONArray("dicSampleType");
//			JSONObject dst = dicSampleType.getJSONObject(i);
//			JSONObject dst = info.getJSONObject("dicSampleType");
			
//			String dicSampleTypeId = info.getString("dicSampleTypeId");
//			String productNums = info.getString("productNum").trim();
//			int productNum = Integer.parseInt(productNums);
			//根据中间产物数量生成结果
//			for(int j=0;j<productNum;j++){
				QtTaskInfo dsi = new QtTaskInfo();
				
				if (dsi.getCode() == null || dsi.getCode().equals("")) {
					String markCode = "";
//					if (info.getString("sampleType") != null
//							|| !info.getString("sampleType").equals("")) {
//						DicSampleType d = newQtTaskDao.get(DicSampleType.class, dicSampleTypeId);
//						dsi.setDicSampleType(d);
						String[] sampleCode = info.getString("sampleCode").split(",");
						if (sampleCode.length > 1) {
							String str = sampleCode[0].substring(0,
									sampleCode[0].length() - 4);
							for (int k = 0; k < sampleCode.length; k++) {
								str += sampleCode[k].substring(
										sampleCode[k].length() - 4,
										sampleCode[k].length());
							}
//							if (d == null) {
								markCode = str;
//							} else {
//								markCode = str + d.getCode();
//							}
						} else {
//							if (d == null) {
								markCode = info.getString("sampleCode");
//							} else {
//								markCode = info.getString("sampleCode") + d.getCode();
//							}
						}
//					}
					String code = codingRuleService.getCode("QtTaskInfo",
							markCode, 00, 2, null);
//					dsi.setCode(code);
					dsi.setCode(info.getString("code"));
				}
//				编码：id， 样本编号：code ， 原始样本号：sampleCode，参考基因：referenceGene，预测拷贝数：cnPredicted,
//				可信度:confidence,Z-Score:zScore,平行重复:replicateCount ,
//				FAM Ct Mean:famCtMean ,VIC Ct Mean:vicCtMean,Batch number:batchNumber, 位点：chromosomalLocation。

				dsi.setSampleCode(info.getString("sampleCode"));
				dsi.setChromosomalLocation(info.getString("chromosomalLocation"));
				QtTask dt = new QtTask();
				dt.setId(formId);
				dsi.setQtTask(dt);
				this.newQtTaskDao.saveOrUpdate(dsi);
				list.add(dsi);
//			}
		}
		return list;
	}
	
//	//跟新结果表信息
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void updResult(String sJson) throws Exception{
		JSONObject  dataJson=new JSONObject(sJson);
		JSONArray data=dataJson.getJSONArray("strobj");
		int size = data.length();
		for(int i=0;i<size;i++){
			JSONObject info=data.getJSONObject(i);
			String id=info.getString("id");
			//根据编码查询明细表数据
//			编码：id， 样本编号：code ， 原始样本号：sampleCode，参考基因：referenceGene，预测拷贝数：cnPredicted,
//			可信度:confidence,Z-Score:zScore,平行重复:replicateCount ,
//			FAM Ct Mean:famCtMean ,VIC Ct Mean:vicCtMean,Batch number:batchNumber, 位点：chromosomalLocation。
			QtTaskInfo dti = newQtTaskDao.get(QtTaskInfo.class,id);
			String code = info.getString("code");//样本编号
			dti.setCode(code); 
			if(!"".equals(info.getString("sampleCode"))){
				dti.setSampleCode(info.getString("sampleCode"));
			}
			if(!"".equals(info.getString("referenceGene"))){
				dti.setReferenceGene((info.getString("referenceGene")));
			}
			if(!"".equals(info.getString("cnPredicted"))){
				dti.setCnPredicted(info.getString("cnPredicted"));
			}
			if(!"".equals(info.getString("confidence"))){
				dti.setConfidence(info.getString("confidence"));
			}
			if(!"".equals(info.getString("zScore"))){
				dti.setzScore(info.getString("zScore"));
			}
			if(!"".equals(info.getString("replicateCount"))){
				dti.setReplicateCount(info.getString("replicateCount"));
			}
			if(!"".equals(info.getString("famCtMean"))){
				dti.setFamCtMean(info.getString("famCtMean"));
			}
			if(!"".equals(info.getString("vicCtMean"))){
				dti.setVicCtMean(info.getString("vicCtMean"));
			}
			if(!"".equals(info.getString("batchNumber"))){
				dti.setBatchNumber(info.getString("batchNumber"));
			}
			if(!"".equals(info.getString("chromosomalLocation"))){
				dti.setChromosomalLocation(info.getString("chromosomalLocation"));
			}
			newQtTaskDao.saveOrUpdate(dti);
		}
	}

	// 根据样本编号查询待入库样本 即样本入库左侧表
	public List<QtTaskItem> selTaskItemByCode(String code, String formId)
			throws Exception {
		return newQtTaskDao.findTaskItemByCode(code, formId);
	}
		
}
