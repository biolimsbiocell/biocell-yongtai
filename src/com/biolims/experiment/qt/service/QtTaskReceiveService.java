package com.biolims.experiment.qt.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qt.dao.QtTaskReceiveDao;
import com.biolims.experiment.qt.model.QtTaskReceiveTemp;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.qt.model.QtTaskReceive;
import com.biolims.experiment.qt.model.QtTaskReceiveItem;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QtTaskReceiveService {
	@Resource
	private QtTaskReceiveDao qtTaskReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQtTaskReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qtTaskReceiveDao.selectQtTaskReceiveList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QtTaskReceive i) throws Exception {
		qtTaskReceiveDao.saveOrUpdate(i);
	}

	public QtTaskReceive get(String id) {
		QtTaskReceive qtTaskReceive = commonDAO.get(QtTaskReceive.class, id);
		return qtTaskReceive;
	}

	public Map<String, Object> findQtTaskReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskReceiveDao
				.selectQtTaskReceiveItemList(scId, startNum, limitNum,
						dir, sort);
		List<QtTaskReceiveItem> list = (List<QtTaskReceiveItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQtTaskReceiveItem(QtTaskReceive sc, String itemDataJson)
			throws Exception {
		List<QtTaskReceiveItem> saveItems = new ArrayList<QtTaskReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskReceiveItem scp = new QtTaskReceiveItem();
			// 将map信息读入实体类
			scp = (QtTaskReceiveItem) qtTaskReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQtTaskReceive(sc);
			saveItems.add(scp);
		}
		qtTaskReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQtTaskReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			QtTaskReceiveItem scp = qtTaskReceiveDao.get(
					QtTaskReceiveItem.class, id);
			qtTaskReceiveDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QtTaskReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qtTaskReceiveDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("qtTaskReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQtTaskReceiveItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 审批完成
	 * @param applicationTypeActionId
	 * @param id
	 */
	public void changeState(String applicationTypeActionId, String id) {
		QtTaskReceive sct = qtTaskReceiveDao.get(QtTaskReceive.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		this.qtTaskReceiveDao.update(sct);
		try {
			List<QtTaskReceiveItem> edri = this.qtTaskReceiveDao
					.selectReceiveByIdList(sct.getId());
			for (QtTaskReceiveItem ed : edri) {
				if (ed.getMethod().equals("1")) {
					QtTaskTemp dst = new QtTaskTemp();
					dst.setConcentration(ed.getConcentration());
					dst.setLocation(ed.getLocation());
					dst.setCode(ed.getCode());
//					dst.setId(ed.getId());
					dst.setSequenceFun(ed.getSequenceFun());
					dst.setPatientName(ed.getPatientName());
					dst.setProductName(ed.getProductName());
					dst.setProductId(ed.getProductId());
					dst.setIdCard(ed.getIdCard());
					dst.setInspectDate(ed.getInspectDate());
					dst.setPhone(ed.getPhone());
					dst.setOrderId(ed.getOrderId());
					dst.setReportDate(ed.getReportDate());
					dst.setState("1");
					dst.setClassify(ed.getClassify());
					dst.setCode(ed.getCode());
					dst.setSampleCode(ed.getSampleCode());
					dst.setSampleType(ed.getSampleType());
					dst.setSampleNum(ed.getSampleNum());
					dst.setLabCode(ed.getLabCode());
					qtTaskReceiveDao.saveOrUpdate(dst);
					QtTaskReceiveTemp temp = commonDAO.get(QtTaskReceiveTemp.class,
							ed.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}
				// 审批完成改变Info中的样本状态为“待QPCR实验QPCR实验”
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(ed
						.getCode());
				if (sf != null) {
					sf.setState("3");
					sf.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);
				}
				ed.setState("1");	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 审批完成之后，将明细表数据保存到QtTaskQPCR实验中间表
	 * @param code
	 */
	public void saveToQtTaskTemp(String code) {
		List<QtTaskReceiveItem> list = qtTaskReceiveDao
				.selectReceiveItemListById(code);
		if (list.size() > 0) {
			for (QtTaskReceiveItem ed : list) {
				QtTaskTemp dst = new QtTaskTemp();
				dst.setConcentration(ed.getConcentration());
				dst.setLocation(ed.getLocation());
				dst.setCode(ed.getCode());
				dst.setId(ed.getId());
				dst.setSequenceFun(ed.getSequenceFun());
				dst.setPatientName(ed.getPatientName());
				dst.setProductName(ed.getProductName());
				dst.setProductId(ed.getProductId());
				dst.setIdCard(ed.getIdCard());
				dst.setInspectDate(ed.getInspectDate());
				dst.setPhone(ed.getPhone());
				dst.setOrderId(ed.getOrderId());
				dst.setReportDate(ed.getReportDate());
				dst.setState("1");
				dst.setClassify(ed.getClassify());
				dst.setSampleCode(ed.getSampleCode());
				dst.setSampleType(ed.getSampleType());
				dst.setSampleNum(ed.getSampleNum());
				qtTaskReceiveDao.saveOrUpdate(dst);
			}
		}
	}
		public Map<String, Object> selectQtTaskReceiveTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = qtTaskReceiveDao.selectQtTaskReceiveTempList(
				startNum, limitNum, dir, sort);
		List<QtTaskReceiveTemp> list = (List<QtTaskReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
	
}
