package com.biolims.experiment.qt.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qt.dao.QtTaskDao;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskAbnormal;
import com.biolims.experiment.qt.model.QtTaskCos;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskInfoManager;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskReagent;
import com.biolims.experiment.qt.model.QtTaskReceiveItem;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.qt.model.QtTaskTemplate;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QtTaskService {
	@Resource
	private QtTaskDao qtTaskDao;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQtTaskList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return qtTaskDao.selectQtTaskList(mapForQuery, startNum, limitNum, dir,
				sort);
	}

	public Map<String, Object> findQtTaskTempList(String scId,
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qtTaskDao.selectQtTaskTempList(scId, mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QtTask i) throws Exception {
		qtTaskDao.saveOrUpdate(i);
	}

	public QtTask get(String id) {
		QtTask qtTask = commonDAO.get(QtTask.class, id);
		return qtTask;
	}

	public Map<String, Object> findQtTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<QtTaskItem> list = (List<QtTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findQtTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskInfoList(scId,
				startNum, limitNum, dir, sort);
		List<QtTaskInfo> list = (List<QtTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 模板
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskTemplateItemList(
				scId, startNum, limitNum, dir, sort);
		List<QtTaskTemplate> list = (List<QtTaskTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findReagentItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskTemplateReagentList(
				scId, startNum, limitNum, dir, sort);
		List<QtTaskReagent> list = (List<QtTaskReagent>) result.get("list");
		return result;
	}

	/**
	 * 根据步骤加载试剂明细
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = qtTaskDao
				.selectQtTaskTemplateReagentListByItemId(scId, startNum,
						limitNum, dir, sort, itemId);
		List<QtTaskReagent> list = (List<QtTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = qtTaskDao
				.selectQtTaskTemplateCosListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<QtTaskCos> list = (List<QtTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskTemplateCosList(
				scId, startNum, limitNum, dir, sort);
		List<QtTaskCos> list = (List<QtTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQtTaskItem(QtTask sc, String itemDataJson) throws Exception {
		List<QtTaskItem> saveItems = new ArrayList<QtTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = qtTaskDao.get(StorageContainer.class, temp
						.getStorageContainer().getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();
			}
		}
		Integer orderNumber = 1;
		for (Map<String, Object> map : list) {
			QtTaskItem scp = new QtTaskItem();
			// 将map信息读入实体类
			scp = (QtTaskItem) qtTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// if (scp.getOrderNumber() != null
			// && !scp.getOrderNumber().equals("")) {
			if (rowCode != null && colCode != null) {
				String row = "";
				String col = "";
				for (int i = 0; i < scp.getNum(); i++) {
					// Double num = Math.floor((scp.getOrderNumber() - 1)
					// / rowCode) + 1;
					// scp.setColCode(Integer.toString(num.intValue()));
					Double num = Math.floor((orderNumber - 1) / rowCode) + 1;
					col += Integer.toString(num.intValue());// + ",";
					if (orderNumber % rowCode == 0) {
						// scp.setRowCode(String.valueOf((char) (64 +
						// rowCode)));
						row += String.valueOf((char) (64 + rowCode));
						// + ",";
					} else {
						// scp.setRowCode(String.valueOf((char) (64 + scp
						// .getOrderNumber() % rowCode)));
						row += String.valueOf((char) (64 + orderNumber
								% rowCode));
						// + ",";
					}
					// scp.setCounts(String.valueOf((scp.getOrderNumber() -
					// 1)
					// / (rowCode * colCode) + 1));
					orderNumber++;
				}
				scp.setColCode(col);
				scp.setRowCode(row);

			}
			// }
			scp.setQtTask(sc);

			if (scp != null) {
				Template t = commonDAO.get(Template.class, sc.getTemplate()
						.getId());
				if (t != null) {
					if (t.getDicSampleType() != null) {
						DicSampleType d = commonDAO.get(DicSampleType.class, t
								.getDicSampleType().getId());
						if (d != null) {
							if (scp.getDicSampleType() == null
									|| (scp.getDicSampleType() != null && scp
											.getDicSampleType().equals(""))) {
								scp.setDicSampleType(d);
							}
						}
					}
					if (t.getProductNum() != null) {
						if (scp.getProductNum() == null
								|| (scp.getProductNum() != null && scp
										.getProductNum().equals(""))) {
							scp.setProductNum(t.getProductNum());
						}
					}
					if (t.getSampleNum() != null) {
						if (scp.getSampleConsume() == null) {
							scp.setSampleConsume(t.getSampleNum());
						}
					}
				}
			}
			saveItems.add(scp);
			if (scp != null) {
				QtTaskInfoManager dm = new QtTaskInfoManager();
				dm.setExpCode(scp.getExpCode());
				dm.setCode(scp.getCode());
				dm.setSampleCode(scp.getSampleCode());
				dm.setSampleId(scp.getTempId());
				dm.setSampleName(scp.getSampleName());
				dm.setSampleVolume(scp.getSampleVolume());
				dm.setAddVolume(scp.getAddVolume());
				dm.setSumVolume(scp.getSumVolume());
				dm.setIndexs(scp.getIndexs());
				dm.setConcentration(scp.getConcentration());
				dm.setReason(scp.getReason());
				dm.setResult(scp.getResult());
				dm.setStepNum(scp.getStepNum());
				dm.setPatientName(scp.getPatientName());
				dm.setProductId(scp.getProductId());
				dm.setProductName(scp.getProductName());
				dm.setPhone(scp.getPhone());
				dm.setReportDate(scp.getReportDate());
				dm.setIdCard(scp.getIdCard());
				dm.setInspectDate(scp.getInspectDate());
				dm.setOrderId(scp.getOrderId());
				dm.setSequenceFun(scp.getSequenceFun());
				qtTaskDao.saveOrUpdate(dm);
			}
			if("".equals(scp.getTempId())||scp.getTempId()==null){
				
			}else{
				QtTaskTemp dt = commonDAO.get(QtTaskTemp.class, scp.getTempId());
				if (dt != null) {
					dt.setState("2");
				}
			}
			
		}
		qtTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(qtTaskDao.selectCountMax(sc.getId())
						.toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}


	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQtTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			if (id != null) {
				QtTaskItem scp = qtTaskDao.get(QtTaskItem.class, id);
				qtTaskDao.delete(scp);
				if("".equals(scp.getTempId())||scp.getTempId()==null){
					
				}else{
					QtTaskTemp dt = commonDAO
							.get(QtTaskTemp.class, scp.getTempId());
					if (dt != null) {
						dt.setState("1");
					}
				}
				
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQtTaskResult(QtTask sc, String itemDataJson)
			throws Exception {
		DecimalFormat df = new DecimalFormat("######.000");
		List<QtTaskInfo> saveItems = new ArrayList<QtTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskInfo scp = new QtTaskInfo();
			// 将map信息读入实体类
			scp = (QtTaskInfo) qtTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQtTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						|| !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());
					markCode = scp.getCode() + d.getCode();
				}
				String code = codingRuleService.getCode("QtTaskInfo", markCode,
						00, 2, null);
				scp.setCode(code);
			}
			// Double b;
			// if (sc.getAcceptUser().getId().equals("D0004")) {
			// // QPCR实验实验组-组织
			// if (scp.getNextFlowId().equals("0022")) {
			// // 超声破碎
			// if (scp.getQbcontraction() < 11) {
			// scp.setVolume(180.0);
			// } else if (scp.getQbcontraction() >= 11
			// && scp.getQbcontraction() < 15) {
			// b = 2000 / scp.getQbcontraction();
			// scp.setVolume(Double.valueOf(df.format(b)));
			// } else if (scp.getQbcontraction() >= 15
			// && scp.getQbcontraction() <= 22) {
			// scp.setVolume(90.0);
			// } else if (scp.getQbcontraction() > 22) {
			// b = 2000 / scp.getQbcontraction();
			// scp.setVolume(Double.valueOf(df.format(b)));
			// }
			// } else if (scp.getNextFlowId().equals("0021")) {// QPCR
			// scp.setVolume(1.0);
			// } else if (scp.getNextFlowId().equals("0009")) {// 样本入库
			//
			// }
			// }
			// if (scp.getVolume() != null) {
			// if (scp.getQbcontraction() != null) {
			// b = scp.getVolume() * scp.getQbcontraction();
			// scp.setSampleNum(Double.valueOf(df.format(b)));
			// }
			// if (scp.getQbcontraction() == null
			// && scp.getContraction() != null) {
			// b = scp.getVolume() * scp.getContraction();
			// scp.setSampleNum(Double.valueOf(df.format(b)));
			// }
			// }
			List<SampleInfo> sil = qtTaskDao.findByProperty(SampleInfo.class,
					"code", scp.getSampleCode());
			if (sil.size() > 0) {
				SampleInfo si = sil.get(0);
				scp.setSampleInfo(si);
			}
			qtTaskDao.saveOrUpdate(scp);
		}
		// // 计算出组织实验组入库样本的体积
		// if (sc.getAcceptUser().getId().equals("D0004")) {
		// // QPCR实验实验组-组织
		// // 根据主表id查询结果表为入库样本的信息
		// List<QtTaskInfo> list1 = this.qtTaskDao
		// .setQtTaskInfo(sc.getId());
		// for (QtTaskInfo s : list1) {
		// // 根据入库的原始样本号查询该结果表信息
		// List<QtTaskInfo> list2 = this.qtTaskDao
		// .setQtTaskInfo2(sc.getId(), s.getCode());
		// Double v = 0.0;
		// for (QtTaskInfo s2 : list2) {
		// if (!s2.getNextFlowId().equals("0009")
		// && s2.getVolume() != null) {
		// v += s2.getVolume();
		// }
		// }
		// s.setVolume(Double.valueOf(df.format(200 - v)));
		// if (s.getVolume() != null) {
		// if (s.getQbcontraction() != null) {
		// s.setSampleNum(Double.valueOf(df.format(s.getVolume()
		// * s.getQbcontraction())));
		// }
		// if (s.getQbcontraction() == null
		// && s.getContraction() != null) {
		// s.setSampleNum(Double.valueOf(df.format(s.getVolume()
		// * s.getContraction())));
		// }
		// }
		// }
		// }
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQtTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			QtTaskInfo scp = qtTaskDao.get(QtTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				qtTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(QtTask sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			qtTaskDao.saveOrUpdate(sc);
			// if (sc.getTemplate() != null) {
			// String id = sc.getTemplate().getId();
			// templateService.setCosIsUsedSave(id);
			// }
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("qtTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQtTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qtTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQtTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qtTaskTemplateItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTemplateItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qtTaskTemplateReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				re = saveReagentItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qtTaskTemplateCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				in = saveCosItem(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTemplateItem(QtTask sc, String itemDataJson)
			throws Exception {
		List<QtTaskTemplate> saveItems = new ArrayList<QtTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskTemplate scp = new QtTaskTemplate();
			// 将map信息读入实体类
			scp = (QtTaskTemplate) qtTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQtTask(sc);
			saveItems.add(scp);
		}
		qtTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItem(String[] ids) throws Exception {
		for (String id : ids) {
			QtTaskTemplate scp = qtTaskDao.get(QtTaskTemplate.class, id);
			qtTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateItemOne(String ids) throws Exception {
		QtTaskTemplate scp = qtTaskDao.get(QtTaskTemplate.class, ids);
		if (scp != null) {
			qtTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveReagentItem(QtTask sc, String itemDataJson)
			throws Exception {
		Integer re = 0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskReagent scp = new QtTaskReagent();
			// 将map信息读入实体类
			scp = (QtTaskReagent) qtTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getSn() != null && !scp.getSn().equals("")) {
				StorageOutItem soi = storageOutService.findStorageOutItem(scp
						.getSn());
				if (soi != null) {
					if (scp.getCode().equals(soi.getStorage().getId())) {
						scp.setBatch(soi.getCode());
						scp.setExpireDate(soi.getExpireDate());
						re++;
					} else {
						scp.setSn("");
					}
				}
			}
			scp.setQtTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getNum() != null && scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getNum());
			}
			qtTaskDao.saveOrUpdate(scp);
		}
		return re;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItem(String[] ids) throws Exception {
		for (String id : ids) {
			QtTaskReagent scp = qtTaskDao.get(QtTaskReagent.class, id);
			qtTaskDao.delete(scp);
		}
	}

	/**
	 * delTemplateReagentOne 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTemplateReagentOne(String ids) throws Exception {
		QtTaskReagent scp = qtTaskDao.get(QtTaskReagent.class, ids);
		if (scp != null) {
			qtTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveCosItem(QtTask sc, String itemDataJson) throws Exception {
		Integer saveItem = 0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskCos scp = new QtTaskCos();
			// 将map信息读入实体类
			scp = (QtTaskCos) qtTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			Instrument ins = null;
			if (scp.getCode() != null && !scp.equals("")) {
				ins = qtTaskDao.get(Instrument.class, scp.getCode());
			}
			if (ins != null
					&& ins.getType().getId().equals(scp.getType().getId())) {
				scp.setName(ins.getName());
				scp.setState(ins.getState().getName());
				saveItem++;
			} else {
				scp.setName("");
				scp.setCode("");
				scp.setState("");
			}
			scp.setQtTask(sc);

			qtTaskDao.saveOrUpdate(scp);
		}
		return saveItem;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delCosItem(String[] ids) throws Exception {
		for (String id : ids) {
			QtTaskCos scp = qtTaskDao.get(QtTaskCos.class, id);
			qtTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQtTaskTemplateCosOne(String ids) throws Exception {
		QtTaskCos scp = qtTaskDao.get(QtTaskCos.class, ids);
		if (scp != null) {
			qtTaskDao.delete(scp);
		}
	}

	/**
	 * 审批完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		QtTask sct = qtTaskDao.get(QtTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setReceiveDate(new Date());
		qtTaskDao.update(sct);
		// 提交样本
		submit(id, null);

		// 完成的时候判断设备解除占用
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		String did = sct.getId();
		// 将批次信息反馈到模板中
		List<QtTaskReagent> list1 = qtTaskDao.setReagentList(did);
		for (QtTaskReagent dt : list1) {
			String bat1 = dt.getBatch();
			// QtTask实验中试剂的批次
			String drid = dt.gettReagent();
			// QtTask实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
		}
		// 将状态信息反馈到开箱检验的样本中
		List<QtTaskItem> eList = qtTaskDao.setQtTaskItemList(did);
		for (QtTaskItem edg : eList) {
			if("".equals(edg.getTempId())||edg.getTempId()==null){
				
			}else{
				String sid = edg.getTempId();
				List<SampleReceiveItem> sList = sampleReceiveDao
						.selectItemList(sid);
				for (SampleReceiveItem sri : sList) {
					sri.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
					sri.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE);
				}
				updateTempState(sid);
			}
		
		}
		List<String> orderList = new ArrayList<String>();
		List<QtTaskInfo> infoList = qtTaskDao.getQtTaskInfoList(did);
		// for (QtTaskInfo qis : infoList) {
		//
		// SampleInfo si = new SampleInfo();
		//
		// List<SampleInfo> sil = commonService.get(SampleInfo.class, "code",
		// qis.getSampleCode());
		// if (sil.size() > 0)
		// si = sil.get(0);
		//
		// if ((si.getOrderNum() != null)
		// && !orderList.contains(si.getOrderNum())) {
		// SampleReportTemp st = new SampleReportTemp();
		//
		// st.setGenotype(qis.getGenotype());
		// st.setCode(qis.getCode());
		// st.setPatientName(si.getPatientName());
		// st.setSampleCode(qis.getSampleCode());
		// st.setProductId(si.getProductId());
		// st.setProductName(si.getProductName());
		// if (qis.getInspectDate() != null
		// && !qis.getInspectDate().equals("")) {
		// st.setInspectDate(DateUtil.parse(si.getInspectDate()));
		// }
		// if (qis.getReportDate() != null
		// && !qis.getReportDate().equals("")) {
		// st.setReportDate(DateUtil.parse(si.getReportDate()));
		// }
		// st.setVolume(qis.getVolume());
		// st.setUnit(qis.getUnit());
		// st.setIdCard(qis.getIdCard());
		// st.setSequenceFun(qis.getSequenceFun());
		// st.setPhone(qis.getPhone());
		// st.setOrderNum(si.getOrderNum());
		// st.setState(qis.getState());
		// st.setNote(qis.getNote());
		// st.setClassify(qis.getClassify());
		// qtTaskDao.saveOrUpdate(st);
		// orderList.add(si.getOrderNum());
		// }
		// }
	}

	/**
	 * 生成结果之后修改QtTaskQPCR实验中间表的样本状态
	 * 
	 * @param code
	 */
	public void updateTempState(String code) {
		QtTaskTemp dt = commonDAO.get(QtTaskTemp.class, code);
		if (dt != null) {
			dt.setState("2");
		}
	}

	/**
	 * 到QPCR的样本或到一代测序
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findExperimentQtTaskReslutList(Integer startNum,
			Integer limitNum, String dir, String sort, String state)
			throws Exception {
		Map<String, Object> result = qtTaskDao
				.selectExperimentQtTaskReslutList(startNum, limitNum, dir,
						sort, state);
		List<QtTaskInfo> list = (List<QtTaskInfo>) result.get("list");

		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQtTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskFromReceiveList(
				scId, startNum, limitNum, dir, sort);
		List<QtTaskReceiveItem> list = (List<QtTaskReceiveItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> selectQtTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qtTaskDao.selectQtTaskAbnormalList(scId,
				startNum, limitNum, dir, sort);
		List<QtTaskInfo> list = (List<QtTaskInfo>) result.get("list");
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qtTaskDao.setReagent(id, code);
		List<QtTaskReagent> list = (List<QtTaskReagent>) result.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			for (QtTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());
				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}
				if (ti.getNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}
				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				if (ti.getExpireDate() != null) {
					map.put("expireDate", sdf.format(ti.getExpireDate()));
				}
				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getQtTask().getId());
				map.put("tName", ti.getQtTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qtTaskDao.setCos(id, code);
		List<QtTaskCos> list = (List<QtTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (QtTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}
				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}
				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				if (ti.getType() != null) {
					map.put("typeId", ti.getType().getId());
					map.put("typeName", ti.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getQtTask().getId());
				map.put("tName", ti.getQtTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<QtTaskItem> findQtTaskItemList(String scId) throws Exception {
		List<QtTaskItem> list = qtTaskDao.selectQtTaskItemList(scId);
		return list;
	}

	/**
	 * 提交样本
	 * 
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submit(String id, String[] ids) throws Exception {
		// 获取主表信息
		QtTask sc = this.qtTaskDao.get(QtTask.class, id);
		// 获取结果表样本信息
		List<QtTaskInfo> list;
		if (ids == null)
			list = this.qtTaskDao.setQtTaskResultById(id);
		else
			list = this.qtTaskDao.setQtTaskResultByIds(ids);

		List<String> sampleState = new ArrayList<String>();
		for (QtTaskInfo scp : list) {
			if (scp != null) {
				if (scp.getNextFlowId() != null
						&& scp.getResult() != null
						&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
								.getSubmit().equals("")))) {
					if (!scp.getResult().equals("")
							&& !scp.getNextFlowId().equals("")) {
						String isOk = scp.getResult();
						String nextFlowId = scp.getNextFlowId();
						NextFlow nf = commonService.get(NextFlow.class,
								nextFlowId);
						nextFlowId = nf.getSysCode();
						if (isOk.equals("1")) {
							if (nextFlowId.equals("0009")) {
								// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setNum(scp.getSampleNum());
								st.setSampleType(scp.getSampleType());
								st.setSampleTypeId(scp.getDicSampleType()
										.getId());
								st.setInfoFrom("QtTaskInfo");
								st.setState("1");
								SampleInfo s = this.sampleInfoMainDao
										.findSampleInfo(scp.getSampleCode());
								st.setPatientName(s.getPatientName());
								qtTaskDao.saveOrUpdate(st);
							} else if (nextFlowId.equals("0010")) {
								// 重QPCR实验
								QtTaskAbnormal eda = new QtTaskAbnormal();
								scp.setState("4");
								// 状态为4 在重QPCR实验中显示
								sampleInputService.copy(eda, scp);
							} else if (nextFlowId.equals("0011")) {
								// 重建库
								WkTaskAbnormal wka = new WkTaskAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0024")) {
								// 重抽血
								PlasmaAbnormal wka = new PlasmaAbnormal();
								scp.setState("4");
								sampleInputService.copy(wka, scp);
							} else if (nextFlowId.equals("0012")) {// 暂停
							} else if (nextFlowId.equals("0013")) {// 终止
							} else if (nextFlowId.equals("0018")) {// 文库构建
								WkTaskTemp d = new WkTaskTemp();
								scp.setState("1");
								sampleInputService.copy(d, scp);
//								if (scp.getReportDate() != null) {
//									d.setReportDate(DateUtil.parse(scp
//											.getReportDate()));
//								}
								commonService.saveOrUpdate(d);
							} else if (nextFlowId.equals("0020")) {// 2100质控
								Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
								scp.setState("1");
								sampleInputService.copy(qc2100, scp);
								qc2100.setWkType("2");
								commonService.saveOrUpdate(qc2100);
							} else if (nextFlowId.equals("0021")) {// QPCR质控
								QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
								scp.setState("1");
								sampleInputService.copy(qpcr, scp);
								qpcr.setWkType("1");
								commonService.saveOrUpdate(qpcr);
							} else if (nextFlowId.equals("0022")) {// 超声破碎
								UfTaskTemp uf = new UfTaskTemp();
								scp.setState("1");
								sampleInputService.copy(uf, scp);
							} else if (nextFlowId.equals("0031")||nextFlowId.equals("0035")) { //报告模块

								SampleInfo si = new SampleInfo();

								List<SampleInfo> sil = commonService.get(SampleInfo.class, "code",
										scp.getSampleCode());
								if (sil.size() > 0)
									si = sil.get(0);

								if (si.getOrderNum() != null) {
									List<SampleReportTemp> ab = commonService.get(SampleReportTemp.class, "orderNum",
											si.getOrderNum());
									if (ab.size() > 0){
										boolean a = false;
										SampleReportTemp st = new SampleReportTemp();
										for(SampleReportTemp abc : ab){
											if("qpcr".equals(abc.getTestCode())&&scp.getCode().equals(abc.getCode())&&scp.getProductId().equals(abc.getProductId())){
												a=true;
												st=abc;
												break;
											}
										}
										if(a){
											st.setState("1");
											commonService.saveOrUpdate(st);
										}else{
											st.setGenotype(scp.getGenotype());
											st.setCode(scp.getCode());
											st.setPatientName(si.getPatientName());
											st.setSampleCode(scp.getSampleCode());
											st.setProductId(scp.getProductId());
											st.setProductName(scp.getProductName());
											if (scp.getInspectDate() != null
													&& !scp.getInspectDate().equals("")) {
												st.setInspectDate(DateUtil.parse(scp.getInspectDate()));
											}
											if (scp.getReportDate() != null
													&& !scp.getReportDate().equals("")) {
												st.setReportDate(DateUtil.parse(scp.getReportDate()));
											}
											st.setVolume(scp.getVolume());
											st.setUnit(scp.getUnit());
											st.setIdCard(scp.getIdCard());
											st.setSequenceFun(scp.getSequenceFun());
											st.setPhone(scp.getPhone());
											st.setOrderNum(si.getOrderNum());
											st.setState(scp.getState());
											st.setNote(scp.getNote());
											st.setClassify(scp.getClassify());
											st.setState("1");
											/*
											 * 赋值订单实体
											 */
											SampleOrder od = commonDAO.get(SampleOrder.class,
													si.getOrderNum());
											if (od!=null||"".equals(od)){
												st.setSampleOrder(od);
											}
											st.setTestCode("qpcr");
											st.setTestFroms("QPCR实验");
											commonService.saveOrUpdate(st);
										}
									}else{
										SampleReportTemp st = new SampleReportTemp();

										st.setGenotype(scp.getGenotype());
										st.setCode(scp.getCode());
										st.setPatientName(si.getPatientName());
										st.setSampleCode(scp.getSampleCode());
										st.setProductId(scp.getProductId());
										st.setProductName(scp.getProductName());
										if (scp.getInspectDate() != null
												&& !scp.getInspectDate().equals("")) {
											st.setInspectDate(DateUtil.parse(scp.getInspectDate()));
										}
										if (scp.getReportDate() != null
												&& !scp.getReportDate().equals("")) {
											st.setReportDate(DateUtil.parse(scp.getReportDate()));
										}
										st.setVolume(scp.getVolume());
										st.setUnit(scp.getUnit());
										st.setIdCard(scp.getIdCard());
										st.setSequenceFun(scp.getSequenceFun());
										st.setPhone(scp.getPhone());
										st.setOrderNum(si.getOrderNum());
										st.setState(scp.getState());
										st.setNote(scp.getNote());
										st.setClassify(scp.getClassify());
										st.setState("1");
										/*
										 * 赋值订单实体
										 */
										SampleOrder od = commonDAO.get(SampleOrder.class,
												si.getOrderNum());
										if (od!=null||"".equals(od)){
											st.setSampleOrder(od);
										}
										st.setTestCode("qpcr");
										st.setTestFroms("QPCR实验");
										commonService.saveOrUpdate(st);
//										orderList.add(si.getOrderNum());
//										qtTaskDao.saveOrUpdate(st);
									}
								}
							} else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(nextFlowId);
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}

						} else {// 不合格
							if (nextFlowId.equals("0014")) {// 反馈项目组
								// 下一步流向是：反馈至项目组

							} else {
								// 提交的不合格样本到异常管理
								QtTaskAbnormal eda = new QtTaskAbnormal();
								scp.setState("2");// 状态为2 在QPCR实验异常中显示
								sampleInputService.copy(eda, scp);
							
							}

						}
						if (!sampleState.contains(scp.getCode())) {
							DateFormat format = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							sampleStateService
									.saveSampleState(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											sc.getCreateDate(),
											format.format(new Date()),
											"QtTask",
											"QPCR实验",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											sc.getId(), scp.getNextFlow(),
											scp.getResult(), null, null, null,
											null, null, null, null, null);
							scp.setSubmit("1");

							sampleState.add(scp.getCode());
						}

						commonService.saveOrUpdate(scp);
					}
				}
			}
		}
	}

	// 根据主表id查询明细
	public List<QtTaskItem> findDnaItemList(String scId) throws Exception {
		List<QtTaskItem> list = qtTaskDao.setQtTaskItemList(scId);
		return list;
	}

	/**
	 * 质控品竖向或横向排序
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void sortTwo(String id,String order) throws Exception {
		List<QtTaskItem> list1=qtTaskDao.getQtTaskItemListById(id);
		if(list1.size()>0){
			for(QtTaskItem qi:list1){                                                              
				qi.setRowCode(null);
				qi.setColCode(null);
				commonDAO.saveOrUpdate(qi);
			}
		}
		//先清空坐标后进行位置设定
		if(list1.size()>0){
			for(QtTaskItem qi:list1){
				//0:横向    1:竖向
				if(order.equals("0")){
					qi.getQtTask().setOrders("0");
					//横向排序
					if(qi.getOrderNumber()%12==0){
						qi.setColCode(String.valueOf(12));
						qi.setRowCode(String.valueOf((char) (64 + (qi.getOrderNumber()/12))));
					}else{
						qi.setColCode(String.valueOf(qi.getOrderNumber()%12));
						qi.setRowCode(String.valueOf((char) (64 + 1 +(qi.getOrderNumber()/12))));
					}
				}else{
					qi.getQtTask().setOrders("1");
					if(qi.getOrderNumber()%8==0){
						qi.setColCode(String.valueOf(qi.getOrderNumber()/8));
						qi.setRowCode(String.valueOf((char) (64 + 8)));
					}else{
						qi.setColCode(String.valueOf(qi.getOrderNumber()/8+1));
						qi.setRowCode(String.valueOf((char) (64 + (qi.getOrderNumber()%8))));
					}
				}
			}
		}
	}
	
	/**
	 * 质控品按照指定位置竖向或横向排序
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(String id,String ids,int local,String st,String order) throws Exception {
		List<QtTaskItem> list1=qtTaskDao.getQtTaskItemListById(id);
		if(list1.size()>0){
			for(QtTaskItem qi:list1){                                                              
				qi.setRowCode(null);
				qi.setColCode(null);
				commonDAO.saveOrUpdate(qi);
			}
		}
		
		//获取排序号大于所选样本(质控品)中最大排序号的样本集合
		Integer maxNum0=0;
		List<Integer> maxNumList=qtTaskDao.getQtTaskItemListMaxNum(id,st);
		if(maxNumList.size()>0){
			maxNum0=maxNumList.get(0);
		}
		//获取选中的(质控品)中的排序号最小的样本集合
		Integer minNum0=0;
		List<Integer> minNumList=qtTaskDao.getQtTaskItemListMinxNum(id,st);
		if(minNumList.size()>0){
			minNum0=minNumList.get(0);
		}
		
		if(minNum0>local){
			//先把移动的质控品序号排列好
			String [] itemId=ids.split(",");
			for(int i=0;i<itemId.length;i++){
				QtTaskItem qt=commonDAO.get(QtTaskItem.class, itemId[i]);
				if(qt!=null){
					qt.setOrderNumber(local+i+1);
				}
			}
			List<QtTaskItem> list=qtTaskDao.getQtTaskItemList(id, local,st,maxNum0);
			if(list.size()>0){
				for(QtTaskItem qi:list){                                                              
					qi.setOrderNumber(qi.getOrderNumber()+itemId.length);
				}
			}
			
		}else{
			
			//先把移动的质控品序号排列好
			String [] itemId=ids.split(",");
			for(int i=0;i<itemId.length;i++){
				QtTaskItem qt=commonDAO.get(QtTaskItem.class, itemId[i]);
				if(qt!=null){
					qt.setOrderNumber(local-i);
				}
			}
			List<QtTaskItem> list=qtTaskDao.getQtTaskItemListmin(id, local,st,minNum0);
			if(list.size()>0){
				for(QtTaskItem qi:list){                                                              
					qi.setOrderNumber(qi.getOrderNumber()-itemId.length);
				}
			}
			
			
	}
	
		//List<QtTaskItem> list2=qtTaskDao.getQtTaskItemListById(id);
		//排序号分配好之后进行位置设定
		if(list1.size()>0){
			for(QtTaskItem qi:list1){
				//0:横向    1:竖向
				if(order.equals("0")){
					//横向排序
					if(qi.getOrderNumber()%12==0){
						qi.setColCode(String.valueOf(12));
						qi.setRowCode(String.valueOf((char) (64 + (qi.getOrderNumber()/12))));
					}else{
						qi.setColCode(String.valueOf(qi.getOrderNumber()%12));
						qi.setRowCode(String.valueOf((char) (64 + 1 +(qi.getOrderNumber()/12))));
					}
				}else{
					if(qi.getOrderNumber()%8==0){
						qi.setColCode(String.valueOf(qi.getOrderNumber()/8));
						qi.setRowCode(String.valueOf((char) (64 + 8)));
					}else{
						qi.setColCode(String.valueOf(qi.getOrderNumber()/8+1));
						qi.setRowCode(String.valueOf((char) (64 + (qi.getOrderNumber()%8))));
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: getQtTaskInfoByIds
	 * @Description: TODO(查找对应的结果信息)
	 * @param @param idc
	 * @param @return    设定文件
	 * @return QtTaskInfo    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-25 上午11:33:32
	 * @throws
	 */
	public QtTaskInfo getQtTaskInfoByIds(String id) {
		return qtTaskDao.getQtTaskInfoByIds(id);
	}

	/**
	 * 
	 * @Title: updateForUpload
	 * @Description: TODO(将信息上传到对应的数据中)
	 * @param @param code
	 * @param @param famCtMean
	 * @param @param vicCtMean
	 * @param @param genotype
	 * @param @param chromosomalLocation
	 * @param @param results    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-25 上午11:48:53
	 * @throws
	 */
	public void updateForUpload(String[] codes, String[] famCtMeans,
			String[] vicCtMeans, String[] genotypes,
			String[] chromosomalLocations, String[] results) {
		// 获取结果表样本信息
		List<QtTaskInfo> list = null;
		for (int i = 0; i < codes.length; i++) {
			String code = codes[i];
			String famCtMean = famCtMeans[i];
			String vicCtMean = vicCtMeans[i];
			String genotype = genotypes[i];
			String chromosomalLocation = chromosomalLocations[i];
			String result = results[i];
			if (code != null) {
				list = this.qtTaskDao
						.findQtTaskInfoByCode(code);
			}
			for (QtTaskInfo scp : list) {
				if (scp != null) {
					if (!famCtMean.equals("") && !famCtMean.equals(null)) {
						scp.setFamCtMean(famCtMean);
					} else {
						scp.setFamCtMean(null);
					}
					if (!vicCtMean.equals("") && !vicCtMean.equals(null)) {
						scp.setVicCtMean(vicCtMean);
					} else {
						scp.setVicCtMean(null);
					}
					if (!chromosomalLocation.equals("") && !chromosomalLocation.equals(null)) {
						scp.setChromosomalLocation(chromosomalLocation);
					} else {
						scp.setChromosomalLocation(null);
					}
//					scp.setGenotype(genotype);
					qtTaskDao.saveOrUpdate(scp);
				}
			}
		}

	}
}