package com.biolims.experiment.qt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.qt.dao.QtTaskRepeatDao;
import com.biolims.experiment.qt.model.QtTaskAbnormal;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleInputTemp;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QtTaskRepeatService {
	@Resource
	private QtTaskRepeatDao qtTaskRepeatDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findRepeatQtTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qtTaskRepeatDao.selectRepeatPlasmaList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	


	/**
	 * 保存重QPCR实验
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQtTaskRepeatList(String itemDataJson) throws Exception {
		List<QtTaskAbnormal> saveItems = new ArrayList<QtTaskAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QtTaskAbnormal sbi = new QtTaskAbnormal();
			sbi = (QtTaskAbnormal) qtTaskRepeatDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			if (sbi != null && sbi.getResult() != null
					&& sbi.getIsExecute() != null) {// 如果处理意见不为null，到QPCR实验异常。
				if (sbi.getIsExecute().equals("1")) {
					if (sbi.getResult().equals("1")) {// 重新QPCR实验
						QtTaskTemp dst = new QtTaskTemp();
						dst.setCode(sbi.getCode());
						dst.setCode(sbi.getCode());
						dst.setSequenceFun(sbi.getSequenceFun());
						dst.setPatientName(sbi.getPatientName());
						dst.setProductName(sbi.getProductName());
						dst.setProductId(sbi.getProductId());
						dst.setIdCard(sbi.getIdCard());
						dst.setInspectDate(sbi.getInspectDate());
						dst.setPhone(sbi.getPhone());
						dst.setOrderId(sbi.getOrderId());
						dst.setReportDate(sbi.getReportDate());
						dst.setState("1");
						dst.setClassify(sbi.getClassify());
						dst.setCode(sbi.getCode());
						this.qtTaskRepeatDao.saveOrUpdate(dst);
						sbi.setState("1");
					} else {// 不合格
						sbi.setState("2");
						qtTaskRepeatDao.saveOrUpdate(sbi);
					}
				}
			}
			saveItems.add(sbi);
		}
		qtTaskRepeatDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 根据条件检索数据
	 * @param experiment.qtCode
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> selectQtTaskRepeat(String code,
			String sampleCode) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = qtTaskRepeatDao.selectRepeat(
				code, sampleCode);
		List<QtTaskInfo> list = (List<QtTaskInfo>) result.get("list");
		if (list != null && list.size() > 0) {
			for (QtTaskInfo srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("code", srai.getCode());
				map.put("sampleCode", srai.getSampleCode());
				map.put("isExecute", srai.getIsExecute());
				map.put("nextFlow", srai.getNextFlow());
				map.put("note", srai.getNote());
				map.put("method", srai.getMethod());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public SampleInfo findSampleInfo(String id) {
		SampleInfo findSampleInfoById = qtTaskRepeatDao
				.findSampleInfoById(id);
		return findSampleInfoById;
	}

	public SampleInputTemp findSampleInputTemp(String id) {
		SampleInputTemp findSampleInputTempById = qtTaskRepeatDao
				.findSampleInputTempById(id).get(0);
		return findSampleInputTempById;
	}
}
