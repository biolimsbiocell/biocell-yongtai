package com.biolims.experiment.qt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qt.model.QtTaskAbnormal;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInputTemp;

@Repository
@SuppressWarnings("unchecked")
public class QtTaskRepeatDao extends BaseHibernateDao {
	public Map<String, Object> selectRepeatPlasmaList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QtTaskAbnormal  where 1=1 and state='4'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QtTaskAbnormal> list = new ArrayList<QtTaskAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public SampleInfo findSampleInfoById(String code) {
		String hql = "from Info t where 1=1 and t.code ='" + code + "'";
		SampleInfo sampleInfo = (SampleInfo) this.getSession().createQuery(hql)
				.uniqueResult();
		return sampleInfo;
	}

	public List<SampleInputTemp> findSampleInputTempById(String code) {
		String hql = "from SampleInputTemp t where 1=1 and t.code ='" + code
				+ "'";
		List<SampleInputTemp> sampleInputTemp = this.getSession()
				.createQuery(hql).list();
		return sampleInputTemp;
	}

	/**
	 * 样本异常总数
	 * @return
	 */
	public Long selectCount() {
		String hql = "from Feedback t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 血浆异常总数
	 * @return
	 */
	public Long selectCount1() {
		String hql = "from FeedbackPlasma t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * QtTask异常总数
	 * @return
	 */
	public Long selectCount2() {
		String hql = "from FeedbackQtTask t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 文库异常总数
	 * @return
	 */
	public Long selectCount3() {
		String hql = "from FeedbackWk t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * pooling异常总数
	 * @return
	 */
	public Long selectCount4() {
		String hql = "from FeedbackPooling t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 质控异常总数
	 * @return
	 */
	public Long selectCount5() {
		String hql = "from FeedbackQuality t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 上机异常总数
	 * @return
	 */
	public Long selectCount6() {
		String hql = "from FeedbackSequencing t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 下机异常总数
	 * @return
	 */
	public Long selectCount7() {
		String hql = "from FeedbackDeSequencing t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 分析异常总数
	 * @return
	 */
	public Long selectCount8() {
		String hql = "from FeedbackAnalysis t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 解读异常总数
	 * @return
	 */
	public Long selectCount9() {
		String hql = "from FeedbackInterpret t where 1=1";
		Long list = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据样本编号查出Info对象
	 * @param code
	 * @return
	 */
	public SampleInfo findSampleInfo(String code) {
		String hql = "from SampleInfo t where 1=1 and t.code = '" + code + "'";
		SampleInfo sampleInfo = (SampleInfo) this.getSession().createQuery(hql)
				.uniqueResult();
		return sampleInfo;
	}

	/**
	 * 根据条件查询
	 * @param code
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectRepeat(String code, String sampleCode)
			throws Exception {
		String hql = "from QtTaskAbnormal t where 1=1 ";
		String key = "";
		if (!code.equals("") && sampleCode.equals("")) {
			key = key + " and  t.code like '%" + code + "%'";
		} else if (code.equals("") && !sampleCode.equals("")) {
			key = key + " and  t.sampleCode like '%" + sampleCode + "%'";
		} else if (!code.equals("") && !sampleCode.equals("")) {
			key = key + " and  t.code like '%" + code
					+ "%' and  t.sampleCode like '%" + sampleCode + "%'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskAbnormal> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据条件查询
	 * @param experiment.qtCode
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public List<QtTaskAbnormal> selectRepeatList(String code, String sampleCode)
			throws Exception {
		String hql = "from QtTaskAbnormal t where 1=1 ";
		String key = "";
		List<QtTaskAbnormal> list = this.getSession().createQuery(hql + key)
				.list();
		return list;
	}
}