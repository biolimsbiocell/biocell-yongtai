package com.biolims.experiment.qt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskCos;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskReagent;
import com.biolims.experiment.qt.model.QtTaskReceiveItem;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.qt.model.QtTaskTemplate;
import com.biolims.system.primers.model.MappingPrimersLibrary;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;

@Repository
@SuppressWarnings("unchecked")
public class QtTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectQtTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QtTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QtTask> list = new ArrayList<QtTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQtTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskItem> list = new ArrayList<QtTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQtTaskInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskInfo> list = new ArrayList<QtTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 模板对应的子表
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQtTaskTemplateItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskTemplate> list = new ArrayList<QtTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by"+castStrId("code")+"";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQtTaskTemplateReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskReagent> list = new ArrayList<QtTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据ItemId查询试剂明细
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQtTaskTemplateReagentListByItemId(
			String scId, Integer startNum, Integer limitNum, String dir,
			String sort, String itemId) throws Exception {
		String hql = "from QtTaskReagent t where 1=1 and t.itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskReagent> list = new ArrayList<QtTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from QtTaskReagent t where 1=1 and t.qtTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<QtTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from QtTaskCos t where 1=1 and t.qtTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<QtTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQtTaskTemplateCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskCos> list = new ArrayList<QtTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQtTaskTemplateCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from QtTaskCos t where 1=1 and t.itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskCos> list = new ArrayList<QtTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据QtTaskQPCR实验主表ID查询试剂明细
	 */
	public List<QtTaskReagent> setReagentList(String code) {
		String hql = "from QtTaskReagent where 1=1 and qtTask.id='" + code
				+ "'";
		List<QtTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据QtTaskQPCR实验主表ID查询QPCR实验明细
	 */
	public List<QtTaskItem> setQtTaskItemList(String code) {
		String hql = "from QtTaskItem where 1=1 and qtTask.id='" + code + "'";
		List<QtTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据QtTaskQPCR实验主表ID查询QPCR实结果
	 */
	public List<QtTaskInfo> getQtTaskInfoList(String code) {
		String hql = "from QtTaskInfo where 1=1 and qtTask.id='" + code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据状态查询QtTask
	 * 
	 * @return
	 */
	public List<QtTask> selectQtTaskList() {
		// String hql = "from QtTask t where 1=1 and t.state=1";
		String hql = "from QtTask t where 1=1 and t.state='1'";
		List<QtTask> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QtTaskInfo> selectQtTaskResultList(String id) {
		String hql = "from QtTaskInfo t where 1=1 and qtTask.state='1' and qtTask='"
				+ id + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * 
	 * @param code
	 * @return
	 */
	public List<QtTaskInfo> setQtTaskResultByCode(String code) {
		String hql = "from QtTaskInfo t where 1=1 and qtTask.id='" + code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * 
	 * @param code
	 * @return
	 */
	public List<QtTaskInfo> setQtTaskResultById(String code) {
		String hql = "from QtTaskInfo t where (submit is null or submit='') and qtTask.id='"
				+ code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * 
	 * @param codes
	 * @return
	 */
	public List<QtTaskInfo> setQtTaskResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from QtTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询QPCR实验样本去到QPCR或到一代测序
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param state
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectExperimentQtTaskReslutList(
			Integer startNum, Integer limitNum, String dir, String sort,
			String state) throws Exception {
		String hql = "from QtTaskInfo where 1=1 and experiment.qtInfo.state='完成' and qtTask.stateName='"
				+ state + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskInfo> list = new ArrayList<QtTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 获取从样本接收来的样本
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQtTaskFromReceiveList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskReceiveItem where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskReceiveItem> list = new ArrayList<QtTaskReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询异常QPCR实验
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectQtTaskAbnormalList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QtTaskInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskInfo> list = new ArrayList<QtTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询左侧中间表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectQtTaskTempList(String scId,
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = "";
		if("1".equals(scId)){
			hql = " from QtTaskTemp where 1=1 and state='1' and techJkServiceTask is null";
		}else{
			hql = " from QtTaskTemp where 1=1 and state='1' ";
		}
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QtTaskTemp> list = new ArrayList<QtTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询QPCR实验异常样本
	 * 
	 * @param code
	 * @return
	 */
	public List<QtTaskInfo> selectQtTaskResultAbnormalList(String code) {
		String hql = "from QtTaskInfo where 1=1 and  qtTask='" + code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * 
	 * @param code
	 * @return
	 */
	public List<QtTaskInfo> selectQtTaskAllList(String code) {
		String hql = "from QtTaskInfo  where 1=1  and qtTask='" + code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据id查询QPCR实验生成结果 用于更新是否合格字段
	 * 
	 * @param id
	 * @return
	 */
	public List<QtTaskInfo> findQtTaskResultById(String id) {
		String hql = "from QtTaskInfo  where 1=1 and code='" + id + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 状态完成下一步流向
	 * 
	 * @param code
	 * @return
	 */
	public List<QtTaskInfo> setNextFlowList(String code) {
		String hql = "from QtTaskInfo where 1=1  and qtTask.state='1' and qtTask.id='"
				+ code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从上一步样本接收完成的QPCR实验样本
	 * 
	 * @return
	 */
	public List<QtTaskReceiveItem> selectReceiveQtTaskList() {
		String hql = "from QtTaskReceiveItem where 1=1  and state='1' ";
		List<QtTaskReceiveItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	/**
	 * 根据ID相关样本的数量
	 * 
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from QtTaskItem t where 1=1 and t.qtTask.id='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据id查询QPCR实验结果
	 * 
	 * @param id
	 * @return
	 */
	public List<QtTaskInfo> selectQtTaskResultListById(String id) {
		String hql = "from QtTaskInfo where 1=1  and qtTask.id='" + id + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据ID相关样本的QPCR实验次数
	 * 
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from QtTaskItem t where 1=1 and t.sampleCode='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.Code)" + hql).uniqueResult();
		return list;
	}

	/**
	 * 查询从样本接收完成的的样本
	 * 
	 * @param id
	 * @return
	 */
	public List<QtTaskItem> findQtTaskItemList(String id) {
		String hql = "from QtTaskItem  where 1=1 and state='1' and qtTask='"
				+ id + "'";
		List<QtTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据Code查询QtTaskInfo
	 * 
	 * @param code
	 * @return
	 */
	public String findQtTaskInfo(String code) {
		String hql = "from QtTaskInfo  where 1=1 and tempId='" + code + "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 根据样本编号查出QtTaskTemp对象
	 * 
	 * @param code
	 * @return
	 */
	public QtTaskTemp findQtTaskTemp(String code) {
		String hql = "from QtTaskTemp t where 1=1 and t.sampleCode = '" + code
				+ "'";
		QtTaskTemp QtTaskTemp = (QtTaskTemp) this.getSession().createQuery(hql)
				.uniqueResult();
		return QtTaskTemp;
	}

	/**
	 * 根据样本查询主表
	 * 
	 * @param sid
	 * @return
	 */
	public String selectTask(String sid) {
		String hql = "from TechCheckServiceOrder t where t.id = '" + sid + "'";
		String str = (String) this.getSession()
				.createQuery(" select t.id " + hql).uniqueResult();
		return str;
	}

	public List<QtTaskItem> selectQtTaskItemList(String scId) throws Exception {
		String hql = "from QtTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskItem> list = new ArrayList<QtTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	/**
	 * 根据样本编号查询检测临时表
	 * 
	 * @param id
	 * @return
	 */
	public List<QtTaskInfo> setQtTaskInfo(String id) {
		String hql = "from QtTaskInfo where 1=1 and qtTask.id='" + id
				+ "' and nextFlowId='0009'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据入库的原始样本号查询该结果表信息
	 * 
	 * @param id
	 * @param Code
	 * @return
	 */
	public List<QtTaskInfo> setQtTaskInfo2(String id, String code) {
		String hql = "from QtTaskInfo t where 1=1 and qtTask.id='" + id
				+ "' and sampleCode='" + code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据检测项目编号获取项目对应引物库
	 * 
	 * @param productId
	 * @return
	 */
	public List<MappingPrimersLibrary> getMappingPrimersLibrary(String productId) {
		String hql = "from MappingPrimersLibrary  where prodectId='"
				+ productId + "'";
		List<MappingPrimersLibrary> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

//	/**
//	 * 根据引物库编号获取项目对应引物库明细
//	 * 
//	 * @param productId
//	 * @return
//	 */
//	public List<MappingPrimersLibraryItem> getMappingPrimersLibraryItem(
//			String mpId) {
//		String hql = "";
//		if (mpId.contains(",")) {
//			String[] a = mpId.split(",");
//			mpId = "(";
//			for (String a1 : a) {
//				if (mpId.equals("("))
//					mpId = mpId + "'" + a1 + "'";
//				else
//					mpId = mpId + ",'" + a1 + "'";
//			}
//			mpId = mpId + ")";
//			hql = "from MappingPrimersLibraryItem  where 1=1 and mappingPrimersLibrary.id in "
//					+ mpId + "";
//
//		} else
//			hql = "from MappingPrimersLibraryItem  where 1=1 and mappingPrimersLibrary='"
//					+ mpId + "'";
//		List<MappingPrimersLibraryItem> list = this.getSession()
//				.createQuery(hql).list();
//		return list;
//	}
	/**
	 * 根据引物库编号获取项目对应引物库明细
	 * 
	 * @param productId
	 * @return
	 */
	public List<MappingPrimersLibraryItem> getMappingPrimersLibraryItem(
			String mpId) {
		String hql = "";
		if (mpId.contains(",")) {
			String[] a = mpId.split(",");
			mpId = "(";
			for (String a1 : a) {
				if (mpId.equals("("))
					mpId = mpId + "'" + a1 + "'";
				else
					mpId = mpId + ",'" + a1 + "'";
			}
			mpId = mpId + ")";
			hql = "from MappingPrimersLibraryItem  where 1=1 and mappingPrimersLibrary.id in (select id from MappingPrimersLibrary where prodectId in "
					+ mpId + ")";

		} else
			hql = "from MappingPrimersLibraryItem  where 1=1 and mappingPrimersLibrary.id in (select id from MappingPrimersLibrary where prodectId = '"
					+ mpId + "')";
		List<MappingPrimersLibraryItem> list = this.getSession()
				.createQuery(hql).list();
		return list;
	}

	/**
	 * 查询该主表ID所属的样本集合
	 * @param id
	 * @return
	 */
	public List<QtTaskItem> getQtTaskItemListById(String id) {
		String hql = "from QtTaskItem t where 1=1 and qtTask='"
				+ id+"'";
		List<QtTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 所选样本(质控品)中最大排序号
	 * @param id
	 * @return
	 */
	public List<Integer> getQtTaskItemListMaxNum(String id,String st) {
		String hql = "from QtTaskItem t where 1=1 and qtTask='"+ id + "' and id in ("+st+") order by t.orderNumber desc limit 1";
		List<Integer> num = this.getSession().createQuery("select orderNumber "+hql).list();
		return num;
	}
	/**
	 * 所选样本(质控品)中最小排序号
	 * @param id
	 * @return
	 */
	public List<Integer> getQtTaskItemListMinxNum(String id,String st) {
		String hql = "from QtTaskItem t where 1=1 and qtTask='"+ id + "' and id in ("+st+") order by t.orderNumber asc limit 1";
		List<Integer> num = this.getSession().createQuery("select orderNumber "+hql).list();
		return num;
	}
	/**
	 * 查询排序号大于排序位置
	 * 且不包括移动的样本的样本集合(大的往上排)
	 * @param id
	 * @return
	 */
	public List<QtTaskItem> getQtTaskItemList(String id,Integer local,String st,Integer maxNum) {
		String hql = "from QtTaskItem t where 1=1 and qtTask='"
				+ id + "' and orderNumber>"+local+"  and orderNumber<="+maxNum+" and id not in ("+st+")";
		List<QtTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 查询排序号大于排序位置
	 * 且不包括移动的样本的样本集合(小的往下排)
	 * @param id
	 * @return
	 */
	public List<QtTaskItem> getQtTaskItemListmin(String id,Integer local,String st,Integer minNum) {
		String hql = "from QtTaskItem t where 1=1 and qtTask='"
				+ id + "' and orderNumber<="+local+"  and orderNumber>="+minNum+" and id not in ("+st+")";
		List<QtTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public QtTaskInfo getQtTaskInfoByIds(String id) {
		String hql = "from QtTaskInfo t where 1=1 and t.id='" + id + "'";
		QtTaskInfo list = (QtTaskInfo) this.getSession()
				.createQuery(hql).uniqueResult();
		return list;
	}

	public List<QtTaskInfo> findQtTaskInfoByCode(String code) {
		String hql = "from QtTaskInfo t where 1=1 and code='" + code + "'";
		List<QtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	
	}
}