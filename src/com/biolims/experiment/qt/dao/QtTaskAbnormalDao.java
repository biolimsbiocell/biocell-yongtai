package com.biolims.experiment.qt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qt.model.QtTaskAbnormal;

@Repository
@SuppressWarnings("unchecked")
public class QtTaskAbnormalDao extends BaseHibernateDao {
	public Map<String, Object> selectQtTaskAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QtTaskAbnormal t where 1=1 and t.state = '2'";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " ";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QtTaskAbnormal> list = new ArrayList<QtTaskAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

    /** 
     * 下一步流向为：文库构建
	 * 判断条件 结果判定：合格 下一步流向为：文库构建
	 */
	public List<QtTaskAbnormal> selectQtTaskToWKList(String code) {
		String hql = "from QtTaskAbnormal where 1=1 and method='1' and nextFlow='0' and sampleCode='"
				+ code + "'";
		List<QtTaskAbnormal> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据条件查询
	 * @param code
	 * @param Code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAbnormal(String code, String Code)
			throws Exception {
		String hql = "from QtTaskAbnormal t where 1=1 ";
		String key = "";
		if (!code.equals("") && Code.equals("")) {
			key = key + " and  t.code like '%" + code + "%'";
		} else if (code.equals("") && !Code.equals("")) {
			key = key + " and  t.Code like '%" + Code + "%'";
		} else if (!code.equals("") && !Code.equals("")) {
			key = key + " and  t.code like '%" + code
					+ "%' and  t.Code like '%" + Code + "%'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QtTaskAbnormal> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

}