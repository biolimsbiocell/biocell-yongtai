package com.biolims.experiment.qt.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskCos;
import com.biolims.experiment.qt.model.QtTaskInfo;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskReagent;
import com.biolims.experiment.qt.model.QtTaskTemplate;


@Repository
public class NewQtTaskDao extends CommonDAO {

	

	//点击待办事项查询明细
	public List<QtTaskItem> selTaskItem(String formId) throws Exception {
		String hql = "from QtTaskItem t where 1=1 and t.qtTask = '" + formId
				+ "'";
		List<QtTaskItem> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询实验模板ID
	public List<QtTask> selTemplat(String formId) throws Exception {
		String hql = "from QtTask t where 1=1 and id = '" + formId
				+ "'";
		List<QtTask> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表ID查询模板步骤明细
	public List<QtTaskTemplate> selTemplateItem(String formId) throws Exception {
		String hql = "from QtTaskTemplate t where 1=1 and t.qtTask = '" + formId
				+ "' order by t.code";
		List<QtTaskTemplate> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表ID查询模板试剂明细
	public List<QtTaskReagent> selReagent(String formId) throws Exception {
		String hql = "from QtTaskReagent t where 1=1 and t.qtTask = '" + formId
				+ "'";
		List<QtTaskReagent> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表ID查询模板设备明细
	public List<QtTaskCos> selCos(String formId) throws Exception {
		String hql = "from QtTaskCos t where 1=1 and t.qtTask = '" + formId
				+ "'";
		List<QtTaskCos> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表id判断结果是否已经生成
	public List<QtTaskInfo> selResult(String formId) throws Exception {
		String hql = "from QtTaskInfo t where 1=1 and t.qtTask = '" + formId
				+ "'";
		List<QtTaskInfo> list =  this.getSession().createQuery(hql).list();
		return list;
	}

	// 根木样本编号查询待入库样本findSampleInTempByCode
	public List<QtTaskItem> findTaskItemByCode(String code, String formId)
			throws Exception {
		String hql = "from QtTaskItem t where 1=1 and t.code='" + code
				+ "' and t.qtTask = '" + formId + "'";
		List<QtTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}


}
