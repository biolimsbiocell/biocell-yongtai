package com.biolims.experiment.qc.qpcrjd.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTask;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskCos;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskItem;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskReagent;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskResult;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemplate;
import com.biolims.experiment.qc.qpcrjd.service.QpcrjdTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/qc/qpcrjd/qpcrjdTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QpcrjdTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240408";
	@Autowired
	private QpcrjdTaskService qpcrjdTaskService;
	private QpcrjdTask qpcrjdTask = new QpcrjdTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showQpcrjdTaskList")
	public String showQpcrjdTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTask.jsp");
	}

	@Action(value = "showQpcrjdTaskListJson")
	public void showQpcrjdTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qpcrjdTaskService.findQpcrjdTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QpcrjdTask> list = (List<QpcrjdTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qpcrjdTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQpcrjdTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskDialog.jsp");
	}

	@Action(value = "showDialogQpcrjdTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQpcrjdTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qpcrjdTaskService.findQpcrjdTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QpcrjdTask> list = (List<QpcrjdTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQpcrjdTask")
	public String editQpcrjdTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qpcrjdTask = qpcrjdTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qpcrjdTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {

			qpcrjdTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qpcrjdTask.setCreateUser(user);
			qpcrjdTask.setCreateDate(new Date());
			qpcrjdTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qpcrjdTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}

		toState(qpcrjdTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskEdit.jsp");
	}

	@Action(value = "copyQpcrjdTask")
	public String copyQpcrjdTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qpcrjdTask = qpcrjdTaskService.get(id);
		qpcrjdTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qpcrjdTask.getId();

		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QpcrjdTask";
			String markCode = "QPCRJD";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qpcrjdTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("qpcrjdTaskItem",
				getParameterFromRequest("qpcrjdTaskItemJson"));

		aMap.put("qpcrjdTaskTemplate",
				getParameterFromRequest("qpcrjdTaskTemplateJson"));

		aMap.put("qpcrjdTaskReagent",
				getParameterFromRequest("qpcrjdTaskReagentJson"));

		aMap.put("qpcrjdTaskCos", getParameterFromRequest("qpcrjdTaskCosJson"));

		aMap.put("qpcrjdTaskResult",
				getParameterFromRequest("qpcrjdTaskResultJson"));

		qpcrjdTaskService.save(qpcrjdTask, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/qc/qpcrjd/qpcrjdTask/editQpcrjdTask.action?id="
				+ qpcrjdTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewQpcrjdTask")
	public String toViewQpcrjdTask() throws Exception {
		String id = getParameterFromRequest("id");
		qpcrjdTask = qpcrjdTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskEdit.jsp");
	}

	@Action(value = "showQpcrjdTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrjdTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskItem.jsp");
	}

	@Action(value = "showQpcrjdTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrjdTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskItem> list = (List<QpcrjdTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("productNum", "");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("state", "");
			map.put("qpcrjdTask-name", "");
			map.put("qpcrjdTask-id", "");
			map.put("note", "");
			map.put("tempId", "");

			map.put("sample", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("wkpdcd", "");
			map.put("wknd", "");
			map.put("xycd", "");
			map.put("ctcd", "");
			map.put("fcd", "");
			map.put("xzcd", "");
			map.put("qtcd", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskItem")
	public void delQpcrjdTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrjdTaskService.delQpcrjdTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrjdTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrjdTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskTemplate.jsp");
	}

	@Action(value = "showQpcrjdTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrjdTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskTemplate> list = (List<QpcrjdTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("qpcrjdTask-name", "");
			map.put("qpcrjdTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskTemplate> list = (List<QpcrjdTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("qpcrjdTask-name", "");
			map.put("qpcrjdTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskTemplate")
	public void delQpcrjdTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrjdTaskService.delQpcrjdTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskTemplateOne")
	public void delQpcrjdTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qpcrjdTaskService.delQpcrjdTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrjdTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrjdTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskReagent.jsp");
	}

	@Action(value = "showQpcrjdTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrjdTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskReagent> list = (List<QpcrjdTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("qpcrjdTask-name", "");
			map.put("qpcrjdTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskReagent")
	public void delQpcrjdTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrjdTaskService.delQpcrjdTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskReagentOne")
	public void delQpcrjdTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qpcrjdTaskService.delQpcrjdTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrjdTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrjdTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskCos.jsp");
	}

	@Action(value = "showQpcrjdTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrjdTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskCosList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskCos> list = (List<QpcrjdTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("qpcrjdTask-name", "");
			map.put("qpcrjdTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskCos")
	public void delQpcrjdTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrjdTaskService.delQpcrjdTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskCosOne")
	public void delQpcrjdTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qpcrjdTaskService.delQpcrjdTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrjdTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrjdTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskResult.jsp");
	}

	@Action(value = "showQpcrjdTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrjdTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskResult> list = (List<QpcrjdTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("note", "");
			map.put("qpcrjdTask", "");
			map.put("tempId", "");
			map.put("qpcrjdTask-name", "");
			map.put("qpcrjdTask-id", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("wkpdcd", "");
			map.put("zlnd", "");
			map.put("qtlr", "");
			map.put("mend", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskResult")
	public void delQpcrjdTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrjdTaskService.delQpcrjdTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrjdTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrjdTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrjd/qpcrjdTaskTemp.jsp");
	}

	@Action(value = "showQpcrjdTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrjdTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrjdTaskService
					.findQpcrjdTaskTempList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QpcrjdTaskTemp> list = (List<QpcrjdTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("volume", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note", "");

			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("wkpdcd", "");
			map.put("wknd", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrjdTaskTemp")
	public void delQpcrjdTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrjdTaskService.delQpcrjdTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QpcrjdTaskService getQpcrjdTaskService() {
		return qpcrjdTaskService;
	}

	public void setQpcrjdTaskService(QpcrjdTaskService qpcrjdTaskService) {
		this.qpcrjdTaskService = qpcrjdTaskService;
	}

	public QpcrjdTask getQpcrjdTask() {
		return qpcrjdTask;
	}

	public void setQpcrjdTask(QpcrjdTask qpcrjdTask) {
		this.qpcrjdTask = qpcrjdTask;
	}

}
