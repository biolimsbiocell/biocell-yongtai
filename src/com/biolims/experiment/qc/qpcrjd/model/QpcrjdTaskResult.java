package com.biolims.experiment.qc.qpcrjd.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: QPCR绝对定量结果(定量QPCR)
 * @author lims-platform
 * @date 2016-07-21 15:25:53
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QPCRJD_TASK_RESULT")
@SuppressWarnings("serial")
public class QpcrjdTaskResult extends EntityDao<QpcrjdTaskResult> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 实验室样本号 */
	private String labCode;
	/** 样本类型 */
	private String sampleType;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** index */
	private String indexa;
	/** 浓度 */
	private Double concentration;
	/** 体积 */
	private Double volume;
	/** 总量 */
	private Double sumTotal;
	/** 处理结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 是否提交 */
	private String submit;
	/** 下一步流向id */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 取样时间 */
	private Date inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 关联任务单 */
	private String orderId;
	/** 应出报告时间 */
	private Date reportDate;
	/** 状态 */
	private String state;
	/** 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private QpcrjdTask qpcrjdTask;
	/** 临时表Id */
	private String tempId;
	// 预混合组号
	private String yhhzh;
	// 混合组号
	private String hhzh;
	// 血液文库比例
	private String xylr;
	// ctDNA文库比例
	private String clr;
	// FFPR文库比例
	private String flr;
	// 新鲜组织文库比例
	private String xzlr;
	// 其他文库比例
	private String qtlr;
	// 通量
	private String tl;
	// 各组文库片段长度
	private String wkpdcd;
	// 质量浓度（ng/ul）
	private String zlnd;
	// 摩尔浓度（nM）
	private String mend;

	public String getYhhzh() {
		return yhhzh;
	}

	public void setYhhzh(String yhhzh) {
		this.yhhzh = yhhzh;
	}

	public String getHhzh() {
		return hhzh;
	}

	public void setHhzh(String hhzh) {
		this.hhzh = hhzh;
	}

	public String getXylr() {
		return xylr;
	}

	public void setXylr(String xylr) {
		this.xylr = xylr;
	}

	public String getClr() {
		return clr;
	}

	public void setClr(String clr) {
		this.clr = clr;
	}

	public String getFlr() {
		return flr;
	}

	public void setFlr(String flr) {
		this.flr = flr;
	}

	public String getXzlr() {
		return xzlr;
	}

	public void setXzlr(String xzlr) {
		this.xzlr = xzlr;
	}

	public String getQtlr() {
		return qtlr;
	}

	public void setQtlr(String qtlr) {
		this.qtlr = qtlr;
	}

	public String getTl() {
		return tl;
	}

	public void setTl(String tl) {
		this.tl = tl;
	}

	public String getWkpdcd() {
		return wkpdcd;
	}

	public void setWkpdcd(String wkpdcd) {
		this.wkpdcd = wkpdcd;
	}

	public String getZlnd() {
		return zlnd;
	}

	public void setZlnd(String zlnd) {
		this.zlnd = zlnd;
	}

	public String getMend() {
		return mend;
	}

	public void setMend(String mend) {
		this.mend = mend;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验室样本号
	 */
	@Column(name = "LAB_CODE", length = 50)
	public String getLabCode() {
		return this.labCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验室样本号
	 */
	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index
	 */
	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return this.indexa;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index
	 */
	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 总量
	 */
	@Column(name = "SUM_TOTAL", length = 50)
	public Double getSumTotal() {
		return this.sumTotal;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 总量
	 */
	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理结果
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理结果
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否提交
	 */
	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return this.submit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否提交
	 */
	public void setSubmit(String submit) {
		this.submit = submit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向id
	 */
	@Column(name = "NEXT_FLOW_ID", length = 50)
	public String getNextFlowId() {
		return this.nextFlowId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向id
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 取样时间
	 */
	@Column(name = "INSPECT_DATE", length = 50)
	public Date getInspectDate() {
		return this.inspectDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 取样时间
	 */
	public void setInspectDate(Date inspectDate) {
		this.inspectDate = inspectDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 接收日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 接收日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 应出报告时间
	 */
	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return this.reportDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 应出报告时间
	 */
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理意见
	 */
	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return this.method;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理意见
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得QpcrjdTask
	 * 
	 * @return: QpcrjdTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QPCRJD_TASK")
	public QpcrjdTask getQpcrjdTask() {
		return this.qpcrjdTask;
	}

	/**
	 * 方法: 设置QpcrjdTask
	 * 
	 * @param: QpcrjdTask 相关主表
	 */
	public void setQpcrjdTask(QpcrjdTask qpcrjdTask) {
		this.qpcrjdTask = qpcrjdTask;
	}
}