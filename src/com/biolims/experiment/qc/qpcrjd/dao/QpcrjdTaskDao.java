package com.biolims.experiment.qc.qpcrjd.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTask;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskCos;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskItem;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskReagent;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskResult;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemplate;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskReagent;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class QpcrjdTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectQpcrjdTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QpcrjdTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTask> list = new ArrayList<QpcrjdTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectQpcrjdTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrjdTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrjdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskItem> list = new ArrayList<QpcrjdTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrjdTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrjdTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrjdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskTemplate> list = new ArrayList<QpcrjdTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrjdTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrjdTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrjdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskReagent> list = new ArrayList<QpcrjdTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrjdTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrjdTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrjdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskCos> list = new ArrayList<QpcrjdTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrjdTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrjdTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrjdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskResult> list = new ArrayList<QpcrjdTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrjdTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrjdTaskTemp where 1=1 and state='1' ";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskTemp> list = new ArrayList<QpcrjdTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by sampleCode desc";

			}

			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<QpcrjdTaskItem> setItemList(String id) {
		String hql = "from QpcrjdTaskItem where 1=1 and qpcrjdTask.id='" + id
				+ "'";
		List<QpcrjdTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QpcrjdTaskTemp> setTempList(String code) {
		String hql = "from QpcrjdTaskTemp where 1=1 and code='" + code + "'";
		List<QpcrjdTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QpcrjdTaskResult> setResultList(String code) {
		String hql = "from QpcrjdTaskResult where 1=1 and qpcrjdTask.id='"
				+ code + "'";
		List<QpcrjdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 科技服务
	public Map<String, Object> selectTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		// String hql =
		// " from TechJkServiceTask t where 1=1 and t.orderType = '0'";//任务单类型为：建库
		String hql = " from TechJkServiceTask t where 1=1";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from TechJkServiceTaskItem where 1=1 and state = '1'";
		String key = "";
		if (scId != null)
			key = key + " and techJkServiceTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据科技服务主表id获取明细表信息
	public Map<String, Object> selectTechTaskItemByTaskId(String taskId) {
		String hql = "from TechJkServiceTaskItem t where 1=1 and t.techJkServiceTask='"
				+ taskId + "' and t.state = '1'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<TechJkServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<QpcrjdTaskItem> selectQpcrjdTaskItemList(String scId)
			throws Exception {
		String hql = "from QpcrjdTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrjd.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrjdTaskItem> list = new ArrayList<QpcrjdTaskItem>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 根据样本编号查询
	public List<QpcrjdTaskResult> setQpcrjdResultById(String code) {
		String hql = "from QpcrjdTaskResult t where (submit is null or submit='') and qpcrjdTask.id='"
				+ code + "'";
		List<QpcrjdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<QpcrjdTaskResult> setQpcrjdResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from QpcrjdTaskResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<QpcrjdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<QpcrjdTaskReagent> setReagentList(String code) {
		String hql = "from QpcrjdTaskReagent where 1=1 and qpcrxdTask.id='"
				+ code + "'";
		List<QpcrjdTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}
}
