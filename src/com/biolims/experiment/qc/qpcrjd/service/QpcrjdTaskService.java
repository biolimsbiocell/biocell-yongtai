package com.biolims.experiment.qc.qpcrjd.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.qc.qpcrjd.dao.QpcrjdTaskDao;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTask;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskCos;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskItem;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskReagent;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskResult;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemplate;
import com.biolims.experiment.qc.qpcrxd.dao.QpcrxdTaskDao;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskReagent;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskResult;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QpcrjdTaskService {
	@Resource
	private QpcrjdTaskDao qpcrjdTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private QpcrxdTaskDao qpcrxdTaskDao;
	@Resource
	private PoolingDao poolingDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private StorageService storageService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQpcrjdTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qpcrjdTaskDao.selectQpcrjdTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QpcrjdTask i) throws Exception {

		qpcrjdTaskDao.saveOrUpdate(i);

	}

	public QpcrjdTask get(String id) {
		QpcrjdTask qpcrjdTask = commonDAO.get(QpcrjdTask.class, id);
		return qpcrjdTask;
	}

	public Map<String, Object> findQpcrjdTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrjdTaskDao.selectQpcrjdTaskItemList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrjdTaskItem> list = (List<QpcrjdTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findQpcrjdTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrjdTaskDao
				.selectQpcrjdTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<QpcrjdTaskTemplate> list = (List<QpcrjdTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findQpcrjdTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrjdTaskDao.selectQpcrjdTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrjdTaskReagent> list = (List<QpcrjdTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findQpcrjdTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrjdTaskDao.selectQpcrjdTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrjdTaskCos> list = (List<QpcrjdTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findQpcrjdTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrjdTaskDao.selectQpcrjdTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrjdTaskResult> list = (List<QpcrjdTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findQpcrjdTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrjdTaskDao.selectQpcrjdTaskTempList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrjdTaskTemp> list = (List<QpcrjdTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrjdTaskItem(QpcrjdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrjdTaskItem> saveItems = new ArrayList<QpcrjdTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrjdTaskItem scp = new QpcrjdTaskItem();
			// 将map信息读入实体类
			scp = (QpcrjdTaskItem) qpcrjdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrjdTask(sc);

			saveItems.add(scp);
		}
		qpcrjdTaskDao.saveOrUpdateAll(saveItems);

		// // 设置保留三维小数
		// DecimalFormat df = new DecimalFormat("######.000");
		//
		// List<QpcrjdTaskItem> list2 =
		// this.qpcrjdTaskDao.setItemList(sc.getId());
		// for (QpcrjdTaskItem q : list2) {
		// // wkpdcd = xylr * xycd + clr * ctcd + flr * fcd + qtlr * qtcd +
		// // Xzlr * Xzcd 混合后文库长度（bp）计算公式\
		//
		// Double b = 0.0;
		// if (q.getXylr() != null && !"".equals(q.getXylr())
		// && q.getXycd() != null) {
		// b += Double.parseDouble(q.getXylr())
		// * Double.parseDouble(q.getXycd());
		// }
		// if (q.getClr() != null && !"".equals(q.getClr())
		// && q.getCtcd() != null) {
		// b += Double.parseDouble(q.getClr())
		// * Double.parseDouble(q.getCtcd());
		// }
		// if (q.getFlr() != null && !"".equals(q.getFlr())
		// && q.getFcd() != null) {
		// b += Double.parseDouble(q.getFlr())
		// * Double.parseDouble(q.getFcd());
		// }
		// if (q.getFlr() != null && !"".equals(q.getFlr())
		// && q.getFcd() != null) {
		// b += Double.parseDouble(q.getFlr())
		// * Double.parseDouble(q.getFcd());
		// }
		// if (q.getXzlr() != null && !"".equals(q.getXzlr())
		// && q.getXzcd() != null) {
		// b += Double.parseDouble(q.getXzlr())
		// * Double.parseDouble(q.getXzcd());
		// }
		// q.setWkpdcd(Double.valueOf(df.format(b / 100)).toString());
		// }
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrjdTaskItem scp = qpcrjdTaskDao.get(QpcrjdTaskItem.class, id);
			qpcrjdTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrjdTaskTemplate(QpcrjdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrjdTaskTemplate> saveItems = new ArrayList<QpcrjdTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrjdTaskTemplate scp = new QpcrjdTaskTemplate();
			// 将map信息读入实体类
			scp = (QpcrjdTaskTemplate) qpcrjdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrjdTask(sc);

			saveItems.add(scp);
		}
		qpcrjdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrjdTaskTemplate scp = qpcrjdTaskDao.get(
					QpcrjdTaskTemplate.class, id);
			qpcrjdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskTemplateOne(String ids) throws Exception {

		QpcrjdTaskTemplate scp = qpcrjdTaskDao.get(QpcrjdTaskTemplate.class,
				ids);
		qpcrjdTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrjdTaskReagent(QpcrjdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrjdTaskReagent> saveItems = new ArrayList<QpcrjdTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrjdTaskReagent scp = new QpcrjdTaskReagent();
			// 将map信息读入实体类
			scp = (QpcrjdTaskReagent) qpcrjdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrjdTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		qpcrjdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrjdTaskReagent scp = qpcrjdTaskDao.get(QpcrjdTaskReagent.class,
					id);
			qpcrjdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskReagentOne(String ids) throws Exception {

		QpcrjdTaskReagent scp = qpcrjdTaskDao.get(QpcrjdTaskReagent.class, ids);
		qpcrjdTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrjdTaskCos(QpcrjdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrjdTaskCos> saveItems = new ArrayList<QpcrjdTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrjdTaskCos scp = new QpcrjdTaskCos();
			// 将map信息读入实体类
			scp = (QpcrjdTaskCos) qpcrjdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrjdTask(sc);

			saveItems.add(scp);
		}
		qpcrjdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrjdTaskCos scp = qpcrjdTaskDao.get(QpcrjdTaskCos.class, id);
			qpcrjdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskCosOne(String ids) throws Exception {

		QpcrjdTaskCos scp = qpcrjdTaskDao.get(QpcrjdTaskCos.class, ids);
		qpcrjdTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrjdTaskResult(QpcrjdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrjdTaskResult> saveItems = new ArrayList<QpcrjdTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrjdTaskResult scp = new QpcrjdTaskResult();
			// 将map信息读入实体类
			scp = (QpcrjdTaskResult) qpcrjdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrjdTask(sc);

			saveItems.add(scp);
		}
		qpcrjdTaskDao.saveOrUpdateAll(saveItems);

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrjdTaskResult scp = qpcrjdTaskDao
					.get(QpcrjdTaskResult.class, id);
			qpcrjdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrjdTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrjdTaskTemp scp = qpcrjdTaskDao.get(QpcrjdTaskTemp.class, id);
			qpcrjdTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QpcrjdTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qpcrjdTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("qpcrjdTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrjdTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrjdTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrjdTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrjdTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrjdTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrjdTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrjdTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrjdTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrjdTaskResult(sc, jsonStr);
			}
		}
	}

	// 审核完成
//	public void changeState(String applicationTypeActionId, String id)
//			throws Exception {
//		QpcrjdTask sct = qpcrjdTaskDao.get(QpcrjdTask.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//		qpcrjdTaskDao.update(sct);
//
//		// 将批次信息反馈到模板中
//		List<QpcrjdTaskReagent> list1 = qpcrjdTaskDao.setReagentList(id);
//		for (QpcrjdTaskReagent dt : list1) {
//			String bat1 = dt.getBatch(); // 实验中试剂的批次
//			String drid = dt.getTReagent(); // 实验中保存的试剂ID
//			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
//			for (ReagentItem ri : list2) {
//				if (bat1 != null) {
//					ri.setBatch(bat1);
//				}
//			}
//			// 改变库存主数据试剂数量
//			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
//					dt.getBatch(), dt.getNum());
//		}
//
//		// 获取结果表样本信息
//		List<QpcrjdTaskResult> list = this.qpcrjdTaskDao
//				.setQpcrjdResultById(id);
//		for (QpcrjdTaskResult scp : list) {
//			if (scp != null) {
//				if (scp.getNextFlow() != null) {
//					if (scp.getNextFlowId().equals("0009")) {// 样本入库
//						SampleInItemTemp st = new SampleInItemTemp();
//						st.setCode(scp.getSampleCode());
//						st.setSampleCode(scp.getHhzh());
//						st.setInfoFrom("QpcrjdTaskResult");
//						st.setState("1");
//						st.setVolume(Double.parseDouble(scp.getTl()));
//						this.qpcrjdTaskDao.saveOrUpdate(st);
//					} else if (scp.getNextFlowId().equals("0033")) {// 文库浓度调整
//						WkndTaskTemp w = new WkndTaskTemp();
//						sampleInputService.copy(w, scp);
//						w.setSjfz(scp.getSampleCode());
//						w.setYzlnd(scp.getZlnd());
//						w.setYmend(scp.getMend());
//						w.setCxlx(scp.getProductId());
//						w.setCxpt(scp.getProductName());
//						w.setCxdc(scp.getName());
//						w.setState("1");
//						this.qpcrjdTaskDao.saveOrUpdate(w);
//					} else {
//						// 得到下一步流向的相关表单
//						List<NextFlow> list_nextFlow = nextFlowDao
//								.seletNextFlowById(scp.getNextFlowId());
//						for (NextFlow n : list_nextFlow) {
//							Object o = Class.forName(
//									n.getApplicationTypeTable().getClassPath())
//									.newInstance();
//							scp.setState("1");
//							sampleInputService.copy(o, scp);
//						}
//					}
//				}
//			}
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			// 根据上机分组号查询文库混合QPCR
//			List<QpcrxdTaskResult> listQpcrxdTaskResult = this.qpcrxdTaskDao
//					.setResultListbySjfz(scp.getSampleCode());
//			for (QpcrxdTaskResult q : listQpcrxdTaskResult) {
//				// 根据富集文库查询明细样本
//				List<PoolingItem> listPoolingTaskItem = this.poolingDao
//						.getItemListByfjwk(q.getCode());
//				for (PoolingItem p : listPoolingTaskItem) {
//					sampleStateService.saveSampleState(
//							p.getCode(),
//							p.getSampleCode(),
//							p.getProductId(),
//							p.getProductName(),
//							"",
//							format.format(sct.getCreateDate()),
//							format.format(new Date()),
//							"QpcrjdTask",
//							"文库定量QPCR",
//							(User) ServletActionContext
//									.getRequest()
//									.getSession()
//									.getAttribute(
//											SystemConstants.USER_SESSION_KEY),
//							id, scp.getNextFlow(), null, null, null, null,
//							null, null, null, null, null);
//				}
//			}
//		}
//	}

	public List<QpcrjdTaskItem> findDnaItemList(String scId) throws Exception {
		List<QpcrjdTaskItem> list = qpcrjdTaskDao.setItemList(scId);
		return list;
	}

	// 科技服务-任务单主表
	public Map<String, Object> findTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return this.qpcrjdTaskDao.selectTechJkServiceTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	// 科技服务-任务单明细表
	public Map<String, Object> findTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.qpcrjdTaskDao
				.selectTechServiceTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		return result;
	}

	// 根据科技服务主表id 查询明细信息
	public List<Map<String, Object>> selectTechTaskItemByTaskId(String taskId) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = this.qpcrjdTaskDao
				.selectTechTaskItemByTaskId(taskId);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (TechJkServiceTaskItem ti : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", ti.getId());
				map.put("name", ti.getName());
				map.put("sampleCode", ti.getSampleCode());
				map.put("state", ti.getState());
				map.put("stateName", ti.getStateName());

				map.put("projectId", ti.getTechJkServiceTask().getProjectId());
				map.put("contractId", ti.getTechJkServiceTask().getContractId());
				map.put("orderType", ti.getTechJkServiceTask().getOrderType());
				map.put("experimentUser", ti.getExperimentUser());
				map.put("endDate", ti.getEndDate());
				map.put("species", ti.getSpecies());
				map.put("classify", ti.getClassify());

				map.put("techJkServiceTaskId", ti.getTechJkServiceTask()
						.getId());
				map.put("techJkServiceTaskName", ti.getTechJkServiceTask()
						.getName());
				// if(ti.getSampleCode()!=null){
				// List<TechJkServiceTask> l =
				// commonService.get(TechJkServiceTask.class, "sampleCode",
				// ti.getSampleCode());
				// if(l.size()>0)
				// ti.setTechJkServiceTask(l.get(0));
				// }
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<QpcrjdTaskItem> findQpcrjdTaskItemList(String scId)
			throws Exception {
		List<QpcrjdTaskItem> list = qpcrjdTaskDao
				.selectQpcrjdTaskItemList(scId);
		return list;
	}

}
