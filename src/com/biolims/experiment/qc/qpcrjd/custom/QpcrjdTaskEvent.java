package com.biolims.experiment.qc.qpcrjd.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.qc.qpcrjd.service.QpcrjdTaskService;

public class QpcrjdTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QpcrjdTaskService mbService = (QpcrjdTaskService) ctx.getBean("qpcrjdTaskService");
//		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}