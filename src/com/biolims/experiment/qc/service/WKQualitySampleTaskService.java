package com.biolims.experiment.qc.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.qc.dao.QualityCheckDao;
import com.biolims.experiment.qc.dao.WKAcceptDao;
import com.biolims.experiment.qc.dao.WKExceptionDao;
import com.biolims.experiment.qc.dao.WKQpcrSampleTaskDao;
import com.biolims.experiment.qc.dao.WKQualitySampleTaskDao;
import com.biolims.experiment.qc.model.Qc2100Abnormal;
import com.biolims.experiment.qc.model.Qc2100Task;
import com.biolims.experiment.qc.model.Qc2100TaskCos;
import com.biolims.experiment.qc.model.Qc2100TaskItem;
import com.biolims.experiment.qc.model.Qc2100TaskReagent;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemplate;
import com.biolims.experiment.qc.model.QcCheck;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackQuality;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.main.service.StorageService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKQualitySampleTaskService {
	@Resource
	private WKQualitySampleTaskDao wKQualitySampleTaskDao;
	@Resource
	private WKQpcrSampleTaskDao wKQpcrSampleTaskDao;
	@Resource
	private QualityCheckDao qualityCheckDao;
	@Resource
	private WKAcceptDao wKAcceptDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private WKExceptionDao wKExceptionDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private StorageService storageService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWKQualityList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKQualitySampleTaskDao.selectWKQualityList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Qc2100Task i) throws Exception {

		wKQualitySampleTaskDao.saveOrUpdate(i);

	}

	public Qc2100Task get(String id) {
		Qc2100Task wKQuality = commonDAO.get(Qc2100Task.class, id);
		return wKQuality;
	}

	public Map<String, Object> findWKQualityItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectWKQualityItemList(scId, startNum, limitNum, dir, sort);
		List<Qc2100TaskItem> list = (List<Qc2100TaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findExecuteStepItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectExecuteStepItemList(scId, startNum, limitNum, dir, sort);
		List<Qc2100TaskTemplate> list = (List<Qc2100TaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findDeviceItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectDeviceItemList(scId, startNum, limitNum, dir, sort);
		List<Qc2100TaskCos> list = (List<Qc2100TaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findQualityResultItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectQualityResultItemList(scId, startNum, limitNum, dir,
						sort);
		List<SampleQc2100Info> list = (List<SampleQc2100Info>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findReagentItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectReagentItemList(scId, startNum, limitNum, dir, sort);
		return result;
	}

	// 根据关联item查询
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectReagentItemListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wKQualitySampleTaskDao
				.selectCosItemListByItemId(scId, startNum, limitNum, dir, sort,
						itemId);
		List<Qc2100TaskCos> list = (List<Qc2100TaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKQualityItem(Qc2100Task sc, String itemDataJson)
			throws Exception {
		List<Qc2100TaskItem> saveItems = new ArrayList<Qc2100TaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);

		// String index1 = wKQualitySampleTaskDao.getIndexMax();
		// List<Qc2100Task> list1 = wKQualitySampleTaskDao.getIndexValue(sc
		// .getId());
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = wKQualitySampleTaskDao.get(Template.class, sc.getTemplate()
					.getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = wKQualitySampleTaskDao.get(
						StorageContainer.class, temp.getStorageContainer()
								.getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();
			}
		}
		for (Map<String, Object> map : list) {
			Qc2100TaskItem scp = new Qc2100TaskItem();
			// 将map信息读入实体类
			scp = (Qc2100TaskItem) wKQualitySampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQc2100Task(sc);

			Template t = templateDao.get(Template.class, sc.getTemplate()
					.getId());
			if (t.getSampleNum() != null) {
				if (scp.getSampleConsume() == null) {
					scp.setSampleConsume(t.getSampleNum());
				}
			}
			saveItems.add(scp);
		}
		wKQualitySampleTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(wKQualitySampleTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQualityItem(String[] ids) throws Exception {
		for (String id : ids) {
			Qc2100TaskItem scp = wKQualitySampleTaskDao.get(
					Qc2100TaskItem.class, id);
			wKQualitySampleTaskDao.delete(scp);
		}
	}

	// ===========2015-12-02 ly==============
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQualityDeviceOne(String ids) throws Exception {
		Qc2100TaskCos scp = wKQualitySampleTaskDao
				.get(Qc2100TaskCos.class, ids);
		wKQualitySampleTaskDao.delete(scp);
	}

	// =========================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveExecuteStepItem(Qc2100Task sc, String itemDataJson)
			throws Exception {
		List<Qc2100TaskTemplate> saveItems = new ArrayList<Qc2100TaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			Qc2100TaskTemplate scp = new Qc2100TaskTemplate();
			// 将map信息读入实体类
			scp = (Qc2100TaskTemplate) wKQualitySampleTaskDao
					.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setwKQualitySampleTask(sc);

			saveItems.add(scp);
		}
		wKQualitySampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delExecuteStepItem(String[] ids) throws Exception {
		for (String id : ids) {
			Qc2100TaskTemplate scp = wKQualitySampleTaskDao.get(
					Qc2100TaskTemplate.class, id);
			wKQualitySampleTaskDao.delete(scp);
		}
	}

	// ================2015-12-02 ly==============
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delExecuteStepItemOne(String ids) throws Exception {
		Qc2100TaskTemplate scp = wKQualitySampleTaskDao.get(
				Qc2100TaskTemplate.class, ids);
		wKQualitySampleTaskDao.delete(scp);
	}

	// ==============================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDeviceItem(Qc2100Task sc, String itemDataJson)
			throws Exception {
		List<Qc2100TaskCos> saveItems = new ArrayList<Qc2100TaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			Qc2100TaskCos scp = new Qc2100TaskCos();
			// 将map信息读入实体类
			scp = (Qc2100TaskCos) wKQualitySampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQc2100Task(sc);

			saveItems.add(scp);
		}
		wKQualitySampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDeviceItem(String[] ids) throws Exception {
		for (String id : ids) {
			Qc2100TaskCos scp = wKQualitySampleTaskDao.get(Qc2100TaskCos.class,
					id);
			wKQualitySampleTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQualityResultItem(Qc2100Task sc, String itemDataJson)
			throws Exception {

		List<SampleQc2100Info> saveItems = new ArrayList<SampleQc2100Info>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleQc2100Info scp = new SampleQc2100Info();
			// 将map信息读入实体类
			scp = (SampleQc2100Info) wKQualitySampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setState("1");
			scp.setwKQualitySampleTask(sc);
			// 将临时表中样本的状态，设为无效
			// List<Qc2100TaskTemp> wkt = wKQualitySampleTaskDao
			// .findWKQualityTempList();
			// for (Qc2100TaskTemp wkti : wkt) {
			// if (scp.getCode().equals(wkti.getCode())) {
			// // 将临时表中样本的状态，设为无效
			// wkti.setState("2");
			// }
			// }
			saveItems.add(scp);

		}
		wKQualitySampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQualityResultItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleQc2100Info scp = wKQualitySampleTaskDao.get(
					SampleQc2100Info.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				wKQualitySampleTaskDao.delete(scp);
		}
	}

	/**
	 * 向pooling传递数据
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
//	public void setToPoolingTemp(SampleQc2100Info scp) {
//		PoolingTaskTemp pt = new PoolingTaskTemp();
//		WorkOrder wo = null;// 产前任务单
//		TechJkServiceTask t = null;// 科技服务任务单
//		if (scp.getClassify().equals("0")) {// 产前
//			if (scp.getOrderId() != null) {
//				wo = sampleInfoMainDao.get(WorkOrder.class, scp.getOrderId());
//				if (wo != null) {
//					pt.setSequencingPlatform(wo.getSequencingPlatform());
//					pt.setSequencingReadLong(wo.getSequencingReadLong());
//					pt.setSequencingType(wo.getSequencingMethod());
//					pt.setMixRatio(wo.getMixRatio());
//					pt.setYw(wo.getYw());
//					pt.setDensity(wo.getDensity());
//
//					pt.setSampleCode(scp.getSampleCode());
//					pt.setWkCode(scp.getWkId());
//					pt.setWkIndex(scp.getIndexa());
//					// pt.setConcentration(scp.getConcentration());
//					pt.setOrderId(scp.getOrderId());
//					pt.setPatient(scp.getPatientName());
//					pt.setProductId(scp.getProductId());
//					pt.setProductName(scp.getProductName());
//					pt.setInspectDate(scp.getInspectDate());
//					pt.setAcceptDate(scp.getAcceptDate());
//					pt.setIdCard(scp.getIdCard());
//					pt.setPhone(scp.getPhone());
//					pt.setReportDate(scp.getReportDate());
//					pt.setState("1");
//					pt.setQcId(scp.getwKQualitySampleTask().getId());
//					pt.setVolume(scp.getVolume());
//					pt.setClassify(scp.getClassify());
//					this.wKQualitySampleTaskDao.saveOrUpdate(pt);
//				}
//			}
//		}
//		if (scp.getClassify().equals("1")) {// 科技服务
//			if (scp.getTechTaskId() != null) {
//				t = wkDao.get(TechJkServiceTask.class, scp.getTechTaskId());
//				if (t != null) {
//					if (t.getOrderType().equals("1")) {// 任务单类型：建库
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QC_COMPLETE);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_QC_COMPLETE_NAME);
//						}
//					} else {// 任务单类型：建库测序及其他
//						pt.setSequencingPlatform(t.getSequencePlatform());
//						pt.setSequencingReadLong(t.getSequenceLength());
//						pt.setSequencingType(t.getSequenceType());
//						pt.setMixRatio(t.getMixRatio());
//						// pt.setYw(t.getYw());
//						pt.setDensity(t.getDensity());
//						// pt.setProjectId(t.getProjectId());
//						// pt.setOrderType(t.getOrderType());
//						// pt.setContractId(t.getContractId());
//						// pt.setTechTaskId(scp.getTechTaskId());
//
//						pt.setSampleCode(scp.getSampleCode());
//						pt.setWkCode(scp.getWkId());
//						pt.setWkIndex(scp.getIndexa());
//						// pt.setConcentration(scp.getConcentration());
//						pt.setOrderId(scp.getOrderId());
//						pt.setPatient(scp.getPatientName());
//						pt.setProductId(scp.getProductId());
//						pt.setProductName(scp.getProductName());
//						pt.setInspectDate(scp.getInspectDate());
//						pt.setAcceptDate(scp.getAcceptDate());
//						pt.setIdCard(scp.getIdCard());
//						pt.setPhone(scp.getPhone());
//						pt.setReportDate(scp.getReportDate());
//						pt.setState("1");
//						pt.setQcId(scp.getwKQualitySampleTask().getId());
//						pt.setVolume(scp.getVolume());
//						pt.setClassify(scp.getClassify());
//						this.wKQualitySampleTaskDao.saveOrUpdate(pt);
//					}
//				}
//			}
//		}
//
//		// pt.setContractId(scp.getContractId());
//		// pt.setOrderType(scp.getOrderType());
//		// pt.setProjectId(scp.getProjectId());
//		// pt.setTechTaskId(scp.getTechTaskId());
//		qualityCheckDao.saveOrUpdate(pt);
//		// 审批完成后，改变SampleInfo中原始样本的状态为“2100质检完成”
//		SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp.getSampleCode());
//		if (sf != null) {
//			sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QC_COMPLETE);
//			sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_QC_COMPLETE_NAME);
//		}
//	}

	/**
	 * 数据到质检审核
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	public void setToQcCheck(SampleQc2100Info scp) {

		QcCheck qCheck = new QcCheck();
		qCheck.setWkCode(scp.getWkId());
		qCheck.setSampleCode(scp.getSampleCode());
		qCheck.setWkType(scp.getWkType());
		qCheck.setIndexa(scp.getIndexa());
		qCheck.setAcceptDate(scp.getAcceptDate());
		qCheck.setInspectDate(scp.getInspectDate());
		qCheck.setReportDate(scp.getReportDate());
		qCheck.setResult(scp.getResult());
		qualityCheckDao.saveOrUpdate(qCheck);
	}

	/**
	 * 审批完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		Qc2100Task wkq = wKQualitySampleTaskDao.get(Qc2100Task.class, id);
		wkq.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		wkq.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		// sct.setConfirmUser(user);
		// sct.setConfirmDate(new Date());

		wKQualitySampleTaskDao.update(wkq);
		String mainid = wkq.getId();

		// 将批次信息反馈到模板中
		List<Qc2100TaskReagent> list1 = wKQualitySampleTaskDao
				.setReagentList(wkq.getId());
		for (Qc2100TaskReagent dt : list1) {
			String bat1 = dt.getBatch(); // qc2100实验中试剂的批次
			String drid = dt.gettReagent(); // qc2100实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}

			// 改变库存主数据试剂数量
			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
					dt.getBatch(), dt.getNum());
		}

		// 改变左侧状态
		List<Qc2100TaskItem> item = wKQualitySampleTaskDao.setItemList(wkq
				.getId());
		for (Qc2100TaskItem c : item) {
			Qc2100TaskTemp temp = commonDAO.get(Qc2100TaskTemp.class,
					c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReagentList(Qc2100Task sc, String itemDataJson)
			throws Exception {
		List<Qc2100TaskReagent> saveItems = new ArrayList<Qc2100TaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			Qc2100TaskReagent scp = new Qc2100TaskReagent();
			// 将map信息读入实体类
			scp = (Qc2100TaskReagent) wKQualitySampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQc2100Task(sc);
			// 用量 = 单个用量*数量
			if (scp != null && scp.getOneNum() != null
					&& scp.getSampleNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}
			saveItems.add(scp);
		}
		wKQualitySampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItem(String[] ids) throws Exception {
		for (String id : ids) {
			ReagentItem scp = wKQualitySampleTaskDao.get(ReagentItem.class, id);
			wKQualitySampleTaskDao.delete(scp);
		}
	}

	// =============2015-12-02 ly================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReagentItemOne(String ids) throws Exception {
		ReagentItem scp = wKQualitySampleTaskDao.get(ReagentItem.class, ids);
		wKQualitySampleTaskDao.delete(scp);
	}

	// =============================
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Qc2100Task sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wKQualitySampleTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wKQualityItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKQualityItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKQualityTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveExecuteStepItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKQualityDevice");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDeviceItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKQualitySampleInfo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQualityResultItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKQualityReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveReagentList(sc, jsonStr);

			}
		}
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKQualitySampleTaskDao
				.setReagent(id, code);
		List<Qc2100TaskReagent> list = (List<Qc2100TaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (Qc2100TaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsTestSuccess());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getQc2100Task().getId());
				map.put("tName", ti.getQc2100Task().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKQualitySampleTaskDao.setCos(id, code);
		List<Qc2100TaskCos> list = (List<Qc2100TaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (Qc2100TaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsTestSuccess());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}

				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getQc2100Task().getId());
				map.put("tName", ti.getQc2100Task().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	public List<Qc2100TaskItem> findQc2100ItemList(String scId)
			throws Exception {
		List<Qc2100TaskItem> list = wKQualitySampleTaskDao
				.selectQc2100ItemList(scId);
		return list;
	}

	// 提交样本
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		Qc2100Task sc = this.wKQualitySampleTaskDao.get(Qc2100Task.class, id);
		// 获取结果表样本信息
		List<SampleQc2100Info> list;
		if (ids == null)
			list = this.wKQualitySampleTaskDao.setQc2100ResultById(id);
		else
			list = this.wKQualitySampleTaskDao.setQc2100ResultByIds(ids);
		for (SampleQc2100Info scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))
						&& scp.getNextFlowId() != null) {
					if (scp.getResult().equals("1")) {// 合格
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setSampleType(scp.getSampleType());
							DicSampleType d = dicSampleTypeDao
									.selectDicSampleTypeByName(scp
											.getSampleType());
							st.setSampleTypeId(d.getId());
							st.setInfoFrom("SampleQc2100Info");
							st.setState("1");
							st.setConcentration(scp.getWkConcentration());
							st.setVolume(scp.getWkVolume());
							st.setSumTotal(scp.getWkSumTotal());
							SampleInfo s = this.sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							st.setPatientName(s.getPatientName());
							wKQpcrSampleTaskDao.saveOrUpdate(st);
							// 入库，改变SampleInfo中原始样本的状态为“待入库”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
							}
						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
//							WkAbnormalBack wka = new WkAbnormalBack();
//							scp.setState("4");
//							sampleInputService.copy(wka, scp);
//							wka.setWkCode(scp.getWkId());
//							wKQualitySampleTaskDao.saveOrUpdate(wka);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(eda, scp);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
							}
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
							// 终止，改变SampleInfo中原始样本的状态为“实验终止”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
							}
						} else if (scp.getNextFlowId().equals("0015")) {// pooling
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							scp.setState("1");
//							sampleInputService.copy(ptt, scp);
//							ptt.setWkCode(scp.getWkId());
//							ptt.setWkIndex(scp.getIndexa());
//							this.wKQpcrSampleTaskDao.saveOrUpdate(ptt);
							// sampleInputService.copy(ptt, scp);
						}
					} else {// 不合格
						if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
							// 下一步流向是：反馈至项目组
							FeedbackQuality fd = new FeedbackQuality();
							scp.setState("1");
							sampleInputService.copy(fd, scp);
							fd.setSubmit("");
							fd.setWkCode(scp.getWkId());
							this.wKQualitySampleTaskDao.saveOrUpdate(fd);
						} else {
							Qc2100Abnormal qc = new Qc2100Abnormal();
							scp.setState("1");
							sampleInputService.copy(qc, scp);
							qc.setWkCode(scp.getWkId());
							wKQualitySampleTaskDao.saveOrUpdate(qc);
						}

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getReleaseDate(),
							format.format(new Date()),
							"Qc2100Task",
							"2100质控",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
				}
			}
		}
	}
}
