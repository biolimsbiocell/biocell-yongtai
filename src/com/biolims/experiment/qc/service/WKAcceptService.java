package com.biolims.experiment.qc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qc.dao.WKAcceptDao;
import com.biolims.experiment.qc.dao.WKQpcrSampleTaskDao;
import com.biolims.experiment.qc.dao.WKQualitySampleTaskDao;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcPoolingTaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.QcReceive;
import com.biolims.experiment.qc.model.QcReceiveItem;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.experiment.qc.model.TechCheckServiceQcTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKAcceptService {
	@Resource
	private WKAcceptDao wKAcceptDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private WKQualitySampleTaskDao wKQualitySampleTaskDao;
	@Resource
	private WKQpcrSampleTaskDao wKQpcrSampleTaskDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWKAcceptList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKAcceptDao.selectWKAcceptList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	/**
	 * 左侧任务单中间表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> findTechCheckServiceQcOrder(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKAcceptDao.selectTechCheckServiceQcOrder(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	/**
	 * 左侧科技服务样本中间表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> findTechCheckServiceQcTemp(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKAcceptDao.selectTechCheckServiceQcTemp(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QcReceive i) throws Exception {

		wKAcceptDao.saveOrUpdate(i);

	}

	public QcReceive get(String id) {
		QcReceive wKAccept = commonDAO.get(QcReceive.class, id);
		return wKAccept;
	}

	public Map<String, Object> findWKAcceptItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKAcceptDao.selectWKAcceptItemList(scId,
				startNum, limitNum, dir, sort);
		List<QcReceiveItem> list = (List<QcReceiveItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findPOOLINGAcceptItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKAcceptDao.selectPOOLINGAcceptItemList(
				scId, startNum, limitNum, dir, sort);
		// List<PoolingAcceptItem> list = (List<PoolingAcceptItem>)
		// result.get("list");
		return result;
	}

	// 查询子表数据，加载到2100质控左侧
	public Map<String, Object> findWKQualityTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKAcceptDao.selectWKQualityTempList(
				startNum, limitNum, dir, sort);
		List<Qc2100TaskTemp> list = (List<Qc2100TaskTemp>) result.get("list");
		return result;
	}

	// 查询临时表数据，加载到QPCR质控左侧
	public Map<String, Object> findWKQpcrTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKAcceptDao.selectWKQpcrTempList(startNum,
				limitNum, dir, sort);
		List<QcQpcrTaskTemp> list = (List<QcQpcrTaskTemp>) result.get("list");
		return result;
	}

	// 查询临时表数据，加载到QPCR质控左侧
	public Map<String, Object> findQcQpcrTaskTemppList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKAcceptDao.selectQcPoolingTaskTempList(
				startNum, limitNum, dir, sort);
		List<QcPoolingTaskTemp> list = (List<QcPoolingTaskTemp>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKAcceptItem(QcReceive sc, String itemDataJson)
			throws Exception {
		List<QcReceiveItem> saveItems = new ArrayList<QcReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QcReceiveItem scp = new QcReceiveItem();
			// 将map信息读入实体类
			scp = (QcReceiveItem) wKAcceptDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkAccept(sc);
			// //将临时表的样本状态，设为不可查
			// List<QcReceiveTemp> tmp = wKAcceptDao.findWKAcceptTemList();
			// for(QcReceiveTemp tmpp : tmp){
			// if(scp!=null && tmpp.getSampleCode()!=null){
			// if(tmpp.getSampleCode().equals(scp.getSampleCode())){
			// tmpp.setState("2");
			// }
			// }
			// }
			saveItems.add(scp);
		}
		this.modifyXyName(saveItems, sc.getLocation());
		wKAcceptDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 赋值存储位置编码
	 * 
	 * @param saveItems
	 * @throws Exception
	 */
	private void modifyXyName(List<QcReceiveItem> saveItems, String loca)
			throws Exception {
		Integer boardNum = 1;
		Integer yNum = 0;
		Integer xNum = 1;
		String[] xName = { "A", "B", "C", "D", "E", "F", "G", "H" };
		Object obj = wKAcceptDao.selectMaxBoardNumAndMaxXNum();
		if (obj != null) {
			Object[] objs = (Object[]) obj;
			boardNum = (Integer) (objs[0] != null ? objs[0] : boardNum);
			xNum = (Integer) (objs[1] != null ? objs[1] : xNum);
			yNum = (Integer) (objs[2] != null ? objs[2] : yNum);
		}
		yNum++;
		for (QcReceiveItem item : saveItems) {

			if (item.getXyName() != null && !item.getXyName().equals(""))
				continue;

			if (yNum > 12) {// 96孔板，为12列
				yNum = 1;
				xNum++;
			}
			if (xNum > xName.length) {
				boardNum++;
				yNum = 1;
				xNum = 1;
			}
			// item.setCounts(boardNum);
			item.setXyName(loca + "-" + boardNum + "-" + xName[xNum - 1] + yNum);
			item.setNumX(xNum);
			item.setNumY(yNum);
			yNum++;
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKAcceptItem(String[] ids) throws Exception {
		for (String id : ids) {
			QcReceiveItem scp = wKAcceptDao.get(QcReceiveItem.class, id);
			wKAcceptDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePOOLINGAcceptItem(QcReceive sc, String itemDataJson)
			throws Exception {
		// List<PoolingAcceptItem> saveItems = new
		// ArrayList<PoolingAcceptItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			// PoolingAcceptItem scp = new PoolingAcceptItem();
			// 将map信息读入实体类
			// scp = (PoolingAcceptItem) wKAcceptDao.Map2Bean(map, scp);
			// if (scp.getId() != null && scp.getId().equals(""))
			// scp.setId(null);
			// scp.setWkAccept(sc);
			//
			// saveItems.add(scp);
		}
		// wKAcceptDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPOOLINGAcceptItem(String[] ids) throws Exception {
		for (String id : ids) {
			// PoolingAcceptItem scp = wKAcceptDao.get(PoolingAcceptItem.class,
			// id);
			// wKAcceptDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QcReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wKAcceptDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wKAcceptItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKAcceptItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("poolingAcceptItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savePOOLINGAcceptItem(sc, jsonStr);
			}
		}
	}

	public void changeState(String applicationTypeActionId, String id) {
		QcReceive wka = wKAcceptDao.get(QcReceive.class, id);
		wka.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		wka.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		// sct.setConfirmUser(user);
		// sct.setConfirmDate(new Date());

		wKAcceptDao.update(wka);
		String wkid = wka.getId();
		try {
			List<QcReceiveItem> wki = wKAcceptDao.findWKAcceptItemList(wkid);
			for (QcReceiveItem wkii : wki) {
				if (wkii.getResult().equals("1")) {
					// 根据文库类型，把数据存放到不同的临时表中
					// 类型是文库质控的，2100质控临时表和QPCR质控都传数据
					if (wkii.getWkType() != null
							&& wkii.getWkType().equals("0")) {
						Qc2100TaskTemp wqt = new Qc2100TaskTemp();
						wqt.setCode(wkii.getCode());
						wqt.setWkCode(wkii.getWkCode());
						wqt.setName(wkii.getName());
						wqt.setSampleCode(wkii.getSampleCode());
						wqt.setWkType("2");
						wqt.setWkVolume(wkii.getWkVolume());
						wqt.setFragminSize(wkii.getFragminSize());
						wqt.setResult(wkii.getResult());
						wqt.setReason(wkii.getReason());
						wqt.setNote(wkii.getNote());
						wqt.setPatientName(wkii.getPatientName());
						wqt.setProductId(wkii.getProductId());
						wqt.setProductName(wkii.getProductName());
						wqt.setInspectDate(wkii.getInspectDate());
						wqt.setAcceptDate(wkii.getAcceptDate());
						wqt.setIdCard(wkii.getIdCard());
						wqt.setPhone(wkii.getPhone());
						wqt.setOrderId(wkii.getOrderId());
						wqt.setSequenceFun(wkii.getSequenceFun());
						wqt.setReportDate(wkii.getReportDate());
						wqt.setLocation(wkii.getLocation());
						wqt.setState("1");

						wqt.setRowCode(wkii.getRowCode());
						wqt.setColCode(wkii.getColCode());
						if (wkii.getCounts() != null) {
							wqt.setCounts(wkii.getCounts().toString());
						} else {
							wqt.setCounts(null);
						}

						wqt.setOrderType(wkii.getOrderType());
						wqt.setProjectId(wkii.getProjectId());
						wqt.setTechTaskId(wkii.getTechTaskId());
						wqt.setContractId(wkii.getContractId());
						wqt.setClassify(wkii.getClassify());
						wqt.setSampleType(wkii.getSampleType());
						wqt.setI5(wkii.getI5());
						wqt.setI7(wkii.getI7());
						wqt.setIndexa(wkii.getIndexa());
						wqt.setSampleNum(wkii.getSampleNum());
						wKQualitySampleTaskDao.saveOrUpdate(wqt);

						QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
						wkqt.setCode(wkii.getCode());
						wkqt.setWkCode(wkii.getWkCode());
						wkqt.setName(wkii.getName());
						wkqt.setSampleCode(wkii.getSampleCode());
						wkqt.setWkType("1");
						wkqt.setWkVolume(wkii.getWkVolume());
						wkqt.setFragminSize(wkii.getFragminSize());
						wkqt.setResult(wkii.getResult());
						wkqt.setReason(wkii.getReason());
						wkqt.setNote(wkii.getNote());
						wkqt.setPatientName(wkii.getPatientName());
						wkqt.setProductId(wkii.getProductId());
						wkqt.setProductName(wkii.getProductName());
						wkqt.setInspectDate(wkii.getInspectDate());
						wkqt.setAcceptDate(wkii.getAcceptDate());
						wkqt.setIdCard(wkii.getIdCard());
						wkqt.setPhone(wkii.getPhone());
						wkqt.setOrderId(wkii.getOrderId());
						wkqt.setSequenceFun(wkii.getSequenceFun());
						wkqt.setReportDate(wkii.getReportDate());
						wkqt.setLocation(wkii.getLocation());
						wkqt.setState("1");
						wkqt.setRowCode(wkii.getRowCode());
						wkqt.setColCode(wkii.getColCode());
						// wkqt.setCounts(wkii.getCounts().toString());
						if (wkii.getCounts() != null) {
							wkqt.setCounts(wkii.getCounts().toString());
						} else {
							wkqt.setCounts(null);
						}
						wkqt.setOrderType(wkii.getOrderType());
						wkqt.setProjectId(wkii.getProjectId());
						wkqt.setTechTaskId(wkii.getTechTaskId());
						wkqt.setContractId(wkii.getContractId());
						wkqt.setClassify(wkii.getClassify());
						wkqt.setSampleType(wkii.getSampleType());
						wkqt.setI5(wkii.getI5());
						wkqt.setI7(wkii.getI7());
						wkqt.setIndexa(wkii.getIndexa());
						wkqt.setSampleNum(wkii.getSampleNum());
						wKQpcrSampleTaskDao.saveOrUpdate(wkqt);
						// {审批完成后，改变SampleInfo中原始样本的状态为“待2100质检”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(wkii
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QC_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_QC_NEW_NAME);
						}

					} else if (wkii.getWkType() != null
							&& wkii.getWkType().equals("1")) {
						// QPCR质控临时表
						QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
						wkqt.setCode(wkii.getCode());
						wkqt.setWkCode(wkii.getWkCode());
						wkqt.setName(wkii.getName());
						wkqt.setSampleCode(wkii.getSampleCode());
						wkqt.setWkType("1");
						wkqt.setWkVolume(wkii.getWkVolume());
						wkqt.setFragminSize(wkii.getFragminSize());
						wkqt.setResult(wkii.getResult());
						wkqt.setReason(wkii.getReason());
						wkqt.setNote(wkii.getNote());
						wkqt.setPatientName(wkii.getPatientName());
						wkqt.setProductId(wkii.getProductId());
						wkqt.setProductName(wkii.getProductName());
						wkqt.setInspectDate(wkii.getInspectDate());
						wkqt.setAcceptDate(wkii.getAcceptDate());
						wkqt.setIdCard(wkii.getIdCard());
						wkqt.setPhone(wkii.getPhone());
						wkqt.setOrderId(wkii.getOrderId());
						wkqt.setSequenceFun(wkii.getSequenceFun());
						wkqt.setReportDate(wkii.getReportDate());
						wkqt.setLocation(wkii.getLocation());
						wkqt.setState("1");
						wkqt.setRowCode(wkii.getRowCode());
						wkqt.setColCode(wkii.getColCode());
						// wkqt.setCounts(wkii.getCounts().toString());
						if (wkii.getCounts() != null) {
							wkqt.setCounts(wkii.getCounts().toString());
						} else {
							wkqt.setCounts(null);
						}
						wkqt.setOrderType(wkii.getOrderType());
						wkqt.setProjectId(wkii.getProjectId());
						wkqt.setTechTaskId(wkii.getTechTaskId());
						wkqt.setContractId(wkii.getContractId());
						wkqt.setClassify(wkii.getClassify());
						wkqt.setSampleType(wkii.getSampleType());
						wkqt.setI5(wkii.getI5());
						wkqt.setI7(wkii.getI7());
						wkqt.setIndexa(wkii.getIndexa());
						wkqt.setSampleNum(wkii.getSampleNum());
						wKQpcrSampleTaskDao.saveOrUpdate(wkqt);
						// {审批完成后，改变SampleInfo中原始样本的状态为“待QPCR质控”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(wkii
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_NEW_NAME);
						}
						// ---------------------------------}

					} else {
						// 2100 Caliper质控临时表
						Qc2100TaskTemp wqt = new Qc2100TaskTemp();
						wqt.setCode(wkii.getCode());
						wqt.setWkCode(wkii.getWkCode());
						wqt.setName(wkii.getName());
						wqt.setSampleCode(wkii.getSampleCode());
						wqt.setWkType("2");
						wqt.setWkVolume(wkii.getWkVolume());
						wqt.setFragminSize(wkii.getFragminSize());
						wqt.setResult(wkii.getResult());
						wqt.setReason(wkii.getReason());
						wqt.setNote(wkii.getNote());
						wqt.setPatientName(wkii.getPatientName());
						wqt.setProductId(wkii.getProductId());
						wqt.setProductName(wkii.getProductName());
						wqt.setInspectDate(wkii.getInspectDate());
						wqt.setAcceptDate(wkii.getAcceptDate());
						wqt.setIdCard(wkii.getIdCard());
						wqt.setPhone(wkii.getPhone());
						wqt.setOrderId(wkii.getOrderId());
						wqt.setSequenceFun(wkii.getSequenceFun());
						wqt.setReportDate(wkii.getReportDate());
						wqt.setLocation(wkii.getLocation());
						wqt.setState("1");
						wqt.setRowCode(wkii.getRowCode());
						wqt.setColCode(wkii.getColCode());
						// wqt.setCounts(wkii.getCounts().toString());
						if (wkii.getCounts() != null) {
							wqt.setCounts(wkii.getCounts().toString());
						} else {
							wqt.setCounts(null);
						}
						wqt.setOrderType(wkii.getOrderType());
						wqt.setProjectId(wkii.getProjectId());
						wqt.setTechTaskId(wkii.getTechTaskId());
						wqt.setContractId(wkii.getContractId());
						wqt.setClassify(wkii.getClassify());
						wqt.setSampleType(wkii.getSampleType());
						wqt.setI5(wkii.getI5());
						wqt.setI7(wkii.getI7());
						wqt.setIndexa(wkii.getIndexa());
						wqt.setSampleNum(wkii.getSampleNum());
						wKQualitySampleTaskDao.saveOrUpdate(wqt);
					}
					// 将临时表的样本状态，设为不可查
					// List<QcReceiveTemp> tmp =
					// wKAcceptDao.findWKAcceptTemList();
					// for (QcReceiveTemp tmpp : tmp) {
					// if (tmpp.getCode() != null
					// && !tmpp.getCode().equals("")) {
					// if (tmpp.getCode().equals(wkii.getCode())) {
					// tmpp.setState("2");
					// } else {
					// tmpp.setState("1");
					// }
					// }
					// }
					QcReceiveTemp temp = commonDAO.get(QcReceiveTemp.class,
							wkii.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 查询临时表
	public Map<String, Object> findWKAcceptTemList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKAcceptDao.findWKAcceptTemList(startNum,
				limitNum, dir, sort);
		List<QcReceiveTemp> list = (List<QcReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的任务单查询相关的样本
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setTempByOrder(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKAcceptDao.setTempByOrder(code);
		List<TechCheckServiceQcTemp> list = (List<TechCheckServiceQcTemp>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (TechCheckServiceQcTemp t : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", t.getId());
				map.put("wkCode", t.getWkCode());
				map.put("sampleCode", t.getSampleCode());
				map.put("molarResult", t.getMolarResult());
				map.put("orderId", t.getOrderId());
				map.put("orderType", t.getOrderType());
				map.put("pdResult", t.getPdResult());
				if (t.getMassCon() != null) {
					map.put("massCon", t.getMassCon().toString());
				} else {
					map.put("massCon", "");
				}

				if (t.getMolarCon() != null) {
					map.put("molarCon", t.getMolarCon().toString());
				} else {
					map.put("molarCon", "");
				}

				if (t.getPdSzie() != null) {
					map.put("pdSize", t.getPdSzie().toString());
				} else {
					map.put("pdSize", "");
				}

				if (t.getVolume() != null) {
					map.put("volume", t.getVolume().toString());
				} else {
					map.put("volume", "");
				}
				mapList.add(map);
			}

		}
		return mapList;
	}
}
