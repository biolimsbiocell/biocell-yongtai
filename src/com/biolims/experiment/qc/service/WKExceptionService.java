package com.biolims.experiment.qc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.qc.dao.WKExceptionDao;
import com.biolims.experiment.qc.dao.WKQpcrSampleTaskDao;
import com.biolims.experiment.qc.dao.WKQualitySampleTaskDao;
import com.biolims.experiment.qc.model.Qc2100Abnormal;
import com.biolims.experiment.qc.model.QcAbnormal;
import com.biolims.experiment.qc.model.QcQpcrAbnormal;
import com.biolims.experiment.qc.model.SampleQcPoolingInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.project.feedback.dao.QualityFeedbackDao;
import com.biolims.project.feedback.model.FeedbackQpcrQuality;
import com.biolims.project.feedback.model.FeedbackQuality;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKExceptionService {
	@Resource
	private WKExceptionDao wKExceptionDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WKQualitySampleTaskDao wKQualitySampleTaskDao;
	@Resource
	private WKQpcrSampleTaskDao wKQpcrSampleTaskDao;
	@Resource
	private QualityFeedbackDao qualityFeedbackDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QcAbnormal i) throws Exception {

		wKExceptionDao.saveOrUpdate(i);

	}

	public QcAbnormal get(String id) {
		QcAbnormal wKException = commonDAO.get(QcAbnormal.class, id);
		return wKException;
	}

	// QC2100质控异常
	public Map<String, Object> findQc2100AbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKExceptionDao.findQc2100AbnormalList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// QPCR质控异常
	public Map<String, Object> findQcQpcrAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKExceptionDao.findQcQpcrAbnormalList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findWKQualityExceptionList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKExceptionDao.selectWKQualityExceptionList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findSampleQcQpcrInfoList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKExceptionDao.selectSampleQcQpcrInfoList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findQcPoolingAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKExceptionDao.selectQcPoolingAbnormalList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	// 保存pooling异常
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveQcPoolingAbnormal(String itemDataJson) throws Exception {
//		List<SampleQcPoolingInfo> saveItems = new ArrayList<SampleQcPoolingInfo>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			SampleQcPoolingInfo sbi = new SampleQcPoolingInfo();
//			sbi = (SampleQcPoolingInfo) wKExceptionDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//			sbi.setState("3");
//			saveItems.add(sbi);
//			if (sbi != null && sbi.getIsExecute() != null) {
//				// 确认执行后，pooling数据传送到上机测序临时表
//				if (sbi.getIsExecute().equals("1")) {
//					SequencingTaskTemp st = new SequencingTaskTemp();
//					st.setCode(sbi.getPoolingCode());
//					st.setCode(sbi.getCode());
//					st.setSampleCode(sbi.getSampleCode());
//					st.setSequenceType(sbi.getSequencingType());
//					st.setTemplate(sbi.getTemplate());
//					st.setSequencingReadLong(sbi.getSequencingReadLong());
//					st.setSequencingPlatform(sbi.getSequencingPlatform());
//					st.setProduct(sbi.getProduct());
//					st.setPatientName(sbi.getPatientName());
//					st.setProductId(sbi.getProductId());
//					st.setProductName(sbi.getProductName());
//					st.setInspectDate(sbi.getInspectDate());
//					st.setAcceptDate(sbi.getAcceptDate());
//					st.setIdCard(sbi.getIdCard());
//					st.setPhone(sbi.getPhone());
//					st.setOrderId(sbi.getOrderId());
//					st.setSequenceFun(sbi.getSequenceFun());
//					st.setReportDate(sbi.getReportDate());
//					st.setState("1");
//
//					wKExceptionDao.saveOrUpdate(st);
//				}
//
//			}
//			wKExceptionDao.saveOrUpdateAll(saveItems);
//		}
//	}

	// 保存2100异常
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveWKQualityException(String itemDataJson) throws Exception {
//		List<Qc2100Abnormal> saveItems = new ArrayList<Qc2100Abnormal>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			Qc2100Abnormal sbi = new Qc2100Abnormal();
//			sbi = (Qc2100Abnormal) wKExceptionDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//
//			if (sbi != null && !sbi.getResult().equals("")
//					&& !sbi.getIsRun().equals("")) {
//				// 确认执行
//				if (sbi.getIsRun().equals("1")) {
//					if (sbi.getResult().equals("1")) {// 合格
//						if (sbi.getNextFlowId().equals("0009")) {// 样本入库
//							SampleInItemTemp st = new SampleInItemTemp();
//							st.setCode(sbi.getCode());
//							st.setSampleCode(sbi.getSampleCode());
//							st.setNum(sbi.getSampleNum());
//							st.setState("1");
//							wKQpcrSampleTaskDao.saveOrUpdate(st);
//							// 入库，改变SampleInfo中原始样本的状态为“待入库”
//							SampleInfo sf = sampleInfoMainDao
//									.findSampleInfo(sbi.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//							}
//						} else if (sbi.getNextFlowId().equals("0011")) {// 重建库
//							WkAbnormalBack wka = new WkAbnormalBack();
//							sbi.setState("4");// 状态为4 在重建库中显示
//							sampleInputService.copy(wka, sbi);
//						} else if (sbi.getNextFlowId().equals("0010")) {// 重提取
//							DnaTaskAbnormal eda = new DnaTaskAbnormal();
//							sbi.setState("4");// 状态为4 在重建库中显示
//							sampleInputService.copy(eda, sbi);
//						} else if (sbi.getNextFlowId().equals("0012")) {// 暂停
//							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
//							SampleInfo sf = sampleInfoMainDao
//									.findSampleInfo(sbi.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//							}
//						} else if (sbi.getNextFlowId().equals("0013")) {// 终止
//							// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//							SampleInfo sf = sampleInfoMainDao
//									.findSampleInfo(sbi.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//							}
//						} else if (sbi.getNextFlowId().equals("0014")) {// 反馈项目组
//							// 下一步流向是：反馈至项目组
//							FeedbackQuality fd = new FeedbackQuality();
//							sbi.setState("1");
//							sampleInputService.copy(fd, sbi);
//							fd.setSubmit("");
//							this.wKExceptionDao.saveOrUpdate(fd);
//						} else if (sbi.getNextFlowId().equals("0015")) {// pooling
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							sbi.setState("1");
//							sampleInputService.copy(ptt, sbi);
//							ptt.setWkIndex(sbi.getIndexa());
//							this.wKExceptionDao.saveOrUpdate(ptt);
//						}
//						sbi.setState("2");
//					}
//				}
//			}
//			saveItems.add(sbi);
//		}
//		wKExceptionDao.saveOrUpdateAll(saveItems);
//	}

	// 保存QPCR异常
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveWKQpcrException(String itemDataJson) throws Exception {
//		List<QcQpcrAbnormal> saveItems = new ArrayList<QcQpcrAbnormal>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			QcQpcrAbnormal sbi = new QcQpcrAbnormal();
//			sbi = (QcQpcrAbnormal) wKExceptionDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//
//			if (sbi != null && !sbi.getIsRun().equals("")
//					&& !sbi.getResult().equals("")) {
//				// 确认执行
//				if (sbi.getIsRun().equals("1")) {
//					if (sbi.getResult().equals("1")) {// 合格
//						// 合格
//						if (sbi.getNextFlowId().equals("0009")) {// 样本入库
//							SampleInItemTemp st = new SampleInItemTemp();
//							st.setCode(sbi.getCode());
//							st.setSampleCode(sbi.getSampleCode());
//							st.setNum(sbi.getSampleNum());
//							st.setState("1");
//							wKQpcrSampleTaskDao.saveOrUpdate(st);
//							// 入库，改变SampleInfo中原始样本的状态为“待入库”
//							SampleInfo sf = sampleInfoMainDao
//									.findSampleInfo(sbi.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//							}
//						} else if (sbi.getNextFlowId().equals("0011")) {// 重建库
//							WkAbnormalBack wka = new WkAbnormalBack();
//							sbi.setState("4");// 状态为4 在重建库中显示
//							sampleInputService.copy(wka, sbi);
//						} else if (sbi.getNextFlowId().equals("0010")) {// 重提取
//							DnaTaskAbnormal eda = new DnaTaskAbnormal();
//							sbi.setState("4");// 状态为4 在重建库中显示
//							sampleInputService.copy(eda, sbi);
//						} else if (sbi.getNextFlowId().equals("0012")) {// 暂停
//							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
//							SampleInfo sf = sampleInfoMainDao
//									.findSampleInfo(sbi.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//							}
//						} else if (sbi.getNextFlowId().equals("0013")) {// 终止
//							// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//							SampleInfo sf = sampleInfoMainDao
//									.findSampleInfo(sbi.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//							}
//						} else if (sbi.getNextFlowId().equals("0014")) {// 反馈项目组
//							// 下一步流向是：反馈至项目组
//							FeedbackQpcrQuality fd = new FeedbackQpcrQuality();
//							sbi.setState("1");
//							sampleInputService.copy(fd, sbi);
//							fd.setSubmit("");
//							this.wKExceptionDao.saveOrUpdate(fd);
//						} else if (sbi.getNextFlowId().equals("0015")) {// pooling
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							sbi.setState("1");
//							sampleInputService.copy(ptt, sbi);
//							ptt.setWkIndex(sbi.getIndexa());
//							this.wKQpcrSampleTaskDao.saveOrUpdate(ptt);
//						}
//						sbi.setState("2");
//					}
//				}
//
//			}
//			saveItems.add(sbi);
//		}
//		wKExceptionDao.saveOrUpdateAll(saveItems);
//
//	}
}
