package com.biolims.experiment.qc.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.qc.dao.QualityCheckDao;
import com.biolims.experiment.qc.dao.WKAcceptDao;
import com.biolims.experiment.qc.dao.WKExceptionDao;
import com.biolims.experiment.qc.dao.WKQpcrSampleTaskDao;
import com.biolims.experiment.qc.dao.WKQualitySampleTaskDao;
import com.biolims.experiment.qc.model.QcCheck;
import com.biolims.experiment.qc.model.QcPoolingTaskItem;
import com.biolims.experiment.qc.model.QcPoolingTaskTemp;
import com.biolims.experiment.qc.model.QcQpcrAbnormal;
import com.biolims.experiment.qc.model.QcQpcrTask;
import com.biolims.experiment.qc.model.QcQpcrTaskCos;
import com.biolims.experiment.qc.model.QcQpcrTaskItem;
import com.biolims.experiment.qc.model.QcQpcrTaskReagent;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemplate;
import com.biolims.experiment.qc.model.SampleQcPoolingInfo;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.sequencing.dao.SequencingTaskDao;
import com.biolims.experiment.sequencing.model.SequencingTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackQpcrQuality;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.main.service.StorageService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKQpcrSampleTaskService {
	@Resource
	private SequencingTaskDao sequencingTaskDao;
	@Resource
	private WKQpcrSampleTaskDao wKQpcrSampleTaskDao;
	@Resource
	private QualityCheckDao qualityCheckDao;
	@Resource
	private WKQualitySampleTaskDao wKQualitySampleTaskDao;
	@Resource
	private WKAcceptDao wKAcceptDao;
	@Resource
	private WKExceptionDao wKExceptionDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private StorageService storageService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWKQpcrList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return wKQpcrSampleTaskDao.selectWKQpcrList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QcQpcrTask i) throws Exception {

		wKQpcrSampleTaskDao.saveOrUpdate(i);

	}

	public QcQpcrTask get(String id) {
		QcQpcrTask wKQpcr = commonDAO.get(QcQpcrTask.class, id);
		return wKQpcr;
	}

	public Map<String, Object> findWKQpcrItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao.selectWKQpcrItemList(
				scId, startNum, limitNum, dir, sort);
		List<QcQpcrTaskItem> list = (List<QcQpcrTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findQcQpcrTaskItemmList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectQcPoolingTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<QcPoolingTaskItem> list = (List<QcPoolingTaskItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWKQpcrTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectWKQpcrTemplateList(scId, startNum, limitNum, dir, sort);
		List<QcQpcrTaskTemplate> list = (List<QcQpcrTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWKQpcrDeviceList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectWKQpcrDeviceList(scId, startNum, limitNum, dir, sort);
		List<QcQpcrTaskCos> list = (List<QcQpcrTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKQpcrResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectWKQpcrResultList(scId, startNum, limitNum, dir, sort);
		List<SampleQcQpcrInfo> list = (List<SampleQcQpcrInfo>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSampleQcPoolingInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectSampleQcPoolingInfoList(scId, startNum, limitNum, dir,
						sort);
		List<SampleQcPoolingInfo> list = (List<SampleQcPoolingInfo>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWKQpcrReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectWKQpcrReagentList(scId, startNum, limitNum, dir, sort);
		List<QcQpcrTaskReagent> list = (List<QcQpcrTaskReagent>) result
				.get("list");
		return result;
	}

	// 根据关联item查询
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectReagentItemListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		List<ReagentItem> list = (List<ReagentItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wKQpcrSampleTaskDao
				.selectCosItemListByItemId(scId, startNum, limitNum, dir, sort,
						itemId);
		List<CosItem> list = (List<CosItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKQpcrItem(QcQpcrTask sc, String itemDataJson)
			throws Exception {
		List<QcQpcrTaskItem> saveItems = new ArrayList<QcQpcrTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);

		String index1 = wKQpcrSampleTaskDao.getIndexMax();
		List<QcQpcrTask> list1 = wKQpcrSampleTaskDao.getIndexValue(sc.getId());
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = wKQpcrSampleTaskDao.get(Template.class, sc.getTemplate()
					.getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = wKQpcrSampleTaskDao.get(
						StorageContainer.class, temp.getStorageContainer()
								.getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();
			}
		}
		// String index2="";
		// for(QcQpcrTask w:list1){
		// index2=w.getIndexa();
		// }
		// Integer index3=wKQpcrSampleTaskDao.getIndexNum();
		// if(index3==null){
		// index3=0;
		// }
		// Integer count=1;
		// boolean flag=true;
		for (Map<String, Object> map : list) {
			QcQpcrTaskItem scp = new QcQpcrTaskItem();
			// 将map信息读入实体类
			scp = (QcQpcrTaskItem) wKQpcrSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQcQpcrTask(sc);
			Template t = templateDao.get(Template.class, sc.getTemplate()
					.getId());
			if (t.getSampleNum() != null) {
				if (scp.getSampleConsume() == null) {
					scp.setSampleConsume(t.getSampleNum());
				}
			}

			saveItems.add(scp);
		}
		wKQpcrSampleTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = wKQpcrSampleTaskDao.get(Template.class, sc.getTemplate()
					.getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(wKQpcrSampleTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrItem(String[] ids) throws Exception {
		for (String id : ids) {
			QcQpcrTaskItem scp = wKQpcrSampleTaskDao.get(QcQpcrTaskItem.class,
					id);
			wKQpcrSampleTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQcQpcrTaskItemm(QcQpcrTask sc, String itemDataJson)
			throws Exception {
		List<QcPoolingTaskItem> saveItems = new ArrayList<QcPoolingTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QcPoolingTaskItem scp = new QcPoolingTaskItem();
			// 将map信息读入实体类
			scp = (QcPoolingTaskItem) wKQpcrSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQcQpcrTask(sc);

			saveItems.add(scp);
		}
		wKQpcrSampleTaskDao.saveOrUpdateAll(saveItems);
	}

//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveSampleQcPoolingInfo(QcQpcrTask sc, String itemDataJson)
//			throws Exception {
//		List<SampleQcPoolingInfo> saveItems = new ArrayList<SampleQcPoolingInfo>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			SampleQcPoolingInfo scp = new SampleQcPoolingInfo();
//			// 将map信息读入实体类
//			scp = (SampleQcPoolingInfo) wKQpcrSampleTaskDao.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			// scp.setState("1");
//			scp.setQcQpcrTask(sc);
//			// 将左下侧临时表的状态，设为无效
//			List<QcPoolingTaskTemp> qtp = wKQpcrSampleTaskDao
//					.findQcPoolingTaskTempList();
//			for (QcPoolingTaskTemp qtpp : qtp) {
//				if (scp.getPoolingCode().equals(qtpp.getPoolingCode())) {
//
//					qtpp.setState("2");
//				}
//			}
//			saveItems.add(scp);
//			if (scp != null) {
//				if (!scp.getSubmit().equals("") && scp.getSubmit().equals("1")
//						&& !scp.getResult().equals("")) {
//					if (scp.getResult().equals("1")) {
//						// 合格的pooling，数据传送到上机测序临时表
//						SequencingTaskTemp st = new SequencingTaskTemp();
//						WorkOrder wo = this.wKQpcrSampleTaskDao.get(
//								WorkOrder.class, scp.getOrderId());
//						st.setPoolingCode(scp.getPoolingCode());
//						// st.setCode(scp.getCode());
//						// st.setSampleCode(scp.getSampleCode());
//						st.setSequenceType(scp.getSequencingType());
//						st.setTemplate(scp.getTemplate());
//						st.setSequencingReadLong(scp.getSequencingReadLong());
//						st.setSequencingPlatform(scp.getSequencingPlatform());
//						st.setProduct(scp.getProduct());
//						st.setPatientName(scp.getPatientName());
//						st.setProductId(scp.getProductId());
//						st.setProductName(scp.getProductName());
//						st.setInspectDate(scp.getInspectDate());
//						st.setAcceptDate(scp.getAcceptDate());
//						st.setIdCard(scp.getIdCard());
//						st.setPhone(scp.getPhone());
//						st.setOrderId(scp.getOrderId());
//						st.setSequenceFun(scp.getSequenceFun());
//						st.setReportDate(scp.getReportDate());
//						st.setState("1");
//
//						st.setYw(wo.getYw());// 引物
//						st.setMixRatio(wo.getMixRatio());// 混合比例
//						st.setDensity(wo.getDensity());// 预期密度
//						sequencingDao.saveOrUpdate(st);
//					} else {
//						// 不合格的POOLING，数据传送到异常界面
//						scp.setState("3");
//					}
//				}
//			}
//		}
//		wKQpcrSampleTaskDao.saveOrUpdateAll(saveItems);
//	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQcQpcrTaskItemm(String[] ids) throws Exception {
		for (String id : ids) {
			QcPoolingTaskItem scp = wKQpcrSampleTaskDao.get(
					QcPoolingTaskItem.class, id);
			wKQpcrSampleTaskDao.delete(scp);
		}
	}

	// ===========2015-12-02 ly==============
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrDeviceOne(String ids) throws Exception {
		QcQpcrTaskCos scp = wKQpcrSampleTaskDao.get(QcQpcrTaskCos.class, ids);
		wKQpcrSampleTaskDao.delete(scp);
	}

	// =========================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKQpcrTemplate(QcQpcrTask sc, String itemDataJson)
			throws Exception {
		List<QcQpcrTaskTemplate> saveItems = new ArrayList<QcQpcrTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QcQpcrTaskTemplate scp = new QcQpcrTaskTemplate();
			// 将map信息读入实体类
			scp = (QcQpcrTaskTemplate) wKQpcrSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQcQpcrTask(sc);

			saveItems.add(scp);
		}
		wKQpcrSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			QcQpcrTaskTemplate scp = wKQpcrSampleTaskDao.get(
					QcQpcrTaskTemplate.class, id);
			wKQpcrSampleTaskDao.delete(scp);
		}
	}

	// ================2015-12-02 ly==============
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrTemplateOne(String ids) throws Exception {
		QcQpcrTaskTemplate scp = wKQpcrSampleTaskDao.get(
				QcQpcrTaskTemplate.class, ids);
		wKQpcrSampleTaskDao.delete(scp);
	}

	// ==============================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKQpcrDevice(QcQpcrTask sc, String itemDataJson)
			throws Exception {
		List<QcQpcrTaskCos> saveItems = new ArrayList<QcQpcrTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QcQpcrTaskCos scp = new QcQpcrTaskCos();
			// 将map信息读入实体类
			scp = (QcQpcrTaskCos) wKQpcrSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQcQpcrTask(sc);

			saveItems.add(scp);
		}
		wKQpcrSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrDevice(String[] ids) throws Exception {
		for (String id : ids) {
			QcQpcrTaskCos scp = wKQpcrSampleTaskDao
					.get(QcQpcrTaskCos.class, id);
			wKQpcrSampleTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKQpcrResult(QcQpcrTask sc, String itemDataJson)
			throws Exception {

		List<SampleQcQpcrInfo> saveItems = new ArrayList<SampleQcQpcrInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleQcQpcrInfo scp = new SampleQcQpcrInfo();
			// 将map信息读入实体类
			scp = (SampleQcQpcrInfo) wKQpcrSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setState("1");
			scp.setQcQpcrTask(sc);
			// 将左上侧临时表的状态，设为无效
			// List<QcQpcrTaskTemp> wkt = wKQpcrSampleTaskDao
			// .findWKQpcrTemptList();
			// for (QcQpcrTaskTemp wkti : wkt) {
			// if (scp.getCode().equals(wkti.getCode())) {
			// wkti.setState("2");
			// }
			// }

			// saveItems.add(scp);
			wKQpcrSampleTaskDao.saveOrUpdate(scp);

		}
	}

	/**
	 * 判断合格的QPCR质控的数据传到Pooling
	 * 
	 * @param scp
	 */
//	public void setToPoolingTemp(SampleQcQpcrInfo scp) {
//		// 流向:Pooling
//		PoolingTaskTemp pt = new PoolingTaskTemp();
//		WorkOrder wo = null;// 产前任务单
//		TechJkServiceTask t = null;// 科技服务任务单
//		if (scp.getClassify().equals("0")) {// 产前
//			if (scp.getOrderId() != null) {
//				wo = sampleInfoMainDao.get(WorkOrder.class, scp.getOrderId());
//				if (wo != null) {
//					pt.setSequencingPlatform(wo.getSequencingPlatform());
//					pt.setSequencingReadLong(wo.getSequencingReadLong());
//					pt.setSequencingType(wo.getSequencingMethod());
//					pt.setMixRatio(wo.getMixRatio());
//					pt.setYw(wo.getYw());
//					pt.setDensity(wo.getDensity());
//
//					pt.setSampleCode(scp.getSampleCode());
//					pt.setWkCode(scp.getWkId());
//
//					pt.setWkIndex(scp.getIndexa());
//					pt.setConcentration(scp.getQpcr());
//					pt.setOrderId(scp.getOrderId());
//					pt.setPatient(scp.getPatientName());
//					pt.setProductId(scp.getProductId());
//
//					pt.setProductName(scp.getProductName());
//					pt.setInspectDate(scp.getInspectDate());
//					pt.setAcceptDate(scp.getAcceptDate());
//					pt.setIdCard(scp.getIdCard());
//					pt.setPhone(scp.getPhone());
//
//					pt.setOrderId(scp.getOrderId());
//					pt.setReportDate(scp.getReportDate());
//					pt.setState("1");
//					pt.setQcId(scp.getQcQpcrTask().getId());
//					pt.setVolume(scp.getVolume());
//					pt.setClassify(scp.getClassify());
//					this.wKQpcrSampleTaskDao.saveOrUpdate(pt);
//				}
//			}
//		}
//		if (scp.getClassify().equals("1")) {// 科技服务
//			if (scp.getTechTaskId() != null) {
//				t = this.wkDao
//						.get(TechJkServiceTask.class, scp.getTechTaskId());
//				if (t != null) {
//					if (t.getOrderType().equals("1")) {// 任务单类型：建库-->质控完停止。
//						// scp.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_COMPLETE);
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_COMPLETE);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_COMPLETE);
//						}
//					} else {// 任务单类型：其他类型
//						pt.setSequencingPlatform(t.getSequencePlatform());
//						pt.setSequencingReadLong(t.getSequenceLength());
//						pt.setSequencingType(t.getSequenceType());
//						pt.setMixRatio(t.getMixRatio());
//						// pt.setYw(t.getYw());
//						pt.setDensity(t.getDensity());
//						// pt.setProjectId(t.getProjectId());
//						// pt.setOrderType(t.getOrderType());
//						// pt.setContractId(t.getContractId());
//						pt.setTechTaskId(scp.getTechTaskId());
//
//						pt.setSampleCode(scp.getSampleCode());
//						pt.setWkCode(scp.getWkId());
//
//						pt.setWkIndex(scp.getIndexa());
//						pt.setConcentration(scp.getQpcr());
//						pt.setOrderId(scp.getOrderId());
//						pt.setPatient(scp.getPatientName());
//						pt.setProductId(scp.getProductId());
//
//						pt.setProductName(scp.getProductName());
//						pt.setInspectDate(scp.getInspectDate());
//						pt.setAcceptDate(scp.getAcceptDate());
//						pt.setIdCard(scp.getIdCard());
//						pt.setPhone(scp.getPhone());
//
//						pt.setOrderId(scp.getOrderId());
//						pt.setReportDate(scp.getReportDate());
//						pt.setState("1");
//						pt.setQcId(scp.getQcQpcrTask().getId());
//						pt.setVolume(scp.getVolume());
//						pt.setClassify(scp.getClassify());
//						this.wKQpcrSampleTaskDao.saveOrUpdate(pt);
//					}
//				}
//			}
//		}
//
//		// pt.setContractId(scp.getContractId());
//		// pt.setOrderType(scp.getOrderType());
//		// pt.setProjectId(scp.getProjectId());
//		// pt.setTechTaskId(scp.getTechTaskId());
//		qualityCheckDao.saveOrUpdate(pt);
//		// 审批完成后，改变SampleInfo中原始样本的状态为“QPCR质检完成”
//		SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp.getSampleCode());
//		if (sf != null) {
//			sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_COMPLETE);
//			sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_QPCR_COMPLETE_NAME);
//		}
//	}

	/**
	 * 数据到质检审核
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	public void setToQcCheck(SampleQcQpcrInfo scp) {
		QcCheck qCheck = new QcCheck();
		qCheck.setWkCode(scp.getWkId());
		qCheck.setSampleCode(scp.getSampleCode());
		qCheck.setWkType(scp.getWkType());
		qCheck.setIndexa(scp.getIndexa());
		qCheck.setAcceptDate(scp.getAcceptDate());
		qCheck.setInspectDate(scp.getInspectDate());
		qCheck.setReportDate(scp.getReportDate());
		qCheck.setQpcrCon(scp.getQpcr());
		qCheck.setClassify(scp.getClassify());
		qualityCheckDao.saveOrUpdate(qCheck);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrResult(String[] ids) throws Exception {
		for (String id : ids) {
			SampleQcQpcrInfo scp = wKQpcrSampleTaskDao.get(
					SampleQcQpcrInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals(""))))
				wKQpcrSampleTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleQcPoolingInfo(String[] ids) throws Exception {
		for (String id : ids) {
			SampleQcPoolingInfo scp = wKQpcrSampleTaskDao.get(
					SampleQcPoolingInfo.class, id);
			wKQpcrSampleTaskDao.delete(scp);
		}
	}

	// 审批完成
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		QcQpcrTask wkq = wKQpcrSampleTaskDao.get(QcQpcrTask.class, id);
		wkq.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		wkq.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		// sct.setConfirmUser(user);
		// sct.setConfirmDate(new Date());

		wKQpcrSampleTaskDao.update(wkq);
		String mainid = wkq.getId();
		// 将批次信息反馈到模板中
		List<QcQpcrTaskReagent> list1 = wKQpcrSampleTaskDao.setReagentList(wkq
				.getId());
		for (QcQpcrTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); // qcQpcr实验中试剂的批次
			String drid = dt.gettReagent(); // qcQpcr实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
			// 改变库存主数据试剂数量
			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
					dt.getBatch(), dt.getNum());
		}
		// 改变左侧状态
		List<QcQpcrTaskItem> item = wKQpcrSampleTaskDao
				.setItemList(wkq.getId());
		for (QcQpcrTaskItem c : item) {
			QcQpcrTaskTemp temp = commonDAO.get(QcQpcrTaskTemp.class,
					c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
		submitSample(id, null);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKQpcrReagent(QcQpcrTask sc, String itemDataJson)
			throws Exception {
		List<QcQpcrTaskReagent> saveItems = new ArrayList<QcQpcrTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QcQpcrTaskReagent scp = new QcQpcrTaskReagent();
			// 将map信息读入实体类
			scp = (QcQpcrTaskReagent) wKQpcrSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQcQpcrTask(sc);
			// 用量 = 单个用量*数量
			if (scp != null && scp.getOneNum() != null
					&& scp.getSampleNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}
			saveItems.add(scp);
		}
		wKQpcrSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrReagent(String[] ids) throws Exception {
		for (String id : ids) {
			ReagentItem scp = wKQpcrSampleTaskDao.get(ReagentItem.class, id);
			wKQpcrSampleTaskDao.delete(scp);
		}
	}

	// =============2015-12-02 ly================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKQpcrReagentOne(String ids) throws Exception {
		QcQpcrTaskReagent scp = wKQpcrSampleTaskDao.get(
				QcQpcrTaskReagent.class, ids);
		wKQpcrSampleTaskDao.delete(scp);
	}

	// =============================
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
//	public void save(QcQpcrTask sc, Map jsonMap) throws Exception {
//		if (sc != null) {
//			wKQpcrSampleTaskDao.saveOrUpdate(sc);
//
//			String jsonStr = "";
//			jsonStr = (String) jsonMap.get("wKQpcrItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveWKQpcrItem(sc, jsonStr);
//			}
//			jsonStr = (String) jsonMap.get("qcQpcrTaskItemm");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveQcQpcrTaskItemm(sc, jsonStr);
//			}
//			jsonStr = (String) jsonMap.get("wKQpcrTemplate");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveWKQpcrTemplate(sc, jsonStr);
//			}
//			jsonStr = (String) jsonMap.get("wKQpcrDevice");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveWKQpcrDevice(sc, jsonStr);
//			}
//			jsonStr = (String) jsonMap.get("wKQpcrSampleInfo");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveWKQpcrResult(sc, jsonStr);
//			}
//			jsonStr = (String) jsonMap.get("sampleQcPoolingInfo");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveSampleQcPoolingInfo(sc, jsonStr);
//			}
//			jsonStr = (String) jsonMap.get("wKQpcrReagent");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveWKQpcrReagent(sc, jsonStr);
//			}
//		}
//	}

	public List<QcQpcrTaskItem> findQcQpcrItemList(String scId)
			throws Exception {
		List<QcQpcrTaskItem> list = wKQpcrSampleTaskDao
				.selectQcQpcrItemList(scId);
		return list;
	}

	// 根据选中的步骤查询相关的试剂明细
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKQpcrSampleTaskDao.setReagent(id, code);
		List<QcQpcrTaskReagent> list = (List<QcQpcrTaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (QcQpcrTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsTestSuccess());
				// map.put("note",ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getQcQpcrTask().getId());
				map.put("tName", ti.getQcQpcrTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据选中的步骤查询相关的设备明细
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKQpcrSampleTaskDao.setCos(id, code);
		List<QcQpcrTaskCos> list = (List<QcQpcrTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (QcQpcrTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsTestSuccess());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}

				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getQcQpcrTask().getId());
				map.put("tName", ti.getQcQpcrTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 提交样本
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		QcQpcrTask sc = this.wKQpcrSampleTaskDao.get(QcQpcrTask.class, id);
		// 获取结果表样本信息
		List<SampleQcQpcrInfo> list = this.wKQpcrSampleTaskDao
				.findWKQpcrResultList(id);
		if (ids == null)
			list = this.wKQpcrSampleTaskDao.setQcQpcrResultById(id);
		else
			list = this.wKQpcrSampleTaskDao.setQcQpcrResultByIds(ids);
		for (SampleQcQpcrInfo scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))
						&& scp.getNextFlowId() != null) {
					if (scp.getResult().equals("1")) {// 合格
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setSampleType(scp.getSampleType());
							DicSampleType d = dicSampleTypeDao
									.selectDicSampleTypeByName(scp
											.getSampleType());
							st.setSampleTypeId(d.getId());
							st.setInfoFrom("SampleQcQpcrInfo");
							st.setState("1");
							st.setConcentration(scp.getWkConcentration());
							st.setVolume(scp.getWkVolume());
							st.setSumTotal(scp.getWkSumTotal());
							SampleInfo s = this.sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							st.setPatientName(s.getPatientName());
							wKQpcrSampleTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
//							WkAbnormalBack wka = new WkAbnormalBack();
//							scp.setState("4");
//							sampleInputService.copy(wka, scp);
//							wka.setWkCode(scp.getWkId());
//							wKQpcrSampleTaskDao.saveOrUpdate(wka);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(eda, scp);
							// wKQpcrSampleTaskDao.saveOrUpdate(eda);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else if (scp.getNextFlowId().equals("0015")) {// pooling
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							scp.setState("1");
//							sampleInputService.copy(ptt, scp);
//							ptt.setWkIndex(scp.getIndexa());
//							this.wKQpcrSampleTaskDao.saveOrUpdate(ptt);
						}
					} else {// 不合格
						if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
							// 下一步流向是：反馈至项目组
							FeedbackQpcrQuality fd = new FeedbackQpcrQuality();
							scp.setState("1");
							sampleInputService.copy(fd, scp);
							fd.setSubmit("");
							fd.setWkCode(scp.getWkId());
							this.wKQpcrSampleTaskDao.saveOrUpdate(fd);
						} else {
							QcQpcrAbnormal qqa = new QcQpcrAbnormal();
							scp.setState("1");
							sampleInputService.copy(qqa, scp);
							qqa.setWkCode(scp.getWkId());
							wKQpcrSampleTaskDao.saveOrUpdate(qqa);
						}

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getReleaseDate(),
							format.format(new Date()),
							"QcQpcrTask",
							"QPCR质控",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
				}
			}
		}
	}
}
